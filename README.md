# ONE PIECE TREASURE CRUISE

Source code for the game one piece treasure cruise.
The following source code is extracted by decompiling the 8.2.4 apk using `apktool`

The `smali` files were expanded using `dex2jar` and the source was dumped via a jar decompiler.

Edits can be made via any Java based IDE such as `IntelliJ`

The repository exists purely for educational purposes.

The source code of the files belong to Namco Bandai and the Treasure Cruise team.
All credit in regards to this repository goes to them. This repository is just maintaining the source code to observe version differences at the source code level