package twitter4j;

import java.io.Serializable;
import java.util.List;

public abstract interface QueryResult
  extends TwitterResponse, Serializable
{
  public abstract double getCompletedIn();
  
  public abstract int getCount();
  
  public abstract long getMaxId();
  
  public abstract String getQuery();
  
  public abstract String getRefreshURL();
  
  public abstract long getSinceId();
  
  public abstract List<Status> getTweets();
  
  public abstract boolean hasNext();
  
  public abstract Query nextQuery();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\QueryResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */