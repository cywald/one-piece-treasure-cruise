package twitter4j.api;

public abstract interface SavedSearchesResourcesAsync
{
  public abstract void createSavedSearch(String paramString);
  
  public abstract void destroySavedSearch(int paramInt);
  
  public abstract void getSavedSearches();
  
  public abstract void showSavedSearch(int paramInt);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\SavedSearchesResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */