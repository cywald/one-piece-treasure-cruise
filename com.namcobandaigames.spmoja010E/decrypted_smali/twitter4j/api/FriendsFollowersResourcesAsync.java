package twitter4j.api;

public abstract interface FriendsFollowersResourcesAsync
{
  public abstract void createFriendship(long paramLong);
  
  public abstract void createFriendship(long paramLong, boolean paramBoolean);
  
  public abstract void createFriendship(String paramString);
  
  public abstract void createFriendship(String paramString, boolean paramBoolean);
  
  public abstract void destroyFriendship(long paramLong);
  
  public abstract void destroyFriendship(String paramString);
  
  public abstract void getFollowersIDs(long paramLong);
  
  public abstract void getFollowersIDs(long paramLong1, long paramLong2);
  
  public abstract void getFollowersIDs(String paramString, long paramLong);
  
  public abstract void getFollowersList(long paramLong1, long paramLong2);
  
  public abstract void getFollowersList(String paramString, long paramLong);
  
  public abstract void getFriendsIDs(long paramLong);
  
  public abstract void getFriendsIDs(long paramLong1, long paramLong2);
  
  public abstract void getFriendsIDs(String paramString, long paramLong);
  
  public abstract void getFriendsList(long paramLong1, long paramLong2);
  
  public abstract void getFriendsList(String paramString, long paramLong);
  
  public abstract void getIncomingFriendships(long paramLong);
  
  public abstract void getOutgoingFriendships(long paramLong);
  
  public abstract void lookupFriendships(long[] paramArrayOfLong);
  
  public abstract void lookupFriendships(String[] paramArrayOfString);
  
  public abstract void showFriendship(long paramLong1, long paramLong2);
  
  public abstract void showFriendship(String paramString1, String paramString2);
  
  public abstract void updateFriendship(long paramLong, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract void updateFriendship(String paramString, boolean paramBoolean1, boolean paramBoolean2);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\FriendsFollowersResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */