package twitter4j.api;

public abstract interface SuggestedUsersResourcesAsync
{
  public abstract void getMemberSuggestions(String paramString);
  
  public abstract void getSuggestedUserCategories();
  
  public abstract void getUserSuggestions(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\SuggestedUsersResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */