package twitter4j.api;

import twitter4j.GeoLocation;
import twitter4j.TwitterException;

public abstract interface TrendsResourcesAsync
{
  public abstract void getAvailableTrends();
  
  public abstract void getClosestTrends(GeoLocation paramGeoLocation)
    throws TwitterException;
  
  public abstract void getPlaceTrends(int paramInt);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\TrendsResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */