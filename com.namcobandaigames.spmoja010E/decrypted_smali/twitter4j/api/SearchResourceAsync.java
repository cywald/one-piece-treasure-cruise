package twitter4j.api;

import twitter4j.Query;

public abstract interface SearchResourceAsync
{
  public abstract void search(Query paramQuery);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\SearchResourceAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */