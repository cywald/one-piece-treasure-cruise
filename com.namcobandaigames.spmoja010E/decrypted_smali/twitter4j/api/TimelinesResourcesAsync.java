package twitter4j.api;

import twitter4j.Paging;

public abstract interface TimelinesResourcesAsync
{
  public abstract void getHomeTimeline();
  
  public abstract void getHomeTimeline(Paging paramPaging);
  
  public abstract void getMentions();
  
  public abstract void getMentions(Paging paramPaging);
  
  public abstract void getRetweetsOfMe();
  
  public abstract void getRetweetsOfMe(Paging paramPaging);
  
  public abstract void getUserTimeline();
  
  public abstract void getUserTimeline(long paramLong);
  
  public abstract void getUserTimeline(long paramLong, Paging paramPaging);
  
  public abstract void getUserTimeline(String paramString);
  
  public abstract void getUserTimeline(String paramString, Paging paramPaging);
  
  public abstract void getUserTimeline(Paging paramPaging);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\TimelinesResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */