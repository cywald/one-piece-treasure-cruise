package twitter4j.api;

import twitter4j.Paging;

public abstract interface FavoritesResourcesAsync
{
  public abstract void createFavorite(long paramLong);
  
  public abstract void destroyFavorite(long paramLong);
  
  public abstract void getFavorites();
  
  public abstract void getFavorites(long paramLong);
  
  public abstract void getFavorites(long paramLong, Paging paramPaging);
  
  public abstract void getFavorites(String paramString);
  
  public abstract void getFavorites(String paramString, Paging paramPaging);
  
  public abstract void getFavorites(Paging paramPaging);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\FavoritesResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */