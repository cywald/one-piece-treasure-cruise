package twitter4j.api;

import twitter4j.GeoLocation;
import twitter4j.GeoQuery;

public abstract interface PlacesGeoResourcesAsync
{
  public abstract void getGeoDetails(String paramString);
  
  public abstract void getSimilarPlaces(GeoLocation paramGeoLocation, String paramString1, String paramString2, String paramString3);
  
  public abstract void reverseGeoCode(GeoQuery paramGeoQuery);
  
  public abstract void searchPlaces(GeoQuery paramGeoQuery);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\PlacesGeoResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */