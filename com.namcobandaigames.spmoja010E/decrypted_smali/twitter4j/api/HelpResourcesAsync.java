package twitter4j.api;

public abstract interface HelpResourcesAsync
{
  public abstract void getAPIConfiguration();
  
  public abstract void getLanguages();
  
  public abstract void getPrivacyPolicy();
  
  public abstract void getRateLimitStatus();
  
  public abstract void getRateLimitStatus(String... paramVarArgs);
  
  public abstract void getTermsOfService();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\HelpResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */