package twitter4j.api;

import twitter4j.Paging;

public abstract interface DirectMessagesResourcesAsync
{
  public abstract void destroyDirectMessage(long paramLong);
  
  public abstract void getDirectMessages();
  
  public abstract void getDirectMessages(Paging paramPaging);
  
  public abstract void getSentDirectMessages();
  
  public abstract void getSentDirectMessages(Paging paramPaging);
  
  public abstract void sendDirectMessage(long paramLong, String paramString);
  
  public abstract void sendDirectMessage(String paramString1, String paramString2);
  
  public abstract void showDirectMessage(long paramLong);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\DirectMessagesResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */