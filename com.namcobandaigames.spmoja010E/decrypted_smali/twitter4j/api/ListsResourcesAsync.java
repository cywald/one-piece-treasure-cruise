package twitter4j.api;

import twitter4j.Paging;

public abstract interface ListsResourcesAsync
{
  public abstract void createUserList(String paramString1, boolean paramBoolean, String paramString2);
  
  public abstract void createUserListMember(long paramLong1, long paramLong2);
  
  public abstract void createUserListMember(long paramLong1, String paramString, long paramLong2);
  
  public abstract void createUserListMembers(long paramLong, String paramString, long[] paramArrayOfLong);
  
  public abstract void createUserListMembers(long paramLong, String paramString, String[] paramArrayOfString);
  
  public abstract void createUserListMembers(long paramLong, long[] paramArrayOfLong);
  
  public abstract void createUserListMembers(long paramLong, String[] paramArrayOfString);
  
  public abstract void createUserListSubscription(long paramLong);
  
  public abstract void createUserListSubscription(long paramLong, String paramString);
  
  public abstract void destroyUserList(long paramLong);
  
  public abstract void destroyUserList(long paramLong, String paramString);
  
  public abstract void destroyUserListMember(long paramLong1, long paramLong2);
  
  public abstract void destroyUserListMember(long paramLong1, String paramString, long paramLong2);
  
  public abstract void destroyUserListSubscription(long paramLong);
  
  public abstract void destroyUserListSubscription(long paramLong, String paramString);
  
  public abstract void getUserListMembers(long paramLong1, long paramLong2);
  
  public abstract void getUserListMembers(long paramLong1, String paramString, long paramLong2);
  
  public abstract void getUserListMemberships(long paramLong);
  
  public abstract void getUserListMemberships(long paramLong1, long paramLong2);
  
  public abstract void getUserListMemberships(long paramLong1, long paramLong2, boolean paramBoolean);
  
  public abstract void getUserListMemberships(String paramString, long paramLong);
  
  public abstract void getUserListMemberships(String paramString, long paramLong, boolean paramBoolean);
  
  public abstract void getUserListStatuses(long paramLong, String paramString, Paging paramPaging);
  
  public abstract void getUserListStatuses(long paramLong, Paging paramPaging);
  
  public abstract void getUserListSubscribers(long paramLong1, long paramLong2);
  
  public abstract void getUserListSubscribers(long paramLong1, String paramString, long paramLong2);
  
  public abstract void getUserListSubscriptions(String paramString, long paramLong);
  
  public abstract void getUserLists(long paramLong);
  
  public abstract void getUserLists(String paramString);
  
  public abstract void showUserList(long paramLong);
  
  public abstract void showUserList(long paramLong, String paramString);
  
  public abstract void showUserListMembership(long paramLong1, long paramLong2);
  
  public abstract void showUserListMembership(long paramLong1, String paramString, long paramLong2);
  
  public abstract void showUserListSubscription(long paramLong1, long paramLong2);
  
  public abstract void showUserListSubscription(long paramLong1, String paramString, long paramLong2);
  
  public abstract void updateUserList(long paramLong, String paramString1, String paramString2, boolean paramBoolean, String paramString3);
  
  public abstract void updateUserList(long paramLong, String paramString1, boolean paramBoolean, String paramString2);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\ListsResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */