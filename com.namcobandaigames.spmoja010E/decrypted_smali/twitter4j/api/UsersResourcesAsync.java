package twitter4j.api;

import java.io.File;
import java.io.InputStream;

public abstract interface UsersResourcesAsync
{
  public abstract void createBlock(long paramLong);
  
  public abstract void createBlock(String paramString);
  
  public abstract void createMute(long paramLong);
  
  public abstract void createMute(String paramString);
  
  public abstract void destroyBlock(long paramLong);
  
  public abstract void destroyBlock(String paramString);
  
  public abstract void destroyMute(long paramLong);
  
  public abstract void destroyMute(String paramString);
  
  public abstract void getAccountSettings();
  
  public abstract void getBlocksIDs();
  
  public abstract void getBlocksIDs(long paramLong);
  
  public abstract void getBlocksList();
  
  public abstract void getBlocksList(long paramLong);
  
  public abstract void getContributees(long paramLong);
  
  public abstract void getContributees(String paramString);
  
  public abstract void getContributors(long paramLong);
  
  public abstract void getContributors(String paramString);
  
  public abstract void getMutesIDs(long paramLong);
  
  public abstract void getMutesList(long paramLong);
  
  public abstract void lookupUsers(long[] paramArrayOfLong);
  
  public abstract void lookupUsers(String[] paramArrayOfString);
  
  public abstract void removeProfileBanner();
  
  public abstract void searchUsers(String paramString, int paramInt);
  
  public abstract void showUser(long paramLong);
  
  public abstract void showUser(String paramString);
  
  public abstract void updateAccountSettings(Integer paramInteger, Boolean paramBoolean, String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void updateProfile(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void updateProfileBackgroundImage(File paramFile, boolean paramBoolean);
  
  public abstract void updateProfileBackgroundImage(InputStream paramInputStream, boolean paramBoolean);
  
  public abstract void updateProfileBanner(File paramFile);
  
  public abstract void updateProfileBanner(InputStream paramInputStream);
  
  public abstract void updateProfileColors(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract void updateProfileImage(File paramFile);
  
  public abstract void updateProfileImage(InputStream paramInputStream);
  
  public abstract void verifyCredentials();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\UsersResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */