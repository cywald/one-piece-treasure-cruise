package twitter4j.api;

public abstract interface SpamReportingResourceAsync
{
  public abstract void reportSpam(long paramLong);
  
  public abstract void reportSpam(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\SpamReportingResourceAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */