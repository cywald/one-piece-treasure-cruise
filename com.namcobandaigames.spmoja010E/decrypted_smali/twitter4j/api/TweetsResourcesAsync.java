package twitter4j.api;

import twitter4j.OEmbedRequest;
import twitter4j.StatusUpdate;

public abstract interface TweetsResourcesAsync
{
  public abstract void destroyStatus(long paramLong);
  
  public abstract void getOEmbed(OEmbedRequest paramOEmbedRequest);
  
  public abstract void getRetweets(long paramLong);
  
  public abstract void lookup(long[] paramArrayOfLong);
  
  public abstract void retweetStatus(long paramLong);
  
  public abstract void showStatus(long paramLong);
  
  public abstract void updateStatus(String paramString);
  
  public abstract void updateStatus(StatusUpdate paramStatusUpdate);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\api\TweetsResourcesAsync.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */