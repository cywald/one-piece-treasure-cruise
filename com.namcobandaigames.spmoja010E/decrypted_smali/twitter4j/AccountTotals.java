package twitter4j;

import java.io.Serializable;

public abstract interface AccountTotals
  extends TwitterResponse, Serializable
{
  public abstract int getFavorites();
  
  public abstract int getFollowers();
  
  public abstract int getFriends();
  
  public abstract int getUpdates();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\AccountTotals.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */