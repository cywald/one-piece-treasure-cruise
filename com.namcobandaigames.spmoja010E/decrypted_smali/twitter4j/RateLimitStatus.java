package twitter4j;

import java.io.Serializable;

public abstract interface RateLimitStatus
  extends Serializable
{
  public abstract int getLimit();
  
  public abstract int getRemaining();
  
  public abstract int getResetTimeInSeconds();
  
  public abstract int getSecondsUntilReset();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\RateLimitStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */