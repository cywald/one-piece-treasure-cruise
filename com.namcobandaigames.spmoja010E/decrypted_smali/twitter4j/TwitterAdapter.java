package twitter4j;

import java.util.Map;
import twitter4j.api.HelpResources.Language;
import twitter4j.auth.AccessToken;
import twitter4j.auth.OAuth2Token;
import twitter4j.auth.RequestToken;

public class TwitterAdapter
  implements TwitterListener
{
  public void checkedUserListMembership(User paramUser) {}
  
  public void checkedUserListSubscription(User paramUser) {}
  
  public void createdBlock(User paramUser) {}
  
  public void createdFavorite(Status paramStatus) {}
  
  public void createdFriendship(User paramUser) {}
  
  public void createdMute(User paramUser) {}
  
  public void createdSavedSearch(SavedSearch paramSavedSearch) {}
  
  public void createdUserList(UserList paramUserList) {}
  
  public void createdUserListMember(UserList paramUserList) {}
  
  public void createdUserListMembers(UserList paramUserList) {}
  
  public void destroyedBlock(User paramUser) {}
  
  public void destroyedDirectMessage(DirectMessage paramDirectMessage) {}
  
  public void destroyedFavorite(Status paramStatus) {}
  
  public void destroyedFriendship(User paramUser) {}
  
  public void destroyedMute(User paramUser) {}
  
  public void destroyedSavedSearch(SavedSearch paramSavedSearch) {}
  
  public void destroyedStatus(Status paramStatus) {}
  
  public void destroyedUserList(UserList paramUserList) {}
  
  public void destroyedUserListMember(UserList paramUserList) {}
  
  public void gotAPIConfiguration(TwitterAPIConfiguration paramTwitterAPIConfiguration) {}
  
  public void gotAccountSettings(AccountSettings paramAccountSettings) {}
  
  public void gotAvailableTrends(ResponseList<Location> paramResponseList) {}
  
  public void gotBlockIDs(IDs paramIDs) {}
  
  public void gotBlocksList(ResponseList<User> paramResponseList) {}
  
  public void gotClosestTrends(ResponseList<Location> paramResponseList) {}
  
  public void gotContributees(ResponseList<User> paramResponseList) {}
  
  public void gotContributors(ResponseList<User> paramResponseList) {}
  
  public void gotDirectMessage(DirectMessage paramDirectMessage) {}
  
  public void gotDirectMessages(ResponseList<DirectMessage> paramResponseList) {}
  
  public void gotFavorites(ResponseList<Status> paramResponseList) {}
  
  public void gotFollowersIDs(IDs paramIDs) {}
  
  public void gotFollowersList(PagableResponseList<User> paramPagableResponseList) {}
  
  public void gotFriendsIDs(IDs paramIDs) {}
  
  public void gotFriendsList(PagableResponseList<User> paramPagableResponseList) {}
  
  public void gotGeoDetails(Place paramPlace) {}
  
  public void gotHomeTimeline(ResponseList<Status> paramResponseList) {}
  
  public void gotIncomingFriendships(IDs paramIDs) {}
  
  public void gotLanguages(ResponseList<HelpResources.Language> paramResponseList) {}
  
  public void gotMemberSuggestions(ResponseList<User> paramResponseList) {}
  
  public void gotMentions(ResponseList<Status> paramResponseList) {}
  
  public void gotMuteIDs(IDs paramIDs) {}
  
  public void gotMutesList(ResponseList<User> paramResponseList) {}
  
  public void gotOAuth2Token(OAuth2Token paramOAuth2Token) {}
  
  public void gotOAuthAccessToken(AccessToken paramAccessToken) {}
  
  public void gotOAuthRequestToken(RequestToken paramRequestToken) {}
  
  public void gotOEmbed(OEmbed paramOEmbed) {}
  
  public void gotOutgoingFriendships(IDs paramIDs) {}
  
  public void gotPlaceTrends(Trends paramTrends) {}
  
  public void gotPrivacyPolicy(String paramString) {}
  
  public void gotRateLimitStatus(Map<String, RateLimitStatus> paramMap) {}
  
  public void gotRetweets(ResponseList<Status> paramResponseList) {}
  
  public void gotRetweetsOfMe(ResponseList<Status> paramResponseList) {}
  
  public void gotReverseGeoCode(ResponseList<Place> paramResponseList) {}
  
  public void gotSavedSearch(SavedSearch paramSavedSearch) {}
  
  public void gotSavedSearches(ResponseList<SavedSearch> paramResponseList) {}
  
  public void gotSentDirectMessages(ResponseList<DirectMessage> paramResponseList) {}
  
  public void gotShowFriendship(Relationship paramRelationship) {}
  
  public void gotShowStatus(Status paramStatus) {}
  
  public void gotShowUserList(UserList paramUserList) {}
  
  public void gotSimilarPlaces(ResponseList<Place> paramResponseList) {}
  
  public void gotSuggestedUserCategories(ResponseList<Category> paramResponseList) {}
  
  public void gotTermsOfService(String paramString) {}
  
  public void gotUserDetail(User paramUser) {}
  
  public void gotUserListMembers(PagableResponseList<User> paramPagableResponseList) {}
  
  public void gotUserListMemberships(PagableResponseList<UserList> paramPagableResponseList) {}
  
  public void gotUserListStatuses(ResponseList<Status> paramResponseList) {}
  
  public void gotUserListSubscribers(PagableResponseList<User> paramPagableResponseList) {}
  
  public void gotUserListSubscriptions(PagableResponseList<UserList> paramPagableResponseList) {}
  
  public void gotUserLists(ResponseList<UserList> paramResponseList) {}
  
  public void gotUserSuggestions(ResponseList<User> paramResponseList) {}
  
  public void gotUserTimeline(ResponseList<Status> paramResponseList) {}
  
  public void lookedUpFriendships(ResponseList<Friendship> paramResponseList) {}
  
  public void lookedup(ResponseList<Status> paramResponseList) {}
  
  public void lookedupUsers(ResponseList<User> paramResponseList) {}
  
  public void onException(TwitterException paramTwitterException, TwitterMethod paramTwitterMethod) {}
  
  public void removedProfileBanner() {}
  
  public void reportedSpam(User paramUser) {}
  
  public void retweetedStatus(Status paramStatus) {}
  
  public void searched(QueryResult paramQueryResult) {}
  
  public void searchedPlaces(ResponseList<Place> paramResponseList) {}
  
  public void searchedUser(ResponseList<User> paramResponseList) {}
  
  public void sentDirectMessage(DirectMessage paramDirectMessage) {}
  
  public void subscribedUserList(UserList paramUserList) {}
  
  public void unsubscribedUserList(UserList paramUserList) {}
  
  public void updatedAccountSettings(AccountSettings paramAccountSettings) {}
  
  public void updatedFriendship(Relationship paramRelationship) {}
  
  public void updatedProfile(User paramUser) {}
  
  public void updatedProfileBackgroundImage(User paramUser) {}
  
  public void updatedProfileBanner() {}
  
  public void updatedProfileColors(User paramUser) {}
  
  public void updatedProfileImage(User paramUser) {}
  
  public void updatedStatus(Status paramStatus) {}
  
  public void updatedUserList(UserList paramUserList) {}
  
  public void verifiedCredentials(User paramUser) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\TwitterAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */