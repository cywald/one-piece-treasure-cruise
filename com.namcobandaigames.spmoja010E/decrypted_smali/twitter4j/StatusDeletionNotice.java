package twitter4j;

import java.io.Serializable;

public abstract interface StatusDeletionNotice
  extends Comparable<StatusDeletionNotice>, Serializable
{
  public abstract long getStatusId();
  
  public abstract long getUserId();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\StatusDeletionNotice.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */