package twitter4j.auth;

public abstract interface AsyncOAuthSupport
{
  public abstract void getOAuthAccessTokenAsync();
  
  public abstract void getOAuthAccessTokenAsync(String paramString);
  
  public abstract void getOAuthAccessTokenAsync(String paramString1, String paramString2);
  
  public abstract void getOAuthAccessTokenAsync(RequestToken paramRequestToken);
  
  public abstract void getOAuthAccessTokenAsync(RequestToken paramRequestToken, String paramString);
  
  public abstract void getOAuthRequestTokenAsync();
  
  public abstract void getOAuthRequestTokenAsync(String paramString);
  
  public abstract void getOAuthRequestTokenAsync(String paramString1, String paramString2);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\auth\AsyncOAuthSupport.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */