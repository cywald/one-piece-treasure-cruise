package twitter4j.auth;

import java.io.Serializable;
import twitter4j.HttpRequest;

public abstract interface Authorization
  extends Serializable
{
  public abstract String getAuthorizationHeader(HttpRequest paramHttpRequest);
  
  public abstract boolean isEnabled();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\auth\Authorization.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */