package twitter4j;

public class ScopesImpl
  implements Scopes
{
  private final String[] placeIds;
  
  ScopesImpl()
  {
    this.placeIds = new String[0];
  }
  
  public ScopesImpl(String[] paramArrayOfString)
  {
    this.placeIds = paramArrayOfString;
  }
  
  public String[] getPlaceIds()
  {
    return this.placeIds;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\ScopesImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */