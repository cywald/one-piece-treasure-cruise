package twitter4j;

import java.io.Serializable;

public abstract interface AccountSettings
  extends TwitterResponse, Serializable
{
  public abstract String getLanguage();
  
  public abstract String getScreenName();
  
  public abstract String getSleepEndTime();
  
  public abstract String getSleepStartTime();
  
  public abstract TimeZone getTimeZone();
  
  public abstract Location[] getTrendLocations();
  
  public abstract boolean isAlwaysUseHttps();
  
  public abstract boolean isDiscoverableByEmail();
  
  public abstract boolean isGeoEnabled();
  
  public abstract boolean isSleepTimeEnabled();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\AccountSettings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */