package twitter4j;

public abstract interface TweetEntity
{
  public abstract int getEnd();
  
  public abstract int getStart();
  
  public abstract String getText();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\TweetEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */