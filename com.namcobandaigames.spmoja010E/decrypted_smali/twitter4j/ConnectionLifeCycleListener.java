package twitter4j;

public abstract interface ConnectionLifeCycleListener
{
  public abstract void onCleanUp();
  
  public abstract void onConnect();
  
  public abstract void onDisconnect();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\ConnectionLifeCycleListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */