package twitter4j;

import java.util.Map;
import twitter4j.api.HelpResources.Language;
import twitter4j.auth.AccessToken;
import twitter4j.auth.OAuth2Token;
import twitter4j.auth.RequestToken;

public abstract interface TwitterListener
{
  public abstract void checkedUserListMembership(User paramUser);
  
  public abstract void checkedUserListSubscription(User paramUser);
  
  public abstract void createdBlock(User paramUser);
  
  public abstract void createdFavorite(Status paramStatus);
  
  public abstract void createdFriendship(User paramUser);
  
  public abstract void createdMute(User paramUser);
  
  public abstract void createdSavedSearch(SavedSearch paramSavedSearch);
  
  public abstract void createdUserList(UserList paramUserList);
  
  public abstract void createdUserListMember(UserList paramUserList);
  
  public abstract void createdUserListMembers(UserList paramUserList);
  
  public abstract void destroyedBlock(User paramUser);
  
  public abstract void destroyedDirectMessage(DirectMessage paramDirectMessage);
  
  public abstract void destroyedFavorite(Status paramStatus);
  
  public abstract void destroyedFriendship(User paramUser);
  
  public abstract void destroyedMute(User paramUser);
  
  public abstract void destroyedSavedSearch(SavedSearch paramSavedSearch);
  
  public abstract void destroyedStatus(Status paramStatus);
  
  public abstract void destroyedUserList(UserList paramUserList);
  
  public abstract void destroyedUserListMember(UserList paramUserList);
  
  public abstract void gotAPIConfiguration(TwitterAPIConfiguration paramTwitterAPIConfiguration);
  
  public abstract void gotAccountSettings(AccountSettings paramAccountSettings);
  
  public abstract void gotAvailableTrends(ResponseList<Location> paramResponseList);
  
  public abstract void gotBlockIDs(IDs paramIDs);
  
  public abstract void gotBlocksList(ResponseList<User> paramResponseList);
  
  public abstract void gotClosestTrends(ResponseList<Location> paramResponseList);
  
  public abstract void gotContributees(ResponseList<User> paramResponseList);
  
  public abstract void gotContributors(ResponseList<User> paramResponseList);
  
  public abstract void gotDirectMessage(DirectMessage paramDirectMessage);
  
  public abstract void gotDirectMessages(ResponseList<DirectMessage> paramResponseList);
  
  public abstract void gotFavorites(ResponseList<Status> paramResponseList);
  
  public abstract void gotFollowersIDs(IDs paramIDs);
  
  public abstract void gotFollowersList(PagableResponseList<User> paramPagableResponseList);
  
  public abstract void gotFriendsIDs(IDs paramIDs);
  
  public abstract void gotFriendsList(PagableResponseList<User> paramPagableResponseList);
  
  public abstract void gotGeoDetails(Place paramPlace);
  
  public abstract void gotHomeTimeline(ResponseList<Status> paramResponseList);
  
  public abstract void gotIncomingFriendships(IDs paramIDs);
  
  public abstract void gotLanguages(ResponseList<HelpResources.Language> paramResponseList);
  
  public abstract void gotMemberSuggestions(ResponseList<User> paramResponseList);
  
  public abstract void gotMentions(ResponseList<Status> paramResponseList);
  
  public abstract void gotMuteIDs(IDs paramIDs);
  
  public abstract void gotMutesList(ResponseList<User> paramResponseList);
  
  public abstract void gotOAuth2Token(OAuth2Token paramOAuth2Token);
  
  public abstract void gotOAuthAccessToken(AccessToken paramAccessToken);
  
  public abstract void gotOAuthRequestToken(RequestToken paramRequestToken);
  
  public abstract void gotOEmbed(OEmbed paramOEmbed);
  
  public abstract void gotOutgoingFriendships(IDs paramIDs);
  
  public abstract void gotPlaceTrends(Trends paramTrends);
  
  public abstract void gotPrivacyPolicy(String paramString);
  
  public abstract void gotRateLimitStatus(Map<String, RateLimitStatus> paramMap);
  
  public abstract void gotRetweets(ResponseList<Status> paramResponseList);
  
  public abstract void gotRetweetsOfMe(ResponseList<Status> paramResponseList);
  
  public abstract void gotReverseGeoCode(ResponseList<Place> paramResponseList);
  
  public abstract void gotSavedSearch(SavedSearch paramSavedSearch);
  
  public abstract void gotSavedSearches(ResponseList<SavedSearch> paramResponseList);
  
  public abstract void gotSentDirectMessages(ResponseList<DirectMessage> paramResponseList);
  
  public abstract void gotShowFriendship(Relationship paramRelationship);
  
  public abstract void gotShowStatus(Status paramStatus);
  
  public abstract void gotShowUserList(UserList paramUserList);
  
  public abstract void gotSimilarPlaces(ResponseList<Place> paramResponseList);
  
  public abstract void gotSuggestedUserCategories(ResponseList<Category> paramResponseList);
  
  public abstract void gotTermsOfService(String paramString);
  
  public abstract void gotUserDetail(User paramUser);
  
  public abstract void gotUserListMembers(PagableResponseList<User> paramPagableResponseList);
  
  public abstract void gotUserListMemberships(PagableResponseList<UserList> paramPagableResponseList);
  
  public abstract void gotUserListStatuses(ResponseList<Status> paramResponseList);
  
  public abstract void gotUserListSubscribers(PagableResponseList<User> paramPagableResponseList);
  
  public abstract void gotUserListSubscriptions(PagableResponseList<UserList> paramPagableResponseList);
  
  public abstract void gotUserLists(ResponseList<UserList> paramResponseList);
  
  public abstract void gotUserSuggestions(ResponseList<User> paramResponseList);
  
  public abstract void gotUserTimeline(ResponseList<Status> paramResponseList);
  
  public abstract void lookedUpFriendships(ResponseList<Friendship> paramResponseList);
  
  public abstract void lookedup(ResponseList<Status> paramResponseList);
  
  public abstract void lookedupUsers(ResponseList<User> paramResponseList);
  
  public abstract void onException(TwitterException paramTwitterException, TwitterMethod paramTwitterMethod);
  
  public abstract void removedProfileBanner();
  
  public abstract void reportedSpam(User paramUser);
  
  public abstract void retweetedStatus(Status paramStatus);
  
  public abstract void searched(QueryResult paramQueryResult);
  
  public abstract void searchedPlaces(ResponseList<Place> paramResponseList);
  
  public abstract void searchedUser(ResponseList<User> paramResponseList);
  
  public abstract void sentDirectMessage(DirectMessage paramDirectMessage);
  
  public abstract void subscribedUserList(UserList paramUserList);
  
  public abstract void unsubscribedUserList(UserList paramUserList);
  
  public abstract void updatedAccountSettings(AccountSettings paramAccountSettings);
  
  public abstract void updatedFriendship(Relationship paramRelationship);
  
  public abstract void updatedProfile(User paramUser);
  
  public abstract void updatedProfileBackgroundImage(User paramUser);
  
  public abstract void updatedProfileBanner();
  
  public abstract void updatedProfileColors(User paramUser);
  
  public abstract void updatedProfileImage(User paramUser);
  
  public abstract void updatedStatus(Status paramStatus);
  
  public abstract void updatedUserList(UserList paramUserList);
  
  public abstract void verifiedCredentials(User paramUser);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\TwitterListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */