package twitter4j;

import java.io.Serializable;

public abstract interface SymbolEntity
  extends TweetEntity, Serializable
{
  public abstract int getEnd();
  
  public abstract int getStart();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\SymbolEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */