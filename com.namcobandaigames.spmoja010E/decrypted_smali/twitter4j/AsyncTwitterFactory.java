package twitter4j;

import java.io.Serializable;
import twitter4j.auth.AccessToken;
import twitter4j.auth.Authorization;
import twitter4j.auth.AuthorizationFactory;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;

public final class AsyncTwitterFactory
  implements Serializable
{
  private static final AsyncTwitter SINGLETON = new AsyncTwitterImpl(ConfigurationContext.getInstance(), TwitterFactory.DEFAULT_AUTHORIZATION);
  private static final long serialVersionUID = 1394946919393640158L;
  private final Configuration conf;
  
  public AsyncTwitterFactory()
  {
    this(ConfigurationContext.getInstance());
  }
  
  public AsyncTwitterFactory(String paramString)
  {
    this.conf = ConfigurationContext.getInstance(paramString);
  }
  
  public AsyncTwitterFactory(Configuration paramConfiguration)
  {
    if (paramConfiguration == null) {
      throw new NullPointerException("configuration cannot be null");
    }
    this.conf = paramConfiguration;
  }
  
  public static AsyncTwitter getSingleton()
  {
    return SINGLETON;
  }
  
  public AsyncTwitter getInstance()
  {
    return getInstance(AuthorizationFactory.getInstance(this.conf));
  }
  
  public AsyncTwitter getInstance(Twitter paramTwitter)
  {
    return new AsyncTwitterImpl(paramTwitter.getConfiguration(), paramTwitter.getAuthorization());
  }
  
  public AsyncTwitter getInstance(AccessToken paramAccessToken)
  {
    String str1 = this.conf.getOAuthConsumerKey();
    String str2 = this.conf.getOAuthConsumerSecret();
    if ((str1 == null) && (str2 == null)) {
      throw new IllegalStateException("Consumer key and Consumer secret not supplied.");
    }
    OAuthAuthorization localOAuthAuthorization = new OAuthAuthorization(this.conf);
    localOAuthAuthorization.setOAuthConsumer(str1, str2);
    localOAuthAuthorization.setOAuthAccessToken(paramAccessToken);
    return new AsyncTwitterImpl(this.conf, localOAuthAuthorization);
  }
  
  public AsyncTwitter getInstance(Authorization paramAuthorization)
  {
    return new AsyncTwitterImpl(this.conf, paramAuthorization);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\AsyncTwitterFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */