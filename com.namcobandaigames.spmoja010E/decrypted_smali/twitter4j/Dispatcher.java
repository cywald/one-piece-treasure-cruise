package twitter4j;

public abstract interface Dispatcher
{
  public abstract void invokeLater(Runnable paramRunnable);
  
  public abstract void shutdown();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\Dispatcher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */