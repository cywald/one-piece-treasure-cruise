package twitter4j;

public abstract interface Category
{
  public abstract String getName();
  
  public abstract int getSize();
  
  public abstract String getSlug();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\Category.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */