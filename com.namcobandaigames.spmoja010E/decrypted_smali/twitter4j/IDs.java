package twitter4j;

import java.io.Serializable;

public abstract interface IDs
  extends TwitterResponse, CursorSupport, Serializable
{
  public abstract long[] getIDs();
  
  public abstract long getNextCursor();
  
  public abstract long getPreviousCursor();
  
  public abstract boolean hasNext();
  
  public abstract boolean hasPrevious();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\twitter4j\IDs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */