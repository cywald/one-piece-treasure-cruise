package com.ex.android.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class IabBroadcastReceiver
  extends BroadcastReceiver
{
  public static final String ACTION = "com.android.vending.billing.PURCHASES_UPDATED";
  private final IabBroadcastListener mListener;
  
  public IabBroadcastReceiver(IabBroadcastListener paramIabBroadcastListener)
  {
    this.mListener = paramIabBroadcastListener;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (this.mListener != null) {
      this.mListener.receivedBroadcast();
    }
  }
  
  public static abstract interface IabBroadcastListener
  {
    public abstract void receivedBroadcast();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\ex\android\util\IabBroadcastReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */