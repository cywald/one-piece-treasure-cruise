package com.ex.android.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.android.vending.billing.IInAppBillingService;
import com.android.vending.billing.IInAppBillingService.Stub;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;

public class IabHelper
{
  public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
  public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
  public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
  public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
  public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;
  public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
  public static final int BILLING_RESPONSE_RESULT_OK = 0;
  public static final int BILLING_RESPONSE_RESULT_SERVICE_UNAVAILABLE = 2;
  public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
  public static final String GET_SKU_DETAILS_ITEM_LIST = "ITEM_ID_LIST";
  public static final String GET_SKU_DETAILS_ITEM_TYPE_LIST = "ITEM_TYPE_LIST";
  public static final int IABHELPER_BAD_RESPONSE = -1002;
  public static final int IABHELPER_ERROR_BASE = -1000;
  public static final int IABHELPER_INVALID_CONSUMPTION = -1010;
  public static final int IABHELPER_MISSING_TOKEN = -1007;
  public static final int IABHELPER_REMOTE_EXCEPTION = -1001;
  public static final int IABHELPER_SEND_INTENT_FAILED = -1004;
  public static final int IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE = -1009;
  public static final int IABHELPER_SUBSCRIPTION_UPDATE_NOT_AVAILABLE = -1011;
  public static final int IABHELPER_UNKNOWN_ERROR = -1008;
  public static final int IABHELPER_UNKNOWN_PURCHASE_RESPONSE = -1006;
  public static final int IABHELPER_USER_CANCELLED = -1005;
  public static final int IABHELPER_VERIFICATION_FAILED = -1003;
  public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";
  public static final String ITEM_TYPE_INAPP = "inapp";
  public static final String ITEM_TYPE_SUBS = "subs";
  public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";
  public static final String RESPONSE_CODE = "RESPONSE_CODE";
  public static final String RESPONSE_GET_SKU_DETAILS_LIST = "DETAILS_LIST";
  public static final String RESPONSE_INAPP_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
  public static final String RESPONSE_INAPP_PURCHASE_DATA = "INAPP_PURCHASE_DATA";
  public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
  public static final String RESPONSE_INAPP_SIGNATURE = "INAPP_DATA_SIGNATURE";
  public static final String RESPONSE_INAPP_SIGNATURE_LIST = "INAPP_DATA_SIGNATURE_LIST";
  boolean mAsyncInProgress = false;
  private final Object mAsyncInProgressLock = new Object();
  String mAsyncOperation = "";
  Context mContext;
  boolean mDebugLog = false;
  String mDebugTag = "IabHelper";
  boolean mDisposeAfterAsync = false;
  boolean mDisposed = false;
  OnIabPurchaseFinishedListener mPurchaseListener;
  String mPurchasingItemType;
  int mRequestCode;
  IInAppBillingService mService;
  ServiceConnection mServiceConn;
  boolean mSetupDone = false;
  String mSignatureBase64 = null;
  boolean mSubscriptionUpdateSupported = false;
  boolean mSubscriptionsSupported = false;
  
  public IabHelper(Context paramContext, String paramString)
  {
    this.mContext = paramContext.getApplicationContext();
    this.mSignatureBase64 = paramString;
    logDebug("IAB helper created.");
  }
  
  private void checkNotDisposed()
  {
    if (this.mDisposed) {
      throw new IllegalStateException("IabHelper was disposed of, so it cannot be used.");
    }
  }
  
  public static String getResponseDesc(int paramInt)
  {
    String[] arrayOfString1 = "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned".split("/");
    String[] arrayOfString2 = "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt".split("/");
    if (paramInt <= 64536)
    {
      int i = 64536 - paramInt;
      if ((i >= 0) && (i < arrayOfString2.length)) {
        return arrayOfString2[i];
      }
      return String.valueOf(paramInt) + ":Unknown IAB Helper Error";
    }
    if ((paramInt < 0) || (paramInt >= arrayOfString1.length)) {
      return String.valueOf(paramInt) + ":Unknown";
    }
    return arrayOfString1[paramInt];
  }
  
  void checkSetupDone(String paramString)
  {
    if (!this.mSetupDone)
    {
      logError("Illegal state for operation (" + paramString + "): IAB helper is not set up.");
      throw new IllegalStateException("IAB helper is not set up. Can't perform operation: " + paramString);
    }
  }
  
  void consume(Purchase paramPurchase)
    throws IabException
  {
    checkNotDisposed();
    checkSetupDone("consume");
    if (!paramPurchase.mItemType.equals("inapp")) {
      throw new IabException(64526, "Items of type '" + paramPurchase.mItemType + "' can't be consumed.");
    }
    String str2;
    try
    {
      String str1 = paramPurchase.getToken();
      str2 = paramPurchase.getSku();
      if ((str1 == null) || (str1.equals("")))
      {
        logError("Can't consume " + str2 + ". No token.");
        throw new IabException(64529, "PurchaseInfo is missing token for sku: " + str2 + " " + paramPurchase);
      }
    }
    catch (RemoteException localRemoteException)
    {
      throw new IabException(64535, "Remote exception while consuming. PurchaseInfo: " + paramPurchase, localRemoteException);
    }
    logDebug("Consuming sku: " + str2 + ", token: " + localRemoteException);
    int i = this.mService.consumePurchase(3, this.mContext.getPackageName(), localRemoteException);
    if (i == 0)
    {
      logDebug("Successfully consumed sku: " + str2);
      return;
    }
    logDebug("Error consuming consuming sku " + str2 + ". " + getResponseDesc(i));
    throw new IabException(i, "Error consuming sku " + str2);
  }
  
  public void consumeAsync(Purchase paramPurchase, OnConsumeFinishedListener paramOnConsumeFinishedListener)
    throws IabHelper.IabAsyncInProgressException
  {
    checkNotDisposed();
    checkSetupDone("consume");
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramPurchase);
    consumeAsyncInternal(localArrayList, paramOnConsumeFinishedListener, null);
  }
  
  public void consumeAsync(List<Purchase> paramList, OnConsumeMultiFinishedListener paramOnConsumeMultiFinishedListener)
    throws IabHelper.IabAsyncInProgressException
  {
    checkNotDisposed();
    checkSetupDone("consume");
    consumeAsyncInternal(paramList, null, paramOnConsumeMultiFinishedListener);
  }
  
  void consumeAsyncInternal(final List<Purchase> paramList, final OnConsumeFinishedListener paramOnConsumeFinishedListener, final OnConsumeMultiFinishedListener paramOnConsumeMultiFinishedListener)
    throws IabHelper.IabAsyncInProgressException
  {
    final Handler localHandler = new Handler();
    flagStartAsync("consume");
    new Thread(new Runnable()
    {
      public void run()
      {
        final ArrayList localArrayList = new ArrayList();
        Iterator localIterator = paramList.iterator();
        while (localIterator.hasNext())
        {
          Purchase localPurchase = (Purchase)localIterator.next();
          try
          {
            IabHelper.this.consume(localPurchase);
            localArrayList.add(new IabResult(0, "Successful consume of sku " + localPurchase.getSku()));
          }
          catch (IabException localIabException)
          {
            localArrayList.add(localIabException.getResult());
          }
        }
        IabHelper.this.flagEndAsync();
        if ((!IabHelper.this.mDisposed) && (paramOnConsumeFinishedListener != null)) {
          localHandler.post(new Runnable()
          {
            public void run()
            {
              IabHelper.3.this.val$singleListener.onConsumeFinished((Purchase)IabHelper.3.this.val$purchases.get(0), (IabResult)localArrayList.get(0));
            }
          });
        }
        if ((!IabHelper.this.mDisposed) && (paramOnConsumeMultiFinishedListener != null)) {
          localHandler.post(new Runnable()
          {
            public void run()
            {
              IabHelper.3.this.val$multiListener.onConsumeMultiFinished(IabHelper.3.this.val$purchases, localArrayList);
            }
          });
        }
      }
    }).start();
  }
  
  public void dispose()
    throws IabHelper.IabAsyncInProgressException
  {
    synchronized (this.mAsyncInProgressLock)
    {
      if (this.mAsyncInProgress) {
        throw new IabAsyncInProgressException("Can't dispose because an async operation (" + this.mAsyncOperation + ") is in progress.");
      }
    }
    logDebug("Disposing.");
    this.mSetupDone = false;
    if (this.mServiceConn != null)
    {
      logDebug("Unbinding from service.");
      if (this.mContext != null) {
        this.mContext.unbindService(this.mServiceConn);
      }
    }
    this.mDisposed = true;
    this.mContext = null;
    this.mServiceConn = null;
    this.mService = null;
    this.mPurchaseListener = null;
  }
  
  public void disposeWhenFinished()
  {
    synchronized (this.mAsyncInProgressLock)
    {
      if (this.mAsyncInProgress)
      {
        logDebug("Will dispose after async operation finishes.");
        this.mDisposeAfterAsync = true;
      }
      for (;;)
      {
        return;
        try
        {
          dispose();
        }
        catch (IabAsyncInProgressException localIabAsyncInProgressException) {}
      }
    }
  }
  
  public void enableDebugLogging(boolean paramBoolean)
  {
    checkNotDisposed();
    this.mDebugLog = paramBoolean;
  }
  
  public void enableDebugLogging(boolean paramBoolean, String paramString)
  {
    checkNotDisposed();
    this.mDebugLog = paramBoolean;
    this.mDebugTag = paramString;
  }
  
  void flagEndAsync()
  {
    synchronized (this.mAsyncInProgressLock)
    {
      logDebug("Ending async operation: " + this.mAsyncOperation);
      this.mAsyncOperation = "";
      this.mAsyncInProgress = false;
      boolean bool = this.mDisposeAfterAsync;
      if (!bool) {}
    }
    try
    {
      dispose();
      return;
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
    catch (IabAsyncInProgressException localIabAsyncInProgressException)
    {
      for (;;) {}
    }
  }
  
  void flagStartAsync(String paramString)
    throws IabHelper.IabAsyncInProgressException
  {
    synchronized (this.mAsyncInProgressLock)
    {
      if (this.mAsyncInProgress) {
        throw new IabAsyncInProgressException("Can't start async operation (" + paramString + ") because another async operation (" + this.mAsyncOperation + ") is in progress.");
      }
    }
    this.mAsyncOperation = paramString;
    this.mAsyncInProgress = true;
    logDebug("Starting async operation: " + paramString);
  }
  
  int getResponseCodeFromBundle(Bundle paramBundle)
  {
    paramBundle = paramBundle.get("RESPONSE_CODE");
    if (paramBundle == null)
    {
      logDebug("Bundle with null response code, assuming OK (known issue)");
      return 0;
    }
    if ((paramBundle instanceof Integer)) {
      return ((Integer)paramBundle).intValue();
    }
    if ((paramBundle instanceof Long)) {
      return (int)((Long)paramBundle).longValue();
    }
    logError("Unexpected type for bundle response code.");
    logError(paramBundle.getClass().getName());
    throw new RuntimeException("Unexpected type for bundle response code: " + paramBundle.getClass().getName());
  }
  
  int getResponseCodeFromIntent(Intent paramIntent)
  {
    paramIntent = paramIntent.getExtras().get("RESPONSE_CODE");
    if (paramIntent == null)
    {
      logError("Intent with no response code, assuming OK (known issue)");
      return 0;
    }
    if ((paramIntent instanceof Integer)) {
      return ((Integer)paramIntent).intValue();
    }
    if ((paramIntent instanceof Long)) {
      return (int)((Long)paramIntent).longValue();
    }
    logError("Unexpected type for intent response code.");
    logError(paramIntent.getClass().getName());
    throw new RuntimeException("Unexpected type for intent response code: " + paramIntent.getClass().getName());
  }
  
  /* Error */
  public boolean handleActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    // Byte code:
    //   0: iload_1
    //   1: aload_0
    //   2: getfield 436	com/ex/android/util/IabHelper:mRequestCode	I
    //   5: if_icmpeq +5 -> 10
    //   8: iconst_0
    //   9: ireturn
    //   10: aload_0
    //   11: invokespecial 241	com/ex/android/util/IabHelper:checkNotDisposed	()V
    //   14: aload_0
    //   15: ldc_w 437
    //   18: invokevirtual 244	com/ex/android/util/IabHelper:checkSetupDone	(Ljava/lang/String;)V
    //   21: aload_0
    //   22: invokevirtual 439	com/ex/android/util/IabHelper:flagEndAsync	()V
    //   25: aload_3
    //   26: ifnonnull +44 -> 70
    //   29: aload_0
    //   30: ldc_w 441
    //   33: invokevirtual 231	com/ex/android/util/IabHelper:logError	(Ljava/lang/String;)V
    //   36: new 443	com/ex/android/util/IabResult
    //   39: dup
    //   40: sipush 64534
    //   43: ldc_w 445
    //   46: invokespecial 446	com/ex/android/util/IabResult:<init>	(ILjava/lang/String;)V
    //   49: astore_3
    //   50: aload_0
    //   51: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   54: ifnull +14 -> 68
    //   57: aload_0
    //   58: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   61: aload_3
    //   62: aconst_null
    //   63: invokeinterface 450 3 0
    //   68: iconst_1
    //   69: ireturn
    //   70: aload_0
    //   71: aload_3
    //   72: invokevirtual 452	com/ex/android/util/IabHelper:getResponseCodeFromIntent	(Landroid/content/Intent;)I
    //   75: istore_1
    //   76: aload_3
    //   77: ldc 106
    //   79: invokevirtual 456	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   82: astore 4
    //   84: aload_3
    //   85: ldc 112
    //   87: invokevirtual 456	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   90: astore 5
    //   92: iload_2
    //   93: iconst_m1
    //   94: if_icmpne +398 -> 492
    //   97: iload_1
    //   98: ifne +394 -> 492
    //   101: aload_0
    //   102: ldc_w 458
    //   105: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   108: aload_0
    //   109: new 207	java/lang/StringBuilder
    //   112: dup
    //   113: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   116: ldc_w 460
    //   119: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   122: aload 4
    //   124: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   127: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   130: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   133: aload_0
    //   134: new 207	java/lang/StringBuilder
    //   137: dup
    //   138: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   141: ldc_w 462
    //   144: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   147: aload 5
    //   149: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   152: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   155: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   158: aload_0
    //   159: new 207	java/lang/StringBuilder
    //   162: dup
    //   163: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   166: ldc_w 464
    //   169: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: aload_3
    //   173: invokevirtual 424	android/content/Intent:getExtras	()Landroid/os/Bundle;
    //   176: invokevirtual 277	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   179: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   182: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   185: aload_0
    //   186: new 207	java/lang/StringBuilder
    //   189: dup
    //   190: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   193: ldc_w 466
    //   196: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   199: aload_0
    //   200: getfield 468	com/ex/android/util/IabHelper:mPurchasingItemType	Ljava/lang/String;
    //   203: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   209: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   212: aload 4
    //   214: ifnull +8 -> 222
    //   217: aload 5
    //   219: ifnonnull +74 -> 293
    //   222: aload_0
    //   223: ldc_w 470
    //   226: invokevirtual 231	com/ex/android/util/IabHelper:logError	(Ljava/lang/String;)V
    //   229: aload_0
    //   230: new 207	java/lang/StringBuilder
    //   233: dup
    //   234: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   237: ldc_w 464
    //   240: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   243: aload_3
    //   244: invokevirtual 424	android/content/Intent:getExtras	()Landroid/os/Bundle;
    //   247: invokevirtual 471	android/os/Bundle:toString	()Ljava/lang/String;
    //   250: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   253: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   256: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   259: new 443	com/ex/android/util/IabResult
    //   262: dup
    //   263: sipush 64528
    //   266: ldc_w 473
    //   269: invokespecial 446	com/ex/android/util/IabResult:<init>	(ILjava/lang/String;)V
    //   272: astore_3
    //   273: aload_0
    //   274: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   277: ifnull +14 -> 291
    //   280: aload_0
    //   281: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   284: aload_3
    //   285: aconst_null
    //   286: invokeinterface 450 3 0
    //   291: iconst_1
    //   292: ireturn
    //   293: new 246	com/ex/android/util/Purchase
    //   296: dup
    //   297: aload_0
    //   298: getfield 468	com/ex/android/util/IabHelper:mPurchasingItemType	Ljava/lang/String;
    //   301: aload 4
    //   303: aload 5
    //   305: invokespecial 476	com/ex/android/util/Purchase:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   308: astore_3
    //   309: aload_3
    //   310: invokevirtual 266	com/ex/android/util/Purchase:getSku	()Ljava/lang/String;
    //   313: astore 6
    //   315: aload_0
    //   316: getfield 169	com/ex/android/util/IabHelper:mSignatureBase64	Ljava/lang/String;
    //   319: aload 4
    //   321: aload 5
    //   323: invokestatic 482	com/ex/android/util/Security:verifyPurchase	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    //   326: ifne +83 -> 409
    //   329: aload_0
    //   330: new 207	java/lang/StringBuilder
    //   333: dup
    //   334: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   337: ldc_w 484
    //   340: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   343: aload 6
    //   345: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   348: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   351: invokevirtual 231	com/ex/android/util/IabHelper:logError	(Ljava/lang/String;)V
    //   354: new 443	com/ex/android/util/IabResult
    //   357: dup
    //   358: sipush 64533
    //   361: new 207	java/lang/StringBuilder
    //   364: dup
    //   365: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   368: ldc_w 486
    //   371: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   374: aload 6
    //   376: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   379: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   382: invokespecial 446	com/ex/android/util/IabResult:<init>	(ILjava/lang/String;)V
    //   385: astore 4
    //   387: aload_0
    //   388: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   391: ifnull +311 -> 702
    //   394: aload_0
    //   395: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   398: aload 4
    //   400: aload_3
    //   401: invokeinterface 450 3 0
    //   406: goto +296 -> 702
    //   409: aload_0
    //   410: ldc_w 488
    //   413: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   416: aload_0
    //   417: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   420: ifnull +24 -> 444
    //   423: aload_0
    //   424: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   427: new 443	com/ex/android/util/IabResult
    //   430: dup
    //   431: iconst_0
    //   432: ldc_w 490
    //   435: invokespecial 446	com/ex/android/util/IabResult:<init>	(ILjava/lang/String;)V
    //   438: aload_3
    //   439: invokeinterface 450 3 0
    //   444: iconst_1
    //   445: ireturn
    //   446: astore_3
    //   447: aload_0
    //   448: ldc_w 492
    //   451: invokevirtual 231	com/ex/android/util/IabHelper:logError	(Ljava/lang/String;)V
    //   454: aload_3
    //   455: invokevirtual 495	org/json/JSONException:printStackTrace	()V
    //   458: new 443	com/ex/android/util/IabResult
    //   461: dup
    //   462: sipush 64534
    //   465: ldc_w 492
    //   468: invokespecial 446	com/ex/android/util/IabResult:<init>	(ILjava/lang/String;)V
    //   471: astore_3
    //   472: aload_0
    //   473: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   476: ifnull +14 -> 490
    //   479: aload_0
    //   480: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   483: aload_3
    //   484: aconst_null
    //   485: invokeinterface 450 3 0
    //   490: iconst_1
    //   491: ireturn
    //   492: iload_2
    //   493: iconst_m1
    //   494: if_icmpne +63 -> 557
    //   497: aload_0
    //   498: new 207	java/lang/StringBuilder
    //   501: dup
    //   502: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   505: ldc_w 497
    //   508: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   511: iload_1
    //   512: invokestatic 305	com/ex/android/util/IabHelper:getResponseDesc	(I)Ljava/lang/String;
    //   515: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   518: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   521: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   524: aload_0
    //   525: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   528: ifnull -84 -> 444
    //   531: new 443	com/ex/android/util/IabResult
    //   534: dup
    //   535: iload_1
    //   536: ldc_w 499
    //   539: invokespecial 446	com/ex/android/util/IabResult:<init>	(ILjava/lang/String;)V
    //   542: astore_3
    //   543: aload_0
    //   544: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   547: aload_3
    //   548: aconst_null
    //   549: invokeinterface 450 3 0
    //   554: goto -110 -> 444
    //   557: iload_2
    //   558: ifne +65 -> 623
    //   561: aload_0
    //   562: new 207	java/lang/StringBuilder
    //   565: dup
    //   566: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   569: ldc_w 501
    //   572: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   575: iload_1
    //   576: invokestatic 305	com/ex/android/util/IabHelper:getResponseDesc	(I)Ljava/lang/String;
    //   579: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   582: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   585: invokevirtual 183	com/ex/android/util/IabHelper:logDebug	(Ljava/lang/String;)V
    //   588: new 443	com/ex/android/util/IabResult
    //   591: dup
    //   592: sipush 64531
    //   595: ldc_w 503
    //   598: invokespecial 446	com/ex/android/util/IabResult:<init>	(ILjava/lang/String;)V
    //   601: astore_3
    //   602: aload_0
    //   603: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   606: ifnull -162 -> 444
    //   609: aload_0
    //   610: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   613: aload_3
    //   614: aconst_null
    //   615: invokeinterface 450 3 0
    //   620: goto -176 -> 444
    //   623: aload_0
    //   624: new 207	java/lang/StringBuilder
    //   627: dup
    //   628: invokespecial 208	java/lang/StringBuilder:<init>	()V
    //   631: ldc_w 505
    //   634: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   637: iload_2
    //   638: invokestatic 507	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   641: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   644: ldc_w 509
    //   647: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   650: iload_1
    //   651: invokestatic 305	com/ex/android/util/IabHelper:getResponseDesc	(I)Ljava/lang/String;
    //   654: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   657: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   660: invokevirtual 231	com/ex/android/util/IabHelper:logError	(Ljava/lang/String;)V
    //   663: new 443	com/ex/android/util/IabResult
    //   666: dup
    //   667: sipush 64530
    //   670: ldc_w 511
    //   673: invokespecial 446	com/ex/android/util/IabResult:<init>	(ILjava/lang/String;)V
    //   676: astore_3
    //   677: aload_0
    //   678: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   681: ifnull -237 -> 444
    //   684: aload_0
    //   685: getfield 361	com/ex/android/util/IabHelper:mPurchaseListener	Lcom/ex/android/util/IabHelper$OnIabPurchaseFinishedListener;
    //   688: aload_3
    //   689: aconst_null
    //   690: invokeinterface 450 3 0
    //   695: goto -251 -> 444
    //   698: astore_3
    //   699: goto -252 -> 447
    //   702: iconst_1
    //   703: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	704	0	this	IabHelper
    //   0	704	1	paramInt1	int
    //   0	704	2	paramInt2	int
    //   0	704	3	paramIntent	Intent
    //   82	317	4	localObject	Object
    //   90	232	5	str1	String
    //   313	62	6	str2	String
    // Exception table:
    //   from	to	target	type
    //   293	309	446	org/json/JSONException
    //   309	406	698	org/json/JSONException
    //   409	416	698	org/json/JSONException
  }
  
  public void launchPurchaseFlow(Activity paramActivity, String paramString, int paramInt, OnIabPurchaseFinishedListener paramOnIabPurchaseFinishedListener)
    throws IabHelper.IabAsyncInProgressException
  {
    launchPurchaseFlow(paramActivity, paramString, paramInt, paramOnIabPurchaseFinishedListener, "");
  }
  
  public void launchPurchaseFlow(Activity paramActivity, String paramString1, int paramInt, OnIabPurchaseFinishedListener paramOnIabPurchaseFinishedListener, String paramString2)
    throws IabHelper.IabAsyncInProgressException
  {
    launchPurchaseFlow(paramActivity, paramString1, "inapp", null, paramInt, paramOnIabPurchaseFinishedListener, paramString2);
  }
  
  public void launchPurchaseFlow(Activity paramActivity, String paramString1, String paramString2, List<String> paramList, int paramInt, OnIabPurchaseFinishedListener paramOnIabPurchaseFinishedListener, String paramString3)
    throws IabHelper.IabAsyncInProgressException
  {
    checkNotDisposed();
    checkSetupDone("launchPurchaseFlow");
    flagStartAsync("launchPurchaseFlow");
    if ((paramString2.equals("subs")) && (!this.mSubscriptionsSupported))
    {
      paramActivity = new IabResult(64527, "Subscriptions are not available.");
      flagEndAsync();
      if (paramOnIabPurchaseFinishedListener != null) {
        paramOnIabPurchaseFinishedListener.onIabPurchaseFinished(paramActivity, null);
      }
      return;
    }
    for (;;)
    {
      try
      {
        logDebug("Constructing buy intent for " + paramString1 + ", item type: " + paramString2);
        if ((paramList == null) || (paramList.isEmpty()))
        {
          paramList = this.mService.getBuyIntent(3, this.mContext.getPackageName(), paramString1, paramString2, paramString3);
          int i = getResponseCodeFromBundle(paramList);
          if (i == 0) {
            break label404;
          }
          logError("Unable to buy item, Error response: " + getResponseDesc(i));
          flagEndAsync();
          paramActivity = new IabResult(i, "Unable to buy item");
          if (paramOnIabPurchaseFinishedListener == null) {
            break;
          }
          paramOnIabPurchaseFinishedListener.onIabPurchaseFinished(paramActivity, null);
          return;
        }
      }
      catch (IntentSender.SendIntentException paramActivity)
      {
        logError("SendIntentException while launching purchase flow for sku " + paramString1);
        paramActivity.printStackTrace();
        flagEndAsync();
        paramActivity = new IabResult(64532, "Failed to send intent.");
        if (paramOnIabPurchaseFinishedListener == null) {
          break;
        }
        paramOnIabPurchaseFinishedListener.onIabPurchaseFinished(paramActivity, null);
        return;
        if (this.mSubscriptionUpdateSupported) {
          break label376;
        }
        paramActivity = new IabResult(64525, "Subscription updates are not available.");
        flagEndAsync();
        if (paramOnIabPurchaseFinishedListener == null) {
          break;
        }
        paramOnIabPurchaseFinishedListener.onIabPurchaseFinished(paramActivity, null);
        return;
      }
      catch (RemoteException paramActivity)
      {
        logError("RemoteException while launching purchase flow for sku " + paramString1);
        paramActivity.printStackTrace();
        flagEndAsync();
        paramActivity = new IabResult(64535, "Remote exception while starting purchase flow");
      }
      if (paramOnIabPurchaseFinishedListener == null) {
        break;
      }
      paramOnIabPurchaseFinishedListener.onIabPurchaseFinished(paramActivity, null);
      return;
      label376:
      paramList = this.mService.getBuyIntentToReplaceSkus(5, this.mContext.getPackageName(), paramList, paramString1, paramString2, paramString3);
    }
    label404:
    paramList = (PendingIntent)paramList.getParcelable("BUY_INTENT");
    logDebug("Launching buy intent for " + paramString1 + ". Request code: " + paramInt);
    this.mRequestCode = paramInt;
    this.mPurchaseListener = paramOnIabPurchaseFinishedListener;
    this.mPurchasingItemType = paramString2;
    paramActivity.startIntentSenderForResult(paramList.getIntentSender(), paramInt, new Intent(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue());
  }
  
  public void launchSubscriptionPurchaseFlow(Activity paramActivity, String paramString, int paramInt, OnIabPurchaseFinishedListener paramOnIabPurchaseFinishedListener)
    throws IabHelper.IabAsyncInProgressException
  {
    launchSubscriptionPurchaseFlow(paramActivity, paramString, paramInt, paramOnIabPurchaseFinishedListener, "");
  }
  
  public void launchSubscriptionPurchaseFlow(Activity paramActivity, String paramString1, int paramInt, OnIabPurchaseFinishedListener paramOnIabPurchaseFinishedListener, String paramString2)
    throws IabHelper.IabAsyncInProgressException
  {
    launchPurchaseFlow(paramActivity, paramString1, "subs", null, paramInt, paramOnIabPurchaseFinishedListener, paramString2);
  }
  
  void logDebug(String paramString)
  {
    if (this.mDebugLog) {
      Log.d(this.mDebugTag, paramString);
    }
  }
  
  void logError(String paramString)
  {
    Log.e(this.mDebugTag, "In-app billing error: " + paramString);
  }
  
  void logWarn(String paramString)
  {
    Log.w(this.mDebugTag, "In-app billing warning: " + paramString);
  }
  
  public Inventory queryInventory()
    throws IabException
  {
    return queryInventory(false, null, null);
  }
  
  public Inventory queryInventory(boolean paramBoolean, List<String> paramList1, List<String> paramList2)
    throws IabException
  {
    checkNotDisposed();
    checkSetupDone("queryInventory");
    Inventory localInventory;
    int i;
    try
    {
      localInventory = new Inventory();
      i = queryPurchases(localInventory, "inapp");
      if (i != 0) {
        throw new IabException(i, "Error refreshing inventory (querying owned items).");
      }
    }
    catch (RemoteException paramList1)
    {
      throw new IabException(64535, "Remote exception while refreshing inventory.", paramList1);
      if (paramBoolean)
      {
        i = querySkuDetails("inapp", localInventory, paramList1);
        if (i != 0) {
          throw new IabException(i, "Error refreshing inventory (querying prices of items).");
        }
      }
    }
    catch (JSONException paramList1)
    {
      throw new IabException(64534, "Error parsing JSON response while refreshing inventory.", paramList1);
    }
    if (this.mSubscriptionsSupported)
    {
      i = queryPurchases(localInventory, "subs");
      if (i != 0) {
        throw new IabException(i, "Error refreshing inventory (querying owned subscriptions).");
      }
      if (paramBoolean)
      {
        i = querySkuDetails("subs", localInventory, paramList2);
        if (i != 0) {
          throw new IabException(i, "Error refreshing inventory (querying prices of subscriptions).");
        }
      }
    }
    return localInventory;
  }
  
  public void queryInventoryAsync(QueryInventoryFinishedListener paramQueryInventoryFinishedListener)
    throws IabHelper.IabAsyncInProgressException
  {
    queryInventoryAsync(false, null, null, paramQueryInventoryFinishedListener);
  }
  
  public void queryInventoryAsync(final boolean paramBoolean, final List<String> paramList1, final List<String> paramList2, final QueryInventoryFinishedListener paramQueryInventoryFinishedListener)
    throws IabHelper.IabAsyncInProgressException
  {
    final Handler localHandler = new Handler();
    checkNotDisposed();
    checkSetupDone("queryInventory");
    flagStartAsync("refresh inventory");
    new Thread(new Runnable()
    {
      public void run()
      {
        final IabResult localIabResult1 = new IabResult(0, "Inventory refresh successful.");
        final Object localObject = null;
        try
        {
          Inventory localInventory = IabHelper.this.queryInventory(paramBoolean, paramList1, paramList2);
          localObject = localInventory;
        }
        catch (IabException localIabException)
        {
          for (;;)
          {
            IabResult localIabResult2 = localIabException.getResult();
          }
        }
        IabHelper.this.flagEndAsync();
        if ((!IabHelper.this.mDisposed) && (paramQueryInventoryFinishedListener != null)) {
          localHandler.post(new Runnable()
          {
            public void run()
            {
              IabHelper.2.this.val$listener.onQueryInventoryFinished(localIabResult1, localObject);
            }
          });
        }
      }
    }).start();
  }
  
  int queryPurchases(Inventory paramInventory, String paramString)
    throws JSONException, RemoteException
  {
    logDebug("Querying owned items, item type: " + paramString);
    logDebug("Package name: " + this.mContext.getPackageName());
    int i = 0;
    Object localObject1 = null;
    int j;
    Object localObject2;
    do
    {
      logDebug("Calling getPurchases with continuation token: " + (String)localObject1);
      localObject1 = this.mService.getPurchases(3, this.mContext.getPackageName(), paramString, (String)localObject1);
      j = getResponseCodeFromBundle((Bundle)localObject1);
      logDebug("Owned items response: " + String.valueOf(j));
      if (j != 0)
      {
        logDebug("getPurchases() failed: " + getResponseDesc(j));
        return j;
      }
      if ((!((Bundle)localObject1).containsKey("INAPP_PURCHASE_ITEM_LIST")) || (!((Bundle)localObject1).containsKey("INAPP_PURCHASE_DATA_LIST")) || (!((Bundle)localObject1).containsKey("INAPP_DATA_SIGNATURE_LIST")))
      {
        logError("Bundle returned from getPurchases() doesn't contain required fields.");
        return 64534;
      }
      localObject2 = ((Bundle)localObject1).getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
      ArrayList localArrayList1 = ((Bundle)localObject1).getStringArrayList("INAPP_PURCHASE_DATA_LIST");
      ArrayList localArrayList2 = ((Bundle)localObject1).getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
      int k = 0;
      j = i;
      i = k;
      if (i < localArrayList1.size())
      {
        String str1 = (String)localArrayList1.get(i);
        Object localObject3 = (String)localArrayList2.get(i);
        String str2 = (String)((ArrayList)localObject2).get(i);
        if (Security.verifyPurchase(this.mSignatureBase64, str1, (String)localObject3))
        {
          logDebug("Sku is owned: " + str2);
          localObject3 = new Purchase(paramString, str1, (String)localObject3);
          if (TextUtils.isEmpty(((Purchase)localObject3).getToken()))
          {
            logWarn("BUG: empty/null token!");
            logDebug("Purchase data: " + str1);
          }
          paramInventory.addPurchase((Purchase)localObject3);
        }
        for (;;)
        {
          i += 1;
          break;
          logWarn("Purchase signature verification **FAILED**. Not adding item.");
          logDebug("   Purchase data: " + str1);
          logDebug("   Signature: " + (String)localObject3);
          j = 1;
        }
      }
      localObject2 = ((Bundle)localObject1).getString("INAPP_CONTINUATION_TOKEN");
      logDebug("Continuation token: " + (String)localObject2);
      localObject1 = localObject2;
      i = j;
    } while (!TextUtils.isEmpty((CharSequence)localObject2));
    if (j != 0) {}
    for (i = 64533;; i = 0) {
      return i;
    }
  }
  
  int querySkuDetails(String paramString, Inventory paramInventory, List<String> paramList)
    throws RemoteException, JSONException
  {
    logDebug("Querying SKU details.");
    Object localObject1 = new ArrayList();
    ((ArrayList)localObject1).addAll(paramInventory.getAllOwnedSkus(paramString));
    Object localObject2;
    if (paramList != null)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        localObject2 = (String)paramList.next();
        if (!((ArrayList)localObject1).contains(localObject2)) {
          ((ArrayList)localObject1).add(localObject2);
        }
      }
    }
    if (((ArrayList)localObject1).size() == 0)
    {
      logDebug("queryPrices: nothing to do because there are no SKUs.");
      return 0;
    }
    paramList = new ArrayList();
    int j = ((ArrayList)localObject1).size() / 20;
    int k = ((ArrayList)localObject1).size() % 20;
    int i = 0;
    while (i < j)
    {
      localObject2 = new ArrayList();
      Iterator localIterator = ((ArrayList)localObject1).subList(i * 20, i * 20 + 20).iterator();
      while (localIterator.hasNext()) {
        ((ArrayList)localObject2).add((String)localIterator.next());
      }
      paramList.add(localObject2);
      i += 1;
    }
    if (k != 0)
    {
      localObject2 = new ArrayList();
      localObject1 = ((ArrayList)localObject1).subList(j * 20, j * 20 + k).iterator();
      while (((Iterator)localObject1).hasNext()) {
        ((ArrayList)localObject2).add((String)((Iterator)localObject1).next());
      }
      paramList.add(localObject2);
    }
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      localObject1 = (ArrayList)paramList.next();
      localObject2 = new Bundle();
      ((Bundle)localObject2).putStringArrayList("ITEM_ID_LIST", (ArrayList)localObject1);
      localObject1 = this.mService.getSkuDetails(3, this.mContext.getPackageName(), paramString, (Bundle)localObject2);
      if (!((Bundle)localObject1).containsKey("DETAILS_LIST"))
      {
        i = getResponseCodeFromBundle((Bundle)localObject1);
        if (i != 0)
        {
          logDebug("getSkuDetails() failed: " + getResponseDesc(i));
          return i;
        }
        logError("getSkuDetails() returned a bundle with neither an error nor a detail list.");
        return 64534;
      }
      localObject1 = ((Bundle)localObject1).getStringArrayList("DETAILS_LIST").iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = new SkuDetails(paramString, (String)((Iterator)localObject1).next());
        logDebug("Got sku details: " + localObject2);
        paramInventory.addSkuDetails((SkuDetails)localObject2);
      }
    }
    return 0;
  }
  
  public void startSetup(final OnIabSetupFinishedListener paramOnIabSetupFinishedListener)
  {
    checkNotDisposed();
    if (this.mSetupDone) {
      throw new IllegalStateException("IAB helper is already set up.");
    }
    logDebug("Starting in-app billing setup.");
    this.mServiceConn = new ServiceConnection()
    {
      public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
      {
        if (IabHelper.this.mDisposed) {
          return;
        }
        IabHelper.this.logDebug("Billing service connected.");
        IabHelper.this.mService = IInAppBillingService.Stub.asInterface(paramAnonymousIBinder);
        paramAnonymousComponentName = IabHelper.this.mContext.getPackageName();
        int i;
        try
        {
          IabHelper.this.logDebug("Checking for in-app billing 3 support.");
          i = IabHelper.this.mService.isBillingSupported(3, paramAnonymousComponentName, "inapp");
          if (i != 0)
          {
            if (paramOnIabSetupFinishedListener != null) {
              paramOnIabSetupFinishedListener.onIabSetupFinished(new IabResult(i, "Error checking for billing v3 support."));
            }
            IabHelper.this.mSubscriptionsSupported = false;
            IabHelper.this.mSubscriptionUpdateSupported = false;
            return;
          }
        }
        catch (RemoteException paramAnonymousComponentName)
        {
          if (paramOnIabSetupFinishedListener != null) {
            paramOnIabSetupFinishedListener.onIabSetupFinished(new IabResult(64535, "RemoteException while setting up in-app billing."));
          }
          paramAnonymousComponentName.printStackTrace();
          return;
        }
        IabHelper.this.logDebug("In-app billing version 3 supported for " + paramAnonymousComponentName);
        if (IabHelper.this.mService.isBillingSupported(5, paramAnonymousComponentName, "subs") == 0)
        {
          IabHelper.this.logDebug("Subscription re-signup AVAILABLE.");
          IabHelper.this.mSubscriptionUpdateSupported = true;
          label211:
          if (!IabHelper.this.mSubscriptionUpdateSupported) {
            break label284;
          }
          IabHelper.this.mSubscriptionsSupported = true;
        }
        for (;;)
        {
          IabHelper.this.mSetupDone = true;
          if (paramOnIabSetupFinishedListener == null) {
            break;
          }
          paramOnIabSetupFinishedListener.onIabSetupFinished(new IabResult(0, "Setup successful."));
          return;
          IabHelper.this.logDebug("Subscription re-signup not available.");
          IabHelper.this.mSubscriptionUpdateSupported = false;
          break label211;
          label284:
          i = IabHelper.this.mService.isBillingSupported(3, paramAnonymousComponentName, "subs");
          if (i == 0)
          {
            IabHelper.this.logDebug("Subscriptions AVAILABLE.");
            IabHelper.this.mSubscriptionsSupported = true;
          }
          else
          {
            IabHelper.this.logDebug("Subscriptions NOT AVAILABLE. Response: " + i);
            IabHelper.this.mSubscriptionsSupported = false;
            IabHelper.this.mSubscriptionUpdateSupported = false;
          }
        }
      }
      
      public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
      {
        IabHelper.this.logDebug("Billing service disconnected.");
        IabHelper.this.mService = null;
      }
    };
    Intent localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
    localIntent.setPackage("com.android.vending");
    List localList = this.mContext.getPackageManager().queryIntentServices(localIntent, 0);
    if ((localList != null) && (!localList.isEmpty())) {
      this.mContext.bindService(localIntent, this.mServiceConn, 1);
    }
    while (paramOnIabSetupFinishedListener == null) {
      return;
    }
    paramOnIabSetupFinishedListener.onIabSetupFinished(new IabResult(3, "Billing service unavailable on device."));
  }
  
  public boolean subscriptionsSupported()
  {
    checkNotDisposed();
    return this.mSubscriptionsSupported;
  }
  
  public static class IabAsyncInProgressException
    extends Exception
  {
    public IabAsyncInProgressException(String paramString)
    {
      super();
    }
  }
  
  public static abstract interface OnConsumeFinishedListener
  {
    public abstract void onConsumeFinished(Purchase paramPurchase, IabResult paramIabResult);
  }
  
  public static abstract interface OnConsumeMultiFinishedListener
  {
    public abstract void onConsumeMultiFinished(List<Purchase> paramList, List<IabResult> paramList1);
  }
  
  public static abstract interface OnIabPurchaseFinishedListener
  {
    public abstract void onIabPurchaseFinished(IabResult paramIabResult, Purchase paramPurchase);
  }
  
  public static abstract interface OnIabSetupFinishedListener
  {
    public abstract void onIabSetupFinished(IabResult paramIabResult);
  }
  
  public static abstract interface QueryInventoryFinishedListener
  {
    public abstract void onQueryInventoryFinished(IabResult paramIabResult, Inventory paramInventory);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\ex\android\util\IabHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */