package com.ex.android.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public class SkuDetails
{
  private final String mDescription;
  private final String mItemType;
  private final String mJson;
  private final String mPrice;
  private final long mPriceAmountMicros;
  private final String mPriceAmountMicrosString;
  private final String mPriceCurrencyCode;
  private final float mPriceValueFloat;
  private final String mPriceValueString;
  private final String mSku;
  private final String mTitle;
  private final String mType;
  
  public SkuDetails(String paramString)
    throws JSONException
  {
    this("inapp", paramString);
  }
  
  public SkuDetails(String paramString1, String paramString2)
    throws JSONException
  {
    this.mItemType = paramString1;
    this.mJson = paramString2;
    paramString1 = new JSONObject(this.mJson);
    this.mSku = paramString1.optString("productId");
    this.mType = paramString1.optString("type");
    this.mPrice = paramString1.optString("price");
    this.mPriceAmountMicros = paramString1.optLong("price_amount_micros");
    this.mPriceCurrencyCode = paramString1.optString("price_currency_code");
    this.mTitle = paramString1.optString("title");
    this.mDescription = paramString1.optString("description");
    this.mPriceAmountMicrosString = paramString1.optString("price_amount_micros");
    if ((this.mPriceAmountMicrosString != null) && (this.mPriceAmountMicrosString.length() >= 1))
    {
      paramString1 = new BigDecimal(this.mPriceAmountMicrosString).divide(new BigDecimal(1000000));
      this.mPriceValueFloat = paramString1.floatValue();
      paramString2 = (DecimalFormat)DecimalFormat.getNumberInstance(Locale.JAPAN);
      paramString2.setGroupingUsed(false);
      this.mPriceValueString = paramString2.format(paramString1);
      return;
    }
    this.mPriceValueFloat = 0.0F;
    this.mPriceValueString = "";
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public long getPriceAmountMicros()
  {
    return this.mPriceAmountMicros;
  }
  
  public String getPriceCurrencyCode()
  {
    return this.mPriceCurrencyCode;
  }
  
  public float getPriceValueFloat()
  {
    return this.mPriceValueFloat;
  }
  
  public String getPriceValueString()
  {
    return this.mPriceValueString;
  }
  
  public String getSku()
  {
    return this.mSku;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public String toString()
  {
    return "SkuDetails:" + this.mJson;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\ex\android\util\SkuDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */