package com.ex.android.util;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class Security
{
  private static final String KEY_FACTORY_ALGORITHM = "RSA";
  private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
  private static final String TAG = "IABUtil/Security";
  
  public static PublicKey generatePublicKey(String paramString)
  {
    try
    {
      paramString = Base64.decode(paramString, 0);
      paramString = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(paramString));
      return paramString;
    }
    catch (NoSuchAlgorithmException paramString)
    {
      throw new RuntimeException(paramString);
    }
    catch (InvalidKeySpecException paramString)
    {
      Log.e("IABUtil/Security", "Invalid key specification.");
      throw new IllegalArgumentException(paramString);
    }
  }
  
  /* Error */
  public static boolean verify(PublicKey paramPublicKey, String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_2
    //   1: iconst_0
    //   2: invokestatic 31	android/util/Base64:decode	(Ljava/lang/String;I)[B
    //   5: astore_2
    //   6: ldc 11
    //   8: invokestatic 73	java/security/Signature:getInstance	(Ljava/lang/String;)Ljava/security/Signature;
    //   11: astore_3
    //   12: aload_3
    //   13: aload_0
    //   14: invokevirtual 77	java/security/Signature:initVerify	(Ljava/security/PublicKey;)V
    //   17: aload_3
    //   18: aload_1
    //   19: invokevirtual 83	java/lang/String:getBytes	()[B
    //   22: invokevirtual 86	java/security/Signature:update	([B)V
    //   25: aload_3
    //   26: aload_2
    //   27: invokevirtual 89	java/security/Signature:verify	([B)Z
    //   30: ifne +24 -> 54
    //   33: ldc 14
    //   35: ldc 91
    //   37: invokestatic 59	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   40: pop
    //   41: iconst_0
    //   42: ireturn
    //   43: astore_0
    //   44: ldc 14
    //   46: ldc 93
    //   48: invokestatic 59	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   51: pop
    //   52: iconst_0
    //   53: ireturn
    //   54: iconst_1
    //   55: ireturn
    //   56: astore_0
    //   57: ldc 14
    //   59: ldc 95
    //   61: invokestatic 59	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   64: pop
    //   65: iconst_0
    //   66: ireturn
    //   67: astore_0
    //   68: ldc 14
    //   70: ldc 53
    //   72: invokestatic 59	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   75: pop
    //   76: iconst_0
    //   77: ireturn
    //   78: astore_0
    //   79: ldc 14
    //   81: ldc 97
    //   83: invokestatic 59	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   86: pop
    //   87: iconst_0
    //   88: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	89	0	paramPublicKey	PublicKey
    //   0	89	1	paramString1	String
    //   0	89	2	paramString2	String
    //   11	15	3	localSignature	java.security.Signature
    // Exception table:
    //   from	to	target	type
    //   0	6	43	java/lang/IllegalArgumentException
    //   6	41	56	java/security/NoSuchAlgorithmException
    //   6	41	67	java/security/InvalidKeyException
    //   6	41	78	java/security/SignatureException
  }
  
  public static boolean verifyPurchase(String paramString1, String paramString2, String paramString3)
  {
    if ((TextUtils.isEmpty(paramString2)) || (TextUtils.isEmpty(paramString1)) || (TextUtils.isEmpty(paramString3)))
    {
      Log.e("IABUtil/Security", "Purchase verification failed: missing data.");
      return false;
    }
    return verify(generatePublicKey(paramString1), paramString2, paramString3);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\ex\android\util\Security.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */