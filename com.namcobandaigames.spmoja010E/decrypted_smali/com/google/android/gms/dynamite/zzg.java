package com.google.android.gms.dynamite;

import android.content.Context;

final class zzg
  implements DynamiteModule.VersionPolicy
{
  public final DynamiteModule.VersionPolicy.zzb zza(Context paramContext, String paramString, DynamiteModule.VersionPolicy.zza paramzza)
    throws DynamiteModule.LoadingException
  {
    DynamiteModule.VersionPolicy.zzb localzzb = new DynamiteModule.VersionPolicy.zzb();
    localzzb.zziq = paramzza.getLocalVersion(paramContext, paramString);
    if (localzzb.zziq != 0) {}
    for (localzzb.zzir = paramzza.zza(paramContext, paramString, false); (localzzb.zziq == 0) && (localzzb.zzir == 0); localzzb.zzir = paramzza.zza(paramContext, paramString, true))
    {
      localzzb.zzis = 0;
      return localzzb;
    }
    if (localzzb.zzir >= localzzb.zziq)
    {
      localzzb.zzis = 1;
      return localzzb;
    }
    localzzb.zzis = -1;
    return localzzb;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\dynamite\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */