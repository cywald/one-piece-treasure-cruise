package com.google.android.gms.dynamite;

import android.content.Context;

final class zzb
  implements DynamiteModule.VersionPolicy
{
  public final DynamiteModule.VersionPolicy.zzb zza(Context paramContext, String paramString, DynamiteModule.VersionPolicy.zza paramzza)
    throws DynamiteModule.LoadingException
  {
    DynamiteModule.VersionPolicy.zzb localzzb = new DynamiteModule.VersionPolicy.zzb();
    localzzb.zzir = paramzza.zza(paramContext, paramString, true);
    if (localzzb.zzir != 0) {
      localzzb.zzis = 1;
    }
    do
    {
      return localzzb;
      localzzb.zziq = paramzza.getLocalVersion(paramContext, paramString);
    } while (localzzb.zziq == 0);
    localzzb.zzis = -1;
    return localzzb;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\dynamite\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */