package com.google.android.gms.dynamite;

import android.content.Context;
import android.database.Cursor;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.CrashUtils;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.lang.reflect.Field;
import javax.annotation.concurrent.GuardedBy;

@KeepForSdk
public final class DynamiteModule
{
  @KeepForSdk
  public static final VersionPolicy PREFER_HIGHEST_OR_LOCAL_VERSION = new zzd();
  @KeepForSdk
  public static final VersionPolicy PREFER_HIGHEST_OR_LOCAL_VERSION_NO_FORCE_STAGING = new zze();
  @KeepForSdk
  public static final VersionPolicy PREFER_HIGHEST_OR_REMOTE_VERSION = new zzf();
  @KeepForSdk
  public static final VersionPolicy PREFER_REMOTE;
  @GuardedBy("DynamiteModule.class")
  private static Boolean zzid;
  @GuardedBy("DynamiteModule.class")
  private static zzi zzie;
  @GuardedBy("DynamiteModule.class")
  private static zzk zzif;
  @GuardedBy("DynamiteModule.class")
  private static String zzig;
  @GuardedBy("DynamiteModule.class")
  private static int zzih = -1;
  private static final ThreadLocal<zza> zzii = new ThreadLocal();
  private static final DynamiteModule.VersionPolicy.zza zzij = new zza();
  private static final VersionPolicy zzik;
  private static final VersionPolicy zzil = new zzg();
  private final Context zzim;
  
  static
  {
    PREFER_REMOTE = new zzb();
    zzik = new zzc();
  }
  
  private DynamiteModule(Context paramContext)
  {
    this.zzim = ((Context)Preconditions.checkNotNull(paramContext));
  }
  
  @KeepForSdk
  public static int getLocalVersion(Context paramContext, String paramString)
  {
    try
    {
      Object localObject = paramContext.getApplicationContext().getClassLoader().loadClass(String.valueOf(paramString).length() + 61 + "com.google.android.gms.dynamite.descriptors." + paramString + ".ModuleDescriptor");
      paramContext = ((Class)localObject).getDeclaredField("MODULE_ID");
      localObject = ((Class)localObject).getDeclaredField("MODULE_VERSION");
      if (!paramContext.get(null).equals(paramString))
      {
        paramContext = String.valueOf(paramContext.get(null));
        Log.e("DynamiteModule", String.valueOf(paramContext).length() + 51 + String.valueOf(paramString).length() + "Module descriptor id '" + paramContext + "' didn't match expected id '" + paramString + "'");
        return 0;
      }
      int i = ((Field)localObject).getInt(null);
      return i;
    }
    catch (ClassNotFoundException paramContext)
    {
      Log.w("DynamiteModule", String.valueOf(paramString).length() + 45 + "Local module descriptor class for " + paramString + " not found.");
      return 0;
    }
    catch (Exception paramContext)
    {
      paramContext = String.valueOf(paramContext.getMessage());
      if (paramContext.length() == 0) {}
    }
    for (paramContext = "Failed to load module descriptor class: ".concat(paramContext);; paramContext = new String("Failed to load module descriptor class: "))
    {
      Log.e("DynamiteModule", paramContext);
      break;
    }
  }
  
  @KeepForSdk
  public static int getRemoteVersion(Context paramContext, String paramString)
  {
    return zza(paramContext, paramString, false);
  }
  
  @KeepForSdk
  public static DynamiteModule load(Context paramContext, VersionPolicy paramVersionPolicy, String paramString)
    throws DynamiteModule.LoadingException
  {
    zza localzza1 = (zza)zzii.get();
    zza localzza2 = new zza(null);
    zzii.set(localzza2);
    DynamiteModule.VersionPolicy.zzb localzzb;
    try
    {
      localzzb = paramVersionPolicy.zza(paramContext, paramString, zzij);
      i = localzzb.zziq;
      int j = localzzb.zzir;
      Log.i("DynamiteModule", String.valueOf(paramString).length() + 68 + String.valueOf(paramString).length() + "Considering local module " + paramString + ":" + i + " and remote module " + paramString + ":" + j);
      if ((localzzb.zzis == 0) || ((localzzb.zzis == -1) && (localzzb.zziq == 0)) || ((localzzb.zzis == 1) && (localzzb.zzir == 0)))
      {
        i = localzzb.zziq;
        j = localzzb.zzir;
        throw new LoadingException(91 + "No acceptable module found. Local version is " + i + " and remote version is " + j + ".", null);
      }
    }
    finally
    {
      if (localzza2.zzin != null) {
        localzza2.zzin.close();
      }
      zzii.set(localzza1);
    }
    if (localzzb.zzis == -1)
    {
      paramContext = zze(paramContext, paramString);
      if (localzza2.zzin != null) {
        localzza2.zzin.close();
      }
      zzii.set(localzza1);
      return paramContext;
    }
    int i = localzzb.zzis;
    if (i == 1) {
      try
      {
        localObject = zza(paramContext, paramString, localzzb.zzir);
        if (localzza2.zzin != null) {
          localzza2.zzin.close();
        }
        zzii.set(localzza1);
        return (DynamiteModule)localObject;
      }
      catch (LoadingException localLoadingException)
      {
        Object localObject = String.valueOf(localLoadingException.getMessage());
        if (((String)localObject).length() != 0) {}
        for (localObject = "Failed to load remote module: ".concat((String)localObject);; localObject = new String("Failed to load remote module: "))
        {
          Log.w("DynamiteModule", (String)localObject);
          if ((localzzb.zziq == 0) || (paramVersionPolicy.zza(paramContext, paramString, new zzb(localzzb.zziq, 0)).zzis != -1)) {
            break;
          }
          paramContext = zze(paramContext, paramString);
          if (localzza2.zzin != null) {
            localzza2.zzin.close();
          }
          zzii.set(localzza1);
          return paramContext;
        }
        throw new LoadingException("Remote load failed. No local fallback found.", localLoadingException, null);
      }
    }
    i = localzzb.zzis;
    throw new LoadingException(47 + "VersionPolicy returned invalid code:" + i, null);
  }
  
  /* Error */
  public static int zza(Context paramContext, String paramString, boolean paramBoolean)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 301	com/google/android/gms/dynamite/DynamiteModule:zzid	Ljava/lang/Boolean;
    //   6: astore 6
    //   8: aload 6
    //   10: astore 5
    //   12: aload 6
    //   14: ifnonnull +70 -> 84
    //   17: aload_0
    //   18: invokevirtual 120	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   21: invokevirtual 124	android/content/Context:getClassLoader	()Ljava/lang/ClassLoader;
    //   24: ldc 6
    //   26: invokevirtual 304	java/lang/Class:getName	()Ljava/lang/String;
    //   29: invokevirtual 157	java/lang/ClassLoader:loadClass	(Ljava/lang/String;)Ljava/lang/Class;
    //   32: astore 6
    //   34: aload 6
    //   36: ldc_w 306
    //   39: invokevirtual 165	java/lang/Class:getDeclaredField	(Ljava/lang/String;)Ljava/lang/reflect/Field;
    //   42: astore 5
    //   44: aload 6
    //   46: monitorenter
    //   47: aload 5
    //   49: aconst_null
    //   50: invokevirtual 172	java/lang/reflect/Field:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   53: checkcast 153	java/lang/ClassLoader
    //   56: astore 7
    //   58: aload 7
    //   60: ifnull +61 -> 121
    //   63: aload 7
    //   65: invokestatic 309	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
    //   68: if_acmpne +40 -> 108
    //   71: getstatic 314	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   74: astore 5
    //   76: aload 6
    //   78: monitorexit
    //   79: aload 5
    //   81: putstatic 301	com/google/android/gms/dynamite/DynamiteModule:zzid	Ljava/lang/Boolean;
    //   84: ldc 2
    //   86: monitorexit
    //   87: aload 5
    //   89: invokevirtual 318	java/lang/Boolean:booleanValue	()Z
    //   92: istore 4
    //   94: iload 4
    //   96: ifeq +278 -> 374
    //   99: aload_0
    //   100: aload_1
    //   101: iload_2
    //   102: invokestatic 321	com/google/android/gms/dynamite/DynamiteModule:zzc	(Landroid/content/Context;Ljava/lang/String;Z)I
    //   105: istore_3
    //   106: iload_3
    //   107: ireturn
    //   108: aload 7
    //   110: invokestatic 324	com/google/android/gms/dynamite/DynamiteModule:zza	(Ljava/lang/ClassLoader;)V
    //   113: getstatic 327	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   116: astore 5
    //   118: goto -42 -> 76
    //   121: ldc_w 329
    //   124: aload_0
    //   125: invokevirtual 120	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   128: invokevirtual 332	android/content/Context:getPackageName	()Ljava/lang/String;
    //   131: invokevirtual 333	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   134: ifeq +20 -> 154
    //   137: aload 5
    //   139: aconst_null
    //   140: invokestatic 309	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
    //   143: invokevirtual 336	java/lang/reflect/Field:set	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   146: getstatic 314	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   149: astore 5
    //   151: goto -75 -> 76
    //   154: aload_0
    //   155: aload_1
    //   156: iload_2
    //   157: invokestatic 321	com/google/android/gms/dynamite/DynamiteModule:zzc	(Landroid/content/Context;Ljava/lang/String;Z)I
    //   160: istore_3
    //   161: getstatic 338	com/google/android/gms/dynamite/DynamiteModule:zzig	Ljava/lang/String;
    //   164: ifnull +16 -> 180
    //   167: getstatic 338	com/google/android/gms/dynamite/DynamiteModule:zzig	Ljava/lang/String;
    //   170: invokevirtual 341	java/lang/String:isEmpty	()Z
    //   173: istore 4
    //   175: iload 4
    //   177: ifeq +26 -> 203
    //   180: aload 6
    //   182: monitorexit
    //   183: ldc 2
    //   185: monitorexit
    //   186: iload_3
    //   187: ireturn
    //   188: astore_1
    //   189: ldc 2
    //   191: monitorexit
    //   192: aload_1
    //   193: athrow
    //   194: astore_1
    //   195: aload_0
    //   196: aload_1
    //   197: invokestatic 347	com/google/android/gms/common/util/CrashUtils:addDynamiteErrorToDropBox	(Landroid/content/Context;Ljava/lang/Throwable;)Z
    //   200: pop
    //   201: aload_1
    //   202: athrow
    //   203: new 349	com/google/android/gms/dynamite/zzh
    //   206: dup
    //   207: getstatic 338	com/google/android/gms/dynamite/DynamiteModule:zzig	Ljava/lang/String;
    //   210: invokestatic 309	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
    //   213: invokespecial 352	com/google/android/gms/dynamite/zzh:<init>	(Ljava/lang/String;Ljava/lang/ClassLoader;)V
    //   216: astore 7
    //   218: aload 7
    //   220: invokestatic 324	com/google/android/gms/dynamite/DynamiteModule:zza	(Ljava/lang/ClassLoader;)V
    //   223: aload 5
    //   225: aconst_null
    //   226: aload 7
    //   228: invokevirtual 336	java/lang/reflect/Field:set	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   231: getstatic 327	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   234: putstatic 301	com/google/android/gms/dynamite/DynamiteModule:zzid	Ljava/lang/Boolean;
    //   237: aload 6
    //   239: monitorexit
    //   240: ldc 2
    //   242: monitorexit
    //   243: iload_3
    //   244: ireturn
    //   245: astore 7
    //   247: aload 5
    //   249: aconst_null
    //   250: invokestatic 309	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
    //   253: invokevirtual 336	java/lang/reflect/Field:set	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   256: getstatic 314	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   259: astore 5
    //   261: goto -185 -> 76
    //   264: astore 5
    //   266: aload 6
    //   268: monitorexit
    //   269: aload 5
    //   271: athrow
    //   272: astore 5
    //   274: aload 5
    //   276: invokestatic 132	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   279: astore 5
    //   281: ldc -78
    //   283: new 126	java/lang/StringBuilder
    //   286: dup
    //   287: aload 5
    //   289: invokestatic 132	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   292: invokevirtual 136	java/lang/String:length	()I
    //   295: bipush 30
    //   297: iadd
    //   298: invokespecial 139	java/lang/StringBuilder:<init>	(I)V
    //   301: ldc_w 354
    //   304: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   307: aload 5
    //   309: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   312: invokevirtual 151	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   315: invokestatic 201	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   318: pop
    //   319: getstatic 314	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   322: astore 5
    //   324: goto -245 -> 79
    //   327: astore_1
    //   328: aload_1
    //   329: invokevirtual 280	com/google/android/gms/dynamite/DynamiteModule$LoadingException:getMessage	()Ljava/lang/String;
    //   332: invokestatic 132	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   335: astore_1
    //   336: aload_1
    //   337: invokevirtual 136	java/lang/String:length	()I
    //   340: ifeq +20 -> 360
    //   343: ldc_w 356
    //   346: aload_1
    //   347: invokevirtual 210	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   350: astore_1
    //   351: ldc -78
    //   353: aload_1
    //   354: invokestatic 201	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   357: pop
    //   358: iconst_0
    //   359: ireturn
    //   360: new 128	java/lang/String
    //   363: dup
    //   364: ldc_w 356
    //   367: invokespecial 213	java/lang/String:<init>	(Ljava/lang/String;)V
    //   370: astore_1
    //   371: goto -20 -> 351
    //   374: aload_0
    //   375: aload_1
    //   376: iload_2
    //   377: invokestatic 358	com/google/android/gms/dynamite/DynamiteModule:zzb	(Landroid/content/Context;Ljava/lang/String;Z)I
    //   380: istore_3
    //   381: iload_3
    //   382: ireturn
    //   383: astore 5
    //   385: goto -272 -> 113
    //   388: astore 5
    //   390: goto -116 -> 274
    //   393: astore 5
    //   395: goto -121 -> 274
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	398	0	paramContext	Context
    //   0	398	1	paramString	String
    //   0	398	2	paramBoolean	boolean
    //   105	277	3	i	int
    //   92	84	4	bool	boolean
    //   10	250	5	localObject1	Object
    //   264	6	5	localObject2	Object
    //   272	3	5	localClassNotFoundException	ClassNotFoundException
    //   279	44	5	localObject3	Object
    //   383	1	5	localLoadingException1	LoadingException
    //   388	1	5	localNoSuchFieldException	NoSuchFieldException
    //   393	1	5	localIllegalAccessException	IllegalAccessException
    //   6	261	6	localObject4	Object
    //   56	171	7	localObject5	Object
    //   245	1	7	localLoadingException2	LoadingException
    // Exception table:
    //   from	to	target	type
    //   3	8	188	finally
    //   17	47	188	finally
    //   79	84	188	finally
    //   84	87	188	finally
    //   183	186	188	finally
    //   189	192	188	finally
    //   240	243	188	finally
    //   269	272	188	finally
    //   274	324	188	finally
    //   0	3	194	java/lang/Throwable
    //   87	94	194	java/lang/Throwable
    //   99	106	194	java/lang/Throwable
    //   192	194	194	java/lang/Throwable
    //   328	351	194	java/lang/Throwable
    //   351	358	194	java/lang/Throwable
    //   360	371	194	java/lang/Throwable
    //   374	381	194	java/lang/Throwable
    //   154	175	245	com/google/android/gms/dynamite/DynamiteModule$LoadingException
    //   203	237	245	com/google/android/gms/dynamite/DynamiteModule$LoadingException
    //   47	58	264	finally
    //   63	76	264	finally
    //   76	79	264	finally
    //   108	113	264	finally
    //   113	118	264	finally
    //   121	151	264	finally
    //   154	175	264	finally
    //   180	183	264	finally
    //   203	237	264	finally
    //   237	240	264	finally
    //   247	261	264	finally
    //   266	269	264	finally
    //   17	47	272	java/lang/ClassNotFoundException
    //   269	272	272	java/lang/ClassNotFoundException
    //   99	106	327	com/google/android/gms/dynamite/DynamiteModule$LoadingException
    //   108	113	383	com/google/android/gms/dynamite/DynamiteModule$LoadingException
    //   17	47	388	java/lang/NoSuchFieldException
    //   269	272	388	java/lang/NoSuchFieldException
    //   17	47	393	java/lang/IllegalAccessException
    //   269	272	393	java/lang/IllegalAccessException
  }
  
  private static Context zza(Context paramContext, String paramString, int paramInt, Cursor paramCursor, zzk paramzzk)
  {
    try
    {
      ObjectWrapper.wrap(null);
      if (zzai().booleanValue()) {
        Log.v("DynamiteModule", "Dynamite loader version >= 2, using loadModule2NoCrashUtils");
      }
      for (paramContext = paramzzk.zzb(ObjectWrapper.wrap(paramContext), paramString, paramInt, ObjectWrapper.wrap(paramCursor));; paramContext = paramzzk.zza(ObjectWrapper.wrap(paramContext), paramString, paramInt, ObjectWrapper.wrap(paramCursor)))
      {
        return (Context)ObjectWrapper.unwrap(paramContext);
        Log.w("DynamiteModule", "Dynamite loader version < 2, falling back to loadModule2");
      }
      paramContext = "Failed to load DynamiteLoader: ".concat(paramContext);
    }
    catch (Exception paramContext)
    {
      paramContext = String.valueOf(paramContext.toString());
      if (paramContext.length() == 0) {}
    }
    for (;;)
    {
      Log.e("DynamiteModule", paramContext);
      return null;
      paramContext = new String("Failed to load DynamiteLoader: ");
    }
  }
  
  private static DynamiteModule zza(Context paramContext, String paramString, int paramInt)
    throws DynamiteModule.LoadingException
  {
    try
    {
      Boolean localBoolean;
      if (!localBoolean.booleanValue()) {
        break label55;
      }
    }
    catch (Throwable paramString)
    {
      try
      {
        localBoolean = zzid;
        if (localBoolean != null) {
          break label41;
        }
        throw new LoadingException("Failed to determine which loading route to use.", null);
      }
      finally {}
      paramString = paramString;
      CrashUtils.addDynamiteErrorToDropBox(paramContext, paramString);
      throw paramString;
    }
    label41:
    return zzc(paramContext, paramString, paramInt);
    label55:
    paramString = zzb(paramContext, paramString, paramInt);
    return paramString;
  }
  
  /* Error */
  @GuardedBy("DynamiteModule.class")
  private static void zza(ClassLoader paramClassLoader)
    throws DynamiteModule.LoadingException
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc_w 404
    //   4: invokevirtual 157	java/lang/ClassLoader:loadClass	(Ljava/lang/String;)Ljava/lang/Class;
    //   7: iconst_0
    //   8: anewarray 161	java/lang/Class
    //   11: invokevirtual 408	java/lang/Class:getConstructor	([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    //   14: iconst_0
    //   15: anewarray 4	java/lang/Object
    //   18: invokevirtual 414	java/lang/reflect/Constructor:newInstance	([Ljava/lang/Object;)Ljava/lang/Object;
    //   21: checkcast 416	android/os/IBinder
    //   24: astore_0
    //   25: aload_0
    //   26: ifnonnull +10 -> 36
    //   29: aconst_null
    //   30: astore_0
    //   31: aload_0
    //   32: putstatic 418	com/google/android/gms/dynamite/DynamiteModule:zzif	Lcom/google/android/gms/dynamite/zzk;
    //   35: return
    //   36: aload_0
    //   37: ldc_w 420
    //   40: invokeinterface 424 2 0
    //   45: astore_1
    //   46: aload_1
    //   47: instanceof 376
    //   50: ifeq +11 -> 61
    //   53: aload_1
    //   54: checkcast 376	com/google/android/gms/dynamite/zzk
    //   57: astore_0
    //   58: goto -27 -> 31
    //   61: new 426	com/google/android/gms/dynamite/zzl
    //   64: dup
    //   65: aload_0
    //   66: invokespecial 429	com/google/android/gms/dynamite/zzl:<init>	(Landroid/os/IBinder;)V
    //   69: astore_0
    //   70: goto -39 -> 31
    //   73: astore_0
    //   74: new 9	com/google/android/gms/dynamite/DynamiteModule$LoadingException
    //   77: dup
    //   78: ldc_w 431
    //   81: aload_0
    //   82: aconst_null
    //   83: invokespecial 290	com/google/android/gms/dynamite/DynamiteModule$LoadingException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;Lcom/google/android/gms/dynamite/zza;)V
    //   86: athrow
    //   87: astore_0
    //   88: goto -14 -> 74
    //   91: astore_0
    //   92: goto -18 -> 74
    //   95: astore_0
    //   96: goto -22 -> 74
    //   99: astore_0
    //   100: goto -26 -> 74
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	103	0	paramClassLoader	ClassLoader
    //   45	9	1	localIInterface	android.os.IInterface
    // Exception table:
    //   from	to	target	type
    //   0	25	73	java/lang/ClassNotFoundException
    //   31	35	73	java/lang/ClassNotFoundException
    //   36	58	73	java/lang/ClassNotFoundException
    //   61	70	73	java/lang/ClassNotFoundException
    //   0	25	87	java/lang/InstantiationException
    //   31	35	87	java/lang/InstantiationException
    //   36	58	87	java/lang/InstantiationException
    //   61	70	87	java/lang/InstantiationException
    //   0	25	91	java/lang/IllegalAccessException
    //   31	35	91	java/lang/IllegalAccessException
    //   36	58	91	java/lang/IllegalAccessException
    //   61	70	91	java/lang/IllegalAccessException
    //   0	25	95	java/lang/NoSuchMethodException
    //   31	35	95	java/lang/NoSuchMethodException
    //   36	58	95	java/lang/NoSuchMethodException
    //   61	70	95	java/lang/NoSuchMethodException
    //   0	25	99	java/lang/reflect/InvocationTargetException
    //   31	35	99	java/lang/reflect/InvocationTargetException
    //   36	58	99	java/lang/reflect/InvocationTargetException
    //   61	70	99	java/lang/reflect/InvocationTargetException
  }
  
  private static Boolean zzai()
  {
    for (;;)
    {
      try
      {
        if (zzih >= 2)
        {
          bool = true;
          return Boolean.valueOf(bool);
        }
      }
      finally {}
      boolean bool = false;
    }
  }
  
  private static int zzb(Context paramContext, String paramString, boolean paramBoolean)
  {
    zzi localzzi = zzj(paramContext);
    if (localzzi == null) {
      return 0;
    }
    try
    {
      if (localzzi.zzaj() >= 2) {
        return localzzi.zzb(ObjectWrapper.wrap(paramContext), paramString, paramBoolean);
      }
      Log.w("DynamiteModule", "IDynamite loader version < 2, falling back to getModuleVersion2");
      int i = localzzi.zza(ObjectWrapper.wrap(paramContext), paramString, paramBoolean);
      return i;
    }
    catch (RemoteException paramContext)
    {
      paramContext = String.valueOf(paramContext.getMessage());
      if (paramContext.length() == 0) {}
    }
    for (paramContext = "Failed to retrieve remote module version: ".concat(paramContext);; paramContext = new String("Failed to retrieve remote module version: "))
    {
      Log.w("DynamiteModule", paramContext);
      return 0;
    }
  }
  
  private static DynamiteModule zzb(Context paramContext, String paramString, int paramInt)
    throws DynamiteModule.LoadingException
  {
    Log.i("DynamiteModule", String.valueOf(paramString).length() + 51 + "Selected remote version of " + paramString + ", version >= " + paramInt);
    zzi localzzi = zzj(paramContext);
    if (localzzi == null) {
      throw new LoadingException("Failed to create IDynamiteLoader.", null);
    }
    try
    {
      if (localzzi.zzaj() >= 2) {}
      for (paramContext = localzzi.zzb(ObjectWrapper.wrap(paramContext), paramString, paramInt); ObjectWrapper.unwrap(paramContext) == null; paramContext = localzzi.zza(ObjectWrapper.wrap(paramContext), paramString, paramInt))
      {
        throw new LoadingException("Failed to load remote module.", null);
        Log.w("DynamiteModule", "Dynamite loader version < 2, falling back to createModuleContext");
      }
      return new DynamiteModule((Context)ObjectWrapper.unwrap(paramContext));
    }
    catch (RemoteException paramContext)
    {
      throw new LoadingException("Failed to load remote module.", paramContext, null);
    }
  }
  
  /* Error */
  private static int zzc(Context paramContext, String paramString, boolean paramBoolean)
    throws DynamiteModule.LoadingException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 474	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   4: astore 5
    //   6: iload_2
    //   7: ifeq +138 -> 145
    //   10: ldc_w 476
    //   13: astore_0
    //   14: aload 5
    //   16: new 126	java/lang/StringBuilder
    //   19: dup
    //   20: aload_0
    //   21: invokestatic 132	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   24: invokevirtual 136	java/lang/String:length	()I
    //   27: bipush 42
    //   29: iadd
    //   30: aload_1
    //   31: invokestatic 132	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   34: invokevirtual 136	java/lang/String:length	()I
    //   37: iadd
    //   38: invokespecial 139	java/lang/StringBuilder:<init>	(I)V
    //   41: ldc_w 478
    //   44: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: aload_0
    //   48: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   51: ldc_w 480
    //   54: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: aload_1
    //   58: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: invokevirtual 151	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   64: invokestatic 486	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   67: aconst_null
    //   68: aconst_null
    //   69: aconst_null
    //   70: aconst_null
    //   71: invokevirtual 492	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   74: astore_0
    //   75: aload_0
    //   76: ifnull +14 -> 90
    //   79: aload_0
    //   80: astore_1
    //   81: aload_0
    //   82: invokeinterface 495 1 0
    //   87: ifne +65 -> 152
    //   90: aload_0
    //   91: astore_1
    //   92: ldc -78
    //   94: ldc_w 497
    //   97: invokestatic 201	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   100: pop
    //   101: aload_0
    //   102: astore_1
    //   103: new 9	com/google/android/gms/dynamite/DynamiteModule$LoadingException
    //   106: dup
    //   107: ldc_w 499
    //   110: aconst_null
    //   111: invokespecial 263	com/google/android/gms/dynamite/DynamiteModule$LoadingException:<init>	(Ljava/lang/String;Lcom/google/android/gms/dynamite/zza;)V
    //   114: athrow
    //   115: astore 5
    //   117: aload_0
    //   118: astore_1
    //   119: aload 5
    //   121: instanceof 9
    //   124: ifeq +159 -> 283
    //   127: aload_0
    //   128: astore_1
    //   129: aload 5
    //   131: athrow
    //   132: astore_0
    //   133: aload_1
    //   134: ifnull +9 -> 143
    //   137: aload_1
    //   138: invokeinterface 272 1 0
    //   143: aload_0
    //   144: athrow
    //   145: ldc_w 501
    //   148: astore_0
    //   149: goto -135 -> 14
    //   152: aload_0
    //   153: astore_1
    //   154: aload_0
    //   155: iconst_0
    //   156: invokeinterface 504 2 0
    //   161: istore_3
    //   162: aload_0
    //   163: astore 5
    //   165: iload_3
    //   166: ifle +93 -> 259
    //   169: aload_0
    //   170: astore_1
    //   171: ldc 2
    //   173: monitorenter
    //   174: aload_0
    //   175: iconst_2
    //   176: invokeinterface 508 2 0
    //   181: putstatic 338	com/google/android/gms/dynamite/DynamiteModule:zzig	Ljava/lang/String;
    //   184: aload_0
    //   185: ldc_w 510
    //   188: invokeinterface 514 2 0
    //   193: istore 4
    //   195: iload 4
    //   197: iflt +14 -> 211
    //   200: aload_0
    //   201: iload 4
    //   203: invokeinterface 504 2 0
    //   208: putstatic 55	com/google/android/gms/dynamite/DynamiteModule:zzih	I
    //   211: ldc 2
    //   213: monitorexit
    //   214: aload_0
    //   215: astore_1
    //   216: getstatic 62	com/google/android/gms/dynamite/DynamiteModule:zzii	Ljava/lang/ThreadLocal;
    //   219: invokevirtual 223	java/lang/ThreadLocal:get	()Ljava/lang/Object;
    //   222: checkcast 21	com/google/android/gms/dynamite/DynamiteModule$zza
    //   225: astore 6
    //   227: aload_0
    //   228: astore 5
    //   230: aload 6
    //   232: ifnull +27 -> 259
    //   235: aload_0
    //   236: astore_1
    //   237: aload_0
    //   238: astore 5
    //   240: aload 6
    //   242: getfield 267	com/google/android/gms/dynamite/DynamiteModule$zza:zzin	Landroid/database/Cursor;
    //   245: ifnonnull +14 -> 259
    //   248: aload_0
    //   249: astore_1
    //   250: aload 6
    //   252: aload_0
    //   253: putfield 267	com/google/android/gms/dynamite/DynamiteModule$zza:zzin	Landroid/database/Cursor;
    //   256: aconst_null
    //   257: astore 5
    //   259: aload 5
    //   261: ifnull +10 -> 271
    //   264: aload 5
    //   266: invokeinterface 272 1 0
    //   271: iload_3
    //   272: ireturn
    //   273: astore 5
    //   275: ldc 2
    //   277: monitorexit
    //   278: aload_0
    //   279: astore_1
    //   280: aload 5
    //   282: athrow
    //   283: aload_0
    //   284: astore_1
    //   285: new 9	com/google/android/gms/dynamite/DynamiteModule$LoadingException
    //   288: dup
    //   289: ldc_w 516
    //   292: aload 5
    //   294: aconst_null
    //   295: invokespecial 290	com/google/android/gms/dynamite/DynamiteModule$LoadingException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;Lcom/google/android/gms/dynamite/zza;)V
    //   298: athrow
    //   299: astore_0
    //   300: aconst_null
    //   301: astore_1
    //   302: goto -169 -> 133
    //   305: astore 5
    //   307: aconst_null
    //   308: astore_0
    //   309: goto -192 -> 117
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	312	0	paramContext	Context
    //   0	312	1	paramString	String
    //   0	312	2	paramBoolean	boolean
    //   161	111	3	i	int
    //   193	9	4	j	int
    //   4	11	5	localContentResolver	android.content.ContentResolver
    //   115	15	5	localException1	Exception
    //   163	102	5	localContext	Context
    //   273	20	5	localThrowable	Throwable
    //   305	1	5	localException2	Exception
    //   225	26	6	localzza	zza
    // Exception table:
    //   from	to	target	type
    //   81	90	115	java/lang/Exception
    //   92	101	115	java/lang/Exception
    //   103	115	115	java/lang/Exception
    //   154	162	115	java/lang/Exception
    //   171	174	115	java/lang/Exception
    //   216	227	115	java/lang/Exception
    //   240	248	115	java/lang/Exception
    //   250	256	115	java/lang/Exception
    //   280	283	115	java/lang/Exception
    //   81	90	132	finally
    //   92	101	132	finally
    //   103	115	132	finally
    //   119	127	132	finally
    //   129	132	132	finally
    //   154	162	132	finally
    //   171	174	132	finally
    //   216	227	132	finally
    //   240	248	132	finally
    //   250	256	132	finally
    //   280	283	132	finally
    //   285	299	132	finally
    //   174	195	273	finally
    //   200	211	273	finally
    //   211	214	273	finally
    //   275	278	273	finally
    //   0	6	299	finally
    //   14	75	299	finally
    //   0	6	305	java/lang/Exception
    //   14	75	305	java/lang/Exception
  }
  
  private static DynamiteModule zzc(Context paramContext, String paramString, int paramInt)
    throws DynamiteModule.LoadingException
  {
    Log.i("DynamiteModule", String.valueOf(paramString).length() + 51 + "Selected remote version of " + paramString + ", version >= " + paramInt);
    zzk localzzk;
    try
    {
      localzzk = zzif;
      if (localzzk == null) {
        throw new LoadingException("DynamiteLoaderV2 was not cached.", null);
      }
    }
    finally {}
    zza localzza = (zza)zzii.get();
    if ((localzza == null) || (localzza.zzin == null)) {
      throw new LoadingException("No result cursor", null);
    }
    paramContext = zza(paramContext.getApplicationContext(), paramString, paramInt, localzza.zzin, localzzk);
    if (paramContext == null) {
      throw new LoadingException("Failed to get module context", null);
    }
    return new DynamiteModule(paramContext);
  }
  
  private static DynamiteModule zze(Context paramContext, String paramString)
  {
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {}
    for (paramString = "Selected local version of ".concat(paramString);; paramString = new String("Selected local version of "))
    {
      Log.i("DynamiteModule", paramString);
      return new DynamiteModule(paramContext.getApplicationContext());
    }
  }
  
  /* Error */
  private static zzi zzj(Context paramContext)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 528	com/google/android/gms/dynamite/DynamiteModule:zzie	Lcom/google/android/gms/dynamite/zzi;
    //   6: ifnull +12 -> 18
    //   9: getstatic 528	com/google/android/gms/dynamite/DynamiteModule:zzie	Lcom/google/android/gms/dynamite/zzi;
    //   12: astore_0
    //   13: ldc 2
    //   15: monitorexit
    //   16: aload_0
    //   17: areturn
    //   18: invokestatic 534	com/google/android/gms/common/GoogleApiAvailabilityLight:getInstance	()Lcom/google/android/gms/common/GoogleApiAvailabilityLight;
    //   21: aload_0
    //   22: invokevirtual 538	com/google/android/gms/common/GoogleApiAvailabilityLight:isGooglePlayServicesAvailable	(Landroid/content/Context;)I
    //   25: ifeq +8 -> 33
    //   28: ldc 2
    //   30: monitorexit
    //   31: aconst_null
    //   32: areturn
    //   33: aload_0
    //   34: ldc_w 329
    //   37: iconst_3
    //   38: invokevirtual 542	android/content/Context:createPackageContext	(Ljava/lang/String;I)Landroid/content/Context;
    //   41: invokevirtual 124	android/content/Context:getClassLoader	()Ljava/lang/ClassLoader;
    //   44: ldc_w 544
    //   47: invokevirtual 157	java/lang/ClassLoader:loadClass	(Ljava/lang/String;)Ljava/lang/Class;
    //   50: invokevirtual 546	java/lang/Class:newInstance	()Ljava/lang/Object;
    //   53: checkcast 416	android/os/IBinder
    //   56: astore_0
    //   57: aload_0
    //   58: ifnonnull +24 -> 82
    //   61: aconst_null
    //   62: astore_0
    //   63: aload_0
    //   64: ifnull +86 -> 150
    //   67: aload_0
    //   68: putstatic 528	com/google/android/gms/dynamite/DynamiteModule:zzie	Lcom/google/android/gms/dynamite/zzi;
    //   71: ldc 2
    //   73: monitorexit
    //   74: aload_0
    //   75: areturn
    //   76: astore_0
    //   77: ldc 2
    //   79: monitorexit
    //   80: aload_0
    //   81: athrow
    //   82: aload_0
    //   83: ldc_w 548
    //   86: invokeinterface 424 2 0
    //   91: astore_1
    //   92: aload_1
    //   93: instanceof 442
    //   96: ifeq +11 -> 107
    //   99: aload_1
    //   100: checkcast 442	com/google/android/gms/dynamite/zzi
    //   103: astore_0
    //   104: goto -41 -> 63
    //   107: new 550	com/google/android/gms/dynamite/zzj
    //   110: dup
    //   111: aload_0
    //   112: invokespecial 551	com/google/android/gms/dynamite/zzj:<init>	(Landroid/os/IBinder;)V
    //   115: astore_0
    //   116: goto -53 -> 63
    //   119: astore_0
    //   120: aload_0
    //   121: invokevirtual 204	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   124: invokestatic 132	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   127: astore_0
    //   128: aload_0
    //   129: invokevirtual 136	java/lang/String:length	()I
    //   132: ifeq +23 -> 155
    //   135: ldc_w 553
    //   138: aload_0
    //   139: invokevirtual 210	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   142: astore_0
    //   143: ldc -78
    //   145: aload_0
    //   146: invokestatic 190	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   149: pop
    //   150: ldc 2
    //   152: monitorexit
    //   153: aconst_null
    //   154: areturn
    //   155: new 128	java/lang/String
    //   158: dup
    //   159: ldc_w 553
    //   162: invokespecial 213	java/lang/String:<init>	(Ljava/lang/String;)V
    //   165: astore_0
    //   166: goto -23 -> 143
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	169	0	paramContext	Context
    //   91	9	1	localIInterface	android.os.IInterface
    // Exception table:
    //   from	to	target	type
    //   3	16	76	finally
    //   18	31	76	finally
    //   33	57	76	finally
    //   67	71	76	finally
    //   71	74	76	finally
    //   77	80	76	finally
    //   82	104	76	finally
    //   107	116	76	finally
    //   120	143	76	finally
    //   143	150	76	finally
    //   150	153	76	finally
    //   155	166	76	finally
    //   33	57	119	java/lang/Exception
    //   67	71	119	java/lang/Exception
    //   82	104	119	java/lang/Exception
    //   107	116	119	java/lang/Exception
  }
  
  @KeepForSdk
  public final Context getModuleContext()
  {
    return this.zzim;
  }
  
  @KeepForSdk
  public final IBinder instantiate(String paramString)
    throws DynamiteModule.LoadingException
  {
    try
    {
      IBinder localIBinder = (IBinder)this.zzim.getClassLoader().loadClass(paramString).newInstance();
      return localIBinder;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      paramString = String.valueOf(paramString);
      if (paramString.length() != 0) {}
      for (paramString = "Failed to instantiate module class: ".concat(paramString);; paramString = new String("Failed to instantiate module class: ")) {
        throw new LoadingException(paramString, localClassNotFoundException, null);
      }
    }
    catch (InstantiationException localInstantiationException)
    {
      for (;;) {}
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      for (;;) {}
    }
  }
  
  @DynamiteApi
  public static class DynamiteLoaderClassLoader
  {
    @GuardedBy("DynamiteLoaderClassLoader.class")
    public static ClassLoader sClassLoader;
  }
  
  @KeepForSdk
  public static class LoadingException
    extends Exception
  {
    private LoadingException(String paramString)
    {
      super();
    }
    
    private LoadingException(String paramString, Throwable paramThrowable)
    {
      super(paramThrowable);
    }
  }
  
  public static abstract interface VersionPolicy
  {
    public abstract zzb zza(Context paramContext, String paramString, zza paramzza)
      throws DynamiteModule.LoadingException;
    
    public static abstract interface zza
    {
      public abstract int getLocalVersion(Context paramContext, String paramString);
      
      public abstract int zza(Context paramContext, String paramString, boolean paramBoolean)
        throws DynamiteModule.LoadingException;
    }
    
    public static final class zzb
    {
      public int zziq = 0;
      public int zzir = 0;
      public int zzis = 0;
    }
  }
  
  private static final class zza
  {
    public Cursor zzin;
  }
  
  private static final class zzb
    implements DynamiteModule.VersionPolicy.zza
  {
    private final int zzio;
    private final int zzip;
    
    public zzb(int paramInt1, int paramInt2)
    {
      this.zzio = paramInt1;
      this.zzip = 0;
    }
    
    public final int getLocalVersion(Context paramContext, String paramString)
    {
      return this.zzio;
    }
    
    public final int zza(Context paramContext, String paramString, boolean paramBoolean)
    {
      return 0;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\dynamite\DynamiteModule.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */