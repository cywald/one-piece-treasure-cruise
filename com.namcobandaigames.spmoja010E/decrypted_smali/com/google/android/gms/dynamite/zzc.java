package com.google.android.gms.dynamite;

import android.content.Context;

final class zzc
  implements DynamiteModule.VersionPolicy
{
  public final DynamiteModule.VersionPolicy.zzb zza(Context paramContext, String paramString, DynamiteModule.VersionPolicy.zza paramzza)
    throws DynamiteModule.LoadingException
  {
    DynamiteModule.VersionPolicy.zzb localzzb = new DynamiteModule.VersionPolicy.zzb();
    localzzb.zziq = paramzza.getLocalVersion(paramContext, paramString);
    if (localzzb.zziq != 0) {
      localzzb.zzis = -1;
    }
    do
    {
      return localzzb;
      localzzb.zzir = paramzza.zza(paramContext, paramString, true);
    } while (localzzb.zzir == 0);
    localzzb.zzis = 1;
    return localzzb;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\dynamite\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */