package com.google.android.gms.dynamite;

import android.content.Context;

final class zzd
  implements DynamiteModule.VersionPolicy
{
  public final DynamiteModule.VersionPolicy.zzb zza(Context paramContext, String paramString, DynamiteModule.VersionPolicy.zza paramzza)
    throws DynamiteModule.LoadingException
  {
    DynamiteModule.VersionPolicy.zzb localzzb = new DynamiteModule.VersionPolicy.zzb();
    localzzb.zziq = paramzza.getLocalVersion(paramContext, paramString);
    localzzb.zzir = paramzza.zza(paramContext, paramString, true);
    if ((localzzb.zziq == 0) && (localzzb.zzir == 0))
    {
      localzzb.zzis = 0;
      return localzzb;
    }
    if (localzzb.zziq >= localzzb.zzir)
    {
      localzzb.zzis = -1;
      return localzzb;
    }
    localzzb.zzis = 1;
    return localzzb;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\dynamite\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */