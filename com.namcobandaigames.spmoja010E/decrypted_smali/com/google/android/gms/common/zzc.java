package com.google.android.gms.common;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.zzn;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.DynamiteModule.LoadingException;
import javax.annotation.CheckReturnValue;

@CheckReturnValue
final class zzc
{
  private static volatile com.google.android.gms.common.internal.zzm zzn;
  private static final Object zzo = new Object();
  private static Context zzp;
  
  static zzm zza(String paramString, zze paramzze, boolean paramBoolean)
  {
    StrictMode.ThreadPolicy localThreadPolicy = StrictMode.allowThreadDiskReads();
    try
    {
      paramString = zzb(paramString, paramzze, paramBoolean);
      return paramString;
    }
    finally
    {
      StrictMode.setThreadPolicy(localThreadPolicy);
    }
  }
  
  /* Error */
  static void zza(Context paramContext)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 50	com/google/android/gms/common/zzc:zzp	Landroid/content/Context;
    //   6: ifnonnull +18 -> 24
    //   9: aload_0
    //   10: ifnull +10 -> 20
    //   13: aload_0
    //   14: invokevirtual 56	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   17: putstatic 50	com/google/android/gms/common/zzc:zzp	Landroid/content/Context;
    //   20: ldc 2
    //   22: monitorexit
    //   23: return
    //   24: ldc 58
    //   26: ldc 60
    //   28: invokestatic 66	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   31: pop
    //   32: goto -12 -> 20
    //   35: astore_0
    //   36: ldc 2
    //   38: monitorexit
    //   39: aload_0
    //   40: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	41	0	paramContext	Context
    // Exception table:
    //   from	to	target	type
    //   3	9	35	finally
    //   13	20	35	finally
    //   24	32	35	finally
  }
  
  private static zzm zzb(String paramString, zze paramzze, boolean paramBoolean)
  {
    try
    {
      if (zzn == null) {
        Preconditions.checkNotNull(zzp);
      }
      boolean bool;
      synchronized (zzo)
      {
        if (zzn == null) {
          zzn = zzn.zzc(DynamiteModule.load(zzp, DynamiteModule.PREFER_HIGHEST_OR_LOCAL_VERSION_NO_FORCE_STAGING, "com.google.android.gms.googlecertificates").instantiate("com.google.android.gms.common.GoogleCertificatesImpl"));
        }
        Preconditions.checkNotNull(zzp);
        ??? = new zzk(paramString, paramzze, paramBoolean);
      }
      return zzm.zza(new zzd(paramBoolean, paramString, paramzze));
    }
    catch (DynamiteModule.LoadingException paramzze)
    {
      Log.e("GoogleCertificates", "Failed to get Google certificates from remote", paramzze);
      paramString = String.valueOf(paramzze.getMessage());
      if (paramString.length() != 0) {}
      for (paramString = "module init: ".concat(paramString);; paramString = new String("module init: ")) {
        return zzm.zza(paramString, paramzze);
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */