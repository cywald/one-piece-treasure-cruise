package com.google.android.gms.common;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
final class ProGuardCanary
{
  @KeepForSdk
  static final String CANARY = "gms_proguard_canary";
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\ProGuardCanary.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */