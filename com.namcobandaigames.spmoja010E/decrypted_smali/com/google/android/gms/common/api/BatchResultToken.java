package com.google.android.gms.common.api;

public final class BatchResultToken<R extends Result>
{
  protected final int mId;
  
  BatchResultToken(int paramInt)
  {
    this.mId = paramInt;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\BatchResultToken.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */