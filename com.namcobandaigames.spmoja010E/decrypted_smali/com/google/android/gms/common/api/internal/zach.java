package com.google.android.gms.common.api.internal;

import android.support.annotation.WorkerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.IAccountAccessor;
import java.util.Set;

@WorkerThread
public abstract interface zach
{
  public abstract void zaa(IAccountAccessor paramIAccountAccessor, Set<Scope> paramSet);
  
  public abstract void zag(ConnectionResult paramConnectionResult);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zach.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */