package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.TaskCompletionSource;

final class zaaf
{
  private final zai<?> zafp;
  private final TaskCompletionSource<Boolean> zafq = new TaskCompletionSource();
  
  public zaaf(zai<?> paramzai)
  {
    this.zafp = paramzai;
  }
  
  public final TaskCompletionSource<Boolean> zaal()
  {
    return this.zafq;
  }
  
  public final zai<?> zak()
  {
    return this.zafp;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */