package com.google.android.gms.common.api.internal;

import android.support.annotation.WorkerThread;
import java.util.concurrent.locks.Lock;

abstract class zaau
  implements Runnable
{
  private zaau(zaak paramzaak) {}
  
  @WorkerThread
  public void run()
  {
    zaak.zac(this.zagi).lock();
    try
    {
      boolean bool = Thread.interrupted();
      if (bool) {
        return;
      }
      zaan();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      zaak.zad(this.zagi).zab(localRuntimeException);
      return;
    }
    finally
    {
      zaak.zac(this.zagi).unlock();
    }
  }
  
  @WorkerThread
  protected abstract void zaan();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaau.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */