package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.BasePendingResult;

final class zaa
  implements PendingResult.StatusListener
{
  zaa(Batch paramBatch) {}
  
  public final void onComplete(Status paramStatus)
  {
    for (;;)
    {
      synchronized (Batch.zaa(this.zabd))
      {
        if (this.zabd.isCanceled()) {
          return;
        }
        if (paramStatus.isCanceled())
        {
          Batch.zaa(this.zabd, true);
          Batch.zab(this.zabd);
          if (Batch.zac(this.zabd) == 0)
          {
            if (!Batch.zad(this.zabd)) {
              break;
            }
            Batch.zae(this.zabd);
          }
          return;
        }
      }
      if (!paramStatus.isSuccess()) {
        Batch.zab(this.zabd, true);
      }
    }
    if (Batch.zaf(this.zabd)) {}
    for (paramStatus = new Status(13);; paramStatus = Status.RESULT_SUCCESS)
    {
      this.zabd.setResult(new BatchResult(paramStatus, Batch.zag(this.zabd)));
      break;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\zaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */