package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AbstractClientBuilder;
import com.google.android.gms.common.api.Api.AnyClient;
import com.google.android.gms.common.api.Api.AnyClientKey;
import com.google.android.gms.common.api.Api.Client;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import javax.annotation.concurrent.GuardedBy;

final class zas
  implements zabs
{
  private final Context mContext;
  private final Looper zabj;
  private final zaaw zaed;
  private final zabe zaee;
  private final zabe zaef;
  private final Map<Api.AnyClientKey<?>, zabe> zaeg;
  private final Set<SignInConnectionListener> zaeh = Collections.newSetFromMap(new WeakHashMap());
  private final Api.Client zaei;
  private Bundle zaej;
  private ConnectionResult zaek = null;
  private ConnectionResult zael = null;
  private boolean zaem = false;
  private final Lock zaen;
  @GuardedBy("mLock")
  private int zaeo = 0;
  
  private zas(Context paramContext, zaaw paramzaaw, Lock paramLock, Looper paramLooper, GoogleApiAvailabilityLight paramGoogleApiAvailabilityLight, Map<Api.AnyClientKey<?>, Api.Client> paramMap1, Map<Api.AnyClientKey<?>, Api.Client> paramMap2, ClientSettings paramClientSettings, Api.AbstractClientBuilder<? extends zad, SignInOptions> paramAbstractClientBuilder, Api.Client paramClient, ArrayList<zaq> paramArrayList1, ArrayList<zaq> paramArrayList2, Map<Api<?>, Boolean> paramMap3, Map<Api<?>, Boolean> paramMap4)
  {
    this.mContext = paramContext;
    this.zaed = paramzaaw;
    this.zaen = paramLock;
    this.zabj = paramLooper;
    this.zaei = paramClient;
    this.zaee = new zabe(paramContext, this.zaed, paramLock, paramLooper, paramGoogleApiAvailabilityLight, paramMap2, null, paramMap4, null, paramArrayList2, new zau(this, null));
    this.zaef = new zabe(paramContext, this.zaed, paramLock, paramLooper, paramGoogleApiAvailabilityLight, paramMap1, paramClientSettings, paramMap3, paramAbstractClientBuilder, paramArrayList1, new zav(this, null));
    paramContext = new ArrayMap();
    paramzaaw = paramMap2.keySet().iterator();
    while (paramzaaw.hasNext()) {
      paramContext.put((Api.AnyClientKey)paramzaaw.next(), this.zaee);
    }
    paramzaaw = paramMap1.keySet().iterator();
    while (paramzaaw.hasNext()) {
      paramContext.put((Api.AnyClientKey)paramzaaw.next(), this.zaef);
    }
    this.zaeg = Collections.unmodifiableMap(paramContext);
  }
  
  public static zas zaa(Context paramContext, zaaw paramzaaw, Lock paramLock, Looper paramLooper, GoogleApiAvailabilityLight paramGoogleApiAvailabilityLight, Map<Api.AnyClientKey<?>, Api.Client> paramMap, ClientSettings paramClientSettings, Map<Api<?>, Boolean> paramMap1, Api.AbstractClientBuilder<? extends zad, SignInOptions> paramAbstractClientBuilder, ArrayList<zaq> paramArrayList)
  {
    Object localObject1 = null;
    ArrayMap localArrayMap1 = new ArrayMap();
    ArrayMap localArrayMap2 = new ArrayMap();
    Object localObject2 = paramMap.entrySet().iterator();
    paramMap = (Map<Api.AnyClientKey<?>, Api.Client>)localObject1;
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      localObject1 = (Api.Client)((Map.Entry)localObject3).getValue();
      if (((Api.Client)localObject1).providesSignIn()) {
        paramMap = (Map<Api.AnyClientKey<?>, Api.Client>)localObject1;
      }
      if (((Api.Client)localObject1).requiresSignIn()) {
        localArrayMap1.put((Api.AnyClientKey)((Map.Entry)localObject3).getKey(), localObject1);
      } else {
        localArrayMap2.put((Api.AnyClientKey)((Map.Entry)localObject3).getKey(), localObject1);
      }
    }
    boolean bool;
    if (!localArrayMap1.isEmpty())
    {
      bool = true;
      Preconditions.checkState(bool, "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
      localObject1 = new ArrayMap();
      localObject2 = new ArrayMap();
      localObject3 = paramMap1.keySet().iterator();
    }
    Object localObject4;
    for (;;)
    {
      if (((Iterator)localObject3).hasNext())
      {
        localObject4 = (Api)((Iterator)localObject3).next();
        Api.AnyClientKey localAnyClientKey = ((Api)localObject4).getClientKey();
        if (localArrayMap1.containsKey(localAnyClientKey))
        {
          ((Map)localObject1).put(localObject4, (Boolean)paramMap1.get(localObject4));
          continue;
          bool = false;
          break;
        }
        if (localArrayMap2.containsKey(localAnyClientKey)) {
          ((Map)localObject2).put(localObject4, (Boolean)paramMap1.get(localObject4));
        } else {
          throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
        }
      }
    }
    paramMap1 = new ArrayList();
    Object localObject3 = new ArrayList();
    paramArrayList = (ArrayList)paramArrayList;
    int j = paramArrayList.size();
    int i = 0;
    while (i < j)
    {
      localObject4 = paramArrayList.get(i);
      i += 1;
      localObject4 = (zaq)localObject4;
      if (((Map)localObject1).containsKey(((zaq)localObject4).mApi)) {
        paramMap1.add(localObject4);
      } else if (((Map)localObject2).containsKey(((zaq)localObject4).mApi)) {
        ((ArrayList)localObject3).add(localObject4);
      } else {
        throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
      }
    }
    return new zas(paramContext, paramzaaw, paramLock, paramLooper, paramGoogleApiAvailabilityLight, localArrayMap1, localArrayMap2, paramClientSettings, paramAbstractClientBuilder, paramMap, paramMap1, (ArrayList)localObject3, (Map)localObject1, (Map)localObject2);
  }
  
  @GuardedBy("mLock")
  private final void zaa(int paramInt, boolean paramBoolean)
  {
    this.zaed.zab(paramInt, paramBoolean);
    this.zael = null;
    this.zaek = null;
  }
  
  private final void zaa(Bundle paramBundle)
  {
    if (this.zaej == null) {
      this.zaej = paramBundle;
    }
    while (paramBundle == null) {
      return;
    }
    this.zaej.putAll(paramBundle);
  }
  
  @GuardedBy("mLock")
  private final void zaa(ConnectionResult paramConnectionResult)
  {
    switch (this.zaeo)
    {
    default: 
      Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
    }
    for (;;)
    {
      this.zaeo = 0;
      return;
      this.zaed.zac(paramConnectionResult);
      zay();
    }
  }
  
  private final boolean zaa(BaseImplementation.ApiMethodImpl<? extends Result, ? extends Api.AnyClient> paramApiMethodImpl)
  {
    paramApiMethodImpl = paramApiMethodImpl.getClientKey();
    Preconditions.checkArgument(this.zaeg.containsKey(paramApiMethodImpl), "GoogleApiClient is not configured to use the API required for this call.");
    return ((zabe)this.zaeg.get(paramApiMethodImpl)).equals(this.zaef);
  }
  
  @Nullable
  private final PendingIntent zaaa()
  {
    if (this.zaei == null) {
      return null;
    }
    return PendingIntent.getActivity(this.mContext, System.identityHashCode(this.zaed), this.zaei.getSignInIntent(), 134217728);
  }
  
  private static boolean zab(ConnectionResult paramConnectionResult)
  {
    return (paramConnectionResult != null) && (paramConnectionResult.isSuccess());
  }
  
  @GuardedBy("mLock")
  private final void zax()
  {
    if (zab(this.zaek)) {
      if ((zab(this.zael)) || (zaz())) {
        switch (this.zaeo)
        {
        default: 
          Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
          this.zaeo = 0;
        }
      }
    }
    do
    {
      do
      {
        return;
        this.zaed.zab(this.zaej);
        zay();
        break;
      } while (this.zael == null);
      if (this.zaeo == 1)
      {
        zay();
        return;
      }
      zaa(this.zael);
      this.zaee.disconnect();
      return;
      if ((this.zaek != null) && (zab(this.zael)))
      {
        this.zaef.disconnect();
        zaa(this.zaek);
        return;
      }
    } while ((this.zaek == null) || (this.zael == null));
    ConnectionResult localConnectionResult = this.zaek;
    if (this.zaef.zahr < this.zaee.zahr) {
      localConnectionResult = this.zael;
    }
    zaa(localConnectionResult);
  }
  
  @GuardedBy("mLock")
  private final void zay()
  {
    Iterator localIterator = this.zaeh.iterator();
    while (localIterator.hasNext()) {
      ((SignInConnectionListener)localIterator.next()).onComplete();
    }
    this.zaeh.clear();
  }
  
  @GuardedBy("mLock")
  private final boolean zaz()
  {
    return (this.zael != null) && (this.zael.getErrorCode() == 4);
  }
  
  @GuardedBy("mLock")
  public final ConnectionResult blockingConnect()
  {
    throw new UnsupportedOperationException();
  }
  
  @GuardedBy("mLock")
  public final ConnectionResult blockingConnect(long paramLong, @NonNull TimeUnit paramTimeUnit)
  {
    throw new UnsupportedOperationException();
  }
  
  @GuardedBy("mLock")
  public final void connect()
  {
    this.zaeo = 2;
    this.zaem = false;
    this.zael = null;
    this.zaek = null;
    this.zaee.connect();
    this.zaef.connect();
  }
  
  @GuardedBy("mLock")
  public final void disconnect()
  {
    this.zael = null;
    this.zaek = null;
    this.zaeo = 0;
    this.zaee.disconnect();
    this.zaef.disconnect();
    zay();
  }
  
  public final void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    paramPrintWriter.append(paramString).append("authClient").println(":");
    this.zaef.dump(String.valueOf(paramString).concat("  "), paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    paramPrintWriter.append(paramString).append("anonClient").println(":");
    this.zaee.dump(String.valueOf(paramString).concat("  "), paramFileDescriptor, paramPrintWriter, paramArrayOfString);
  }
  
  @GuardedBy("mLock")
  public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T enqueue(@NonNull T paramT)
  {
    if (zaa(paramT))
    {
      if (zaz())
      {
        paramT.setFailedResult(new Status(4, null, zaaa()));
        return paramT;
      }
      return this.zaef.enqueue(paramT);
    }
    return this.zaee.enqueue(paramT);
  }
  
  @GuardedBy("mLock")
  public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T execute(@NonNull T paramT)
  {
    if (zaa(paramT))
    {
      if (zaz())
      {
        paramT.setFailedResult(new Status(4, null, zaaa()));
        return paramT;
      }
      return this.zaef.execute(paramT);
    }
    return this.zaee.execute(paramT);
  }
  
  @Nullable
  @GuardedBy("mLock")
  public final ConnectionResult getConnectionResult(@NonNull Api<?> paramApi)
  {
    if (((zabe)this.zaeg.get(paramApi.getClientKey())).equals(this.zaef))
    {
      if (zaz()) {
        return new ConnectionResult(4, zaaa());
      }
      return this.zaef.getConnectionResult(paramApi);
    }
    return this.zaee.getConnectionResult(paramApi);
  }
  
  /* Error */
  public final boolean isConnected()
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_0
    //   3: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   6: invokeinterface 410 1 0
    //   11: aload_0
    //   12: getfield 83	com/google/android/gms/common/api/internal/zas:zaee	Lcom/google/android/gms/common/api/internal/zabe;
    //   15: invokevirtual 412	com/google/android/gms/common/api/internal/zabe:isConnected	()Z
    //   18: ifeq +47 -> 65
    //   21: iload_3
    //   22: istore_2
    //   23: aload_0
    //   24: getfield 88	com/google/android/gms/common/api/internal/zas:zaef	Lcom/google/android/gms/common/api/internal/zabe;
    //   27: invokevirtual 412	com/google/android/gms/common/api/internal/zabe:isConnected	()Z
    //   30: ifne +24 -> 54
    //   33: iload_3
    //   34: istore_2
    //   35: aload_0
    //   36: invokespecial 306	com/google/android/gms/common/api/internal/zas:zaz	()Z
    //   39: ifne +15 -> 54
    //   42: aload_0
    //   43: getfield 61	com/google/android/gms/common/api/internal/zas:zaeo	I
    //   46: istore_1
    //   47: iload_1
    //   48: iconst_1
    //   49: if_icmpne +16 -> 65
    //   52: iload_3
    //   53: istore_2
    //   54: aload_0
    //   55: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   58: invokeinterface 415 1 0
    //   63: iload_2
    //   64: ireturn
    //   65: iconst_0
    //   66: istore_2
    //   67: goto -13 -> 54
    //   70: astore 4
    //   72: aload_0
    //   73: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   76: invokeinterface 415 1 0
    //   81: aload 4
    //   83: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	84	0	this	zas
    //   46	4	1	i	int
    //   22	45	2	bool1	boolean
    //   1	52	3	bool2	boolean
    //   70	12	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   11	21	70	finally
    //   23	33	70	finally
    //   35	47	70	finally
  }
  
  /* Error */
  public final boolean isConnecting()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   4: invokeinterface 410 1 0
    //   9: aload_0
    //   10: getfield 61	com/google/android/gms/common/api/internal/zas:zaeo	I
    //   13: istore_1
    //   14: iload_1
    //   15: iconst_2
    //   16: if_icmpne +16 -> 32
    //   19: iconst_1
    //   20: istore_2
    //   21: aload_0
    //   22: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   25: invokeinterface 415 1 0
    //   30: iload_2
    //   31: ireturn
    //   32: iconst_0
    //   33: istore_2
    //   34: goto -13 -> 21
    //   37: astore_3
    //   38: aload_0
    //   39: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   42: invokeinterface 415 1 0
    //   47: aload_3
    //   48: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	49	0	this	zas
    //   13	4	1	i	int
    //   20	14	2	bool	boolean
    //   37	11	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	14	37	finally
  }
  
  public final boolean maybeSignIn(SignInConnectionListener paramSignInConnectionListener)
  {
    this.zaen.lock();
    try
    {
      if (((isConnecting()) || (isConnected())) && (!this.zaef.isConnected()))
      {
        this.zaeh.add(paramSignInConnectionListener);
        if (this.zaeo == 0) {
          this.zaeo = 1;
        }
        this.zael = null;
        this.zaef.connect();
        return true;
      }
      return false;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  /* Error */
  public final void maybeSignOut()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   4: invokeinterface 410 1 0
    //   9: aload_0
    //   10: invokevirtual 420	com/google/android/gms/common/api/internal/zas:isConnecting	()Z
    //   13: istore_1
    //   14: aload_0
    //   15: getfield 88	com/google/android/gms/common/api/internal/zas:zaef	Lcom/google/android/gms/common/api/internal/zabe;
    //   18: invokevirtual 318	com/google/android/gms/common/api/internal/zabe:disconnect	()V
    //   21: aload_0
    //   22: new 292	com/google/android/gms/common/ConnectionResult
    //   25: dup
    //   26: iconst_4
    //   27: invokespecial 426	com/google/android/gms/common/ConnectionResult:<init>	(I)V
    //   30: putfield 57	com/google/android/gms/common/api/internal/zas:zael	Lcom/google/android/gms/common/ConnectionResult;
    //   33: iload_1
    //   34: ifeq +36 -> 70
    //   37: new 428	com/google/android/gms/internal/base/zal
    //   40: dup
    //   41: aload_0
    //   42: getfield 69	com/google/android/gms/common/api/internal/zas:zabj	Landroid/os/Looper;
    //   45: invokespecial 431	com/google/android/gms/internal/base/zal:<init>	(Landroid/os/Looper;)V
    //   48: new 433	com/google/android/gms/common/api/internal/zat
    //   51: dup
    //   52: aload_0
    //   53: invokespecial 435	com/google/android/gms/common/api/internal/zat:<init>	(Lcom/google/android/gms/common/api/internal/zas;)V
    //   56: invokevirtual 441	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   59: pop
    //   60: aload_0
    //   61: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   64: invokeinterface 415 1 0
    //   69: return
    //   70: aload_0
    //   71: invokespecial 245	com/google/android/gms/common/api/internal/zas:zay	()V
    //   74: goto -14 -> 60
    //   77: astore_2
    //   78: aload_0
    //   79: getfield 67	com/google/android/gms/common/api/internal/zas:zaen	Ljava/util/concurrent/locks/Lock;
    //   82: invokeinterface 415 1 0
    //   87: aload_2
    //   88: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	89	0	this	zas
    //   13	21	1	bool	boolean
    //   77	11	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	33	77	finally
    //   37	60	77	finally
    //   70	74	77	finally
  }
  
  @GuardedBy("mLock")
  public final void zaw()
  {
    this.zaee.zaw();
    this.zaef.zaw();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */