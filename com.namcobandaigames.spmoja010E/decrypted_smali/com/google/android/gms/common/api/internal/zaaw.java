package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AbstractClientBuilder;
import com.google.android.gms.common.api.Api.AnyClient;
import com.google.android.gms.common.api.Api.AnyClientKey;
import com.google.android.gms.common.api.Api.Client;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.GmsClientEventManager;
import com.google.android.gms.common.internal.GmsClientEventManager.GmsClientEventState;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.service.Common;
import com.google.android.gms.common.internal.service.zac;
import com.google.android.gms.common.util.ClientLibraryUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import javax.annotation.concurrent.GuardedBy;

public final class zaaw
  extends GoogleApiClient
  implements zabt
{
  private final Context mContext;
  private final Looper zabj;
  private final int zaca;
  private final GoogleApiAvailability zacc;
  private final Api.AbstractClientBuilder<? extends zad, SignInOptions> zacd;
  private boolean zacg;
  private final Lock zaen;
  private final ClientSettings zaes;
  private final Map<Api<?>, Boolean> zaev;
  @VisibleForTesting
  final Queue<BaseImplementation.ApiMethodImpl<?, ?>> zafb = new LinkedList();
  private final GmsClientEventManager zagr;
  private zabs zags = null;
  private volatile boolean zagt;
  private long zagu;
  private long zagv;
  private final zabb zagw;
  @VisibleForTesting
  private zabq zagx;
  final Map<Api.AnyClientKey<?>, Api.Client> zagy;
  Set<Scope> zagz;
  private final ListenerHolders zaha;
  private final ArrayList<zaq> zahb;
  private Integer zahc;
  Set<zacm> zahd;
  final zacp zahe;
  private final GmsClientEventManager.GmsClientEventState zahf;
  
  public zaaw(Context paramContext, Lock paramLock, Looper paramLooper, ClientSettings paramClientSettings, GoogleApiAvailability paramGoogleApiAvailability, Api.AbstractClientBuilder<? extends zad, SignInOptions> paramAbstractClientBuilder, Map<Api<?>, Boolean> paramMap, List<GoogleApiClient.ConnectionCallbacks> paramList, List<GoogleApiClient.OnConnectionFailedListener> paramList1, Map<Api.AnyClientKey<?>, Api.Client> paramMap1, int paramInt1, int paramInt2, ArrayList<zaq> paramArrayList, boolean paramBoolean)
  {
    if (ClientLibraryUtils.isPackageSide()) {}
    for (long l = 10000L;; l = 120000L)
    {
      this.zagu = l;
      this.zagv = 5000L;
      this.zagz = new HashSet();
      this.zaha = new ListenerHolders();
      this.zahc = null;
      this.zahd = null;
      this.zahf = new zaax(this);
      this.mContext = paramContext;
      this.zaen = paramLock;
      this.zacg = false;
      this.zagr = new GmsClientEventManager(paramLooper, this.zahf);
      this.zabj = paramLooper;
      this.zagw = new zabb(this, paramLooper);
      this.zacc = paramGoogleApiAvailability;
      this.zaca = paramInt1;
      if (this.zaca >= 0) {
        this.zahc = Integer.valueOf(paramInt2);
      }
      this.zaev = paramMap;
      this.zagy = paramMap1;
      this.zahb = paramArrayList;
      this.zahe = new zacp(this.zagy);
      paramContext = paramList.iterator();
      while (paramContext.hasNext())
      {
        paramLock = (GoogleApiClient.ConnectionCallbacks)paramContext.next();
        this.zagr.registerConnectionCallbacks(paramLock);
      }
    }
    paramContext = paramList1.iterator();
    while (paramContext.hasNext())
    {
      paramLock = (GoogleApiClient.OnConnectionFailedListener)paramContext.next();
      this.zagr.registerConnectionFailedListener(paramLock);
    }
    this.zaes = paramClientSettings;
    this.zacd = paramAbstractClientBuilder;
  }
  
  private final void resume()
  {
    this.zaen.lock();
    try
    {
      if (this.zagt) {
        zaau();
      }
      return;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  public static int zaa(Iterable<Api.Client> paramIterable, boolean paramBoolean)
  {
    int k = 1;
    paramIterable = paramIterable.iterator();
    int i = 0;
    int j = 0;
    if (paramIterable.hasNext())
    {
      Api.Client localClient = (Api.Client)paramIterable.next();
      if (localClient.requiresSignIn()) {
        j = 1;
      }
      if (!localClient.providesSignIn()) {
        break label85;
      }
      i = 1;
    }
    label85:
    for (;;)
    {
      break;
      if (j != 0)
      {
        j = k;
        if (i != 0)
        {
          j = k;
          if (paramBoolean) {
            j = 2;
          }
        }
        return j;
      }
      return 3;
    }
  }
  
  private final void zaa(GoogleApiClient paramGoogleApiClient, StatusPendingResult paramStatusPendingResult, boolean paramBoolean)
  {
    Common.zaph.zaa(paramGoogleApiClient).setResultCallback(new zaba(this, paramStatusPendingResult, paramBoolean, paramGoogleApiClient));
  }
  
  @GuardedBy("mLock")
  private final void zaau()
  {
    this.zagr.enableCallbacks();
    this.zags.connect();
  }
  
  private final void zaav()
  {
    this.zaen.lock();
    try
    {
      if (zaaw()) {
        zaau();
      }
      return;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  private final void zae(int paramInt)
  {
    if (this.zahc == null) {
      this.zahc = Integer.valueOf(paramInt);
    }
    Object localObject2;
    while (this.zags != null)
    {
      return;
      if (this.zahc.intValue() != paramInt)
      {
        localObject1 = zaf(paramInt);
        localObject2 = zaf(this.zahc.intValue());
        throw new IllegalStateException(String.valueOf(localObject1).length() + 51 + String.valueOf(localObject2).length() + "Cannot use sign-in mode: " + (String)localObject1 + ". Mode was already set to " + (String)localObject2);
      }
    }
    Object localObject1 = this.zagy.values().iterator();
    paramInt = 0;
    int i = 0;
    if (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Api.Client)((Iterator)localObject1).next();
      if (((Api.Client)localObject2).requiresSignIn()) {
        i = 1;
      }
      if (!((Api.Client)localObject2).providesSignIn()) {
        break label455;
      }
      paramInt = 1;
    }
    label455:
    for (;;)
    {
      break;
      switch (this.zahc.intValue())
      {
      }
      while ((this.zacg) && (paramInt == 0))
      {
        this.zags = new zax(this.mContext, this.zaen, this.zabj, this.zacc, this.zagy, this.zaes, this.zaev, this.zacd, this.zahb, this, false);
        return;
        if (i == 0) {
          throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
        }
        if (paramInt != 0)
        {
          throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
          if (i != 0)
          {
            if (this.zacg)
            {
              this.zags = new zax(this.mContext, this.zaen, this.zabj, this.zacc, this.zagy, this.zaes, this.zaev, this.zacd, this.zahb, this, true);
              return;
            }
            this.zags = zas.zaa(this.mContext, this, this.zaen, this.zabj, this.zacc, this.zagy, this.zaes, this.zaev, this.zacd, this.zahb);
            return;
          }
        }
      }
      this.zags = new zabe(this.mContext, this, this.zaen, this.zabj, this.zacc, this.zagy, this.zaes, this.zaev, this.zacd, this.zahb, this);
      return;
    }
  }
  
  private static String zaf(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "UNKNOWN";
    case 3: 
      return "SIGN_IN_MODE_NONE";
    case 1: 
      return "SIGN_IN_MODE_REQUIRED";
    }
    return "SIGN_IN_MODE_OPTIONAL";
  }
  
  public final ConnectionResult blockingConnect()
  {
    boolean bool2 = true;
    boolean bool1;
    if (Looper.myLooper() != Looper.getMainLooper()) {
      bool1 = true;
    }
    for (;;)
    {
      Preconditions.checkState(bool1, "blockingConnect must not be called on the UI thread");
      this.zaen.lock();
      try
      {
        if (this.zaca >= 0) {
          if (this.zahc != null)
          {
            bool1 = bool2;
            label45:
            Preconditions.checkState(bool1, "Sign-in mode should have been set explicitly by auto-manage.");
          }
        }
        do
        {
          for (;;)
          {
            zae(this.zahc.intValue());
            this.zagr.enableCallbacks();
            ConnectionResult localConnectionResult = this.zags.blockingConnect();
            return localConnectionResult;
            bool1 = false;
            break;
            bool1 = false;
            break label45;
            if (this.zahc != null) {
              break label143;
            }
            this.zahc = Integer.valueOf(zaa(this.zagy.values(), false));
          }
        } while (this.zahc.intValue() != 2);
      }
      finally
      {
        this.zaen.unlock();
      }
    }
    label143:
    throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
  }
  
  public final ConnectionResult blockingConnect(long paramLong, @NonNull TimeUnit paramTimeUnit)
  {
    boolean bool = false;
    if (Looper.myLooper() != Looper.getMainLooper()) {
      bool = true;
    }
    Preconditions.checkState(bool, "blockingConnect must not be called on the UI thread");
    Preconditions.checkNotNull(paramTimeUnit, "TimeUnit must not be null");
    this.zaen.lock();
    try
    {
      if (this.zahc == null) {
        this.zahc = Integer.valueOf(zaa(this.zagy.values(), false));
      }
      while (this.zahc.intValue() != 2)
      {
        zae(this.zahc.intValue());
        this.zagr.enableCallbacks();
        paramTimeUnit = this.zags.blockingConnect(paramLong, paramTimeUnit);
        return paramTimeUnit;
      }
      throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  public final PendingResult<Status> clearDefaultAccountAndReconnect()
  {
    Preconditions.checkState(isConnected(), "GoogleApiClient is not connected yet.");
    if (this.zahc.intValue() != 2) {}
    StatusPendingResult localStatusPendingResult;
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
      localStatusPendingResult = new StatusPendingResult(this);
      if (!this.zagy.containsKey(Common.CLIENT_KEY)) {
        break;
      }
      zaa(this, localStatusPendingResult, false);
      return localStatusPendingResult;
    }
    AtomicReference localAtomicReference = new AtomicReference();
    Object localObject = new zaay(this, localAtomicReference, localStatusPendingResult);
    zaaz localzaaz = new zaaz(this, localStatusPendingResult);
    localObject = new GoogleApiClient.Builder(this.mContext).addApi(Common.API).addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks)localObject).addOnConnectionFailedListener(localzaaz).setHandler(this.zagw).build();
    localAtomicReference.set(localObject);
    ((GoogleApiClient)localObject).connect();
    return localStatusPendingResult;
  }
  
  public final void connect()
  {
    boolean bool = false;
    this.zaen.lock();
    try
    {
      if (this.zaca >= 0)
      {
        if (this.zahc != null) {
          bool = true;
        }
        Preconditions.checkState(bool, "Sign-in mode should have been set explicitly by auto-manage.");
      }
      do
      {
        for (;;)
        {
          connect(this.zahc.intValue());
          return;
          if (this.zahc != null) {
            break;
          }
          this.zahc = Integer.valueOf(zaa(this.zagy.values(), false));
        }
      } while (this.zahc.intValue() != 2);
    }
    finally
    {
      this.zaen.unlock();
    }
    throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
  }
  
  /* Error */
  public final void connect(int paramInt)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_0
    //   3: getfield 111	com/google/android/gms/common/api/internal/zaaw:zaen	Ljava/util/concurrent/locks/Lock;
    //   6: invokeinterface 194 1 0
    //   11: iload_3
    //   12: istore_2
    //   13: iload_1
    //   14: iconst_3
    //   15: if_icmpeq +17 -> 32
    //   18: iload_3
    //   19: istore_2
    //   20: iload_1
    //   21: iconst_1
    //   22: if_icmpeq +10 -> 32
    //   25: iload_1
    //   26: iconst_2
    //   27: if_icmpne +50 -> 77
    //   30: iload_3
    //   31: istore_2
    //   32: iload_2
    //   33: new 279	java/lang/StringBuilder
    //   36: dup
    //   37: bipush 33
    //   39: invokespecial 289	java/lang/StringBuilder:<init>	(I)V
    //   42: ldc_w 456
    //   45: invokevirtual 295	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   48: iload_1
    //   49: invokevirtual 459	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   52: invokevirtual 301	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   55: invokestatic 462	com/google/android/gms/common/internal/Preconditions:checkArgument	(ZLjava/lang/Object;)V
    //   58: aload_0
    //   59: iload_1
    //   60: invokespecial 363	com/google/android/gms/common/api/internal/zaaw:zae	(I)V
    //   63: aload_0
    //   64: invokespecial 199	com/google/android/gms/common/api/internal/zaaw:zaau	()V
    //   67: aload_0
    //   68: getfield 111	com/google/android/gms/common/api/internal/zaaw:zaen	Ljava/util/concurrent/locks/Lock;
    //   71: invokeinterface 202 1 0
    //   76: return
    //   77: iconst_0
    //   78: istore_2
    //   79: goto -47 -> 32
    //   82: astore 4
    //   84: aload_0
    //   85: getfield 111	com/google/android/gms/common/api/internal/zaaw:zaen	Ljava/util/concurrent/locks/Lock;
    //   88: invokeinterface 202 1 0
    //   93: aload 4
    //   95: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	this	zaaw
    //   0	96	1	paramInt	int
    //   12	67	2	bool1	boolean
    //   1	30	3	bool2	boolean
    //   82	12	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   32	67	82	finally
  }
  
  public final void disconnect()
  {
    this.zaen.lock();
    try
    {
      this.zahe.release();
      if (this.zags != null) {
        this.zags.disconnect();
      }
      this.zaha.release();
      Iterator localIterator = this.zafb.iterator();
      while (localIterator.hasNext())
      {
        BaseImplementation.ApiMethodImpl localApiMethodImpl = (BaseImplementation.ApiMethodImpl)localIterator.next();
        localApiMethodImpl.zaa(null);
        localApiMethodImpl.cancel();
      }
      this.zafb.clear();
    }
    finally
    {
      this.zaen.unlock();
    }
    zabs localzabs = this.zags;
    if (localzabs == null)
    {
      this.zaen.unlock();
      return;
    }
    zaaw();
    this.zagr.disableCallbacks();
    this.zaen.unlock();
  }
  
  public final void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    paramPrintWriter.append(paramString).append("mContext=").println(this.mContext);
    paramPrintWriter.append(paramString).append("mResuming=").print(this.zagt);
    paramPrintWriter.append(" mWorkQueue.size()=").print(this.zafb.size());
    zacp localzacp = this.zahe;
    paramPrintWriter.append(" mUnconsumedApiCalls.size()=").println(localzacp.zaky.size());
    if (this.zags != null) {
      this.zags.dump(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }
  }
  
  public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T enqueue(@NonNull T paramT)
  {
    boolean bool;
    if (paramT.getClientKey() != null) {
      bool = true;
    }
    for (;;)
    {
      Preconditions.checkArgument(bool, "This task can not be enqueued (it's probably a Batch or malformed)");
      bool = this.zagy.containsKey(paramT.getClientKey());
      String str;
      if (paramT.getApi() != null)
      {
        str = paramT.getApi().getName();
        label45:
        Preconditions.checkArgument(bool, String.valueOf(str).length() + 65 + "GoogleApiClient is not configured to use " + str + " required for this call.");
        this.zaen.lock();
      }
      try
      {
        if (this.zags == null)
        {
          this.zafb.add(paramT);
          return paramT;
          bool = false;
          continue;
          str = "the API";
          break label45;
        }
        paramT = this.zags.enqueue(paramT);
        return paramT;
      }
      finally
      {
        this.zaen.unlock();
      }
    }
  }
  
  public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T execute(@NonNull T paramT)
  {
    boolean bool;
    if (paramT.getClientKey() != null)
    {
      bool = true;
      Preconditions.checkArgument(bool, "This task can not be executed (it's probably a Batch or malformed)");
      bool = this.zagy.containsKey(paramT.getClientKey());
      if (paramT.getApi() == null) {
        break label129;
      }
    }
    label129:
    for (Object localObject = paramT.getApi().getName();; localObject = "the API")
    {
      Preconditions.checkArgument(bool, String.valueOf(localObject).length() + 65 + "GoogleApiClient is not configured to use " + (String)localObject + " required for this call.");
      this.zaen.lock();
      try
      {
        if (this.zags != null) {
          break label136;
        }
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
      }
      finally
      {
        this.zaen.unlock();
      }
      bool = false;
      break;
    }
    label136:
    if (this.zagt)
    {
      this.zafb.add(paramT);
      while (!this.zafb.isEmpty())
      {
        localObject = (BaseImplementation.ApiMethodImpl)this.zafb.remove();
        this.zahe.zab((BasePendingResult)localObject);
        ((BaseImplementation.ApiMethodImpl)localObject).setFailedResult(Status.RESULT_INTERNAL_ERROR);
      }
      this.zaen.unlock();
      return paramT;
    }
    paramT = this.zags.execute(paramT);
    this.zaen.unlock();
    return paramT;
  }
  
  @NonNull
  public final <C extends Api.Client> C getClient(@NonNull Api.AnyClientKey<C> paramAnyClientKey)
  {
    paramAnyClientKey = (Api.Client)this.zagy.get(paramAnyClientKey);
    Preconditions.checkNotNull(paramAnyClientKey, "Appropriate Api was not requested.");
    return paramAnyClientKey;
  }
  
  @NonNull
  public final ConnectionResult getConnectionResult(@NonNull Api<?> paramApi)
  {
    this.zaen.lock();
    try
    {
      if ((!isConnected()) && (!this.zagt)) {
        throw new IllegalStateException("Cannot invoke getConnectionResult unless GoogleApiClient is connected");
      }
    }
    finally
    {
      this.zaen.unlock();
    }
    if (this.zagy.containsKey(paramApi.getClientKey()))
    {
      ConnectionResult localConnectionResult = this.zags.getConnectionResult(paramApi);
      if (localConnectionResult == null)
      {
        if (this.zagt)
        {
          paramApi = ConnectionResult.RESULT_SUCCESS;
          this.zaen.unlock();
          return paramApi;
        }
        Log.w("GoogleApiClientImpl", zaay());
        Log.wtf("GoogleApiClientImpl", String.valueOf(paramApi.getName()).concat(" requested in getConnectionResult is not connected but is not present in the failed  connections map"), new Exception());
        paramApi = new ConnectionResult(8, null);
        this.zaen.unlock();
        return paramApi;
      }
      this.zaen.unlock();
      return localConnectionResult;
    }
    throw new IllegalArgumentException(String.valueOf(paramApi.getName()).concat(" was never registered with GoogleApiClient"));
  }
  
  public final Context getContext()
  {
    return this.mContext;
  }
  
  public final Looper getLooper()
  {
    return this.zabj;
  }
  
  public final boolean hasApi(@NonNull Api<?> paramApi)
  {
    return this.zagy.containsKey(paramApi.getClientKey());
  }
  
  public final boolean hasConnectedApi(@NonNull Api<?> paramApi)
  {
    if (!isConnected()) {
      return false;
    }
    paramApi = (Api.Client)this.zagy.get(paramApi.getClientKey());
    return (paramApi != null) && (paramApi.isConnected());
  }
  
  public final boolean isConnected()
  {
    return (this.zags != null) && (this.zags.isConnected());
  }
  
  public final boolean isConnecting()
  {
    return (this.zags != null) && (this.zags.isConnecting());
  }
  
  public final boolean isConnectionCallbacksRegistered(@NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    return this.zagr.isConnectionCallbacksRegistered(paramConnectionCallbacks);
  }
  
  public final boolean isConnectionFailedListenerRegistered(@NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    return this.zagr.isConnectionFailedListenerRegistered(paramOnConnectionFailedListener);
  }
  
  public final boolean maybeSignIn(SignInConnectionListener paramSignInConnectionListener)
  {
    return (this.zags != null) && (this.zags.maybeSignIn(paramSignInConnectionListener));
  }
  
  public final void maybeSignOut()
  {
    if (this.zags != null) {
      this.zags.maybeSignOut();
    }
  }
  
  public final void reconnect()
  {
    disconnect();
    connect();
  }
  
  public final void registerConnectionCallbacks(@NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    this.zagr.registerConnectionCallbacks(paramConnectionCallbacks);
  }
  
  public final void registerConnectionFailedListener(@NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    this.zagr.registerConnectionFailedListener(paramOnConnectionFailedListener);
  }
  
  public final <L> ListenerHolder<L> registerListener(@NonNull L paramL)
  {
    this.zaen.lock();
    try
    {
      paramL = this.zaha.zaa(paramL, this.zabj, "NO_TYPE");
      return paramL;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  public final void stopAutoManage(@NonNull FragmentActivity paramFragmentActivity)
  {
    paramFragmentActivity = new LifecycleActivity(paramFragmentActivity);
    if (this.zaca >= 0)
    {
      zaj.zaa(paramFragmentActivity).zaa(this.zaca);
      return;
    }
    throw new IllegalStateException("Called stopAutoManage but automatic lifecycle management is not enabled.");
  }
  
  public final void unregisterConnectionCallbacks(@NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    this.zagr.unregisterConnectionCallbacks(paramConnectionCallbacks);
  }
  
  public final void unregisterConnectionFailedListener(@NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    this.zagr.unregisterConnectionFailedListener(paramOnConnectionFailedListener);
  }
  
  public final void zaa(zacm paramzacm)
  {
    this.zaen.lock();
    try
    {
      if (this.zahd == null) {
        this.zahd = new HashSet();
      }
      this.zahd.add(paramzacm);
      return;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  @GuardedBy("mLock")
  final boolean zaaw()
  {
    if (!this.zagt) {
      return false;
    }
    this.zagt = false;
    this.zagw.removeMessages(2);
    this.zagw.removeMessages(1);
    if (this.zagx != null)
    {
      this.zagx.unregister();
      this.zagx = null;
    }
    return true;
  }
  
  final boolean zaax()
  {
    boolean bool1 = false;
    this.zaen.lock();
    try
    {
      Set localSet = this.zahd;
      if (localSet == null) {
        return false;
      }
      boolean bool2 = this.zahd.isEmpty();
      if (!bool2) {
        bool1 = true;
      }
      return bool1;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  final String zaay()
  {
    StringWriter localStringWriter = new StringWriter();
    dump("", null, new PrintWriter(localStringWriter), null);
    return localStringWriter.toString();
  }
  
  @GuardedBy("mLock")
  public final void zab(int paramInt, boolean paramBoolean)
  {
    if ((paramInt == 1) && (!paramBoolean) && (!this.zagt))
    {
      this.zagt = true;
      if ((this.zagx == null) && (!ClientLibraryUtils.isPackageSide())) {
        this.zagx = this.zacc.zaa(this.mContext.getApplicationContext(), new zabc(this));
      }
      this.zagw.sendMessageDelayed(this.zagw.obtainMessage(1), this.zagu);
      this.zagw.sendMessageDelayed(this.zagw.obtainMessage(2), this.zagv);
    }
    this.zahe.zabx();
    this.zagr.onUnintentionalDisconnection(paramInt);
    this.zagr.disableCallbacks();
    if (paramInt == 2) {
      zaau();
    }
  }
  
  @GuardedBy("mLock")
  public final void zab(Bundle paramBundle)
  {
    while (!this.zafb.isEmpty()) {
      execute((BaseImplementation.ApiMethodImpl)this.zafb.remove());
    }
    this.zagr.onConnectionSuccess(paramBundle);
  }
  
  public final void zab(zacm paramzacm)
  {
    this.zaen.lock();
    for (;;)
    {
      try
      {
        if (this.zahd == null)
        {
          Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
          return;
        }
        if (!this.zahd.remove(paramzacm))
        {
          Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
          continue;
        }
        if (zaax()) {
          continue;
        }
      }
      finally
      {
        this.zaen.unlock();
      }
      this.zags.zaw();
    }
  }
  
  @GuardedBy("mLock")
  public final void zac(ConnectionResult paramConnectionResult)
  {
    if (!this.zacc.isPlayServicesPossiblyUpdating(this.mContext, paramConnectionResult.getErrorCode())) {
      zaaw();
    }
    if (!this.zagt)
    {
      this.zagr.onConnectionFailure(paramConnectionResult);
      this.zagr.disableCallbacks();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaaw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */