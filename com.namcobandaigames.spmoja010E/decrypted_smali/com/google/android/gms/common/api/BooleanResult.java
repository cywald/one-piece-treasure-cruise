package com.google.android.gms.common.api;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ShowFirstParty;

@KeepForSdk
public class BooleanResult
  implements Result
{
  private final Status mStatus;
  private final boolean zabg;
  
  @KeepForSdk
  @ShowFirstParty
  public BooleanResult(Status paramStatus, boolean paramBoolean)
  {
    this.mStatus = ((Status)Preconditions.checkNotNull(paramStatus, "Status must not be null"));
    this.zabg = paramBoolean;
  }
  
  @KeepForSdk
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof BooleanResult)) {
        return false;
      }
      paramObject = (BooleanResult)paramObject;
    } while ((this.mStatus.equals(((BooleanResult)paramObject).mStatus)) && (this.zabg == ((BooleanResult)paramObject).zabg));
    return false;
  }
  
  @KeepForSdk
  public Status getStatus()
  {
    return this.mStatus;
  }
  
  @KeepForSdk
  public boolean getValue()
  {
    return this.zabg;
  }
  
  @KeepForSdk
  public final int hashCode()
  {
    int j = this.mStatus.hashCode();
    if (this.zabg) {}
    for (int i = 1;; i = 0) {
      return i + (j + 527) * 31;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\BooleanResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */