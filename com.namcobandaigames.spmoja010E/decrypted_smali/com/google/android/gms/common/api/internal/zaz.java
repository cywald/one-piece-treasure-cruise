package com.google.android.gms.common.api.internal;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

final class zaz
  implements OnCompleteListener<Map<zai<?>, String>>
{
  private zaz(zax paramzax) {}
  
  public final void onComplete(@NonNull Task<Map<zai<?>, String>> paramTask)
  {
    zax.zaa(this.zafh).lock();
    Object localObject;
    try
    {
      boolean bool = zax.zab(this.zafh);
      if (!bool) {
        return;
      }
      if (paramTask.isSuccessful())
      {
        zax.zaa(this.zafh, new ArrayMap(zax.zac(this.zafh).size()));
        paramTask = zax.zac(this.zafh).values().iterator();
        while (paramTask.hasNext())
        {
          localObject = (zaw)paramTask.next();
          zax.zad(this.zafh).put(((GoogleApi)localObject).zak(), ConnectionResult.RESULT_SUCCESS);
        }
      }
      if (!(paramTask.getException() instanceof AvailabilityException)) {
        break label435;
      }
    }
    finally
    {
      zax.zaa(this.zafh).unlock();
    }
    paramTask = (AvailabilityException)paramTask.getException();
    if (zax.zae(this.zafh))
    {
      zax.zaa(this.zafh, new ArrayMap(zax.zac(this.zafh).size()));
      localObject = zax.zac(this.zafh).values().iterator();
      while (((Iterator)localObject).hasNext())
      {
        zaw localzaw = (zaw)((Iterator)localObject).next();
        zai localzai = localzaw.zak();
        ConnectionResult localConnectionResult = paramTask.getConnectionResult(localzaw);
        if (zax.zaa(this.zafh, localzaw, localConnectionResult)) {
          zax.zad(this.zafh).put(localzai, new ConnectionResult(16));
        } else {
          zax.zad(this.zafh).put(localzai, localConnectionResult);
        }
      }
    }
    zax.zaa(this.zafh, paramTask.zaj());
    zax.zaa(this.zafh, zax.zaf(this.zafh));
    if (zax.zag(this.zafh) != null)
    {
      zax.zad(this.zafh).putAll(zax.zag(this.zafh));
      zax.zaa(this.zafh, zax.zaf(this.zafh));
    }
    if (zax.zah(this.zafh) == null)
    {
      zax.zai(this.zafh);
      zax.zaj(this.zafh);
    }
    for (;;)
    {
      zax.zal(this.zafh).signalAll();
      zax.zaa(this.zafh).unlock();
      return;
      label435:
      Log.e("ConnectionlessGAC", "Unexpected availability exception", paramTask.getException());
      zax.zaa(this.zafh, Collections.emptyMap());
      zax.zaa(this.zafh, new ConnectionResult(8));
      break;
      zax.zaa(this.zafh, false);
      zax.zak(this.zafh).zac(zax.zah(this.zafh));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */