package com.google.android.gms.common.api.internal;

import android.support.annotation.WorkerThread;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultTransform;
import java.lang.ref.WeakReference;

final class zacn
  implements Runnable
{
  zacn(zacm paramzacm, Result paramResult) {}
  
  @WorkerThread
  public final void run()
  {
    try
    {
      BasePendingResult.zadm.set(Boolean.valueOf(true));
      Object localObject1 = zacm.zac(this.zakv).onSuccess(this.zaku);
      zacm.zad(this.zakv).sendMessage(zacm.zad(this.zakv).obtainMessage(0, localObject1));
      BasePendingResult.zadm.set(Boolean.valueOf(false));
      zacm.zaa(this.zakv, this.zaku);
      localObject1 = (GoogleApiClient)zacm.zae(this.zakv).get();
      if (localObject1 != null) {
        ((GoogleApiClient)localObject1).zab(this.zakv);
      }
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      zacm.zad(this.zakv).sendMessage(zacm.zad(this.zakv).obtainMessage(1, localRuntimeException));
      GoogleApiClient localGoogleApiClient1;
      return;
    }
    finally
    {
      BasePendingResult.zadm.set(Boolean.valueOf(false));
      zacm.zaa(this.zakv, this.zaku);
      GoogleApiClient localGoogleApiClient2 = (GoogleApiClient)zacm.zae(this.zakv).get();
      if (localGoogleApiClient2 != null) {
        localGoogleApiClient2.zab(this.zakv);
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zacn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */