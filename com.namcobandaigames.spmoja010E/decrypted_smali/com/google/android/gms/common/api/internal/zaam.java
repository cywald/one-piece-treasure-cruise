package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks;
import com.google.android.gms.common.internal.Preconditions;
import java.lang.ref.WeakReference;
import java.util.concurrent.locks.Lock;

final class zaam
  implements BaseGmsClient.ConnectionProgressReportCallbacks
{
  private final Api<?> mApi;
  private final boolean zaeb;
  private final WeakReference<zaak> zagj;
  
  public zaam(zaak paramzaak, Api<?> paramApi, boolean paramBoolean)
  {
    this.zagj = new WeakReference(paramzaak);
    this.mApi = paramApi;
    this.zaeb = paramBoolean;
  }
  
  public final void onReportServiceBinding(@NonNull ConnectionResult paramConnectionResult)
  {
    boolean bool = false;
    zaak localzaak = (zaak)this.zagj.get();
    if (localzaak == null) {
      return;
    }
    if (Looper.myLooper() == zaak.zad(localzaak).zaed.getLooper()) {
      bool = true;
    }
    Preconditions.checkState(bool, "onReportServiceBinding must be called on the GoogleApiClient handler thread");
    zaak.zac(localzaak).lock();
    try
    {
      bool = zaak.zaa(localzaak, 0);
      if (!bool) {
        return;
      }
      if (!paramConnectionResult.isSuccess()) {
        zaak.zaa(localzaak, paramConnectionResult, this.mApi, this.zaeb);
      }
      if (zaak.zak(localzaak)) {
        zaak.zaj(localzaak);
      }
      return;
    }
    finally
    {
      zaak.zac(localzaak).unlock();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaam.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */