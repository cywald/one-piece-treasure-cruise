package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.common.R.string;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.StringResourceValueReader;
import com.google.android.gms.common.internal.zzp;
import com.google.android.gms.common.util.VisibleForTesting;
import javax.annotation.concurrent.GuardedBy;

@Deprecated
@KeepForSdk
public final class GoogleServices
{
  private static final Object sLock = new Object();
  @GuardedBy("sLock")
  private static GoogleServices zzax;
  private final String zzay;
  private final Status zzaz;
  private final boolean zzba;
  private final boolean zzbb;
  
  @KeepForSdk
  @VisibleForTesting
  GoogleServices(Context paramContext)
  {
    Object localObject = paramContext.getResources();
    int i = ((Resources)localObject).getIdentifier("google_app_measurement_enable", "integer", ((Resources)localObject).getResourcePackageName(R.string.common_google_play_services_unknown_issue));
    if (i != 0) {
      if (((Resources)localObject).getInteger(i) != 0)
      {
        bool1 = true;
        if (bool1) {
          break label127;
        }
      }
    }
    label52:
    for (this.zzbb = bool2;; this.zzbb = false)
    {
      this.zzba = bool1;
      String str = zzp.zzc(paramContext);
      localObject = str;
      if (str == null) {
        localObject = new StringResourceValueReader(paramContext).getString("google_app_id");
      }
      if (!TextUtils.isEmpty((CharSequence)localObject)) {
        break label141;
      }
      this.zzaz = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
      this.zzay = null;
      return;
      bool1 = false;
      break;
      label127:
      bool2 = false;
      break label52;
    }
    label141:
    this.zzay = ((String)localObject);
    this.zzaz = Status.RESULT_SUCCESS;
  }
  
  @KeepForSdk
  @VisibleForTesting
  GoogleServices(String paramString, boolean paramBoolean)
  {
    this.zzay = paramString;
    this.zzaz = Status.RESULT_SUCCESS;
    this.zzba = paramBoolean;
    if (!paramBoolean) {}
    for (paramBoolean = true;; paramBoolean = false)
    {
      this.zzbb = paramBoolean;
      return;
    }
  }
  
  @KeepForSdk
  private static GoogleServices checkInitialized(String paramString)
  {
    synchronized (sLock)
    {
      if (zzax == null) {
        throw new IllegalStateException(String.valueOf(paramString).length() + 34 + "Initialize must be called before " + paramString + ".");
      }
    }
    paramString = zzax;
    return paramString;
  }
  
  @KeepForSdk
  @VisibleForTesting
  static void clearInstanceForTest()
  {
    synchronized (sLock)
    {
      zzax = null;
      return;
    }
  }
  
  @KeepForSdk
  public static String getGoogleAppId()
  {
    return checkInitialized("getGoogleAppId").zzay;
  }
  
  @KeepForSdk
  public static Status initialize(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext, "Context must not be null.");
    synchronized (sLock)
    {
      if (zzax == null) {
        zzax = new GoogleServices(paramContext);
      }
      paramContext = zzax.zzaz;
      return paramContext;
    }
  }
  
  @KeepForSdk
  public static Status initialize(Context arg0, String paramString, boolean paramBoolean)
  {
    Preconditions.checkNotNull(???, "Context must not be null.");
    Preconditions.checkNotEmpty(paramString, "App ID must be nonempty.");
    synchronized (sLock)
    {
      if (zzax != null)
      {
        paramString = zzax.checkGoogleAppId(paramString);
        return paramString;
      }
      paramString = new GoogleServices(paramString, paramBoolean);
      zzax = paramString;
      paramString = paramString.zzaz;
      return paramString;
    }
  }
  
  @KeepForSdk
  public static boolean isMeasurementEnabled()
  {
    GoogleServices localGoogleServices = checkInitialized("isMeasurementEnabled");
    return (localGoogleServices.zzaz.isSuccess()) && (localGoogleServices.zzba);
  }
  
  @KeepForSdk
  public static boolean isMeasurementExplicitlyDisabled()
  {
    return checkInitialized("isMeasurementExplicitlyDisabled").zzbb;
  }
  
  @KeepForSdk
  @VisibleForTesting
  final Status checkGoogleAppId(String paramString)
  {
    if ((this.zzay != null) && (!this.zzay.equals(paramString)))
    {
      paramString = this.zzay;
      return new Status(10, String.valueOf(paramString).length() + 97 + "Initialize was called with two different Google App IDs.  Only the first app ID will be used: '" + paramString + "'.");
    }
    return Status.RESULT_SUCCESS;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\GoogleServices.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */