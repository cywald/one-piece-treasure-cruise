package com.google.android.gms.common.api;

import android.support.annotation.NonNull;

public abstract interface ResultCallback<R extends Result>
{
  public abstract void onResult(@NonNull R paramR);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\ResultCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */