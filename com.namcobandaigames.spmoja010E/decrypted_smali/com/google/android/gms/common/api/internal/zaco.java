package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.base.zal;

final class zaco
  extends zal
{
  public zaco(zacm paramzacm, Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default: 
      int i = paramMessage.what;
      Log.e("TransformedResultImpl", 70 + "TransformationResultHandler received unknown message type: " + i);
      return;
    case 0: 
      PendingResult localPendingResult1 = (PendingResult)paramMessage.obj;
      paramMessage = zacm.zaf(this.zakv);
      if (localPendingResult1 == null) {}
      for (;;)
      {
        try
        {
          zacm.zaa(zacm.zag(this.zakv), new Status(13, "Transform returned null"));
          return;
        }
        finally {}
        if ((localPendingResult2 instanceof zacd)) {
          zacm.zaa(zacm.zag(this.zakv), ((zacd)localPendingResult2).getStatus());
        } else {
          zacm.zag(this.zakv).zaa(localPendingResult2);
        }
      }
    }
    RuntimeException localRuntimeException = (RuntimeException)paramMessage.obj;
    paramMessage = String.valueOf(localRuntimeException.getMessage());
    if (paramMessage.length() != 0) {}
    for (paramMessage = "Runtime exception on the transformation worker thread: ".concat(paramMessage);; paramMessage = new String("Runtime exception on the transformation worker thread: "))
    {
      Log.e("TransformedResultImpl", paramMessage);
      throw localRuntimeException;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaco.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */