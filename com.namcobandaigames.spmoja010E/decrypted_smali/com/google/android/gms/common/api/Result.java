package com.google.android.gms.common.api;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract interface Result
{
  @KeepForSdk
  public abstract Status getStatus();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\Result.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */