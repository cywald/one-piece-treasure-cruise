package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl;
import com.google.android.gms.common.api.internal.LifecycleActivity;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.SignInConnectionListener;
import com.google.android.gms.common.api.internal.zaaw;
import com.google.android.gms.common.api.internal.zacm;
import com.google.android.gms.common.api.internal.zaj;
import com.google.android.gms.common.api.internal.zaq;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.ClientSettings.OptionalApiSettings;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zaa;
import com.google.android.gms.signin.zad;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.concurrent.GuardedBy;

@KeepForSdk
public abstract class GoogleApiClient
{
  @KeepForSdk
  public static final String DEFAULT_ACCOUNT = "<<default account>>";
  public static final int SIGN_IN_MODE_OPTIONAL = 2;
  public static final int SIGN_IN_MODE_REQUIRED = 1;
  @GuardedBy("sAllClients")
  private static final Set<GoogleApiClient> zabq = Collections.newSetFromMap(new WeakHashMap());
  
  public static void dumpAll(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    synchronized (zabq)
    {
      String str = String.valueOf(paramString).concat("  ");
      Iterator localIterator = zabq.iterator();
      int i = 0;
      while (localIterator.hasNext())
      {
        GoogleApiClient localGoogleApiClient = (GoogleApiClient)localIterator.next();
        paramPrintWriter.append(paramString).append("GoogleApiClient#").println(i);
        localGoogleApiClient.dump(str, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        i += 1;
      }
      return;
    }
  }
  
  @KeepForSdk
  public static Set<GoogleApiClient> getAllClients()
  {
    synchronized (zabq)
    {
      Set localSet2 = zabq;
      return localSet2;
    }
  }
  
  public abstract ConnectionResult blockingConnect();
  
  public abstract ConnectionResult blockingConnect(long paramLong, @NonNull TimeUnit paramTimeUnit);
  
  public abstract PendingResult<Status> clearDefaultAccountAndReconnect();
  
  public abstract void connect();
  
  public void connect(int paramInt)
  {
    throw new UnsupportedOperationException();
  }
  
  public abstract void disconnect();
  
  public abstract void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString);
  
  @KeepForSdk
  public <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T enqueue(@NonNull T paramT)
  {
    throw new UnsupportedOperationException();
  }
  
  @KeepForSdk
  public <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T execute(@NonNull T paramT)
  {
    throw new UnsupportedOperationException();
  }
  
  @NonNull
  @KeepForSdk
  public <C extends Api.Client> C getClient(@NonNull Api.AnyClientKey<C> paramAnyClientKey)
  {
    throw new UnsupportedOperationException();
  }
  
  @NonNull
  public abstract ConnectionResult getConnectionResult(@NonNull Api<?> paramApi);
  
  @KeepForSdk
  public Context getContext()
  {
    throw new UnsupportedOperationException();
  }
  
  @KeepForSdk
  public Looper getLooper()
  {
    throw new UnsupportedOperationException();
  }
  
  @KeepForSdk
  public boolean hasApi(@NonNull Api<?> paramApi)
  {
    throw new UnsupportedOperationException();
  }
  
  public abstract boolean hasConnectedApi(@NonNull Api<?> paramApi);
  
  public abstract boolean isConnected();
  
  public abstract boolean isConnecting();
  
  public abstract boolean isConnectionCallbacksRegistered(@NonNull ConnectionCallbacks paramConnectionCallbacks);
  
  public abstract boolean isConnectionFailedListenerRegistered(@NonNull OnConnectionFailedListener paramOnConnectionFailedListener);
  
  @KeepForSdk
  public boolean maybeSignIn(SignInConnectionListener paramSignInConnectionListener)
  {
    throw new UnsupportedOperationException();
  }
  
  @KeepForSdk
  public void maybeSignOut()
  {
    throw new UnsupportedOperationException();
  }
  
  public abstract void reconnect();
  
  public abstract void registerConnectionCallbacks(@NonNull ConnectionCallbacks paramConnectionCallbacks);
  
  public abstract void registerConnectionFailedListener(@NonNull OnConnectionFailedListener paramOnConnectionFailedListener);
  
  @KeepForSdk
  public <L> ListenerHolder<L> registerListener(@NonNull L paramL)
  {
    throw new UnsupportedOperationException();
  }
  
  public abstract void stopAutoManage(@NonNull FragmentActivity paramFragmentActivity);
  
  public abstract void unregisterConnectionCallbacks(@NonNull ConnectionCallbacks paramConnectionCallbacks);
  
  public abstract void unregisterConnectionFailedListener(@NonNull OnConnectionFailedListener paramOnConnectionFailedListener);
  
  public void zaa(zacm paramzacm)
  {
    throw new UnsupportedOperationException();
  }
  
  public void zab(zacm paramzacm)
  {
    throw new UnsupportedOperationException();
  }
  
  @KeepForSdk
  public static final class Builder
  {
    private final Context mContext;
    private Looper zabj;
    private final Set<Scope> zabr = new HashSet();
    private final Set<Scope> zabs = new HashSet();
    private int zabt;
    private View zabu;
    private String zabv;
    private String zabw;
    private final Map<Api<?>, ClientSettings.OptionalApiSettings> zabx = new ArrayMap();
    private final Map<Api<?>, Api.ApiOptions> zaby = new ArrayMap();
    private LifecycleActivity zabz;
    private int zaca = -1;
    private GoogleApiClient.OnConnectionFailedListener zacb;
    private GoogleApiAvailability zacc = GoogleApiAvailability.getInstance();
    private Api.AbstractClientBuilder<? extends zad, SignInOptions> zacd = zaa.zapg;
    private final ArrayList<GoogleApiClient.ConnectionCallbacks> zace = new ArrayList();
    private final ArrayList<GoogleApiClient.OnConnectionFailedListener> zacf = new ArrayList();
    private boolean zacg = false;
    private Account zax;
    
    @KeepForSdk
    public Builder(@NonNull Context paramContext)
    {
      this.mContext = paramContext;
      this.zabj = paramContext.getMainLooper();
      this.zabv = paramContext.getPackageName();
      this.zabw = paramContext.getClass().getName();
    }
    
    @KeepForSdk
    public Builder(@NonNull Context paramContext, @NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, @NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      this(paramContext);
      Preconditions.checkNotNull(paramConnectionCallbacks, "Must provide a connected listener");
      this.zace.add(paramConnectionCallbacks);
      Preconditions.checkNotNull(paramOnConnectionFailedListener, "Must provide a connection failed listener");
      this.zacf.add(paramOnConnectionFailedListener);
    }
    
    private final <O extends Api.ApiOptions> void zaa(Api<O> paramApi, O paramO, Scope... paramVarArgs)
    {
      paramO = new HashSet(paramApi.zah().getImpliedScopes(paramO));
      int j = paramVarArgs.length;
      int i = 0;
      while (i < j)
      {
        paramO.add(paramVarArgs[i]);
        i += 1;
      }
      this.zabx.put(paramApi, new ClientSettings.OptionalApiSettings(paramO));
    }
    
    public final Builder addApi(@NonNull Api<? extends Api.ApiOptions.NotRequiredOptions> paramApi)
    {
      Preconditions.checkNotNull(paramApi, "Api must not be null");
      this.zaby.put(paramApi, null);
      paramApi = paramApi.zah().getImpliedScopes(null);
      this.zabs.addAll(paramApi);
      this.zabr.addAll(paramApi);
      return this;
    }
    
    public final <O extends Api.ApiOptions.HasOptions> Builder addApi(@NonNull Api<O> paramApi, @NonNull O paramO)
    {
      Preconditions.checkNotNull(paramApi, "Api must not be null");
      Preconditions.checkNotNull(paramO, "Null options are not permitted for this Api");
      this.zaby.put(paramApi, paramO);
      paramApi = paramApi.zah().getImpliedScopes(paramO);
      this.zabs.addAll(paramApi);
      this.zabr.addAll(paramApi);
      return this;
    }
    
    public final <O extends Api.ApiOptions.HasOptions> Builder addApiIfAvailable(@NonNull Api<O> paramApi, @NonNull O paramO, Scope... paramVarArgs)
    {
      Preconditions.checkNotNull(paramApi, "Api must not be null");
      Preconditions.checkNotNull(paramO, "Null options are not permitted for this Api");
      this.zaby.put(paramApi, paramO);
      zaa(paramApi, paramO, paramVarArgs);
      return this;
    }
    
    public final Builder addApiIfAvailable(@NonNull Api<? extends Api.ApiOptions.NotRequiredOptions> paramApi, Scope... paramVarArgs)
    {
      Preconditions.checkNotNull(paramApi, "Api must not be null");
      this.zaby.put(paramApi, null);
      zaa(paramApi, null, paramVarArgs);
      return this;
    }
    
    public final Builder addConnectionCallbacks(@NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
    {
      Preconditions.checkNotNull(paramConnectionCallbacks, "Listener must not be null");
      this.zace.add(paramConnectionCallbacks);
      return this;
    }
    
    public final Builder addOnConnectionFailedListener(@NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      Preconditions.checkNotNull(paramOnConnectionFailedListener, "Listener must not be null");
      this.zacf.add(paramOnConnectionFailedListener);
      return this;
    }
    
    public final Builder addScope(@NonNull Scope paramScope)
    {
      Preconditions.checkNotNull(paramScope, "Scope must not be null");
      this.zabr.add(paramScope);
      return this;
    }
    
    @KeepForSdk
    public final Builder addScopeNames(String[] paramArrayOfString)
    {
      int i = 0;
      while (i < paramArrayOfString.length)
      {
        this.zabr.add(new Scope(paramArrayOfString[i]));
        i += 1;
      }
      return this;
    }
    
    public final GoogleApiClient build()
    {
      boolean bool;
      ClientSettings localClientSettings;
      ArrayMap localArrayMap1;
      ArrayMap localArrayMap2;
      ArrayList localArrayList;
      int i;
      label80:
      Api localApi;
      Object localObject2;
      label129:
      Object localObject4;
      if (!this.zaby.isEmpty())
      {
        bool = true;
        Preconditions.checkArgument(bool, "must call addApi() to add at least one API");
        localClientSettings = buildClientSettings();
        ??? = null;
        Map localMap = localClientSettings.getOptionalApiSettings();
        localArrayMap1 = new ArrayMap();
        localArrayMap2 = new ArrayMap();
        localArrayList = new ArrayList();
        Iterator localIterator = this.zaby.keySet().iterator();
        i = 0;
        if (!localIterator.hasNext()) {
          break label330;
        }
        localApi = (Api)localIterator.next();
        localObject2 = this.zaby.get(localApi);
        if (localMap.get(localApi) == null) {
          break label311;
        }
        bool = true;
        localArrayMap1.put(localApi, Boolean.valueOf(bool));
        localObject4 = new zaq(localApi, bool);
        localArrayList.add(localObject4);
        Api.AbstractClientBuilder localAbstractClientBuilder = localApi.zai();
        localObject4 = localAbstractClientBuilder.buildClient(this.mContext, this.zabj, localClientSettings, localObject2, (GoogleApiClient.ConnectionCallbacks)localObject4, (GoogleApiClient.OnConnectionFailedListener)localObject4);
        localArrayMap2.put(localApi.getClientKey(), localObject4);
        if (localAbstractClientBuilder.getPriority() != 1) {
          break label571;
        }
        if (localObject2 == null) {
          break label316;
        }
        i = 1;
      }
      label311:
      label316:
      label324:
      label330:
      label559:
      label571:
      for (;;)
      {
        if (((Api.Client)localObject4).providesSignIn())
        {
          localObject2 = localApi;
          if (??? == null) {
            break label324;
          }
          localObject2 = localApi.getName();
          ??? = ((Api)???).getName();
          throw new IllegalStateException(String.valueOf(localObject2).length() + 21 + String.valueOf(???).length() + (String)localObject2 + " cannot be used with " + (String)???);
          bool = false;
          break;
          bool = false;
          break label129;
          i = 0;
          continue;
        }
        localObject2 = ???;
        ??? = localObject2;
        break label80;
        if (??? != null)
        {
          if (i != 0)
          {
            ??? = ((Api)???).getName();
            throw new IllegalStateException(String.valueOf(???).length() + 82 + "With using " + (String)??? + ", GamesOptions can only be specified within GoogleSignInOptions.Builder");
          }
          if (this.zax != null) {
            break label559;
          }
          bool = true;
        }
        for (;;)
        {
          Preconditions.checkState(bool, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", new Object[] { ((Api)???).getName() });
          Preconditions.checkState(this.zabr.equals(this.zabs), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", new Object[] { ((Api)???).getName() });
          i = zaaw.zaa(localArrayMap2.values(), true);
          localObject2 = new zaaw(this.mContext, new ReentrantLock(), this.zabj, localClientSettings, this.zacc, this.zacd, localArrayMap1, this.zace, this.zacf, localArrayMap2, this.zaca, i, localArrayList, false);
          synchronized (GoogleApiClient.zal())
          {
            GoogleApiClient.zal().add(localObject2);
            if (this.zaca >= 0) {
              zaj.zaa(this.zabz).zaa(this.zaca, (GoogleApiClient)localObject2, this.zacb);
            }
            return (GoogleApiClient)localObject2;
            bool = false;
          }
        }
      }
    }
    
    @KeepForSdk
    @VisibleForTesting
    public final ClientSettings buildClientSettings()
    {
      SignInOptions localSignInOptions = SignInOptions.DEFAULT;
      if (this.zaby.containsKey(zaa.API)) {
        localSignInOptions = (SignInOptions)this.zaby.get(zaa.API);
      }
      return new ClientSettings(this.zax, this.zabr, this.zabx, this.zabt, this.zabu, this.zabv, this.zabw, localSignInOptions);
    }
    
    public final Builder enableAutoManage(@NonNull FragmentActivity paramFragmentActivity, int paramInt, @Nullable GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      paramFragmentActivity = new LifecycleActivity(paramFragmentActivity);
      if (paramInt >= 0) {}
      for (boolean bool = true;; bool = false)
      {
        Preconditions.checkArgument(bool, "clientId must be non-negative");
        this.zaca = paramInt;
        this.zacb = paramOnConnectionFailedListener;
        this.zabz = paramFragmentActivity;
        return this;
      }
    }
    
    public final Builder enableAutoManage(@NonNull FragmentActivity paramFragmentActivity, @Nullable GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      return enableAutoManage(paramFragmentActivity, 0, paramOnConnectionFailedListener);
    }
    
    public final Builder setAccountName(String paramString)
    {
      if (paramString == null) {}
      for (paramString = null;; paramString = new Account(paramString, "com.google"))
      {
        this.zax = paramString;
        return this;
      }
    }
    
    public final Builder setGravityForPopups(int paramInt)
    {
      this.zabt = paramInt;
      return this;
    }
    
    public final Builder setHandler(@NonNull Handler paramHandler)
    {
      Preconditions.checkNotNull(paramHandler, "Handler must not be null");
      this.zabj = paramHandler.getLooper();
      return this;
    }
    
    public final Builder setViewForPopups(@NonNull View paramView)
    {
      Preconditions.checkNotNull(paramView, "View must not be null");
      this.zabu = paramView;
      return this;
    }
    
    public final Builder useDefaultAccount()
    {
      return setAccountName("<<default account>>");
    }
  }
  
  public static abstract interface ConnectionCallbacks
  {
    public static final int CAUSE_NETWORK_LOST = 2;
    public static final int CAUSE_SERVICE_DISCONNECTED = 1;
    
    public abstract void onConnected(@Nullable Bundle paramBundle);
    
    public abstract void onConnectionSuspended(int paramInt);
  }
  
  public static abstract interface OnConnectionFailedListener
  {
    public abstract void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\GoogleApiClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */