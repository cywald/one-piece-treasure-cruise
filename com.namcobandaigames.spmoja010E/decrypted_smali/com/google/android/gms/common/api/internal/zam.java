package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.Preconditions;

final class zam
{
  private final int zadg;
  private final ConnectionResult zadh;
  
  zam(ConnectionResult paramConnectionResult, int paramInt)
  {
    Preconditions.checkNotNull(paramConnectionResult);
    this.zadh = paramConnectionResult;
    this.zadg = paramInt;
  }
  
  final ConnectionResult getConnectionResult()
  {
    return this.zadh;
  }
  
  final int zar()
  {
    return this.zadg;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zam.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */