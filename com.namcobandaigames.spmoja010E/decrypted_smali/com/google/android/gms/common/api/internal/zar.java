package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;

public abstract interface zar
  extends GoogleApiClient.ConnectionCallbacks
{
  public abstract void zaa(ConnectionResult paramConnectionResult, Api<?> paramApi, boolean paramBoolean);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */