package com.google.android.gms.common.api;

import com.google.android.gms.common.Feature;
import com.google.android.gms.common.annotation.KeepForSdk;

public final class UnsupportedApiCallException
  extends UnsupportedOperationException
{
  private final Feature zzar;
  
  @KeepForSdk
  public UnsupportedApiCallException(Feature paramFeature)
  {
    this.zzar = paramFeature;
  }
  
  public final String getMessage()
  {
    String str = String.valueOf(this.zzar);
    return String.valueOf(str).length() + 8 + "Missing " + str;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\UnsupportedApiCallException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */