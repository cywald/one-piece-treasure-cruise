package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AbstractClientBuilder;
import com.google.android.gms.common.api.Api.AnyClient;
import com.google.android.gms.common.api.Api.AnyClientKey;
import com.google.android.gms.common.api.Api.BaseClientBuilder;
import com.google.android.gms.common.api.Api.Client;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.ClientSettings.OptionalApiSettings;
import com.google.android.gms.common.util.concurrent.HandlerExecutor;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;
import com.google.android.gms.tasks.Task;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import javax.annotation.concurrent.GuardedBy;

public final class zax
  implements zabs
{
  private final Looper zabj;
  private final GoogleApiManager zabm;
  private final Lock zaen;
  private final ClientSettings zaes;
  private final Map<Api.AnyClientKey<?>, zaw<?>> zaet = new HashMap();
  private final Map<Api.AnyClientKey<?>, zaw<?>> zaeu = new HashMap();
  private final Map<Api<?>, Boolean> zaev;
  private final zaaw zaew;
  private final GoogleApiAvailabilityLight zaex;
  private final Condition zaey;
  private final boolean zaez;
  private final boolean zafa;
  private final Queue<BaseImplementation.ApiMethodImpl<?, ?>> zafb = new LinkedList();
  @GuardedBy("mLock")
  private boolean zafc;
  @GuardedBy("mLock")
  private Map<zai<?>, ConnectionResult> zafd;
  @GuardedBy("mLock")
  private Map<zai<?>, ConnectionResult> zafe;
  @GuardedBy("mLock")
  private zaaa zaff;
  @GuardedBy("mLock")
  private ConnectionResult zafg;
  
  public zax(Context paramContext, Lock paramLock, Looper paramLooper, GoogleApiAvailabilityLight paramGoogleApiAvailabilityLight, Map<Api.AnyClientKey<?>, Api.Client> paramMap, ClientSettings paramClientSettings, Map<Api<?>, Boolean> paramMap1, Api.AbstractClientBuilder<? extends zad, SignInOptions> paramAbstractClientBuilder, ArrayList<zaq> paramArrayList, zaaw paramzaaw, boolean paramBoolean)
  {
    this.zaen = paramLock;
    this.zabj = paramLooper;
    this.zaey = paramLock.newCondition();
    this.zaex = paramGoogleApiAvailabilityLight;
    this.zaew = paramzaaw;
    this.zaev = paramMap1;
    this.zaes = paramClientSettings;
    this.zaez = paramBoolean;
    paramLock = new HashMap();
    paramGoogleApiAvailabilityLight = paramMap1.keySet().iterator();
    while (paramGoogleApiAvailabilityLight.hasNext())
    {
      paramMap1 = (Api)paramGoogleApiAvailabilityLight.next();
      paramLock.put(paramMap1.getClientKey(), paramMap1);
    }
    paramGoogleApiAvailabilityLight = new HashMap();
    paramMap1 = (ArrayList)paramArrayList;
    int j = paramMap1.size();
    int i = 0;
    while (i < j)
    {
      paramArrayList = paramMap1.get(i);
      i += 1;
      paramArrayList = (zaq)paramArrayList;
      paramGoogleApiAvailabilityLight.put(paramArrayList.mApi, paramArrayList);
    }
    paramMap = paramMap.entrySet().iterator();
    j = 1;
    i = 0;
    int k = 0;
    if (paramMap.hasNext())
    {
      paramMap1 = (Map.Entry)paramMap.next();
      paramzaaw = (Api)paramLock.get(paramMap1.getKey());
      paramArrayList = (Api.Client)paramMap1.getValue();
      if (paramArrayList.requiresGooglePlayServices())
      {
        k = 1;
        if (((Boolean)this.zaev.get(paramzaaw)).booleanValue()) {
          break label488;
        }
        i = j;
        j = 1;
      }
    }
    for (;;)
    {
      paramzaaw = new zaw(paramContext, paramzaaw, paramLooper, paramArrayList, (zaq)paramGoogleApiAvailabilityLight.get(paramzaaw), paramClientSettings, paramAbstractClientBuilder);
      this.zaet.put((Api.AnyClientKey)paramMap1.getKey(), paramzaaw);
      if (paramArrayList.requiresSignIn()) {
        this.zaeu.put((Api.AnyClientKey)paramMap1.getKey(), paramzaaw);
      }
      int m = j;
      j = i;
      i = m;
      break;
      m = 0;
      j = i;
      i = m;
      continue;
      if ((k != 0) && (j == 0) && (i == 0)) {}
      for (paramBoolean = true;; paramBoolean = false)
      {
        this.zafa = paramBoolean;
        this.zabm = GoogleApiManager.zabc();
        return;
      }
      label488:
      m = i;
      i = j;
      j = m;
    }
  }
  
  @Nullable
  private final ConnectionResult zaa(@NonNull Api.AnyClientKey<?> paramAnyClientKey)
  {
    this.zaen.lock();
    try
    {
      paramAnyClientKey = (zaw)this.zaet.get(paramAnyClientKey);
      if ((this.zafd != null) && (paramAnyClientKey != null))
      {
        paramAnyClientKey = (ConnectionResult)this.zafd.get(paramAnyClientKey.zak());
        return paramAnyClientKey;
      }
      return null;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  private final boolean zaa(zaw<?> paramzaw, ConnectionResult paramConnectionResult)
  {
    return (!paramConnectionResult.isSuccess()) && (!paramConnectionResult.hasResolution()) && (((Boolean)this.zaev.get(paramzaw.getApi())).booleanValue()) && (paramzaw.zaab().requiresGooglePlayServices()) && (this.zaex.isUserResolvableError(paramConnectionResult.getErrorCode()));
  }
  
  private final boolean zaac()
  {
    this.zaen.lock();
    try
    {
      boolean bool;
      if (this.zafc)
      {
        bool = this.zaez;
        if (bool) {}
      }
      else
      {
        return false;
      }
      Iterator localIterator = this.zaeu.keySet().iterator();
      while (localIterator.hasNext())
      {
        ConnectionResult localConnectionResult = zaa((Api.AnyClientKey)localIterator.next());
        if (localConnectionResult != null)
        {
          bool = localConnectionResult.isSuccess();
          if (bool) {
            break;
          }
        }
        else
        {
          return false;
        }
      }
      return true;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  @GuardedBy("mLock")
  private final void zaad()
  {
    if (this.zaes == null)
    {
      this.zaew.zagz = Collections.emptySet();
      return;
    }
    HashSet localHashSet = new HashSet(this.zaes.getRequiredScopes());
    Map localMap = this.zaes.getOptionalApiSettings();
    Iterator localIterator = localMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      Api localApi = (Api)localIterator.next();
      ConnectionResult localConnectionResult = getConnectionResult(localApi);
      if ((localConnectionResult != null) && (localConnectionResult.isSuccess())) {
        localHashSet.addAll(((ClientSettings.OptionalApiSettings)localMap.get(localApi)).mScopes);
      }
    }
    this.zaew.zagz = localHashSet;
  }
  
  @GuardedBy("mLock")
  private final void zaae()
  {
    while (!this.zafb.isEmpty()) {
      execute((BaseImplementation.ApiMethodImpl)this.zafb.remove());
    }
    this.zaew.zab(null);
  }
  
  @Nullable
  @GuardedBy("mLock")
  private final ConnectionResult zaaf()
  {
    Iterator localIterator = this.zaet.values().iterator();
    int j = 0;
    Object localObject2 = null;
    int i = 0;
    Object localObject1 = null;
    while (localIterator.hasNext())
    {
      Object localObject3 = (zaw)localIterator.next();
      Api localApi = ((GoogleApi)localObject3).getApi();
      localObject3 = ((GoogleApi)localObject3).zak();
      localObject3 = (ConnectionResult)this.zafd.get(localObject3);
      if ((!((ConnectionResult)localObject3).isSuccess()) && ((!((Boolean)this.zaev.get(localApi)).booleanValue()) || (((ConnectionResult)localObject3).hasResolution()) || (this.zaex.isUserResolvableError(((ConnectionResult)localObject3).getErrorCode()))))
      {
        int k;
        if ((((ConnectionResult)localObject3).getErrorCode() == 4) && (this.zaez))
        {
          k = localApi.zah().getPriority();
          if ((localObject2 == null) || (j > k))
          {
            j = k;
            localObject2 = localObject3;
          }
        }
        else
        {
          k = localApi.zah().getPriority();
          if ((localObject1 != null) && (i <= k)) {
            break label222;
          }
          localObject1 = localObject3;
          i = k;
        }
      }
    }
    label222:
    for (;;)
    {
      break;
      if ((localObject1 != null) && (localObject2 != null) && (i > j)) {
        return (ConnectionResult)localObject2;
      }
      return (ConnectionResult)localObject1;
    }
  }
  
  private final <T extends BaseImplementation.ApiMethodImpl<? extends Result, ? extends Api.AnyClient>> boolean zab(@NonNull T paramT)
  {
    Api.AnyClientKey localAnyClientKey = paramT.getClientKey();
    ConnectionResult localConnectionResult = zaa(localAnyClientKey);
    if ((localConnectionResult != null) && (localConnectionResult.getErrorCode() == 4))
    {
      paramT.setFailedResult(new Status(4, null, this.zabm.zaa(((zaw)this.zaet.get(localAnyClientKey)).zak(), System.identityHashCode(this.zaew))));
      return true;
    }
    return false;
  }
  
  @GuardedBy("mLock")
  public final ConnectionResult blockingConnect()
  {
    connect();
    while (isConnecting()) {
      try
      {
        this.zaey.await();
      }
      catch (InterruptedException localInterruptedException)
      {
        Thread.currentThread().interrupt();
        return new ConnectionResult(15, null);
      }
    }
    if (isConnected()) {
      return ConnectionResult.RESULT_SUCCESS;
    }
    if (this.zafg != null) {
      return this.zafg;
    }
    return new ConnectionResult(13, null);
  }
  
  @GuardedBy("mLock")
  public final ConnectionResult blockingConnect(long paramLong, TimeUnit paramTimeUnit)
  {
    connect();
    for (paramLong = paramTimeUnit.toNanos(paramLong); isConnecting(); paramLong = this.zaey.awaitNanos(paramLong))
    {
      if (paramLong <= 0L) {}
      try
      {
        disconnect();
        return new ConnectionResult(14, null);
      }
      catch (InterruptedException paramTimeUnit)
      {
        Thread.currentThread().interrupt();
        return new ConnectionResult(15, null);
      }
    }
    if (isConnected()) {
      return ConnectionResult.RESULT_SUCCESS;
    }
    if (this.zafg != null) {
      return this.zafg;
    }
    return new ConnectionResult(13, null);
  }
  
  public final void connect()
  {
    this.zaen.lock();
    try
    {
      boolean bool = this.zafc;
      if (bool) {
        return;
      }
      this.zafc = true;
      this.zafd = null;
      this.zafe = null;
      this.zaff = null;
      this.zafg = null;
      this.zabm.zao();
      this.zabm.zaa(this.zaet.values()).addOnCompleteListener(new HandlerExecutor(this.zabj), new zaz(this, null));
      return;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  public final void disconnect()
  {
    this.zaen.lock();
    try
    {
      this.zafc = false;
      this.zafd = null;
      this.zafe = null;
      if (this.zaff != null)
      {
        this.zaff.cancel();
        this.zaff = null;
      }
      this.zafg = null;
      while (!this.zafb.isEmpty())
      {
        BaseImplementation.ApiMethodImpl localApiMethodImpl = (BaseImplementation.ApiMethodImpl)this.zafb.remove();
        localApiMethodImpl.zaa(null);
        localApiMethodImpl.cancel();
      }
      this.zaey.signalAll();
    }
    finally
    {
      this.zaen.unlock();
    }
    this.zaen.unlock();
  }
  
  public final void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {}
  
  public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T enqueue(@NonNull T paramT)
  {
    if ((this.zaez) && (zab(paramT))) {
      return paramT;
    }
    if (!isConnected())
    {
      this.zafb.add(paramT);
      return paramT;
    }
    this.zaew.zahe.zab(paramT);
    return ((zaw)this.zaet.get(paramT.getClientKey())).doRead(paramT);
  }
  
  public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T execute(@NonNull T paramT)
  {
    Api.AnyClientKey localAnyClientKey = paramT.getClientKey();
    if ((this.zaez) && (zab(paramT))) {
      return paramT;
    }
    this.zaew.zahe.zab(paramT);
    return ((zaw)this.zaet.get(localAnyClientKey)).doWrite(paramT);
  }
  
  @Nullable
  public final ConnectionResult getConnectionResult(@NonNull Api<?> paramApi)
  {
    return zaa(paramApi.getClientKey());
  }
  
  /* Error */
  public final boolean isConnected()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 62	com/google/android/gms/common/api/internal/zax:zaen	Ljava/util/concurrent/locks/Lock;
    //   4: invokeinterface 184 1 0
    //   9: aload_0
    //   10: getfield 186	com/google/android/gms/common/api/internal/zax:zafd	Ljava/util/Map;
    //   13: ifnull +25 -> 38
    //   16: aload_0
    //   17: getfield 203	com/google/android/gms/common/api/internal/zax:zafg	Lcom/google/android/gms/common/ConnectionResult;
    //   20: astore_2
    //   21: aload_2
    //   22: ifnonnull +16 -> 38
    //   25: iconst_1
    //   26: istore_1
    //   27: aload_0
    //   28: getfield 62	com/google/android/gms/common/api/internal/zax:zaen	Ljava/util/concurrent/locks/Lock;
    //   31: invokeinterface 197 1 0
    //   36: iload_1
    //   37: ireturn
    //   38: iconst_0
    //   39: istore_1
    //   40: goto -13 -> 27
    //   43: astore_2
    //   44: aload_0
    //   45: getfield 62	com/google/android/gms/common/api/internal/zax:zaen	Ljava/util/concurrent/locks/Lock;
    //   48: invokeinterface 197 1 0
    //   53: aload_2
    //   54: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	55	0	this	zax
    //   26	14	1	bool	boolean
    //   20	2	2	localConnectionResult	ConnectionResult
    //   43	11	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	21	43	finally
  }
  
  /* Error */
  public final boolean isConnecting()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 62	com/google/android/gms/common/api/internal/zax:zaen	Ljava/util/concurrent/locks/Lock;
    //   4: invokeinterface 184 1 0
    //   9: aload_0
    //   10: getfield 186	com/google/android/gms/common/api/internal/zax:zafd	Ljava/util/Map;
    //   13: ifnonnull +25 -> 38
    //   16: aload_0
    //   17: getfield 236	com/google/android/gms/common/api/internal/zax:zafc	Z
    //   20: istore_1
    //   21: iload_1
    //   22: ifeq +16 -> 38
    //   25: iconst_1
    //   26: istore_1
    //   27: aload_0
    //   28: getfield 62	com/google/android/gms/common/api/internal/zax:zaen	Ljava/util/concurrent/locks/Lock;
    //   31: invokeinterface 197 1 0
    //   36: iload_1
    //   37: ireturn
    //   38: iconst_0
    //   39: istore_1
    //   40: goto -13 -> 27
    //   43: astore_2
    //   44: aload_0
    //   45: getfield 62	com/google/android/gms/common/api/internal/zax:zaen	Ljava/util/concurrent/locks/Lock;
    //   48: invokeinterface 197 1 0
    //   53: aload_2
    //   54: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	55	0	this	zax
    //   20	20	1	bool	boolean
    //   43	11	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	21	43	finally
  }
  
  public final boolean maybeSignIn(SignInConnectionListener paramSignInConnectionListener)
  {
    this.zaen.lock();
    try
    {
      if ((this.zafc) && (!zaac()))
      {
        this.zabm.zao();
        this.zaff = new zaaa(this, paramSignInConnectionListener);
        this.zabm.zaa(this.zaeu.values()).addOnCompleteListener(new HandlerExecutor(this.zabj), this.zaff);
        return true;
      }
      return false;
    }
    finally
    {
      this.zaen.unlock();
    }
  }
  
  public final void maybeSignOut()
  {
    this.zaen.lock();
    try
    {
      this.zabm.maybeSignOut();
      if (this.zaff != null)
      {
        this.zaff.cancel();
        this.zaff = null;
      }
      if (this.zafe == null) {
        this.zafe = new ArrayMap(this.zaeu.size());
      }
      ConnectionResult localConnectionResult = new ConnectionResult(4);
      Iterator localIterator = this.zaeu.values().iterator();
      while (localIterator.hasNext())
      {
        zaw localzaw = (zaw)localIterator.next();
        this.zafe.put(localzaw.zak(), localConnectionResult);
      }
      if (this.zafd == null) {
        break label155;
      }
    }
    finally
    {
      this.zaen.unlock();
    }
    this.zafd.putAll(this.zafe);
    label155:
    this.zaen.unlock();
  }
  
  public final void zaw() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zax.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */