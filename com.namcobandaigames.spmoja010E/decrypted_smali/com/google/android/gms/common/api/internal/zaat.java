package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.signin.zad;

final class zaat
  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
  private zaat(zaak paramzaak) {}
  
  public final void onConnected(Bundle paramBundle)
  {
    zaak.zaf(this.zagi).zaa(new zaar(this.zagi));
  }
  
  /* Error */
  public final void onConnectionFailed(@android.support.annotation.NonNull com.google.android.gms.common.ConnectionResult paramConnectionResult)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 14	com/google/android/gms/common/api/internal/zaat:zagi	Lcom/google/android/gms/common/api/internal/zaak;
    //   4: invokestatic 45	com/google/android/gms/common/api/internal/zaak:zac	(Lcom/google/android/gms/common/api/internal/zaak;)Ljava/util/concurrent/locks/Lock;
    //   7: invokeinterface 50 1 0
    //   12: aload_0
    //   13: getfield 14	com/google/android/gms/common/api/internal/zaat:zagi	Lcom/google/android/gms/common/api/internal/zaak;
    //   16: aload_1
    //   17: invokestatic 54	com/google/android/gms/common/api/internal/zaak:zab	(Lcom/google/android/gms/common/api/internal/zaak;Lcom/google/android/gms/common/ConnectionResult;)Z
    //   20: ifeq +30 -> 50
    //   23: aload_0
    //   24: getfield 14	com/google/android/gms/common/api/internal/zaat:zagi	Lcom/google/android/gms/common/api/internal/zaak;
    //   27: invokestatic 57	com/google/android/gms/common/api/internal/zaak:zai	(Lcom/google/android/gms/common/api/internal/zaak;)V
    //   30: aload_0
    //   31: getfield 14	com/google/android/gms/common/api/internal/zaat:zagi	Lcom/google/android/gms/common/api/internal/zaak;
    //   34: invokestatic 60	com/google/android/gms/common/api/internal/zaak:zaj	(Lcom/google/android/gms/common/api/internal/zaak;)V
    //   37: aload_0
    //   38: getfield 14	com/google/android/gms/common/api/internal/zaat:zagi	Lcom/google/android/gms/common/api/internal/zaak;
    //   41: invokestatic 45	com/google/android/gms/common/api/internal/zaak:zac	(Lcom/google/android/gms/common/api/internal/zaak;)Ljava/util/concurrent/locks/Lock;
    //   44: invokeinterface 63 1 0
    //   49: return
    //   50: aload_0
    //   51: getfield 14	com/google/android/gms/common/api/internal/zaat:zagi	Lcom/google/android/gms/common/api/internal/zaak;
    //   54: aload_1
    //   55: invokestatic 66	com/google/android/gms/common/api/internal/zaak:zaa	(Lcom/google/android/gms/common/api/internal/zaak;Lcom/google/android/gms/common/ConnectionResult;)V
    //   58: goto -21 -> 37
    //   61: astore_1
    //   62: aload_0
    //   63: getfield 14	com/google/android/gms/common/api/internal/zaat:zagi	Lcom/google/android/gms/common/api/internal/zaak;
    //   66: invokestatic 45	com/google/android/gms/common/api/internal/zaak:zac	(Lcom/google/android/gms/common/api/internal/zaak;)Ljava/util/concurrent/locks/Lock;
    //   69: invokeinterface 63 1 0
    //   74: aload_1
    //   75: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	76	0	this	zaat
    //   0	76	1	paramConnectionResult	com.google.android.gms.common.ConnectionResult
    // Exception table:
    //   from	to	target	type
    //   12	37	61	finally
    //   50	58	61	finally
  }
  
  public final void onConnectionSuspended(int paramInt) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */