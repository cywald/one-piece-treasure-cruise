package com.google.android.gms.common.api.internal;

import java.util.concurrent.locks.Lock;

final class zat
  implements Runnable
{
  zat(zas paramzas) {}
  
  public final void run()
  {
    zas.zaa(this.zaep).lock();
    try
    {
      zas.zab(this.zaep);
      return;
    }
    finally
    {
      zas.zaa(this.zaep).unlock();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */