package com.google.android.gms.common.api.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.Preconditions;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicReference;

public class zaj
  extends zal
{
  private final SparseArray<zaa> zacv = new SparseArray();
  
  private zaj(LifecycleFragment paramLifecycleFragment)
  {
    super(paramLifecycleFragment);
    this.mLifecycleFragment.addCallback("AutoManageHelper", this);
  }
  
  public static zaj zaa(LifecycleActivity paramLifecycleActivity)
  {
    paramLifecycleActivity = getFragment(paramLifecycleActivity);
    zaj localzaj = (zaj)paramLifecycleActivity.getCallbackOrNull("AutoManageHelper", zaj.class);
    if (localzaj != null) {
      return localzaj;
    }
    return new zaj(paramLifecycleActivity);
  }
  
  @Nullable
  private final zaa zab(int paramInt)
  {
    if (this.zacv.size() <= paramInt) {
      return null;
    }
    return (zaa)this.zacv.get(this.zacv.keyAt(paramInt));
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    int i = 0;
    while (i < this.zacv.size())
    {
      zaa localzaa = zab(i);
      if (localzaa != null)
      {
        paramPrintWriter.append(paramString).append("GoogleApiClient #").print(localzaa.zacw);
        paramPrintWriter.println(":");
        localzaa.zacx.dump(String.valueOf(paramString).concat("  "), paramFileDescriptor, paramPrintWriter, paramArrayOfString);
      }
      i += 1;
    }
  }
  
  public void onStart()
  {
    super.onStart();
    boolean bool = this.mStarted;
    Object localObject = String.valueOf(this.zacv);
    Log.d("AutoManageHelper", String.valueOf(localObject).length() + 14 + "onStart " + bool + " " + (String)localObject);
    if (this.zade.get() == null)
    {
      int i = 0;
      while (i < this.zacv.size())
      {
        localObject = zab(i);
        if (localObject != null) {
          ((zaa)localObject).zacx.connect();
        }
        i += 1;
      }
    }
  }
  
  public void onStop()
  {
    super.onStop();
    int i = 0;
    while (i < this.zacv.size())
    {
      zaa localzaa = zab(i);
      if (localzaa != null) {
        localzaa.zacx.disconnect();
      }
      i += 1;
    }
  }
  
  public final void zaa(int paramInt)
  {
    zaa localzaa = (zaa)this.zacv.get(paramInt);
    this.zacv.remove(paramInt);
    if (localzaa != null)
    {
      localzaa.zacx.unregisterConnectionFailedListener(localzaa);
      localzaa.zacx.disconnect();
    }
  }
  
  public final void zaa(int paramInt, GoogleApiClient paramGoogleApiClient, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    Preconditions.checkNotNull(paramGoogleApiClient, "GoogleApiClient instance cannot be null");
    if (this.zacv.indexOfKey(paramInt) < 0) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, 54 + "Already managing a GoogleApiClient with id " + paramInt);
      zam localzam = (zam)this.zade.get();
      bool = this.mStarted;
      String str = String.valueOf(localzam);
      Log.d("AutoManageHelper", String.valueOf(str).length() + 49 + "starting AutoManage for client " + paramInt + " " + bool + " " + str);
      paramOnConnectionFailedListener = new zaa(paramInt, paramGoogleApiClient, paramOnConnectionFailedListener);
      this.zacv.put(paramInt, paramOnConnectionFailedListener);
      if ((this.mStarted) && (localzam == null))
      {
        paramOnConnectionFailedListener = String.valueOf(paramGoogleApiClient);
        Log.d("AutoManageHelper", String.valueOf(paramOnConnectionFailedListener).length() + 11 + "connecting " + paramOnConnectionFailedListener);
        paramGoogleApiClient.connect();
      }
      return;
    }
  }
  
  protected final void zaa(ConnectionResult paramConnectionResult, int paramInt)
  {
    Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
    if (paramInt < 0) {
      Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
    }
    Object localObject;
    do
    {
      do
      {
        return;
        localObject = (zaa)this.zacv.get(paramInt);
      } while (localObject == null);
      zaa(paramInt);
      localObject = ((zaa)localObject).zacy;
    } while (localObject == null);
    ((GoogleApiClient.OnConnectionFailedListener)localObject).onConnectionFailed(paramConnectionResult);
  }
  
  protected final void zao()
  {
    int i = 0;
    while (i < this.zacv.size())
    {
      zaa localzaa = zab(i);
      if (localzaa != null) {
        localzaa.zacx.connect();
      }
      i += 1;
    }
  }
  
  private final class zaa
    implements GoogleApiClient.OnConnectionFailedListener
  {
    public final int zacw;
    public final GoogleApiClient zacx;
    public final GoogleApiClient.OnConnectionFailedListener zacy;
    
    public zaa(int paramInt, GoogleApiClient paramGoogleApiClient, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      this.zacw = paramInt;
      this.zacx = paramGoogleApiClient;
      this.zacy = paramOnConnectionFailedListener;
      paramGoogleApiClient.registerConnectionFailedListener(this);
    }
    
    public final void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
    {
      String str = String.valueOf(paramConnectionResult);
      Log.d("AutoManageHelper", String.valueOf(str).length() + 27 + "beginFailureResolution for " + str);
      zaj.this.zab(paramConnectionResult, this.zacw);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */