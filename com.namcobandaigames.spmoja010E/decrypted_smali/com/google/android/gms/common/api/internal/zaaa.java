package com.google.android.gms.common.api.internal;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

final class zaaa
  implements OnCompleteListener<Map<zai<?>, String>>
{
  private SignInConnectionListener zafi;
  
  zaaa(zax paramzax, SignInConnectionListener paramSignInConnectionListener)
  {
    this.zafi = paramSignInConnectionListener;
  }
  
  final void cancel()
  {
    this.zafi.onComplete();
  }
  
  public final void onComplete(@NonNull Task<Map<zai<?>, String>> paramTask)
  {
    zax.zaa(this.zafh).lock();
    Object localObject;
    try
    {
      if (!zax.zab(this.zafh))
      {
        this.zafi.onComplete();
        return;
      }
      if (paramTask.isSuccessful())
      {
        zax.zab(this.zafh, new ArrayMap(zax.zam(this.zafh).size()));
        paramTask = zax.zam(this.zafh).values().iterator();
        while (paramTask.hasNext())
        {
          localObject = (zaw)paramTask.next();
          zax.zag(this.zafh).put(((GoogleApi)localObject).zak(), ConnectionResult.RESULT_SUCCESS);
        }
      }
      if (!(paramTask.getException() instanceof AvailabilityException)) {
        break label417;
      }
    }
    finally
    {
      zax.zaa(this.zafh).unlock();
    }
    paramTask = (AvailabilityException)paramTask.getException();
    if (zax.zae(this.zafh))
    {
      zax.zab(this.zafh, new ArrayMap(zax.zam(this.zafh).size()));
      localObject = zax.zam(this.zafh).values().iterator();
      while (((Iterator)localObject).hasNext())
      {
        zaw localzaw = (zaw)((Iterator)localObject).next();
        zai localzai = localzaw.zak();
        ConnectionResult localConnectionResult = paramTask.getConnectionResult(localzaw);
        if (zax.zaa(this.zafh, localzaw, localConnectionResult)) {
          zax.zag(this.zafh).put(localzai, new ConnectionResult(16));
        } else {
          zax.zag(this.zafh).put(localzai, localConnectionResult);
        }
      }
    }
    zax.zab(this.zafh, paramTask.zaj());
    for (;;)
    {
      if (this.zafh.isConnected())
      {
        zax.zad(this.zafh).putAll(zax.zag(this.zafh));
        if (zax.zaf(this.zafh) == null)
        {
          zax.zai(this.zafh);
          zax.zaj(this.zafh);
          zax.zal(this.zafh).signalAll();
        }
      }
      this.zafi.onComplete();
      zax.zaa(this.zafh).unlock();
      return;
      label417:
      Log.e("ConnectionlessGAC", "Unexpected availability exception", paramTask.getException());
      zax.zab(this.zafh, Collections.emptyMap());
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zaaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */