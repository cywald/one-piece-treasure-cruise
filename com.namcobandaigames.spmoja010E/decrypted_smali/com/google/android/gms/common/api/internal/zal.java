package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.concurrent.atomic.AtomicReference;

public abstract class zal
  extends LifecycleCallback
  implements DialogInterface.OnCancelListener
{
  protected volatile boolean mStarted;
  protected final GoogleApiAvailability zacc;
  protected final AtomicReference<zam> zade = new AtomicReference(null);
  private final Handler zadf = new com.google.android.gms.internal.base.zal(Looper.getMainLooper());
  
  protected zal(LifecycleFragment paramLifecycleFragment)
  {
    this(paramLifecycleFragment, GoogleApiAvailability.getInstance());
  }
  
  @VisibleForTesting
  private zal(LifecycleFragment paramLifecycleFragment, GoogleApiAvailability paramGoogleApiAvailability)
  {
    super(paramLifecycleFragment);
    this.zacc = paramGoogleApiAvailability;
  }
  
  private static int zaa(@Nullable zam paramzam)
  {
    if (paramzam == null) {
      return -1;
    }
    return paramzam.zar();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i = 13;
    zam localzam = (zam)this.zade.get();
    switch (paramInt1)
    {
    default: 
      paramInt1 = 0;
      paramIntent = localzam;
      if (paramInt1 != 0)
      {
        zaq();
        return;
      }
      break;
    case 2: 
      label45:
      label53:
      i = this.zacc.isGooglePlayServicesAvailable(getActivity());
      if (i != 0) {}
      break;
    }
    for (paramInt2 = 1;; paramInt2 = 0)
    {
      if (localzam == null) {
        break label53;
      }
      paramIntent = localzam;
      paramInt1 = paramInt2;
      if (localzam.getConnectionResult().getErrorCode() != 18) {
        break label45;
      }
      paramIntent = localzam;
      paramInt1 = paramInt2;
      if (i != 18) {
        break label45;
      }
      return;
      if (paramInt2 == -1)
      {
        paramInt1 = 1;
        paramIntent = localzam;
        break label45;
      }
      if (paramInt2 != 0) {
        break;
      }
      paramInt1 = i;
      if (paramIntent != null) {
        paramInt1 = paramIntent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13);
      }
      paramIntent = new zam(new ConnectionResult(paramInt1, null), zaa(localzam));
      this.zade.set(paramIntent);
      paramInt1 = 0;
      break label45;
      if (paramIntent == null) {
        break label53;
      }
      zaa(paramIntent.getConnectionResult(), paramIntent.zar());
      return;
    }
  }
  
  public void onCancel(DialogInterface paramDialogInterface)
  {
    zaa(new ConnectionResult(13, null), zaa((zam)this.zade.get()));
    zaq();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    AtomicReference localAtomicReference;
    if (paramBundle != null)
    {
      localAtomicReference = this.zade;
      if (!paramBundle.getBoolean("resolving_error", false)) {
        break label67;
      }
    }
    label67:
    for (paramBundle = new zam(new ConnectionResult(paramBundle.getInt("failed_status"), (PendingIntent)paramBundle.getParcelable("failed_resolution")), paramBundle.getInt("failed_client_id", -1));; paramBundle = null)
    {
      localAtomicReference.set(paramBundle);
      return;
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    zam localzam = (zam)this.zade.get();
    if (localzam != null)
    {
      paramBundle.putBoolean("resolving_error", true);
      paramBundle.putInt("failed_client_id", localzam.zar());
      paramBundle.putInt("failed_status", localzam.getConnectionResult().getErrorCode());
      paramBundle.putParcelable("failed_resolution", localzam.getConnectionResult().getResolution());
    }
  }
  
  public void onStart()
  {
    super.onStart();
    this.mStarted = true;
  }
  
  public void onStop()
  {
    super.onStop();
    this.mStarted = false;
  }
  
  protected abstract void zaa(ConnectionResult paramConnectionResult, int paramInt);
  
  public final void zab(ConnectionResult paramConnectionResult, int paramInt)
  {
    paramConnectionResult = new zam(paramConnectionResult, paramInt);
    if (this.zade.compareAndSet(null, paramConnectionResult)) {
      this.zadf.post(new zan(this, paramConnectionResult));
    }
  }
  
  protected abstract void zao();
  
  protected final void zaq()
  {
    this.zade.set(null);
    zao();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\api\internal\zal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */