package com.google.android.gms.common.images;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.internal.base.zae;
import com.google.android.gms.internal.base.zaj;
import java.lang.ref.WeakReference;

public final class zac
  extends zaa
{
  private WeakReference<ImageView> zanb;
  
  public zac(ImageView paramImageView, int paramInt)
  {
    super(null, paramInt);
    Asserts.checkNotNull(paramImageView);
    this.zanb = new WeakReference(paramImageView);
  }
  
  public zac(ImageView paramImageView, Uri paramUri)
  {
    super(paramUri, 0);
    Asserts.checkNotNull(paramImageView);
    this.zanb = new WeakReference(paramImageView);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof zac)) {
      return false;
    }
    if (this == paramObject) {
      return true;
    }
    Object localObject = (zac)paramObject;
    paramObject = (ImageView)this.zanb.get();
    localObject = (ImageView)((zac)localObject).zanb.get();
    return (localObject != null) && (paramObject != null) && (Objects.equal(localObject, paramObject));
  }
  
  public final int hashCode()
  {
    return 0;
  }
  
  protected final void zaa(Drawable paramDrawable, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    Object localObject2 = null;
    ImageView localImageView = (ImageView)this.zanb.get();
    int i;
    Object localObject1;
    if (localImageView != null)
    {
      if ((paramBoolean2) || (paramBoolean3)) {
        break label190;
      }
      i = 1;
      if ((i != 0) && ((localImageView instanceof zaj)))
      {
        int j = zaj.zach();
        if ((this.zamw != 0) && (j == this.zamw)) {}
      }
      else
      {
        paramBoolean1 = zaa(paramBoolean1, paramBoolean2);
        if (!paramBoolean1) {
          break label208;
        }
        Drawable localDrawable = localImageView.getDrawable();
        if (localDrawable == null) {
          break label202;
        }
        localObject1 = localDrawable;
        if ((localDrawable instanceof zae)) {
          localObject1 = ((zae)localDrawable).zacf();
        }
        label111:
        paramDrawable = new zae((Drawable)localObject1, paramDrawable);
      }
    }
    label190:
    label196:
    label202:
    label208:
    for (;;)
    {
      localImageView.setImageDrawable(paramDrawable);
      if ((localImageView instanceof zaj))
      {
        localObject1 = localObject2;
        if (paramBoolean3) {
          localObject1 = this.zamu.uri;
        }
        zaj.zaa((Uri)localObject1);
        if (i == 0) {
          break label196;
        }
      }
      for (i = this.zamw;; i = 0)
      {
        zaj.zai(i);
        if (paramBoolean1) {
          ((zae)paramDrawable).startTransition(250);
        }
        return;
        i = 0;
        break;
      }
      localObject1 = null;
      break label111;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\images\zac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */