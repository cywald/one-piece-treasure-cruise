package com.google.android.gms.common.images;

public final class Size
{
  private final int zand;
  private final int zane;
  
  public Size(int paramInt1, int paramInt2)
  {
    this.zand = paramInt1;
    this.zane = paramInt2;
  }
  
  public static Size parseSize(String paramString)
    throws NumberFormatException
  {
    if (paramString == null) {
      throw new IllegalArgumentException("string must not be null");
    }
    int j = paramString.indexOf('*');
    int i = j;
    if (j < 0) {
      i = paramString.indexOf('x');
    }
    if (i < 0) {
      throw zah(paramString);
    }
    try
    {
      Size localSize = new Size(Integer.parseInt(paramString.substring(0, i)), Integer.parseInt(paramString.substring(i + 1)));
      return localSize;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw zah(paramString);
    }
  }
  
  private static NumberFormatException zah(String paramString)
  {
    throw new NumberFormatException(String.valueOf(paramString).length() + 16 + "Invalid Size: \"" + paramString + "\"");
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == null) {}
    do
    {
      do
      {
        return false;
        if (this == paramObject) {
          return true;
        }
      } while (!(paramObject instanceof Size));
      paramObject = (Size)paramObject;
    } while ((this.zand != ((Size)paramObject).zand) || (this.zane != ((Size)paramObject).zane));
    return true;
  }
  
  public final int getHeight()
  {
    return this.zane;
  }
  
  public final int getWidth()
  {
    return this.zand;
  }
  
  public final int hashCode()
  {
    return this.zane ^ (this.zand << 16 | this.zand >>> 16);
  }
  
  public final String toString()
  {
    int i = this.zand;
    int j = this.zane;
    return 23 + i + "x" + j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\images\Size.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */