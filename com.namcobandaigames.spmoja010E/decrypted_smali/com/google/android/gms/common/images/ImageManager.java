package com.google.android.gms.common.images;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.internal.base.zak;
import com.google.android.gms.internal.base.zal;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ImageManager
{
  private static final Object zamg = new Object();
  private static HashSet<Uri> zamh = new HashSet();
  private static ImageManager zami;
  private final Context mContext;
  private final Handler mHandler;
  private final ExecutorService zamj;
  private final zaa zamk;
  private final zak zaml;
  private final Map<zaa, ImageReceiver> zamm;
  private final Map<Uri, ImageReceiver> zamn;
  private final Map<Uri, Long> zamo;
  
  private ImageManager(Context paramContext, boolean paramBoolean)
  {
    this.mContext = paramContext.getApplicationContext();
    this.mHandler = new zal(Looper.getMainLooper());
    this.zamj = Executors.newFixedThreadPool(4);
    this.zamk = null;
    this.zaml = new zak();
    this.zamm = new HashMap();
    this.zamn = new HashMap();
    this.zamo = new HashMap();
  }
  
  public static ImageManager create(Context paramContext)
  {
    if (zami == null) {
      zami = new ImageManager(paramContext, false);
    }
    return zami;
  }
  
  private final Bitmap zaa(zab paramzab)
  {
    if (this.zamk == null) {
      return null;
    }
    return (Bitmap)this.zamk.get(paramzab);
  }
  
  private final void zaa(zaa paramzaa)
  {
    Asserts.checkMainThread("ImageManager.loadImage() must be called in the main thread");
    new zac(paramzaa).run();
  }
  
  public final void loadImage(ImageView paramImageView, int paramInt)
  {
    zaa(new zac(paramImageView, paramInt));
  }
  
  public final void loadImage(ImageView paramImageView, Uri paramUri)
  {
    zaa(new zac(paramImageView, paramUri));
  }
  
  public final void loadImage(ImageView paramImageView, Uri paramUri, int paramInt)
  {
    paramImageView = new zac(paramImageView, paramUri);
    paramImageView.zamw = paramInt;
    zaa(paramImageView);
  }
  
  public final void loadImage(OnImageLoadedListener paramOnImageLoadedListener, Uri paramUri)
  {
    zaa(new zad(paramOnImageLoadedListener, paramUri));
  }
  
  public final void loadImage(OnImageLoadedListener paramOnImageLoadedListener, Uri paramUri, int paramInt)
  {
    paramOnImageLoadedListener = new zad(paramOnImageLoadedListener, paramUri);
    paramOnImageLoadedListener.zamw = paramInt;
    zaa(paramOnImageLoadedListener);
  }
  
  @KeepName
  private final class ImageReceiver
    extends ResultReceiver
  {
    private final Uri mUri;
    private final ArrayList<zaa> zamp;
    
    ImageReceiver(Uri paramUri)
    {
      super();
      this.mUri = paramUri;
      this.zamp = new ArrayList();
    }
    
    public final void onReceiveResult(int paramInt, Bundle paramBundle)
    {
      paramBundle = (ParcelFileDescriptor)paramBundle.getParcelable("com.google.android.gms.extra.fileDescriptor");
      ImageManager.zaf(ImageManager.this).execute(new ImageManager.zab(ImageManager.this, this.mUri, paramBundle));
    }
    
    public final void zab(zaa paramzaa)
    {
      Asserts.checkMainThread("ImageReceiver.addImageRequest() must be called in the main thread");
      this.zamp.add(paramzaa);
    }
    
    public final void zac(zaa paramzaa)
    {
      Asserts.checkMainThread("ImageReceiver.removeImageRequest() must be called in the main thread");
      this.zamp.remove(paramzaa);
    }
    
    public final void zace()
    {
      Intent localIntent = new Intent("com.google.android.gms.common.images.LOAD_IMAGE");
      localIntent.putExtra("com.google.android.gms.extras.uri", this.mUri);
      localIntent.putExtra("com.google.android.gms.extras.resultReceiver", this);
      localIntent.putExtra("com.google.android.gms.extras.priority", 3);
      ImageManager.zab(ImageManager.this).sendBroadcast(localIntent);
    }
  }
  
  public static abstract interface OnImageLoadedListener
  {
    public abstract void onImageLoaded(Uri paramUri, Drawable paramDrawable, boolean paramBoolean);
  }
  
  private static final class zaa
    extends LruCache<zab, Bitmap>
  {}
  
  private final class zab
    implements Runnable
  {
    private final Uri mUri;
    private final ParcelFileDescriptor zamr;
    
    public zab(Uri paramUri, ParcelFileDescriptor paramParcelFileDescriptor)
    {
      this.mUri = paramUri;
      this.zamr = paramParcelFileDescriptor;
    }
    
    public final void run()
    {
      Asserts.checkNotMainThread("LoadBitmapFromDiskRunnable can't be executed in the main thread");
      boolean bool1 = false;
      boolean bool2 = false;
      Bitmap localBitmap = null;
      CountDownLatch localCountDownLatch = null;
      if (this.zamr != null) {}
      try
      {
        localBitmap = BitmapFactory.decodeFileDescriptor(this.zamr.getFileDescriptor());
        bool1 = bool2;
        String str2;
        Object localObject;
        String str1;
        return;
      }
      catch (OutOfMemoryError localOutOfMemoryError)
      {
        try
        {
          for (;;)
          {
            this.zamr.close();
            localCountDownLatch = new CountDownLatch(1);
            ImageManager.zag(ImageManager.this).post(new ImageManager.zad(ImageManager.this, this.mUri, localBitmap, bool1, localCountDownLatch));
            try
            {
              localCountDownLatch.await();
              return;
            }
            catch (InterruptedException localInterruptedException)
            {
              str1 = String.valueOf(this.mUri);
              Log.w("ImageManager", String.valueOf(str1).length() + 32 + "Latch interrupted while posting " + str1);
            }
            localOutOfMemoryError = localOutOfMemoryError;
            str2 = String.valueOf(this.mUri);
            Log.e("ImageManager", String.valueOf(str2).length() + 34 + "OOM while loading bitmap for uri: " + str2, localOutOfMemoryError);
            bool1 = true;
            localObject = localCountDownLatch;
          }
        }
        catch (IOException localIOException)
        {
          for (;;)
          {
            Log.e("ImageManager", "closed failed", localIOException);
          }
        }
      }
    }
  }
  
  private final class zac
    implements Runnable
  {
    private final zaa zams;
    
    public zac(zaa paramzaa)
    {
      this.zams = paramzaa;
    }
    
    public final void run()
    {
      Asserts.checkMainThread("LoadImageRunnable must be executed on the main thread");
      Object localObject1 = (ImageManager.ImageReceiver)ImageManager.zaa(ImageManager.this).get(this.zams);
      if (localObject1 != null)
      {
        ImageManager.zaa(ImageManager.this).remove(this.zams);
        ((ImageManager.ImageReceiver)localObject1).zac(this.zams);
      }
      zab localzab = this.zams.zamu;
      if (localzab.uri == null)
      {
        this.zams.zaa(ImageManager.zab(ImageManager.this), ImageManager.zac(ImageManager.this), true);
        return;
      }
      localObject1 = ImageManager.zaa(ImageManager.this, localzab);
      if (localObject1 != null)
      {
        this.zams.zaa(ImageManager.zab(ImageManager.this), (Bitmap)localObject1, true);
        return;
      }
      localObject1 = (Long)ImageManager.zad(ImageManager.this).get(localzab.uri);
      if (localObject1 != null)
      {
        if (SystemClock.elapsedRealtime() - ((Long)localObject1).longValue() < 3600000L)
        {
          this.zams.zaa(ImageManager.zab(ImageManager.this), ImageManager.zac(ImageManager.this), true);
          return;
        }
        ImageManager.zad(ImageManager.this).remove(localzab.uri);
      }
      this.zams.zaa(ImageManager.zab(ImageManager.this), ImageManager.zac(ImageManager.this));
      ??? = (ImageManager.ImageReceiver)ImageManager.zae(ImageManager.this).get(localzab.uri);
      localObject1 = ???;
      if (??? == null)
      {
        localObject1 = new ImageManager.ImageReceiver(ImageManager.this, localzab.uri);
        ImageManager.zae(ImageManager.this).put(localzab.uri, localObject1);
      }
      ((ImageManager.ImageReceiver)localObject1).zab(this.zams);
      if (!(this.zams instanceof zad)) {
        ImageManager.zaa(ImageManager.this).put(this.zams, localObject1);
      }
      synchronized (ImageManager.zacc())
      {
        if (!ImageManager.zacd().contains(localzab.uri))
        {
          ImageManager.zacd().add(localzab.uri);
          ((ImageManager.ImageReceiver)localObject1).zace();
        }
        return;
      }
    }
  }
  
  private final class zad
    implements Runnable
  {
    private final Bitmap mBitmap;
    private final Uri mUri;
    private final CountDownLatch zadq;
    private boolean zamt;
    
    public zad(Uri paramUri, Bitmap paramBitmap, boolean paramBoolean, CountDownLatch paramCountDownLatch)
    {
      this.mUri = paramUri;
      this.mBitmap = paramBitmap;
      this.zamt = paramBoolean;
      this.zadq = paramCountDownLatch;
    }
    
    public final void run()
    {
      Asserts.checkMainThread("OnBitmapLoadedRunnable must be executed in the main thread");
      int i;
      if (this.mBitmap != null) {
        i = 1;
      }
      while (ImageManager.zah(ImageManager.this) != null) {
        if (this.zamt)
        {
          ImageManager.zah(ImageManager.this).evictAll();
          System.gc();
          this.zamt = false;
          ImageManager.zag(ImageManager.this).post(this);
          return;
          i = 0;
        }
        else if (i != 0)
        {
          ImageManager.zah(ImageManager.this).put(new zab(this.mUri), this.mBitmap);
        }
      }
      ??? = (ImageManager.ImageReceiver)ImageManager.zae(ImageManager.this).remove(this.mUri);
      if (??? != null)
      {
        ??? = ImageManager.ImageReceiver.zaa((ImageManager.ImageReceiver)???);
        int k = ((ArrayList)???).size();
        int j = 0;
        if (j < k)
        {
          zaa localzaa = (zaa)((ArrayList)???).get(j);
          if (i != 0) {
            localzaa.zaa(ImageManager.zab(ImageManager.this), this.mBitmap, false);
          }
          for (;;)
          {
            if (!(localzaa instanceof zad)) {
              ImageManager.zaa(ImageManager.this).remove(localzaa);
            }
            j += 1;
            break;
            ImageManager.zad(ImageManager.this).put(this.mUri, Long.valueOf(SystemClock.elapsedRealtime()));
            localzaa.zaa(ImageManager.zab(ImageManager.this), ImageManager.zac(ImageManager.this), false);
          }
        }
      }
      this.zadq.countDown();
      synchronized (ImageManager.zacc())
      {
        ImageManager.zacd().remove(this.mUri);
        return;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\images\ImageManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */