package com.google.android.gms.common;

import java.util.Arrays;

final class zzf
  extends zze
{
  private final byte[] zzu;
  
  zzf(byte[] paramArrayOfByte)
  {
    super(Arrays.copyOfRange(paramArrayOfByte, 0, 25));
    this.zzu = paramArrayOfByte;
  }
  
  final byte[] getBytes()
  {
    return this.zzu;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */