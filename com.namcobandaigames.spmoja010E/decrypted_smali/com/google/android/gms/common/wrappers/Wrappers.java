package com.google.android.gms.common.wrappers;

import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class Wrappers
{
  private static Wrappers zzhx = new Wrappers();
  private PackageManagerWrapper zzhw = null;
  
  @KeepForSdk
  public static PackageManagerWrapper packageManager(Context paramContext)
  {
    return zzhx.zzi(paramContext);
  }
  
  /* Error */
  @com.google.android.gms.common.util.VisibleForTesting
  private final PackageManagerWrapper zzi(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 20	com/google/android/gms/common/wrappers/Wrappers:zzhw	Lcom/google/android/gms/common/wrappers/PackageManagerWrapper;
    //   6: ifnonnull +22 -> 28
    //   9: aload_1
    //   10: invokevirtual 33	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   13: ifnonnull +24 -> 37
    //   16: aload_0
    //   17: new 35	com/google/android/gms/common/wrappers/PackageManagerWrapper
    //   20: dup
    //   21: aload_1
    //   22: invokespecial 38	com/google/android/gms/common/wrappers/PackageManagerWrapper:<init>	(Landroid/content/Context;)V
    //   25: putfield 20	com/google/android/gms/common/wrappers/Wrappers:zzhw	Lcom/google/android/gms/common/wrappers/PackageManagerWrapper;
    //   28: aload_0
    //   29: getfield 20	com/google/android/gms/common/wrappers/Wrappers:zzhw	Lcom/google/android/gms/common/wrappers/PackageManagerWrapper;
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: areturn
    //   37: aload_1
    //   38: invokevirtual 33	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   41: astore_1
    //   42: goto -26 -> 16
    //   45: astore_1
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_1
    //   49: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	50	0	this	Wrappers
    //   0	50	1	paramContext	Context
    // Exception table:
    //   from	to	target	type
    //   2	16	45	finally
    //   16	28	45	finally
    //   28	33	45	finally
    //   37	42	45	finally
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\wrappers\Wrappers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */