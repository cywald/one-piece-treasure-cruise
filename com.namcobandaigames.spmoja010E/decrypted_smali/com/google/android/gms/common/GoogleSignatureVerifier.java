package com.google.android.gms.common;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.wrappers.PackageManagerWrapper;
import com.google.android.gms.common.wrappers.Wrappers;
import javax.annotation.CheckReturnValue;

@CheckReturnValue
@KeepForSdk
@ShowFirstParty
public class GoogleSignatureVerifier
{
  private static GoogleSignatureVerifier zzal;
  private final Context mContext;
  private volatile String zzam;
  
  private GoogleSignatureVerifier(Context paramContext)
  {
    this.mContext = paramContext.getApplicationContext();
  }
  
  @KeepForSdk
  public static GoogleSignatureVerifier getInstance(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    try
    {
      if (zzal == null)
      {
        zzc.zza(paramContext);
        zzal = new GoogleSignatureVerifier(paramContext);
      }
      return zzal;
    }
    finally {}
  }
  
  private static zze zza(PackageInfo paramPackageInfo, zze... paramVarArgs)
  {
    int i = 0;
    if (paramPackageInfo.signatures == null) {
      return null;
    }
    if (paramPackageInfo.signatures.length != 1)
    {
      Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
      return null;
    }
    paramPackageInfo = new zzf(paramPackageInfo.signatures[0].toByteArray());
    while (i < paramVarArgs.length)
    {
      if (paramVarArgs[i].equals(paramPackageInfo)) {
        return paramVarArgs[i];
      }
      i += 1;
    }
    return null;
  }
  
  private final zzm zza(PackageInfo paramPackageInfo)
  {
    boolean bool = GooglePlayServicesUtilLight.honorsDebugCertificates(this.mContext);
    Object localObject;
    if (paramPackageInfo == null) {
      localObject = zzm.zzb("null pkg");
    }
    zzf localzzf;
    String str;
    do
    {
      zzm localzzm;
      do
      {
        do
        {
          do
          {
            return (zzm)localObject;
            if (paramPackageInfo.signatures.length != 1) {
              return zzm.zzb("single cert required");
            }
            localzzf = new zzf(paramPackageInfo.signatures[0].toByteArray());
            str = paramPackageInfo.packageName;
            localzzm = zzc.zza(str, localzzf, bool);
            localObject = localzzm;
          } while (!localzzm.zzac);
          localObject = localzzm;
        } while (paramPackageInfo.applicationInfo == null);
        localObject = localzzm;
      } while ((paramPackageInfo.applicationInfo.flags & 0x2) == 0);
      if (!bool) {
        break;
      }
      localObject = localzzm;
    } while (!zzc.zza(str, localzzf, false).zzac);
    return zzm.zzb("debuggable release cert app rejected");
  }
  
  private final zzm zza(String paramString, int paramInt)
  {
    try
    {
      zzm localzzm = zza(Wrappers.packageManager(this.mContext).zza(paramString, 64, paramInt));
      return localzzm;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      paramString = String.valueOf(paramString);
      if (paramString.length() == 0) {}
    }
    for (paramString = "no pkg ".concat(paramString);; paramString = new String("no pkg ")) {
      return zzm.zzb(paramString);
    }
  }
  
  public static boolean zza(PackageInfo paramPackageInfo, boolean paramBoolean)
  {
    if ((paramPackageInfo != null) && (paramPackageInfo.signatures != null))
    {
      if (paramBoolean) {}
      for (paramPackageInfo = zza(paramPackageInfo, zzh.zzx); paramPackageInfo != null; paramPackageInfo = zza(paramPackageInfo, new zze[] { zzh.zzx[0] })) {
        return true;
      }
    }
    return false;
  }
  
  private final zzm zzc(String paramString)
  {
    Object localObject;
    if (paramString == null) {
      localObject = zzm.zzb("null pkg");
    }
    for (;;)
    {
      return (zzm)localObject;
      if (paramString.equals(this.zzam)) {
        return zzm.zze();
      }
      try
      {
        localObject = Wrappers.packageManager(this.mContext).getPackageInfo(paramString, 64);
        zzm localzzm = zza((PackageInfo)localObject);
        localObject = localzzm;
        if (localzzm.zzac)
        {
          this.zzam = paramString;
          return localzzm;
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        paramString = String.valueOf(paramString);
        if (paramString.length() == 0) {}
      }
    }
    for (paramString = "no pkg ".concat(paramString);; paramString = new String("no pkg ")) {
      return zzm.zzb(paramString);
    }
  }
  
  @KeepForSdk
  public boolean isGooglePublicSignedPackage(PackageInfo paramPackageInfo)
  {
    if (paramPackageInfo == null) {}
    do
    {
      return false;
      if (zza(paramPackageInfo, false)) {
        return true;
      }
    } while (!zza(paramPackageInfo, true));
    if (GooglePlayServicesUtilLight.honorsDebugCertificates(this.mContext)) {
      return true;
    }
    Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
    return false;
  }
  
  @KeepForSdk
  @ShowFirstParty
  public boolean isPackageGoogleSigned(String paramString)
  {
    paramString = zzc(paramString);
    paramString.zzf();
    return paramString.zzac;
  }
  
  @KeepForSdk
  @ShowFirstParty
  public boolean isUidGoogleSigned(int paramInt)
  {
    String[] arrayOfString = Wrappers.packageManager(this.mContext).getPackagesForUid(paramInt);
    Object localObject;
    if ((arrayOfString == null) || (arrayOfString.length == 0)) {
      localObject = zzm.zzb("no pkgs");
    }
    label88:
    for (;;)
    {
      ((zzm)localObject).zzf();
      return ((zzm)localObject).zzac;
      int j = arrayOfString.length;
      localObject = null;
      int i = 0;
      for (;;)
      {
        if (i >= j) {
          break label88;
        }
        zzm localzzm = zza(arrayOfString[i], paramInt);
        localObject = localzzm;
        if (localzzm.zzac) {
          break;
        }
        i += 1;
        localObject = localzzm;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\GoogleSignatureVerifier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */