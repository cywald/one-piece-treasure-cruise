package com.google.android.gms.common.util;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@VisibleForTesting
public class NumberUtils
{
  @KeepForSdk
  public static long parseHexLong(String paramString)
  {
    if (paramString.length() > 16) {
      throw new NumberFormatException(String.valueOf(paramString).length() + 37 + "Invalid input: " + paramString + " exceeds 16 characters");
    }
    if (paramString.length() == 16) {
      return Long.parseLong(paramString.substring(8), 16) | Long.parseLong(paramString.substring(0, 8), 16) << 32;
    }
    return Long.parseLong(paramString, 16);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\util\NumberUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */