package com.google.android.gms.common.util;

import java.util.regex.Pattern;

public final class zzb
{
  private static Pattern zzgv = null;
  
  public static int zzc(int paramInt)
  {
    if (paramInt == -1) {
      return -1;
    }
    return paramInt / 1000;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\util\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */