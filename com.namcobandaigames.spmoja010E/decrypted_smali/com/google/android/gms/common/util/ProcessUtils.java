package com.google.android.gms.common.util;

import android.os.Process;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import com.google.android.gms.common.annotation.KeepForSdk;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.annotation.Nullable;

@KeepForSdk
public class ProcessUtils
{
  private static String zzhd = null;
  private static int zzhe = 0;
  
  @Nullable
  @KeepForSdk
  public static String getMyProcessName()
  {
    if (zzhd == null)
    {
      if (zzhe == 0) {
        zzhe = Process.myPid();
      }
      zzhd = zzd(zzhe);
    }
    return zzhd;
  }
  
  /* Error */
  @Nullable
  private static String zzd(int paramInt)
  {
    // Byte code:
    //   0: iload_0
    //   1: ifgt +5 -> 6
    //   4: aconst_null
    //   5: areturn
    //   6: new 38	java/lang/StringBuilder
    //   9: dup
    //   10: bipush 25
    //   12: invokespecial 41	java/lang/StringBuilder:<init>	(I)V
    //   15: ldc 43
    //   17: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   20: iload_0
    //   21: invokevirtual 50	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   24: ldc 52
    //   26: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: invokevirtual 55	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   32: invokestatic 59	com/google/android/gms/common/util/ProcessUtils:zzj	(Ljava/lang/String;)Ljava/io/BufferedReader;
    //   35: astore_1
    //   36: aload_1
    //   37: invokevirtual 64	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   40: invokevirtual 69	java/lang/String:trim	()Ljava/lang/String;
    //   43: astore_2
    //   44: aload_1
    //   45: invokestatic 75	com/google/android/gms/common/util/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   48: aload_2
    //   49: areturn
    //   50: astore_1
    //   51: aconst_null
    //   52: astore_1
    //   53: aload_1
    //   54: invokestatic 75	com/google/android/gms/common/util/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   57: aconst_null
    //   58: areturn
    //   59: astore_1
    //   60: aconst_null
    //   61: astore_3
    //   62: aload_1
    //   63: astore_2
    //   64: aload_3
    //   65: invokestatic 75	com/google/android/gms/common/util/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   68: aload_2
    //   69: athrow
    //   70: astore_2
    //   71: aload_1
    //   72: astore_3
    //   73: goto -9 -> 64
    //   76: astore_2
    //   77: goto -24 -> 53
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	80	0	paramInt	int
    //   35	10	1	localBufferedReader	BufferedReader
    //   50	1	1	localIOException1	IOException
    //   52	2	1	localCloseable	java.io.Closeable
    //   59	13	1	localObject1	Object
    //   43	26	2	localObject2	Object
    //   70	1	2	localObject3	Object
    //   76	1	2	localIOException2	IOException
    //   61	12	3	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   6	36	50	java/io/IOException
    //   6	36	59	finally
    //   36	44	70	finally
    //   36	44	76	java/io/IOException
  }
  
  private static BufferedReader zzj(String paramString)
    throws IOException
  {
    StrictMode.ThreadPolicy localThreadPolicy = StrictMode.allowThreadDiskReads();
    try
    {
      paramString = new BufferedReader(new FileReader(paramString));
      return paramString;
    }
    finally
    {
      StrictMode.setThreadPolicy(localThreadPolicy);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\util\ProcessUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */