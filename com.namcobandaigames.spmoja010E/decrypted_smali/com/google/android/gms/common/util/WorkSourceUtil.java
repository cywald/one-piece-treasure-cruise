package com.google.android.gms.common.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Process;
import android.os.WorkSource;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.wrappers.PackageManagerWrapper;
import com.google.android.gms.common.wrappers.Wrappers;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@KeepForSdk
public class WorkSourceUtil
{
  private static final int zzhh = ;
  private static final Method zzhi = zzw();
  private static final Method zzhj = zzx();
  private static final Method zzhk = zzy();
  private static final Method zzhl = zzz();
  private static final Method zzhm = zzaa();
  private static final Method zzhn = zzab();
  private static final Method zzho = zzac();
  
  @Nullable
  @KeepForSdk
  public static WorkSource fromPackage(Context paramContext, @Nullable String paramString)
  {
    if ((paramContext == null) || (paramContext.getPackageManager() == null) || (paramString == null)) {
      return null;
    }
    try
    {
      paramContext = Wrappers.packageManager(paramContext).getApplicationInfo(paramString, 0);
      if (paramContext != null) {
        break label114;
      }
      paramContext = String.valueOf(paramString);
      if (paramContext.length() == 0) {
        break label101;
      }
      paramContext = "Could not get applicationInfo from package: ".concat(paramContext);
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;)
      {
        paramContext = String.valueOf(paramString);
        if (paramContext.length() != 0) {}
        for (paramContext = "Could not find package: ".concat(paramContext);; paramContext = new String("Could not find package: "))
        {
          Log.e("WorkSourceUtil", paramContext);
          return null;
        }
        paramContext = new String("Could not get applicationInfo from package: ");
      }
    }
    Log.e("WorkSourceUtil", paramContext);
    return null;
    label101:
    label114:
    return zza(paramContext.uid, paramString);
  }
  
  @KeepForSdk
  public static WorkSource fromPackageAndModuleExperimentalPi(Context paramContext, String paramString1, String paramString2)
  {
    if ((paramContext == null) || (paramContext.getPackageManager() == null) || (paramString2 == null) || (paramString1 == null)) {
      Log.w("WorkSourceUtil", "Unexpected null arguments");
    }
    int i;
    do
    {
      return null;
      i = zzd(paramContext, paramString1);
    } while (i < 0);
    paramContext = new WorkSource();
    if ((zzhn == null) || (zzho == null))
    {
      zza(paramContext, i, paramString1);
      return paramContext;
    }
    try
    {
      Object localObject = zzhn.invoke(paramContext, new Object[0]);
      if (i != zzhh) {
        zzho.invoke(localObject, new Object[] { Integer.valueOf(i), paramString1 });
      }
      zzho.invoke(localObject, new Object[] { Integer.valueOf(zzhh), paramString2 });
      return paramContext;
    }
    catch (Exception paramString1)
    {
      Log.w("WorkSourceUtil", "Unable to assign chained blame through WorkSource", paramString1);
    }
    return paramContext;
  }
  
  @KeepForSdk
  public static List<String> getNames(@Nullable WorkSource paramWorkSource)
  {
    int j = 0;
    if (paramWorkSource == null) {}
    Object localObject;
    for (int i = 0; i == 0; i = zza(paramWorkSource))
    {
      localObject = Collections.emptyList();
      return (List<String>)localObject;
    }
    ArrayList localArrayList = new ArrayList();
    for (;;)
    {
      localObject = localArrayList;
      if (j >= i) {
        break;
      }
      localObject = zza(paramWorkSource, j);
      if (!Strings.isEmptyOrWhitespace((String)localObject)) {
        localArrayList.add(localObject);
      }
      j += 1;
    }
  }
  
  @KeepForSdk
  public static boolean hasWorkSourcePermission(Context paramContext)
  {
    if (paramContext == null) {}
    while ((paramContext.getPackageManager() == null) || (Wrappers.packageManager(paramContext).checkPermission("android.permission.UPDATE_DEVICE_STATS", paramContext.getPackageName()) != 0)) {
      return false;
    }
    return true;
  }
  
  private static int zza(WorkSource paramWorkSource)
  {
    if (zzhk != null) {
      try
      {
        int i = ((Integer)zzhk.invoke(paramWorkSource, new Object[0])).intValue();
        return i;
      }
      catch (Exception paramWorkSource)
      {
        Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", paramWorkSource);
      }
    }
    return 0;
  }
  
  private static WorkSource zza(int paramInt, String paramString)
  {
    WorkSource localWorkSource = new WorkSource();
    zza(localWorkSource, paramInt, paramString);
    return localWorkSource;
  }
  
  @Nullable
  private static String zza(WorkSource paramWorkSource, int paramInt)
  {
    if (zzhm != null) {
      try
      {
        paramWorkSource = (String)zzhm.invoke(paramWorkSource, new Object[] { Integer.valueOf(paramInt) });
        return paramWorkSource;
      }
      catch (Exception paramWorkSource)
      {
        Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", paramWorkSource);
      }
    }
    return null;
  }
  
  private static void zza(WorkSource paramWorkSource, int paramInt, @Nullable String paramString)
  {
    if (zzhj != null)
    {
      str = paramString;
      if (paramString == null) {
        str = "";
      }
    }
    while (zzhi == null) {
      try
      {
        String str;
        zzhj.invoke(paramWorkSource, new Object[] { Integer.valueOf(paramInt), str });
        return;
      }
      catch (Exception paramWorkSource)
      {
        Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", paramWorkSource);
        return;
      }
    }
    try
    {
      zzhi.invoke(paramWorkSource, new Object[] { Integer.valueOf(paramInt) });
      return;
    }
    catch (Exception paramWorkSource)
    {
      Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", paramWorkSource);
    }
  }
  
  private static Method zzaa()
  {
    Method localMethod = null;
    if (PlatformVersion.isAtLeastJellyBeanMR2()) {}
    try
    {
      localMethod = WorkSource.class.getMethod("getName", new Class[] { Integer.TYPE });
      return localMethod;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static final Method zzab()
  {
    Method localMethod = null;
    if (PlatformVersion.isAtLeastP()) {}
    try
    {
      localMethod = WorkSource.class.getMethod("createWorkChain", new Class[0]);
      return localMethod;
    }
    catch (Exception localException)
    {
      Log.w("WorkSourceUtil", "Missing WorkChain API createWorkChain", localException);
    }
    return null;
  }
  
  @SuppressLint({"PrivateApi"})
  private static final Method zzac()
  {
    Method localMethod = null;
    if (PlatformVersion.isAtLeastP()) {}
    try
    {
      localMethod = Class.forName("android.os.WorkSource$WorkChain").getMethod("addNode", new Class[] { Integer.TYPE, String.class });
      return localMethod;
    }
    catch (Exception localException)
    {
      Log.w("WorkSourceUtil", "Missing WorkChain class", localException);
    }
    return null;
  }
  
  private static int zzd(Context paramContext, String paramString)
  {
    try
    {
      paramContext = Wrappers.packageManager(paramContext).getApplicationInfo(paramString, 0);
      if (paramContext != null) {
        break label97;
      }
      paramContext = String.valueOf(paramString);
      if (paramContext.length() == 0) {
        break label84;
      }
      paramContext = "Could not get applicationInfo from package: ".concat(paramContext);
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;)
      {
        paramContext = String.valueOf(paramString);
        if (paramContext.length() != 0) {}
        for (paramContext = "Could not find package: ".concat(paramContext);; paramContext = new String("Could not find package: "))
        {
          Log.e("WorkSourceUtil", paramContext);
          return -1;
        }
        label84:
        paramContext = new String("Could not get applicationInfo from package: ");
      }
    }
    Log.e("WorkSourceUtil", paramContext);
    return -1;
    label97:
    return paramContext.uid;
  }
  
  private static Method zzw()
  {
    try
    {
      Method localMethod = WorkSource.class.getMethod("add", new Class[] { Integer.TYPE });
      return localMethod;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static Method zzx()
  {
    Method localMethod = null;
    if (PlatformVersion.isAtLeastJellyBeanMR2()) {}
    try
    {
      localMethod = WorkSource.class.getMethod("add", new Class[] { Integer.TYPE, String.class });
      return localMethod;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static Method zzy()
  {
    try
    {
      Method localMethod = WorkSource.class.getMethod("size", new Class[0]);
      return localMethod;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static Method zzz()
  {
    try
    {
      Method localMethod = WorkSource.class.getMethod("get", new Class[] { Integer.TYPE });
      return localMethod;
    }
    catch (Exception localException) {}
    return null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\util\WorkSourceUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */