package com.google.android.gms.common.util;

import com.google.android.gms.common.annotation.KeepForSdk;
import java.lang.annotation.Annotation;

@KeepForSdk
public @interface RetainForClient {}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\util\RetainForClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */