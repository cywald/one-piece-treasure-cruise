package com.google.android.gms.common.util;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Objects;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

@KeepForSdk
@VisibleForTesting
public final class ArrayUtils
{
  @KeepForSdk
  public static <T> T[] appendToArray(T[] paramArrayOfT, T paramT)
  {
    if ((paramArrayOfT == null) && (paramT == null)) {
      throw new IllegalArgumentException("Cannot generate array of generic type w/o class info");
    }
    if (paramArrayOfT == null) {}
    for (paramArrayOfT = (Object[])Array.newInstance(paramT.getClass(), 1);; paramArrayOfT = Arrays.copyOf(paramArrayOfT, paramArrayOfT.length + 1))
    {
      paramArrayOfT[(paramArrayOfT.length - 1)] = paramT;
      return paramArrayOfT;
    }
  }
  
  @KeepForSdk
  public static <T> T[] concat(T[]... paramVarArgs)
  {
    if (paramVarArgs.length == 0) {
      return (Object[])Array.newInstance(paramVarArgs.getClass(), 0);
    }
    int i = 0;
    int j = 0;
    while (i < paramVarArgs.length)
    {
      j += paramVarArgs[i].length;
      i += 1;
    }
    Object[] arrayOfObject = Arrays.copyOf(paramVarArgs[0], j);
    j = paramVarArgs[0].length;
    i = 1;
    while (i < paramVarArgs.length)
    {
      T[] arrayOfT = paramVarArgs[i];
      System.arraycopy(arrayOfT, 0, arrayOfObject, j, arrayOfT.length);
      j += arrayOfT.length;
      i += 1;
    }
    return arrayOfObject;
  }
  
  @KeepForSdk
  public static byte[] concatByteArrays(byte[]... paramVarArgs)
  {
    if (paramVarArgs.length == 0) {
      return new byte[0];
    }
    int i = 0;
    int j = 0;
    while (i < paramVarArgs.length)
    {
      j += paramVarArgs[i].length;
      i += 1;
    }
    byte[] arrayOfByte1 = Arrays.copyOf(paramVarArgs[0], j);
    j = paramVarArgs[0].length;
    i = 1;
    while (i < paramVarArgs.length)
    {
      byte[] arrayOfByte2 = paramVarArgs[i];
      System.arraycopy(arrayOfByte2, 0, arrayOfByte1, j, arrayOfByte2.length);
      j += arrayOfByte2.length;
      i += 1;
    }
    return arrayOfByte1;
  }
  
  @KeepForSdk
  public static boolean contains(int[] paramArrayOfInt, int paramInt)
  {
    if (paramArrayOfInt == null) {}
    for (;;)
    {
      return false;
      int j = paramArrayOfInt.length;
      int i = 0;
      while (i < j)
      {
        if (paramArrayOfInt[i] == paramInt) {
          return true;
        }
        i += 1;
      }
    }
  }
  
  @KeepForSdk
  public static <T> boolean contains(T[] paramArrayOfT, T paramT)
  {
    boolean bool = false;
    int j;
    int i;
    if (paramArrayOfT != null)
    {
      j = paramArrayOfT.length;
      i = 0;
      label12:
      if (i >= j) {
        break label49;
      }
      if (!Objects.equal(paramArrayOfT[i], paramT)) {
        break label42;
      }
    }
    for (;;)
    {
      if (i >= 0) {
        bool = true;
      }
      return bool;
      j = 0;
      break;
      label42:
      i += 1;
      break label12;
      label49:
      i = -1;
    }
  }
  
  @KeepForSdk
  public static <T> ArrayList<T> newArrayList()
  {
    return new ArrayList();
  }
  
  @KeepForSdk
  public static <T> T[] removeAll(T[] paramArrayOfT1, T... paramVarArgs)
  {
    int j = 0;
    if (paramArrayOfT1 == null)
    {
      paramArrayOfT1 = null;
      return paramArrayOfT1;
    }
    if ((paramVarArgs == null) || (paramVarArgs.length == 0)) {
      return Arrays.copyOf(paramArrayOfT1, paramArrayOfT1.length);
    }
    Object[] arrayOfObject = (Object[])Array.newInstance(paramVarArgs.getClass().getComponentType(), paramArrayOfT1.length);
    int m;
    int i;
    label57:
    int k;
    T ?;
    if (paramVarArgs.length == 1)
    {
      m = paramArrayOfT1.length;
      j = 0;
      i = 0;
      k = i;
      if (j >= m) {
        break label153;
      }
      ? = paramArrayOfT1[j];
      if (Objects.equal(paramVarArgs[0], ?)) {
        break label182;
      }
      k = i + 1;
      arrayOfObject[i] = ?;
      i = k;
    }
    label109:
    label153:
    label179:
    label182:
    for (;;)
    {
      j += 1;
      break label57;
      m = paramArrayOfT1.length;
      i = 0;
      k = i;
      if (j < m)
      {
        ? = paramArrayOfT1[j];
        if (contains(paramVarArgs, ?)) {
          break label179;
        }
        k = i + 1;
        arrayOfObject[i] = ?;
        i = k;
      }
      for (;;)
      {
        j += 1;
        break label109;
        if (arrayOfObject == null) {
          return null;
        }
        paramArrayOfT1 = arrayOfObject;
        if (k == arrayOfObject.length) {
          break;
        }
        return Arrays.copyOf(arrayOfObject, k);
      }
    }
  }
  
  @KeepForSdk
  public static <T> ArrayList<T> toArrayList(T[] paramArrayOfT)
  {
    int j = paramArrayOfT.length;
    ArrayList localArrayList = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      localArrayList.add(paramArrayOfT[i]);
      i += 1;
    }
    return localArrayList;
  }
  
  @KeepForSdk
  public static int[] toPrimitiveArray(Collection<Integer> paramCollection)
  {
    if ((paramCollection == null) || (paramCollection.size() == 0)) {
      return new int[0];
    }
    int[] arrayOfInt = new int[paramCollection.size()];
    paramCollection = paramCollection.iterator();
    int i = 0;
    while (paramCollection.hasNext())
    {
      arrayOfInt[i] = ((Integer)paramCollection.next()).intValue();
      i += 1;
    }
    return arrayOfInt;
  }
  
  @KeepForSdk
  public static Integer[] toWrapperArray(int[] paramArrayOfInt)
  {
    Object localObject;
    if (paramArrayOfInt == null)
    {
      localObject = null;
      return (Integer[])localObject;
    }
    int j = paramArrayOfInt.length;
    Integer[] arrayOfInteger = new Integer[j];
    int i = 0;
    for (;;)
    {
      localObject = arrayOfInteger;
      if (i >= j) {
        break;
      }
      arrayOfInteger[i] = Integer.valueOf(paramArrayOfInt[i]);
      i += 1;
    }
  }
  
  @KeepForSdk
  public static void writeArray(StringBuilder paramStringBuilder, double[] paramArrayOfDouble)
  {
    int j = paramArrayOfDouble.length;
    int i = 0;
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(",");
      }
      paramStringBuilder.append(Double.toString(paramArrayOfDouble[i]));
      i += 1;
    }
  }
  
  @KeepForSdk
  public static void writeArray(StringBuilder paramStringBuilder, float[] paramArrayOfFloat)
  {
    int j = paramArrayOfFloat.length;
    int i = 0;
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(",");
      }
      paramStringBuilder.append(Float.toString(paramArrayOfFloat[i]));
      i += 1;
    }
  }
  
  @KeepForSdk
  public static void writeArray(StringBuilder paramStringBuilder, int[] paramArrayOfInt)
  {
    int j = paramArrayOfInt.length;
    int i = 0;
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(",");
      }
      paramStringBuilder.append(Integer.toString(paramArrayOfInt[i]));
      i += 1;
    }
  }
  
  @KeepForSdk
  public static void writeArray(StringBuilder paramStringBuilder, long[] paramArrayOfLong)
  {
    int j = paramArrayOfLong.length;
    int i = 0;
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(",");
      }
      paramStringBuilder.append(Long.toString(paramArrayOfLong[i]));
      i += 1;
    }
  }
  
  @KeepForSdk
  public static <T> void writeArray(StringBuilder paramStringBuilder, T[] paramArrayOfT)
  {
    int j = paramArrayOfT.length;
    int i = 0;
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(",");
      }
      paramStringBuilder.append(paramArrayOfT[i].toString());
      i += 1;
    }
  }
  
  @KeepForSdk
  public static void writeArray(StringBuilder paramStringBuilder, boolean[] paramArrayOfBoolean)
  {
    int j = paramArrayOfBoolean.length;
    int i = 0;
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(",");
      }
      paramStringBuilder.append(Boolean.toString(paramArrayOfBoolean[i]));
      i += 1;
    }
  }
  
  @KeepForSdk
  public static void writeStringArray(StringBuilder paramStringBuilder, String[] paramArrayOfString)
  {
    int j = paramArrayOfString.length;
    int i = 0;
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(",");
      }
      paramStringBuilder.append("\"").append(paramArrayOfString[i]).append("\"");
      i += 1;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\util\ArrayUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */