package com.google.android.gms.common.util;

import android.os.Looper;

public final class zzc
{
  public static boolean isMainThread()
  {
    return Looper.getMainLooper() == Looper.myLooper();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\util\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */