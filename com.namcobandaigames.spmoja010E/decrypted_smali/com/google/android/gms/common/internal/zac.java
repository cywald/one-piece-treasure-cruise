package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.Intent;

final class zac
  extends DialogRedirect
{
  zac(Intent paramIntent, Activity paramActivity, int paramInt) {}
  
  public final void redirect()
  {
    if (this.zaog != null) {
      this.val$activity.startActivityForResult(this.zaog, this.val$requestCode);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\zac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */