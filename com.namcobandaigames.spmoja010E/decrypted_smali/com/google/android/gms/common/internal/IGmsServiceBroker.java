package com.google.android.gms.common.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.google.android.gms.common.annotation.KeepForSdk;

public abstract interface IGmsServiceBroker
  extends IInterface
{
  @KeepForSdk
  public abstract void getService(IGmsCallbacks paramIGmsCallbacks, GetServiceRequest paramGetServiceRequest)
    throws RemoteException;
  
  public static abstract class Stub
    extends Binder
    implements IGmsServiceBroker
  {
    public Stub()
    {
      attachInterface(this, "com.google.android.gms.common.internal.IGmsServiceBroker");
    }
    
    @KeepForSdk
    public IBinder asBinder()
    {
      return this;
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      if (paramInt1 > 16777215) {
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      }
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      Object localObject = paramParcel1.readStrongBinder();
      if (localObject == null)
      {
        localObject = null;
        if (paramInt1 != 46) {
          break label118;
        }
        if (paramParcel1.readInt() == 0) {
          break label536;
        }
      }
      label118:
      label536:
      for (paramParcel1 = (GetServiceRequest)GetServiceRequest.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
      {
        getService((IGmsCallbacks)localObject, paramParcel1);
        paramParcel2.writeNoException();
        return true;
        IInterface localIInterface = ((IBinder)localObject).queryLocalInterface("com.google.android.gms.common.internal.IGmsCallbacks");
        if ((localIInterface instanceof IGmsCallbacks))
        {
          localObject = (IGmsCallbacks)localIInterface;
          break;
        }
        localObject = new zzl((IBinder)localObject);
        break;
        if (paramInt1 == 47)
        {
          if (paramParcel1.readInt() != 0) {
            zzr.CREATOR.createFromParcel(paramParcel1);
          }
          throw new UnsupportedOperationException();
        }
        paramParcel1.readInt();
        if (paramInt1 != 4) {
          paramParcel1.readString();
        }
        switch (paramInt1)
        {
        }
        for (;;)
        {
          throw new UnsupportedOperationException();
          paramParcel1.readStrongBinder();
          if (paramParcel1.readInt() != 0)
          {
            Bundle.CREATOR.createFromParcel(paramParcel1);
            continue;
            paramParcel1.readString();
            paramParcel1.createStringArray();
            paramParcel1.readString();
            if (paramParcel1.readInt() != 0)
            {
              Bundle.CREATOR.createFromParcel(paramParcel1);
              continue;
              paramParcel1.readString();
              paramParcel1.createStringArray();
              paramParcel1.readString();
              paramParcel1.readStrongBinder();
              paramParcel1.readString();
              if (paramParcel1.readInt() != 0)
              {
                Bundle.CREATOR.createFromParcel(paramParcel1);
                continue;
                paramParcel1.createStringArray();
                paramParcel1.readString();
                if (paramParcel1.readInt() != 0)
                {
                  Bundle.CREATOR.createFromParcel(paramParcel1);
                  continue;
                  paramParcel1.readString();
                  paramParcel1.createStringArray();
                  continue;
                  paramParcel1.readString();
                  continue;
                  if (paramParcel1.readInt() != 0) {
                    Bundle.CREATOR.createFromParcel(paramParcel1);
                  }
                }
              }
            }
          }
        }
      }
    }
    
    private static final class zza
      implements IGmsServiceBroker
    {
      private final IBinder zza;
      
      zza(IBinder paramIBinder)
      {
        this.zza = paramIBinder;
      }
      
      public final IBinder asBinder()
      {
        return this.zza;
      }
      
      /* Error */
      public final void getService(IGmsCallbacks paramIGmsCallbacks, GetServiceRequest paramGetServiceRequest)
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore_3
        //   4: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   7: astore 4
        //   9: aload_3
        //   10: ldc 33
        //   12: invokevirtual 37	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   15: aload_1
        //   16: ifnull +61 -> 77
        //   19: aload_1
        //   20: invokeinterface 41 1 0
        //   25: astore_1
        //   26: aload_3
        //   27: aload_1
        //   28: invokevirtual 44	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
        //   31: aload_2
        //   32: ifnull +50 -> 82
        //   35: aload_3
        //   36: iconst_1
        //   37: invokevirtual 48	android/os/Parcel:writeInt	(I)V
        //   40: aload_2
        //   41: aload_3
        //   42: iconst_0
        //   43: invokevirtual 54	com/google/android/gms/common/internal/GetServiceRequest:writeToParcel	(Landroid/os/Parcel;I)V
        //   46: aload_0
        //   47: getfield 18	com/google/android/gms/common/internal/IGmsServiceBroker$Stub$zza:zza	Landroid/os/IBinder;
        //   50: bipush 46
        //   52: aload_3
        //   53: aload 4
        //   55: iconst_0
        //   56: invokeinterface 60 5 0
        //   61: pop
        //   62: aload 4
        //   64: invokevirtual 63	android/os/Parcel:readException	()V
        //   67: aload 4
        //   69: invokevirtual 66	android/os/Parcel:recycle	()V
        //   72: aload_3
        //   73: invokevirtual 66	android/os/Parcel:recycle	()V
        //   76: return
        //   77: aconst_null
        //   78: astore_1
        //   79: goto -53 -> 26
        //   82: aload_3
        //   83: iconst_0
        //   84: invokevirtual 48	android/os/Parcel:writeInt	(I)V
        //   87: goto -41 -> 46
        //   90: astore_1
        //   91: aload 4
        //   93: invokevirtual 66	android/os/Parcel:recycle	()V
        //   96: aload_3
        //   97: invokevirtual 66	android/os/Parcel:recycle	()V
        //   100: aload_1
        //   101: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	102	0	this	zza
        //   0	102	1	paramIGmsCallbacks	IGmsCallbacks
        //   0	102	2	paramGetServiceRequest	GetServiceRequest
        //   3	94	3	localParcel1	Parcel
        //   7	85	4	localParcel2	Parcel
        // Exception table:
        //   from	to	target	type
        //   9	15	90	finally
        //   19	26	90	finally
        //   26	31	90	finally
        //   35	46	90	finally
        //   46	67	90	finally
        //   82	87	90	finally
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\IGmsServiceBroker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */