package com.google.android.gms.common.internal;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class AccountType
{
  @KeepForSdk
  public static final String GOOGLE = "com.google";
  private static final String[] zzbr = { "com.google", "com.google.work", "cn.google" };
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\AccountType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */