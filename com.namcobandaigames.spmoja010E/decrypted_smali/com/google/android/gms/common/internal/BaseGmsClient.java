package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.BinderThread;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.common.zze;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.concurrent.GuardedBy;

@KeepForSdk
public abstract class BaseGmsClient<T extends IInterface>
{
  @KeepForSdk
  public static final int CONNECT_STATE_CONNECTED = 4;
  @KeepForSdk
  public static final int CONNECT_STATE_DISCONNECTED = 1;
  @KeepForSdk
  public static final int CONNECT_STATE_DISCONNECTING = 5;
  @KeepForSdk
  public static final String DEFAULT_ACCOUNT = "<<default account>>";
  @KeepForSdk
  public static final String[] GOOGLE_PLUS_REQUIRED_FEATURES = { "service_esmobile", "service_googleme" };
  @KeepForSdk
  public static final String KEY_PENDING_INTENT = "pendingIntent";
  private static final Feature[] zzbs = new Feature[0];
  private final Context mContext;
  final Handler mHandler;
  private final Object mLock = new Object();
  private int zzbt;
  private long zzbu;
  private long zzbv;
  private int zzbw;
  private long zzbx;
  @VisibleForTesting
  private zzh zzby;
  private final Looper zzbz;
  private final GmsClientSupervisor zzca;
  private final GoogleApiAvailabilityLight zzcb;
  private final Object zzcc = new Object();
  @GuardedBy("mServiceBrokerLock")
  private IGmsServiceBroker zzcd;
  @VisibleForTesting
  protected ConnectionProgressReportCallbacks zzce;
  @GuardedBy("mLock")
  private T zzcf;
  private final ArrayList<zzc<?>> zzcg = new ArrayList();
  @GuardedBy("mLock")
  private zze zzch;
  @GuardedBy("mLock")
  private int zzci = 1;
  private final BaseConnectionCallbacks zzcj;
  private final BaseOnConnectionFailedListener zzck;
  private final int zzcl;
  private final String zzcm;
  private ConnectionResult zzcn = null;
  private boolean zzco = false;
  private volatile zzb zzcp = null;
  @VisibleForTesting
  protected AtomicInteger zzcq = new AtomicInteger(0);
  
  @KeepForSdk
  @VisibleForTesting
  protected BaseGmsClient(Context paramContext, Handler paramHandler, GmsClientSupervisor paramGmsClientSupervisor, GoogleApiAvailabilityLight paramGoogleApiAvailabilityLight, int paramInt, BaseConnectionCallbacks paramBaseConnectionCallbacks, BaseOnConnectionFailedListener paramBaseOnConnectionFailedListener)
  {
    this.mContext = ((Context)Preconditions.checkNotNull(paramContext, "Context must not be null"));
    this.mHandler = ((Handler)Preconditions.checkNotNull(paramHandler, "Handler must not be null"));
    this.zzbz = paramHandler.getLooper();
    this.zzca = ((GmsClientSupervisor)Preconditions.checkNotNull(paramGmsClientSupervisor, "Supervisor must not be null"));
    this.zzcb = ((GoogleApiAvailabilityLight)Preconditions.checkNotNull(paramGoogleApiAvailabilityLight, "API availability must not be null"));
    this.zzcl = paramInt;
    this.zzcj = paramBaseConnectionCallbacks;
    this.zzck = paramBaseOnConnectionFailedListener;
    this.zzcm = null;
  }
  
  @KeepForSdk
  protected BaseGmsClient(Context paramContext, Looper paramLooper, int paramInt, BaseConnectionCallbacks paramBaseConnectionCallbacks, BaseOnConnectionFailedListener paramBaseOnConnectionFailedListener, String paramString)
  {
    this(paramContext, paramLooper, GmsClientSupervisor.getInstance(paramContext), GoogleApiAvailabilityLight.getInstance(), paramInt, (BaseConnectionCallbacks)Preconditions.checkNotNull(paramBaseConnectionCallbacks), (BaseOnConnectionFailedListener)Preconditions.checkNotNull(paramBaseOnConnectionFailedListener), paramString);
  }
  
  @KeepForSdk
  @VisibleForTesting
  protected BaseGmsClient(Context paramContext, Looper paramLooper, GmsClientSupervisor paramGmsClientSupervisor, GoogleApiAvailabilityLight paramGoogleApiAvailabilityLight, int paramInt, BaseConnectionCallbacks paramBaseConnectionCallbacks, BaseOnConnectionFailedListener paramBaseOnConnectionFailedListener, String paramString)
  {
    this.mContext = ((Context)Preconditions.checkNotNull(paramContext, "Context must not be null"));
    this.zzbz = ((Looper)Preconditions.checkNotNull(paramLooper, "Looper must not be null"));
    this.zzca = ((GmsClientSupervisor)Preconditions.checkNotNull(paramGmsClientSupervisor, "Supervisor must not be null"));
    this.zzcb = ((GoogleApiAvailabilityLight)Preconditions.checkNotNull(paramGoogleApiAvailabilityLight, "API availability must not be null"));
    this.mHandler = new zzb(paramLooper);
    this.zzcl = paramInt;
    this.zzcj = paramBaseConnectionCallbacks;
    this.zzck = paramBaseOnConnectionFailedListener;
    this.zzcm = paramString;
  }
  
  private final void zza(int paramInt, T paramT)
  {
    boolean bool = true;
    int i;
    int j;
    if (paramInt == 4)
    {
      i = 1;
      if (paramT == null) {
        break label523;
      }
      j = 1;
      label17:
      if (i != j) {
        break label529;
      }
    }
    for (;;)
    {
      Preconditions.checkArgument(bool);
      for (;;)
      {
        synchronized (this.mLock)
        {
          this.zzci = paramInt;
          this.zzcf = paramT;
          onSetConnectState(paramInt, paramT);
          switch (paramInt)
          {
          case 2: 
            return;
          case 3: 
            String str1;
            if ((this.zzch != null) && (this.zzby != null))
            {
              paramT = this.zzby.zzt();
              str1 = this.zzby.getPackageName();
              Log.e("GmsClient", String.valueOf(paramT).length() + 70 + String.valueOf(str1).length() + "Calling connect() while still connected, missing disconnect() for " + paramT + " on " + str1);
              this.zzca.zza(this.zzby.zzt(), this.zzby.getPackageName(), this.zzby.zzq(), this.zzch, zzj());
              this.zzcq.incrementAndGet();
            }
            this.zzch = new zze(this.zzcq.get());
            if ((this.zzci == 3) && (getLocalStartServiceAction() != null))
            {
              paramT = new zzh(getContext().getPackageName(), getLocalStartServiceAction(), true, 129);
              this.zzby = paramT;
              paramT = this.zzca;
              str1 = this.zzby.zzt();
              String str2 = this.zzby.getPackageName();
              paramInt = this.zzby.zzq();
              zze localzze = this.zzch;
              String str3 = zzj();
              if (paramT.zza(new GmsClientSupervisor.zza(str1, str2, paramInt), localzze, str3)) {
                continue;
              }
              paramT = this.zzby.zzt();
              str1 = this.zzby.getPackageName();
              Log.e("GmsClient", String.valueOf(paramT).length() + 34 + String.valueOf(str1).length() + "unable to connect to service: " + paramT + " on " + str1);
              zza(16, null, this.zzcq.get());
            }
            break;
          }
        }
        paramT = new zzh(getStartServicePackage(), getStartServiceAction(), false, 129);
        continue;
        onConnectedLocked(paramT);
        continue;
        if (this.zzch != null)
        {
          this.zzca.zza(getStartServiceAction(), getStartServicePackage(), 129, this.zzch, zzj());
          this.zzch = null;
        }
      }
      i = 0;
      break;
      label523:
      j = 0;
      break label17;
      label529:
      bool = false;
    }
  }
  
  private final void zza(zzb paramzzb)
  {
    this.zzcp = paramzzb;
  }
  
  private final boolean zza(int paramInt1, int paramInt2, T paramT)
  {
    synchronized (this.mLock)
    {
      if (this.zzci != paramInt1) {
        return false;
      }
      zza(paramInt2, paramT);
      return true;
    }
  }
  
  private final void zzb(int paramInt)
  {
    if (zzk())
    {
      paramInt = 5;
      this.zzco = true;
    }
    for (;;)
    {
      this.mHandler.sendMessage(this.mHandler.obtainMessage(paramInt, this.zzcq.get(), 16));
      return;
      paramInt = 4;
    }
  }
  
  @Nullable
  private final String zzj()
  {
    if (this.zzcm == null) {
      return this.mContext.getClass().getName();
    }
    return this.zzcm;
  }
  
  private final boolean zzk()
  {
    for (;;)
    {
      synchronized (this.mLock)
      {
        if (this.zzci == 3)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  private final boolean zzl()
  {
    if (this.zzco) {}
    while ((TextUtils.isEmpty(getServiceDescriptor())) || (TextUtils.isEmpty(getLocalStartServiceAction()))) {
      return false;
    }
    try
    {
      Class.forName(getServiceDescriptor());
      return true;
    }
    catch (ClassNotFoundException localClassNotFoundException) {}
    return false;
  }
  
  @KeepForSdk
  public void checkAvailabilityAndConnect()
  {
    int i = this.zzcb.isGooglePlayServicesAvailable(this.mContext, getMinApkVersion());
    if (i != 0)
    {
      zza(1, null);
      triggerNotAvailable(new LegacyClientCallbackAdapter(), i, null);
      return;
    }
    connect(new LegacyClientCallbackAdapter());
  }
  
  @KeepForSdk
  protected final void checkConnected()
  {
    if (!isConnected()) {
      throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
    }
  }
  
  @KeepForSdk
  public void connect(@NonNull ConnectionProgressReportCallbacks paramConnectionProgressReportCallbacks)
  {
    this.zzce = ((ConnectionProgressReportCallbacks)Preconditions.checkNotNull(paramConnectionProgressReportCallbacks, "Connection progress callbacks cannot be null."));
    zza(2, null);
  }
  
  @Nullable
  @KeepForSdk
  protected abstract T createServiceInterface(IBinder paramIBinder);
  
  @KeepForSdk
  public void disconnect()
  {
    this.zzcq.incrementAndGet();
    synchronized (this.zzcg)
    {
      int j = this.zzcg.size();
      int i = 0;
      while (i < j)
      {
        ((zzc)this.zzcg.get(i)).removeListener();
        i += 1;
      }
      this.zzcg.clear();
    }
    synchronized (this.zzcc)
    {
      this.zzcd = null;
      zza(1, null);
      return;
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
  }
  
  @KeepForSdk
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] arg4)
  {
    int i;
    synchronized (this.mLock)
    {
      i = this.zzci;
      paramFileDescriptor = this.zzcf;
    }
    for (;;)
    {
      Object localObject;
      synchronized (this.zzcc)
      {
        localObject = this.zzcd;
        paramPrintWriter.append(paramString).append("mConnectState=");
        switch (i)
        {
        default: 
          paramPrintWriter.print("UNKNOWN");
          paramPrintWriter.append(" mService=");
          if (paramFileDescriptor != null) {
            break label533;
          }
          paramPrintWriter.append("null");
          paramPrintWriter.append(" mServiceBroker=");
          if (localObject != null) {
            break label566;
          }
          paramPrintWriter.println("null");
          paramFileDescriptor = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
          long l;
          if (this.zzbv > 0L)
          {
            ??? = paramPrintWriter.append(paramString).append("lastConnectedTime=");
            l = this.zzbv;
            localObject = paramFileDescriptor.format(new Date(this.zzbv));
            ???.println(String.valueOf(localObject).length() + 21 + l + " " + (String)localObject);
          }
          if (this.zzbu > 0L) {
            paramPrintWriter.append(paramString).append("lastSuspendedCause=");
          }
          switch (this.zzbt)
          {
          default: 
            paramPrintWriter.append(String.valueOf(this.zzbt));
            ??? = paramPrintWriter.append(" lastSuspendedTime=");
            l = this.zzbu;
            localObject = paramFileDescriptor.format(new Date(this.zzbu));
            ???.println(String.valueOf(localObject).length() + 21 + l + " " + (String)localObject);
            if (this.zzbx > 0L)
            {
              paramPrintWriter.append(paramString).append("lastFailedStatus=").append(CommonStatusCodes.getStatusCodeString(this.zzbw));
              paramString = paramPrintWriter.append(" lastFailedTime=");
              l = this.zzbx;
              paramFileDescriptor = paramFileDescriptor.format(new Date(this.zzbx));
              paramString.println(String.valueOf(paramFileDescriptor).length() + 21 + l + " " + paramFileDescriptor);
            }
            return;
            paramString = finally;
            throw paramString;
          }
          break;
        }
      }
      paramPrintWriter.print("REMOTE_CONNECTING");
      continue;
      paramPrintWriter.print("LOCAL_CONNECTING");
      continue;
      paramPrintWriter.print("CONNECTED");
      continue;
      paramPrintWriter.print("DISCONNECTING");
      continue;
      paramPrintWriter.print("DISCONNECTED");
      continue;
      label533:
      paramPrintWriter.append(getServiceDescriptor()).append("@").append(Integer.toHexString(System.identityHashCode(paramFileDescriptor.asBinder())));
      continue;
      label566:
      paramPrintWriter.append("IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(((IGmsServiceBroker)localObject).asBinder())));
      continue;
      paramPrintWriter.append("CAUSE_SERVICE_DISCONNECTED");
      continue;
      paramPrintWriter.append("CAUSE_NETWORK_LOST");
    }
  }
  
  @KeepForSdk
  public Account getAccount()
  {
    return null;
  }
  
  @KeepForSdk
  public Feature[] getApiFeatures()
  {
    return zzbs;
  }
  
  @Nullable
  @KeepForSdk
  public final Feature[] getAvailableFeatures()
  {
    zzb localzzb = this.zzcp;
    if (localzzb == null) {
      return null;
    }
    return localzzb.zzda;
  }
  
  @KeepForSdk
  public Bundle getConnectionHint()
  {
    return null;
  }
  
  @KeepForSdk
  public final Context getContext()
  {
    return this.mContext;
  }
  
  @KeepForSdk
  public String getEndpointPackageName()
  {
    if ((isConnected()) && (this.zzby != null)) {
      return this.zzby.getPackageName();
    }
    throw new RuntimeException("Failed to connect when checking package");
  }
  
  @KeepForSdk
  protected Bundle getGetServiceRequestExtraArgs()
  {
    return new Bundle();
  }
  
  @Nullable
  @KeepForSdk
  protected String getLocalStartServiceAction()
  {
    return null;
  }
  
  @KeepForSdk
  public final Looper getLooper()
  {
    return this.zzbz;
  }
  
  @KeepForSdk
  public int getMinApkVersion()
  {
    return GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
  }
  
  /* Error */
  @android.support.annotation.WorkerThread
  @KeepForSdk
  public void getRemoteService(IAccountAccessor arg1, Set<Scope> paramSet)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 595	com/google/android/gms/common/internal/BaseGmsClient:getGetServiceRequestExtraArgs	()Landroid/os/Bundle;
    //   4: astore 4
    //   6: new 597	com/google/android/gms/common/internal/GetServiceRequest
    //   9: dup
    //   10: aload_0
    //   11: getfield 193	com/google/android/gms/common/internal/BaseGmsClient:zzcl	I
    //   14: invokespecial 598	com/google/android/gms/common/internal/GetServiceRequest:<init>	(I)V
    //   17: astore_3
    //   18: aload_3
    //   19: aload_0
    //   20: getfield 167	com/google/android/gms/common/internal/BaseGmsClient:mContext	Landroid/content/Context;
    //   23: invokevirtual 305	android/content/Context:getPackageName	()Ljava/lang/String;
    //   26: putfield 601	com/google/android/gms/common/internal/GetServiceRequest:zzdh	Ljava/lang/String;
    //   29: aload_3
    //   30: aload 4
    //   32: putfield 605	com/google/android/gms/common/internal/GetServiceRequest:zzdk	Landroid/os/Bundle;
    //   35: aload_2
    //   36: ifnull +25 -> 61
    //   39: aload_3
    //   40: aload_2
    //   41: aload_2
    //   42: invokeinterface 608 1 0
    //   47: anewarray 610	com/google/android/gms/common/api/Scope
    //   50: invokeinterface 614 2 0
    //   55: checkcast 616	[Lcom/google/android/gms/common/api/Scope;
    //   58: putfield 619	com/google/android/gms/common/internal/GetServiceRequest:zzdj	[Lcom/google/android/gms/common/api/Scope;
    //   61: aload_0
    //   62: invokevirtual 622	com/google/android/gms/common/internal/BaseGmsClient:requiresSignIn	()Z
    //   65: ifeq +107 -> 172
    //   68: aload_0
    //   69: invokevirtual 624	com/google/android/gms/common/internal/BaseGmsClient:getAccount	()Landroid/accounts/Account;
    //   72: ifnull +84 -> 156
    //   75: aload_0
    //   76: invokevirtual 624	com/google/android/gms/common/internal/BaseGmsClient:getAccount	()Landroid/accounts/Account;
    //   79: astore_2
    //   80: aload_3
    //   81: aload_2
    //   82: putfield 628	com/google/android/gms/common/internal/GetServiceRequest:zzdl	Landroid/accounts/Account;
    //   85: aload_1
    //   86: ifnull +13 -> 99
    //   89: aload_3
    //   90: aload_1
    //   91: invokeinterface 631 1 0
    //   96: putfield 635	com/google/android/gms/common/internal/GetServiceRequest:zzdi	Landroid/os/IBinder;
    //   99: aload_3
    //   100: getstatic 118	com/google/android/gms/common/internal/BaseGmsClient:zzbs	[Lcom/google/android/gms/common/Feature;
    //   103: putfield 638	com/google/android/gms/common/internal/GetServiceRequest:zzdm	[Lcom/google/android/gms/common/Feature;
    //   106: aload_3
    //   107: aload_0
    //   108: invokevirtual 640	com/google/android/gms/common/internal/BaseGmsClient:getApiFeatures	()[Lcom/google/android/gms/common/Feature;
    //   111: putfield 643	com/google/android/gms/common/internal/GetServiceRequest:zzdn	[Lcom/google/android/gms/common/Feature;
    //   114: aload_0
    //   115: getfield 135	com/google/android/gms/common/internal/BaseGmsClient:zzcc	Ljava/lang/Object;
    //   118: astore_1
    //   119: aload_1
    //   120: monitorenter
    //   121: aload_0
    //   122: getfield 225	com/google/android/gms/common/internal/BaseGmsClient:zzcd	Lcom/google/android/gms/common/internal/IGmsServiceBroker;
    //   125: ifnull +65 -> 190
    //   128: aload_0
    //   129: getfield 225	com/google/android/gms/common/internal/BaseGmsClient:zzcd	Lcom/google/android/gms/common/internal/IGmsServiceBroker;
    //   132: new 31	com/google/android/gms/common/internal/BaseGmsClient$zzd
    //   135: dup
    //   136: aload_0
    //   137: aload_0
    //   138: getfield 155	com/google/android/gms/common/internal/BaseGmsClient:zzcq	Ljava/util/concurrent/atomic/AtomicInteger;
    //   141: invokevirtual 294	java/util/concurrent/atomic/AtomicInteger:get	()I
    //   144: invokespecial 644	com/google/android/gms/common/internal/BaseGmsClient$zzd:<init>	(Lcom/google/android/gms/common/internal/BaseGmsClient;I)V
    //   147: aload_3
    //   148: invokeinterface 648 3 0
    //   153: aload_1
    //   154: monitorexit
    //   155: return
    //   156: new 650	android/accounts/Account
    //   159: dup
    //   160: ldc 53
    //   162: ldc_w 652
    //   165: invokespecial 655	android/accounts/Account:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   168: astore_2
    //   169: goto -89 -> 80
    //   172: aload_0
    //   173: invokevirtual 658	com/google/android/gms/common/internal/BaseGmsClient:requiresAccount	()Z
    //   176: ifeq -77 -> 99
    //   179: aload_3
    //   180: aload_0
    //   181: invokevirtual 624	com/google/android/gms/common/internal/BaseGmsClient:getAccount	()Landroid/accounts/Account;
    //   184: putfield 628	com/google/android/gms/common/internal/GetServiceRequest:zzdl	Landroid/accounts/Account;
    //   187: goto -88 -> 99
    //   190: ldc -5
    //   192: ldc_w 660
    //   195: invokestatic 663	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   198: pop
    //   199: goto -46 -> 153
    //   202: astore_2
    //   203: aload_1
    //   204: monitorexit
    //   205: aload_2
    //   206: athrow
    //   207: astore_1
    //   208: ldc -5
    //   210: ldc_w 665
    //   213: aload_1
    //   214: invokestatic 668	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   217: pop
    //   218: aload_0
    //   219: iconst_1
    //   220: invokevirtual 671	com/google/android/gms/common/internal/BaseGmsClient:triggerConnectionSuspended	(I)V
    //   223: return
    //   224: astore_1
    //   225: aload_1
    //   226: athrow
    //   227: astore_1
    //   228: ldc -5
    //   230: ldc_w 665
    //   233: aload_1
    //   234: invokestatic 668	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   237: pop
    //   238: aload_0
    //   239: bipush 8
    //   241: aconst_null
    //   242: aconst_null
    //   243: aload_0
    //   244: getfield 155	com/google/android/gms/common/internal/BaseGmsClient:zzcq	Ljava/util/concurrent/atomic/AtomicInteger;
    //   247: invokevirtual 294	java/util/concurrent/atomic/AtomicInteger:get	()I
    //   250: invokevirtual 675	com/google/android/gms/common/internal/BaseGmsClient:onPostInitHandler	(ILandroid/os/IBinder;Landroid/os/Bundle;I)V
    //   253: return
    //   254: astore_1
    //   255: goto -27 -> 228
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	258	0	this	BaseGmsClient
    //   0	258	2	paramSet	Set<Scope>
    //   17	163	3	localGetServiceRequest	GetServiceRequest
    //   4	27	4	localBundle	Bundle
    // Exception table:
    //   from	to	target	type
    //   121	153	202	finally
    //   153	155	202	finally
    //   190	199	202	finally
    //   203	205	202	finally
    //   114	121	207	android/os/DeadObjectException
    //   205	207	207	android/os/DeadObjectException
    //   114	121	224	java/lang/SecurityException
    //   205	207	224	java/lang/SecurityException
    //   114	121	227	android/os/RemoteException
    //   205	207	227	android/os/RemoteException
    //   114	121	254	java/lang/RuntimeException
    //   205	207	254	java/lang/RuntimeException
  }
  
  @KeepForSdk
  protected Set<Scope> getScopes()
  {
    return Collections.EMPTY_SET;
  }
  
  @KeepForSdk
  public final T getService()
    throws DeadObjectException
  {
    synchronized (this.mLock)
    {
      if (this.zzci == 5) {
        throw new DeadObjectException();
      }
    }
    checkConnected();
    if (this.zzcf != null) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "Client is connected but service is null");
      IInterface localIInterface = this.zzcf;
      return localIInterface;
    }
  }
  
  @Nullable
  @KeepForSdk
  public IBinder getServiceBrokerBinder()
  {
    synchronized (this.zzcc)
    {
      if (this.zzcd == null) {
        return null;
      }
      IBinder localIBinder = this.zzcd.asBinder();
      return localIBinder;
    }
  }
  
  @NonNull
  @KeepForSdk
  protected abstract String getServiceDescriptor();
  
  @KeepForSdk
  public Intent getSignInIntent()
  {
    throw new UnsupportedOperationException("Not a sign in API");
  }
  
  @NonNull
  @KeepForSdk
  protected abstract String getStartServiceAction();
  
  @KeepForSdk
  protected String getStartServicePackage()
  {
    return "com.google.android.gms";
  }
  
  @KeepForSdk
  public boolean isConnected()
  {
    for (;;)
    {
      synchronized (this.mLock)
      {
        if (this.zzci == 4)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  @KeepForSdk
  public boolean isConnecting()
  {
    for (;;)
    {
      synchronized (this.mLock)
      {
        if (this.zzci != 2)
        {
          if (this.zzci != 3) {
            break label40;
          }
          break label35;
          return bool;
        }
      }
      label35:
      boolean bool = true;
      continue;
      label40:
      bool = false;
    }
  }
  
  @CallSuper
  @KeepForSdk
  protected void onConnectedLocked(@NonNull T paramT)
  {
    this.zzbv = System.currentTimeMillis();
  }
  
  @CallSuper
  @KeepForSdk
  protected void onConnectionFailed(ConnectionResult paramConnectionResult)
  {
    this.zzbw = paramConnectionResult.getErrorCode();
    this.zzbx = System.currentTimeMillis();
  }
  
  @CallSuper
  @KeepForSdk
  protected void onConnectionSuspended(int paramInt)
  {
    this.zzbt = paramInt;
    this.zzbu = System.currentTimeMillis();
  }
  
  @KeepForSdk
  protected void onPostInitHandler(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    this.mHandler.sendMessage(this.mHandler.obtainMessage(1, paramInt2, -1, new zzf(paramInt1, paramIBinder, paramBundle)));
  }
  
  @KeepForSdk
  void onSetConnectState(int paramInt, T paramT) {}
  
  @KeepForSdk
  public void onUserSignOut(@NonNull SignOutCallbacks paramSignOutCallbacks)
  {
    paramSignOutCallbacks.onSignOutComplete();
  }
  
  @KeepForSdk
  public boolean providesSignIn()
  {
    return false;
  }
  
  @KeepForSdk
  public boolean requiresAccount()
  {
    return false;
  }
  
  @KeepForSdk
  public boolean requiresGooglePlayServices()
  {
    return true;
  }
  
  @KeepForSdk
  public boolean requiresSignIn()
  {
    return false;
  }
  
  @KeepForSdk
  public void triggerConnectionSuspended(int paramInt)
  {
    this.mHandler.sendMessage(this.mHandler.obtainMessage(6, this.zzcq.get(), paramInt));
  }
  
  @KeepForSdk
  @VisibleForTesting
  protected void triggerNotAvailable(@NonNull ConnectionProgressReportCallbacks paramConnectionProgressReportCallbacks, int paramInt, @Nullable PendingIntent paramPendingIntent)
  {
    this.zzce = ((ConnectionProgressReportCallbacks)Preconditions.checkNotNull(paramConnectionProgressReportCallbacks, "Connection progress callbacks cannot be null."));
    this.mHandler.sendMessage(this.mHandler.obtainMessage(3, this.zzcq.get(), paramInt, paramPendingIntent));
  }
  
  protected final void zza(int paramInt1, @Nullable Bundle paramBundle, int paramInt2)
  {
    this.mHandler.sendMessage(this.mHandler.obtainMessage(7, paramInt2, -1, new zzg(paramInt1, null)));
  }
  
  @KeepForSdk
  public static abstract interface BaseConnectionCallbacks
  {
    @KeepForSdk
    public abstract void onConnected(@Nullable Bundle paramBundle);
    
    @KeepForSdk
    public abstract void onConnectionSuspended(int paramInt);
  }
  
  @KeepForSdk
  public static abstract interface BaseOnConnectionFailedListener
  {
    public abstract void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult);
  }
  
  @KeepForSdk
  public static abstract interface ConnectionProgressReportCallbacks
  {
    @KeepForSdk
    public abstract void onReportServiceBinding(@NonNull ConnectionResult paramConnectionResult);
  }
  
  protected class LegacyClientCallbackAdapter
    implements BaseGmsClient.ConnectionProgressReportCallbacks
  {
    @KeepForSdk
    public LegacyClientCallbackAdapter() {}
    
    public void onReportServiceBinding(@NonNull ConnectionResult paramConnectionResult)
    {
      if (paramConnectionResult.isSuccess()) {
        BaseGmsClient.this.getRemoteService(null, BaseGmsClient.this.getScopes());
      }
      while (BaseGmsClient.zzg(BaseGmsClient.this) == null) {
        return;
      }
      BaseGmsClient.zzg(BaseGmsClient.this).onConnectionFailed(paramConnectionResult);
    }
  }
  
  @KeepForSdk
  public static abstract interface SignOutCallbacks
  {
    @KeepForSdk
    public abstract void onSignOutComplete();
  }
  
  private abstract class zza
    extends BaseGmsClient.zzc<Boolean>
  {
    private final int statusCode;
    private final Bundle zzcr;
    
    @BinderThread
    protected zza(int paramInt, Bundle paramBundle)
    {
      super(Boolean.valueOf(true));
      this.statusCode = paramInt;
      this.zzcr = paramBundle;
    }
    
    protected abstract void zza(ConnectionResult paramConnectionResult);
    
    protected abstract boolean zzm();
    
    protected final void zzn() {}
  }
  
  final class zzb
    extends zze
  {
    public zzb(Looper paramLooper)
    {
      super();
    }
    
    private static void zza(Message paramMessage)
    {
      paramMessage = (BaseGmsClient.zzc)paramMessage.obj;
      paramMessage.zzn();
      paramMessage.unregister();
    }
    
    private static boolean zzb(Message paramMessage)
    {
      return (paramMessage.what == 2) || (paramMessage.what == 1) || (paramMessage.what == 7);
    }
    
    public final void handleMessage(Message paramMessage)
    {
      PendingIntent localPendingIntent = null;
      if (BaseGmsClient.this.zzcq.get() != paramMessage.arg1)
      {
        if (zzb(paramMessage)) {
          zza(paramMessage);
        }
        return;
      }
      if (((paramMessage.what == 1) || (paramMessage.what == 7) || (paramMessage.what == 4) || (paramMessage.what == 5)) && (!BaseGmsClient.this.isConnecting()))
      {
        zza(paramMessage);
        return;
      }
      if (paramMessage.what == 4)
      {
        BaseGmsClient.zza(BaseGmsClient.this, new ConnectionResult(paramMessage.arg2));
        if ((BaseGmsClient.zzb(BaseGmsClient.this)) && (!BaseGmsClient.zzc(BaseGmsClient.this)))
        {
          BaseGmsClient.zza(BaseGmsClient.this, 3, null);
          return;
        }
        if (BaseGmsClient.zzd(BaseGmsClient.this) != null) {}
        for (paramMessage = BaseGmsClient.zzd(BaseGmsClient.this);; paramMessage = new ConnectionResult(8))
        {
          BaseGmsClient.this.zzce.onReportServiceBinding(paramMessage);
          BaseGmsClient.this.onConnectionFailed(paramMessage);
          return;
        }
      }
      if (paramMessage.what == 5)
      {
        if (BaseGmsClient.zzd(BaseGmsClient.this) != null) {}
        for (paramMessage = BaseGmsClient.zzd(BaseGmsClient.this);; paramMessage = new ConnectionResult(8))
        {
          BaseGmsClient.this.zzce.onReportServiceBinding(paramMessage);
          BaseGmsClient.this.onConnectionFailed(paramMessage);
          return;
        }
      }
      if (paramMessage.what == 3)
      {
        if ((paramMessage.obj instanceof PendingIntent)) {
          localPendingIntent = (PendingIntent)paramMessage.obj;
        }
        paramMessage = new ConnectionResult(paramMessage.arg2, localPendingIntent);
        BaseGmsClient.this.zzce.onReportServiceBinding(paramMessage);
        BaseGmsClient.this.onConnectionFailed(paramMessage);
        return;
      }
      if (paramMessage.what == 6)
      {
        BaseGmsClient.zza(BaseGmsClient.this, 5, null);
        if (BaseGmsClient.zze(BaseGmsClient.this) != null) {
          BaseGmsClient.zze(BaseGmsClient.this).onConnectionSuspended(paramMessage.arg2);
        }
        BaseGmsClient.this.onConnectionSuspended(paramMessage.arg2);
        BaseGmsClient.zza(BaseGmsClient.this, 5, 1, null);
        return;
      }
      if ((paramMessage.what == 2) && (!BaseGmsClient.this.isConnected()))
      {
        zza(paramMessage);
        return;
      }
      if (zzb(paramMessage))
      {
        ((BaseGmsClient.zzc)paramMessage.obj).zzo();
        return;
      }
      int i = paramMessage.what;
      Log.wtf("GmsClient", 45 + "Don't know how to handle message: " + i, new Exception());
    }
  }
  
  protected abstract class zzc<TListener>
  {
    private TListener zzct;
    private boolean zzcu;
    
    public zzc()
    {
      Object localObject;
      this.zzct = localObject;
      this.zzcu = false;
    }
    
    public final void removeListener()
    {
      try
      {
        this.zzct = null;
        return;
      }
      finally {}
    }
    
    public final void unregister()
    {
      removeListener();
      synchronized (BaseGmsClient.zzf(BaseGmsClient.this))
      {
        BaseGmsClient.zzf(BaseGmsClient.this).remove(this);
        return;
      }
    }
    
    protected abstract void zza(TListener paramTListener);
    
    protected abstract void zzn();
    
    public final void zzo()
    {
      for (;;)
      {
        try
        {
          Object localObject1 = this.zzct;
          if (this.zzcu)
          {
            String str = String.valueOf(this);
            Log.w("GmsClient", String.valueOf(str).length() + 47 + "Callback proxy " + str + " being reused. This is not safe.");
          }
          if (localObject1 != null) {}
          zzn();
        }
        finally
        {
          try
          {
            zza(localObject1);
          }
          catch (RuntimeException localRuntimeException)
          {
            zzn();
            throw localRuntimeException;
          }
          try
          {
            this.zzcu = true;
            unregister();
            return;
          }
          finally {}
          localObject2 = finally;
        }
      }
    }
  }
  
  @VisibleForTesting
  public static final class zzd
    extends IGmsCallbacks.zza
  {
    private BaseGmsClient zzcv;
    private final int zzcw;
    
    public zzd(@NonNull BaseGmsClient paramBaseGmsClient, int paramInt)
    {
      this.zzcv = paramBaseGmsClient;
      this.zzcw = paramInt;
    }
    
    @BinderThread
    public final void onPostInitComplete(int paramInt, @NonNull IBinder paramIBinder, @Nullable Bundle paramBundle)
    {
      Preconditions.checkNotNull(this.zzcv, "onPostInitComplete can be called only once per call to getRemoteService");
      this.zzcv.onPostInitHandler(paramInt, paramIBinder, paramBundle, this.zzcw);
      this.zzcv = null;
    }
    
    @BinderThread
    public final void zza(int paramInt, @Nullable Bundle paramBundle)
    {
      Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
    }
    
    @BinderThread
    public final void zza(int paramInt, @NonNull IBinder paramIBinder, @NonNull zzb paramzzb)
    {
      Preconditions.checkNotNull(this.zzcv, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
      Preconditions.checkNotNull(paramzzb);
      BaseGmsClient.zza(this.zzcv, paramzzb);
      onPostInitComplete(paramInt, paramIBinder, paramzzb.zzcz);
    }
  }
  
  @VisibleForTesting
  public final class zze
    implements ServiceConnection
  {
    private final int zzcw;
    
    public zze(int paramInt)
    {
      this.zzcw = paramInt;
    }
    
    public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
    {
      if (paramIBinder == null)
      {
        BaseGmsClient.zza(BaseGmsClient.this, 16);
        return;
      }
      synchronized (BaseGmsClient.zza(BaseGmsClient.this))
      {
        BaseGmsClient localBaseGmsClient = BaseGmsClient.this;
        if (paramIBinder == null) {}
        for (paramComponentName = null;; paramComponentName = (IGmsServiceBroker)paramComponentName)
        {
          BaseGmsClient.zza(localBaseGmsClient, paramComponentName);
          BaseGmsClient.this.zza(0, null, this.zzcw);
          return;
          paramComponentName = paramIBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
          if ((paramComponentName == null) || (!(paramComponentName instanceof IGmsServiceBroker))) {
            break;
          }
        }
        paramComponentName = new IGmsServiceBroker.Stub.zza(paramIBinder);
      }
    }
    
    public final void onServiceDisconnected(ComponentName arg1)
    {
      synchronized (BaseGmsClient.zza(BaseGmsClient.this))
      {
        BaseGmsClient.zza(BaseGmsClient.this, null);
        BaseGmsClient.this.mHandler.sendMessage(BaseGmsClient.this.mHandler.obtainMessage(6, this.zzcw, 1));
        return;
      }
    }
  }
  
  protected final class zzf
    extends BaseGmsClient.zza
  {
    private final IBinder zzcx;
    
    @BinderThread
    public zzf(int paramInt, IBinder paramIBinder, Bundle paramBundle)
    {
      super(paramInt, paramBundle);
      this.zzcx = paramIBinder;
    }
    
    protected final void zza(ConnectionResult paramConnectionResult)
    {
      if (BaseGmsClient.zzg(BaseGmsClient.this) != null) {
        BaseGmsClient.zzg(BaseGmsClient.this).onConnectionFailed(paramConnectionResult);
      }
      BaseGmsClient.this.onConnectionFailed(paramConnectionResult);
    }
    
    protected final boolean zzm()
    {
      do
      {
        try
        {
          String str1 = this.zzcx.getInterfaceDescriptor();
          if (!BaseGmsClient.this.getServiceDescriptor().equals(str1))
          {
            String str2 = BaseGmsClient.this.getServiceDescriptor();
            Log.e("GmsClient", String.valueOf(str2).length() + 34 + String.valueOf(str1).length() + "service descriptor mismatch: " + str2 + " vs. " + str1);
            return false;
          }
        }
        catch (RemoteException localRemoteException)
        {
          Log.w("GmsClient", "service probably died");
          return false;
        }
        localObject = BaseGmsClient.this.createServiceInterface(this.zzcx);
      } while ((localObject == null) || ((!BaseGmsClient.zza(BaseGmsClient.this, 2, 4, (IInterface)localObject)) && (!BaseGmsClient.zza(BaseGmsClient.this, 3, 4, (IInterface)localObject))));
      BaseGmsClient.zza(BaseGmsClient.this, null);
      Object localObject = BaseGmsClient.this.getConnectionHint();
      if (BaseGmsClient.zze(BaseGmsClient.this) != null) {
        BaseGmsClient.zze(BaseGmsClient.this).onConnected((Bundle)localObject);
      }
      return true;
    }
  }
  
  protected final class zzg
    extends BaseGmsClient.zza
  {
    @BinderThread
    public zzg(@Nullable int paramInt, Bundle paramBundle)
    {
      super(paramInt, null);
    }
    
    protected final void zza(ConnectionResult paramConnectionResult)
    {
      BaseGmsClient.this.zzce.onReportServiceBinding(paramConnectionResult);
      BaseGmsClient.this.onConnectionFailed(paramConnectionResult);
    }
    
    protected final boolean zzm()
    {
      BaseGmsClient.this.zzce.onReportServiceBinding(ConnectionResult.RESULT_SUCCESS);
      return true;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\BaseGmsClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */