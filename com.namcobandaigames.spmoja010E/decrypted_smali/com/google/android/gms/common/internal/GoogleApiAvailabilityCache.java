package com.google.android.gms.common.internal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.SparseIntArray;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api.Client;

public class GoogleApiAvailabilityCache
{
  private final SparseIntArray zaor = new SparseIntArray();
  private GoogleApiAvailabilityLight zaos;
  
  public GoogleApiAvailabilityCache()
  {
    this(GoogleApiAvailability.getInstance());
  }
  
  public GoogleApiAvailabilityCache(@NonNull GoogleApiAvailabilityLight paramGoogleApiAvailabilityLight)
  {
    Preconditions.checkNotNull(paramGoogleApiAvailabilityLight);
    this.zaos = paramGoogleApiAvailabilityLight;
  }
  
  public void flush()
  {
    this.zaor.clear();
  }
  
  public int getClientAvailability(@NonNull Context paramContext, @NonNull Api.Client paramClient)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramClient);
    if (!paramClient.requiresGooglePlayServices()) {
      return 0;
    }
    int k = paramClient.getMinApkVersion();
    int j = this.zaor.get(k, -1);
    if (j != -1) {
      return j;
    }
    int i = 0;
    if (i < this.zaor.size())
    {
      int m = this.zaor.keyAt(i);
      if ((m <= k) || (this.zaor.get(m) != 0)) {}
    }
    for (i = 0;; i = j)
    {
      j = i;
      if (i == -1) {
        j = this.zaos.isGooglePlayServicesAvailable(paramContext, k);
      }
      this.zaor.put(k, j);
      return j;
      i += 1;
      break;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\GoogleApiAvailabilityCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */