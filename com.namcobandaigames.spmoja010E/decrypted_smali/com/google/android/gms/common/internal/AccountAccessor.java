package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;

public class AccountAccessor
  extends IAccountAccessor.Stub
{
  @KeepForSdk
  public static Account getAccountBinderSafe(IAccountAccessor paramIAccountAccessor)
  {
    Account localAccount = null;
    long l;
    if (paramIAccountAccessor != null) {
      l = Binder.clearCallingIdentity();
    }
    try
    {
      localAccount = paramIAccountAccessor.getAccount();
      return localAccount;
    }
    catch (RemoteException paramIAccountAccessor)
    {
      Log.w("AccountAccessor", "Remote account accessor probably died");
      return null;
    }
    finally
    {
      Binder.restoreCallingIdentity(l);
    }
  }
  
  public boolean equals(Object paramObject)
  {
    throw new NoSuchMethodError();
  }
  
  public final Account getAccount()
  {
    throw new NoSuchMethodError();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\AccountAccessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */