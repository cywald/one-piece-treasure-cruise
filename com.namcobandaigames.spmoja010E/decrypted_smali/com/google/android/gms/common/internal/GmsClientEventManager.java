package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.base.zal;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public final class GmsClientEventManager
  implements Handler.Callback
{
  private final Handler mHandler;
  private final Object mLock = new Object();
  private final GmsClientEventState zaok;
  private final ArrayList<GoogleApiClient.ConnectionCallbacks> zaol = new ArrayList();
  @VisibleForTesting
  private final ArrayList<GoogleApiClient.ConnectionCallbacks> zaom = new ArrayList();
  private final ArrayList<GoogleApiClient.OnConnectionFailedListener> zaon = new ArrayList();
  private volatile boolean zaoo = false;
  private final AtomicInteger zaop = new AtomicInteger(0);
  private boolean zaoq = false;
  
  public GmsClientEventManager(Looper paramLooper, GmsClientEventState paramGmsClientEventState)
  {
    this.zaok = paramGmsClientEventState;
    this.mHandler = new zal(paramLooper, this);
  }
  
  public final boolean areCallbacksEnabled()
  {
    return this.zaoo;
  }
  
  public final void disableCallbacks()
  {
    this.zaoo = false;
    this.zaop.incrementAndGet();
  }
  
  public final void enableCallbacks()
  {
    this.zaoo = true;
  }
  
  public final boolean handleMessage(Message arg1)
  {
    if (???.what == 1)
    {
      GoogleApiClient.ConnectionCallbacks localConnectionCallbacks = (GoogleApiClient.ConnectionCallbacks)???.obj;
      synchronized (this.mLock)
      {
        if ((this.zaoo) && (this.zaok.isConnected()) && (this.zaol.contains(localConnectionCallbacks))) {
          localConnectionCallbacks.onConnected(this.zaok.getConnectionHint());
        }
        return true;
      }
    }
    int i = ???.what;
    Log.wtf("GmsClientEvents", 45 + "Don't know how to handle message: " + i, new Exception());
    return false;
  }
  
  public final boolean isConnectionCallbacksRegistered(GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    Preconditions.checkNotNull(paramConnectionCallbacks);
    synchronized (this.mLock)
    {
      boolean bool = this.zaol.contains(paramConnectionCallbacks);
      return bool;
    }
  }
  
  public final boolean isConnectionFailedListenerRegistered(GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    Preconditions.checkNotNull(paramOnConnectionFailedListener);
    synchronized (this.mLock)
    {
      boolean bool = this.zaon.contains(paramOnConnectionFailedListener);
      return bool;
    }
  }
  
  @VisibleForTesting
  public final void onConnectionFailure(ConnectionResult paramConnectionResult)
  {
    int i = 0;
    if (Looper.myLooper() == this.mHandler.getLooper()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "onConnectionFailure must only be called on the Handler thread");
      this.mHandler.removeMessages(1);
      synchronized (this.mLock)
      {
        ArrayList localArrayList = new ArrayList(this.zaon);
        int k = this.zaop.get();
        localArrayList = (ArrayList)localArrayList;
        int m = localArrayList.size();
        while (i < m)
        {
          Object localObject2 = localArrayList.get(i);
          int j = i + 1;
          localObject2 = (GoogleApiClient.OnConnectionFailedListener)localObject2;
          if ((!this.zaoo) || (this.zaop.get() != k)) {
            return;
          }
          i = j;
          if (this.zaon.contains(localObject2))
          {
            ((GoogleApiClient.OnConnectionFailedListener)localObject2).onConnectionFailed(paramConnectionResult);
            i = j;
          }
        }
      }
      return;
    }
  }
  
  @VisibleForTesting
  protected final void onConnectionSuccess()
  {
    synchronized (this.mLock)
    {
      onConnectionSuccess(this.zaok.getConnectionHint());
      return;
    }
  }
  
  @VisibleForTesting
  public final void onConnectionSuccess(Bundle paramBundle)
  {
    boolean bool2 = true;
    int i = 0;
    boolean bool1;
    if (Looper.myLooper() == this.mHandler.getLooper())
    {
      bool1 = true;
      Preconditions.checkState(bool1, "onConnectionSuccess must only be called on the Handler thread");
    }
    for (;;)
    {
      synchronized (this.mLock)
      {
        if (this.zaoq) {
          break label215;
        }
        bool1 = true;
        Preconditions.checkState(bool1);
        this.mHandler.removeMessages(1);
        this.zaoq = true;
        if (this.zaom.size() != 0) {
          break label221;
        }
        bool1 = bool2;
        Preconditions.checkState(bool1);
        ArrayList localArrayList = new ArrayList(this.zaol);
        int k = this.zaop.get();
        localArrayList = (ArrayList)localArrayList;
        int m = localArrayList.size();
        if (i >= m) {
          break label227;
        }
        Object localObject2 = localArrayList.get(i);
        int j = i + 1;
        localObject2 = (GoogleApiClient.ConnectionCallbacks)localObject2;
        if ((!this.zaoo) || (!this.zaok.isConnected()) || (this.zaop.get() != k)) {
          break label227;
        }
        i = j;
        if (this.zaom.contains(localObject2)) {
          continue;
        }
        ((GoogleApiClient.ConnectionCallbacks)localObject2).onConnected(paramBundle);
        i = j;
      }
      bool1 = false;
      break;
      label215:
      bool1 = false;
      continue;
      label221:
      bool1 = false;
    }
    label227:
    this.zaom.clear();
    this.zaoq = false;
  }
  
  @VisibleForTesting
  public final void onUnintentionalDisconnection(int paramInt)
  {
    int i = 0;
    if (Looper.myLooper() == this.mHandler.getLooper()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "onUnintentionalDisconnection must only be called on the Handler thread");
      this.mHandler.removeMessages(1);
      synchronized (this.mLock)
      {
        this.zaoq = true;
        ArrayList localArrayList = new ArrayList(this.zaol);
        int k = this.zaop.get();
        localArrayList = (ArrayList)localArrayList;
        int m = localArrayList.size();
        Object localObject3;
        int j;
        do
        {
          if (i >= m) {
            break;
          }
          localObject3 = localArrayList.get(i);
          j = i + 1;
          localObject3 = (GoogleApiClient.ConnectionCallbacks)localObject3;
          if ((!this.zaoo) || (this.zaop.get() != k)) {
            break;
          }
          i = j;
        } while (!this.zaol.contains(localObject3));
        ((GoogleApiClient.ConnectionCallbacks)localObject3).onConnectionSuspended(paramInt);
        i = j;
      }
    }
    this.zaom.clear();
    this.zaoq = false;
  }
  
  public final void registerConnectionCallbacks(GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    Preconditions.checkNotNull(paramConnectionCallbacks);
    synchronized (this.mLock)
    {
      if (this.zaol.contains(paramConnectionCallbacks))
      {
        String str = String.valueOf(paramConnectionCallbacks);
        Log.w("GmsClientEvents", String.valueOf(str).length() + 62 + "registerConnectionCallbacks(): listener " + str + " is already registered");
        if (this.zaok.isConnected()) {
          this.mHandler.sendMessage(this.mHandler.obtainMessage(1, paramConnectionCallbacks));
        }
        return;
      }
      this.zaol.add(paramConnectionCallbacks);
    }
  }
  
  public final void registerConnectionFailedListener(GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    Preconditions.checkNotNull(paramOnConnectionFailedListener);
    synchronized (this.mLock)
    {
      if (this.zaon.contains(paramOnConnectionFailedListener))
      {
        paramOnConnectionFailedListener = String.valueOf(paramOnConnectionFailedListener);
        Log.w("GmsClientEvents", String.valueOf(paramOnConnectionFailedListener).length() + 67 + "registerConnectionFailedListener(): listener " + paramOnConnectionFailedListener + " is already registered");
        return;
      }
      this.zaon.add(paramOnConnectionFailedListener);
    }
  }
  
  public final void unregisterConnectionCallbacks(GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    Preconditions.checkNotNull(paramConnectionCallbacks);
    synchronized (this.mLock)
    {
      if (!this.zaol.remove(paramConnectionCallbacks))
      {
        paramConnectionCallbacks = String.valueOf(paramConnectionCallbacks);
        Log.w("GmsClientEvents", String.valueOf(paramConnectionCallbacks).length() + 52 + "unregisterConnectionCallbacks(): listener " + paramConnectionCallbacks + " not found");
      }
      while (!this.zaoq) {
        return;
      }
      this.zaom.add(paramConnectionCallbacks);
    }
  }
  
  public final void unregisterConnectionFailedListener(GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    Preconditions.checkNotNull(paramOnConnectionFailedListener);
    synchronized (this.mLock)
    {
      if (!this.zaon.remove(paramOnConnectionFailedListener))
      {
        paramOnConnectionFailedListener = String.valueOf(paramOnConnectionFailedListener);
        Log.w("GmsClientEvents", String.valueOf(paramOnConnectionFailedListener).length() + 57 + "unregisterConnectionFailedListener(): listener " + paramOnConnectionFailedListener + " not found");
      }
      return;
    }
  }
  
  @VisibleForTesting
  public static abstract interface GmsClientEventState
  {
    public abstract Bundle getConnectionHint();
    
    public abstract boolean isConnected();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\GmsClientEventManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */