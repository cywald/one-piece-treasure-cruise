package com.google.android.gms.common.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

@KeepForSdk
public class LibraryVersion
{
  private static final GmsLogger zzel = new GmsLogger("LibraryVersion", "");
  private static LibraryVersion zzem = new LibraryVersion();
  private ConcurrentHashMap<String, String> zzen = new ConcurrentHashMap();
  
  @KeepForSdk
  public static LibraryVersion getInstance()
  {
    return zzem;
  }
  
  @KeepForSdk
  public String getVersion(@NonNull String paramString)
  {
    Preconditions.checkNotEmpty(paramString, "Please provide a valid libraryName");
    if (this.zzen.containsKey(paramString)) {
      return (String)this.zzen.get(paramString);
    }
    Object localObject1 = new Properties();
    for (;;)
    {
      try
      {
        localObject2 = LibraryVersion.class.getResourceAsStream(String.format("/%s.properties", new Object[] { paramString }));
        if (localObject2 != null)
        {
          ((Properties)localObject1).load((InputStream)localObject2);
          localObject1 = ((Properties)localObject1).getProperty("version", null);
        }
      }
      catch (IOException localIOException1)
      {
        Object localObject2;
        localObject1 = null;
      }
      try
      {
        zzel.v("LibraryVersion", String.valueOf(paramString).length() + 12 + String.valueOf(localObject1).length() + paramString + " version is " + (String)localObject1);
        localObject2 = localObject1;
        if (localObject1 == null)
        {
          localObject2 = "UNKNOWN";
          zzel.d("LibraryVersion", ".properties file is dropped during release process. Failure to read app version isexpected druing Google internal testing where locally-built libraries are used");
        }
        this.zzen.put(paramString, localObject2);
        return (String)localObject2;
      }
      catch (IOException localIOException2)
      {
        GmsLogger localGmsLogger;
        String str;
        for (;;) {}
      }
    }
    localObject2 = zzel;
    localObject1 = String.valueOf(paramString);
    if (((String)localObject1).length() != 0) {}
    for (localObject1 = "Failed to get app version for libraryName: ".concat((String)localObject1);; localObject1 = new String("Failed to get app version for libraryName: "))
    {
      ((GmsLogger)localObject2).e("LibraryVersion", (String)localObject1);
      localObject1 = null;
      break;
    }
    localGmsLogger = zzel;
    str = String.valueOf(paramString);
    if (str.length() != 0) {}
    for (str = "Failed to get app version for libraryName: ".concat(str);; str = new String("Failed to get app version for libraryName: "))
    {
      localGmsLogger.e("LibraryVersion", str, localIOException1);
      break;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\LibraryVersion.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */