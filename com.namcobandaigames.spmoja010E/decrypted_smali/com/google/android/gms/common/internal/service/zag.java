package com.google.android.gms.common.internal.service;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl;

abstract class zag<R extends Result>
  extends BaseImplementation.ApiMethodImpl<R, zai>
{
  public zag(GoogleApiClient paramGoogleApiClient)
  {
    super(Common.API, paramGoogleApiClient);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\service\zag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */