package com.google.android.gms.common.internal.service;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public final class zad
  implements zac
{
  public final PendingResult<Status> zaa(GoogleApiClient paramGoogleApiClient)
  {
    return paramGoogleApiClient.execute(new zae(this, paramGoogleApiClient));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\service\zad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */