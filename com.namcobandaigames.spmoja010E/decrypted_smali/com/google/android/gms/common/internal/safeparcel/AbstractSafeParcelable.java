package com.google.android.gms.common.internal.safeparcel;

public abstract class AbstractSafeParcelable
  implements SafeParcelable
{
  public final int describeContents()
  {
    return 0;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\safeparcel\AbstractSafeParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */