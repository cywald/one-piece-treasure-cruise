package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;

final class zai
  implements PendingResultUtil.zaa
{
  public final ApiException zaf(Status paramStatus)
  {
    return ApiExceptionUtil.fromStatus(paramStatus);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\internal\zai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */