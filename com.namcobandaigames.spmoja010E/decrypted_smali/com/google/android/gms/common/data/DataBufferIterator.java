package com.google.android.gms.common.data;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Iterator;
import java.util.NoSuchElementException;

@KeepForSdk
public class DataBufferIterator<T>
  implements Iterator<T>
{
  protected final DataBuffer<T> zalj;
  protected int zalk;
  
  public DataBufferIterator(DataBuffer<T> paramDataBuffer)
  {
    this.zalj = ((DataBuffer)Preconditions.checkNotNull(paramDataBuffer));
    this.zalk = -1;
  }
  
  public boolean hasNext()
  {
    return this.zalk < this.zalj.getCount() - 1;
  }
  
  public T next()
  {
    if (!hasNext())
    {
      i = this.zalk;
      throw new NoSuchElementException(46 + "Cannot advance the iterator beyond " + i);
    }
    DataBuffer localDataBuffer = this.zalj;
    int i = this.zalk + 1;
    this.zalk = i;
    return (T)localDataBuffer.get(i);
  }
  
  public void remove()
  {
    throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\data\DataBufferIterator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */