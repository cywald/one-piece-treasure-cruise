package com.google.android.gms.common.data;

import com.google.android.gms.common.annotation.KeepForSdk;
import java.util.NoSuchElementException;

@KeepForSdk
public class SingleRefDataBufferIterator<T>
  extends DataBufferIterator<T>
{
  private T zamf;
  
  public SingleRefDataBufferIterator(DataBuffer<T> paramDataBuffer)
  {
    super(paramDataBuffer);
  }
  
  public T next()
  {
    if (!hasNext())
    {
      int i = this.zalk;
      throw new NoSuchElementException(46 + "Cannot advance the iterator beyond " + i);
    }
    this.zalk += 1;
    if (this.zalk == 0)
    {
      this.zamf = this.zalj.get(0);
      if (!(this.zamf instanceof DataBufferRef))
      {
        String str = String.valueOf(this.zamf.getClass());
        throw new IllegalStateException(String.valueOf(str).length() + 44 + "DataBuffer reference of type " + str + " is not movable");
      }
    }
    else
    {
      ((DataBufferRef)this.zamf).zag(this.zalk);
    }
    return (T)this.zamf;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\data\SingleRefDataBufferIterator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */