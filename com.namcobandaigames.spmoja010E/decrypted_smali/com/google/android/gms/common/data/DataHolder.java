package com.google.android.gms.common.data;

import android.content.ContentValues;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.VersionField;
import com.google.android.gms.common.sqlite.CursorWrapper;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@KeepForSdk
@KeepName
@SafeParcelable.Class(creator="DataHolderCreator", validate=true)
public final class DataHolder
  extends AbstractSafeParcelable
  implements Closeable
{
  @KeepForSdk
  public static final Parcelable.Creator<DataHolder> CREATOR = new zac();
  private static final Builder zalx = new zab(new String[0], null);
  private boolean mClosed = false;
  @SafeParcelable.VersionField(id=1000)
  private final int zale;
  @SafeParcelable.Field(getter="getColumns", id=1)
  private final String[] zalp;
  private Bundle zalq;
  @SafeParcelable.Field(getter="getWindows", id=2)
  private final CursorWindow[] zalr;
  @SafeParcelable.Field(getter="getStatusCode", id=3)
  private final int zals;
  @SafeParcelable.Field(getter="getMetadata", id=4)
  private final Bundle zalt;
  private int[] zalu;
  private int zalv;
  private boolean zalw = true;
  
  @SafeParcelable.Constructor
  DataHolder(@SafeParcelable.Param(id=1000) int paramInt1, @SafeParcelable.Param(id=1) String[] paramArrayOfString, @SafeParcelable.Param(id=2) CursorWindow[] paramArrayOfCursorWindow, @SafeParcelable.Param(id=3) int paramInt2, @SafeParcelable.Param(id=4) Bundle paramBundle)
  {
    this.zale = paramInt1;
    this.zalp = paramArrayOfString;
    this.zalr = paramArrayOfCursorWindow;
    this.zals = paramInt2;
    this.zalt = paramBundle;
  }
  
  @KeepForSdk
  public DataHolder(Cursor paramCursor, int paramInt, Bundle paramBundle)
  {
    this(new CursorWrapper(paramCursor), paramInt, paramBundle);
  }
  
  private DataHolder(Builder paramBuilder, int paramInt, Bundle paramBundle)
  {
    this(Builder.zaa(paramBuilder), zaa(paramBuilder, -1), paramInt, null);
  }
  
  private DataHolder(Builder paramBuilder, int paramInt1, Bundle paramBundle, int paramInt2)
  {
    this(Builder.zaa(paramBuilder), zaa(paramBuilder, -1), paramInt1, paramBundle);
  }
  
  private DataHolder(CursorWrapper paramCursorWrapper, int paramInt, Bundle paramBundle)
  {
    this(paramCursorWrapper.getColumnNames(), zaa(paramCursorWrapper), paramInt, paramBundle);
  }
  
  @KeepForSdk
  public DataHolder(String[] paramArrayOfString, CursorWindow[] paramArrayOfCursorWindow, int paramInt, Bundle paramBundle)
  {
    this.zale = 1;
    this.zalp = ((String[])Preconditions.checkNotNull(paramArrayOfString));
    this.zalr = ((CursorWindow[])Preconditions.checkNotNull(paramArrayOfCursorWindow));
    this.zals = paramInt;
    this.zalt = paramBundle;
    zaca();
  }
  
  @KeepForSdk
  public static Builder builder(String[] paramArrayOfString)
  {
    return new Builder(paramArrayOfString, null, null);
  }
  
  @KeepForSdk
  public static DataHolder empty(int paramInt)
  {
    return new DataHolder(zalx, paramInt, null);
  }
  
  private final void zaa(String paramString, int paramInt)
  {
    if ((this.zalq == null) || (!this.zalq.containsKey(paramString)))
    {
      paramString = String.valueOf(paramString);
      if (paramString.length() != 0) {}
      for (paramString = "No such column: ".concat(paramString);; paramString = new String("No such column: ")) {
        throw new IllegalArgumentException(paramString);
      }
    }
    if (isClosed()) {
      throw new IllegalArgumentException("Buffer is closed.");
    }
    if ((paramInt < 0) || (paramInt >= this.zalv)) {
      throw new CursorIndexOutOfBoundsException(paramInt, this.zalv);
    }
  }
  
  private static CursorWindow[] zaa(Builder paramBuilder, int paramInt)
  {
    int k = 0;
    if (Builder.zaa(paramBuilder).length == 0) {
      return new CursorWindow[0];
    }
    Object localObject3;
    Object localObject2;
    ArrayList localArrayList;
    int i;
    label88:
    Object localObject1;
    if ((paramInt < 0) || (paramInt >= Builder.zab(paramBuilder).size()))
    {
      localObject3 = Builder.zab(paramBuilder);
      int m = ((List)localObject3).size();
      localObject2 = new CursorWindow(false);
      localArrayList = new ArrayList();
      localArrayList.add(localObject2);
      ((CursorWindow)localObject2).setNumColumns(Builder.zaa(paramBuilder).length);
      paramInt = 0;
      i = 0;
      if (paramInt >= m) {
        break label726;
      }
      localObject1 = localObject2;
    }
    for (;;)
    {
      int j;
      try
      {
        if (!((CursorWindow)localObject2).allocRow())
        {
          Log.d("DataHolder", 72 + "Allocating additional cursor window for large data set (row " + paramInt + ")");
          localObject2 = new CursorWindow(false);
          ((CursorWindow)localObject2).setStartPosition(paramInt);
          ((CursorWindow)localObject2).setNumColumns(Builder.zaa(paramBuilder).length);
          localArrayList.add(localObject2);
          localObject1 = localObject2;
          if (!((CursorWindow)localObject2).allocRow())
          {
            Log.e("DataHolder", "Unable to allocate row to hold data.");
            localArrayList.remove(localObject2);
            paramBuilder = (CursorWindow[])localArrayList.toArray(new CursorWindow[localArrayList.size()]);
            return paramBuilder;
            localObject3 = Builder.zab(paramBuilder).subList(0, paramInt);
            break;
          }
        }
        Map localMap = (Map)((List)localObject3).get(paramInt);
        j = 0;
        bool = true;
        if ((j < Builder.zaa(paramBuilder).length) && (bool))
        {
          localObject2 = Builder.zaa(paramBuilder)[j];
          Object localObject4 = localMap.get(localObject2);
          if (localObject4 == null)
          {
            bool = ((CursorWindow)localObject1).putNull(paramInt, j);
            break label743;
          }
          if ((localObject4 instanceof String))
          {
            bool = ((CursorWindow)localObject1).putString((String)localObject4, paramInt, j);
            break label743;
          }
          if ((localObject4 instanceof Long))
          {
            bool = ((CursorWindow)localObject1).putLong(((Long)localObject4).longValue(), paramInt, j);
            break label743;
          }
          if ((localObject4 instanceof Integer))
          {
            bool = ((CursorWindow)localObject1).putLong(((Integer)localObject4).intValue(), paramInt, j);
            break label743;
          }
          if ((localObject4 instanceof Boolean))
          {
            if (!((Boolean)localObject4).booleanValue()) {
              break label750;
            }
            l = 1L;
            bool = ((CursorWindow)localObject1).putLong(l, paramInt, j);
            break label743;
          }
          if ((localObject4 instanceof byte[]))
          {
            bool = ((CursorWindow)localObject1).putBlob((byte[])localObject4, paramInt, j);
            break label743;
          }
          if ((localObject4 instanceof Double))
          {
            bool = ((CursorWindow)localObject1).putDouble(((Double)localObject4).doubleValue(), paramInt, j);
            break label743;
          }
          if ((localObject4 instanceof Float))
          {
            bool = ((CursorWindow)localObject1).putDouble(((Float)localObject4).floatValue(), paramInt, j);
            break label743;
          }
          paramBuilder = String.valueOf(localObject4);
          throw new IllegalArgumentException(String.valueOf(localObject2).length() + 32 + String.valueOf(paramBuilder).length() + "Unsupported object for column " + (String)localObject2 + ": " + paramBuilder);
        }
      }
      catch (RuntimeException paramBuilder)
      {
        boolean bool;
        i = localArrayList.size();
        paramInt = k;
        if (paramInt < i)
        {
          ((CursorWindow)localArrayList.get(paramInt)).close();
          paramInt += 1;
          continue;
          if (!bool)
          {
            if (i != 0) {
              throw new zaa("Could not add the value to a new CursorWindow. The size of value may be larger than what a CursorWindow can handle.");
            }
            Log.d("DataHolder", 74 + "Couldn't populate window data for row " + paramInt + " - allocating new window.");
            ((CursorWindow)localObject1).freeLastRow();
            localObject1 = new CursorWindow(false);
            ((CursorWindow)localObject1).setStartPosition(paramInt);
            ((CursorWindow)localObject1).setNumColumns(Builder.zaa(paramBuilder).length);
            localArrayList.add(localObject1);
            i = paramInt - 1;
            paramInt = 1;
            j = paramInt;
            paramInt = i + 1;
            localObject2 = localObject1;
            i = j;
            break label88;
          }
          j = 0;
          i = paramInt;
          paramInt = j;
          continue;
        }
        throw paramBuilder;
      }
      label726:
      return (CursorWindow[])localArrayList.toArray(new CursorWindow[localArrayList.size()]);
      label743:
      j += 1;
      continue;
      label750:
      long l = 0L;
    }
  }
  
  private static CursorWindow[] zaa(CursorWrapper paramCursorWrapper)
  {
    ArrayList localArrayList = new ArrayList();
    for (;;)
    {
      try
      {
        int j = paramCursorWrapper.getCount();
        CursorWindow localCursorWindow = paramCursorWrapper.getWindow();
        if ((localCursorWindow == null) || (localCursorWindow.getStartPosition() != 0)) {
          break label162;
        }
        localCursorWindow.acquireReference();
        paramCursorWrapper.setWindow(null);
        localArrayList.add(localCursorWindow);
        i = localCursorWindow.getNumRows();
        if ((i < j) && (paramCursorWrapper.moveToPosition(i)))
        {
          localCursorWindow = paramCursorWrapper.getWindow();
          if (localCursorWindow != null)
          {
            localCursorWindow.acquireReference();
            paramCursorWrapper.setWindow(null);
            if (localCursorWindow.getNumRows() != 0)
            {
              localArrayList.add(localCursorWindow);
              i = localCursorWindow.getStartPosition();
              i = localCursorWindow.getNumRows() + i;
            }
          }
          else
          {
            localCursorWindow = new CursorWindow(false);
            localCursorWindow.setStartPosition(i);
            paramCursorWrapper.fillWindow(i, localCursorWindow);
            continue;
          }
        }
      }
      finally
      {
        paramCursorWrapper.close();
      }
      return (CursorWindow[])localArrayList.toArray(new CursorWindow[localArrayList.size()]);
      label162:
      int i = 0;
    }
  }
  
  @KeepForSdk
  public final void close()
  {
    try
    {
      if (!this.mClosed)
      {
        this.mClosed = true;
        int i = 0;
        while (i < this.zalr.length)
        {
          this.zalr[i].close();
          i += 1;
        }
      }
      return;
    }
    finally {}
  }
  
  protected final void finalize()
    throws Throwable
  {
    try
    {
      if ((this.zalw) && (this.zalr.length > 0) && (!isClosed()))
      {
        close();
        String str = toString();
        Log.e("DataBuffer", String.valueOf(str).length() + 178 + "Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: " + str + ")");
      }
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  @KeepForSdk
  public final boolean getBoolean(String paramString, int paramInt1, int paramInt2)
  {
    zaa(paramString, paramInt1);
    return Long.valueOf(this.zalr[paramInt2].getLong(paramInt1, this.zalq.getInt(paramString))).longValue() == 1L;
  }
  
  @KeepForSdk
  public final byte[] getByteArray(String paramString, int paramInt1, int paramInt2)
  {
    zaa(paramString, paramInt1);
    return this.zalr[paramInt2].getBlob(paramInt1, this.zalq.getInt(paramString));
  }
  
  @KeepForSdk
  public final int getCount()
  {
    return this.zalv;
  }
  
  @KeepForSdk
  public final int getInteger(String paramString, int paramInt1, int paramInt2)
  {
    zaa(paramString, paramInt1);
    return this.zalr[paramInt2].getInt(paramInt1, this.zalq.getInt(paramString));
  }
  
  @KeepForSdk
  public final long getLong(String paramString, int paramInt1, int paramInt2)
  {
    zaa(paramString, paramInt1);
    return this.zalr[paramInt2].getLong(paramInt1, this.zalq.getInt(paramString));
  }
  
  @KeepForSdk
  public final Bundle getMetadata()
  {
    return this.zalt;
  }
  
  @KeepForSdk
  public final int getStatusCode()
  {
    return this.zals;
  }
  
  @KeepForSdk
  public final String getString(String paramString, int paramInt1, int paramInt2)
  {
    zaa(paramString, paramInt1);
    return this.zalr[paramInt2].getString(paramInt1, this.zalq.getInt(paramString));
  }
  
  @KeepForSdk
  public final int getWindowIndex(int paramInt)
  {
    int j = 0;
    boolean bool;
    if ((paramInt >= 0) && (paramInt < this.zalv))
    {
      bool = true;
      Preconditions.checkState(bool);
    }
    for (;;)
    {
      int i = j;
      if (j < this.zalu.length)
      {
        if (paramInt < this.zalu[j]) {
          i = j - 1;
        }
      }
      else
      {
        paramInt = i;
        if (i == this.zalu.length) {
          paramInt = i - 1;
        }
        return paramInt;
        bool = false;
        break;
      }
      j += 1;
    }
  }
  
  @KeepForSdk
  public final boolean hasColumn(String paramString)
  {
    return this.zalq.containsKey(paramString);
  }
  
  @KeepForSdk
  public final boolean hasNull(String paramString, int paramInt1, int paramInt2)
  {
    zaa(paramString, paramInt1);
    return this.zalr[paramInt2].isNull(paramInt1, this.zalq.getInt(paramString));
  }
  
  @KeepForSdk
  public final boolean isClosed()
  {
    try
    {
      boolean bool = this.mClosed;
      return bool;
    }
    finally {}
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeStringArray(paramParcel, 1, this.zalp, false);
    SafeParcelWriter.writeTypedArray(paramParcel, 2, this.zalr, paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 3, getStatusCode());
    SafeParcelWriter.writeBundle(paramParcel, 4, getMetadata(), false);
    SafeParcelWriter.writeInt(paramParcel, 1000, this.zale);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
    if ((paramInt & 0x1) != 0) {
      close();
    }
  }
  
  public final float zaa(String paramString, int paramInt1, int paramInt2)
  {
    zaa(paramString, paramInt1);
    return this.zalr[paramInt2].getFloat(paramInt1, this.zalq.getInt(paramString));
  }
  
  public final void zaa(String paramString, int paramInt1, int paramInt2, CharArrayBuffer paramCharArrayBuffer)
  {
    zaa(paramString, paramInt1);
    this.zalr[paramInt2].copyStringToBuffer(paramInt1, this.zalq.getInt(paramString), paramCharArrayBuffer);
  }
  
  public final double zab(String paramString, int paramInt1, int paramInt2)
  {
    zaa(paramString, paramInt1);
    return this.zalr[paramInt2].getDouble(paramInt1, this.zalq.getInt(paramString));
  }
  
  public final void zaca()
  {
    int j = 0;
    this.zalq = new Bundle();
    int i = 0;
    while (i < this.zalp.length)
    {
      this.zalq.putInt(this.zalp[i], i);
      i += 1;
    }
    this.zalu = new int[this.zalr.length];
    int k = 0;
    i = j;
    j = k;
    while (i < this.zalr.length)
    {
      this.zalu[i] = j;
      k = this.zalr[i].getStartPosition();
      j += this.zalr[i].getNumRows() - (j - k);
      i += 1;
    }
    this.zalv = j;
  }
  
  @KeepForSdk
  public static class Builder
  {
    private final String[] zalp;
    private final ArrayList<HashMap<String, Object>> zaly;
    private final String zalz;
    private final HashMap<Object, Integer> zama;
    private boolean zamb;
    private String zamc;
    
    private Builder(String[] paramArrayOfString, String paramString)
    {
      this.zalp = ((String[])Preconditions.checkNotNull(paramArrayOfString));
      this.zaly = new ArrayList();
      this.zalz = paramString;
      this.zama = new HashMap();
      this.zamb = false;
      this.zamc = null;
    }
    
    @KeepForSdk
    public DataHolder build(int paramInt)
    {
      return new DataHolder(this, paramInt, null, null);
    }
    
    @KeepForSdk
    public DataHolder build(int paramInt, Bundle paramBundle)
    {
      return new DataHolder(this, paramInt, paramBundle, -1, null);
    }
    
    @KeepForSdk
    public Builder withRow(ContentValues paramContentValues)
    {
      Asserts.checkNotNull(paramContentValues);
      HashMap localHashMap = new HashMap(paramContentValues.size());
      paramContentValues = paramContentValues.valueSet().iterator();
      while (paramContentValues.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramContentValues.next();
        localHashMap.put((String)localEntry.getKey(), localEntry.getValue());
      }
      return zaa(localHashMap);
    }
    
    public Builder zaa(HashMap<String, Object> paramHashMap)
    {
      Asserts.checkNotNull(paramHashMap);
      int i;
      if (this.zalz == null)
      {
        i = -1;
        if (i != -1) {
          break label103;
        }
        this.zaly.add(paramHashMap);
      }
      for (;;)
      {
        this.zamb = false;
        return this;
        Object localObject = paramHashMap.get(this.zalz);
        if (localObject == null)
        {
          i = -1;
          break;
        }
        Integer localInteger = (Integer)this.zama.get(localObject);
        if (localInteger == null)
        {
          this.zama.put(localObject, Integer.valueOf(this.zaly.size()));
          i = -1;
          break;
        }
        i = localInteger.intValue();
        break;
        label103:
        this.zaly.remove(i);
        this.zaly.add(i, paramHashMap);
      }
    }
  }
  
  public static final class zaa
    extends RuntimeException
  {
    public zaa(String paramString)
    {
      super();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\data\DataHolder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */