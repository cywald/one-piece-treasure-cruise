package com.google.android.gms.common.data;

import com.google.android.gms.common.annotation.KeepForSdk;
import java.util.ArrayList;

@KeepForSdk
public abstract class EntityBuffer<T>
  extends AbstractDataBuffer<T>
{
  private boolean zamd = false;
  private ArrayList<Integer> zame;
  
  @KeepForSdk
  protected EntityBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
  }
  
  private final void zacb()
  {
    for (;;)
    {
      int i;
      String str2;
      try
      {
        if (this.zamd) {
          break label204;
        }
        int j = this.mDataHolder.getCount();
        this.zame = new ArrayList();
        if (j <= 0) {
          break label199;
        }
        this.zame.add(Integer.valueOf(0));
        String str3 = getPrimaryDataMarkerColumn();
        i = this.mDataHolder.getWindowIndex(0);
        String str1 = this.mDataHolder.getString(str3, 0, i);
        i = 1;
        if (i >= j) {
          break label199;
        }
        int k = this.mDataHolder.getWindowIndex(i);
        str2 = this.mDataHolder.getString(str3, i, k);
        if (str2 == null) {
          throw new NullPointerException(String.valueOf(str3).length() + 78 + "Missing value for markerColumn: " + str3 + ", at row: " + i + ", for window: " + k);
        }
      }
      finally {}
      if (!str2.equals(localObject1))
      {
        this.zame.add(Integer.valueOf(i));
        Object localObject2 = str2;
        break label207;
        label199:
        this.zamd = true;
        label204:
        return;
      }
      label207:
      i += 1;
    }
  }
  
  private final int zah(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.zame.size())) {
      throw new IllegalArgumentException(53 + "Position " + paramInt + " is out of bounds for this buffer");
    }
    return ((Integer)this.zame.get(paramInt)).intValue();
  }
  
  @KeepForSdk
  public final T get(int paramInt)
  {
    zacb();
    int k = zah(paramInt);
    int i;
    if ((paramInt < 0) || (paramInt == this.zame.size()))
    {
      i = 0;
      return (T)getEntry(k, i);
    }
    if (paramInt == this.zame.size() - 1) {}
    for (int j = this.mDataHolder.getCount() - ((Integer)this.zame.get(paramInt)).intValue();; j = ((Integer)this.zame.get(paramInt + 1)).intValue() - ((Integer)this.zame.get(paramInt)).intValue())
    {
      i = j;
      if (j != 1) {
        break;
      }
      paramInt = zah(paramInt);
      int m = this.mDataHolder.getWindowIndex(paramInt);
      String str = getChildDataMarkerColumn();
      i = j;
      if (str == null) {
        break;
      }
      i = j;
      if (this.mDataHolder.getString(str, paramInt, m) != null) {
        break;
      }
      i = 0;
      break;
    }
  }
  
  @KeepForSdk
  protected String getChildDataMarkerColumn()
  {
    return null;
  }
  
  @KeepForSdk
  public int getCount()
  {
    zacb();
    return this.zame.size();
  }
  
  @KeepForSdk
  protected abstract T getEntry(int paramInt1, int paramInt2);
  
  @KeepForSdk
  protected abstract String getPrimaryDataMarkerColumn();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\data\EntityBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */