package com.google.android.gms.common.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Target;

@Deprecated
@Documented
@Target({java.lang.annotation.ElementType.TYPE})
public @interface KeepForSdkWithMembers {}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\annotation\KeepForSdkWithMembers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */