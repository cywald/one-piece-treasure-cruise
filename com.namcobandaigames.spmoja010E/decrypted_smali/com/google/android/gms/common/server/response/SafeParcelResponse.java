package com.google.android.gms.common.server.response;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.SparseArray;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ParseException;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.VersionField;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.common.util.MapUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@KeepForSdk
@SafeParcelable.Class(creator="SafeParcelResponseCreator")
@VisibleForTesting
public class SafeParcelResponse
  extends FastSafeParcelableJsonResponse
{
  @KeepForSdk
  public static final Parcelable.Creator<SafeParcelResponse> CREATOR = new zap();
  private final String mClassName;
  @SafeParcelable.VersionField(getter="getVersionCode", id=1)
  private final int zale;
  @SafeParcelable.Field(getter="getFieldMappingDictionary", id=3)
  private final zak zapy;
  @SafeParcelable.Field(getter="getParcel", id=2)
  private final Parcel zara;
  private final int zarb;
  private int zarc;
  private int zard;
  
  @SafeParcelable.Constructor
  SafeParcelResponse(@SafeParcelable.Param(id=1) int paramInt, @SafeParcelable.Param(id=2) Parcel paramParcel, @SafeParcelable.Param(id=3) zak paramzak)
  {
    this.zale = paramInt;
    this.zara = ((Parcel)Preconditions.checkNotNull(paramParcel));
    this.zarb = 2;
    this.zapy = paramzak;
    if (this.zapy == null) {}
    for (this.mClassName = null;; this.mClassName = this.zapy.zact())
    {
      this.zarc = 2;
      return;
    }
  }
  
  private SafeParcelResponse(SafeParcelable paramSafeParcelable, zak paramzak, String paramString)
  {
    this.zale = 1;
    this.zara = Parcel.obtain();
    paramSafeParcelable.writeToParcel(this.zara, 0);
    this.zarb = 1;
    this.zapy = ((zak)Preconditions.checkNotNull(paramzak));
    this.mClassName = ((String)Preconditions.checkNotNull(paramString));
    this.zarc = 2;
  }
  
  public SafeParcelResponse(zak paramzak, String paramString)
  {
    this.zale = 1;
    this.zara = Parcel.obtain();
    this.zarb = 0;
    this.zapy = ((zak)Preconditions.checkNotNull(paramzak));
    this.mClassName = ((String)Preconditions.checkNotNull(paramString));
    this.zarc = 0;
  }
  
  @KeepForSdk
  public static <T extends FastJsonResponse,  extends SafeParcelable> SafeParcelResponse from(T paramT)
  {
    String str = paramT.getClass().getCanonicalName();
    zak localzak = new zak(paramT.getClass());
    zaa(localzak, paramT);
    localzak.zacs();
    localzak.zacr();
    return new SafeParcelResponse((SafeParcelable)paramT, localzak, str);
  }
  
  private static void zaa(zak paramzak, FastJsonResponse paramFastJsonResponse)
  {
    Object localObject = paramFastJsonResponse.getClass();
    if (!paramzak.zaa((Class)localObject))
    {
      Map localMap = paramFastJsonResponse.getFieldMappings();
      paramzak.zaa((Class)localObject, localMap);
      localObject = localMap.keySet().iterator();
      for (;;)
      {
        if (((Iterator)localObject).hasNext())
        {
          paramFastJsonResponse = (FastJsonResponse.Field)localMap.get((String)((Iterator)localObject).next());
          Class localClass = paramFastJsonResponse.zapw;
          if (localClass != null) {
            try
            {
              zaa(paramzak, (FastJsonResponse)localClass.newInstance());
            }
            catch (InstantiationException localInstantiationException)
            {
              paramzak = String.valueOf(paramFastJsonResponse.zapw.getCanonicalName());
              if (paramzak.length() != 0) {}
              for (paramzak = "Could not instantiate an object of type ".concat(paramzak);; paramzak = new String("Could not instantiate an object of type ")) {
                throw new IllegalStateException(paramzak, localInstantiationException);
              }
            }
            catch (IllegalAccessException localIllegalAccessException)
            {
              paramzak = String.valueOf(paramFastJsonResponse.zapw.getCanonicalName());
              if (paramzak.length() != 0) {}
              for (paramzak = "Could not access object of type ".concat(paramzak);; paramzak = new String("Could not access object of type ")) {
                throw new IllegalStateException(paramzak, localIllegalAccessException);
              }
            }
          }
        }
      }
    }
  }
  
  private static void zaa(StringBuilder paramStringBuilder, int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException(26 + "Unknown type = " + paramInt);
    case 0: 
    case 1: 
    case 2: 
    case 3: 
    case 4: 
    case 5: 
    case 6: 
      paramStringBuilder.append(paramObject);
      return;
    case 7: 
      paramStringBuilder.append("\"").append(JsonUtils.escapeString(paramObject.toString())).append("\"");
      return;
    case 8: 
      paramStringBuilder.append("\"").append(Base64Utils.encode((byte[])paramObject)).append("\"");
      return;
    case 9: 
      paramStringBuilder.append("\"").append(Base64Utils.encodeUrlSafe((byte[])paramObject));
      paramStringBuilder.append("\"");
      return;
    case 10: 
      MapUtils.writeStringMapToJson(paramStringBuilder, (HashMap)paramObject);
      return;
    }
    throw new IllegalArgumentException("Method does not accept concrete type.");
  }
  
  private final void zaa(StringBuilder paramStringBuilder, Map<String, FastJsonResponse.Field<?, ?>> paramMap, Parcel paramParcel)
  {
    SparseArray localSparseArray = new SparseArray();
    paramMap = paramMap.entrySet().iterator();
    Object localObject1;
    while (paramMap.hasNext())
    {
      localObject1 = (Map.Entry)paramMap.next();
      localSparseArray.put(((FastJsonResponse.Field)((Map.Entry)localObject1).getValue()).getSafeParcelableFieldId(), localObject1);
    }
    paramStringBuilder.append('{');
    int j = SafeParcelReader.validateObjectHeader(paramParcel);
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = SafeParcelReader.readHeader(paramParcel);
      paramMap = (Map.Entry)localSparseArray.get(SafeParcelReader.getFieldId(k));
      if (paramMap != null)
      {
        if (i != 0) {
          paramStringBuilder.append(",");
        }
        localObject1 = (String)paramMap.getKey();
        paramMap = (FastJsonResponse.Field)paramMap.getValue();
        paramStringBuilder.append("\"").append((String)localObject1).append("\":");
        if (paramMap.zacn()) {
          switch (paramMap.zaps)
          {
          default: 
            i = paramMap.zaps;
            throw new IllegalArgumentException(36 + "Unknown field out type = " + i);
          case 0: 
            zab(paramStringBuilder, paramMap, zab(paramMap, Integer.valueOf(SafeParcelReader.readInt(paramParcel, k))));
          }
        }
        for (;;)
        {
          i = 1;
          break;
          zab(paramStringBuilder, paramMap, zab(paramMap, SafeParcelReader.createBigInteger(paramParcel, k)));
          continue;
          zab(paramStringBuilder, paramMap, zab(paramMap, Long.valueOf(SafeParcelReader.readLong(paramParcel, k))));
          continue;
          zab(paramStringBuilder, paramMap, zab(paramMap, Float.valueOf(SafeParcelReader.readFloat(paramParcel, k))));
          continue;
          zab(paramStringBuilder, paramMap, zab(paramMap, Double.valueOf(SafeParcelReader.readDouble(paramParcel, k))));
          continue;
          zab(paramStringBuilder, paramMap, zab(paramMap, SafeParcelReader.createBigDecimal(paramParcel, k)));
          continue;
          zab(paramStringBuilder, paramMap, zab(paramMap, Boolean.valueOf(SafeParcelReader.readBoolean(paramParcel, k))));
          continue;
          zab(paramStringBuilder, paramMap, zab(paramMap, SafeParcelReader.createString(paramParcel, k)));
          continue;
          zab(paramStringBuilder, paramMap, zab(paramMap, SafeParcelReader.createByteArray(paramParcel, k)));
          continue;
          localObject1 = SafeParcelReader.createBundle(paramParcel, k);
          Object localObject2 = new HashMap();
          Iterator localIterator = ((Bundle)localObject1).keySet().iterator();
          while (localIterator.hasNext())
          {
            String str = (String)localIterator.next();
            ((HashMap)localObject2).put(str, ((Bundle)localObject1).getString(str));
          }
          zab(paramStringBuilder, paramMap, zab(paramMap, localObject2));
          continue;
          throw new IllegalArgumentException("Method does not accept concrete type.");
          if (paramMap.zapt)
          {
            paramStringBuilder.append("[");
            switch (paramMap.zaps)
            {
            default: 
              throw new IllegalStateException("Unknown field type out.");
            case 0: 
              ArrayUtils.writeArray(paramStringBuilder, SafeParcelReader.createIntArray(paramParcel, k));
            }
            for (;;)
            {
              paramStringBuilder.append("]");
              break;
              ArrayUtils.writeArray(paramStringBuilder, SafeParcelReader.createBigIntegerArray(paramParcel, k));
              continue;
              ArrayUtils.writeArray(paramStringBuilder, SafeParcelReader.createLongArray(paramParcel, k));
              continue;
              ArrayUtils.writeArray(paramStringBuilder, SafeParcelReader.createFloatArray(paramParcel, k));
              continue;
              ArrayUtils.writeArray(paramStringBuilder, SafeParcelReader.createDoubleArray(paramParcel, k));
              continue;
              ArrayUtils.writeArray(paramStringBuilder, SafeParcelReader.createBigDecimalArray(paramParcel, k));
              continue;
              ArrayUtils.writeArray(paramStringBuilder, SafeParcelReader.createBooleanArray(paramParcel, k));
              continue;
              ArrayUtils.writeStringArray(paramStringBuilder, SafeParcelReader.createStringArray(paramParcel, k));
              continue;
              throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
              localObject1 = SafeParcelReader.createParcelArray(paramParcel, k);
              k = localObject1.length;
              i = 0;
              while (i < k)
              {
                if (i > 0) {
                  paramStringBuilder.append(",");
                }
                localObject1[i].setDataPosition(0);
                zaa(paramStringBuilder, paramMap.zacq(), localObject1[i]);
                i += 1;
              }
            }
          }
          switch (paramMap.zaps)
          {
          default: 
            throw new IllegalStateException("Unknown field type out");
          case 0: 
            paramStringBuilder.append(SafeParcelReader.readInt(paramParcel, k));
            break;
          case 1: 
            paramStringBuilder.append(SafeParcelReader.createBigInteger(paramParcel, k));
            break;
          case 2: 
            paramStringBuilder.append(SafeParcelReader.readLong(paramParcel, k));
            break;
          case 3: 
            paramStringBuilder.append(SafeParcelReader.readFloat(paramParcel, k));
            break;
          case 4: 
            paramStringBuilder.append(SafeParcelReader.readDouble(paramParcel, k));
            break;
          case 5: 
            paramStringBuilder.append(SafeParcelReader.createBigDecimal(paramParcel, k));
            break;
          case 6: 
            paramStringBuilder.append(SafeParcelReader.readBoolean(paramParcel, k));
            break;
          case 7: 
            paramMap = SafeParcelReader.createString(paramParcel, k);
            paramStringBuilder.append("\"").append(JsonUtils.escapeString(paramMap)).append("\"");
            break;
          case 8: 
            paramMap = SafeParcelReader.createByteArray(paramParcel, k);
            paramStringBuilder.append("\"").append(Base64Utils.encode(paramMap)).append("\"");
            break;
          case 9: 
            paramMap = SafeParcelReader.createByteArray(paramParcel, k);
            paramStringBuilder.append("\"").append(Base64Utils.encodeUrlSafe(paramMap));
            paramStringBuilder.append("\"");
            break;
          case 10: 
            paramMap = SafeParcelReader.createBundle(paramParcel, k);
            localObject1 = paramMap.keySet();
            ((Set)localObject1).size();
            paramStringBuilder.append("{");
            localObject1 = ((Set)localObject1).iterator();
            for (i = 1; ((Iterator)localObject1).hasNext(); i = 0)
            {
              localObject2 = (String)((Iterator)localObject1).next();
              if (i == 0) {
                paramStringBuilder.append(",");
              }
              paramStringBuilder.append("\"").append((String)localObject2).append("\"");
              paramStringBuilder.append(":");
              paramStringBuilder.append("\"").append(JsonUtils.escapeString(paramMap.getString((String)localObject2))).append("\"");
            }
            paramStringBuilder.append("}");
            break;
          case 11: 
            localObject1 = SafeParcelReader.createParcel(paramParcel, k);
            ((Parcel)localObject1).setDataPosition(0);
            zaa(paramStringBuilder, paramMap.zacq(), (Parcel)localObject1);
          }
        }
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new SafeParcelReader.ParseException(37 + "Overread allowed size end=" + j, paramParcel);
    }
    paramStringBuilder.append('}');
  }
  
  private final void zab(FastJsonResponse.Field<?, ?> paramField)
  {
    if (paramField.zapv != -1) {}
    for (int i = 1; i == 0; i = 0) {
      throw new IllegalStateException("Field does not have a valid safe parcelable field id.");
    }
    if (this.zara == null) {
      throw new IllegalStateException("Internal Parcel object is null.");
    }
    switch (this.zarc)
    {
    default: 
      throw new IllegalStateException("Unknown parse state in SafeParcelResponse.");
    case 0: 
      this.zard = SafeParcelWriter.beginObjectHeader(this.zara);
      this.zarc = 1;
    case 1: 
      return;
    }
    throw new IllegalStateException("Attempted to parse JSON with a SafeParcelResponse object that is already filled with data.");
  }
  
  private final void zab(StringBuilder paramStringBuilder, FastJsonResponse.Field<?, ?> paramField, Object paramObject)
  {
    if (paramField.zapr)
    {
      paramObject = (ArrayList)paramObject;
      paramStringBuilder.append("[");
      int j = ((ArrayList)paramObject).size();
      int i = 0;
      while (i < j)
      {
        if (i != 0) {
          paramStringBuilder.append(",");
        }
        zaa(paramStringBuilder, paramField.zapq, ((ArrayList)paramObject).get(i));
        i += 1;
      }
      paramStringBuilder.append("]");
      return;
    }
    zaa(paramStringBuilder, paramField.zapq, paramObject);
  }
  
  private final Parcel zacu()
  {
    switch (this.zarc)
    {
    }
    for (;;)
    {
      return this.zara;
      this.zard = SafeParcelWriter.beginObjectHeader(this.zara);
      SafeParcelWriter.finishObjectHeader(this.zara, this.zard);
      this.zarc = 2;
    }
  }
  
  public <T extends FastJsonResponse> void addConcreteTypeArrayInternal(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<T> paramArrayList)
  {
    zab(paramField);
    paramString = new ArrayList();
    paramArrayList.size();
    paramArrayList = (ArrayList)paramArrayList;
    int j = paramArrayList.size();
    int i = 0;
    while (i < j)
    {
      Object localObject = paramArrayList.get(i);
      i += 1;
      paramString.add(((SafeParcelResponse)localObject).zacu());
    }
    SafeParcelWriter.writeParcelList(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  public <T extends FastJsonResponse> void addConcreteTypeInternal(FastJsonResponse.Field<?, ?> paramField, String paramString, T paramT)
  {
    zab(paramField);
    paramString = ((SafeParcelResponse)paramT).zacu();
    SafeParcelWriter.writeParcel(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  public Map<String, FastJsonResponse.Field<?, ?>> getFieldMappings()
  {
    if (this.zapy == null) {
      return null;
    }
    return this.zapy.zai(this.mClassName);
  }
  
  public Object getValueObject(String paramString)
  {
    throw new UnsupportedOperationException("Converting to JSON does not require this method.");
  }
  
  public boolean isPrimitiveFieldSet(String paramString)
  {
    throw new UnsupportedOperationException("Converting to JSON does not require this method.");
  }
  
  protected void setBooleanInternal(FastJsonResponse.Field<?, ?> paramField, String paramString, boolean paramBoolean)
  {
    zab(paramField);
    SafeParcelWriter.writeBoolean(this.zara, paramField.getSafeParcelableFieldId(), paramBoolean);
  }
  
  protected void setDecodedBytesInternal(FastJsonResponse.Field<?, ?> paramField, String paramString, byte[] paramArrayOfByte)
  {
    zab(paramField);
    SafeParcelWriter.writeByteArray(this.zara, paramField.getSafeParcelableFieldId(), paramArrayOfByte, true);
  }
  
  protected void setIntegerInternal(FastJsonResponse.Field<?, ?> paramField, String paramString, int paramInt)
  {
    zab(paramField);
    SafeParcelWriter.writeInt(this.zara, paramField.getSafeParcelableFieldId(), paramInt);
  }
  
  protected void setLongInternal(FastJsonResponse.Field<?, ?> paramField, String paramString, long paramLong)
  {
    zab(paramField);
    SafeParcelWriter.writeLong(this.zara, paramField.getSafeParcelableFieldId(), paramLong);
  }
  
  protected void setStringInternal(FastJsonResponse.Field<?, ?> paramField, String paramString1, String paramString2)
  {
    zab(paramField);
    SafeParcelWriter.writeString(this.zara, paramField.getSafeParcelableFieldId(), paramString2, true);
  }
  
  protected void setStringsInternal(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<String> paramArrayList)
  {
    zab(paramField);
    int j = paramArrayList.size();
    paramString = new String[j];
    int i = 0;
    while (i < j)
    {
      paramString[i] = ((String)paramArrayList.get(i));
      i += 1;
    }
    SafeParcelWriter.writeStringArray(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  public String toString()
  {
    Preconditions.checkNotNull(this.zapy, "Cannot convert to JSON on client side.");
    Parcel localParcel = zacu();
    localParcel.setDataPosition(0);
    StringBuilder localStringBuilder = new StringBuilder(100);
    zaa(localStringBuilder, this.zapy.zai(this.mClassName), localParcel);
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 1, this.zale);
    SafeParcelWriter.writeParcel(paramParcel, 2, zacu(), false);
    Object localObject;
    switch (this.zarb)
    {
    default: 
      paramInt = this.zarb;
      throw new IllegalStateException(34 + "Invalid creation type: " + paramInt);
    case 0: 
      localObject = null;
    }
    for (;;)
    {
      SafeParcelWriter.writeParcelable(paramParcel, 3, (Parcelable)localObject, paramInt, false);
      SafeParcelWriter.finishObjectHeader(paramParcel, i);
      return;
      localObject = this.zapy;
      continue;
      localObject = this.zapy;
    }
  }
  
  protected final void zaa(FastJsonResponse.Field<?, ?> paramField, String paramString, double paramDouble)
  {
    zab(paramField);
    SafeParcelWriter.writeDouble(this.zara, paramField.getSafeParcelableFieldId(), paramDouble);
  }
  
  protected final void zaa(FastJsonResponse.Field<?, ?> paramField, String paramString, float paramFloat)
  {
    zab(paramField);
    SafeParcelWriter.writeFloat(this.zara, paramField.getSafeParcelableFieldId(), paramFloat);
  }
  
  protected final void zaa(FastJsonResponse.Field<?, ?> paramField, String paramString, BigDecimal paramBigDecimal)
  {
    zab(paramField);
    SafeParcelWriter.writeBigDecimal(this.zara, paramField.getSafeParcelableFieldId(), paramBigDecimal, true);
  }
  
  protected final void zaa(FastJsonResponse.Field<?, ?> paramField, String paramString, BigInteger paramBigInteger)
  {
    zab(paramField);
    SafeParcelWriter.writeBigInteger(this.zara, paramField.getSafeParcelableFieldId(), paramBigInteger, true);
  }
  
  protected final void zaa(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<Integer> paramArrayList)
  {
    zab(paramField);
    int j = paramArrayList.size();
    paramString = new int[j];
    int i = 0;
    while (i < j)
    {
      paramString[i] = ((Integer)paramArrayList.get(i)).intValue();
      i += 1;
    }
    SafeParcelWriter.writeIntArray(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  protected final void zaa(FastJsonResponse.Field<?, ?> paramField, String paramString, Map<String, String> paramMap)
  {
    zab(paramField);
    paramString = new Bundle();
    Iterator localIterator = paramMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      paramString.putString(str, (String)paramMap.get(str));
    }
    SafeParcelWriter.writeBundle(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  protected final void zab(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<BigInteger> paramArrayList)
  {
    zab(paramField);
    int j = paramArrayList.size();
    paramString = new BigInteger[j];
    int i = 0;
    while (i < j)
    {
      paramString[i] = ((BigInteger)paramArrayList.get(i));
      i += 1;
    }
    SafeParcelWriter.writeBigIntegerArray(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  protected final void zac(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<Long> paramArrayList)
  {
    zab(paramField);
    int j = paramArrayList.size();
    paramString = new long[j];
    int i = 0;
    while (i < j)
    {
      paramString[i] = ((Long)paramArrayList.get(i)).longValue();
      i += 1;
    }
    SafeParcelWriter.writeLongArray(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  protected final void zad(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<Float> paramArrayList)
  {
    zab(paramField);
    int j = paramArrayList.size();
    paramString = new float[j];
    int i = 0;
    while (i < j)
    {
      paramString[i] = ((Float)paramArrayList.get(i)).floatValue();
      i += 1;
    }
    SafeParcelWriter.writeFloatArray(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  protected final void zae(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<Double> paramArrayList)
  {
    zab(paramField);
    int j = paramArrayList.size();
    paramString = new double[j];
    int i = 0;
    while (i < j)
    {
      paramString[i] = ((Double)paramArrayList.get(i)).doubleValue();
      i += 1;
    }
    SafeParcelWriter.writeDoubleArray(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  protected final void zaf(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<BigDecimal> paramArrayList)
  {
    zab(paramField);
    int j = paramArrayList.size();
    paramString = new BigDecimal[j];
    int i = 0;
    while (i < j)
    {
      paramString[i] = ((BigDecimal)paramArrayList.get(i));
      i += 1;
    }
    SafeParcelWriter.writeBigDecimalArray(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
  
  protected final void zag(FastJsonResponse.Field<?, ?> paramField, String paramString, ArrayList<Boolean> paramArrayList)
  {
    zab(paramField);
    int j = paramArrayList.size();
    paramString = new boolean[j];
    int i = 0;
    while (i < j)
    {
      paramString[i] = ((Boolean)paramArrayList.get(i)).booleanValue();
      i += 1;
    }
    SafeParcelWriter.writeBooleanArray(this.zara, paramField.getSafeParcelableFieldId(), paramString, true);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\server\response\SafeParcelResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */