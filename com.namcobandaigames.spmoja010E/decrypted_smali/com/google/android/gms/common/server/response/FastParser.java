package com.google.android.gms.common.server.response;

import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.gms.common.util.JsonUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

@KeepForSdk
@ShowFirstParty
public class FastParser<T extends FastJsonResponse>
{
  private static final char[] zaqf = { 117, 108, 108 };
  private static final char[] zaqg = { 114, 117, 101 };
  private static final char[] zaqh = { 114, 117, 101, 34 };
  private static final char[] zaqi = { 97, 108, 115, 101 };
  private static final char[] zaqj = { 97, 108, 115, 101, 34 };
  private static final char[] zaqk = { '\n' };
  private static final zaa<Integer> zaqm = new zaa();
  private static final zaa<Long> zaqn = new zab();
  private static final zaa<Float> zaqo = new zac();
  private static final zaa<Double> zaqp = new zad();
  private static final zaa<Boolean> zaqq = new zae();
  private static final zaa<String> zaqr = new zaf();
  private static final zaa<BigInteger> zaqs = new zag();
  private static final zaa<BigDecimal> zaqt = new zah();
  private final char[] zaqa = new char[1];
  private final char[] zaqb = new char[32];
  private final char[] zaqc = new char['Ѐ'];
  private final StringBuilder zaqd = new StringBuilder(32);
  private final StringBuilder zaqe = new StringBuilder(1024);
  private final Stack<Integer> zaql = new Stack();
  
  private final int zaa(BufferedReader paramBufferedReader, char[] paramArrayOfChar)
    throws FastParser.ParseException, IOException
  {
    char c = zaj(paramBufferedReader);
    if (c == 0) {
      throw new ParseException("Unexpected EOF");
    }
    if (c == ',') {
      throw new ParseException("Missing value");
    }
    if (c == 'n')
    {
      zab(paramBufferedReader, zaqf);
      return 0;
    }
    paramBufferedReader.mark(1024);
    int i;
    int j;
    if (c == '"')
    {
      i = 0;
      j = 0;
      if ((j < paramArrayOfChar.length) && (paramBufferedReader.read(paramArrayOfChar, j, 1) != -1))
      {
        c = paramArrayOfChar[j];
        if (Character.isISOControl(c)) {
          throw new ParseException("Unexpected control character while reading string");
        }
        if ((c == '"') && (i == 0))
        {
          paramBufferedReader.reset();
          paramBufferedReader.skip(j + 1);
          return j;
        }
        if (c == '\\') {
          if (i == 0) {
            i = 1;
          }
        }
        for (;;)
        {
          j += 1;
          break;
          i = 0;
          continue;
          i = 0;
        }
      }
      if (j == paramArrayOfChar.length) {
        throw new ParseException("Absurdly long value");
      }
    }
    else
    {
      paramArrayOfChar[0] = c;
      i = 1;
      for (;;)
      {
        j = i;
        if (i >= paramArrayOfChar.length) {
          break;
        }
        j = i;
        if (paramBufferedReader.read(paramArrayOfChar, i, 1) == -1) {
          break;
        }
        if ((paramArrayOfChar[i] == '}') || (paramArrayOfChar[i] == ',') || (Character.isWhitespace(paramArrayOfChar[i])) || (paramArrayOfChar[i] == ']'))
        {
          paramBufferedReader.reset();
          paramBufferedReader.skip(i - 1);
          paramArrayOfChar[i] = '\000';
          return i;
        }
        i += 1;
      }
    }
    throw new ParseException("Unexpected EOF");
  }
  
  private final String zaa(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    String str = null;
    this.zaql.push(Integer.valueOf(2));
    char c = zaj(paramBufferedReader);
    switch (c)
    {
    default: 
      throw new ParseException(19 + "Unexpected token: " + c);
    case '}': 
      zak(2);
    }
    do
    {
      return str;
      zak(2);
      zak(1);
      zak(5);
      return null;
      this.zaql.push(Integer.valueOf(3));
      str = zab(paramBufferedReader, this.zaqb, this.zaqd, null);
      zak(3);
    } while (zaj(paramBufferedReader) == ':');
    throw new ParseException("Expected key/value separator");
  }
  
  private final String zaa(BufferedReader paramBufferedReader, char[] paramArrayOfChar1, StringBuilder paramStringBuilder, char[] paramArrayOfChar2)
    throws FastParser.ParseException, IOException
  {
    switch (zaj(paramBufferedReader))
    {
    default: 
      throw new ParseException("Expected string");
    case '"': 
      return zab(paramBufferedReader, paramArrayOfChar1, paramStringBuilder, paramArrayOfChar2);
    }
    zab(paramBufferedReader, zaqf);
    return null;
  }
  
  private final <T extends FastJsonResponse> ArrayList<T> zaa(BufferedReader paramBufferedReader, FastJsonResponse.Field<?, ?> paramField)
    throws FastParser.ParseException, IOException
  {
    ArrayList localArrayList = new ArrayList();
    char c = zaj(paramBufferedReader);
    switch (c)
    {
    default: 
      throw new ParseException(19 + "Unexpected token: " + c);
    case ']': 
      zak(5);
      return localArrayList;
    case '{': 
      this.zaql.push(Integer.valueOf(1));
    }
    for (;;)
    {
      try
      {
        FastJsonResponse localFastJsonResponse = paramField.zacp();
        if (!zaa(paramBufferedReader, localFastJsonResponse)) {
          break;
        }
        localArrayList.add(localFastJsonResponse);
        c = zaj(paramBufferedReader);
        switch (c)
        {
        default: 
          throw new ParseException(19 + "Unexpected token: " + c);
        }
      }
      catch (InstantiationException paramBufferedReader)
      {
        throw new ParseException("Error instantiating inner object", paramBufferedReader);
      }
      catch (IllegalAccessException paramBufferedReader)
      {
        throw new ParseException("Error instantiating inner object", paramBufferedReader);
      }
      zab(paramBufferedReader, zaqf);
      zak(5);
      return null;
      if (zaj(paramBufferedReader) != '{') {
        throw new ParseException("Expected start of next object in array");
      }
      this.zaql.push(Integer.valueOf(1));
    }
    zak(5);
    return localArrayList;
  }
  
  private final <O> ArrayList<O> zaa(BufferedReader paramBufferedReader, zaa<O> paramzaa)
    throws FastParser.ParseException, IOException
  {
    int i = zaj(paramBufferedReader);
    if (i == 110)
    {
      zab(paramBufferedReader, zaqf);
      return null;
    }
    if (i != 91) {
      throw new ParseException("Expected start of array");
    }
    this.zaql.push(Integer.valueOf(5));
    ArrayList localArrayList = new ArrayList();
    for (;;)
    {
      paramBufferedReader.mark(1024);
      switch (zaj(paramBufferedReader))
      {
      case ',': 
      default: 
        paramBufferedReader.reset();
        localArrayList.add(paramzaa.zah(this, paramBufferedReader));
      }
    }
    zak(5);
    return localArrayList;
    throw new ParseException("Unexpected EOF");
  }
  
  private final boolean zaa(BufferedReader paramBufferedReader, FastJsonResponse paramFastJsonResponse)
    throws FastParser.ParseException, IOException
  {
    Map localMap = paramFastJsonResponse.getFieldMappings();
    Object localObject2 = zaa(paramBufferedReader);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      zak(1);
      return false;
      localObject1 = null;
    }
    while (localObject1 != null)
    {
      localObject2 = (FastJsonResponse.Field)localMap.get(localObject1);
      if (localObject2 == null)
      {
        localObject1 = zab(paramBufferedReader);
      }
      else
      {
        this.zaql.push(Integer.valueOf(4));
        int i;
        switch (((FastJsonResponse.Field)localObject2).zapq)
        {
        default: 
          i = ((FastJsonResponse.Field)localObject2).zapq;
          throw new ParseException(30 + "Invalid field type " + i);
        case 0: 
          if (((FastJsonResponse.Field)localObject2).zapr) {
            paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, zaqm));
          }
          break;
        }
        for (;;)
        {
          zak(4);
          zak(2);
          char c = zaj(paramBufferedReader);
          switch (c)
          {
          case '}': 
          default: 
            throw new ParseException(55 + "Expected end of object or field separator, but found: " + c);
            paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zad(paramBufferedReader));
            continue;
            if (((FastJsonResponse.Field)localObject2).zapr)
            {
              paramFastJsonResponse.zab((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, zaqs));
            }
            else
            {
              paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zaf(paramBufferedReader));
              continue;
              if (((FastJsonResponse.Field)localObject2).zapr)
              {
                paramFastJsonResponse.zac((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, zaqn));
              }
              else
              {
                paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zae(paramBufferedReader));
                continue;
                if (((FastJsonResponse.Field)localObject2).zapr)
                {
                  paramFastJsonResponse.zad((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, zaqo));
                }
                else
                {
                  paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zag(paramBufferedReader));
                  continue;
                  if (((FastJsonResponse.Field)localObject2).zapr)
                  {
                    paramFastJsonResponse.zae((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, zaqp));
                  }
                  else
                  {
                    paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zah(paramBufferedReader));
                    continue;
                    if (((FastJsonResponse.Field)localObject2).zapr)
                    {
                      paramFastJsonResponse.zaf((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, zaqt));
                    }
                    else
                    {
                      paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zai(paramBufferedReader));
                      continue;
                      if (((FastJsonResponse.Field)localObject2).zapr)
                      {
                        paramFastJsonResponse.zag((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, zaqq));
                      }
                      else
                      {
                        paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, false));
                        continue;
                        if (((FastJsonResponse.Field)localObject2).zapr)
                        {
                          paramFastJsonResponse.zah((FastJsonResponse.Field)localObject2, zaa(paramBufferedReader, zaqr));
                        }
                        else
                        {
                          paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, zac(paramBufferedReader));
                          continue;
                          paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, Base64Utils.decode(zaa(paramBufferedReader, this.zaqc, this.zaqe, zaqk)));
                          continue;
                          paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, Base64Utils.decodeUrlSafe(zaa(paramBufferedReader, this.zaqc, this.zaqe, zaqk)));
                          continue;
                          i = zaj(paramBufferedReader);
                          if (i == 110)
                          {
                            zab(paramBufferedReader, zaqf);
                            localObject1 = null;
                          }
                          for (;;)
                          {
                            paramFastJsonResponse.zaa((FastJsonResponse.Field)localObject2, (Map)localObject1);
                            break;
                            if (i != 123) {
                              throw new ParseException("Expected start of a map object");
                            }
                            this.zaql.push(Integer.valueOf(1));
                            localObject1 = new HashMap();
                            do
                            {
                              for (;;)
                              {
                                switch (zaj(paramBufferedReader))
                                {
                                }
                              }
                              throw new ParseException("Unexpected EOF");
                              String str = zab(paramBufferedReader, this.zaqb, this.zaqd, null);
                              if (zaj(paramBufferedReader) != ':')
                              {
                                paramBufferedReader = String.valueOf(str);
                                if (paramBufferedReader.length() != 0) {}
                                for (paramBufferedReader = "No map value found for key ".concat(paramBufferedReader);; paramBufferedReader = new String("No map value found for key ")) {
                                  throw new ParseException(paramBufferedReader);
                                }
                              }
                              if (zaj(paramBufferedReader) != '"')
                              {
                                paramBufferedReader = String.valueOf(str);
                                if (paramBufferedReader.length() != 0) {}
                                for (paramBufferedReader = "Expected String value for key ".concat(paramBufferedReader);; paramBufferedReader = new String("Expected String value for key ")) {
                                  throw new ParseException(paramBufferedReader);
                                }
                              }
                              ((HashMap)localObject1).put(str, zab(paramBufferedReader, this.zaqb, this.zaqd, null));
                              c = zaj(paramBufferedReader);
                            } while (c == ',');
                            if (c == '}')
                            {
                              zak(1);
                            }
                            else
                            {
                              throw new ParseException(48 + "Unexpected character while parsing string map: " + c);
                              zak(1);
                            }
                          }
                          if (((FastJsonResponse.Field)localObject2).zapr)
                          {
                            i = zaj(paramBufferedReader);
                            if (i == 110)
                            {
                              zab(paramBufferedReader, zaqf);
                              paramFastJsonResponse.addConcreteTypeArrayInternal((FastJsonResponse.Field)localObject2, ((FastJsonResponse.Field)localObject2).zapu, null);
                            }
                            else
                            {
                              this.zaql.push(Integer.valueOf(5));
                              if (i != 91) {
                                throw new ParseException("Expected array start");
                              }
                              paramFastJsonResponse.addConcreteTypeArrayInternal((FastJsonResponse.Field)localObject2, ((FastJsonResponse.Field)localObject2).zapu, zaa(paramBufferedReader, (FastJsonResponse.Field)localObject2));
                            }
                          }
                          else
                          {
                            i = zaj(paramBufferedReader);
                            if (i == 110)
                            {
                              zab(paramBufferedReader, zaqf);
                              paramFastJsonResponse.addConcreteTypeInternal((FastJsonResponse.Field)localObject2, ((FastJsonResponse.Field)localObject2).zapu, null);
                            }
                            else
                            {
                              this.zaql.push(Integer.valueOf(1));
                              if (i != 123) {
                                throw new ParseException("Expected start of object");
                              }
                              try
                              {
                                localObject1 = ((FastJsonResponse.Field)localObject2).zacp();
                                zaa(paramBufferedReader, (FastJsonResponse)localObject1);
                                paramFastJsonResponse.addConcreteTypeInternal((FastJsonResponse.Field)localObject2, ((FastJsonResponse.Field)localObject2).zapu, (FastJsonResponse)localObject1);
                              }
                              catch (InstantiationException paramBufferedReader)
                              {
                                throw new ParseException("Error instantiating inner object", paramBufferedReader);
                              }
                              catch (IllegalAccessException paramBufferedReader)
                              {
                                throw new ParseException("Error instantiating inner object", paramBufferedReader);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            break;
          }
        }
        localObject1 = zaa(paramBufferedReader);
      }
    }
    zak(1);
    return true;
  }
  
  private final boolean zaa(BufferedReader paramBufferedReader, boolean paramBoolean)
    throws FastParser.ParseException, IOException
  {
    for (;;)
    {
      char c = zaj(paramBufferedReader);
      char[] arrayOfChar;
      switch (c)
      {
      default: 
        throw new ParseException(19 + "Unexpected token: " + c);
      case 'n': 
        zab(paramBufferedReader, zaqf);
        return false;
      case 't': 
        if (paramBoolean) {}
        for (arrayOfChar = zaqh;; arrayOfChar = zaqg)
        {
          zab(paramBufferedReader, arrayOfChar);
          return true;
        }
      case 'f': 
        if (paramBoolean) {}
        for (arrayOfChar = zaqj;; arrayOfChar = zaqi)
        {
          zab(paramBufferedReader, arrayOfChar);
          return false;
        }
      }
      if (paramBoolean) {
        throw new ParseException("No boolean value found in string");
      }
      paramBoolean = true;
    }
  }
  
  private final String zab(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    paramBufferedReader.mark(1024);
    char c;
    int i;
    int j;
    switch (zaj(paramBufferedReader))
    {
    default: 
      paramBufferedReader.reset();
      zaa(paramBufferedReader, this.zaqc);
      c = zaj(paramBufferedReader);
      switch (c)
      {
      default: 
        throw new ParseException(18 + "Unexpected token " + c);
      }
    case '"': 
      if (paramBufferedReader.read(this.zaqa) == -1) {
        throw new ParseException("Unexpected EOF while parsing string");
      }
      i = this.zaqa[0];
      j = 0;
    }
    while ((i != 34) || (j != 0))
    {
      if (i == 92) {
        if (j == 0) {
          i = 1;
        }
      }
      while (paramBufferedReader.read(this.zaqa) == -1)
      {
        throw new ParseException("Unexpected EOF while parsing string");
        i = 0;
        continue;
        i = 0;
      }
      c = this.zaqa[0];
      if (Character.isISOControl(c))
      {
        throw new ParseException("Unexpected control character while reading string");
        this.zaql.push(Integer.valueOf(1));
        paramBufferedReader.mark(32);
        c = zaj(paramBufferedReader);
        if (c == '}')
        {
          zak(1);
          break;
        }
        if (c == '"')
        {
          paramBufferedReader.reset();
          zaa(paramBufferedReader);
          while (zab(paramBufferedReader) != null) {}
          zak(1);
          break;
        }
        throw new ParseException(18 + "Unexpected token " + c);
        this.zaql.push(Integer.valueOf(5));
        paramBufferedReader.mark(32);
        if (zaj(paramBufferedReader) == ']')
        {
          zak(5);
          break;
        }
        paramBufferedReader.reset();
        int k = 1;
        i = 0;
        j = 0;
        label396:
        if (k > 0)
        {
          c = zaj(paramBufferedReader);
          if (c == 0) {
            throw new ParseException("Unexpected EOF while parsing array");
          }
          if (Character.isISOControl(c)) {
            throw new ParseException("Unexpected control character while reading array");
          }
          if ((c != '"') || (j != 0)) {
            break label569;
          }
          if (i == 0) {
            i = 1;
          }
        }
        label457:
        label569:
        for (;;)
        {
          if ((c == '[') && (i == 0)) {
            k += 1;
          }
          for (;;)
          {
            if ((c == ']') && (i == 0)) {
              k -= 1;
            }
            for (;;)
            {
              if ((c == '\\') && (i != 0))
              {
                if (j == 0) {}
                for (j = 1;; j = 0)
                {
                  break;
                  i = 0;
                  break label457;
                }
              }
              j = 0;
              break label396;
              zak(5);
              break;
              throw new ParseException("Missing value");
              zak(2);
              return zaa(paramBufferedReader);
              zak(2);
              return null;
            }
          }
        }
      }
      j = i;
      i = c;
    }
  }
  
  private static String zab(BufferedReader paramBufferedReader, char[] paramArrayOfChar1, StringBuilder paramStringBuilder, char[] paramArrayOfChar2)
    throws FastParser.ParseException, IOException
  {
    paramStringBuilder.setLength(0);
    paramBufferedReader.mark(paramArrayOfChar1.length);
    int k = 0;
    int i = 0;
    for (;;)
    {
      int n = paramBufferedReader.read(paramArrayOfChar1);
      if (n == -1) {
        break;
      }
      int j = 0;
      if (j < n)
      {
        char c = paramArrayOfChar1[j];
        if (Character.isISOControl(c))
        {
          if (paramArrayOfChar2 != null)
          {
            m = 0;
            if (m < paramArrayOfChar2.length) {
              if (paramArrayOfChar2[m] != c) {}
            }
          }
          for (int m = 1;; m = 0)
          {
            if (m != 0) {
              break label110;
            }
            throw new ParseException("Unexpected control character while reading string");
            m += 1;
            break;
          }
        }
        label110:
        if ((c == '"') && (i == 0))
        {
          paramStringBuilder.append(paramArrayOfChar1, 0, j);
          paramBufferedReader.reset();
          paramBufferedReader.skip(j + 1);
          if (k != 0) {
            return JsonUtils.unescapeString(paramStringBuilder.toString());
          }
          return paramStringBuilder.toString();
        }
        if (c == '\\') {
          if (i == 0)
          {
            i = 1;
            label178:
            k = 1;
          }
        }
        for (;;)
        {
          j += 1;
          break;
          i = 0;
          break label178;
          i = 0;
        }
      }
      paramStringBuilder.append(paramArrayOfChar1, 0, n);
      paramBufferedReader.mark(paramArrayOfChar1.length);
    }
    throw new ParseException("Unexpected EOF while parsing string");
  }
  
  private final void zab(BufferedReader paramBufferedReader, char[] paramArrayOfChar)
    throws FastParser.ParseException, IOException
  {
    int i = 0;
    while (i < paramArrayOfChar.length)
    {
      int k = paramBufferedReader.read(this.zaqb, 0, paramArrayOfChar.length - i);
      if (k == -1) {
        throw new ParseException("Unexpected EOF");
      }
      int j = 0;
      while (j < k)
      {
        if (paramArrayOfChar[(j + i)] != this.zaqb[j]) {
          throw new ParseException("Unexpected character");
        }
        j += 1;
      }
      i += k;
    }
  }
  
  private final String zac(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    return zaa(paramBufferedReader, this.zaqb, this.zaqd, null);
  }
  
  private final int zad(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    int i = 0;
    int j = 0;
    int i1 = zaa(paramBufferedReader, this.zaqc);
    if (i1 == 0) {
      i = j;
    }
    int k;
    label106:
    do
    {
      return i;
      paramBufferedReader = this.zaqc;
      int n;
      if (i1 > 0)
      {
        int m;
        if (paramBufferedReader[0] == '-')
        {
          m = Integer.MIN_VALUE;
          n = 1;
        }
        for (j = 1;; j = 0)
        {
          k = j;
          if (j >= i1) {
            break label106;
          }
          i = Character.digit(paramBufferedReader[j], 10);
          if (i >= 0) {
            break;
          }
          throw new ParseException("Unexpected non-digit character");
          m = -2147483647;
          n = 0;
        }
        i = -i;
        k = j + 1;
        while (k < i1)
        {
          j = Character.digit(paramBufferedReader[k], 10);
          if (j < 0) {
            throw new ParseException("Unexpected non-digit character");
          }
          if (i < -214748364) {
            throw new ParseException("Number too large");
          }
          i *= 10;
          if (i < m + j) {
            throw new ParseException("Number too large");
          }
          i -= j;
          k += 1;
        }
      }
      throw new ParseException("No number to parse");
      if (n == 0) {
        break;
      }
    } while (k > 1);
    throw new ParseException("No digits to parse");
    return -i;
  }
  
  private final long zae(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    long l1 = 0L;
    int m = zaa(paramBufferedReader, this.zaqc);
    if (m == 0) {}
    int j;
    label104:
    do
    {
      return l1;
      paramBufferedReader = this.zaqc;
      int k;
      if (m > 0)
      {
        long l2;
        if (paramBufferedReader[0] == '-')
        {
          l2 = Long.MIN_VALUE;
          k = 1;
        }
        for (int i = 1;; i = 0)
        {
          j = i;
          if (i >= m) {
            break label104;
          }
          j = Character.digit(paramBufferedReader[i], 10);
          if (j >= 0) {
            break;
          }
          throw new ParseException("Unexpected non-digit character");
          k = 0;
          l2 = -9223372036854775807L;
        }
        l1 = -j;
        j = i + 1;
        while (j < m)
        {
          i = Character.digit(paramBufferedReader[j], 10);
          if (i < 0) {
            throw new ParseException("Unexpected non-digit character");
          }
          if (l1 < -922337203685477580L) {
            throw new ParseException("Number too large");
          }
          l1 *= 10L;
          if (l1 < i + l2) {
            throw new ParseException("Number too large");
          }
          l1 -= i;
          j += 1;
        }
      }
      throw new ParseException("No number to parse");
      if (k == 0) {
        break;
      }
    } while (j > 1);
    throw new ParseException("No digits to parse");
    return -l1;
  }
  
  private final BigInteger zaf(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    int i = zaa(paramBufferedReader, this.zaqc);
    if (i == 0) {
      return null;
    }
    return new BigInteger(new String(this.zaqc, 0, i));
  }
  
  private final float zag(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    int i = zaa(paramBufferedReader, this.zaqc);
    if (i == 0) {
      return 0.0F;
    }
    return Float.parseFloat(new String(this.zaqc, 0, i));
  }
  
  private final double zah(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    int i = zaa(paramBufferedReader, this.zaqc);
    if (i == 0) {
      return 0.0D;
    }
    return Double.parseDouble(new String(this.zaqc, 0, i));
  }
  
  private final BigDecimal zai(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    int i = zaa(paramBufferedReader, this.zaqc);
    if (i == 0) {
      return null;
    }
    return new BigDecimal(new String(this.zaqc, 0, i));
  }
  
  private final char zaj(BufferedReader paramBufferedReader)
    throws FastParser.ParseException, IOException
  {
    if (paramBufferedReader.read(this.zaqa) == -1) {
      return '\000';
    }
    while (Character.isWhitespace(this.zaqa[0])) {
      if (paramBufferedReader.read(this.zaqa) == -1) {
        return '\000';
      }
    }
    return this.zaqa[0];
  }
  
  private final void zak(int paramInt)
    throws FastParser.ParseException
  {
    if (this.zaql.isEmpty()) {
      throw new ParseException(46 + "Expected state " + paramInt + " but had empty stack");
    }
    int i = ((Integer)this.zaql.pop()).intValue();
    if (i != paramInt) {
      throw new ParseException(46 + "Expected state " + paramInt + " but had " + i);
    }
  }
  
  @KeepForSdk
  public void parse(InputStream paramInputStream, T paramT)
    throws FastParser.ParseException
  {
    paramInputStream = new BufferedReader(new InputStreamReader(paramInputStream), 1024);
    try
    {
      this.zaql.push(Integer.valueOf(0));
      c = zaj(paramInputStream);
      switch (c)
      {
      }
    }
    catch (IOException paramT)
    {
      char c;
      throw new ParseException(paramT);
    }
    finally
    {
      for (;;)
      {
        try
        {
          paramInputStream.close();
          throw paramT;
          this.zaql.push(Integer.valueOf(1));
          zaa(paramInputStream, paramT);
          zak(0);
          try
          {
            paramInputStream.close();
            return;
          }
          catch (IOException paramInputStream)
          {
            Object localObject;
            ArrayList localArrayList;
            Log.w("FastParser", "Failed to close reader while parsing.");
            return;
          }
          this.zaql.push(Integer.valueOf(5));
          localObject = paramT.getFieldMappings();
          if (((Map)localObject).size() != 1) {
            throw new ParseException("Object array response class must have a single Field");
          }
          localObject = (FastJsonResponse.Field)((Map.Entry)((Map)localObject).entrySet().iterator().next()).getValue();
          localArrayList = zaa(paramInputStream, (FastJsonResponse.Field)localObject);
          paramT.addConcreteTypeArrayInternal((FastJsonResponse.Field)localObject, ((FastJsonResponse.Field)localObject).zapu, localArrayList);
          continue;
          throw new ParseException("No data to parse");
        }
        catch (IOException paramInputStream)
        {
          Log.w("FastParser", "Failed to close reader while parsing.");
        }
      }
    }
    throw new ParseException(19 + "Unexpected token: " + c);
  }
  
  @KeepForSdk
  @ShowFirstParty
  public static class ParseException
    extends Exception
  {
    public ParseException(String paramString)
    {
      super();
    }
    
    public ParseException(String paramString, Throwable paramThrowable)
    {
      super(paramThrowable);
    }
    
    public ParseException(Throwable paramThrowable)
    {
      super();
    }
  }
  
  private static abstract interface zaa<O>
  {
    public abstract O zah(FastParser paramFastParser, BufferedReader paramBufferedReader)
      throws FastParser.ParseException, IOException;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\common\server\response\FastParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */