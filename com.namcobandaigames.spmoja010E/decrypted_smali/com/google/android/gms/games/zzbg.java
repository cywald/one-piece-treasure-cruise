package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzy;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbg
  extends zzah<String>
{
  zzbg(RealTimeMultiplayerClient paramRealTimeMultiplayerClient, String paramString) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<String> paramTaskCompletionSource)
    throws RemoteException
  {
    paramTaskCompletionSource = new zzbh(this, paramTaskCompletionSource);
    ((zzy)paramzze.getService()).zza(paramTaskCompletionSource, this.zzdg);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */