package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.internal.zzq;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class PlayersClient
  extends zzu
{
  public static final String EXTRA_PLAYER_SEARCH_RESULTS = "player_search_results";
  private static final PendingResultUtil.ResultConverter<Players.LoadPlayersResult, PlayerBuffer> zzcz = new zzax();
  private static final zzq<Players.LoadPlayersResult> zzda = new zzay();
  private static final PendingResultUtil.ResultConverter<Players.LoadPlayersResult, Player> zzdb = new zzaz();
  
  PlayersClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  PlayersClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<Intent> getCompareProfileIntent(@NonNull Player paramPlayer)
  {
    return doRead(new zzav(this, paramPlayer));
  }
  
  public Task<Player> getCurrentPlayer()
  {
    return doRead(new zzau(this));
  }
  
  public Task<String> getCurrentPlayerId()
  {
    return doRead(new zzat(this));
  }
  
  public Task<Intent> getPlayerSearchIntent()
  {
    return doRead(new zzaw(this));
  }
  
  public Task<AnnotatedData<PlayerBuffer>> loadMoreRecentlyPlayedWithPlayers(@IntRange(from=1L, to=25L) int paramInt)
  {
    return zzi.zzb(Games.Players.loadMoreRecentlyPlayedWithPlayers(asGoogleApiClient(), paramInt), zzcz);
  }
  
  public Task<AnnotatedData<Player>> loadPlayer(@NonNull String paramString)
  {
    return loadPlayer(paramString, false);
  }
  
  public Task<AnnotatedData<Player>> loadPlayer(@NonNull String paramString, boolean paramBoolean)
  {
    return zzi.zza(Games.Players.loadPlayer(asGoogleApiClient(), paramString, paramBoolean), zzdb, zzda);
  }
  
  public Task<AnnotatedData<PlayerBuffer>> loadRecentlyPlayedWithPlayers(@IntRange(from=1L, to=25L) int paramInt, boolean paramBoolean)
  {
    return zzi.zzb(Games.Players.loadRecentlyPlayedWithPlayers(asGoogleApiClient(), paramInt, paramBoolean), zzcz);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\PlayersClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */