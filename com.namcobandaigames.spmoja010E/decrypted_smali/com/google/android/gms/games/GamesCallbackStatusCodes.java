package com.google.android.gms.games;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class GamesCallbackStatusCodes
{
  @Deprecated
  public static final int CLIENT_RECONNECT_REQUIRED = 2;
  public static final int INTERNAL_ERROR = 1;
  public static final int MULTIPLAYER_DISABLED = 6003;
  public static final int OK = 0;
  public static final int REAL_TIME_CONNECTION_FAILED = 7000;
  public static final int REAL_TIME_MESSAGE_SEND_FAILED = 7001;
  public static final int REAL_TIME_ROOM_NOT_JOINED = 7004;
  
  public static String getStatusCodeString(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return 47 + "unknown games callback status code: " + paramInt;
    case 0: 
      return "OK";
    case 1: 
      return "INTERNAL_ERROR";
    case 2: 
      return "CLIENT_RECONNECT_REQUIRED";
    case 6003: 
      return "MULTIPLAYER_DISABLED";
    case 7000: 
      return "REAL_TIME_CONNECTION_FAILED";
    case 7001: 
      return "REAL_TIME_MESSAGE_SEND_FAILED";
    }
    return "REAL_TIME_ROOM_NOT_JOINED";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OnJoinedRoomStatusCodes {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OnLeftRoomStatusCodes {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OnRealTimeMessageSentStatusCodes {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OnRoomConnectedStatusCodes {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OnRoomCreatedStatusCodes {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\GamesCallbackStatusCodes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */