package com.google.android.gms.games.event;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="EventEntityCreator")
@SafeParcelable.Reserved({1000})
public final class EventEntity
  extends zzd
  implements Event
{
  public static final Parcelable.Creator<EventEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getDescription", id=3)
  private final String description;
  @SafeParcelable.Field(getter="getName", id=2)
  private final String name;
  @SafeParcelable.Field(getter="getValue", id=7)
  private final long value;
  @SafeParcelable.Field(getter="getIconImageUrl", id=5)
  private final String zzac;
  @SafeParcelable.Field(getter="getPlayer", id=6)
  private final PlayerEntity zzfh;
  @SafeParcelable.Field(getter="getEventId", id=1)
  private final String zzfm;
  @SafeParcelable.Field(getter="getFormattedValue", id=8)
  private final String zzfn;
  @SafeParcelable.Field(getter="isVisible", id=9)
  private final boolean zzfo;
  @SafeParcelable.Field(getter="getIconImageUri", id=4)
  private final Uri zzr;
  
  public EventEntity(Event paramEvent)
  {
    this.zzfm = paramEvent.getEventId();
    this.name = paramEvent.getName();
    this.description = paramEvent.getDescription();
    this.zzr = paramEvent.getIconImageUri();
    this.zzac = paramEvent.getIconImageUrl();
    this.zzfh = ((PlayerEntity)paramEvent.getPlayer().freeze());
    this.value = paramEvent.getValue();
    this.zzfn = paramEvent.getFormattedValue();
    this.zzfo = paramEvent.isVisible();
  }
  
  @SafeParcelable.Constructor
  EventEntity(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) String paramString2, @SafeParcelable.Param(id=3) String paramString3, @SafeParcelable.Param(id=4) Uri paramUri, @SafeParcelable.Param(id=5) String paramString4, @SafeParcelable.Param(id=6) Player paramPlayer, @SafeParcelable.Param(id=7) long paramLong, @SafeParcelable.Param(id=8) String paramString5, @SafeParcelable.Param(id=9) boolean paramBoolean)
  {
    this.zzfm = paramString1;
    this.name = paramString2;
    this.description = paramString3;
    this.zzr = paramUri;
    this.zzac = paramString4;
    this.zzfh = new PlayerEntity(paramPlayer);
    this.value = paramLong;
    this.zzfn = paramString5;
    this.zzfo = paramBoolean;
  }
  
  static int zza(Event paramEvent)
  {
    return Objects.hashCode(new Object[] { paramEvent.getEventId(), paramEvent.getName(), paramEvent.getDescription(), paramEvent.getIconImageUri(), paramEvent.getIconImageUrl(), paramEvent.getPlayer(), Long.valueOf(paramEvent.getValue()), paramEvent.getFormattedValue(), Boolean.valueOf(paramEvent.isVisible()) });
  }
  
  static boolean zza(Event paramEvent, Object paramObject)
  {
    if (!(paramObject instanceof Event)) {}
    do
    {
      return false;
      if (paramEvent == paramObject) {
        return true;
      }
      paramObject = (Event)paramObject;
    } while ((!Objects.equal(((Event)paramObject).getEventId(), paramEvent.getEventId())) || (!Objects.equal(((Event)paramObject).getName(), paramEvent.getName())) || (!Objects.equal(((Event)paramObject).getDescription(), paramEvent.getDescription())) || (!Objects.equal(((Event)paramObject).getIconImageUri(), paramEvent.getIconImageUri())) || (!Objects.equal(((Event)paramObject).getIconImageUrl(), paramEvent.getIconImageUrl())) || (!Objects.equal(((Event)paramObject).getPlayer(), paramEvent.getPlayer())) || (!Objects.equal(Long.valueOf(((Event)paramObject).getValue()), Long.valueOf(paramEvent.getValue()))) || (!Objects.equal(((Event)paramObject).getFormattedValue(), paramEvent.getFormattedValue())) || (!Objects.equal(Boolean.valueOf(((Event)paramObject).isVisible()), Boolean.valueOf(paramEvent.isVisible()))));
    return true;
  }
  
  static String zzb(Event paramEvent)
  {
    return Objects.toStringHelper(paramEvent).add("Id", paramEvent.getEventId()).add("Name", paramEvent.getName()).add("Description", paramEvent.getDescription()).add("IconImageUri", paramEvent.getIconImageUri()).add("IconImageUrl", paramEvent.getIconImageUrl()).add("Player", paramEvent.getPlayer()).add("Value", Long.valueOf(paramEvent.getValue())).add("FormattedValue", paramEvent.getFormattedValue()).add("isVisible", Boolean.valueOf(paramEvent.isVisible())).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final Event freeze()
  {
    return this;
  }
  
  public final String getDescription()
  {
    return this.description;
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.description, paramCharArrayBuffer);
  }
  
  public final String getEventId()
  {
    return this.zzfm;
  }
  
  public final String getFormattedValue()
  {
    return this.zzfn;
  }
  
  public final void getFormattedValue(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.zzfn, paramCharArrayBuffer);
  }
  
  public final Uri getIconImageUri()
  {
    return this.zzr;
  }
  
  public final String getIconImageUrl()
  {
    return this.zzac;
  }
  
  public final String getName()
  {
    return this.name;
  }
  
  public final void getName(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.name, paramCharArrayBuffer);
  }
  
  public final Player getPlayer()
  {
    return this.zzfh;
  }
  
  public final long getValue()
  {
    return this.value;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final boolean isVisible()
  {
    return this.zzfo;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, getEventId(), false);
    SafeParcelWriter.writeString(paramParcel, 2, getName(), false);
    SafeParcelWriter.writeString(paramParcel, 3, getDescription(), false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, getIconImageUri(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 5, getIconImageUrl(), false);
    SafeParcelWriter.writeParcelable(paramParcel, 6, getPlayer(), paramInt, false);
    SafeParcelWriter.writeLong(paramParcel, 7, getValue());
    SafeParcelWriter.writeString(paramParcel, 8, getFormattedValue(), false);
    SafeParcelWriter.writeBoolean(paramParcel, 9, isVisible());
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\event\EventEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */