package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.internal.zzq;
import com.google.android.gms.games.internal.zzr;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.Leaderboards.LeaderboardMetadataResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadScoresResult;
import com.google.android.gms.games.leaderboard.Leaderboards.SubmitScoreResult;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class LeaderboardsClient
  extends zzu
{
  private static final PendingResultUtil.ResultConverter<Leaderboards.LeaderboardMetadataResult, LeaderboardBuffer> zzbj = new zzal();
  private static final PendingResultUtil.ResultConverter<Leaderboards.LeaderboardMetadataResult, Leaderboard> zzbk = new zzam();
  private static final zzq<Leaderboards.LeaderboardMetadataResult> zzbl = new zzan();
  private static final PendingResultUtil.ResultConverter<Leaderboards.LoadPlayerScoreResult, LeaderboardScore> zzbm = new zzac();
  private static final zzr zzbn = new zzad();
  private static final PendingResultUtil.ResultConverter<Leaderboards.SubmitScoreResult, ScoreSubmissionData> zzbo = new zzae();
  private static final PendingResultUtil.ResultConverter<Leaderboards.LoadScoresResult, LeaderboardScores> zzbp = new zzaf();
  
  LeaderboardsClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  LeaderboardsClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<Intent> getAllLeaderboardsIntent()
  {
    return doRead(new zzab(this));
  }
  
  public Task<Intent> getLeaderboardIntent(@NonNull String paramString)
  {
    return doRead(new zzag(this, paramString));
  }
  
  public Task<Intent> getLeaderboardIntent(@NonNull String paramString, int paramInt)
  {
    return doRead(new zzah(this, paramString, paramInt));
  }
  
  public Task<Intent> getLeaderboardIntent(@NonNull String paramString, int paramInt1, int paramInt2)
  {
    return doRead(new zzai(this, paramString, paramInt1, paramInt2));
  }
  
  public Task<AnnotatedData<LeaderboardScore>> loadCurrentPlayerLeaderboardScore(@NonNull String paramString, int paramInt1, int paramInt2)
  {
    return zzi.zza(Games.Leaderboards.loadCurrentPlayerLeaderboardScore(asGoogleApiClient(), paramString, paramInt1, paramInt2), zzbm);
  }
  
  public Task<AnnotatedData<Leaderboard>> loadLeaderboardMetadata(@NonNull String paramString, boolean paramBoolean)
  {
    return zzi.zza(Games.Leaderboards.loadLeaderboardMetadata(asGoogleApiClient(), paramString, paramBoolean), zzbk, zzbl);
  }
  
  public Task<AnnotatedData<LeaderboardBuffer>> loadLeaderboardMetadata(boolean paramBoolean)
  {
    return zzi.zzb(Games.Leaderboards.loadLeaderboardMetadata(asGoogleApiClient(), paramBoolean), zzbj);
  }
  
  public Task<AnnotatedData<LeaderboardScores>> loadMoreScores(@NonNull LeaderboardScoreBuffer paramLeaderboardScoreBuffer, @IntRange(from=1L, to=25L) int paramInt1, int paramInt2)
  {
    return zzi.zzb(Games.Leaderboards.loadMoreScores(asGoogleApiClient(), paramLeaderboardScoreBuffer, paramInt1, paramInt2), zzbp);
  }
  
  public Task<AnnotatedData<LeaderboardScores>> loadPlayerCenteredScores(@NonNull String paramString, int paramInt1, int paramInt2, @IntRange(from=1L, to=25L) int paramInt3)
  {
    return zzi.zzb(Games.Leaderboards.loadPlayerCenteredScores(asGoogleApiClient(), paramString, paramInt1, paramInt2, paramInt3), zzbp);
  }
  
  public Task<AnnotatedData<LeaderboardScores>> loadPlayerCenteredScores(@NonNull String paramString, int paramInt1, int paramInt2, @IntRange(from=1L, to=25L) int paramInt3, boolean paramBoolean)
  {
    return zzi.zzb(Games.Leaderboards.loadPlayerCenteredScores(asGoogleApiClient(), paramString, paramInt1, paramInt2, paramInt3, paramBoolean), zzbp);
  }
  
  public Task<AnnotatedData<LeaderboardScores>> loadTopScores(@NonNull String paramString, int paramInt1, int paramInt2, @IntRange(from=1L, to=25L) int paramInt3)
  {
    return zzi.zzb(Games.Leaderboards.loadTopScores(asGoogleApiClient(), paramString, paramInt1, paramInt2, paramInt3), zzbp);
  }
  
  public Task<AnnotatedData<LeaderboardScores>> loadTopScores(@NonNull String paramString, int paramInt1, int paramInt2, @IntRange(from=1L, to=25L) int paramInt3, boolean paramBoolean)
  {
    return zzi.zzb(Games.Leaderboards.loadTopScores(asGoogleApiClient(), paramString, paramInt1, paramInt2, paramInt3, paramBoolean), zzbp);
  }
  
  public void submitScore(@NonNull String paramString, long paramLong)
  {
    doWrite(new zzaj(this, paramString, paramLong));
  }
  
  public void submitScore(@NonNull String paramString1, long paramLong, @NonNull String paramString2)
  {
    doWrite(new zzak(this, paramString1, paramLong, paramString2));
  }
  
  public Task<ScoreSubmissionData> submitScoreImmediate(@NonNull String paramString, long paramLong)
  {
    return zzi.zza(Games.Leaderboards.submitScoreImmediate(asGoogleApiClient(), paramString, paramLong), zzbn, zzbo);
  }
  
  public Task<ScoreSubmissionData> submitScoreImmediate(@NonNull String paramString1, long paramLong, @NonNull String paramString2)
  {
    return zzi.zza(Games.Leaderboards.submitScoreImmediate(asGoogleApiClient(), paramString1, paramLong, paramString2), zzbn, zzbo);
  }
  
  public static class LeaderboardScores
    implements Releasable
  {
    private final Leaderboard zzbv;
    private final LeaderboardScoreBuffer zzbw;
    
    LeaderboardScores(@Nullable Leaderboard paramLeaderboard, @NonNull LeaderboardScoreBuffer paramLeaderboardScoreBuffer)
    {
      this.zzbv = paramLeaderboard;
      this.zzbw = paramLeaderboardScoreBuffer;
    }
    
    @Nullable
    public Leaderboard getLeaderboard()
    {
      return this.zzbv;
    }
    
    @NonNull
    public LeaderboardScoreBuffer getScores()
    {
      return this.zzbw;
    }
    
    public void release()
    {
      if (this.zzbw != null) {
        this.zzbw.release();
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\LeaderboardsClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */