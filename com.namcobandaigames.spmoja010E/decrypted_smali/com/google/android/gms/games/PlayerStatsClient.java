package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.Stats;
import com.google.android.gms.games.stats.Stats.LoadPlayerStatsResult;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class PlayerStatsClient
  extends zzu
{
  private static final PendingResultUtil.ResultConverter<Stats.LoadPlayerStatsResult, PlayerStats> zzcy = new zzas();
  
  PlayerStatsClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  PlayerStatsClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<AnnotatedData<PlayerStats>> loadPlayerStats(boolean paramBoolean)
  {
    return zzi.zza(Games.Stats.loadPlayerStats(asGoogleApiClient(), paramBoolean), zzcy);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\PlayerStatsClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */