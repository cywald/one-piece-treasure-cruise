package com.google.android.gms.games.achievement;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="AchievementEntityCreator")
@SafeParcelable.Reserved({1000})
public final class AchievementEntity
  extends zzd
  implements Achievement
{
  public static final Parcelable.Creator<AchievementEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getDescription", id=4)
  private final String description;
  @SafeParcelable.Field(getter="getName", id=3)
  private final String name;
  @SafeParcelable.Field(getter="getState", id=12)
  private final int state;
  @SafeParcelable.Field(getter="getType", id=2)
  private final int type;
  @SafeParcelable.Field(getter="getAchievementId", id=1)
  private final String zzfa;
  @SafeParcelable.Field(getter="getUnlockedImageUri", id=5)
  private final Uri zzfb;
  @SafeParcelable.Field(getter="getUnlockedImageUrl", id=6)
  private final String zzfc;
  @SafeParcelable.Field(getter="getRevealedImageUri", id=7)
  private final Uri zzfd;
  @SafeParcelable.Field(getter="getRevealedImageUrl", id=8)
  private final String zzfe;
  @SafeParcelable.Field(getter="getTotalStepsRaw", id=9)
  private final int zzff;
  @SafeParcelable.Field(getter="getFormattedTotalStepsRaw", id=10)
  private final String zzfg;
  @SafeParcelable.Field(getter="getPlayer", id=11)
  private final PlayerEntity zzfh;
  @SafeParcelable.Field(getter="getCurrentStepsRaw", id=13)
  private final int zzfi;
  @SafeParcelable.Field(getter="getFormattedCurrentStepsRaw", id=14)
  private final String zzfj;
  @SafeParcelable.Field(getter="getLastUpdatedTimestamp", id=15)
  private final long zzfk;
  @SafeParcelable.Field(getter="getXpValue", id=16)
  private final long zzfl;
  
  public AchievementEntity(Achievement paramAchievement)
  {
    this.zzfa = paramAchievement.getAchievementId();
    this.type = paramAchievement.getType();
    this.name = paramAchievement.getName();
    this.description = paramAchievement.getDescription();
    this.zzfb = paramAchievement.getUnlockedImageUri();
    this.zzfc = paramAchievement.getUnlockedImageUrl();
    this.zzfd = paramAchievement.getRevealedImageUri();
    this.zzfe = paramAchievement.getRevealedImageUrl();
    this.zzfh = ((PlayerEntity)paramAchievement.getPlayer().freeze());
    this.state = paramAchievement.getState();
    this.zzfk = paramAchievement.getLastUpdatedTimestamp();
    this.zzfl = paramAchievement.getXpValue();
    if (paramAchievement.getType() == 1)
    {
      this.zzff = paramAchievement.getTotalSteps();
      this.zzfg = paramAchievement.getFormattedTotalSteps();
      this.zzfi = paramAchievement.getCurrentSteps();
    }
    for (this.zzfj = paramAchievement.getFormattedCurrentSteps();; this.zzfj = null)
    {
      Asserts.checkNotNull(this.zzfa);
      Asserts.checkNotNull(this.description);
      return;
      this.zzff = 0;
      this.zzfg = null;
      this.zzfi = 0;
    }
  }
  
  @SafeParcelable.Constructor
  AchievementEntity(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) int paramInt1, @SafeParcelable.Param(id=3) String paramString2, @SafeParcelable.Param(id=4) String paramString3, @SafeParcelable.Param(id=5) Uri paramUri1, @SafeParcelable.Param(id=6) String paramString4, @SafeParcelable.Param(id=7) Uri paramUri2, @SafeParcelable.Param(id=8) String paramString5, @SafeParcelable.Param(id=9) int paramInt2, @SafeParcelable.Param(id=10) String paramString6, @SafeParcelable.Param(id=11) PlayerEntity paramPlayerEntity, @SafeParcelable.Param(id=12) int paramInt3, @SafeParcelable.Param(id=13) int paramInt4, @SafeParcelable.Param(id=14) String paramString7, @SafeParcelable.Param(id=15) long paramLong1, @SafeParcelable.Param(id=16) long paramLong2)
  {
    this.zzfa = paramString1;
    this.type = paramInt1;
    this.name = paramString2;
    this.description = paramString3;
    this.zzfb = paramUri1;
    this.zzfc = paramString4;
    this.zzfd = paramUri2;
    this.zzfe = paramString5;
    this.zzff = paramInt2;
    this.zzfg = paramString6;
    this.zzfh = paramPlayerEntity;
    this.state = paramInt3;
    this.zzfi = paramInt4;
    this.zzfj = paramString7;
    this.zzfk = paramLong1;
    this.zzfl = paramLong2;
  }
  
  static String zza(Achievement paramAchievement)
  {
    Objects.ToStringHelper localToStringHelper = Objects.toStringHelper(paramAchievement).add("Id", paramAchievement.getAchievementId()).add("Type", Integer.valueOf(paramAchievement.getType())).add("Name", paramAchievement.getName()).add("Description", paramAchievement.getDescription()).add("Player", paramAchievement.getPlayer()).add("State", Integer.valueOf(paramAchievement.getState()));
    if (paramAchievement.getType() == 1)
    {
      localToStringHelper.add("CurrentSteps", Integer.valueOf(paramAchievement.getCurrentSteps()));
      localToStringHelper.add("TotalSteps", Integer.valueOf(paramAchievement.getTotalSteps()));
    }
    return localToStringHelper.toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof Achievement))
    {
      if (this == paramObject) {}
      do
      {
        return true;
        paramObject = (Achievement)paramObject;
      } while ((((Achievement)paramObject).getType() == getType()) && ((getType() != 1) || ((((Achievement)paramObject).getCurrentSteps() == getCurrentSteps()) && (((Achievement)paramObject).getTotalSteps() == getTotalSteps()))) && (((Achievement)paramObject).getXpValue() == getXpValue()) && (((Achievement)paramObject).getState() == getState()) && (((Achievement)paramObject).getLastUpdatedTimestamp() == getLastUpdatedTimestamp()) && (Objects.equal(((Achievement)paramObject).getAchievementId(), getAchievementId())) && (Objects.equal(((Achievement)paramObject).getName(), getName())) && (Objects.equal(((Achievement)paramObject).getDescription(), getDescription())) && (Objects.equal(((Achievement)paramObject).getPlayer(), getPlayer())));
    }
    return false;
  }
  
  public final Achievement freeze()
  {
    return this;
  }
  
  public final String getAchievementId()
  {
    return this.zzfa;
  }
  
  public final int getCurrentSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      return this.zzfi;
      bool = false;
    }
  }
  
  public final String getDescription()
  {
    return this.description;
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.description, paramCharArrayBuffer);
  }
  
  public final String getFormattedCurrentSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      return this.zzfj;
      bool = false;
    }
  }
  
  public final void getFormattedCurrentSteps(CharArrayBuffer paramCharArrayBuffer)
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      DataUtils.copyStringToBuffer(this.zzfj, paramCharArrayBuffer);
      return;
      bool = false;
    }
  }
  
  public final String getFormattedTotalSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      return this.zzfg;
      bool = false;
    }
  }
  
  public final void getFormattedTotalSteps(CharArrayBuffer paramCharArrayBuffer)
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      DataUtils.copyStringToBuffer(this.zzfg, paramCharArrayBuffer);
      return;
      bool = false;
    }
  }
  
  public final long getLastUpdatedTimestamp()
  {
    return this.zzfk;
  }
  
  public final String getName()
  {
    return this.name;
  }
  
  public final void getName(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.name, paramCharArrayBuffer);
  }
  
  public final Player getPlayer()
  {
    return this.zzfh;
  }
  
  public final Uri getRevealedImageUri()
  {
    return this.zzfd;
  }
  
  public final String getRevealedImageUrl()
  {
    return this.zzfe;
  }
  
  public final int getState()
  {
    return this.state;
  }
  
  public final int getTotalSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      return this.zzff;
      bool = false;
    }
  }
  
  public final int getType()
  {
    return this.type;
  }
  
  public final Uri getUnlockedImageUri()
  {
    return this.zzfb;
  }
  
  public final String getUnlockedImageUrl()
  {
    return this.zzfc;
  }
  
  public final long getXpValue()
  {
    return this.zzfl;
  }
  
  public final int hashCode()
  {
    int j;
    int i;
    if (getType() == 1)
    {
      j = getCurrentSteps();
      i = getTotalSteps();
    }
    for (;;)
    {
      return Objects.hashCode(new Object[] { getAchievementId(), getName(), Integer.valueOf(getType()), getDescription(), Long.valueOf(getXpValue()), Integer.valueOf(getState()), Long.valueOf(getLastUpdatedTimestamp()), getPlayer(), Integer.valueOf(j), Integer.valueOf(i) });
      i = 0;
      j = 0;
    }
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zza(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, getAchievementId(), false);
    SafeParcelWriter.writeInt(paramParcel, 2, getType());
    SafeParcelWriter.writeString(paramParcel, 3, getName(), false);
    SafeParcelWriter.writeString(paramParcel, 4, getDescription(), false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, getUnlockedImageUri(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 6, getUnlockedImageUrl(), false);
    SafeParcelWriter.writeParcelable(paramParcel, 7, getRevealedImageUri(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 8, getRevealedImageUrl(), false);
    SafeParcelWriter.writeInt(paramParcel, 9, this.zzff);
    SafeParcelWriter.writeString(paramParcel, 10, this.zzfg, false);
    SafeParcelWriter.writeParcelable(paramParcel, 11, getPlayer(), paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 12, getState());
    SafeParcelWriter.writeInt(paramParcel, 13, this.zzfi);
    SafeParcelWriter.writeString(paramParcel, 14, this.zzfj, false);
    SafeParcelWriter.writeLong(paramParcel, 15, getLastUpdatedTimestamp());
    SafeParcelWriter.writeLong(paramParcel, 16, getXpValue());
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\achievement\AchievementEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */