package com.google.android.gms.games.achievement;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class AchievementRef
  extends DataBufferRef
  implements Achievement
{
  AchievementRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final String getAchievementId()
  {
    return getString("external_achievement_id");
  }
  
  public final int getCurrentSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      return getInteger("current_steps");
      bool = false;
    }
  }
  
  public final String getDescription()
  {
    return getString("description");
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    copyToBuffer("description", paramCharArrayBuffer);
  }
  
  public final String getFormattedCurrentSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      return getString("formatted_current_steps");
      bool = false;
    }
  }
  
  public final void getFormattedCurrentSteps(CharArrayBuffer paramCharArrayBuffer)
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      copyToBuffer("formatted_current_steps", paramCharArrayBuffer);
      return;
      bool = false;
    }
  }
  
  public final String getFormattedTotalSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      return getString("formatted_total_steps");
      bool = false;
    }
  }
  
  public final void getFormattedTotalSteps(CharArrayBuffer paramCharArrayBuffer)
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      copyToBuffer("formatted_total_steps", paramCharArrayBuffer);
      return;
      bool = false;
    }
  }
  
  public final long getLastUpdatedTimestamp()
  {
    return getLong("last_updated_timestamp");
  }
  
  public final String getName()
  {
    return getString("name");
  }
  
  public final void getName(CharArrayBuffer paramCharArrayBuffer)
  {
    copyToBuffer("name", paramCharArrayBuffer);
  }
  
  public final Player getPlayer()
  {
    return new PlayerRef(this.mDataHolder, this.mDataRow);
  }
  
  public final Uri getRevealedImageUri()
  {
    return parseUri("revealed_icon_image_uri");
  }
  
  public final String getRevealedImageUrl()
  {
    return getString("revealed_icon_image_url");
  }
  
  public final int getState()
  {
    return getInteger("state");
  }
  
  public final int getTotalSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      Asserts.checkState(bool);
      return getInteger("total_steps");
      bool = false;
    }
  }
  
  public final int getType()
  {
    return getInteger("type");
  }
  
  public final Uri getUnlockedImageUri()
  {
    return parseUri("unlocked_icon_image_uri");
  }
  
  public final String getUnlockedImageUrl()
  {
    return getString("unlocked_icon_image_url");
  }
  
  public final long getXpValue()
  {
    if ((!hasColumn("instance_xp_value")) || (hasNull("instance_xp_value"))) {
      return getLong("definition_xp_value");
    }
    return getLong("instance_xp_value");
  }
  
  public final String toString()
  {
    return AchievementEntity.zza(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AchievementEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\achievement\AchievementRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */