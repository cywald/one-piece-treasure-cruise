package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.view.View;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class GamesClient
  extends zzu
{
  GamesClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  GamesClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<Bundle> getActivationHint()
  {
    return doRead(new zzs(this));
  }
  
  public Task<String> getAppId()
  {
    return doRead(new zzq(this));
  }
  
  @RequiresPermission("android.permission.GET_ACCOUNTS")
  public Task<String> getCurrentAccountName()
  {
    return doRead(new zzp(this));
  }
  
  @KeepForSdk
  public Task<Integer> getSdkVariant()
  {
    return doRead(new zzt(this));
  }
  
  public Task<Intent> getSettingsIntent()
  {
    return doRead(new zzr(this));
  }
  
  public Task<Void> setGravityForPopups(int paramInt)
  {
    return doWrite(new zzn(this, paramInt));
  }
  
  public Task<Void> setViewForPopups(@NonNull View paramView)
  {
    return doWrite(new zzo(this, paramView));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\GamesClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */