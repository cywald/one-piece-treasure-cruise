package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder.ListenerKey;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzt;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcp
  extends zzt<OnTurnBasedMatchUpdateReceivedListener>
{
  zzcp(TurnBasedMultiplayerClient paramTurnBasedMultiplayerClient, ListenerHolder.ListenerKey paramListenerKey)
  {
    super(paramListenerKey);
  }
  
  protected final void zzc(zze paramzze, TaskCompletionSource<Boolean> paramTaskCompletionSource)
    throws RemoteException, SecurityException
  {
    paramzze.zzac();
    paramTaskCompletionSource.setResult(Boolean.valueOf(true));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzcp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */