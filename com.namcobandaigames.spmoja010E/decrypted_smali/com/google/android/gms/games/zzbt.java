package com.google.android.gms.games;

import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.Snapshots.OpenSnapshotResult;

final class zzbt
  implements PendingResultUtil.ResultConverter<Snapshots.OpenSnapshotResult, SnapshotsClient.DataOrConflict<Snapshot>>
{}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */