package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;

final class zzv
  implements PendingResultUtil.ResultConverter<GamesMetadata.LoadGamesResult, Game>
{
  private static Game zza(@Nullable GamesMetadata.LoadGamesResult paramLoadGamesResult)
  {
    if (paramLoadGamesResult == null) {}
    do
    {
      return null;
      paramLoadGamesResult = paramLoadGamesResult.getGames();
    } while (paramLoadGamesResult == null);
    try
    {
      if (paramLoadGamesResult.getCount() > 0)
      {
        Game localGame = (Game)((Game)paramLoadGamesResult.get(0)).freeze();
        return localGame;
      }
      return null;
    }
    finally
    {
      paramLoadGamesResult.release();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */