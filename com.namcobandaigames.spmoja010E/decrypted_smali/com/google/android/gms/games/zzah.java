package com.google.android.gms.games;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzah
  extends com.google.android.gms.internal.games.zzah<Intent>
{
  zzah(LeaderboardsClient paramLeaderboardsClient, String paramString, int paramInt) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Intent> paramTaskCompletionSource)
    throws RemoteException
  {
    paramTaskCompletionSource.setResult(paramzze.zza(this.zzbq, this.zzbr, -1));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */