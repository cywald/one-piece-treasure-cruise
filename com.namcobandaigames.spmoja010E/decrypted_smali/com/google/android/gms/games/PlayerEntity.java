package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.player.zza;
import com.google.android.gms.games.internal.player.zzb;

@SafeParcelable.Class(creator="PlayerEntityCreator")
@SafeParcelable.Reserved({1000})
@RetainForClient
public final class PlayerEntity
  extends GamesDowngradeableSafeParcel
  implements Player
{
  public static final Parcelable.Creator<PlayerEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getName", id=21)
  private final String name;
  @SafeParcelable.Field(getter="getIconImageUrl", id=8)
  private final String zzac;
  @SafeParcelable.Field(getter="getHiResImageUrl", id=9)
  private final String zzad;
  @SafeParcelable.Field(getter="getPlayerId", id=1)
  private String zzby;
  @SafeParcelable.Field(getter="getRetrievedTimestamp", id=5)
  private final long zzbz;
  @SafeParcelable.Field(getter="isInCircles", id=6)
  private final int zzca;
  @SafeParcelable.Field(getter="getLastPlayedWithTimestamp", id=7)
  private final long zzcb;
  @SafeParcelable.Field(getter="getTitle", id=14)
  private final String zzcc;
  @SafeParcelable.Field(getter="getMostRecentGameInfo", id=15)
  private final zzb zzcd;
  @SafeParcelable.Field(getter="getLevelInfo", id=16)
  private final PlayerLevelInfo zzce;
  @SafeParcelable.Field(getter="isProfileVisible", id=18)
  private final boolean zzcf;
  @SafeParcelable.Field(getter="hasDebugAccess", id=19)
  private final boolean zzcg;
  @SafeParcelable.Field(getter="getGamerTag", id=20)
  private final String zzch;
  @SafeParcelable.Field(getter="getBannerImageLandscapeUri", id=22)
  private final Uri zzci;
  @SafeParcelable.Field(getter="getBannerImageLandscapeUrl", id=23)
  private final String zzcj;
  @SafeParcelable.Field(getter="getBannerImagePortraitUri", id=24)
  private final Uri zzck;
  @SafeParcelable.Field(getter="getBannerImagePortraitUrl", id=25)
  private final String zzcl;
  @SafeParcelable.Field(getter="getGamerFriendStatus", id=26)
  private final int zzcm;
  @SafeParcelable.Field(getter="getGamerFriendUpdateTimestamp", id=27)
  private final long zzcn;
  @SafeParcelable.Field(getter="isMuted", id=28)
  private final boolean zzco;
  @SafeParcelable.Field(getter="getDisplayName", id=2)
  private String zzn;
  @SafeParcelable.Field(getter="getIconImageUri", id=3)
  private final Uri zzr;
  @SafeParcelable.Field(getter="getHiResImageUri", id=4)
  private final Uri zzs;
  
  public PlayerEntity(Player paramPlayer)
  {
    this(paramPlayer, true);
  }
  
  private PlayerEntity(Player paramPlayer, boolean paramBoolean)
  {
    this.zzby = paramPlayer.getPlayerId();
    this.zzn = paramPlayer.getDisplayName();
    this.zzr = paramPlayer.getIconImageUri();
    this.zzac = paramPlayer.getIconImageUrl();
    this.zzs = paramPlayer.getHiResImageUri();
    this.zzad = paramPlayer.getHiResImageUrl();
    this.zzbz = paramPlayer.getRetrievedTimestamp();
    this.zzca = paramPlayer.zzi();
    this.zzcb = paramPlayer.getLastPlayedWithTimestamp();
    this.zzcc = paramPlayer.getTitle();
    this.zzcf = paramPlayer.zzj();
    Object localObject = paramPlayer.zzk();
    if (localObject == null)
    {
      localObject = null;
      this.zzcd = ((zzb)localObject);
      this.zzce = paramPlayer.getLevelInfo();
      this.zzcg = paramPlayer.zzh();
      this.zzch = paramPlayer.zzg();
      this.name = paramPlayer.getName();
      this.zzci = paramPlayer.getBannerImageLandscapeUri();
      this.zzcj = paramPlayer.getBannerImageLandscapeUrl();
      this.zzck = paramPlayer.getBannerImagePortraitUri();
      this.zzcl = paramPlayer.getBannerImagePortraitUrl();
      this.zzcm = paramPlayer.zzl();
      this.zzcn = paramPlayer.zzm();
      this.zzco = paramPlayer.isMuted();
      Asserts.checkNotNull(this.zzby);
      Asserts.checkNotNull(this.zzn);
      if (this.zzbz <= 0L) {
        break label284;
      }
    }
    label284:
    for (paramBoolean = true;; paramBoolean = false)
    {
      Asserts.checkState(paramBoolean);
      return;
      localObject = new zzb((zza)localObject);
      break;
    }
  }
  
  @SafeParcelable.Constructor
  PlayerEntity(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) String paramString2, @SafeParcelable.Param(id=3) Uri paramUri1, @SafeParcelable.Param(id=4) Uri paramUri2, @SafeParcelable.Param(id=5) long paramLong1, @SafeParcelable.Param(id=6) int paramInt1, @SafeParcelable.Param(id=7) long paramLong2, @SafeParcelable.Param(id=8) String paramString3, @SafeParcelable.Param(id=9) String paramString4, @SafeParcelable.Param(id=14) String paramString5, @SafeParcelable.Param(id=15) zzb paramzzb, @SafeParcelable.Param(id=16) PlayerLevelInfo paramPlayerLevelInfo, @SafeParcelable.Param(id=18) boolean paramBoolean1, @SafeParcelable.Param(id=19) boolean paramBoolean2, @SafeParcelable.Param(id=20) String paramString6, @SafeParcelable.Param(id=21) String paramString7, @SafeParcelable.Param(id=22) Uri paramUri3, @SafeParcelable.Param(id=23) String paramString8, @SafeParcelable.Param(id=24) Uri paramUri4, @SafeParcelable.Param(id=25) String paramString9, @SafeParcelable.Param(id=26) int paramInt2, @SafeParcelable.Param(id=27) long paramLong3, @SafeParcelable.Param(id=28) boolean paramBoolean3)
  {
    this.zzby = paramString1;
    this.zzn = paramString2;
    this.zzr = paramUri1;
    this.zzac = paramString3;
    this.zzs = paramUri2;
    this.zzad = paramString4;
    this.zzbz = paramLong1;
    this.zzca = paramInt1;
    this.zzcb = paramLong2;
    this.zzcc = paramString5;
    this.zzcf = paramBoolean1;
    this.zzcd = paramzzb;
    this.zzce = paramPlayerLevelInfo;
    this.zzcg = paramBoolean2;
    this.zzch = paramString6;
    this.name = paramString7;
    this.zzci = paramUri3;
    this.zzcj = paramString8;
    this.zzck = paramUri4;
    this.zzcl = paramString9;
    this.zzcm = paramInt2;
    this.zzcn = paramLong3;
    this.zzco = paramBoolean3;
  }
  
  static int zza(Player paramPlayer)
  {
    return Objects.hashCode(new Object[] { paramPlayer.getPlayerId(), paramPlayer.getDisplayName(), Boolean.valueOf(paramPlayer.zzh()), paramPlayer.getIconImageUri(), paramPlayer.getHiResImageUri(), Long.valueOf(paramPlayer.getRetrievedTimestamp()), paramPlayer.getTitle(), paramPlayer.getLevelInfo(), paramPlayer.zzg(), paramPlayer.getName(), paramPlayer.getBannerImageLandscapeUri(), paramPlayer.getBannerImagePortraitUri(), Integer.valueOf(paramPlayer.zzl()), Long.valueOf(paramPlayer.zzm()), Boolean.valueOf(paramPlayer.isMuted()) });
  }
  
  static boolean zza(Player paramPlayer, Object paramObject)
  {
    if (!(paramObject instanceof Player)) {}
    do
    {
      return false;
      if (paramPlayer == paramObject) {
        return true;
      }
      paramObject = (Player)paramObject;
    } while ((!Objects.equal(((Player)paramObject).getPlayerId(), paramPlayer.getPlayerId())) || (!Objects.equal(((Player)paramObject).getDisplayName(), paramPlayer.getDisplayName())) || (!Objects.equal(Boolean.valueOf(((Player)paramObject).zzh()), Boolean.valueOf(paramPlayer.zzh()))) || (!Objects.equal(((Player)paramObject).getIconImageUri(), paramPlayer.getIconImageUri())) || (!Objects.equal(((Player)paramObject).getHiResImageUri(), paramPlayer.getHiResImageUri())) || (!Objects.equal(Long.valueOf(((Player)paramObject).getRetrievedTimestamp()), Long.valueOf(paramPlayer.getRetrievedTimestamp()))) || (!Objects.equal(((Player)paramObject).getTitle(), paramPlayer.getTitle())) || (!Objects.equal(((Player)paramObject).getLevelInfo(), paramPlayer.getLevelInfo())) || (!Objects.equal(((Player)paramObject).zzg(), paramPlayer.zzg())) || (!Objects.equal(((Player)paramObject).getName(), paramPlayer.getName())) || (!Objects.equal(((Player)paramObject).getBannerImageLandscapeUri(), paramPlayer.getBannerImageLandscapeUri())) || (!Objects.equal(((Player)paramObject).getBannerImagePortraitUri(), paramPlayer.getBannerImagePortraitUri())) || (!Objects.equal(Integer.valueOf(((Player)paramObject).zzl()), Integer.valueOf(paramPlayer.zzl()))) || (!Objects.equal(Long.valueOf(((Player)paramObject).zzm()), Long.valueOf(paramPlayer.zzm()))) || (!Objects.equal(Boolean.valueOf(((Player)paramObject).isMuted()), Boolean.valueOf(paramPlayer.isMuted()))));
    return true;
  }
  
  static String zzb(Player paramPlayer)
  {
    return Objects.toStringHelper(paramPlayer).add("PlayerId", paramPlayer.getPlayerId()).add("DisplayName", paramPlayer.getDisplayName()).add("HasDebugAccess", Boolean.valueOf(paramPlayer.zzh())).add("IconImageUri", paramPlayer.getIconImageUri()).add("IconImageUrl", paramPlayer.getIconImageUrl()).add("HiResImageUri", paramPlayer.getHiResImageUri()).add("HiResImageUrl", paramPlayer.getHiResImageUrl()).add("RetrievedTimestamp", Long.valueOf(paramPlayer.getRetrievedTimestamp())).add("Title", paramPlayer.getTitle()).add("LevelInfo", paramPlayer.getLevelInfo()).add("GamerTag", paramPlayer.zzg()).add("Name", paramPlayer.getName()).add("BannerImageLandscapeUri", paramPlayer.getBannerImageLandscapeUri()).add("BannerImageLandscapeUrl", paramPlayer.getBannerImageLandscapeUrl()).add("BannerImagePortraitUri", paramPlayer.getBannerImagePortraitUri()).add("BannerImagePortraitUrl", paramPlayer.getBannerImagePortraitUrl()).add("GamerFriendStatus", Integer.valueOf(paramPlayer.zzl())).add("GamerFriendUpdateTimestamp", Long.valueOf(paramPlayer.zzm())).add("IsMuted", Boolean.valueOf(paramPlayer.isMuted())).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final Player freeze()
  {
    return this;
  }
  
  public final Uri getBannerImageLandscapeUri()
  {
    return this.zzci;
  }
  
  public final String getBannerImageLandscapeUrl()
  {
    return this.zzcj;
  }
  
  public final Uri getBannerImagePortraitUri()
  {
    return this.zzck;
  }
  
  public final String getBannerImagePortraitUrl()
  {
    return this.zzcl;
  }
  
  public final String getDisplayName()
  {
    return this.zzn;
  }
  
  public final void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.zzn, paramCharArrayBuffer);
  }
  
  public final Uri getHiResImageUri()
  {
    return this.zzs;
  }
  
  public final String getHiResImageUrl()
  {
    return this.zzad;
  }
  
  public final Uri getIconImageUri()
  {
    return this.zzr;
  }
  
  public final String getIconImageUrl()
  {
    return this.zzac;
  }
  
  public final long getLastPlayedWithTimestamp()
  {
    return this.zzcb;
  }
  
  public final PlayerLevelInfo getLevelInfo()
  {
    return this.zzce;
  }
  
  public final String getName()
  {
    return this.name;
  }
  
  public final String getPlayerId()
  {
    return this.zzby;
  }
  
  public final long getRetrievedTimestamp()
  {
    return this.zzbz;
  }
  
  public final String getTitle()
  {
    return this.zzcc;
  }
  
  public final void getTitle(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.zzcc, paramCharArrayBuffer);
  }
  
  public final boolean hasHiResImage()
  {
    return getHiResImageUri() != null;
  }
  
  public final boolean hasIconImage()
  {
    return getIconImageUri() != null;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final boolean isMuted()
  {
    return this.zzco;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject2 = null;
    if (!shouldDowngrade())
    {
      int i = SafeParcelWriter.beginObjectHeader(paramParcel);
      SafeParcelWriter.writeString(paramParcel, 1, getPlayerId(), false);
      SafeParcelWriter.writeString(paramParcel, 2, getDisplayName(), false);
      SafeParcelWriter.writeParcelable(paramParcel, 3, getIconImageUri(), paramInt, false);
      SafeParcelWriter.writeParcelable(paramParcel, 4, getHiResImageUri(), paramInt, false);
      SafeParcelWriter.writeLong(paramParcel, 5, getRetrievedTimestamp());
      SafeParcelWriter.writeInt(paramParcel, 6, this.zzca);
      SafeParcelWriter.writeLong(paramParcel, 7, getLastPlayedWithTimestamp());
      SafeParcelWriter.writeString(paramParcel, 8, getIconImageUrl(), false);
      SafeParcelWriter.writeString(paramParcel, 9, getHiResImageUrl(), false);
      SafeParcelWriter.writeString(paramParcel, 14, getTitle(), false);
      SafeParcelWriter.writeParcelable(paramParcel, 15, this.zzcd, paramInt, false);
      SafeParcelWriter.writeParcelable(paramParcel, 16, getLevelInfo(), paramInt, false);
      SafeParcelWriter.writeBoolean(paramParcel, 18, this.zzcf);
      SafeParcelWriter.writeBoolean(paramParcel, 19, this.zzcg);
      SafeParcelWriter.writeString(paramParcel, 20, this.zzch, false);
      SafeParcelWriter.writeString(paramParcel, 21, this.name, false);
      SafeParcelWriter.writeParcelable(paramParcel, 22, getBannerImageLandscapeUri(), paramInt, false);
      SafeParcelWriter.writeString(paramParcel, 23, getBannerImageLandscapeUrl(), false);
      SafeParcelWriter.writeParcelable(paramParcel, 24, getBannerImagePortraitUri(), paramInt, false);
      SafeParcelWriter.writeString(paramParcel, 25, getBannerImagePortraitUrl(), false);
      SafeParcelWriter.writeInt(paramParcel, 26, this.zzcm);
      SafeParcelWriter.writeLong(paramParcel, 27, this.zzcn);
      SafeParcelWriter.writeBoolean(paramParcel, 28, this.zzco);
      SafeParcelWriter.finishObjectHeader(paramParcel, i);
      return;
    }
    paramParcel.writeString(this.zzby);
    paramParcel.writeString(this.zzn);
    if (this.zzr == null)
    {
      localObject1 = null;
      paramParcel.writeString((String)localObject1);
      if (this.zzs != null) {
        break label337;
      }
    }
    label337:
    for (Object localObject1 = localObject2;; localObject1 = this.zzs.toString())
    {
      paramParcel.writeString((String)localObject1);
      paramParcel.writeLong(this.zzbz);
      return;
      localObject1 = this.zzr.toString();
      break;
    }
  }
  
  public final String zzg()
  {
    return this.zzch;
  }
  
  public final boolean zzh()
  {
    return this.zzcg;
  }
  
  public final int zzi()
  {
    return this.zzca;
  }
  
  public final boolean zzj()
  {
    return this.zzcf;
  }
  
  public final zza zzk()
  {
    return this.zzcd;
  }
  
  public final int zzl()
  {
    return this.zzcm;
  }
  
  public final long zzm()
  {
    return this.zzcn;
  }
  
  static final class zza
    extends zzap
  {
    public final PlayerEntity zzc(Parcel paramParcel)
    {
      if ((PlayerEntity.zza(PlayerEntity.zze())) || (PlayerEntity.zza(PlayerEntity.class.getCanonicalName()))) {
        return super.zzc(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2 = paramParcel.readString();
      if (localObject1 == null)
      {
        localObject1 = null;
        if (localObject2 != null) {
          break label114;
        }
      }
      label114:
      for (localObject2 = null;; localObject2 = Uri.parse((String)localObject2))
      {
        return new PlayerEntity(str1, str2, (Uri)localObject1, (Uri)localObject2, paramParcel.readLong(), -1, -1L, null, null, null, null, null, true, false, paramParcel.readString(), paramParcel.readString(), null, null, null, null, -1, -1L, false);
        localObject1 = Uri.parse((String)localObject1);
        break;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\PlayerEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */