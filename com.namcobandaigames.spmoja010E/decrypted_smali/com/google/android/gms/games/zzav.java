package com.google.android.gms.games;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzav
  extends zzah<Intent>
{
  zzav(PlayersClient paramPlayersClient, Player paramPlayer) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Intent> paramTaskCompletionSource)
    throws RemoteException
  {
    paramTaskCompletionSource.setResult(paramzze.zza(new PlayerEntity(this.zzdc)));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzav.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */