package com.google.android.gms.games;

import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards.LeaderboardMetadataResult;

final class zzam
  implements PendingResultUtil.ResultConverter<Leaderboards.LeaderboardMetadataResult, Leaderboard>
{
  private static Leaderboard zza(Leaderboards.LeaderboardMetadataResult paramLeaderboardMetadataResult)
  {
    Leaderboard localLeaderboard = null;
    if (paramLeaderboardMetadataResult == null) {
      paramLeaderboardMetadataResult = localLeaderboard;
    }
    for (;;)
    {
      return paramLeaderboardMetadataResult;
      LeaderboardBuffer localLeaderboardBuffer = paramLeaderboardMetadataResult.getLeaderboards();
      if (localLeaderboardBuffer != null) {}
      try
      {
        if (localLeaderboardBuffer.getCount() > 0)
        {
          localLeaderboard = (Leaderboard)((Leaderboard)localLeaderboardBuffer.get(0)).freeze();
          paramLeaderboardMetadataResult = localLeaderboard;
          return localLeaderboard;
        }
        paramLeaderboardMetadataResult = localLeaderboard;
        return null;
      }
      finally
      {
        if (localLeaderboardBuffer != null) {
          localLeaderboardBuffer.release();
        }
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzam.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */