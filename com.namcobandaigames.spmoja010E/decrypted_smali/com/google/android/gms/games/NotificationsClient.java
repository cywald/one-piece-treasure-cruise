package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class NotificationsClient
  extends zzu
{
  public static final int NOTIFICATION_TYPES_ALL = 19;
  public static final int NOTIFICATION_TYPES_MULTIPLAYER = 3;
  public static final int NOTIFICATION_TYPE_INVITATION = 1;
  public static final int NOTIFICATION_TYPE_LEVEL_UP = 16;
  public static final int NOTIFICATION_TYPE_MATCH_UPDATE = 2;
  
  NotificationsClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  NotificationsClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<Void> clear(int paramInt)
  {
    return doWrite(new zzao(this, paramInt));
  }
  
  public Task<Void> clearAll()
  {
    return clear(19);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\NotificationsClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */