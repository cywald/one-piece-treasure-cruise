package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.internal.zzr;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class AchievementsClient
  extends zzu
{
  private static final PendingResultUtil.ResultConverter<Achievements.LoadAchievementsResult, AchievementBuffer> zze = new zzb();
  private static final PendingResultUtil.ResultConverter<Achievements.UpdateAchievementResult, Void> zzf = new zzc();
  private static final PendingResultUtil.ResultConverter<Achievements.UpdateAchievementResult, Boolean> zzg = new zzd();
  private static final zzr zzh = new zze();
  
  AchievementsClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  AchievementsClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  private static Task<Void> zza(@NonNull PendingResult<Achievements.UpdateAchievementResult> paramPendingResult)
  {
    return zzi.zza(paramPendingResult, zzh, zzf);
  }
  
  private static Task<Boolean> zzb(@NonNull PendingResult<Achievements.UpdateAchievementResult> paramPendingResult)
  {
    return zzi.zza(paramPendingResult, zzh, zzg);
  }
  
  public Task<Intent> getAchievementsIntent()
  {
    return doRead(new zza(this));
  }
  
  public void increment(@NonNull String paramString, @IntRange(from=0L) int paramInt)
  {
    Games.Achievements.increment(asGoogleApiClient(), paramString, paramInt);
  }
  
  public Task<Boolean> incrementImmediate(@NonNull String paramString, @IntRange(from=0L) int paramInt)
  {
    return zzb(Games.Achievements.incrementImmediate(asGoogleApiClient(), paramString, paramInt));
  }
  
  public Task<AnnotatedData<AchievementBuffer>> load(boolean paramBoolean)
  {
    return zzi.zzb(Games.Achievements.load(asGoogleApiClient(), paramBoolean), zze);
  }
  
  public void reveal(@NonNull String paramString)
  {
    Games.Achievements.reveal(asGoogleApiClient(), paramString);
  }
  
  public Task<Void> revealImmediate(@NonNull String paramString)
  {
    return zza(Games.Achievements.revealImmediate(asGoogleApiClient(), paramString));
  }
  
  public void setSteps(@NonNull String paramString, @IntRange(from=0L) int paramInt)
  {
    Games.Achievements.setSteps(asGoogleApiClient(), paramString, paramInt);
  }
  
  public Task<Boolean> setStepsImmediate(@NonNull String paramString, @IntRange(from=0L) int paramInt)
  {
    return zzb(Games.Achievements.setStepsImmediate(asGoogleApiClient(), paramString, paramInt));
  }
  
  public void unlock(@NonNull String paramString)
  {
    Games.Achievements.unlock(asGoogleApiClient(), paramString);
  }
  
  public Task<Void> unlockImmediate(@NonNull String paramString)
  {
    return zza(Games.Achievements.unlockImmediate(asGoogleApiClient(), paramString));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\AchievementsClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */