package com.google.android.gms.games;

import com.google.android.gms.common.api.internal.ListenerHolder.Notifier;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateCallback;
import com.google.android.gms.games.multiplayer.realtime.zzh;

final class zzbp
  implements ListenerHolder.Notifier<zzh>
{
  zzbp(zzbo paramzzbo) {}
  
  public final void onNotifyListenerFailed()
  {
    this.zzds.zzdr.zzdo.getRoomUpdateCallback().onLeftRoom(0, this.zzds.zzdr.zzdg);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */