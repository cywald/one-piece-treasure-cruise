package com.google.android.gms.games;

import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.Stats.LoadPlayerStatsResult;

final class zzas
  implements PendingResultUtil.ResultConverter<Stats.LoadPlayerStatsResult, PlayerStats>
{}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */