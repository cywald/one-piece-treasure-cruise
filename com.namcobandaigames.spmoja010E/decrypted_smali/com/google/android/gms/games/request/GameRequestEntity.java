package com.google.android.gms.games.request;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzd;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Deprecated
@SafeParcelable.Class(creator="GameRequestEntityCreator")
@SafeParcelable.Reserved({1000})
public final class GameRequestEntity
  extends zzd
  implements GameRequest
{
  public static final Parcelable.Creator<GameRequestEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getData", id=3)
  private final byte[] data;
  @SafeParcelable.Field(getter="getStatus", id=12)
  private final int status;
  @SafeParcelable.Field(getter="getType", id=7)
  private final int type;
  @SafeParcelable.Field(getter="getRequestId", id=4)
  private final String zzht;
  @SafeParcelable.Field(getter="getGame", id=1)
  private final GameEntity zzky;
  @SafeParcelable.Field(getter="getCreationTimestamp", id=9)
  private final long zzoa;
  @SafeParcelable.Field(getter="getSender", id=2)
  private final PlayerEntity zzqh;
  @SafeParcelable.Field(getter="getRecipients", id=5)
  private final ArrayList<PlayerEntity> zzqi;
  @SafeParcelable.Field(getter="getExpirationTimestamp", id=10)
  private final long zzqj;
  @SafeParcelable.Field(getter="getRecipientStatusBundle", id=11)
  private final Bundle zzqk;
  
  @SafeParcelable.Constructor
  GameRequestEntity(@SafeParcelable.Param(id=1) GameEntity paramGameEntity, @SafeParcelable.Param(id=2) PlayerEntity paramPlayerEntity, @SafeParcelable.Param(id=3) byte[] paramArrayOfByte, @SafeParcelable.Param(id=4) String paramString, @SafeParcelable.Param(id=5) ArrayList<PlayerEntity> paramArrayList, @SafeParcelable.Param(id=7) int paramInt1, @SafeParcelable.Param(id=9) long paramLong1, @SafeParcelable.Param(id=10) long paramLong2, @SafeParcelable.Param(id=11) Bundle paramBundle, @SafeParcelable.Param(id=12) int paramInt2)
  {
    this.zzky = paramGameEntity;
    this.zzqh = paramPlayerEntity;
    this.data = paramArrayOfByte;
    this.zzht = paramString;
    this.zzqi = paramArrayList;
    this.type = paramInt1;
    this.zzoa = paramLong1;
    this.zzqj = paramLong2;
    this.zzqk = paramBundle;
    this.status = paramInt2;
  }
  
  public GameRequestEntity(GameRequest paramGameRequest)
  {
    this.zzky = new GameEntity(paramGameRequest.getGame());
    this.zzqh = new PlayerEntity(paramGameRequest.getSender());
    this.zzht = paramGameRequest.getRequestId();
    this.type = paramGameRequest.getType();
    this.zzoa = paramGameRequest.getCreationTimestamp();
    this.zzqj = paramGameRequest.getExpirationTimestamp();
    this.status = paramGameRequest.getStatus();
    Object localObject = paramGameRequest.getData();
    if (localObject == null) {
      this.data = null;
    }
    for (;;)
    {
      localObject = paramGameRequest.getRecipients();
      int j = ((List)localObject).size();
      this.zzqi = new ArrayList(j);
      this.zzqk = new Bundle();
      int i = 0;
      while (i < j)
      {
        Player localPlayer = (Player)((Player)((List)localObject).get(i)).freeze();
        String str = localPlayer.getPlayerId();
        this.zzqi.add((PlayerEntity)localPlayer);
        this.zzqk.putInt(str, paramGameRequest.getRecipientStatus(str));
        i += 1;
      }
      this.data = new byte[localObject.length];
      System.arraycopy(localObject, 0, this.data, 0, localObject.length);
    }
  }
  
  static int zza(GameRequest paramGameRequest)
  {
    return Arrays.hashCode(zzb(paramGameRequest)) * 31 + Objects.hashCode(new Object[] { paramGameRequest.getGame(), paramGameRequest.getRecipients(), paramGameRequest.getRequestId(), paramGameRequest.getSender(), Integer.valueOf(paramGameRequest.getType()), Long.valueOf(paramGameRequest.getCreationTimestamp()), Long.valueOf(paramGameRequest.getExpirationTimestamp()) });
  }
  
  static boolean zza(GameRequest paramGameRequest, Object paramObject)
  {
    if (!(paramObject instanceof GameRequest)) {}
    do
    {
      return false;
      if (paramGameRequest == paramObject) {
        return true;
      }
      paramObject = (GameRequest)paramObject;
    } while ((!Objects.equal(((GameRequest)paramObject).getGame(), paramGameRequest.getGame())) || (!Objects.equal(((GameRequest)paramObject).getRecipients(), paramGameRequest.getRecipients())) || (!Objects.equal(((GameRequest)paramObject).getRequestId(), paramGameRequest.getRequestId())) || (!Objects.equal(((GameRequest)paramObject).getSender(), paramGameRequest.getSender())) || (!Arrays.equals(zzb((GameRequest)paramObject), zzb(paramGameRequest))) || (!Objects.equal(Integer.valueOf(((GameRequest)paramObject).getType()), Integer.valueOf(paramGameRequest.getType()))) || (!Objects.equal(Long.valueOf(((GameRequest)paramObject).getCreationTimestamp()), Long.valueOf(paramGameRequest.getCreationTimestamp()))) || (!Objects.equal(Long.valueOf(((GameRequest)paramObject).getExpirationTimestamp()), Long.valueOf(paramGameRequest.getExpirationTimestamp()))));
    return true;
  }
  
  private static int[] zzb(GameRequest paramGameRequest)
  {
    List localList = paramGameRequest.getRecipients();
    int j = localList.size();
    int[] arrayOfInt = new int[j];
    int i = 0;
    while (i < j)
    {
      arrayOfInt[i] = paramGameRequest.getRecipientStatus(((Player)localList.get(i)).getPlayerId());
      i += 1;
    }
    return arrayOfInt;
  }
  
  static String zzc(GameRequest paramGameRequest)
  {
    return Objects.toStringHelper(paramGameRequest).add("Game", paramGameRequest.getGame()).add("Sender", paramGameRequest.getSender()).add("Recipients", paramGameRequest.getRecipients()).add("Data", paramGameRequest.getData()).add("RequestId", paramGameRequest.getRequestId()).add("Type", Integer.valueOf(paramGameRequest.getType())).add("CreationTimestamp", Long.valueOf(paramGameRequest.getCreationTimestamp())).add("ExpirationTimestamp", Long.valueOf(paramGameRequest.getExpirationTimestamp())).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final GameRequest freeze()
  {
    return this;
  }
  
  public final long getCreationTimestamp()
  {
    return this.zzoa;
  }
  
  public final byte[] getData()
  {
    return this.data;
  }
  
  public final long getExpirationTimestamp()
  {
    return this.zzqj;
  }
  
  public final Game getGame()
  {
    return this.zzky;
  }
  
  public final int getRecipientStatus(String paramString)
  {
    return this.zzqk.getInt(paramString, 0);
  }
  
  public final List<Player> getRecipients()
  {
    return new ArrayList(this.zzqi);
  }
  
  public final String getRequestId()
  {
    return this.zzht;
  }
  
  public final Player getSender()
  {
    return this.zzqh;
  }
  
  public final int getStatus()
  {
    return this.status;
  }
  
  public final int getType()
  {
    return this.type;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isConsumed(String paramString)
  {
    return getRecipientStatus(paramString) == 1;
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zzc(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, getGame(), paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 2, getSender(), paramInt, false);
    SafeParcelWriter.writeByteArray(paramParcel, 3, getData(), false);
    SafeParcelWriter.writeString(paramParcel, 4, getRequestId(), false);
    SafeParcelWriter.writeTypedList(paramParcel, 5, getRecipients(), false);
    SafeParcelWriter.writeInt(paramParcel, 7, getType());
    SafeParcelWriter.writeLong(paramParcel, 9, getCreationTimestamp());
    SafeParcelWriter.writeLong(paramParcel, 10, getExpirationTimestamp());
    SafeParcelWriter.writeBundle(paramParcel, 11, this.zzqk, false);
    SafeParcelWriter.writeInt(paramParcel, 12, getStatus());
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\request\GameRequestEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */