package com.google.android.gms.games.request;

import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public final class zzb
  extends DataBufferRef
  implements GameRequest
{
  private final int zzmz;
  
  public zzb(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    super(paramDataHolder, paramInt1);
    this.zzmz = paramInt2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    return GameRequestEntity.zza(this, paramObject);
  }
  
  public final long getCreationTimestamp()
  {
    return getLong("creation_timestamp");
  }
  
  public final byte[] getData()
  {
    return getByteArray("data");
  }
  
  public final long getExpirationTimestamp()
  {
    return getLong("expiration_timestamp");
  }
  
  public final Game getGame()
  {
    return new GameRef(this.mDataHolder, this.mDataRow);
  }
  
  public final int getRecipientStatus(String paramString)
  {
    int i = this.mDataRow;
    while (i < this.mDataRow + this.zzmz)
    {
      int j = this.mDataHolder.getWindowIndex(i);
      if (this.mDataHolder.getString("recipient_external_player_id", i, j).equals(paramString)) {
        return this.mDataHolder.getInteger("recipient_status", i, j);
      }
      i += 1;
    }
    return -1;
  }
  
  public final List<Player> getRecipients()
  {
    ArrayList localArrayList = new ArrayList(this.zzmz);
    int i = 0;
    while (i < this.zzmz)
    {
      localArrayList.add(new PlayerRef(this.mDataHolder, this.mDataRow + i, "recipient_"));
      i += 1;
    }
    return localArrayList;
  }
  
  public final String getRequestId()
  {
    return getString("external_request_id");
  }
  
  public final Player getSender()
  {
    return new PlayerRef(this.mDataHolder, getDataRow(), "sender_");
  }
  
  public final int getStatus()
  {
    return getInteger("status");
  }
  
  public final int getType()
  {
    return getInteger("type");
  }
  
  public final int hashCode()
  {
    return GameRequestEntity.zza(this);
  }
  
  public final boolean isConsumed(String paramString)
  {
    return getRecipientStatus(paramString) == 1;
  }
  
  public final String toString()
  {
    return GameRequestEntity.zzc(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((GameRequestEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\request\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */