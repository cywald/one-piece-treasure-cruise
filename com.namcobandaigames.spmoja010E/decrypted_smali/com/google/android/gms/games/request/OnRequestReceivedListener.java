package com.google.android.gms.games.request;

@Deprecated
public abstract interface OnRequestReceivedListener
{
  public abstract void onRequestReceived(GameRequest paramGameRequest);
  
  public abstract void onRequestRemoved(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\request\OnRequestReceivedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */