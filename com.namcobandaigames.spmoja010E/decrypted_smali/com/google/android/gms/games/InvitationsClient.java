package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolders;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.InvitationCallback;
import com.google.android.gms.games.multiplayer.Invitations;
import com.google.android.gms.games.multiplayer.Invitations.LoadInvitationsResult;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class InvitationsClient
  extends zzu
{
  private static final PendingResultUtil.ResultConverter<Invitations.LoadInvitationsResult, InvitationBuffer> zzbh = new zzaa();
  
  InvitationsClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  InvitationsClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<Intent> getInvitationInboxIntent()
  {
    return doRead(new zzx(this));
  }
  
  public Task<AnnotatedData<InvitationBuffer>> loadInvitations()
  {
    return loadInvitations(0);
  }
  
  public Task<AnnotatedData<InvitationBuffer>> loadInvitations(int paramInt)
  {
    return zzi.zzb(Games.Invitations.loadInvitations(asGoogleApiClient(), paramInt), zzbh);
  }
  
  public Task<Void> registerInvitationCallback(@NonNull InvitationCallback paramInvitationCallback)
  {
    paramInvitationCallback = registerListener(paramInvitationCallback, InvitationCallback.class.getSimpleName());
    return doRegisterEventListener(new zzy(this, paramInvitationCallback, paramInvitationCallback), new zzz(this, paramInvitationCallback.getListenerKey()));
  }
  
  public Task<Boolean> unregisterInvitationCallback(@NonNull InvitationCallback paramInvitationCallback)
  {
    return doUnregisterEventListener(ListenerHolders.createListenerKey(paramInvitationCallback, InvitationCallback.class.getSimpleName()));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\InvitationsClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */