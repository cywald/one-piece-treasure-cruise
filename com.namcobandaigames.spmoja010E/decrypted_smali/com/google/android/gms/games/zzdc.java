package com.google.android.gms.games;

import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos.CaptureCapabilitiesResult;

final class zzdc
  implements PendingResultUtil.ResultConverter<Videos.CaptureCapabilitiesResult, VideoCapabilities>
{}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzdc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */