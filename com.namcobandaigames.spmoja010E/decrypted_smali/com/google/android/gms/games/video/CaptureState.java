package com.google.android.gms.games.video;

import android.os.Bundle;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.Preconditions;

public final class CaptureState
{
  private final boolean zzrp;
  private final int zzrq;
  private final int zzrr;
  private final boolean zzrs;
  private final boolean zzrt;
  
  private CaptureState(boolean paramBoolean1, int paramInt1, int paramInt2, boolean paramBoolean2, boolean paramBoolean3)
  {
    Preconditions.checkArgument(VideoConfiguration.isValidCaptureMode(paramInt1, true));
    Preconditions.checkArgument(VideoConfiguration.isValidQualityLevel(paramInt2, true));
    this.zzrp = paramBoolean1;
    this.zzrq = paramInt1;
    this.zzrr = paramInt2;
    this.zzrs = paramBoolean2;
    this.zzrt = paramBoolean3;
  }
  
  public static CaptureState zzb(Bundle paramBundle)
  {
    if ((paramBundle == null) || (paramBundle.get("IsCapturing") == null)) {
      return null;
    }
    return new CaptureState(paramBundle.getBoolean("IsCapturing", false), paramBundle.getInt("CaptureMode", -1), paramBundle.getInt("CaptureQuality", -1), paramBundle.getBoolean("IsOverlayVisible", false), paramBundle.getBoolean("IsPaused", false));
  }
  
  public final int getCaptureMode()
  {
    return this.zzrq;
  }
  
  public final int getCaptureQuality()
  {
    return this.zzrr;
  }
  
  public final boolean isCapturing()
  {
    return this.zzrp;
  }
  
  public final boolean isOverlayVisible()
  {
    return this.zzrs;
  }
  
  public final boolean isPaused()
  {
    return this.zzrt;
  }
  
  public final String toString()
  {
    return Objects.toStringHelper(this).add("IsCapturing", Boolean.valueOf(this.zzrp)).add("CaptureMode", Integer.valueOf(this.zzrq)).add("CaptureQuality", Integer.valueOf(this.zzrr)).add("IsOverlayVisible", Boolean.valueOf(this.zzrs)).add("IsPaused", Boolean.valueOf(this.zzrt)).toString();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\video\CaptureState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */