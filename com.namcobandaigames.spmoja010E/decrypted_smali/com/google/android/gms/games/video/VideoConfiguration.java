package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SafeParcelable.Class(creator="VideoConfigurationCreator")
@SafeParcelable.Reserved({1000})
public final class VideoConfiguration
  extends AbstractSafeParcelable
{
  public static final int CAPTURE_MODE_FILE = 0;
  public static final int CAPTURE_MODE_STREAM = 1;
  public static final int CAPTURE_MODE_UNKNOWN = -1;
  public static final Parcelable.Creator<VideoConfiguration> CREATOR = new zzb();
  public static final int NUM_CAPTURE_MODE = 2;
  public static final int NUM_QUALITY_LEVEL = 4;
  public static final int QUALITY_LEVEL_FULLHD = 3;
  public static final int QUALITY_LEVEL_HD = 1;
  public static final int QUALITY_LEVEL_SD = 0;
  public static final int QUALITY_LEVEL_UNKNOWN = -1;
  public static final int QUALITY_LEVEL_XHD = 2;
  @SafeParcelable.Field(getter="getCaptureMode", id=2)
  private final int zzrq;
  @SafeParcelable.Field(getter="getQualityLevel", id=1)
  private final int zzrz;
  @SafeParcelable.Field(getter="shouldShowToastAfterRecording", id=7)
  private final boolean zzsa;
  
  @SafeParcelable.Constructor
  public VideoConfiguration(@SafeParcelable.Param(id=1) int paramInt1, @SafeParcelable.Param(id=2) int paramInt2, @SafeParcelable.Param(id=7) boolean paramBoolean)
  {
    Preconditions.checkArgument(isValidQualityLevel(paramInt1, false));
    Preconditions.checkArgument(isValidCaptureMode(paramInt2, false));
    this.zzrz = paramInt1;
    this.zzrq = paramInt2;
    this.zzsa = paramBoolean;
  }
  
  public static boolean isValidCaptureMode(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default: 
      paramBoolean = false;
    case -1: 
    case 1: 
      return paramBoolean;
    }
    return true;
  }
  
  public static boolean isValidQualityLevel(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default: 
      paramBoolean = false;
    case -1: 
      return paramBoolean;
    }
    return true;
  }
  
  public final int getCaptureMode()
  {
    return this.zzrq;
  }
  
  public final int getQualityLevel()
  {
    return this.zzrz;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 1, getQualityLevel());
    SafeParcelWriter.writeInt(paramParcel, 2, getCaptureMode());
    SafeParcelWriter.writeBoolean(paramParcel, 7, this.zzsa);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private int zzrq;
    private int zzrz;
    private boolean zzsa;
    
    public Builder(int paramInt1, int paramInt2)
    {
      this.zzrz = paramInt1;
      this.zzrq = paramInt2;
      this.zzsa = true;
    }
    
    public final VideoConfiguration build()
    {
      return new VideoConfiguration(this.zzrz, this.zzrq, this.zzsa);
    }
    
    public final Builder setCaptureMode(int paramInt)
    {
      this.zzrq = paramInt;
      return this;
    }
    
    public final Builder setQualityLevel(int paramInt)
    {
      this.zzrz = paramInt;
      return this;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ValidCaptureModes {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\video\VideoConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */