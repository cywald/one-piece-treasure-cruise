package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="VideoCapabilitiesCreator")
@SafeParcelable.Reserved({1000})
public final class VideoCapabilities
  extends zzd
{
  public static final Parcelable.Creator<VideoCapabilities> CREATOR = new zza();
  @SafeParcelable.Field(getter="isCameraSupported", id=1)
  private final boolean zzru;
  @SafeParcelable.Field(getter="isMicSupported", id=2)
  private final boolean zzrv;
  @SafeParcelable.Field(getter="isWriteStorageSupported", id=3)
  private final boolean zzrw;
  @SafeParcelable.Field(getter="getSupportedCaptureModes", id=4)
  private final boolean[] zzrx;
  @SafeParcelable.Field(getter="getSupportedQualityLevels", id=5)
  private final boolean[] zzry;
  
  @SafeParcelable.Constructor
  public VideoCapabilities(@SafeParcelable.Param(id=1) boolean paramBoolean1, @SafeParcelable.Param(id=2) boolean paramBoolean2, @SafeParcelable.Param(id=3) boolean paramBoolean3, @SafeParcelable.Param(id=4) boolean[] paramArrayOfBoolean1, @SafeParcelable.Param(id=5) boolean[] paramArrayOfBoolean2)
  {
    this.zzru = paramBoolean1;
    this.zzrv = paramBoolean2;
    this.zzrw = paramBoolean3;
    this.zzrx = paramArrayOfBoolean1;
    this.zzry = paramArrayOfBoolean2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof VideoCapabilities)) {}
    do
    {
      return false;
      if (this == paramObject) {
        return true;
      }
      paramObject = (VideoCapabilities)paramObject;
    } while ((!Objects.equal(((VideoCapabilities)paramObject).getSupportedCaptureModes(), getSupportedCaptureModes())) || (!Objects.equal(((VideoCapabilities)paramObject).getSupportedQualityLevels(), getSupportedQualityLevels())) || (!Objects.equal(Boolean.valueOf(((VideoCapabilities)paramObject).isCameraSupported()), Boolean.valueOf(isCameraSupported()))) || (!Objects.equal(Boolean.valueOf(((VideoCapabilities)paramObject).isMicSupported()), Boolean.valueOf(isMicSupported()))) || (!Objects.equal(Boolean.valueOf(((VideoCapabilities)paramObject).isWriteStorageSupported()), Boolean.valueOf(isWriteStorageSupported()))));
    return true;
  }
  
  public final boolean[] getSupportedCaptureModes()
  {
    return this.zzrx;
  }
  
  public final boolean[] getSupportedQualityLevels()
  {
    return this.zzry;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { getSupportedCaptureModes(), getSupportedQualityLevels(), Boolean.valueOf(isCameraSupported()), Boolean.valueOf(isMicSupported()), Boolean.valueOf(isWriteStorageSupported()) });
  }
  
  public final boolean isCameraSupported()
  {
    return this.zzru;
  }
  
  public final boolean isFullySupported(int paramInt1, int paramInt2)
  {
    return (this.zzru) && (this.zzrv) && (this.zzrw) && (supportsCaptureMode(paramInt1)) && (supportsQualityLevel(paramInt2));
  }
  
  public final boolean isMicSupported()
  {
    return this.zzrv;
  }
  
  public final boolean isWriteStorageSupported()
  {
    return this.zzrw;
  }
  
  public final boolean supportsCaptureMode(int paramInt)
  {
    Preconditions.checkState(VideoConfiguration.isValidCaptureMode(paramInt, false));
    return this.zzrx[paramInt];
  }
  
  public final boolean supportsQualityLevel(int paramInt)
  {
    Preconditions.checkState(VideoConfiguration.isValidQualityLevel(paramInt, false));
    return this.zzry[paramInt];
  }
  
  public final String toString()
  {
    return Objects.toStringHelper(this).add("SupportedCaptureModes", getSupportedCaptureModes()).add("SupportedQualityLevels", getSupportedQualityLevels()).add("CameraSupported", Boolean.valueOf(isCameraSupported())).add("MicSupported", Boolean.valueOf(isMicSupported())).add("StorageWriteSupported", Boolean.valueOf(isWriteStorageSupported())).toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeBoolean(paramParcel, 1, isCameraSupported());
    SafeParcelWriter.writeBoolean(paramParcel, 2, isMicSupported());
    SafeParcelWriter.writeBoolean(paramParcel, 3, isWriteStorageSupported());
    SafeParcelWriter.writeBooleanArray(paramParcel, 4, getSupportedCaptureModes(), false);
    SafeParcelWriter.writeBooleanArray(paramParcel, 5, getSupportedQualityLevels(), false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\video\VideoCapabilities.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */