package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzs;
import com.google.android.gms.games.video.Videos.CaptureOverlayStateListener;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcy
  extends zzs<Videos.CaptureOverlayStateListener>
{
  zzcy(VideosClient paramVideosClient, ListenerHolder paramListenerHolder1, ListenerHolder paramListenerHolder2)
  {
    super(paramListenerHolder1);
  }
  
  protected final void zzb(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException, SecurityException
  {
    paramzze.zzg(this.zzbi);
    paramTaskCompletionSource.setResult(null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzcy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */