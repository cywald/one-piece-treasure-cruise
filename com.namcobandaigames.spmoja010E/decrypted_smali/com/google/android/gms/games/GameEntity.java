package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

@SafeParcelable.Class(creator="GameEntityCreator")
@SafeParcelable.Reserved({1000})
@RetainForClient
public final class GameEntity
  extends GamesDowngradeableSafeParcel
  implements Game
{
  public static final Parcelable.Creator<GameEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getDescription", id=5)
  private final String description;
  @SafeParcelable.Field(getter="isRealTimeMultiplayerEnabled", id=16)
  private final boolean zzaa;
  @SafeParcelable.Field(getter="isTurnBasedMultiplayerEnabled", id=17)
  private final boolean zzab;
  @SafeParcelable.Field(getter="getIconImageUrl", id=18)
  private final String zzac;
  @SafeParcelable.Field(getter="getHiResImageUrl", id=19)
  private final String zzad;
  @SafeParcelable.Field(getter="getFeaturedImageUrl", id=20)
  private final String zzae;
  @SafeParcelable.Field(getter="isMuted", id=21)
  private final boolean zzaf;
  @SafeParcelable.Field(getter="isIdentitySharingConfirmed", id=22)
  private final boolean zzag;
  @SafeParcelable.Field(getter="areSnapshotsEnabled", id=23)
  private final boolean zzah;
  @SafeParcelable.Field(getter="getThemeColor", id=24)
  private final String zzai;
  @SafeParcelable.Field(getter="hasGamepadSupport", id=25)
  private final boolean zzaj;
  @SafeParcelable.Field(getter="getApplicationId", id=1)
  private final String zzm;
  @SafeParcelable.Field(getter="getDisplayName", id=2)
  private final String zzn;
  @SafeParcelable.Field(getter="getPrimaryCategory", id=3)
  private final String zzo;
  @SafeParcelable.Field(getter="getSecondaryCategory", id=4)
  private final String zzp;
  @SafeParcelable.Field(getter="getDeveloperName", id=6)
  private final String zzq;
  @SafeParcelable.Field(getter="getIconImageUri", id=7)
  private final Uri zzr;
  @SafeParcelable.Field(getter="getHiResImageUri", id=8)
  private final Uri zzs;
  @SafeParcelable.Field(getter="getFeaturedImageUri", id=9)
  private final Uri zzt;
  @SafeParcelable.Field(getter="isPlayEnabledGame", id=10)
  private final boolean zzu;
  @SafeParcelable.Field(getter="isInstanceInstalled", id=11)
  private final boolean zzv;
  @SafeParcelable.Field(getter="getInstancePackageName", id=12)
  private final String zzw;
  @SafeParcelable.Field(getter="getGameplayAclStatus", id=13)
  private final int zzx;
  @SafeParcelable.Field(getter="getAchievementTotalCount", id=14)
  private final int zzy;
  @SafeParcelable.Field(getter="getLeaderboardCount", id=15)
  private final int zzz;
  
  public GameEntity(Game paramGame)
  {
    this.zzm = paramGame.getApplicationId();
    this.zzo = paramGame.getPrimaryCategory();
    this.zzp = paramGame.getSecondaryCategory();
    this.description = paramGame.getDescription();
    this.zzq = paramGame.getDeveloperName();
    this.zzn = paramGame.getDisplayName();
    this.zzr = paramGame.getIconImageUri();
    this.zzac = paramGame.getIconImageUrl();
    this.zzs = paramGame.getHiResImageUri();
    this.zzad = paramGame.getHiResImageUrl();
    this.zzt = paramGame.getFeaturedImageUri();
    this.zzae = paramGame.getFeaturedImageUrl();
    this.zzu = paramGame.zza();
    this.zzv = paramGame.zzc();
    this.zzw = paramGame.zzd();
    this.zzx = 1;
    this.zzy = paramGame.getAchievementTotalCount();
    this.zzz = paramGame.getLeaderboardCount();
    this.zzaa = paramGame.isRealTimeMultiplayerEnabled();
    this.zzab = paramGame.isTurnBasedMultiplayerEnabled();
    this.zzaf = paramGame.isMuted();
    this.zzag = paramGame.zzb();
    this.zzah = paramGame.areSnapshotsEnabled();
    this.zzai = paramGame.getThemeColor();
    this.zzaj = paramGame.hasGamepadSupport();
  }
  
  @SafeParcelable.Constructor
  GameEntity(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) String paramString2, @SafeParcelable.Param(id=3) String paramString3, @SafeParcelable.Param(id=4) String paramString4, @SafeParcelable.Param(id=5) String paramString5, @SafeParcelable.Param(id=6) String paramString6, @SafeParcelable.Param(id=7) Uri paramUri1, @SafeParcelable.Param(id=8) Uri paramUri2, @SafeParcelable.Param(id=9) Uri paramUri3, @SafeParcelable.Param(id=10) boolean paramBoolean1, @SafeParcelable.Param(id=11) boolean paramBoolean2, @SafeParcelable.Param(id=12) String paramString7, @SafeParcelable.Param(id=13) int paramInt1, @SafeParcelable.Param(id=14) int paramInt2, @SafeParcelable.Param(id=15) int paramInt3, @SafeParcelable.Param(id=16) boolean paramBoolean3, @SafeParcelable.Param(id=17) boolean paramBoolean4, @SafeParcelable.Param(id=18) String paramString8, @SafeParcelable.Param(id=19) String paramString9, @SafeParcelable.Param(id=20) String paramString10, @SafeParcelable.Param(id=21) boolean paramBoolean5, @SafeParcelable.Param(id=22) boolean paramBoolean6, @SafeParcelable.Param(id=23) boolean paramBoolean7, @SafeParcelable.Param(id=24) String paramString11, @SafeParcelable.Param(id=25) boolean paramBoolean8)
  {
    this.zzm = paramString1;
    this.zzn = paramString2;
    this.zzo = paramString3;
    this.zzp = paramString4;
    this.description = paramString5;
    this.zzq = paramString6;
    this.zzr = paramUri1;
    this.zzac = paramString8;
    this.zzs = paramUri2;
    this.zzad = paramString9;
    this.zzt = paramUri3;
    this.zzae = paramString10;
    this.zzu = paramBoolean1;
    this.zzv = paramBoolean2;
    this.zzw = paramString7;
    this.zzx = paramInt1;
    this.zzy = paramInt2;
    this.zzz = paramInt3;
    this.zzaa = paramBoolean3;
    this.zzab = paramBoolean4;
    this.zzaf = paramBoolean5;
    this.zzag = paramBoolean6;
    this.zzah = paramBoolean7;
    this.zzai = paramString11;
    this.zzaj = paramBoolean8;
  }
  
  static int zza(Game paramGame)
  {
    return Objects.hashCode(new Object[] { paramGame.getApplicationId(), paramGame.getDisplayName(), paramGame.getPrimaryCategory(), paramGame.getSecondaryCategory(), paramGame.getDescription(), paramGame.getDeveloperName(), paramGame.getIconImageUri(), paramGame.getHiResImageUri(), paramGame.getFeaturedImageUri(), Boolean.valueOf(paramGame.zza()), Boolean.valueOf(paramGame.zzc()), paramGame.zzd(), Integer.valueOf(paramGame.getAchievementTotalCount()), Integer.valueOf(paramGame.getLeaderboardCount()), Boolean.valueOf(paramGame.isRealTimeMultiplayerEnabled()), Boolean.valueOf(paramGame.isTurnBasedMultiplayerEnabled()), Boolean.valueOf(paramGame.isMuted()), Boolean.valueOf(paramGame.zzb()), Boolean.valueOf(paramGame.areSnapshotsEnabled()), paramGame.getThemeColor(), Boolean.valueOf(paramGame.hasGamepadSupport()) });
  }
  
  static boolean zza(Game paramGame, Object paramObject)
  {
    if (!(paramObject instanceof Game)) {}
    do
    {
      return false;
      if (paramGame == paramObject) {
        return true;
      }
      paramObject = (Game)paramObject;
    } while ((!Objects.equal(((Game)paramObject).getApplicationId(), paramGame.getApplicationId())) || (!Objects.equal(((Game)paramObject).getDisplayName(), paramGame.getDisplayName())) || (!Objects.equal(((Game)paramObject).getPrimaryCategory(), paramGame.getPrimaryCategory())) || (!Objects.equal(((Game)paramObject).getSecondaryCategory(), paramGame.getSecondaryCategory())) || (!Objects.equal(((Game)paramObject).getDescription(), paramGame.getDescription())) || (!Objects.equal(((Game)paramObject).getDeveloperName(), paramGame.getDeveloperName())) || (!Objects.equal(((Game)paramObject).getIconImageUri(), paramGame.getIconImageUri())) || (!Objects.equal(((Game)paramObject).getHiResImageUri(), paramGame.getHiResImageUri())) || (!Objects.equal(((Game)paramObject).getFeaturedImageUri(), paramGame.getFeaturedImageUri())) || (!Objects.equal(Boolean.valueOf(((Game)paramObject).zza()), Boolean.valueOf(paramGame.zza()))) || (!Objects.equal(Boolean.valueOf(((Game)paramObject).zzc()), Boolean.valueOf(paramGame.zzc()))) || (!Objects.equal(((Game)paramObject).zzd(), paramGame.zzd())) || (!Objects.equal(Integer.valueOf(((Game)paramObject).getAchievementTotalCount()), Integer.valueOf(paramGame.getAchievementTotalCount()))) || (!Objects.equal(Integer.valueOf(((Game)paramObject).getLeaderboardCount()), Integer.valueOf(paramGame.getLeaderboardCount()))) || (!Objects.equal(Boolean.valueOf(((Game)paramObject).isRealTimeMultiplayerEnabled()), Boolean.valueOf(paramGame.isRealTimeMultiplayerEnabled()))) || (!Objects.equal(Boolean.valueOf(((Game)paramObject).isTurnBasedMultiplayerEnabled()), Boolean.valueOf(paramGame.isTurnBasedMultiplayerEnabled()))) || (!Objects.equal(Boolean.valueOf(((Game)paramObject).isMuted()), Boolean.valueOf(paramGame.isMuted()))) || (!Objects.equal(Boolean.valueOf(((Game)paramObject).zzb()), Boolean.valueOf(paramGame.zzb()))) || (!Objects.equal(Boolean.valueOf(((Game)paramObject).areSnapshotsEnabled()), Boolean.valueOf(paramGame.areSnapshotsEnabled()))) || (!Objects.equal(((Game)paramObject).getThemeColor(), paramGame.getThemeColor())) || (!Objects.equal(Boolean.valueOf(((Game)paramObject).hasGamepadSupport()), Boolean.valueOf(paramGame.hasGamepadSupport()))));
    return true;
  }
  
  static String zzb(Game paramGame)
  {
    return Objects.toStringHelper(paramGame).add("ApplicationId", paramGame.getApplicationId()).add("DisplayName", paramGame.getDisplayName()).add("PrimaryCategory", paramGame.getPrimaryCategory()).add("SecondaryCategory", paramGame.getSecondaryCategory()).add("Description", paramGame.getDescription()).add("DeveloperName", paramGame.getDeveloperName()).add("IconImageUri", paramGame.getIconImageUri()).add("IconImageUrl", paramGame.getIconImageUrl()).add("HiResImageUri", paramGame.getHiResImageUri()).add("HiResImageUrl", paramGame.getHiResImageUrl()).add("FeaturedImageUri", paramGame.getFeaturedImageUri()).add("FeaturedImageUrl", paramGame.getFeaturedImageUrl()).add("PlayEnabledGame", Boolean.valueOf(paramGame.zza())).add("InstanceInstalled", Boolean.valueOf(paramGame.zzc())).add("InstancePackageName", paramGame.zzd()).add("AchievementTotalCount", Integer.valueOf(paramGame.getAchievementTotalCount())).add("LeaderboardCount", Integer.valueOf(paramGame.getLeaderboardCount())).add("RealTimeMultiplayerEnabled", Boolean.valueOf(paramGame.isRealTimeMultiplayerEnabled())).add("TurnBasedMultiplayerEnabled", Boolean.valueOf(paramGame.isTurnBasedMultiplayerEnabled())).add("AreSnapshotsEnabled", Boolean.valueOf(paramGame.areSnapshotsEnabled())).add("ThemeColor", paramGame.getThemeColor()).add("HasGamepadSupport", Boolean.valueOf(paramGame.hasGamepadSupport())).toString();
  }
  
  public final boolean areSnapshotsEnabled()
  {
    return this.zzah;
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final Game freeze()
  {
    return this;
  }
  
  public final int getAchievementTotalCount()
  {
    return this.zzy;
  }
  
  public final String getApplicationId()
  {
    return this.zzm;
  }
  
  public final String getDescription()
  {
    return this.description;
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.description, paramCharArrayBuffer);
  }
  
  public final String getDeveloperName()
  {
    return this.zzq;
  }
  
  public final void getDeveloperName(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.zzq, paramCharArrayBuffer);
  }
  
  public final String getDisplayName()
  {
    return this.zzn;
  }
  
  public final void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.zzn, paramCharArrayBuffer);
  }
  
  public final Uri getFeaturedImageUri()
  {
    return this.zzt;
  }
  
  public final String getFeaturedImageUrl()
  {
    return this.zzae;
  }
  
  public final Uri getHiResImageUri()
  {
    return this.zzs;
  }
  
  public final String getHiResImageUrl()
  {
    return this.zzad;
  }
  
  public final Uri getIconImageUri()
  {
    return this.zzr;
  }
  
  public final String getIconImageUrl()
  {
    return this.zzac;
  }
  
  public final int getLeaderboardCount()
  {
    return this.zzz;
  }
  
  public final String getPrimaryCategory()
  {
    return this.zzo;
  }
  
  public final String getSecondaryCategory()
  {
    return this.zzp;
  }
  
  public final String getThemeColor()
  {
    return this.zzai;
  }
  
  public final boolean hasGamepadSupport()
  {
    return this.zzaj;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final boolean isMuted()
  {
    return this.zzaf;
  }
  
  public final boolean isRealTimeMultiplayerEnabled()
  {
    return this.zzaa;
  }
  
  public final boolean isTurnBasedMultiplayerEnabled()
  {
    return this.zzab;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject2 = null;
    int i = 1;
    if (!shouldDowngrade())
    {
      i = SafeParcelWriter.beginObjectHeader(paramParcel);
      SafeParcelWriter.writeString(paramParcel, 1, getApplicationId(), false);
      SafeParcelWriter.writeString(paramParcel, 2, getDisplayName(), false);
      SafeParcelWriter.writeString(paramParcel, 3, getPrimaryCategory(), false);
      SafeParcelWriter.writeString(paramParcel, 4, getSecondaryCategory(), false);
      SafeParcelWriter.writeString(paramParcel, 5, getDescription(), false);
      SafeParcelWriter.writeString(paramParcel, 6, getDeveloperName(), false);
      SafeParcelWriter.writeParcelable(paramParcel, 7, getIconImageUri(), paramInt, false);
      SafeParcelWriter.writeParcelable(paramParcel, 8, getHiResImageUri(), paramInt, false);
      SafeParcelWriter.writeParcelable(paramParcel, 9, getFeaturedImageUri(), paramInt, false);
      SafeParcelWriter.writeBoolean(paramParcel, 10, this.zzu);
      SafeParcelWriter.writeBoolean(paramParcel, 11, this.zzv);
      SafeParcelWriter.writeString(paramParcel, 12, this.zzw, false);
      SafeParcelWriter.writeInt(paramParcel, 13, this.zzx);
      SafeParcelWriter.writeInt(paramParcel, 14, getAchievementTotalCount());
      SafeParcelWriter.writeInt(paramParcel, 15, getLeaderboardCount());
      SafeParcelWriter.writeBoolean(paramParcel, 16, isRealTimeMultiplayerEnabled());
      SafeParcelWriter.writeBoolean(paramParcel, 17, isTurnBasedMultiplayerEnabled());
      SafeParcelWriter.writeString(paramParcel, 18, getIconImageUrl(), false);
      SafeParcelWriter.writeString(paramParcel, 19, getHiResImageUrl(), false);
      SafeParcelWriter.writeString(paramParcel, 20, getFeaturedImageUrl(), false);
      SafeParcelWriter.writeBoolean(paramParcel, 21, this.zzaf);
      SafeParcelWriter.writeBoolean(paramParcel, 22, this.zzag);
      SafeParcelWriter.writeBoolean(paramParcel, 23, areSnapshotsEnabled());
      SafeParcelWriter.writeString(paramParcel, 24, getThemeColor(), false);
      SafeParcelWriter.writeBoolean(paramParcel, 25, hasGamepadSupport());
      SafeParcelWriter.finishObjectHeader(paramParcel, i);
      return;
    }
    paramParcel.writeString(this.zzm);
    paramParcel.writeString(this.zzn);
    paramParcel.writeString(this.zzo);
    paramParcel.writeString(this.zzp);
    paramParcel.writeString(this.description);
    paramParcel.writeString(this.zzq);
    Object localObject1;
    if (this.zzr == null)
    {
      localObject1 = null;
      paramParcel.writeString((String)localObject1);
      if (this.zzs != null) {
        break label455;
      }
      localObject1 = null;
      label359:
      paramParcel.writeString((String)localObject1);
      if (this.zzt != null) {
        break label467;
      }
      localObject1 = localObject2;
      label376:
      paramParcel.writeString((String)localObject1);
      if (!this.zzu) {
        break label479;
      }
      paramInt = 1;
      label391:
      paramParcel.writeInt(paramInt);
      if (!this.zzv) {
        break label484;
      }
    }
    label455:
    label467:
    label479:
    label484:
    for (paramInt = i;; paramInt = 0)
    {
      paramParcel.writeInt(paramInt);
      paramParcel.writeString(this.zzw);
      paramParcel.writeInt(this.zzx);
      paramParcel.writeInt(this.zzy);
      paramParcel.writeInt(this.zzz);
      return;
      localObject1 = this.zzr.toString();
      break;
      localObject1 = this.zzs.toString();
      break label359;
      localObject1 = this.zzt.toString();
      break label376;
      paramInt = 0;
      break label391;
    }
  }
  
  public final boolean zza()
  {
    return this.zzu;
  }
  
  public final boolean zzb()
  {
    return this.zzag;
  }
  
  public final boolean zzc()
  {
    return this.zzv;
  }
  
  public final String zzd()
  {
    return this.zzw;
  }
  
  static final class zza
    extends zzh
  {
    public final GameEntity zzb(Parcel paramParcel)
    {
      if ((GameEntity.zza(GameEntity.zze())) || (GameEntity.zza(GameEntity.class.getCanonicalName()))) {
        return super.zzb(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      String str3 = paramParcel.readString();
      String str4 = paramParcel.readString();
      String str5 = paramParcel.readString();
      String str6 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2;
      label90:
      Object localObject3;
      label104:
      boolean bool1;
      if (localObject1 == null)
      {
        localObject1 = null;
        localObject2 = paramParcel.readString();
        if (localObject2 != null) {
          break label186;
        }
        localObject2 = null;
        localObject3 = paramParcel.readString();
        if (localObject3 != null) {
          break label196;
        }
        localObject3 = null;
        if (paramParcel.readInt() <= 0) {
          break label206;
        }
        bool1 = true;
        label113:
        if (paramParcel.readInt() <= 0) {
          break label211;
        }
      }
      label186:
      label196:
      label206:
      label211:
      for (boolean bool2 = true;; bool2 = false)
      {
        return new GameEntity(str1, str2, str3, str4, str5, str6, (Uri)localObject1, (Uri)localObject2, (Uri)localObject3, bool1, bool2, paramParcel.readString(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), false, false, null, null, null, false, false, false, null, false);
        localObject1 = Uri.parse((String)localObject1);
        break;
        localObject2 = Uri.parse((String)localObject2);
        break label90;
        localObject3 = Uri.parse((String)localObject3);
        break label104;
        bool1 = false;
        break label113;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\GameEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */