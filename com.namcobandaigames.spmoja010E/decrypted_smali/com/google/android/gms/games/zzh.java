package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public class zzh
  implements Parcelable.Creator<GameEntity>
{
  public GameEntity zzb(Parcel paramParcel)
  {
    int m = SafeParcelReader.validateObjectHeader(paramParcel);
    String str11 = null;
    String str10 = null;
    String str9 = null;
    String str8 = null;
    String str7 = null;
    String str6 = null;
    Uri localUri3 = null;
    Uri localUri2 = null;
    Uri localUri1 = null;
    boolean bool8 = false;
    boolean bool7 = false;
    String str5 = null;
    int k = 0;
    int j = 0;
    int i = 0;
    boolean bool6 = false;
    boolean bool5 = false;
    String str4 = null;
    String str3 = null;
    String str2 = null;
    boolean bool4 = false;
    boolean bool3 = false;
    boolean bool2 = false;
    String str1 = null;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < m)
    {
      int n = SafeParcelReader.readHeader(paramParcel);
      switch (SafeParcelReader.getFieldId(n))
      {
      default: 
        SafeParcelReader.skipUnknownField(paramParcel, n);
        break;
      case 1: 
        str11 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 2: 
        str10 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 3: 
        str9 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 4: 
        str8 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 5: 
        str7 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 6: 
        str6 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 7: 
        localUri3 = (Uri)SafeParcelReader.createParcelable(paramParcel, n, Uri.CREATOR);
        break;
      case 8: 
        localUri2 = (Uri)SafeParcelReader.createParcelable(paramParcel, n, Uri.CREATOR);
        break;
      case 9: 
        localUri1 = (Uri)SafeParcelReader.createParcelable(paramParcel, n, Uri.CREATOR);
        break;
      case 10: 
        bool8 = SafeParcelReader.readBoolean(paramParcel, n);
        break;
      case 11: 
        bool7 = SafeParcelReader.readBoolean(paramParcel, n);
        break;
      case 12: 
        str5 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 13: 
        k = SafeParcelReader.readInt(paramParcel, n);
        break;
      case 14: 
        j = SafeParcelReader.readInt(paramParcel, n);
        break;
      case 15: 
        i = SafeParcelReader.readInt(paramParcel, n);
        break;
      case 16: 
        bool6 = SafeParcelReader.readBoolean(paramParcel, n);
        break;
      case 17: 
        bool5 = SafeParcelReader.readBoolean(paramParcel, n);
        break;
      case 18: 
        str4 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 19: 
        str3 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 20: 
        str2 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 21: 
        bool4 = SafeParcelReader.readBoolean(paramParcel, n);
        break;
      case 22: 
        bool3 = SafeParcelReader.readBoolean(paramParcel, n);
        break;
      case 23: 
        bool2 = SafeParcelReader.readBoolean(paramParcel, n);
        break;
      case 24: 
        str1 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 25: 
        bool1 = SafeParcelReader.readBoolean(paramParcel, n);
      }
    }
    SafeParcelReader.ensureAtEnd(paramParcel, m);
    return new GameEntity(str11, str10, str9, str8, str7, str6, localUri3, localUri2, localUri1, bool8, bool7, str5, k, j, i, bool6, bool5, str4, str3, str2, bool4, bool3, bool2, str1, bool1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */