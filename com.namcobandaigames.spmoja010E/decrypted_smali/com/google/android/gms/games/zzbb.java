package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.internal.ApiExceptionUtil;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzy;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbb
  extends zzah<Void>
{
  zzbb(RealTimeMultiplayerClient paramRealTimeMultiplayerClient, byte[] paramArrayOfByte, String paramString1, String paramString2) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException
  {
    if (((zzy)paramzze.getService()).zzb(this.zzdf, this.zzdg, new String[] { this.zzdh }) == 0)
    {
      paramTaskCompletionSource.setResult(null);
      return;
    }
    paramTaskCompletionSource.trySetException(ApiExceptionUtil.fromStatus(GamesClientStatusCodes.zza(26601)));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */