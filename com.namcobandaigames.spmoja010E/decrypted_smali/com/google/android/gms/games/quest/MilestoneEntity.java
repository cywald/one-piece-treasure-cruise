package com.google.android.gms.games.quest;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.internal.zzd;

@Deprecated
@SafeParcelable.Class(creator="MilestoneEntityCreator")
@SafeParcelable.Reserved({1000})
public final class MilestoneEntity
  extends zzd
  implements Milestone
{
  public static final Parcelable.Creator<MilestoneEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getState", id=5)
  private final int state;
  @SafeParcelable.Field(getter="getEventId", id=6)
  private final String zzfm;
  @SafeParcelable.Field(getter="getMilestoneId", id=1)
  private final String zzho;
  @SafeParcelable.Field(getter="getCurrentProgress", id=2)
  private final long zzpu;
  @SafeParcelable.Field(getter="getTargetProgress", id=3)
  private final long zzpv;
  @SafeParcelable.Field(getter="getCompletionRewardData", id=4)
  private final byte[] zzpw;
  
  public MilestoneEntity(Milestone paramMilestone)
  {
    this.zzho = paramMilestone.getMilestoneId();
    this.zzpu = paramMilestone.getCurrentProgress();
    this.zzpv = paramMilestone.getTargetProgress();
    this.state = paramMilestone.getState();
    this.zzfm = paramMilestone.getEventId();
    paramMilestone = paramMilestone.getCompletionRewardData();
    if (paramMilestone == null)
    {
      this.zzpw = null;
      return;
    }
    this.zzpw = new byte[paramMilestone.length];
    System.arraycopy(paramMilestone, 0, this.zzpw, 0, paramMilestone.length);
  }
  
  @SafeParcelable.Constructor
  MilestoneEntity(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) long paramLong1, @SafeParcelable.Param(id=3) long paramLong2, @SafeParcelable.Param(id=4) byte[] paramArrayOfByte, @SafeParcelable.Param(id=5) int paramInt, @SafeParcelable.Param(id=6) String paramString2)
  {
    this.zzho = paramString1;
    this.zzpu = paramLong1;
    this.zzpv = paramLong2;
    this.zzpw = paramArrayOfByte;
    this.state = paramInt;
    this.zzfm = paramString2;
  }
  
  static int zza(Milestone paramMilestone)
  {
    return Objects.hashCode(new Object[] { paramMilestone.getMilestoneId(), Long.valueOf(paramMilestone.getCurrentProgress()), Long.valueOf(paramMilestone.getTargetProgress()), Integer.valueOf(paramMilestone.getState()), paramMilestone.getEventId() });
  }
  
  static boolean zza(Milestone paramMilestone, Object paramObject)
  {
    if (!(paramObject instanceof Milestone)) {}
    do
    {
      return false;
      if (paramMilestone == paramObject) {
        return true;
      }
      paramObject = (Milestone)paramObject;
    } while ((!Objects.equal(((Milestone)paramObject).getMilestoneId(), paramMilestone.getMilestoneId())) || (!Objects.equal(Long.valueOf(((Milestone)paramObject).getCurrentProgress()), Long.valueOf(paramMilestone.getCurrentProgress()))) || (!Objects.equal(Long.valueOf(((Milestone)paramObject).getTargetProgress()), Long.valueOf(paramMilestone.getTargetProgress()))) || (!Objects.equal(Integer.valueOf(((Milestone)paramObject).getState()), Integer.valueOf(paramMilestone.getState()))) || (!Objects.equal(((Milestone)paramObject).getEventId(), paramMilestone.getEventId())));
    return true;
  }
  
  static String zzb(Milestone paramMilestone)
  {
    return Objects.toStringHelper(paramMilestone).add("MilestoneId", paramMilestone.getMilestoneId()).add("CurrentProgress", Long.valueOf(paramMilestone.getCurrentProgress())).add("TargetProgress", Long.valueOf(paramMilestone.getTargetProgress())).add("State", Integer.valueOf(paramMilestone.getState())).add("CompletionRewardData", paramMilestone.getCompletionRewardData()).add("EventId", paramMilestone.getEventId()).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final Milestone freeze()
  {
    return this;
  }
  
  public final byte[] getCompletionRewardData()
  {
    return this.zzpw;
  }
  
  public final long getCurrentProgress()
  {
    return this.zzpu;
  }
  
  public final String getEventId()
  {
    return this.zzfm;
  }
  
  public final String getMilestoneId()
  {
    return this.zzho;
  }
  
  public final int getState()
  {
    return this.state;
  }
  
  public final long getTargetProgress()
  {
    return this.zzpv;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, getMilestoneId(), false);
    SafeParcelWriter.writeLong(paramParcel, 2, getCurrentProgress());
    SafeParcelWriter.writeLong(paramParcel, 3, getTargetProgress());
    SafeParcelWriter.writeByteArray(paramParcel, 4, getCompletionRewardData(), false);
    SafeParcelWriter.writeInt(paramParcel, 5, getState());
    SafeParcelWriter.writeString(paramParcel, 6, getEventId(), false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\quest\MilestoneEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */