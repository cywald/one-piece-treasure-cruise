package com.google.android.gms.games.quest;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.EntityBuffer;
import com.google.android.gms.common.util.VisibleForTesting;

@Deprecated
@VisibleForTesting
public final class QuestBuffer
  extends EntityBuffer<Quest>
{
  public QuestBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
  }
  
  protected final String getPrimaryDataMarkerColumn()
  {
    return "external_quest_id";
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\quest\QuestBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */