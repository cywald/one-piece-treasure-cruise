package com.google.android.gms.games.quest;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.zzd;
import java.util.ArrayList;
import java.util.List;

@Deprecated
@SafeParcelable.Class(creator="QuestEntityCreator")
@SafeParcelable.Reserved({1000})
public final class QuestEntity
  extends zzd
  implements Quest
{
  public static final Parcelable.Creator<QuestEntity> CREATOR = new zzc();
  @SafeParcelable.Field(getter="getDescription", id=6)
  private final String description;
  @SafeParcelable.Field(getter="getName", id=12)
  private final String name;
  @SafeParcelable.Field(getter="getState", id=15)
  private final int state;
  @SafeParcelable.Field(getter="getType", id=16)
  private final int type;
  @SafeParcelable.Field(getter="getLastUpdatedTimestamp", id=8)
  private final long zzfk;
  @SafeParcelable.Field(getter="getGame", id=1)
  private final GameEntity zzky;
  @SafeParcelable.Field(getter="getQuestId", id=2)
  private final String zzpx;
  @SafeParcelable.Field(getter="getAcceptedTimestamp", id=3)
  private final long zzpy;
  @SafeParcelable.Field(getter="getBannerImageUri", id=4)
  private final Uri zzpz;
  @SafeParcelable.Field(getter="getBannerImageUrl", id=5)
  private final String zzqa;
  @SafeParcelable.Field(getter="getEndTimestamp", id=7)
  private final long zzqb;
  @SafeParcelable.Field(getter="getIconImageUri", id=9)
  private final Uri zzqc;
  @SafeParcelable.Field(getter="getIconImageUrl", id=10)
  private final String zzqd;
  @SafeParcelable.Field(getter="getNotifyTimestamp", id=13)
  private final long zzqe;
  @SafeParcelable.Field(getter="getStartTimestamp", id=14)
  private final long zzqf;
  @SafeParcelable.Field(getter="getMilestones", id=17)
  private final ArrayList<MilestoneEntity> zzqg;
  
  @SafeParcelable.Constructor
  QuestEntity(@SafeParcelable.Param(id=1) GameEntity paramGameEntity, @SafeParcelable.Param(id=2) String paramString1, @SafeParcelable.Param(id=3) long paramLong1, @SafeParcelable.Param(id=4) Uri paramUri1, @SafeParcelable.Param(id=5) String paramString2, @SafeParcelable.Param(id=6) String paramString3, @SafeParcelable.Param(id=7) long paramLong2, @SafeParcelable.Param(id=8) long paramLong3, @SafeParcelable.Param(id=9) Uri paramUri2, @SafeParcelable.Param(id=10) String paramString4, @SafeParcelable.Param(id=12) String paramString5, @SafeParcelable.Param(id=13) long paramLong4, @SafeParcelable.Param(id=14) long paramLong5, @SafeParcelable.Param(id=15) int paramInt1, @SafeParcelable.Param(id=16) int paramInt2, @SafeParcelable.Param(id=17) ArrayList<MilestoneEntity> paramArrayList)
  {
    this.zzky = paramGameEntity;
    this.zzpx = paramString1;
    this.zzpy = paramLong1;
    this.zzpz = paramUri1;
    this.zzqa = paramString2;
    this.description = paramString3;
    this.zzqb = paramLong2;
    this.zzfk = paramLong3;
    this.zzqc = paramUri2;
    this.zzqd = paramString4;
    this.name = paramString5;
    this.zzqe = paramLong4;
    this.zzqf = paramLong5;
    this.state = paramInt1;
    this.type = paramInt2;
    this.zzqg = paramArrayList;
  }
  
  public QuestEntity(Quest paramQuest)
  {
    this.zzky = new GameEntity(paramQuest.getGame());
    this.zzpx = paramQuest.getQuestId();
    this.zzpy = paramQuest.getAcceptedTimestamp();
    this.description = paramQuest.getDescription();
    this.zzpz = paramQuest.getBannerImageUri();
    this.zzqa = paramQuest.getBannerImageUrl();
    this.zzqb = paramQuest.getEndTimestamp();
    this.zzqc = paramQuest.getIconImageUri();
    this.zzqd = paramQuest.getIconImageUrl();
    this.zzfk = paramQuest.getLastUpdatedTimestamp();
    this.name = paramQuest.getName();
    this.zzqe = paramQuest.zzck();
    this.zzqf = paramQuest.getStartTimestamp();
    this.state = paramQuest.getState();
    this.type = paramQuest.getType();
    paramQuest = paramQuest.zzcj();
    int j = paramQuest.size();
    this.zzqg = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      this.zzqg.add((MilestoneEntity)((Milestone)paramQuest.get(i)).freeze());
      i += 1;
    }
  }
  
  static int zza(Quest paramQuest)
  {
    return Objects.hashCode(new Object[] { paramQuest.getGame(), paramQuest.getQuestId(), Long.valueOf(paramQuest.getAcceptedTimestamp()), paramQuest.getBannerImageUri(), paramQuest.getDescription(), Long.valueOf(paramQuest.getEndTimestamp()), paramQuest.getIconImageUri(), Long.valueOf(paramQuest.getLastUpdatedTimestamp()), paramQuest.zzcj(), paramQuest.getName(), Long.valueOf(paramQuest.zzck()), Long.valueOf(paramQuest.getStartTimestamp()), Integer.valueOf(paramQuest.getState()) });
  }
  
  static boolean zza(Quest paramQuest, Object paramObject)
  {
    if (!(paramObject instanceof Quest)) {}
    do
    {
      return false;
      if (paramQuest == paramObject) {
        return true;
      }
      paramObject = (Quest)paramObject;
    } while ((!Objects.equal(((Quest)paramObject).getGame(), paramQuest.getGame())) || (!Objects.equal(((Quest)paramObject).getQuestId(), paramQuest.getQuestId())) || (!Objects.equal(Long.valueOf(((Quest)paramObject).getAcceptedTimestamp()), Long.valueOf(paramQuest.getAcceptedTimestamp()))) || (!Objects.equal(((Quest)paramObject).getBannerImageUri(), paramQuest.getBannerImageUri())) || (!Objects.equal(((Quest)paramObject).getDescription(), paramQuest.getDescription())) || (!Objects.equal(Long.valueOf(((Quest)paramObject).getEndTimestamp()), Long.valueOf(paramQuest.getEndTimestamp()))) || (!Objects.equal(((Quest)paramObject).getIconImageUri(), paramQuest.getIconImageUri())) || (!Objects.equal(Long.valueOf(((Quest)paramObject).getLastUpdatedTimestamp()), Long.valueOf(paramQuest.getLastUpdatedTimestamp()))) || (!Objects.equal(((Quest)paramObject).zzcj(), paramQuest.zzcj())) || (!Objects.equal(((Quest)paramObject).getName(), paramQuest.getName())) || (!Objects.equal(Long.valueOf(((Quest)paramObject).zzck()), Long.valueOf(paramQuest.zzck()))) || (!Objects.equal(Long.valueOf(((Quest)paramObject).getStartTimestamp()), Long.valueOf(paramQuest.getStartTimestamp()))) || (!Objects.equal(Integer.valueOf(((Quest)paramObject).getState()), Integer.valueOf(paramQuest.getState()))));
    return true;
  }
  
  static String zzb(Quest paramQuest)
  {
    return Objects.toStringHelper(paramQuest).add("Game", paramQuest.getGame()).add("QuestId", paramQuest.getQuestId()).add("AcceptedTimestamp", Long.valueOf(paramQuest.getAcceptedTimestamp())).add("BannerImageUri", paramQuest.getBannerImageUri()).add("BannerImageUrl", paramQuest.getBannerImageUrl()).add("Description", paramQuest.getDescription()).add("EndTimestamp", Long.valueOf(paramQuest.getEndTimestamp())).add("IconImageUri", paramQuest.getIconImageUri()).add("IconImageUrl", paramQuest.getIconImageUrl()).add("LastUpdatedTimestamp", Long.valueOf(paramQuest.getLastUpdatedTimestamp())).add("Milestones", paramQuest.zzcj()).add("Name", paramQuest.getName()).add("NotifyTimestamp", Long.valueOf(paramQuest.zzck())).add("StartTimestamp", Long.valueOf(paramQuest.getStartTimestamp())).add("State", Integer.valueOf(paramQuest.getState())).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final Quest freeze()
  {
    return this;
  }
  
  public final long getAcceptedTimestamp()
  {
    return this.zzpy;
  }
  
  public final Uri getBannerImageUri()
  {
    return this.zzpz;
  }
  
  public final String getBannerImageUrl()
  {
    return this.zzqa;
  }
  
  public final Milestone getCurrentMilestone()
  {
    return (Milestone)zzcj().get(0);
  }
  
  public final String getDescription()
  {
    return this.description;
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.description, paramCharArrayBuffer);
  }
  
  public final long getEndTimestamp()
  {
    return this.zzqb;
  }
  
  public final Game getGame()
  {
    return this.zzky;
  }
  
  public final Uri getIconImageUri()
  {
    return this.zzqc;
  }
  
  public final String getIconImageUrl()
  {
    return this.zzqd;
  }
  
  public final long getLastUpdatedTimestamp()
  {
    return this.zzfk;
  }
  
  public final String getName()
  {
    return this.name;
  }
  
  public final void getName(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.name, paramCharArrayBuffer);
  }
  
  public final String getQuestId()
  {
    return this.zzpx;
  }
  
  public final long getStartTimestamp()
  {
    return this.zzqf;
  }
  
  public final int getState()
  {
    return this.state;
  }
  
  public final int getType()
  {
    return this.type;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final boolean isEndingSoon()
  {
    return this.zzqe <= System.currentTimeMillis() + 1800000L;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, getGame(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 2, getQuestId(), false);
    SafeParcelWriter.writeLong(paramParcel, 3, getAcceptedTimestamp());
    SafeParcelWriter.writeParcelable(paramParcel, 4, getBannerImageUri(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 5, getBannerImageUrl(), false);
    SafeParcelWriter.writeString(paramParcel, 6, getDescription(), false);
    SafeParcelWriter.writeLong(paramParcel, 7, getEndTimestamp());
    SafeParcelWriter.writeLong(paramParcel, 8, getLastUpdatedTimestamp());
    SafeParcelWriter.writeParcelable(paramParcel, 9, getIconImageUri(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 10, getIconImageUrl(), false);
    SafeParcelWriter.writeString(paramParcel, 12, getName(), false);
    SafeParcelWriter.writeLong(paramParcel, 13, this.zzqe);
    SafeParcelWriter.writeLong(paramParcel, 14, getStartTimestamp());
    SafeParcelWriter.writeInt(paramParcel, 15, getState());
    SafeParcelWriter.writeInt(paramParcel, 16, this.type);
    SafeParcelWriter.writeTypedList(paramParcel, 17, zzcj(), false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final List<Milestone> zzcj()
  {
    return new ArrayList(this.zzqg);
  }
  
  public final long zzck()
  {
    return this.zzqe;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\quest\QuestEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */