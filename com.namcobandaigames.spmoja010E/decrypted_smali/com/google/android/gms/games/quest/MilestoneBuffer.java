package com.google.android.gms.games.quest;

import com.google.android.gms.common.data.AbstractDataBuffer;

@Deprecated
public final class MilestoneBuffer
  extends AbstractDataBuffer<Milestone>
{
  public final Milestone get(int paramInt)
  {
    return new zzb(this.mDataHolder, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\quest\MilestoneBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */