package com.google.android.gms.games.quest;

@Deprecated
public abstract interface QuestUpdateListener
{
  public abstract void onQuestCompleted(Quest paramQuest);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\quest\QuestUpdateListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */