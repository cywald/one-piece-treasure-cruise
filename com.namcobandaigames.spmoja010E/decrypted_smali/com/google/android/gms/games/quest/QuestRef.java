package com.google.android.gms.games.quest;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import java.util.ArrayList;
import java.util.List;

@VisibleForTesting
public final class QuestRef
  extends DataBufferRef
  implements Quest
{
  private final Game zzmy;
  private final int zzmz;
  
  QuestRef(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    super(paramDataHolder, paramInt1);
    this.zzmy = new GameRef(paramDataHolder, paramInt1);
    this.zzmz = paramInt2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    return QuestEntity.zza(this, paramObject);
  }
  
  public final long getAcceptedTimestamp()
  {
    return getLong("accepted_ts");
  }
  
  public final Uri getBannerImageUri()
  {
    return parseUri("quest_banner_image_uri");
  }
  
  public final String getBannerImageUrl()
  {
    return getString("quest_banner_image_url");
  }
  
  public final Milestone getCurrentMilestone()
  {
    return (Milestone)zzcj().get(0);
  }
  
  public final String getDescription()
  {
    return getString("quest_description");
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    copyToBuffer("quest_description", paramCharArrayBuffer);
  }
  
  public final long getEndTimestamp()
  {
    return getLong("quest_end_ts");
  }
  
  public final Game getGame()
  {
    return this.zzmy;
  }
  
  public final Uri getIconImageUri()
  {
    return parseUri("quest_icon_image_uri");
  }
  
  public final String getIconImageUrl()
  {
    return getString("quest_icon_image_url");
  }
  
  public final long getLastUpdatedTimestamp()
  {
    return getLong("quest_last_updated_ts");
  }
  
  public final String getName()
  {
    return getString("quest_name");
  }
  
  public final void getName(CharArrayBuffer paramCharArrayBuffer)
  {
    copyToBuffer("quest_name", paramCharArrayBuffer);
  }
  
  public final String getQuestId()
  {
    return getString("external_quest_id");
  }
  
  public final long getStartTimestamp()
  {
    return getLong("quest_start_ts");
  }
  
  public final int getState()
  {
    return getInteger("quest_state");
  }
  
  public final int getType()
  {
    return getInteger("quest_type");
  }
  
  public final int hashCode()
  {
    return QuestEntity.zza(this);
  }
  
  public final boolean isEndingSoon()
  {
    return getLong("notification_ts") <= System.currentTimeMillis() + 1800000L;
  }
  
  public final String toString()
  {
    return QuestEntity.zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((QuestEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public final List<Milestone> zzcj()
  {
    ArrayList localArrayList = new ArrayList(this.zzmz);
    int i = 0;
    while (i < this.zzmz)
    {
      localArrayList.add(new zzb(this.mDataHolder, this.mDataRow + i));
      i += 1;
    }
    return localArrayList;
  }
  
  public final long zzck()
  {
    return getLong("notification_ts");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\quest\QuestRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */