package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.internal.ApiExceptionUtil;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzy;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.List;

final class zzbc
  extends zzah<Void>
{
  zzbc(RealTimeMultiplayerClient paramRealTimeMultiplayerClient, List paramList, byte[] paramArrayOfByte, String paramString) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException
  {
    Preconditions.checkNotNull(this.zzdi, "Participant IDs must not be null");
    String[] arrayOfString = (String[])this.zzdi.toArray(new String[this.zzdi.size()]);
    if (((zzy)paramzze.getService()).zzb(this.zzdf, this.zzdg, arrayOfString) == 0)
    {
      paramTaskCompletionSource.setResult(null);
      return;
    }
    paramTaskCompletionSource.trySetException(ApiExceptionUtil.fromStatus(GamesClientStatusCodes.zza(26601)));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */