package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcr
  extends zzah<Void>
{
  zzcr(TurnBasedMultiplayerClient paramTurnBasedMultiplayerClient, String paramString) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException
  {
    paramzze.zzd(this.zzdj, 1);
    paramTaskCompletionSource.setResult(null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzcr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */