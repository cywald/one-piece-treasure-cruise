package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbs
  extends zzah<Integer>
{
  zzbs(SnapshotsClient paramSnapshotsClient) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Integer> paramTaskCompletionSource)
    throws RemoteException
  {
    paramTaskCompletionSource.setResult(Integer.valueOf(paramzze.zzat()));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */