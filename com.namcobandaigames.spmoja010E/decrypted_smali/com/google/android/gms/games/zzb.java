package com.google.android.gms.games;

import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;

final class zzb
  implements PendingResultUtil.ResultConverter<Achievements.LoadAchievementsResult, AchievementBuffer>
{}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */