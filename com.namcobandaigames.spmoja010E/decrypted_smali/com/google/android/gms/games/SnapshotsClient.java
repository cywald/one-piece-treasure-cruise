package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.internal.zzp;
import com.google.android.gms.games.internal.zzr;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.games.snapshot.Snapshots.CommitSnapshotResult;
import com.google.android.gms.games.snapshot.Snapshots.DeleteSnapshotResult;
import com.google.android.gms.games.snapshot.Snapshots.LoadSnapshotsResult;
import com.google.android.gms.games.snapshot.Snapshots.OpenSnapshotResult;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class SnapshotsClient
  extends zzu
{
  public static final int DISPLAY_LIMIT_NONE = -1;
  public static final String EXTRA_SNAPSHOT_METADATA = "com.google.android.gms.games.SNAPSHOT_METADATA";
  public static final String EXTRA_SNAPSHOT_NEW = "com.google.android.gms.games.SNAPSHOT_NEW";
  public static final int RESOLUTION_POLICY_HIGHEST_PROGRESS = 4;
  public static final int RESOLUTION_POLICY_LAST_KNOWN_GOOD = 2;
  public static final int RESOLUTION_POLICY_LONGEST_PLAYTIME = 1;
  public static final int RESOLUTION_POLICY_MANUAL = -1;
  public static final int RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED = 3;
  private static final zzp<Snapshots.OpenSnapshotResult> zzdu = new zzby();
  private static final PendingResultUtil.ResultConverter<Snapshots.DeleteSnapshotResult, String> zzdv = new zzbz();
  private static final PendingResultUtil.ResultConverter<Snapshots.CommitSnapshotResult, SnapshotMetadata> zzdw = new zzca();
  private static final PendingResultUtil.ResultConverter<Snapshots.OpenSnapshotResult, Snapshots.OpenSnapshotResult> zzdx = new zzcb();
  private static final zzr zzdy = new zzcc();
  private static final PendingResultUtil.ResultConverter<Snapshots.OpenSnapshotResult, DataOrConflict<Snapshot>> zzdz = new zzbt();
  private static final PendingResultUtil.ResultConverter<Snapshots.LoadSnapshotsResult, SnapshotMetadataBuffer> zzea = new zzbu();
  
  SnapshotsClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  SnapshotsClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  @Nullable
  public static SnapshotMetadata getSnapshotFromBundle(@NonNull Bundle paramBundle)
  {
    return Games.Snapshots.getSnapshotFromBundle(paramBundle);
  }
  
  private static Task<DataOrConflict<Snapshot>> zzc(@NonNull PendingResult<Snapshots.OpenSnapshotResult> paramPendingResult)
  {
    return zzi.zza(paramPendingResult, zzdy, zzdz, zzdx, zzdu);
  }
  
  public Task<SnapshotMetadata> commitAndClose(@NonNull Snapshot paramSnapshot, @NonNull SnapshotMetadataChange paramSnapshotMetadataChange)
  {
    return zzi.toTask(Games.Snapshots.commitAndClose(asGoogleApiClient(), paramSnapshot, paramSnapshotMetadataChange), zzdw);
  }
  
  public Task<String> delete(@NonNull SnapshotMetadata paramSnapshotMetadata)
  {
    return zzi.toTask(Games.Snapshots.delete(asGoogleApiClient(), paramSnapshotMetadata), zzdv);
  }
  
  public Task<Void> discardAndClose(@NonNull Snapshot paramSnapshot)
  {
    return doWrite(new zzbx(this, paramSnapshot));
  }
  
  public Task<Integer> getMaxCoverImageSize()
  {
    return doRead(new zzbv(this));
  }
  
  public Task<Integer> getMaxDataSize()
  {
    return doRead(new zzbs(this));
  }
  
  public Task<Intent> getSelectSnapshotIntent(@NonNull String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    return doRead(new zzbw(this, paramString, paramBoolean1, paramBoolean2, paramInt));
  }
  
  public Task<AnnotatedData<SnapshotMetadataBuffer>> load(boolean paramBoolean)
  {
    return zzi.zzb(Games.Snapshots.load(asGoogleApiClient(), paramBoolean), zzea);
  }
  
  public Task<DataOrConflict<Snapshot>> open(@NonNull SnapshotMetadata paramSnapshotMetadata)
  {
    return zzc(Games.Snapshots.open(asGoogleApiClient(), paramSnapshotMetadata));
  }
  
  public Task<DataOrConflict<Snapshot>> open(@NonNull SnapshotMetadata paramSnapshotMetadata, int paramInt)
  {
    return zzc(Games.Snapshots.open(asGoogleApiClient(), paramSnapshotMetadata, paramInt));
  }
  
  public Task<DataOrConflict<Snapshot>> open(@NonNull String paramString, boolean paramBoolean)
  {
    return zzc(Games.Snapshots.open(asGoogleApiClient(), paramString, paramBoolean));
  }
  
  public Task<DataOrConflict<Snapshot>> open(@NonNull String paramString, boolean paramBoolean, int paramInt)
  {
    return zzc(Games.Snapshots.open(asGoogleApiClient(), paramString, paramBoolean, paramInt));
  }
  
  public Task<DataOrConflict<Snapshot>> resolveConflict(@NonNull String paramString, @NonNull Snapshot paramSnapshot)
  {
    return zzc(Games.Snapshots.resolveConflict(asGoogleApiClient(), paramString, paramSnapshot));
  }
  
  public Task<DataOrConflict<Snapshot>> resolveConflict(@NonNull String paramString1, @NonNull String paramString2, @NonNull SnapshotMetadataChange paramSnapshotMetadataChange, @NonNull SnapshotContents paramSnapshotContents)
  {
    return zzc(Games.Snapshots.resolveConflict(asGoogleApiClient(), paramString1, paramString2, paramSnapshotMetadataChange, paramSnapshotContents));
  }
  
  public static class DataOrConflict<T>
  {
    private final T data;
    private final SnapshotsClient.SnapshotConflict zzeg;
    
    DataOrConflict(@Nullable T paramT, @Nullable SnapshotsClient.SnapshotConflict paramSnapshotConflict)
    {
      this.data = paramT;
      this.zzeg = paramSnapshotConflict;
    }
    
    @Nullable
    public SnapshotsClient.SnapshotConflict getConflict()
    {
      if (!isConflict()) {
        throw new IllegalStateException("getConflict called when there is no conflict.");
      }
      return this.zzeg;
    }
    
    @Nullable
    public T getData()
    {
      if (isConflict()) {
        throw new IllegalStateException("getData called when there is a conflict.");
      }
      return (T)this.data;
    }
    
    public boolean isConflict()
    {
      return this.zzeg != null;
    }
  }
  
  public static class SnapshotConflict
  {
    private final Snapshot zzeh;
    private final String zzei;
    private final Snapshot zzej;
    private final SnapshotContents zzek;
    
    SnapshotConflict(@NonNull Snapshot paramSnapshot1, @NonNull String paramString, @NonNull Snapshot paramSnapshot2, @NonNull SnapshotContents paramSnapshotContents)
    {
      this.zzeh = paramSnapshot1;
      this.zzei = paramString;
      this.zzej = paramSnapshot2;
      this.zzek = paramSnapshotContents;
    }
    
    public String getConflictId()
    {
      return this.zzei;
    }
    
    public Snapshot getConflictingSnapshot()
    {
      return this.zzej;
    }
    
    public SnapshotContents getResolutionSnapshotContents()
    {
      return this.zzek;
    }
    
    public Snapshot getSnapshot()
    {
      return this.zzeh;
    }
  }
  
  public static class SnapshotContentUnavailableApiException
    extends ApiException
  {
    protected final SnapshotMetadata metadata;
    
    SnapshotContentUnavailableApiException(@NonNull Status paramStatus, @NonNull SnapshotMetadata paramSnapshotMetadata)
    {
      super();
      this.metadata = paramSnapshotMetadata;
    }
    
    public SnapshotMetadata getSnapshotMetadata()
    {
      return this.metadata;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\SnapshotsClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */