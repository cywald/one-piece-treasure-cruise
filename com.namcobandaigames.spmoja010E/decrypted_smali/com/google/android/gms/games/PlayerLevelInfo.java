package com.google.android.gms.games;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="PlayerLevelInfoCreator")
@SafeParcelable.Reserved({1000})
public final class PlayerLevelInfo
  extends zzd
{
  public static final Parcelable.Creator<PlayerLevelInfo> CREATOR = new zzar();
  @SafeParcelable.Field(getter="getCurrentXpTotal", id=1)
  private final long zzcs;
  @SafeParcelable.Field(getter="getLastLevelUpTimestamp", id=2)
  private final long zzct;
  @SafeParcelable.Field(getter="getCurrentLevel", id=3)
  private final PlayerLevel zzcu;
  @SafeParcelable.Field(getter="getNextLevel", id=4)
  private final PlayerLevel zzcv;
  
  @SafeParcelable.Constructor
  public PlayerLevelInfo(@SafeParcelable.Param(id=1) long paramLong1, @SafeParcelable.Param(id=2) long paramLong2, @SafeParcelable.Param(id=3) PlayerLevel paramPlayerLevel1, @SafeParcelable.Param(id=4) PlayerLevel paramPlayerLevel2)
  {
    if (paramLong1 != -1L) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool);
      Preconditions.checkNotNull(paramPlayerLevel1);
      Preconditions.checkNotNull(paramPlayerLevel2);
      this.zzcs = paramLong1;
      this.zzct = paramLong2;
      this.zzcu = paramPlayerLevel1;
      this.zzcv = paramPlayerLevel2;
      return;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof PlayerLevelInfo)) {}
    do
    {
      return false;
      if (paramObject == this) {
        return true;
      }
      paramObject = (PlayerLevelInfo)paramObject;
    } while ((!Objects.equal(Long.valueOf(this.zzcs), Long.valueOf(((PlayerLevelInfo)paramObject).zzcs))) || (!Objects.equal(Long.valueOf(this.zzct), Long.valueOf(((PlayerLevelInfo)paramObject).zzct))) || (!Objects.equal(this.zzcu, ((PlayerLevelInfo)paramObject).zzcu)) || (!Objects.equal(this.zzcv, ((PlayerLevelInfo)paramObject).zzcv)));
    return true;
  }
  
  public final PlayerLevel getCurrentLevel()
  {
    return this.zzcu;
  }
  
  public final long getCurrentXpTotal()
  {
    return this.zzcs;
  }
  
  public final long getLastLevelUpTimestamp()
  {
    return this.zzct;
  }
  
  public final PlayerLevel getNextLevel()
  {
    return this.zzcv;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { Long.valueOf(this.zzcs), Long.valueOf(this.zzct), this.zzcu, this.zzcv });
  }
  
  public final boolean isMaxLevel()
  {
    return this.zzcu.equals(this.zzcv);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeLong(paramParcel, 1, getCurrentXpTotal());
    SafeParcelWriter.writeLong(paramParcel, 2, getLastLevelUpTimestamp());
    SafeParcelWriter.writeParcelable(paramParcel, 3, getCurrentLevel(), paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, getNextLevel(), paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\PlayerLevelInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */