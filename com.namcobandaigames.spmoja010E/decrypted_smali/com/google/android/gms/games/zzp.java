package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzp
  extends zzah<String>
{
  zzp(GamesClient paramGamesClient) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<String> paramTaskCompletionSource)
    throws RemoteException
  {
    paramTaskCompletionSource.setResult(paramzze.zzp());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */