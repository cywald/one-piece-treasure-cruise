package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder.ListenerKey;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzt;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzz
  extends zzt<OnInvitationReceivedListener>
{
  zzz(InvitationsClient paramInvitationsClient, ListenerHolder.ListenerKey paramListenerKey)
  {
    super(paramListenerKey);
  }
  
  protected final void zzc(zze paramzze, TaskCompletionSource<Boolean> paramTaskCompletionSource)
    throws RemoteException, SecurityException
  {
    paramzze.zzaa();
    paramTaskCompletionSource.setResult(Boolean.valueOf(true));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */