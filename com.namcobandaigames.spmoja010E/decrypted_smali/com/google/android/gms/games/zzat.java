package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzat
  extends zzah<String>
{
  zzat(PlayersClient paramPlayersClient) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<String> paramTaskCompletionSource)
    throws RemoteException
  {
    paramTaskCompletionSource.setResult(paramzze.zza(true));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */