package com.google.android.gms.games;

import android.support.annotation.Nullable;

public class AnnotatedData<T>
{
  private final T data;
  private final boolean zzi;
  
  public AnnotatedData(@Nullable T paramT, boolean paramBoolean)
  {
    this.data = paramT;
    this.zzi = paramBoolean;
  }
  
  @Nullable
  public T get()
  {
    return (T)this.data;
  }
  
  public boolean isStale()
  {
    return this.zzi;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\AnnotatedData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */