package com.google.android.gms.games.leaderboard;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

public final class LeaderboardEntity
  implements Leaderboard
{
  private final String zzac;
  private final String zzmv;
  private final int zzmw;
  private final ArrayList<zzb> zzmx;
  private final Game zzmy;
  private final String zzn;
  private final Uri zzr;
  
  public LeaderboardEntity(Leaderboard paramLeaderboard)
  {
    this.zzmv = paramLeaderboard.getLeaderboardId();
    this.zzn = paramLeaderboard.getDisplayName();
    this.zzr = paramLeaderboard.getIconImageUri();
    this.zzac = paramLeaderboard.getIconImageUrl();
    this.zzmw = paramLeaderboard.getScoreOrder();
    Object localObject = paramLeaderboard.getGame();
    if (localObject == null) {}
    for (localObject = null;; localObject = new GameEntity((Game)localObject))
    {
      this.zzmy = ((Game)localObject);
      paramLeaderboard = paramLeaderboard.getVariants();
      int j = paramLeaderboard.size();
      this.zzmx = new ArrayList(j);
      int i = 0;
      while (i < j)
      {
        this.zzmx.add((zzb)((LeaderboardVariant)paramLeaderboard.get(i)).freeze());
        i += 1;
      }
    }
  }
  
  static int zza(Leaderboard paramLeaderboard)
  {
    return Objects.hashCode(new Object[] { paramLeaderboard.getLeaderboardId(), paramLeaderboard.getDisplayName(), paramLeaderboard.getIconImageUri(), Integer.valueOf(paramLeaderboard.getScoreOrder()), paramLeaderboard.getVariants() });
  }
  
  static boolean zza(Leaderboard paramLeaderboard, Object paramObject)
  {
    if (!(paramObject instanceof Leaderboard)) {}
    do
    {
      return false;
      if (paramLeaderboard == paramObject) {
        return true;
      }
      paramObject = (Leaderboard)paramObject;
    } while ((!Objects.equal(((Leaderboard)paramObject).getLeaderboardId(), paramLeaderboard.getLeaderboardId())) || (!Objects.equal(((Leaderboard)paramObject).getDisplayName(), paramLeaderboard.getDisplayName())) || (!Objects.equal(((Leaderboard)paramObject).getIconImageUri(), paramLeaderboard.getIconImageUri())) || (!Objects.equal(Integer.valueOf(((Leaderboard)paramObject).getScoreOrder()), Integer.valueOf(paramLeaderboard.getScoreOrder()))) || (!Objects.equal(((Leaderboard)paramObject).getVariants(), paramLeaderboard.getVariants())));
    return true;
  }
  
  static String zzb(Leaderboard paramLeaderboard)
  {
    return Objects.toStringHelper(paramLeaderboard).add("LeaderboardId", paramLeaderboard.getLeaderboardId()).add("DisplayName", paramLeaderboard.getDisplayName()).add("IconImageUri", paramLeaderboard.getIconImageUri()).add("IconImageUrl", paramLeaderboard.getIconImageUrl()).add("ScoreOrder", Integer.valueOf(paramLeaderboard.getScoreOrder())).add("Variants", paramLeaderboard.getVariants()).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final String getDisplayName()
  {
    return this.zzn;
  }
  
  public final void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.zzn, paramCharArrayBuffer);
  }
  
  public final Game getGame()
  {
    return this.zzmy;
  }
  
  public final Uri getIconImageUri()
  {
    return this.zzr;
  }
  
  public final String getIconImageUrl()
  {
    return this.zzac;
  }
  
  public final String getLeaderboardId()
  {
    return this.zzmv;
  }
  
  public final int getScoreOrder()
  {
    return this.zzmw;
  }
  
  public final ArrayList<LeaderboardVariant> getVariants()
  {
    return new ArrayList(this.zzmx);
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\leaderboard\LeaderboardEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */