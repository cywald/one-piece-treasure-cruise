package com.google.android.gms.games.leaderboard;

import android.util.SparseArray;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.games.zzei;

public final class ScoreSubmissionData
{
  private static final String[] zzmt = { "leaderboardId", "playerId", "timeSpan", "hasResult", "rawScore", "formattedScore", "newBest", "scoreTag" };
  private int statusCode;
  private String zzby;
  private String zzmv;
  private SparseArray<Result> zznz;
  
  public ScoreSubmissionData(DataHolder paramDataHolder)
  {
    this.statusCode = paramDataHolder.getStatusCode();
    this.zznz = new SparseArray();
    int j = paramDataHolder.getCount();
    if (j == 3) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool);
      int i = 0;
      while (i < j)
      {
        int k = paramDataHolder.getWindowIndex(i);
        if (i == 0)
        {
          this.zzmv = paramDataHolder.getString("leaderboardId", i, k);
          this.zzby = paramDataHolder.getString("playerId", i, k);
        }
        if (paramDataHolder.getBoolean("hasResult", i, k))
        {
          Result localResult = new Result(paramDataHolder.getLong("rawScore", i, k), paramDataHolder.getString("formattedScore", i, k), paramDataHolder.getString("scoreTag", i, k), paramDataHolder.getBoolean("newBest", i, k));
          k = paramDataHolder.getInteger("timeSpan", i, k);
          this.zznz.put(k, localResult);
        }
        i += 1;
      }
    }
  }
  
  public final String getLeaderboardId()
  {
    return this.zzmv;
  }
  
  public final String getPlayerId()
  {
    return this.zzby;
  }
  
  public final Result getScoreResult(int paramInt)
  {
    return (Result)this.zznz.get(paramInt);
  }
  
  public final String toString()
  {
    Objects.ToStringHelper localToStringHelper = Objects.toStringHelper(this).add("PlayerId", this.zzby).add("StatusCode", Integer.valueOf(this.statusCode));
    int i = 0;
    if (i < 3)
    {
      Object localObject = (Result)this.zznz.get(i);
      localToStringHelper.add("TimesSpan", zzei.zzn(i));
      if (localObject == null) {}
      for (localObject = "null";; localObject = ((Result)localObject).toString())
      {
        localToStringHelper.add("Result", localObject);
        i += 1;
        break;
      }
    }
    return localToStringHelper.toString();
  }
  
  public static final class Result
  {
    public final String formattedScore;
    public final boolean newBest;
    public final long rawScore;
    public final String scoreTag;
    
    public Result(long paramLong, String paramString1, String paramString2, boolean paramBoolean)
    {
      this.rawScore = paramLong;
      this.formattedScore = paramString1;
      this.scoreTag = paramString2;
      this.newBest = paramBoolean;
    }
    
    public final String toString()
    {
      return Objects.toStringHelper(this).add("RawScore", Long.valueOf(this.rawScore)).add("FormattedScore", this.formattedScore).add("ScoreTag", this.scoreTag).add("NewBest", Boolean.valueOf(this.newBest)).toString();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\leaderboard\ScoreSubmissionData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */