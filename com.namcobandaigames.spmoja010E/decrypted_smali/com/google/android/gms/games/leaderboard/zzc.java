package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;

public final class zzc
  extends DataBufferRef
  implements LeaderboardVariant
{
  zzc(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public final boolean equals(Object paramObject)
  {
    return zzb.zza(this, paramObject);
  }
  
  public final int getCollection()
  {
    return getInteger("collection");
  }
  
  public final String getDisplayPlayerRank()
  {
    return getString("player_display_rank");
  }
  
  public final String getDisplayPlayerScore()
  {
    return getString("player_display_score");
  }
  
  public final long getNumScores()
  {
    if (hasNull("total_scores")) {
      return -1L;
    }
    return getLong("total_scores");
  }
  
  public final long getPlayerRank()
  {
    if (hasNull("player_rank")) {
      return -1L;
    }
    return getLong("player_rank");
  }
  
  public final String getPlayerScoreTag()
  {
    return getString("player_score_tag");
  }
  
  public final long getRawPlayerScore()
  {
    if (hasNull("player_raw_score")) {
      return -1L;
    }
    return getLong("player_raw_score");
  }
  
  public final int getTimeSpan()
  {
    return getInteger("timespan");
  }
  
  public final boolean hasPlayerInfo()
  {
    return !hasNull("player_raw_score");
  }
  
  public final int hashCode()
  {
    return zzb.zza(this);
  }
  
  public final String toString()
  {
    return zzb.zzb(this);
  }
  
  public final String zzcd()
  {
    return getString("top_page_token_next");
  }
  
  public final String zzce()
  {
    return getString("window_page_token_prev");
  }
  
  public final String zzcf()
  {
    return getString("window_page_token_next");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\leaderboard\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */