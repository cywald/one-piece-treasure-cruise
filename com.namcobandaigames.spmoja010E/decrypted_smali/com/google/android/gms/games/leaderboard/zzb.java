package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.internal.games.zzei;

public final class zzb
  implements LeaderboardVariant
{
  private final int zznn;
  private final int zzno;
  private final boolean zznp;
  private final long zznq;
  private final String zznr;
  private final long zzns;
  private final String zznt;
  private final String zznu;
  private final long zznv;
  private final String zznw;
  private final String zznx;
  private final String zzny;
  
  public zzb(LeaderboardVariant paramLeaderboardVariant)
  {
    this.zznn = paramLeaderboardVariant.getTimeSpan();
    this.zzno = paramLeaderboardVariant.getCollection();
    this.zznp = paramLeaderboardVariant.hasPlayerInfo();
    this.zznq = paramLeaderboardVariant.getRawPlayerScore();
    this.zznr = paramLeaderboardVariant.getDisplayPlayerScore();
    this.zzns = paramLeaderboardVariant.getPlayerRank();
    this.zznt = paramLeaderboardVariant.getDisplayPlayerRank();
    this.zznu = paramLeaderboardVariant.getPlayerScoreTag();
    this.zznv = paramLeaderboardVariant.getNumScores();
    this.zznw = paramLeaderboardVariant.zzcd();
    this.zznx = paramLeaderboardVariant.zzce();
    this.zzny = paramLeaderboardVariant.zzcf();
  }
  
  static int zza(LeaderboardVariant paramLeaderboardVariant)
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(paramLeaderboardVariant.getTimeSpan()), Integer.valueOf(paramLeaderboardVariant.getCollection()), Boolean.valueOf(paramLeaderboardVariant.hasPlayerInfo()), Long.valueOf(paramLeaderboardVariant.getRawPlayerScore()), paramLeaderboardVariant.getDisplayPlayerScore(), Long.valueOf(paramLeaderboardVariant.getPlayerRank()), paramLeaderboardVariant.getDisplayPlayerRank(), Long.valueOf(paramLeaderboardVariant.getNumScores()), paramLeaderboardVariant.zzcd(), paramLeaderboardVariant.zzcf(), paramLeaderboardVariant.zzce() });
  }
  
  static boolean zza(LeaderboardVariant paramLeaderboardVariant, Object paramObject)
  {
    if (!(paramObject instanceof LeaderboardVariant)) {}
    do
    {
      return false;
      if (paramLeaderboardVariant == paramObject) {
        return true;
      }
      paramObject = (LeaderboardVariant)paramObject;
    } while ((!Objects.equal(Integer.valueOf(((LeaderboardVariant)paramObject).getTimeSpan()), Integer.valueOf(paramLeaderboardVariant.getTimeSpan()))) || (!Objects.equal(Integer.valueOf(((LeaderboardVariant)paramObject).getCollection()), Integer.valueOf(paramLeaderboardVariant.getCollection()))) || (!Objects.equal(Boolean.valueOf(((LeaderboardVariant)paramObject).hasPlayerInfo()), Boolean.valueOf(paramLeaderboardVariant.hasPlayerInfo()))) || (!Objects.equal(Long.valueOf(((LeaderboardVariant)paramObject).getRawPlayerScore()), Long.valueOf(paramLeaderboardVariant.getRawPlayerScore()))) || (!Objects.equal(((LeaderboardVariant)paramObject).getDisplayPlayerScore(), paramLeaderboardVariant.getDisplayPlayerScore())) || (!Objects.equal(Long.valueOf(((LeaderboardVariant)paramObject).getPlayerRank()), Long.valueOf(paramLeaderboardVariant.getPlayerRank()))) || (!Objects.equal(((LeaderboardVariant)paramObject).getDisplayPlayerRank(), paramLeaderboardVariant.getDisplayPlayerRank())) || (!Objects.equal(Long.valueOf(((LeaderboardVariant)paramObject).getNumScores()), Long.valueOf(paramLeaderboardVariant.getNumScores()))) || (!Objects.equal(((LeaderboardVariant)paramObject).zzcd(), paramLeaderboardVariant.zzcd())) || (!Objects.equal(((LeaderboardVariant)paramObject).zzcf(), paramLeaderboardVariant.zzcf())) || (!Objects.equal(((LeaderboardVariant)paramObject).zzce(), paramLeaderboardVariant.zzce())));
    return true;
  }
  
  static String zzb(LeaderboardVariant paramLeaderboardVariant)
  {
    Objects.ToStringHelper localToStringHelper = Objects.toStringHelper(paramLeaderboardVariant).add("TimeSpan", zzei.zzn(paramLeaderboardVariant.getTimeSpan()));
    int i = paramLeaderboardVariant.getCollection();
    switch (i)
    {
    default: 
      throw new IllegalArgumentException(43 + "Unknown leaderboard collection: " + i);
    case -1: 
      localObject = "UNKNOWN";
      localToStringHelper = localToStringHelper.add("Collection", localObject);
      if (paramLeaderboardVariant.hasPlayerInfo())
      {
        localObject = Long.valueOf(paramLeaderboardVariant.getRawPlayerScore());
        label115:
        localToStringHelper = localToStringHelper.add("RawPlayerScore", localObject);
        if (!paramLeaderboardVariant.hasPlayerInfo()) {
          break label272;
        }
        localObject = paramLeaderboardVariant.getDisplayPlayerScore();
        label139:
        localToStringHelper = localToStringHelper.add("DisplayPlayerScore", localObject);
        if (!paramLeaderboardVariant.hasPlayerInfo()) {
          break label278;
        }
        localObject = Long.valueOf(paramLeaderboardVariant.getPlayerRank());
        label166:
        localToStringHelper = localToStringHelper.add("PlayerRank", localObject);
        if (!paramLeaderboardVariant.hasPlayerInfo()) {
          break label284;
        }
      }
      break;
    }
    label272:
    label278:
    label284:
    for (Object localObject = paramLeaderboardVariant.getDisplayPlayerRank();; localObject = "none")
    {
      return localToStringHelper.add("DisplayPlayerRank", localObject).add("NumScores", Long.valueOf(paramLeaderboardVariant.getNumScores())).add("TopPageNextToken", paramLeaderboardVariant.zzcd()).add("WindowPageNextToken", paramLeaderboardVariant.zzcf()).add("WindowPagePrevToken", paramLeaderboardVariant.zzce()).toString();
      localObject = "PUBLIC";
      break;
      localObject = "SOCIAL";
      break;
      localObject = "SOCIAL_1P";
      break;
      localObject = "none";
      break label115;
      localObject = "none";
      break label139;
      localObject = "none";
      break label166;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final int getCollection()
  {
    return this.zzno;
  }
  
  public final String getDisplayPlayerRank()
  {
    return this.zznt;
  }
  
  public final String getDisplayPlayerScore()
  {
    return this.zznr;
  }
  
  public final long getNumScores()
  {
    return this.zznv;
  }
  
  public final long getPlayerRank()
  {
    return this.zzns;
  }
  
  public final String getPlayerScoreTag()
  {
    return this.zznu;
  }
  
  public final long getRawPlayerScore()
  {
    return this.zznq;
  }
  
  public final int getTimeSpan()
  {
    return this.zznn;
  }
  
  public final boolean hasPlayerInfo()
  {
    return this.zznp;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final String zzcd()
  {
    return this.zznw;
  }
  
  public final String zzce()
  {
    return this.zznx;
  }
  
  public final String zzcf()
  {
    return this.zzny;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\leaderboard\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */