package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;

public final class LeaderboardScoreBuffer
  extends AbstractDataBuffer<LeaderboardScore>
{
  private final zza zzna;
  
  public LeaderboardScoreBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
    this.zzna = new zza(paramDataHolder.getMetadata());
  }
  
  public final LeaderboardScore get(int paramInt)
  {
    return new LeaderboardScoreRef(this.mDataHolder, paramInt);
  }
  
  public final zza zzcb()
  {
    return this.zzna;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\leaderboard\LeaderboardScoreBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */