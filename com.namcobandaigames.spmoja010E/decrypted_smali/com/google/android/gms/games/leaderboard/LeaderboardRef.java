package com.google.android.gms.games.leaderboard;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import java.util.ArrayList;

public final class LeaderboardRef
  extends DataBufferRef
  implements Leaderboard
{
  private final Game zzmy;
  private final int zzmz;
  
  LeaderboardRef(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    super(paramDataHolder, paramInt1);
    this.zzmz = paramInt2;
    this.zzmy = new GameRef(paramDataHolder, paramInt1);
  }
  
  public final boolean equals(Object paramObject)
  {
    return LeaderboardEntity.zza(this, paramObject);
  }
  
  public final String getDisplayName()
  {
    return getString("name");
  }
  
  public final void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    copyToBuffer("name", paramCharArrayBuffer);
  }
  
  public final Game getGame()
  {
    return this.zzmy;
  }
  
  public final Uri getIconImageUri()
  {
    return parseUri("board_icon_image_uri");
  }
  
  public final String getIconImageUrl()
  {
    return getString("board_icon_image_url");
  }
  
  public final String getLeaderboardId()
  {
    return getString("external_leaderboard_id");
  }
  
  public final int getScoreOrder()
  {
    return getInteger("score_order");
  }
  
  public final ArrayList<LeaderboardVariant> getVariants()
  {
    ArrayList localArrayList = new ArrayList(this.zzmz);
    int i = 0;
    while (i < this.zzmz)
    {
      localArrayList.add(new zzc(this.mDataHolder, this.mDataRow + i));
      i += 1;
    }
    return localArrayList;
  }
  
  public final int hashCode()
  {
    return LeaderboardEntity.zza(this);
  }
  
  public final String toString()
  {
    return LeaderboardEntity.zzb(this);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\leaderboard\LeaderboardRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */