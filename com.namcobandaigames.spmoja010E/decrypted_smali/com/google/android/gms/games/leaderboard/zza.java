package com.google.android.gms.games.leaderboard;

import android.os.Bundle;

public final class zza
{
  private final Bundle zznb;
  
  public zza(Bundle paramBundle)
  {
    Bundle localBundle = paramBundle;
    if (paramBundle == null) {
      localBundle = new Bundle();
    }
    this.zznb = localBundle;
  }
  
  public final Bundle zzcc()
  {
    return this.zznb;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\leaderboard\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */