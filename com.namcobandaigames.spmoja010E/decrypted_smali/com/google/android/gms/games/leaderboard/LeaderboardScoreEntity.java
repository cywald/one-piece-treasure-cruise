package com.google.android.gms.games.leaderboard;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public final class LeaderboardScoreEntity
  implements LeaderboardScore
{
  private final long rawScore;
  private final String scoreTag;
  private final long zznc;
  private final String zznd;
  private final String zzne;
  private final long zznf;
  private final String zzng;
  private final Uri zznh;
  private final Uri zzni;
  private final PlayerEntity zznj;
  private final String zznk;
  private final String zznl;
  
  public LeaderboardScoreEntity(LeaderboardScore paramLeaderboardScore)
  {
    this.zznc = paramLeaderboardScore.getRank();
    this.zznd = ((String)Preconditions.checkNotNull(paramLeaderboardScore.getDisplayRank()));
    this.zzne = ((String)Preconditions.checkNotNull(paramLeaderboardScore.getDisplayScore()));
    this.rawScore = paramLeaderboardScore.getRawScore();
    this.zznf = paramLeaderboardScore.getTimestampMillis();
    this.zzng = paramLeaderboardScore.getScoreHolderDisplayName();
    this.zznh = paramLeaderboardScore.getScoreHolderIconImageUri();
    this.zzni = paramLeaderboardScore.getScoreHolderHiResImageUri();
    Object localObject = paramLeaderboardScore.getScoreHolder();
    if (localObject == null) {}
    for (localObject = null;; localObject = (PlayerEntity)((Player)localObject).freeze())
    {
      this.zznj = ((PlayerEntity)localObject);
      this.scoreTag = paramLeaderboardScore.getScoreTag();
      this.zznk = paramLeaderboardScore.getScoreHolderIconImageUrl();
      this.zznl = paramLeaderboardScore.getScoreHolderHiResImageUrl();
      return;
    }
  }
  
  static int zza(LeaderboardScore paramLeaderboardScore)
  {
    return Objects.hashCode(new Object[] { Long.valueOf(paramLeaderboardScore.getRank()), paramLeaderboardScore.getDisplayRank(), Long.valueOf(paramLeaderboardScore.getRawScore()), paramLeaderboardScore.getDisplayScore(), Long.valueOf(paramLeaderboardScore.getTimestampMillis()), paramLeaderboardScore.getScoreHolderDisplayName(), paramLeaderboardScore.getScoreHolderIconImageUri(), paramLeaderboardScore.getScoreHolderHiResImageUri(), paramLeaderboardScore.getScoreHolder() });
  }
  
  static boolean zza(LeaderboardScore paramLeaderboardScore, Object paramObject)
  {
    if (!(paramObject instanceof LeaderboardScore)) {}
    do
    {
      return false;
      if (paramLeaderboardScore == paramObject) {
        return true;
      }
      paramObject = (LeaderboardScore)paramObject;
    } while ((!Objects.equal(Long.valueOf(((LeaderboardScore)paramObject).getRank()), Long.valueOf(paramLeaderboardScore.getRank()))) || (!Objects.equal(((LeaderboardScore)paramObject).getDisplayRank(), paramLeaderboardScore.getDisplayRank())) || (!Objects.equal(Long.valueOf(((LeaderboardScore)paramObject).getRawScore()), Long.valueOf(paramLeaderboardScore.getRawScore()))) || (!Objects.equal(((LeaderboardScore)paramObject).getDisplayScore(), paramLeaderboardScore.getDisplayScore())) || (!Objects.equal(Long.valueOf(((LeaderboardScore)paramObject).getTimestampMillis()), Long.valueOf(paramLeaderboardScore.getTimestampMillis()))) || (!Objects.equal(((LeaderboardScore)paramObject).getScoreHolderDisplayName(), paramLeaderboardScore.getScoreHolderDisplayName())) || (!Objects.equal(((LeaderboardScore)paramObject).getScoreHolderIconImageUri(), paramLeaderboardScore.getScoreHolderIconImageUri())) || (!Objects.equal(((LeaderboardScore)paramObject).getScoreHolderHiResImageUri(), paramLeaderboardScore.getScoreHolderHiResImageUri())) || (!Objects.equal(((LeaderboardScore)paramObject).getScoreHolder(), paramLeaderboardScore.getScoreHolder())) || (!Objects.equal(((LeaderboardScore)paramObject).getScoreTag(), paramLeaderboardScore.getScoreTag())));
    return true;
  }
  
  static String zzb(LeaderboardScore paramLeaderboardScore)
  {
    Objects.ToStringHelper localToStringHelper = Objects.toStringHelper(paramLeaderboardScore).add("Rank", Long.valueOf(paramLeaderboardScore.getRank())).add("DisplayRank", paramLeaderboardScore.getDisplayRank()).add("Score", Long.valueOf(paramLeaderboardScore.getRawScore())).add("DisplayScore", paramLeaderboardScore.getDisplayScore()).add("Timestamp", Long.valueOf(paramLeaderboardScore.getTimestampMillis())).add("DisplayName", paramLeaderboardScore.getScoreHolderDisplayName()).add("IconImageUri", paramLeaderboardScore.getScoreHolderIconImageUri()).add("IconImageUrl", paramLeaderboardScore.getScoreHolderIconImageUrl()).add("HiResImageUri", paramLeaderboardScore.getScoreHolderHiResImageUri()).add("HiResImageUrl", paramLeaderboardScore.getScoreHolderHiResImageUrl());
    if (paramLeaderboardScore.getScoreHolder() == null) {}
    for (Object localObject = null;; localObject = paramLeaderboardScore.getScoreHolder()) {
      return localToStringHelper.add("Player", localObject).add("ScoreTag", paramLeaderboardScore.getScoreTag()).toString();
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final String getDisplayRank()
  {
    return this.zznd;
  }
  
  public final void getDisplayRank(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.zznd, paramCharArrayBuffer);
  }
  
  public final String getDisplayScore()
  {
    return this.zzne;
  }
  
  public final void getDisplayScore(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.zzne, paramCharArrayBuffer);
  }
  
  public final long getRank()
  {
    return this.zznc;
  }
  
  public final long getRawScore()
  {
    return this.rawScore;
  }
  
  public final Player getScoreHolder()
  {
    return this.zznj;
  }
  
  public final String getScoreHolderDisplayName()
  {
    if (this.zznj == null) {
      return this.zzng;
    }
    return this.zznj.getDisplayName();
  }
  
  public final void getScoreHolderDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    if (this.zznj == null)
    {
      DataUtils.copyStringToBuffer(this.zzng, paramCharArrayBuffer);
      return;
    }
    this.zznj.getDisplayName(paramCharArrayBuffer);
  }
  
  public final Uri getScoreHolderHiResImageUri()
  {
    if (this.zznj == null) {
      return this.zzni;
    }
    return this.zznj.getHiResImageUri();
  }
  
  public final String getScoreHolderHiResImageUrl()
  {
    if (this.zznj == null) {
      return this.zznl;
    }
    return this.zznj.getHiResImageUrl();
  }
  
  public final Uri getScoreHolderIconImageUri()
  {
    if (this.zznj == null) {
      return this.zznh;
    }
    return this.zznj.getIconImageUri();
  }
  
  public final String getScoreHolderIconImageUrl()
  {
    if (this.zznj == null) {
      return this.zznk;
    }
    return this.zznj.getIconImageUrl();
  }
  
  public final String getScoreTag()
  {
    return this.scoreTag;
  }
  
  public final long getTimestampMillis()
  {
    return this.zznf;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\leaderboard\LeaderboardScoreEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */