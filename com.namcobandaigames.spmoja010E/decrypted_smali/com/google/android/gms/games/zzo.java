package com.google.android.gms.games;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzo
  extends zzah<Void>
{
  zzo(GamesClient paramGamesClient, View paramView) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException
  {
    paramzze.zza(this.zzbe);
    paramTaskCompletionSource.setResult(null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */