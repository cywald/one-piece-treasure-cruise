package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder.ListenerKey;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzt;
import com.google.android.gms.games.video.Videos.CaptureOverlayStateListener;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcz
  extends zzt<Videos.CaptureOverlayStateListener>
{
  zzcz(VideosClient paramVideosClient, ListenerHolder.ListenerKey paramListenerKey)
  {
    super(paramListenerKey);
  }
  
  protected final void zzc(zze paramzze, TaskCompletionSource<Boolean> paramTaskCompletionSource)
    throws RemoteException, SecurityException
  {
    paramzze.zzbb();
    paramTaskCompletionSource.setResult(Boolean.valueOf(true));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzcz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */