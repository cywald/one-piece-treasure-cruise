package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzs;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.zzh;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbj
  extends zzs<zzh>
{
  zzbj(RealTimeMultiplayerClient paramRealTimeMultiplayerClient, ListenerHolder paramListenerHolder1, ListenerHolder paramListenerHolder2, RoomConfig paramRoomConfig)
  {
    super(paramListenerHolder1);
  }
  
  protected final void zzb(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException, SecurityException
  {
    paramzze.zza(this.zzbi, this.zzbi, this.zzbi, this.zzdo);
    paramTaskCompletionSource.setResult(null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */