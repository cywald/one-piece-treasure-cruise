package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.internal.zzr;

final class zze
  implements zzr
{
  public final boolean zza(@NonNull Status paramStatus)
  {
    return (paramStatus.isSuccess()) || (paramStatus.getStatusCode() == 3003);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */