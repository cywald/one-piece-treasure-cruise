package com.google.android.gms.games;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="PlayerLevelCreator")
@SafeParcelable.Reserved({1000})
public final class PlayerLevel
  extends zzd
{
  public static final Parcelable.Creator<PlayerLevel> CREATOR = new zzaq();
  @SafeParcelable.Field(getter="getLevelNumber", id=1)
  private final int zzcp;
  @SafeParcelable.Field(getter="getMinXp", id=2)
  private final long zzcq;
  @SafeParcelable.Field(getter="getMaxXp", id=3)
  private final long zzcr;
  
  @SafeParcelable.Constructor
  public PlayerLevel(@SafeParcelable.Param(id=1) int paramInt, @SafeParcelable.Param(id=2) long paramLong1, @SafeParcelable.Param(id=3) long paramLong2)
  {
    if (paramLong1 >= 0L)
    {
      bool1 = true;
      Preconditions.checkState(bool1, "Min XP must be positive!");
      if (paramLong2 <= paramLong1) {
        break label64;
      }
    }
    label64:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      Preconditions.checkState(bool1, "Max XP must be more than min XP!");
      this.zzcp = paramInt;
      this.zzcq = paramLong1;
      this.zzcr = paramLong2;
      return;
      bool1 = false;
      break;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof PlayerLevel)) {}
    do
    {
      return false;
      if (this == paramObject) {
        return true;
      }
      paramObject = (PlayerLevel)paramObject;
    } while ((!Objects.equal(Integer.valueOf(((PlayerLevel)paramObject).getLevelNumber()), Integer.valueOf(getLevelNumber()))) || (!Objects.equal(Long.valueOf(((PlayerLevel)paramObject).getMinXp()), Long.valueOf(getMinXp()))) || (!Objects.equal(Long.valueOf(((PlayerLevel)paramObject).getMaxXp()), Long.valueOf(getMaxXp()))));
    return true;
  }
  
  public final int getLevelNumber()
  {
    return this.zzcp;
  }
  
  public final long getMaxXp()
  {
    return this.zzcr;
  }
  
  public final long getMinXp()
  {
    return this.zzcq;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(this.zzcp), Long.valueOf(this.zzcq), Long.valueOf(this.zzcr) });
  }
  
  public final String toString()
  {
    return Objects.toStringHelper(this).add("LevelNumber", Integer.valueOf(getLevelNumber())).add("MinXp", Long.valueOf(getMinXp())).add("MaxXp", Long.valueOf(getMaxXp())).toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 1, getLevelNumber());
    SafeParcelWriter.writeLong(paramParcel, 2, getMinXp());
    SafeParcelWriter.writeLong(paramParcel, 3, getMaxXp());
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\PlayerLevel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */