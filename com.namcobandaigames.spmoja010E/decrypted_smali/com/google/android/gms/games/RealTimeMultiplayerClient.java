package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolders;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer.ReliableMessageSentCallback;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.zzh;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;
import java.util.List;

public class RealTimeMultiplayerClient
  extends zzu
{
  RealTimeMultiplayerClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  RealTimeMultiplayerClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<Void> create(@NonNull RoomConfig paramRoomConfig)
  {
    ListenerHolder localListenerHolder = registerListener(paramRoomConfig.zzch(), zzh.class.getSimpleName());
    return doRegisterEventListener(new zzbj(this, localListenerHolder, localListenerHolder, paramRoomConfig), new zzbk(this, localListenerHolder.getListenerKey()));
  }
  
  public Task<Void> declineInvitation(@NonNull String paramString)
  {
    return doWrite(new zzbe(this, paramString));
  }
  
  public Task<Void> dismissInvitation(@NonNull String paramString)
  {
    return doWrite(new zzbf(this, paramString));
  }
  
  public Task<Intent> getSelectOpponentsIntent(@IntRange(from=1L) int paramInt1, @IntRange(from=1L) int paramInt2)
  {
    return getSelectOpponentsIntent(paramInt1, paramInt2, true);
  }
  
  public Task<Intent> getSelectOpponentsIntent(@IntRange(from=1L) int paramInt1, @IntRange(from=1L) int paramInt2, boolean paramBoolean)
  {
    return doRead(new zzbi(this, paramInt1, paramInt2, paramBoolean));
  }
  
  public Task<Intent> getWaitingRoomIntent(@NonNull Room paramRoom, @IntRange(from=0L) int paramInt)
  {
    return doRead(new zzba(this, paramRoom, paramInt));
  }
  
  public Task<Void> join(@NonNull RoomConfig paramRoomConfig)
  {
    ListenerHolder localListenerHolder = registerListener(paramRoomConfig.zzch(), zzh.class.getSimpleName());
    return doRegisterEventListener(new zzbl(this, localListenerHolder, localListenerHolder, paramRoomConfig), new zzbm(this, localListenerHolder.getListenerKey()));
  }
  
  public Task<Void> leave(@NonNull RoomConfig paramRoomConfig, @NonNull String paramString)
  {
    ListenerHolder localListenerHolder = registerListener(paramRoomConfig.zzch(), zzh.class.getSimpleName());
    return doRead(new zzbg(this, paramString)).continueWithTask(new zzbq(this, localListenerHolder)).continueWithTask(new zzbn(this, localListenerHolder, paramString, paramRoomConfig));
  }
  
  public Task<Integer> sendReliableMessage(@NonNull byte[] paramArrayOfByte, @NonNull String paramString1, @NonNull String paramString2, @Nullable ReliableMessageSentCallback paramReliableMessageSentCallback)
  {
    ListenerHolder localListenerHolder = null;
    if (paramReliableMessageSentCallback != null) {
      localListenerHolder = ListenerHolders.createListenerHolder(paramReliableMessageSentCallback, Looper.getMainLooper(), ReliableMessageSentCallback.class.getSimpleName());
    }
    return doWrite(new zzbr(this, localListenerHolder, paramArrayOfByte, paramString1, paramString2));
  }
  
  public Task<Void> sendUnreliableMessage(@NonNull byte[] paramArrayOfByte, @NonNull String paramString1, @NonNull String paramString2)
  {
    return doWrite(new zzbb(this, paramArrayOfByte, paramString1, paramString2));
  }
  
  public Task<Void> sendUnreliableMessage(@NonNull byte[] paramArrayOfByte, @NonNull String paramString, @NonNull List<String> paramList)
  {
    return doWrite(new zzbc(this, paramList, paramArrayOfByte, paramString));
  }
  
  public Task<Void> sendUnreliableMessageToOthers(@NonNull byte[] paramArrayOfByte, @NonNull String paramString)
  {
    return doWrite(new zzbd(this, paramArrayOfByte, paramString));
  }
  
  public static abstract interface ReliableMessageSentCallback
    extends RealTimeMultiplayer.ReliableMessageSentCallback
  {
    public abstract void onRealTimeMessageSent(int paramInt1, int paramInt2, String paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\RealTimeMultiplayerClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */