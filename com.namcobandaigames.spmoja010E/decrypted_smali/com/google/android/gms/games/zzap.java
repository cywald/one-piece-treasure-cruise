package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.games.internal.player.zzb;

public class zzap
  implements Parcelable.Creator<PlayerEntity>
{
  public PlayerEntity zzc(Parcel paramParcel)
  {
    int k = SafeParcelReader.validateObjectHeader(paramParcel);
    String str9 = null;
    String str8 = null;
    Uri localUri4 = null;
    Uri localUri3 = null;
    long l3 = 0L;
    int j = 0;
    long l2 = 0L;
    String str7 = null;
    String str6 = null;
    String str5 = null;
    zzb localzzb = null;
    PlayerLevelInfo localPlayerLevelInfo = null;
    boolean bool3 = false;
    boolean bool2 = false;
    String str4 = null;
    String str3 = null;
    Uri localUri2 = null;
    String str2 = null;
    Uri localUri1 = null;
    String str1 = null;
    int i = 0;
    long l1 = 0L;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < k)
    {
      int m = SafeParcelReader.readHeader(paramParcel);
      switch (SafeParcelReader.getFieldId(m))
      {
      case 10: 
      case 11: 
      case 12: 
      case 13: 
      case 17: 
      default: 
        SafeParcelReader.skipUnknownField(paramParcel, m);
        break;
      case 1: 
        str9 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 2: 
        str8 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 3: 
        localUri4 = (Uri)SafeParcelReader.createParcelable(paramParcel, m, Uri.CREATOR);
        break;
      case 4: 
        localUri3 = (Uri)SafeParcelReader.createParcelable(paramParcel, m, Uri.CREATOR);
        break;
      case 5: 
        l3 = SafeParcelReader.readLong(paramParcel, m);
        break;
      case 6: 
        j = SafeParcelReader.readInt(paramParcel, m);
        break;
      case 7: 
        l2 = SafeParcelReader.readLong(paramParcel, m);
        break;
      case 8: 
        str7 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 9: 
        str6 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 14: 
        str5 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 15: 
        localzzb = (zzb)SafeParcelReader.createParcelable(paramParcel, m, zzb.CREATOR);
        break;
      case 16: 
        localPlayerLevelInfo = (PlayerLevelInfo)SafeParcelReader.createParcelable(paramParcel, m, PlayerLevelInfo.CREATOR);
        break;
      case 18: 
        bool3 = SafeParcelReader.readBoolean(paramParcel, m);
        break;
      case 19: 
        bool2 = SafeParcelReader.readBoolean(paramParcel, m);
        break;
      case 20: 
        str4 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 21: 
        str3 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 22: 
        localUri2 = (Uri)SafeParcelReader.createParcelable(paramParcel, m, Uri.CREATOR);
        break;
      case 23: 
        str2 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 24: 
        localUri1 = (Uri)SafeParcelReader.createParcelable(paramParcel, m, Uri.CREATOR);
        break;
      case 25: 
        str1 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 26: 
        i = SafeParcelReader.readInt(paramParcel, m);
        break;
      case 27: 
        l1 = SafeParcelReader.readLong(paramParcel, m);
        break;
      case 28: 
        bool1 = SafeParcelReader.readBoolean(paramParcel, m);
      }
    }
    SafeParcelReader.ensureAtEnd(paramParcel, k);
    return new PlayerEntity(str9, str8, localUri4, localUri3, l3, j, l2, str7, str6, str5, localzzb, localPlayerLevelInfo, bool3, bool2, str4, str3, localUri2, str2, localUri1, str1, i, l1, bool1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */