package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks;

final class zzg
  implements BaseImplementation.ResultHolder<Status>
{
  zzg(zze paramzze, BaseGmsClient.SignOutCallbacks paramSignOutCallbacks) {}
  
  public final void setFailedResult(Status paramStatus)
  {
    this.zzgb.onSignOutComplete();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */