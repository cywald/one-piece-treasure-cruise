package com.google.android.gms.games.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.games.zzb;
import com.google.android.gms.internal.games.zzc;

public abstract class zzx
  extends zzb
  implements zzw
{
  public zzx()
  {
    super("com.google.android.gms.games.internal.IGamesClient");
  }
  
  protected final boolean dispatchTransaction(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
    throws RemoteException
  {
    if (paramInt1 == 1001)
    {
      paramParcel1 = zzn();
      paramParcel2.writeNoException();
      zzc.zzb(paramParcel2, paramParcel1);
      return true;
    }
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */