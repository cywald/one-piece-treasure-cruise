package com.google.android.gms.games.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.Display;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import com.google.android.gms.common.util.PlatformVersion;
import java.lang.ref.WeakReference;

@TargetApi(12)
final class zzaf
  extends zzac
  implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener
{
  private boolean zzfu = false;
  private WeakReference<View> zzjf;
  
  protected zzaf(zze paramzze, int paramInt)
  {
    super(paramzze, paramInt, null);
  }
  
  @TargetApi(17)
  private final void zzc(View paramView)
  {
    int j = -1;
    int i = j;
    if (PlatformVersion.isAtLeastJellyBeanMR1())
    {
      localObject = paramView.getDisplay();
      i = j;
      if (localObject != null) {
        i = ((Display)localObject).getDisplayId();
      }
    }
    Object localObject = paramView.getWindowToken();
    int[] arrayOfInt = new int[2];
    paramView.getLocationInWindow(arrayOfInt);
    j = paramView.getWidth();
    int k = paramView.getHeight();
    this.zzjd.zzje = i;
    this.zzjd.zzjb = ((IBinder)localObject);
    this.zzjd.left = arrayOfInt[0];
    this.zzjd.top = arrayOfInt[1];
    this.zzjd.right = (arrayOfInt[0] + j);
    this.zzjd.bottom = (arrayOfInt[1] + k);
    if (this.zzfu) {
      zzbj();
    }
  }
  
  public final void onGlobalLayout()
  {
    if (this.zzjf == null) {}
    View localView;
    do
    {
      return;
      localView = (View)this.zzjf.get();
    } while (localView == null);
    zzc(localView);
  }
  
  public final void onViewAttachedToWindow(View paramView)
  {
    zzc(paramView);
  }
  
  public final void onViewDetachedFromWindow(View paramView)
  {
    this.zzjc.zzbd();
    paramView.removeOnAttachStateChangeListener(this);
  }
  
  @TargetApi(16)
  public final void zzb(View paramView)
  {
    this.zzjc.zzbd();
    Object localObject2;
    Object localObject1;
    if (this.zzjf != null)
    {
      localObject2 = (View)this.zzjf.get();
      Context localContext = this.zzjc.getContext();
      localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = localObject2;
        if ((localContext instanceof Activity)) {
          localObject1 = ((Activity)localContext).getWindow().getDecorView();
        }
      }
      if (localObject1 != null)
      {
        ((View)localObject1).removeOnAttachStateChangeListener(this);
        localObject1 = ((View)localObject1).getViewTreeObserver();
        if (!PlatformVersion.isAtLeastJellyBean()) {
          break label186;
        }
        ((ViewTreeObserver)localObject1).removeOnGlobalLayoutListener(this);
      }
    }
    for (;;)
    {
      this.zzjf = null;
      localObject2 = this.zzjc.getContext();
      localObject1 = paramView;
      if (paramView == null)
      {
        localObject1 = paramView;
        if ((localObject2 instanceof Activity))
        {
          localObject1 = ((Activity)localObject2).findViewById(16908290);
          paramView = (View)localObject1;
          if (localObject1 == null) {
            paramView = ((Activity)localObject2).getWindow().getDecorView();
          }
          zzh.w("PopupManager", "You have not specified a View to use as content view for popups. Falling back to the Activity content view. Note that this may not work as expected in multi-screen environments");
          localObject1 = paramView;
        }
      }
      if (localObject1 == null) {
        break;
      }
      zzc((View)localObject1);
      this.zzjf = new WeakReference(localObject1);
      ((View)localObject1).addOnAttachStateChangeListener(this);
      ((View)localObject1).getViewTreeObserver().addOnGlobalLayoutListener(this);
      return;
      label186:
      ((ViewTreeObserver)localObject1).removeGlobalOnLayoutListener(this);
    }
    zzh.e("PopupManager", "No content view usable to display popups. Popups will not be displayed in response to this client's calls. Use setViewForPopups() to set your content view.");
  }
  
  public final void zzbj()
  {
    if (this.zzjd.zzjb != null)
    {
      super.zzbj();
      this.zzfu = false;
      return;
    }
    this.zzfu = true;
  }
  
  protected final void zzm(int paramInt)
  {
    this.zzjd = new zzae(paramInt, null, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */