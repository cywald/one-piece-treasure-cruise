package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.internal.games.zzb;
import com.google.android.gms.internal.games.zzc;

public abstract class zzv
  extends zzb
  implements zzu
{
  public zzv()
  {
    super("com.google.android.gms.games.internal.IGamesCallbacks");
  }
  
  protected final boolean dispatchTransaction(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
    throws RemoteException
  {
    switch (paramInt1)
    {
    default: 
      return false;
    case 5001: 
      zza(paramParcel1.readInt(), paramParcel1.readString());
    }
    for (;;)
    {
      paramParcel2.writeNoException();
      return true;
      zza((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzb(paramParcel1.readInt(), paramParcel1.readString());
      continue;
      zzc((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zza((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), (DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzd((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zze((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzf((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzg((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzh((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzi((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      onSignOutComplete();
      continue;
      zzk((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzl((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzs((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzt((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      onLeftRoom(paramParcel1.readInt(), paramParcel1.readString());
      continue;
      zzu((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzv((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzw((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzx((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzy((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zza((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), paramParcel1.createStringArray());
      continue;
      zzb((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), paramParcel1.createStringArray());
      continue;
      zzc((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), paramParcel1.createStringArray());
      continue;
      zzd((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), paramParcel1.createStringArray());
      continue;
      zze((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), paramParcel1.createStringArray());
      continue;
      zzf((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), paramParcel1.createStringArray());
      continue;
      onRealTimeMessageReceived((RealTimeMessage)zzc.zza(paramParcel1, RealTimeMessage.CREATOR));
      continue;
      zza(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readString());
      continue;
      zza(paramParcel1.readInt(), paramParcel1.readString(), zzc.zza(paramParcel1));
      continue;
      zzz((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzaa((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzc(paramParcel1.readInt());
      continue;
      zzab((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzd(paramParcel1.readInt());
      continue;
      onP2PConnected(paramParcel1.readString());
      continue;
      onP2PDisconnected(paramParcel1.readString());
      continue;
      zzac((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zza(paramParcel1.readInt(), (Bundle)zzc.zza(paramParcel1, Bundle.CREATOR));
      continue;
      zzn((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzo((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzp((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzq((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzc(paramParcel1.readInt(), paramParcel1.readString());
      continue;
      zzr((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      onTurnBasedMatchRemoved(paramParcel1.readString());
      continue;
      onInvitationRemoved(paramParcel1.readString());
      continue;
      zzj((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzm((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      onRequestRemoved(paramParcel1.readString());
      continue;
      zzad((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzae((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzb(paramParcel1.readInt(), (Bundle)zzc.zza(paramParcel1, Bundle.CREATOR));
      continue;
      zzaf((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzc(paramParcel1.readInt(), (Bundle)zzc.zza(paramParcel1, Bundle.CREATOR));
      continue;
      zzag((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zza((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), (Contents)zzc.zza(paramParcel1, Contents.CREATOR));
      continue;
      zza((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR), paramParcel1.readString(), (Contents)zzc.zza(paramParcel1, Contents.CREATOR), (Contents)zzc.zza(paramParcel1, Contents.CREATOR), (Contents)zzc.zza(paramParcel1, Contents.CREATOR));
      continue;
      zzah((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzd(paramParcel1.readInt(), paramParcel1.readString());
      continue;
      zzd(paramParcel1.readInt(), (Bundle)zzc.zza(paramParcel1, Bundle.CREATOR));
      continue;
      zzan((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzb((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzai((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzaj((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzak((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzal((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzam((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zze(paramParcel1.readInt(), (Bundle)zzc.zza(paramParcel1, Bundle.CREATOR));
      continue;
      zzao((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zze(paramParcel1.readInt());
      continue;
      zza((DataHolder[])paramParcel1.createTypedArray(DataHolder.CREATOR));
      continue;
      zzap((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzf(paramParcel1.readInt());
      continue;
      zza(paramParcel1.readInt(), (VideoCapabilities)zzc.zza(paramParcel1, VideoCapabilities.CREATOR));
      continue;
      zza(paramParcel1.readInt(), zzc.zza(paramParcel1));
      continue;
      zzaq((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzf(paramParcel1.readInt(), (Bundle)zzc.zza(paramParcel1, Bundle.CREATOR));
      continue;
      zzg(paramParcel1.readInt());
      continue;
      zzh(paramParcel1.readInt());
      continue;
      zzi(paramParcel1.readInt());
      continue;
      zzar((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzas((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzat((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzau((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzav((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzaw((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzax((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzay((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzaz((DataHolder)zzc.zza(paramParcel1, DataHolder.CREATOR));
      continue;
      zzb((Status)zzc.zza(paramParcel1, Status.CREATOR));
      continue;
      onCaptureOverlayStateChanged(paramParcel1.readInt());
      continue;
      zzg(paramParcel1.readInt(), (Bundle)zzc.zza(paramParcel1, Bundle.CREATOR));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */