package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;

public final class zzd
  extends DataBufferRef
  implements zza
{
  private final zze zzcw;
  
  public zzd(DataHolder paramDataHolder, int paramInt, zze paramzze)
  {
    super(paramDataHolder, paramInt);
    this.zzcw = paramzze;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    return zzb.zza(this, paramObject);
  }
  
  public final int hashCode()
  {
    return zzb.zza(this);
  }
  
  public final String toString()
  {
    return zzb.zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((zzb)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public final String zzbt()
  {
    return getString(this.zzcw.zzmd);
  }
  
  public final String zzbu()
  {
    return getString(this.zzcw.zzme);
  }
  
  public final long zzbv()
  {
    return getLong(this.zzcw.zzmf);
  }
  
  public final Uri zzbw()
  {
    return parseUri(this.zzcw.zzmg);
  }
  
  public final Uri zzbx()
  {
    return parseUri(this.zzcw.zzmh);
  }
  
  public final Uri zzby()
  {
    return parseUri(this.zzcw.zzmi);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\player\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */