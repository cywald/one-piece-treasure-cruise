package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.games.Game;

@VisibleForTesting
public abstract interface ExperienceEvent
  extends Parcelable, Freezable<ExperienceEvent>
{
  public abstract Game getGame();
  
  public abstract Uri getIconImageUri();
  
  @Deprecated
  @KeepName
  public abstract String getIconImageUrl();
  
  public abstract int getType();
  
  public abstract String zzbm();
  
  public abstract String zzbn();
  
  public abstract String zzbo();
  
  public abstract long zzbp();
  
  public abstract long zzbq();
  
  public abstract long zzbr();
  
  public abstract int zzbs();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\experience\ExperienceEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */