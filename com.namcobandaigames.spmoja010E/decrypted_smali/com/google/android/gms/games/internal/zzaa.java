package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="PopupLocationInfoParcelableCreator")
@SafeParcelable.Reserved({1000})
public final class zzaa
  extends zzd
{
  public static final Parcelable.Creator<zzaa> CREATOR = new zzab();
  @SafeParcelable.Field(getter="getInfoBundle", id=1)
  private final Bundle zzja;
  @SafeParcelable.Field(getter="getWindowToken", id=2)
  private final IBinder zzjb;
  
  @SafeParcelable.Constructor
  zzaa(@SafeParcelable.Param(id=1) Bundle paramBundle, @SafeParcelable.Param(id=2) IBinder paramIBinder)
  {
    this.zzja = paramBundle;
    this.zzjb = paramIBinder;
  }
  
  public zzaa(zzae paramzzae)
  {
    this.zzja = paramzzae.zzbk();
    this.zzjb = paramzzae.zzjb;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeBundle(paramParcel, 1, this.zzja, false);
    SafeParcelWriter.writeIBinder(paramParcel, 2, this.zzjb, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */