package com.google.android.gms.games.internal.player;

import android.text.TextUtils;

public final class zze
{
  public final String name;
  public final String zzcc;
  public final String zzch;
  public final String zzll;
  public final String zzlm;
  public final String zzln;
  public final String zzlo;
  public final String zzlp;
  public final String zzlq;
  public final String zzlr;
  public final String zzls;
  public final String zzlt;
  public final String zzlu;
  public final String zzlv;
  public final String zzlw;
  public final String zzlx;
  public final String zzly;
  public final String zzlz;
  public final String zzma;
  private final String zzmb;
  public final String zzmc;
  public final String zzmd;
  public final String zzme;
  public final String zzmf;
  public final String zzmg;
  public final String zzmh;
  public final String zzmi;
  public final String zzmj;
  public final String zzmk;
  public final String zzml;
  public final String zzmm;
  public final String zzmn;
  public final String zzmo;
  public final String zzmp;
  public final String zzmq;
  
  public zze(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
    {
      this.zzll = "external_player_id";
      this.zzlm = "profile_name";
      this.zzln = "profile_icon_image_uri";
      this.zzlo = "profile_icon_image_url";
      this.zzlp = "profile_hi_res_image_uri";
      this.zzlq = "profile_hi_res_image_url";
      this.zzlr = "last_updated";
      this.zzls = "is_in_circles";
      this.zzlt = "played_with_timestamp";
      this.zzlu = "current_xp_total";
      this.zzlv = "current_level";
      this.zzlw = "current_level_min_xp";
      this.zzlx = "current_level_max_xp";
      this.zzly = "next_level";
      this.zzlz = "next_level_max_xp";
      this.zzma = "last_level_up_timestamp";
      this.zzcc = "player_title";
      this.zzmb = "has_all_public_acls";
      this.zzmc = "is_profile_visible";
      this.zzmd = "most_recent_external_game_id";
      this.zzme = "most_recent_game_name";
      this.zzmf = "most_recent_activity_timestamp";
      this.zzmg = "most_recent_game_icon_uri";
      this.zzmh = "most_recent_game_hi_res_uri";
      this.zzmi = "most_recent_game_featured_uri";
      this.zzmj = "has_debug_access";
      this.zzch = "gamer_tag";
      this.name = "real_name";
      this.zzmk = "banner_image_landscape_uri";
      this.zzml = "banner_image_landscape_url";
      this.zzmm = "banner_image_portrait_uri";
      this.zzmn = "banner_image_portrait_url";
      this.zzmo = "gamer_friend_status";
      this.zzmp = "gamer_friend_update_timestamp";
      paramString = "is_muted";
    }
    for (;;)
    {
      this.zzmq = paramString;
      return;
      String str1 = String.valueOf(paramString);
      String str2 = String.valueOf("external_player_id");
      if (str2.length() != 0)
      {
        str1 = str1.concat(str2);
        label248:
        this.zzll = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("profile_name");
        if (str2.length() == 0) {
          break label1249;
        }
        str1 = str1.concat(str2);
        label277:
        this.zzlm = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("profile_icon_image_uri");
        if (str2.length() == 0) {
          break label1261;
        }
        str1 = str1.concat(str2);
        label306:
        this.zzln = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("profile_icon_image_url");
        if (str2.length() == 0) {
          break label1273;
        }
        str1 = str1.concat(str2);
        label335:
        this.zzlo = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("profile_hi_res_image_uri");
        if (str2.length() == 0) {
          break label1285;
        }
        str1 = str1.concat(str2);
        label364:
        this.zzlp = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("profile_hi_res_image_url");
        if (str2.length() == 0) {
          break label1297;
        }
        str1 = str1.concat(str2);
        label393:
        this.zzlq = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("last_updated");
        if (str2.length() == 0) {
          break label1309;
        }
        str1 = str1.concat(str2);
        label422:
        this.zzlr = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("is_in_circles");
        if (str2.length() == 0) {
          break label1321;
        }
        str1 = str1.concat(str2);
        label451:
        this.zzls = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("played_with_timestamp");
        if (str2.length() == 0) {
          break label1333;
        }
        str1 = str1.concat(str2);
        label480:
        this.zzlt = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("current_xp_total");
        if (str2.length() == 0) {
          break label1345;
        }
        str1 = str1.concat(str2);
        label509:
        this.zzlu = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("current_level");
        if (str2.length() == 0) {
          break label1357;
        }
        str1 = str1.concat(str2);
        label538:
        this.zzlv = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("current_level_min_xp");
        if (str2.length() == 0) {
          break label1369;
        }
        str1 = str1.concat(str2);
        label567:
        this.zzlw = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("current_level_max_xp");
        if (str2.length() == 0) {
          break label1381;
        }
        str1 = str1.concat(str2);
        label596:
        this.zzlx = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("next_level");
        if (str2.length() == 0) {
          break label1393;
        }
        str1 = str1.concat(str2);
        label625:
        this.zzly = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("next_level_max_xp");
        if (str2.length() == 0) {
          break label1405;
        }
        str1 = str1.concat(str2);
        label654:
        this.zzlz = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("last_level_up_timestamp");
        if (str2.length() == 0) {
          break label1417;
        }
        str1 = str1.concat(str2);
        label683:
        this.zzma = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("player_title");
        if (str2.length() == 0) {
          break label1429;
        }
        str1 = str1.concat(str2);
        label712:
        this.zzcc = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("has_all_public_acls");
        if (str2.length() == 0) {
          break label1441;
        }
        str1 = str1.concat(str2);
        label741:
        this.zzmb = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("is_profile_visible");
        if (str2.length() == 0) {
          break label1453;
        }
        str1 = str1.concat(str2);
        label770:
        this.zzmc = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("most_recent_external_game_id");
        if (str2.length() == 0) {
          break label1465;
        }
        str1 = str1.concat(str2);
        label799:
        this.zzmd = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("most_recent_game_name");
        if (str2.length() == 0) {
          break label1477;
        }
        str1 = str1.concat(str2);
        label828:
        this.zzme = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("most_recent_activity_timestamp");
        if (str2.length() == 0) {
          break label1489;
        }
        str1 = str1.concat(str2);
        label857:
        this.zzmf = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("most_recent_game_icon_uri");
        if (str2.length() == 0) {
          break label1501;
        }
        str1 = str1.concat(str2);
        label886:
        this.zzmg = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("most_recent_game_hi_res_uri");
        if (str2.length() == 0) {
          break label1513;
        }
        str1 = str1.concat(str2);
        label915:
        this.zzmh = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("most_recent_game_featured_uri");
        if (str2.length() == 0) {
          break label1525;
        }
        str1 = str1.concat(str2);
        label944:
        this.zzmi = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("has_debug_access");
        if (str2.length() == 0) {
          break label1537;
        }
        str1 = str1.concat(str2);
        label973:
        this.zzmj = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("gamer_tag");
        if (str2.length() == 0) {
          break label1549;
        }
        str1 = str1.concat(str2);
        label1002:
        this.zzch = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("real_name");
        if (str2.length() == 0) {
          break label1561;
        }
        str1 = str1.concat(str2);
        label1031:
        this.name = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("banner_image_landscape_uri");
        if (str2.length() == 0) {
          break label1573;
        }
        str1 = str1.concat(str2);
        label1060:
        this.zzmk = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("banner_image_landscape_url");
        if (str2.length() == 0) {
          break label1585;
        }
        str1 = str1.concat(str2);
        label1089:
        this.zzml = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("banner_image_portrait_uri");
        if (str2.length() == 0) {
          break label1597;
        }
        str1 = str1.concat(str2);
        label1118:
        this.zzmm = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("banner_image_portrait_url");
        if (str2.length() == 0) {
          break label1609;
        }
        str1 = str1.concat(str2);
        label1147:
        this.zzmn = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("gamer_friend_status");
        if (str2.length() == 0) {
          break label1621;
        }
        str1 = str1.concat(str2);
        label1176:
        this.zzmo = str1;
        str1 = String.valueOf(paramString);
        str2 = String.valueOf("gamer_friend_update_timestamp");
        if (str2.length() == 0) {
          break label1633;
        }
      }
      label1249:
      label1261:
      label1273:
      label1285:
      label1297:
      label1309:
      label1321:
      label1333:
      label1345:
      label1357:
      label1369:
      label1381:
      label1393:
      label1405:
      label1417:
      label1429:
      label1441:
      label1453:
      label1465:
      label1477:
      label1489:
      label1501:
      label1513:
      label1525:
      label1537:
      label1549:
      label1561:
      label1573:
      label1585:
      label1597:
      label1609:
      label1621:
      label1633:
      for (str1 = str1.concat(str2);; str1 = new String(str1))
      {
        this.zzmp = str1;
        paramString = String.valueOf(paramString);
        str1 = String.valueOf("is_muted");
        if (str1.length() == 0) {
          break label1645;
        }
        paramString = paramString.concat(str1);
        break;
        str1 = new String(str1);
        break label248;
        str1 = new String(str1);
        break label277;
        str1 = new String(str1);
        break label306;
        str1 = new String(str1);
        break label335;
        str1 = new String(str1);
        break label364;
        str1 = new String(str1);
        break label393;
        str1 = new String(str1);
        break label422;
        str1 = new String(str1);
        break label451;
        str1 = new String(str1);
        break label480;
        str1 = new String(str1);
        break label509;
        str1 = new String(str1);
        break label538;
        str1 = new String(str1);
        break label567;
        str1 = new String(str1);
        break label596;
        str1 = new String(str1);
        break label625;
        str1 = new String(str1);
        break label654;
        str1 = new String(str1);
        break label683;
        str1 = new String(str1);
        break label712;
        str1 = new String(str1);
        break label741;
        str1 = new String(str1);
        break label770;
        str1 = new String(str1);
        break label799;
        str1 = new String(str1);
        break label828;
        str1 = new String(str1);
        break label857;
        str1 = new String(str1);
        break label886;
        str1 = new String(str1);
        break label915;
        str1 = new String(str1);
        break label944;
        str1 = new String(str1);
        break label973;
        str1 = new String(str1);
        break label1002;
        str1 = new String(str1);
        break label1031;
        str1 = new String(str1);
        break label1060;
        str1 = new String(str1);
        break label1089;
        str1 = new String(str1);
        break label1118;
        str1 = new String(str1);
        break label1147;
        str1 = new String(str1);
        break label1176;
      }
      label1645:
      paramString = new String(paramString);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\player\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */