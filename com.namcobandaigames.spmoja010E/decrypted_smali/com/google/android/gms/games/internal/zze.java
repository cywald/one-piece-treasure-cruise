package com.google.android.gms.games.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.common.api.internal.DataHolderNotifier;
import com.google.android.gms.common.api.internal.DataHolderResult;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolder.Notifier;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks;
import com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.GmsClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.GamesOptions;
import com.google.android.gms.games.Games.GetServerAuthCodeResult;
import com.google.android.gms.games.GamesClientStatusCodes;
import com.google.android.gms.games.GamesMetadata.LoadGamesResult;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.Players.LoadPlayersResult;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events.LoadEventsResult;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardEntity;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScoreEntity;
import com.google.android.gms.games.leaderboard.Leaderboards.LeaderboardMetadataResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadScoresResult;
import com.google.android.gms.games.leaderboard.Leaderboards.SubmitScoreResult;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.Invitations.LoadInvitationsResult;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer.ReliableMessageSentCallback;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchBuffer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.CancelMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.InitiateMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LeaveMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchesResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.UpdateMatchResult;
import com.google.android.gms.games.quest.Milestone;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.QuestBuffer;
import com.google.android.gms.games.quest.QuestUpdateListener;
import com.google.android.gms.games.quest.Quests.AcceptQuestResult;
import com.google.android.gms.games.quest.Quests.ClaimMilestoneResult;
import com.google.android.gms.games.quest.Quests.LoadQuestsResult;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.GameRequestBuffer;
import com.google.android.gms.games.request.OnRequestReceivedListener;
import com.google.android.gms.games.request.Requests.LoadRequestsResult;
import com.google.android.gms.games.request.Requests.UpdateRequestsResult;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotEntity;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.SnapshotMetadataEntity;
import com.google.android.gms.games.snapshot.Snapshots.CommitSnapshotResult;
import com.google.android.gms.games.snapshot.Snapshots.DeleteSnapshotResult;
import com.google.android.gms.games.snapshot.Snapshots.LoadSnapshotsResult;
import com.google.android.gms.games.snapshot.Snapshots.OpenSnapshotResult;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.Stats.LoadPlayerStatsResult;
import com.google.android.gms.games.video.CaptureState;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos.CaptureAvailableResult;
import com.google.android.gms.games.video.Videos.CaptureCapabilitiesResult;
import com.google.android.gms.games.video.Videos.CaptureOverlayStateListener;
import com.google.android.gms.games.video.Videos.CaptureStateResult;
import com.google.android.gms.internal.games.zzej;
import com.google.android.gms.internal.games.zzel;
import com.google.android.gms.internal.games.zzem;
import com.google.android.gms.signin.internal.SignInClientImpl;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class zze
  extends GmsClient<zzy>
{
  private zzel zzfp = new zzf(this);
  private final String zzfq;
  private PlayerEntity zzfr;
  private GameEntity zzfs;
  private final zzac zzft;
  private boolean zzfu = false;
  private final Binder zzfv;
  private final long zzfw;
  private final Games.GamesOptions zzfx;
  private boolean zzfy = false;
  private Bundle zzfz;
  
  public zze(Context paramContext, Looper paramLooper, ClientSettings paramClientSettings, Games.GamesOptions paramGamesOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 1, paramClientSettings, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.zzfq = paramClientSettings.getRealClientPackageName();
    this.zzfv = new Binder();
    this.zzft = zzac.zza(this, paramClientSettings.getGravityForPopups());
    this.zzfw = hashCode();
    this.zzfx = paramGamesOptions;
    if ((!this.zzfx.zzaz) && ((paramClientSettings.getViewForPopups() != null) || ((paramContext instanceof Activity)))) {
      zza(paramClientSettings.getViewForPopups());
    }
  }
  
  private static void zza(RemoteException paramRemoteException)
  {
    zzh.w("GamesClientImpl", "service died", paramRemoteException);
  }
  
  private static <R> void zza(BaseImplementation.ResultHolder<R> paramResultHolder, SecurityException paramSecurityException)
  {
    if (paramResultHolder != null) {
      paramResultHolder.setFailedResult(GamesClientStatusCodes.zza(4));
    }
  }
  
  private static void zza(SecurityException paramSecurityException)
  {
    zzh.e("GamesClientImpl", "Is player signed out?", paramSecurityException);
  }
  
  private static Room zzba(DataHolder paramDataHolder)
  {
    com.google.android.gms.games.multiplayer.realtime.zzb localzzb = new com.google.android.gms.games.multiplayer.realtime.zzb(paramDataHolder);
    paramDataHolder = null;
    try
    {
      if (localzzb.getCount() > 0) {
        paramDataHolder = (Room)((Room)localzzb.get(0)).freeze();
      }
      return paramDataHolder;
    }
    finally
    {
      localzzb.release();
    }
  }
  
  public void connect(BaseGmsClient.ConnectionProgressReportCallbacks paramConnectionProgressReportCallbacks)
  {
    this.zzfr = null;
    this.zzfs = null;
    super.connect(paramConnectionProgressReportCallbacks);
  }
  
  public void disconnect()
  {
    this.zzfu = false;
    if (isConnected()) {}
    try
    {
      zzy localzzy = (zzy)getService();
      localzzy.zzbd();
      this.zzfp.flush();
      localzzy.zza(this.zzfw);
      super.disconnect();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzh.w("GamesClientImpl", "Failed to notify client disconnect.");
      }
    }
  }
  
  public Bundle getConnectionHint()
  {
    try
    {
      Bundle localBundle = ((zzy)getService()).getConnectionHint();
      if (localBundle != null)
      {
        localBundle.setClassLoader(zze.class.getClassLoader());
        this.zzfz = localBundle;
      }
      return localBundle;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  protected Bundle getGetServiceRequestExtraArgs()
  {
    String str = getContext().getResources().getConfiguration().locale.toString();
    Bundle localBundle = this.zzfx.zzf();
    localBundle.putString("com.google.android.gms.games.key.gamePackageName", this.zzfq);
    localBundle.putString("com.google.android.gms.games.key.desiredLocale", str);
    localBundle.putParcelable("com.google.android.gms.games.key.popupWindowToken", new BinderWrapper(this.zzft.zzjd.zzjb));
    localBundle.putInt("com.google.android.gms.games.key.API_VERSION", 6);
    localBundle.putBundle("com.google.android.gms.games.key.signInOptions", SignInClientImpl.createBundleFromClientSettings(getClientSettings()));
    return localBundle;
  }
  
  public int getMinApkVersion()
  {
    return 12451000;
  }
  
  protected String getServiceDescriptor()
  {
    return "com.google.android.gms.games.internal.IGamesService";
  }
  
  protected String getStartServiceAction()
  {
    return "com.google.android.gms.games.service.START";
  }
  
  public void onConnectionFailed(ConnectionResult paramConnectionResult)
  {
    super.onConnectionFailed(paramConnectionResult);
    this.zzfu = false;
  }
  
  protected void onPostInitHandler(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    if ((paramInt1 == 0) && (paramBundle != null))
    {
      paramBundle.setClassLoader(zze.class.getClassLoader());
      this.zzfu = paramBundle.getBoolean("show_welcome_popup");
      this.zzfy = this.zzfu;
      this.zzfr = ((PlayerEntity)paramBundle.getParcelable("com.google.android.gms.games.current_player"));
      this.zzfs = ((GameEntity)paramBundle.getParcelable("com.google.android.gms.games.current_game"));
    }
    super.onPostInitHandler(paramInt1, paramIBinder, paramBundle, paramInt2);
  }
  
  public void onUserSignOut(@NonNull BaseGmsClient.SignOutCallbacks paramSignOutCallbacks)
  {
    try
    {
      zzb(new zzg(this, paramSignOutCallbacks));
      return;
    }
    catch (RemoteException localRemoteException)
    {
      paramSignOutCallbacks.onSignOutComplete();
    }
  }
  
  public boolean requiresSignIn()
  {
    return true;
  }
  
  protected Set<Scope> validateScopes(Set<Scope> paramSet)
  {
    HashSet localHashSet = new HashSet(paramSet);
    boolean bool2 = paramSet.contains(Games.SCOPE_GAMES);
    boolean bool3 = paramSet.contains(Games.SCOPE_GAMES_LITE);
    if (paramSet.contains(Games.zzal))
    {
      if (!bool2) {}
      for (bool1 = true;; bool1 = false)
      {
        Preconditions.checkState(bool1, "Cannot have both %s and %s!", new Object[] { "https://www.googleapis.com/auth/games", "https://www.googleapis.com/auth/games.firstparty" });
        return localHashSet;
      }
    }
    if ((bool2) || (bool3)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      Preconditions.checkState(bool1, "Games APIs requires %s function.", new Object[] { "https://www.googleapis.com/auth/games_lite" });
      if ((!bool3) || (!bool2)) {
        break;
      }
      localHashSet.remove(Games.SCOPE_GAMES_LITE);
      return localHashSet;
    }
  }
  
  public final int zza(ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> paramListenerHolder, byte[] paramArrayOfByte, String paramString1, String paramString2)
    throws RemoteException
  {
    return ((zzy)getService()).zza(new zzbv(paramListenerHolder), paramArrayOfByte, paramString1, paramString2);
  }
  
  public final int zza(byte[] paramArrayOfByte, String paramString)
    throws RemoteException
  {
    return ((zzy)getService()).zzb(paramArrayOfByte, paramString, null);
  }
  
  public final int zza(byte[] paramArrayOfByte, String paramString, String[] paramArrayOfString)
  {
    Preconditions.checkNotNull(paramArrayOfString, "Participant IDs must not be null");
    try
    {
      Preconditions.checkNotNull(paramArrayOfString, "Participant IDs must not be null");
      int i = ((zzy)getService()).zzb(paramArrayOfByte, paramString, paramArrayOfString);
      return i;
    }
    catch (RemoteException paramArrayOfByte)
    {
      zza(paramArrayOfByte);
    }
    return -1;
  }
  
  public final Intent zza(int paramInt1, int paramInt2, boolean paramBoolean)
    throws RemoteException
  {
    return ((zzy)getService()).zza(paramInt1, paramInt2, paramBoolean);
  }
  
  public final Intent zza(int paramInt1, byte[] paramArrayOfByte, int paramInt2, Bitmap paramBitmap, String paramString)
  {
    try
    {
      paramArrayOfByte = ((zzy)getService()).zza(paramInt1, paramArrayOfByte, paramInt2, paramString);
      Preconditions.checkNotNull(paramBitmap, "Must provide a non null icon");
      paramArrayOfByte.putExtra("com.google.android.gms.games.REQUEST_ITEM_ICON", paramBitmap);
      return paramArrayOfByte;
    }
    catch (RemoteException paramArrayOfByte)
    {
      zza(paramArrayOfByte);
    }
    return null;
  }
  
  public final Intent zza(PlayerEntity paramPlayerEntity)
    throws RemoteException
  {
    return ((zzy)getService()).zza(paramPlayerEntity);
  }
  
  public final Intent zza(Room paramRoom, int paramInt)
    throws RemoteException
  {
    return ((zzy)getService()).zza((RoomEntity)paramRoom.freeze(), paramInt);
  }
  
  public final Intent zza(String paramString, int paramInt1, int paramInt2)
  {
    try
    {
      paramString = ((zzy)getService()).zzb(paramString, paramInt1, paramInt2);
      return paramString;
    }
    catch (RemoteException paramString)
    {
      zza(paramString);
    }
    return null;
  }
  
  public final Intent zza(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
    throws RemoteException
  {
    return ((zzy)getService()).zza(paramString, paramBoolean1, paramBoolean2, paramInt);
  }
  
  public final Intent zza(int[] paramArrayOfInt)
  {
    try
    {
      paramArrayOfInt = ((zzy)getService()).zza(paramArrayOfInt);
      return paramArrayOfInt;
    }
    catch (RemoteException paramArrayOfInt)
    {
      zza(paramArrayOfInt);
    }
    return null;
  }
  
  public final String zza(boolean paramBoolean)
    throws RemoteException
  {
    if (this.zzfr != null) {
      return this.zzfr.getPlayerId();
    }
    return ((zzy)getService()).zzbf();
  }
  
  public final void zza(IBinder paramIBinder, Bundle paramBundle)
  {
    if (isConnected()) {}
    try
    {
      ((zzy)getService()).zza(paramIBinder, paramBundle);
      return;
    }
    catch (RemoteException paramIBinder)
    {
      zza(paramIBinder);
    }
  }
  
  public final void zza(View paramView)
  {
    this.zzft.zzb(paramView);
  }
  
  public final void zza(BaseImplementation.ResultHolder<GamesMetadata.LoadGamesResult> paramResultHolder)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzb(new zzx(paramResultHolder));
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Invitations.LoadInvitationsResult> paramResultHolder, int paramInt)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzae(paramResultHolder), paramInt);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Requests.LoadRequestsResult> paramResultHolder, int paramInt1, int paramInt2, int paramInt3)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzbz(paramResultHolder), paramInt1, paramInt2, paramInt3);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Players.LoadPlayersResult> paramResultHolder, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzbn(paramResultHolder), paramInt, paramBoolean1, paramBoolean2);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchesResult> paramResultHolder, int paramInt, int[] paramArrayOfInt)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzct(paramResultHolder), paramInt, paramArrayOfInt);
      return;
    }
    catch (SecurityException paramArrayOfInt)
    {
      zza(paramResultHolder, paramArrayOfInt);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> paramResultHolder, LeaderboardScoreBuffer paramLeaderboardScoreBuffer, int paramInt1, int paramInt2)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzah(paramResultHolder), paramLeaderboardScoreBuffer.zzcb().zzcc(), paramInt1, paramInt2);
      return;
    }
    catch (SecurityException paramLeaderboardScoreBuffer)
    {
      zza(paramResultHolder, paramLeaderboardScoreBuffer);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> paramResultHolder, TurnBasedMatchConfig paramTurnBasedMatchConfig)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzco(paramResultHolder), paramTurnBasedMatchConfig.getVariant(), paramTurnBasedMatchConfig.zzci(), paramTurnBasedMatchConfig.getInvitedPlayerIds(), paramTurnBasedMatchConfig.getAutoMatchCriteria());
      return;
    }
    catch (SecurityException paramTurnBasedMatchConfig)
    {
      zza(paramResultHolder, paramTurnBasedMatchConfig);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Snapshots.CommitSnapshotResult> paramResultHolder, Snapshot paramSnapshot, SnapshotMetadataChange paramSnapshotMetadataChange)
    throws RemoteException
  {
    SnapshotContents localSnapshotContents = paramSnapshot.getSnapshotContents();
    if (!localSnapshotContents.isClosed()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "Snapshot already closed");
      Object localObject = paramSnapshotMetadataChange.zzcm();
      if (localObject != null) {
        ((BitmapTeleporter)localObject).setTempDir(getContext().getCacheDir());
      }
      localObject = localSnapshotContents.zzcl();
      localSnapshotContents.close();
      try
      {
        ((zzy)getService()).zza(new zzch(paramResultHolder), paramSnapshot.getMetadata().getSnapshotId(), (com.google.android.gms.games.snapshot.zze)paramSnapshotMetadataChange, (Contents)localObject);
        return;
      }
      catch (SecurityException paramSnapshot)
      {
        zza(paramResultHolder, paramSnapshot);
      }
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    if (paramResultHolder == null) {}
    for (Object localObject = null;; localObject = new zze(paramResultHolder)) {
      try
      {
        ((zzy)getService()).zza((zzu)localObject, paramString, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        return;
      }
      catch (SecurityException paramString)
      {
        zza(paramResultHolder, paramString);
      }
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> paramResultHolder, String paramString, int paramInt)
    throws RemoteException
  {
    if (paramResultHolder == null) {}
    for (Object localObject = null;; localObject = new zze(paramResultHolder)) {
      try
      {
        ((zzy)getService()).zza((zzu)localObject, paramString, paramInt, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        return;
      }
      catch (SecurityException paramString)
      {
        zza(paramResultHolder, paramString);
      }
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> paramResultHolder, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzah(paramResultHolder), paramString, paramInt1, paramInt2, paramInt3, paramBoolean);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Players.LoadPlayersResult> paramResultHolder, String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    int i = -1;
    switch (paramString.hashCode())
    {
    default: 
      switch (i)
      {
      default: 
        paramResultHolder = String.valueOf(paramString);
        if (paramResultHolder.length() == 0) {}
        break;
      }
      break;
    }
    for (paramResultHolder = "Invalid player collection: ".concat(paramResultHolder);; paramResultHolder = new String("Invalid player collection: "))
    {
      throw new IllegalArgumentException(paramResultHolder);
      if (!paramString.equals("played_with")) {
        break;
      }
      i = 0;
      break;
    }
    try
    {
      ((zzy)getService()).zza(new zzbn(paramResultHolder), paramString, paramInt, paramBoolean1, paramBoolean2);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Leaderboards.SubmitScoreResult> paramResultHolder, String paramString1, long paramLong, String paramString2)
    throws RemoteException
  {
    if (paramResultHolder == null) {}
    for (Object localObject = null;; localObject = new zzcl(paramResultHolder)) {
      try
      {
        ((zzy)getService()).zza((zzu)localObject, paramString1, paramLong, paramString2);
        return;
      }
      catch (SecurityException paramString1)
      {
        zza(paramResultHolder, paramString1);
      }
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LeaveMatchResult> paramResultHolder, String paramString1, String paramString2)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzcp(paramResultHolder), paramString1, paramString2);
      return;
    }
    catch (SecurityException paramString1)
    {
      zza(paramResultHolder, paramString1);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Leaderboards.LoadPlayerScoreResult> paramResultHolder, String paramString1, String paramString2, int paramInt1, int paramInt2)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzbl(paramResultHolder), null, paramString2, paramInt1, paramInt2);
      return;
    }
    catch (SecurityException paramString1)
    {
      zza(paramResultHolder, paramString1);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Snapshots.OpenSnapshotResult> paramResultHolder, String paramString1, String paramString2, SnapshotMetadataChange paramSnapshotMetadataChange, SnapshotContents paramSnapshotContents)
    throws RemoteException
  {
    if (!paramSnapshotContents.isClosed()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "SnapshotContents already closed");
      Object localObject = paramSnapshotMetadataChange.zzcm();
      if (localObject != null) {
        ((BitmapTeleporter)localObject).setTempDir(getContext().getCacheDir());
      }
      localObject = paramSnapshotContents.zzcl();
      paramSnapshotContents.close();
      try
      {
        ((zzy)getService()).zza(new zzcj(paramResultHolder), paramString1, paramString2, (com.google.android.gms.games.snapshot.zze)paramSnapshotMetadataChange, (Contents)localObject);
        return;
      }
      catch (SecurityException paramString1)
      {
        zza(paramResultHolder, paramString1);
      }
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Players.LoadPlayersResult> paramResultHolder, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzb(new zzbn(paramResultHolder), paramString, paramBoolean);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Snapshots.OpenSnapshotResult> paramResultHolder, String paramString, boolean paramBoolean, int paramInt)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzcj(paramResultHolder), paramString, paramBoolean, paramInt);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.UpdateMatchResult> paramResultHolder, String paramString1, byte[] paramArrayOfByte, String paramString2, ParticipantResult[] paramArrayOfParticipantResult)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzcs(paramResultHolder), paramString1, paramArrayOfByte, paramString2, paramArrayOfParticipantResult);
      return;
    }
    catch (SecurityException paramString1)
    {
      zza(paramResultHolder, paramString1);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.UpdateMatchResult> paramResultHolder, String paramString, byte[] paramArrayOfByte, ParticipantResult[] paramArrayOfParticipantResult)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzcs(paramResultHolder), paramString, paramArrayOfByte, paramArrayOfParticipantResult);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Players.LoadPlayersResult> paramResultHolder, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzc(new zzbn(paramResultHolder), paramBoolean);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Events.LoadEventsResult> paramResultHolder, boolean paramBoolean, String... paramVarArgs)
    throws RemoteException
  {
    this.zzfp.flush();
    try
    {
      ((zzy)getService()).zza(new zzu(paramResultHolder), paramBoolean, paramVarArgs);
      return;
    }
    catch (SecurityException paramVarArgs)
    {
      zza(paramResultHolder, paramVarArgs);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Quests.LoadQuestsResult> paramResultHolder, int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
    throws RemoteException
  {
    this.zzfp.flush();
    try
    {
      ((zzy)getService()).zza(new zzbt(paramResultHolder), paramArrayOfInt, paramInt, paramBoolean);
      return;
    }
    catch (SecurityException paramArrayOfInt)
    {
      zza(paramResultHolder, paramArrayOfInt);
    }
  }
  
  public final void zza(BaseImplementation.ResultHolder<Requests.UpdateRequestsResult> paramResultHolder, String[] paramArrayOfString)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzca(paramResultHolder), paramArrayOfString);
      return;
    }
    catch (SecurityException paramArrayOfString)
    {
      zza(paramResultHolder, paramArrayOfString);
    }
  }
  
  public final void zza(ListenerHolder<OnInvitationReceivedListener> paramListenerHolder)
    throws RemoteException
  {
    paramListenerHolder = new zzab(paramListenerHolder);
    ((zzy)getService()).zza(paramListenerHolder, this.zzfw);
  }
  
  public final void zza(ListenerHolder<? extends RoomUpdateListener> paramListenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> paramListenerHolder1, ListenerHolder<? extends RealTimeMessageReceivedListener> paramListenerHolder2, RoomConfig paramRoomConfig)
    throws RemoteException
  {
    paramListenerHolder = new zzcc(paramListenerHolder, paramListenerHolder1, paramListenerHolder2);
    ((zzy)getService()).zza(paramListenerHolder, this.zzfv, paramRoomConfig.getVariant(), paramRoomConfig.getInvitedPlayerIds(), paramRoomConfig.getAutoMatchCriteria(), false, this.zzfw);
  }
  
  public final void zza(ListenerHolder<? extends RoomUpdateListener> paramListenerHolder, String paramString)
  {
    try
    {
      ((zzy)getService()).zza(new zzcc(paramListenerHolder), paramString);
      return;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
  }
  
  public final void zza(Snapshot paramSnapshot)
    throws RemoteException
  {
    paramSnapshot = paramSnapshot.getSnapshotContents();
    if (!paramSnapshot.isClosed()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "Snapshot already closed");
      Contents localContents = paramSnapshot.zzcl();
      paramSnapshot.close();
      ((zzy)getService()).zza(localContents);
      return;
    }
  }
  
  public final void zza(String paramString, int paramInt)
  {
    this.zzfp.zza(paramString, paramInt);
  }
  
  public final void zza(String paramString, BaseImplementation.ResultHolder<Games.GetServerAuthCodeResult> paramResultHolder)
    throws RemoteException
  {
    Preconditions.checkNotEmpty(paramString, "Please provide a valid serverClientId");
    try
    {
      ((zzy)getService()).zza(paramString, new zzy(paramResultHolder));
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzaa()
    throws RemoteException
  {
    ((zzy)getService()).zzb(this.zzfw);
  }
  
  public final void zzab()
  {
    try
    {
      zzaa();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
  }
  
  public final void zzac()
    throws RemoteException
  {
    ((zzy)getService()).zzc(this.zzfw);
  }
  
  public final void zzad()
  {
    try
    {
      zzac();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
  }
  
  public final void zzae()
  {
    try
    {
      ((zzy)getService()).zze(this.zzfw);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
  }
  
  public final void zzaf()
  {
    try
    {
      ((zzy)getService()).zzd(this.zzfw);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
  }
  
  public final Intent zzag()
    throws RemoteException
  {
    return ((zzy)getService()).zzag();
  }
  
  public final Intent zzah()
  {
    try
    {
      Intent localIntent = zzag();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final Intent zzai()
    throws RemoteException
  {
    return ((zzy)getService()).zzai();
  }
  
  public final Intent zzaj()
  {
    try
    {
      Intent localIntent = zzai();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final int zzak()
    throws RemoteException
  {
    return ((zzy)getService()).zzak();
  }
  
  public final int zzal()
  {
    try
    {
      int i = zzak();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return 4368;
  }
  
  public final String zzam()
    throws RemoteException
  {
    return ((zzy)getService()).zzam();
  }
  
  public final String zzan()
  {
    try
    {
      String str = zzam();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final int zzao()
    throws RemoteException
  {
    return ((zzy)getService()).zzao();
  }
  
  public final int zzap()
  {
    try
    {
      int i = zzao();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return -1;
  }
  
  public final Intent zzaq()
  {
    try
    {
      Intent localIntent = ((zzy)getService()).zzaq();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final int zzar()
  {
    try
    {
      int i = ((zzy)getService()).zzar();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return -1;
  }
  
  public final int zzas()
  {
    try
    {
      int i = ((zzy)getService()).zzas();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return -1;
  }
  
  public final int zzat()
    throws RemoteException
  {
    return ((zzy)getService()).zzat();
  }
  
  public final int zzau()
  {
    try
    {
      int i = zzat();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return -1;
  }
  
  public final int zzav()
    throws RemoteException
  {
    return ((zzy)getService()).zzav();
  }
  
  public final int zzaw()
  {
    try
    {
      int i = zzav();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return -1;
  }
  
  public final Intent zzax()
    throws RemoteException
  {
    return ((zzy)getService()).zzbi();
  }
  
  public final Intent zzay()
  {
    try
    {
      Intent localIntent = zzax();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final boolean zzaz()
    throws RemoteException
  {
    return ((zzy)getService()).zzaz();
  }
  
  public final int zzb(ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> paramListenerHolder, byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    try
    {
      int i = zza(paramListenerHolder, paramArrayOfByte, paramString1, paramString2);
      return i;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
    return -1;
  }
  
  public final int zzb(byte[] paramArrayOfByte, String paramString)
  {
    try
    {
      int i = zza(paramArrayOfByte, paramString);
      return i;
    }
    catch (RemoteException paramArrayOfByte)
    {
      zza(paramArrayOfByte);
    }
    return -1;
  }
  
  public final Intent zzb(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    try
    {
      Intent localIntent = zza(paramInt1, paramInt2, paramBoolean);
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final Intent zzb(PlayerEntity paramPlayerEntity)
  {
    try
    {
      paramPlayerEntity = zza(paramPlayerEntity);
      return paramPlayerEntity;
    }
    catch (RemoteException paramPlayerEntity)
    {
      zza(paramPlayerEntity);
    }
    return null;
  }
  
  public final Intent zzb(Room paramRoom, int paramInt)
  {
    try
    {
      paramRoom = zza(paramRoom, paramInt);
      return paramRoom;
    }
    catch (RemoteException paramRoom)
    {
      zza(paramRoom);
    }
    return null;
  }
  
  public final Intent zzb(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    try
    {
      paramString = zza(paramString, paramBoolean1, paramBoolean2, paramInt);
      return paramString;
    }
    catch (RemoteException paramString)
    {
      zza(paramString);
    }
    return null;
  }
  
  public final String zzb(boolean paramBoolean)
  {
    try
    {
      String str = zza(true);
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Status> paramResultHolder)
    throws RemoteException
  {
    this.zzfp.flush();
    try
    {
      ((zzy)getService()).zza(new zzcg(paramResultHolder));
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Videos.CaptureAvailableResult> paramResultHolder, int paramInt)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzb(new zzh(paramResultHolder), paramInt);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    if (paramResultHolder == null) {}
    for (Object localObject = null;; localObject = new zze(paramResultHolder)) {
      try
      {
        ((zzy)getService()).zzb((zzu)localObject, paramString, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        return;
      }
      catch (SecurityException paramString)
      {
        zza(paramResultHolder, paramString);
      }
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> paramResultHolder, String paramString, int paramInt)
    throws RemoteException
  {
    if (paramResultHolder == null) {}
    for (Object localObject = null;; localObject = new zze(paramResultHolder)) {
      try
      {
        ((zzy)getService()).zzb((zzu)localObject, paramString, paramInt, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        return;
      }
      catch (SecurityException paramString)
      {
        zza(paramResultHolder, paramString);
      }
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> paramResultHolder, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzb(new zzah(paramResultHolder), paramString, paramInt1, paramInt2, paramInt3, paramBoolean);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Quests.ClaimMilestoneResult> paramResultHolder, String paramString1, String paramString2)
    throws RemoteException
  {
    this.zzfp.flush();
    try
    {
      ((zzy)getService()).zzb(new zzbr(paramResultHolder, paramString2), paramString1, paramString2);
      return;
    }
    catch (SecurityException paramString1)
    {
      zza(paramResultHolder, paramString1);
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult> paramResultHolder, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzai(paramResultHolder), paramString, paramBoolean);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult> paramResultHolder, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzb(new zzai(paramResultHolder), paramBoolean);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Quests.LoadQuestsResult> paramResultHolder, boolean paramBoolean, String[] paramArrayOfString)
    throws RemoteException
  {
    this.zzfp.flush();
    try
    {
      ((zzy)getService()).zza(new zzbt(paramResultHolder), paramArrayOfString, paramBoolean);
      return;
    }
    catch (SecurityException paramArrayOfString)
    {
      zza(paramResultHolder, paramArrayOfString);
    }
  }
  
  public final void zzb(BaseImplementation.ResultHolder<Requests.UpdateRequestsResult> paramResultHolder, String[] paramArrayOfString)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzb(new zzca(paramResultHolder), paramArrayOfString);
      return;
    }
    catch (SecurityException paramArrayOfString)
    {
      zza(paramResultHolder, paramArrayOfString);
    }
  }
  
  public final void zzb(ListenerHolder<OnInvitationReceivedListener> paramListenerHolder)
  {
    try
    {
      zza(paramListenerHolder);
      return;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
  }
  
  public final void zzb(ListenerHolder<? extends RoomUpdateListener> paramListenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> paramListenerHolder1, ListenerHolder<? extends RealTimeMessageReceivedListener> paramListenerHolder2, RoomConfig paramRoomConfig)
  {
    try
    {
      zza(paramListenerHolder, paramListenerHolder1, paramListenerHolder2, paramRoomConfig);
      return;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
  }
  
  public final void zzb(Snapshot paramSnapshot)
  {
    try
    {
      zza(paramSnapshot);
      return;
    }
    catch (RemoteException paramSnapshot)
    {
      zza(paramSnapshot);
    }
  }
  
  public final void zzb(String paramString)
    throws RemoteException
  {
    ((zzy)getService()).zzf(paramString);
  }
  
  public final void zzb(String paramString, int paramInt)
    throws RemoteException
  {
    ((zzy)getService()).zzb(paramString, paramInt);
  }
  
  public final boolean zzba()
  {
    try
    {
      boolean bool = zzaz();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return false;
  }
  
  public final void zzbb()
    throws RemoteException
  {
    ((zzy)getService()).zzf(this.zzfw);
  }
  
  public final void zzbc()
  {
    try
    {
      zzbb();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
  }
  
  public final void zzbd()
  {
    if (isConnected()) {}
    try
    {
      ((zzy)getService()).zzbd();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
  }
  
  public final Intent zzc(int paramInt1, int paramInt2, boolean paramBoolean)
    throws RemoteException
  {
    return ((zzy)getService()).zzc(paramInt1, paramInt2, paramBoolean);
  }
  
  public final void zzc(BaseImplementation.ResultHolder<Videos.CaptureCapabilitiesResult> paramResultHolder)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzc(new zzj(paramResultHolder));
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zzc(BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzb(new zzco(paramResultHolder), paramString);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzc(BaseImplementation.ResultHolder<Achievements.LoadAchievementsResult> paramResultHolder, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zza(new zzf(paramResultHolder), paramBoolean);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zzc(ListenerHolder<OnTurnBasedMatchUpdateReceivedListener> paramListenerHolder)
    throws RemoteException
  {
    paramListenerHolder = new zzaz(paramListenerHolder);
    ((zzy)getService()).zzb(paramListenerHolder, this.zzfw);
  }
  
  public final void zzc(ListenerHolder<? extends RoomUpdateListener> paramListenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> paramListenerHolder1, ListenerHolder<? extends RealTimeMessageReceivedListener> paramListenerHolder2, RoomConfig paramRoomConfig)
    throws RemoteException
  {
    paramListenerHolder = new zzcc(paramListenerHolder, paramListenerHolder1, paramListenerHolder2);
    ((zzy)getService()).zza(paramListenerHolder, this.zzfv, paramRoomConfig.getInvitationId(), false, this.zzfw);
  }
  
  public final void zzc(String paramString)
  {
    try
    {
      zzb(paramString);
      return;
    }
    catch (RemoteException paramString)
    {
      zza(paramString);
    }
  }
  
  public final void zzc(String paramString, int paramInt)
  {
    try
    {
      zzb(paramString, paramInt);
      return;
    }
    catch (RemoteException paramString)
    {
      zza(paramString);
    }
  }
  
  public final Intent zzd(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    try
    {
      Intent localIntent = zzc(paramInt1, paramInt2, paramBoolean);
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final Intent zzd(String paramString)
  {
    try
    {
      paramString = ((zzy)getService()).zzd(paramString);
      return paramString;
    }
    catch (RemoteException paramString)
    {
      zza(paramString);
    }
    return null;
  }
  
  public final void zzd(BaseImplementation.ResultHolder<Videos.CaptureStateResult> paramResultHolder)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzd(new zzn(paramResultHolder));
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zzd(BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzc(new zzco(paramResultHolder), paramString);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzd(BaseImplementation.ResultHolder<Events.LoadEventsResult> paramResultHolder, boolean paramBoolean)
    throws RemoteException
  {
    this.zzfp.flush();
    try
    {
      ((zzy)getService()).zze(new zzu(paramResultHolder), paramBoolean);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zzd(ListenerHolder<OnTurnBasedMatchUpdateReceivedListener> paramListenerHolder)
  {
    try
    {
      zzc(paramListenerHolder);
      return;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
  }
  
  public final void zzd(ListenerHolder<? extends RoomUpdateListener> paramListenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> paramListenerHolder1, ListenerHolder<? extends RealTimeMessageReceivedListener> paramListenerHolder2, RoomConfig paramRoomConfig)
  {
    try
    {
      zzc(paramListenerHolder, paramListenerHolder1, paramListenerHolder2, paramRoomConfig);
      return;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
  }
  
  public final void zzd(String paramString, int paramInt)
    throws RemoteException
  {
    ((zzy)getService()).zzd(paramString, paramInt);
  }
  
  public final void zze(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LeaveMatchResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zze(new zzcp(paramResultHolder), paramString);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zze(BaseImplementation.ResultHolder<Stats.LoadPlayerStatsResult> paramResultHolder, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzf(new zzbm(paramResultHolder), paramBoolean);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zze(ListenerHolder<QuestUpdateListener> paramListenerHolder)
  {
    try
    {
      paramListenerHolder = new zzbs(paramListenerHolder);
      ((zzy)getService()).zzd(paramListenerHolder, this.zzfw);
      return;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
  }
  
  public final void zze(String paramString)
  {
    try
    {
      ((zzy)getService()).zza(paramString, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
      return;
    }
    catch (RemoteException paramString)
    {
      zza(paramString);
    }
  }
  
  public final void zze(String paramString, int paramInt)
  {
    try
    {
      zzd(paramString, paramInt);
      return;
    }
    catch (RemoteException paramString)
    {
      zza(paramString);
    }
  }
  
  public final void zzf(BaseImplementation.ResultHolder<TurnBasedMultiplayer.CancelMatchResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzd(new zzcn(paramResultHolder), paramString);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzf(BaseImplementation.ResultHolder<Snapshots.LoadSnapshotsResult> paramResultHolder, boolean paramBoolean)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzd(new zzck(paramResultHolder), paramBoolean);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      zza(paramResultHolder, localSecurityException);
    }
  }
  
  public final void zzf(ListenerHolder<OnRequestReceivedListener> paramListenerHolder)
  {
    try
    {
      paramListenerHolder = new zzbw(paramListenerHolder);
      ((zzy)getService()).zzc(paramListenerHolder, this.zzfw);
      return;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
  }
  
  public final void zzg(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzf(new zzcq(paramResultHolder), paramString);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzg(ListenerHolder<Videos.CaptureOverlayStateListener> paramListenerHolder)
    throws RemoteException
  {
    paramListenerHolder = new zzl(paramListenerHolder);
    ((zzy)getService()).zze(paramListenerHolder, this.zzfw);
  }
  
  public final void zzh(BaseImplementation.ResultHolder<Quests.AcceptQuestResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    this.zzfp.flush();
    try
    {
      ((zzy)getService()).zzh(new zzbp(paramResultHolder), paramString);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzh(ListenerHolder<Videos.CaptureOverlayStateListener> paramListenerHolder)
  {
    try
    {
      zzg(paramListenerHolder);
      return;
    }
    catch (RemoteException paramListenerHolder)
    {
      zza(paramListenerHolder);
    }
  }
  
  public final void zzi(BaseImplementation.ResultHolder<Snapshots.DeleteSnapshotResult> paramResultHolder, String paramString)
    throws RemoteException
  {
    try
    {
      ((zzy)getService()).zzg(new zzci(paramResultHolder), paramString);
      return;
    }
    catch (SecurityException paramString)
    {
      zza(paramResultHolder, paramString);
    }
  }
  
  public final void zzj(int paramInt)
  {
    this.zzft.zzjd.gravity = paramInt;
  }
  
  public final void zzk(int paramInt)
    throws RemoteException
  {
    ((zzy)getService()).zzk(paramInt);
  }
  
  public final void zzl(int paramInt)
  {
    try
    {
      zzk(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
  }
  
  @Nullable
  public final Bundle zzo()
  {
    Bundle localBundle2 = getConnectionHint();
    Bundle localBundle1 = localBundle2;
    if (localBundle2 == null) {
      localBundle1 = this.zzfz;
    }
    this.zzfz = null;
    return localBundle1;
  }
  
  public final String zzp()
    throws RemoteException
  {
    return ((zzy)getService()).zzp();
  }
  
  public final String zzq()
  {
    try
    {
      String str = zzp();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  /* Error */
  public final Player zzr()
    throws RemoteException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 1274	com/google/android/gms/games/internal/zze:checkConnected	()V
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield 459	com/google/android/gms/games/internal/zze:zzfr	Lcom/google/android/gms/games/PlayerEntity;
    //   10: ifnonnull +54 -> 64
    //   13: new 1276	com/google/android/gms/games/PlayerBuffer
    //   16: dup
    //   17: aload_0
    //   18: invokevirtual 491	com/google/android/gms/games/internal/zze:getService	()Landroid/os/IInterface;
    //   21: checkcast 475	com/google/android/gms/games/internal/zzy
    //   24: invokeinterface 1279 1 0
    //   29: invokespecial 1280	com/google/android/gms/games/PlayerBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
    //   32: astore_1
    //   33: aload_1
    //   34: invokevirtual 1281	com/google/android/gms/games/PlayerBuffer:getCount	()I
    //   37: ifle +23 -> 60
    //   40: aload_0
    //   41: aload_1
    //   42: iconst_0
    //   43: invokevirtual 1282	com/google/android/gms/games/PlayerBuffer:get	(I)Ljava/lang/Object;
    //   46: checkcast 1284	com/google/android/gms/games/Player
    //   49: invokeinterface 1285 1 0
    //   54: checkcast 646	com/google/android/gms/games/PlayerEntity
    //   57: putfield 459	com/google/android/gms/games/internal/zze:zzfr	Lcom/google/android/gms/games/PlayerEntity;
    //   60: aload_1
    //   61: invokevirtual 1286	com/google/android/gms/games/PlayerBuffer:release	()V
    //   64: aload_0
    //   65: monitorexit
    //   66: aload_0
    //   67: getfield 459	com/google/android/gms/games/internal/zze:zzfr	Lcom/google/android/gms/games/PlayerEntity;
    //   70: areturn
    //   71: astore_2
    //   72: aload_1
    //   73: invokevirtual 1286	com/google/android/gms/games/PlayerBuffer:release	()V
    //   76: aload_2
    //   77: athrow
    //   78: astore_1
    //   79: aload_0
    //   80: monitorexit
    //   81: aload_1
    //   82: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	83	0	this	zze
    //   32	41	1	localPlayerBuffer	PlayerBuffer
    //   78	4	1	localObject1	Object
    //   71	6	2	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   33	60	71	finally
    //   6	33	78	finally
    //   60	64	78	finally
    //   64	66	78	finally
    //   72	78	78	finally
    //   79	81	78	finally
  }
  
  public final Player zzs()
  {
    try
    {
      Player localPlayer = zzr();
      return localPlayer;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  /* Error */
  public final Game zzt()
    throws RemoteException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 1274	com/google/android/gms/games/internal/zze:checkConnected	()V
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield 461	com/google/android/gms/games/internal/zze:zzfs	Lcom/google/android/gms/games/GameEntity;
    //   10: ifnonnull +54 -> 64
    //   13: new 1291	com/google/android/gms/games/GameBuffer
    //   16: dup
    //   17: aload_0
    //   18: invokevirtual 491	com/google/android/gms/games/internal/zze:getService	()Landroid/os/IInterface;
    //   21: checkcast 475	com/google/android/gms/games/internal/zzy
    //   24: invokeinterface 1293 1 0
    //   29: invokespecial 1294	com/google/android/gms/games/GameBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
    //   32: astore_1
    //   33: aload_1
    //   34: invokevirtual 1295	com/google/android/gms/games/GameBuffer:getCount	()I
    //   37: ifle +23 -> 60
    //   40: aload_0
    //   41: aload_1
    //   42: iconst_0
    //   43: invokevirtual 1296	com/google/android/gms/games/GameBuffer:get	(I)Ljava/lang/Object;
    //   46: checkcast 1298	com/google/android/gms/games/Game
    //   49: invokeinterface 1299 1 0
    //   54: checkcast 650	com/google/android/gms/games/GameEntity
    //   57: putfield 461	com/google/android/gms/games/internal/zze:zzfs	Lcom/google/android/gms/games/GameEntity;
    //   60: aload_1
    //   61: invokevirtual 1300	com/google/android/gms/games/GameBuffer:release	()V
    //   64: aload_0
    //   65: monitorexit
    //   66: aload_0
    //   67: getfield 461	com/google/android/gms/games/internal/zze:zzfs	Lcom/google/android/gms/games/GameEntity;
    //   70: areturn
    //   71: astore_2
    //   72: aload_1
    //   73: invokevirtual 1300	com/google/android/gms/games/GameBuffer:release	()V
    //   76: aload_2
    //   77: athrow
    //   78: astore_1
    //   79: aload_0
    //   80: monitorexit
    //   81: aload_1
    //   82: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	83	0	this	zze
    //   32	41	1	localGameBuffer	GameBuffer
    //   78	4	1	localObject1	Object
    //   71	6	2	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   33	60	71	finally
    //   6	33	78	finally
    //   60	64	78	finally
    //   64	66	78	finally
    //   72	78	78	finally
    //   79	81	78	finally
  }
  
  public final Game zzu()
  {
    try
    {
      Game localGame = zzt();
      return localGame;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final Intent zzv()
    throws RemoteException
  {
    return ((zzy)getService()).zzv();
  }
  
  public final Intent zzw()
  {
    try
    {
      Intent localIntent = zzv();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final Intent zzx()
  {
    try
    {
      Intent localIntent = ((zzy)getService()).zzx();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final Intent zzy()
  {
    try
    {
      Intent localIntent = ((zzy)getService()).zzy();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  public final Intent zzz()
  {
    try
    {
      Intent localIntent = ((zzy)getService()).zzz();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zza(localRemoteException);
    }
    return null;
  }
  
  private static abstract class zza
    extends zze.zzc
  {
    private final ArrayList<String> zzgc = new ArrayList();
    
    zza(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super();
      int i = 0;
      int j = paramArrayOfString.length;
      while (i < j)
      {
        this.zzgc.add(paramArrayOfString[i]);
        i += 1;
      }
    }
    
    protected final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      zza(paramRoomStatusUpdateListener, paramRoom, this.zzgc);
    }
    
    protected abstract void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList);
  }
  
  private static final class zzaa
    extends zze.zzcr
    implements TurnBasedMultiplayer.InitiateMatchResult
  {
    zzaa(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class zzab
    extends zza
  {
    private final ListenerHolder<OnInvitationReceivedListener> zzgj;
    
    zzab(ListenerHolder<OnInvitationReceivedListener> paramListenerHolder)
    {
      this.zzgj = paramListenerHolder;
    }
    
    public final void onInvitationRemoved(String paramString)
    {
      this.zzgj.notifyListener(new zze.zzad(paramString));
    }
    
    public final void zzl(DataHolder paramDataHolder)
    {
      InvitationBuffer localInvitationBuffer = new InvitationBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localInvitationBuffer.getCount() > 0) {
          paramDataHolder = (Invitation)((Invitation)localInvitationBuffer.get(0)).freeze();
        }
        localInvitationBuffer.release();
        if (paramDataHolder != null) {
          this.zzgj.notifyListener(new zze.zzac(paramDataHolder));
        }
        return;
      }
      finally
      {
        localInvitationBuffer.release();
      }
    }
  }
  
  private static final class zzac
    implements ListenerHolder.Notifier<OnInvitationReceivedListener>
  {
    private final Invitation zzgq;
    
    zzac(Invitation paramInvitation)
    {
      this.zzgq = paramInvitation;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzad
    implements ListenerHolder.Notifier<OnInvitationReceivedListener>
  {
    private final String zzgr;
    
    zzad(String paramString)
    {
      this.zzgr = paramString;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzae
    extends zza
  {
    private final BaseImplementation.ResultHolder<Invitations.LoadInvitationsResult> zzge;
    
    zzae(BaseImplementation.ResultHolder<Invitations.LoadInvitationsResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzk(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzao(paramDataHolder));
    }
  }
  
  private static final class zzaf
    extends zze.zzb
  {
    public zzaf(DataHolder paramDataHolder)
    {
      super();
    }
    
    public final void zza(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.onJoinedRoom(paramInt, paramRoom);
    }
  }
  
  private static final class zzag
    extends zze.zzw
    implements Leaderboards.LeaderboardMetadataResult
  {
    private final LeaderboardBuffer zzgs;
    
    zzag(DataHolder paramDataHolder)
    {
      super();
      this.zzgs = new LeaderboardBuffer(paramDataHolder);
    }
    
    public final LeaderboardBuffer getLeaderboards()
    {
      return this.zzgs;
    }
  }
  
  private static final class zzah
    extends zza
  {
    private final BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> zzge;
    
    zzah(BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zza(DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    {
      this.zzge.setResult(new zze.zzaw(paramDataHolder1, paramDataHolder2));
    }
  }
  
  private static final class zzai
    extends zza
  {
    private final BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult> zzge;
    
    zzai(BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzc(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzag(paramDataHolder));
    }
  }
  
  private static final class zzaj
    extends zze.zzcr
    implements TurnBasedMultiplayer.LeaveMatchResult
  {
    zzaj(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class zzak
    implements ListenerHolder.Notifier<RoomUpdateListener>
  {
    private final int statusCode;
    private final String zzgt;
    
    zzak(int paramInt, String paramString)
    {
      this.statusCode = paramInt;
      this.zzgt = paramString;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzal
    extends zze.zzw
    implements Achievements.LoadAchievementsResult
  {
    private final AchievementBuffer zzgu;
    
    zzal(DataHolder paramDataHolder)
    {
      super();
      this.zzgu = new AchievementBuffer(paramDataHolder);
    }
    
    public final AchievementBuffer getAchievements()
    {
      return this.zzgu;
    }
  }
  
  private static final class zzam
    extends zze.zzw
    implements Events.LoadEventsResult
  {
    private final EventBuffer zzgv;
    
    zzam(DataHolder paramDataHolder)
    {
      super();
      this.zzgv = new EventBuffer(paramDataHolder);
    }
    
    public final EventBuffer getEvents()
    {
      return this.zzgv;
    }
  }
  
  private static final class zzan
    extends zze.zzw
    implements GamesMetadata.LoadGamesResult
  {
    private final GameBuffer zzgw;
    
    zzan(DataHolder paramDataHolder)
    {
      super();
      this.zzgw = new GameBuffer(paramDataHolder);
    }
    
    public final GameBuffer getGames()
    {
      return this.zzgw;
    }
  }
  
  private static final class zzao
    extends zze.zzw
    implements Invitations.LoadInvitationsResult
  {
    private final InvitationBuffer zzgx;
    
    zzao(DataHolder paramDataHolder)
    {
      super();
      this.zzgx = new InvitationBuffer(paramDataHolder);
    }
    
    public final InvitationBuffer getInvitations()
    {
      return this.zzgx;
    }
  }
  
  private static final class zzap
    extends zze.zzcr
    implements TurnBasedMultiplayer.LoadMatchResult
  {
    zzap(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class zzaq
    implements TurnBasedMultiplayer.LoadMatchesResult
  {
    private final Status zzgf;
    private final LoadMatchesResponse zzgy;
    
    zzaq(Status paramStatus, Bundle paramBundle)
    {
      this.zzgf = paramStatus;
      this.zzgy = new LoadMatchesResponse(paramBundle);
    }
    
    public final LoadMatchesResponse getMatches()
    {
      return this.zzgy;
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
    
    public final void release()
    {
      this.zzgy.release();
    }
  }
  
  private static final class zzar
    extends zze.zzw
    implements Leaderboards.LoadPlayerScoreResult
  {
    private final LeaderboardScoreEntity zzgz;
    
    /* Error */
    zzar(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/zze$zzw:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:getCount	()I
      //   18: ifle +28 -> 46
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 26	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:get	(I)Ljava/lang/Object;
      //   27: checkcast 28	com/google/android/gms/games/leaderboard/LeaderboardScore
      //   30: invokeinterface 32 1 0
      //   35: checkcast 34	com/google/android/gms/games/leaderboard/LeaderboardScoreEntity
      //   38: putfield 36	com/google/android/gms/games/internal/zze$zzar:zzgz	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreEntity;
      //   41: aload_1
      //   42: invokevirtual 40	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:release	()V
      //   45: return
      //   46: aload_0
      //   47: aconst_null
      //   48: putfield 36	com/google/android/gms/games/internal/zze$zzar:zzgz	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreEntity;
      //   51: goto -10 -> 41
      //   54: astore_2
      //   55: aload_1
      //   56: invokevirtual 40	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:release	()V
      //   59: aload_2
      //   60: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	61	0	this	zzar
      //   0	61	1	paramDataHolder	DataHolder
      //   54	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	41	54	finally
      //   46	51	54	finally
    }
    
    public final LeaderboardScore getScore()
    {
      return this.zzgz;
    }
  }
  
  private static final class zzas
    extends zze.zzw
    implements Stats.LoadPlayerStatsResult
  {
    private final PlayerStats zzha;
    
    /* Error */
    zzas(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/zze$zzw:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/stats/PlayerStatsBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/stats/PlayerStatsBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/stats/PlayerStatsBuffer:getCount	()I
      //   18: ifle +27 -> 45
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/stats/zza
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/stats/PlayerStatsBuffer:get	(I)Ljava/lang/Object;
      //   31: checkcast 30	com/google/android/gms/games/stats/PlayerStats
      //   34: invokespecial 33	com/google/android/gms/games/stats/zza:<init>	(Lcom/google/android/gms/games/stats/PlayerStats;)V
      //   37: putfield 35	com/google/android/gms/games/internal/zze$zzas:zzha	Lcom/google/android/gms/games/stats/PlayerStats;
      //   40: aload_1
      //   41: invokevirtual 39	com/google/android/gms/games/stats/PlayerStatsBuffer:release	()V
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 35	com/google/android/gms/games/internal/zze$zzas:zzha	Lcom/google/android/gms/games/stats/PlayerStats;
      //   50: goto -10 -> 40
      //   53: astore_2
      //   54: aload_1
      //   55: invokevirtual 39	com/google/android/gms/games/stats/PlayerStatsBuffer:release	()V
      //   58: aload_2
      //   59: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	60	0	this	zzas
      //   0	60	1	paramDataHolder	DataHolder
      //   53	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	40	53	finally
      //   45	50	53	finally
    }
    
    public final PlayerStats getPlayerStats()
    {
      return this.zzha;
    }
  }
  
  private static final class zzat
    extends zze.zzw
    implements Players.LoadPlayersResult
  {
    private final PlayerBuffer zzhb;
    
    zzat(DataHolder paramDataHolder)
    {
      super();
      this.zzhb = new PlayerBuffer(paramDataHolder);
    }
    
    public final PlayerBuffer getPlayers()
    {
      return this.zzhb;
    }
  }
  
  private static final class zzau
    extends zze.zzw
    implements Quests.LoadQuestsResult
  {
    private final DataHolder zzhc;
    
    zzau(DataHolder paramDataHolder)
    {
      super();
      this.zzhc = paramDataHolder;
    }
    
    public final QuestBuffer getQuests()
    {
      return new QuestBuffer(this.zzhc);
    }
  }
  
  private static final class zzav
    implements Requests.LoadRequestsResult
  {
    private final Status zzgf;
    private final Bundle zzhd;
    
    zzav(Status paramStatus, Bundle paramBundle)
    {
      this.zzgf = paramStatus;
      this.zzhd = paramBundle;
    }
    
    public final GameRequestBuffer getRequests(int paramInt)
    {
      String str;
      switch (paramInt)
      {
      default: 
        zzh.e("RequestType", 33 + "Unknown request type: " + paramInt);
        str = "UNKNOWN_TYPE";
      }
      while (!this.zzhd.containsKey(str))
      {
        return null;
        str = "GIFT";
        continue;
        str = "WISH";
      }
      return new GameRequestBuffer((DataHolder)this.zzhd.get(str));
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
    
    public final void release()
    {
      Iterator localIterator = this.zzhd.keySet().iterator();
      while (localIterator.hasNext())
      {
        Object localObject = (String)localIterator.next();
        localObject = (DataHolder)this.zzhd.getParcelable((String)localObject);
        if (localObject != null) {
          ((DataHolder)localObject).close();
        }
      }
    }
  }
  
  private static final class zzaw
    extends zze.zzw
    implements Leaderboards.LoadScoresResult
  {
    private final LeaderboardEntity zzhe;
    private final LeaderboardScoreBuffer zzhf;
    
    /* Error */
    zzaw(DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_2
      //   2: invokespecial 18	com/google/android/gms/games/internal/zze$zzw:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 20	com/google/android/gms/games/leaderboard/LeaderboardBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 21	com/google/android/gms/games/leaderboard/LeaderboardBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 25	com/google/android/gms/games/leaderboard/LeaderboardBuffer:getCount	()I
      //   18: ifle +40 -> 58
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 29	com/google/android/gms/games/leaderboard/LeaderboardBuffer:get	(I)Ljava/lang/Object;
      //   27: checkcast 31	com/google/android/gms/games/leaderboard/Leaderboard
      //   30: invokeinterface 35 1 0
      //   35: checkcast 37	com/google/android/gms/games/leaderboard/LeaderboardEntity
      //   38: putfield 39	com/google/android/gms/games/internal/zze$zzaw:zzhe	Lcom/google/android/gms/games/leaderboard/LeaderboardEntity;
      //   41: aload_1
      //   42: invokevirtual 43	com/google/android/gms/games/leaderboard/LeaderboardBuffer:release	()V
      //   45: aload_0
      //   46: new 45	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer
      //   49: dup
      //   50: aload_2
      //   51: invokespecial 46	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   54: putfield 48	com/google/android/gms/games/internal/zze$zzaw:zzhf	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreBuffer;
      //   57: return
      //   58: aload_0
      //   59: aconst_null
      //   60: putfield 39	com/google/android/gms/games/internal/zze$zzaw:zzhe	Lcom/google/android/gms/games/leaderboard/LeaderboardEntity;
      //   63: goto -22 -> 41
      //   66: astore_2
      //   67: aload_1
      //   68: invokevirtual 43	com/google/android/gms/games/leaderboard/LeaderboardBuffer:release	()V
      //   71: aload_2
      //   72: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	73	0	this	zzaw
      //   0	73	1	paramDataHolder1	DataHolder
      //   0	73	2	paramDataHolder2	DataHolder
      // Exception table:
      //   from	to	target	type
      //   14	41	66	finally
      //   58	63	66	finally
    }
    
    public final Leaderboard getLeaderboard()
    {
      return this.zzhe;
    }
    
    public final LeaderboardScoreBuffer getScores()
    {
      return this.zzhf;
    }
  }
  
  private static final class zzax
    extends zze.zzw
    implements Snapshots.LoadSnapshotsResult
  {
    zzax(DataHolder paramDataHolder)
    {
      super();
    }
    
    public final SnapshotMetadataBuffer getSnapshots()
    {
      return new SnapshotMetadataBuffer(this.mDataHolder);
    }
  }
  
  private static final class zzay
    implements ListenerHolder.Notifier<OnTurnBasedMatchUpdateReceivedListener>
  {
    private final String zzhg;
    
    zzay(String paramString)
    {
      this.zzhg = paramString;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzaz
    extends zza
  {
    private final ListenerHolder<OnTurnBasedMatchUpdateReceivedListener> zzgj;
    
    zzaz(ListenerHolder<OnTurnBasedMatchUpdateReceivedListener> paramListenerHolder)
    {
      this.zzgj = paramListenerHolder;
    }
    
    public final void onTurnBasedMatchRemoved(String paramString)
    {
      this.zzgj.notifyListener(new zze.zzay(paramString));
    }
    
    public final void zzr(DataHolder paramDataHolder)
    {
      TurnBasedMatchBuffer localTurnBasedMatchBuffer = new TurnBasedMatchBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localTurnBasedMatchBuffer.getCount() > 0) {
          paramDataHolder = (TurnBasedMatch)((TurnBasedMatch)localTurnBasedMatchBuffer.get(0)).freeze();
        }
        localTurnBasedMatchBuffer.release();
        if (paramDataHolder != null) {
          this.zzgj.notifyListener(new zze.zzba(paramDataHolder));
        }
        return;
      }
      finally
      {
        localTurnBasedMatchBuffer.release();
      }
    }
  }
  
  private static abstract class zzb
    extends DataHolderNotifier<RoomUpdateListener>
  {
    zzb(DataHolder paramDataHolder)
    {
      super();
    }
    
    protected abstract void zza(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt);
  }
  
  private static final class zzba
    implements ListenerHolder.Notifier<OnTurnBasedMatchUpdateReceivedListener>
  {
    private final TurnBasedMatch match;
    
    zzba(TurnBasedMatch paramTurnBasedMatch)
    {
      this.match = paramTurnBasedMatch;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzbb
    implements ListenerHolder.Notifier<RealTimeMessageReceivedListener>
  {
    private final RealTimeMessage zzhh;
    
    zzbb(RealTimeMessage paramRealTimeMessage)
    {
      this.zzhh = paramRealTimeMessage;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzbc
    extends zze.zzw
    implements Snapshots.OpenSnapshotResult
  {
    private final String zzei;
    private final Snapshot zzhi;
    private final Snapshot zzhj;
    private final SnapshotContents zzhk;
    
    zzbc(DataHolder paramDataHolder, Contents paramContents)
    {
      this(paramDataHolder, null, paramContents, null, null);
    }
    
    zzbc(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3)
    {
      super();
      SnapshotMetadataBuffer localSnapshotMetadataBuffer = new SnapshotMetadataBuffer(paramDataHolder);
      for (;;)
      {
        try
        {
          if (localSnapshotMetadataBuffer.getCount() == 0)
          {
            this.zzhi = null;
            this.zzhj = null;
            localSnapshotMetadataBuffer.release();
            this.zzei = paramString;
            this.zzhk = new com.google.android.gms.games.snapshot.zza(paramContents3);
            return;
          }
          if (localSnapshotMetadataBuffer.getCount() != 1) {
            break label141;
          }
          if (paramDataHolder.getStatusCode() != 4004)
          {
            Asserts.checkState(bool);
            this.zzhi = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata)localSnapshotMetadataBuffer.get(0)), new com.google.android.gms.games.snapshot.zza(paramContents1));
            this.zzhj = null;
            continue;
          }
          bool = false;
        }
        finally
        {
          localSnapshotMetadataBuffer.release();
        }
        continue;
        label141:
        this.zzhi = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata)localSnapshotMetadataBuffer.get(0)), new com.google.android.gms.games.snapshot.zza(paramContents1));
        this.zzhj = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata)localSnapshotMetadataBuffer.get(1)), new com.google.android.gms.games.snapshot.zza(paramContents2));
      }
    }
    
    public final String getConflictId()
    {
      return this.zzei;
    }
    
    public final Snapshot getConflictingSnapshot()
    {
      return this.zzhj;
    }
    
    public final SnapshotContents getResolutionSnapshotContents()
    {
      return this.zzhk;
    }
    
    public final Snapshot getSnapshot()
    {
      return this.zzhi;
    }
  }
  
  private static final class zzbd
    implements ListenerHolder.Notifier<RoomStatusUpdateListener>
  {
    private final String zzhl;
    
    zzbd(String paramString)
    {
      this.zzhl = paramString;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzbe
    implements ListenerHolder.Notifier<RoomStatusUpdateListener>
  {
    private final String zzhl;
    
    zzbe(String paramString)
    {
      this.zzhl = paramString;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzbf
    extends zze.zza
  {
    zzbf(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeersConnected(paramRoom, paramArrayList);
    }
  }
  
  private static final class zzbg
    extends zze.zza
  {
    zzbg(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeerDeclined(paramRoom, paramArrayList);
    }
  }
  
  private static final class zzbh
    extends zze.zza
  {
    zzbh(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeersDisconnected(paramRoom, paramArrayList);
    }
  }
  
  private static final class zzbi
    extends zze.zza
  {
    zzbi(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeerInvitedToRoom(paramRoom, paramArrayList);
    }
  }
  
  private static final class zzbj
    extends zze.zza
  {
    zzbj(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeerJoined(paramRoom, paramArrayList);
    }
  }
  
  private static final class zzbk
    extends zze.zza
  {
    zzbk(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeerLeft(paramRoom, paramArrayList);
    }
  }
  
  private static final class zzbl
    extends zza
  {
    private final BaseImplementation.ResultHolder<Leaderboards.LoadPlayerScoreResult> zzge;
    
    zzbl(BaseImplementation.ResultHolder<Leaderboards.LoadPlayerScoreResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzac(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzar(paramDataHolder));
    }
  }
  
  private static final class zzbm
    extends zza
  {
    private final BaseImplementation.ResultHolder<Stats.LoadPlayerStatsResult> zzge;
    
    public zzbm(BaseImplementation.ResultHolder<Stats.LoadPlayerStatsResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzap(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzas(paramDataHolder));
    }
  }
  
  private static final class zzbn
    extends zza
  {
    private final BaseImplementation.ResultHolder<Players.LoadPlayersResult> zzge;
    
    zzbn(BaseImplementation.ResultHolder<Players.LoadPlayersResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zze(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzat(paramDataHolder));
    }
    
    public final void zzf(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzat(paramDataHolder));
    }
  }
  
  private static final class zzbo
    extends zzb
  {
    private final zzac zzft;
    
    public zzbo(zzac paramzzac)
    {
      this.zzft = paramzzac;
    }
    
    public final zzaa zzn()
    {
      return new zzaa(this.zzft.zzjd);
    }
  }
  
  private static final class zzbp
    extends zza
  {
    private final BaseImplementation.ResultHolder<Quests.AcceptQuestResult> zzhm;
    
    public zzbp(BaseImplementation.ResultHolder<Quests.AcceptQuestResult> paramResultHolder)
    {
      this.zzhm = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzaj(DataHolder paramDataHolder)
    {
      this.zzhm.setResult(new zze.zzd(paramDataHolder));
    }
  }
  
  private static final class zzbq
    implements ListenerHolder.Notifier<QuestUpdateListener>
  {
    private final Quest zzgd;
    
    zzbq(Quest paramQuest)
    {
      this.zzgd = paramQuest;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzbr
    extends zza
  {
    private final BaseImplementation.ResultHolder<Quests.ClaimMilestoneResult> zzhn;
    private final String zzho;
    
    public zzbr(BaseImplementation.ResultHolder<Quests.ClaimMilestoneResult> paramResultHolder, String paramString)
    {
      this.zzhn = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
      this.zzho = ((String)Preconditions.checkNotNull(paramString, "MilestoneId must not be null"));
    }
    
    public final void zzai(DataHolder paramDataHolder)
    {
      this.zzhn.setResult(new zze.zzp(paramDataHolder, this.zzho));
    }
  }
  
  private static final class zzbs
    extends zza
  {
    private final ListenerHolder<QuestUpdateListener> zzgj;
    
    zzbs(ListenerHolder<QuestUpdateListener> paramListenerHolder)
    {
      this.zzgj = paramListenerHolder;
    }
    
    private static Quest zzbc(DataHolder paramDataHolder)
    {
      QuestBuffer localQuestBuffer = new QuestBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localQuestBuffer.getCount() > 0) {
          paramDataHolder = (Quest)((Quest)localQuestBuffer.get(0)).freeze();
        }
        return paramDataHolder;
      }
      finally
      {
        localQuestBuffer.release();
      }
    }
    
    public final void zzak(DataHolder paramDataHolder)
    {
      paramDataHolder = zzbc(paramDataHolder);
      if (paramDataHolder != null) {
        this.zzgj.notifyListener(new zze.zzbq(paramDataHolder));
      }
    }
  }
  
  private static final class zzbt
    extends zza
  {
    private final BaseImplementation.ResultHolder<Quests.LoadQuestsResult> zzhp;
    
    public zzbt(BaseImplementation.ResultHolder<Quests.LoadQuestsResult> paramResultHolder)
    {
      this.zzhp = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzam(DataHolder paramDataHolder)
    {
      this.zzhp.setResult(new zze.zzau(paramDataHolder));
    }
  }
  
  private static final class zzbu
    implements ListenerHolder.Notifier<RealTimeMultiplayer.ReliableMessageSentCallback>
  {
    private final int statusCode;
    private final int token;
    private final String zzhq;
    
    zzbu(int paramInt1, int paramInt2, String paramString)
    {
      this.statusCode = paramInt1;
      this.token = paramInt2;
      this.zzhq = paramString;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzbv
    extends zza
  {
    private final ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> zzhr;
    
    public zzbv(ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> paramListenerHolder)
    {
      this.zzhr = paramListenerHolder;
    }
    
    public final void zza(int paramInt1, int paramInt2, String paramString)
    {
      if (this.zzhr != null) {
        this.zzhr.notifyListener(new zze.zzbu(paramInt1, paramInt2, paramString));
      }
    }
  }
  
  private static final class zzbw
    extends zza
  {
    private final ListenerHolder<OnRequestReceivedListener> zzgj;
    
    zzbw(ListenerHolder<OnRequestReceivedListener> paramListenerHolder)
    {
      this.zzgj = paramListenerHolder;
    }
    
    public final void onRequestRemoved(String paramString)
    {
      this.zzgj.notifyListener(new zze.zzby(paramString));
    }
    
    public final void zzm(DataHolder paramDataHolder)
    {
      GameRequestBuffer localGameRequestBuffer = new GameRequestBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localGameRequestBuffer.getCount() > 0) {
          paramDataHolder = (GameRequest)((GameRequest)localGameRequestBuffer.get(0)).freeze();
        }
        localGameRequestBuffer.release();
        if (paramDataHolder != null) {
          this.zzgj.notifyListener(new zze.zzbx(paramDataHolder));
        }
        return;
      }
      finally
      {
        localGameRequestBuffer.release();
      }
    }
  }
  
  private static final class zzbx
    implements ListenerHolder.Notifier<OnRequestReceivedListener>
  {
    private final GameRequest zzhs;
    
    zzbx(GameRequest paramGameRequest)
    {
      this.zzhs = paramGameRequest;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzby
    implements ListenerHolder.Notifier<OnRequestReceivedListener>
  {
    private final String zzht;
    
    zzby(String paramString)
    {
      this.zzht = paramString;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzbz
    extends zza
  {
    private final BaseImplementation.ResultHolder<Requests.LoadRequestsResult> zzhu;
    
    public zzbz(BaseImplementation.ResultHolder<Requests.LoadRequestsResult> paramResultHolder)
    {
      this.zzhu = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzb(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.zza(paramInt);
      this.zzhu.setResult(new zze.zzav(localStatus, paramBundle));
    }
  }
  
  private static abstract class zzc
    extends DataHolderNotifier<RoomStatusUpdateListener>
  {
    zzc(DataHolder paramDataHolder)
    {
      super();
    }
    
    protected abstract void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom);
  }
  
  private static final class zzca
    extends zza
  {
    private final BaseImplementation.ResultHolder<Requests.UpdateRequestsResult> zzhv;
    
    public zzca(BaseImplementation.ResultHolder<Requests.UpdateRequestsResult> paramResultHolder)
    {
      this.zzhv = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzad(DataHolder paramDataHolder)
    {
      this.zzhv.setResult(new zze.zzcw(paramDataHolder));
    }
  }
  
  private static final class zzcb
    extends zze.zzc
  {
    zzcb(DataHolder paramDataHolder)
    {
      super();
    }
    
    public final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.onRoomAutoMatching(paramRoom);
    }
  }
  
  private static final class zzcc
    extends zza
  {
    private final ListenerHolder<? extends RoomUpdateListener> zzhw;
    private final ListenerHolder<? extends RoomStatusUpdateListener> zzhx;
    private final ListenerHolder<? extends RealTimeMessageReceivedListener> zzhy;
    
    public zzcc(ListenerHolder<? extends RoomUpdateListener> paramListenerHolder)
    {
      this.zzhw = ((ListenerHolder)Preconditions.checkNotNull(paramListenerHolder, "Callbacks must not be null"));
      this.zzhx = null;
      this.zzhy = null;
    }
    
    public zzcc(ListenerHolder<? extends RoomUpdateListener> paramListenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> paramListenerHolder1, ListenerHolder<? extends RealTimeMessageReceivedListener> paramListenerHolder2)
    {
      this.zzhw = ((ListenerHolder)Preconditions.checkNotNull(paramListenerHolder, "Callbacks must not be null"));
      this.zzhx = paramListenerHolder1;
      this.zzhy = paramListenerHolder2;
    }
    
    public final void onLeftRoom(int paramInt, String paramString)
    {
      this.zzhw.notifyListener(new zze.zzak(paramInt, paramString));
    }
    
    public final void onP2PConnected(String paramString)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzbd(paramString));
      }
    }
    
    public final void onP2PDisconnected(String paramString)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzbe(paramString));
      }
    }
    
    public final void onRealTimeMessageReceived(RealTimeMessage paramRealTimeMessage)
    {
      if (this.zzhy != null) {
        this.zzhy.notifyListener(new zze.zzbb(paramRealTimeMessage));
      }
    }
    
    public final void zza(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzbi(paramDataHolder, paramArrayOfString));
      }
    }
    
    public final void zzb(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzbj(paramDataHolder, paramArrayOfString));
      }
    }
    
    public final void zzc(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzbk(paramDataHolder, paramArrayOfString));
      }
    }
    
    public final void zzd(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzbg(paramDataHolder, paramArrayOfString));
      }
    }
    
    public final void zze(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzbf(paramDataHolder, paramArrayOfString));
      }
    }
    
    public final void zzf(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzbh(paramDataHolder, paramArrayOfString));
      }
    }
    
    public final void zzs(DataHolder paramDataHolder)
    {
      this.zzhw.notifyListener(new zze.zzcf(paramDataHolder));
    }
    
    public final void zzt(DataHolder paramDataHolder)
    {
      this.zzhw.notifyListener(new zze.zzaf(paramDataHolder));
    }
    
    public final void zzu(DataHolder paramDataHolder)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzce(paramDataHolder));
      }
    }
    
    public final void zzv(DataHolder paramDataHolder)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzcb(paramDataHolder));
      }
    }
    
    public final void zzw(DataHolder paramDataHolder)
    {
      this.zzhw.notifyListener(new zze.zzcd(paramDataHolder));
    }
    
    public final void zzx(DataHolder paramDataHolder)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzr(paramDataHolder));
      }
    }
    
    public final void zzy(DataHolder paramDataHolder)
    {
      if (this.zzhx != null) {
        this.zzhx.notifyListener(new zze.zzt(paramDataHolder));
      }
    }
  }
  
  private static final class zzcd
    extends zze.zzb
  {
    zzcd(DataHolder paramDataHolder)
    {
      super();
    }
    
    public final void zza(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.onRoomConnected(paramInt, paramRoom);
    }
  }
  
  private static final class zzce
    extends zze.zzc
  {
    zzce(DataHolder paramDataHolder)
    {
      super();
    }
    
    public final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.onRoomConnecting(paramRoom);
    }
  }
  
  private static final class zzcf
    extends zze.zzb
  {
    public zzcf(DataHolder paramDataHolder)
    {
      super();
    }
    
    public final void zza(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.onRoomCreated(paramInt, paramRoom);
    }
  }
  
  private static final class zzcg
    extends zza
  {
    private final BaseImplementation.ResultHolder<Status> zzge;
    
    public zzcg(BaseImplementation.ResultHolder<Status> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void onSignOutComplete()
    {
      Status localStatus = GamesStatusCodes.zza(0);
      this.zzge.setResult(localStatus);
    }
  }
  
  private static final class zzch
    extends zza
  {
    private final BaseImplementation.ResultHolder<Snapshots.CommitSnapshotResult> zzhz;
    
    public zzch(BaseImplementation.ResultHolder<Snapshots.CommitSnapshotResult> paramResultHolder)
    {
      this.zzhz = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzah(DataHolder paramDataHolder)
    {
      this.zzhz.setResult(new zze.zzq(paramDataHolder));
    }
  }
  
  static final class zzci
    extends zza
  {
    private final BaseImplementation.ResultHolder<Snapshots.DeleteSnapshotResult> zzge;
    
    public zzci(BaseImplementation.ResultHolder<Snapshots.DeleteSnapshotResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzd(int paramInt, String paramString)
    {
      this.zzge.setResult(new zze.zzs(paramInt, paramString));
    }
  }
  
  private static final class zzcj
    extends zza
  {
    private final BaseImplementation.ResultHolder<Snapshots.OpenSnapshotResult> zzia;
    
    public zzcj(BaseImplementation.ResultHolder<Snapshots.OpenSnapshotResult> paramResultHolder)
    {
      this.zzia = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zza(DataHolder paramDataHolder, Contents paramContents)
    {
      this.zzia.setResult(new zze.zzbc(paramDataHolder, paramContents));
    }
    
    public final void zza(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3)
    {
      this.zzia.setResult(new zze.zzbc(paramDataHolder, paramString, paramContents1, paramContents2, paramContents3));
    }
  }
  
  private static final class zzck
    extends zza
  {
    private final BaseImplementation.ResultHolder<Snapshots.LoadSnapshotsResult> zzib;
    
    public zzck(BaseImplementation.ResultHolder<Snapshots.LoadSnapshotsResult> paramResultHolder)
    {
      this.zzib = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzag(DataHolder paramDataHolder)
    {
      this.zzib.setResult(new zze.zzax(paramDataHolder));
    }
  }
  
  private static final class zzcl
    extends zza
  {
    private final BaseImplementation.ResultHolder<Leaderboards.SubmitScoreResult> zzge;
    
    public zzcl(BaseImplementation.ResultHolder<Leaderboards.SubmitScoreResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzd(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzcm(paramDataHolder));
    }
  }
  
  private static final class zzcm
    extends zze.zzw
    implements Leaderboards.SubmitScoreResult
  {
    private final ScoreSubmissionData zzic;
    
    public zzcm(DataHolder paramDataHolder)
    {
      super();
      try
      {
        this.zzic = new ScoreSubmissionData(paramDataHolder);
        return;
      }
      finally
      {
        paramDataHolder.close();
      }
    }
    
    public final ScoreSubmissionData getScoreData()
    {
      return this.zzic;
    }
  }
  
  private static final class zzcn
    extends zza
  {
    private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.CancelMatchResult> zzid;
    
    public zzcn(BaseImplementation.ResultHolder<TurnBasedMultiplayer.CancelMatchResult> paramResultHolder)
    {
      this.zzid = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzc(int paramInt, String paramString)
    {
      Status localStatus = GamesStatusCodes.zza(paramInt);
      this.zzid.setResult(new zze.zzg(localStatus, paramString));
    }
  }
  
  private static final class zzco
    extends zza
  {
    private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> zzie;
    
    public zzco(BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> paramResultHolder)
    {
      this.zzie = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzo(DataHolder paramDataHolder)
    {
      this.zzie.setResult(new zze.zzaa(paramDataHolder));
    }
  }
  
  private static final class zzcp
    extends zza
  {
    private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.LeaveMatchResult> zzif;
    
    public zzcp(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LeaveMatchResult> paramResultHolder)
    {
      this.zzif = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzq(DataHolder paramDataHolder)
    {
      this.zzif.setResult(new zze.zzaj(paramDataHolder));
    }
  }
  
  private static final class zzcq
    extends zza
  {
    private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchResult> zzig;
    
    public zzcq(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchResult> paramResultHolder)
    {
      this.zzig = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzn(DataHolder paramDataHolder)
    {
      this.zzig.setResult(new zze.zzap(paramDataHolder));
    }
  }
  
  private static class zzcr
    extends zze.zzw
  {
    private final TurnBasedMatch match;
    
    /* Error */
    zzcr(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 13	com/google/android/gms/games/internal/zze$zzw:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 15	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 16	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 20	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:getCount	()I
      //   18: ifle +28 -> 46
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 24	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:get	(I)Ljava/lang/Object;
      //   27: checkcast 26	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch
      //   30: invokeinterface 30 1 0
      //   35: checkcast 26	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch
      //   38: putfield 32	com/google/android/gms/games/internal/zze$zzcr:match	Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
      //   41: aload_1
      //   42: invokevirtual 36	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:release	()V
      //   45: return
      //   46: aload_0
      //   47: aconst_null
      //   48: putfield 32	com/google/android/gms/games/internal/zze$zzcr:match	Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
      //   51: goto -10 -> 41
      //   54: astore_2
      //   55: aload_1
      //   56: invokevirtual 36	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:release	()V
      //   59: aload_2
      //   60: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	61	0	this	zzcr
      //   0	61	1	paramDataHolder	DataHolder
      //   54	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	41	54	finally
      //   46	51	54	finally
    }
    
    public TurnBasedMatch getMatch()
    {
      return this.match;
    }
  }
  
  private static final class zzcs
    extends zza
  {
    private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.UpdateMatchResult> zzih;
    
    public zzcs(BaseImplementation.ResultHolder<TurnBasedMultiplayer.UpdateMatchResult> paramResultHolder)
    {
      this.zzih = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzp(DataHolder paramDataHolder)
    {
      this.zzih.setResult(new zze.zzcv(paramDataHolder));
    }
  }
  
  private static final class zzct
    extends zza
  {
    private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchesResult> zzii;
    
    public zzct(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchesResult> paramResultHolder)
    {
      this.zzii = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zza(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.zza(paramInt);
      this.zzii.setResult(new zze.zzaq(localStatus, paramBundle));
    }
  }
  
  private static final class zzcu
    implements Achievements.UpdateAchievementResult
  {
    private final String zzfa;
    private final Status zzgf;
    
    zzcu(int paramInt, String paramString)
    {
      this.zzgf = GamesStatusCodes.zza(paramInt);
      this.zzfa = paramString;
    }
    
    public final String getAchievementId()
    {
      return this.zzfa;
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
  }
  
  private static final class zzcv
    extends zze.zzcr
    implements TurnBasedMultiplayer.UpdateMatchResult
  {
    zzcv(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class zzcw
    extends zze.zzw
    implements Requests.UpdateRequestsResult
  {
    private final zzem zzij;
    
    zzcw(DataHolder paramDataHolder)
    {
      super();
      this.zzij = zzem.zzbd(paramDataHolder);
    }
    
    public final Set<String> getRequestIds()
    {
      return this.zzij.getRequestIds();
    }
    
    public final int getRequestOutcome(String paramString)
    {
      return this.zzij.getRequestOutcome(paramString);
    }
  }
  
  private static final class zzd
    extends zze.zzw
    implements Quests.AcceptQuestResult
  {
    private final Quest zzgd;
    
    /* Error */
    zzd(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/zze$zzw:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/quest/QuestBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/quest/QuestBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/quest/QuestBuffer:getCount	()I
      //   18: ifle +27 -> 45
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/quest/QuestEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/quest/QuestBuffer:get	(I)Ljava/lang/Object;
      //   31: checkcast 30	com/google/android/gms/games/quest/Quest
      //   34: invokespecial 33	com/google/android/gms/games/quest/QuestEntity:<init>	(Lcom/google/android/gms/games/quest/Quest;)V
      //   37: putfield 35	com/google/android/gms/games/internal/zze$zzd:zzgd	Lcom/google/android/gms/games/quest/Quest;
      //   40: aload_1
      //   41: invokevirtual 39	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 35	com/google/android/gms/games/internal/zze$zzd:zzgd	Lcom/google/android/gms/games/quest/Quest;
      //   50: goto -10 -> 40
      //   53: astore_2
      //   54: aload_1
      //   55: invokevirtual 39	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   58: aload_2
      //   59: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	60	0	this	zzd
      //   0	60	1	paramDataHolder	DataHolder
      //   53	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	40	53	finally
      //   45	50	53	finally
    }
    
    public final Quest getQuest()
    {
      return this.zzgd;
    }
  }
  
  private static final class zze
    extends zza
  {
    private final BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> zzge;
    
    zze(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzb(int paramInt, String paramString)
    {
      this.zzge.setResult(new zze.zzcu(paramInt, paramString));
    }
  }
  
  private static final class zzf
    extends zza
  {
    private final BaseImplementation.ResultHolder<Achievements.LoadAchievementsResult> zzge;
    
    zzf(BaseImplementation.ResultHolder<Achievements.LoadAchievementsResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zza(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzal(paramDataHolder));
    }
  }
  
  private static final class zzg
    implements TurnBasedMultiplayer.CancelMatchResult
  {
    private final Status zzgf;
    private final String zzgg;
    
    zzg(Status paramStatus, String paramString)
    {
      this.zzgf = paramStatus;
      this.zzgg = paramString;
    }
    
    public final String getMatchId()
    {
      return this.zzgg;
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
  }
  
  private static final class zzh
    extends zza
  {
    private final BaseImplementation.ResultHolder<Videos.CaptureAvailableResult> zzge;
    
    zzh(BaseImplementation.ResultHolder<Videos.CaptureAvailableResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zza(int paramInt, boolean paramBoolean)
    {
      this.zzge.setResult(new zze.zzi(new Status(paramInt), paramBoolean));
    }
  }
  
  private static final class zzi
    implements Videos.CaptureAvailableResult
  {
    private final Status zzgf;
    private final boolean zzgh;
    
    zzi(Status paramStatus, boolean paramBoolean)
    {
      this.zzgf = paramStatus;
      this.zzgh = paramBoolean;
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
    
    public final boolean isAvailable()
    {
      return this.zzgh;
    }
  }
  
  private static final class zzj
    extends zza
  {
    private final BaseImplementation.ResultHolder<Videos.CaptureCapabilitiesResult> zzge;
    
    zzj(BaseImplementation.ResultHolder<Videos.CaptureCapabilitiesResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zza(int paramInt, VideoCapabilities paramVideoCapabilities)
    {
      this.zzge.setResult(new zze.zzk(new Status(paramInt), paramVideoCapabilities));
    }
  }
  
  private static final class zzk
    implements Videos.CaptureCapabilitiesResult
  {
    private final Status zzgf;
    private final VideoCapabilities zzgi;
    
    zzk(Status paramStatus, VideoCapabilities paramVideoCapabilities)
    {
      this.zzgf = paramStatus;
      this.zzgi = paramVideoCapabilities;
    }
    
    public final VideoCapabilities getCapabilities()
    {
      return this.zzgi;
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
  }
  
  private static final class zzl
    extends zza
  {
    private final ListenerHolder<Videos.CaptureOverlayStateListener> zzgj;
    
    zzl(ListenerHolder<Videos.CaptureOverlayStateListener> paramListenerHolder)
    {
      this.zzgj = ((ListenerHolder)Preconditions.checkNotNull(paramListenerHolder, "Callback must not be null"));
    }
    
    public final void onCaptureOverlayStateChanged(int paramInt)
    {
      this.zzgj.notifyListener(new zze.zzm(paramInt));
    }
  }
  
  private static final class zzm
    implements ListenerHolder.Notifier<Videos.CaptureOverlayStateListener>
  {
    private final int zzgk;
    
    zzm(int paramInt)
    {
      this.zzgk = paramInt;
    }
    
    public final void onNotifyListenerFailed() {}
  }
  
  private static final class zzn
    extends zza
  {
    private final BaseImplementation.ResultHolder<Videos.CaptureStateResult> zzge;
    
    public zzn(BaseImplementation.ResultHolder<Videos.CaptureStateResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzg(int paramInt, Bundle paramBundle)
    {
      this.zzge.setResult(new zze.zzo(new Status(paramInt), CaptureState.zzb(paramBundle)));
    }
  }
  
  private static final class zzo
    implements Videos.CaptureStateResult
  {
    private final Status zzgf;
    private final CaptureState zzgl;
    
    zzo(Status paramStatus, CaptureState paramCaptureState)
    {
      this.zzgf = paramStatus;
      this.zzgl = paramCaptureState;
    }
    
    public final CaptureState getCaptureState()
    {
      return this.zzgl;
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
  }
  
  private static final class zzp
    extends zze.zzw
    implements Quests.ClaimMilestoneResult
  {
    private final Quest zzgd;
    private final Milestone zzgm;
    
    /* Error */
    zzp(DataHolder paramDataHolder, String paramString)
    {
      // Byte code:
      //   0: iconst_0
      //   1: istore_3
      //   2: aload_0
      //   3: aload_1
      //   4: invokespecial 18	com/google/android/gms/games/internal/zze$zzw:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   7: new 20	com/google/android/gms/games/quest/QuestBuffer
      //   10: dup
      //   11: aload_1
      //   12: invokespecial 21	com/google/android/gms/games/quest/QuestBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   15: astore_1
      //   16: aload_1
      //   17: invokevirtual 25	com/google/android/gms/games/quest/QuestBuffer:getCount	()I
      //   20: ifle +108 -> 128
      //   23: aload_0
      //   24: new 27	com/google/android/gms/games/quest/QuestEntity
      //   27: dup
      //   28: aload_1
      //   29: iconst_0
      //   30: invokevirtual 31	com/google/android/gms/games/quest/QuestBuffer:get	(I)Ljava/lang/Object;
      //   33: checkcast 33	com/google/android/gms/games/quest/Quest
      //   36: invokespecial 36	com/google/android/gms/games/quest/QuestEntity:<init>	(Lcom/google/android/gms/games/quest/Quest;)V
      //   39: putfield 38	com/google/android/gms/games/internal/zze$zzp:zzgd	Lcom/google/android/gms/games/quest/Quest;
      //   42: aload_0
      //   43: getfield 38	com/google/android/gms/games/internal/zze$zzp:zzgd	Lcom/google/android/gms/games/quest/Quest;
      //   46: invokeinterface 42 1 0
      //   51: astore 5
      //   53: aload 5
      //   55: invokeinterface 47 1 0
      //   60: istore 4
      //   62: iload_3
      //   63: iload 4
      //   65: if_icmpge +53 -> 118
      //   68: aload 5
      //   70: iload_3
      //   71: invokeinterface 48 2 0
      //   76: checkcast 50	com/google/android/gms/games/quest/Milestone
      //   79: invokeinterface 54 1 0
      //   84: aload_2
      //   85: invokevirtual 60	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   88: ifeq +23 -> 111
      //   91: aload_0
      //   92: aload 5
      //   94: iload_3
      //   95: invokeinterface 48 2 0
      //   100: checkcast 50	com/google/android/gms/games/quest/Milestone
      //   103: putfield 62	com/google/android/gms/games/internal/zze$zzp:zzgm	Lcom/google/android/gms/games/quest/Milestone;
      //   106: aload_1
      //   107: invokevirtual 66	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   110: return
      //   111: iload_3
      //   112: iconst_1
      //   113: iadd
      //   114: istore_3
      //   115: goto -53 -> 62
      //   118: aload_0
      //   119: aconst_null
      //   120: putfield 62	com/google/android/gms/games/internal/zze$zzp:zzgm	Lcom/google/android/gms/games/quest/Milestone;
      //   123: aload_1
      //   124: invokevirtual 66	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   127: return
      //   128: aload_0
      //   129: aconst_null
      //   130: putfield 62	com/google/android/gms/games/internal/zze$zzp:zzgm	Lcom/google/android/gms/games/quest/Milestone;
      //   133: aload_0
      //   134: aconst_null
      //   135: putfield 38	com/google/android/gms/games/internal/zze$zzp:zzgd	Lcom/google/android/gms/games/quest/Quest;
      //   138: goto -15 -> 123
      //   141: astore_2
      //   142: aload_1
      //   143: invokevirtual 66	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   146: aload_2
      //   147: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	148	0	this	zzp
      //   0	148	1	paramDataHolder	DataHolder
      //   0	148	2	paramString	String
      //   1	114	3	i	int
      //   60	6	4	j	int
      //   51	42	5	localList	java.util.List
      // Exception table:
      //   from	to	target	type
      //   16	62	141	finally
      //   68	106	141	finally
      //   118	123	141	finally
      //   128	138	141	finally
    }
    
    public final Milestone getMilestone()
    {
      return this.zzgm;
    }
    
    public final Quest getQuest()
    {
      return this.zzgd;
    }
  }
  
  private static final class zzq
    extends zze.zzw
    implements Snapshots.CommitSnapshotResult
  {
    private final SnapshotMetadata zzgn;
    
    /* Error */
    zzq(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/zze$zzw:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:getCount	()I
      //   18: ifle +27 -> 45
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/snapshot/SnapshotMetadataEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:get	(I)Ljava/lang/Object;
      //   31: checkcast 30	com/google/android/gms/games/snapshot/SnapshotMetadata
      //   34: invokespecial 33	com/google/android/gms/games/snapshot/SnapshotMetadataEntity:<init>	(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V
      //   37: putfield 35	com/google/android/gms/games/internal/zze$zzq:zzgn	Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
      //   40: aload_1
      //   41: invokevirtual 39	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:release	()V
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 35	com/google/android/gms/games/internal/zze$zzq:zzgn	Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
      //   50: goto -10 -> 40
      //   53: astore_2
      //   54: aload_1
      //   55: invokevirtual 39	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:release	()V
      //   58: aload_2
      //   59: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	60	0	this	zzq
      //   0	60	1	paramDataHolder	DataHolder
      //   53	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	40	53	finally
      //   45	50	53	finally
    }
    
    public final SnapshotMetadata getSnapshotMetadata()
    {
      return this.zzgn;
    }
  }
  
  private static final class zzr
    extends zze.zzc
  {
    zzr(DataHolder paramDataHolder)
    {
      super();
    }
    
    public final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.onConnectedToRoom(paramRoom);
    }
  }
  
  private static final class zzs
    implements Snapshots.DeleteSnapshotResult
  {
    private final Status zzgf;
    private final String zzgo;
    
    zzs(int paramInt, String paramString)
    {
      this.zzgf = GamesStatusCodes.zza(paramInt);
      this.zzgo = paramString;
    }
    
    public final String getSnapshotId()
    {
      return this.zzgo;
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
  }
  
  private static final class zzt
    extends zze.zzc
  {
    zzt(DataHolder paramDataHolder)
    {
      super();
    }
    
    public final void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.onDisconnectedFromRoom(paramRoom);
    }
  }
  
  private static final class zzu
    extends zza
  {
    private final BaseImplementation.ResultHolder<Events.LoadEventsResult> zzge;
    
    zzu(BaseImplementation.ResultHolder<Events.LoadEventsResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzb(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzam(paramDataHolder));
    }
  }
  
  private final class zzv
    extends zzej
  {
    public zzv()
    {
      super(1000);
    }
    
    protected final void zzf(String paramString, int paramInt)
    {
      try
      {
        if (zze.this.isConnected())
        {
          ((zzy)zze.this.getService()).zza(paramString, paramInt);
          return;
        }
        zzh.e("GamesClientImpl", String.valueOf(paramString).length() + 89 + "Unable to increment event " + paramString + " by " + paramInt + " because the games client is no longer connected");
        return;
      }
      catch (RemoteException paramString)
      {
        zze.zza(zze.this, paramString);
        return;
      }
      catch (SecurityException paramString)
      {
        zze.zza(zze.this, paramString);
      }
    }
  }
  
  private static class zzw
    extends DataHolderResult
  {
    protected zzw(DataHolder paramDataHolder)
    {
      super(GamesStatusCodes.zza(paramDataHolder.getStatusCode()));
    }
  }
  
  private static final class zzx
    extends zza
  {
    private final BaseImplementation.ResultHolder<GamesMetadata.LoadGamesResult> zzge;
    
    zzx(BaseImplementation.ResultHolder<GamesMetadata.LoadGamesResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zzg(DataHolder paramDataHolder)
    {
      this.zzge.setResult(new zze.zzan(paramDataHolder));
    }
  }
  
  private static final class zzy
    extends zza
  {
    private final BaseImplementation.ResultHolder<Games.GetServerAuthCodeResult> zzge;
    
    public zzy(BaseImplementation.ResultHolder<Games.GetServerAuthCodeResult> paramResultHolder)
    {
      this.zzge = ((BaseImplementation.ResultHolder)Preconditions.checkNotNull(paramResultHolder, "Holder must not be null"));
    }
    
    public final void zza(int paramInt, String paramString)
    {
      Status localStatus = GamesStatusCodes.zza(paramInt);
      this.zzge.setResult(new zze.zzz(localStatus, paramString));
    }
  }
  
  private static final class zzz
    implements Games.GetServerAuthCodeResult
  {
    private final Status zzgf;
    private final String zzgp;
    
    zzz(Status paramStatus, String paramString)
    {
      this.zzgf = paramStatus;
      this.zzgp = paramString;
    }
    
    public final String getCode()
    {
      return this.zzgp;
    }
    
    public final Status getStatus()
    {
      return this.zzgf;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */