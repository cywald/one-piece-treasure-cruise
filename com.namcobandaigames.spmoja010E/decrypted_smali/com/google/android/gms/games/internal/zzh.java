package com.google.android.gms.games.internal;

import com.google.android.gms.common.config.GservicesValue;
import com.google.android.gms.common.internal.GmsLogger;

public final class zzh
{
  private static final GmsLogger zzik = new GmsLogger("Games");
  private static final GservicesValue<Boolean> zzil = GservicesValue.value("games.play_games_dogfood", false);
  
  public static void e(String paramString1, String paramString2)
  {
    zzik.e(paramString1, paramString2);
  }
  
  public static void e(String paramString1, String paramString2, Throwable paramThrowable)
  {
    zzik.e(paramString1, paramString2, paramThrowable);
  }
  
  public static void i(String paramString1, String paramString2, Throwable paramThrowable)
  {
    zzik.i(paramString1, paramString2, paramThrowable);
  }
  
  public static void w(String paramString1, String paramString2)
  {
    zzik.w(paramString1, paramString2);
  }
  
  public static void w(String paramString1, String paramString2, Throwable paramThrowable)
  {
    zzik.w(paramString1, paramString2, paramThrowable);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */