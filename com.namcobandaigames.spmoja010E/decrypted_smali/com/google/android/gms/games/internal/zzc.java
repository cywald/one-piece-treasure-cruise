package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

public final class zzc
{
  public static int zza(@Nullable Bundle paramBundle)
  {
    if (paramBundle == null) {
      return 0;
    }
    int i = paramBundle.size();
    if (i == 0) {
      return 0;
    }
    String[] arrayOfString = (String[])paramBundle.keySet().toArray(new String[i]);
    Arrays.sort(arrayOfString);
    int n = arrayOfString.length;
    int j = 0;
    i = 1;
    Object localObject1;
    int m;
    if (j < n)
    {
      localObject1 = arrayOfString[j];
      m = i * 31;
      localObject1 = paramBundle.get((String)localObject1);
      if (localObject1 == null) {
        break label438;
      }
      if ((localObject1 instanceof Bundle)) {
        i = zza((Bundle)localObject1) + m;
      }
    }
    for (;;)
    {
      j += 1;
      break;
      if ((localObject1 instanceof byte[]))
      {
        i = Arrays.hashCode((byte[])localObject1) + m;
      }
      else if ((localObject1 instanceof char[]))
      {
        i = Arrays.hashCode((char[])localObject1) + m;
      }
      else if ((localObject1 instanceof short[]))
      {
        i = Arrays.hashCode((short[])localObject1) + m;
      }
      else if ((localObject1 instanceof float[]))
      {
        i = Arrays.hashCode((float[])localObject1) + m;
      }
      else if ((localObject1 instanceof CharSequence[]))
      {
        i = Arrays.hashCode((CharSequence[])localObject1) + m;
      }
      else
      {
        int i1;
        int k;
        label246:
        Object localObject2;
        if ((localObject1 instanceof Object[]))
        {
          localObject1 = (Object[])localObject1;
          i1 = localObject1.length;
          k = 0;
          i = 1;
          if (k < i1)
          {
            localObject2 = localObject1[k];
            i *= 31;
            if ((localObject2 instanceof Bundle)) {
              i = zza((Bundle)localObject2) + i;
            }
          }
        }
        for (;;)
        {
          k += 1;
          break label246;
          if (localObject2 != null)
          {
            i = localObject2.hashCode() + i;
            continue;
            i = m + i;
            break;
            if ((localObject1 instanceof SparseArray))
            {
              localObject1 = (SparseArray)localObject1;
              i1 = ((SparseArray)localObject1).size();
              k = 0;
              i = 1;
              label339:
              if (k < i1)
              {
                i = (i * 31 + ((SparseArray)localObject1).keyAt(k)) * 31;
                localObject2 = ((SparseArray)localObject1).valueAt(k);
                if ((localObject2 instanceof Bundle)) {
                  i = zza((Bundle)localObject2) + i;
                }
              }
            }
            for (;;)
            {
              k += 1;
              break label339;
              if (localObject2 != null)
              {
                i = localObject2.hashCode() + i;
                continue;
                i = m + i;
                break;
                i = localObject1.hashCode() + m;
                break;
                return i;
              }
            }
          }
        }
        label438:
        i = m;
      }
    }
  }
  
  public static boolean zza(@Nullable Bundle paramBundle1, @Nullable Bundle paramBundle2)
  {
    if (paramBundle1 == paramBundle2) {
      return true;
    }
    if ((paramBundle1 == null) || (paramBundle2 == null)) {
      return false;
    }
    if (paramBundle1.size() != paramBundle2.size()) {
      return false;
    }
    Object localObject1 = paramBundle1.keySet();
    if (!((Set)localObject1).equals(paramBundle2.keySet())) {
      return false;
    }
    localObject1 = ((Set)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject3 = (String)((Iterator)localObject1).next();
      Object localObject2 = paramBundle1.get((String)localObject3);
      localObject3 = paramBundle2.get((String)localObject3);
      if (localObject2 == null)
      {
        if (localObject3 != null) {
          return false;
        }
      }
      else if ((localObject2 instanceof Bundle))
      {
        if ((!(localObject3 instanceof Bundle)) || (!zza((Bundle)localObject2, (Bundle)localObject3))) {
          return false;
        }
      }
      else if ((localObject2 instanceof byte[]))
      {
        if ((!(localObject3 instanceof byte[])) || (!Arrays.equals((byte[])localObject2, (byte[])localObject3))) {
          return false;
        }
      }
      else if ((localObject2 instanceof char[]))
      {
        if ((!(localObject3 instanceof char[])) || (!Arrays.equals((char[])localObject2, (char[])localObject3))) {
          return false;
        }
      }
      else if ((localObject2 instanceof short[]))
      {
        if ((!(localObject3 instanceof short[])) || (!Arrays.equals((short[])localObject2, (short[])localObject3))) {
          return false;
        }
      }
      else if ((localObject2 instanceof float[]))
      {
        if ((!(localObject3 instanceof float[])) || (!Arrays.equals((float[])localObject2, (float[])localObject3))) {
          return false;
        }
      }
      else if ((localObject2 instanceof CharSequence[]))
      {
        if ((!(localObject3 instanceof CharSequence[])) || (!Arrays.equals((CharSequence[])localObject2, (CharSequence[])localObject3))) {
          return false;
        }
      }
      else
      {
        int j;
        int i;
        label371:
        Object localObject4;
        Object localObject5;
        if ((localObject2 instanceof Object[]))
        {
          if ((localObject3 instanceof Object[]))
          {
            localObject2 = (Parcelable[])localObject2;
            localObject3 = (Parcelable[])localObject3;
            if (localObject2 == localObject3) {
              break label464;
            }
            j = localObject2.length;
            if (localObject3.length == j) {
              break label371;
            }
            i = 0;
          }
          while (i == 0)
          {
            return false;
            i = 0;
            for (;;)
            {
              if (i >= j) {
                break label464;
              }
              localObject4 = localObject2[i];
              localObject5 = localObject3[i];
              if (localObject4 == null)
              {
                if (localObject5 == null) {
                  break label457;
                }
                i = 0;
                break;
              }
              if ((localObject4 instanceof Bundle))
              {
                if (((localObject5 instanceof Bundle)) && (zza((Bundle)localObject4, (Bundle)localObject5))) {
                  break label457;
                }
                i = 0;
                break;
              }
              if (!localObject4.equals(localObject5))
              {
                i = 0;
                break;
              }
              label457:
              i += 1;
            }
            label464:
            i = 1;
          }
        }
        else if ((localObject2 instanceof SparseArray))
        {
          if ((localObject3 instanceof SparseArray))
          {
            localObject2 = (SparseArray)localObject2;
            localObject3 = (SparseArray)localObject3;
            if (localObject2 == localObject3) {
              break label646;
            }
            j = ((SparseArray)localObject2).size();
            if (((SparseArray)localObject3).size() == j) {
              break label529;
            }
            i = 0;
          }
          while (i == 0)
          {
            return false;
            label529:
            i = 0;
            for (;;)
            {
              if (i >= j) {
                break label646;
              }
              if (((SparseArray)localObject2).keyAt(i) != ((SparseArray)localObject3).keyAt(i))
              {
                i = 0;
                break;
              }
              localObject4 = ((SparseArray)localObject2).valueAt(i);
              localObject5 = ((SparseArray)localObject3).valueAt(i);
              if (localObject4 == null)
              {
                if (localObject5 == null) {
                  break label639;
                }
                i = 0;
                break;
              }
              if ((localObject4 instanceof Bundle))
              {
                if (((localObject5 instanceof Bundle)) && (zza((Bundle)localObject4, (Bundle)localObject5))) {
                  break label639;
                }
                i = 0;
                break;
              }
              if (!localObject4.equals(localObject5))
              {
                i = 0;
                break;
              }
              label639:
              i += 1;
            }
            label646:
            i = 1;
          }
        }
        else if (!localObject2.equals(localObject3))
        {
          return false;
        }
      }
    }
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */