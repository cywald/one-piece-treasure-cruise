package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;

public class StockProfileImageRef
  extends DataBufferRef
  implements StockProfileImage
{
  public int describeContents()
  {
    throw new NoSuchMethodError();
  }
  
  public String getImageUrl()
  {
    return getString("image_url");
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    throw new NoSuchMethodError();
  }
  
  public final Uri zzbz()
  {
    throw new NoSuchMethodError();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\player\StockProfileImageRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */