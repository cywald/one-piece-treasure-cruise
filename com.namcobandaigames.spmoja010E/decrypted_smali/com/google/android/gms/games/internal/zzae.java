package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.IBinder;

public final class zzae
{
  public int bottom = 0;
  public int gravity;
  public int left = 0;
  public int right = 0;
  public int top = 0;
  public IBinder zzjb;
  public int zzje = -1;
  
  private zzae(int paramInt, IBinder paramIBinder)
  {
    this.gravity = paramInt;
    this.zzjb = paramIBinder;
  }
  
  public final Bundle zzbk()
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("popupLocationInfo.gravity", this.gravity);
    localBundle.putInt("popupLocationInfo.displayId", this.zzje);
    localBundle.putInt("popupLocationInfo.left", this.left);
    localBundle.putInt("popupLocationInfo.top", this.top);
    localBundle.putInt("popupLocationInfo.right", this.right);
    localBundle.putInt("popupLocationInfo.bottom", this.bottom);
    return localBundle;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */