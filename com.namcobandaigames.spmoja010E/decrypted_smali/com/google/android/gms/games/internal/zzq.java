package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;

public abstract interface zzq<R>
{
  public abstract void release(@NonNull R paramR);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */