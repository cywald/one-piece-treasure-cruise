package com.google.android.gms.games.internal;

import com.google.android.gms.common.internal.DowngradeableSafeParcel;
import com.google.android.gms.common.util.GmsVersion;

public abstract class GamesDowngradeableSafeParcel
  extends DowngradeableSafeParcel
{
  protected static boolean zzb(Integer paramInteger)
  {
    if (paramInteger == null) {
      return false;
    }
    return GmsVersion.isAtLeastFenacho(paramInteger.intValue());
  }
  
  public boolean prepareForClientVersion(int paramInt)
  {
    if (!zzb(Integer.valueOf(paramInt))) {}
    for (boolean bool = true;; bool = false)
    {
      setShouldDowngrade(bool);
      return true;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\GamesDowngradeableSafeParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */