package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="StockProfileImageEntityCreator")
@SafeParcelable.Reserved({1000})
public final class StockProfileImageEntity
  extends zzd
  implements StockProfileImage
{
  public static final Parcelable.Creator<StockProfileImageEntity> CREATOR = new zzf();
  @SafeParcelable.Field(getter="getImageUrl", id=1)
  private final String zzmr;
  @SafeParcelable.Field(getter="getImageUri", id=2)
  private final Uri zzms;
  
  @SafeParcelable.Constructor
  public StockProfileImageEntity(@SafeParcelable.Param(id=1) String paramString, @SafeParcelable.Param(id=2) Uri paramUri)
  {
    this.zzmr = paramString;
    this.zzms = paramUri;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof StockProfileImage)) {}
    do
    {
      return false;
      if (paramObject == this) {
        return true;
      }
      paramObject = (StockProfileImage)paramObject;
    } while ((!Objects.equal(this.zzmr, ((StockProfileImage)paramObject).getImageUrl())) || (!Objects.equal(this.zzms, ((StockProfileImage)paramObject).zzbz())));
    return true;
  }
  
  public final String getImageUrl()
  {
    return this.zzmr;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzmr, this.zzms });
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return Objects.toStringHelper(this).add("ImageId", this.zzmr).add("ImageUri", this.zzms).toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, getImageUrl(), false);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzms, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final Uri zzbz()
  {
    return this.zzms;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\player\StockProfileImageEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */