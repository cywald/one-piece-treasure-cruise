package com.google.android.gms.games.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.video.VideoCapabilities;

public class zza
  extends zzv
{
  public void onCaptureOverlayStateChanged(int paramInt) {}
  
  public void onInvitationRemoved(String paramString) {}
  
  public void onLeftRoom(int paramInt, String paramString) {}
  
  public void onP2PConnected(String paramString) {}
  
  public void onP2PDisconnected(String paramString) {}
  
  public void onRealTimeMessageReceived(RealTimeMessage paramRealTimeMessage) {}
  
  public void onRequestRemoved(String paramString) {}
  
  public void onSignOutComplete() {}
  
  public void onTurnBasedMatchRemoved(String paramString) {}
  
  public void zza(int paramInt1, int paramInt2, String paramString) {}
  
  public void zza(int paramInt, Bundle paramBundle) {}
  
  public void zza(int paramInt, VideoCapabilities paramVideoCapabilities) {}
  
  public void zza(int paramInt, String paramString) {}
  
  public final void zza(int paramInt, String paramString, boolean paramBoolean) {}
  
  public void zza(int paramInt, boolean paramBoolean) {}
  
  public void zza(DataHolder paramDataHolder) {}
  
  public void zza(DataHolder paramDataHolder1, DataHolder paramDataHolder2) {}
  
  public void zza(DataHolder paramDataHolder, Contents paramContents) {}
  
  public void zza(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3) {}
  
  public void zza(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public final void zza(DataHolder[] paramArrayOfDataHolder) {}
  
  public final void zzaa(DataHolder paramDataHolder) {}
  
  public final void zzab(DataHolder paramDataHolder) {}
  
  public void zzac(DataHolder paramDataHolder) {}
  
  public void zzad(DataHolder paramDataHolder) {}
  
  public final void zzae(DataHolder paramDataHolder) {}
  
  public final void zzaf(DataHolder paramDataHolder) {}
  
  public void zzag(DataHolder paramDataHolder) {}
  
  public void zzah(DataHolder paramDataHolder) {}
  
  public void zzai(DataHolder paramDataHolder) {}
  
  public void zzaj(DataHolder paramDataHolder) {}
  
  public void zzak(DataHolder paramDataHolder) {}
  
  public final void zzal(DataHolder paramDataHolder) {}
  
  public void zzam(DataHolder paramDataHolder) {}
  
  public final void zzan(DataHolder paramDataHolder) {}
  
  public final void zzao(DataHolder paramDataHolder) {}
  
  public void zzap(DataHolder paramDataHolder) {}
  
  public final void zzaq(DataHolder paramDataHolder) {}
  
  public final void zzar(DataHolder paramDataHolder) {}
  
  public final void zzas(DataHolder paramDataHolder) {}
  
  public final void zzat(DataHolder paramDataHolder) {}
  
  public final void zzau(DataHolder paramDataHolder) {}
  
  public final void zzav(DataHolder paramDataHolder) {}
  
  public final void zzaw(DataHolder paramDataHolder) {}
  
  public final void zzax(DataHolder paramDataHolder) {}
  
  public final void zzay(DataHolder paramDataHolder) {}
  
  public final void zzaz(DataHolder paramDataHolder) {}
  
  public void zzb(int paramInt, Bundle paramBundle) {}
  
  public void zzb(int paramInt, String paramString) {}
  
  public final void zzb(Status paramStatus) {}
  
  public void zzb(DataHolder paramDataHolder) {}
  
  public void zzb(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public final void zzc(int paramInt) {}
  
  public final void zzc(int paramInt, Bundle paramBundle) {}
  
  public void zzc(int paramInt, String paramString) {}
  
  public void zzc(DataHolder paramDataHolder) {}
  
  public void zzc(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public final void zzd(int paramInt) {}
  
  public final void zzd(int paramInt, Bundle paramBundle) {}
  
  public void zzd(int paramInt, String paramString) {}
  
  public void zzd(DataHolder paramDataHolder) {}
  
  public void zzd(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public final void zze(int paramInt) {}
  
  public final void zze(int paramInt, Bundle paramBundle) {}
  
  public void zze(DataHolder paramDataHolder) {}
  
  public void zze(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public final void zzf(int paramInt) {}
  
  public final void zzf(int paramInt, Bundle paramBundle) {}
  
  public void zzf(DataHolder paramDataHolder) {}
  
  public void zzf(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public final void zzg(int paramInt) {}
  
  public void zzg(int paramInt, Bundle paramBundle) {}
  
  public void zzg(DataHolder paramDataHolder) {}
  
  public final void zzh(int paramInt) {}
  
  public final void zzh(DataHolder paramDataHolder) {}
  
  public final void zzi(int paramInt) {}
  
  public final void zzi(DataHolder paramDataHolder) {}
  
  public final void zzj(DataHolder paramDataHolder) {}
  
  public void zzk(DataHolder paramDataHolder) {}
  
  public void zzl(DataHolder paramDataHolder) {}
  
  public void zzm(DataHolder paramDataHolder) {}
  
  public void zzn(DataHolder paramDataHolder) {}
  
  public void zzo(DataHolder paramDataHolder) {}
  
  public void zzp(DataHolder paramDataHolder) {}
  
  public void zzq(DataHolder paramDataHolder) {}
  
  public void zzr(DataHolder paramDataHolder) {}
  
  public void zzs(DataHolder paramDataHolder) {}
  
  public void zzt(DataHolder paramDataHolder) {}
  
  public void zzu(DataHolder paramDataHolder) {}
  
  public void zzv(DataHolder paramDataHolder) {}
  
  public void zzw(DataHolder paramDataHolder) {}
  
  public void zzx(DataHolder paramDataHolder) {}
  
  public void zzy(DataHolder paramDataHolder) {}
  
  public final void zzz(DataHolder paramDataHolder) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */