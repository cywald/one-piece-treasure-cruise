package com.google.android.gms.games.internal;

import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zzw
  extends IInterface
{
  public abstract zzaa zzn()
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */