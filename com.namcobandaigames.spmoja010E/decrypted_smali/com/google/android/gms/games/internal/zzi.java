package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.GamesClientStatusCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzi
{
  private static final zzr zzim = zzo.zziz;
  
  public static <R, PendingR extends Result> Task<R> toTask(@NonNull PendingResult<PendingR> paramPendingResult, @NonNull PendingResultUtil.ResultConverter<PendingR, R> paramResultConverter)
  {
    TaskCompletionSource localTaskCompletionSource = new TaskCompletionSource();
    paramPendingResult.addStatusListener(new zzk(paramPendingResult, localTaskCompletionSource, paramResultConverter));
    return localTaskCompletionSource.getTask();
  }
  
  public static <R, PendingR extends Result> Task<AnnotatedData<R>> zza(@NonNull PendingResult<PendingR> paramPendingResult, @NonNull PendingResultUtil.ResultConverter<PendingR, R> paramResultConverter)
  {
    return zza(paramPendingResult, paramResultConverter, null);
  }
  
  public static <R, PendingR extends Result> Task<AnnotatedData<R>> zza(@NonNull PendingResult<PendingR> paramPendingResult, @NonNull PendingResultUtil.ResultConverter<PendingR, R> paramResultConverter, @Nullable zzq<PendingR> paramzzq)
  {
    TaskCompletionSource localTaskCompletionSource = new TaskCompletionSource();
    paramPendingResult.addStatusListener(new zzl(paramPendingResult, localTaskCompletionSource, paramResultConverter, paramzzq));
    return localTaskCompletionSource.getTask();
  }
  
  public static <R, PendingR extends Result> Task<R> zza(@NonNull PendingResult<PendingR> paramPendingResult, @NonNull zzr paramzzr, @NonNull PendingResultUtil.ResultConverter<PendingR, R> paramResultConverter)
  {
    TaskCompletionSource localTaskCompletionSource = new TaskCompletionSource();
    paramPendingResult.addStatusListener(new zzn(paramzzr, paramPendingResult, localTaskCompletionSource, paramResultConverter));
    return localTaskCompletionSource.getTask();
  }
  
  public static <R, PendingR extends Result, ExceptionData> Task<R> zza(@NonNull PendingResult<PendingR> paramPendingResult, @NonNull zzr paramzzr, @NonNull PendingResultUtil.ResultConverter<PendingR, R> paramResultConverter, @NonNull PendingResultUtil.ResultConverter<PendingR, ExceptionData> paramResultConverter1, @NonNull zzp<ExceptionData> paramzzp)
  {
    TaskCompletionSource localTaskCompletionSource = new TaskCompletionSource();
    paramPendingResult.addStatusListener(new zzj(paramPendingResult, paramzzr, localTaskCompletionSource, paramResultConverter, paramResultConverter1, paramzzp));
    return localTaskCompletionSource.getTask();
  }
  
  public static <R extends Releasable, PendingR extends Result> Task<AnnotatedData<R>> zzb(@NonNull PendingResult<PendingR> paramPendingResult, @NonNull PendingResultUtil.ResultConverter<PendingR, R> paramResultConverter)
  {
    TaskCompletionSource localTaskCompletionSource = new TaskCompletionSource();
    paramPendingResult.addStatusListener(new zzm(paramResultConverter, paramPendingResult, localTaskCompletionSource));
    return localTaskCompletionSource.getTask();
  }
  
  private static Status zzc(@NonNull Status paramStatus)
  {
    int i = GamesClientStatusCodes.zzb(paramStatus.getStatusCode());
    Status localStatus = paramStatus;
    if (i != paramStatus.getStatusCode())
    {
      if (GamesStatusCodes.getStatusString(paramStatus.getStatusCode()).equals(paramStatus.getStatusMessage())) {
        localStatus = GamesClientStatusCodes.zza(i);
      }
    }
    else {
      return localStatus;
    }
    return new Status(i, paramStatus.getStatusMessage());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */