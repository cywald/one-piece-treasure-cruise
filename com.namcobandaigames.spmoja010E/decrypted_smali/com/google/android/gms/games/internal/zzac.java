package com.google.android.gms.games.internal;

import android.os.Binder;
import android.view.View;
import com.google.android.gms.common.util.PlatformVersion;

public class zzac
{
  protected zze zzjc;
  protected zzae zzjd;
  
  private zzac(zze paramzze, int paramInt)
  {
    this.zzjc = paramzze;
    zzm(paramInt);
  }
  
  public static zzac zza(zze paramzze, int paramInt)
  {
    if (PlatformVersion.isAtLeastHoneycombMR1()) {
      return new zzaf(paramzze, paramInt);
    }
    return new zzac(paramzze, paramInt);
  }
  
  public void zzb(View paramView) {}
  
  public void zzbj()
  {
    this.zzjc.zza(this.zzjd.zzjb, this.zzjd.zzbk());
  }
  
  protected void zzm(int paramInt)
  {
    this.zzjd = new zzae(paramInt, new Binder(), null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */