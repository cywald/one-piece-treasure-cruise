package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.games.Game;

public final class ExperienceEventRef
  extends DataBufferRef
  implements ExperienceEvent
{
  public final int describeContents()
  {
    throw new NoSuchMethodError();
  }
  
  public final boolean equals(Object paramObject)
  {
    throw new NoSuchMethodError();
  }
  
  public final Game getGame()
  {
    throw new NoSuchMethodError();
  }
  
  public final Uri getIconImageUri()
  {
    throw new NoSuchMethodError();
  }
  
  public final String getIconImageUrl()
  {
    return getString("icon_url");
  }
  
  public final int getType()
  {
    throw new NoSuchMethodError();
  }
  
  public final int hashCode()
  {
    throw new NoSuchMethodError();
  }
  
  public final String toString()
  {
    throw new NoSuchMethodError();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    throw new NoSuchMethodError();
  }
  
  public final String zzbm()
  {
    throw new NoSuchMethodError();
  }
  
  public final String zzbn()
  {
    throw new NoSuchMethodError();
  }
  
  public final String zzbo()
  {
    throw new NoSuchMethodError();
  }
  
  public final long zzbp()
  {
    throw new NoSuchMethodError();
  }
  
  public final long zzbq()
  {
    throw new NoSuchMethodError();
  }
  
  public final long zzbr()
  {
    throw new NoSuchMethodError();
  }
  
  public final int zzbs()
  {
    throw new NoSuchMethodError();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\experience\ExperienceEventRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */