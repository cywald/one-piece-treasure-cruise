package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="MostRecentGameInfoEntityCreator")
@SafeParcelable.Reserved({1000})
public final class zzb
  extends zzd
  implements zza
{
  public static final Parcelable.Creator<zzb> CREATOR = new zzc();
  @SafeParcelable.Field(getter="getGameId", id=1)
  private final String zzlf;
  @SafeParcelable.Field(getter="getGameName", id=2)
  private final String zzlg;
  @SafeParcelable.Field(getter="getActivityTimestampMillis", id=3)
  private final long zzlh;
  @SafeParcelable.Field(getter="getGameIconImageUri", id=4)
  private final Uri zzli;
  @SafeParcelable.Field(getter="getGameHiResImageUri", id=5)
  private final Uri zzlj;
  @SafeParcelable.Field(getter="getGameFeaturedImageUri", id=6)
  private final Uri zzlk;
  
  public zzb(zza paramzza)
  {
    this.zzlf = paramzza.zzbt();
    this.zzlg = paramzza.zzbu();
    this.zzlh = paramzza.zzbv();
    this.zzli = paramzza.zzbw();
    this.zzlj = paramzza.zzbx();
    this.zzlk = paramzza.zzby();
  }
  
  @SafeParcelable.Constructor
  zzb(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) String paramString2, @SafeParcelable.Param(id=3) long paramLong, @SafeParcelable.Param(id=4) Uri paramUri1, @SafeParcelable.Param(id=5) Uri paramUri2, @SafeParcelable.Param(id=6) Uri paramUri3)
  {
    this.zzlf = paramString1;
    this.zzlg = paramString2;
    this.zzlh = paramLong;
    this.zzli = paramUri1;
    this.zzlj = paramUri2;
    this.zzlk = paramUri3;
  }
  
  static int zza(zza paramzza)
  {
    return Objects.hashCode(new Object[] { paramzza.zzbt(), paramzza.zzbu(), Long.valueOf(paramzza.zzbv()), paramzza.zzbw(), paramzza.zzbx(), paramzza.zzby() });
  }
  
  static boolean zza(zza paramzza, Object paramObject)
  {
    if (!(paramObject instanceof zza)) {}
    do
    {
      return false;
      if (paramzza == paramObject) {
        return true;
      }
      paramObject = (zza)paramObject;
    } while ((!Objects.equal(((zza)paramObject).zzbt(), paramzza.zzbt())) || (!Objects.equal(((zza)paramObject).zzbu(), paramzza.zzbu())) || (!Objects.equal(Long.valueOf(((zza)paramObject).zzbv()), Long.valueOf(paramzza.zzbv()))) || (!Objects.equal(((zza)paramObject).zzbw(), paramzza.zzbw())) || (!Objects.equal(((zza)paramObject).zzbx(), paramzza.zzbx())) || (!Objects.equal(((zza)paramObject).zzby(), paramzza.zzby())));
    return true;
  }
  
  static String zzb(zza paramzza)
  {
    return Objects.toStringHelper(paramzza).add("GameId", paramzza.zzbt()).add("GameName", paramzza.zzbu()).add("ActivityTimestampMillis", Long.valueOf(paramzza.zzbv())).add("GameIconUri", paramzza.zzbw()).add("GameHiResUri", paramzza.zzbx()).add("GameFeaturedUri", paramzza.zzby()).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, this.zzlf, false);
    SafeParcelWriter.writeString(paramParcel, 2, this.zzlg, false);
    SafeParcelWriter.writeLong(paramParcel, 3, this.zzlh);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzli, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzlj, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 6, this.zzlk, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final String zzbt()
  {
    return this.zzlf;
  }
  
  public final String zzbu()
  {
    return this.zzlg;
  }
  
  public final long zzbv()
  {
    return this.zzlh;
  }
  
  public final Uri zzbw()
  {
    return this.zzli;
  }
  
  public final Uri zzbx()
  {
    return this.zzlj;
  }
  
  public final Uri zzby()
  {
    return this.zzlk;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\player\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */