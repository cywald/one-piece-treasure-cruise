package com.google.android.gms.games.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzs<L>
  extends RegisterListenerMethod<zze, L>
{
  protected zzs(ListenerHolder<L> paramListenerHolder)
  {
    super(paramListenerHolder);
  }
  
  protected abstract void zzb(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */