package com.google.android.gms.games.internal;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;
import com.google.android.gms.games.snapshot.zze;

public abstract interface zzy
  extends IInterface
{
  public abstract Bundle getConnectionHint()
    throws RemoteException;
  
  public abstract int zza(zzu paramzzu, byte[] paramArrayOfByte, String paramString1, String paramString2)
    throws RemoteException;
  
  public abstract Intent zza(int paramInt1, int paramInt2, boolean paramBoolean)
    throws RemoteException;
  
  public abstract Intent zza(int paramInt1, byte[] paramArrayOfByte, int paramInt2, String paramString)
    throws RemoteException;
  
  public abstract Intent zza(PlayerEntity paramPlayerEntity)
    throws RemoteException;
  
  public abstract Intent zza(RoomEntity paramRoomEntity, int paramInt)
    throws RemoteException;
  
  public abstract Intent zza(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
    throws RemoteException;
  
  public abstract Intent zza(int[] paramArrayOfInt)
    throws RemoteException;
  
  public abstract void zza(long paramLong)
    throws RemoteException;
  
  public abstract void zza(IBinder paramIBinder, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zza(Contents paramContents)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, int paramInt)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, int paramInt1, int paramInt2, int paramInt3)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, int paramInt1, int paramInt2, String[] paramArrayOfString, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, int paramInt, int[] paramArrayOfInt)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, long paramLong)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, Bundle paramBundle, int paramInt1, int paramInt2)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, IBinder paramIBinder, int paramInt, String[] paramArrayOfString, Bundle paramBundle, boolean paramBoolean, long paramLong)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, IBinder paramIBinder, String paramString, boolean paramBoolean, long paramLong)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString, int paramInt, IBinder paramIBinder, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString1, long paramLong, String paramString2)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString, IBinder paramIBinder, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString, zze paramzze, Contents paramContents)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString1, String paramString2)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString1, String paramString2, int paramInt1, int paramInt2)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString1, String paramString2, zze paramzze, Contents paramContents)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString, boolean paramBoolean, int paramInt)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString1, byte[] paramArrayOfByte, String paramString2, ParticipantResult[] paramArrayOfParticipantResult)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String paramString, byte[] paramArrayOfByte, ParticipantResult[] paramArrayOfParticipantResult)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, boolean paramBoolean, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zza(zzu paramzzu, String[] paramArrayOfString, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zza(zzw paramzzw, long paramLong)
    throws RemoteException;
  
  public abstract void zza(String paramString, int paramInt)
    throws RemoteException;
  
  public abstract void zza(String paramString, IBinder paramIBinder, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zza(String paramString, zzu paramzzu)
    throws RemoteException;
  
  public abstract Intent zzag()
    throws RemoteException;
  
  public abstract Intent zzai()
    throws RemoteException;
  
  public abstract int zzak()
    throws RemoteException;
  
  public abstract String zzam()
    throws RemoteException;
  
  public abstract int zzao()
    throws RemoteException;
  
  public abstract Intent zzaq()
    throws RemoteException;
  
  public abstract int zzar()
    throws RemoteException;
  
  public abstract int zzas()
    throws RemoteException;
  
  public abstract int zzat()
    throws RemoteException;
  
  public abstract int zzav()
    throws RemoteException;
  
  public abstract boolean zzaz()
    throws RemoteException;
  
  public abstract int zzb(byte[] paramArrayOfByte, String paramString, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract Intent zzb(String paramString, int paramInt1, int paramInt2)
    throws RemoteException;
  
  public abstract void zzb(long paramLong)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, int paramInt)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, long paramLong)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, String paramString)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, String paramString, int paramInt, IBinder paramIBinder, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, String paramString, IBinder paramIBinder, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, String paramString1, String paramString2)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, String paramString, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zzb(zzu paramzzu, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zzb(String paramString, int paramInt)
    throws RemoteException;
  
  public abstract void zzbd()
    throws RemoteException;
  
  public abstract String zzbf()
    throws RemoteException;
  
  public abstract DataHolder zzbg()
    throws RemoteException;
  
  public abstract DataHolder zzbh()
    throws RemoteException;
  
  public abstract Intent zzbi()
    throws RemoteException;
  
  public abstract Intent zzc(int paramInt1, int paramInt2, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zzc(long paramLong)
    throws RemoteException;
  
  public abstract void zzc(zzu paramzzu)
    throws RemoteException;
  
  public abstract void zzc(zzu paramzzu, long paramLong)
    throws RemoteException;
  
  public abstract void zzc(zzu paramzzu, String paramString)
    throws RemoteException;
  
  public abstract void zzc(zzu paramzzu, boolean paramBoolean)
    throws RemoteException;
  
  public abstract Intent zzd(String paramString)
    throws RemoteException;
  
  public abstract void zzd(long paramLong)
    throws RemoteException;
  
  public abstract void zzd(zzu paramzzu)
    throws RemoteException;
  
  public abstract void zzd(zzu paramzzu, long paramLong)
    throws RemoteException;
  
  public abstract void zzd(zzu paramzzu, String paramString)
    throws RemoteException;
  
  public abstract void zzd(zzu paramzzu, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zzd(String paramString, int paramInt)
    throws RemoteException;
  
  public abstract void zze(long paramLong)
    throws RemoteException;
  
  public abstract void zze(zzu paramzzu, long paramLong)
    throws RemoteException;
  
  public abstract void zze(zzu paramzzu, String paramString)
    throws RemoteException;
  
  public abstract void zze(zzu paramzzu, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zzf(long paramLong)
    throws RemoteException;
  
  public abstract void zzf(zzu paramzzu, String paramString)
    throws RemoteException;
  
  public abstract void zzf(zzu paramzzu, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zzf(String paramString)
    throws RemoteException;
  
  public abstract void zzg(zzu paramzzu, String paramString)
    throws RemoteException;
  
  public abstract void zzh(zzu paramzzu, String paramString)
    throws RemoteException;
  
  public abstract void zzk(int paramInt)
    throws RemoteException;
  
  public abstract String zzp()
    throws RemoteException;
  
  public abstract Intent zzv()
    throws RemoteException;
  
  public abstract Intent zzx()
    throws RemoteException;
  
  public abstract Intent zzy()
    throws RemoteException;
  
  public abstract Intent zzz()
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */