package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.video.VideoCapabilities;

public abstract interface zzu
  extends IInterface
{
  public abstract void onCaptureOverlayStateChanged(int paramInt)
    throws RemoteException;
  
  public abstract void onInvitationRemoved(String paramString)
    throws RemoteException;
  
  public abstract void onLeftRoom(int paramInt, String paramString)
    throws RemoteException;
  
  public abstract void onP2PConnected(String paramString)
    throws RemoteException;
  
  public abstract void onP2PDisconnected(String paramString)
    throws RemoteException;
  
  public abstract void onRealTimeMessageReceived(RealTimeMessage paramRealTimeMessage)
    throws RemoteException;
  
  public abstract void onRequestRemoved(String paramString)
    throws RemoteException;
  
  public abstract void onSignOutComplete()
    throws RemoteException;
  
  public abstract void onTurnBasedMatchRemoved(String paramString)
    throws RemoteException;
  
  public abstract void zza(int paramInt1, int paramInt2, String paramString)
    throws RemoteException;
  
  public abstract void zza(int paramInt, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zza(int paramInt, VideoCapabilities paramVideoCapabilities)
    throws RemoteException;
  
  public abstract void zza(int paramInt, String paramString)
    throws RemoteException;
  
  public abstract void zza(int paramInt, String paramString, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zza(int paramInt, boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zza(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zza(DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    throws RemoteException;
  
  public abstract void zza(DataHolder paramDataHolder, Contents paramContents)
    throws RemoteException;
  
  public abstract void zza(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3)
    throws RemoteException;
  
  public abstract void zza(DataHolder paramDataHolder, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zza(DataHolder[] paramArrayOfDataHolder)
    throws RemoteException;
  
  public abstract void zzaa(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzab(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzac(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzad(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzae(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzaf(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzag(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzah(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzai(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzaj(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzak(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzal(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzam(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzan(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzao(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzap(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzaq(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzar(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzas(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzat(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzau(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzav(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzaw(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzax(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzay(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzaz(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzb(int paramInt, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zzb(int paramInt, String paramString)
    throws RemoteException;
  
  public abstract void zzb(Status paramStatus)
    throws RemoteException;
  
  public abstract void zzb(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzb(DataHolder paramDataHolder, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zzc(int paramInt)
    throws RemoteException;
  
  public abstract void zzc(int paramInt, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zzc(int paramInt, String paramString)
    throws RemoteException;
  
  public abstract void zzc(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzc(DataHolder paramDataHolder, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zzd(int paramInt)
    throws RemoteException;
  
  public abstract void zzd(int paramInt, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zzd(int paramInt, String paramString)
    throws RemoteException;
  
  public abstract void zzd(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzd(DataHolder paramDataHolder, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zze(int paramInt)
    throws RemoteException;
  
  public abstract void zze(int paramInt, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zze(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zze(DataHolder paramDataHolder, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zzf(int paramInt)
    throws RemoteException;
  
  public abstract void zzf(int paramInt, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zzf(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzf(DataHolder paramDataHolder, String[] paramArrayOfString)
    throws RemoteException;
  
  public abstract void zzg(int paramInt)
    throws RemoteException;
  
  public abstract void zzg(int paramInt, Bundle paramBundle)
    throws RemoteException;
  
  public abstract void zzg(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzh(int paramInt)
    throws RemoteException;
  
  public abstract void zzh(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzi(int paramInt)
    throws RemoteException;
  
  public abstract void zzi(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzj(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzk(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzl(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzm(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzn(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzo(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzp(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzq(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzr(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzs(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzt(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzu(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzv(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzw(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzx(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzy(DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zzz(DataHolder paramDataHolder)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */