package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="ExperienceEventEntityCreator")
@SafeParcelable.Reserved({1000})
public final class ExperienceEventEntity
  extends zzd
  implements ExperienceEvent
{
  public static final Parcelable.Creator<ExperienceEventEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getType", id=10)
  private final int type;
  @SafeParcelable.Field(getter="getIconImageUrl", id=5)
  private final String zzac;
  @SafeParcelable.Field(getter="getExperienceId", id=1)
  private final String zzkx;
  @SafeParcelable.Field(getter="getGame", id=2)
  private final GameEntity zzky;
  @SafeParcelable.Field(getter="getDisplayTitle", id=3)
  private final String zzkz;
  @SafeParcelable.Field(getter="getDisplayDescription", id=4)
  private final String zzla;
  @SafeParcelable.Field(getter="getCreatedTimestamp", id=7)
  private final long zzlb;
  @SafeParcelable.Field(getter="getXpEarned", id=8)
  private final long zzlc;
  @SafeParcelable.Field(getter="getCurrentXp", id=9)
  private final long zzld;
  @SafeParcelable.Field(getter="getNewLevel", id=11)
  private final int zzle;
  @SafeParcelable.Field(getter="getIconImageUri", id=6)
  private final Uri zzr;
  
  @SafeParcelable.Constructor
  ExperienceEventEntity(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) GameEntity paramGameEntity, @SafeParcelable.Param(id=3) String paramString2, @SafeParcelable.Param(id=4) String paramString3, @SafeParcelable.Param(id=5) String paramString4, @SafeParcelable.Param(id=6) Uri paramUri, @SafeParcelable.Param(id=7) long paramLong1, @SafeParcelable.Param(id=8) long paramLong2, @SafeParcelable.Param(id=9) long paramLong3, @SafeParcelable.Param(id=10) int paramInt1, @SafeParcelable.Param(id=11) int paramInt2)
  {
    this.zzkx = paramString1;
    this.zzky = paramGameEntity;
    this.zzkz = paramString2;
    this.zzla = paramString3;
    this.zzac = paramString4;
    this.zzr = paramUri;
    this.zzlb = paramLong1;
    this.zzlc = paramLong2;
    this.zzld = paramLong3;
    this.type = paramInt1;
    this.zzle = paramInt2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof ExperienceEvent))
    {
      if (this == paramObject) {}
      do
      {
        return true;
        paramObject = (ExperienceEvent)paramObject;
      } while ((Objects.equal(((ExperienceEvent)paramObject).zzbm(), zzbm())) && (Objects.equal(((ExperienceEvent)paramObject).getGame(), getGame())) && (Objects.equal(((ExperienceEvent)paramObject).zzbn(), zzbn())) && (Objects.equal(((ExperienceEvent)paramObject).zzbo(), zzbo())) && (Objects.equal(((ExperienceEvent)paramObject).getIconImageUrl(), getIconImageUrl())) && (Objects.equal(((ExperienceEvent)paramObject).getIconImageUri(), getIconImageUri())) && (Objects.equal(Long.valueOf(((ExperienceEvent)paramObject).zzbp()), Long.valueOf(zzbp()))) && (Objects.equal(Long.valueOf(((ExperienceEvent)paramObject).zzbq()), Long.valueOf(zzbq()))) && (Objects.equal(Long.valueOf(((ExperienceEvent)paramObject).zzbr()), Long.valueOf(zzbr()))) && (Objects.equal(Integer.valueOf(((ExperienceEvent)paramObject).getType()), Integer.valueOf(getType()))) && (Objects.equal(Integer.valueOf(((ExperienceEvent)paramObject).zzbs()), Integer.valueOf(zzbs()))));
    }
    return false;
  }
  
  public final Game getGame()
  {
    return this.zzky;
  }
  
  public final Uri getIconImageUri()
  {
    return this.zzr;
  }
  
  public final String getIconImageUrl()
  {
    return this.zzac;
  }
  
  public final int getType()
  {
    return this.type;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { zzbm(), getGame(), zzbn(), zzbo(), getIconImageUrl(), getIconImageUri(), Long.valueOf(zzbp()), Long.valueOf(zzbq()), Long.valueOf(zzbr()), Integer.valueOf(getType()), Integer.valueOf(zzbs()) });
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return Objects.toStringHelper(this).add("ExperienceId", zzbm()).add("Game", getGame()).add("DisplayTitle", zzbn()).add("DisplayDescription", zzbo()).add("IconImageUrl", getIconImageUrl()).add("IconImageUri", getIconImageUri()).add("CreatedTimestamp", Long.valueOf(zzbp())).add("XpEarned", Long.valueOf(zzbq())).add("CurrentXp", Long.valueOf(zzbr())).add("Type", Integer.valueOf(getType())).add("NewLevel", Integer.valueOf(zzbs())).toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, this.zzkx, false);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzky, paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 3, this.zzkz, false);
    SafeParcelWriter.writeString(paramParcel, 4, this.zzla, false);
    SafeParcelWriter.writeString(paramParcel, 5, getIconImageUrl(), false);
    SafeParcelWriter.writeParcelable(paramParcel, 6, this.zzr, paramInt, false);
    SafeParcelWriter.writeLong(paramParcel, 7, this.zzlb);
    SafeParcelWriter.writeLong(paramParcel, 8, this.zzlc);
    SafeParcelWriter.writeLong(paramParcel, 9, this.zzld);
    SafeParcelWriter.writeInt(paramParcel, 10, this.type);
    SafeParcelWriter.writeInt(paramParcel, 11, this.zzle);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final String zzbm()
  {
    return this.zzkx;
  }
  
  public final String zzbn()
  {
    return this.zzkz;
  }
  
  public final String zzbo()
  {
    return this.zzla;
  }
  
  public final long zzbp()
  {
    return this.zzlb;
  }
  
  public final long zzbq()
  {
    return this.zzlc;
  }
  
  public final long zzbr()
  {
    return this.zzld;
  }
  
  public final int zzbs()
  {
    return this.zzle;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\experience\ExperienceEventEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */