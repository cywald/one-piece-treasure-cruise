package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface zza
  extends Parcelable, Freezable<zza>
{
  public abstract String zzbt();
  
  public abstract String zzbu();
  
  public abstract long zzbv();
  
  public abstract Uri zzbw();
  
  public abstract Uri zzbx();
  
  public abstract Uri zzby();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\player\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */