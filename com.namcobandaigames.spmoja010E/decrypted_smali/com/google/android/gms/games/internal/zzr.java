package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Status;

public abstract interface zzr
{
  public abstract boolean zza(@NonNull Status paramStatus);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\internal\zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */