package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.player.zza;
import com.google.android.gms.games.internal.player.zzd;
import com.google.android.gms.games.internal.player.zze;

public final class PlayerRef
  extends DataBufferRef
  implements Player
{
  private final PlayerLevelInfo zzce;
  private final zze zzcw;
  private final zzd zzcx;
  
  public PlayerRef(DataHolder paramDataHolder, int paramInt)
  {
    this(paramDataHolder, paramInt, null);
  }
  
  public PlayerRef(DataHolder paramDataHolder, int paramInt, String paramString)
  {
    super(paramDataHolder, paramInt);
    this.zzcw = new zze(paramString);
    this.zzcx = new zzd(paramDataHolder, paramInt, this.zzcw);
    int i;
    if ((!hasNull(this.zzcw.zzlu)) && (getLong(this.zzcw.zzlu) != -1L))
    {
      paramInt = 1;
      if (paramInt == 0) {
        break label208;
      }
      paramInt = getInteger(this.zzcw.zzlv);
      i = getInteger(this.zzcw.zzly);
      paramString = new PlayerLevel(paramInt, getLong(this.zzcw.zzlw), getLong(this.zzcw.zzlx));
      if (paramInt == i) {
        break label214;
      }
    }
    label208:
    label214:
    for (paramDataHolder = new PlayerLevel(i, getLong(this.zzcw.zzlx), getLong(this.zzcw.zzlz));; paramDataHolder = paramString)
    {
      this.zzce = new PlayerLevelInfo(getLong(this.zzcw.zzlu), getLong(this.zzcw.zzma), paramString, paramDataHolder);
      return;
      paramInt = 0;
      break;
      this.zzce = null;
      return;
    }
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    return PlayerEntity.zza(this, paramObject);
  }
  
  public final Uri getBannerImageLandscapeUri()
  {
    return parseUri(this.zzcw.zzmk);
  }
  
  public final String getBannerImageLandscapeUrl()
  {
    return getString(this.zzcw.zzml);
  }
  
  public final Uri getBannerImagePortraitUri()
  {
    return parseUri(this.zzcw.zzmm);
  }
  
  public final String getBannerImagePortraitUrl()
  {
    return getString(this.zzcw.zzmn);
  }
  
  public final String getDisplayName()
  {
    return getString(this.zzcw.zzlm);
  }
  
  public final void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    copyToBuffer(this.zzcw.zzlm, paramCharArrayBuffer);
  }
  
  public final Uri getHiResImageUri()
  {
    return parseUri(this.zzcw.zzlp);
  }
  
  public final String getHiResImageUrl()
  {
    return getString(this.zzcw.zzlq);
  }
  
  public final Uri getIconImageUri()
  {
    return parseUri(this.zzcw.zzln);
  }
  
  public final String getIconImageUrl()
  {
    return getString(this.zzcw.zzlo);
  }
  
  public final long getLastPlayedWithTimestamp()
  {
    if ((!hasColumn(this.zzcw.zzlt)) || (hasNull(this.zzcw.zzlt))) {
      return -1L;
    }
    return getLong(this.zzcw.zzlt);
  }
  
  public final PlayerLevelInfo getLevelInfo()
  {
    return this.zzce;
  }
  
  public final String getName()
  {
    return getString(this.zzcw.name);
  }
  
  public final String getPlayerId()
  {
    return getString(this.zzcw.zzll);
  }
  
  public final long getRetrievedTimestamp()
  {
    return getLong(this.zzcw.zzlr);
  }
  
  public final String getTitle()
  {
    return getString(this.zzcw.zzcc);
  }
  
  public final void getTitle(CharArrayBuffer paramCharArrayBuffer)
  {
    copyToBuffer(this.zzcw.zzcc, paramCharArrayBuffer);
  }
  
  public final boolean hasHiResImage()
  {
    return getHiResImageUri() != null;
  }
  
  public final boolean hasIconImage()
  {
    return getIconImageUri() != null;
  }
  
  public final int hashCode()
  {
    return PlayerEntity.zza(this);
  }
  
  public final boolean isMuted()
  {
    return getBoolean(this.zzcw.zzmq);
  }
  
  public final String toString()
  {
    return PlayerEntity.zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((PlayerEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public final String zzg()
  {
    return getString(this.zzcw.zzch);
  }
  
  public final boolean zzh()
  {
    return getBoolean(this.zzcw.zzmj);
  }
  
  public final int zzi()
  {
    return getInteger(this.zzcw.zzls);
  }
  
  public final boolean zzj()
  {
    return getBoolean(this.zzcw.zzmc);
  }
  
  public final zza zzk()
  {
    if (hasNull(this.zzcw.zzmd)) {
      return null;
    }
    return this.zzcx;
  }
  
  public final int zzl()
  {
    return getInteger(this.zzcw.zzmo);
  }
  
  public final long zzm()
  {
    return getLong(this.zzcw.zzmp);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\PlayerRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */