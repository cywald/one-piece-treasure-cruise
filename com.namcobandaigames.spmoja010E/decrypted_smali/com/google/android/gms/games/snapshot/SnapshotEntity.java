package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="SnapshotEntityCreator")
@SafeParcelable.Reserved({1000})
public final class SnapshotEntity
  extends zzd
  implements Snapshot
{
  public static final Parcelable.Creator<SnapshotEntity> CREATOR = new zzc();
  @SafeParcelable.Field(getter="getMetadata", id=1)
  private final SnapshotMetadataEntity zzqn;
  @SafeParcelable.Field(getter="getSnapshotContents", id=3)
  private final zza zzqo;
  
  @SafeParcelable.Constructor
  public SnapshotEntity(@SafeParcelable.Param(id=1) SnapshotMetadata paramSnapshotMetadata, @SafeParcelable.Param(id=3) zza paramzza)
  {
    this.zzqn = new SnapshotMetadataEntity(paramSnapshotMetadata);
    this.zzqo = paramzza;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof Snapshot))
    {
      if (this == paramObject) {}
      do
      {
        return true;
        paramObject = (Snapshot)paramObject;
      } while ((Objects.equal(((Snapshot)paramObject).getMetadata(), getMetadata())) && (Objects.equal(((Snapshot)paramObject).getSnapshotContents(), getSnapshotContents())));
    }
    return false;
  }
  
  public final Snapshot freeze()
  {
    return this;
  }
  
  public final SnapshotMetadata getMetadata()
  {
    return this.zzqn;
  }
  
  public final SnapshotContents getSnapshotContents()
  {
    if (this.zzqo.isClosed()) {
      return null;
    }
    return this.zzqo;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { getMetadata(), getSnapshotContents() });
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    Objects.ToStringHelper localToStringHelper = Objects.toStringHelper(this).add("Metadata", getMetadata());
    if (getSnapshotContents() != null) {}
    for (boolean bool = true;; bool = false) {
      return localToStringHelper.add("HasContents", Boolean.valueOf(bool)).toString();
    }
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, getMetadata(), paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 3, getSnapshotContents(), paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\snapshot\SnapshotEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */