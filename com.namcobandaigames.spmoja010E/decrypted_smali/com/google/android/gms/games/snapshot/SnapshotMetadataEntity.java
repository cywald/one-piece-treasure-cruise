package com.google.android.gms.games.snapshot;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="SnapshotMetadataEntityCreator")
@SafeParcelable.Reserved({1000})
public final class SnapshotMetadataEntity
  extends zzd
  implements SnapshotMetadata
{
  public static final Parcelable.Creator<SnapshotMetadataEntity> CREATOR = new zzf();
  @SafeParcelable.Field(getter="getDescription", id=8)
  private final String description;
  @SafeParcelable.Field(getter="getDeviceName", id=15)
  private final String deviceName;
  @SafeParcelable.Field(getter="getTitle", id=7)
  private final String zzcc;
  @SafeParcelable.Field(getter="getSnapshotId", id=3)
  private final String zzgo;
  @SafeParcelable.Field(getter="getGame", id=1)
  private final GameEntity zzky;
  @SafeParcelable.Field(getter="getCoverImageUri", id=5)
  private final Uri zzqs;
  @SafeParcelable.Field(getter="getOwner", id=2)
  private final PlayerEntity zzqv;
  @SafeParcelable.Field(getter="getCoverImageUrl", id=6)
  private final String zzqw;
  @SafeParcelable.Field(getter="getLastModifiedTimestamp", id=9)
  private final long zzqx;
  @SafeParcelable.Field(getter="getPlayedTime", id=10)
  private final long zzqy;
  @SafeParcelable.Field(getter="getCoverImageAspectRatio", id=11)
  private final float zzqz;
  @SafeParcelable.Field(getter="getUniqueName", id=12)
  private final String zzra;
  @SafeParcelable.Field(getter="hasChangePending", id=13)
  private final boolean zzrb;
  @SafeParcelable.Field(getter="getProgressValue", id=14)
  private final long zzrc;
  
  @SafeParcelable.Constructor
  SnapshotMetadataEntity(@SafeParcelable.Param(id=1) GameEntity paramGameEntity, @SafeParcelable.Param(id=2) PlayerEntity paramPlayerEntity, @SafeParcelable.Param(id=3) String paramString1, @SafeParcelable.Param(id=5) Uri paramUri, @SafeParcelable.Param(id=6) String paramString2, @SafeParcelable.Param(id=7) String paramString3, @SafeParcelable.Param(id=8) String paramString4, @SafeParcelable.Param(id=9) long paramLong1, @SafeParcelable.Param(id=10) long paramLong2, @SafeParcelable.Param(id=11) float paramFloat, @SafeParcelable.Param(id=12) String paramString5, @SafeParcelable.Param(id=13) boolean paramBoolean, @SafeParcelable.Param(id=14) long paramLong3, @SafeParcelable.Param(id=15) String paramString6)
  {
    this.zzky = paramGameEntity;
    this.zzqv = paramPlayerEntity;
    this.zzgo = paramString1;
    this.zzqs = paramUri;
    this.zzqw = paramString2;
    this.zzqz = paramFloat;
    this.zzcc = paramString3;
    this.description = paramString4;
    this.zzqx = paramLong1;
    this.zzqy = paramLong2;
    this.zzra = paramString5;
    this.zzrb = paramBoolean;
    this.zzrc = paramLong3;
    this.deviceName = paramString6;
  }
  
  public SnapshotMetadataEntity(SnapshotMetadata paramSnapshotMetadata)
  {
    this.zzky = new GameEntity(paramSnapshotMetadata.getGame());
    this.zzqv = new PlayerEntity(paramSnapshotMetadata.getOwner());
    this.zzgo = paramSnapshotMetadata.getSnapshotId();
    this.zzqs = paramSnapshotMetadata.getCoverImageUri();
    this.zzqw = paramSnapshotMetadata.getCoverImageUrl();
    this.zzqz = paramSnapshotMetadata.getCoverImageAspectRatio();
    this.zzcc = paramSnapshotMetadata.getTitle();
    this.description = paramSnapshotMetadata.getDescription();
    this.zzqx = paramSnapshotMetadata.getLastModifiedTimestamp();
    this.zzqy = paramSnapshotMetadata.getPlayedTime();
    this.zzra = paramSnapshotMetadata.getUniqueName();
    this.zzrb = paramSnapshotMetadata.hasChangePending();
    this.zzrc = paramSnapshotMetadata.getProgressValue();
    this.deviceName = paramSnapshotMetadata.getDeviceName();
  }
  
  static int zza(SnapshotMetadata paramSnapshotMetadata)
  {
    return Objects.hashCode(new Object[] { paramSnapshotMetadata.getGame(), paramSnapshotMetadata.getOwner(), paramSnapshotMetadata.getSnapshotId(), paramSnapshotMetadata.getCoverImageUri(), Float.valueOf(paramSnapshotMetadata.getCoverImageAspectRatio()), paramSnapshotMetadata.getTitle(), paramSnapshotMetadata.getDescription(), Long.valueOf(paramSnapshotMetadata.getLastModifiedTimestamp()), Long.valueOf(paramSnapshotMetadata.getPlayedTime()), paramSnapshotMetadata.getUniqueName(), Boolean.valueOf(paramSnapshotMetadata.hasChangePending()), Long.valueOf(paramSnapshotMetadata.getProgressValue()), paramSnapshotMetadata.getDeviceName() });
  }
  
  static boolean zza(SnapshotMetadata paramSnapshotMetadata, Object paramObject)
  {
    if (!(paramObject instanceof SnapshotMetadata)) {}
    do
    {
      return false;
      if (paramSnapshotMetadata == paramObject) {
        return true;
      }
      paramObject = (SnapshotMetadata)paramObject;
    } while ((!Objects.equal(((SnapshotMetadata)paramObject).getGame(), paramSnapshotMetadata.getGame())) || (!Objects.equal(((SnapshotMetadata)paramObject).getOwner(), paramSnapshotMetadata.getOwner())) || (!Objects.equal(((SnapshotMetadata)paramObject).getSnapshotId(), paramSnapshotMetadata.getSnapshotId())) || (!Objects.equal(((SnapshotMetadata)paramObject).getCoverImageUri(), paramSnapshotMetadata.getCoverImageUri())) || (!Objects.equal(Float.valueOf(((SnapshotMetadata)paramObject).getCoverImageAspectRatio()), Float.valueOf(paramSnapshotMetadata.getCoverImageAspectRatio()))) || (!Objects.equal(((SnapshotMetadata)paramObject).getTitle(), paramSnapshotMetadata.getTitle())) || (!Objects.equal(((SnapshotMetadata)paramObject).getDescription(), paramSnapshotMetadata.getDescription())) || (!Objects.equal(Long.valueOf(((SnapshotMetadata)paramObject).getLastModifiedTimestamp()), Long.valueOf(paramSnapshotMetadata.getLastModifiedTimestamp()))) || (!Objects.equal(Long.valueOf(((SnapshotMetadata)paramObject).getPlayedTime()), Long.valueOf(paramSnapshotMetadata.getPlayedTime()))) || (!Objects.equal(((SnapshotMetadata)paramObject).getUniqueName(), paramSnapshotMetadata.getUniqueName())) || (!Objects.equal(Boolean.valueOf(((SnapshotMetadata)paramObject).hasChangePending()), Boolean.valueOf(paramSnapshotMetadata.hasChangePending()))) || (!Objects.equal(Long.valueOf(((SnapshotMetadata)paramObject).getProgressValue()), Long.valueOf(paramSnapshotMetadata.getProgressValue()))) || (!Objects.equal(((SnapshotMetadata)paramObject).getDeviceName(), paramSnapshotMetadata.getDeviceName())));
    return true;
  }
  
  static String zzb(SnapshotMetadata paramSnapshotMetadata)
  {
    return Objects.toStringHelper(paramSnapshotMetadata).add("Game", paramSnapshotMetadata.getGame()).add("Owner", paramSnapshotMetadata.getOwner()).add("SnapshotId", paramSnapshotMetadata.getSnapshotId()).add("CoverImageUri", paramSnapshotMetadata.getCoverImageUri()).add("CoverImageUrl", paramSnapshotMetadata.getCoverImageUrl()).add("CoverImageAspectRatio", Float.valueOf(paramSnapshotMetadata.getCoverImageAspectRatio())).add("Description", paramSnapshotMetadata.getDescription()).add("LastModifiedTimestamp", Long.valueOf(paramSnapshotMetadata.getLastModifiedTimestamp())).add("PlayedTime", Long.valueOf(paramSnapshotMetadata.getPlayedTime())).add("UniqueName", paramSnapshotMetadata.getUniqueName()).add("ChangePending", Boolean.valueOf(paramSnapshotMetadata.hasChangePending())).add("ProgressValue", Long.valueOf(paramSnapshotMetadata.getProgressValue())).add("DeviceName", paramSnapshotMetadata.getDeviceName()).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final SnapshotMetadata freeze()
  {
    return this;
  }
  
  public final float getCoverImageAspectRatio()
  {
    return this.zzqz;
  }
  
  public final Uri getCoverImageUri()
  {
    return this.zzqs;
  }
  
  public final String getCoverImageUrl()
  {
    return this.zzqw;
  }
  
  public final String getDescription()
  {
    return this.description;
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.description, paramCharArrayBuffer);
  }
  
  public final String getDeviceName()
  {
    return this.deviceName;
  }
  
  public final Game getGame()
  {
    return this.zzky;
  }
  
  public final long getLastModifiedTimestamp()
  {
    return this.zzqx;
  }
  
  public final Player getOwner()
  {
    return this.zzqv;
  }
  
  public final long getPlayedTime()
  {
    return this.zzqy;
  }
  
  public final long getProgressValue()
  {
    return this.zzrc;
  }
  
  public final String getSnapshotId()
  {
    return this.zzgo;
  }
  
  public final String getTitle()
  {
    return this.zzcc;
  }
  
  public final String getUniqueName()
  {
    return this.zzra;
  }
  
  public final boolean hasChangePending()
  {
    return this.zzrb;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, getGame(), paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 2, getOwner(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 3, getSnapshotId(), false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, getCoverImageUri(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 6, getCoverImageUrl(), false);
    SafeParcelWriter.writeString(paramParcel, 7, this.zzcc, false);
    SafeParcelWriter.writeString(paramParcel, 8, getDescription(), false);
    SafeParcelWriter.writeLong(paramParcel, 9, getLastModifiedTimestamp());
    SafeParcelWriter.writeLong(paramParcel, 10, getPlayedTime());
    SafeParcelWriter.writeFloat(paramParcel, 11, getCoverImageAspectRatio());
    SafeParcelWriter.writeString(paramParcel, 12, getUniqueName(), false);
    SafeParcelWriter.writeBoolean(paramParcel, 13, hasChangePending());
    SafeParcelWriter.writeLong(paramParcel, 14, getProgressValue());
    SafeParcelWriter.writeString(paramParcel, 15, getDeviceName(), false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\snapshot\SnapshotMetadataEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */