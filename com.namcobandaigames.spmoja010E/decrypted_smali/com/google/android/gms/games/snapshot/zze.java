package com.google.android.gms.games.snapshot;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="SnapshotMetadataChangeCreator")
@SafeParcelable.Reserved({1000})
public final class zze
  extends com.google.android.gms.games.internal.zzd
  implements SnapshotMetadataChange
{
  public static final Parcelable.Creator<zze> CREATOR = new zzd();
  @SafeParcelable.Field(getter="getDescription", id=1)
  private final String description;
  @SafeParcelable.Field(getter="getProgressValue", id=6)
  private final Long zzqq;
  @SafeParcelable.Field(getter="getCoverImageUri", id=4)
  private final Uri zzqs;
  @SafeParcelable.Field(getter="getPlayedTimeMillis", id=2)
  private final Long zzqt;
  @SafeParcelable.Field(getter="getCoverImageTeleporter", id=5)
  private BitmapTeleporter zzqu;
  
  zze()
  {
    this(null, null, null, null, null);
  }
  
  @SafeParcelable.Constructor
  zze(@SafeParcelable.Param(id=1) String paramString, @SafeParcelable.Param(id=2) Long paramLong1, @SafeParcelable.Param(id=5) BitmapTeleporter paramBitmapTeleporter, @SafeParcelable.Param(id=4) Uri paramUri, @SafeParcelable.Param(id=6) Long paramLong2)
  {
    this.description = paramString;
    this.zzqt = paramLong1;
    this.zzqu = paramBitmapTeleporter;
    this.zzqs = paramUri;
    this.zzqq = paramLong2;
    if (this.zzqu != null) {
      if (this.zzqs == null) {
        Preconditions.checkState(bool1, "Cannot set both a URI and an image");
      }
    }
    while (this.zzqs == null) {
      for (;;)
      {
        return;
        bool1 = false;
      }
    }
    if (this.zzqu == null) {}
    for (bool1 = bool2;; bool1 = false)
    {
      Preconditions.checkState(bool1, "Cannot set both a URI and an image");
      return;
    }
  }
  
  public final Bitmap getCoverImage()
  {
    if (this.zzqu == null) {
      return null;
    }
    return this.zzqu.get();
  }
  
  public final String getDescription()
  {
    return this.description;
  }
  
  public final Long getPlayedTimeMillis()
  {
    return this.zzqt;
  }
  
  public final Long getProgressValue()
  {
    return this.zzqq;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, getDescription(), false);
    SafeParcelWriter.writeLongObject(paramParcel, 2, getPlayedTimeMillis(), false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzqs, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzqu, paramInt, false);
    SafeParcelWriter.writeLongObject(paramParcel, 6, getProgressValue(), false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final BitmapTeleporter zzcm()
  {
    return this.zzqu;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\snapshot\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */