package com.google.android.gms.games.snapshot;

import android.graphics.Bitmap;
import android.net.Uri;
import com.google.android.gms.common.data.BitmapTeleporter;

public abstract interface SnapshotMetadataChange
{
  public static final SnapshotMetadataChange EMPTY_CHANGE = new zze();
  
  public abstract Bitmap getCoverImage();
  
  public abstract String getDescription();
  
  public abstract Long getPlayedTimeMillis();
  
  public abstract Long getProgressValue();
  
  public abstract BitmapTeleporter zzcm();
  
  public static final class Builder
  {
    private String description;
    private Long zzqp;
    private Long zzqq;
    private BitmapTeleporter zzqr;
    private Uri zzqs;
    
    public final SnapshotMetadataChange build()
    {
      return new zze(this.description, this.zzqp, this.zzqr, this.zzqs, this.zzqq);
    }
    
    public final Builder fromMetadata(SnapshotMetadata paramSnapshotMetadata)
    {
      this.description = paramSnapshotMetadata.getDescription();
      this.zzqp = Long.valueOf(paramSnapshotMetadata.getPlayedTime());
      this.zzqq = Long.valueOf(paramSnapshotMetadata.getProgressValue());
      if (this.zzqp.longValue() == -1L) {
        this.zzqp = null;
      }
      this.zzqs = paramSnapshotMetadata.getCoverImageUri();
      if (this.zzqs != null) {
        this.zzqr = null;
      }
      return this;
    }
    
    public final Builder setCoverImage(Bitmap paramBitmap)
    {
      this.zzqr = new BitmapTeleporter(paramBitmap);
      this.zzqs = null;
      return this;
    }
    
    public final Builder setDescription(String paramString)
    {
      this.description = paramString;
      return this;
    }
    
    public final Builder setPlayedTimeMillis(long paramLong)
    {
      this.zzqp = Long.valueOf(paramLong);
      return this;
    }
    
    public final Builder setProgressValue(long paramLong)
    {
      this.zzqq = Long.valueOf(paramLong);
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\snapshot\SnapshotMetadataChange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */