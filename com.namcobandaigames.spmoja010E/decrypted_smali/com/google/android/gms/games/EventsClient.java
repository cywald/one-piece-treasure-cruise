package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events;
import com.google.android.gms.games.event.Events.LoadEventsResult;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class EventsClient
  extends zzu
{
  private static final PendingResultUtil.ResultConverter<Events.LoadEventsResult, EventBuffer> zzj = new zzg();
  
  EventsClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  EventsClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public void increment(@NonNull String paramString, @IntRange(from=0L) int paramInt)
  {
    doWrite(new zzf(this, paramString, paramInt));
  }
  
  public Task<AnnotatedData<EventBuffer>> load(boolean paramBoolean)
  {
    return zzi.zzb(Games.Events.load(asGoogleApiClient(), paramBoolean), zzj);
  }
  
  public Task<AnnotatedData<EventBuffer>> loadByIds(boolean paramBoolean, @NonNull String... paramVarArgs)
  {
    return zzi.zzb(Games.Events.loadByIds(asGoogleApiClient(), paramBoolean, paramVarArgs), zzj);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\EventsClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */