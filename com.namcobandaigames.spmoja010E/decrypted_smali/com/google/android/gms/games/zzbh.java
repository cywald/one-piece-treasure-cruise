package com.google.android.gms.games;

import com.google.android.gms.common.api.internal.TaskUtil;
import com.google.android.gms.games.internal.zza;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbh
  extends zza
{
  zzbh(zzbg paramzzbg, TaskCompletionSource paramTaskCompletionSource) {}
  
  public final void onLeftRoom(int paramInt, String paramString)
  {
    TaskUtil.setResultOrApiException(GamesClientStatusCodes.zza(GamesClientStatusCodes.zzb(paramInt)), paramString, this.zzdk);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */