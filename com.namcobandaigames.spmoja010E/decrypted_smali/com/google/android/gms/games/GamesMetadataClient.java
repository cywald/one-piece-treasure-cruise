package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.internal.zzq;
import com.google.android.gms.tasks.Task;

public class GamesMetadataClient
  extends com.google.android.gms.internal.games.zzu
{
  private static final PendingResultUtil.ResultConverter<GamesMetadata.LoadGamesResult, Game> zzbf = new zzv();
  private static final zzq<GamesMetadata.LoadGamesResult> zzbg = new zzw();
  
  GamesMetadataClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  GamesMetadataClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<Game> getCurrentGame()
  {
    return doRead(new zzu(this));
  }
  
  public Task<AnnotatedData<Game>> loadGame()
  {
    return zzi.zza(Games.GamesMetadata.loadGame(asGoogleApiClient()), zzbf, zzbg);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\GamesMetadataClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */