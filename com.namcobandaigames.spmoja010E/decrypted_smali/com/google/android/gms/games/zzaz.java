package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;

final class zzaz
  implements PendingResultUtil.ResultConverter<Players.LoadPlayersResult, Player>
{
  private static Player zza(@Nullable Players.LoadPlayersResult paramLoadPlayersResult)
  {
    Player localPlayer = null;
    if (paramLoadPlayersResult == null) {
      paramLoadPlayersResult = localPlayer;
    }
    for (;;)
    {
      return paramLoadPlayersResult;
      PlayerBuffer localPlayerBuffer = paramLoadPlayersResult.getPlayers();
      if (localPlayerBuffer != null) {}
      try
      {
        if (localPlayerBuffer.getCount() > 0)
        {
          localPlayer = (Player)((Player)localPlayerBuffer.get(0)).freeze();
          paramLoadPlayersResult = localPlayer;
          return localPlayer;
        }
        paramLoadPlayersResult = localPlayer;
        return null;
      }
      finally
      {
        if (localPlayerBuffer != null) {
          localPlayerBuffer.release();
        }
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzaz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */