package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolders;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.internal.zzp;
import com.google.android.gms.games.internal.zzq;
import com.google.android.gms.games.internal.zzr;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchUpdateCallback;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.CancelMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.InitiateMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LeaveMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchesResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.UpdateMatchResult;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;
import java.util.List;

public class TurnBasedMultiplayerClient
  extends zzu
{
  private static final zzp<TurnBasedMatch> zzel = new zzcv();
  private static final PendingResultUtil.ResultConverter<TurnBasedMultiplayer.LoadMatchesResult, LoadMatchesResponse> zzem = new zzce();
  private static final zzq<TurnBasedMultiplayer.LoadMatchesResult> zzen = new zzcf();
  private static final PendingResultUtil.ResultConverter<TurnBasedMultiplayer.LoadMatchResult, TurnBasedMatch> zzeo = new zzcg();
  private static final PendingResultUtil.ResultConverter<TurnBasedMultiplayer.CancelMatchResult, String> zzep = new zzch();
  private static final zzr zzeq = new zzci();
  private static final PendingResultUtil.ResultConverter<TurnBasedMultiplayer.LeaveMatchResult, Void> zzer = new zzcj();
  private static final PendingResultUtil.ResultConverter<TurnBasedMultiplayer.LeaveMatchResult, TurnBasedMatch> zzes = new zzck();
  private static final zzr zzet = new zzcl();
  private static final PendingResultUtil.ResultConverter<TurnBasedMultiplayer.UpdateMatchResult, TurnBasedMatch> zzeu = new zzcm();
  private static final PendingResultUtil.ResultConverter<TurnBasedMultiplayer.InitiateMatchResult, TurnBasedMatch> zzev = new zzcn();
  
  TurnBasedMultiplayerClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  TurnBasedMultiplayerClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  private static Task<Void> zzd(@NonNull PendingResult<TurnBasedMultiplayer.LeaveMatchResult> paramPendingResult)
  {
    return zzi.zza(paramPendingResult, zzeq, zzer, zzes, zzel);
  }
  
  private static Task<TurnBasedMatch> zze(@NonNull PendingResult<TurnBasedMultiplayer.UpdateMatchResult> paramPendingResult)
  {
    return zzi.zza(paramPendingResult, zzet, zzeu, zzeu, zzel);
  }
  
  public Task<TurnBasedMatch> acceptInvitation(@NonNull String paramString)
  {
    return zzi.toTask(Games.TurnBasedMultiplayer.acceptInvitation(asGoogleApiClient(), paramString), zzev);
  }
  
  public Task<String> cancelMatch(@NonNull String paramString)
  {
    return zzi.toTask(Games.TurnBasedMultiplayer.cancelMatch(asGoogleApiClient(), paramString), zzep);
  }
  
  public Task<TurnBasedMatch> createMatch(@NonNull TurnBasedMatchConfig paramTurnBasedMatchConfig)
  {
    return zzi.toTask(Games.TurnBasedMultiplayer.createMatch(asGoogleApiClient(), paramTurnBasedMatchConfig), zzev);
  }
  
  public Task<Void> declineInvitation(@NonNull String paramString)
  {
    return doWrite(new zzcr(this, paramString));
  }
  
  public Task<Void> dismissInvitation(@NonNull String paramString)
  {
    return doWrite(new zzcs(this, paramString));
  }
  
  public Task<Void> dismissMatch(@NonNull String paramString)
  {
    return doWrite(new zzcu(this, paramString));
  }
  
  public Task<TurnBasedMatch> finishMatch(@NonNull String paramString)
  {
    return zze(Games.TurnBasedMultiplayer.finishMatch(asGoogleApiClient(), paramString));
  }
  
  public Task<TurnBasedMatch> finishMatch(@NonNull String paramString, @Nullable byte[] paramArrayOfByte, @Nullable List<ParticipantResult> paramList)
  {
    return zze(Games.TurnBasedMultiplayer.finishMatch(asGoogleApiClient(), paramString, paramArrayOfByte, paramList));
  }
  
  public Task<TurnBasedMatch> finishMatch(@NonNull String paramString, @Nullable byte[] paramArrayOfByte, @Nullable ParticipantResult... paramVarArgs)
  {
    return zze(Games.TurnBasedMultiplayer.finishMatch(asGoogleApiClient(), paramString, paramArrayOfByte, paramVarArgs));
  }
  
  public Task<Intent> getInboxIntent()
  {
    return doRead(new zzcd(this));
  }
  
  public Task<Integer> getMaxMatchDataSize()
  {
    return doRead(new zzct(this));
  }
  
  public Task<Intent> getSelectOpponentsIntent(@IntRange(from=1L) int paramInt1, @IntRange(from=1L) int paramInt2)
  {
    return getSelectOpponentsIntent(paramInt1, paramInt2, true);
  }
  
  public Task<Intent> getSelectOpponentsIntent(@IntRange(from=1L) int paramInt1, @IntRange(from=1L) int paramInt2, boolean paramBoolean)
  {
    return doRead(new zzcq(this, paramInt1, paramInt2, paramBoolean));
  }
  
  public Task<Void> leaveMatch(@NonNull String paramString)
  {
    return zzd(Games.TurnBasedMultiplayer.leaveMatch(asGoogleApiClient(), paramString));
  }
  
  public Task<Void> leaveMatchDuringTurn(@NonNull String paramString1, @Nullable String paramString2)
  {
    return zzd(Games.TurnBasedMultiplayer.leaveMatchDuringTurn(asGoogleApiClient(), paramString1, paramString2));
  }
  
  public Task<AnnotatedData<TurnBasedMatch>> loadMatch(@NonNull String paramString)
  {
    return zzi.zza(Games.TurnBasedMultiplayer.loadMatch(asGoogleApiClient(), paramString), zzeo);
  }
  
  public Task<AnnotatedData<LoadMatchesResponse>> loadMatchesByStatus(int paramInt, @NonNull int[] paramArrayOfInt)
  {
    return zzi.zza(Games.TurnBasedMultiplayer.loadMatchesByStatus(asGoogleApiClient(), paramInt, paramArrayOfInt), zzem, zzen);
  }
  
  public Task<AnnotatedData<LoadMatchesResponse>> loadMatchesByStatus(@NonNull int[] paramArrayOfInt)
  {
    return zzi.zza(Games.TurnBasedMultiplayer.loadMatchesByStatus(asGoogleApiClient(), paramArrayOfInt), zzem, zzen);
  }
  
  public Task<Void> registerTurnBasedMatchUpdateCallback(@NonNull TurnBasedMatchUpdateCallback paramTurnBasedMatchUpdateCallback)
  {
    paramTurnBasedMatchUpdateCallback = registerListener(paramTurnBasedMatchUpdateCallback, TurnBasedMatchUpdateCallback.class.getSimpleName());
    return doRegisterEventListener(new zzco(this, paramTurnBasedMatchUpdateCallback, paramTurnBasedMatchUpdateCallback), new zzcp(this, paramTurnBasedMatchUpdateCallback.getListenerKey()));
  }
  
  public Task<TurnBasedMatch> rematch(@NonNull String paramString)
  {
    return zzi.toTask(Games.TurnBasedMultiplayer.rematch(asGoogleApiClient(), paramString), zzev);
  }
  
  public Task<TurnBasedMatch> takeTurn(@NonNull String paramString1, @Nullable byte[] paramArrayOfByte, @Nullable String paramString2)
  {
    return zze(Games.TurnBasedMultiplayer.takeTurn(asGoogleApiClient(), paramString1, paramArrayOfByte, paramString2));
  }
  
  public Task<TurnBasedMatch> takeTurn(@NonNull String paramString1, @Nullable byte[] paramArrayOfByte, @Nullable String paramString2, @Nullable List<ParticipantResult> paramList)
  {
    return zze(Games.TurnBasedMultiplayer.takeTurn(asGoogleApiClient(), paramString1, paramArrayOfByte, paramString2, paramList));
  }
  
  public Task<TurnBasedMatch> takeTurn(@NonNull String paramString1, @Nullable byte[] paramArrayOfByte, @Nullable String paramString2, @Nullable ParticipantResult... paramVarArgs)
  {
    return zze(Games.TurnBasedMultiplayer.takeTurn(asGoogleApiClient(), paramString1, paramArrayOfByte, paramString2, paramVarArgs));
  }
  
  public Task<Boolean> unregisterTurnBasedMatchUpdateCallback(@NonNull TurnBasedMatchUpdateCallback paramTurnBasedMatchUpdateCallback)
  {
    return doUnregisterEventListener(ListenerHolders.createListenerKey(paramTurnBasedMatchUpdateCallback, TurnBasedMatchUpdateCallback.class.getSimpleName()));
  }
  
  public static class MatchOutOfDateApiException
    extends ApiException
  {
    protected final TurnBasedMatch match;
    
    MatchOutOfDateApiException(@NonNull Status paramStatus, @NonNull TurnBasedMatch paramTurnBasedMatch)
    {
      super();
      this.match = paramTurnBasedMatch;
    }
    
    public TurnBasedMatch getMatch()
    {
      return this.match;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\TurnBasedMultiplayerClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */