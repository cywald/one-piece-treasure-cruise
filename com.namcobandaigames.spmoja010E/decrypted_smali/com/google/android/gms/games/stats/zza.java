package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator="PlayerStatsEntityCreator")
@SafeParcelable.Reserved({1000})
public final class zza
  extends zzd
  implements PlayerStats
{
  public static final Parcelable.Creator<zza> CREATOR = new zzb();
  @SafeParcelable.Field(getter="getAverageSessionLength", id=1)
  private final float zzre;
  @SafeParcelable.Field(getter="getChurnProbability", id=2)
  private final float zzrf;
  @SafeParcelable.Field(getter="getDaysSinceLastPlayed", id=3)
  private final int zzrg;
  @SafeParcelable.Field(getter="getNumberOfPurchases", id=4)
  private final int zzrh;
  @SafeParcelable.Field(getter="getNumberOfSessions", id=5)
  private final int zzri;
  @SafeParcelable.Field(getter="getSessionPercentile", id=6)
  private final float zzrj;
  @SafeParcelable.Field(getter="getSpendPercentile", id=7)
  private final float zzrk;
  @SafeParcelable.Field(getter="getRawValues", id=8)
  private final Bundle zzrl;
  @SafeParcelable.Field(getter="getSpendProbability", id=9)
  private final float zzrm;
  @SafeParcelable.Field(getter="getHighSpenderProbability", id=10)
  private final float zzrn;
  @SafeParcelable.Field(getter="getTotalSpendNext28Days", id=11)
  private final float zzro;
  
  @SafeParcelable.Constructor
  zza(@SafeParcelable.Param(id=1) float paramFloat1, @SafeParcelable.Param(id=2) float paramFloat2, @SafeParcelable.Param(id=3) int paramInt1, @SafeParcelable.Param(id=4) int paramInt2, @SafeParcelable.Param(id=5) int paramInt3, @SafeParcelable.Param(id=6) float paramFloat3, @SafeParcelable.Param(id=7) float paramFloat4, @SafeParcelable.Param(id=8) Bundle paramBundle, @SafeParcelable.Param(id=9) float paramFloat5, @SafeParcelable.Param(id=10) float paramFloat6, @SafeParcelable.Param(id=11) float paramFloat7)
  {
    this.zzre = paramFloat1;
    this.zzrf = paramFloat2;
    this.zzrg = paramInt1;
    this.zzrh = paramInt2;
    this.zzri = paramInt3;
    this.zzrj = paramFloat3;
    this.zzrk = paramFloat4;
    this.zzrl = paramBundle;
    this.zzrm = paramFloat5;
    this.zzrn = paramFloat6;
    this.zzro = paramFloat7;
  }
  
  public zza(PlayerStats paramPlayerStats)
  {
    this.zzre = paramPlayerStats.getAverageSessionLength();
    this.zzrf = paramPlayerStats.getChurnProbability();
    this.zzrg = paramPlayerStats.getDaysSinceLastPlayed();
    this.zzrh = paramPlayerStats.getNumberOfPurchases();
    this.zzri = paramPlayerStats.getNumberOfSessions();
    this.zzrj = paramPlayerStats.getSessionPercentile();
    this.zzrk = paramPlayerStats.getSpendPercentile();
    this.zzrm = paramPlayerStats.getSpendProbability();
    this.zzrn = paramPlayerStats.getHighSpenderProbability();
    this.zzro = paramPlayerStats.getTotalSpendNext28Days();
    this.zzrl = paramPlayerStats.zzcn();
  }
  
  static int zza(PlayerStats paramPlayerStats)
  {
    return Objects.hashCode(new Object[] { Float.valueOf(paramPlayerStats.getAverageSessionLength()), Float.valueOf(paramPlayerStats.getChurnProbability()), Integer.valueOf(paramPlayerStats.getDaysSinceLastPlayed()), Integer.valueOf(paramPlayerStats.getNumberOfPurchases()), Integer.valueOf(paramPlayerStats.getNumberOfSessions()), Float.valueOf(paramPlayerStats.getSessionPercentile()), Float.valueOf(paramPlayerStats.getSpendPercentile()), Float.valueOf(paramPlayerStats.getSpendProbability()), Float.valueOf(paramPlayerStats.getHighSpenderProbability()), Float.valueOf(paramPlayerStats.getTotalSpendNext28Days()) });
  }
  
  static boolean zza(PlayerStats paramPlayerStats, Object paramObject)
  {
    if (!(paramObject instanceof PlayerStats)) {}
    do
    {
      return false;
      if (paramPlayerStats == paramObject) {
        return true;
      }
      paramObject = (PlayerStats)paramObject;
    } while ((!Objects.equal(Float.valueOf(((PlayerStats)paramObject).getAverageSessionLength()), Float.valueOf(paramPlayerStats.getAverageSessionLength()))) || (!Objects.equal(Float.valueOf(((PlayerStats)paramObject).getChurnProbability()), Float.valueOf(paramPlayerStats.getChurnProbability()))) || (!Objects.equal(Integer.valueOf(((PlayerStats)paramObject).getDaysSinceLastPlayed()), Integer.valueOf(paramPlayerStats.getDaysSinceLastPlayed()))) || (!Objects.equal(Integer.valueOf(((PlayerStats)paramObject).getNumberOfPurchases()), Integer.valueOf(paramPlayerStats.getNumberOfPurchases()))) || (!Objects.equal(Integer.valueOf(((PlayerStats)paramObject).getNumberOfSessions()), Integer.valueOf(paramPlayerStats.getNumberOfSessions()))) || (!Objects.equal(Float.valueOf(((PlayerStats)paramObject).getSessionPercentile()), Float.valueOf(paramPlayerStats.getSessionPercentile()))) || (!Objects.equal(Float.valueOf(((PlayerStats)paramObject).getSpendPercentile()), Float.valueOf(paramPlayerStats.getSpendPercentile()))) || (!Objects.equal(Float.valueOf(((PlayerStats)paramObject).getSpendProbability()), Float.valueOf(paramPlayerStats.getSpendProbability()))) || (!Objects.equal(Float.valueOf(((PlayerStats)paramObject).getHighSpenderProbability()), Float.valueOf(paramPlayerStats.getHighSpenderProbability()))) || (!Objects.equal(Float.valueOf(((PlayerStats)paramObject).getTotalSpendNext28Days()), Float.valueOf(paramPlayerStats.getTotalSpendNext28Days()))));
    return true;
  }
  
  static String zzb(PlayerStats paramPlayerStats)
  {
    return Objects.toStringHelper(paramPlayerStats).add("AverageSessionLength", Float.valueOf(paramPlayerStats.getAverageSessionLength())).add("ChurnProbability", Float.valueOf(paramPlayerStats.getChurnProbability())).add("DaysSinceLastPlayed", Integer.valueOf(paramPlayerStats.getDaysSinceLastPlayed())).add("NumberOfPurchases", Integer.valueOf(paramPlayerStats.getNumberOfPurchases())).add("NumberOfSessions", Integer.valueOf(paramPlayerStats.getNumberOfSessions())).add("SessionPercentile", Float.valueOf(paramPlayerStats.getSessionPercentile())).add("SpendPercentile", Float.valueOf(paramPlayerStats.getSpendPercentile())).add("SpendProbability", Float.valueOf(paramPlayerStats.getSpendProbability())).add("HighSpenderProbability", Float.valueOf(paramPlayerStats.getHighSpenderProbability())).add("TotalSpendNext28Days", Float.valueOf(paramPlayerStats.getTotalSpendNext28Days())).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final float getAverageSessionLength()
  {
    return this.zzre;
  }
  
  public final float getChurnProbability()
  {
    return this.zzrf;
  }
  
  public final int getDaysSinceLastPlayed()
  {
    return this.zzrg;
  }
  
  public final float getHighSpenderProbability()
  {
    return this.zzrn;
  }
  
  public final int getNumberOfPurchases()
  {
    return this.zzrh;
  }
  
  public final int getNumberOfSessions()
  {
    return this.zzri;
  }
  
  public final float getSessionPercentile()
  {
    return this.zzrj;
  }
  
  public final float getSpendPercentile()
  {
    return this.zzrk;
  }
  
  public final float getSpendProbability()
  {
    return this.zzrm;
  }
  
  public final float getTotalSpendNext28Days()
  {
    return this.zzro;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeFloat(paramParcel, 1, getAverageSessionLength());
    SafeParcelWriter.writeFloat(paramParcel, 2, getChurnProbability());
    SafeParcelWriter.writeInt(paramParcel, 3, getDaysSinceLastPlayed());
    SafeParcelWriter.writeInt(paramParcel, 4, getNumberOfPurchases());
    SafeParcelWriter.writeInt(paramParcel, 5, getNumberOfSessions());
    SafeParcelWriter.writeFloat(paramParcel, 6, getSessionPercentile());
    SafeParcelWriter.writeFloat(paramParcel, 7, getSpendPercentile());
    SafeParcelWriter.writeBundle(paramParcel, 8, this.zzrl, false);
    SafeParcelWriter.writeFloat(paramParcel, 9, getSpendProbability());
    SafeParcelWriter.writeFloat(paramParcel, 10, getHighSpenderProbability());
    SafeParcelWriter.writeFloat(paramParcel, 11, getTotalSpendNext28Days());
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
  
  public final Bundle zzcn()
  {
    return this.zzrl;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\stats\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */