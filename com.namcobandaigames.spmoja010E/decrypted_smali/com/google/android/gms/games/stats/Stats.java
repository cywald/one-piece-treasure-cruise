package com.google.android.gms.games.stats;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.util.VisibleForTesting;

@Deprecated
@VisibleForTesting
public abstract interface Stats
{
  public abstract PendingResult<LoadPlayerStatsResult> loadPlayerStats(GoogleApiClient paramGoogleApiClient, boolean paramBoolean);
  
  @Deprecated
  public static abstract interface LoadPlayerStatsResult
    extends Releasable, Result
  {
    public abstract PlayerStats getPlayerStats();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\stats\Stats.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */