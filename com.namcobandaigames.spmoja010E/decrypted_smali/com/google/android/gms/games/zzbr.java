package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.internal.ApiExceptionUtil;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbr
  extends zzah<Integer>
{
  zzbr(RealTimeMultiplayerClient paramRealTimeMultiplayerClient, ListenerHolder paramListenerHolder, byte[] paramArrayOfByte, String paramString1, String paramString2) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Integer> paramTaskCompletionSource)
    throws RemoteException
  {
    paramzze = Integer.valueOf(paramzze.zza(this.zzdt, this.zzdf, this.zzdg, this.zzdh));
    if (paramzze.intValue() == -1)
    {
      paramTaskCompletionSource.trySetException(ApiExceptionUtil.fromStatus(GamesClientStatusCodes.zza(26601)));
      return;
    }
    paramTaskCompletionSource.setResult(paramzze);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzbr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */