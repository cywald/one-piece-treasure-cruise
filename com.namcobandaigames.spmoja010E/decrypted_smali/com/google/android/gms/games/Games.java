package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.view.View;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AbstractClientBuilder;
import com.google.android.gms.common.api.Api.ApiOptions.HasGoogleSignInAccountOptions;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.ClientKey;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.event.Events;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.multiplayer.Invitations;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.android.gms.games.quest.Quests;
import com.google.android.gms.games.request.Requests;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.games.stats.Stats;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.internal.games.zzad;
import com.google.android.gms.internal.games.zzai;
import com.google.android.gms.internal.games.zzam;
import com.google.android.gms.internal.games.zzbc;
import com.google.android.gms.internal.games.zzbd;
import com.google.android.gms.internal.games.zzbe;
import com.google.android.gms.internal.games.zzbo;
import com.google.android.gms.internal.games.zzbz;
import com.google.android.gms.internal.games.zzca;
import com.google.android.gms.internal.games.zzci;
import com.google.android.gms.internal.games.zzcw;
import com.google.android.gms.internal.games.zzcx;
import com.google.android.gms.internal.games.zzdb;
import com.google.android.gms.internal.games.zzdy;
import com.google.android.gms.internal.games.zzep;
import com.google.android.gms.internal.games.zzf;
import com.google.android.gms.internal.games.zzt;
import com.google.android.gms.internal.games.zzv;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@KeepForSdk
@VisibleForTesting
public final class Games
{
  @Deprecated
  public static final Api<GamesOptions> API;
  @Deprecated
  public static final Achievements Achievements;
  private static final Api.AbstractClientBuilder<com.google.android.gms.games.internal.zze, GamesOptions> CLIENT_BUILDER;
  static final Api.ClientKey<com.google.android.gms.games.internal.zze> CLIENT_KEY = new Api.ClientKey();
  public static final String EXTRA_PLAYER_IDS = "players";
  public static final String EXTRA_STATUS = "status";
  @Deprecated
  public static final Events Events;
  @Deprecated
  public static final GamesMetadata GamesMetadata;
  @Deprecated
  public static final Invitations Invitations;
  @Deprecated
  public static final Leaderboards Leaderboards;
  @Deprecated
  public static final Notifications Notifications = new zzbd();
  @Deprecated
  public static final Players Players;
  @Deprecated
  public static final Quests Quests = new zzbo();
  @Deprecated
  public static final RealTimeMultiplayer RealTimeMultiplayer;
  @Deprecated
  public static final Requests Requests = new zzca();
  public static final Scope SCOPE_GAMES;
  public static final Scope SCOPE_GAMES_LITE;
  @Deprecated
  public static final Snapshots Snapshots = new zzci();
  @Deprecated
  public static final Stats Stats = new zzcx();
  @Deprecated
  public static final TurnBasedMultiplayer TurnBasedMultiplayer;
  @Deprecated
  public static final Videos Videos = new zzdy();
  private static final Api.AbstractClientBuilder<com.google.android.gms.games.internal.zze, GamesOptions> zzak;
  public static final Scope zzal;
  private static final Api<GamesOptions> zzam;
  private static final com.google.android.gms.internal.games.zze zzan;
  private static final Multiplayer zzao;
  private static final zzep zzap = new zzcw();
  
  static
  {
    CLIENT_BUILDER = new zzi();
    zzak = new zzj();
    SCOPE_GAMES = new Scope("https://www.googleapis.com/auth/games");
    SCOPE_GAMES_LITE = new Scope("https://www.googleapis.com/auth/games_lite");
    API = new Api("Games.API", CLIENT_BUILDER, CLIENT_KEY);
    zzal = new Scope("https://www.googleapis.com/auth/games.firstparty");
    zzam = new Api("Games.API_1P", zzak, CLIENT_KEY);
    GamesMetadata = new zzad();
    Achievements = new zzf();
    zzan = new zzt();
    Events = new zzv();
    Leaderboards = new zzam();
    Invitations = new zzai();
    TurnBasedMultiplayer = new zzdb();
    RealTimeMultiplayer = new zzbz();
    zzao = new zzbc();
    Players = new zzbe();
  }
  
  public static AchievementsClient getAchievementsClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new AchievementsClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static AchievementsClient getAchievementsClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new AchievementsClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  @Deprecated
  public static String getAppId(GoogleApiClient paramGoogleApiClient)
  {
    return zza(paramGoogleApiClient, true).zzan();
  }
  
  @Deprecated
  @RequiresPermission("android.permission.GET_ACCOUNTS")
  public static String getCurrentAccountName(GoogleApiClient paramGoogleApiClient)
  {
    return zza(paramGoogleApiClient, true).zzq();
  }
  
  public static EventsClient getEventsClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new EventsClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static EventsClient getEventsClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new EventsClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static GamesClient getGamesClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new GamesClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static GamesClient getGamesClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new GamesClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static GamesMetadataClient getGamesMetadataClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new GamesMetadataClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static GamesMetadataClient getGamesMetadataClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new GamesMetadataClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  @Deprecated
  @KeepForSdk
  public static PendingResult<GetServerAuthCodeResult> getGamesServerAuthCode(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    Preconditions.checkNotEmpty(paramString, "Please provide a valid serverClientId");
    return paramGoogleApiClient.execute(new zzk(paramGoogleApiClient, paramString));
  }
  
  public static InvitationsClient getInvitationsClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new InvitationsClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static InvitationsClient getInvitationsClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new InvitationsClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static LeaderboardsClient getLeaderboardsClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new LeaderboardsClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static LeaderboardsClient getLeaderboardsClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new LeaderboardsClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static NotificationsClient getNotificationsClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new NotificationsClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static NotificationsClient getNotificationsClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new NotificationsClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static PlayerStatsClient getPlayerStatsClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new PlayerStatsClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static PlayerStatsClient getPlayerStatsClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new PlayerStatsClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static PlayersClient getPlayersClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new PlayersClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static PlayersClient getPlayersClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new PlayersClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static RealTimeMultiplayerClient getRealTimeMultiplayerClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new RealTimeMultiplayerClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static RealTimeMultiplayerClient getRealTimeMultiplayerClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new RealTimeMultiplayerClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  @Deprecated
  public static int getSdkVariant(GoogleApiClient paramGoogleApiClient)
  {
    return zza(paramGoogleApiClient, true).zzal();
  }
  
  @Deprecated
  public static Intent getSettingsIntent(GoogleApiClient paramGoogleApiClient)
  {
    return zza(paramGoogleApiClient, true).zzaj();
  }
  
  public static SnapshotsClient getSnapshotsClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new SnapshotsClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static SnapshotsClient getSnapshotsClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new SnapshotsClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static TurnBasedMultiplayerClient getTurnBasedMultiplayerClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new TurnBasedMultiplayerClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static TurnBasedMultiplayerClient getTurnBasedMultiplayerClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new TurnBasedMultiplayerClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  public static VideosClient getVideosClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new VideosClient(paramActivity, zza(paramGoogleSignInAccount));
  }
  
  public static VideosClient getVideosClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount, "GoogleSignInAccount must not be null");
    return new VideosClient(paramContext, zza(paramGoogleSignInAccount));
  }
  
  @Deprecated
  public static void setGravityForPopups(GoogleApiClient paramGoogleApiClient, int paramInt)
  {
    paramGoogleApiClient = zza(paramGoogleApiClient, false);
    if (paramGoogleApiClient != null) {
      paramGoogleApiClient.zzj(paramInt);
    }
  }
  
  @Deprecated
  public static void setViewForPopups(GoogleApiClient paramGoogleApiClient, View paramView)
  {
    Preconditions.checkNotNull(paramView);
    paramGoogleApiClient = zza(paramGoogleApiClient, false);
    if (paramGoogleApiClient != null) {
      paramGoogleApiClient.zza(paramView);
    }
  }
  
  @Deprecated
  public static PendingResult<Status> signOut(GoogleApiClient paramGoogleApiClient)
  {
    return paramGoogleApiClient.execute(new zzl(paramGoogleApiClient));
  }
  
  private static GamesOptions zza(@NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    Games.GamesOptions.Builder localBuilder = new Games.GamesOptions.Builder(null, null);
    localBuilder.zzbb = paramGoogleSignInAccount;
    return localBuilder.setSdkVariant(1052947).build();
  }
  
  public static com.google.android.gms.games.internal.zze zza(GoogleApiClient paramGoogleApiClient)
  {
    return zza(paramGoogleApiClient, true);
  }
  
  public static com.google.android.gms.games.internal.zze zza(GoogleApiClient paramGoogleApiClient, boolean paramBoolean)
  {
    if (paramGoogleApiClient != null) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool, "GoogleApiClient parameter is required.");
      Preconditions.checkState(paramGoogleApiClient.isConnected(), "GoogleApiClient must be connected.");
      return zzb(paramGoogleApiClient, paramBoolean);
    }
  }
  
  public static com.google.android.gms.games.internal.zze zzb(GoogleApiClient paramGoogleApiClient, boolean paramBoolean)
  {
    Preconditions.checkState(paramGoogleApiClient.hasApi(API), "GoogleApiClient is not configured to use the Games Api. Pass Games.API into GoogleApiClient.Builder#addApi() to use this feature.");
    boolean bool = paramGoogleApiClient.hasConnectedApi(API);
    if ((paramBoolean) && (!bool)) {
      throw new IllegalStateException("GoogleApiClient has an optional Games.API and is not connected to Games. Use GoogleApiClient.hasConnectedApi(Games.API) to guard this call.");
    }
    if (bool) {
      return (com.google.android.gms.games.internal.zze)paramGoogleApiClient.getClient(CLIENT_KEY);
    }
    return null;
  }
  
  @Deprecated
  public static final class GamesOptions
    implements GoogleSignInOptionsExtension, Api.ApiOptions.HasGoogleSignInAccountOptions, Api.ApiOptions.Optional
  {
    public final boolean zzar;
    public final boolean zzas;
    public final int zzat;
    public final boolean zzau;
    public final int zzav;
    public final String zzaw;
    public final ArrayList<String> zzax;
    public final boolean zzay;
    public final boolean zzaz;
    public final boolean zzba;
    public final GoogleSignInAccount zzbb;
    
    private GamesOptions(boolean paramBoolean1, boolean paramBoolean2, int paramInt1, boolean paramBoolean3, int paramInt2, String paramString, ArrayList<String> paramArrayList, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, GoogleSignInAccount paramGoogleSignInAccount)
    {
      this.zzar = paramBoolean1;
      this.zzas = paramBoolean2;
      this.zzat = paramInt1;
      this.zzau = paramBoolean3;
      this.zzav = paramInt2;
      this.zzaw = paramString;
      this.zzax = paramArrayList;
      this.zzay = paramBoolean4;
      this.zzaz = paramBoolean5;
      this.zzba = paramBoolean6;
      this.zzbb = paramGoogleSignInAccount;
    }
    
    public static Builder builder()
    {
      return new Builder(null);
    }
    
    public final boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof GamesOptions)) {
          return false;
        }
        paramObject = (GamesOptions)paramObject;
        if ((this.zzar != ((GamesOptions)paramObject).zzar) || (this.zzas != ((GamesOptions)paramObject).zzas) || (this.zzat != ((GamesOptions)paramObject).zzat) || (this.zzau != ((GamesOptions)paramObject).zzau) || (this.zzav != ((GamesOptions)paramObject).zzav)) {
          break;
        }
        if (this.zzaw != null) {
          break label153;
        }
        if (((GamesOptions)paramObject).zzaw != null) {
          break;
        }
        if ((!this.zzax.equals(((GamesOptions)paramObject).zzax)) || (this.zzay != ((GamesOptions)paramObject).zzay) || (this.zzaz != ((GamesOptions)paramObject).zzaz) || (this.zzba != ((GamesOptions)paramObject).zzba)) {
          break;
        }
        if (this.zzbb != null) {
          break label170;
        }
      } while (((GamesOptions)paramObject).zzbb == null);
      label153:
      label170:
      while (!this.zzbb.equals(((GamesOptions)paramObject).zzbb))
      {
        do
        {
          return false;
        } while (!this.zzaw.equals(((GamesOptions)paramObject).zzaw));
        break;
      }
      return true;
    }
    
    public final int getExtensionType()
    {
      return 1;
    }
    
    public final GoogleSignInAccount getGoogleSignInAccount()
    {
      return this.zzbb;
    }
    
    public final List<Scope> getImpliedScopes()
    {
      if (this.zzay) {
        return Collections.singletonList(Games.SCOPE_GAMES);
      }
      return Collections.singletonList(Games.SCOPE_GAMES_LITE);
    }
    
    public final int hashCode()
    {
      int i2 = 1;
      int i3 = 0;
      int i;
      int j;
      label24:
      int i4;
      int k;
      label39:
      int i5;
      int m;
      label55:
      int i6;
      int n;
      label74:
      int i1;
      if (this.zzar)
      {
        i = 1;
        if (!this.zzas) {
          break label167;
        }
        j = 1;
        i4 = this.zzat;
        if (!this.zzau) {
          break label172;
        }
        k = 1;
        i5 = this.zzav;
        if (this.zzaw != null) {
          break label177;
        }
        m = 0;
        i6 = this.zzax.hashCode();
        if (!this.zzay) {
          break label189;
        }
        n = 1;
        if (!this.zzaz) {
          break label195;
        }
        i1 = 1;
        label84:
        if (!this.zzba) {
          break label201;
        }
        label91:
        if (this.zzbb != null) {
          break label207;
        }
      }
      for (;;)
      {
        return ((i1 + (n + ((m + ((k + ((j + (i + 527) * 31) * 31 + i4) * 31) * 31 + i5) * 31) * 31 + i6) * 31) * 31) * 31 + i2) * 31 + i3;
        i = 0;
        break;
        label167:
        j = 0;
        break label24;
        label172:
        k = 0;
        break label39;
        label177:
        m = this.zzaw.hashCode();
        break label55;
        label189:
        n = 0;
        break label74;
        label195:
        i1 = 0;
        break label84;
        label201:
        i2 = 0;
        break label91;
        label207:
        i3 = this.zzbb.hashCode();
      }
    }
    
    public final Bundle toBundle()
    {
      return zzf();
    }
    
    public final Bundle zzf()
    {
      Bundle localBundle = new Bundle();
      localBundle.putBoolean("com.google.android.gms.games.key.isHeadless", this.zzar);
      localBundle.putBoolean("com.google.android.gms.games.key.showConnectingPopup", this.zzas);
      localBundle.putInt("com.google.android.gms.games.key.connectingPopupGravity", this.zzat);
      localBundle.putBoolean("com.google.android.gms.games.key.retryingSignIn", this.zzau);
      localBundle.putInt("com.google.android.gms.games.key.sdkVariant", this.zzav);
      localBundle.putString("com.google.android.gms.games.key.forceResolveAccountKey", this.zzaw);
      localBundle.putStringArrayList("com.google.android.gms.games.key.proxyApis", this.zzax);
      localBundle.putBoolean("com.google.android.gms.games.key.requireGooglePlus", this.zzay);
      localBundle.putBoolean("com.google.android.gms.games.key.unauthenticated", this.zzaz);
      localBundle.putBoolean("com.google.android.gms.games.key.skipWelcomePopup", this.zzba);
      return localBundle;
    }
    
    @Deprecated
    public static final class Builder
    {
      private boolean zzar = false;
      private boolean zzas = true;
      private int zzat = 17;
      private boolean zzau = false;
      private int zzav = 4368;
      private String zzaw = null;
      private ArrayList<String> zzax = new ArrayList();
      private boolean zzay = false;
      private boolean zzaz = false;
      private boolean zzba = false;
      GoogleSignInAccount zzbb = null;
      
      private Builder() {}
      
      private Builder(Games.GamesOptions paramGamesOptions)
      {
        if (paramGamesOptions != null)
        {
          this.zzar = paramGamesOptions.zzar;
          this.zzas = paramGamesOptions.zzas;
          this.zzat = paramGamesOptions.zzat;
          this.zzau = paramGamesOptions.zzau;
          this.zzav = paramGamesOptions.zzav;
          this.zzaw = paramGamesOptions.zzaw;
          this.zzax = paramGamesOptions.zzax;
          this.zzay = paramGamesOptions.zzay;
          this.zzaz = paramGamesOptions.zzaz;
          this.zzba = paramGamesOptions.zzba;
          this.zzbb = paramGamesOptions.zzbb;
        }
      }
      
      public final Games.GamesOptions build()
      {
        return new Games.GamesOptions(this.zzar, this.zzas, this.zzat, this.zzau, this.zzav, this.zzaw, this.zzax, this.zzay, this.zzaz, this.zzba, this.zzbb, null);
      }
      
      public final Builder setSdkVariant(int paramInt)
      {
        this.zzav = paramInt;
        return this;
      }
      
      public final Builder setShowConnectingPopup(boolean paramBoolean)
      {
        this.zzas = paramBoolean;
        this.zzat = 17;
        return this;
      }
      
      public final Builder setShowConnectingPopup(boolean paramBoolean, int paramInt)
      {
        this.zzas = paramBoolean;
        this.zzat = paramInt;
        return this;
      }
    }
  }
  
  @Deprecated
  @KeepForSdk
  public static abstract interface GetServerAuthCodeResult
    extends Result
  {
    @KeepForSdk
    public abstract String getCode();
  }
  
  public static abstract class zza<R extends Result>
    extends BaseImplementation.ApiMethodImpl<R, com.google.android.gms.games.internal.zze>
  {
    public zza(GoogleApiClient paramGoogleApiClient)
    {
      super(paramGoogleApiClient);
    }
  }
  
  private static class zzb
    extends Api.AbstractClientBuilder<com.google.android.gms.games.internal.zze, Games.GamesOptions>
  {
    public int getPriority()
    {
      return 1;
    }
  }
  
  private static abstract class zzc
    extends Games.zza<Games.GetServerAuthCodeResult>
  {
    private zzc(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
  }
  
  private static abstract class zzd
    extends Games.zza<Status>
  {
    private zzd(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\Games.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */