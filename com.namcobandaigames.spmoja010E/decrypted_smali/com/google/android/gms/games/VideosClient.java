package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolders;
import com.google.android.gms.common.internal.PendingResultUtil.ResultConverter;
import com.google.android.gms.games.internal.zzi;
import com.google.android.gms.games.video.CaptureState;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.games.video.Videos.CaptureAvailableResult;
import com.google.android.gms.games.video.Videos.CaptureCapabilitiesResult;
import com.google.android.gms.games.video.Videos.CaptureOverlayStateListener;
import com.google.android.gms.games.video.Videos.CaptureStateResult;
import com.google.android.gms.internal.games.zzu;
import com.google.android.gms.tasks.Task;

public class VideosClient
  extends zzu
{
  public static final int CAPTURE_OVERLAY_STATE_CAPTURE_STARTED = 2;
  public static final int CAPTURE_OVERLAY_STATE_CAPTURE_STOPPED = 3;
  public static final int CAPTURE_OVERLAY_STATE_DISMISSED = 4;
  public static final int CAPTURE_OVERLAY_STATE_SHOWN = 1;
  private static final PendingResultUtil.ResultConverter<Videos.CaptureAvailableResult, Boolean> zzex = new zzda();
  private static final PendingResultUtil.ResultConverter<Videos.CaptureStateResult, CaptureState> zzey = new zzdb();
  private static final PendingResultUtil.ResultConverter<Videos.CaptureCapabilitiesResult, VideoCapabilities> zzez = new zzdc();
  
  VideosClient(@NonNull Activity paramActivity, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, paramGamesOptions);
  }
  
  VideosClient(@NonNull Context paramContext, @NonNull Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, paramGamesOptions);
  }
  
  public Task<VideoCapabilities> getCaptureCapabilities()
  {
    return zzi.toTask(Games.Videos.getCaptureCapabilities(asGoogleApiClient()), zzez);
  }
  
  public Task<Intent> getCaptureOverlayIntent()
  {
    return doRead(new zzcw(this));
  }
  
  public Task<CaptureState> getCaptureState()
  {
    return zzi.toTask(Games.Videos.getCaptureState(asGoogleApiClient()), zzey);
  }
  
  public Task<Boolean> isCaptureAvailable(int paramInt)
  {
    return zzi.toTask(Games.Videos.isCaptureAvailable(asGoogleApiClient(), paramInt), zzex);
  }
  
  public Task<Boolean> isCaptureSupported()
  {
    return doRead(new zzcx(this));
  }
  
  public Task<Void> registerOnCaptureOverlayStateChangedListener(@NonNull OnCaptureOverlayStateListener paramOnCaptureOverlayStateListener)
  {
    paramOnCaptureOverlayStateListener = registerListener(paramOnCaptureOverlayStateListener, OnCaptureOverlayStateListener.class.getSimpleName());
    return doRegisterEventListener(new zzcy(this, paramOnCaptureOverlayStateListener, paramOnCaptureOverlayStateListener), new zzcz(this, paramOnCaptureOverlayStateListener.getListenerKey()));
  }
  
  public Task<Boolean> unregisterOnCaptureOverlayStateChangedListener(@NonNull OnCaptureOverlayStateListener paramOnCaptureOverlayStateListener)
  {
    return doUnregisterEventListener(ListenerHolders.createListenerKey(paramOnCaptureOverlayStateListener, OnCaptureOverlayStateListener.class.getSimpleName()));
  }
  
  public static abstract interface OnCaptureOverlayStateListener
    extends Videos.CaptureOverlayStateListener
  {
    public abstract void onCaptureOverlayStateChanged(int paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\VideosClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */