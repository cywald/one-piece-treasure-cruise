package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;

public abstract class TurnBasedMatchConfig
{
  public static Builder builder()
  {
    return new Builder(null);
  }
  
  public static Bundle createAutoMatchCriteria(int paramInt1, int paramInt2, long paramLong)
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("min_automatch_players", paramInt1);
    localBundle.putInt("max_automatch_players", paramInt2);
    localBundle.putLong("exclusive_bit_mask", paramLong);
    return localBundle;
  }
  
  public abstract Bundle getAutoMatchCriteria();
  
  public abstract String[] getInvitedPlayerIds();
  
  public abstract int getVariant();
  
  public abstract int zzci();
  
  public static final class Builder
  {
    int zzoe = -1;
    ArrayList<String> zzoy = new ArrayList();
    Bundle zzoz = null;
    int zzpk = 2;
    
    public final Builder addInvitedPlayer(String paramString)
    {
      Preconditions.checkNotNull(paramString);
      this.zzoy.add(paramString);
      return this;
    }
    
    public final Builder addInvitedPlayers(ArrayList<String> paramArrayList)
    {
      Preconditions.checkNotNull(paramArrayList);
      this.zzoy.addAll(paramArrayList);
      return this;
    }
    
    public final TurnBasedMatchConfig build()
    {
      return new zzb(this);
    }
    
    public final Builder setAutoMatchCriteria(Bundle paramBundle)
    {
      this.zzoz = paramBundle;
      return this;
    }
    
    public final Builder setVariant(int paramInt)
    {
      if ((paramInt == -1) || (paramInt > 0)) {}
      for (boolean bool = true;; bool = false)
      {
        Preconditions.checkArgument(bool, "Variant must be a positive integer or TurnBasedMatch.MATCH_VARIANT_ANY");
        this.zzoe = paramInt;
        return this;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\turnbased\TurnBasedMatchConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */