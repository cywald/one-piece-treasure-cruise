package com.google.android.gms.games.multiplayer;

import android.support.annotation.NonNull;

public abstract class InvitationCallback
  implements OnInvitationReceivedListener
{
  public abstract void onInvitationReceived(@NonNull Invitation paramInvitation);
  
  public abstract void onInvitationRemoved(@NonNull String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\InvitationCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */