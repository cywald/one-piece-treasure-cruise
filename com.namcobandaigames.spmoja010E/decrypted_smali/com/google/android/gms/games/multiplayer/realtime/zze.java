package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;

public class zze
  implements Parcelable.Creator<RoomEntity>
{
  public RoomEntity zzf(Parcel paramParcel)
  {
    int i = 0;
    ArrayList localArrayList = null;
    int m = SafeParcelReader.validateObjectHeader(paramParcel);
    long l = 0L;
    Bundle localBundle = null;
    int j = 0;
    String str1 = null;
    int k = 0;
    String str2 = null;
    String str3 = null;
    while (paramParcel.dataPosition() < m)
    {
      int n = SafeParcelReader.readHeader(paramParcel);
      switch (SafeParcelReader.getFieldId(n))
      {
      default: 
        SafeParcelReader.skipUnknownField(paramParcel, n);
        break;
      case 1: 
        str3 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 2: 
        str2 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 3: 
        l = SafeParcelReader.readLong(paramParcel, n);
        break;
      case 4: 
        k = SafeParcelReader.readInt(paramParcel, n);
        break;
      case 5: 
        str1 = SafeParcelReader.createString(paramParcel, n);
        break;
      case 6: 
        j = SafeParcelReader.readInt(paramParcel, n);
        break;
      case 7: 
        localBundle = SafeParcelReader.createBundle(paramParcel, n);
        break;
      case 8: 
        localArrayList = SafeParcelReader.createTypedList(paramParcel, n, ParticipantEntity.CREATOR);
        break;
      case 9: 
        i = SafeParcelReader.readInt(paramParcel, n);
      }
    }
    SafeParcelReader.ensureAtEnd(paramParcel, m);
    return new RoomEntity(str3, str2, l, k, str1, j, localBundle, localArrayList, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */