package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.games.PlayerEntity;

public class zzc
  implements Parcelable.Creator<ParticipantEntity>
{
  public ParticipantEntity zze(Parcel paramParcel)
  {
    int k = SafeParcelReader.validateObjectHeader(paramParcel);
    String str5 = null;
    String str4 = null;
    Uri localUri2 = null;
    Uri localUri1 = null;
    int j = 0;
    String str3 = null;
    boolean bool = false;
    PlayerEntity localPlayerEntity = null;
    int i = 0;
    ParticipantResult localParticipantResult = null;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < k)
    {
      int m = SafeParcelReader.readHeader(paramParcel);
      switch (SafeParcelReader.getFieldId(m))
      {
      default: 
        SafeParcelReader.skipUnknownField(paramParcel, m);
        break;
      case 1: 
        str5 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 2: 
        str4 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 3: 
        localUri2 = (Uri)SafeParcelReader.createParcelable(paramParcel, m, Uri.CREATOR);
        break;
      case 4: 
        localUri1 = (Uri)SafeParcelReader.createParcelable(paramParcel, m, Uri.CREATOR);
        break;
      case 5: 
        j = SafeParcelReader.readInt(paramParcel, m);
        break;
      case 6: 
        str3 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 7: 
        bool = SafeParcelReader.readBoolean(paramParcel, m);
        break;
      case 8: 
        localPlayerEntity = (PlayerEntity)SafeParcelReader.createParcelable(paramParcel, m, PlayerEntity.CREATOR);
        break;
      case 9: 
        i = SafeParcelReader.readInt(paramParcel, m);
        break;
      case 10: 
        localParticipantResult = (ParticipantResult)SafeParcelReader.createParcelable(paramParcel, m, ParticipantResult.CREATOR);
        break;
      case 11: 
        str2 = SafeParcelReader.createString(paramParcel, m);
        break;
      case 12: 
        str1 = SafeParcelReader.createString(paramParcel, m);
      }
    }
    SafeParcelReader.ensureAtEnd(paramParcel, k);
    return new ParticipantEntity(str5, str4, localUri2, localUri1, j, str3, bool, localPlayerEntity, i, localParticipantResult, str2, str1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */