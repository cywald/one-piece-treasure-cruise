package com.google.android.gms.games.multiplayer.realtime;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;

public final class RealTimeMessage
  implements Parcelable
{
  public static final Parcelable.Creator<RealTimeMessage> CREATOR = new zza();
  public static final int RELIABLE = 1;
  public static final int UNRELIABLE = 0;
  private final String zzoo;
  private final byte[] zzop;
  private final int zzoq;
  
  private RealTimeMessage(Parcel paramParcel)
  {
    this(paramParcel.readString(), paramParcel.createByteArray(), paramParcel.readInt());
  }
  
  private RealTimeMessage(String paramString, byte[] paramArrayOfByte, int paramInt)
  {
    this.zzoo = ((String)Preconditions.checkNotNull(paramString));
    this.zzop = ((byte[])((byte[])Preconditions.checkNotNull(paramArrayOfByte)).clone());
    this.zzoq = paramInt;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final byte[] getMessageData()
  {
    return this.zzop;
  }
  
  public final String getSenderParticipantId()
  {
    return this.zzoo;
  }
  
  public final boolean isReliable()
  {
    return this.zzoq == 1;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.zzoo);
    paramParcel.writeByteArray(this.zzop);
    paramParcel.writeInt(this.zzoq);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\RealTimeMessage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */