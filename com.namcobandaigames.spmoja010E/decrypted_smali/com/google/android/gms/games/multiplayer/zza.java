package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

public class zza
  implements Parcelable.Creator<InvitationEntity>
{
  public InvitationEntity zzd(Parcel paramParcel)
  {
    int i = 0;
    ArrayList localArrayList = null;
    int m = SafeParcelReader.validateObjectHeader(paramParcel);
    long l = 0L;
    int j = 0;
    ParticipantEntity localParticipantEntity = null;
    int k = 0;
    String str = null;
    GameEntity localGameEntity = null;
    while (paramParcel.dataPosition() < m)
    {
      int n = SafeParcelReader.readHeader(paramParcel);
      switch (SafeParcelReader.getFieldId(n))
      {
      default: 
        SafeParcelReader.skipUnknownField(paramParcel, n);
        break;
      case 1: 
        localGameEntity = (GameEntity)SafeParcelReader.createParcelable(paramParcel, n, GameEntity.CREATOR);
        break;
      case 2: 
        str = SafeParcelReader.createString(paramParcel, n);
        break;
      case 3: 
        l = SafeParcelReader.readLong(paramParcel, n);
        break;
      case 4: 
        k = SafeParcelReader.readInt(paramParcel, n);
        break;
      case 5: 
        localParticipantEntity = (ParticipantEntity)SafeParcelReader.createParcelable(paramParcel, n, ParticipantEntity.CREATOR);
        break;
      case 6: 
        localArrayList = SafeParcelReader.createTypedList(paramParcel, n, ParticipantEntity.CREATOR);
        break;
      case 7: 
        j = SafeParcelReader.readInt(paramParcel, n);
        break;
      case 8: 
        i = SafeParcelReader.readInt(paramParcel, n);
      }
    }
    SafeParcelReader.ensureAtEnd(paramParcel, m);
    return new InvitationEntity(localGameEntity, str, l, k, localParticipantEntity, localArrayList, j, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */