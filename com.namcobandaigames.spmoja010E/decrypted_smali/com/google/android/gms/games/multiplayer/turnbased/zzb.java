package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import java.util.ArrayList;

public final class zzb
  extends TurnBasedMatchConfig
{
  private final int zzoe;
  private final Bundle zzoz;
  private final String[] zzpb;
  private final int zzpk;
  
  zzb(TurnBasedMatchConfig.Builder paramBuilder)
  {
    this.zzoe = paramBuilder.zzoe;
    this.zzpk = paramBuilder.zzpk;
    this.zzoz = paramBuilder.zzoz;
    int i = paramBuilder.zzoy.size();
    this.zzpb = ((String[])paramBuilder.zzoy.toArray(new String[i]));
  }
  
  public final Bundle getAutoMatchCriteria()
  {
    return this.zzoz;
  }
  
  public final String[] getInvitedPlayerIds()
  {
    return this.zzpb;
  }
  
  public final int getVariant()
  {
    return this.zzoe;
  }
  
  public final int zzci()
  {
    return this.zzpk;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\turnbased\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */