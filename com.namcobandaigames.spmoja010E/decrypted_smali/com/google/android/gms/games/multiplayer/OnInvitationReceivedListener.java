package com.google.android.gms.games.multiplayer;

@Deprecated
public abstract interface OnInvitationReceivedListener
{
  public abstract void onInvitationReceived(Invitation paramInvitation);
  
  public abstract void onInvitationRemoved(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\OnInvitationReceivedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */