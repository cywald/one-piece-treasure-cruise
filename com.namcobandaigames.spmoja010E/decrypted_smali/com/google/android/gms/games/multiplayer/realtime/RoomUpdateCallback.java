package com.google.android.gms.games.multiplayer.realtime;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class RoomUpdateCallback
  implements RoomUpdateListener
{
  public abstract void onJoinedRoom(int paramInt, @Nullable Room paramRoom);
  
  public abstract void onLeftRoom(int paramInt, @NonNull String paramString);
  
  public abstract void onRoomConnected(int paramInt, @Nullable Room paramRoom);
  
  public abstract void onRoomCreated(int paramInt, @Nullable Room paramRoom);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\RoomUpdateCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */