package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zzh;
import com.google.android.gms.games.multiplayer.InvitationBuffer;

public final class LoadMatchesResponse
{
  private final InvitationBuffer zzpg;
  private final TurnBasedMatchBuffer zzph;
  private final TurnBasedMatchBuffer zzpi;
  private final TurnBasedMatchBuffer zzpj;
  
  public LoadMatchesResponse(Bundle paramBundle)
  {
    DataHolder localDataHolder = zza(paramBundle, 0);
    if (localDataHolder != null)
    {
      this.zzpg = new InvitationBuffer(localDataHolder);
      localDataHolder = zza(paramBundle, 1);
      if (localDataHolder == null) {
        break label101;
      }
      this.zzph = new TurnBasedMatchBuffer(localDataHolder);
      label48:
      localDataHolder = zza(paramBundle, 2);
      if (localDataHolder == null) {
        break label109;
      }
    }
    label101:
    label109:
    for (this.zzpi = new TurnBasedMatchBuffer(localDataHolder);; this.zzpi = null)
    {
      paramBundle = zza(paramBundle, 3);
      if (paramBundle == null) {
        break label117;
      }
      this.zzpj = new TurnBasedMatchBuffer(paramBundle);
      return;
      this.zzpg = null;
      break;
      this.zzph = null;
      break label48;
    }
    label117:
    this.zzpj = null;
  }
  
  private static DataHolder zza(Bundle paramBundle, int paramInt)
  {
    String str;
    switch (paramInt)
    {
    default: 
      zzh.e("MatchTurnStatus", 38 + "Unknown match turn status: " + paramInt);
      str = "TURN_STATUS_UNKNOWN";
    }
    while (!paramBundle.containsKey(str))
    {
      return null;
      str = "TURN_STATUS_INVITED";
      continue;
      str = "TURN_STATUS_MY_TURN";
      continue;
      str = "TURN_STATUS_THEIR_TURN";
      continue;
      str = "TURN_STATUS_COMPLETE";
    }
    return (DataHolder)paramBundle.getParcelable(str);
  }
  
  @Deprecated
  public final void close()
  {
    release();
  }
  
  public final TurnBasedMatchBuffer getCompletedMatches()
  {
    return this.zzpj;
  }
  
  public final InvitationBuffer getInvitations()
  {
    return this.zzpg;
  }
  
  public final TurnBasedMatchBuffer getMyTurnMatches()
  {
    return this.zzph;
  }
  
  public final TurnBasedMatchBuffer getTheirTurnMatches()
  {
    return this.zzpi;
  }
  
  public final boolean hasData()
  {
    if ((this.zzpg != null) && (this.zzpg.getCount() > 0)) {}
    while (((this.zzph != null) && (this.zzph.getCount() > 0)) || ((this.zzpi != null) && (this.zzpi.getCount() > 0)) || ((this.zzpj != null) && (this.zzpj.getCount() > 0))) {
      return true;
    }
    return false;
  }
  
  public final void release()
  {
    if (this.zzpg != null) {
      this.zzpg.release();
    }
    if (this.zzph != null) {
      this.zzph.release();
    }
    if (this.zzpi != null) {
      this.zzpi.release();
    }
    if (this.zzpj != null) {
      this.zzpj.release();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\turnbased\LoadMatchesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */