package com.google.android.gms.games.multiplayer.realtime;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.EntityBuffer;

public final class zzb
  extends EntityBuffer<Room>
{
  public zzb(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
  }
  
  protected final String getPrimaryDataMarkerColumn()
  {
    return "external_match_id";
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */