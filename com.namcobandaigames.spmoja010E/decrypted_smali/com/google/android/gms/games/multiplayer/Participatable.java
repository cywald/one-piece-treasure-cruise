package com.google.android.gms.games.multiplayer;

import java.util.ArrayList;

public abstract interface Participatable
{
  public abstract ArrayList<Participant> getParticipants();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\Participatable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */