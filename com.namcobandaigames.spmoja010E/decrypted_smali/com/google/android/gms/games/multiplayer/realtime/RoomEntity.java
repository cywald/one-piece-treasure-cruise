package com.google.android.gms.games.multiplayer.realtime;

import android.database.CharArrayBuffer;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;

@SafeParcelable.Class(creator="RoomEntityCreator")
@SafeParcelable.Reserved({1000})
@RetainForClient
public final class RoomEntity
  extends GamesDowngradeableSafeParcel
  implements Room
{
  public static final Parcelable.Creator<RoomEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getDescription", id=5)
  private final String description;
  @SafeParcelable.Field(getter="getRoomId", id=1)
  private final String zzgt;
  @SafeParcelable.Field(getter="getCreationTimestamp", id=3)
  private final long zzoa;
  @SafeParcelable.Field(getter="getParticipants", id=8)
  private final ArrayList<ParticipantEntity> zzod;
  @SafeParcelable.Field(getter="getVariant", id=6)
  private final int zzoe;
  @Nullable
  @SafeParcelable.Field(getter="getAutoMatchCriteria", id=7)
  private final Bundle zzoz;
  @SafeParcelable.Field(getter="getCreatorId", id=2)
  private final String zzpc;
  @SafeParcelable.Field(getter="getStatus", id=4)
  private final int zzpd;
  @SafeParcelable.Field(getter="getAutoMatchWaitEstimateSeconds", id=9)
  private final int zzpe;
  
  public RoomEntity(Room paramRoom)
  {
    this.zzgt = paramRoom.getRoomId();
    this.zzpc = paramRoom.getCreatorId();
    this.zzoa = paramRoom.getCreationTimestamp();
    this.zzpd = paramRoom.getStatus();
    this.description = paramRoom.getDescription();
    this.zzoe = paramRoom.getVariant();
    this.zzoz = paramRoom.getAutoMatchCriteria();
    ArrayList localArrayList = paramRoom.getParticipants();
    int j = localArrayList.size();
    this.zzod = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      this.zzod.add((ParticipantEntity)((Participant)localArrayList.get(i)).freeze());
      i += 1;
    }
    this.zzpe = paramRoom.getAutoMatchWaitEstimateSeconds();
  }
  
  @SafeParcelable.Constructor
  RoomEntity(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) String paramString2, @SafeParcelable.Param(id=3) long paramLong, @SafeParcelable.Param(id=4) int paramInt1, @SafeParcelable.Param(id=5) String paramString3, @SafeParcelable.Param(id=6) int paramInt2, @Nullable @SafeParcelable.Param(id=7) Bundle paramBundle, @SafeParcelable.Param(id=8) ArrayList<ParticipantEntity> paramArrayList, @SafeParcelable.Param(id=9) int paramInt3)
  {
    this.zzgt = paramString1;
    this.zzpc = paramString2;
    this.zzoa = paramLong;
    this.zzpd = paramInt1;
    this.description = paramString3;
    this.zzoe = paramInt2;
    this.zzoz = paramBundle;
    this.zzod = paramArrayList;
    this.zzpe = paramInt3;
  }
  
  static int zza(Room paramRoom)
  {
    return Objects.hashCode(new Object[] { paramRoom.getRoomId(), paramRoom.getCreatorId(), Long.valueOf(paramRoom.getCreationTimestamp()), Integer.valueOf(paramRoom.getStatus()), paramRoom.getDescription(), Integer.valueOf(paramRoom.getVariant()), Integer.valueOf(zzc.zza(paramRoom.getAutoMatchCriteria())), paramRoom.getParticipants(), Integer.valueOf(paramRoom.getAutoMatchWaitEstimateSeconds()) });
  }
  
  static int zza(Room paramRoom, String paramString)
  {
    ArrayList localArrayList = paramRoom.getParticipants();
    int j = localArrayList.size();
    int i = 0;
    while (i < j)
    {
      Participant localParticipant = (Participant)localArrayList.get(i);
      if (localParticipant.getParticipantId().equals(paramString)) {
        return localParticipant.getStatus();
      }
      i += 1;
    }
    paramRoom = paramRoom.getRoomId();
    throw new IllegalStateException(String.valueOf(paramString).length() + 28 + String.valueOf(paramRoom).length() + "Participant " + paramString + " is not in room " + paramRoom);
  }
  
  static boolean zza(Room paramRoom, Object paramObject)
  {
    if (!(paramObject instanceof Room)) {}
    do
    {
      return false;
      if (paramRoom == paramObject) {
        return true;
      }
      paramObject = (Room)paramObject;
    } while ((!Objects.equal(((Room)paramObject).getRoomId(), paramRoom.getRoomId())) || (!Objects.equal(((Room)paramObject).getCreatorId(), paramRoom.getCreatorId())) || (!Objects.equal(Long.valueOf(((Room)paramObject).getCreationTimestamp()), Long.valueOf(paramRoom.getCreationTimestamp()))) || (!Objects.equal(Integer.valueOf(((Room)paramObject).getStatus()), Integer.valueOf(paramRoom.getStatus()))) || (!Objects.equal(((Room)paramObject).getDescription(), paramRoom.getDescription())) || (!Objects.equal(Integer.valueOf(((Room)paramObject).getVariant()), Integer.valueOf(paramRoom.getVariant()))) || (!zzc.zza(((Room)paramObject).getAutoMatchCriteria(), paramRoom.getAutoMatchCriteria())) || (!Objects.equal(((Room)paramObject).getParticipants(), paramRoom.getParticipants())) || (!Objects.equal(Integer.valueOf(((Room)paramObject).getAutoMatchWaitEstimateSeconds()), Integer.valueOf(paramRoom.getAutoMatchWaitEstimateSeconds()))));
    return true;
  }
  
  static String zzb(Room paramRoom)
  {
    return Objects.toStringHelper(paramRoom).add("RoomId", paramRoom.getRoomId()).add("CreatorId", paramRoom.getCreatorId()).add("CreationTimestamp", Long.valueOf(paramRoom.getCreationTimestamp())).add("RoomStatus", Integer.valueOf(paramRoom.getStatus())).add("Description", paramRoom.getDescription()).add("Variant", Integer.valueOf(paramRoom.getVariant())).add("AutoMatchCriteria", paramRoom.getAutoMatchCriteria()).add("Participants", paramRoom.getParticipants()).add("AutoMatchWaitEstimateSeconds", Integer.valueOf(paramRoom.getAutoMatchWaitEstimateSeconds())).toString();
  }
  
  static String zzb(Room paramRoom, String paramString)
  {
    paramRoom = paramRoom.getParticipants();
    int j = paramRoom.size();
    int i = 0;
    while (i < j)
    {
      Participant localParticipant = (Participant)paramRoom.get(i);
      Player localPlayer = localParticipant.getPlayer();
      if ((localPlayer != null) && (localPlayer.getPlayerId().equals(paramString))) {
        return localParticipant.getParticipantId();
      }
      i += 1;
    }
    return null;
  }
  
  static Participant zzc(Room paramRoom, String paramString)
  {
    ArrayList localArrayList = paramRoom.getParticipants();
    int j = localArrayList.size();
    int i = 0;
    while (i < j)
    {
      Participant localParticipant = (Participant)localArrayList.get(i);
      if (localParticipant.getParticipantId().equals(paramString)) {
        return localParticipant;
      }
      i += 1;
    }
    paramRoom = paramRoom.getRoomId();
    throw new IllegalStateException(String.valueOf(paramString).length() + 29 + String.valueOf(paramRoom).length() + "Participant " + paramString + " is not in match " + paramRoom);
  }
  
  static ArrayList<String> zzc(Room paramRoom)
  {
    paramRoom = paramRoom.getParticipants();
    int j = paramRoom.size();
    ArrayList localArrayList = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      localArrayList.add(((Participant)paramRoom.get(i)).getParticipantId());
      i += 1;
    }
    return localArrayList;
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final Room freeze()
  {
    return this;
  }
  
  @Nullable
  public final Bundle getAutoMatchCriteria()
  {
    return this.zzoz;
  }
  
  public final int getAutoMatchWaitEstimateSeconds()
  {
    return this.zzpe;
  }
  
  public final long getCreationTimestamp()
  {
    return this.zzoa;
  }
  
  public final String getCreatorId()
  {
    return this.zzpc;
  }
  
  public final String getDescription()
  {
    return this.description;
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.description, paramCharArrayBuffer);
  }
  
  public final Participant getParticipant(String paramString)
  {
    return zzc(this, paramString);
  }
  
  public final String getParticipantId(String paramString)
  {
    return zzb(this, paramString);
  }
  
  public final ArrayList<String> getParticipantIds()
  {
    return zzc(this);
  }
  
  public final int getParticipantStatus(String paramString)
  {
    return zza(this, paramString);
  }
  
  public final ArrayList<Participant> getParticipants()
  {
    return new ArrayList(this.zzod);
  }
  
  public final String getRoomId()
  {
    return this.zzgt;
  }
  
  public final int getStatus()
  {
    return this.zzpd;
  }
  
  public final int getVariant()
  {
    return this.zzoe;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final void setShouldDowngrade(boolean paramBoolean)
  {
    super.setShouldDowngrade(paramBoolean);
    int j = this.zzod.size();
    int i = 0;
    while (i < j)
    {
      ((ParticipantEntity)this.zzod.get(i)).setShouldDowngrade(paramBoolean);
      i += 1;
    }
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (!shouldDowngrade())
    {
      paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
      SafeParcelWriter.writeString(paramParcel, 1, getRoomId(), false);
      SafeParcelWriter.writeString(paramParcel, 2, getCreatorId(), false);
      SafeParcelWriter.writeLong(paramParcel, 3, getCreationTimestamp());
      SafeParcelWriter.writeInt(paramParcel, 4, getStatus());
      SafeParcelWriter.writeString(paramParcel, 5, getDescription(), false);
      SafeParcelWriter.writeInt(paramParcel, 6, getVariant());
      SafeParcelWriter.writeBundle(paramParcel, 7, getAutoMatchCriteria(), false);
      SafeParcelWriter.writeTypedList(paramParcel, 8, getParticipants(), false);
      SafeParcelWriter.writeInt(paramParcel, 9, getAutoMatchWaitEstimateSeconds());
      SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
    }
    for (;;)
    {
      return;
      paramParcel.writeString(this.zzgt);
      paramParcel.writeString(this.zzpc);
      paramParcel.writeLong(this.zzoa);
      paramParcel.writeInt(this.zzpd);
      paramParcel.writeString(this.description);
      paramParcel.writeInt(this.zzoe);
      paramParcel.writeBundle(this.zzoz);
      int j = this.zzod.size();
      paramParcel.writeInt(j);
      int i = 0;
      while (i < j)
      {
        ((ParticipantEntity)this.zzod.get(i)).writeToParcel(paramParcel, paramInt);
        i += 1;
      }
    }
  }
  
  static final class zza
    extends zze
  {
    public final RoomEntity zzf(Parcel paramParcel)
    {
      if ((RoomEntity.zza(RoomEntity.zze())) || (RoomEntity.zza(RoomEntity.class.getCanonicalName()))) {
        return super.zzf(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      long l = paramParcel.readLong();
      int j = paramParcel.readInt();
      String str3 = paramParcel.readString();
      int k = paramParcel.readInt();
      Bundle localBundle = paramParcel.readBundle();
      int m = paramParcel.readInt();
      ArrayList localArrayList = new ArrayList(m);
      int i = 0;
      while (i < m)
      {
        localArrayList.add((ParticipantEntity)ParticipantEntity.CREATOR.createFromParcel(paramParcel));
        i += 1;
      }
      return new RoomEntity(str1, str2, l, j, str3, k, localBundle, localArrayList, -1);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\RoomEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */