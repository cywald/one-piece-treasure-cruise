package com.google.android.gms.games.multiplayer.realtime;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public abstract class RoomStatusUpdateCallback
  implements RoomStatusUpdateListener
{
  public abstract void onConnectedToRoom(@Nullable Room paramRoom);
  
  public abstract void onDisconnectedFromRoom(@Nullable Room paramRoom);
  
  public abstract void onP2PConnected(@NonNull String paramString);
  
  public abstract void onP2PDisconnected(@NonNull String paramString);
  
  public abstract void onPeerDeclined(@Nullable Room paramRoom, @NonNull List<String> paramList);
  
  public abstract void onPeerInvitedToRoom(@Nullable Room paramRoom, @NonNull List<String> paramList);
  
  public abstract void onPeerJoined(@Nullable Room paramRoom, @NonNull List<String> paramList);
  
  public abstract void onPeerLeft(@Nullable Room paramRoom, @NonNull List<String> paramList);
  
  public abstract void onPeersConnected(@Nullable Room paramRoom, @NonNull List<String> paramList);
  
  public abstract void onPeersDisconnected(@Nullable Room paramRoom, @NonNull List<String> paramList);
  
  public abstract void onRoomAutoMatching(@Nullable Room paramRoom);
  
  public abstract void onRoomConnecting(@Nullable Room paramRoom);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\RoomStatusUpdateCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */