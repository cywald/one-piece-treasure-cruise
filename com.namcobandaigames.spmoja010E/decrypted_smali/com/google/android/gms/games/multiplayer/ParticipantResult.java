package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="ParticipantResultCreator")
@SafeParcelable.Reserved({1000})
public final class ParticipantResult
  extends com.google.android.gms.games.internal.zzd
{
  public static final Parcelable.Creator<ParticipantResult> CREATOR = new zzd();
  public static final int MATCH_RESULT_DISAGREED = 5;
  public static final int MATCH_RESULT_DISCONNECT = 4;
  public static final int MATCH_RESULT_LOSS = 1;
  public static final int MATCH_RESULT_NONE = 3;
  public static final int MATCH_RESULT_TIE = 2;
  public static final int MATCH_RESULT_UNINITIALIZED = -1;
  public static final int MATCH_RESULT_WIN = 0;
  public static final int PLACING_UNINITIALIZED = -1;
  @SafeParcelable.Field(getter="getParticipantId", id=1)
  private final String zzhl;
  @SafeParcelable.Field(getter="getResult", id=2)
  private final int zzom;
  @SafeParcelable.Field(getter="getPlacing", id=3)
  private final int zzon;
  
  @SafeParcelable.Constructor
  public ParticipantResult(@SafeParcelable.Param(id=1) String paramString, @SafeParcelable.Param(id=2) int paramInt1, @SafeParcelable.Param(id=3) int paramInt2)
  {
    this.zzhl = ((String)Preconditions.checkNotNull(paramString));
    switch (paramInt1)
    {
    }
    for (boolean bool = false;; bool = true)
    {
      Preconditions.checkState(bool);
      this.zzom = paramInt1;
      this.zzon = paramInt2;
      return;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof ParticipantResult)) {}
    do
    {
      return false;
      if (this == paramObject) {
        return true;
      }
      paramObject = (ParticipantResult)paramObject;
    } while ((((ParticipantResult)paramObject).getPlacing() != getPlacing()) || (((ParticipantResult)paramObject).getResult() != getResult()) || (!Objects.equal(((ParticipantResult)paramObject).getParticipantId(), getParticipantId())));
    return true;
  }
  
  public final String getParticipantId()
  {
    return this.zzhl;
  }
  
  public final int getPlacing()
  {
    return this.zzon;
  }
  
  public final int getResult()
  {
    return this.zzom;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(getPlacing()), Integer.valueOf(getResult()), getParticipantId() });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, getParticipantId(), false);
    SafeParcelWriter.writeInt(paramParcel, 2, getResult());
    SafeParcelWriter.writeInt(paramParcel, 3, getPlacing());
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\ParticipantResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */