package com.google.android.gms.games.multiplayer.turnbased;

import android.support.annotation.NonNull;

public abstract class TurnBasedMatchUpdateCallback
  implements OnTurnBasedMatchUpdateReceivedListener
{
  public abstract void onTurnBasedMatchReceived(@NonNull TurnBasedMatch paramTurnBasedMatch);
  
  public abstract void onTurnBasedMatchRemoved(@NonNull String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\turnbased\TurnBasedMatchUpdateCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */