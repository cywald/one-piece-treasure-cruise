package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import java.util.ArrayList;

public final class zzd
  extends RoomConfig
{
  private final String zzgr;
  private final int zzoe;
  @Deprecated
  private final RoomUpdateListener zzor;
  @Deprecated
  private final RoomStatusUpdateListener zzos;
  @Deprecated
  private final RealTimeMessageReceivedListener zzot;
  private final RoomUpdateCallback zzou;
  private final RoomStatusUpdateCallback zzov;
  private final OnRealTimeMessageReceivedListener zzow;
  private final Bundle zzoz;
  private final zzg zzpa;
  private final String[] zzpb;
  
  zzd(RoomConfig.Builder paramBuilder)
  {
    this.zzor = paramBuilder.zzor;
    this.zzos = paramBuilder.zzos;
    this.zzot = paramBuilder.zzot;
    this.zzou = paramBuilder.zzou;
    this.zzov = paramBuilder.zzov;
    this.zzow = paramBuilder.zzow;
    if (this.zzov != null) {}
    for (this.zzpa = new zzg(this.zzou, this.zzov, this.zzow);; this.zzpa = null)
    {
      this.zzgr = paramBuilder.zzox;
      this.zzoe = paramBuilder.zzoe;
      this.zzoz = paramBuilder.zzoz;
      int i = paramBuilder.zzoy.size();
      this.zzpb = ((String[])paramBuilder.zzoy.toArray(new String[i]));
      if ((this.zzow != null) || (this.zzot != null)) {
        break;
      }
      throw new NullPointerException(String.valueOf("Must specify a message listener"));
    }
  }
  
  public final Bundle getAutoMatchCriteria()
  {
    return this.zzoz;
  }
  
  public final String getInvitationId()
  {
    return this.zzgr;
  }
  
  public final String[] getInvitedPlayerIds()
  {
    return this.zzpb;
  }
  
  @Deprecated
  @Nullable
  public final RealTimeMessageReceivedListener getMessageReceivedListener()
  {
    return this.zzot;
  }
  
  @Nullable
  public final OnRealTimeMessageReceivedListener getOnMessageReceivedListener()
  {
    return this.zzow;
  }
  
  @Nullable
  public final RoomStatusUpdateCallback getRoomStatusUpdateCallback()
  {
    return this.zzov;
  }
  
  @Deprecated
  @Nullable
  public final RoomStatusUpdateListener getRoomStatusUpdateListener()
  {
    return this.zzos;
  }
  
  @Nullable
  public final RoomUpdateCallback getRoomUpdateCallback()
  {
    return this.zzou;
  }
  
  @Deprecated
  @Nullable
  public final RoomUpdateListener getRoomUpdateListener()
  {
    return this.zzor;
  }
  
  public final int getVariant()
  {
    return this.zzoe;
  }
  
  public final zzh zzch()
  {
    return this.zzpa;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */