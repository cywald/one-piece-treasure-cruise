package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import java.util.ArrayList;

@SafeParcelable.Class(creator="InvitationEntityCreator")
@SafeParcelable.Reserved({1000})
@RetainForClient
public final class InvitationEntity
  extends GamesDowngradeableSafeParcel
  implements Invitation
{
  public static final Parcelable.Creator<InvitationEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getInvitationId", id=2)
  private final String zzgr;
  @SafeParcelable.Field(getter="getGame", id=1)
  private final GameEntity zzky;
  @SafeParcelable.Field(getter="getCreationTimestamp", id=3)
  private final long zzoa;
  @SafeParcelable.Field(getter="getInvitationType", id=4)
  private final int zzob;
  @SafeParcelable.Field(getter="getInviter", id=5)
  private final ParticipantEntity zzoc;
  @SafeParcelable.Field(getter="getParticipants", id=6)
  private final ArrayList<ParticipantEntity> zzod;
  @SafeParcelable.Field(getter="getVariant", id=7)
  private final int zzoe;
  @SafeParcelable.Field(getter="getAvailableAutoMatchSlots", id=8)
  private final int zzof;
  
  @SafeParcelable.Constructor
  InvitationEntity(@SafeParcelable.Param(id=1) GameEntity paramGameEntity, @SafeParcelable.Param(id=2) String paramString, @SafeParcelable.Param(id=3) long paramLong, @SafeParcelable.Param(id=4) int paramInt1, @SafeParcelable.Param(id=5) ParticipantEntity paramParticipantEntity, @SafeParcelable.Param(id=6) ArrayList<ParticipantEntity> paramArrayList, @SafeParcelable.Param(id=7) int paramInt2, @SafeParcelable.Param(id=8) int paramInt3)
  {
    this.zzky = paramGameEntity;
    this.zzgr = paramString;
    this.zzoa = paramLong;
    this.zzob = paramInt1;
    this.zzoc = paramParticipantEntity;
    this.zzod = paramArrayList;
    this.zzoe = paramInt2;
    this.zzof = paramInt3;
  }
  
  InvitationEntity(Invitation paramInvitation)
  {
    this.zzky = new GameEntity(paramInvitation.getGame());
    this.zzgr = paramInvitation.getInvitationId();
    this.zzoa = paramInvitation.getCreationTimestamp();
    this.zzob = paramInvitation.getInvitationType();
    this.zzoe = paramInvitation.getVariant();
    this.zzof = paramInvitation.getAvailableAutoMatchSlots();
    String str = paramInvitation.getInviter().getParticipantId();
    Participant localParticipant = null;
    ArrayList localArrayList = paramInvitation.getParticipants();
    int j = localArrayList.size();
    this.zzod = new ArrayList(j);
    int i = 0;
    paramInvitation = localParticipant;
    while (i < j)
    {
      localParticipant = (Participant)localArrayList.get(i);
      if (localParticipant.getParticipantId().equals(str)) {
        paramInvitation = localParticipant;
      }
      this.zzod.add((ParticipantEntity)localParticipant.freeze());
      i += 1;
    }
    Preconditions.checkNotNull(paramInvitation, "Must have a valid inviter!");
    this.zzoc = ((ParticipantEntity)paramInvitation.freeze());
  }
  
  static int zza(Invitation paramInvitation)
  {
    return Objects.hashCode(new Object[] { paramInvitation.getGame(), paramInvitation.getInvitationId(), Long.valueOf(paramInvitation.getCreationTimestamp()), Integer.valueOf(paramInvitation.getInvitationType()), paramInvitation.getInviter(), paramInvitation.getParticipants(), Integer.valueOf(paramInvitation.getVariant()), Integer.valueOf(paramInvitation.getAvailableAutoMatchSlots()) });
  }
  
  static boolean zza(Invitation paramInvitation, Object paramObject)
  {
    if (!(paramObject instanceof Invitation)) {}
    do
    {
      return false;
      if (paramInvitation == paramObject) {
        return true;
      }
      paramObject = (Invitation)paramObject;
    } while ((!Objects.equal(((Invitation)paramObject).getGame(), paramInvitation.getGame())) || (!Objects.equal(((Invitation)paramObject).getInvitationId(), paramInvitation.getInvitationId())) || (!Objects.equal(Long.valueOf(((Invitation)paramObject).getCreationTimestamp()), Long.valueOf(paramInvitation.getCreationTimestamp()))) || (!Objects.equal(Integer.valueOf(((Invitation)paramObject).getInvitationType()), Integer.valueOf(paramInvitation.getInvitationType()))) || (!Objects.equal(((Invitation)paramObject).getInviter(), paramInvitation.getInviter())) || (!Objects.equal(((Invitation)paramObject).getParticipants(), paramInvitation.getParticipants())) || (!Objects.equal(Integer.valueOf(((Invitation)paramObject).getVariant()), Integer.valueOf(paramInvitation.getVariant()))) || (!Objects.equal(Integer.valueOf(((Invitation)paramObject).getAvailableAutoMatchSlots()), Integer.valueOf(paramInvitation.getAvailableAutoMatchSlots()))));
    return true;
  }
  
  static String zzb(Invitation paramInvitation)
  {
    return Objects.toStringHelper(paramInvitation).add("Game", paramInvitation.getGame()).add("InvitationId", paramInvitation.getInvitationId()).add("CreationTimestamp", Long.valueOf(paramInvitation.getCreationTimestamp())).add("InvitationType", Integer.valueOf(paramInvitation.getInvitationType())).add("Inviter", paramInvitation.getInviter()).add("Participants", paramInvitation.getParticipants()).add("Variant", Integer.valueOf(paramInvitation.getVariant())).add("AvailableAutoMatchSlots", Integer.valueOf(paramInvitation.getAvailableAutoMatchSlots())).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final Invitation freeze()
  {
    return this;
  }
  
  public final int getAvailableAutoMatchSlots()
  {
    return this.zzof;
  }
  
  public final long getCreationTimestamp()
  {
    return this.zzoa;
  }
  
  public final Game getGame()
  {
    return this.zzky;
  }
  
  public final String getInvitationId()
  {
    return this.zzgr;
  }
  
  public final int getInvitationType()
  {
    return this.zzob;
  }
  
  public final Participant getInviter()
  {
    return this.zzoc;
  }
  
  public final ArrayList<Participant> getParticipants()
  {
    return new ArrayList(this.zzod);
  }
  
  public final int getVariant()
  {
    return this.zzoe;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final void setShouldDowngrade(boolean paramBoolean)
  {
    super.setShouldDowngrade(paramBoolean);
    this.zzky.setShouldDowngrade(paramBoolean);
    this.zzoc.setShouldDowngrade(paramBoolean);
    int j = this.zzod.size();
    int i = 0;
    while (i < j)
    {
      ((ParticipantEntity)this.zzod.get(i)).setShouldDowngrade(paramBoolean);
      i += 1;
    }
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i;
    if (!shouldDowngrade())
    {
      i = SafeParcelWriter.beginObjectHeader(paramParcel);
      SafeParcelWriter.writeParcelable(paramParcel, 1, getGame(), paramInt, false);
      SafeParcelWriter.writeString(paramParcel, 2, getInvitationId(), false);
      SafeParcelWriter.writeLong(paramParcel, 3, getCreationTimestamp());
      SafeParcelWriter.writeInt(paramParcel, 4, getInvitationType());
      SafeParcelWriter.writeParcelable(paramParcel, 5, getInviter(), paramInt, false);
      SafeParcelWriter.writeTypedList(paramParcel, 6, getParticipants(), false);
      SafeParcelWriter.writeInt(paramParcel, 7, getVariant());
      SafeParcelWriter.writeInt(paramParcel, 8, getAvailableAutoMatchSlots());
      SafeParcelWriter.finishObjectHeader(paramParcel, i);
    }
    for (;;)
    {
      return;
      this.zzky.writeToParcel(paramParcel, paramInt);
      paramParcel.writeString(this.zzgr);
      paramParcel.writeLong(this.zzoa);
      paramParcel.writeInt(this.zzob);
      this.zzoc.writeToParcel(paramParcel, paramInt);
      int j = this.zzod.size();
      paramParcel.writeInt(j);
      i = 0;
      while (i < j)
      {
        ((ParticipantEntity)this.zzod.get(i)).writeToParcel(paramParcel, paramInt);
        i += 1;
      }
    }
  }
  
  static final class zza
    extends zza
  {
    public final InvitationEntity zzd(Parcel paramParcel)
    {
      if ((InvitationEntity.zza(InvitationEntity.zze())) || (InvitationEntity.zza(InvitationEntity.class.getCanonicalName()))) {
        return super.zzd(paramParcel);
      }
      GameEntity localGameEntity = (GameEntity)GameEntity.CREATOR.createFromParcel(paramParcel);
      String str = paramParcel.readString();
      long l = paramParcel.readLong();
      int j = paramParcel.readInt();
      ParticipantEntity localParticipantEntity = (ParticipantEntity)ParticipantEntity.CREATOR.createFromParcel(paramParcel);
      int k = paramParcel.readInt();
      ArrayList localArrayList = new ArrayList(k);
      int i = 0;
      while (i < k)
      {
        localArrayList.add((ParticipantEntity)ParticipantEntity.CREATOR.createFromParcel(paramParcel));
        i += 1;
      }
      return new InvitationEntity(localGameEntity, str, l, j, localParticipantEntity, localArrayList, -1, 0);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\InvitationEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */