package com.google.android.gms.games.multiplayer.realtime;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public final class zzg
  implements zzh
{
  private final RoomUpdateCallback zzou;
  private final RoomStatusUpdateCallback zzov;
  private final OnRealTimeMessageReceivedListener zzpf;
  
  public zzg(@NonNull RoomUpdateCallback paramRoomUpdateCallback, @Nullable RoomStatusUpdateCallback paramRoomStatusUpdateCallback, @Nullable OnRealTimeMessageReceivedListener paramOnRealTimeMessageReceivedListener)
  {
    this.zzou = paramRoomUpdateCallback;
    this.zzov = paramRoomStatusUpdateCallback;
    this.zzpf = paramOnRealTimeMessageReceivedListener;
  }
  
  public final void onConnectedToRoom(@Nullable Room paramRoom)
  {
    if (this.zzov != null) {
      this.zzov.onConnectedToRoom(paramRoom);
    }
  }
  
  public final void onDisconnectedFromRoom(@Nullable Room paramRoom)
  {
    if (this.zzov != null) {
      this.zzov.onDisconnectedFromRoom(paramRoom);
    }
  }
  
  public final void onJoinedRoom(int paramInt, @Nullable Room paramRoom)
  {
    if (this.zzou != null) {
      this.zzou.onJoinedRoom(paramInt, paramRoom);
    }
  }
  
  public final void onLeftRoom(int paramInt, @NonNull String paramString)
  {
    if (this.zzou != null) {
      this.zzou.onLeftRoom(paramInt, paramString);
    }
  }
  
  public final void onP2PConnected(@NonNull String paramString)
  {
    if (this.zzov != null) {
      this.zzov.onP2PConnected(paramString);
    }
  }
  
  public final void onP2PDisconnected(@NonNull String paramString)
  {
    if (this.zzov != null) {
      this.zzov.onP2PDisconnected(paramString);
    }
  }
  
  public final void onPeerDeclined(@Nullable Room paramRoom, @NonNull List<String> paramList)
  {
    if (this.zzov != null) {
      this.zzov.onPeerDeclined(paramRoom, paramList);
    }
  }
  
  public final void onPeerInvitedToRoom(@Nullable Room paramRoom, @NonNull List<String> paramList)
  {
    if (this.zzov != null) {
      this.zzov.onPeerInvitedToRoom(paramRoom, paramList);
    }
  }
  
  public final void onPeerJoined(@Nullable Room paramRoom, @NonNull List<String> paramList)
  {
    if (this.zzov != null) {
      this.zzov.onPeerJoined(paramRoom, paramList);
    }
  }
  
  public final void onPeerLeft(@Nullable Room paramRoom, @NonNull List<String> paramList)
  {
    if (this.zzov != null) {
      this.zzov.onPeerLeft(paramRoom, paramList);
    }
  }
  
  public final void onPeersConnected(@Nullable Room paramRoom, @NonNull List<String> paramList)
  {
    if (this.zzov != null) {
      this.zzov.onPeersConnected(paramRoom, paramList);
    }
  }
  
  public final void onPeersDisconnected(@Nullable Room paramRoom, @NonNull List<String> paramList)
  {
    if (this.zzov != null) {
      this.zzov.onPeersDisconnected(paramRoom, paramList);
    }
  }
  
  public final void onRealTimeMessageReceived(@NonNull RealTimeMessage paramRealTimeMessage)
  {
    if (this.zzpf != null) {
      this.zzpf.onRealTimeMessageReceived(paramRealTimeMessage);
    }
  }
  
  public final void onRoomAutoMatching(@Nullable Room paramRoom)
  {
    if (this.zzov != null) {
      this.zzov.onRoomAutoMatching(paramRoom);
    }
  }
  
  public final void onRoomConnected(int paramInt, @Nullable Room paramRoom)
  {
    if (this.zzou != null) {
      this.zzou.onRoomConnected(paramInt, paramRoom);
    }
  }
  
  public final void onRoomConnecting(@Nullable Room paramRoom)
  {
    if (this.zzov != null) {
      this.zzov.onRoomConnecting(paramRoom);
    }
  }
  
  public final void onRoomCreated(int paramInt, @Nullable Room paramRoom)
  {
    if (this.zzou != null) {
      this.zzou.onRoomCreated(paramInt, paramRoom);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */