package com.google.android.gms.games.multiplayer;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

@SafeParcelable.Class(creator="ParticipantEntityCreator")
@SafeParcelable.Reserved({1000})
@RetainForClient
public final class ParticipantEntity
  extends GamesDowngradeableSafeParcel
  implements Participant
{
  public static final Parcelable.Creator<ParticipantEntity> CREATOR = new zza();
  @SafeParcelable.Field(getter="getStatus", id=5)
  private final int status;
  @SafeParcelable.Field(getter="getIconImageUrl", id=11)
  private final String zzac;
  @SafeParcelable.Field(getter="getHiResImageUrl", id=12)
  private final String zzad;
  @SafeParcelable.Field(getter="getPlayer", id=8)
  private final PlayerEntity zzfh;
  @SafeParcelable.Field(getter="getParticipantId", id=1)
  private final String zzhl;
  @SafeParcelable.Field(getter="getDisplayName", id=2)
  private final String zzn;
  @SafeParcelable.Field(getter="getClientAddress", id=6)
  private final String zzoh;
  @SafeParcelable.Field(getter="isConnectedToRoom", id=7)
  private final boolean zzoi;
  @SafeParcelable.Field(getter="getCapabilities", id=9)
  private final int zzoj;
  @SafeParcelable.Field(getter="getResult", id=10)
  private final ParticipantResult zzok;
  @SafeParcelable.Field(getter="getIconImageUri", id=3)
  private final Uri zzr;
  @SafeParcelable.Field(getter="getHiResImageUri", id=4)
  private final Uri zzs;
  
  public ParticipantEntity(Participant paramParticipant)
  {
    this.zzhl = paramParticipant.getParticipantId();
    this.zzn = paramParticipant.getDisplayName();
    this.zzr = paramParticipant.getIconImageUri();
    this.zzs = paramParticipant.getHiResImageUri();
    this.status = paramParticipant.getStatus();
    this.zzoh = paramParticipant.zzcg();
    this.zzoi = paramParticipant.isConnectedToRoom();
    Object localObject = paramParticipant.getPlayer();
    if (localObject == null) {}
    for (localObject = null;; localObject = new PlayerEntity((Player)localObject))
    {
      this.zzfh = ((PlayerEntity)localObject);
      this.zzoj = paramParticipant.getCapabilities();
      this.zzok = paramParticipant.getResult();
      this.zzac = paramParticipant.getIconImageUrl();
      this.zzad = paramParticipant.getHiResImageUrl();
      return;
    }
  }
  
  @SafeParcelable.Constructor
  ParticipantEntity(@SafeParcelable.Param(id=1) String paramString1, @SafeParcelable.Param(id=2) String paramString2, @SafeParcelable.Param(id=3) Uri paramUri1, @SafeParcelable.Param(id=4) Uri paramUri2, @SafeParcelable.Param(id=5) int paramInt1, @SafeParcelable.Param(id=6) String paramString3, @SafeParcelable.Param(id=7) boolean paramBoolean, @SafeParcelable.Param(id=8) PlayerEntity paramPlayerEntity, @SafeParcelable.Param(id=9) int paramInt2, @SafeParcelable.Param(id=10) ParticipantResult paramParticipantResult, @SafeParcelable.Param(id=11) String paramString4, @SafeParcelable.Param(id=12) String paramString5)
  {
    this.zzhl = paramString1;
    this.zzn = paramString2;
    this.zzr = paramUri1;
    this.zzs = paramUri2;
    this.status = paramInt1;
    this.zzoh = paramString3;
    this.zzoi = paramBoolean;
    this.zzfh = paramPlayerEntity;
    this.zzoj = paramInt2;
    this.zzok = paramParticipantResult;
    this.zzac = paramString4;
    this.zzad = paramString5;
  }
  
  static int zza(Participant paramParticipant)
  {
    return Objects.hashCode(new Object[] { paramParticipant.getPlayer(), Integer.valueOf(paramParticipant.getStatus()), paramParticipant.zzcg(), Boolean.valueOf(paramParticipant.isConnectedToRoom()), paramParticipant.getDisplayName(), paramParticipant.getIconImageUri(), paramParticipant.getHiResImageUri(), Integer.valueOf(paramParticipant.getCapabilities()), paramParticipant.getResult(), paramParticipant.getParticipantId() });
  }
  
  static boolean zza(Participant paramParticipant, Object paramObject)
  {
    if (!(paramObject instanceof Participant)) {}
    do
    {
      return false;
      if (paramParticipant == paramObject) {
        return true;
      }
      paramObject = (Participant)paramObject;
    } while ((!Objects.equal(((Participant)paramObject).getPlayer(), paramParticipant.getPlayer())) || (!Objects.equal(Integer.valueOf(((Participant)paramObject).getStatus()), Integer.valueOf(paramParticipant.getStatus()))) || (!Objects.equal(((Participant)paramObject).zzcg(), paramParticipant.zzcg())) || (!Objects.equal(Boolean.valueOf(((Participant)paramObject).isConnectedToRoom()), Boolean.valueOf(paramParticipant.isConnectedToRoom()))) || (!Objects.equal(((Participant)paramObject).getDisplayName(), paramParticipant.getDisplayName())) || (!Objects.equal(((Participant)paramObject).getIconImageUri(), paramParticipant.getIconImageUri())) || (!Objects.equal(((Participant)paramObject).getHiResImageUri(), paramParticipant.getHiResImageUri())) || (!Objects.equal(Integer.valueOf(((Participant)paramObject).getCapabilities()), Integer.valueOf(paramParticipant.getCapabilities()))) || (!Objects.equal(((Participant)paramObject).getResult(), paramParticipant.getResult())) || (!Objects.equal(((Participant)paramObject).getParticipantId(), paramParticipant.getParticipantId())));
    return true;
  }
  
  static String zzb(Participant paramParticipant)
  {
    return Objects.toStringHelper(paramParticipant).add("ParticipantId", paramParticipant.getParticipantId()).add("Player", paramParticipant.getPlayer()).add("Status", Integer.valueOf(paramParticipant.getStatus())).add("ClientAddress", paramParticipant.zzcg()).add("ConnectedToRoom", Boolean.valueOf(paramParticipant.isConnectedToRoom())).add("DisplayName", paramParticipant.getDisplayName()).add("IconImage", paramParticipant.getIconImageUri()).add("IconImageUrl", paramParticipant.getIconImageUrl()).add("HiResImage", paramParticipant.getHiResImageUri()).add("HiResImageUrl", paramParticipant.getHiResImageUrl()).add("Capabilities", Integer.valueOf(paramParticipant.getCapabilities())).add("Result", paramParticipant.getResult()).toString();
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final Participant freeze()
  {
    return this;
  }
  
  public final int getCapabilities()
  {
    return this.zzoj;
  }
  
  public final String getDisplayName()
  {
    if (this.zzfh == null) {
      return this.zzn;
    }
    return this.zzfh.getDisplayName();
  }
  
  public final void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    if (this.zzfh == null)
    {
      DataUtils.copyStringToBuffer(this.zzn, paramCharArrayBuffer);
      return;
    }
    this.zzfh.getDisplayName(paramCharArrayBuffer);
  }
  
  public final Uri getHiResImageUri()
  {
    if (this.zzfh == null) {
      return this.zzs;
    }
    return this.zzfh.getHiResImageUri();
  }
  
  public final String getHiResImageUrl()
  {
    if (this.zzfh == null) {
      return this.zzad;
    }
    return this.zzfh.getHiResImageUrl();
  }
  
  public final Uri getIconImageUri()
  {
    if (this.zzfh == null) {
      return this.zzr;
    }
    return this.zzfh.getIconImageUri();
  }
  
  public final String getIconImageUrl()
  {
    if (this.zzfh == null) {
      return this.zzac;
    }
    return this.zzfh.getIconImageUrl();
  }
  
  public final String getParticipantId()
  {
    return this.zzhl;
  }
  
  public final Player getPlayer()
  {
    return this.zzfh;
  }
  
  public final ParticipantResult getResult()
  {
    return this.zzok;
  }
  
  public final int getStatus()
  {
    return this.status;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isConnectedToRoom()
  {
    return this.zzoi;
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final void setShouldDowngrade(boolean paramBoolean)
  {
    super.setShouldDowngrade(paramBoolean);
    if (this.zzfh != null) {
      this.zzfh.setShouldDowngrade(paramBoolean);
    }
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject2 = null;
    int j = 0;
    if (!shouldDowngrade())
    {
      i = SafeParcelWriter.beginObjectHeader(paramParcel);
      SafeParcelWriter.writeString(paramParcel, 1, getParticipantId(), false);
      SafeParcelWriter.writeString(paramParcel, 2, getDisplayName(), false);
      SafeParcelWriter.writeParcelable(paramParcel, 3, getIconImageUri(), paramInt, false);
      SafeParcelWriter.writeParcelable(paramParcel, 4, getHiResImageUri(), paramInt, false);
      SafeParcelWriter.writeInt(paramParcel, 5, getStatus());
      SafeParcelWriter.writeString(paramParcel, 6, this.zzoh, false);
      SafeParcelWriter.writeBoolean(paramParcel, 7, isConnectedToRoom());
      SafeParcelWriter.writeParcelable(paramParcel, 8, getPlayer(), paramInt, false);
      SafeParcelWriter.writeInt(paramParcel, 9, this.zzoj);
      SafeParcelWriter.writeParcelable(paramParcel, 10, getResult(), paramInt, false);
      SafeParcelWriter.writeString(paramParcel, 11, getIconImageUrl(), false);
      SafeParcelWriter.writeString(paramParcel, 12, getHiResImageUrl(), false);
      SafeParcelWriter.finishObjectHeader(paramParcel, i);
      return;
    }
    paramParcel.writeString(this.zzhl);
    paramParcel.writeString(this.zzn);
    Object localObject1;
    if (this.zzr == null)
    {
      localObject1 = null;
      label178:
      paramParcel.writeString((String)localObject1);
      if (this.zzs != null) {
        break label275;
      }
      localObject1 = localObject2;
      label195:
      paramParcel.writeString((String)localObject1);
      paramParcel.writeInt(this.status);
      paramParcel.writeString(this.zzoh);
      if (!this.zzoi) {
        break label287;
      }
      i = 1;
      label226:
      paramParcel.writeInt(i);
      if (this.zzfh != null) {
        break label292;
      }
    }
    label275:
    label287:
    label292:
    for (int i = j;; i = 1)
    {
      paramParcel.writeInt(i);
      if (this.zzfh == null) {
        break;
      }
      this.zzfh.writeToParcel(paramParcel, paramInt);
      return;
      localObject1 = this.zzr.toString();
      break label178;
      localObject1 = this.zzs.toString();
      break label195;
      i = 0;
      break label226;
    }
  }
  
  public final String zzcg()
  {
    return this.zzoh;
  }
  
  static final class zza
    extends zzc
  {
    public final ParticipantEntity zze(Parcel paramParcel)
    {
      int i = 1;
      if ((ParticipantEntity.zza(ParticipantEntity.zze())) || (ParticipantEntity.zza(ParticipantEntity.class.getCanonicalName()))) {
        return super.zze(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2;
      label68:
      int j;
      String str3;
      boolean bool;
      if (localObject1 == null)
      {
        localObject1 = null;
        localObject2 = paramParcel.readString();
        if (localObject2 != null) {
          break label150;
        }
        localObject2 = null;
        j = paramParcel.readInt();
        str3 = paramParcel.readString();
        if (paramParcel.readInt() <= 0) {
          break label160;
        }
        bool = true;
        label89:
        if (paramParcel.readInt() <= 0) {
          break label166;
        }
        label96:
        if (i == 0) {
          break label171;
        }
      }
      label150:
      label160:
      label166:
      label171:
      for (paramParcel = (PlayerEntity)PlayerEntity.CREATOR.createFromParcel(paramParcel);; paramParcel = null)
      {
        return new ParticipantEntity(str1, str2, (Uri)localObject1, (Uri)localObject2, j, str3, bool, paramParcel, 7, null, null, null);
        localObject1 = Uri.parse((String)localObject1);
        break;
        localObject2 = Uri.parse((String)localObject2);
        break label68;
        bool = false;
        break label89;
        i = 0;
        break label96;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\ParticipantEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */