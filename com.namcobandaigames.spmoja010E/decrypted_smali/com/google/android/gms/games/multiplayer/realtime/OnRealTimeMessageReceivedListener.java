package com.google.android.gms.games.multiplayer.realtime;

import android.support.annotation.NonNull;

public abstract interface OnRealTimeMessageReceivedListener
  extends RealTimeMessageReceivedListener
{
  public abstract void onRealTimeMessageReceived(@NonNull RealTimeMessage paramRealTimeMessage);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\OnRealTimeMessageReceivedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */