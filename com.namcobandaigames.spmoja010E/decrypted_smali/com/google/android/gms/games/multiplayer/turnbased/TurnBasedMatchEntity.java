package com.google.android.gms.games.multiplayer.turnbased;

import android.database.CharArrayBuffer;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.internal.zzd;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;

@SafeParcelable.Class(creator="TurnBasedMatchEntityCreator")
@SafeParcelable.Reserved({1000})
public final class TurnBasedMatchEntity
  extends zzd
  implements TurnBasedMatch
{
  public static final Parcelable.Creator<TurnBasedMatchEntity> CREATOR = new zzc();
  @SafeParcelable.Field(getter="getData", id=12)
  private final byte[] data;
  @SafeParcelable.Field(getter="getDescription", id=20)
  private final String description;
  @SafeParcelable.Field(getter="getVersion", id=11)
  private final int version;
  @SafeParcelable.Field(getter="getLastUpdatedTimestamp", id=6)
  private final long zzfk;
  @SafeParcelable.Field(getter="getMatchId", id=2)
  private final String zzhg;
  @SafeParcelable.Field(getter="getGame", id=1)
  private final GameEntity zzky;
  @SafeParcelable.Field(getter="getCreationTimestamp", id=4)
  private final long zzoa;
  @SafeParcelable.Field(getter="getParticipants", id=13)
  private final ArrayList<ParticipantEntity> zzod;
  @SafeParcelable.Field(getter="getVariant", id=10)
  private final int zzoe;
  @Nullable
  @SafeParcelable.Field(getter="getAutoMatchCriteria", id=17)
  private final Bundle zzoz;
  @SafeParcelable.Field(getter="getCreatorId", id=3)
  private final String zzpc;
  @SafeParcelable.Field(getter="getLastUpdaterId", id=5)
  private final String zzpl;
  @SafeParcelable.Field(getter="getPendingParticipantId", id=7)
  private final String zzpm;
  @SafeParcelable.Field(getter="getStatus", id=8)
  private final int zzpn;
  @SafeParcelable.Field(getter="getRematchId", id=14)
  private final String zzpo;
  @SafeParcelable.Field(getter="getPreviousMatchData", id=15)
  private final byte[] zzpp;
  @SafeParcelable.Field(getter="getMatchNumber", id=16)
  private final int zzpq;
  @SafeParcelable.Field(getter="getTurnStatus", id=18)
  private final int zzpr;
  @SafeParcelable.Field(getter="isLocallyModified", id=19)
  private final boolean zzps;
  @SafeParcelable.Field(getter="getDescriptionParticipantId", id=21)
  private final String zzpt;
  
  @SafeParcelable.Constructor
  TurnBasedMatchEntity(@SafeParcelable.Param(id=1) GameEntity paramGameEntity, @SafeParcelable.Param(id=2) String paramString1, @SafeParcelable.Param(id=3) String paramString2, @SafeParcelable.Param(id=4) long paramLong1, @SafeParcelable.Param(id=5) String paramString3, @SafeParcelable.Param(id=6) long paramLong2, @SafeParcelable.Param(id=7) String paramString4, @SafeParcelable.Param(id=8) int paramInt1, @SafeParcelable.Param(id=10) int paramInt2, @SafeParcelable.Param(id=11) int paramInt3, @SafeParcelable.Param(id=12) byte[] paramArrayOfByte1, @SafeParcelable.Param(id=13) ArrayList<ParticipantEntity> paramArrayList, @SafeParcelable.Param(id=14) String paramString5, @SafeParcelable.Param(id=15) byte[] paramArrayOfByte2, @SafeParcelable.Param(id=16) int paramInt4, @Nullable @SafeParcelable.Param(id=17) Bundle paramBundle, @SafeParcelable.Param(id=18) int paramInt5, @SafeParcelable.Param(id=19) boolean paramBoolean, @SafeParcelable.Param(id=20) String paramString6, @SafeParcelable.Param(id=21) String paramString7)
  {
    this.zzky = paramGameEntity;
    this.zzhg = paramString1;
    this.zzpc = paramString2;
    this.zzoa = paramLong1;
    this.zzpl = paramString3;
    this.zzfk = paramLong2;
    this.zzpm = paramString4;
    this.zzpn = paramInt1;
    this.zzpr = paramInt5;
    this.zzoe = paramInt2;
    this.version = paramInt3;
    this.data = paramArrayOfByte1;
    this.zzod = paramArrayList;
    this.zzpo = paramString5;
    this.zzpp = paramArrayOfByte2;
    this.zzpq = paramInt4;
    this.zzoz = paramBundle;
    this.zzps = paramBoolean;
    this.description = paramString6;
    this.zzpt = paramString7;
  }
  
  public TurnBasedMatchEntity(TurnBasedMatch paramTurnBasedMatch)
  {
    this.zzky = new GameEntity(paramTurnBasedMatch.getGame());
    this.zzhg = paramTurnBasedMatch.getMatchId();
    this.zzpc = paramTurnBasedMatch.getCreatorId();
    this.zzoa = paramTurnBasedMatch.getCreationTimestamp();
    this.zzpl = paramTurnBasedMatch.getLastUpdaterId();
    this.zzfk = paramTurnBasedMatch.getLastUpdatedTimestamp();
    this.zzpm = paramTurnBasedMatch.getPendingParticipantId();
    this.zzpn = paramTurnBasedMatch.getStatus();
    this.zzpr = paramTurnBasedMatch.getTurnStatus();
    this.zzoe = paramTurnBasedMatch.getVariant();
    this.version = paramTurnBasedMatch.getVersion();
    this.zzpo = paramTurnBasedMatch.getRematchId();
    this.zzpq = paramTurnBasedMatch.getMatchNumber();
    this.zzoz = paramTurnBasedMatch.getAutoMatchCriteria();
    this.zzps = paramTurnBasedMatch.isLocallyModified();
    this.description = paramTurnBasedMatch.getDescription();
    this.zzpt = paramTurnBasedMatch.getDescriptionParticipantId();
    byte[] arrayOfByte = paramTurnBasedMatch.getData();
    if (arrayOfByte == null)
    {
      this.data = null;
      arrayOfByte = paramTurnBasedMatch.getPreviousMatchData();
      if (arrayOfByte != null) {
        break label305;
      }
      this.zzpp = null;
    }
    for (;;)
    {
      paramTurnBasedMatch = paramTurnBasedMatch.getParticipants();
      int j = paramTurnBasedMatch.size();
      this.zzod = new ArrayList(j);
      int i = 0;
      while (i < j)
      {
        this.zzod.add((ParticipantEntity)((Participant)paramTurnBasedMatch.get(i)).freeze());
        i += 1;
      }
      this.data = new byte[arrayOfByte.length];
      System.arraycopy(arrayOfByte, 0, this.data, 0, arrayOfByte.length);
      break;
      label305:
      this.zzpp = new byte[arrayOfByte.length];
      System.arraycopy(arrayOfByte, 0, this.zzpp, 0, arrayOfByte.length);
    }
  }
  
  static int zza(TurnBasedMatch paramTurnBasedMatch)
  {
    return Objects.hashCode(new Object[] { paramTurnBasedMatch.getGame(), paramTurnBasedMatch.getMatchId(), paramTurnBasedMatch.getCreatorId(), Long.valueOf(paramTurnBasedMatch.getCreationTimestamp()), paramTurnBasedMatch.getLastUpdaterId(), Long.valueOf(paramTurnBasedMatch.getLastUpdatedTimestamp()), paramTurnBasedMatch.getPendingParticipantId(), Integer.valueOf(paramTurnBasedMatch.getStatus()), Integer.valueOf(paramTurnBasedMatch.getTurnStatus()), paramTurnBasedMatch.getDescription(), Integer.valueOf(paramTurnBasedMatch.getVariant()), Integer.valueOf(paramTurnBasedMatch.getVersion()), paramTurnBasedMatch.getParticipants(), paramTurnBasedMatch.getRematchId(), Integer.valueOf(paramTurnBasedMatch.getMatchNumber()), Integer.valueOf(com.google.android.gms.games.internal.zzc.zza(paramTurnBasedMatch.getAutoMatchCriteria())), Integer.valueOf(paramTurnBasedMatch.getAvailableAutoMatchSlots()), Boolean.valueOf(paramTurnBasedMatch.isLocallyModified()) });
  }
  
  static int zza(TurnBasedMatch paramTurnBasedMatch, String paramString)
  {
    ArrayList localArrayList = paramTurnBasedMatch.getParticipants();
    int j = localArrayList.size();
    int i = 0;
    while (i < j)
    {
      Participant localParticipant = (Participant)localArrayList.get(i);
      if (localParticipant.getParticipantId().equals(paramString)) {
        return localParticipant.getStatus();
      }
      i += 1;
    }
    paramTurnBasedMatch = paramTurnBasedMatch.getMatchId();
    throw new IllegalStateException(String.valueOf(paramString).length() + 29 + String.valueOf(paramTurnBasedMatch).length() + "Participant " + paramString + " is not in match " + paramTurnBasedMatch);
  }
  
  static boolean zza(TurnBasedMatch paramTurnBasedMatch, Object paramObject)
  {
    if (!(paramObject instanceof TurnBasedMatch)) {}
    do
    {
      return false;
      if (paramTurnBasedMatch == paramObject) {
        return true;
      }
      paramObject = (TurnBasedMatch)paramObject;
    } while ((!Objects.equal(((TurnBasedMatch)paramObject).getGame(), paramTurnBasedMatch.getGame())) || (!Objects.equal(((TurnBasedMatch)paramObject).getMatchId(), paramTurnBasedMatch.getMatchId())) || (!Objects.equal(((TurnBasedMatch)paramObject).getCreatorId(), paramTurnBasedMatch.getCreatorId())) || (!Objects.equal(Long.valueOf(((TurnBasedMatch)paramObject).getCreationTimestamp()), Long.valueOf(paramTurnBasedMatch.getCreationTimestamp()))) || (!Objects.equal(((TurnBasedMatch)paramObject).getLastUpdaterId(), paramTurnBasedMatch.getLastUpdaterId())) || (!Objects.equal(Long.valueOf(((TurnBasedMatch)paramObject).getLastUpdatedTimestamp()), Long.valueOf(paramTurnBasedMatch.getLastUpdatedTimestamp()))) || (!Objects.equal(((TurnBasedMatch)paramObject).getPendingParticipantId(), paramTurnBasedMatch.getPendingParticipantId())) || (!Objects.equal(Integer.valueOf(((TurnBasedMatch)paramObject).getStatus()), Integer.valueOf(paramTurnBasedMatch.getStatus()))) || (!Objects.equal(Integer.valueOf(((TurnBasedMatch)paramObject).getTurnStatus()), Integer.valueOf(paramTurnBasedMatch.getTurnStatus()))) || (!Objects.equal(((TurnBasedMatch)paramObject).getDescription(), paramTurnBasedMatch.getDescription())) || (!Objects.equal(Integer.valueOf(((TurnBasedMatch)paramObject).getVariant()), Integer.valueOf(paramTurnBasedMatch.getVariant()))) || (!Objects.equal(Integer.valueOf(((TurnBasedMatch)paramObject).getVersion()), Integer.valueOf(paramTurnBasedMatch.getVersion()))) || (!Objects.equal(((TurnBasedMatch)paramObject).getParticipants(), paramTurnBasedMatch.getParticipants())) || (!Objects.equal(((TurnBasedMatch)paramObject).getRematchId(), paramTurnBasedMatch.getRematchId())) || (!Objects.equal(Integer.valueOf(((TurnBasedMatch)paramObject).getMatchNumber()), Integer.valueOf(paramTurnBasedMatch.getMatchNumber()))) || (!com.google.android.gms.games.internal.zzc.zza(((TurnBasedMatch)paramObject).getAutoMatchCriteria(), paramTurnBasedMatch.getAutoMatchCriteria())) || (!Objects.equal(Integer.valueOf(((TurnBasedMatch)paramObject).getAvailableAutoMatchSlots()), Integer.valueOf(paramTurnBasedMatch.getAvailableAutoMatchSlots()))) || (!Objects.equal(Boolean.valueOf(((TurnBasedMatch)paramObject).isLocallyModified()), Boolean.valueOf(paramTurnBasedMatch.isLocallyModified()))));
    return true;
  }
  
  static String zzb(TurnBasedMatch paramTurnBasedMatch)
  {
    return Objects.toStringHelper(paramTurnBasedMatch).add("Game", paramTurnBasedMatch.getGame()).add("MatchId", paramTurnBasedMatch.getMatchId()).add("CreatorId", paramTurnBasedMatch.getCreatorId()).add("CreationTimestamp", Long.valueOf(paramTurnBasedMatch.getCreationTimestamp())).add("LastUpdaterId", paramTurnBasedMatch.getLastUpdaterId()).add("LastUpdatedTimestamp", Long.valueOf(paramTurnBasedMatch.getLastUpdatedTimestamp())).add("PendingParticipantId", paramTurnBasedMatch.getPendingParticipantId()).add("MatchStatus", Integer.valueOf(paramTurnBasedMatch.getStatus())).add("TurnStatus", Integer.valueOf(paramTurnBasedMatch.getTurnStatus())).add("Description", paramTurnBasedMatch.getDescription()).add("Variant", Integer.valueOf(paramTurnBasedMatch.getVariant())).add("Data", paramTurnBasedMatch.getData()).add("Version", Integer.valueOf(paramTurnBasedMatch.getVersion())).add("Participants", paramTurnBasedMatch.getParticipants()).add("RematchId", paramTurnBasedMatch.getRematchId()).add("PreviousData", paramTurnBasedMatch.getPreviousMatchData()).add("MatchNumber", Integer.valueOf(paramTurnBasedMatch.getMatchNumber())).add("AutoMatchCriteria", paramTurnBasedMatch.getAutoMatchCriteria()).add("AvailableAutoMatchSlots", Integer.valueOf(paramTurnBasedMatch.getAvailableAutoMatchSlots())).add("LocallyModified", Boolean.valueOf(paramTurnBasedMatch.isLocallyModified())).add("DescriptionParticipantId", paramTurnBasedMatch.getDescriptionParticipantId()).toString();
  }
  
  static String zzb(TurnBasedMatch paramTurnBasedMatch, String paramString)
  {
    paramTurnBasedMatch = paramTurnBasedMatch.getParticipants();
    int j = paramTurnBasedMatch.size();
    int i = 0;
    while (i < j)
    {
      Participant localParticipant = (Participant)paramTurnBasedMatch.get(i);
      Player localPlayer = localParticipant.getPlayer();
      if ((localPlayer != null) && (localPlayer.getPlayerId().equals(paramString))) {
        return localParticipant.getParticipantId();
      }
      i += 1;
    }
    return null;
  }
  
  static Participant zzc(TurnBasedMatch paramTurnBasedMatch, String paramString)
  {
    ArrayList localArrayList = paramTurnBasedMatch.getParticipants();
    int j = localArrayList.size();
    int i = 0;
    while (i < j)
    {
      Participant localParticipant = (Participant)localArrayList.get(i);
      if (localParticipant.getParticipantId().equals(paramString)) {
        return localParticipant;
      }
      i += 1;
    }
    paramTurnBasedMatch = paramTurnBasedMatch.getMatchId();
    throw new IllegalStateException(String.valueOf(paramString).length() + 29 + String.valueOf(paramTurnBasedMatch).length() + "Participant " + paramString + " is not in match " + paramTurnBasedMatch);
  }
  
  static ArrayList<String> zzc(TurnBasedMatch paramTurnBasedMatch)
  {
    paramTurnBasedMatch = paramTurnBasedMatch.getParticipants();
    int j = paramTurnBasedMatch.size();
    ArrayList localArrayList = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      localArrayList.add(((Participant)paramTurnBasedMatch.get(i)).getParticipantId());
      i += 1;
    }
    return localArrayList;
  }
  
  public final boolean canRematch()
  {
    return (this.zzpn == 2) && (this.zzpo == null);
  }
  
  public final boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public final TurnBasedMatch freeze()
  {
    return this;
  }
  
  @Nullable
  public final Bundle getAutoMatchCriteria()
  {
    return this.zzoz;
  }
  
  public final int getAvailableAutoMatchSlots()
  {
    if (this.zzoz == null) {
      return 0;
    }
    return this.zzoz.getInt("max_automatch_players");
  }
  
  public final long getCreationTimestamp()
  {
    return this.zzoa;
  }
  
  public final String getCreatorId()
  {
    return this.zzpc;
  }
  
  public final byte[] getData()
  {
    return this.data;
  }
  
  public final String getDescription()
  {
    return this.description;
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    DataUtils.copyStringToBuffer(this.description, paramCharArrayBuffer);
  }
  
  public final Participant getDescriptionParticipant()
  {
    String str = getDescriptionParticipantId();
    if (str == null) {
      return null;
    }
    return getParticipant(str);
  }
  
  public final String getDescriptionParticipantId()
  {
    return this.zzpt;
  }
  
  public final Game getGame()
  {
    return this.zzky;
  }
  
  public final long getLastUpdatedTimestamp()
  {
    return this.zzfk;
  }
  
  public final String getLastUpdaterId()
  {
    return this.zzpl;
  }
  
  public final String getMatchId()
  {
    return this.zzhg;
  }
  
  public final int getMatchNumber()
  {
    return this.zzpq;
  }
  
  public final Participant getParticipant(String paramString)
  {
    return zzc(this, paramString);
  }
  
  public final String getParticipantId(String paramString)
  {
    return zzb(this, paramString);
  }
  
  public final ArrayList<String> getParticipantIds()
  {
    return zzc(this);
  }
  
  public final int getParticipantStatus(String paramString)
  {
    return zza(this, paramString);
  }
  
  public final ArrayList<Participant> getParticipants()
  {
    return new ArrayList(this.zzod);
  }
  
  public final String getPendingParticipantId()
  {
    return this.zzpm;
  }
  
  public final byte[] getPreviousMatchData()
  {
    return this.zzpp;
  }
  
  public final String getRematchId()
  {
    return this.zzpo;
  }
  
  public final int getStatus()
  {
    return this.zzpn;
  }
  
  public final int getTurnStatus()
  {
    return this.zzpr;
  }
  
  public final int getVariant()
  {
    return this.zzoe;
  }
  
  public final int getVersion()
  {
    return this.version;
  }
  
  public final int hashCode()
  {
    return zza(this);
  }
  
  public final boolean isDataValid()
  {
    return true;
  }
  
  public final boolean isLocallyModified()
  {
    return this.zzps;
  }
  
  public final String toString()
  {
    return zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, getGame(), paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 2, getMatchId(), false);
    SafeParcelWriter.writeString(paramParcel, 3, getCreatorId(), false);
    SafeParcelWriter.writeLong(paramParcel, 4, getCreationTimestamp());
    SafeParcelWriter.writeString(paramParcel, 5, getLastUpdaterId(), false);
    SafeParcelWriter.writeLong(paramParcel, 6, getLastUpdatedTimestamp());
    SafeParcelWriter.writeString(paramParcel, 7, getPendingParticipantId(), false);
    SafeParcelWriter.writeInt(paramParcel, 8, getStatus());
    SafeParcelWriter.writeInt(paramParcel, 10, getVariant());
    SafeParcelWriter.writeInt(paramParcel, 11, getVersion());
    SafeParcelWriter.writeByteArray(paramParcel, 12, getData(), false);
    SafeParcelWriter.writeTypedList(paramParcel, 13, getParticipants(), false);
    SafeParcelWriter.writeString(paramParcel, 14, getRematchId(), false);
    SafeParcelWriter.writeByteArray(paramParcel, 15, getPreviousMatchData(), false);
    SafeParcelWriter.writeInt(paramParcel, 16, getMatchNumber());
    SafeParcelWriter.writeBundle(paramParcel, 17, getAutoMatchCriteria(), false);
    SafeParcelWriter.writeInt(paramParcel, 18, getTurnStatus());
    SafeParcelWriter.writeBoolean(paramParcel, 19, isLocallyModified());
    SafeParcelWriter.writeString(paramParcel, 20, getDescription(), false);
    SafeParcelWriter.writeString(paramParcel, 21, getDescriptionParticipantId(), false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\turnbased\TurnBasedMatchEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */