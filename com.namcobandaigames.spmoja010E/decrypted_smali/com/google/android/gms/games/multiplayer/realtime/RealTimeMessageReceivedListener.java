package com.google.android.gms.games.multiplayer.realtime;

@Deprecated
public abstract interface RealTimeMessageReceivedListener
{
  public abstract void onRealTimeMessageReceived(RealTimeMessage paramRealTimeMessage);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\RealTimeMessageReceivedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */