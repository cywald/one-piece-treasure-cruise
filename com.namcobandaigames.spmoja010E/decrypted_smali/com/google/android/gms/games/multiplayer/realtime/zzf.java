package com.google.android.gms.games.multiplayer.realtime;

import android.database.CharArrayBuffer;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantRef;
import java.util.ArrayList;

public final class zzf
  extends DataBufferRef
  implements Room
{
  private final int zzmz;
  
  zzf(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    super(paramDataHolder, paramInt1);
    this.zzmz = paramInt2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    return RoomEntity.zza(this, paramObject);
  }
  
  @Nullable
  public final Bundle getAutoMatchCriteria()
  {
    if (!getBoolean("has_automatch_criteria")) {
      return null;
    }
    return RoomConfig.createAutoMatchCriteria(getInteger("automatch_min_players"), getInteger("automatch_max_players"), getLong("automatch_bit_mask"));
  }
  
  public final int getAutoMatchWaitEstimateSeconds()
  {
    return getInteger("automatch_wait_estimate_sec");
  }
  
  public final long getCreationTimestamp()
  {
    return getLong("creation_timestamp");
  }
  
  public final String getCreatorId()
  {
    return getString("creator_external");
  }
  
  public final String getDescription()
  {
    return getString("description");
  }
  
  public final void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    copyToBuffer("description", paramCharArrayBuffer);
  }
  
  public final Participant getParticipant(String paramString)
  {
    return RoomEntity.zzc(this, paramString);
  }
  
  public final String getParticipantId(String paramString)
  {
    return RoomEntity.zzb(this, paramString);
  }
  
  public final ArrayList<String> getParticipantIds()
  {
    return RoomEntity.zzc(this);
  }
  
  public final int getParticipantStatus(String paramString)
  {
    return RoomEntity.zza(this, paramString);
  }
  
  public final ArrayList<Participant> getParticipants()
  {
    ArrayList localArrayList = new ArrayList(this.zzmz);
    int i = 0;
    while (i < this.zzmz)
    {
      localArrayList.add(new ParticipantRef(this.mDataHolder, this.mDataRow + i));
      i += 1;
    }
    return localArrayList;
  }
  
  public final String getRoomId()
  {
    return getString("external_match_id");
  }
  
  public final int getStatus()
  {
    return getInteger("status");
  }
  
  public final int getVariant()
  {
    return getInteger("variant");
  }
  
  public final int hashCode()
  {
    return RoomEntity.zza(this);
  }
  
  public final String toString()
  {
    return RoomEntity.zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((RoomEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */