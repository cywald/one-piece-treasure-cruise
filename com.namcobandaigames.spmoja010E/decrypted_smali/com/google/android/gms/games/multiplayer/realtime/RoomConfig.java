package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class RoomConfig
{
  public static Builder builder(@NonNull RoomUpdateCallback paramRoomUpdateCallback)
  {
    return new Builder(paramRoomUpdateCallback, null);
  }
  
  @Deprecated
  public static Builder builder(RoomUpdateListener paramRoomUpdateListener)
  {
    return new Builder(paramRoomUpdateListener, null);
  }
  
  public static Bundle createAutoMatchCriteria(int paramInt1, int paramInt2, long paramLong)
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("min_automatch_players", paramInt1);
    localBundle.putInt("max_automatch_players", paramInt2);
    localBundle.putLong("exclusive_bit_mask", paramLong);
    return localBundle;
  }
  
  public abstract Bundle getAutoMatchCriteria();
  
  public abstract String getInvitationId();
  
  public abstract String[] getInvitedPlayerIds();
  
  @Deprecated
  public abstract RealTimeMessageReceivedListener getMessageReceivedListener();
  
  public abstract OnRealTimeMessageReceivedListener getOnMessageReceivedListener();
  
  public abstract RoomStatusUpdateCallback getRoomStatusUpdateCallback();
  
  @Deprecated
  public abstract RoomStatusUpdateListener getRoomStatusUpdateListener();
  
  public abstract RoomUpdateCallback getRoomUpdateCallback();
  
  @Deprecated
  public abstract RoomUpdateListener getRoomUpdateListener();
  
  public abstract int getVariant();
  
  public abstract zzh zzch();
  
  public static final class Builder
  {
    int zzoe = -1;
    @Deprecated
    final RoomUpdateListener zzor;
    @Deprecated
    RoomStatusUpdateListener zzos;
    @Deprecated
    RealTimeMessageReceivedListener zzot;
    final RoomUpdateCallback zzou;
    RoomStatusUpdateCallback zzov;
    OnRealTimeMessageReceivedListener zzow;
    String zzox = null;
    ArrayList<String> zzoy = new ArrayList();
    Bundle zzoz;
    
    private Builder(@NonNull RoomUpdateCallback paramRoomUpdateCallback)
    {
      this.zzou = ((RoomUpdateCallback)Preconditions.checkNotNull(paramRoomUpdateCallback, "Must provide a RoomUpdateCallback"));
      this.zzor = null;
    }
    
    @Deprecated
    private Builder(RoomUpdateListener paramRoomUpdateListener)
    {
      this.zzor = ((RoomUpdateListener)Preconditions.checkNotNull(paramRoomUpdateListener, "Must provide a RoomUpdateListener"));
      this.zzou = null;
    }
    
    public final Builder addPlayersToInvite(@NonNull ArrayList<String> paramArrayList)
    {
      Preconditions.checkNotNull(paramArrayList);
      this.zzoy.addAll(paramArrayList);
      return this;
    }
    
    public final Builder addPlayersToInvite(@NonNull String... paramVarArgs)
    {
      Preconditions.checkNotNull(paramVarArgs);
      this.zzoy.addAll(Arrays.asList(paramVarArgs));
      return this;
    }
    
    public final RoomConfig build()
    {
      return new zzd(this);
    }
    
    public final Builder setAutoMatchCriteria(Bundle paramBundle)
    {
      this.zzoz = paramBundle;
      return this;
    }
    
    public final Builder setInvitationIdToAccept(String paramString)
    {
      Preconditions.checkNotNull(paramString);
      this.zzox = paramString;
      return this;
    }
    
    @Deprecated
    public final Builder setMessageReceivedListener(RealTimeMessageReceivedListener paramRealTimeMessageReceivedListener)
    {
      this.zzot = paramRealTimeMessageReceivedListener;
      return this;
    }
    
    public final Builder setOnMessageReceivedListener(@NonNull OnRealTimeMessageReceivedListener paramOnRealTimeMessageReceivedListener)
    {
      this.zzow = paramOnRealTimeMessageReceivedListener;
      return this;
    }
    
    public final Builder setRoomStatusUpdateCallback(@Nullable RoomStatusUpdateCallback paramRoomStatusUpdateCallback)
    {
      this.zzov = paramRoomStatusUpdateCallback;
      return this;
    }
    
    @Deprecated
    public final Builder setRoomStatusUpdateListener(RoomStatusUpdateListener paramRoomStatusUpdateListener)
    {
      this.zzos = paramRoomStatusUpdateListener;
      return this;
    }
    
    public final Builder setVariant(int paramInt)
    {
      if ((paramInt == -1) || (paramInt > 0)) {}
      for (boolean bool = true;; bool = false)
      {
        Preconditions.checkArgument(bool, "Variant must be a positive integer or Room.ROOM_VARIANT_ANY");
        this.zzoe = paramInt;
        return this;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\realtime\RoomConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */