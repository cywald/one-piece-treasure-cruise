package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import java.util.ArrayList;

public final class zzb
  extends DataBufferRef
  implements Invitation
{
  private final Game zzmy;
  private final ArrayList<Participant> zzod;
  private final ParticipantRef zzog;
  
  zzb(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    super(paramDataHolder, paramInt1);
    this.zzmy = new GameRef(paramDataHolder, paramInt1);
    this.zzod = new ArrayList(paramInt2);
    String str = getString("external_inviter_id");
    paramInt1 = 0;
    paramDataHolder = null;
    while (paramInt1 < paramInt2)
    {
      ParticipantRef localParticipantRef = new ParticipantRef(this.mDataHolder, this.mDataRow + paramInt1);
      if (localParticipantRef.getParticipantId().equals(str)) {
        paramDataHolder = localParticipantRef;
      }
      this.zzod.add(localParticipantRef);
      paramInt1 += 1;
    }
    this.zzog = ((ParticipantRef)Preconditions.checkNotNull(paramDataHolder, "Must have a valid inviter!"));
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    return InvitationEntity.zza(this, paramObject);
  }
  
  public final int getAvailableAutoMatchSlots()
  {
    if (!getBoolean("has_automatch_criteria")) {
      return 0;
    }
    return getInteger("automatch_max_players");
  }
  
  public final long getCreationTimestamp()
  {
    return Math.max(getLong("creation_timestamp"), getLong("last_modified_timestamp"));
  }
  
  public final Game getGame()
  {
    return this.zzmy;
  }
  
  public final String getInvitationId()
  {
    return getString("external_invitation_id");
  }
  
  public final int getInvitationType()
  {
    return getInteger("type");
  }
  
  public final Participant getInviter()
  {
    return this.zzog;
  }
  
  public final ArrayList<Participant> getParticipants()
  {
    return this.zzod;
  }
  
  public final int getVariant()
  {
    return getInteger("variant");
  }
  
  public final int hashCode()
  {
    return InvitationEntity.zza(this);
  }
  
  public final String toString()
  {
    return InvitationEntity.zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((InvitationEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */