package com.google.android.gms.games.multiplayer;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataBufferRef;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class ParticipantRef
  extends DataBufferRef
  implements Participant
{
  private final PlayerRef zzol;
  
  public ParticipantRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
    this.zzol = new PlayerRef(paramDataHolder, paramInt);
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    return ParticipantEntity.zza(this, paramObject);
  }
  
  public final int getCapabilities()
  {
    return getInteger("capabilities");
  }
  
  public final String getDisplayName()
  {
    if (hasNull("external_player_id")) {
      return getString("default_display_name");
    }
    return this.zzol.getDisplayName();
  }
  
  public final void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    if (hasNull("external_player_id"))
    {
      copyToBuffer("default_display_name", paramCharArrayBuffer);
      return;
    }
    this.zzol.getDisplayName(paramCharArrayBuffer);
  }
  
  public final Uri getHiResImageUri()
  {
    if (hasNull("external_player_id")) {
      return parseUri("default_display_hi_res_image_uri");
    }
    return this.zzol.getHiResImageUri();
  }
  
  public final String getHiResImageUrl()
  {
    if (hasNull("external_player_id")) {
      return getString("default_display_hi_res_image_url");
    }
    return this.zzol.getHiResImageUrl();
  }
  
  public final Uri getIconImageUri()
  {
    if (hasNull("external_player_id")) {
      return parseUri("default_display_image_uri");
    }
    return this.zzol.getIconImageUri();
  }
  
  public final String getIconImageUrl()
  {
    if (hasNull("external_player_id")) {
      return getString("default_display_image_url");
    }
    return this.zzol.getIconImageUrl();
  }
  
  public final String getParticipantId()
  {
    return getString("external_participant_id");
  }
  
  public final Player getPlayer()
  {
    if (hasNull("external_player_id")) {
      return null;
    }
    return this.zzol;
  }
  
  public final ParticipantResult getResult()
  {
    if (hasNull("result_type")) {
      return null;
    }
    int i = getInteger("result_type");
    int j = getInteger("placing");
    return new ParticipantResult(getParticipantId(), i, j);
  }
  
  public final int getStatus()
  {
    return getInteger("player_status");
  }
  
  public final int hashCode()
  {
    return ParticipantEntity.zza(this);
  }
  
  public final boolean isConnectedToRoom()
  {
    return getInteger("connected") > 0;
  }
  
  public final String toString()
  {
    return ParticipantEntity.zzb(this);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((ParticipantEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public final String zzcg()
  {
    return getString("client_address");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\multiplayer\ParticipantRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */