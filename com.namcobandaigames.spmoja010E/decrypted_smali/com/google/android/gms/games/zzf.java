package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzf
  extends zzah<Void>
{
  zzf(EventsClient paramEventsClient, String paramString, int paramInt) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException
  {
    paramzze.zza(this.zzk, this.zzl);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */