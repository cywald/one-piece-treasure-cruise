package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzs;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzy
  extends zzs<OnInvitationReceivedListener>
{
  zzy(InvitationsClient paramInvitationsClient, ListenerHolder paramListenerHolder1, ListenerHolder paramListenerHolder2)
  {
    super(paramListenerHolder1);
  }
  
  protected final void zzb(zze paramzze, TaskCompletionSource<Void> paramTaskCompletionSource)
    throws RemoteException, SecurityException
  {
    paramzze.zza(this.zzbi);
    paramTaskCompletionSource.setResult(null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */