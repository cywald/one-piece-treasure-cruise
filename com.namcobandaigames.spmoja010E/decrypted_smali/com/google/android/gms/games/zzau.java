package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.zzah;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzau
  extends zzah<Player>
{
  zzau(PlayersClient paramPlayersClient) {}
  
  protected final void zza(zze paramzze, TaskCompletionSource<Player> paramTaskCompletionSource)
    throws RemoteException
  {
    paramTaskCompletionSource.setResult(paramzze.zzr());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\games\zzau.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */