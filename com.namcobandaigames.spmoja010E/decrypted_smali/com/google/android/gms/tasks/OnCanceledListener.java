package com.google.android.gms.tasks;

public abstract interface OnCanceledListener
{
  public abstract void onCanceled();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tasks\OnCanceledListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */