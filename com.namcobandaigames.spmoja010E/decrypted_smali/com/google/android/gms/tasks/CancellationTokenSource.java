package com.google.android.gms.tasks;

public class CancellationTokenSource
{
  private final zza zzc = new zza();
  
  public void cancel()
  {
    this.zzc.cancel();
  }
  
  public CancellationToken getToken()
  {
    return this.zzc;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tasks\CancellationTokenSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */