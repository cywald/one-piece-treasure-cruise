package com.google.android.gms.tasks;

final class zzs
  implements OnTokenCanceledListener
{
  zzs(TaskCompletionSource paramTaskCompletionSource) {}
  
  public final void onCanceled()
  {
    TaskCompletionSource.zza(this.zzv).zza();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tasks\zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */