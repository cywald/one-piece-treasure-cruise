package com.google.android.gms.tasks;

public abstract interface OnSuccessListener<TResult>
{
  public abstract void onSuccess(TResult paramTResult);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tasks\OnSuccessListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */