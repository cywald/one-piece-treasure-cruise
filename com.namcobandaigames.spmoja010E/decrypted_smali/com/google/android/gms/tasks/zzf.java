package com.google.android.gms.tasks;

final class zzf
  implements Runnable
{
  zzf(zze paramzze, Task paramTask) {}
  
  public final void run()
  {
    try
    {
      Task localTask = (Task)zze.zza(this.zzi).then(this.zzg);
      if (localTask == null)
      {
        this.zzi.onFailure(new NullPointerException("Continuation returned null"));
        return;
      }
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      if ((localRuntimeExecutionException.getCause() instanceof Exception))
      {
        zze.zzb(this.zzi).setException((Exception)localRuntimeExecutionException.getCause());
        return;
      }
      zze.zzb(this.zzi).setException(localRuntimeExecutionException);
      return;
    }
    catch (Exception localException)
    {
      zze.zzb(this.zzi).setException(localException);
      return;
    }
    localException.addOnSuccessListener(TaskExecutors.zzw, this.zzi);
    localException.addOnFailureListener(TaskExecutors.zzw, this.zzi);
    localException.addOnCanceledListener(TaskExecutors.zzw, this.zzi);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tasks\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */