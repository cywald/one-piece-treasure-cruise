package com.google.android.gms.tasks;

import android.support.annotation.NonNull;

abstract interface zzq<TResult>
{
  public abstract void cancel();
  
  public abstract void onComplete(@NonNull Task<TResult> paramTask);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tasks\zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */