package com.google.android.gms.tasks;

import android.support.annotation.NonNull;

public abstract interface OnFailureListener
{
  public abstract void onFailure(@NonNull Exception paramException);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tasks\OnFailureListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */