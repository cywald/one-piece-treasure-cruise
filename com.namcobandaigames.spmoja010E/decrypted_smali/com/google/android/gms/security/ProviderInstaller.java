package com.google.android.gms.security;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.internal.Preconditions;
import java.lang.reflect.Method;

public class ProviderInstaller
{
  public static final String PROVIDER_NAME = "GmsCore_OpenSSL";
  private static final Object lock = new Object();
  private static final GoogleApiAvailabilityLight zziu = ;
  private static Method zziv = null;
  
  public static void installIfNeeded(Context paramContext)
    throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException
  {
    Preconditions.checkNotNull(paramContext, "Context must not be null");
    zziu.verifyGooglePlayServicesIsAvailable(paramContext, 11925000);
    try
    {
      paramContext = GooglePlayServicesUtilLight.getRemoteContext(paramContext);
      if (paramContext == null)
      {
        if (Log.isLoggable("ProviderInstaller", 6)) {
          Log.e("ProviderInstaller", "Failed to get remote context");
        }
        throw new GooglePlayServicesNotAvailableException(8);
      }
    }
    catch (Resources.NotFoundException paramContext)
    {
      if (Log.isLoggable("ProviderInstaller", 6)) {
        Log.e("ProviderInstaller", "Failed to get remote context - resource not found");
      }
      throw new GooglePlayServicesNotAvailableException(8);
    }
    for (;;)
    {
      Throwable localThrowable;
      synchronized (lock)
      {
        try
        {
          if (zziv == null) {
            zziv = paramContext.getClassLoader().loadClass("com.google.android.gms.common.security.ProviderInstallerImpl").getMethod("insertProvider", new Class[] { Context.class });
          }
          zziv.invoke(null, new Object[] { paramContext });
          return;
        }
        catch (Exception paramContext)
        {
          localThrowable = paramContext.getCause();
          if (!Log.isLoggable("ProviderInstaller", 6)) {
            continue;
          }
        }
        if (localThrowable == null)
        {
          paramContext = paramContext.getMessage();
          paramContext = String.valueOf(paramContext);
          if (paramContext.length() == 0) {
            break label213;
          }
          paramContext = "Failed to install provider: ".concat(paramContext);
          Log.e("ProviderInstaller", paramContext);
          throw new GooglePlayServicesNotAvailableException(8);
        }
      }
      paramContext = localThrowable.getMessage();
      continue;
      label213:
      paramContext = new String("Failed to install provider: ");
    }
  }
  
  public static void installIfNeededAsync(Context paramContext, ProviderInstallListener paramProviderInstallListener)
  {
    Preconditions.checkNotNull(paramContext, "Context must not be null");
    Preconditions.checkNotNull(paramProviderInstallListener, "Listener must not be null");
    Preconditions.checkMainThread("Must be called on the UI thread");
    new zza(paramContext, paramProviderInstallListener).execute(new Void[0]);
  }
  
  public static abstract interface ProviderInstallListener
  {
    public abstract void onProviderInstallFailed(int paramInt, Intent paramIntent);
    
    public abstract void onProviderInstalled();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\security\ProviderInstaller.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */