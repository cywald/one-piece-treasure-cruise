package com.google.android.gms.actions;

public class SearchIntents
{
  public static final String ACTION_SEARCH = "com.google.android.gms.actions.SEARCH_ACTION";
  public static final String EXTRA_QUERY = "query";
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\actions\SearchIntents.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */