package com.google.android.gms.auth.api.phone;

import com.google.android.gms.tasks.Task;

public abstract interface SmsRetrieverApi
{
  public abstract Task<Void> startSmsRetriever();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\phone\SmsRetrieverApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */