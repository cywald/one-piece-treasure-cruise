package com.google.android.gms.auth.api.signin.internal;

import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

public class zzc
  extends zzt
{
  public void zzc(GoogleSignInAccount paramGoogleSignInAccount, Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void zze(Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void zzf(Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\signin\internal\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */