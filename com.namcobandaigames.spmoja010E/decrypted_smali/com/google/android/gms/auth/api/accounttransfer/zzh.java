package com.google.android.gms.auth.api.accounttransfer;

final class zzh
  extends AccountTransferClient.zza<DeviceMetaData>
{
  zzh(zzg paramzzg, AccountTransferClient.zzb paramzzb)
  {
    super(paramzzb);
  }
  
  public final void zza(DeviceMetaData paramDeviceMetaData)
  {
    this.zzas.setResult(paramDeviceMetaData);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\accounttransfer\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */