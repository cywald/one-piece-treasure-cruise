package com.google.android.gms.auth.api.accounttransfer;

import android.os.RemoteException;
import com.google.android.gms.internal.auth.zzaf;
import com.google.android.gms.internal.auth.zzz;

final class zzd
  extends AccountTransferClient.zzc
{
  zzd(AccountTransferClient paramAccountTransferClient, zzaf paramzzaf)
  {
    super(null);
  }
  
  protected final void zza(zzz paramzzz)
    throws RemoteException
  {
    paramzzz.zza(this.zzax, this.zzao);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\accounttransfer\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */