package com.google.android.gms.auth.api;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;

@KeepForSdk
public final class AuthProxyOptions
  implements Api.ApiOptions.Optional
{}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\AuthProxyOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */