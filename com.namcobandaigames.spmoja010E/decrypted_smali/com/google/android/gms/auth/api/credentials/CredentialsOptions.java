package com.google.android.gms.auth.api.credentials;

import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions;
import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions.Builder;

public final class CredentialsOptions
  extends Auth.AuthCredentialsOptions
{
  public static final CredentialsOptions DEFAULT = (CredentialsOptions)new Builder().zzc();
  
  private CredentialsOptions(Builder paramBuilder)
  {
    super(paramBuilder);
  }
  
  public static final class Builder
    extends Auth.AuthCredentialsOptions.Builder
  {
    public final CredentialsOptions build()
    {
      return new CredentialsOptions(this, null);
    }
    
    public final Builder forceEnableSaveDialog()
    {
      this.zzn = Boolean.valueOf(true);
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\credentials\CredentialsOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */