package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.SignInConnectionListener;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public final class zze
  extends AsyncTaskLoader<Void>
  implements SignInConnectionListener
{
  private Semaphore zzbg = new Semaphore(0);
  private Set<GoogleApiClient> zzbh;
  
  public zze(Context paramContext, Set<GoogleApiClient> paramSet)
  {
    super(paramContext);
    this.zzbh = paramSet;
  }
  
  private final Void zzf()
  {
    Iterator localIterator = this.zzbh.iterator();
    int i = 0;
    if (localIterator.hasNext())
    {
      if (!((GoogleApiClient)localIterator.next()).maybeSignIn(this)) {
        break label80;
      }
      i += 1;
    }
    label80:
    for (;;)
    {
      break;
      try
      {
        this.zzbg.tryAcquire(i, 5L, TimeUnit.SECONDS);
        return null;
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;)
        {
          Log.i("GACSignInLoader", "Unexpected InterruptedException", localInterruptedException);
          Thread.currentThread().interrupt();
        }
      }
    }
  }
  
  public final void onComplete()
  {
    this.zzbg.release();
  }
  
  protected final void onStartLoading()
  {
    this.zzbg.drainPermits();
    forceLoad();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\signin\internal\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */