package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.util.VisibleForTesting;

public class HashAccumulator
{
  @VisibleForTesting
  private static int zaah = 31;
  private int zaai = 1;
  
  @KeepForSdk
  public HashAccumulator addObject(Object paramObject)
  {
    int j = zaah;
    int k = this.zaai;
    if (paramObject == null) {}
    for (int i = 0;; i = paramObject.hashCode())
    {
      this.zaai = (i + k * j);
      return this;
    }
  }
  
  @KeepForSdk
  public int hash()
  {
    return this.zaai;
  }
  
  public final HashAccumulator zaa(boolean paramBoolean)
  {
    int j = zaah;
    int k = this.zaai;
    if (paramBoolean) {}
    for (int i = 1;; i = 0)
    {
      this.zaai = (i + k * j);
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\signin\internal\HashAccumulator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */