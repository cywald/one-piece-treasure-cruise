package com.google.android.gms.auth.api;

public final class R
{
  public static final class attr
  {
    public static final int buttonSize = 2130837567;
    public static final int circleCrop = 2130837574;
    public static final int colorScheme = 2130837589;
    public static final int imageAspectRatio = 2130837638;
    public static final int imageAspectRatioAdjust = 2130837639;
    public static final int scopeUris = 2130837686;
  }
  
  public static final class color
  {
    public static final int common_google_signin_btn_text_dark = 2130968614;
    public static final int common_google_signin_btn_text_dark_default = 2130968615;
    public static final int common_google_signin_btn_text_dark_disabled = 2130968616;
    public static final int common_google_signin_btn_text_dark_focused = 2130968617;
    public static final int common_google_signin_btn_text_dark_pressed = 2130968618;
    public static final int common_google_signin_btn_text_light = 2130968619;
    public static final int common_google_signin_btn_text_light_default = 2130968620;
    public static final int common_google_signin_btn_text_light_disabled = 2130968621;
    public static final int common_google_signin_btn_text_light_focused = 2130968622;
    public static final int common_google_signin_btn_text_light_pressed = 2130968623;
    public static final int common_google_signin_btn_tint = 2130968624;
  }
  
  public static final class drawable
  {
    public static final int common_full_open_on_phone = 2131099731;
    public static final int common_google_signin_btn_icon_dark = 2131099732;
    public static final int common_google_signin_btn_icon_dark_focused = 2131099733;
    public static final int common_google_signin_btn_icon_dark_normal = 2131099734;
    public static final int common_google_signin_btn_icon_dark_normal_background = 2131099735;
    public static final int common_google_signin_btn_icon_disabled = 2131099736;
    public static final int common_google_signin_btn_icon_light = 2131099737;
    public static final int common_google_signin_btn_icon_light_focused = 2131099738;
    public static final int common_google_signin_btn_icon_light_normal = 2131099739;
    public static final int common_google_signin_btn_icon_light_normal_background = 2131099740;
    public static final int common_google_signin_btn_text_dark = 2131099741;
    public static final int common_google_signin_btn_text_dark_focused = 2131099742;
    public static final int common_google_signin_btn_text_dark_normal = 2131099743;
    public static final int common_google_signin_btn_text_dark_normal_background = 2131099744;
    public static final int common_google_signin_btn_text_disabled = 2131099745;
    public static final int common_google_signin_btn_text_light = 2131099746;
    public static final int common_google_signin_btn_text_light_focused = 2131099747;
    public static final int common_google_signin_btn_text_light_normal = 2131099748;
    public static final int common_google_signin_btn_text_light_normal_background = 2131099749;
    public static final int googleg_disabled_color_18 = 2131099750;
    public static final int googleg_standard_color_18 = 2131099751;
  }
  
  public static final class id
  {
    public static final int adjust_height = 2131165211;
    public static final int adjust_width = 2131165212;
    public static final int auto = 2131165216;
    public static final int dark = 2131165228;
    public static final int icon_only = 2131165242;
    public static final int light = 2131165247;
    public static final int none = 2131165257;
    public static final int standard = 2131165292;
    public static final int wide = 2131165310;
  }
  
  public static final class integer
  {
    public static final int google_play_services_version = 2131230724;
  }
  
  public static final class string
  {
    public static final int common_google_play_services_enable_button = 2131361825;
    public static final int common_google_play_services_enable_text = 2131361826;
    public static final int common_google_play_services_enable_title = 2131361827;
    public static final int common_google_play_services_install_button = 2131361828;
    public static final int common_google_play_services_install_text = 2131361829;
    public static final int common_google_play_services_install_title = 2131361830;
    public static final int common_google_play_services_notification_channel_name = 2131361831;
    public static final int common_google_play_services_notification_ticker = 2131361832;
    public static final int common_google_play_services_unknown_issue = 2131361833;
    public static final int common_google_play_services_unsupported_text = 2131361834;
    public static final int common_google_play_services_update_button = 2131361835;
    public static final int common_google_play_services_update_text = 2131361836;
    public static final int common_google_play_services_update_title = 2131361837;
    public static final int common_google_play_services_updating_text = 2131361838;
    public static final int common_google_play_services_wear_update_text = 2131361839;
    public static final int common_open_on_phone = 2131361840;
    public static final int common_signin_button_text = 2131361841;
    public static final int common_signin_button_text_long = 2131361842;
  }
  
  public static final class styleable
  {
    public static final int[] LoadingImageView = { 2130837574, 2130837638, 2130837639 };
    public static final int LoadingImageView_circleCrop = 0;
    public static final int LoadingImageView_imageAspectRatio = 1;
    public static final int LoadingImageView_imageAspectRatioAdjust = 2;
    public static final int[] SignInButton = { 2130837567, 2130837589, 2130837686 };
    public static final int SignInButton_buttonSize = 0;
    public static final int SignInButton_colorScheme = 1;
    public static final int SignInButton_scopeUris = 2;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */