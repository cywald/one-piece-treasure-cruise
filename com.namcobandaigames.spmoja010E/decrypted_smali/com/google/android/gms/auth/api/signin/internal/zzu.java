package com.google.android.gms.auth.api.signin.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

public abstract interface zzu
  extends IInterface
{
  public abstract void zzc(zzs paramzzs, GoogleSignInOptions paramGoogleSignInOptions)
    throws RemoteException;
  
  public abstract void zzd(zzs paramzzs, GoogleSignInOptions paramGoogleSignInOptions)
    throws RemoteException;
  
  public abstract void zze(zzs paramzzs, GoogleSignInOptions paramGoogleSignInOptions)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\signin\internal\zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */