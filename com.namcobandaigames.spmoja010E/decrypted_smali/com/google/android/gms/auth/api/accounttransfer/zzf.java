package com.google.android.gms.auth.api.accounttransfer;

final class zzf
  extends AccountTransferClient.zza<byte[]>
{
  zzf(zze paramzze, AccountTransferClient.zzb paramzzb)
  {
    super(paramzzb);
  }
  
  public final void zza(byte[] paramArrayOfByte)
  {
    this.zzaq.setResult(paramArrayOfByte);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\accounttransfer\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */