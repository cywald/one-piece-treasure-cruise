package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.StatusPendingResult;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.logging.Logger;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public final class zzd
  implements Runnable
{
  private static final Logger zzbd = new Logger("RevokeAccessOperation", new String[0]);
  private final String zzbe;
  private final StatusPendingResult zzbf;
  
  private zzd(String paramString)
  {
    Preconditions.checkNotEmpty(paramString);
    this.zzbe = paramString;
    this.zzbf = new StatusPendingResult(null);
  }
  
  public static PendingResult<Status> zzc(String paramString)
  {
    if (paramString == null) {
      return PendingResults.immediateFailedResult(new Status(4), null);
    }
    paramString = new zzd(paramString);
    new Thread(paramString).start();
    return paramString.zzbf;
  }
  
  public final void run()
  {
    Object localObject1 = Status.RESULT_INTERNAL_ERROR;
    for (;;)
    {
      try
      {
        localObject2 = String.valueOf("https://accounts.google.com/o/oauth2/revoke?token=");
        localObject3 = String.valueOf(this.zzbe);
        if (((String)localObject3).length() != 0)
        {
          localObject2 = ((String)localObject2).concat((String)localObject3);
          localObject2 = (HttpURLConnection)new URL((String)localObject2).openConnection();
          ((HttpURLConnection)localObject2).setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
          i = ((HttpURLConnection)localObject2).getResponseCode();
          if (i != 200) {
            continue;
          }
          localObject2 = Status.RESULT_SUCCESS;
          localObject1 = localObject2;
        }
      }
      catch (IOException localIOException1)
      {
        Object localObject2;
        int i;
        localObject3 = zzbd;
        String str1 = String.valueOf(localIOException1.toString());
        if (str1.length() != 0)
        {
          str1 = "IOException when revoking access: ".concat(str1);
          ((Logger)localObject3).e(str1, new Object[0]);
          continue;
          zzbd.e("Unable to revoke access!", new Object[0]);
          continue;
        }
        str1 = new String("IOException when revoking access: ");
        continue;
      }
      catch (Exception localException1)
      {
        Object localObject3 = zzbd;
        String str2 = String.valueOf(localException1.toString());
        if (str2.length() != 0)
        {
          str2 = "Exception when revoking access: ".concat(str2);
          ((Logger)localObject3).e(str2, new Object[0]);
          continue;
        }
        str2 = new String("Exception when revoking access: ");
        continue;
      }
      try
      {
        zzbd.d(26 + "Response Code: " + i, new Object[0]);
        this.zzbf.setResult((Result)localObject1);
        return;
      }
      catch (Exception localException2)
      {
        continue;
      }
      catch (IOException localIOException2)
      {
        continue;
      }
      localObject2 = new String((String)localObject2);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\signin\internal\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */