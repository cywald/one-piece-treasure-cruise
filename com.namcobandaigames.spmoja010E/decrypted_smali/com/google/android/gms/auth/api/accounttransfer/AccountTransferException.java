package com.google.android.gms.auth.api.accounttransfer;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;

public class AccountTransferException
  extends ApiException
{
  public AccountTransferException(@NonNull Status paramStatus)
  {
    super(paramStatus);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\accounttransfer\AccountTransferException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */