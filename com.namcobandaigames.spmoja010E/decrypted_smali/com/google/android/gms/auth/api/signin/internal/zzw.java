package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.os.Binder;
import com.google.android.gms.common.GooglePlayServicesUtil;

public final class zzw
  extends zzr
{
  private final Context mContext;
  
  public zzw(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  private final void zzl()
  {
    if (!GooglePlayServicesUtil.isGooglePlayServicesUid(this.mContext, Binder.getCallingUid()))
    {
      int i = Binder.getCallingUid();
      throw new SecurityException(52 + "Calling UID " + i + " is not Google Play services.");
    }
  }
  
  /* Error */
  public final void zzj()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 55	com/google/android/gms/auth/api/signin/internal/zzw:zzl	()V
    //   4: aload_0
    //   5: getfield 13	com/google/android/gms/auth/api/signin/internal/zzw:mContext	Landroid/content/Context;
    //   8: invokestatic 61	com/google/android/gms/auth/api/signin/internal/Storage:getInstance	(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/internal/Storage;
    //   11: astore_3
    //   12: aload_3
    //   13: invokevirtual 65	com/google/android/gms/auth/api/signin/internal/Storage:getSavedDefaultGoogleSignInAccount	()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
    //   16: astore_2
    //   17: getstatic 71	com/google/android/gms/auth/api/signin/GoogleSignInOptions:DEFAULT_SIGN_IN	Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;
    //   20: astore_1
    //   21: aload_2
    //   22: ifnull +8 -> 30
    //   25: aload_3
    //   26: invokevirtual 75	com/google/android/gms/auth/api/signin/internal/Storage:getSavedDefaultGoogleSignInOptions	()Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;
    //   29: astore_1
    //   30: new 77	com/google/android/gms/common/api/GoogleApiClient$Builder
    //   33: dup
    //   34: aload_0
    //   35: getfield 13	com/google/android/gms/auth/api/signin/internal/zzw:mContext	Landroid/content/Context;
    //   38: invokespecial 79	com/google/android/gms/common/api/GoogleApiClient$Builder:<init>	(Landroid/content/Context;)V
    //   41: getstatic 85	com/google/android/gms/auth/api/Auth:GOOGLE_SIGN_IN_API	Lcom/google/android/gms/common/api/Api;
    //   44: aload_1
    //   45: invokevirtual 89	com/google/android/gms/common/api/GoogleApiClient$Builder:addApi	(Lcom/google/android/gms/common/api/Api;Lcom/google/android/gms/common/api/Api$ApiOptions$HasOptions;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;
    //   48: invokevirtual 93	com/google/android/gms/common/api/GoogleApiClient$Builder:build	()Lcom/google/android/gms/common/api/GoogleApiClient;
    //   51: astore_1
    //   52: aload_1
    //   53: invokevirtual 99	com/google/android/gms/common/api/GoogleApiClient:blockingConnect	()Lcom/google/android/gms/common/ConnectionResult;
    //   56: invokevirtual 105	com/google/android/gms/common/ConnectionResult:isSuccess	()Z
    //   59: ifeq +17 -> 76
    //   62: aload_2
    //   63: ifnull +18 -> 81
    //   66: getstatic 109	com/google/android/gms/auth/api/Auth:GoogleSignInApi	Lcom/google/android/gms/auth/api/signin/GoogleSignInApi;
    //   69: aload_1
    //   70: invokeinterface 115 2 0
    //   75: pop
    //   76: aload_1
    //   77: invokevirtual 118	com/google/android/gms/common/api/GoogleApiClient:disconnect	()V
    //   80: return
    //   81: aload_1
    //   82: invokevirtual 122	com/google/android/gms/common/api/GoogleApiClient:clearDefaultAccountAndReconnect	()Lcom/google/android/gms/common/api/PendingResult;
    //   85: pop
    //   86: goto -10 -> 76
    //   89: astore_2
    //   90: aload_1
    //   91: invokevirtual 118	com/google/android/gms/common/api/GoogleApiClient:disconnect	()V
    //   94: aload_2
    //   95: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	this	zzw
    //   20	71	1	localObject1	Object
    //   16	47	2	localGoogleSignInAccount	com.google.android.gms.auth.api.signin.GoogleSignInAccount
    //   89	6	2	localObject2	Object
    //   11	15	3	localStorage	Storage
    // Exception table:
    //   from	to	target	type
    //   52	62	89	finally
    //   66	76	89	finally
    //   81	86	89	finally
  }
  
  public final void zzk()
  {
    zzl();
    zzp.zzd(this.mContext).clear();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\signin\internal\zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */