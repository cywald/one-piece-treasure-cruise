package com.google.android.gms.auth.api.accounttransfer;

import android.os.RemoteException;
import com.google.android.gms.internal.auth.zzah;
import com.google.android.gms.internal.auth.zzz;

final class zzi
  extends AccountTransferClient.zzc
{
  zzi(AccountTransferClient paramAccountTransferClient, zzah paramzzah)
  {
    super(null);
  }
  
  protected final void zza(zzz paramzzz)
    throws RemoteException
  {
    paramzzz.zza(this.zzax, this.zzat);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\accounttransfer\zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */