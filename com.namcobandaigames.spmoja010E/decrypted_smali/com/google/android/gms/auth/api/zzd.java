package com.google.android.gms.auth.api;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.zzg;
import com.google.android.gms.common.api.Api.AbstractClientBuilder;

final class zzd
  extends Api.AbstractClientBuilder<zzg, GoogleSignInOptions>
{}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */