package com.google.android.gms.auth.api.accounttransfer;

import android.os.RemoteException;
import com.google.android.gms.internal.auth.zzad;
import com.google.android.gms.internal.auth.zzz;

final class zze
  extends AccountTransferClient.zzb<byte[]>
{
  zze(AccountTransferClient paramAccountTransferClient, zzad paramzzad)
  {
    super(null);
  }
  
  protected final void zza(zzz paramzzz)
    throws RemoteException
  {
    paramzzz.zza(new zzf(this, this), this.zzap);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\api\accounttransfer\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */