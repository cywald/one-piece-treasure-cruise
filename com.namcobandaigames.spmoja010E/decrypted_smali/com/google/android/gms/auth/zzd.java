package com.google.android.gms.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.RequiresPermission;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.logging.Logger;
import java.io.IOException;
import java.util.List;

public class zzd
{
  private static final String[] ACCEPTABLE_ACCOUNT_TYPES = { "com.google", "com.google.work", "cn.google" };
  public static final int CHANGE_TYPE_ACCOUNT_ADDED = 1;
  public static final int CHANGE_TYPE_ACCOUNT_REMOVED = 2;
  public static final int CHANGE_TYPE_ACCOUNT_RENAMED_FROM = 3;
  public static final int CHANGE_TYPE_ACCOUNT_RENAMED_TO = 4;
  public static final String GOOGLE_ACCOUNT_TYPE = "com.google";
  @SuppressLint({"InlinedApi"})
  public static final String KEY_ANDROID_PACKAGE_NAME = "androidPackageName";
  @SuppressLint({"InlinedApi"})
  public static final String KEY_CALLER_UID = "callerUid";
  public static final String KEY_SUPPRESS_PROGRESS_SCREEN = "suppressProgressScreen";
  public static final String WORK_ACCOUNT_TYPE = "com.google.work";
  private static final ComponentName zzm = new ComponentName("com.google.android.gms", "com.google.android.gms.auth.GetToken");
  private static final Logger zzn = new Logger("Auth", new String[] { "GoogleAuthUtil" });
  
  public static void clearToken(Context paramContext, String paramString)
    throws GooglePlayServicesAvailabilityException, GoogleAuthException, IOException
  {
    Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
    ensurePlayServicesAvailable(paramContext, 8400000);
    Bundle localBundle = new Bundle();
    String str = paramContext.getApplicationInfo().packageName;
    localBundle.putString("clientPackageName", str);
    if (!localBundle.containsKey(KEY_ANDROID_PACKAGE_NAME)) {
      localBundle.putString(KEY_ANDROID_PACKAGE_NAME, str);
    }
    paramString = new zzf(paramString, localBundle);
    zza(paramContext, zzm, paramString);
  }
  
  private static void ensurePlayServicesAvailable(Context paramContext, int paramInt)
    throws GoogleAuthException
  {
    try
    {
      GooglePlayServicesUtilLight.ensurePlayServicesAvailable(paramContext.getApplicationContext(), paramInt);
      return;
    }
    catch (GooglePlayServicesRepairableException paramContext)
    {
      throw new GooglePlayServicesAvailabilityException(paramContext.getConnectionStatusCode(), paramContext.getMessage(), paramContext.getIntent());
    }
    catch (GooglePlayServicesNotAvailableException paramContext)
    {
      throw new GoogleAuthException(paramContext.getMessage());
    }
  }
  
  public static List<AccountChangeEvent> getAccountChangeEvents(Context paramContext, int paramInt, String paramString)
    throws GoogleAuthException, IOException
  {
    Preconditions.checkNotEmpty(paramString, "accountName must be provided");
    Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
    ensurePlayServicesAvailable(paramContext, 8400000);
    paramString = new zzg(paramString, paramInt);
    return (List)zza(paramContext, zzm, paramString);
  }
  
  public static String getAccountId(Context paramContext, String paramString)
    throws GoogleAuthException, IOException
  {
    Preconditions.checkNotEmpty(paramString, "accountName must be provided");
    Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
    ensurePlayServicesAvailable(paramContext, 8400000);
    return getToken(paramContext, paramString, "^^_account_id_^^", new Bundle());
  }
  
  public static String getToken(Context paramContext, Account paramAccount, String paramString)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return getToken(paramContext, paramAccount, paramString, new Bundle());
  }
  
  public static String getToken(Context paramContext, Account paramAccount, String paramString, Bundle paramBundle)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    zzb(paramAccount);
    return zzb(paramContext, paramAccount, paramString, paramBundle).zzb();
  }
  
  @Deprecated
  public static String getToken(Context paramContext, String paramString1, String paramString2)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return getToken(paramContext, new Account(paramString1, "com.google"), paramString2);
  }
  
  @Deprecated
  public static String getToken(Context paramContext, String paramString1, String paramString2, Bundle paramBundle)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return getToken(paramContext, new Account(paramString1, "com.google"), paramString2, paramBundle);
  }
  
  @Deprecated
  @RequiresPermission("android.permission.MANAGE_ACCOUNTS")
  public static void invalidateToken(Context paramContext, String paramString)
  {
    AccountManager.get(paramContext).invalidateAuthToken("com.google", paramString);
  }
  
  @TargetApi(23)
  public static Bundle removeAccount(Context paramContext, Account paramAccount)
    throws GoogleAuthException, IOException
  {
    Preconditions.checkNotNull(paramContext);
    zzb(paramAccount);
    ensurePlayServicesAvailable(paramContext, 8400000);
    paramAccount = new zzh(paramAccount);
    return (Bundle)zza(paramContext, zzm, paramAccount);
  }
  
  @TargetApi(26)
  public static Boolean requestGoogleAccountsAccess(Context paramContext)
    throws GoogleAuthException, IOException
  {
    Preconditions.checkNotNull(paramContext);
    ensurePlayServicesAvailable(paramContext, 11400000);
    zzi localzzi = new zzi(paramContext.getApplicationInfo().packageName);
    return (Boolean)zza(paramContext, zzm, localzzi);
  }
  
  /* Error */
  private static <T> T zza(Context paramContext, ComponentName paramComponentName, zzj<T> paramzzj)
    throws IOException, GoogleAuthException
  {
    // Byte code:
    //   0: new 250	com/google/android/gms/common/BlockingServiceConnection
    //   3: dup
    //   4: invokespecial 251	com/google/android/gms/common/BlockingServiceConnection:<init>	()V
    //   7: astore_3
    //   8: aload_0
    //   9: invokestatic 257	com/google/android/gms/common/internal/GmsClientSupervisor:getInstance	(Landroid/content/Context;)Lcom/google/android/gms/common/internal/GmsClientSupervisor;
    //   12: astore 4
    //   14: aload 4
    //   16: aload_1
    //   17: aload_3
    //   18: ldc 68
    //   20: invokevirtual 261	com/google/android/gms/common/internal/GmsClientSupervisor:bindService	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)Z
    //   23: ifeq +72 -> 95
    //   26: aload_2
    //   27: aload_3
    //   28: invokevirtual 265	com/google/android/gms/common/BlockingServiceConnection:getService	()Landroid/os/IBinder;
    //   31: invokeinterface 270 2 0
    //   36: astore_0
    //   37: aload 4
    //   39: aload_1
    //   40: aload_3
    //   41: ldc 68
    //   43: invokevirtual 274	com/google/android/gms/common/internal/GmsClientSupervisor:unbindService	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)V
    //   46: aload_0
    //   47: areturn
    //   48: astore_0
    //   49: getstatic 73	com/google/android/gms/auth/zzd:zzn	Lcom/google/android/gms/common/logging/Logger;
    //   52: ldc 68
    //   54: iconst_2
    //   55: anewarray 4	java/lang/Object
    //   58: dup
    //   59: iconst_0
    //   60: ldc_w 276
    //   63: aastore
    //   64: dup
    //   65: iconst_1
    //   66: aload_0
    //   67: aastore
    //   68: invokevirtual 280	com/google/android/gms/common/logging/Logger:i	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   71: new 84	java/io/IOException
    //   74: dup
    //   75: ldc_w 276
    //   78: aload_0
    //   79: invokespecial 283	java/io/IOException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   82: athrow
    //   83: astore_0
    //   84: aload 4
    //   86: aload_1
    //   87: aload_3
    //   88: ldc 68
    //   90: invokevirtual 274	com/google/android/gms/common/internal/GmsClientSupervisor:unbindService	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)V
    //   93: aload_0
    //   94: athrow
    //   95: new 84	java/io/IOException
    //   98: dup
    //   99: ldc_w 285
    //   102: invokespecial 286	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   105: athrow
    //   106: astore_0
    //   107: goto -58 -> 49
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	110	0	paramContext	Context
    //   0	110	1	paramComponentName	ComponentName
    //   0	110	2	paramzzj	zzj<T>
    //   7	81	3	localBlockingServiceConnection	com.google.android.gms.common.BlockingServiceConnection
    //   12	73	4	localGmsClientSupervisor	com.google.android.gms.common.internal.GmsClientSupervisor
    // Exception table:
    //   from	to	target	type
    //   26	37	48	java/lang/InterruptedException
    //   26	37	83	finally
    //   49	83	83	finally
    //   26	37	106	android/os/RemoteException
  }
  
  private static <T> T zza(T paramT)
    throws IOException
  {
    if (paramT == null)
    {
      zzn.w("GoogleAuthUtil", new Object[] { "Binder call returned null." });
      throw new IOException("Service unavailable.");
    }
    return paramT;
  }
  
  public static TokenData zzb(Context paramContext, Account paramAccount, String paramString, Bundle paramBundle)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
    Preconditions.checkNotEmpty(paramString, "Scope cannot be empty or null.");
    zzb(paramAccount);
    ensurePlayServicesAvailable(paramContext, 8400000);
    if (paramBundle == null) {}
    for (paramBundle = new Bundle();; paramBundle = new Bundle(paramBundle))
    {
      String str = paramContext.getApplicationInfo().packageName;
      paramBundle.putString("clientPackageName", str);
      if (TextUtils.isEmpty(paramBundle.getString(KEY_ANDROID_PACKAGE_NAME))) {
        paramBundle.putString(KEY_ANDROID_PACKAGE_NAME, str);
      }
      paramBundle.putLong("service_connection_start_time_millis", SystemClock.elapsedRealtime());
      paramAccount = new zze(paramAccount, paramString, paramBundle);
      return (TokenData)zza(paramContext, zzm, paramAccount);
    }
  }
  
  private static void zzb(Account paramAccount)
  {
    if (paramAccount == null) {
      throw new IllegalArgumentException("Account cannot be null");
    }
    if (TextUtils.isEmpty(paramAccount.name)) {
      throw new IllegalArgumentException("Account name cannot be empty!");
    }
    String[] arrayOfString = ACCEPTABLE_ACCOUNT_TYPES;
    int j = arrayOfString.length;
    int i = 0;
    while (i < j)
    {
      if (arrayOfString[i].equals(paramAccount.type)) {
        return;
      }
      i += 1;
    }
    throw new IllegalArgumentException("Account type not supported");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */