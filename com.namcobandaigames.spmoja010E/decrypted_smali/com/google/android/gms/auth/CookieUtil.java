package com.google.android.gms.auth;

import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;

public final class CookieUtil
{
  public static String getCookieUrl(String paramString, Boolean paramBoolean)
  {
    Preconditions.checkNotEmpty(paramString);
    if (zza(paramBoolean)) {}
    for (paramBoolean = "https";; paramBoolean = "http") {
      return String.valueOf(paramBoolean).length() + 3 + String.valueOf(paramString).length() + paramBoolean + "://" + paramString;
    }
  }
  
  public static String getCookieValue(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean1, Boolean paramBoolean2, Long paramLong)
  {
    paramString1 = new StringBuilder(paramString1).append('=');
    if (!TextUtils.isEmpty(paramString2)) {
      paramString1.append(paramString2);
    }
    if (zza(paramBoolean1)) {
      paramString1.append(";HttpOnly");
    }
    if (zza(paramBoolean2)) {
      paramString1.append(";Secure");
    }
    if (!TextUtils.isEmpty(paramString3)) {
      paramString1.append(";Domain=").append(paramString3);
    }
    if (!TextUtils.isEmpty(paramString4)) {
      paramString1.append(";Path=").append(paramString4);
    }
    if ((paramLong != null) && (paramLong.longValue() > 0L)) {
      paramString1.append(";Max-Age=").append(paramLong);
    }
    return paramString1.toString();
  }
  
  private static boolean zza(Boolean paramBoolean)
  {
    return (paramBoolean != null) && (paramBoolean.booleanValue());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\CookieUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */