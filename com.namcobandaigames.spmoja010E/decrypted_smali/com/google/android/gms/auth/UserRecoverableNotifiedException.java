package com.google.android.gms.auth;

import com.google.android.gms.common.util.VisibleForTesting;

public class UserRecoverableNotifiedException
  extends GoogleAuthException
{
  @VisibleForTesting
  public UserRecoverableNotifiedException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\UserRecoverableNotifiedException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */