package com.google.android.gms.auth.account;

import android.accounts.Account;
import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zza
  extends IInterface
{
  public abstract void zza(boolean paramBoolean)
    throws RemoteException;
  
  public abstract void zzc(Account paramAccount)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\account\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */