package com.google.android.gms.auth.account;

import android.accounts.Account;
import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zzc
  extends IInterface
{
  public abstract void zza(zza paramzza, Account paramAccount)
    throws RemoteException;
  
  public abstract void zza(zza paramzza, String paramString)
    throws RemoteException;
  
  public abstract void zzb(boolean paramBoolean)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\auth\account\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */