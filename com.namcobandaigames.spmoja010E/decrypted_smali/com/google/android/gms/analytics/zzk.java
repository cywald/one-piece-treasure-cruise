package com.google.android.gms.analytics;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Process;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzac;
import com.google.android.gms.internal.measurement.zzdg;
import com.google.android.gms.internal.measurement.zzx;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressLint({"StaticFieldLeak"})
@VisibleForTesting
public final class zzk
{
  private static volatile zzk zzsm;
  private final Context zzri;
  private final List<zzn> zzsn;
  private final zze zzso;
  private final zza zzsp;
  private volatile zzx zzsq;
  private Thread.UncaughtExceptionHandler zzsr;
  
  @VisibleForTesting
  private zzk(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    Preconditions.checkNotNull(paramContext);
    this.zzri = paramContext;
    this.zzsp = new zza();
    this.zzsn = new CopyOnWriteArrayList();
    this.zzso = new zze();
  }
  
  public static void zzaf()
  {
    if (!(Thread.currentThread() instanceof zzc)) {
      throw new IllegalStateException("Call expected from worker thread");
    }
  }
  
  public static zzk zzb(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    if (zzsm == null) {}
    try
    {
      if (zzsm == null) {
        zzsm = new zzk(paramContext);
      }
      return zzsm;
    }
    finally {}
  }
  
  private static void zzb(zzg paramzzg)
  {
    Preconditions.checkNotMainThread("deliver should be called from worker thread");
    Preconditions.checkArgument(paramzzg.zzx(), "Measurement must be submitted");
    Object localObject = paramzzg.zzu();
    if (((List)localObject).isEmpty()) {}
    for (;;)
    {
      return;
      HashSet localHashSet = new HashSet();
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        zzo localzzo = (zzo)((Iterator)localObject).next();
        Uri localUri = localzzo.zzo();
        if (!localHashSet.contains(localUri))
        {
          localHashSet.add(localUri);
          localzzo.zzb(paramzzg);
        }
      }
    }
  }
  
  public final Context getContext()
  {
    return this.zzri;
  }
  
  public final <V> Future<V> zza(Callable<V> paramCallable)
  {
    Preconditions.checkNotNull(paramCallable);
    if ((Thread.currentThread() instanceof zzc))
    {
      paramCallable = new FutureTask(paramCallable);
      paramCallable.run();
      return paramCallable;
    }
    return this.zzsp.submit(paramCallable);
  }
  
  public final void zza(Runnable paramRunnable)
  {
    Preconditions.checkNotNull(paramRunnable);
    this.zzsp.submit(paramRunnable);
  }
  
  public final void zza(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler)
  {
    this.zzsr = paramUncaughtExceptionHandler;
  }
  
  public final zzx zzad()
  {
    if (this.zzsq == null) {}
    label202:
    for (;;)
    {
      try
      {
        zzx localzzx;
        PackageManager localPackageManager;
        String str3;
        Object localObject5;
        Object localObject3;
        if (this.zzsq == null)
        {
          localzzx = new zzx();
          localPackageManager = this.zzri.getPackageManager();
          str3 = this.zzri.getPackageName();
          localzzx.setAppId(str3);
          localzzx.setAppInstallerId(localPackageManager.getInstallerPackageName(str3));
          localObject5 = null;
          localObject3 = str3;
        }
        try
        {
          PackageInfo localPackageInfo = localPackageManager.getPackageInfo(this.zzri.getPackageName(), 0);
          localObject4 = localObject5;
          String str1 = str3;
          if (localPackageInfo != null)
          {
            localObject3 = str3;
            localObject4 = localPackageManager.getApplicationLabel(localPackageInfo.applicationInfo);
            str1 = str3;
            localObject3 = str3;
            if (!TextUtils.isEmpty((CharSequence)localObject4))
            {
              localObject3 = str3;
              str1 = ((CharSequence)localObject4).toString();
            }
            localObject3 = str1;
            localObject4 = localPackageInfo.versionName;
          }
          localzzx.setAppName(str1);
          localzzx.setAppVersion((String)localObject4);
          this.zzsq = localzzx;
          return this.zzsq;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
          localObject1 = String.valueOf(localObject3);
          if (((String)localObject1).length() == 0) {
            break label202;
          }
        }
        Object localObject1 = "Error retrieving package info: appName set to ".concat((String)localObject1);
        Log.e("GAv4", (String)localObject1);
        Object localObject4 = localObject5;
        localObject1 = localObject3;
        continue;
        String str2 = new String("Error retrieving package info: appName set to ");
      }
      finally {}
    }
  }
  
  public final zzac zzae()
  {
    DisplayMetrics localDisplayMetrics = this.zzri.getResources().getDisplayMetrics();
    zzac localzzac = new zzac();
    localzzac.setLanguage(zzdg.zza(Locale.getDefault()));
    localzzac.zzuh = localDisplayMetrics.widthPixels;
    localzzac.zzui = localDisplayMetrics.heightPixels;
    return localzzac;
  }
  
  final void zze(zzg paramzzg)
  {
    if (paramzzg.zzaa()) {
      throw new IllegalStateException("Measurement prototype can't be submitted");
    }
    if (paramzzg.zzx()) {
      throw new IllegalStateException("Measurement can only be submitted once");
    }
    paramzzg = paramzzg.zzs();
    paramzzg.zzy();
    this.zzsp.execute(new zzl(this, paramzzg));
  }
  
  final class zza
    extends ThreadPoolExecutor
  {
    public zza()
    {
      super(1, 1L, TimeUnit.MINUTES, new LinkedBlockingQueue());
      setThreadFactory(new zzk.zzb(null));
      allowCoreThreadTimeOut(true);
    }
    
    protected final <T> RunnableFuture<T> newTaskFor(Runnable paramRunnable, T paramT)
    {
      return new zzm(this, paramRunnable, paramT);
    }
  }
  
  static final class zzb
    implements ThreadFactory
  {
    private static final AtomicInteger zzsv = new AtomicInteger();
    
    public final Thread newThread(Runnable paramRunnable)
    {
      int i = zzsv.incrementAndGet();
      return new zzk.zzc(paramRunnable, 23 + "measurement-" + i);
    }
  }
  
  static final class zzc
    extends Thread
  {
    zzc(Runnable paramRunnable, String paramString)
    {
      super(paramString);
    }
    
    public final void run()
    {
      Process.setThreadPriority(10);
      super.run();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */