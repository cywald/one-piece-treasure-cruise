package com.google.android.gms.analytics;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzco;

@VisibleForTesting
public final class zzd
{
  private static String zza(String paramString, int paramInt)
  {
    if (paramInt <= 0)
    {
      zzco.zzf("index out of range for prefix", paramString);
      return "";
    }
    return String.valueOf(paramString).length() + 11 + paramString + paramInt;
  }
  
  public static String zzd(int paramInt)
  {
    return zza("&cd", paramInt);
  }
  
  public static String zze(int paramInt)
  {
    return zza("cd", paramInt);
  }
  
  public static String zzf(int paramInt)
  {
    return zza("&cm", paramInt);
  }
  
  public static String zzg(int paramInt)
  {
    return zza("cm", paramInt);
  }
  
  public static String zzh(int paramInt)
  {
    return zza("&pr", paramInt);
  }
  
  public static String zzi(int paramInt)
  {
    return zza("pr", paramInt);
  }
  
  public static String zzj(int paramInt)
  {
    return zza("&promo", paramInt);
  }
  
  public static String zzk(int paramInt)
  {
    return zza("promo", paramInt);
  }
  
  public static String zzl(int paramInt)
  {
    return zza("pi", paramInt);
  }
  
  public static String zzm(int paramInt)
  {
    return zza("&il", paramInt);
  }
  
  public static String zzn(int paramInt)
  {
    return zza("il", paramInt);
  }
  
  public static String zzo(int paramInt)
  {
    return zza("cd", paramInt);
  }
  
  public static String zzp(int paramInt)
  {
    return zza("cm", paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */