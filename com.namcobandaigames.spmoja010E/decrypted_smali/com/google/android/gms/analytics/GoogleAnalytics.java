package com.google.android.gms.analytics;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.RequiresPermission;
import android.util.Log;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzal;
import com.google.android.gms.internal.measurement.zzau;
import com.google.android.gms.internal.measurement.zzaw;
import com.google.android.gms.internal.measurement.zzcf;
import com.google.android.gms.internal.measurement.zzcg;
import com.google.android.gms.internal.measurement.zzco;
import com.google.android.gms.internal.measurement.zzdd;
import com.google.android.gms.internal.measurement.zzdf;
import com.google.android.gms.internal.measurement.zzdh;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@VisibleForTesting
public final class GoogleAnalytics
  extends zza
{
  private static List<Runnable> zzrl = new ArrayList();
  private boolean zzrm;
  private Set<zza> zzrn = new HashSet();
  private boolean zzro;
  private boolean zzrp;
  private volatile boolean zzrq;
  private boolean zzrr;
  
  @VisibleForTesting
  public GoogleAnalytics(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public static GoogleAnalytics getInstance(Context paramContext)
  {
    return zzaw.zzc(paramContext).zzco();
  }
  
  public static void zzr()
  {
    try
    {
      if (zzrl != null)
      {
        Iterator localIterator = zzrl.iterator();
        while (localIterator.hasNext()) {
          ((Runnable)localIterator.next()).run();
        }
        zzrl = null;
      }
    }
    finally {}
  }
  
  public final void dispatchLocalHits()
  {
    zzl().zzcc().zzbs();
  }
  
  @TargetApi(14)
  public final void enableAutoActivityReports(Application paramApplication)
  {
    if (!this.zzro)
    {
      paramApplication.registerActivityLifecycleCallbacks(new zzb());
      this.zzro = true;
    }
  }
  
  public final boolean getAppOptOut()
  {
    return this.zzrq;
  }
  
  @Deprecated
  public final Logger getLogger()
  {
    return zzco.getLogger();
  }
  
  public final boolean isDryRunEnabled()
  {
    return this.zzrp;
  }
  
  public final boolean isInitialized()
  {
    return this.zzrm;
  }
  
  public final Tracker newTracker(int paramInt)
  {
    try
    {
      Tracker localTracker = new Tracker(zzl(), null, null);
      if (paramInt > 0)
      {
        zzdf localzzdf = (zzdf)new zzdd(zzl()).zzq(paramInt);
        if (localzzdf != null) {
          localTracker.zza(localzzdf);
        }
      }
      localTracker.zzq();
      return localTracker;
    }
    finally {}
  }
  
  public final Tracker newTracker(String paramString)
  {
    try
    {
      paramString = new Tracker(zzl(), paramString, null);
      paramString.zzq();
      return paramString;
    }
    finally {}
  }
  
  public final void reportActivityStart(Activity paramActivity)
  {
    if (!this.zzro) {
      zza(paramActivity);
    }
  }
  
  public final void reportActivityStop(Activity paramActivity)
  {
    if (!this.zzro) {
      zzb(paramActivity);
    }
  }
  
  public final void setAppOptOut(boolean paramBoolean)
  {
    this.zzrq = paramBoolean;
    if (this.zzrq) {
      zzl().zzcc().zzbr();
    }
  }
  
  public final void setDryRun(boolean paramBoolean)
  {
    this.zzrp = paramBoolean;
  }
  
  public final void setLocalDispatchPeriod(int paramInt)
  {
    zzl().zzcc().setLocalDispatchPeriod(paramInt);
  }
  
  @Deprecated
  public final void setLogger(Logger paramLogger)
  {
    zzco.setLogger(paramLogger);
    if (!this.zzrr)
    {
      paramLogger = (String)zzcf.zzyx.get();
      String str = (String)zzcf.zzyx.get();
      Log.i(paramLogger, String.valueOf(str).length() + 112 + "GoogleAnalytics.setLogger() is deprecated. To enable debug logging, please run:\nadb shell setprop log.tag." + str + " DEBUG");
      this.zzrr = true;
    }
  }
  
  @VisibleForTesting
  final void zza(Activity paramActivity)
  {
    Iterator localIterator = this.zzrn.iterator();
    while (localIterator.hasNext()) {
      ((zza)localIterator.next()).zzc(paramActivity);
    }
  }
  
  final void zza(zza paramzza)
  {
    this.zzrn.add(paramzza);
    paramzza = zzl().getContext();
    if ((paramzza instanceof Application)) {
      enableAutoActivityReports((Application)paramzza);
    }
  }
  
  @VisibleForTesting
  final void zzb(Activity paramActivity)
  {
    Iterator localIterator = this.zzrn.iterator();
    while (localIterator.hasNext()) {
      ((zza)localIterator.next()).zzd(paramActivity);
    }
  }
  
  final void zzb(zza paramzza)
  {
    this.zzrn.remove(paramzza);
  }
  
  public final void zzq()
  {
    zzdh localzzdh = zzl().zzce();
    localzzdh.zzfr();
    if (localzzdh.zzfs()) {
      setDryRun(localzzdh.zzft());
    }
    localzzdh.zzfr();
    this.zzrm = true;
  }
  
  static abstract interface zza
  {
    public abstract void zzc(Activity paramActivity);
    
    public abstract void zzd(Activity paramActivity);
  }
  
  @TargetApi(14)
  final class zzb
    implements Application.ActivityLifecycleCallbacks
  {
    zzb() {}
    
    public final void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
    
    public final void onActivityDestroyed(Activity paramActivity) {}
    
    public final void onActivityPaused(Activity paramActivity) {}
    
    public final void onActivityResumed(Activity paramActivity) {}
    
    public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
    
    public final void onActivityStarted(Activity paramActivity)
    {
      GoogleAnalytics.this.zza(paramActivity);
    }
    
    public final void onActivityStopped(Activity paramActivity)
    {
      GoogleAnalytics.this.zzb(paramActivity);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\GoogleAnalytics.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */