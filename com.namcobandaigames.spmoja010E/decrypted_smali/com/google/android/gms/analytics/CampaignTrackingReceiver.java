package com.google.android.gms.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.RequiresPermission;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzal;
import com.google.android.gms.internal.measurement.zzat;
import com.google.android.gms.internal.measurement.zzaw;
import com.google.android.gms.internal.measurement.zzbx;
import com.google.android.gms.internal.measurement.zzcp;
import com.google.android.gms.internal.measurement.zzdg;

@VisibleForTesting
public class CampaignTrackingReceiver
  extends BroadcastReceiver
{
  private static Boolean zzre;
  
  public static boolean zza(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    if (zzre != null) {
      return zzre.booleanValue();
    }
    boolean bool = zzdg.zza(paramContext, "com.google.android.gms.analytics.CampaignTrackingReceiver", true);
    zzre = Boolean.valueOf(bool);
    return bool;
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    zzaw localzzaw = zzaw.zzc(paramContext);
    zzcp localzzcp = localzzaw.zzby();
    if (paramIntent == null)
    {
      localzzcp.zzt("CampaignTrackingReceiver received null intent");
      return;
    }
    String str = paramIntent.getStringExtra("referrer");
    paramIntent = paramIntent.getAction();
    localzzcp.zza("CampaignTrackingReceiver received", paramIntent);
    if ((!"com.android.vending.INSTALL_REFERRER".equals(paramIntent)) || (TextUtils.isEmpty(str)))
    {
      localzzcp.zzt("CampaignTrackingReceiver received unexpected intent without referrer extra");
      return;
    }
    zza(paramContext, str);
    int i = zzbx.zzdy();
    if (str.length() <= i) {}
    for (paramContext = str;; paramContext = str.substring(0, i))
    {
      paramIntent = goAsync();
      localzzaw.zzcc().zza(paramContext, new zzc(this, paramIntent));
      return;
      localzzcp.zzc("Campaign data exceed the maximum supported size and will be clipped. size, limit", Integer.valueOf(str.length()), Integer.valueOf(i));
    }
  }
  
  protected void zza(Context paramContext, String paramString) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\CampaignTrackingReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */