package com.google.android.gms.analytics;

import android.net.Uri;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public abstract interface zzo
{
  public abstract void zzb(zzg paramzzg);
  
  public abstract Uri zzo();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */