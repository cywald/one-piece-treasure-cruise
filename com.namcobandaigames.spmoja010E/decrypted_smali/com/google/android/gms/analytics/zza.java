package com.google.android.gms.analytics;

import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzag;
import com.google.android.gms.internal.measurement.zzak;
import com.google.android.gms.internal.measurement.zzaw;
import com.google.android.gms.internal.measurement.zzbh;
import com.google.android.gms.internal.measurement.zzbo;
import com.google.android.gms.internal.measurement.zzcb;
import java.util.List;
import java.util.ListIterator;

@VisibleForTesting
public class zza
  extends zzj<zza>
{
  private final zzaw zzqx;
  private boolean zzqy;
  
  @VisibleForTesting
  public zza(zzaw paramzzaw)
  {
    super(paramzzaw.zzca(), paramzzaw.zzbx());
    this.zzqx = paramzzaw;
  }
  
  public final void enableAdvertisingIdCollection(boolean paramBoolean)
  {
    this.zzqy = paramBoolean;
  }
  
  protected final void zza(zzg paramzzg)
  {
    paramzzg = (zzag)paramzzg.zzb(zzag.class);
    if (TextUtils.isEmpty(paramzzg.zzbd())) {
      paramzzg.setClientId(this.zzqx.zzcr().zzdr());
    }
    if ((this.zzqy) && (TextUtils.isEmpty(paramzzg.zzbf())))
    {
      zzak localzzak = this.zzqx.zzcq();
      paramzzg.zzm(localzzak.zzbn());
      paramzzg.zza(localzzak.zzbg());
    }
  }
  
  public final void zza(String paramString)
  {
    Preconditions.checkNotEmpty(paramString);
    Uri localUri = zzb.zzb(paramString);
    ListIterator localListIterator = this.zzsk.zzu().listIterator();
    while (localListIterator.hasNext()) {
      if (localUri.equals(((zzo)localListIterator.next()).zzo())) {
        localListIterator.remove();
      }
    }
    this.zzsk.zzu().add(new zzb(this.zzqx, paramString));
  }
  
  @VisibleForTesting
  final zzaw zzl()
  {
    return this.zzqx;
  }
  
  public final zzg zzm()
  {
    zzg localzzg = this.zzsk.zzs();
    localzzg.zza(this.zzqx.zzci().zzdf());
    localzzg.zza(this.zzqx.zzcj().zzek());
    zzd(localzzg);
    return localzzg;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */