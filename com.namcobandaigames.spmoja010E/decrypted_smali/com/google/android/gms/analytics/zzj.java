package com.google.android.gms.analytics;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class zzj<T extends zzj>
{
  private final zzk zzsj;
  protected final zzg zzsk;
  private final List<zzh> zzsl;
  
  @VisibleForTesting
  protected zzj(zzk paramzzk, Clock paramClock)
  {
    Preconditions.checkNotNull(paramzzk);
    this.zzsj = paramzzk;
    this.zzsl = new ArrayList();
    paramzzk = new zzg(this, paramClock);
    paramzzk.zzab();
    this.zzsk = paramzzk;
  }
  
  protected void zza(zzg paramzzg) {}
  
  protected final zzk zzac()
  {
    return this.zzsj;
  }
  
  protected final void zzd(zzg paramzzg)
  {
    Iterator localIterator = this.zzsl.iterator();
    while (localIterator.hasNext()) {
      ((zzh)localIterator.next()).zza(this, paramzzg);
    }
  }
  
  public zzg zzm()
  {
    zzg localzzg = this.zzsk.zzs();
    zzd(localzzg);
    return localzzg;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */