package com.google.android.gms.analytics;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@VisibleForTesting
public final class zzg
{
  private final zzj zzry;
  private final Clock zzrz;
  private boolean zzsa;
  private long zzsb;
  private long zzsc;
  private long zzsd;
  private long zzse;
  private long zzsf;
  private boolean zzsg;
  private final Map<Class<? extends zzi>, zzi> zzsh;
  private final List<zzo> zzsi;
  
  private zzg(zzg paramzzg)
  {
    this.zzry = paramzzg.zzry;
    this.zzrz = paramzzg.zzrz;
    this.zzsb = paramzzg.zzsb;
    this.zzsc = paramzzg.zzsc;
    this.zzsd = paramzzg.zzsd;
    this.zzse = paramzzg.zzse;
    this.zzsf = paramzzg.zzsf;
    this.zzsi = new ArrayList(paramzzg.zzsi);
    this.zzsh = new HashMap(paramzzg.zzsh.size());
    paramzzg = paramzzg.zzsh.entrySet().iterator();
    while (paramzzg.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramzzg.next();
      zzi localzzi = zzc((Class)localEntry.getKey());
      ((zzi)localEntry.getValue()).zzb(localzzi);
      this.zzsh.put((Class)localEntry.getKey(), localzzi);
    }
  }
  
  @VisibleForTesting
  zzg(zzj paramzzj, Clock paramClock)
  {
    Preconditions.checkNotNull(paramzzj);
    Preconditions.checkNotNull(paramClock);
    this.zzry = paramzzj;
    this.zzrz = paramClock;
    this.zzse = 1800000L;
    this.zzsf = 3024000000L;
    this.zzsh = new HashMap();
    this.zzsi = new ArrayList();
  }
  
  @TargetApi(19)
  private static <T extends zzi> T zzc(Class<T> paramClass)
  {
    try
    {
      paramClass = (zzi)paramClass.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
      return paramClass;
    }
    catch (Exception paramClass)
    {
      if ((paramClass instanceof InstantiationException)) {
        throw new IllegalArgumentException("dataType doesn't have default constructor", paramClass);
      }
      if ((paramClass instanceof IllegalAccessException)) {
        throw new IllegalArgumentException("dataType default constructor is not accessible", paramClass);
      }
      if ((Build.VERSION.SDK_INT >= 19) && ((paramClass instanceof ReflectiveOperationException))) {
        throw new IllegalArgumentException("Linkage exception", paramClass);
      }
      throw new RuntimeException(paramClass);
    }
  }
  
  @VisibleForTesting
  public final <T extends zzi> T zza(Class<T> paramClass)
  {
    return (zzi)this.zzsh.get(paramClass);
  }
  
  @VisibleForTesting
  public final void zza(long paramLong)
  {
    this.zzsc = paramLong;
  }
  
  @VisibleForTesting
  public final void zza(zzi paramzzi)
  {
    Preconditions.checkNotNull(paramzzi);
    Class localClass = paramzzi.getClass();
    if (localClass.getSuperclass() != zzi.class) {
      throw new IllegalArgumentException();
    }
    paramzzi.zzb(zzb(localClass));
  }
  
  @VisibleForTesting
  final boolean zzaa()
  {
    return this.zzsg;
  }
  
  @VisibleForTesting
  final void zzab()
  {
    this.zzsg = true;
  }
  
  @VisibleForTesting
  public final <T extends zzi> T zzb(Class<T> paramClass)
  {
    zzi localzzi2 = (zzi)this.zzsh.get(paramClass);
    zzi localzzi1 = localzzi2;
    if (localzzi2 == null)
    {
      localzzi1 = zzc(paramClass);
      this.zzsh.put(paramClass, localzzi1);
    }
    return localzzi1;
  }
  
  @VisibleForTesting
  public final zzg zzs()
  {
    return new zzg(this);
  }
  
  @VisibleForTesting
  public final Collection<zzi> zzt()
  {
    return this.zzsh.values();
  }
  
  public final List<zzo> zzu()
  {
    return this.zzsi;
  }
  
  @VisibleForTesting
  public final long zzv()
  {
    return this.zzsb;
  }
  
  @VisibleForTesting
  public final void zzw()
  {
    this.zzry.zzac().zze(this);
  }
  
  @VisibleForTesting
  public final boolean zzx()
  {
    return this.zzsa;
  }
  
  @VisibleForTesting
  final void zzy()
  {
    this.zzsd = this.zzrz.elapsedRealtime();
    if (this.zzsc != 0L) {}
    for (this.zzsb = this.zzsc;; this.zzsb = this.zzrz.currentTimeMillis())
    {
      this.zzsa = true;
      return;
    }
  }
  
  final zzj zzz()
  {
    return this.zzry;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */