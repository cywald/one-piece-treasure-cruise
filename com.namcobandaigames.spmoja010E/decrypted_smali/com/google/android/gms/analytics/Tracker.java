package com.google.android.gms.analytics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzat;
import com.google.android.gms.internal.measurement.zzau;
import com.google.android.gms.internal.measurement.zzaw;
import com.google.android.gms.internal.measurement.zzbh;
import com.google.android.gms.internal.measurement.zzbo;
import com.google.android.gms.internal.measurement.zzcb;
import com.google.android.gms.internal.measurement.zzcn;
import com.google.android.gms.internal.measurement.zzcp;
import com.google.android.gms.internal.measurement.zzdf;
import com.google.android.gms.internal.measurement.zzdg;
import com.google.android.gms.internal.measurement.zzdh;
import com.google.android.gms.internal.measurement.zzx;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

@VisibleForTesting
public class Tracker
  extends zzau
{
  private boolean zzsx;
  private final Map<String, String> zzsy = new HashMap();
  private final Map<String, String> zzsz = new HashMap();
  private final zzcn zzta;
  private final zza zztb;
  private ExceptionReporter zztc;
  private zzdf zztd;
  
  Tracker(zzaw paramzzaw, String paramString, zzcn paramzzcn)
  {
    super(paramzzaw);
    if (paramString != null) {
      this.zzsy.put("&tid", paramString);
    }
    this.zzsy.put("useSecure", "1");
    this.zzsy.put("&a", Integer.toString(new Random().nextInt(Integer.MAX_VALUE) + 1));
    this.zzta = new zzcn("tracking", zzbx());
    this.zztb = new zza(paramzzaw);
  }
  
  private static String zza(Map.Entry<String, String> paramEntry)
  {
    String str = (String)paramEntry.getKey();
    if ((!str.startsWith("&")) || (str.length() < 2)) {}
    for (int i = 0; i == 0; i = 1) {
      return null;
    }
    return ((String)paramEntry.getKey()).substring(1);
  }
  
  private static void zza(Map<String, String> paramMap1, Map<String, String> paramMap2)
  {
    Preconditions.checkNotNull(paramMap2);
    if (paramMap1 == null) {}
    for (;;)
    {
      return;
      paramMap1 = paramMap1.entrySet().iterator();
      while (paramMap1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap1.next();
        String str = zza(localEntry);
        if (str != null) {
          paramMap2.put(str, (String)localEntry.getValue());
        }
      }
    }
  }
  
  public void enableAdvertisingIdCollection(boolean paramBoolean)
  {
    this.zzsx = paramBoolean;
  }
  
  public void enableAutoActivityTracking(boolean paramBoolean)
  {
    this.zztb.enableAutoActivityTracking(paramBoolean);
  }
  
  public void enableExceptionReporting(boolean paramBoolean)
  {
    for (;;)
    {
      try
      {
        if (this.zztc == null) {
          break label84;
        }
        bool = true;
        if (bool == paramBoolean) {
          return;
        }
        if (paramBoolean)
        {
          Context localContext = getContext();
          this.zztc = new ExceptionReporter(this, Thread.getDefaultUncaughtExceptionHandler(), localContext);
          Thread.setDefaultUncaughtExceptionHandler(this.zztc);
          zzq("Uncaught exceptions will be reported to Google Analytics");
          return;
        }
      }
      finally {}
      Thread.setDefaultUncaughtExceptionHandler(this.zztc.zzp());
      zzq("Uncaught exceptions will not be reported to Google Analytics");
      continue;
      label84:
      boolean bool = false;
    }
  }
  
  public String get(String paramString)
  {
    zzcl();
    if (TextUtils.isEmpty(paramString)) {}
    do
    {
      return null;
      if (this.zzsy.containsKey(paramString)) {
        return (String)this.zzsy.get(paramString);
      }
      if (paramString.equals("&ul")) {
        return zzdg.zza(Locale.getDefault());
      }
      if (paramString.equals("&cid")) {
        return zzcg().zzdr();
      }
      if (paramString.equals("&sr")) {
        return zzcj().zzel();
      }
      if (paramString.equals("&aid")) {
        return zzci().zzdf().zzal();
      }
      if (paramString.equals("&an")) {
        return zzci().zzdf().zzaj();
      }
      if (paramString.equals("&av")) {
        return zzci().zzdf().zzak();
      }
    } while (!paramString.equals("&aiid"));
    return zzci().zzdf().zzam();
  }
  
  public void send(Map<String, String> paramMap)
  {
    long l = zzbx().currentTimeMillis();
    if (zzcb().getAppOptOut())
    {
      zzr("AppOptOut is set to true. Not sending Google Analytics hit");
      return;
    }
    boolean bool1 = zzcb().isDryRunEnabled();
    HashMap localHashMap = new HashMap();
    zza(this.zzsy, localHashMap);
    zza(paramMap, localHashMap);
    boolean bool2 = zzdg.zzb((String)this.zzsy.get("useSecure"), true);
    paramMap = this.zzsz;
    Preconditions.checkNotNull(localHashMap);
    if (paramMap != null)
    {
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        localObject = (Map.Entry)paramMap.next();
        String str = zza((Map.Entry)localObject);
        if ((str != null) && (!localHashMap.containsKey(str))) {
          localHashMap.put(str, (String)((Map.Entry)localObject).getValue());
        }
      }
    }
    this.zzsz.clear();
    paramMap = (String)localHashMap.get("t");
    if (TextUtils.isEmpty(paramMap))
    {
      zzby().zza(localHashMap, "Missing hit type parameter");
      return;
    }
    Object localObject = (String)localHashMap.get("tid");
    if (TextUtils.isEmpty((CharSequence)localObject))
    {
      zzby().zza(localHashMap, "Missing tracking id parameter");
      return;
    }
    boolean bool3 = this.zzsx;
    try
    {
      if (("screenview".equalsIgnoreCase(paramMap)) || ("pageview".equalsIgnoreCase(paramMap)) || ("appview".equalsIgnoreCase(paramMap)) || (TextUtils.isEmpty(paramMap)))
      {
        int j = Integer.parseInt((String)this.zzsy.get("&a")) + 1;
        int i = j;
        if (j >= Integer.MAX_VALUE) {
          i = 1;
        }
        this.zzsy.put("&a", Integer.toString(i));
      }
      zzca().zza(new zzp(this, localHashMap, bool3, paramMap, l, bool1, bool2, (String)localObject));
      return;
    }
    finally {}
  }
  
  public void set(String paramString1, String paramString2)
  {
    Preconditions.checkNotNull(paramString1, "Key should be non-null");
    if (TextUtils.isEmpty(paramString1)) {
      return;
    }
    this.zzsy.put(paramString1, paramString2);
  }
  
  public void setAnonymizeIp(boolean paramBoolean)
  {
    set("&aip", zzdg.zzc(paramBoolean));
  }
  
  public void setAppId(String paramString)
  {
    set("&aid", paramString);
  }
  
  public void setAppInstallerId(String paramString)
  {
    set("&aiid", paramString);
  }
  
  public void setAppName(String paramString)
  {
    set("&an", paramString);
  }
  
  public void setAppVersion(String paramString)
  {
    set("&av", paramString);
  }
  
  public void setCampaignParamsOnNextHit(Uri paramUri)
  {
    if ((paramUri == null) || (paramUri.isOpaque())) {}
    do
    {
      return;
      paramUri = paramUri.getQueryParameter("referrer");
    } while (TextUtils.isEmpty(paramUri));
    paramUri = String.valueOf(paramUri);
    if (paramUri.length() != 0) {}
    for (paramUri = "http://hostname/?".concat(paramUri);; paramUri = new String("http://hostname/?"))
    {
      paramUri = Uri.parse(paramUri);
      String str = paramUri.getQueryParameter("utm_id");
      if (str != null) {
        this.zzsz.put("&ci", str);
      }
      str = paramUri.getQueryParameter("anid");
      if (str != null) {
        this.zzsz.put("&anid", str);
      }
      str = paramUri.getQueryParameter("utm_campaign");
      if (str != null) {
        this.zzsz.put("&cn", str);
      }
      str = paramUri.getQueryParameter("utm_content");
      if (str != null) {
        this.zzsz.put("&cc", str);
      }
      str = paramUri.getQueryParameter("utm_medium");
      if (str != null) {
        this.zzsz.put("&cm", str);
      }
      str = paramUri.getQueryParameter("utm_source");
      if (str != null) {
        this.zzsz.put("&cs", str);
      }
      str = paramUri.getQueryParameter("utm_term");
      if (str != null) {
        this.zzsz.put("&ck", str);
      }
      str = paramUri.getQueryParameter("dclid");
      if (str != null) {
        this.zzsz.put("&dclid", str);
      }
      str = paramUri.getQueryParameter("gclid");
      if (str != null) {
        this.zzsz.put("&gclid", str);
      }
      paramUri = paramUri.getQueryParameter("aclid");
      if (paramUri == null) {
        break;
      }
      this.zzsz.put("&aclid", paramUri);
      return;
    }
  }
  
  public void setClientId(String paramString)
  {
    set("&cid", paramString);
  }
  
  public void setEncoding(String paramString)
  {
    set("&de", paramString);
  }
  
  public void setHostname(String paramString)
  {
    set("&dh", paramString);
  }
  
  public void setLanguage(String paramString)
  {
    set("&ul", paramString);
  }
  
  public void setLocation(String paramString)
  {
    set("&dl", paramString);
  }
  
  public void setPage(String paramString)
  {
    set("&dp", paramString);
  }
  
  public void setReferrer(String paramString)
  {
    set("&dr", paramString);
  }
  
  public void setSampleRate(double paramDouble)
  {
    set("&sf", Double.toString(paramDouble));
  }
  
  public void setScreenColors(String paramString)
  {
    set("&sd", paramString);
  }
  
  public void setScreenName(String paramString)
  {
    set("&cd", paramString);
  }
  
  public void setScreenResolution(int paramInt1, int paramInt2)
  {
    if ((paramInt1 < 0) && (paramInt2 < 0))
    {
      zzt("Invalid width or height. The values should be non-negative.");
      return;
    }
    set("&sr", 23 + paramInt1 + "x" + paramInt2);
  }
  
  public void setSessionTimeout(long paramLong)
  {
    this.zztb.setSessionTimeout(1000L * paramLong);
  }
  
  public void setTitle(String paramString)
  {
    set("&dt", paramString);
  }
  
  public void setUseSecure(boolean paramBoolean)
  {
    set("useSecure", zzdg.zzc(paramBoolean));
  }
  
  public void setViewportSize(String paramString)
  {
    set("&vp", paramString);
  }
  
  final void zza(zzdf paramzzdf)
  {
    boolean bool2 = true;
    zzq("Loading Tracker config values");
    this.zztd = paramzzdf;
    int i;
    if (this.zztd.zzaci != null)
    {
      i = 1;
      if (i != 0)
      {
        paramzzdf = this.zztd.zzaci;
        set("&tid", paramzzdf);
        zza("trackingId loaded", paramzzdf);
      }
      if (this.zztd.zzacj < 0.0D) {
        break label253;
      }
      i = 1;
      label68:
      if (i != 0)
      {
        paramzzdf = Double.toString(this.zztd.zzacj);
        set("&sf", paramzzdf);
        zza("Sample frequency loaded", paramzzdf);
      }
      if (this.zztd.zzack < 0) {
        break label258;
      }
      i = 1;
      label111:
      if (i != 0)
      {
        i = this.zztd.zzack;
        setSessionTimeout(i);
        zza("Session timeout loaded", Integer.valueOf(i));
      }
      if (this.zztd.zzacl != -1)
      {
        if (this.zztd.zzacl != 1) {
          break label263;
        }
        bool1 = true;
        label164:
        enableAutoActivityTracking(bool1);
        zza("Auto activity tracking loaded", Boolean.valueOf(bool1));
      }
      if (this.zztd.zzacm != -1)
      {
        if (this.zztd.zzacm != 1) {
          break label268;
        }
        bool1 = true;
        label204:
        if (bool1) {
          set("&aip", "1");
        }
        zza("Anonymize ip loaded", Boolean.valueOf(bool1));
      }
      if (this.zztd.zzacn != 1) {
        break label273;
      }
    }
    label253:
    label258:
    label263:
    label268:
    label273:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      enableExceptionReporting(bool1);
      return;
      i = 0;
      break;
      i = 0;
      break label68;
      i = 0;
      break label111;
      bool1 = false;
      break label164;
      bool1 = false;
      break label204;
    }
  }
  
  protected final void zzag()
  {
    this.zztb.zzq();
    String str = zzce().zzaj();
    if (str != null) {
      set("&an", str);
    }
    str = zzce().zzak();
    if (str != null) {
      set("&av", str);
    }
  }
  
  final class zza
    extends zzau
    implements GoogleAnalytics.zza
  {
    private boolean zztm;
    private int zztn;
    private long zzto = -1L;
    private boolean zztp;
    private long zztq;
    
    protected zza(zzaw paramzzaw)
    {
      super();
    }
    
    private final void zzai()
    {
      if ((this.zzto >= 0L) || (this.zztm))
      {
        zzcb().zza(Tracker.zza(Tracker.this));
        return;
      }
      zzcb().zzb(Tracker.zza(Tracker.this));
    }
    
    public final void enableAutoActivityTracking(boolean paramBoolean)
    {
      this.zztm = paramBoolean;
      zzai();
    }
    
    public final void setSessionTimeout(long paramLong)
    {
      this.zzto = paramLong;
      zzai();
    }
    
    protected final void zzag() {}
    
    public final boolean zzah()
    {
      try
      {
        boolean bool = this.zztp;
        this.zztp = false;
        return bool;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
    
    public final void zzc(Activity paramActivity)
    {
      int i;
      if (this.zztn == 0)
      {
        if (zzbx().elapsedRealtime() < this.zztq + Math.max(1000L, this.zzto)) {
          break label231;
        }
        i = 1;
        if (i != 0) {
          this.zztp = true;
        }
      }
      this.zztn += 1;
      Object localObject1;
      HashMap localHashMap;
      if (this.zztm)
      {
        localObject1 = paramActivity.getIntent();
        if (localObject1 != null) {
          Tracker.this.setCampaignParamsOnNextHit(((Intent)localObject1).getData());
        }
        localHashMap = new HashMap();
        localHashMap.put("&t", "screenview");
        Tracker localTracker = Tracker.this;
        if (Tracker.zzk(Tracker.this) == null) {
          break label239;
        }
        Object localObject2 = Tracker.zzk(Tracker.this);
        localObject1 = paramActivity.getClass().getCanonicalName();
        localObject2 = (String)((zzdf)localObject2).zzaco.get(localObject1);
        if (localObject2 == null) {
          break label236;
        }
        localObject1 = localObject2;
        label161:
        localTracker.set("&cd", (String)localObject1);
        if (TextUtils.isEmpty((CharSequence)localHashMap.get("&dr")))
        {
          Preconditions.checkNotNull(paramActivity);
          paramActivity = paramActivity.getIntent();
          if (paramActivity != null) {
            break label250;
          }
          paramActivity = null;
        }
      }
      for (;;)
      {
        if (!TextUtils.isEmpty(paramActivity)) {
          localHashMap.put("&dr", paramActivity);
        }
        Tracker.this.send(localHashMap);
        return;
        label231:
        i = 0;
        break;
        label236:
        break label161;
        label239:
        localObject1 = paramActivity.getClass().getCanonicalName();
        break label161;
        label250:
        localObject1 = paramActivity.getStringExtra("android.intent.extra.REFERRER_NAME");
        paramActivity = (Activity)localObject1;
        if (TextUtils.isEmpty((CharSequence)localObject1)) {
          paramActivity = null;
        }
      }
    }
    
    public final void zzd(Activity paramActivity)
    {
      this.zztn -= 1;
      this.zztn = Math.max(0, this.zztn);
      if (this.zztn == 0) {
        this.zztq = zzbx().elapsedRealtime();
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\Tracker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */