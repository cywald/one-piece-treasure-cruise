package com.google.android.gms.analytics;

import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzaa;
import com.google.android.gms.internal.measurement.zzab;
import com.google.android.gms.internal.measurement.zzac;
import com.google.android.gms.internal.measurement.zzad;
import com.google.android.gms.internal.measurement.zzae;
import com.google.android.gms.internal.measurement.zzaf;
import com.google.android.gms.internal.measurement.zzag;
import com.google.android.gms.internal.measurement.zzah;
import com.google.android.gms.internal.measurement.zzai;
import com.google.android.gms.internal.measurement.zzaj;
import com.google.android.gms.internal.measurement.zzal;
import com.google.android.gms.internal.measurement.zzat;
import com.google.android.gms.internal.measurement.zzav;
import com.google.android.gms.internal.measurement.zzaw;
import com.google.android.gms.internal.measurement.zzaz;
import com.google.android.gms.internal.measurement.zzck;
import com.google.android.gms.internal.measurement.zzcp;
import com.google.android.gms.internal.measurement.zzdg;
import com.google.android.gms.internal.measurement.zzx;
import com.google.android.gms.internal.measurement.zzy;
import com.google.android.gms.internal.measurement.zzz;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class zzb
  extends zzat
  implements zzo
{
  private static DecimalFormat zzrb;
  private final zzaw zzqx;
  private final String zzrc;
  private final Uri zzrd;
  
  public zzb(zzaw paramzzaw, String paramString)
  {
    this(paramzzaw, paramString, true, false);
  }
  
  private zzb(zzaw paramzzaw, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramzzaw);
    Preconditions.checkNotEmpty(paramString);
    this.zzqx = paramzzaw;
    this.zzrc = paramString;
    this.zzrd = zzb(this.zzrc);
  }
  
  private static String zza(double paramDouble)
  {
    if (zzrb == null) {
      zzrb = new DecimalFormat("0.######");
    }
    return zzrb.format(paramDouble);
  }
  
  private static void zza(Map<String, String> paramMap, String paramString, double paramDouble)
  {
    if (paramDouble != 0.0D) {
      paramMap.put(paramString, zza(paramDouble));
    }
  }
  
  private static void zza(Map<String, String> paramMap, String paramString, int paramInt1, int paramInt2)
  {
    if ((paramInt1 > 0) && (paramInt2 > 0)) {
      paramMap.put(paramString, 23 + paramInt1 + "x" + paramInt2);
    }
  }
  
  private static void zza(Map<String, String> paramMap, String paramString1, String paramString2)
  {
    if (!TextUtils.isEmpty(paramString2)) {
      paramMap.put(paramString1, paramString2);
    }
  }
  
  private static void zza(Map<String, String> paramMap, String paramString, boolean paramBoolean)
  {
    if (paramBoolean) {
      paramMap.put(paramString, "1");
    }
  }
  
  static Uri zzb(String paramString)
  {
    Preconditions.checkNotEmpty(paramString);
    Uri.Builder localBuilder = new Uri.Builder();
    localBuilder.scheme("uri");
    localBuilder.authority("google-analytics.com");
    localBuilder.path(paramString);
    return localBuilder.build();
  }
  
  @VisibleForTesting
  private static Map<String, String> zzc(zzg paramzzg)
  {
    HashMap localHashMap = new HashMap();
    Object localObject1 = (zzab)paramzzg.zza(zzab.class);
    Object localObject3;
    Object localObject4;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject3 = ((zzab)localObject1).zzaw().entrySet().iterator();
      label198:
      while (((Iterator)localObject3).hasNext())
      {
        localObject4 = (Map.Entry)((Iterator)localObject3).next();
        localObject1 = ((Map.Entry)localObject4).getValue();
        if (localObject1 == null) {
          localObject1 = null;
        }
        for (;;)
        {
          if (localObject1 == null) {
            break label198;
          }
          localHashMap.put((String)((Map.Entry)localObject4).getKey(), localObject1);
          break;
          if ((localObject1 instanceof String))
          {
            localObject2 = (String)localObject1;
            localObject1 = localObject2;
            if (TextUtils.isEmpty((CharSequence)localObject2)) {
              localObject1 = null;
            }
          }
          else if ((localObject1 instanceof Double))
          {
            localObject1 = (Double)localObject1;
            if (((Double)localObject1).doubleValue() != 0.0D) {
              localObject1 = zza(((Double)localObject1).doubleValue());
            } else {
              localObject1 = null;
            }
          }
          else if ((localObject1 instanceof Boolean))
          {
            if (localObject1 != Boolean.FALSE) {
              localObject1 = "1";
            } else {
              localObject1 = null;
            }
          }
          else
          {
            localObject1 = String.valueOf(localObject1);
          }
        }
      }
    }
    localObject1 = (zzag)paramzzg.zza(zzag.class);
    if (localObject1 != null)
    {
      zza(localHashMap, "t", ((zzag)localObject1).zzbc());
      zza(localHashMap, "cid", ((zzag)localObject1).zzbd());
      zza(localHashMap, "uid", ((zzag)localObject1).zzbe());
      zza(localHashMap, "sc", ((zzag)localObject1).zzbh());
      zza(localHashMap, "sf", ((zzag)localObject1).zzbj());
      zza(localHashMap, "ni", ((zzag)localObject1).zzbi());
      zza(localHashMap, "adid", ((zzag)localObject1).zzbf());
      zza(localHashMap, "ate", ((zzag)localObject1).zzbg());
    }
    localObject1 = (zzah)paramzzg.zza(zzah.class);
    if (localObject1 != null)
    {
      zza(localHashMap, "cd", ((zzah)localObject1).zzbk());
      zza(localHashMap, "a", ((zzah)localObject1).zzbl());
      zza(localHashMap, "dr", ((zzah)localObject1).zzbm());
    }
    localObject1 = (zzae)paramzzg.zza(zzae.class);
    if (localObject1 != null)
    {
      zza(localHashMap, "ec", ((zzae)localObject1).zzbb());
      zza(localHashMap, "ea", ((zzae)localObject1).getAction());
      zza(localHashMap, "el", ((zzae)localObject1).getLabel());
      zza(localHashMap, "ev", ((zzae)localObject1).getValue());
    }
    localObject1 = (zzy)paramzzg.zza(zzy.class);
    if (localObject1 != null)
    {
      zza(localHashMap, "cn", ((zzy)localObject1).getName());
      zza(localHashMap, "cs", ((zzy)localObject1).getSource());
      zza(localHashMap, "cm", ((zzy)localObject1).zzan());
      zza(localHashMap, "ck", ((zzy)localObject1).zzao());
      zza(localHashMap, "cc", ((zzy)localObject1).zzap());
      zza(localHashMap, "ci", ((zzy)localObject1).getId());
      zza(localHashMap, "anid", ((zzy)localObject1).zzaq());
      zza(localHashMap, "gclid", ((zzy)localObject1).zzar());
      zza(localHashMap, "dclid", ((zzy)localObject1).zzas());
      zza(localHashMap, "aclid", ((zzy)localObject1).zzat());
    }
    localObject1 = (zzaf)paramzzg.zza(zzaf.class);
    if (localObject1 != null)
    {
      zza(localHashMap, "exd", ((zzaf)localObject1).zzum);
      zza(localHashMap, "exf", ((zzaf)localObject1).zzun);
    }
    localObject1 = (zzai)paramzzg.zza(zzai.class);
    if (localObject1 != null)
    {
      zza(localHashMap, "sn", ((zzai)localObject1).zzvd);
      zza(localHashMap, "sa", ((zzai)localObject1).zzve);
      zza(localHashMap, "st", ((zzai)localObject1).zzvf);
    }
    localObject1 = (zzaj)paramzzg.zza(zzaj.class);
    if (localObject1 != null)
    {
      zza(localHashMap, "utv", ((zzaj)localObject1).zzvg);
      zza(localHashMap, "utt", ((zzaj)localObject1).zzvh);
      zza(localHashMap, "utc", ((zzaj)localObject1).mCategory);
      zza(localHashMap, "utl", ((zzaj)localObject1).zzvi);
    }
    localObject1 = (zzz)paramzzg.zza(zzz.class);
    if (localObject1 != null)
    {
      localObject1 = ((zzz)localObject1).zzau().entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        localObject3 = zzd.zze(((Integer)((Map.Entry)localObject2).getKey()).intValue());
        if (!TextUtils.isEmpty((CharSequence)localObject3)) {
          localHashMap.put(localObject3, (String)((Map.Entry)localObject2).getValue());
        }
      }
    }
    localObject1 = (zzaa)paramzzg.zza(zzaa.class);
    if (localObject1 != null)
    {
      localObject1 = ((zzaa)localObject1).zzav().entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        localObject3 = zzd.zzg(((Integer)((Map.Entry)localObject2).getKey()).intValue());
        if (!TextUtils.isEmpty((CharSequence)localObject3)) {
          localHashMap.put(localObject3, zza(((Double)((Map.Entry)localObject2).getValue()).doubleValue()));
        }
      }
    }
    localObject1 = (zzad)paramzzg.zza(zzad.class);
    if (localObject1 != null)
    {
      localObject2 = ((zzad)localObject1).zzax();
      if (localObject2 != null)
      {
        localObject2 = ((ProductAction)localObject2).build().entrySet().iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (Map.Entry)((Iterator)localObject2).next();
          if (((String)((Map.Entry)localObject3).getKey()).startsWith("&")) {
            localHashMap.put(((String)((Map.Entry)localObject3).getKey()).substring(1), (String)((Map.Entry)localObject3).getValue());
          } else {
            localHashMap.put((String)((Map.Entry)localObject3).getKey(), (String)((Map.Entry)localObject3).getValue());
          }
        }
      }
      localObject2 = ((zzad)localObject1).zzba().iterator();
      int i = 1;
      while (((Iterator)localObject2).hasNext())
      {
        localHashMap.putAll(((Promotion)((Iterator)localObject2).next()).zzn(zzd.zzk(i)));
        i += 1;
      }
      localObject2 = ((zzad)localObject1).zzay().iterator();
      i = 1;
      while (((Iterator)localObject2).hasNext())
      {
        localHashMap.putAll(((Product)((Iterator)localObject2).next()).zzn(zzd.zzi(i)));
        i += 1;
      }
      localObject2 = ((zzad)localObject1).zzaz().entrySet().iterator();
      i = 1;
      if (((Iterator)localObject2).hasNext())
      {
        localObject3 = (Map.Entry)((Iterator)localObject2).next();
        localObject1 = (List)((Map.Entry)localObject3).getValue();
        localObject4 = zzd.zzn(i);
        Iterator localIterator = ((List)localObject1).iterator();
        int j = 1;
        if (localIterator.hasNext())
        {
          Product localProduct = (Product)localIterator.next();
          localObject1 = String.valueOf(localObject4);
          String str = String.valueOf(zzd.zzl(j));
          if (str.length() != 0) {}
          for (localObject1 = ((String)localObject1).concat(str);; localObject1 = new String((String)localObject1))
          {
            localHashMap.putAll(localProduct.zzn((String)localObject1));
            j += 1;
            break;
          }
        }
        if (!TextUtils.isEmpty((CharSequence)((Map.Entry)localObject3).getKey()))
        {
          localObject1 = String.valueOf(localObject4);
          localObject4 = String.valueOf("nm");
          if (((String)localObject4).length() == 0) {
            break label1385;
          }
        }
        label1385:
        for (localObject1 = ((String)localObject1).concat((String)localObject4);; localObject1 = new String((String)localObject1))
        {
          localHashMap.put(localObject1, (String)((Map.Entry)localObject3).getKey());
          i += 1;
          break;
        }
      }
    }
    localObject1 = (zzac)paramzzg.zza(zzac.class);
    if (localObject1 != null)
    {
      zza(localHashMap, "ul", ((zzac)localObject1).getLanguage());
      zza(localHashMap, "sd", ((zzac)localObject1).zzug);
      zza(localHashMap, "sr", ((zzac)localObject1).zzuh, ((zzac)localObject1).zzui);
      zza(localHashMap, "vp", ((zzac)localObject1).zzuj, ((zzac)localObject1).zzuk);
    }
    paramzzg = (zzx)paramzzg.zza(zzx.class);
    if (paramzzg != null)
    {
      zza(localHashMap, "an", paramzzg.zzaj());
      zza(localHashMap, "aid", paramzzg.zzal());
      zza(localHashMap, "aiid", paramzzg.zzam());
      zza(localHashMap, "av", paramzzg.zzak());
    }
    return localHashMap;
  }
  
  public final void zzb(zzg paramzzg)
  {
    Preconditions.checkNotNull(paramzzg);
    Preconditions.checkArgument(paramzzg.zzx(), "Can't deliver not submitted measurement");
    Preconditions.checkNotMainThread("deliver should be called on worker thread");
    Object localObject1 = paramzzg.zzs();
    Object localObject2 = (zzag)((zzg)localObject1).zzb(zzag.class);
    if (TextUtils.isEmpty(((zzag)localObject2).zzbc())) {
      zzby().zza(zzc((zzg)localObject1), "Ignoring measurement without type");
    }
    do
    {
      return;
      if (TextUtils.isEmpty(((zzag)localObject2).zzbd()))
      {
        zzby().zza(zzc((zzg)localObject1), "Ignoring measurement without client id");
        return;
      }
    } while (this.zzqx.zzco().getAppOptOut());
    double d = ((zzag)localObject2).zzbj();
    if (zzdg.zza(d, ((zzag)localObject2).zzbd()))
    {
      zzb("Sampling enabled. Hit sampled out. sampling rate", Double.valueOf(d));
      return;
    }
    localObject1 = zzc((zzg)localObject1);
    ((Map)localObject1).put("v", "1");
    ((Map)localObject1).put("_v", zzav.zzwa);
    ((Map)localObject1).put("tid", this.zzrc);
    if (this.zzqx.zzco().isDryRunEnabled())
    {
      paramzzg = new StringBuilder();
      localObject1 = ((Map)localObject1).entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        if (paramzzg.length() != 0) {
          paramzzg.append(", ");
        }
        paramzzg.append((String)((Map.Entry)localObject2).getKey());
        paramzzg.append("=");
        paramzzg.append((String)((Map.Entry)localObject2).getValue());
      }
      zzc("Dry run is enabled. GoogleAnalytics would have sent", paramzzg.toString());
      return;
    }
    HashMap localHashMap = new HashMap();
    zzdg.zzb(localHashMap, "uid", ((zzag)localObject2).zzbe());
    Object localObject3 = (zzx)paramzzg.zza(zzx.class);
    if (localObject3 != null)
    {
      zzdg.zzb(localHashMap, "an", ((zzx)localObject3).zzaj());
      zzdg.zzb(localHashMap, "aid", ((zzx)localObject3).zzal());
      zzdg.zzb(localHashMap, "av", ((zzx)localObject3).zzak());
      zzdg.zzb(localHashMap, "aiid", ((zzx)localObject3).zzam());
    }
    localObject3 = ((zzag)localObject2).zzbd();
    String str = this.zzrc;
    if (!TextUtils.isEmpty(((zzag)localObject2).zzbf())) {}
    for (boolean bool = true;; bool = false)
    {
      localObject2 = new zzaz(0L, (String)localObject3, str, bool, 0L, localHashMap);
      ((Map)localObject1).put("_s", String.valueOf(zzcc().zza((zzaz)localObject2)));
      paramzzg = new zzck(zzby(), (Map)localObject1, paramzzg.zzv(), true);
      zzcc().zza(paramzzg);
      return;
    }
  }
  
  public final Uri zzo()
  {
    return this.zzrd;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */