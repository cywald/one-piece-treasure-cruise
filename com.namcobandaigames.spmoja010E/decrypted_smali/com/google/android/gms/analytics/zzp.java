package com.google.android.gms.analytics;

import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.measurement.zzac;
import com.google.android.gms.internal.measurement.zzak;
import com.google.android.gms.internal.measurement.zzal;
import com.google.android.gms.internal.measurement.zzat;
import com.google.android.gms.internal.measurement.zzav;
import com.google.android.gms.internal.measurement.zzaw;
import com.google.android.gms.internal.measurement.zzaz;
import com.google.android.gms.internal.measurement.zzbh;
import com.google.android.gms.internal.measurement.zzbo;
import com.google.android.gms.internal.measurement.zzcb;
import com.google.android.gms.internal.measurement.zzck;
import com.google.android.gms.internal.measurement.zzcn;
import com.google.android.gms.internal.measurement.zzcp;
import com.google.android.gms.internal.measurement.zzdg;
import com.google.android.gms.internal.measurement.zzx;
import java.util.HashMap;
import java.util.Map;

final class zzp
  implements Runnable
{
  zzp(Tracker paramTracker, Map paramMap, boolean paramBoolean1, String paramString1, long paramLong, boolean paramBoolean2, boolean paramBoolean3, String paramString2) {}
  
  public final void run()
  {
    boolean bool = true;
    if (Tracker.zza(this.zztl).zzah()) {
      this.zzte.put("sc", "start");
    }
    Object localObject1 = this.zzte;
    Object localObject2 = this.zztl.zzcb();
    Preconditions.checkNotMainThread("getClientId can not be called from the main thread");
    zzdg.zzc((Map)localObject1, "cid", ((zza)localObject2).zzl().zzcr().zzdr());
    localObject1 = (String)this.zzte.get("sf");
    if (localObject1 != null)
    {
      double d = zzdg.zza((String)localObject1, 100.0D);
      if (zzdg.zza(d, (String)this.zzte.get("cid")))
      {
        this.zztl.zzb("Sampling enabled. Hit sampled out. sample rate", Double.valueOf(d));
        return;
      }
    }
    localObject1 = Tracker.zzb(this.zztl);
    if (this.zztf)
    {
      zzdg.zzb(this.zzte, "ate", ((zzak)localObject1).zzbg());
      zzdg.zzb(this.zzte, "adid", ((zzak)localObject1).zzbn());
      localObject1 = Tracker.zzc(this.zztl).zzdf();
      zzdg.zzb(this.zzte, "an", ((zzx)localObject1).zzaj());
      zzdg.zzb(this.zzte, "av", ((zzx)localObject1).zzak());
      zzdg.zzb(this.zzte, "aid", ((zzx)localObject1).zzal());
      zzdg.zzb(this.zzte, "aiid", ((zzx)localObject1).zzam());
      this.zzte.put("v", "1");
      this.zzte.put("_v", zzav.zzwa);
      zzdg.zzb(this.zzte, "ul", Tracker.zzd(this.zztl).zzek().getLanguage());
      zzdg.zzb(this.zzte, "sr", Tracker.zze(this.zztl).zzel());
      if ((!this.zztg.equals("transaction")) && (!this.zztg.equals("item"))) {
        break label402;
      }
    }
    label402:
    for (int i = 1;; i = 0)
    {
      if ((i != 0) || (Tracker.zzf(this.zztl).zzew())) {
        break label407;
      }
      Tracker.zzg(this.zztl).zza(this.zzte, "Too many hits sent too quickly, rate limiting invoked");
      return;
      this.zzte.remove("ate");
      this.zzte.remove("adid");
      break;
    }
    label407:
    long l2 = zzdg.zzaf((String)this.zzte.get("ht"));
    long l1 = l2;
    if (l2 == 0L) {
      l1 = this.zzth;
    }
    if (this.zzti)
    {
      localObject1 = new zzck(this.zztl, this.zzte, l1, this.zztj);
      Tracker.zzh(this.zztl).zzc("Dry run enabled. Would have sent hit", localObject1);
      return;
    }
    localObject1 = (String)this.zzte.get("cid");
    localObject2 = new HashMap();
    zzdg.zza((Map)localObject2, "uid", this.zzte);
    zzdg.zza((Map)localObject2, "an", this.zzte);
    zzdg.zza((Map)localObject2, "aid", this.zzte);
    zzdg.zza((Map)localObject2, "av", this.zzte);
    zzdg.zza((Map)localObject2, "aiid", this.zzte);
    String str = this.zztk;
    if (!TextUtils.isEmpty((CharSequence)this.zzte.get("adid"))) {}
    for (;;)
    {
      localObject1 = new zzaz(0L, (String)localObject1, str, bool, 0L, (Map)localObject2);
      l2 = Tracker.zzi(this.zztl).zza((zzaz)localObject1);
      this.zzte.put("_s", String.valueOf(l2));
      localObject1 = new zzck(this.zztl, this.zzte, l1, this.zztj);
      Tracker.zzj(this.zztl).zza((zzck)localObject1);
      return;
      bool = false;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */