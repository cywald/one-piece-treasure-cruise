package com.google.android.gms.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.internal.measurement.zzcw;

public final class AnalyticsReceiver
  extends BroadcastReceiver
{
  private zzcw zzra;
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (this.zzra == null) {
      this.zzra = new zzcw();
    }
    zzcw.onReceive(paramContext, paramIntent);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\AnalyticsReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */