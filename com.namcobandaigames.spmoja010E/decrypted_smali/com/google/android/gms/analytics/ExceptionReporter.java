package com.google.android.gms.analytics;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzal;
import com.google.android.gms.internal.measurement.zzaw;
import com.google.android.gms.internal.measurement.zzco;
import java.util.ArrayList;

@VisibleForTesting
public class ExceptionReporter
  implements Thread.UncaughtExceptionHandler
{
  private final Thread.UncaughtExceptionHandler zzrg;
  private final Tracker zzrh;
  private final Context zzri;
  private ExceptionParser zzrj;
  private GoogleAnalytics zzrk;
  
  public ExceptionReporter(Tracker paramTracker, Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler, Context paramContext)
  {
    if (paramTracker == null) {
      throw new NullPointerException("tracker cannot be null");
    }
    if (paramContext == null) {
      throw new NullPointerException("context cannot be null");
    }
    this.zzrg = paramUncaughtExceptionHandler;
    this.zzrh = paramTracker;
    this.zzrj = new StandardExceptionParser(paramContext, new ArrayList());
    this.zzri = paramContext.getApplicationContext();
    if (paramUncaughtExceptionHandler == null)
    {
      paramTracker = "null";
      paramTracker = String.valueOf(paramTracker);
      if (paramTracker.length() == 0) {
        break label111;
      }
    }
    label111:
    for (paramTracker = "ExceptionReporter created, original handler is ".concat(paramTracker);; paramTracker = new String("ExceptionReporter created, original handler is "))
    {
      zzco.v(paramTracker);
      return;
      paramTracker = paramUncaughtExceptionHandler.getClass().getName();
      break;
    }
  }
  
  public ExceptionParser getExceptionParser()
  {
    return this.zzrj;
  }
  
  public void setExceptionParser(ExceptionParser paramExceptionParser)
  {
    this.zzrj = paramExceptionParser;
  }
  
  public void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    Object localObject = "UncaughtException";
    if (this.zzrj != null)
    {
      if (paramThread != null)
      {
        localObject = paramThread.getName();
        localObject = this.zzrj.getDescription((String)localObject, paramThrowable);
      }
    }
    else
    {
      str = String.valueOf(localObject);
      if (str.length() == 0) {
        break label151;
      }
    }
    label151:
    for (String str = "Reporting uncaught exception: ".concat(str);; str = new String("Reporting uncaught exception: "))
    {
      zzco.v(str);
      this.zzrh.send(new HitBuilders.ExceptionBuilder().setDescription((String)localObject).setFatal(true).build());
      if (this.zzrk == null) {
        this.zzrk = GoogleAnalytics.getInstance(this.zzri);
      }
      localObject = this.zzrk;
      ((GoogleAnalytics)localObject).dispatchLocalHits();
      ((zza)localObject).zzl().zzcc().zzbt();
      if (this.zzrg != null)
      {
        zzco.v("Passing exception to the original handler");
        this.zzrg.uncaughtException(paramThread, paramThrowable);
      }
      return;
      localObject = null;
      break;
    }
  }
  
  final Thread.UncaughtExceptionHandler zzp()
  {
    return this.zzrg;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\ExceptionReporter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */