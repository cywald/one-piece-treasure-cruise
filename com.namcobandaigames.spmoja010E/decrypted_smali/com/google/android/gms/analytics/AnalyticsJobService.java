package com.google.android.gms.analytics;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.internal.measurement.zzcx;
import com.google.android.gms.internal.measurement.zzdb;

@TargetApi(24)
public final class AnalyticsJobService
  extends JobService
  implements zzdb
{
  private zzcx<AnalyticsJobService> zzqz;
  
  private final zzcx<AnalyticsJobService> zzn()
  {
    if (this.zzqz == null) {
      this.zzqz = new zzcx(this);
    }
    return this.zzqz;
  }
  
  public final boolean callServiceStopSelfResult(int paramInt)
  {
    return stopSelfResult(paramInt);
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public final void onCreate()
  {
    super.onCreate();
    zzn().onCreate();
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public final void onDestroy()
  {
    zzn().onDestroy();
    super.onDestroy();
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    return zzn().onStartCommand(paramIntent, paramInt1, paramInt2);
  }
  
  public final boolean onStartJob(JobParameters paramJobParameters)
  {
    return zzn().onStartJob(paramJobParameters);
  }
  
  public final boolean onStopJob(JobParameters paramJobParameters)
  {
    return false;
  }
  
  @TargetApi(24)
  public final void zza(JobParameters paramJobParameters, boolean paramBoolean)
  {
    jobFinished(paramJobParameters, false);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\AnalyticsJobService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */