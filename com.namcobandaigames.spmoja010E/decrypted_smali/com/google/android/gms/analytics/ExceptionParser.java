package com.google.android.gms.analytics;

public abstract interface ExceptionParser
{
  public abstract String getDescription(String paramString, Throwable paramThrowable);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\analytics\ExceptionParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */