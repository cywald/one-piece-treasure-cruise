package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.PackageManagerWrapper;
import com.google.android.gms.common.wrappers.Wrappers;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class zzn
  extends zzco
{
  private Boolean zzahf;
  @NonNull
  private zzp zzahg = zzo.zzahh;
  private Boolean zzyk;
  
  zzn(zzbt paramzzbt)
  {
    super(paramzzbt);
    zzaf.zza(paramzzbt);
  }
  
  static String zzht()
  {
    return (String)zzaf.zzajd.get();
  }
  
  public static long zzhw()
  {
    return ((Long)zzaf.zzakg.get()).longValue();
  }
  
  public static long zzhx()
  {
    return ((Long)zzaf.zzajg.get()).longValue();
  }
  
  public static boolean zzhz()
  {
    return ((Boolean)zzaf.zzajc.get()).booleanValue();
  }
  
  static boolean zzia()
  {
    return ((Boolean)zzaf.zzalb.get()).booleanValue();
  }
  
  @WorkerThread
  static boolean zzic()
  {
    return ((Boolean)zzaf.zzald.get()).booleanValue();
  }
  
  @WorkerThread
  public final long zza(String paramString, @NonNull zzaf.zza<Long> paramzza)
  {
    if (paramString == null) {
      return ((Long)paramzza.get()).longValue();
    }
    paramString = this.zzahg.zzf(paramString, paramzza.getKey());
    if (TextUtils.isEmpty(paramString)) {
      return ((Long)paramzza.get()).longValue();
    }
    try
    {
      long l = ((Long)paramzza.get(Long.valueOf(Long.parseLong(paramString)))).longValue();
      return l;
    }
    catch (NumberFormatException paramString) {}
    return ((Long)paramzza.get()).longValue();
  }
  
  final void zza(@NonNull zzp paramzzp)
  {
    this.zzahg = paramzzp;
  }
  
  public final boolean zza(zzaf.zza<Boolean> paramzza)
  {
    return zzd(null, paramzza);
  }
  
  @WorkerThread
  public final int zzat(@Size(min=1L) String paramString)
  {
    return zzb(paramString, zzaf.zzajr);
  }
  
  @Nullable
  @VisibleForTesting
  final Boolean zzau(@Size(min=1L) String paramString)
  {
    Boolean localBoolean = null;
    Preconditions.checkNotEmpty(paramString);
    ApplicationInfo localApplicationInfo;
    try
    {
      if (getContext().getPackageManager() == null)
      {
        zzgo().zzjd().zzbx("Failed to load metadata: PackageManager is null");
        return null;
      }
      localApplicationInfo = Wrappers.packageManager(getContext()).getApplicationInfo(getContext().getPackageName(), 128);
      if (localApplicationInfo == null)
      {
        zzgo().zzjd().zzbx("Failed to load metadata: ApplicationInfo is null");
        return null;
      }
    }
    catch (PackageManager.NameNotFoundException paramString)
    {
      zzgo().zzjd().zzg("Failed to load metadata: Package name not found", paramString);
      return null;
    }
    if (localApplicationInfo.metaData == null)
    {
      zzgo().zzjd().zzbx("Failed to load metadata: Metadata bundle is null");
      return null;
    }
    if (localApplicationInfo.metaData.containsKey(paramString))
    {
      boolean bool = localApplicationInfo.metaData.getBoolean(paramString);
      localBoolean = Boolean.valueOf(bool);
    }
    return localBoolean;
  }
  
  public final boolean zzav(String paramString)
  {
    return "1".equals(this.zzahg.zzf(paramString, "gaia_collection_enabled"));
  }
  
  public final boolean zzaw(String paramString)
  {
    return "1".equals(this.zzahg.zzf(paramString, "measurement.event_sampling_enabled"));
  }
  
  @WorkerThread
  final boolean zzax(String paramString)
  {
    return zzd(paramString, zzaf.zzakp);
  }
  
  @WorkerThread
  final boolean zzay(String paramString)
  {
    return zzd(paramString, zzaf.zzakr);
  }
  
  @WorkerThread
  final boolean zzaz(String paramString)
  {
    return zzd(paramString, zzaf.zzaks);
  }
  
  @WorkerThread
  public final int zzb(String paramString, @NonNull zzaf.zza<Integer> paramzza)
  {
    if (paramString == null) {
      return ((Integer)paramzza.get()).intValue();
    }
    paramString = this.zzahg.zzf(paramString, paramzza.getKey());
    if (TextUtils.isEmpty(paramString)) {
      return ((Integer)paramzza.get()).intValue();
    }
    try
    {
      int i = ((Integer)paramzza.get(Integer.valueOf(Integer.parseInt(paramString)))).intValue();
      return i;
    }
    catch (NumberFormatException paramString) {}
    return ((Integer)paramzza.get()).intValue();
  }
  
  @WorkerThread
  final boolean zzba(String paramString)
  {
    return zzd(paramString, zzaf.zzakk);
  }
  
  @WorkerThread
  final String zzbb(String paramString)
  {
    zzaf.zza localzza = zzaf.zzakl;
    if (paramString == null) {
      return (String)localzza.get();
    }
    return (String)localzza.get(this.zzahg.zzf(paramString, localzza.getKey()));
  }
  
  final boolean zzbc(String paramString)
  {
    return zzd(paramString, zzaf.zzakt);
  }
  
  @WorkerThread
  final boolean zzbd(String paramString)
  {
    return zzd(paramString, zzaf.zzaku);
  }
  
  @WorkerThread
  final boolean zzbe(String paramString)
  {
    return zzd(paramString, zzaf.zzakx);
  }
  
  @WorkerThread
  final boolean zzbf(String paramString)
  {
    return zzd(paramString, zzaf.zzaky);
  }
  
  @WorkerThread
  final boolean zzbg(String paramString)
  {
    return zzd(paramString, zzaf.zzala);
  }
  
  @WorkerThread
  final boolean zzbh(String paramString)
  {
    return zzd(paramString, zzaf.zzakz);
  }
  
  @WorkerThread
  final boolean zzbi(String paramString)
  {
    return zzd(paramString, zzaf.zzale);
  }
  
  @WorkerThread
  final boolean zzbj(String paramString)
  {
    return zzd(paramString, zzaf.zzalf);
  }
  
  @WorkerThread
  public final double zzc(String paramString, @NonNull zzaf.zza<Double> paramzza)
  {
    if (paramString == null) {
      return ((Double)paramzza.get()).doubleValue();
    }
    paramString = this.zzahg.zzf(paramString, paramzza.getKey());
    if (TextUtils.isEmpty(paramString)) {
      return ((Double)paramzza.get()).doubleValue();
    }
    try
    {
      double d = ((Double)paramzza.get(Double.valueOf(Double.parseDouble(paramString)))).doubleValue();
      return d;
    }
    catch (NumberFormatException paramString) {}
    return ((Double)paramzza.get()).doubleValue();
  }
  
  @WorkerThread
  public final boolean zzd(String paramString, @NonNull zzaf.zza<Boolean> paramzza)
  {
    if (paramString == null) {
      return ((Boolean)paramzza.get()).booleanValue();
    }
    paramString = this.zzahg.zzf(paramString, paramzza.getKey());
    if (TextUtils.isEmpty(paramString)) {
      return ((Boolean)paramzza.get()).booleanValue();
    }
    return ((Boolean)paramzza.get(Boolean.valueOf(Boolean.parseBoolean(paramString)))).booleanValue();
  }
  
  /* Error */
  public final boolean zzdw()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 317	com/google/android/gms/measurement/internal/zzn:zzyk	Ljava/lang/Boolean;
    //   4: ifnonnull +84 -> 88
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield 317	com/google/android/gms/measurement/internal/zzn:zzyk	Ljava/lang/Boolean;
    //   13: ifnonnull +73 -> 86
    //   16: aload_0
    //   17: invokevirtual 79	com/google/android/gms/measurement/internal/zzco:getContext	()Landroid/content/Context;
    //   20: invokevirtual 320	android/content/Context:getApplicationInfo	()Landroid/content/pm/ApplicationInfo;
    //   23: astore_3
    //   24: invokestatic 325	com/google/android/gms/common/util/ProcessUtils:getMyProcessName	()Ljava/lang/String;
    //   27: astore_2
    //   28: aload_3
    //   29: ifnull +30 -> 59
    //   32: aload_3
    //   33: getfield 329	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
    //   36: astore_3
    //   37: aload_3
    //   38: ifnull +58 -> 96
    //   41: aload_3
    //   42: aload_2
    //   43: invokevirtual 223	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   46: ifeq +50 -> 96
    //   49: iconst_1
    //   50: istore_1
    //   51: aload_0
    //   52: iload_1
    //   53: invokestatic 214	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   56: putfield 317	com/google/android/gms/measurement/internal/zzn:zzyk	Ljava/lang/Boolean;
    //   59: aload_0
    //   60: getfield 317	com/google/android/gms/measurement/internal/zzn:zzyk	Ljava/lang/Boolean;
    //   63: ifnonnull +23 -> 86
    //   66: aload_0
    //   67: getstatic 332	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   70: putfield 317	com/google/android/gms/measurement/internal/zzn:zzyk	Ljava/lang/Boolean;
    //   73: aload_0
    //   74: invokevirtual 157	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   77: invokevirtual 163	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   80: ldc_w 334
    //   83: invokevirtual 171	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   86: aload_0
    //   87: monitorexit
    //   88: aload_0
    //   89: getfield 317	com/google/android/gms/measurement/internal/zzn:zzyk	Ljava/lang/Boolean;
    //   92: invokevirtual 65	java/lang/Boolean:booleanValue	()Z
    //   95: ireturn
    //   96: iconst_0
    //   97: istore_1
    //   98: goto -47 -> 51
    //   101: astore_2
    //   102: aload_0
    //   103: monitorexit
    //   104: aload_2
    //   105: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	106	0	this	zzn
    //   50	48	1	bool	boolean
    //   27	16	2	str	String
    //   101	4	2	localObject1	Object
    //   23	19	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   9	28	101	finally
    //   32	37	101	finally
    //   41	49	101	finally
    //   51	59	101	finally
    //   59	86	101	finally
    //   86	88	101	finally
    //   102	104	101	finally
  }
  
  public final boolean zze(String paramString, zzaf.zza<Boolean> paramzza)
  {
    return zzd(paramString, paramzza);
  }
  
  public final long zzhc()
  {
    zzgr();
    return 13001L;
  }
  
  public final boolean zzhu()
  {
    zzgr();
    Boolean localBoolean = zzau("firebase_analytics_collection_deactivated");
    return (localBoolean != null) && (localBoolean.booleanValue());
  }
  
  public final Boolean zzhv()
  {
    zzgr();
    return zzau("firebase_analytics_collection_enabled");
  }
  
  public final String zzhy()
  {
    try
    {
      String str = (String)Class.forName("android.os.SystemProperties").getMethod("get", new Class[] { String.class, String.class }).invoke(null, new Object[] { "debug.firebase.analytics.app", "" });
      return str;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      zzgo().zzjd().zzg("Could not find SystemProperties class", localClassNotFoundException);
      return "";
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      for (;;)
      {
        zzgo().zzjd().zzg("Could not find SystemProperties.get() method", localNoSuchMethodException);
      }
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      for (;;)
      {
        zzgo().zzjd().zzg("Could not access SystemProperties.get()", localIllegalAccessException);
      }
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      for (;;)
      {
        zzgo().zzjd().zzg("SystemProperties.get() threw an exception", localInvocationTargetException);
      }
    }
  }
  
  @WorkerThread
  final boolean zzib()
  {
    boolean bool = false;
    if (this.zzahf == null)
    {
      this.zzahf = zzau("app_measurement_lite");
      if (this.zzahf == null) {
        this.zzahf = Boolean.valueOf(false);
      }
    }
    if ((this.zzahf.booleanValue()) || (!this.zzadj.zzkn())) {
      bool = true;
    }
    return bool;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */