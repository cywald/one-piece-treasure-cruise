package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;

final class zzg
{
  private final zzbt zzadj;
  private long zzadt;
  private String zzafw;
  private String zzafx;
  private String zzafy;
  private String zzafz;
  private long zzaga;
  private long zzagb;
  private long zzagc;
  private long zzagd;
  private String zzage;
  private long zzagf;
  private boolean zzagg;
  private long zzagh;
  private boolean zzagi;
  private boolean zzagj;
  private String zzagk;
  private long zzagl;
  private long zzagm;
  private long zzagn;
  private long zzago;
  private long zzagp;
  private long zzagq;
  private String zzagr;
  private boolean zzags;
  private long zzagt;
  private long zzagu;
  private String zzts;
  private final String zztt;
  
  @WorkerThread
  zzg(zzbt paramzzbt, String paramString)
  {
    Preconditions.checkNotNull(paramzzbt);
    Preconditions.checkNotEmpty(paramString);
    this.zzadj = paramzzbt;
    this.zztt = paramString;
    this.zzadj.zzgn().zzaf();
  }
  
  @WorkerThread
  public final String getAppInstanceId()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzafw;
  }
  
  @WorkerThread
  public final String getFirebaseInstanceId()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzafz;
  }
  
  @WorkerThread
  public final String getGmpAppId()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzafx;
  }
  
  @WorkerThread
  public final boolean isMeasurementEnabled()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagg;
  }
  
  @WorkerThread
  public final void setAppVersion(String paramString)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (!zzfk.zzu(this.zzts, paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzts = paramString;
      return;
    }
  }
  
  @WorkerThread
  public final void setMeasurementEnabled(boolean paramBoolean)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagg != paramBoolean) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagg = paramBoolean;
      return;
    }
  }
  
  @WorkerThread
  public final void zzaa(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagl != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagl = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzab(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagm != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagm = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzac(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagn != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagn = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzad(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzago != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzago = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzae(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagq != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagq = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzaf(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagp != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagp = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzag(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagh != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagh = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final String zzak()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzts;
  }
  
  @WorkerThread
  public final String zzal()
  {
    this.zzadj.zzgn().zzaf();
    return this.zztt;
  }
  
  @WorkerThread
  public final void zzam(String paramString)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (!zzfk.zzu(this.zzafw, paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzafw = paramString;
      return;
    }
  }
  
  @WorkerThread
  public final void zzan(String paramString)
  {
    this.zzadj.zzgn().zzaf();
    String str = paramString;
    if (TextUtils.isEmpty(paramString)) {
      str = null;
    }
    boolean bool2 = this.zzags;
    if (!zzfk.zzu(this.zzafx, str)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzafx = str;
      return;
    }
  }
  
  @WorkerThread
  public final void zzao(String paramString)
  {
    this.zzadj.zzgn().zzaf();
    String str = paramString;
    if (TextUtils.isEmpty(paramString)) {
      str = null;
    }
    boolean bool2 = this.zzags;
    if (!zzfk.zzu(this.zzagk, str)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagk = str;
      return;
    }
  }
  
  @WorkerThread
  public final void zzap(String paramString)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (!zzfk.zzu(this.zzafy, paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzafy = paramString;
      return;
    }
  }
  
  @WorkerThread
  public final void zzaq(String paramString)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (!zzfk.zzu(this.zzafz, paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzafz = paramString;
      return;
    }
  }
  
  @WorkerThread
  public final void zzar(String paramString)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (!zzfk.zzu(this.zzage, paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzage = paramString;
      return;
    }
  }
  
  @WorkerThread
  public final void zzas(String paramString)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (!zzfk.zzu(this.zzagr, paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagr = paramString;
      return;
    }
  }
  
  @WorkerThread
  public final void zze(boolean paramBoolean)
  {
    this.zzadj.zzgn().zzaf();
    if (this.zzagi != paramBoolean) {}
    for (boolean bool = true;; bool = false)
    {
      this.zzags = bool;
      this.zzagi = paramBoolean;
      return;
    }
  }
  
  @WorkerThread
  public final void zzf(boolean paramBoolean)
  {
    this.zzadj.zzgn().zzaf();
    if (this.zzagj != paramBoolean) {}
    for (boolean bool = true;; bool = false)
    {
      this.zzags = bool;
      this.zzagj = paramBoolean;
      return;
    }
  }
  
  @WorkerThread
  public final void zzgv()
  {
    this.zzadj.zzgn().zzaf();
    this.zzags = false;
  }
  
  @WorkerThread
  public final String zzgw()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagk;
  }
  
  @WorkerThread
  public final String zzgx()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzafy;
  }
  
  @WorkerThread
  public final long zzgy()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagb;
  }
  
  @WorkerThread
  public final long zzgz()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagc;
  }
  
  @WorkerThread
  public final long zzha()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagd;
  }
  
  @WorkerThread
  public final String zzhb()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzage;
  }
  
  @WorkerThread
  public final long zzhc()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzadt;
  }
  
  @WorkerThread
  public final long zzhd()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagf;
  }
  
  @WorkerThread
  public final long zzhe()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzaga;
  }
  
  @WorkerThread
  public final long zzhf()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagt;
  }
  
  @WorkerThread
  public final long zzhg()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagu;
  }
  
  @WorkerThread
  public final void zzhh()
  {
    this.zzadj.zzgn().zzaf();
    long l2 = this.zzaga + 1L;
    long l1 = l2;
    if (l2 > 2147483647L)
    {
      this.zzadj.zzgo().zzjg().zzg("Bundle index overflow. appId", zzap.zzbv(this.zztt));
      l1 = 0L;
    }
    this.zzags = true;
    this.zzaga = l1;
  }
  
  @WorkerThread
  public final long zzhi()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagl;
  }
  
  @WorkerThread
  public final long zzhj()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagm;
  }
  
  @WorkerThread
  public final long zzhk()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagn;
  }
  
  @WorkerThread
  public final long zzhl()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzago;
  }
  
  @WorkerThread
  public final long zzhm()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagq;
  }
  
  @WorkerThread
  public final long zzhn()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagp;
  }
  
  @WorkerThread
  public final String zzho()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagr;
  }
  
  @WorkerThread
  public final String zzhp()
  {
    this.zzadj.zzgn().zzaf();
    String str = this.zzagr;
    zzas(null);
    return str;
  }
  
  @WorkerThread
  public final long zzhq()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagh;
  }
  
  @WorkerThread
  public final boolean zzhr()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagi;
  }
  
  @WorkerThread
  public final boolean zzhs()
  {
    this.zzadj.zzgn().zzaf();
    return this.zzagj;
  }
  
  @WorkerThread
  public final void zzs(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagb != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagb = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzt(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagc != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagc = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzu(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagd != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagd = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzv(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzadt != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzadt = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzw(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagf != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagf = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzx(long paramLong)
  {
    boolean bool1 = true;
    boolean bool2;
    if (paramLong >= 0L)
    {
      bool2 = true;
      Preconditions.checkArgument(bool2);
      this.zzadj.zzgn().zzaf();
      bool2 = this.zzags;
      if (this.zzaga == paramLong) {
        break label61;
      }
    }
    for (;;)
    {
      this.zzags = (bool2 | bool1);
      this.zzaga = paramLong;
      return;
      bool2 = false;
      break;
      label61:
      bool1 = false;
    }
  }
  
  @WorkerThread
  public final void zzy(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagt != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagt = paramLong;
      return;
    }
  }
  
  @WorkerThread
  public final void zzz(long paramLong)
  {
    this.zzadj.zzgn().zzaf();
    boolean bool2 = this.zzags;
    if (this.zzagu != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzags = (bool1 | bool2);
      this.zzagu = paramLong;
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */