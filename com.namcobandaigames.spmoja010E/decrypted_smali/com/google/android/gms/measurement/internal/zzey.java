package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;

class zzey
  extends zzco
  implements zzcq
{
  protected final zzfa zzamz;
  
  zzey(zzfa paramzzfa)
  {
    super(paramzzfa.zzmb());
    Preconditions.checkNotNull(paramzzfa);
    this.zzamz = paramzzfa;
  }
  
  public zzfg zzjo()
  {
    return this.zzamz.zzjo();
  }
  
  public zzj zzjp()
  {
    return this.zzamz.zzjp();
  }
  
  public zzq zzjq()
  {
    return this.zzamz.zzjq();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */