package com.google.android.gms.measurement.internal;

final class zzaq
  implements Runnable
{
  zzaq(zzap paramzzap, int paramInt, String paramString, Object paramObject1, Object paramObject2, Object paramObject3) {}
  
  public final void run()
  {
    zzba localzzba = this.zzamm.zzadj.zzgp();
    if (!localzzba.isInitialized())
    {
      this.zzamm.zza(6, "Persisted config not initialized. Not logging error/warn");
      return;
    }
    Object localObject;
    if (zzap.zza(this.zzamm) == 0)
    {
      if (!this.zzamm.zzgq().zzdw()) {
        break label245;
      }
      localObject = this.zzamm;
      this.zzamm.zzgr();
      zzap.zza((zzap)localObject, 'C');
    }
    for (;;)
    {
      if (zzap.zzb(this.zzamm) < 0L) {
        zzap.zza(this.zzamm, this.zzamm.zzgq().zzhc());
      }
      char c1 = "01VDIWEA?".charAt(this.zzamh);
      char c2 = zzap.zza(this.zzamm);
      long l = zzap.zzb(this.zzamm);
      localObject = zzap.zza(true, this.zzami, this.zzamj, this.zzamk, this.zzaml);
      String str = String.valueOf(localObject).length() + 24 + "2" + c1 + c2 + l + ":" + (String)localObject;
      localObject = str;
      if (str.length() > 1024) {
        localObject = this.zzami.substring(0, 1024);
      }
      localzzba.zzand.zzc((String)localObject, 1L);
      return;
      label245:
      localObject = this.zzamm;
      this.zzamm.zzgr();
      zzap.zza((zzap)localObject, 'c');
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzaq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */