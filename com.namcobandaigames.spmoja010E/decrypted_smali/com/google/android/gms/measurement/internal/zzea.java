package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

final class zzea
  implements Runnable
{
  zzea(zzdr paramzzdr, boolean paramBoolean1, boolean paramBoolean2, zzl paramzzl1, zzh paramzzh, zzl paramzzl2) {}
  
  public final void run()
  {
    zzag localzzag = zzdr.zzd(this.zzasg);
    if (localzzag == null)
    {
      this.zzasg.zzgo().zzjd().zzbx("Discarding data. Failed to send conditional user property to service");
      return;
    }
    Object localObject;
    if (this.zzasi)
    {
      zzdr localzzdr = this.zzasg;
      if (this.zzasj)
      {
        localObject = null;
        localzzdr.zza(localzzag, (AbstractSafeParcelable)localObject, this.zzaqn);
      }
    }
    for (;;)
    {
      zzdr.zze(this.zzasg);
      return;
      localObject = this.zzask;
      break;
      try
      {
        if (!TextUtils.isEmpty(this.zzasl.packageName)) {
          break label125;
        }
        localzzag.zza(this.zzask, this.zzaqn);
      }
      catch (RemoteException localRemoteException)
      {
        this.zzasg.zzgo().zzjd().zzg("Failed to send conditional user property to the service", localRemoteException);
      }
      continue;
      label125:
      localzzag.zzb(this.zzask);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzea.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */