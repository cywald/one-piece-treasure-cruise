package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.text.TextUtils;
import com.google.android.gms.common.util.Clock;

@TargetApi(14)
@MainThread
final class zzdm
  implements Application.ActivityLifecycleCallbacks
{
  private zzdm(zzcs paramzzcs) {}
  
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle)
  {
    int j = 1;
    for (;;)
    {
      try
      {
        this.zzarc.zzgo().zzjl().zzbx("onActivityCreated");
        Object localObject = paramActivity.getIntent();
        if (localObject != null)
        {
          Uri localUri = ((Intent)localObject).getData();
          if ((localUri != null) && (localUri.isHierarchical()))
          {
            if (paramBundle == null)
            {
              Bundle localBundle = this.zzarc.zzgm().zza(localUri);
              this.zzarc.zzgm();
              if (!zzfk.zzd((Intent)localObject)) {
                break label297;
              }
              localObject = "gs";
              if (localBundle != null) {
                this.zzarc.logEvent((String)localObject, "_cmp", localBundle);
              }
            }
            localObject = localUri.getQueryParameter("referrer");
            if (TextUtils.isEmpty((CharSequence)localObject)) {
              return;
            }
            if (!((String)localObject).contains("gclid")) {
              break label250;
            }
            i = j;
            if (!((String)localObject).contains("utm_campaign"))
            {
              i = j;
              if (!((String)localObject).contains("utm_source"))
              {
                i = j;
                if (!((String)localObject).contains("utm_medium"))
                {
                  i = j;
                  if (!((String)localObject).contains("utm_term"))
                  {
                    if (!((String)localObject).contains("utm_content")) {
                      break label250;
                    }
                    i = j;
                  }
                }
              }
            }
            if (i != 0) {
              break label255;
            }
            this.zzarc.zzgo().zzjk().zzbx("Activity created with data 'referrer' param without gclid and at least one utm field");
            return;
          }
        }
      }
      catch (Exception localException)
      {
        this.zzarc.zzgo().zzjd().zzg("Throwable caught in onActivityCreated", localException);
        this.zzarc.zzgh().onActivityCreated(paramActivity, paramBundle);
        return;
      }
      label250:
      int i = 0;
      continue;
      label255:
      this.zzarc.zzgo().zzjk().zzg("Activity created with referrer", localException);
      if (!TextUtils.isEmpty(localException))
      {
        this.zzarc.zzb("auto", "_ldl", localException, true);
        continue;
        label297:
        String str = "auto";
      }
    }
  }
  
  public final void onActivityDestroyed(Activity paramActivity)
  {
    this.zzarc.zzgh().onActivityDestroyed(paramActivity);
  }
  
  @MainThread
  public final void onActivityPaused(Activity paramActivity)
  {
    this.zzarc.zzgh().onActivityPaused(paramActivity);
    paramActivity = this.zzarc.zzgj();
    long l = paramActivity.zzbx().elapsedRealtime();
    paramActivity.zzgn().zzc(new zzeu(paramActivity, l));
  }
  
  @MainThread
  public final void onActivityResumed(Activity paramActivity)
  {
    this.zzarc.zzgh().onActivityResumed(paramActivity);
    paramActivity = this.zzarc.zzgj();
    long l = paramActivity.zzbx().elapsedRealtime();
    paramActivity.zzgn().zzc(new zzet(paramActivity, l));
  }
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle)
  {
    this.zzarc.zzgh().onActivitySaveInstanceState(paramActivity, paramBundle);
  }
  
  public final void onActivityStarted(Activity paramActivity) {}
  
  public final void onActivityStopped(Activity paramActivity) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */