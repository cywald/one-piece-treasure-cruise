package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="EventParcelCreator")
@SafeParcelable.Reserved({1})
public final class zzad
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzad> CREATOR = new zzae();
  @SafeParcelable.Field(id=2)
  public final String name;
  @SafeParcelable.Field(id=4)
  public final String origin;
  @SafeParcelable.Field(id=3)
  public final zzaa zzaid;
  @SafeParcelable.Field(id=5)
  public final long zzaip;
  
  zzad(zzad paramzzad, long paramLong)
  {
    Preconditions.checkNotNull(paramzzad);
    this.name = paramzzad.name;
    this.zzaid = paramzzad.zzaid;
    this.origin = paramzzad.origin;
    this.zzaip = paramLong;
  }
  
  @SafeParcelable.Constructor
  public zzad(@SafeParcelable.Param(id=2) String paramString1, @SafeParcelable.Param(id=3) zzaa paramzzaa, @SafeParcelable.Param(id=4) String paramString2, @SafeParcelable.Param(id=5) long paramLong)
  {
    this.name = paramString1;
    this.zzaid = paramzzaa;
    this.origin = paramString2;
    this.zzaip = paramLong;
  }
  
  public final String toString()
  {
    String str1 = this.origin;
    String str2 = this.name;
    String str3 = String.valueOf(this.zzaid);
    return String.valueOf(str1).length() + 21 + String.valueOf(str2).length() + String.valueOf(str3).length() + "origin=" + str1 + ",name=" + str2 + ",params=" + str3;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.name, false);
    SafeParcelWriter.writeParcelable(paramParcel, 3, this.zzaid, paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 4, this.origin, false);
    SafeParcelWriter.writeLong(paramParcel, 5, this.zzaip);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */