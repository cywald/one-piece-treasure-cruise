package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Iterator;
import java.util.Set;

public final class zzy
{
  final String name;
  private final String origin;
  final long timestamp;
  final long zzaic;
  final zzaa zzaid;
  final String zztt;
  
  zzy(zzbt paramzzbt, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, Bundle paramBundle)
  {
    Preconditions.checkNotEmpty(paramString2);
    Preconditions.checkNotEmpty(paramString3);
    this.zztt = paramString2;
    this.name = paramString3;
    paramString3 = paramString1;
    if (TextUtils.isEmpty(paramString1)) {
      paramString3 = null;
    }
    this.origin = paramString3;
    this.timestamp = paramLong1;
    this.zzaic = paramLong2;
    if ((this.zzaic != 0L) && (this.zzaic > this.timestamp)) {
      paramzzbt.zzgo().zzjg().zzg("Event created with reverse previous/current timestamps. appId", zzap.zzbv(paramString2));
    }
    if ((paramBundle != null) && (!paramBundle.isEmpty()))
    {
      paramString1 = new Bundle(paramBundle);
      paramString2 = paramString1.keySet().iterator();
      while (paramString2.hasNext())
      {
        paramString3 = (String)paramString2.next();
        if (paramString3 == null)
        {
          paramzzbt.zzgo().zzjd().zzbx("Param name can't be null");
          paramString2.remove();
        }
        else
        {
          paramBundle = paramzzbt.zzgm().zzh(paramString3, paramString1.get(paramString3));
          if (paramBundle == null)
          {
            paramzzbt.zzgo().zzjg().zzg("Param value can't be null", paramzzbt.zzgl().zzbt(paramString3));
            paramString2.remove();
          }
          else
          {
            paramzzbt.zzgm().zza(paramString1, paramString3, paramBundle);
          }
        }
      }
    }
    for (paramzzbt = new zzaa(paramString1);; paramzzbt = new zzaa(new Bundle()))
    {
      this.zzaid = paramzzbt;
      return;
    }
  }
  
  private zzy(zzbt paramzzbt, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, zzaa paramzzaa)
  {
    Preconditions.checkNotEmpty(paramString2);
    Preconditions.checkNotEmpty(paramString3);
    Preconditions.checkNotNull(paramzzaa);
    this.zztt = paramString2;
    this.name = paramString3;
    String str = paramString1;
    if (TextUtils.isEmpty(paramString1)) {
      str = null;
    }
    this.origin = str;
    this.timestamp = paramLong1;
    this.zzaic = paramLong2;
    if ((this.zzaic != 0L) && (this.zzaic > this.timestamp)) {
      paramzzbt.zzgo().zzjg().zze("Event created with reverse previous/current timestamps. appId, name", zzap.zzbv(paramString2), zzap.zzbv(paramString3));
    }
    this.zzaid = paramzzaa;
  }
  
  public final String toString()
  {
    String str1 = this.zztt;
    String str2 = this.name;
    String str3 = String.valueOf(this.zzaid);
    return String.valueOf(str1).length() + 33 + String.valueOf(str2).length() + String.valueOf(str3).length() + "Event{appId='" + str1 + "', name='" + str2 + "', params=" + str3 + '}';
  }
  
  final zzy zza(zzbt paramzzbt, long paramLong)
  {
    return new zzy(paramzzbt, this.origin, this.zztt, this.name, this.timestamp, paramLong, this.zzaid);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */