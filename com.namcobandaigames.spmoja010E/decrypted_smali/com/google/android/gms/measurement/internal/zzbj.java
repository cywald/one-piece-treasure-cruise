package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver.PendingResult;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.MainThread;
import com.google.android.gms.common.internal.Preconditions;

public final class zzbj
{
  private final zzbm zzaoi;
  
  public zzbj(zzbm paramzzbm)
  {
    Preconditions.checkNotNull(paramzzbm);
    this.zzaoi = paramzzbm;
  }
  
  public static boolean zza(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      if (localPackageManager == null) {
        return false;
      }
      paramContext = localPackageManager.getReceiverInfo(new ComponentName(paramContext, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0);
      if (paramContext != null)
      {
        boolean bool = paramContext.enabled;
        if (bool) {
          return true;
        }
      }
    }
    catch (PackageManager.NameNotFoundException paramContext) {}
    return false;
  }
  
  @MainThread
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    zzbt localzzbt = zzbt.zza(paramContext, null);
    zzap localzzap = localzzbt.zzgo();
    if (paramIntent == null) {
      localzzap.zzjg().zzbx("Receiver called with null intent");
    }
    for (;;)
    {
      return;
      localzzbt.zzgr();
      String str1 = paramIntent.getAction();
      localzzap.zzjl().zzg("Local receiver got", str1);
      if ("com.google.android.gms.measurement.UPLOAD".equals(str1))
      {
        paramIntent = new Intent().setClassName(paramContext, "com.google.android.gms.measurement.AppMeasurementService");
        paramIntent.setAction("com.google.android.gms.measurement.UPLOAD");
        localzzap.zzjl().zzbx("Starting wakeful intent.");
        this.zzaoi.doStartService(paramContext, paramIntent);
        return;
      }
      if ("com.android.vending.INSTALL_REFERRER".equals(str1)) {
        try
        {
          localzzbt.zzgn().zzc(new zzbk(this, localzzbt, localzzap));
          localPendingResult = this.zzaoi.doGoAsync();
          str2 = paramIntent.getStringExtra("referrer");
          if (str2 == null)
          {
            localzzap.zzjl().zzbx("Install referrer extras are null");
            if (localPendingResult == null) {
              continue;
            }
            localPendingResult.finish();
          }
        }
        catch (Exception localException)
        {
          BroadcastReceiver.PendingResult localPendingResult;
          String str2;
          for (;;)
          {
            localzzap.zzjg().zzg("Install Referrer Reporter encountered a problem", localException);
          }
          localzzap.zzjj().zzg("Install referrer extras are", str2);
          Object localObject = str2;
          if (!str2.contains("?"))
          {
            localObject = String.valueOf(str2);
            if (((String)localObject).length() == 0) {
              break label293;
            }
          }
          label293:
          for (localObject = "?".concat((String)localObject);; localObject = new String("?"))
          {
            localObject = Uri.parse((String)localObject);
            localObject = localzzbt.zzgm().zza((Uri)localObject);
            if (localObject != null) {
              break label307;
            }
            localzzap.zzjl().zzbx("No campaign defined in install referrer broadcast");
            if (localPendingResult == null) {
              break;
            }
            localPendingResult.finish();
            return;
          }
          label307:
          long l = 1000L * paramIntent.getLongExtra("referrer_timestamp_seconds", 0L);
          if (l == 0L) {
            localzzap.zzjg().zzbx("Install referrer is missing timestamp");
          }
          localzzbt.zzgn().zzc(new zzbl(this, localzzbt, l, (Bundle)localObject, paramContext, localzzap, localPendingResult));
        }
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */