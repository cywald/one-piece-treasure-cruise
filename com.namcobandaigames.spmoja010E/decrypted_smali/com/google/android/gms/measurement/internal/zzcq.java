package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.util.Clock;

abstract interface zzcq
{
  public abstract Context getContext();
  
  public abstract Clock zzbx();
  
  public abstract zzbo zzgn();
  
  public abstract zzap zzgo();
  
  public abstract zzk zzgr();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzcq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */