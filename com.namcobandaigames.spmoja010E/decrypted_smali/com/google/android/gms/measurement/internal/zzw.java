package com.google.android.gms.measurement.internal;

final class zzw
  implements Runnable
{
  zzw(zzv paramzzv, zzcq paramzzcq) {}
  
  public final void run()
  {
    this.zzahx.zzgr();
    if (zzk.isMainThread()) {
      this.zzahx.zzgn().zzc(this);
    }
    boolean bool;
    do
    {
      return;
      bool = this.zzahy.zzej();
      zzv.zza(this.zzahy, 0L);
    } while (!bool);
    this.zzahy.run();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */