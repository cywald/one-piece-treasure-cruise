package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzsl;
import com.google.android.gms.internal.measurement.zzsv;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@VisibleForTesting
public final class zzaf
{
  private static volatile zzbt zzadj;
  static zzk zzaiq;
  static List<zza<Integer>> zzair = new ArrayList();
  static List<zza<Long>> zzais = new ArrayList();
  static List<zza<Boolean>> zzait = new ArrayList();
  static List<zza<String>> zzaiu = new ArrayList();
  static List<zza<Double>> zzaiv = new ArrayList();
  private static final zzsv zzaiw;
  @VisibleForTesting
  private static Boolean zzaix;
  private static zza<Boolean> zzaiy;
  private static zza<Boolean> zzaiz;
  private static zza<Boolean> zzaja;
  public static zza<Boolean> zzajb;
  public static zza<Boolean> zzajc;
  public static zza<String> zzajd;
  public static zza<Long> zzaje;
  public static zza<Long> zzajf;
  public static zza<Long> zzajg;
  public static zza<String> zzajh;
  public static zza<String> zzaji;
  public static zza<Integer> zzajj;
  public static zza<Integer> zzajk;
  public static zza<Integer> zzajl;
  public static zza<Integer> zzajm;
  public static zza<Integer> zzajn;
  public static zza<Integer> zzajo;
  public static zza<Integer> zzajp;
  public static zza<Integer> zzajq;
  public static zza<Integer> zzajr;
  public static zza<Integer> zzajs;
  public static zza<String> zzajt;
  public static zza<Long> zzaju;
  public static zza<Long> zzajv;
  public static zza<Long> zzajw;
  public static zza<Long> zzajx;
  public static zza<Long> zzajy;
  public static zza<Long> zzajz;
  public static zza<Long> zzaka;
  public static zza<Long> zzakb;
  public static zza<Long> zzakc;
  public static zza<Long> zzakd;
  public static zza<Long> zzake;
  public static zza<Integer> zzakf;
  public static zza<Long> zzakg;
  public static zza<Integer> zzakh;
  public static zza<Integer> zzaki;
  public static zza<Long> zzakj;
  public static zza<Boolean> zzakk;
  public static zza<String> zzakl;
  public static zza<Long> zzakm;
  public static zza<Integer> zzakn;
  public static zza<Double> zzako;
  public static zza<Boolean> zzakp;
  public static zza<Boolean> zzakq;
  public static zza<Boolean> zzakr;
  public static zza<Boolean> zzaks;
  public static zza<Boolean> zzakt;
  public static zza<Boolean> zzaku;
  public static zza<Boolean> zzakv;
  public static zza<Boolean> zzakw;
  public static zza<Boolean> zzakx;
  public static zza<Boolean> zzaky;
  public static zza<Boolean> zzakz;
  public static zza<Boolean> zzala;
  public static zza<Boolean> zzalb;
  public static zza<Boolean> zzalc;
  public static zza<Boolean> zzald;
  public static zza<Boolean> zzale;
  public static zza<Boolean> zzalf;
  private static zza<Boolean> zzalg;
  public static zza<Boolean> zzalh;
  private static zza<Boolean> zzali;
  public static zza<Boolean> zzalj;
  public static zza<Boolean> zzalk;
  
  static
  {
    String str = String.valueOf(Uri.encode("com.google.android.gms.measurement"));
    if (str.length() != 0) {}
    for (str = "content://com.google.android.gms.phenotype/".concat(str);; str = new String("content://com.google.android.gms.phenotype/"))
    {
      zzaiw = new zzsv(Uri.parse(str));
      zzaiy = zza.zzb("measurement.log_third_party_store_events_enabled", false, false);
      zzaiz = zza.zzb("measurement.log_installs_enabled", false, false);
      zzaja = zza.zzb("measurement.log_upgrades_enabled", false, false);
      zzajb = zza.zzb("measurement.log_androidId_enabled", false, false);
      zzajc = zza.zzb("measurement.upload_dsid_enabled", false, false);
      zzajd = zza.zzd("measurement.log_tag", "FA", "FA-SVC");
      zzaje = zza.zzb("measurement.ad_id_cache_time", 10000L, 10000L);
      zzajf = zza.zzb("measurement.monitoring.sample_period_millis", 86400000L, 86400000L);
      zzajg = zza.zzb("measurement.config.cache_time", 86400000L, 3600000L);
      zzajh = zza.zzd("measurement.config.url_scheme", "https", "https");
      zzaji = zza.zzd("measurement.config.url_authority", "app-measurement.com", "app-measurement.com");
      zzajj = zza.zzc("measurement.upload.max_bundles", 100, 100);
      zzajk = zza.zzc("measurement.upload.max_batch_size", 65536, 65536);
      zzajl = zza.zzc("measurement.upload.max_bundle_size", 65536, 65536);
      zzajm = zza.zzc("measurement.upload.max_events_per_bundle", 1000, 1000);
      zzajn = zza.zzc("measurement.upload.max_events_per_day", 100000, 100000);
      zzajo = zza.zzc("measurement.upload.max_error_events_per_day", 1000, 1000);
      zzajp = zza.zzc("measurement.upload.max_public_events_per_day", 50000, 50000);
      zzajq = zza.zzc("measurement.upload.max_conversions_per_day", 500, 500);
      zzajr = zza.zzc("measurement.upload.max_realtime_events_per_day", 10, 10);
      zzajs = zza.zzc("measurement.store.max_stored_events_per_app", 100000, 100000);
      zzajt = zza.zzd("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a");
      zzaju = zza.zzb("measurement.upload.backoff_period", 43200000L, 43200000L);
      zzajv = zza.zzb("measurement.upload.window_interval", 3600000L, 3600000L);
      zzajw = zza.zzb("measurement.upload.interval", 3600000L, 3600000L);
      zzajx = zza.zzb("measurement.upload.realtime_upload_interval", 10000L, 10000L);
      zzajy = zza.zzb("measurement.upload.debug_upload_interval", 1000L, 1000L);
      zzajz = zza.zzb("measurement.upload.minimum_delay", 500L, 500L);
      zzaka = zza.zzb("measurement.alarm_manager.minimum_interval", 60000L, 60000L);
      zzakb = zza.zzb("measurement.upload.stale_data_deletion_interval", 86400000L, 86400000L);
      zzakc = zza.zzb("measurement.upload.refresh_blacklisted_config_interval", 604800000L, 604800000L);
      zzakd = zza.zzb("measurement.upload.initial_upload_delay_time", 15000L, 15000L);
      zzake = zza.zzb("measurement.upload.retry_time", 1800000L, 1800000L);
      zzakf = zza.zzc("measurement.upload.retry_count", 6, 6);
      zzakg = zza.zzb("measurement.upload.max_queue_time", 2419200000L, 2419200000L);
      zzakh = zza.zzc("measurement.lifetimevalue.max_currency_tracked", 4, 4);
      zzaki = zza.zzc("measurement.audience.filter_result_max_count", 200, 200);
      zzakj = zza.zzb("measurement.service_client.idle_disconnect_millis", 5000L, 5000L);
      zzakk = zza.zzb("measurement.test.boolean_flag", false, false);
      zzakl = zza.zzd("measurement.test.string_flag", "---", "---");
      zzakm = zza.zzb("measurement.test.long_flag", -1L, -1L);
      zzakn = zza.zzc("measurement.test.int_flag", -2, -2);
      zzako = zza.zza("measurement.test.double_flag", -3.0D, -3.0D);
      zzakp = zza.zzb("measurement.lifetimevalue.user_engagement_tracking_enabled", false, false);
      zzakq = zza.zzb("measurement.audience.complex_param_evaluation", false, false);
      zzakr = zza.zzb("measurement.validation.internal_limits_internal_event_params", false, false);
      zzaks = zza.zzb("measurement.quality.unsuccessful_update_retry_counter", false, false);
      zzakt = zza.zzb("measurement.iid.disable_on_collection_disabled", true, true);
      zzaku = zza.zzb("measurement.app_launch.call_only_when_enabled", true, true);
      zzakv = zza.zzb("measurement.run_on_worker_inline", true, false);
      zzakw = zza.zzb("measurement.audience.dynamic_filters", false, false);
      zzakx = zza.zzb("measurement.reset_analytics.persist_time", false, false);
      zzaky = zza.zzb("measurement.validation.value_and_currency_params", false, false);
      zzakz = zza.zzb("measurement.sampling.time_zone_offset_enabled", false, false);
      zzala = zza.zzb("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", false, false);
      zzalb = zza.zzb("measurement.disconnect_from_remote_service", false, false);
      zzalc = zza.zzb("measurement.clear_local_database", false, false);
      zzald = zza.zzb("measurement.fetch_config_with_admob_app_id", false, false);
      zzale = zza.zzb("measurement.sessions.session_id_enabled", false, false);
      zzalf = zza.zzb("measurement.sessions.immediate_start_enabled", false, false);
      zzalg = zza.zzb("measurement.sessions.background_sessions_enabled", false, false);
      zzalh = zza.zzb("measurement.collection.firebase_global_collection_flag_enabled", true, true);
      zzali = zza.zzb("measurement.collection.efficient_engagement_reporting_enabled", false, false);
      zzalj = zza.zzb("measurement.personalized_ads_feature_enabled", false, false);
      zzalk = zza.zzb("measurement.remove_app_instance_id_cache_enabled", true, true);
      return;
    }
  }
  
  static void zza(zzbt paramzzbt)
  {
    zzadj = paramzzbt;
  }
  
  static void zza(zzk paramzzk)
  {
    zzaiq = paramzzk;
    zza.zziy();
  }
  
  @VisibleForTesting
  static void zza(Exception paramException)
  {
    if (zzadj == null) {
      return;
    }
    Context localContext = zzadj.getContext();
    if (zzaix == null) {
      if (GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(localContext, 12451000) != 0) {
        break label68;
      }
    }
    label68:
    for (boolean bool = true;; bool = false)
    {
      zzaix = Boolean.valueOf(bool);
      if (!zzaix.booleanValue()) {
        break;
      }
      zzadj.zzgo().zzjd().zzg("Got Exception on PhenotypeFlag.get on Play device", paramException);
      return;
    }
  }
  
  @VisibleForTesting
  public static final class zza<V>
  {
    private final V zzaan;
    private zzsl<V> zzall;
    private final V zzalm;
    private volatile V zzaln;
    private final String zzoj;
    
    private zza(String paramString, V paramV1, V paramV2)
    {
      this.zzoj = paramString;
      this.zzaan = paramV1;
      this.zzalm = paramV2;
    }
    
    static zza<Double> zza(String paramString, double paramDouble1, double paramDouble2)
    {
      paramString = new zza(paramString, Double.valueOf(-3.0D), Double.valueOf(-3.0D));
      zzaf.zzaiv.add(paramString);
      return paramString;
    }
    
    static zza<Long> zzb(String paramString, long paramLong1, long paramLong2)
    {
      paramString = new zza(paramString, Long.valueOf(paramLong1), Long.valueOf(paramLong2));
      zzaf.zzais.add(paramString);
      return paramString;
    }
    
    static zza<Boolean> zzb(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
      paramString = new zza(paramString, Boolean.valueOf(paramBoolean1), Boolean.valueOf(paramBoolean2));
      zzaf.zzait.add(paramString);
      return paramString;
    }
    
    static zza<Integer> zzc(String paramString, int paramInt1, int paramInt2)
    {
      paramString = new zza(paramString, Integer.valueOf(paramInt1), Integer.valueOf(paramInt2));
      zzaf.zzair.add(paramString);
      return paramString;
    }
    
    static zza<String> zzd(String paramString1, String paramString2, String paramString3)
    {
      paramString1 = new zza(paramString1, paramString2, paramString3);
      zzaf.zzaiu.add(paramString1);
      return paramString1;
    }
    
    @WorkerThread
    private static void zzix()
    {
      for (;;)
      {
        zza localzza;
        try
        {
          if (!zzk.isMainThread())
          {
            Object localObject1 = zzaf.zzaiq;
            try
            {
              localObject1 = zzaf.zzait.iterator();
              if (!((Iterator)localObject1).hasNext()) {
                break label80;
              }
              localzza = (zza)((Iterator)localObject1).next();
              localzza.zzaln = localzza.zzall.get();
              continue;
            }
            catch (SecurityException localSecurityException)
            {
              zzaf.zza(localSecurityException);
            }
          }
          else
          {
            throw new IllegalStateException("Tried to refresh flag cache on main thread or on package side.");
          }
        }
        finally {}
        label80:
        Iterator localIterator = zzaf.zzaiu.iterator();
        while (localIterator.hasNext())
        {
          localzza = (zza)localIterator.next();
          localzza.zzaln = localzza.zzall.get();
        }
        localIterator = zzaf.zzais.iterator();
        while (localIterator.hasNext())
        {
          localzza = (zza)localIterator.next();
          localzza.zzaln = localzza.zzall.get();
        }
        localIterator = zzaf.zzair.iterator();
        while (localIterator.hasNext())
        {
          localzza = (zza)localIterator.next();
          localzza.zzaln = localzza.zzall.get();
        }
        localIterator = zzaf.zzaiv.iterator();
        while (localIterator.hasNext())
        {
          localzza = (zza)localIterator.next();
          localzza.zzaln = localzza.zzall.get();
        }
      }
    }
    
    private static void zzq()
    {
      zza localzza;
      zzsv localzzsv;
      String str;
      zzk localzzk;
      try
      {
        Iterator localIterator1 = zzaf.zzait.iterator();
        while (localIterator1.hasNext())
        {
          localzza = (zza)localIterator1.next();
          localzzsv = zzaf.zziw();
          str = localzza.zzoj;
          localzzk = zzaf.zzaiq;
          localzza.zzall = localzzsv.zzf(str, ((Boolean)localzza.zzaan).booleanValue());
        }
        localIterator2 = zzaf.zzaiu.iterator();
      }
      finally {}
      while (localIterator2.hasNext())
      {
        localzza = (zza)localIterator2.next();
        localzzsv = zzaf.zziw();
        str = localzza.zzoj;
        localzzk = zzaf.zzaiq;
        localzza.zzall = localzzsv.zzx(str, (String)localzza.zzaan);
      }
      Iterator localIterator2 = zzaf.zzais.iterator();
      while (localIterator2.hasNext())
      {
        localzza = (zza)localIterator2.next();
        localzzsv = zzaf.zziw();
        str = localzza.zzoj;
        localzzk = zzaf.zzaiq;
        localzza.zzall = localzzsv.zze(str, ((Long)localzza.zzaan).longValue());
      }
      localIterator2 = zzaf.zzair.iterator();
      while (localIterator2.hasNext())
      {
        localzza = (zza)localIterator2.next();
        localzzsv = zzaf.zziw();
        str = localzza.zzoj;
        localzzk = zzaf.zzaiq;
        localzza.zzall = localzzsv.zzd(str, ((Integer)localzza.zzaan).intValue());
      }
      localIterator2 = zzaf.zzaiv.iterator();
      while (localIterator2.hasNext())
      {
        localzza = (zza)localIterator2.next();
        localzzsv = zzaf.zziw();
        str = localzza.zzoj;
        localzzk = zzaf.zzaiq;
        localzza.zzall = localzzsv.zzb(str, ((Double)localzza.zzaan).doubleValue());
      }
    }
    
    public final V get()
    {
      if (zzaf.zzaiq == null) {
        return (V)this.zzaan;
      }
      Object localObject = zzaf.zzaiq;
      if (zzk.isMainThread())
      {
        if (this.zzaln == null) {
          return (V)this.zzaan;
        }
        return (V)this.zzaln;
      }
      zzix();
      try
      {
        localObject = this.zzall.get();
        return (V)localObject;
      }
      catch (SecurityException localSecurityException)
      {
        zzaf.zza(localSecurityException);
      }
      return (V)this.zzall.getDefaultValue();
    }
    
    public final V get(V paramV)
    {
      if (paramV != null) {
        return paramV;
      }
      if (zzaf.zzaiq == null) {
        return (V)this.zzaan;
      }
      paramV = zzaf.zzaiq;
      if (zzk.isMainThread())
      {
        if (this.zzaln == null) {
          return (V)this.zzaan;
        }
        return (V)this.zzaln;
      }
      zzix();
      try
      {
        paramV = this.zzall.get();
        return paramV;
      }
      catch (SecurityException paramV)
      {
        zzaf.zza(paramV);
      }
      return (V)this.zzall.getDefaultValue();
    }
    
    public final String getKey()
    {
      return this.zzoj;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */