package com.google.android.gms.measurement.internal;

import android.os.Bundle;

final class zzdp
  implements Runnable
{
  zzdp(zzdo paramzzdo, boolean paramBoolean, zzdn paramzzdn1, zzdn paramzzdn2) {}
  
  public final void run()
  {
    if ((this.zzaru) && (this.zzarx.zzaro != null)) {
      zzdo.zza(this.zzarx, this.zzarx.zzaro);
    }
    if ((this.zzarv == null) || (this.zzarv.zzarm != this.zzarw.zzarm) || (!zzfk.zzu(this.zzarv.zzarl, this.zzarw.zzarl)) || (!zzfk.zzu(this.zzarv.zzuw, this.zzarw.zzuw))) {}
    for (int i = 1;; i = 0)
    {
      if (i != 0)
      {
        Bundle localBundle = new Bundle();
        zzdo.zza(this.zzarw, localBundle, true);
        if (this.zzarv != null)
        {
          if (this.zzarv.zzuw != null) {
            localBundle.putString("_pn", this.zzarv.zzuw);
          }
          localBundle.putString("_pc", this.zzarv.zzarl);
          localBundle.putLong("_pi", this.zzarv.zzarm);
        }
        this.zzarx.zzge().zza("auto", "_vs", localBundle);
      }
      this.zzarx.zzaro = this.zzarw;
      this.zzarx.zzgg().zzb(this.zzarw);
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */