package com.google.android.gms.measurement.internal;

public final class zzdn
{
  public final String zzarl;
  public final long zzarm;
  boolean zzarn;
  public final String zzuw;
  
  public zzdn(String paramString1, String paramString2, long paramLong)
  {
    this.zzuw = paramString1;
    this.zzarl = paramString2;
    this.zzarm = paramLong;
    this.zzarn = false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */