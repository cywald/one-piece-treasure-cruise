package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzfu;
import com.google.android.gms.internal.measurement.zzfv;
import com.google.android.gms.internal.measurement.zzfw;
import com.google.android.gms.internal.measurement.zzfy;
import com.google.android.gms.internal.measurement.zzga;
import com.google.android.gms.internal.measurement.zzgb;
import com.google.android.gms.internal.measurement.zzgc;
import com.google.android.gms.internal.measurement.zzyx;
import com.google.android.gms.internal.measurement.zzyy;
import com.google.android.gms.internal.measurement.zzzg;
import com.google.android.gms.measurement.AppMeasurement.Event;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.google.android.gms.measurement.AppMeasurement.UserProperty;
import java.io.IOException;
import java.util.Map;

public final class zzbn
  extends zzez
  implements zzp
{
  @VisibleForTesting
  private static int zzaon = 65535;
  @VisibleForTesting
  private static int zzaoo = 2;
  private final Map<String, Map<String, String>> zzaop = new ArrayMap();
  private final Map<String, Map<String, Boolean>> zzaoq = new ArrayMap();
  private final Map<String, Map<String, Boolean>> zzaor = new ArrayMap();
  private final Map<String, zzgb> zzaos = new ArrayMap();
  private final Map<String, Map<String, Integer>> zzaot = new ArrayMap();
  private final Map<String, String> zzaou = new ArrayMap();
  
  zzbn(zzfa paramzzfa)
  {
    super(paramzzfa);
  }
  
  @WorkerThread
  private final zzgb zza(String paramString, byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null) {
      return new zzgb();
    }
    paramArrayOfByte = zzyx.zzj(paramArrayOfByte, 0, paramArrayOfByte.length);
    zzgb localzzgb = new zzgb();
    try
    {
      localzzgb.zza(paramArrayOfByte);
      zzgo().zzjl().zze("Parsed config. version, gmp_app_id", localzzgb.zzawe, localzzgb.zzafx);
      return localzzgb;
    }
    catch (IOException paramArrayOfByte)
    {
      zzgo().zzjg().zze("Unable to merge remote config. appId", zzap.zzbv(paramString), paramArrayOfByte);
    }
    return new zzgb();
  }
  
  private static Map<String, String> zza(zzgb paramzzgb)
  {
    ArrayMap localArrayMap = new ArrayMap();
    if ((paramzzgb != null) && (paramzzgb.zzawg != null))
    {
      paramzzgb = paramzzgb.zzawg;
      int j = paramzzgb.length;
      int i = 0;
      while (i < j)
      {
        Object localObject = paramzzgb[i];
        if (localObject != null) {
          localArrayMap.put(((zzgc)localObject).zzoj, ((zzgc)localObject).value);
        }
        i += 1;
      }
    }
    return localArrayMap;
  }
  
  private final void zza(String paramString, zzgb paramzzgb)
  {
    ArrayMap localArrayMap1 = new ArrayMap();
    ArrayMap localArrayMap2 = new ArrayMap();
    ArrayMap localArrayMap3 = new ArrayMap();
    if ((paramzzgb != null) && (paramzzgb.zzawh != null))
    {
      paramzzgb = paramzzgb.zzawh;
      int j = paramzzgb.length;
      int i = 0;
      if (i < j)
      {
        Object localObject = paramzzgb[i];
        if (TextUtils.isEmpty(((zzga)localObject).name)) {
          zzgo().zzjg().zzbx("EventConfig contained null event name");
        }
        for (;;)
        {
          i += 1;
          break;
          String str = AppMeasurement.Event.zzal(((zzga)localObject).name);
          if (!TextUtils.isEmpty(str)) {
            ((zzga)localObject).name = str;
          }
          localArrayMap1.put(((zzga)localObject).name, ((zzga)localObject).zzawb);
          localArrayMap2.put(((zzga)localObject).name, ((zzga)localObject).zzawc);
          if (((zzga)localObject).zzawd != null) {
            if ((((zzga)localObject).zzawd.intValue() < zzaoo) || (((zzga)localObject).zzawd.intValue() > zzaon)) {
              zzgo().zzjg().zze("Invalid sampling rate. Event name, sample rate", ((zzga)localObject).name, ((zzga)localObject).zzawd);
            } else {
              localArrayMap3.put(((zzga)localObject).name, ((zzga)localObject).zzawd);
            }
          }
        }
      }
    }
    this.zzaoq.put(paramString, localArrayMap1);
    this.zzaor.put(paramString, localArrayMap2);
    this.zzaot.put(paramString, localArrayMap3);
  }
  
  @WorkerThread
  private final void zzce(String paramString)
  {
    zzcl();
    zzaf();
    Preconditions.checkNotEmpty(paramString);
    if (this.zzaos.get(paramString) == null)
    {
      localObject = zzjq().zzbn(paramString);
      if (localObject == null)
      {
        this.zzaop.put(paramString, null);
        this.zzaoq.put(paramString, null);
        this.zzaor.put(paramString, null);
        this.zzaos.put(paramString, null);
        this.zzaou.put(paramString, null);
        this.zzaot.put(paramString, null);
      }
    }
    else
    {
      return;
    }
    Object localObject = zza(paramString, (byte[])localObject);
    this.zzaop.put(paramString, zza((zzgb)localObject));
    zza(paramString, (zzgb)localObject);
    this.zzaos.put(paramString, localObject);
    this.zzaou.put(paramString, null);
  }
  
  @WorkerThread
  protected final boolean zza(String paramString1, byte[] paramArrayOfByte, String paramString2)
  {
    zzcl();
    zzaf();
    Preconditions.checkNotEmpty(paramString1);
    localObject1 = zza(paramString1, paramArrayOfByte);
    if (localObject1 == null) {
      return false;
    }
    zza(paramString1, (zzgb)localObject1);
    this.zzaos.put(paramString1, localObject1);
    this.zzaou.put(paramString1, paramString2);
    this.zzaop.put(paramString1, zza((zzgb)localObject1));
    paramString2 = zzjp();
    zzfu[] arrayOfzzfu = ((zzgb)localObject1).zzawi;
    Preconditions.checkNotNull(arrayOfzzfu);
    int m = arrayOfzzfu.length;
    int i = 0;
    while (i < m)
    {
      Object localObject2 = arrayOfzzfu[i];
      zzfv[] arrayOfzzfv = ((zzfu)localObject2).zzava;
      int n = arrayOfzzfv.length;
      int j = 0;
      Object localObject3;
      while (j < n)
      {
        localObject3 = arrayOfzzfv[j];
        String str1 = AppMeasurement.Event.zzal(((zzfv)localObject3).zzavf);
        if (str1 != null) {
          ((zzfv)localObject3).zzavf = str1;
        }
        localObject3 = ((zzfv)localObject3).zzavg;
        int i1 = localObject3.length;
        k = 0;
        while (k < i1)
        {
          str1 = localObject3[k];
          String str2 = AppMeasurement.Param.zzal(str1.zzavn);
          if (str2 != null) {
            str1.zzavn = str2;
          }
          k += 1;
        }
        j += 1;
      }
      localObject2 = ((zzfu)localObject2).zzauz;
      int k = localObject2.length;
      j = 0;
      while (j < k)
      {
        arrayOfzzfv = localObject2[j];
        localObject3 = AppMeasurement.UserProperty.zzal(arrayOfzzfv.zzavu);
        if (localObject3 != null) {
          arrayOfzzfv.zzavu = ((String)localObject3);
        }
        j += 1;
      }
      i += 1;
    }
    paramString2.zzjq().zza(paramString1, arrayOfzzfu);
    try
    {
      ((zzgb)localObject1).zzawi = null;
      paramString2 = new byte[((zzzg)localObject1).zzvu()];
      ((zzzg)localObject1).zza(zzyy.zzk(paramString2, 0, paramString2.length));
      paramArrayOfByte = paramString2;
    }
    catch (IOException paramString2)
    {
      try
      {
        if (paramString2.getWritableDatabase().update("apps", (ContentValues)localObject1, "app_id = ?", new String[] { paramString1 }) != 0L) {
          break label425;
        }
        paramString2.zzgo().zzjd().zzg("Failed to update remote config (got 0). appId", zzap.zzbv(paramString1));
        return true;
        paramString2 = paramString2;
        zzgo().zzjg().zze("Unable to serialize reduced-size config. Storing full config instead. appId", zzap.zzbv(paramString1), paramString2);
      }
      catch (SQLiteException paramArrayOfByte)
      {
        for (;;)
        {
          paramString2.zzgo().zzjd().zze("Error storing remote config. appId", zzap.zzbv(paramString1), paramArrayOfByte);
        }
      }
    }
    paramString2 = zzjq();
    Preconditions.checkNotEmpty(paramString1);
    paramString2.zzaf();
    paramString2.zzcl();
    localObject1 = new ContentValues();
    ((ContentValues)localObject1).put("remote_config", paramArrayOfByte);
  }
  
  @WorkerThread
  protected final zzgb zzcf(String paramString)
  {
    zzcl();
    zzaf();
    Preconditions.checkNotEmpty(paramString);
    zzce(paramString);
    return (zzgb)this.zzaos.get(paramString);
  }
  
  @WorkerThread
  protected final String zzcg(String paramString)
  {
    zzaf();
    return (String)this.zzaou.get(paramString);
  }
  
  @WorkerThread
  protected final void zzch(String paramString)
  {
    zzaf();
    this.zzaou.put(paramString, null);
  }
  
  @WorkerThread
  final void zzci(String paramString)
  {
    zzaf();
    this.zzaos.remove(paramString);
  }
  
  @WorkerThread
  final long zzcj(String paramString)
  {
    String str = zzf(paramString, "measurement.account.time_zone_offset_minutes");
    if (!TextUtils.isEmpty(str)) {
      try
      {
        long l = Long.parseLong(str);
        return l;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        zzgo().zzjg().zze("Unable to parse timezone offset. appId", zzap.zzbv(paramString), localNumberFormatException);
      }
    }
    return 0L;
  }
  
  final boolean zzck(String paramString)
  {
    return "1".equals(zzf(paramString, "measurement.upload.blacklist_internal"));
  }
  
  final boolean zzcl(String paramString)
  {
    return "1".equals(zzf(paramString, "measurement.upload.blacklist_public"));
  }
  
  @WorkerThread
  public final String zzf(String paramString1, String paramString2)
  {
    zzaf();
    zzce(paramString1);
    paramString1 = (Map)this.zzaop.get(paramString1);
    if (paramString1 != null) {
      return (String)paramString1.get(paramString2);
    }
    return null;
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  @WorkerThread
  final boolean zzo(String paramString1, String paramString2)
  {
    zzaf();
    zzce(paramString1);
    if ((zzck(paramString1)) && (zzfk.zzcv(paramString2))) {}
    while ((zzcl(paramString1)) && (zzfk.zzcq(paramString2))) {
      return true;
    }
    paramString1 = (Map)this.zzaoq.get(paramString1);
    if (paramString1 != null)
    {
      paramString1 = (Boolean)paramString1.get(paramString2);
      if (paramString1 == null) {
        return false;
      }
      return paramString1.booleanValue();
    }
    return false;
  }
  
  @WorkerThread
  final boolean zzp(String paramString1, String paramString2)
  {
    zzaf();
    zzce(paramString1);
    if ("ecommerce_purchase".equals(paramString2)) {
      return true;
    }
    paramString1 = (Map)this.zzaor.get(paramString1);
    if (paramString1 != null)
    {
      paramString1 = (Boolean)paramString1.get(paramString2);
      if (paramString1 == null) {
        return false;
      }
      return paramString1.booleanValue();
    }
    return false;
  }
  
  @WorkerThread
  final int zzq(String paramString1, String paramString2)
  {
    zzaf();
    zzce(paramString1);
    paramString1 = (Map)this.zzaot.get(paramString1);
    if (paramString1 != null)
    {
      paramString1 = (Integer)paramString1.get(paramString2);
      if (paramString1 == null) {
        return 1;
      }
      return paramString1.intValue();
    }
    return 1;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */