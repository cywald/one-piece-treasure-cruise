package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.Parcel;
import android.os.SystemClock;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.util.VisibleForTesting;

public final class zzal
  extends zzf
{
  private final zzam zzalq = new zzam(this, getContext(), "google_app_measurement_local.db");
  private boolean zzalr;
  
  zzal(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  @WorkerThread
  @VisibleForTesting
  private final SQLiteDatabase getWritableDatabase()
    throws SQLiteException
  {
    if (this.zzalr) {
      return null;
    }
    SQLiteDatabase localSQLiteDatabase = this.zzalq.getWritableDatabase();
    if (localSQLiteDatabase == null)
    {
      this.zzalr = true;
      return null;
    }
    return localSQLiteDatabase;
  }
  
  @WorkerThread
  private final boolean zza(int paramInt, byte[] paramArrayOfByte)
  {
    zzgb();
    zzaf();
    if (this.zzalr) {
      return false;
    }
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("type", Integer.valueOf(paramInt));
    localContentValues.put("entry", paramArrayOfByte);
    paramInt = 0;
    int j = 5;
    while (paramInt < 5)
    {
      Object localObject5 = null;
      Object localObject7 = null;
      Object localObject1 = null;
      Object localObject3 = null;
      Object localObject8 = null;
      Object localObject9 = null;
      Object localObject10 = null;
      Cursor localCursor1 = null;
      Cursor localCursor2 = localCursor1;
      Object localObject4 = localObject8;
      Object localObject6 = localObject9;
      paramArrayOfByte = (byte[])localObject10;
      try
      {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        if (localSQLiteDatabase == null)
        {
          localCursor2 = localCursor1;
          localObject3 = localSQLiteDatabase;
          localObject4 = localObject8;
          localObject5 = localSQLiteDatabase;
          localObject6 = localObject9;
          localObject7 = localSQLiteDatabase;
          paramArrayOfByte = (byte[])localObject10;
          localObject1 = localSQLiteDatabase;
          this.zzalr = true;
          if (localSQLiteDatabase != null) {
            localSQLiteDatabase.close();
          }
          return false;
        }
        localCursor2 = localCursor1;
        localObject3 = localSQLiteDatabase;
        localObject4 = localObject8;
        localObject5 = localSQLiteDatabase;
        localObject6 = localObject9;
        localObject7 = localSQLiteDatabase;
        paramArrayOfByte = (byte[])localObject10;
        localObject1 = localSQLiteDatabase;
        localSQLiteDatabase.beginTransaction();
        long l2 = 0L;
        localCursor2 = localCursor1;
        localObject3 = localSQLiteDatabase;
        localObject4 = localObject8;
        localObject5 = localSQLiteDatabase;
        localObject6 = localObject9;
        localObject7 = localSQLiteDatabase;
        paramArrayOfByte = (byte[])localObject10;
        localObject1 = localSQLiteDatabase;
        localCursor1 = localSQLiteDatabase.rawQuery("select count(1) from messages", null);
        l1 = l2;
        if (localCursor1 != null)
        {
          l1 = l2;
          localCursor2 = localCursor1;
          localObject3 = localSQLiteDatabase;
          localObject4 = localCursor1;
          localObject5 = localSQLiteDatabase;
          localObject6 = localCursor1;
          localObject7 = localSQLiteDatabase;
          paramArrayOfByte = localCursor1;
          localObject1 = localSQLiteDatabase;
          if (localCursor1.moveToFirst())
          {
            localCursor2 = localCursor1;
            localObject3 = localSQLiteDatabase;
            localObject4 = localCursor1;
            localObject5 = localSQLiteDatabase;
            localObject6 = localCursor1;
            localObject7 = localSQLiteDatabase;
            paramArrayOfByte = localCursor1;
            localObject1 = localSQLiteDatabase;
            l1 = localCursor1.getLong(0);
          }
        }
        if (l1 >= 100000L)
        {
          localCursor2 = localCursor1;
          localObject3 = localSQLiteDatabase;
          localObject4 = localCursor1;
          localObject5 = localSQLiteDatabase;
          localObject6 = localCursor1;
          localObject7 = localSQLiteDatabase;
          paramArrayOfByte = localCursor1;
          localObject1 = localSQLiteDatabase;
          zzgo().zzjd().zzbx("Data loss, local db full");
          l1 = 100000L - l1 + 1L;
          localCursor2 = localCursor1;
          localObject3 = localSQLiteDatabase;
          localObject4 = localCursor1;
          localObject5 = localSQLiteDatabase;
          localObject6 = localCursor1;
          localObject7 = localSQLiteDatabase;
          paramArrayOfByte = localCursor1;
          localObject1 = localSQLiteDatabase;
          l2 = localSQLiteDatabase.delete("messages", "rowid in (select rowid from messages order by rowid asc limit ?)", new String[] { Long.toString(l1) });
          if (l2 != l1)
          {
            localCursor2 = localCursor1;
            localObject3 = localSQLiteDatabase;
            localObject4 = localCursor1;
            localObject5 = localSQLiteDatabase;
            localObject6 = localCursor1;
            localObject7 = localSQLiteDatabase;
            paramArrayOfByte = localCursor1;
            localObject1 = localSQLiteDatabase;
            zzgo().zzjd().zzd("Different delete count than expected in local db. expected, received, difference", Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l1 - l2));
          }
        }
        localCursor2 = localCursor1;
        localObject3 = localSQLiteDatabase;
        localObject4 = localCursor1;
        localObject5 = localSQLiteDatabase;
        localObject6 = localCursor1;
        localObject7 = localSQLiteDatabase;
        paramArrayOfByte = localCursor1;
        localObject1 = localSQLiteDatabase;
        localSQLiteDatabase.insertOrThrow("messages", null, localContentValues);
        localCursor2 = localCursor1;
        localObject3 = localSQLiteDatabase;
        localObject4 = localCursor1;
        localObject5 = localSQLiteDatabase;
        localObject6 = localCursor1;
        localObject7 = localSQLiteDatabase;
        paramArrayOfByte = localCursor1;
        localObject1 = localSQLiteDatabase;
        localSQLiteDatabase.setTransactionSuccessful();
        localCursor2 = localCursor1;
        localObject3 = localSQLiteDatabase;
        localObject4 = localCursor1;
        localObject5 = localSQLiteDatabase;
        localObject6 = localCursor1;
        localObject7 = localSQLiteDatabase;
        paramArrayOfByte = localCursor1;
        localObject1 = localSQLiteDatabase;
        localSQLiteDatabase.endTransaction();
        if (localCursor1 != null) {
          localCursor1.close();
        }
        if (localSQLiteDatabase != null) {
          localSQLiteDatabase.close();
        }
        return true;
      }
      catch (SQLiteFullException localSQLiteFullException)
      {
        paramArrayOfByte = localCursor2;
        localObject1 = localObject3;
        zzgo().zzjd().zzg("Error writing entry to local database", localSQLiteFullException);
        paramArrayOfByte = localCursor2;
        localObject1 = localObject3;
        this.zzalr = true;
        if (localCursor2 != null) {
          localCursor2.close();
        }
        i = j;
        if (localObject3 != null)
        {
          ((SQLiteDatabase)localObject3).close();
          i = j;
        }
        paramInt += 1;
        j = i;
      }
      catch (SQLiteDatabaseLockedException paramArrayOfByte)
      {
        for (;;)
        {
          long l1 = j;
          paramArrayOfByte = (byte[])localObject4;
          localObject1 = localObject5;
          SystemClock.sleep(l1);
          j += 20;
          if (localObject4 != null) {
            ((Cursor)localObject4).close();
          }
          i = j;
          if (localObject5 != null)
          {
            ((SQLiteDatabase)localObject5).close();
            i = j;
          }
        }
      }
      catch (SQLiteException localSQLiteException)
      {
        for (;;)
        {
          if (localObject7 != null)
          {
            paramArrayOfByte = (byte[])localObject6;
            localObject1 = localObject7;
            if (((SQLiteDatabase)localObject7).inTransaction())
            {
              paramArrayOfByte = (byte[])localObject6;
              localObject1 = localObject7;
              ((SQLiteDatabase)localObject7).endTransaction();
            }
          }
          paramArrayOfByte = (byte[])localObject6;
          localObject1 = localObject7;
          zzgo().zzjd().zzg("Error writing entry to local database", localSQLiteException);
          paramArrayOfByte = (byte[])localObject6;
          localObject1 = localObject7;
          this.zzalr = true;
          if (localObject6 != null) {
            ((Cursor)localObject6).close();
          }
          int i = j;
          if (localObject7 != null)
          {
            ((SQLiteDatabase)localObject7).close();
            i = j;
          }
        }
      }
      finally
      {
        if (paramArrayOfByte != null) {
          paramArrayOfByte.close();
        }
        if (localObject1 != null) {
          ((SQLiteDatabase)localObject1).close();
        }
      }
    }
    zzgo().zzjg().zzbx("Failed to write entry to local database");
    return false;
  }
  
  @WorkerThread
  public final void resetAnalyticsData()
  {
    zzgb();
    zzaf();
    try
    {
      int i = getWritableDatabase().delete("messages", null, null) + 0;
      if (i > 0) {
        zzgo().zzjl().zzg("Reset local analytics data. records", Integer.valueOf(i));
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzgo().zzjd().zzg("Error resetting local analytics data. error", localSQLiteException);
    }
  }
  
  public final boolean zza(zzad paramzzad)
  {
    Parcel localParcel = Parcel.obtain();
    paramzzad.writeToParcel(localParcel, 0);
    paramzzad = localParcel.marshall();
    localParcel.recycle();
    if (paramzzad.length > 131072)
    {
      zzgo().zzjg().zzbx("Event is too long for local database. Sending event directly to service");
      return false;
    }
    return zza(0, paramzzad);
  }
  
  public final boolean zza(zzfh paramzzfh)
  {
    Parcel localParcel = Parcel.obtain();
    paramzzfh.writeToParcel(localParcel, 0);
    paramzzfh = localParcel.marshall();
    localParcel.recycle();
    if (paramzzfh.length > 131072)
    {
      zzgo().zzjg().zzbx("User property too long for local database. Sending directly to service");
      return false;
    }
    return zza(1, paramzzfh);
  }
  
  public final boolean zzc(zzl paramzzl)
  {
    zzgm();
    paramzzl = zzfk.zza(paramzzl);
    if (paramzzl.length > 131072)
    {
      zzgo().zzjg().zzbx("Conditional user property too long for local database. Sending directly to service");
      return false;
    }
    return zza(2, paramzzl);
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  /* Error */
  public final java.util.List<com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable> zzr(int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 53	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   4: aload_0
    //   5: invokevirtual 50	com/google/android/gms/measurement/internal/zzco:zzgb	()V
    //   8: aload_0
    //   9: getfield 36	com/google/android/gms/measurement/internal/zzal:zzalr	Z
    //   12: ifeq +5 -> 17
    //   15: aconst_null
    //   16: areturn
    //   17: new 300	java/util/ArrayList
    //   20: dup
    //   21: invokespecial 301	java/util/ArrayList:<init>	()V
    //   24: astore 13
    //   26: aload_0
    //   27: invokevirtual 20	com/google/android/gms/measurement/internal/zzco:getContext	()Landroid/content/Context;
    //   30: ldc 22
    //   32: invokevirtual 307	android/content/Context:getDatabasePath	(Ljava/lang/String;)Ljava/io/File;
    //   35: invokevirtual 312	java/io/File:exists	()Z
    //   38: ifne +6 -> 44
    //   41: aload 13
    //   43: areturn
    //   44: iconst_5
    //   45: istore_1
    //   46: iconst_0
    //   47: istore_2
    //   48: iload_2
    //   49: iconst_5
    //   50: if_icmpge +817 -> 867
    //   53: aconst_null
    //   54: astore 9
    //   56: aconst_null
    //   57: astore 10
    //   59: aconst_null
    //   60: astore 11
    //   62: aload_0
    //   63: invokespecial 75	com/google/android/gms/measurement/internal/zzal:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   66: astore 8
    //   68: aload 8
    //   70: ifnonnull +20 -> 90
    //   73: aload_0
    //   74: iconst_1
    //   75: putfield 36	com/google/android/gms/measurement/internal/zzal:zzalr	Z
    //   78: aload 8
    //   80: ifnull +8 -> 88
    //   83: aload 8
    //   85: invokevirtual 80	android/database/sqlite/SQLiteDatabase:close	()V
    //   88: aconst_null
    //   89: areturn
    //   90: aload 8
    //   92: invokevirtual 83	android/database/sqlite/SQLiteDatabase:beginTransaction	()V
    //   95: bipush 100
    //   97: invokestatic 315	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   100: astore 9
    //   102: aload 8
    //   104: ldc 121
    //   106: iconst_3
    //   107: anewarray 125	java/lang/String
    //   110: dup
    //   111: iconst_0
    //   112: ldc_w 317
    //   115: aastore
    //   116: dup
    //   117: iconst_1
    //   118: ldc 59
    //   120: aastore
    //   121: dup
    //   122: iconst_2
    //   123: ldc 71
    //   125: aastore
    //   126: aconst_null
    //   127: aconst_null
    //   128: aconst_null
    //   129: aconst_null
    //   130: ldc_w 319
    //   133: aload 9
    //   135: invokevirtual 323	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   138: astore 9
    //   140: ldc2_w 324
    //   143: lstore 4
    //   145: aload 9
    //   147: invokeinterface 328 1 0
    //   152: ifeq +635 -> 787
    //   155: aload 9
    //   157: iconst_0
    //   158: invokeinterface 99 2 0
    //   163: lstore 6
    //   165: aload 9
    //   167: iconst_1
    //   168: invokeinterface 332 2 0
    //   173: istore_3
    //   174: aload 9
    //   176: iconst_2
    //   177: invokeinterface 336 2 0
    //   182: astore 12
    //   184: iload_3
    //   185: ifne +325 -> 510
    //   188: invokestatic 191	android/os/Parcel:obtain	()Landroid/os/Parcel;
    //   191: astore 10
    //   193: aload 10
    //   195: aload 12
    //   197: iconst_0
    //   198: aload 12
    //   200: arraylength
    //   201: invokevirtual 340	android/os/Parcel:unmarshall	([BII)V
    //   204: aload 10
    //   206: iconst_0
    //   207: invokevirtual 344	android/os/Parcel:setDataPosition	(I)V
    //   210: getstatic 348	com/google/android/gms/measurement/internal/zzad:CREATOR	Landroid/os/Parcelable$Creator;
    //   213: aload 10
    //   215: invokeinterface 354 2 0
    //   220: checkcast 193	com/google/android/gms/measurement/internal/zzad
    //   223: astore 11
    //   225: aload 10
    //   227: invokevirtual 204	android/os/Parcel:recycle	()V
    //   230: lload 6
    //   232: lstore 4
    //   234: aload 11
    //   236: ifnull -91 -> 145
    //   239: aload 13
    //   241: aload 11
    //   243: invokeinterface 360 2 0
    //   248: pop
    //   249: lload 6
    //   251: lstore 4
    //   253: goto -108 -> 145
    //   256: astore 11
    //   258: aload 9
    //   260: astore 10
    //   262: aload 8
    //   264: astore 9
    //   266: aload 10
    //   268: astore 8
    //   270: aload 11
    //   272: astore 10
    //   274: aload_0
    //   275: invokevirtual 105	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   278: invokevirtual 111	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   281: ldc_w 362
    //   284: aload 10
    //   286: invokevirtual 161	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   289: aload_0
    //   290: iconst_1
    //   291: putfield 36	com/google/android/gms/measurement/internal/zzal:zzalr	Z
    //   294: aload 8
    //   296: ifnull +10 -> 306
    //   299: aload 8
    //   301: invokeinterface 155 1 0
    //   306: aload 9
    //   308: ifnull +695 -> 1003
    //   311: aload 9
    //   313: invokevirtual 80	android/database/sqlite/SQLiteDatabase:close	()V
    //   316: iload_2
    //   317: iconst_1
    //   318: iadd
    //   319: istore_2
    //   320: goto -272 -> 48
    //   323: astore 11
    //   325: aload_0
    //   326: invokevirtual 105	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   329: invokevirtual 111	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   332: ldc_w 364
    //   335: invokevirtual 119	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   338: aload 10
    //   340: invokevirtual 204	android/os/Parcel:recycle	()V
    //   343: lload 6
    //   345: lstore 4
    //   347: goto -202 -> 145
    //   350: astore 10
    //   352: iload_1
    //   353: i2l
    //   354: lstore 4
    //   356: aload 8
    //   358: astore 12
    //   360: aload 9
    //   362: astore 10
    //   364: lload 4
    //   366: invokestatic 167	android/os/SystemClock:sleep	(J)V
    //   369: iload_1
    //   370: bipush 20
    //   372: iadd
    //   373: istore_3
    //   374: aload 9
    //   376: ifnull +10 -> 386
    //   379: aload 9
    //   381: invokeinterface 155 1 0
    //   386: iload_3
    //   387: istore_1
    //   388: aload 8
    //   390: ifnull -74 -> 316
    //   393: aload 8
    //   395: invokevirtual 80	android/database/sqlite/SQLiteDatabase:close	()V
    //   398: iload_3
    //   399: istore_1
    //   400: goto -84 -> 316
    //   403: astore 11
    //   405: aload 10
    //   407: invokevirtual 204	android/os/Parcel:recycle	()V
    //   410: aload 11
    //   412: athrow
    //   413: astore 11
    //   415: aload 8
    //   417: ifnull +32 -> 449
    //   420: aload 8
    //   422: astore 12
    //   424: aload 9
    //   426: astore 10
    //   428: aload 8
    //   430: invokevirtual 170	android/database/sqlite/SQLiteDatabase:inTransaction	()Z
    //   433: ifeq +16 -> 449
    //   436: aload 8
    //   438: astore 12
    //   440: aload 9
    //   442: astore 10
    //   444: aload 8
    //   446: invokevirtual 154	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   449: aload 8
    //   451: astore 12
    //   453: aload 9
    //   455: astore 10
    //   457: aload_0
    //   458: invokevirtual 105	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   461: invokevirtual 111	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   464: ldc_w 362
    //   467: aload 11
    //   469: invokevirtual 161	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   472: aload 8
    //   474: astore 12
    //   476: aload 9
    //   478: astore 10
    //   480: aload_0
    //   481: iconst_1
    //   482: putfield 36	com/google/android/gms/measurement/internal/zzal:zzalr	Z
    //   485: aload 9
    //   487: ifnull +10 -> 497
    //   490: aload 9
    //   492: invokeinterface 155 1 0
    //   497: aload 8
    //   499: ifnull +504 -> 1003
    //   502: aload 8
    //   504: invokevirtual 80	android/database/sqlite/SQLiteDatabase:close	()V
    //   507: goto -191 -> 316
    //   510: iload_3
    //   511: iconst_1
    //   512: if_icmpne +146 -> 658
    //   515: invokestatic 191	android/os/Parcel:obtain	()Landroid/os/Parcel;
    //   518: astore 11
    //   520: aload 11
    //   522: aload 12
    //   524: iconst_0
    //   525: aload 12
    //   527: arraylength
    //   528: invokevirtual 340	android/os/Parcel:unmarshall	([BII)V
    //   531: aload 11
    //   533: iconst_0
    //   534: invokevirtual 344	android/os/Parcel:setDataPosition	(I)V
    //   537: getstatic 365	com/google/android/gms/measurement/internal/zzfh:CREATOR	Landroid/os/Parcelable$Creator;
    //   540: aload 11
    //   542: invokeinterface 354 2 0
    //   547: checkcast 212	com/google/android/gms/measurement/internal/zzfh
    //   550: astore 10
    //   552: aload 11
    //   554: invokevirtual 204	android/os/Parcel:recycle	()V
    //   557: lload 6
    //   559: lstore 4
    //   561: aload 10
    //   563: ifnull -418 -> 145
    //   566: aload 13
    //   568: aload 10
    //   570: invokeinterface 360 2 0
    //   575: pop
    //   576: lload 6
    //   578: lstore 4
    //   580: goto -435 -> 145
    //   583: astore 11
    //   585: aload 9
    //   587: astore 10
    //   589: aload 8
    //   591: astore 9
    //   593: aload 11
    //   595: astore 8
    //   597: aload 10
    //   599: ifnull +10 -> 609
    //   602: aload 10
    //   604: invokeinterface 155 1 0
    //   609: aload 9
    //   611: ifnull +8 -> 619
    //   614: aload 9
    //   616: invokevirtual 80	android/database/sqlite/SQLiteDatabase:close	()V
    //   619: aload 8
    //   621: athrow
    //   622: astore 10
    //   624: aload_0
    //   625: invokevirtual 105	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   628: invokevirtual 111	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   631: ldc_w 367
    //   634: invokevirtual 119	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   637: aload 11
    //   639: invokevirtual 204	android/os/Parcel:recycle	()V
    //   642: aconst_null
    //   643: astore 10
    //   645: goto -88 -> 557
    //   648: astore 10
    //   650: aload 11
    //   652: invokevirtual 204	android/os/Parcel:recycle	()V
    //   655: aload 10
    //   657: athrow
    //   658: iload_3
    //   659: iconst_2
    //   660: if_icmpne +107 -> 767
    //   663: invokestatic 191	android/os/Parcel:obtain	()Landroid/os/Parcel;
    //   666: astore 11
    //   668: aload 11
    //   670: aload 12
    //   672: iconst_0
    //   673: aload 12
    //   675: arraylength
    //   676: invokevirtual 340	android/os/Parcel:unmarshall	([BII)V
    //   679: aload 11
    //   681: iconst_0
    //   682: invokevirtual 344	android/os/Parcel:setDataPosition	(I)V
    //   685: getstatic 370	com/google/android/gms/measurement/internal/zzl:CREATOR	Landroid/os/Parcelable$Creator;
    //   688: aload 11
    //   690: invokeinterface 354 2 0
    //   695: checkcast 369	com/google/android/gms/measurement/internal/zzl
    //   698: astore 10
    //   700: aload 11
    //   702: invokevirtual 204	android/os/Parcel:recycle	()V
    //   705: lload 6
    //   707: lstore 4
    //   709: aload 10
    //   711: ifnull -566 -> 145
    //   714: aload 13
    //   716: aload 10
    //   718: invokeinterface 360 2 0
    //   723: pop
    //   724: lload 6
    //   726: lstore 4
    //   728: goto -583 -> 145
    //   731: astore 10
    //   733: aload_0
    //   734: invokevirtual 105	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   737: invokevirtual 111	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   740: ldc_w 367
    //   743: invokevirtual 119	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   746: aload 11
    //   748: invokevirtual 204	android/os/Parcel:recycle	()V
    //   751: aconst_null
    //   752: astore 10
    //   754: goto -49 -> 705
    //   757: astore 10
    //   759: aload 11
    //   761: invokevirtual 204	android/os/Parcel:recycle	()V
    //   764: aload 10
    //   766: athrow
    //   767: aload_0
    //   768: invokevirtual 105	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   771: invokevirtual 111	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   774: ldc_w 372
    //   777: invokevirtual 119	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   780: lload 6
    //   782: lstore 4
    //   784: goto -639 -> 145
    //   787: aload 8
    //   789: ldc 121
    //   791: ldc_w 374
    //   794: iconst_1
    //   795: anewarray 125	java/lang/String
    //   798: dup
    //   799: iconst_0
    //   800: lload 4
    //   802: invokestatic 131	java/lang/Long:toString	(J)Ljava/lang/String;
    //   805: aastore
    //   806: invokevirtual 135	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   809: aload 13
    //   811: invokeinterface 378 1 0
    //   816: if_icmpge +16 -> 832
    //   819: aload_0
    //   820: invokevirtual 105	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   823: invokevirtual 111	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   826: ldc_w 380
    //   829: invokevirtual 119	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   832: aload 8
    //   834: invokevirtual 151	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   837: aload 8
    //   839: invokevirtual 154	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   842: aload 9
    //   844: ifnull +10 -> 854
    //   847: aload 9
    //   849: invokeinterface 155 1 0
    //   854: aload 8
    //   856: ifnull +8 -> 864
    //   859: aload 8
    //   861: invokevirtual 80	android/database/sqlite/SQLiteDatabase:close	()V
    //   864: aload 13
    //   866: areturn
    //   867: aload_0
    //   868: invokevirtual 105	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   871: invokevirtual 173	com/google/android/gms/measurement/internal/zzap:zzjg	()Lcom/google/android/gms/measurement/internal/zzar;
    //   874: ldc_w 382
    //   877: invokevirtual 119	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   880: aconst_null
    //   881: areturn
    //   882: astore 8
    //   884: aconst_null
    //   885: astore 11
    //   887: aload 10
    //   889: astore 9
    //   891: aload 11
    //   893: astore 10
    //   895: goto -298 -> 597
    //   898: astore 11
    //   900: aconst_null
    //   901: astore 10
    //   903: aload 8
    //   905: astore 9
    //   907: aload 11
    //   909: astore 8
    //   911: goto -314 -> 597
    //   914: astore 11
    //   916: aload 8
    //   918: astore 10
    //   920: aload 11
    //   922: astore 8
    //   924: goto -327 -> 597
    //   927: astore 8
    //   929: aload 12
    //   931: astore 9
    //   933: goto -336 -> 597
    //   936: astore 11
    //   938: aconst_null
    //   939: astore 10
    //   941: aload 9
    //   943: astore 8
    //   945: aload 10
    //   947: astore 9
    //   949: goto -534 -> 415
    //   952: astore 11
    //   954: aconst_null
    //   955: astore 9
    //   957: goto -542 -> 415
    //   960: astore 8
    //   962: aconst_null
    //   963: astore 9
    //   965: aload 11
    //   967: astore 8
    //   969: goto -617 -> 352
    //   972: astore 9
    //   974: aconst_null
    //   975: astore 9
    //   977: goto -625 -> 352
    //   980: astore 10
    //   982: aconst_null
    //   983: astore 9
    //   985: aconst_null
    //   986: astore 8
    //   988: goto -714 -> 274
    //   991: astore 10
    //   993: aload 8
    //   995: astore 9
    //   997: aconst_null
    //   998: astore 8
    //   1000: goto -726 -> 274
    //   1003: goto -687 -> 316
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1006	0	this	zzal
    //   0	1006	1	paramInt	int
    //   47	273	2	i	int
    //   173	488	3	j	int
    //   143	658	4	l1	long
    //   163	618	6	l2	long
    //   66	794	8	localObject1	Object
    //   882	22	8	localObject2	Object
    //   909	14	8	localObject3	Object
    //   927	1	8	localObject4	Object
    //   943	1	8	localObject5	Object
    //   960	1	8	localSQLiteDatabaseLockedException1	SQLiteDatabaseLockedException
    //   967	32	8	localSQLiteException1	SQLiteException
    //   54	910	9	localObject6	Object
    //   972	1	9	localSQLiteDatabaseLockedException2	SQLiteDatabaseLockedException
    //   975	21	9	localObject7	Object
    //   57	282	10	localObject8	Object
    //   350	1	10	localSQLiteDatabaseLockedException3	SQLiteDatabaseLockedException
    //   362	241	10	localObject9	Object
    //   622	1	10	localParseException1	com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ParseException
    //   643	1	10	localObject10	Object
    //   648	8	10	localObject11	Object
    //   698	19	10	localzzl	zzl
    //   731	1	10	localParseException2	com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ParseException
    //   752	1	10	localObject12	Object
    //   757	131	10	localObject13	Object
    //   893	53	10	localObject14	Object
    //   980	1	10	localSQLiteFullException1	SQLiteFullException
    //   991	1	10	localSQLiteFullException2	SQLiteFullException
    //   60	182	11	localzzad	zzad
    //   256	15	11	localSQLiteFullException3	SQLiteFullException
    //   323	1	11	localParseException3	com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ParseException
    //   403	8	11	localObject15	Object
    //   413	55	11	localSQLiteException2	SQLiteException
    //   518	35	11	localParcel1	Parcel
    //   583	68	11	localObject16	Object
    //   666	226	11	localParcel2	Parcel
    //   898	10	11	localObject17	Object
    //   914	7	11	localObject18	Object
    //   936	1	11	localSQLiteException3	SQLiteException
    //   952	14	11	localSQLiteException4	SQLiteException
    //   182	748	12	localObject19	Object
    //   24	841	13	localArrayList	java.util.ArrayList
    // Exception table:
    //   from	to	target	type
    //   145	184	256	android/database/sqlite/SQLiteFullException
    //   188	193	256	android/database/sqlite/SQLiteFullException
    //   225	230	256	android/database/sqlite/SQLiteFullException
    //   239	249	256	android/database/sqlite/SQLiteFullException
    //   338	343	256	android/database/sqlite/SQLiteFullException
    //   405	413	256	android/database/sqlite/SQLiteFullException
    //   515	520	256	android/database/sqlite/SQLiteFullException
    //   552	557	256	android/database/sqlite/SQLiteFullException
    //   566	576	256	android/database/sqlite/SQLiteFullException
    //   637	642	256	android/database/sqlite/SQLiteFullException
    //   650	658	256	android/database/sqlite/SQLiteFullException
    //   663	668	256	android/database/sqlite/SQLiteFullException
    //   700	705	256	android/database/sqlite/SQLiteFullException
    //   714	724	256	android/database/sqlite/SQLiteFullException
    //   746	751	256	android/database/sqlite/SQLiteFullException
    //   759	767	256	android/database/sqlite/SQLiteFullException
    //   767	780	256	android/database/sqlite/SQLiteFullException
    //   787	832	256	android/database/sqlite/SQLiteFullException
    //   832	842	256	android/database/sqlite/SQLiteFullException
    //   193	225	323	com/google/android/gms/common/internal/safeparcel/SafeParcelReader$ParseException
    //   145	184	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   188	193	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   225	230	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   239	249	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   338	343	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   405	413	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   515	520	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   552	557	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   566	576	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   637	642	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   650	658	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   663	668	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   700	705	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   714	724	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   746	751	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   759	767	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   767	780	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   787	832	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   832	842	350	android/database/sqlite/SQLiteDatabaseLockedException
    //   193	225	403	finally
    //   325	338	403	finally
    //   145	184	413	android/database/sqlite/SQLiteException
    //   188	193	413	android/database/sqlite/SQLiteException
    //   225	230	413	android/database/sqlite/SQLiteException
    //   239	249	413	android/database/sqlite/SQLiteException
    //   338	343	413	android/database/sqlite/SQLiteException
    //   405	413	413	android/database/sqlite/SQLiteException
    //   515	520	413	android/database/sqlite/SQLiteException
    //   552	557	413	android/database/sqlite/SQLiteException
    //   566	576	413	android/database/sqlite/SQLiteException
    //   637	642	413	android/database/sqlite/SQLiteException
    //   650	658	413	android/database/sqlite/SQLiteException
    //   663	668	413	android/database/sqlite/SQLiteException
    //   700	705	413	android/database/sqlite/SQLiteException
    //   714	724	413	android/database/sqlite/SQLiteException
    //   746	751	413	android/database/sqlite/SQLiteException
    //   759	767	413	android/database/sqlite/SQLiteException
    //   767	780	413	android/database/sqlite/SQLiteException
    //   787	832	413	android/database/sqlite/SQLiteException
    //   832	842	413	android/database/sqlite/SQLiteException
    //   145	184	583	finally
    //   188	193	583	finally
    //   225	230	583	finally
    //   239	249	583	finally
    //   338	343	583	finally
    //   405	413	583	finally
    //   515	520	583	finally
    //   552	557	583	finally
    //   566	576	583	finally
    //   637	642	583	finally
    //   650	658	583	finally
    //   663	668	583	finally
    //   700	705	583	finally
    //   714	724	583	finally
    //   746	751	583	finally
    //   759	767	583	finally
    //   767	780	583	finally
    //   787	832	583	finally
    //   832	842	583	finally
    //   520	552	622	com/google/android/gms/common/internal/safeparcel/SafeParcelReader$ParseException
    //   520	552	648	finally
    //   624	637	648	finally
    //   668	700	731	com/google/android/gms/common/internal/safeparcel/SafeParcelReader$ParseException
    //   668	700	757	finally
    //   733	746	757	finally
    //   62	68	882	finally
    //   73	78	898	finally
    //   90	140	898	finally
    //   274	294	914	finally
    //   364	369	927	finally
    //   428	436	927	finally
    //   444	449	927	finally
    //   457	472	927	finally
    //   480	485	927	finally
    //   62	68	936	android/database/sqlite/SQLiteException
    //   73	78	952	android/database/sqlite/SQLiteException
    //   90	140	952	android/database/sqlite/SQLiteException
    //   62	68	960	android/database/sqlite/SQLiteDatabaseLockedException
    //   73	78	972	android/database/sqlite/SQLiteDatabaseLockedException
    //   90	140	972	android/database/sqlite/SQLiteDatabaseLockedException
    //   62	68	980	android/database/sqlite/SQLiteFullException
    //   73	78	991	android/database/sqlite/SQLiteFullException
    //   90	140	991	android/database/sqlite/SQLiteFullException
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */