package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.support.annotation.BinderThread;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.GoogleSignatureVerifier;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.UidVerifier;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public final class zzbv
  extends zzah
{
  private final zzfa zzamz;
  private Boolean zzaql;
  @Nullable
  private String zzaqm;
  
  public zzbv(zzfa paramzzfa)
  {
    this(paramzzfa, null);
  }
  
  private zzbv(zzfa paramzzfa, @Nullable String paramString)
  {
    Preconditions.checkNotNull(paramzzfa);
    this.zzamz = paramzzfa;
    this.zzaqm = null;
  }
  
  @BinderThread
  private final void zzb(zzh paramzzh, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramzzh);
    zzc(paramzzh.packageName, false);
    this.zzamz.zzgm().zzt(paramzzh.zzafx, paramzzh.zzagk);
  }
  
  @BinderThread
  private final void zzc(String paramString, boolean paramBoolean)
  {
    boolean bool = false;
    if (TextUtils.isEmpty(paramString))
    {
      this.zzamz.zzgo().zzjd().zzbx("Measurement Service called without app package");
      throw new SecurityException("Measurement Service called without app package");
    }
    if (paramBoolean) {}
    for (;;)
    {
      try
      {
        if (this.zzaql == null)
        {
          if (("com.google.android.gms".equals(this.zzaqm)) || (UidVerifier.isGooglePlayServicesUid(this.zzamz.getContext(), Binder.getCallingUid()))) {
            break label201;
          }
          paramBoolean = bool;
          if (GoogleSignatureVerifier.getInstance(this.zzamz.getContext()).isUidGoogleSigned(Binder.getCallingUid())) {
            break label201;
          }
          this.zzaql = Boolean.valueOf(paramBoolean);
        }
        if (!this.zzaql.booleanValue())
        {
          if ((this.zzaqm == null) && (GooglePlayServicesUtilLight.uidHasPackageName(this.zzamz.getContext(), Binder.getCallingUid(), paramString))) {
            this.zzaqm = paramString;
          }
          if (!paramString.equals(this.zzaqm)) {
            throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[] { paramString }));
          }
        }
      }
      catch (SecurityException localSecurityException)
      {
        this.zzamz.zzgo().zzjd().zzg("Measurement Service called with invalid calling package. appId", zzap.zzbv(paramString));
        throw localSecurityException;
      }
      return;
      label201:
      paramBoolean = true;
    }
  }
  
  @VisibleForTesting
  private final void zze(Runnable paramRunnable)
  {
    Preconditions.checkNotNull(paramRunnable);
    if ((((Boolean)zzaf.zzakv.get()).booleanValue()) && (this.zzamz.zzgn().zzkb()))
    {
      paramRunnable.run();
      return;
    }
    this.zzamz.zzgn().zzc(paramRunnable);
  }
  
  /* Error */
  @BinderThread
  public final List<zzfh> zza(zzh paramzzh, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: iconst_0
    //   3: invokespecial 200	com/google/android/gms/measurement/internal/zzbv:zzb	(Lcom/google/android/gms/measurement/internal/zzh;Z)V
    //   6: aload_0
    //   7: getfield 28	com/google/android/gms/measurement/internal/zzbv:zzamz	Lcom/google/android/gms/measurement/internal/zzfa;
    //   10: invokevirtual 181	com/google/android/gms/measurement/internal/zzfa:zzgn	()Lcom/google/android/gms/measurement/internal/zzbo;
    //   13: new 202	com/google/android/gms/measurement/internal/zzcl
    //   16: dup
    //   17: aload_0
    //   18: aload_1
    //   19: invokespecial 205	com/google/android/gms/measurement/internal/zzcl:<init>	(Lcom/google/android/gms/measurement/internal/zzbv;Lcom/google/android/gms/measurement/internal/zzh;)V
    //   22: invokevirtual 208	com/google/android/gms/measurement/internal/zzbo:zzb	(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    //   25: astore_3
    //   26: aload_3
    //   27: invokeinterface 211 1 0
    //   32: checkcast 213	java/util/List
    //   35: astore 4
    //   37: new 215	java/util/ArrayList
    //   40: dup
    //   41: aload 4
    //   43: invokeinterface 218 1 0
    //   48: invokespecial 221	java/util/ArrayList:<init>	(I)V
    //   51: astore_3
    //   52: aload 4
    //   54: invokeinterface 225 1 0
    //   59: astore 4
    //   61: aload 4
    //   63: invokeinterface 230 1 0
    //   68: ifeq +75 -> 143
    //   71: aload 4
    //   73: invokeinterface 233 1 0
    //   78: checkcast 235	com/google/android/gms/measurement/internal/zzfj
    //   81: astore 5
    //   83: iload_2
    //   84: ifne +14 -> 98
    //   87: aload 5
    //   89: getfield 238	com/google/android/gms/measurement/internal/zzfj:name	Ljava/lang/String;
    //   92: invokestatic 242	com/google/android/gms/measurement/internal/zzfk:zzcv	(Ljava/lang/String;)Z
    //   95: ifne -34 -> 61
    //   98: aload_3
    //   99: new 244	com/google/android/gms/measurement/internal/zzfh
    //   102: dup
    //   103: aload 5
    //   105: invokespecial 247	com/google/android/gms/measurement/internal/zzfh:<init>	(Lcom/google/android/gms/measurement/internal/zzfj;)V
    //   108: invokeinterface 250 2 0
    //   113: pop
    //   114: goto -53 -> 61
    //   117: astore_3
    //   118: aload_0
    //   119: getfield 28	com/google/android/gms/measurement/internal/zzbv:zzamz	Lcom/google/android/gms/measurement/internal/zzfa;
    //   122: invokevirtual 76	com/google/android/gms/measurement/internal/zzfa:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   125: invokevirtual 82	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   128: ldc -4
    //   130: aload_1
    //   131: getfield 41	com/google/android/gms/measurement/internal/zzh:packageName	Ljava/lang/String;
    //   134: invokestatic 158	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   137: aload_3
    //   138: invokevirtual 255	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   141: aconst_null
    //   142: areturn
    //   143: aload_3
    //   144: areturn
    //   145: astore_3
    //   146: goto -28 -> 118
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	149	0	this	zzbv
    //   0	149	1	paramzzh	zzh
    //   0	149	2	paramBoolean	boolean
    //   25	74	3	localObject1	Object
    //   117	27	3	localInterruptedException	InterruptedException
    //   145	1	3	localExecutionException	ExecutionException
    //   35	37	4	localObject2	Object
    //   81	23	5	localzzfj	zzfj
    // Exception table:
    //   from	to	target	type
    //   26	61	117	java/lang/InterruptedException
    //   61	83	117	java/lang/InterruptedException
    //   87	98	117	java/lang/InterruptedException
    //   98	114	117	java/lang/InterruptedException
    //   26	61	145	java/util/concurrent/ExecutionException
    //   61	83	145	java/util/concurrent/ExecutionException
    //   87	98	145	java/util/concurrent/ExecutionException
    //   98	114	145	java/util/concurrent/ExecutionException
  }
  
  @BinderThread
  public final List<zzl> zza(String paramString1, String paramString2, zzh paramzzh)
  {
    zzb(paramzzh, false);
    paramString1 = this.zzamz.zzgn().zzb(new zzcd(this, paramzzh, paramString1, paramString2));
    try
    {
      paramString1 = (List)paramString1.get();
      return paramString1;
    }
    catch (InterruptedException paramString1)
    {
      this.zzamz.zzgo().zzjd().zzg("Failed to get conditional user properties", paramString1);
      return Collections.emptyList();
    }
    catch (ExecutionException paramString1)
    {
      for (;;) {}
    }
  }
  
  /* Error */
  @BinderThread
  public final List<zzfh> zza(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: iconst_1
    //   3: invokespecial 45	com/google/android/gms/measurement/internal/zzbv:zzc	(Ljava/lang/String;Z)V
    //   6: aload_0
    //   7: getfield 28	com/google/android/gms/measurement/internal/zzbv:zzamz	Lcom/google/android/gms/measurement/internal/zzfa;
    //   10: invokevirtual 181	com/google/android/gms/measurement/internal/zzfa:zzgn	()Lcom/google/android/gms/measurement/internal/zzbo;
    //   13: new 275	com/google/android/gms/measurement/internal/zzcc
    //   16: dup
    //   17: aload_0
    //   18: aload_1
    //   19: aload_2
    //   20: aload_3
    //   21: invokespecial 278	com/google/android/gms/measurement/internal/zzcc:<init>	(Lcom/google/android/gms/measurement/internal/zzbv;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   24: invokevirtual 208	com/google/android/gms/measurement/internal/zzbo:zzb	(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    //   27: astore_2
    //   28: aload_2
    //   29: invokeinterface 211 1 0
    //   34: checkcast 213	java/util/List
    //   37: astore_3
    //   38: new 215	java/util/ArrayList
    //   41: dup
    //   42: aload_3
    //   43: invokeinterface 218 1 0
    //   48: invokespecial 221	java/util/ArrayList:<init>	(I)V
    //   51: astore_2
    //   52: aload_3
    //   53: invokeinterface 225 1 0
    //   58: astore_3
    //   59: aload_3
    //   60: invokeinterface 230 1 0
    //   65: ifeq +74 -> 139
    //   68: aload_3
    //   69: invokeinterface 233 1 0
    //   74: checkcast 235	com/google/android/gms/measurement/internal/zzfj
    //   77: astore 5
    //   79: iload 4
    //   81: ifne +14 -> 95
    //   84: aload 5
    //   86: getfield 238	com/google/android/gms/measurement/internal/zzfj:name	Ljava/lang/String;
    //   89: invokestatic 242	com/google/android/gms/measurement/internal/zzfk:zzcv	(Ljava/lang/String;)Z
    //   92: ifne -33 -> 59
    //   95: aload_2
    //   96: new 244	com/google/android/gms/measurement/internal/zzfh
    //   99: dup
    //   100: aload 5
    //   102: invokespecial 247	com/google/android/gms/measurement/internal/zzfh:<init>	(Lcom/google/android/gms/measurement/internal/zzfj;)V
    //   105: invokeinterface 250 2 0
    //   110: pop
    //   111: goto -52 -> 59
    //   114: astore_2
    //   115: aload_0
    //   116: getfield 28	com/google/android/gms/measurement/internal/zzbv:zzamz	Lcom/google/android/gms/measurement/internal/zzfa;
    //   119: invokevirtual 76	com/google/android/gms/measurement/internal/zzfa:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   122: invokevirtual 82	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   125: ldc -4
    //   127: aload_1
    //   128: invokestatic 158	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   131: aload_2
    //   132: invokevirtual 255	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   135: invokestatic 271	java/util/Collections:emptyList	()Ljava/util/List;
    //   138: areturn
    //   139: aload_2
    //   140: areturn
    //   141: astore_2
    //   142: goto -27 -> 115
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	145	0	this	zzbv
    //   0	145	1	paramString1	String
    //   0	145	2	paramString2	String
    //   0	145	3	paramString3	String
    //   0	145	4	paramBoolean	boolean
    //   77	24	5	localzzfj	zzfj
    // Exception table:
    //   from	to	target	type
    //   28	59	114	java/lang/InterruptedException
    //   59	79	114	java/lang/InterruptedException
    //   84	95	114	java/lang/InterruptedException
    //   95	111	114	java/lang/InterruptedException
    //   28	59	141	java/util/concurrent/ExecutionException
    //   59	79	141	java/util/concurrent/ExecutionException
    //   84	95	141	java/util/concurrent/ExecutionException
    //   95	111	141	java/util/concurrent/ExecutionException
  }
  
  /* Error */
  @BinderThread
  public final List<zzfh> zza(String paramString1, String paramString2, boolean paramBoolean, zzh paramzzh)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload 4
    //   3: iconst_0
    //   4: invokespecial 200	com/google/android/gms/measurement/internal/zzbv:zzb	(Lcom/google/android/gms/measurement/internal/zzh;Z)V
    //   7: aload_0
    //   8: getfield 28	com/google/android/gms/measurement/internal/zzbv:zzamz	Lcom/google/android/gms/measurement/internal/zzfa;
    //   11: invokevirtual 181	com/google/android/gms/measurement/internal/zzfa:zzgn	()Lcom/google/android/gms/measurement/internal/zzbo;
    //   14: new 282	com/google/android/gms/measurement/internal/zzcb
    //   17: dup
    //   18: aload_0
    //   19: aload 4
    //   21: aload_1
    //   22: aload_2
    //   23: invokespecial 283	com/google/android/gms/measurement/internal/zzcb:<init>	(Lcom/google/android/gms/measurement/internal/zzbv;Lcom/google/android/gms/measurement/internal/zzh;Ljava/lang/String;Ljava/lang/String;)V
    //   26: invokevirtual 208	com/google/android/gms/measurement/internal/zzbo:zzb	(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    //   29: astore_1
    //   30: aload_1
    //   31: invokeinterface 211 1 0
    //   36: checkcast 213	java/util/List
    //   39: astore_2
    //   40: new 215	java/util/ArrayList
    //   43: dup
    //   44: aload_2
    //   45: invokeinterface 218 1 0
    //   50: invokespecial 221	java/util/ArrayList:<init>	(I)V
    //   53: astore_1
    //   54: aload_2
    //   55: invokeinterface 225 1 0
    //   60: astore_2
    //   61: aload_2
    //   62: invokeinterface 230 1 0
    //   67: ifeq +77 -> 144
    //   70: aload_2
    //   71: invokeinterface 233 1 0
    //   76: checkcast 235	com/google/android/gms/measurement/internal/zzfj
    //   79: astore 5
    //   81: iload_3
    //   82: ifne +14 -> 96
    //   85: aload 5
    //   87: getfield 238	com/google/android/gms/measurement/internal/zzfj:name	Ljava/lang/String;
    //   90: invokestatic 242	com/google/android/gms/measurement/internal/zzfk:zzcv	(Ljava/lang/String;)Z
    //   93: ifne -32 -> 61
    //   96: aload_1
    //   97: new 244	com/google/android/gms/measurement/internal/zzfh
    //   100: dup
    //   101: aload 5
    //   103: invokespecial 247	com/google/android/gms/measurement/internal/zzfh:<init>	(Lcom/google/android/gms/measurement/internal/zzfj;)V
    //   106: invokeinterface 250 2 0
    //   111: pop
    //   112: goto -51 -> 61
    //   115: astore_1
    //   116: aload_0
    //   117: getfield 28	com/google/android/gms/measurement/internal/zzbv:zzamz	Lcom/google/android/gms/measurement/internal/zzfa;
    //   120: invokevirtual 76	com/google/android/gms/measurement/internal/zzfa:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   123: invokevirtual 82	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   126: ldc -4
    //   128: aload 4
    //   130: getfield 41	com/google/android/gms/measurement/internal/zzh:packageName	Ljava/lang/String;
    //   133: invokestatic 158	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   136: aload_1
    //   137: invokevirtual 255	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   140: invokestatic 271	java/util/Collections:emptyList	()Ljava/util/List;
    //   143: areturn
    //   144: aload_1
    //   145: areturn
    //   146: astore_1
    //   147: goto -31 -> 116
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	150	0	this	zzbv
    //   0	150	1	paramString1	String
    //   0	150	2	paramString2	String
    //   0	150	3	paramBoolean	boolean
    //   0	150	4	paramzzh	zzh
    //   79	23	5	localzzfj	zzfj
    // Exception table:
    //   from	to	target	type
    //   30	61	115	java/lang/InterruptedException
    //   61	81	115	java/lang/InterruptedException
    //   85	96	115	java/lang/InterruptedException
    //   96	112	115	java/lang/InterruptedException
    //   30	61	146	java/util/concurrent/ExecutionException
    //   61	81	146	java/util/concurrent/ExecutionException
    //   85	96	146	java/util/concurrent/ExecutionException
    //   96	112	146	java/util/concurrent/ExecutionException
  }
  
  @BinderThread
  public final void zza(long paramLong, String paramString1, String paramString2, String paramString3)
  {
    zze(new zzcn(this, paramString2, paramString3, paramString1, paramLong));
  }
  
  @BinderThread
  public final void zza(zzad paramzzad, zzh paramzzh)
  {
    Preconditions.checkNotNull(paramzzad);
    zzb(paramzzh, false);
    zze(new zzcg(this, paramzzad, paramzzh));
  }
  
  @BinderThread
  public final void zza(zzad paramzzad, String paramString1, String paramString2)
  {
    Preconditions.checkNotNull(paramzzad);
    Preconditions.checkNotEmpty(paramString1);
    zzc(paramString1, true);
    zze(new zzch(this, paramzzad, paramString1));
  }
  
  @BinderThread
  public final void zza(zzfh paramzzfh, zzh paramzzh)
  {
    Preconditions.checkNotNull(paramzzfh);
    zzb(paramzzh, false);
    if (paramzzfh.getValue() == null)
    {
      zze(new zzcj(this, paramzzfh, paramzzh));
      return;
    }
    zze(new zzck(this, paramzzfh, paramzzh));
  }
  
  @BinderThread
  public final void zza(zzh paramzzh)
  {
    zzb(paramzzh, false);
    zze(new zzcm(this, paramzzh));
  }
  
  @BinderThread
  public final void zza(zzl paramzzl, zzh paramzzh)
  {
    Preconditions.checkNotNull(paramzzl);
    Preconditions.checkNotNull(paramzzl.zzahb);
    zzb(paramzzh, false);
    zzl localzzl = new zzl(paramzzl);
    localzzl.packageName = paramzzh.packageName;
    if (paramzzl.zzahb.getValue() == null)
    {
      zze(new zzbx(this, localzzl, paramzzh));
      return;
    }
    zze(new zzby(this, localzzl, paramzzh));
  }
  
  @BinderThread
  public final byte[] zza(zzad paramzzad, String paramString)
  {
    Preconditions.checkNotEmpty(paramString);
    Preconditions.checkNotNull(paramzzad);
    zzc(paramString, true);
    this.zzamz.zzgo().zzjk().zzg("Log and bundle. event", this.zzamz.zzgl().zzbs(paramzzad.name));
    long l1 = this.zzamz.zzbx().nanoTime() / 1000000L;
    Object localObject = this.zzamz.zzgn().zzc(new zzci(this, paramzzad, paramString));
    try
    {
      byte[] arrayOfByte = (byte[])((Future)localObject).get();
      localObject = arrayOfByte;
      if (arrayOfByte == null)
      {
        this.zzamz.zzgo().zzjd().zzg("Log and bundle returned null. appId", zzap.zzbv(paramString));
        localObject = new byte[0];
      }
      long l2 = this.zzamz.zzbx().nanoTime() / 1000000L;
      this.zzamz.zzgo().zzjk().zzd("Log and bundle processed. event, size, time_ms", this.zzamz.zzgl().zzbs(paramzzad.name), Integer.valueOf(localObject.length), Long.valueOf(l2 - l1));
      return (byte[])localObject;
    }
    catch (InterruptedException localInterruptedException)
    {
      this.zzamz.zzgo().zzjd().zzd("Failed to log and bundle. appId, event, error", zzap.zzbv(paramString), this.zzamz.zzgl().zzbs(paramzzad.name), localInterruptedException);
      return null;
    }
    catch (ExecutionException localExecutionException)
    {
      for (;;) {}
    }
  }
  
  @VisibleForTesting
  final zzad zzb(zzad paramzzad, zzh paramzzh)
  {
    int j = 0;
    int i = j;
    if ("_cmp".equals(paramzzad.name))
    {
      i = j;
      if (paramzzad.zzaid != null)
      {
        if (paramzzad.zzaid.size() != 0) {
          break label89;
        }
        i = j;
      }
    }
    while (i != 0)
    {
      this.zzamz.zzgo().zzjj().zzg("Event has been filtered ", paramzzad.toString());
      return new zzad("_cmpx", paramzzad.zzaid, paramzzad.origin, paramzzad.zzaip);
      label89:
      String str = paramzzad.zzaid.getString("_cis");
      i = j;
      if (!TextUtils.isEmpty(str)) {
        if (!"referrer broadcast".equals(str))
        {
          i = j;
          if (!"referrer API".equals(str)) {}
        }
        else
        {
          i = j;
          if (this.zzamz.zzgq().zzbg(paramzzh.packageName)) {
            i = 1;
          }
        }
      }
    }
    return paramzzad;
  }
  
  @BinderThread
  public final void zzb(zzh paramzzh)
  {
    zzb(paramzzh, false);
    zze(new zzbw(this, paramzzh));
  }
  
  @BinderThread
  public final void zzb(zzl paramzzl)
  {
    Preconditions.checkNotNull(paramzzl);
    Preconditions.checkNotNull(paramzzl.zzahb);
    zzc(paramzzl.packageName, true);
    zzl localzzl = new zzl(paramzzl);
    if (paramzzl.zzahb.getValue() == null)
    {
      zze(new zzbz(this, localzzl));
      return;
    }
    zze(new zzca(this, localzzl));
  }
  
  @BinderThread
  public final String zzc(zzh paramzzh)
  {
    zzb(paramzzh, false);
    return this.zzamz.zzh(paramzzh);
  }
  
  @BinderThread
  public final void zzd(zzh paramzzh)
  {
    zzc(paramzzh.packageName, false);
    zze(new zzcf(this, paramzzh));
  }
  
  @BinderThread
  public final List<zzl> zze(String paramString1, String paramString2, String paramString3)
  {
    zzc(paramString1, true);
    paramString1 = this.zzamz.zzgn().zzb(new zzce(this, paramString1, paramString2, paramString3));
    try
    {
      paramString1 = (List)paramString1.get();
      return paramString1;
    }
    catch (InterruptedException paramString1)
    {
      this.zzamz.zzgo().zzjd().zzg("Failed to get conditional user properties", paramString1);
      return Collections.emptyList();
    }
    catch (ExecutionException paramString1)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */