package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzdx;

public final class zzeq
  extends zzf
{
  private Handler handler;
  @VisibleForTesting
  private long zzasw = zzbx().elapsedRealtime();
  private final zzv zzasx = new zzer(this, this.zzadj);
  private final zzv zzasy = new zzes(this, this.zzadj);
  
  zzeq(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  @WorkerThread
  private final void zzal(long paramLong)
  {
    zzaf();
    zzli();
    zzgo().zzjl().zzg("Activity resumed, time", Long.valueOf(paramLong));
    this.zzasw = paramLong;
    if (zzgq().zzbj(zzgf().zzal()))
    {
      zzam(zzbx().currentTimeMillis());
      return;
    }
    this.zzasx.cancel();
    this.zzasy.cancel();
    if (zzbx().currentTimeMillis() - zzgp().zzanq.get() > zzgp().zzant.get())
    {
      zzgp().zzanr.set(true);
      zzgp().zzanu.set(0L);
    }
    if (zzgp().zzanr.get())
    {
      this.zzasx.zzh(Math.max(0L, zzgp().zzanp.get() - zzgp().zzanu.get()));
      return;
    }
    this.zzasy.zzh(Math.max(0L, 3600000L - zzgp().zzanu.get()));
  }
  
  @WorkerThread
  private final void zzan(long paramLong)
  {
    zzaf();
    zzli();
    this.zzasx.cancel();
    this.zzasy.cancel();
    zzgo().zzjl().zzg("Activity paused, time", Long.valueOf(paramLong));
    if (this.zzasw != 0L) {
      zzgp().zzanu.set(zzgp().zzanu.get() + (paramLong - this.zzasw));
    }
  }
  
  @WorkerThread
  private final void zzao(long paramLong)
  {
    zzaf();
    long l = zzbx().elapsedRealtime();
    zzgo().zzjl().zzg("Session started, time", Long.valueOf(l));
    if (zzgq().zzbi(zzgf().zzal()))
    {
      l = paramLong / 1000L;
      zzge().zza("auto", "_sid", Long.valueOf(l), paramLong);
    }
    for (;;)
    {
      zzgp().zzanr.set(false);
      Bundle localBundle = new Bundle();
      if (zzgq().zzbi(zzgf().zzal())) {
        localBundle.putLong("_sid", paramLong / 1000L);
      }
      zzge().zza("auto", "_s", paramLong, localBundle);
      zzgp().zzant.set(paramLong);
      return;
      zzge().zza("auto", "_sid", null, paramLong);
    }
  }
  
  private final void zzli()
  {
    try
    {
      if (this.handler == null) {
        this.handler = new zzdx(Looper.getMainLooper());
      }
      return;
    }
    finally {}
  }
  
  @WorkerThread
  private final void zzll()
  {
    zzaf();
    zzn(false);
    zzgd().zzq(zzbx().elapsedRealtime());
  }
  
  @WorkerThread
  final void zzam(long paramLong)
  {
    zzaf();
    zzli();
    this.zzasx.cancel();
    this.zzasy.cancel();
    if (paramLong - zzgp().zzanq.get() > zzgp().zzant.get())
    {
      zzgp().zzanr.set(true);
      zzgp().zzanu.set(0L);
    }
    if (zzgp().zzanr.get())
    {
      zzao(paramLong);
      return;
    }
    this.zzasy.zzh(Math.max(0L, 3600000L - zzgp().zzanu.get()));
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  final void zzlj()
  {
    this.zzasx.cancel();
    this.zzasy.cancel();
    this.zzasw = 0L;
  }
  
  @WorkerThread
  @VisibleForTesting
  protected final void zzlk()
  {
    zzaf();
    zzao(zzbx().currentTimeMillis());
  }
  
  @WorkerThread
  public final boolean zzn(boolean paramBoolean)
  {
    zzaf();
    zzcl();
    long l1 = zzbx().elapsedRealtime();
    zzgp().zzant.set(zzbx().currentTimeMillis());
    long l2 = l1 - this.zzasw;
    if ((!paramBoolean) && (l2 < 1000L))
    {
      zzgo().zzjl().zzg("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(l2));
      return false;
    }
    zzgp().zzanu.set(l2);
    zzgo().zzjl().zzg("Recording user engagement, ms", Long.valueOf(l2));
    Bundle localBundle = new Bundle();
    localBundle.putLong("_et", l2);
    zzdo.zza(zzgh().zzla(), localBundle, true);
    zzge().logEvent("auto", "_e", localBundle);
    this.zzasw = l1;
    this.zzasy.cancel();
    this.zzasy.zzh(Math.max(0L, 3600000L - zzgp().zzanu.get()));
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzeq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */