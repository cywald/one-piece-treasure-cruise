package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

final class zzdy
  implements Runnable
{
  zzdy(zzdr paramzzdr, zzh paramzzh) {}
  
  public final void run()
  {
    zzag localzzag = zzdr.zzd(this.zzasg);
    if (localzzag == null)
    {
      this.zzasg.zzgo().zzjd().zzbx("Failed to send measurementEnabled to service");
      return;
    }
    try
    {
      localzzag.zzb(this.zzaqn);
      zzdr.zze(this.zzasg);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.zzasg.zzgo().zzjd().zzg("Failed to send measurementEnabled to the service", localRemoteException);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */