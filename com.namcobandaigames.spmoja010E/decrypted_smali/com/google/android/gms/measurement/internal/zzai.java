package com.google.android.gms.measurement.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.zzq;
import com.google.android.gms.internal.measurement.zzs;
import java.util.List;

public final class zzai
  extends zzq
  implements zzag
{
  zzai(IBinder paramIBinder)
  {
    super(paramIBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
  }
  
  public final List<zzfh> zza(zzh paramzzh, boolean paramBoolean)
    throws RemoteException
  {
    Object localObject = obtainAndWriteInterfaceToken();
    zzs.zza((Parcel)localObject, paramzzh);
    zzs.writeBoolean((Parcel)localObject, paramBoolean);
    paramzzh = transactAndReadException(7, (Parcel)localObject);
    localObject = paramzzh.createTypedArrayList(zzfh.CREATOR);
    paramzzh.recycle();
    return (List<zzfh>)localObject;
  }
  
  public final List<zzl> zza(String paramString1, String paramString2, zzh paramzzh)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    localParcel.writeString(paramString1);
    localParcel.writeString(paramString2);
    zzs.zza(localParcel, paramzzh);
    paramString1 = transactAndReadException(16, localParcel);
    paramString2 = paramString1.createTypedArrayList(zzl.CREATOR);
    paramString1.recycle();
    return paramString2;
  }
  
  public final List<zzfh> zza(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    localParcel.writeString(paramString1);
    localParcel.writeString(paramString2);
    localParcel.writeString(paramString3);
    zzs.writeBoolean(localParcel, paramBoolean);
    paramString1 = transactAndReadException(15, localParcel);
    paramString2 = paramString1.createTypedArrayList(zzfh.CREATOR);
    paramString1.recycle();
    return paramString2;
  }
  
  public final List<zzfh> zza(String paramString1, String paramString2, boolean paramBoolean, zzh paramzzh)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    localParcel.writeString(paramString1);
    localParcel.writeString(paramString2);
    zzs.writeBoolean(localParcel, paramBoolean);
    zzs.zza(localParcel, paramzzh);
    paramString1 = transactAndReadException(14, localParcel);
    paramString2 = paramString1.createTypedArrayList(zzfh.CREATOR);
    paramString1.recycle();
    return paramString2;
  }
  
  public final void zza(long paramLong, String paramString1, String paramString2, String paramString3)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    localParcel.writeLong(paramLong);
    localParcel.writeString(paramString1);
    localParcel.writeString(paramString2);
    localParcel.writeString(paramString3);
    transactAndReadExceptionReturnVoid(10, localParcel);
  }
  
  public final void zza(zzad paramzzad, zzh paramzzh)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzad);
    zzs.zza(localParcel, paramzzh);
    transactAndReadExceptionReturnVoid(1, localParcel);
  }
  
  public final void zza(zzad paramzzad, String paramString1, String paramString2)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzad);
    localParcel.writeString(paramString1);
    localParcel.writeString(paramString2);
    transactAndReadExceptionReturnVoid(5, localParcel);
  }
  
  public final void zza(zzfh paramzzfh, zzh paramzzh)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzfh);
    zzs.zza(localParcel, paramzzh);
    transactAndReadExceptionReturnVoid(2, localParcel);
  }
  
  public final void zza(zzh paramzzh)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzh);
    transactAndReadExceptionReturnVoid(4, localParcel);
  }
  
  public final void zza(zzl paramzzl, zzh paramzzh)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzl);
    zzs.zza(localParcel, paramzzh);
    transactAndReadExceptionReturnVoid(12, localParcel);
  }
  
  public final byte[] zza(zzad paramzzad, String paramString)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzad);
    localParcel.writeString(paramString);
    paramzzad = transactAndReadException(9, localParcel);
    paramString = paramzzad.createByteArray();
    paramzzad.recycle();
    return paramString;
  }
  
  public final void zzb(zzh paramzzh)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzh);
    transactAndReadExceptionReturnVoid(6, localParcel);
  }
  
  public final void zzb(zzl paramzzl)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzl);
    transactAndReadExceptionReturnVoid(13, localParcel);
  }
  
  public final String zzc(zzh paramzzh)
    throws RemoteException
  {
    Object localObject = obtainAndWriteInterfaceToken();
    zzs.zza((Parcel)localObject, paramzzh);
    paramzzh = transactAndReadException(11, (Parcel)localObject);
    localObject = paramzzh.readString();
    paramzzh.recycle();
    return (String)localObject;
  }
  
  public final void zzd(zzh paramzzh)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzs.zza(localParcel, paramzzh);
    transactAndReadExceptionReturnVoid(18, localParcel);
  }
  
  public final List<zzl> zze(String paramString1, String paramString2, String paramString3)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    localParcel.writeString(paramString1);
    localParcel.writeString(paramString2);
    localParcel.writeString(paramString3);
    paramString1 = transactAndReadException(17, localParcel);
    paramString2 = paramString1.createTypedArrayList(zzl.CREATOR);
    paramString1.recycle();
    return paramString2;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */