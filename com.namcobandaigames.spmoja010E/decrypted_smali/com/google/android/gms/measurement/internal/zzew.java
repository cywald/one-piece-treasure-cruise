package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.PersistableBundle;
import com.google.android.gms.common.util.Clock;

public final class zzew
  extends zzez
{
  private final zzv zzata;
  private final AlarmManager zzyt = (AlarmManager)getContext().getSystemService("alarm");
  private Integer zzyu;
  
  protected zzew(zzfa paramzzfa)
  {
    super(paramzzfa);
    this.zzata = new zzex(this, paramzzfa.zzmb(), paramzzfa);
  }
  
  private final int getJobId()
  {
    if (this.zzyu == null)
    {
      str = String.valueOf(getContext().getPackageName());
      if (str.length() == 0) {
        break label51;
      }
    }
    label51:
    for (String str = "measurement".concat(str);; str = new String("measurement"))
    {
      this.zzyu = Integer.valueOf(str.hashCode());
      return this.zzyu.intValue();
    }
  }
  
  private final PendingIntent zzeo()
  {
    Intent localIntent = new Intent().setClassName(getContext(), "com.google.android.gms.measurement.AppMeasurementReceiver");
    localIntent.setAction("com.google.android.gms.measurement.UPLOAD");
    return PendingIntent.getBroadcast(getContext(), 0, localIntent, 0);
  }
  
  @TargetApi(24)
  private final void zzlm()
  {
    JobScheduler localJobScheduler = (JobScheduler)getContext().getSystemService("jobscheduler");
    zzgo().zzjl().zzg("Cancelling job. JobID", Integer.valueOf(getJobId()));
    localJobScheduler.cancel(getJobId());
  }
  
  public final void cancel()
  {
    zzcl();
    this.zzyt.cancel(zzeo());
    this.zzata.cancel();
    if (Build.VERSION.SDK_INT >= 24) {
      zzlm();
    }
  }
  
  protected final boolean zzgt()
  {
    this.zzyt.cancel(zzeo());
    if (Build.VERSION.SDK_INT >= 24) {
      zzlm();
    }
    return false;
  }
  
  public final void zzh(long paramLong)
  {
    zzcl();
    zzgr();
    if (!zzbj.zza(getContext())) {
      zzgo().zzjk().zzbx("Receiver not registered/enabled");
    }
    zzgr();
    if (!zzfk.zza(getContext(), false)) {
      zzgo().zzjk().zzbx("Service not registered/enabled");
    }
    cancel();
    long l = zzbx().elapsedRealtime();
    if ((paramLong < Math.max(0L, ((Long)zzaf.zzaka.get()).longValue())) && (!this.zzata.zzej()))
    {
      zzgo().zzjl().zzbx("Scheduling upload with DelayedRunnable");
      this.zzata.zzh(paramLong);
    }
    zzgr();
    if (Build.VERSION.SDK_INT >= 24)
    {
      zzgo().zzjl().zzbx("Scheduling upload with JobScheduler");
      Object localObject = new ComponentName(getContext(), "com.google.android.gms.measurement.AppMeasurementJobService");
      JobScheduler localJobScheduler = (JobScheduler)getContext().getSystemService("jobscheduler");
      localObject = new JobInfo.Builder(getJobId(), (ComponentName)localObject);
      ((JobInfo.Builder)localObject).setMinimumLatency(paramLong);
      ((JobInfo.Builder)localObject).setOverrideDeadline(paramLong << 1);
      PersistableBundle localPersistableBundle = new PersistableBundle();
      localPersistableBundle.putString("action", "com.google.android.gms.measurement.UPLOAD");
      ((JobInfo.Builder)localObject).setExtras(localPersistableBundle);
      localObject = ((JobInfo.Builder)localObject).build();
      zzgo().zzjl().zzg("Scheduling job. JobID", Integer.valueOf(getJobId()));
      localJobScheduler.schedule((JobInfo)localObject);
      return;
    }
    zzgo().zzjl().zzbx("Scheduling upload with AlarmManager");
    this.zzyt.setInexactRepeating(2, l + paramLong, Math.max(((Long)zzaf.zzajv.get()).longValue(), paramLong), zzeo());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzew.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */