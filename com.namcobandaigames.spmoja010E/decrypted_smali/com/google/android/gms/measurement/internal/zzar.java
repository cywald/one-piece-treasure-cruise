package com.google.android.gms.measurement.internal;

public final class zzar
{
  private final int priority;
  private final boolean zzamn;
  private final boolean zzamo;
  
  zzar(zzap paramzzap, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.priority = paramInt;
    this.zzamn = paramBoolean1;
    this.zzamo = paramBoolean2;
  }
  
  public final void zzbx(String paramString)
  {
    this.zzamm.zza(this.priority, this.zzamn, this.zzamo, paramString, null, null, null);
  }
  
  public final void zzd(String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    this.zzamm.zza(this.priority, this.zzamn, this.zzamo, paramString, paramObject1, paramObject2, paramObject3);
  }
  
  public final void zze(String paramString, Object paramObject1, Object paramObject2)
  {
    this.zzamm.zza(this.priority, this.zzamn, this.zzamo, paramString, paramObject1, paramObject2, null);
  }
  
  public final void zzg(String paramString, Object paramObject)
  {
    this.zzamm.zza(this.priority, this.zzamn, this.zzamo, paramString, paramObject, null, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */