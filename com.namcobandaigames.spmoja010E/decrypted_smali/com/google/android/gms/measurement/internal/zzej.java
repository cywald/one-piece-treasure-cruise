package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;

final class zzej
  implements Runnable
{
  zzej(zzef paramzzef) {}
  
  public final void run()
  {
    zzdr localzzdr = this.zzasp.zzasg;
    Context localContext = this.zzasp.zzasg.getContext();
    this.zzasp.zzasg.zzgr();
    zzdr.zza(localzzdr, new ComponentName(localContext, "com.google.android.gms.measurement.AppMeasurementService"));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzej.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */