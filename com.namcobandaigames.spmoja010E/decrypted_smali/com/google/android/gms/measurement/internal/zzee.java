package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

final class zzee
  implements Runnable
{
  zzee(zzdr paramzzdr, AtomicReference paramAtomicReference, zzh paramzzh, boolean paramBoolean) {}
  
  public final void run()
  {
    localAtomicReference = this.zzash;
    for (;;)
    {
      try
      {
        localzzag = zzdr.zzd(this.zzasg);
        if (localzzag == null) {
          this.zzasg.zzgo().zzjd().zzbx("Failed to get user properties");
        }
      }
      catch (RemoteException localRemoteException)
      {
        zzag localzzag;
        this.zzasg.zzgo().zzjd().zzg("Failed to get user properties", localRemoteException);
        this.zzash.notify();
        continue;
      }
      finally
      {
        this.zzash.notify();
      }
      try
      {
        this.zzash.notify();
        return;
      }
      finally {}
    }
    this.zzash.set(localzzag.zza(this.zzaqn, this.zzaev));
    zzdr.zze(this.zzasg);
    this.zzash.notify();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */