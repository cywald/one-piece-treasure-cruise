package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Looper;

public final class zzk
{
  private final boolean zzaha = false;
  
  zzk(Context paramContext) {}
  
  public static boolean isMainThread()
  {
    return Looper.myLooper() == Looper.getMainLooper();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */