package com.google.android.gms.measurement.internal;

import android.content.Context;

final class zzbk
  implements Runnable
{
  zzbk(zzbj paramzzbj, zzbt paramzzbt, zzap paramzzap) {}
  
  public final void run()
  {
    if (this.zzaoj.zzkg() == null)
    {
      this.zzaok.zzjd().zzbx("Install Referrer Reporter is null");
      return;
    }
    zzbg localzzbg = this.zzaoj.zzkg();
    localzzbg.zzadj.zzgb();
    localzzbg.zzcd(localzzbg.zzadj.getContext().getPackageName());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */