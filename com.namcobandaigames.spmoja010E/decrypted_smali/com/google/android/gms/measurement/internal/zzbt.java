package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.api.internal.GoogleServices;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;
import com.google.android.gms.common.wrappers.PackageManagerWrapper;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.measurement.zzsl;
import com.google.android.gms.measurement.AppMeasurement;
import java.util.concurrent.atomic.AtomicReference;

public class zzbt
  implements zzcq
{
  private static volatile zzbt zzapl;
  private final boolean zzadv;
  private final String zzadx;
  private final long zzagx;
  private final zzk zzaiq;
  private final String zzapm;
  private final String zzapn;
  private final zzn zzapo;
  private final zzba zzapp;
  private final zzap zzapq;
  private final zzbo zzapr;
  private final zzeq zzaps;
  private final AppMeasurement zzapt;
  private final zzfk zzapu;
  private final zzan zzapv;
  private final zzdo zzapw;
  private final zzcs zzapx;
  private final zza zzapy;
  private zzal zzapz;
  private zzdr zzaqa;
  private zzx zzaqb;
  private zzaj zzaqc;
  private zzbg zzaqd;
  private Boolean zzaqe;
  private long zzaqf;
  private volatile Boolean zzaqg;
  private int zzaqh;
  private int zzaqi;
  private final Context zzri;
  private final Clock zzrz;
  private boolean zzvz = false;
  
  private zzbt(zzcr paramzzcr)
  {
    Preconditions.checkNotNull(paramzzcr);
    this.zzaiq = new zzk(paramzzcr.zzri);
    zzaf.zza(this.zzaiq);
    this.zzri = paramzzcr.zzri;
    this.zzadx = paramzzcr.zzadx;
    this.zzapm = paramzzcr.zzapm;
    this.zzapn = paramzzcr.zzapn;
    this.zzadv = paramzzcr.zzadv;
    this.zzaqg = paramzzcr.zzaqg;
    zzsl.init(this.zzri);
    this.zzrz = DefaultClock.getInstance();
    this.zzagx = this.zzrz.currentTimeMillis();
    this.zzapo = new zzn(this);
    Object localObject = new zzba(this);
    ((zzcp)localObject).zzq();
    this.zzapp = ((zzba)localObject);
    localObject = new zzap(this);
    ((zzcp)localObject).zzq();
    this.zzapq = ((zzap)localObject);
    localObject = new zzfk(this);
    ((zzcp)localObject).zzq();
    this.zzapu = ((zzfk)localObject);
    localObject = new zzan(this);
    ((zzcp)localObject).zzq();
    this.zzapv = ((zzan)localObject);
    this.zzapy = new zza(this);
    localObject = new zzdo(this);
    ((zzf)localObject).zzq();
    this.zzapw = ((zzdo)localObject);
    localObject = new zzcs(this);
    ((zzf)localObject).zzq();
    this.zzapx = ((zzcs)localObject);
    this.zzapt = new AppMeasurement(this);
    localObject = new zzeq(this);
    ((zzf)localObject).zzq();
    this.zzaps = ((zzeq)localObject);
    localObject = new zzbo(this);
    ((zzcp)localObject).zzq();
    this.zzapr = ((zzbo)localObject);
    localObject = this.zzaiq;
    if ((this.zzri.getApplicationContext() instanceof Application))
    {
      localObject = zzge();
      if ((((zzco)localObject).getContext().getApplicationContext() instanceof Application))
      {
        Application localApplication = (Application)((zzco)localObject).getContext().getApplicationContext();
        if (((zzcs)localObject).zzaqv == null) {
          ((zzcs)localObject).zzaqv = new zzdm((zzcs)localObject, null);
        }
        localApplication.unregisterActivityLifecycleCallbacks(((zzcs)localObject).zzaqv);
        localApplication.registerActivityLifecycleCallbacks(((zzcs)localObject).zzaqv);
        ((zzco)localObject).zzgo().zzjl().zzbx("Registered activity lifecycle callback");
      }
    }
    for (;;)
    {
      this.zzapr.zzc(new zzbu(this, paramzzcr));
      return;
      zzgo().zzjg().zzbx("Application context is not an Application");
    }
  }
  
  public static zzbt zza(Context paramContext, zzak paramzzak)
  {
    zzak localzzak = paramzzak;
    if (paramzzak != null) {
      if (paramzzak.origin != null)
      {
        localzzak = paramzzak;
        if (paramzzak.zzadx != null) {}
      }
      else
      {
        localzzak = new zzak(paramzzak.zzadt, paramzzak.zzadu, paramzzak.zzadv, paramzzak.zzadw, null, null, paramzzak.zzady);
      }
    }
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramContext.getApplicationContext());
    if (zzapl == null) {}
    for (;;)
    {
      try
      {
        if (zzapl == null) {
          zzapl = new zzbt(new zzcr(paramContext, localzzak));
        }
        return zzapl;
      }
      finally {}
      if ((localzzak != null) && (localzzak.zzady != null) && (localzzak.zzady.containsKey("dataCollectionDefaultEnabled"))) {
        zzapl.zzd(localzzak.zzady.getBoolean("dataCollectionDefaultEnabled"));
      }
    }
  }
  
  private static void zza(zzco paramzzco)
  {
    if (paramzzco == null) {
      throw new IllegalStateException("Component not created");
    }
  }
  
  private static void zza(zzcp paramzzcp)
  {
    if (paramzzcp == null) {
      throw new IllegalStateException("Component not created");
    }
    if (!paramzzcp.isInitialized())
    {
      paramzzcp = String.valueOf(paramzzcp.getClass());
      throw new IllegalStateException(String.valueOf(paramzzcp).length() + 27 + "Component not initialized: " + paramzzcp);
    }
  }
  
  @WorkerThread
  private final void zza(zzcr paramzzcr)
  {
    zzgn().zzaf();
    zzn.zzht();
    paramzzcr = new zzx(this);
    paramzzcr.zzq();
    this.zzaqb = paramzzcr;
    paramzzcr = new zzaj(this);
    paramzzcr.zzq();
    this.zzaqc = paramzzcr;
    Object localObject = new zzal(this);
    ((zzf)localObject).zzq();
    this.zzapz = ((zzal)localObject);
    localObject = new zzdr(this);
    ((zzf)localObject).zzq();
    this.zzaqa = ((zzdr)localObject);
    this.zzapu.zzgs();
    this.zzapp.zzgs();
    this.zzaqd = new zzbg(this);
    this.zzaqc.zzgs();
    zzgo().zzjj().zzg("App measurement is starting up, version", Long.valueOf(this.zzapo.zzhc()));
    localObject = this.zzaiq;
    zzgo().zzjj().zzbx("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
    localObject = this.zzaiq;
    paramzzcr = paramzzcr.zzal();
    if (TextUtils.isEmpty(this.zzadx))
    {
      if (!zzgm().zzcw(paramzzcr)) {
        break label262;
      }
      localObject = zzgo().zzjj();
      paramzzcr = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
    }
    for (;;)
    {
      ((zzar)localObject).zzbx(paramzzcr);
      zzgo().zzjk().zzbx("Debug-level message logging enabled");
      if (this.zzaqh != this.zzaqi) {
        zzgo().zzjd().zze("Not all components initialized", Integer.valueOf(this.zzaqh), Integer.valueOf(this.zzaqi));
      }
      this.zzvz = true;
      return;
      label262:
      localObject = zzgo().zzjj();
      paramzzcr = String.valueOf(paramzzcr);
      if (paramzzcr.length() != 0) {
        paramzzcr = "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(paramzzcr);
      } else {
        paramzzcr = new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
      }
    }
  }
  
  private static void zza(zzf paramzzf)
  {
    if (paramzzf == null) {
      throw new IllegalStateException("Component not created");
    }
    if (!paramzzf.isInitialized())
    {
      paramzzf = String.valueOf(paramzzf.getClass());
      throw new IllegalStateException(String.valueOf(paramzzf).length() + 27 + "Component not initialized: " + paramzzf);
    }
  }
  
  private final void zzcl()
  {
    if (!this.zzvz) {
      throw new IllegalStateException("AppMeasurement is not initialized");
    }
  }
  
  public final Context getContext()
  {
    return this.zzri;
  }
  
  @WorkerThread
  public final boolean isEnabled()
  {
    zzgn().zzaf();
    zzcl();
    if (this.zzapo.zzhu()) {
      return false;
    }
    Boolean localBoolean = this.zzapo.zzhv();
    boolean bool;
    if (localBoolean != null) {
      bool = localBoolean.booleanValue();
    }
    label99:
    for (;;)
    {
      return zzgp().zzh(bool);
      if (!GoogleServices.isMeasurementExplicitlyDisabled()) {}
      for (bool = true;; bool = false)
      {
        if ((!bool) || (this.zzaqg == null) || (!((Boolean)zzaf.zzalh.get()).booleanValue())) {
          break label99;
        }
        bool = this.zzaqg.booleanValue();
        break;
      }
    }
  }
  
  @WorkerThread
  protected final void start()
  {
    zzgn().zzaf();
    if (zzgp().zzane.get() == 0L) {
      zzgp().zzane.set(this.zzrz.currentTimeMillis());
    }
    if (Long.valueOf(zzgp().zzanj.get()).longValue() == 0L)
    {
      zzgo().zzjl().zzg("Persisting first open", Long.valueOf(this.zzagx));
      zzgp().zzanj.set(this.zzagx);
    }
    Object localObject;
    if (!zzkr()) {
      if (isEnabled())
      {
        if (!zzgm().zzx("android.permission.INTERNET")) {
          zzgo().zzjd().zzbx("App is missing INTERNET permission");
        }
        if (!zzgm().zzx("android.permission.ACCESS_NETWORK_STATE")) {
          zzgo().zzjd().zzbx("App is missing ACCESS_NETWORK_STATE permission");
        }
        localObject = this.zzaiq;
        if ((!Wrappers.packageManager(this.zzri).isCallerInstantApp()) && (!this.zzapo.zzib()))
        {
          if (!zzbj.zza(this.zzri)) {
            zzgo().zzjd().zzbx("AppMeasurementReceiver not registered/enabled");
          }
          if (!zzfk.zza(this.zzri, false)) {
            zzgo().zzjd().zzbx("AppMeasurementService not registered/enabled");
          }
        }
        zzgo().zzjd().zzbx("Uploading is not possible. App measurement disabled");
      }
    }
    do
    {
      return;
      localObject = this.zzaiq;
      if ((!TextUtils.isEmpty(zzgf().getGmpAppId())) || (!TextUtils.isEmpty(zzgf().zzgw())))
      {
        zzgm();
        if (zzfk.zza(zzgf().getGmpAppId(), zzgp().zzjs(), zzgf().zzgw(), zzgp().zzjt()))
        {
          zzgo().zzjj().zzbx("Rechecking which service to use due to a GMP App Id change");
          zzgp().zzjv();
          if (this.zzapo.zza(zzaf.zzalc)) {
            zzgi().resetAnalyticsData();
          }
          this.zzaqa.disconnect();
          this.zzaqa.zzdj();
          zzgp().zzanj.set(this.zzagx);
          zzgp().zzanl.zzcc(null);
        }
        zzgp().zzca(zzgf().getGmpAppId());
        zzgp().zzcb(zzgf().zzgw());
        if (this.zzapo.zzbj(zzgf().zzal())) {
          this.zzaps.zzam(this.zzagx);
        }
      }
      zzge().zzcm(zzgp().zzanl.zzjz());
      localObject = this.zzaiq;
    } while ((TextUtils.isEmpty(zzgf().getGmpAppId())) && (TextUtils.isEmpty(zzgf().zzgw())));
    boolean bool2 = isEnabled();
    if ((!zzgp().zzjy()) && (!this.zzapo.zzhu()))
    {
      localObject = zzgp();
      if (bool2) {
        break label613;
      }
    }
    label613:
    for (boolean bool1 = true;; bool1 = false)
    {
      ((zzba)localObject).zzi(bool1);
      if (this.zzapo.zze(zzgf().zzal(), zzaf.zzalj)) {
        zzj(false);
      }
      if ((!this.zzapo.zzbd(zzgf().zzal())) || (bool2)) {
        zzge().zzkz();
      }
      zzgg().zza(new AtomicReference());
      return;
    }
  }
  
  final void zzb(zzcp paramzzcp)
  {
    this.zzaqh += 1;
  }
  
  final void zzb(zzf paramzzf)
  {
    this.zzaqh += 1;
  }
  
  public final Clock zzbx()
  {
    return this.zzrz;
  }
  
  @WorkerThread
  final void zzd(boolean paramBoolean)
  {
    this.zzaqg = Boolean.valueOf(paramBoolean);
  }
  
  final void zzga()
  {
    zzk localzzk = this.zzaiq;
    throw new IllegalStateException("Unexpected call on client side");
  }
  
  final void zzgb()
  {
    zzk localzzk = this.zzaiq;
  }
  
  public final zza zzgd()
  {
    if (this.zzapy == null) {
      throw new IllegalStateException("Component not created");
    }
    return this.zzapy;
  }
  
  public final zzcs zzge()
  {
    zza(this.zzapx);
    return this.zzapx;
  }
  
  public final zzaj zzgf()
  {
    zza(this.zzaqc);
    return this.zzaqc;
  }
  
  public final zzdr zzgg()
  {
    zza(this.zzaqa);
    return this.zzaqa;
  }
  
  public final zzdo zzgh()
  {
    zza(this.zzapw);
    return this.zzapw;
  }
  
  public final zzal zzgi()
  {
    zza(this.zzapz);
    return this.zzapz;
  }
  
  public final zzeq zzgj()
  {
    zza(this.zzaps);
    return this.zzaps;
  }
  
  public final zzx zzgk()
  {
    zza(this.zzaqb);
    return this.zzaqb;
  }
  
  public final zzan zzgl()
  {
    zza(this.zzapv);
    return this.zzapv;
  }
  
  public final zzfk zzgm()
  {
    zza(this.zzapu);
    return this.zzapu;
  }
  
  public final zzbo zzgn()
  {
    zza(this.zzapr);
    return this.zzapr;
  }
  
  public final zzap zzgo()
  {
    zza(this.zzapq);
    return this.zzapq;
  }
  
  public final zzba zzgp()
  {
    zza(this.zzapp);
    return this.zzapp;
  }
  
  public final zzn zzgq()
  {
    return this.zzapo;
  }
  
  public final zzk zzgr()
  {
    return this.zzaiq;
  }
  
  final void zzj(boolean paramBoolean)
  {
    zzgn().zzaf();
    Object localObject = zzgp().zzans.zzjz();
    int i;
    if ((!paramBoolean) && (localObject != null)) {
      if ("unset".equals(localObject))
      {
        zzge().zza("app", "_ap", null, this.zzrz.currentTimeMillis());
        i = 1;
      }
    }
    for (;;)
    {
      zzcs localzzcs;
      if (i != 0)
      {
        localObject = this.zzapo.zzau("google_analytics_default_allow_ad_personalization_signals");
        if (localObject == null) {
          break label160;
        }
        localzzcs = zzge();
        if (!((Boolean)localObject).booleanValue()) {
          break label155;
        }
      }
      label155:
      for (long l = 1L;; l = 0L)
      {
        localzzcs.zza("auto", "_ap", Long.valueOf(l), this.zzrz.currentTimeMillis());
        return;
        zzge().zza("app", "_ap", localObject, this.zzrz.currentTimeMillis());
        i = 0;
        break;
      }
      label160:
      zzge().zza("auto", "_ap", null, this.zzrz.currentTimeMillis());
      return;
      i = 1;
    }
  }
  
  public final zzap zzkf()
  {
    if ((this.zzapq != null) && (this.zzapq.isInitialized())) {
      return this.zzapq;
    }
    return null;
  }
  
  public final zzbg zzkg()
  {
    return this.zzaqd;
  }
  
  final zzbo zzkh()
  {
    return this.zzapr;
  }
  
  public final AppMeasurement zzki()
  {
    return this.zzapt;
  }
  
  public final boolean zzkj()
  {
    return TextUtils.isEmpty(this.zzadx);
  }
  
  public final String zzkk()
  {
    return this.zzadx;
  }
  
  public final String zzkl()
  {
    return this.zzapm;
  }
  
  public final String zzkm()
  {
    return this.zzapn;
  }
  
  public final boolean zzkn()
  {
    return this.zzadv;
  }
  
  @WorkerThread
  public final boolean zzko()
  {
    return (this.zzaqg != null) && (this.zzaqg.booleanValue());
  }
  
  final long zzkp()
  {
    Long localLong = Long.valueOf(zzgp().zzanj.get());
    if (localLong.longValue() == 0L) {
      return this.zzagx;
    }
    return Math.min(this.zzagx, localLong.longValue());
  }
  
  final void zzkq()
  {
    this.zzaqi += 1;
  }
  
  @WorkerThread
  protected final boolean zzkr()
  {
    boolean bool2 = false;
    zzcl();
    zzgn().zzaf();
    if ((this.zzaqe == null) || (this.zzaqf == 0L) || ((this.zzaqe != null) && (!this.zzaqe.booleanValue()) && (Math.abs(this.zzrz.elapsedRealtime() - this.zzaqf) > 1000L)))
    {
      this.zzaqf = this.zzrz.elapsedRealtime();
      zzk localzzk = this.zzaiq;
      if ((!zzgm().zzx("android.permission.INTERNET")) || (!zzgm().zzx("android.permission.ACCESS_NETWORK_STATE")) || ((!Wrappers.packageManager(this.zzri).isCallerInstantApp()) && (!this.zzapo.zzib()) && ((!zzbj.zza(this.zzri)) || (!zzfk.zza(this.zzri, false))))) {
        break label235;
      }
    }
    label235:
    for (boolean bool1 = true;; bool1 = false)
    {
      this.zzaqe = Boolean.valueOf(bool1);
      if (this.zzaqe.booleanValue())
      {
        if (!zzgm().zzt(zzgf().getGmpAppId(), zzgf().zzgw()))
        {
          bool1 = bool2;
          if (TextUtils.isEmpty(zzgf().zzgw())) {}
        }
        else
        {
          bool1 = true;
        }
        this.zzaqe = Boolean.valueOf(bool1);
      }
      return this.zzaqe.booleanValue();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */