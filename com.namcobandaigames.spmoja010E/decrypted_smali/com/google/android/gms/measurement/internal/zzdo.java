package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Map;

public final class zzdo
  extends zzf
{
  @VisibleForTesting
  protected zzdn zzaro;
  private volatile zzdn zzarp;
  private zzdn zzarq;
  private final Map<Activity, zzdn> zzarr = new ArrayMap();
  private zzdn zzars;
  private String zzart;
  
  public zzdo(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  @MainThread
  private final void zza(Activity paramActivity, zzdn paramzzdn, boolean paramBoolean)
  {
    if (this.zzarp == null) {}
    for (zzdn localzzdn1 = this.zzarq;; localzzdn1 = this.zzarp)
    {
      zzdn localzzdn2 = paramzzdn;
      if (paramzzdn.zzarl == null) {
        localzzdn2 = new zzdn(paramzzdn.zzuw, zzcn(paramActivity.getClass().getCanonicalName()), paramzzdn.zzarm);
      }
      this.zzarq = this.zzarp;
      this.zzarp = localzzdn2;
      zzgn().zzc(new zzdp(this, paramBoolean, localzzdn1, localzzdn2));
      return;
    }
  }
  
  @WorkerThread
  private final void zza(@NonNull zzdn paramzzdn)
  {
    zzgd().zzq(zzbx().elapsedRealtime());
    if (zzgj().zzn(paramzzdn.zzarn)) {
      paramzzdn.zzarn = false;
    }
  }
  
  public static void zza(zzdn paramzzdn, Bundle paramBundle, boolean paramBoolean)
  {
    if ((paramBundle != null) && (paramzzdn != null) && ((!paramBundle.containsKey("_sc")) || (paramBoolean))) {
      if (paramzzdn.zzuw != null)
      {
        paramBundle.putString("_sn", paramzzdn.zzuw);
        paramBundle.putString("_sc", paramzzdn.zzarl);
        paramBundle.putLong("_si", paramzzdn.zzarm);
      }
    }
    while ((paramBundle == null) || (paramzzdn != null) || (!paramBoolean)) {
      for (;;)
      {
        return;
        paramBundle.remove("_sn");
      }
    }
    paramBundle.remove("_sn");
    paramBundle.remove("_sc");
    paramBundle.remove("_si");
  }
  
  @VisibleForTesting
  private static String zzcn(String paramString)
  {
    paramString = paramString.split("\\.");
    if (paramString.length > 0) {}
    for (paramString = paramString[(paramString.length - 1)];; paramString = "")
    {
      String str = paramString;
      if (paramString.length() > 100) {
        str = paramString.substring(0, 100);
      }
      return str;
    }
  }
  
  @MainThread
  private final zzdn zze(@NonNull Activity paramActivity)
  {
    Preconditions.checkNotNull(paramActivity);
    zzdn localzzdn2 = (zzdn)this.zzarr.get(paramActivity);
    zzdn localzzdn1 = localzzdn2;
    if (localzzdn2 == null)
    {
      localzzdn1 = new zzdn(null, zzcn(paramActivity.getClass().getCanonicalName()), zzgm().zzmc());
      this.zzarr.put(paramActivity, localzzdn1);
    }
    return localzzdn1;
  }
  
  @MainThread
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle)
  {
    if (paramBundle == null) {}
    do
    {
      return;
      paramBundle = paramBundle.getBundle("com.google.app_measurement.screen_service");
    } while (paramBundle == null);
    paramBundle = new zzdn(paramBundle.getString("name"), paramBundle.getString("referrer_name"), paramBundle.getLong("id"));
    this.zzarr.put(paramActivity, paramBundle);
  }
  
  @MainThread
  public final void onActivityDestroyed(Activity paramActivity)
  {
    this.zzarr.remove(paramActivity);
  }
  
  @MainThread
  public final void onActivityPaused(Activity paramActivity)
  {
    paramActivity = zze(paramActivity);
    this.zzarq = this.zzarp;
    this.zzarp = null;
    zzgn().zzc(new zzdq(this, paramActivity));
  }
  
  @MainThread
  public final void onActivityResumed(Activity paramActivity)
  {
    zza(paramActivity, zze(paramActivity), false);
    paramActivity = zzgd();
    long l = paramActivity.zzbx().elapsedRealtime();
    paramActivity.zzgn().zzc(new zzd(paramActivity, l));
  }
  
  @MainThread
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle)
  {
    if (paramBundle == null) {}
    do
    {
      return;
      paramActivity = (zzdn)this.zzarr.get(paramActivity);
    } while (paramActivity == null);
    Bundle localBundle = new Bundle();
    localBundle.putLong("id", paramActivity.zzarm);
    localBundle.putString("name", paramActivity.zzuw);
    localBundle.putString("referrer_name", paramActivity.zzarl);
    paramBundle.putBundle("com.google.app_measurement.screen_service", localBundle);
  }
  
  public final void setCurrentScreen(@NonNull Activity paramActivity, @Nullable @Size(max=36L, min=1L) String paramString1, @Nullable @Size(max=36L, min=1L) String paramString2)
  {
    if (this.zzarp == null)
    {
      zzgo().zzjg().zzbx("setCurrentScreen cannot be called while no activity active");
      return;
    }
    if (this.zzarr.get(paramActivity) == null)
    {
      zzgo().zzjg().zzbx("setCurrentScreen must be called with an activity in the activity lifecycle");
      return;
    }
    String str = paramString2;
    if (paramString2 == null) {
      str = zzcn(paramActivity.getClass().getCanonicalName());
    }
    boolean bool1 = this.zzarp.zzarl.equals(str);
    boolean bool2 = zzfk.zzu(this.zzarp.zzuw, paramString1);
    if ((bool1) && (bool2))
    {
      zzgo().zzji().zzbx("setCurrentScreen cannot be called with the same class and name");
      return;
    }
    if ((paramString1 != null) && ((paramString1.length() <= 0) || (paramString1.length() > 100)))
    {
      zzgo().zzjg().zzg("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(paramString1.length()));
      return;
    }
    if ((str != null) && ((str.length() <= 0) || (str.length() > 100)))
    {
      zzgo().zzjg().zzg("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
      return;
    }
    zzar localzzar = zzgo().zzjl();
    if (paramString1 == null) {}
    for (paramString2 = "null";; paramString2 = paramString1)
    {
      localzzar.zze("Setting current screen to name, class", paramString2, str);
      paramString1 = new zzdn(paramString1, str, zzgm().zzmc());
      this.zzarr.put(paramActivity, paramString1);
      zza(paramActivity, paramString1, true);
      return;
    }
  }
  
  @WorkerThread
  public final void zza(String paramString, zzdn paramzzdn)
  {
    zzaf();
    try
    {
      if ((this.zzart == null) || (this.zzart.equals(paramString)) || (paramzzdn != null))
      {
        this.zzart = paramString;
        this.zzars = paramzzdn;
      }
      return;
    }
    finally {}
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  @WorkerThread
  public final zzdn zzla()
  {
    zzcl();
    zzaf();
    return this.zzaro;
  }
  
  public final zzdn zzlb()
  {
    zzgb();
    return this.zzarp;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */