package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;

@SafeParcelable.Class(creator="UserAttributeParcelCreator")
public final class zzfh
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzfh> CREATOR = new zzfi();
  @SafeParcelable.Field(id=2)
  public final String name;
  @SafeParcelable.Field(id=7)
  public final String origin;
  @SafeParcelable.Field(id=1)
  private final int versionCode;
  @SafeParcelable.Field(id=6)
  private final String zzamp;
  @SafeParcelable.Field(id=3)
  public final long zzaue;
  @SafeParcelable.Field(id=4)
  private final Long zzauf;
  @SafeParcelable.Field(id=5)
  private final Float zzaug;
  @SafeParcelable.Field(id=8)
  private final Double zzauh;
  
  @SafeParcelable.Constructor
  zzfh(@SafeParcelable.Param(id=1) int paramInt, @SafeParcelable.Param(id=2) String paramString1, @SafeParcelable.Param(id=3) long paramLong, @SafeParcelable.Param(id=4) Long paramLong1, @SafeParcelable.Param(id=5) Float paramFloat, @SafeParcelable.Param(id=6) String paramString2, @SafeParcelable.Param(id=7) String paramString3, @SafeParcelable.Param(id=8) Double paramDouble)
  {
    this.versionCode = paramInt;
    this.name = paramString1;
    this.zzaue = paramLong;
    this.zzauf = paramLong1;
    this.zzaug = null;
    if (paramInt == 1)
    {
      paramString1 = (String)localObject;
      if (paramFloat != null) {
        paramString1 = Double.valueOf(paramFloat.doubleValue());
      }
    }
    for (this.zzauh = paramString1;; this.zzauh = paramDouble)
    {
      this.zzamp = paramString2;
      this.origin = paramString3;
      return;
    }
  }
  
  zzfh(zzfj paramzzfj)
  {
    this(paramzzfj.name, paramzzfj.zzaue, paramzzfj.value, paramzzfj.origin);
  }
  
  zzfh(String paramString1, long paramLong, Object paramObject, String paramString2)
  {
    Preconditions.checkNotEmpty(paramString1);
    this.versionCode = 2;
    this.name = paramString1;
    this.zzaue = paramLong;
    this.origin = paramString2;
    if (paramObject == null)
    {
      this.zzauf = null;
      this.zzaug = null;
      this.zzauh = null;
      this.zzamp = null;
      return;
    }
    if ((paramObject instanceof Long))
    {
      this.zzauf = ((Long)paramObject);
      this.zzaug = null;
      this.zzauh = null;
      this.zzamp = null;
      return;
    }
    if ((paramObject instanceof String))
    {
      this.zzauf = null;
      this.zzaug = null;
      this.zzauh = null;
      this.zzamp = ((String)paramObject);
      return;
    }
    if ((paramObject instanceof Double))
    {
      this.zzauf = null;
      this.zzaug = null;
      this.zzauh = ((Double)paramObject);
      this.zzamp = null;
      return;
    }
    throw new IllegalArgumentException("User attribute given of un-supported type");
  }
  
  public final Object getValue()
  {
    if (this.zzauf != null) {
      return this.zzauf;
    }
    if (this.zzauh != null) {
      return this.zzauh;
    }
    if (this.zzamp != null) {
      return this.zzamp;
    }
    return null;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 1, this.versionCode);
    SafeParcelWriter.writeString(paramParcel, 2, this.name, false);
    SafeParcelWriter.writeLong(paramParcel, 3, this.zzaue);
    SafeParcelWriter.writeLongObject(paramParcel, 4, this.zzauf, false);
    SafeParcelWriter.writeFloatObject(paramParcel, 5, null, false);
    SafeParcelWriter.writeString(paramParcel, 6, this.zzamp, false);
    SafeParcelWriter.writeString(paramParcel, 7, this.origin, false);
    SafeParcelWriter.writeDoubleObject(paramParcel, 8, this.zzauh, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzfh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */