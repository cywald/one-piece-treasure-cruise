package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;

final class zzfj
{
  final String name;
  final String origin;
  final Object value;
  final long zzaue;
  final String zztt;
  
  zzfj(String paramString1, String paramString2, String paramString3, long paramLong, Object paramObject)
  {
    Preconditions.checkNotEmpty(paramString1);
    Preconditions.checkNotEmpty(paramString3);
    Preconditions.checkNotNull(paramObject);
    this.zztt = paramString1;
    this.origin = paramString2;
    this.name = paramString3;
    this.zzaue = paramLong;
    this.value = paramObject;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzfj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */