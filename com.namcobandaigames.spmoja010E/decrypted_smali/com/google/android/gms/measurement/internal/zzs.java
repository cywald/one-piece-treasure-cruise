package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzgf;
import com.google.android.gms.internal.measurement.zzgi;

abstract interface zzs
{
  public abstract boolean zza(long paramLong, zzgf paramzzgf);
  
  public abstract void zzb(zzgi paramzzgi);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */