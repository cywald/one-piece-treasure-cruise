package com.google.android.gms.measurement.internal;

import android.os.Bundle;

public final class zzak
{
  public final String origin;
  public final long zzadt;
  public final long zzadu;
  public final boolean zzadv;
  public final String zzadw;
  public final String zzadx;
  public final Bundle zzady;
  
  zzak(long paramLong1, long paramLong2, boolean paramBoolean, String paramString1, String paramString2, String paramString3, Bundle paramBundle)
  {
    this.zzadt = paramLong1;
    this.zzadu = paramLong2;
    this.zzadv = paramBoolean;
    this.zzadw = paramString1;
    this.origin = paramString2;
    this.zzadx = paramString3;
    this.zzady = paramBundle;
  }
  
  public static final zzak zzc(Bundle paramBundle)
  {
    return new zzak(0L, 0L, true, null, null, null, paramBundle);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */