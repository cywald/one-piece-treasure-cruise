package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class zza
  extends zze
{
  private final Map<String, Long> zzafq = new ArrayMap();
  private final Map<String, Integer> zzafr = new ArrayMap();
  private long zzafs;
  
  public zza(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  @WorkerThread
  private final void zza(long paramLong, zzdn paramzzdn)
  {
    if (paramzzdn == null)
    {
      zzgo().zzjl().zzbx("Not logging ad exposure. No active activity");
      return;
    }
    if (paramLong < 1000L)
    {
      zzgo().zzjl().zzg("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(paramLong));
      return;
    }
    Bundle localBundle = new Bundle();
    localBundle.putLong("_xt", paramLong);
    zzdo.zza(paramzzdn, localBundle, true);
    zzge().logEvent("am", "_xa", localBundle);
  }
  
  @WorkerThread
  private final void zza(String paramString, long paramLong)
  {
    zzgb();
    zzaf();
    Preconditions.checkNotEmpty(paramString);
    if (this.zzafr.isEmpty()) {
      this.zzafs = paramLong;
    }
    Integer localInteger = (Integer)this.zzafr.get(paramString);
    if (localInteger != null)
    {
      this.zzafr.put(paramString, Integer.valueOf(localInteger.intValue() + 1));
      return;
    }
    if (this.zzafr.size() >= 100)
    {
      zzgo().zzjg().zzbx("Too many ads visible");
      return;
    }
    this.zzafr.put(paramString, Integer.valueOf(1));
    this.zzafq.put(paramString, Long.valueOf(paramLong));
  }
  
  @WorkerThread
  private final void zza(String paramString, long paramLong, zzdn paramzzdn)
  {
    if (paramzzdn == null)
    {
      zzgo().zzjl().zzbx("Not logging ad unit exposure. No active activity");
      return;
    }
    if (paramLong < 1000L)
    {
      zzgo().zzjl().zzg("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(paramLong));
      return;
    }
    Bundle localBundle = new Bundle();
    localBundle.putString("_ai", paramString);
    localBundle.putLong("_xt", paramLong);
    zzdo.zza(paramzzdn, localBundle, true);
    zzge().logEvent("am", "_xu", localBundle);
  }
  
  @WorkerThread
  private final void zzb(String paramString, long paramLong)
  {
    zzgb();
    zzaf();
    Preconditions.checkNotEmpty(paramString);
    Object localObject = (Integer)this.zzafr.get(paramString);
    if (localObject != null)
    {
      zzdn localzzdn = zzgh().zzla();
      int i = ((Integer)localObject).intValue() - 1;
      if (i == 0)
      {
        this.zzafr.remove(paramString);
        localObject = (Long)this.zzafq.get(paramString);
        if (localObject == null) {
          zzgo().zzjd().zzbx("First ad unit exposure time was never set");
        }
        for (;;)
        {
          if (this.zzafr.isEmpty())
          {
            if (this.zzafs != 0L) {
              break;
            }
            zzgo().zzjd().zzbx("First ad exposure time was never set");
          }
          return;
          long l = ((Long)localObject).longValue();
          this.zzafq.remove(paramString);
          zza(paramString, paramLong - l, localzzdn);
        }
        zza(paramLong - this.zzafs, localzzdn);
        this.zzafs = 0L;
        return;
      }
      this.zzafr.put(paramString, Integer.valueOf(i));
      return;
    }
    zzgo().zzjd().zzg("Call to endAdUnitExposure for unknown ad unit id", paramString);
  }
  
  @WorkerThread
  private final void zzr(long paramLong)
  {
    Iterator localIterator = this.zzafq.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      this.zzafq.put(str, Long.valueOf(paramLong));
    }
    if (!this.zzafq.isEmpty()) {
      this.zzafs = paramLong;
    }
  }
  
  public final void beginAdUnitExposure(String paramString, long paramLong)
  {
    if ((paramString == null) || (paramString.length() == 0))
    {
      zzgo().zzjd().zzbx("Ad unit id must be a non-empty string");
      return;
    }
    zzgn().zzc(new zzb(this, paramString, paramLong));
  }
  
  public final void endAdUnitExposure(String paramString, long paramLong)
  {
    if ((paramString == null) || (paramString.length() == 0))
    {
      zzgo().zzjd().zzbx("Ad unit id must be a non-empty string");
      return;
    }
    zzgn().zzc(new zzc(this, paramString, paramLong));
  }
  
  @WorkerThread
  public final void zzq(long paramLong)
  {
    zzdn localzzdn = zzgh().zzla();
    Iterator localIterator = this.zzafq.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      zza(str, paramLong - ((Long)this.zzafq.get(str)).longValue(), localzzdn);
    }
    if (!this.zzafq.isEmpty()) {
      zza(paramLong - this.zzafs, localzzdn);
    }
    zzr(paramLong);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */