package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PersistableBundle;
import android.support.annotation.MainThread;
import com.google.android.gms.common.internal.Preconditions;

public final class zzel<T extends Context,  extends zzep>
{
  private final T zzaby;
  
  public zzel(T paramT)
  {
    Preconditions.checkNotNull(paramT);
    this.zzaby = paramT;
  }
  
  private final void zzb(Runnable paramRunnable)
  {
    zzfa localzzfa = zzfa.zzm(this.zzaby);
    localzzfa.zzgn().zzc(new zzeo(this, localzzfa, paramRunnable));
  }
  
  private final zzap zzgo()
  {
    return zzbt.zza(this.zzaby, null).zzgo();
  }
  
  @MainThread
  public final IBinder onBind(Intent paramIntent)
  {
    if (paramIntent == null)
    {
      zzgo().zzjd().zzbx("onBind called with null intent");
      return null;
    }
    paramIntent = paramIntent.getAction();
    if ("com.google.android.gms.measurement.START".equals(paramIntent)) {
      return new zzbv(zzfa.zzm(this.zzaby));
    }
    zzgo().zzjg().zzg("onBind received unknown action", paramIntent);
    return null;
  }
  
  @MainThread
  public final void onCreate()
  {
    zzbt localzzbt = zzbt.zza(this.zzaby, null);
    zzap localzzap = localzzbt.zzgo();
    localzzbt.zzgr();
    localzzap.zzjl().zzbx("Local AppMeasurementService is starting up");
  }
  
  @MainThread
  public final void onDestroy()
  {
    zzbt localzzbt = zzbt.zza(this.zzaby, null);
    zzap localzzap = localzzbt.zzgo();
    localzzbt.zzgr();
    localzzap.zzjl().zzbx("Local AppMeasurementService is shutting down");
  }
  
  @MainThread
  public final void onRebind(Intent paramIntent)
  {
    if (paramIntent == null)
    {
      zzgo().zzjd().zzbx("onRebind called with null intent");
      return;
    }
    paramIntent = paramIntent.getAction();
    zzgo().zzjl().zzg("onRebind called. action", paramIntent);
  }
  
  @MainThread
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    zzbt localzzbt = zzbt.zza(this.zzaby, null);
    zzap localzzap = localzzbt.zzgo();
    if (paramIntent == null) {
      localzzap.zzjg().zzbx("AppMeasurementService started with null intent");
    }
    String str;
    do
    {
      return 2;
      str = paramIntent.getAction();
      localzzbt.zzgr();
      localzzap.zzjl().zze("Local AppMeasurementService called. startId, action", Integer.valueOf(paramInt2), str);
    } while (!"com.google.android.gms.measurement.UPLOAD".equals(str));
    zzb(new zzem(this, paramInt2, localzzap, paramIntent));
    return 2;
  }
  
  @TargetApi(24)
  @MainThread
  public final boolean onStartJob(JobParameters paramJobParameters)
  {
    zzbt localzzbt = zzbt.zza(this.zzaby, null);
    zzap localzzap = localzzbt.zzgo();
    String str = paramJobParameters.getExtras().getString("action");
    localzzbt.zzgr();
    localzzap.zzjl().zzg("Local AppMeasurementJobService called. action", str);
    if ("com.google.android.gms.measurement.UPLOAD".equals(str)) {
      zzb(new zzen(this, localzzap, paramJobParameters));
    }
    return true;
  }
  
  @MainThread
  public final boolean onUnbind(Intent paramIntent)
  {
    if (paramIntent == null)
    {
      zzgo().zzjd().zzbx("onUnbind called with null intent");
      return true;
    }
    paramIntent = paramIntent.getAction();
    zzgo().zzjl().zzg("onUnbind called for intent. action", paramIntent);
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */