package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

final class zzec
  implements Runnable
{
  zzec(zzdr paramzzdr, AtomicReference paramAtomicReference, String paramString1, String paramString2, String paramString3, boolean paramBoolean, zzh paramzzh) {}
  
  public final void run()
  {
    AtomicReference localAtomicReference = this.zzash;
    for (;;)
    {
      try
      {
        localzzag = zzdr.zzd(this.zzasg);
        if (localzzag == null)
        {
          this.zzasg.zzgo().zzjd().zzd("Failed to get user properties", zzap.zzbv(this.zzaqq), this.zzaeh, this.zzaeo);
          this.zzash.set(Collections.emptyList());
        }
      }
      catch (RemoteException localRemoteException)
      {
        zzag localzzag;
        this.zzasg.zzgo().zzjd().zzd("Failed to get user properties", zzap.zzbv(this.zzaqq), this.zzaeh, localRemoteException);
        this.zzash.set(Collections.emptyList());
        this.zzash.notify();
        continue;
      }
      finally
      {
        this.zzash.notify();
      }
      try
      {
        this.zzash.notify();
        return;
      }
      finally {}
    }
    if (TextUtils.isEmpty(this.zzaqq)) {
      this.zzash.set(localzzag.zza(this.zzaeh, this.zzaeo, this.zzaev, this.zzaqn));
    }
    for (;;)
    {
      zzdr.zze(this.zzasg);
      this.zzash.notify();
      return;
      this.zzash.set(((zzag)localObject1).zza(this.zzaqq, this.zzaeh, this.zzaeo, this.zzaev));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */