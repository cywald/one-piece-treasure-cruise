package com.google.android.gms.measurement.internal;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class zzu
{
  static void zza(zzap paramzzap, SQLiteDatabase paramSQLiteDatabase)
  {
    if (paramzzap == null) {
      throw new IllegalArgumentException("Monitor must not be null");
    }
    paramSQLiteDatabase = new File(paramSQLiteDatabase.getPath());
    if (!paramSQLiteDatabase.setReadable(false, false)) {
      paramzzap.zzjg().zzbx("Failed to turn off database read permission");
    }
    if (!paramSQLiteDatabase.setWritable(false, false)) {
      paramzzap.zzjg().zzbx("Failed to turn off database write permission");
    }
    if (!paramSQLiteDatabase.setReadable(true, true)) {
      paramzzap.zzjg().zzbx("Failed to turn on database read permission for owner");
    }
    if (!paramSQLiteDatabase.setWritable(true, true)) {
      paramzzap.zzjg().zzbx("Failed to turn on database write permission for owner");
    }
  }
  
  @WorkerThread
  static void zza(zzap paramzzap, SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2, String paramString3, String[] paramArrayOfString)
    throws SQLiteException
  {
    int j = 0;
    if (paramzzap == null) {
      throw new IllegalArgumentException("Monitor must not be null");
    }
    if (!zza(paramzzap, paramSQLiteDatabase, paramString1)) {
      paramSQLiteDatabase.execSQL(paramString2);
    }
    if (paramzzap == null) {
      try
      {
        throw new IllegalArgumentException("Monitor must not be null");
      }
      catch (SQLiteException paramSQLiteDatabase)
      {
        paramzzap.zzjd().zzg("Failed to verify columns on table that was just created", paramString1);
        throw paramSQLiteDatabase;
      }
    }
    paramString2 = zzb(paramSQLiteDatabase, paramString1);
    paramString3 = paramString3.split(",");
    int k = paramString3.length;
    int i = 0;
    if (i < k)
    {
      Object localObject = paramString3[i];
      if (!paramString2.remove(localObject)) {
        throw new SQLiteException(String.valueOf(paramString1).length() + 35 + String.valueOf(localObject).length() + "Table " + paramString1 + " is missing required column: " + (String)localObject);
      }
    }
    for (;;)
    {
      if (i < paramArrayOfString.length)
      {
        if (!paramString2.remove(paramArrayOfString[i])) {
          paramSQLiteDatabase.execSQL(paramArrayOfString[(i + 1)]);
        }
      }
      else
      {
        do
        {
          if (!paramString2.isEmpty()) {
            paramzzap.zzjg().zze("Table has extra columns. table, columns", paramString1, TextUtils.join(", ", paramString2));
          }
          return;
          i += 1;
          break;
        } while (paramArrayOfString == null);
        i = j;
        continue;
      }
      i += 2;
    }
  }
  
  /* Error */
  @WorkerThread
  private static boolean zza(zzap paramzzap, SQLiteDatabase paramSQLiteDatabase, String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: ifnonnull +13 -> 17
    //   7: new 8	java/lang/IllegalArgumentException
    //   10: dup
    //   11: ldc 10
    //   13: invokespecial 14	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   16: athrow
    //   17: aload_1
    //   18: ldc -122
    //   20: iconst_1
    //   21: anewarray 77	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: ldc -120
    //   28: aastore
    //   29: ldc -118
    //   31: iconst_1
    //   32: anewarray 77	java/lang/String
    //   35: dup
    //   36: iconst_0
    //   37: aload_2
    //   38: aastore
    //   39: aconst_null
    //   40: aconst_null
    //   41: aconst_null
    //   42: invokevirtual 142	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   45: astore_1
    //   46: aload_1
    //   47: astore 4
    //   49: aload 4
    //   51: astore_1
    //   52: aload 4
    //   54: invokeinterface 147 1 0
    //   59: istore_3
    //   60: aload 4
    //   62: ifnull +10 -> 72
    //   65: aload 4
    //   67: invokeinterface 151 1 0
    //   72: iload_3
    //   73: ireturn
    //   74: astore 5
    //   76: aconst_null
    //   77: astore 4
    //   79: aload 4
    //   81: astore_1
    //   82: aload_0
    //   83: invokevirtual 33	com/google/android/gms/measurement/internal/zzap:zzjg	()Lcom/google/android/gms/measurement/internal/zzar;
    //   86: ldc -103
    //   88: aload_2
    //   89: aload 5
    //   91: invokevirtual 130	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   94: aload 4
    //   96: ifnull +10 -> 106
    //   99: aload 4
    //   101: invokeinterface 151 1 0
    //   106: iconst_0
    //   107: ireturn
    //   108: astore_0
    //   109: aload 4
    //   111: astore_1
    //   112: aload_1
    //   113: ifnull +9 -> 122
    //   116: aload_1
    //   117: invokeinterface 151 1 0
    //   122: aload_0
    //   123: athrow
    //   124: astore_0
    //   125: goto -13 -> 112
    //   128: astore 5
    //   130: goto -51 -> 79
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	133	0	paramzzap	zzap
    //   0	133	1	paramSQLiteDatabase	SQLiteDatabase
    //   0	133	2	paramString	String
    //   59	14	3	bool	boolean
    //   1	109	4	localSQLiteDatabase	SQLiteDatabase
    //   74	16	5	localSQLiteException1	SQLiteException
    //   128	1	5	localSQLiteException2	SQLiteException
    // Exception table:
    //   from	to	target	type
    //   17	46	74	android/database/sqlite/SQLiteException
    //   17	46	108	finally
    //   52	60	124	finally
    //   82	94	124	finally
    //   52	60	128	android/database/sqlite/SQLiteException
  }
  
  @WorkerThread
  private static Set<String> zzb(SQLiteDatabase paramSQLiteDatabase, String paramString)
  {
    HashSet localHashSet = new HashSet();
    paramSQLiteDatabase = paramSQLiteDatabase.rawQuery(String.valueOf(paramString).length() + 22 + "SELECT * FROM " + paramString + " LIMIT 0", null);
    try
    {
      Collections.addAll(localHashSet, paramSQLiteDatabase.getColumnNames());
      return localHashSet;
    }
    finally
    {
      paramSQLiteDatabase.close();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */