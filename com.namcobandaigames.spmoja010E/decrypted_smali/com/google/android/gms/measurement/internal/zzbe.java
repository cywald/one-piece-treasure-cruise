package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.WorkerThread;
import android.util.Pair;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.security.SecureRandom;

public final class zzbe
{
  private final long zzabv;
  @VisibleForTesting
  private final String zzaoa;
  private final String zzaob;
  private final String zzaoc;
  
  private zzbe(zzba paramzzba, String paramString, long paramLong)
  {
    Preconditions.checkNotEmpty(paramString);
    if (paramLong > 0L) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool);
      this.zzaoa = String.valueOf(paramString).concat(":start");
      this.zzaob = String.valueOf(paramString).concat(":count");
      this.zzaoc = String.valueOf(paramString).concat(":value");
      this.zzabv = paramLong;
      return;
    }
  }
  
  @WorkerThread
  private final void zzfl()
  {
    this.zzany.zzaf();
    long l = this.zzany.zzbx().currentTimeMillis();
    SharedPreferences.Editor localEditor = zzba.zza(this.zzany).edit();
    localEditor.remove(this.zzaob);
    localEditor.remove(this.zzaoc);
    localEditor.putLong(this.zzaoa, l);
    localEditor.apply();
  }
  
  @WorkerThread
  private final long zzfn()
  {
    return zzba.zza(this.zzany).getLong(this.zzaoa, 0L);
  }
  
  @WorkerThread
  public final void zzc(String paramString, long paramLong)
  {
    this.zzany.zzaf();
    if (zzfn() == 0L) {
      zzfl();
    }
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    paramLong = zzba.zza(this.zzany).getLong(this.zzaob, 0L);
    if (paramLong <= 0L)
    {
      paramString = zzba.zza(this.zzany).edit();
      paramString.putString(this.zzaoc, str);
      paramString.putLong(this.zzaob, 1L);
      paramString.apply();
      return;
    }
    if ((this.zzany.zzgm().zzmd().nextLong() & 0x7FFFFFFFFFFFFFFF) < Long.MAX_VALUE / (paramLong + 1L)) {}
    for (int i = 1;; i = 0)
    {
      paramString = zzba.zza(this.zzany).edit();
      if (i != 0) {
        paramString.putString(this.zzaoc, str);
      }
      paramString.putLong(this.zzaob, paramLong + 1L);
      paramString.apply();
      return;
    }
  }
  
  @WorkerThread
  public final Pair<String, Long> zzfm()
  {
    this.zzany.zzaf();
    this.zzany.zzaf();
    long l = zzfn();
    if (l == 0L) {
      zzfl();
    }
    for (l = 0L; l < this.zzabv; l = Math.abs(l - this.zzany.zzbx().currentTimeMillis())) {
      return null;
    }
    if (l > this.zzabv << 1)
    {
      zzfl();
      return null;
    }
    String str = zzba.zza(this.zzany).getString(this.zzaoc, null);
    l = zzba.zza(this.zzany).getLong(this.zzaob, 0L);
    zzfl();
    if ((str == null) || (l <= 0L)) {
      return zzba.zzanc;
    }
    return new Pair(str, Long.valueOf(l));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */