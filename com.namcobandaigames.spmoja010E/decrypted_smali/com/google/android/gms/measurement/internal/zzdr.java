package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@VisibleForTesting
public final class zzdr
  extends zzf
{
  private final zzef zzarz;
  private zzag zzasa;
  private volatile Boolean zzasb;
  private final zzv zzasc;
  private final zzev zzasd;
  private final List<Runnable> zzase = new ArrayList();
  private final zzv zzasf;
  
  protected zzdr(zzbt paramzzbt)
  {
    super(paramzzbt);
    this.zzasd = new zzev(paramzzbt.zzbx());
    this.zzarz = new zzef(this);
    this.zzasc = new zzds(this, paramzzbt);
    this.zzasf = new zzdx(this, paramzzbt);
  }
  
  @WorkerThread
  private final void onServiceDisconnected(ComponentName paramComponentName)
  {
    zzaf();
    if (this.zzasa != null)
    {
      this.zzasa = null;
      zzgo().zzjl().zzg("Disconnected from device MeasurementService", paramComponentName);
      zzaf();
      zzdj();
    }
  }
  
  @WorkerThread
  private final void zzcy()
  {
    zzaf();
    this.zzasd.start();
    this.zzasc.zzh(((Long)zzaf.zzakj.get()).longValue());
  }
  
  @WorkerThread
  private final void zzcz()
  {
    zzaf();
    if (!isConnected()) {
      return;
    }
    zzgo().zzjl().zzbx("Inactivity, disconnecting from the service");
    disconnect();
  }
  
  @WorkerThread
  private final void zzf(Runnable paramRunnable)
    throws IllegalStateException
  {
    zzaf();
    if (isConnected())
    {
      paramRunnable.run();
      return;
    }
    if (this.zzase.size() >= 1000L)
    {
      zzgo().zzjd().zzbx("Discarding data. Max runnable queue size reached");
      return;
    }
    this.zzase.add(paramRunnable);
    this.zzasf.zzh(60000L);
    zzdj();
  }
  
  private final boolean zzld()
  {
    zzgr();
    return true;
  }
  
  @WorkerThread
  private final void zzlf()
  {
    zzaf();
    zzgo().zzjl().zzg("Processing queued up service tasks", Integer.valueOf(this.zzase.size()));
    Iterator localIterator = this.zzase.iterator();
    while (localIterator.hasNext())
    {
      Runnable localRunnable = (Runnable)localIterator.next();
      try
      {
        localRunnable.run();
      }
      catch (Exception localException)
      {
        zzgo().zzjd().zzg("Task exception while flushing queue", localException);
      }
    }
    this.zzase.clear();
    this.zzasf.cancel();
  }
  
  @Nullable
  @WorkerThread
  private final zzh zzm(boolean paramBoolean)
  {
    zzgr();
    zzaj localzzaj = zzgf();
    if (paramBoolean) {}
    for (String str = zzgo().zzjn();; str = null) {
      return localzzaj.zzbr(str);
    }
  }
  
  @WorkerThread
  public final void disconnect()
  {
    zzaf();
    zzcl();
    if (zzn.zzia()) {
      this.zzarz.zzlg();
    }
    try
    {
      ConnectionTracker.getInstance().unbindService(getContext(), this.zzarz);
      this.zzasa = null;
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      for (;;) {}
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
  }
  
  @WorkerThread
  public final boolean isConnected()
  {
    zzaf();
    zzcl();
    return this.zzasa != null;
  }
  
  @WorkerThread
  protected final void resetAnalyticsData()
  {
    zzaf();
    zzgb();
    zzcl();
    zzh localzzh = zzm(false);
    if (zzld()) {
      zzgi().resetAnalyticsData();
    }
    zzf(new zzdt(this, localzzh));
  }
  
  @WorkerThread
  @VisibleForTesting
  protected final void zza(zzag paramzzag)
  {
    zzaf();
    Preconditions.checkNotNull(paramzzag);
    this.zzasa = paramzzag;
    zzcy();
    zzlf();
  }
  
  @WorkerThread
  @VisibleForTesting
  final void zza(zzag paramzzag, AbstractSafeParcelable paramAbstractSafeParcelable, zzh paramzzh)
  {
    zzaf();
    zzgb();
    zzcl();
    boolean bool = zzld();
    int j = 0;
    int i = 100;
    ArrayList localArrayList;
    Object localObject;
    if ((j < 1001) && (i == 100))
    {
      localArrayList = new ArrayList();
      if (!bool) {
        break label310;
      }
      localObject = zzgi().zzr(100);
      if (localObject == null) {
        break label310;
      }
      localArrayList.addAll((Collection)localObject);
    }
    label310:
    for (i = ((List)localObject).size();; i = 0)
    {
      if ((paramAbstractSafeParcelable != null) && (i < 100)) {
        localArrayList.add(paramAbstractSafeParcelable);
      }
      localArrayList = (ArrayList)localArrayList;
      int m = localArrayList.size();
      int k = 0;
      while (k < m)
      {
        localObject = localArrayList.get(k);
        k += 1;
        localObject = (AbstractSafeParcelable)localObject;
        if ((localObject instanceof zzad)) {
          try
          {
            paramzzag.zza((zzad)localObject, paramzzh);
          }
          catch (RemoteException localRemoteException1)
          {
            zzgo().zzjd().zzg("Failed to send event to the service", localRemoteException1);
          }
        } else if ((localRemoteException1 instanceof zzfh)) {
          try
          {
            paramzzag.zza((zzfh)localRemoteException1, paramzzh);
          }
          catch (RemoteException localRemoteException2)
          {
            zzgo().zzjd().zzg("Failed to send attribute to the service", localRemoteException2);
          }
        } else if ((localRemoteException2 instanceof zzl)) {
          try
          {
            paramzzag.zza((zzl)localRemoteException2, paramzzh);
          }
          catch (RemoteException localRemoteException3)
          {
            zzgo().zzjd().zzg("Failed to send conditional property to the service", localRemoteException3);
          }
        } else {
          zzgo().zzjd().zzbx("Discarding data. Unrecognized parcel type.");
        }
      }
      j += 1;
      break;
      return;
    }
  }
  
  @WorkerThread
  public final void zza(AtomicReference<String> paramAtomicReference)
  {
    zzaf();
    zzcl();
    zzf(new zzdu(this, paramAtomicReference, zzm(false)));
  }
  
  @WorkerThread
  protected final void zza(AtomicReference<List<zzl>> paramAtomicReference, String paramString1, String paramString2, String paramString3)
  {
    zzaf();
    zzcl();
    zzf(new zzeb(this, paramAtomicReference, paramString1, paramString2, paramString3, zzm(false)));
  }
  
  @WorkerThread
  protected final void zza(AtomicReference<List<zzfh>> paramAtomicReference, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    zzaf();
    zzcl();
    zzf(new zzec(this, paramAtomicReference, paramString1, paramString2, paramString3, paramBoolean, zzm(false)));
  }
  
  @WorkerThread
  protected final void zza(AtomicReference<List<zzfh>> paramAtomicReference, boolean paramBoolean)
  {
    zzaf();
    zzcl();
    zzf(new zzee(this, paramAtomicReference, zzm(false), paramBoolean));
  }
  
  @WorkerThread
  protected final void zzb(zzad paramzzad, String paramString)
  {
    Preconditions.checkNotNull(paramzzad);
    zzaf();
    zzcl();
    boolean bool2 = zzld();
    if ((bool2) && (zzgi().zza(paramzzad))) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      zzf(new zzdz(this, bool2, bool1, paramzzad, zzm(true), paramString));
      return;
    }
  }
  
  @WorkerThread
  protected final void zzb(zzdn paramzzdn)
  {
    zzaf();
    zzcl();
    zzf(new zzdw(this, paramzzdn));
  }
  
  @WorkerThread
  protected final void zzb(zzfh paramzzfh)
  {
    zzaf();
    zzcl();
    if ((zzld()) && (zzgi().zza(paramzzfh))) {}
    for (boolean bool = true;; bool = false)
    {
      zzf(new zzed(this, bool, paramzzfh, zzm(true)));
      return;
    }
  }
  
  @WorkerThread
  protected final void zzd(zzl paramzzl)
  {
    Preconditions.checkNotNull(paramzzl);
    zzaf();
    zzcl();
    zzgr();
    if (zzgi().zzc(paramzzl)) {}
    for (boolean bool = true;; bool = false)
    {
      zzf(new zzea(this, true, bool, new zzl(paramzzl), zzm(true), paramzzl));
      return;
    }
  }
  
  @WorkerThread
  final void zzdj()
  {
    int k = 1;
    zzaf();
    zzcl();
    if (isConnected()) {}
    do
    {
      return;
      boolean bool2;
      if (this.zzasb == null)
      {
        zzaf();
        zzcl();
        localObject = zzgp().zzju();
        if ((localObject != null) && (((Boolean)localObject).booleanValue()))
        {
          bool2 = true;
          this.zzasb = Boolean.valueOf(bool2);
        }
      }
      else
      {
        if (!this.zzasb.booleanValue()) {
          continue;
        }
        this.zzarz.zzlh();
        return;
      }
      zzgr();
      boolean bool1;
      if (zzgf().zzjb() == 1)
      {
        i = 1;
        bool1 = true;
      }
      for (;;)
      {
        int j = i;
        if (!bool1)
        {
          j = i;
          if (zzgq().zzib())
          {
            zzgo().zzjd().zzbx("No way to upload. Consider using the full version of Analytics");
            j = 0;
          }
        }
        bool2 = bool1;
        if (j == 0) {
          break;
        }
        zzgp().zzg(bool1);
        bool2 = bool1;
        break;
        zzgo().zzjl().zzbx("Checking service availability");
        localObject = zzgm();
        i = GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(((zzco)localObject).getContext(), 12451000);
        switch (i)
        {
        default: 
          zzgo().zzjg().zzg("Unexpected service status", Integer.valueOf(i));
          i = 0;
          bool1 = false;
          break;
        case 0: 
          zzgo().zzjl().zzbx("Service available");
          i = 1;
          bool1 = true;
          break;
        case 1: 
          zzgo().zzjl().zzbx("Service missing");
          i = 1;
          bool1 = false;
          break;
        case 18: 
          zzgo().zzjg().zzbx("Service updating");
          i = 1;
          bool1 = true;
          break;
        case 2: 
          zzgo().zzjk().zzbx("Service container out of date");
          if (zzgm().zzme() < 13000)
          {
            i = 1;
            bool1 = false;
          }
          else
          {
            localObject = zzgp().zzju();
            if ((localObject == null) || (((Boolean)localObject).booleanValue())) {}
            for (bool1 = true;; bool1 = false)
            {
              i = 0;
              break;
            }
          }
          break;
        case 3: 
          zzgo().zzjg().zzbx("Service disabled");
          i = 0;
          bool1 = false;
          break;
        case 9: 
          zzgo().zzjg().zzbx("Service invalid");
          i = 0;
          bool1 = false;
        }
      }
    } while (zzgq().zzib());
    zzgr();
    Object localObject = getContext().getPackageManager().queryIntentServices(new Intent().setClassName(getContext(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
    if ((localObject != null) && (((List)localObject).size() > 0)) {}
    for (int i = k; i != 0; i = 0)
    {
      localObject = new Intent("com.google.android.gms.measurement.START");
      Context localContext = getContext();
      zzgr();
      ((Intent)localObject).setComponent(new ComponentName(localContext, "com.google.android.gms.measurement.AppMeasurementService"));
      this.zzarz.zzc((Intent)localObject);
      return;
    }
    zzgo().zzjd().zzbx("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  @WorkerThread
  protected final void zzkz()
  {
    zzaf();
    zzcl();
    zzf(new zzdv(this, zzm(true)));
  }
  
  @WorkerThread
  protected final void zzlc()
  {
    zzaf();
    zzcl();
    zzf(new zzdy(this, zzm(true)));
  }
  
  final Boolean zzle()
  {
    return this.zzasb;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */