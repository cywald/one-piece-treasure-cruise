package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

final class zzdu
  implements Runnable
{
  zzdu(zzdr paramzzdr, AtomicReference paramAtomicReference, zzh paramzzh) {}
  
  public final void run()
  {
    localAtomicReference = this.zzash;
    for (;;)
    {
      try
      {
        localObject1 = zzdr.zzd(this.zzasg);
        if (localObject1 == null) {
          this.zzasg.zzgo().zzjd().zzbx("Failed to get app instance id");
        }
      }
      catch (RemoteException localRemoteException)
      {
        Object localObject1;
        this.zzasg.zzgo().zzjd().zzg("Failed to get app instance id", localRemoteException);
        this.zzash.notify();
        continue;
      }
      finally
      {
        this.zzash.notify();
      }
      try
      {
        this.zzash.notify();
        return;
      }
      finally {}
    }
    this.zzash.set(((zzag)localObject1).zzc(this.zzaqn));
    localObject1 = (String)this.zzash.get();
    if (localObject1 != null)
    {
      this.zzasg.zzge().zzcm((String)localObject1);
      this.zzasg.zzgp().zzanl.zzcc((String)localObject1);
    }
    zzdr.zze(this.zzasg);
    this.zzash.notify();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */