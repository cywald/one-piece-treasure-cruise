package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.RemoteException;

final class zzdw
  implements Runnable
{
  zzdw(zzdr paramzzdr, zzdn paramzzdn) {}
  
  public final void run()
  {
    zzag localzzag = zzdr.zzd(this.zzasg);
    if (localzzag == null)
    {
      this.zzasg.zzgo().zzjd().zzbx("Failed to send current screen to service");
      return;
    }
    for (;;)
    {
      try
      {
        if (this.zzary == null)
        {
          localzzag.zza(0L, null, null, this.zzasg.getContext().getPackageName());
          zzdr.zze(this.zzasg);
          return;
        }
      }
      catch (RemoteException localRemoteException)
      {
        this.zzasg.zzgo().zzjd().zzg("Failed to send current screen to the service", localRemoteException);
        return;
      }
      localRemoteException.zza(this.zzary.zzarm, this.zzary.zzuw, this.zzary.zzarl, this.zzasg.getContext().getPackageName());
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */