package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;

@SafeParcelable.Class(creator="ConditionalUserPropertyParcelCreator")
public final class zzl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzl> CREATOR = new zzm();
  @SafeParcelable.Field(id=6)
  public boolean active;
  @SafeParcelable.Field(id=5)
  public long creationTimestamp;
  @SafeParcelable.Field(id=3)
  public String origin;
  @SafeParcelable.Field(id=2)
  public String packageName;
  @SafeParcelable.Field(id=11)
  public long timeToLive;
  @SafeParcelable.Field(id=7)
  public String triggerEventName;
  @SafeParcelable.Field(id=9)
  public long triggerTimeout;
  @SafeParcelable.Field(id=4)
  public zzfh zzahb;
  @SafeParcelable.Field(id=8)
  public zzad zzahc;
  @SafeParcelable.Field(id=10)
  public zzad zzahd;
  @SafeParcelable.Field(id=12)
  public zzad zzahe;
  
  zzl(zzl paramzzl)
  {
    Preconditions.checkNotNull(paramzzl);
    this.packageName = paramzzl.packageName;
    this.origin = paramzzl.origin;
    this.zzahb = paramzzl.zzahb;
    this.creationTimestamp = paramzzl.creationTimestamp;
    this.active = paramzzl.active;
    this.triggerEventName = paramzzl.triggerEventName;
    this.zzahc = paramzzl.zzahc;
    this.triggerTimeout = paramzzl.triggerTimeout;
    this.zzahd = paramzzl.zzahd;
    this.timeToLive = paramzzl.timeToLive;
    this.zzahe = paramzzl.zzahe;
  }
  
  @SafeParcelable.Constructor
  zzl(@SafeParcelable.Param(id=2) String paramString1, @SafeParcelable.Param(id=3) String paramString2, @SafeParcelable.Param(id=4) zzfh paramzzfh, @SafeParcelable.Param(id=5) long paramLong1, @SafeParcelable.Param(id=6) boolean paramBoolean, @SafeParcelable.Param(id=7) String paramString3, @SafeParcelable.Param(id=8) zzad paramzzad1, @SafeParcelable.Param(id=9) long paramLong2, @SafeParcelable.Param(id=10) zzad paramzzad2, @SafeParcelable.Param(id=11) long paramLong3, @SafeParcelable.Param(id=12) zzad paramzzad3)
  {
    this.packageName = paramString1;
    this.origin = paramString2;
    this.zzahb = paramzzfh;
    this.creationTimestamp = paramLong1;
    this.active = paramBoolean;
    this.triggerEventName = paramString3;
    this.zzahc = paramzzad1;
    this.triggerTimeout = paramLong2;
    this.zzahd = paramzzad2;
    this.timeToLive = paramLong3;
    this.zzahe = paramzzad3;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.packageName, false);
    SafeParcelWriter.writeString(paramParcel, 3, this.origin, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzahb, paramInt, false);
    SafeParcelWriter.writeLong(paramParcel, 5, this.creationTimestamp);
    SafeParcelWriter.writeBoolean(paramParcel, 6, this.active);
    SafeParcelWriter.writeString(paramParcel, 7, this.triggerEventName, false);
    SafeParcelWriter.writeParcelable(paramParcel, 8, this.zzahc, paramInt, false);
    SafeParcelWriter.writeLong(paramParcel, 9, this.triggerTimeout);
    SafeParcelWriter.writeParcelable(paramParcel, 10, this.zzahd, paramInt, false);
    SafeParcelWriter.writeLong(paramParcel, 11, this.timeToLive);
    SafeParcelWriter.writeParcelable(paramParcel, 12, this.zzahe, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */