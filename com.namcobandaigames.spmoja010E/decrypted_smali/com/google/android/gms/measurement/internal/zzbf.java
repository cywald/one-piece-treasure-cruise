package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.Preconditions;

public final class zzbf
{
  private String value;
  private boolean zzanx;
  private final String zzaod;
  private final String zzoj;
  
  public zzbf(zzba paramzzba, String paramString1, String paramString2)
  {
    Preconditions.checkNotEmpty(paramString1);
    this.zzoj = paramString1;
    this.zzaod = null;
  }
  
  @WorkerThread
  public final void zzcc(String paramString)
  {
    if (zzfk.zzu(paramString, this.value)) {
      return;
    }
    SharedPreferences.Editor localEditor = zzba.zza(this.zzany).edit();
    localEditor.putString(this.zzoj, paramString);
    localEditor.apply();
    this.value = paramString;
  }
  
  @WorkerThread
  public final String zzjz()
  {
    if (!this.zzanx)
    {
      this.zzanx = true;
      this.value = zzba.zza(this.zzany).getString(this.zzoj, null);
    }
    return this.value;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */