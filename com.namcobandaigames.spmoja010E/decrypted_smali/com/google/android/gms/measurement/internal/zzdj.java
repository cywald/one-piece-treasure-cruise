package com.google.android.gms.measurement.internal;

final class zzdj
  implements Runnable
{
  zzdj(zzcs paramzzcs, boolean paramBoolean) {}
  
  public final void run()
  {
    boolean bool1 = this.zzarc.zzadj.isEnabled();
    boolean bool2 = this.zzarc.zzadj.zzko();
    this.zzarc.zzadj.zzd(this.zzaes);
    if (bool2 == this.zzaes) {
      this.zzarc.zzadj.zzgo().zzjl().zzg("Default data collection state already set to", Boolean.valueOf(this.zzaes));
    }
    if ((this.zzarc.zzadj.isEnabled() == bool1) || (this.zzarc.zzadj.isEnabled() != this.zzarc.zzadj.zzko())) {
      this.zzarc.zzadj.zzgo().zzji().zze("Default data collection is different than actual status", Boolean.valueOf(this.zzaes), Boolean.valueOf(bool1));
    }
    zzcs.zza(this.zzarc);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */