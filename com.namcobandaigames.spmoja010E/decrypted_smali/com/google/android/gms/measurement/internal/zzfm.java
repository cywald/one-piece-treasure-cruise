package com.google.android.gms.measurement.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

final class zzfm
  extends SSLSocket
{
  private final SSLSocket zzaun;
  
  zzfm(zzfl paramzzfl, SSLSocket paramSSLSocket)
  {
    this.zzaun = paramSSLSocket;
  }
  
  public final void addHandshakeCompletedListener(HandshakeCompletedListener paramHandshakeCompletedListener)
  {
    this.zzaun.addHandshakeCompletedListener(paramHandshakeCompletedListener);
  }
  
  public final void bind(SocketAddress paramSocketAddress)
    throws IOException
  {
    this.zzaun.bind(paramSocketAddress);
  }
  
  public final void close()
    throws IOException
  {
    try
    {
      this.zzaun.close();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void connect(SocketAddress paramSocketAddress)
    throws IOException
  {
    this.zzaun.connect(paramSocketAddress);
  }
  
  public final void connect(SocketAddress paramSocketAddress, int paramInt)
    throws IOException
  {
    this.zzaun.connect(paramSocketAddress, paramInt);
  }
  
  public final boolean equals(Object paramObject)
  {
    return this.zzaun.equals(paramObject);
  }
  
  public final SocketChannel getChannel()
  {
    return this.zzaun.getChannel();
  }
  
  public final boolean getEnableSessionCreation()
  {
    return this.zzaun.getEnableSessionCreation();
  }
  
  public final String[] getEnabledCipherSuites()
  {
    return this.zzaun.getEnabledCipherSuites();
  }
  
  public final String[] getEnabledProtocols()
  {
    return this.zzaun.getEnabledProtocols();
  }
  
  public final InetAddress getInetAddress()
  {
    return this.zzaun.getInetAddress();
  }
  
  public final InputStream getInputStream()
    throws IOException
  {
    return this.zzaun.getInputStream();
  }
  
  public final boolean getKeepAlive()
    throws SocketException
  {
    return this.zzaun.getKeepAlive();
  }
  
  public final InetAddress getLocalAddress()
  {
    return this.zzaun.getLocalAddress();
  }
  
  public final int getLocalPort()
  {
    return this.zzaun.getLocalPort();
  }
  
  public final SocketAddress getLocalSocketAddress()
  {
    return this.zzaun.getLocalSocketAddress();
  }
  
  public final boolean getNeedClientAuth()
  {
    return this.zzaun.getNeedClientAuth();
  }
  
  public final boolean getOOBInline()
    throws SocketException
  {
    return this.zzaun.getOOBInline();
  }
  
  public final OutputStream getOutputStream()
    throws IOException
  {
    return this.zzaun.getOutputStream();
  }
  
  public final int getPort()
  {
    return this.zzaun.getPort();
  }
  
  public final int getReceiveBufferSize()
    throws SocketException
  {
    try
    {
      int i = this.zzaun.getReceiveBufferSize();
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final SocketAddress getRemoteSocketAddress()
  {
    return this.zzaun.getRemoteSocketAddress();
  }
  
  public final boolean getReuseAddress()
    throws SocketException
  {
    return this.zzaun.getReuseAddress();
  }
  
  public final int getSendBufferSize()
    throws SocketException
  {
    try
    {
      int i = this.zzaun.getSendBufferSize();
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final SSLSession getSession()
  {
    return this.zzaun.getSession();
  }
  
  public final int getSoLinger()
    throws SocketException
  {
    return this.zzaun.getSoLinger();
  }
  
  public final int getSoTimeout()
    throws SocketException
  {
    try
    {
      int i = this.zzaun.getSoTimeout();
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final String[] getSupportedCipherSuites()
  {
    return this.zzaun.getSupportedCipherSuites();
  }
  
  public final String[] getSupportedProtocols()
  {
    return this.zzaun.getSupportedProtocols();
  }
  
  public final boolean getTcpNoDelay()
    throws SocketException
  {
    return this.zzaun.getTcpNoDelay();
  }
  
  public final int getTrafficClass()
    throws SocketException
  {
    return this.zzaun.getTrafficClass();
  }
  
  public final boolean getUseClientMode()
  {
    return this.zzaun.getUseClientMode();
  }
  
  public final boolean getWantClientAuth()
  {
    return this.zzaun.getWantClientAuth();
  }
  
  public final boolean isBound()
  {
    return this.zzaun.isBound();
  }
  
  public final boolean isClosed()
  {
    return this.zzaun.isClosed();
  }
  
  public final boolean isConnected()
  {
    return this.zzaun.isConnected();
  }
  
  public final boolean isInputShutdown()
  {
    return this.zzaun.isInputShutdown();
  }
  
  public final boolean isOutputShutdown()
  {
    return this.zzaun.isOutputShutdown();
  }
  
  public final void removeHandshakeCompletedListener(HandshakeCompletedListener paramHandshakeCompletedListener)
  {
    this.zzaun.removeHandshakeCompletedListener(paramHandshakeCompletedListener);
  }
  
  public final void sendUrgentData(int paramInt)
    throws IOException
  {
    this.zzaun.sendUrgentData(paramInt);
  }
  
  public final void setEnableSessionCreation(boolean paramBoolean)
  {
    this.zzaun.setEnableSessionCreation(paramBoolean);
  }
  
  public final void setEnabledCipherSuites(String[] paramArrayOfString)
  {
    this.zzaun.setEnabledCipherSuites(paramArrayOfString);
  }
  
  public final void setEnabledProtocols(String[] paramArrayOfString)
  {
    String[] arrayOfString = paramArrayOfString;
    if (paramArrayOfString != null)
    {
      arrayOfString = paramArrayOfString;
      if (Arrays.asList(paramArrayOfString).contains("SSLv3"))
      {
        paramArrayOfString = new ArrayList(Arrays.asList(this.zzaun.getEnabledProtocols()));
        if (paramArrayOfString.size() > 1) {
          paramArrayOfString.remove("SSLv3");
        }
        arrayOfString = (String[])paramArrayOfString.toArray(new String[paramArrayOfString.size()]);
      }
    }
    this.zzaun.setEnabledProtocols(arrayOfString);
  }
  
  public final void setKeepAlive(boolean paramBoolean)
    throws SocketException
  {
    this.zzaun.setKeepAlive(paramBoolean);
  }
  
  public final void setNeedClientAuth(boolean paramBoolean)
  {
    this.zzaun.setNeedClientAuth(paramBoolean);
  }
  
  public final void setOOBInline(boolean paramBoolean)
    throws SocketException
  {
    this.zzaun.setOOBInline(paramBoolean);
  }
  
  public final void setPerformancePreferences(int paramInt1, int paramInt2, int paramInt3)
  {
    this.zzaun.setPerformancePreferences(paramInt1, paramInt2, paramInt3);
  }
  
  public final void setReceiveBufferSize(int paramInt)
    throws SocketException
  {
    try
    {
      this.zzaun.setReceiveBufferSize(paramInt);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void setReuseAddress(boolean paramBoolean)
    throws SocketException
  {
    this.zzaun.setReuseAddress(paramBoolean);
  }
  
  public final void setSendBufferSize(int paramInt)
    throws SocketException
  {
    try
    {
      this.zzaun.setSendBufferSize(paramInt);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void setSoLinger(boolean paramBoolean, int paramInt)
    throws SocketException
  {
    this.zzaun.setSoLinger(paramBoolean, paramInt);
  }
  
  public final void setSoTimeout(int paramInt)
    throws SocketException
  {
    try
    {
      this.zzaun.setSoTimeout(paramInt);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void setTcpNoDelay(boolean paramBoolean)
    throws SocketException
  {
    this.zzaun.setTcpNoDelay(paramBoolean);
  }
  
  public final void setTrafficClass(int paramInt)
    throws SocketException
  {
    this.zzaun.setTrafficClass(paramInt);
  }
  
  public final void setUseClientMode(boolean paramBoolean)
  {
    this.zzaun.setUseClientMode(paramBoolean);
  }
  
  public final void setWantClientAuth(boolean paramBoolean)
  {
    this.zzaun.setWantClientAuth(paramBoolean);
  }
  
  public final void shutdownInput()
    throws IOException
  {
    this.zzaun.shutdownInput();
  }
  
  public final void shutdownOutput()
    throws IOException
  {
    this.zzaun.shutdownOutput();
  }
  
  public final void startHandshake()
    throws IOException
  {
    this.zzaun.startHandshake();
  }
  
  public final String toString()
  {
    return this.zzaun.toString();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzfm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */