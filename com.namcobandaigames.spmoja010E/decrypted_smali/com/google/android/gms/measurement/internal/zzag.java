package com.google.android.gms.measurement.internal;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public abstract interface zzag
  extends IInterface
{
  public abstract List<zzfh> zza(zzh paramzzh, boolean paramBoolean)
    throws RemoteException;
  
  public abstract List<zzl> zza(String paramString1, String paramString2, zzh paramzzh)
    throws RemoteException;
  
  public abstract List<zzfh> zza(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
    throws RemoteException;
  
  public abstract List<zzfh> zza(String paramString1, String paramString2, boolean paramBoolean, zzh paramzzh)
    throws RemoteException;
  
  public abstract void zza(long paramLong, String paramString1, String paramString2, String paramString3)
    throws RemoteException;
  
  public abstract void zza(zzad paramzzad, zzh paramzzh)
    throws RemoteException;
  
  public abstract void zza(zzad paramzzad, String paramString1, String paramString2)
    throws RemoteException;
  
  public abstract void zza(zzfh paramzzfh, zzh paramzzh)
    throws RemoteException;
  
  public abstract void zza(zzh paramzzh)
    throws RemoteException;
  
  public abstract void zza(zzl paramzzl, zzh paramzzh)
    throws RemoteException;
  
  public abstract byte[] zza(zzad paramzzad, String paramString)
    throws RemoteException;
  
  public abstract void zzb(zzh paramzzh)
    throws RemoteException;
  
  public abstract void zzb(zzl paramzzl)
    throws RemoteException;
  
  public abstract String zzc(zzh paramzzh)
    throws RemoteException;
  
  public abstract void zzd(zzh paramzzh)
    throws RemoteException;
  
  public abstract List<zzl> zze(String paramString1, String paramString2, String paramString3)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */