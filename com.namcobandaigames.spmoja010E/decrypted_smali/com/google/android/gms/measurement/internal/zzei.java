package com.google.android.gms.measurement.internal;

final class zzei
  implements Runnable
{
  zzei(zzef paramzzef, zzag paramzzag) {}
  
  public final void run()
  {
    synchronized (this.zzasp)
    {
      zzef.zza(this.zzasp, false);
      if (!this.zzasp.zzasg.isConnected())
      {
        this.zzasp.zzasg.zzgo().zzjk().zzbx("Connected to remote service");
        this.zzasp.zzasg.zza(this.zzasq);
      }
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzei.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */