package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ParseException;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.internal.measurement.zzfv;
import com.google.android.gms.internal.measurement.zzfw;
import com.google.android.gms.internal.measurement.zzfx;
import com.google.android.gms.internal.measurement.zzfy;
import com.google.android.gms.internal.measurement.zzfz;
import com.google.android.gms.internal.measurement.zzgd;
import com.google.android.gms.internal.measurement.zzgf;
import com.google.android.gms.internal.measurement.zzgg;
import com.google.android.gms.internal.measurement.zzgh;
import com.google.android.gms.internal.measurement.zzgi;
import com.google.android.gms.internal.measurement.zzgj;
import com.google.android.gms.internal.measurement.zzgl;
import com.google.android.gms.internal.measurement.zzyy;
import com.google.android.gms.internal.measurement.zzzg;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.BitSet;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public final class zzfg
  extends zzez
{
  zzfg(zzfa paramzzfa)
  {
    super(paramzzfa);
  }
  
  static zzgg zza(zzgf paramzzgf, String paramString)
  {
    paramzzgf = paramzzgf.zzawt;
    int j = paramzzgf.length;
    int i = 0;
    while (i < j)
    {
      zzgg localzzgg = paramzzgf[i];
      if (localzzgg.name.equals(paramString)) {
        return localzzgg;
      }
      i += 1;
    }
    return null;
  }
  
  private static void zza(StringBuilder paramStringBuilder, int paramInt)
  {
    int i = 0;
    while (i < paramInt)
    {
      paramStringBuilder.append("  ");
      i += 1;
    }
  }
  
  private final void zza(StringBuilder paramStringBuilder, int paramInt, zzfw paramzzfw)
  {
    if (paramzzfw == null) {
      return;
    }
    zza(paramStringBuilder, paramInt);
    paramStringBuilder.append("filter {\n");
    zza(paramStringBuilder, paramInt, "complement", paramzzfw.zzavm);
    zza(paramStringBuilder, paramInt, "param_name", zzgl().zzbt(paramzzfw.zzavn));
    int j = paramInt + 1;
    zzfz localzzfz = paramzzfw.zzavk;
    if (localzzfz != null)
    {
      zza(paramStringBuilder, j);
      paramStringBuilder.append("string_filter");
      paramStringBuilder.append(" {\n");
      Object localObject;
      if (localzzfz.zzavw != null)
      {
        localObject = "UNKNOWN_MATCH_TYPE";
        switch (localzzfz.zzavw.intValue())
        {
        }
      }
      for (;;)
      {
        zza(paramStringBuilder, j, "match_type", localObject);
        zza(paramStringBuilder, j, "expression", localzzfz.zzavx);
        zza(paramStringBuilder, j, "case_sensitive", localzzfz.zzavy);
        if (localzzfz.zzavz.length <= 0) {
          break label309;
        }
        zza(paramStringBuilder, j + 1);
        paramStringBuilder.append("expression_list {\n");
        localObject = localzzfz.zzavz;
        int k = localObject.length;
        int i = 0;
        while (i < k)
        {
          localzzfz = localObject[i];
          zza(paramStringBuilder, j + 2);
          paramStringBuilder.append(localzzfz);
          paramStringBuilder.append("\n");
          i += 1;
        }
        localObject = "REGEXP";
        continue;
        localObject = "BEGINS_WITH";
        continue;
        localObject = "ENDS_WITH";
        continue;
        localObject = "PARTIAL";
        continue;
        localObject = "EXACT";
        continue;
        localObject = "IN_LIST";
      }
      paramStringBuilder.append("}\n");
      label309:
      zza(paramStringBuilder, j);
      paramStringBuilder.append("}\n");
    }
    zza(paramStringBuilder, paramInt + 1, "number_filter", paramzzfw.zzavl);
    zza(paramStringBuilder, paramInt);
    paramStringBuilder.append("}\n");
  }
  
  private final void zza(StringBuilder paramStringBuilder, int paramInt, String paramString, zzfx paramzzfx)
  {
    if (paramzzfx == null) {
      return;
    }
    zza(paramStringBuilder, paramInt);
    paramStringBuilder.append(paramString);
    paramStringBuilder.append(" {\n");
    if (paramzzfx.zzavo != null)
    {
      paramString = "UNKNOWN_COMPARISON_TYPE";
      switch (paramzzfx.zzavo.intValue())
      {
      }
    }
    for (;;)
    {
      zza(paramStringBuilder, paramInt, "comparison_type", paramString);
      zza(paramStringBuilder, paramInt, "match_as_float", paramzzfx.zzavp);
      zza(paramStringBuilder, paramInt, "comparison_value", paramzzfx.zzavq);
      zza(paramStringBuilder, paramInt, "min_comparison_value", paramzzfx.zzavr);
      zza(paramStringBuilder, paramInt, "max_comparison_value", paramzzfx.zzavs);
      zza(paramStringBuilder, paramInt);
      paramStringBuilder.append("}\n");
      return;
      paramString = "LESS_THAN";
      continue;
      paramString = "GREATER_THAN";
      continue;
      paramString = "EQUAL";
      continue;
      paramString = "BETWEEN";
    }
  }
  
  private static void zza(StringBuilder paramStringBuilder, int paramInt, String paramString, zzgj paramzzgj)
  {
    int j = 0;
    if (paramzzgj == null) {
      return;
    }
    zza(paramStringBuilder, 3);
    paramStringBuilder.append(paramString);
    paramStringBuilder.append(" {\n");
    int k;
    int i;
    long l;
    if (paramzzgj.zzayf != null)
    {
      zza(paramStringBuilder, 4);
      paramStringBuilder.append("results: ");
      paramString = paramzzgj.zzayf;
      k = paramString.length;
      i = 0;
      paramInt = 0;
      while (i < k)
      {
        l = paramString[i];
        if (paramInt != 0) {
          paramStringBuilder.append(", ");
        }
        paramStringBuilder.append(Long.valueOf(l));
        i += 1;
        paramInt += 1;
      }
      paramStringBuilder.append('\n');
    }
    if (paramzzgj.zzaye != null)
    {
      zza(paramStringBuilder, 4);
      paramStringBuilder.append("status: ");
      paramString = paramzzgj.zzaye;
      k = paramString.length;
      paramInt = 0;
      i = j;
      while (i < k)
      {
        l = paramString[i];
        if (paramInt != 0) {
          paramStringBuilder.append(", ");
        }
        paramStringBuilder.append(Long.valueOf(l));
        i += 1;
        paramInt += 1;
      }
      paramStringBuilder.append('\n');
    }
    zza(paramStringBuilder, 3);
    paramStringBuilder.append("}\n");
  }
  
  private static void zza(StringBuilder paramStringBuilder, int paramInt, String paramString, Object paramObject)
  {
    if (paramObject == null) {
      return;
    }
    zza(paramStringBuilder, paramInt + 1);
    paramStringBuilder.append(paramString);
    paramStringBuilder.append(": ");
    paramStringBuilder.append(paramObject);
    paramStringBuilder.append('\n');
  }
  
  static boolean zza(long[] paramArrayOfLong, int paramInt)
  {
    if (paramInt >= paramArrayOfLong.length << 6) {}
    while ((paramArrayOfLong[(paramInt / 64)] & 1L << paramInt % 64) == 0L) {
      return false;
    }
    return true;
  }
  
  static long[] zza(BitSet paramBitSet)
  {
    int k = (paramBitSet.length() + 63) / 64;
    long[] arrayOfLong = new long[k];
    int i = 0;
    while (i < k)
    {
      arrayOfLong[i] = 0L;
      int j = 0;
      while ((j < 64) && ((i << 6) + j < paramBitSet.length()))
      {
        if (paramBitSet.get((i << 6) + j)) {
          arrayOfLong[i] |= 1L << j;
        }
        j += 1;
      }
      i += 1;
    }
    return arrayOfLong;
  }
  
  static zzgg[] zza(zzgg[] paramArrayOfzzgg, String paramString, Object paramObject)
  {
    int j = paramArrayOfzzgg.length;
    int i = 0;
    while (i < j)
    {
      localObject = paramArrayOfzzgg[i];
      if (paramString.equals(((zzgg)localObject).name))
      {
        ((zzgg)localObject).zzawx = null;
        ((zzgg)localObject).zzamp = null;
        ((zzgg)localObject).zzauh = null;
        if ((paramObject instanceof Long)) {
          ((zzgg)localObject).zzawx = ((Long)paramObject);
        }
        do
        {
          return paramArrayOfzzgg;
          if ((paramObject instanceof String))
          {
            ((zzgg)localObject).zzamp = ((String)paramObject);
            return paramArrayOfzzgg;
          }
        } while (!(paramObject instanceof Double));
        ((zzgg)localObject).zzauh = ((Double)paramObject);
        return paramArrayOfzzgg;
      }
      i += 1;
    }
    Object localObject = new zzgg[paramArrayOfzzgg.length + 1];
    System.arraycopy(paramArrayOfzzgg, 0, localObject, 0, paramArrayOfzzgg.length);
    zzgg localzzgg = new zzgg();
    localzzgg.name = paramString;
    if ((paramObject instanceof Long)) {
      localzzgg.zzawx = ((Long)paramObject);
    }
    for (;;)
    {
      localObject[paramArrayOfzzgg.length] = localzzgg;
      return (zzgg[])localObject;
      if ((paramObject instanceof String)) {
        localzzgg.zzamp = ((String)paramObject);
      } else if ((paramObject instanceof Double)) {
        localzzgg.zzauh = ((Double)paramObject);
      }
    }
  }
  
  static Object zzb(zzgf paramzzgf, String paramString)
  {
    paramzzgf = zza(paramzzgf, paramString);
    if (paramzzgf != null)
    {
      if (paramzzgf.zzamp != null) {
        return paramzzgf.zzamp;
      }
      if (paramzzgf.zzawx != null) {
        return paramzzgf.zzawx;
      }
      if (paramzzgf.zzauh != null) {
        return paramzzgf.zzauh;
      }
    }
    return null;
  }
  
  static boolean zzcp(String paramString)
  {
    return (paramString != null) && (paramString.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)")) && (paramString.length() <= 310);
  }
  
  final <T extends Parcelable> T zza(byte[] paramArrayOfByte, Parcelable.Creator<T> paramCreator)
  {
    if (paramArrayOfByte == null) {
      return null;
    }
    Parcel localParcel = Parcel.obtain();
    try
    {
      localParcel.unmarshall(paramArrayOfByte, 0, paramArrayOfByte.length);
      localParcel.setDataPosition(0);
      paramArrayOfByte = (Parcelable)paramCreator.createFromParcel(localParcel);
      return paramArrayOfByte;
    }
    catch (SafeParcelReader.ParseException paramArrayOfByte)
    {
      zzgo().zzjd().zzbx("Failed to load parcelable from buffer");
      return null;
    }
    finally
    {
      localParcel.recycle();
    }
  }
  
  final String zza(zzfv paramzzfv)
  {
    int i = 0;
    if (paramzzfv == null) {
      return "null";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("\nevent_filter {\n");
    zza(localStringBuilder, 0, "filter_id", paramzzfv.zzave);
    zza(localStringBuilder, 0, "event_name", zzgl().zzbs(paramzzfv.zzavf));
    zza(localStringBuilder, 1, "event_count_filter", paramzzfv.zzavi);
    localStringBuilder.append("  filters {\n");
    paramzzfv = paramzzfv.zzavg;
    int j = paramzzfv.length;
    while (i < j)
    {
      zza(localStringBuilder, 2, paramzzfv[i]);
      i += 1;
    }
    zza(localStringBuilder, 1);
    localStringBuilder.append("}\n}\n");
    return localStringBuilder.toString();
  }
  
  final String zza(zzfy paramzzfy)
  {
    if (paramzzfy == null) {
      return "null";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("\nproperty_filter {\n");
    zza(localStringBuilder, 0, "filter_id", paramzzfy.zzave);
    zza(localStringBuilder, 0, "property_name", zzgl().zzbu(paramzzfy.zzavu));
    zza(localStringBuilder, 1, paramzzfy.zzavv);
    localStringBuilder.append("}\n");
    return localStringBuilder.toString();
  }
  
  final void zza(zzgg paramzzgg, Object paramObject)
  {
    Preconditions.checkNotNull(paramObject);
    paramzzgg.zzamp = null;
    paramzzgg.zzawx = null;
    paramzzgg.zzauh = null;
    if ((paramObject instanceof String))
    {
      paramzzgg.zzamp = ((String)paramObject);
      return;
    }
    if ((paramObject instanceof Long))
    {
      paramzzgg.zzawx = ((Long)paramObject);
      return;
    }
    if ((paramObject instanceof Double))
    {
      paramzzgg.zzauh = ((Double)paramObject);
      return;
    }
    zzgo().zzjd().zzg("Ignoring invalid (type) event param value", paramObject);
  }
  
  final void zza(zzgl paramzzgl, Object paramObject)
  {
    Preconditions.checkNotNull(paramObject);
    paramzzgl.zzamp = null;
    paramzzgl.zzawx = null;
    paramzzgl.zzauh = null;
    if ((paramObject instanceof String))
    {
      paramzzgl.zzamp = ((String)paramObject);
      return;
    }
    if ((paramObject instanceof Long))
    {
      paramzzgl.zzawx = ((Long)paramObject);
      return;
    }
    if ((paramObject instanceof Double))
    {
      paramzzgl.zzauh = ((Double)paramObject);
      return;
    }
    zzgo().zzjd().zzg("Ignoring invalid (type) user attribute value", paramObject);
  }
  
  final byte[] zza(zzgh paramzzgh)
  {
    try
    {
      byte[] arrayOfByte = new byte[paramzzgh.zzvu()];
      zzyy localzzyy = zzyy.zzk(arrayOfByte, 0, arrayOfByte.length);
      paramzzgh.zza(localzzyy);
      localzzyy.zzyt();
      return arrayOfByte;
    }
    catch (IOException paramzzgh)
    {
      zzgo().zzjd().zzg("Data loss. Failed to serialize batch", paramzzgh);
    }
    return null;
  }
  
  final byte[] zza(byte[] paramArrayOfByte)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream;
    try
    {
      paramArrayOfByte = new ByteArrayInputStream(paramArrayOfByte);
      GZIPInputStream localGZIPInputStream = new GZIPInputStream(paramArrayOfByte);
      localByteArrayOutputStream = new ByteArrayOutputStream();
      byte[] arrayOfByte = new byte['Ѐ'];
      for (;;)
      {
        int i = localGZIPInputStream.read(arrayOfByte);
        if (i <= 0) {
          break;
        }
        localByteArrayOutputStream.write(arrayOfByte, 0, i);
      }
      localGZIPInputStream.close();
    }
    catch (IOException paramArrayOfByte)
    {
      zzgo().zzjd().zzg("Failed to ungzip content", paramArrayOfByte);
      throw paramArrayOfByte;
    }
    paramArrayOfByte.close();
    paramArrayOfByte = localByteArrayOutputStream.toByteArray();
    return paramArrayOfByte;
  }
  
  final String zzb(zzgh paramzzgh)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("\nbatch {\n");
    if (paramzzgh.zzawy != null)
    {
      paramzzgh = paramzzgh.zzawy;
      int m = paramzzgh.length;
      int i = 0;
      while (i < m)
      {
        zzgf[] arrayOfzzgf = paramzzgh[i];
        if ((arrayOfzzgf != null) && (arrayOfzzgf != null))
        {
          zza(localStringBuilder, 1);
          localStringBuilder.append("bundle {\n");
          zza(localStringBuilder, 1, "protocol_version", arrayOfzzgf.zzaxa);
          zza(localStringBuilder, 1, "platform", arrayOfzzgf.zzaxi);
          zza(localStringBuilder, 1, "gmp_version", arrayOfzzgf.zzaxm);
          zza(localStringBuilder, 1, "uploading_gmp_version", arrayOfzzgf.zzaxn);
          zza(localStringBuilder, 1, "config_version", arrayOfzzgf.zzaxy);
          zza(localStringBuilder, 1, "gmp_app_id", arrayOfzzgf.zzafx);
          zza(localStringBuilder, 1, "admob_app_id", arrayOfzzgf.zzawj);
          zza(localStringBuilder, 1, "app_id", arrayOfzzgf.zztt);
          zza(localStringBuilder, 1, "app_version", arrayOfzzgf.zzts);
          zza(localStringBuilder, 1, "app_version_major", arrayOfzzgf.zzaxu);
          zza(localStringBuilder, 1, "firebase_instance_id", arrayOfzzgf.zzafz);
          zza(localStringBuilder, 1, "dev_cert_hash", arrayOfzzgf.zzaxq);
          zza(localStringBuilder, 1, "app_store", arrayOfzzgf.zzage);
          zza(localStringBuilder, 1, "upload_timestamp_millis", arrayOfzzgf.zzaxd);
          zza(localStringBuilder, 1, "start_timestamp_millis", arrayOfzzgf.zzaxe);
          zza(localStringBuilder, 1, "end_timestamp_millis", arrayOfzzgf.zzaxf);
          zza(localStringBuilder, 1, "previous_bundle_start_timestamp_millis", arrayOfzzgf.zzaxg);
          zza(localStringBuilder, 1, "previous_bundle_end_timestamp_millis", arrayOfzzgf.zzaxh);
          zza(localStringBuilder, 1, "app_instance_id", arrayOfzzgf.zzafw);
          zza(localStringBuilder, 1, "resettable_device_id", arrayOfzzgf.zzaxo);
          zza(localStringBuilder, 1, "device_id", arrayOfzzgf.zzaxx);
          zza(localStringBuilder, 1, "ds_id", arrayOfzzgf.zzaya);
          zza(localStringBuilder, 1, "limited_ad_tracking", arrayOfzzgf.zzaxp);
          zza(localStringBuilder, 1, "os_version", arrayOfzzgf.zzaxj);
          zza(localStringBuilder, 1, "device_model", arrayOfzzgf.zzaxk);
          zza(localStringBuilder, 1, "user_default_language", arrayOfzzgf.zzaia);
          zza(localStringBuilder, 1, "time_zone_offset_minutes", arrayOfzzgf.zzaxl);
          zza(localStringBuilder, 1, "bundle_sequential_index", arrayOfzzgf.zzaxr);
          zza(localStringBuilder, 1, "service_upload", arrayOfzzgf.zzaxs);
          zza(localStringBuilder, 1, "health_monitor", arrayOfzzgf.zzagv);
          if ((arrayOfzzgf.zzaxz != null) && (arrayOfzzgf.zzaxz.longValue() != 0L)) {
            zza(localStringBuilder, 1, "android_id", arrayOfzzgf.zzaxz);
          }
          if (arrayOfzzgf.zzayc != null) {
            zza(localStringBuilder, 1, "retry_counter", arrayOfzzgf.zzayc);
          }
          Object localObject1 = arrayOfzzgf.zzaxc;
          int k;
          int j;
          Object localObject2;
          if (localObject1 != null)
          {
            k = localObject1.length;
            j = 0;
            while (j < k)
            {
              localObject2 = localObject1[j];
              if (localObject2 != null)
              {
                zza(localStringBuilder, 2);
                localStringBuilder.append("user_property {\n");
                zza(localStringBuilder, 2, "set_timestamp_millis", ((zzgl)localObject2).zzayl);
                zza(localStringBuilder, 2, "name", zzgl().zzbu(((zzgl)localObject2).name));
                zza(localStringBuilder, 2, "string_value", ((zzgl)localObject2).zzamp);
                zza(localStringBuilder, 2, "int_value", ((zzgl)localObject2).zzawx);
                zza(localStringBuilder, 2, "double_value", ((zzgl)localObject2).zzauh);
                zza(localStringBuilder, 2);
                localStringBuilder.append("}\n");
              }
              j += 1;
            }
          }
          localObject1 = arrayOfzzgf.zzaxt;
          if (localObject1 != null)
          {
            k = localObject1.length;
            j = 0;
            while (j < k)
            {
              localObject2 = localObject1[j];
              if (localObject2 != null)
              {
                zza(localStringBuilder, 2);
                localStringBuilder.append("audience_membership {\n");
                zza(localStringBuilder, 2, "audience_id", ((zzgd)localObject2).zzauy);
                zza(localStringBuilder, 2, "new_audience", ((zzgd)localObject2).zzawo);
                zza(localStringBuilder, 2, "current_data", ((zzgd)localObject2).zzawm);
                zza(localStringBuilder, 2, "previous_data", ((zzgd)localObject2).zzawn);
                zza(localStringBuilder, 2);
                localStringBuilder.append("}\n");
              }
              j += 1;
            }
          }
          arrayOfzzgf = arrayOfzzgf.zzaxb;
          if (arrayOfzzgf != null)
          {
            int n = arrayOfzzgf.length;
            j = 0;
            while (j < n)
            {
              localObject1 = arrayOfzzgf[j];
              if (localObject1 != null)
              {
                zza(localStringBuilder, 2);
                localStringBuilder.append("event {\n");
                zza(localStringBuilder, 2, "name", zzgl().zzbs(((zzgf)localObject1).name));
                zza(localStringBuilder, 2, "timestamp_millis", ((zzgf)localObject1).zzawu);
                zza(localStringBuilder, 2, "previous_timestamp_millis", ((zzgf)localObject1).zzawv);
                zza(localStringBuilder, 2, "count", ((zzgf)localObject1).count);
                localObject1 = ((zzgf)localObject1).zzawt;
                if (localObject1 != null)
                {
                  int i1 = localObject1.length;
                  k = 0;
                  while (k < i1)
                  {
                    localObject2 = localObject1[k];
                    if (localObject2 != null)
                    {
                      zza(localStringBuilder, 3);
                      localStringBuilder.append("param {\n");
                      zza(localStringBuilder, 3, "name", zzgl().zzbt(((zzgg)localObject2).name));
                      zza(localStringBuilder, 3, "string_value", ((zzgg)localObject2).zzamp);
                      zza(localStringBuilder, 3, "int_value", ((zzgg)localObject2).zzawx);
                      zza(localStringBuilder, 3, "double_value", ((zzgg)localObject2).zzauh);
                      zza(localStringBuilder, 3);
                      localStringBuilder.append("}\n");
                    }
                    k += 1;
                  }
                }
                zza(localStringBuilder, 2);
                localStringBuilder.append("}\n");
              }
              j += 1;
            }
          }
          zza(localStringBuilder, 1);
          localStringBuilder.append("}\n");
        }
        i += 1;
      }
    }
    localStringBuilder.append("}\n");
    return localStringBuilder.toString();
  }
  
  final boolean zzb(long paramLong1, long paramLong2)
  {
    if ((paramLong1 == 0L) || (paramLong2 <= 0L)) {}
    while (Math.abs(zzbx().currentTimeMillis() - paramLong1) > paramLong2) {
      return true;
    }
    return false;
  }
  
  final byte[] zzb(byte[] paramArrayOfByte)
    throws IOException
  {
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
      GZIPOutputStream localGZIPOutputStream = new GZIPOutputStream(localByteArrayOutputStream);
      localGZIPOutputStream.write(paramArrayOfByte);
      localGZIPOutputStream.close();
      localByteArrayOutputStream.close();
      paramArrayOfByte = localByteArrayOutputStream.toByteArray();
      return paramArrayOfByte;
    }
    catch (IOException paramArrayOfByte)
    {
      zzgo().zzjd().zzg("Failed to gzip content", paramArrayOfByte);
      throw paramArrayOfByte;
    }
  }
  
  @WorkerThread
  final boolean zze(zzad paramzzad, zzh paramzzh)
  {
    Preconditions.checkNotNull(paramzzad);
    Preconditions.checkNotNull(paramzzh);
    if ((TextUtils.isEmpty(paramzzh.zzafx)) && (TextUtils.isEmpty(paramzzh.zzagk)))
    {
      zzgr();
      return false;
    }
    return true;
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzfg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */