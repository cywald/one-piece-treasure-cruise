package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;

final class zzev
{
  private long startTime;
  private final Clock zzrz;
  
  public zzev(Clock paramClock)
  {
    Preconditions.checkNotNull(paramClock);
    this.zzrz = paramClock;
  }
  
  public final void clear()
  {
    this.startTime = 0L;
  }
  
  public final void start()
  {
    this.startTime = this.zzrz.elapsedRealtime();
  }
  
  public final boolean zzj(long paramLong)
  {
    if (this.startTime == 0L) {}
    while (this.zzrz.elapsedRealtime() - this.startTime >= 3600000L) {
      return true;
    }
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzev.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */