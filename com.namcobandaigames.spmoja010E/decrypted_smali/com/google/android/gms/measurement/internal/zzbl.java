package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver.PendingResult;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement;

final class zzbl
  implements Runnable
{
  zzbl(zzbj paramzzbj, zzbt paramzzbt, long paramLong, Bundle paramBundle, Context paramContext, zzap paramzzap, BroadcastReceiver.PendingResult paramPendingResult) {}
  
  public final void run()
  {
    long l3 = this.zzaoj.zzgp().zzanj.get();
    long l2 = this.zzaol;
    long l1 = l2;
    if (l3 > 0L) {
      if (l2 < l3)
      {
        l1 = l2;
        if (l2 > 0L) {}
      }
      else
      {
        l1 = l3 - 1L;
      }
    }
    if (l1 > 0L) {
      this.zzaom.putLong("click_timestamp", l1);
    }
    this.zzaom.putString("_cis", "referrer broadcast");
    AppMeasurement.getInstance(this.val$context).logEventInternal("auto", "_cmp", this.zzaom);
    this.zzaok.zzjl().zzbx("Install campaign recorded");
    if (this.zzrf != null) {
      this.zzrf.finish();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */