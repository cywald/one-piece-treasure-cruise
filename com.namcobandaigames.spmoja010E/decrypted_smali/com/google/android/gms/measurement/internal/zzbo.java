package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class zzbo
  extends zzcp
{
  private static final AtomicLong zzape = new AtomicLong(Long.MIN_VALUE);
  private ExecutorService zzaea;
  private zzbs zzaov;
  private zzbs zzaow;
  private final PriorityBlockingQueue<zzbr<?>> zzaox = new PriorityBlockingQueue();
  private final BlockingQueue<zzbr<?>> zzaoy = new LinkedBlockingQueue();
  private final Thread.UncaughtExceptionHandler zzaoz = new zzbq(this, "Thread death: Uncaught exception on worker thread");
  private final Thread.UncaughtExceptionHandler zzapa = new zzbq(this, "Thread death: Uncaught exception on network thread");
  private final Object zzapb = new Object();
  private final Semaphore zzapc = new Semaphore(2);
  private volatile boolean zzapd;
  
  zzbo(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  private final void zza(zzbr<?> paramzzbr)
  {
    synchronized (this.zzapb)
    {
      this.zzaox.add(paramzzbr);
      if (this.zzaov == null)
      {
        this.zzaov = new zzbs(this, "Measurement Worker", this.zzaox);
        this.zzaov.setUncaughtExceptionHandler(this.zzaoz);
        this.zzaov.start();
        return;
      }
      this.zzaov.zzke();
    }
  }
  
  final <T> T zza(AtomicReference<T> paramAtomicReference, long paramLong, String paramString, Runnable paramRunnable)
  {
    for (;;)
    {
      try
      {
        zzgn().zzc(paramRunnable);
        try
        {
          paramAtomicReference.wait(15000L);
          paramRunnable = paramAtomicReference.get();
          if (paramRunnable == null)
          {
            zzar localzzar = zzgo().zzjg();
            paramAtomicReference = String.valueOf(paramString);
            if (paramAtomicReference.length() == 0) {
              break label136;
            }
            paramAtomicReference = "Timed out waiting for ".concat(paramAtomicReference);
            localzzar.zzbx(paramAtomicReference);
          }
          return paramRunnable;
        }
        catch (InterruptedException paramRunnable)
        {
          paramRunnable = zzgo().zzjg();
          paramString = String.valueOf(paramString);
          if (paramString.length() == 0) {
            break label122;
          }
        }
        paramString = "Interrupted waiting for ".concat(paramString);
        paramRunnable.zzbx(paramString);
        return null;
      }
      finally {}
      label122:
      paramString = new String("Interrupted waiting for ");
      continue;
      label136:
      paramAtomicReference = new String("Timed out waiting for ");
    }
  }
  
  public final void zzaf()
  {
    if (Thread.currentThread() != this.zzaov) {
      throw new IllegalStateException("Call expected from worker thread");
    }
  }
  
  public final <V> Future<V> zzb(Callable<V> paramCallable)
    throws IllegalStateException
  {
    zzcl();
    Preconditions.checkNotNull(paramCallable);
    paramCallable = new zzbr(this, paramCallable, false, "Task exception on worker thread");
    if (Thread.currentThread() == this.zzaov)
    {
      if (!this.zzaox.isEmpty()) {
        zzgo().zzjg().zzbx("Callable skipped the worker queue.");
      }
      paramCallable.run();
      return paramCallable;
    }
    zza(paramCallable);
    return paramCallable;
  }
  
  public final <V> Future<V> zzc(Callable<V> paramCallable)
    throws IllegalStateException
  {
    zzcl();
    Preconditions.checkNotNull(paramCallable);
    paramCallable = new zzbr(this, paramCallable, true, "Task exception on worker thread");
    if (Thread.currentThread() == this.zzaov)
    {
      paramCallable.run();
      return paramCallable;
    }
    zza(paramCallable);
    return paramCallable;
  }
  
  public final void zzc(Runnable paramRunnable)
    throws IllegalStateException
  {
    zzcl();
    Preconditions.checkNotNull(paramRunnable);
    zza(new zzbr(this, paramRunnable, false, "Task exception on worker thread"));
  }
  
  public final void zzd(Runnable arg1)
    throws IllegalStateException
  {
    zzcl();
    Preconditions.checkNotNull(???);
    zzbr localzzbr = new zzbr(this, ???, false, "Task exception on network thread");
    synchronized (this.zzapb)
    {
      this.zzaoy.add(localzzbr);
      if (this.zzaow == null)
      {
        this.zzaow = new zzbs(this, "Measurement Network", this.zzaoy);
        this.zzaow.setUncaughtExceptionHandler(this.zzapa);
        this.zzaow.start();
        return;
      }
      this.zzaow.zzke();
    }
  }
  
  public final void zzgc()
  {
    if (Thread.currentThread() != this.zzaow) {
      throw new IllegalStateException("Call expected from network thread");
    }
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  public final boolean zzkb()
  {
    return Thread.currentThread() == this.zzaov;
  }
  
  public final ExecutorService zzkc()
  {
    synchronized (this.zzapb)
    {
      if (this.zzaea == null) {
        this.zzaea = new ThreadPoolExecutor(0, 1, 30L, TimeUnit.SECONDS, new ArrayBlockingQueue(100));
      }
      ExecutorService localExecutorService = this.zzaea;
      return localExecutorService;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */