package com.google.android.gms.measurement.internal;

final class zzdk
  implements Runnable
{
  zzdk(zzcs paramzzcs, long paramLong) {}
  
  public final void run()
  {
    this.zzarc.zzgp().zzanp.set(this.zzark);
    this.zzarc.zzgo().zzjk().zzg("Minimum session duration set", Long.valueOf(this.zzark));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */