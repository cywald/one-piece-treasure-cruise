package com.google.android.gms.measurement.internal;

import android.support.annotation.GuardedBy;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.measurement.AppMeasurement;

public final class zzap
  extends zzcp
{
  private long zzadt = -1L;
  private char zzalw = '\000';
  @GuardedBy("this")
  private String zzalx;
  private final zzar zzaly = new zzar(this, 6, false, false);
  private final zzar zzalz = new zzar(this, 6, true, false);
  private final zzar zzama = new zzar(this, 6, false, true);
  private final zzar zzamb = new zzar(this, 5, false, false);
  private final zzar zzamc = new zzar(this, 5, true, false);
  private final zzar zzamd = new zzar(this, 5, false, true);
  private final zzar zzame = new zzar(this, 4, false, false);
  private final zzar zzamf = new zzar(this, 3, false, false);
  private final zzar zzamg = new zzar(this, 2, false, false);
  
  zzap(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  @VisibleForTesting
  private static String zza(boolean paramBoolean, Object paramObject)
  {
    if (paramObject == null) {
      return "";
    }
    if ((paramObject instanceof Integer)) {
      paramObject = Long.valueOf(((Integer)paramObject).intValue());
    }
    for (;;)
    {
      String str1;
      if ((paramObject instanceof Long))
      {
        if (!paramBoolean) {
          return String.valueOf(paramObject);
        }
        if (Math.abs(((Long)paramObject).longValue()) < 100L) {
          return String.valueOf(paramObject);
        }
        if (String.valueOf(paramObject).charAt(0) == '-') {}
        for (str1 = "-";; str1 = "")
        {
          paramObject = String.valueOf(Math.abs(((Long)paramObject).longValue()));
          long l1 = Math.round(Math.pow(10.0D, ((String)paramObject).length() - 1));
          long l2 = Math.round(Math.pow(10.0D, ((String)paramObject).length()) - 1.0D);
          return String.valueOf(str1).length() + 43 + String.valueOf(str1).length() + str1 + l1 + "..." + str1 + l2;
        }
      }
      if ((paramObject instanceof Boolean)) {
        return String.valueOf(paramObject);
      }
      if ((paramObject instanceof Throwable))
      {
        Object localObject1 = (Throwable)paramObject;
        String str2;
        int j;
        int i;
        if (paramBoolean)
        {
          paramObject = localObject1.getClass().getName();
          paramObject = new StringBuilder((String)paramObject);
          str1 = zzbw(AppMeasurement.class.getCanonicalName());
          str2 = zzbw(zzbt.class.getCanonicalName());
          localObject1 = ((Throwable)localObject1).getStackTrace();
          j = localObject1.length;
          i = 0;
        }
        for (;;)
        {
          if (i < j)
          {
            Object localObject2 = localObject1[i];
            if (!((StackTraceElement)localObject2).isNativeMethod())
            {
              String str3 = ((StackTraceElement)localObject2).getClassName();
              if (str3 != null)
              {
                str3 = zzbw(str3);
                if ((str3.equals(str1)) || (str3.equals(str2)))
                {
                  ((StringBuilder)paramObject).append(": ");
                  ((StringBuilder)paramObject).append(localObject2);
                }
              }
            }
          }
          else
          {
            return ((StringBuilder)paramObject).toString();
            paramObject = ((Throwable)localObject1).toString();
            break;
          }
          i += 1;
        }
      }
      if ((paramObject instanceof zzas)) {
        return zzas.zza((zzas)paramObject);
      }
      if (paramBoolean) {
        return "-";
      }
      return String.valueOf(paramObject);
    }
  }
  
  static String zza(boolean paramBoolean, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    String str1 = paramString;
    if (paramString == null) {
      str1 = "";
    }
    String str2 = zza(paramBoolean, paramObject1);
    paramObject2 = zza(paramBoolean, paramObject2);
    paramObject3 = zza(paramBoolean, paramObject3);
    StringBuilder localStringBuilder = new StringBuilder();
    paramString = "";
    if (!TextUtils.isEmpty(str1))
    {
      localStringBuilder.append(str1);
      paramString = ": ";
    }
    paramObject1 = paramString;
    if (!TextUtils.isEmpty(str2))
    {
      localStringBuilder.append(paramString);
      localStringBuilder.append(str2);
      paramObject1 = ", ";
    }
    paramString = (String)paramObject1;
    if (!TextUtils.isEmpty((CharSequence)paramObject2))
    {
      localStringBuilder.append((String)paramObject1);
      localStringBuilder.append((String)paramObject2);
      paramString = ", ";
    }
    if (!TextUtils.isEmpty((CharSequence)paramObject3))
    {
      localStringBuilder.append(paramString);
      localStringBuilder.append((String)paramObject3);
    }
    return localStringBuilder.toString();
  }
  
  protected static Object zzbv(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return new zzas(paramString);
  }
  
  private static String zzbw(String paramString)
  {
    String str;
    if (TextUtils.isEmpty(paramString)) {
      str = "";
    }
    int i;
    do
    {
      return str;
      i = paramString.lastIndexOf('.');
      str = paramString;
    } while (i == -1);
    return paramString.substring(0, i);
  }
  
  /* Error */
  @VisibleForTesting
  private final String zzjm()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 222	com/google/android/gms/measurement/internal/zzap:zzalx	Ljava/lang/String;
    //   6: ifnonnull +24 -> 30
    //   9: aload_0
    //   10: getfield 226	com/google/android/gms/measurement/internal/zzap:zzadj	Lcom/google/android/gms/measurement/internal/zzbt;
    //   13: invokevirtual 229	com/google/android/gms/measurement/internal/zzbt:zzkm	()Ljava/lang/String;
    //   16: ifnull +23 -> 39
    //   19: aload_0
    //   20: aload_0
    //   21: getfield 226	com/google/android/gms/measurement/internal/zzap:zzadj	Lcom/google/android/gms/measurement/internal/zzbt;
    //   24: invokevirtual 229	com/google/android/gms/measurement/internal/zzbt:zzkm	()Ljava/lang/String;
    //   27: putfield 222	com/google/android/gms/measurement/internal/zzap:zzalx	Ljava/lang/String;
    //   30: aload_0
    //   31: getfield 222	com/google/android/gms/measurement/internal/zzap:zzalx	Ljava/lang/String;
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: areturn
    //   39: aload_0
    //   40: invokestatic 234	com/google/android/gms/measurement/internal/zzn:zzht	()Ljava/lang/String;
    //   43: putfield 222	com/google/android/gms/measurement/internal/zzap:zzalx	Ljava/lang/String;
    //   46: goto -16 -> 30
    //   49: astore_1
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_1
    //   53: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	54	0	this	zzap
    //   34	4	1	str	String
    //   49	4	1	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	30	49	finally
    //   30	37	49	finally
    //   39	46	49	finally
    //   50	52	49	finally
  }
  
  @VisibleForTesting
  protected final boolean isLoggable(int paramInt)
  {
    return Log.isLoggable(zzjm(), paramInt);
  }
  
  @VisibleForTesting
  protected final void zza(int paramInt, String paramString)
  {
    Log.println(paramInt, zzjm(), paramString);
  }
  
  protected final void zza(int paramInt, boolean paramBoolean1, boolean paramBoolean2, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    int i = 0;
    if ((!paramBoolean1) && (isLoggable(paramInt))) {
      zza(paramInt, zza(false, paramString, paramObject1, paramObject2, paramObject3));
    }
    zzbo localzzbo;
    if ((!paramBoolean2) && (paramInt >= 5))
    {
      Preconditions.checkNotNull(paramString);
      localzzbo = this.zzadj.zzkh();
      if (localzzbo == null) {
        zza(6, "Scheduler not set. Not logging error/warn");
      }
    }
    else
    {
      return;
    }
    if (!localzzbo.isInitialized())
    {
      zza(6, "Scheduler not initialized. Not logging error/warn");
      return;
    }
    if (paramInt < 0) {
      paramInt = i;
    }
    for (;;)
    {
      i = paramInt;
      if (paramInt >= 9) {
        i = 8;
      }
      localzzbo.zzc(new zzaq(this, i, paramString, paramObject1, paramObject2, paramObject3));
      return;
    }
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  public final zzar zzjd()
  {
    return this.zzaly;
  }
  
  public final zzar zzje()
  {
    return this.zzalz;
  }
  
  public final zzar zzjf()
  {
    return this.zzama;
  }
  
  public final zzar zzjg()
  {
    return this.zzamb;
  }
  
  public final zzar zzjh()
  {
    return this.zzamc;
  }
  
  public final zzar zzji()
  {
    return this.zzamd;
  }
  
  public final zzar zzjj()
  {
    return this.zzame;
  }
  
  public final zzar zzjk()
  {
    return this.zzamf;
  }
  
  public final zzar zzjl()
  {
    return this.zzamg;
  }
  
  public final String zzjn()
  {
    Object localObject = zzgp().zzand.zzfm();
    if ((localObject == null) || (localObject == zzba.zzanc)) {
      return null;
    }
    String str = String.valueOf(((Pair)localObject).second);
    localObject = (String)((Pair)localObject).first;
    return String.valueOf(str).length() + 1 + String.valueOf(localObject).length() + str + ":" + (String)localObject;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */