package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import java.util.Iterator;
import java.util.Set;

final class zzab
  implements Iterator<String>
{
  private Iterator<String> zzain = zzaa.zza(this.zzaio).keySet().iterator();
  
  zzab(zzaa paramzzaa) {}
  
  public final boolean hasNext()
  {
    return this.zzain.hasNext();
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Remove not supported");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */