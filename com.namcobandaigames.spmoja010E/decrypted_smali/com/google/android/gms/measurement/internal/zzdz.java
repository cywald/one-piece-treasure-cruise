package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

final class zzdz
  implements Runnable
{
  zzdz(zzdr paramzzdr, boolean paramBoolean1, boolean paramBoolean2, zzad paramzzad, zzh paramzzh, String paramString) {}
  
  public final void run()
  {
    zzag localzzag = zzdr.zzd(this.zzasg);
    if (localzzag == null)
    {
      this.zzasg.zzgo().zzjd().zzbx("Discarding data. Failed to send event to service");
      return;
    }
    Object localObject;
    if (this.zzasi)
    {
      zzdr localzzdr = this.zzasg;
      if (this.zzasj)
      {
        localObject = null;
        localzzdr.zza(localzzag, (AbstractSafeParcelable)localObject, this.zzaqn);
      }
    }
    for (;;)
    {
      zzdr.zze(this.zzasg);
      return;
      localObject = this.zzaqr;
      break;
      try
      {
        if (!TextUtils.isEmpty(this.zzaqq)) {
          break label122;
        }
        localzzag.zza(this.zzaqr, this.zzaqn);
      }
      catch (RemoteException localRemoteException)
      {
        this.zzasg.zzgo().zzjd().zzg("Failed to send event to the service", localRemoteException);
      }
      continue;
      label122:
      localzzag.zza(this.zzaqr, this.zzaqq, this.zzasg.zzgo().zzjn());
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */