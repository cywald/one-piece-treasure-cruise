package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.Preconditions;

public final class zzbc
{
  private boolean value;
  private final boolean zzanw;
  private boolean zzanx;
  private final String zzoj;
  
  public zzbc(zzba paramzzba, String paramString, boolean paramBoolean)
  {
    Preconditions.checkNotEmpty(paramString);
    this.zzoj = paramString;
    this.zzanw = true;
  }
  
  @WorkerThread
  public final boolean get()
  {
    if (!this.zzanx)
    {
      this.zzanx = true;
      this.value = zzba.zza(this.zzany).getBoolean(this.zzoj, this.zzanw);
    }
    return this.value;
  }
  
  @WorkerThread
  public final void set(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = zzba.zza(this.zzany).edit();
    localEditor.putBoolean(this.zzoj, paramBoolean);
    localEditor.apply();
    this.value = paramBoolean;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */