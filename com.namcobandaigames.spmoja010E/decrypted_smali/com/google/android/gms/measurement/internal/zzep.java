package com.google.android.gms.measurement.internal;

import android.app.job.JobParameters;
import android.content.Intent;

public abstract interface zzep
{
  public abstract boolean callServiceStopSelfResult(int paramInt);
  
  public abstract void zza(JobParameters paramJobParameters, boolean paramBoolean);
  
  public abstract void zzb(Intent paramIntent);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzep.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */