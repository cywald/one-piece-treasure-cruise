package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;

class zzay
  extends BroadcastReceiver
{
  @VisibleForTesting
  private static final String zzabi = zzay.class.getName();
  private boolean zzabj;
  private boolean zzabk;
  private final zzfa zzamz;
  
  zzay(zzfa paramzzfa)
  {
    Preconditions.checkNotNull(paramzzfa);
    this.zzamz = paramzzfa;
  }
  
  @MainThread
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    this.zzamz.zzlr();
    paramContext = paramIntent.getAction();
    this.zzamz.zzgo().zzjl().zzg("NetworkBroadcastReceiver received action", paramContext);
    if ("android.net.conn.CONNECTIVITY_CHANGE".equals(paramContext))
    {
      boolean bool = this.zzamz.zzlo().zzfb();
      if (this.zzabk != bool)
      {
        this.zzabk = bool;
        this.zzamz.zzgn().zzc(new zzaz(this, bool));
      }
      return;
    }
    this.zzamz.zzgo().zzjg().zzg("NetworkBroadcastReceiver received unknown action", paramContext);
  }
  
  @WorkerThread
  public final void unregister()
  {
    this.zzamz.zzlr();
    this.zzamz.zzgn().zzaf();
    this.zzamz.zzgn().zzaf();
    if (!this.zzabj) {
      return;
    }
    this.zzamz.zzgo().zzjl().zzbx("Unregistering connectivity change receiver");
    this.zzabj = false;
    this.zzabk = false;
    Context localContext = this.zzamz.getContext();
    try
    {
      localContext.unregisterReceiver(this);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      this.zzamz.zzgo().zzjd().zzg("Failed to unregister the network broadcast receiver", localIllegalArgumentException);
    }
  }
  
  @WorkerThread
  public final void zzey()
  {
    this.zzamz.zzlr();
    this.zzamz.zzgn().zzaf();
    if (this.zzabj) {
      return;
    }
    this.zzamz.getContext().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    this.zzabk = this.zzamz.zzlo().zzfb();
    this.zzamz.zzgo().zzjl().zzg("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.zzabk));
    this.zzabj = true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */