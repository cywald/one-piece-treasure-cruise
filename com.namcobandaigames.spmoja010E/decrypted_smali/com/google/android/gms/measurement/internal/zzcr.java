package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzcr
{
  boolean zzadv = true;
  String zzadx;
  String zzapm;
  String zzapn;
  Boolean zzaqg;
  final Context zzri;
  
  @VisibleForTesting
  public zzcr(Context paramContext, zzak paramzzak)
  {
    Preconditions.checkNotNull(paramContext);
    paramContext = paramContext.getApplicationContext();
    Preconditions.checkNotNull(paramContext);
    this.zzri = paramContext;
    if (paramzzak != null)
    {
      this.zzadx = paramzzak.zzadx;
      this.zzapm = paramzzak.origin;
      this.zzapn = paramzzak.zzadw;
      this.zzadv = paramzzak.zzadv;
      if (paramzzak.zzady != null) {
        this.zzaqg = Boolean.valueOf(paramzzak.zzady.getBoolean("dataCollectionDefaultEnabled", true));
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzcr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */