package com.google.android.gms.measurement.internal;

abstract class zzez
  extends zzey
{
  private boolean zzvz;
  
  zzez(zzfa paramzzfa)
  {
    super(paramzzfa);
    this.zzamz.zzb(this);
  }
  
  final boolean isInitialized()
  {
    return this.zzvz;
  }
  
  protected final void zzcl()
  {
    if (!isInitialized()) {
      throw new IllegalStateException("Not initialized");
    }
  }
  
  protected abstract boolean zzgt();
  
  public final void zzq()
  {
    if (this.zzvz) {
      throw new IllegalStateException("Can't initialize twice");
    }
    zzgt();
    this.zzamz.zzma();
    this.zzvz = true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzez.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */