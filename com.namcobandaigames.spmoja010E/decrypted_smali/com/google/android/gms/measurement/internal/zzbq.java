package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;

final class zzbq
  implements Thread.UncaughtExceptionHandler
{
  private final String zzapf;
  
  public zzbq(zzbo paramzzbo, String paramString)
  {
    Preconditions.checkNotNull(paramString);
    this.zzapf = paramString;
  }
  
  public final void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    try
    {
      this.zzapg.zzgo().zzjd().zzg(this.zzapf, paramThrowable);
      return;
    }
    finally
    {
      paramThread = finally;
      throw paramThread;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */