package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks;
import com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzef
  implements ServiceConnection, BaseGmsClient.BaseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener
{
  private volatile boolean zzasm;
  private volatile zzao zzasn;
  
  protected zzef(zzdr paramzzdr) {}
  
  /* Error */
  @MainThread
  public final void onConnected(@android.support.annotation.Nullable android.os.Bundle paramBundle)
  {
    // Byte code:
    //   0: ldc 39
    //   2: invokestatic 45	com/google/android/gms/common/internal/Preconditions:checkMainThread	(Ljava/lang/String;)V
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 47	com/google/android/gms/measurement/internal/zzef:zzasn	Lcom/google/android/gms/measurement/internal/zzao;
    //   11: invokevirtual 53	com/google/android/gms/measurement/internal/zzao:getService	()Landroid/os/IInterface;
    //   14: checkcast 55	com/google/android/gms/measurement/internal/zzag
    //   17: astore_1
    //   18: invokestatic 61	com/google/android/gms/measurement/internal/zzn:zzia	()Z
    //   21: ifne +8 -> 29
    //   24: aload_0
    //   25: aconst_null
    //   26: putfield 47	com/google/android/gms/measurement/internal/zzef:zzasn	Lcom/google/android/gms/measurement/internal/zzao;
    //   29: aload_0
    //   30: getfield 21	com/google/android/gms/measurement/internal/zzef:zzasg	Lcom/google/android/gms/measurement/internal/zzdr;
    //   33: invokevirtual 67	com/google/android/gms/measurement/internal/zzco:zzgn	()Lcom/google/android/gms/measurement/internal/zzbo;
    //   36: new 69	com/google/android/gms/measurement/internal/zzei
    //   39: dup
    //   40: aload_0
    //   41: aload_1
    //   42: invokespecial 72	com/google/android/gms/measurement/internal/zzei:<init>	(Lcom/google/android/gms/measurement/internal/zzef;Lcom/google/android/gms/measurement/internal/zzag;)V
    //   45: invokevirtual 78	com/google/android/gms/measurement/internal/zzbo:zzc	(Ljava/lang/Runnable;)V
    //   48: aload_0
    //   49: monitorexit
    //   50: return
    //   51: aload_0
    //   52: aconst_null
    //   53: putfield 47	com/google/android/gms/measurement/internal/zzef:zzasn	Lcom/google/android/gms/measurement/internal/zzao;
    //   56: aload_0
    //   57: iconst_0
    //   58: putfield 29	com/google/android/gms/measurement/internal/zzef:zzasm	Z
    //   61: goto -13 -> 48
    //   64: astore_1
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_1
    //   68: athrow
    //   69: astore_1
    //   70: goto -19 -> 51
    //   73: astore_1
    //   74: goto -23 -> 51
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	77	0	this	zzef
    //   0	77	1	paramBundle	android.os.Bundle
    // Exception table:
    //   from	to	target	type
    //   7	29	64	finally
    //   29	48	64	finally
    //   48	50	64	finally
    //   51	61	64	finally
    //   65	67	64	finally
    //   7	29	69	android/os/DeadObjectException
    //   29	48	69	android/os/DeadObjectException
    //   7	29	73	java/lang/IllegalStateException
    //   29	48	73	java/lang/IllegalStateException
  }
  
  @MainThread
  public final void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
  {
    Preconditions.checkMainThread("MeasurementServiceConnection.onConnectionFailed");
    zzap localzzap = this.zzasg.zzadj.zzkf();
    if (localzzap != null) {
      localzzap.zzjg().zzg("Service connection failed", paramConnectionResult);
    }
    try
    {
      this.zzasm = false;
      this.zzasn = null;
      this.zzasg.zzgn().zzc(new zzek(this));
      return;
    }
    finally {}
  }
  
  @MainThread
  public final void onConnectionSuspended(int paramInt)
  {
    Preconditions.checkMainThread("MeasurementServiceConnection.onConnectionSuspended");
    this.zzasg.zzgo().zzjk().zzbx("Service connection suspended");
    this.zzasg.zzgn().zzc(new zzej(this));
  }
  
  /* Error */
  @MainThread
  public final void onServiceConnected(ComponentName paramComponentName, android.os.IBinder paramIBinder)
  {
    // Byte code:
    //   0: ldc -114
    //   2: invokestatic 45	com/google/android/gms/common/internal/Preconditions:checkMainThread	(Ljava/lang/String;)V
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_2
    //   8: ifnonnull +26 -> 34
    //   11: aload_0
    //   12: iconst_0
    //   13: putfield 29	com/google/android/gms/measurement/internal/zzef:zzasm	Z
    //   16: aload_0
    //   17: getfield 21	com/google/android/gms/measurement/internal/zzef:zzasg	Lcom/google/android/gms/measurement/internal/zzdr;
    //   20: invokevirtual 123	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   23: invokevirtual 145	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   26: ldc -109
    //   28: invokevirtual 131	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   31: aload_0
    //   32: monitorexit
    //   33: return
    //   34: aload_2
    //   35: invokeinterface 153 1 0
    //   40: astore_1
    //   41: ldc -101
    //   43: aload_1
    //   44: invokevirtual 161	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   47: istore_3
    //   48: iload_3
    //   49: ifeq +118 -> 167
    //   52: aload_2
    //   53: ifnonnull +57 -> 110
    //   56: aconst_null
    //   57: astore_1
    //   58: aload_0
    //   59: getfield 21	com/google/android/gms/measurement/internal/zzef:zzasg	Lcom/google/android/gms/measurement/internal/zzdr;
    //   62: invokevirtual 123	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   65: invokevirtual 164	com/google/android/gms/measurement/internal/zzap:zzjl	()Lcom/google/android/gms/measurement/internal/zzar;
    //   68: ldc -90
    //   70: invokevirtual 131	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   73: aload_1
    //   74: ifnonnull +114 -> 188
    //   77: aload_0
    //   78: iconst_0
    //   79: putfield 29	com/google/android/gms/measurement/internal/zzef:zzasm	Z
    //   82: invokestatic 172	com/google/android/gms/common/stats/ConnectionTracker:getInstance	()Lcom/google/android/gms/common/stats/ConnectionTracker;
    //   85: aload_0
    //   86: getfield 21	com/google/android/gms/measurement/internal/zzef:zzasg	Lcom/google/android/gms/measurement/internal/zzdr;
    //   89: invokevirtual 176	com/google/android/gms/measurement/internal/zzco:getContext	()Landroid/content/Context;
    //   92: aload_0
    //   93: getfield 21	com/google/android/gms/measurement/internal/zzef:zzasg	Lcom/google/android/gms/measurement/internal/zzdr;
    //   96: invokestatic 179	com/google/android/gms/measurement/internal/zzdr:zza	(Lcom/google/android/gms/measurement/internal/zzdr;)Lcom/google/android/gms/measurement/internal/zzef;
    //   99: invokevirtual 183	com/google/android/gms/common/stats/ConnectionTracker:unbindService	(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    //   102: aload_0
    //   103: monitorexit
    //   104: return
    //   105: astore_1
    //   106: aload_0
    //   107: monitorexit
    //   108: aload_1
    //   109: athrow
    //   110: aload_2
    //   111: ldc -101
    //   113: invokeinterface 187 2 0
    //   118: astore_1
    //   119: aload_1
    //   120: instanceof 55
    //   123: ifeq +11 -> 134
    //   126: aload_1
    //   127: checkcast 55	com/google/android/gms/measurement/internal/zzag
    //   130: astore_1
    //   131: goto -73 -> 58
    //   134: new 189	com/google/android/gms/measurement/internal/zzai
    //   137: dup
    //   138: aload_2
    //   139: invokespecial 192	com/google/android/gms/measurement/internal/zzai:<init>	(Landroid/os/IBinder;)V
    //   142: astore_1
    //   143: goto -85 -> 58
    //   146: astore_1
    //   147: aconst_null
    //   148: astore_1
    //   149: aload_0
    //   150: getfield 21	com/google/android/gms/measurement/internal/zzef:zzasg	Lcom/google/android/gms/measurement/internal/zzdr;
    //   153: invokevirtual 123	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   156: invokevirtual 145	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   159: ldc -62
    //   161: invokevirtual 131	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   164: goto -91 -> 73
    //   167: aload_0
    //   168: getfield 21	com/google/android/gms/measurement/internal/zzef:zzasg	Lcom/google/android/gms/measurement/internal/zzdr;
    //   171: invokevirtual 123	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   174: invokevirtual 145	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   177: ldc -60
    //   179: aload_1
    //   180: invokevirtual 111	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   183: aconst_null
    //   184: astore_1
    //   185: goto -112 -> 73
    //   188: aload_0
    //   189: getfield 21	com/google/android/gms/measurement/internal/zzef:zzasg	Lcom/google/android/gms/measurement/internal/zzdr;
    //   192: invokevirtual 67	com/google/android/gms/measurement/internal/zzco:zzgn	()Lcom/google/android/gms/measurement/internal/zzbo;
    //   195: new 198	com/google/android/gms/measurement/internal/zzeg
    //   198: dup
    //   199: aload_0
    //   200: aload_1
    //   201: invokespecial 199	com/google/android/gms/measurement/internal/zzeg:<init>	(Lcom/google/android/gms/measurement/internal/zzef;Lcom/google/android/gms/measurement/internal/zzag;)V
    //   204: invokevirtual 78	com/google/android/gms/measurement/internal/zzbo:zzc	(Ljava/lang/Runnable;)V
    //   207: goto -105 -> 102
    //   210: astore_1
    //   211: goto -109 -> 102
    //   214: astore_2
    //   215: goto -66 -> 149
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	218	0	this	zzef
    //   0	218	1	paramComponentName	ComponentName
    //   0	218	2	paramIBinder	android.os.IBinder
    //   47	2	3	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   11	33	105	finally
    //   34	48	105	finally
    //   58	73	105	finally
    //   77	82	105	finally
    //   82	102	105	finally
    //   102	104	105	finally
    //   106	108	105	finally
    //   110	131	105	finally
    //   134	143	105	finally
    //   149	164	105	finally
    //   167	183	105	finally
    //   188	207	105	finally
    //   34	48	146	android/os/RemoteException
    //   110	131	146	android/os/RemoteException
    //   134	143	146	android/os/RemoteException
    //   167	183	146	android/os/RemoteException
    //   82	102	210	java/lang/IllegalArgumentException
    //   58	73	214	android/os/RemoteException
  }
  
  @MainThread
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    Preconditions.checkMainThread("MeasurementServiceConnection.onServiceDisconnected");
    this.zzasg.zzgo().zzjk().zzbx("Service disconnected");
    this.zzasg.zzgn().zzc(new zzeh(this, paramComponentName));
  }
  
  @WorkerThread
  public final void zzc(Intent paramIntent)
  {
    this.zzasg.zzaf();
    Context localContext = this.zzasg.getContext();
    ConnectionTracker localConnectionTracker = ConnectionTracker.getInstance();
    try
    {
      if (this.zzasm)
      {
        this.zzasg.zzgo().zzjl().zzbx("Connection attempt already in progress");
        return;
      }
      this.zzasg.zzgo().zzjl().zzbx("Using local app measurement service");
      this.zzasm = true;
      localConnectionTracker.bindService(localContext, paramIntent, zzdr.zza(this.zzasg), 129);
      return;
    }
    finally {}
  }
  
  @WorkerThread
  public final void zzlg()
  {
    if ((this.zzasn != null) && ((this.zzasn.isConnected()) || (this.zzasn.isConnecting()))) {
      this.zzasn.disconnect();
    }
    this.zzasn = null;
  }
  
  @WorkerThread
  public final void zzlh()
  {
    this.zzasg.zzaf();
    Context localContext1 = this.zzasg.getContext();
    try
    {
      if (this.zzasm)
      {
        this.zzasg.zzgo().zzjl().zzbx("Connection attempt already in progress");
        return;
      }
      if ((this.zzasn != null) && ((!zzn.zzia()) || (this.zzasn.isConnecting()) || (this.zzasn.isConnected())))
      {
        this.zzasg.zzgo().zzjl().zzbx("Already awaiting connection attempt");
        return;
      }
    }
    finally {}
    this.zzasn = new zzao(localContext2, Looper.getMainLooper(), this, this);
    this.zzasg.zzgo().zzjl().zzbx("Connecting to remote service");
    this.zzasm = true;
    this.zzasn.checkAvailabilityAndConnect();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */