package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;

class zze
  extends zzco
  implements zzcq
{
  zze(zzbt paramzzbt)
  {
    super(paramzzbt);
    Preconditions.checkNotNull(paramzzbt);
  }
  
  public void zzaf()
  {
    this.zzadj.zzgn().zzaf();
  }
  
  public void zzga()
  {
    this.zzadj.zzga();
  }
  
  public void zzgb()
  {
    this.zzadj.zzgb();
  }
  
  public void zzgc()
  {
    this.zzadj.zzgn().zzgc();
  }
  
  public zza zzgd()
  {
    return this.zzadj.zzgd();
  }
  
  public zzcs zzge()
  {
    return this.zzadj.zzge();
  }
  
  public zzaj zzgf()
  {
    return this.zzadj.zzgf();
  }
  
  public zzdr zzgg()
  {
    return this.zzadj.zzgg();
  }
  
  public zzdo zzgh()
  {
    return this.zzadj.zzgh();
  }
  
  public zzal zzgi()
  {
    return this.zzadj.zzgi();
  }
  
  public zzeq zzgj()
  {
    return this.zzadj.zzgj();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */