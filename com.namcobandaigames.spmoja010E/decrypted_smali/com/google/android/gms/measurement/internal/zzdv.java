package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

final class zzdv
  implements Runnable
{
  zzdv(zzdr paramzzdr, zzh paramzzh) {}
  
  public final void run()
  {
    zzag localzzag = zzdr.zzd(this.zzasg);
    if (localzzag == null)
    {
      this.zzasg.zzgo().zzjd().zzbx("Discarding data. Failed to send app launch");
      return;
    }
    try
    {
      localzzag.zza(this.zzaqn);
      this.zzasg.zza(localzzag, null, this.zzaqn);
      zzdr.zze(this.zzasg);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      this.zzasg.zzgo().zzjd().zzg("Failed to send app launch to the service", localRemoteException);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzdv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */