package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

final class zzed
  implements Runnable
{
  zzed(zzdr paramzzdr, boolean paramBoolean, zzfh paramzzfh, zzh paramzzh) {}
  
  public final void run()
  {
    zzag localzzag = zzdr.zzd(this.zzasg);
    if (localzzag == null)
    {
      this.zzasg.zzgo().zzjd().zzbx("Discarding data. Failed to set user attribute");
      return;
    }
    zzdr localzzdr = this.zzasg;
    if (this.zzasj) {}
    for (Object localObject = null;; localObject = this.zzaqs)
    {
      localzzdr.zza(localzzag, (AbstractSafeParcelable)localObject, this.zzaqn);
      zzdr.zze(this.zzasg);
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */