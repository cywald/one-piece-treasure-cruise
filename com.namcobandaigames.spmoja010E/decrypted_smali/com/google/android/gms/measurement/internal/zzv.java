package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.internal.measurement.zzdx;

abstract class zzv
{
  private static volatile Handler handler;
  private final zzcq zzahw;
  private final Runnable zzyo;
  private volatile long zzyp;
  
  zzv(zzcq paramzzcq)
  {
    Preconditions.checkNotNull(paramzzcq);
    this.zzahw = paramzzcq;
    this.zzyo = new zzw(this, paramzzcq);
  }
  
  private final Handler getHandler()
  {
    if (handler != null) {
      return handler;
    }
    try
    {
      if (handler == null) {
        handler = new zzdx(this.zzahw.getContext().getMainLooper());
      }
      Handler localHandler = handler;
      return localHandler;
    }
    finally {}
  }
  
  final void cancel()
  {
    this.zzyp = 0L;
    getHandler().removeCallbacks(this.zzyo);
  }
  
  public abstract void run();
  
  public final boolean zzej()
  {
    return this.zzyp != 0L;
  }
  
  public final void zzh(long paramLong)
  {
    cancel();
    if (paramLong >= 0L)
    {
      this.zzyp = this.zzahw.zzbx().currentTimeMillis();
      if (!getHandler().postDelayed(this.zzyo, paramLong)) {
        this.zzahw.zzgo().zzjd().zzg("Failed to schedule delayed post. time", Long.valueOf(paramLong));
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */