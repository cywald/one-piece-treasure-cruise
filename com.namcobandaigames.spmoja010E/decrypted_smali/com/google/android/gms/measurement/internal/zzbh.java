package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.MainThread;
import com.google.android.gms.internal.measurement.zzv;

public final class zzbh
  implements ServiceConnection
{
  private final String packageName;
  
  zzbh(zzbg paramzzbg, String paramString)
  {
    this.packageName = paramString;
  }
  
  @MainThread
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    if (paramIBinder == null)
    {
      this.zzaoe.zzadj.zzgo().zzjg().zzbx("Install Referrer connection returned with null binder");
      return;
    }
    try
    {
      paramComponentName = zzv.zza(paramIBinder);
      if (paramComponentName == null)
      {
        this.zzaoe.zzadj.zzgo().zzjg().zzbx("Install Referrer Service implementation was not found");
        return;
      }
    }
    catch (Exception paramComponentName)
    {
      this.zzaoe.zzadj.zzgo().zzjg().zzg("Exception occurred while calling Install Referrer API", paramComponentName);
      return;
    }
    this.zzaoe.zzadj.zzgo().zzjj().zzbx("Install Referrer Service connected");
    this.zzaoe.zzadj.zzgn().zzc(new zzbi(this, paramComponentName, this));
  }
  
  @MainThread
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    this.zzaoe.zzadj.zzgo().zzjj().zzbx("Install Referrer Service disconnected");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */