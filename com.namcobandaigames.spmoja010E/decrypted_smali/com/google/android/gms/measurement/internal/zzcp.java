package com.google.android.gms.measurement.internal;

abstract class zzcp
  extends zzco
{
  private boolean zzvz;
  
  zzcp(zzbt paramzzbt)
  {
    super(paramzzbt);
    this.zzadj.zzb(this);
  }
  
  final boolean isInitialized()
  {
    return this.zzvz;
  }
  
  protected final void zzcl()
  {
    if (!isInitialized()) {
      throw new IllegalStateException("Not initialized");
    }
  }
  
  public final void zzgs()
  {
    if (this.zzvz) {
      throw new IllegalStateException("Can't initialize twice");
    }
    zzgu();
    this.zzadj.zzkq();
    this.zzvz = true;
  }
  
  protected abstract boolean zzgt();
  
  protected void zzgu() {}
  
  public final void zzq()
  {
    if (this.zzvz) {
      throw new IllegalStateException("Can't initialize twice");
    }
    if (!zzgt())
    {
      this.zzadj.zzkq();
      this.zzvz = true;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzcp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */