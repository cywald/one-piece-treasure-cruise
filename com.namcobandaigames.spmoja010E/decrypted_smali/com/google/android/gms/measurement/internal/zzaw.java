package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import java.util.Map;

@WorkerThread
final class zzaw
  implements Runnable
{
  private final String packageName;
  private final int status;
  private final zzav zzamr;
  private final Throwable zzams;
  private final byte[] zzamt;
  private final Map<String, List<String>> zzamu;
  
  private zzaw(String paramString, zzav paramzzav, int paramInt, Throwable paramThrowable, byte[] paramArrayOfByte, Map<String, List<String>> paramMap)
  {
    Preconditions.checkNotNull(paramzzav);
    this.zzamr = paramzzav;
    this.status = paramInt;
    this.zzams = paramThrowable;
    this.zzamt = paramArrayOfByte;
    this.packageName = paramString;
    this.zzamu = paramMap;
  }
  
  public final void run()
  {
    this.zzamr.zza(this.packageName, this.status, this.zzams, this.zzamt, this.zzamu);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzaw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */