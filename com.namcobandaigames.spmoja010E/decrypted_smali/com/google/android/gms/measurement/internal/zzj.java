package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzfv;
import com.google.android.gms.internal.measurement.zzfw;
import com.google.android.gms.internal.measurement.zzfx;
import com.google.android.gms.internal.measurement.zzfy;
import com.google.android.gms.internal.measurement.zzfz;
import com.google.android.gms.internal.measurement.zzgd;
import com.google.android.gms.internal.measurement.zzge;
import com.google.android.gms.internal.measurement.zzgf;
import com.google.android.gms.internal.measurement.zzgg;
import com.google.android.gms.internal.measurement.zzgj;
import com.google.android.gms.internal.measurement.zzgk;
import com.google.android.gms.internal.measurement.zzgl;
import com.google.android.gms.internal.measurement.zzyy;
import com.google.android.gms.internal.measurement.zzzg;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class zzj
  extends zzez
{
  zzj(zzfa paramzzfa)
  {
    super(paramzzfa);
  }
  
  private final Boolean zza(double paramDouble, zzfx paramzzfx)
  {
    try
    {
      paramzzfx = zza(new BigDecimal(paramDouble), paramzzfx, Math.ulp(paramDouble));
      return paramzzfx;
    }
    catch (NumberFormatException paramzzfx) {}
    return null;
  }
  
  private final Boolean zza(long paramLong, zzfx paramzzfx)
  {
    try
    {
      paramzzfx = zza(new BigDecimal(paramLong), paramzzfx, 0.0D);
      return paramzzfx;
    }
    catch (NumberFormatException paramzzfx) {}
    return null;
  }
  
  private final Boolean zza(zzfv paramzzfv, String paramString, zzgg[] paramArrayOfzzgg, long paramLong)
  {
    if (paramzzfv.zzavi != null)
    {
      localObject1 = zza(paramLong, paramzzfv.zzavi);
      if (localObject1 == null) {
        return null;
      }
      if (!((Boolean)localObject1).booleanValue()) {
        return Boolean.valueOf(false);
      }
    }
    Object localObject2 = new HashSet();
    Object localObject1 = paramzzfv.zzavg;
    int j = localObject1.length;
    int i = 0;
    Object localObject3;
    while (i < j)
    {
      localObject3 = localObject1[i];
      if (TextUtils.isEmpty(((zzfw)localObject3).zzavn))
      {
        zzgo().zzjg().zzg("null or empty param name in filter. event", zzgl().zzbs(paramString));
        return null;
      }
      ((Set)localObject2).add(((zzfw)localObject3).zzavn);
      i += 1;
    }
    localObject1 = new ArrayMap();
    j = paramArrayOfzzgg.length;
    i = 0;
    if (i < j)
    {
      localObject3 = paramArrayOfzzgg[i];
      if (((Set)localObject2).contains(((zzgg)localObject3).name))
      {
        if (((zzgg)localObject3).zzawx == null) {
          break label210;
        }
        ((Map)localObject1).put(((zzgg)localObject3).name, ((zzgg)localObject3).zzawx);
      }
      for (;;)
      {
        i += 1;
        break;
        label210:
        if (((zzgg)localObject3).zzauh != null)
        {
          ((Map)localObject1).put(((zzgg)localObject3).name, ((zzgg)localObject3).zzauh);
        }
        else
        {
          if (((zzgg)localObject3).zzamp == null) {
            break label268;
          }
          ((Map)localObject1).put(((zzgg)localObject3).name, ((zzgg)localObject3).zzamp);
        }
      }
      label268:
      zzgo().zzjg().zze("Unknown value for param. event, param", zzgl().zzbs(paramString), zzgl().zzbt(((zzgg)localObject3).name));
      return null;
    }
    paramArrayOfzzgg = paramzzfv.zzavg;
    int k = paramArrayOfzzgg.length;
    i = 0;
    while (i < k)
    {
      paramzzfv = paramArrayOfzzgg[i];
      int m = Boolean.TRUE.equals(paramzzfv.zzavm);
      localObject2 = paramzzfv.zzavn;
      if (TextUtils.isEmpty((CharSequence)localObject2))
      {
        zzgo().zzjg().zzg("Event has empty param name. event", zzgl().zzbs(paramString));
        return null;
      }
      localObject3 = ((Map)localObject1).get(localObject2);
      if ((localObject3 instanceof Long))
      {
        if (paramzzfv.zzavl == null)
        {
          zzgo().zzjg().zze("No number filter for long param. event, param", zzgl().zzbs(paramString), zzgl().zzbt((String)localObject2));
          return null;
        }
        paramzzfv = zza(((Long)localObject3).longValue(), paramzzfv.zzavl);
        if (paramzzfv == null) {
          return null;
        }
        if (!paramzzfv.booleanValue()) {}
        for (j = 1; (j ^ m) != 0; j = 0) {
          return Boolean.valueOf(false);
        }
      }
      if ((localObject3 instanceof Double))
      {
        if (paramzzfv.zzavl == null)
        {
          zzgo().zzjg().zze("No number filter for double param. event, param", zzgl().zzbs(paramString), zzgl().zzbt((String)localObject2));
          return null;
        }
        paramzzfv = zza(((Double)localObject3).doubleValue(), paramzzfv.zzavl);
        if (paramzzfv == null) {
          return null;
        }
        if (!paramzzfv.booleanValue()) {}
        for (j = 1; (j ^ m) != 0; j = 0) {
          return Boolean.valueOf(false);
        }
      }
      if ((localObject3 instanceof String))
      {
        if (paramzzfv.zzavk != null) {
          paramzzfv = zza((String)localObject3, paramzzfv.zzavk);
        }
        while (paramzzfv == null)
        {
          return null;
          if (paramzzfv.zzavl != null)
          {
            if (zzfg.zzcp((String)localObject3))
            {
              paramzzfv = zza((String)localObject3, paramzzfv.zzavl);
            }
            else
            {
              zzgo().zzjg().zze("Invalid param value for number filter. event, param", zzgl().zzbs(paramString), zzgl().zzbt((String)localObject2));
              return null;
            }
          }
          else
          {
            zzgo().zzjg().zze("No filter for String param. event, param", zzgl().zzbs(paramString), zzgl().zzbt((String)localObject2));
            return null;
          }
        }
        if (!paramzzfv.booleanValue()) {}
        for (j = 1; (j ^ m) != 0; j = 0) {
          return Boolean.valueOf(false);
        }
      }
      if (localObject3 == null)
      {
        zzgo().zzjl().zze("Missing param for filter. event, param", zzgl().zzbs(paramString), zzgl().zzbt((String)localObject2));
        return Boolean.valueOf(false);
      }
      zzgo().zzjg().zze("Unknown param type. event, param", zzgl().zzbs(paramString), zzgl().zzbt((String)localObject2));
      return null;
      i += 1;
    }
    return Boolean.valueOf(true);
  }
  
  private final Boolean zza(zzfy paramzzfy, zzgl paramzzgl)
  {
    paramzzfy = paramzzfy.zzavv;
    if (paramzzfy == null)
    {
      zzgo().zzjg().zzg("Missing property filter. property", zzgl().zzbu(paramzzgl.name));
      return null;
    }
    boolean bool = Boolean.TRUE.equals(paramzzfy.zzavm);
    if (paramzzgl.zzawx != null)
    {
      if (paramzzfy.zzavl == null)
      {
        zzgo().zzjg().zzg("No number filter for long property. property", zzgl().zzbu(paramzzgl.name));
        return null;
      }
      return zza(zza(paramzzgl.zzawx.longValue(), paramzzfy.zzavl), bool);
    }
    if (paramzzgl.zzauh != null)
    {
      if (paramzzfy.zzavl == null)
      {
        zzgo().zzjg().zzg("No number filter for double property. property", zzgl().zzbu(paramzzgl.name));
        return null;
      }
      return zza(zza(paramzzgl.zzauh.doubleValue(), paramzzfy.zzavl), bool);
    }
    if (paramzzgl.zzamp != null)
    {
      if (paramzzfy.zzavk == null)
      {
        if (paramzzfy.zzavl == null)
        {
          zzgo().zzjg().zzg("No string or number filter defined. property", zzgl().zzbu(paramzzgl.name));
          return null;
        }
        if (zzfg.zzcp(paramzzgl.zzamp)) {
          return zza(zza(paramzzgl.zzamp, paramzzfy.zzavl), bool);
        }
        zzgo().zzjg().zze("Invalid user property value for Numeric number filter. property, value", zzgl().zzbu(paramzzgl.name), paramzzgl.zzamp);
        return null;
      }
      return zza(zza(paramzzgl.zzamp, paramzzfy.zzavk), bool);
    }
    zzgo().zzjg().zzg("User property has no value, property", zzgl().zzbu(paramzzgl.name));
    return null;
  }
  
  @VisibleForTesting
  private static Boolean zza(Boolean paramBoolean, boolean paramBoolean1)
  {
    if (paramBoolean == null) {
      return null;
    }
    return Boolean.valueOf(paramBoolean.booleanValue() ^ paramBoolean1);
  }
  
  private final Boolean zza(String paramString1, int paramInt, boolean paramBoolean, String paramString2, List<String> paramList, String paramString3)
  {
    if (paramString1 == null) {
      return null;
    }
    if (paramInt == 6)
    {
      if ((paramList == null) || (paramList.size() == 0)) {
        return null;
      }
    }
    else if (paramString2 == null) {
      return null;
    }
    String str = paramString1;
    if (!paramBoolean) {
      if (paramInt != 1) {
        break label94;
      }
    }
    label94:
    for (str = paramString1;; str = paramString1.toUpperCase(Locale.ENGLISH)) {
      switch (paramInt)
      {
      default: 
        return null;
      }
    }
    if (paramBoolean) {}
    for (paramInt = 0;; paramInt = 66) {
      try
      {
        paramBoolean = Pattern.compile(paramString3, paramInt).matcher(str).matches();
        return Boolean.valueOf(paramBoolean);
      }
      catch (PatternSyntaxException paramString1)
      {
        zzgo().zzjg().zzg("Invalid regular expression in REGEXP audience filter. expression", paramString3);
        return null;
      }
    }
    return Boolean.valueOf(str.startsWith(paramString2));
    return Boolean.valueOf(str.endsWith(paramString2));
    return Boolean.valueOf(str.contains(paramString2));
    return Boolean.valueOf(str.equals(paramString2));
    return Boolean.valueOf(paramList.contains(str));
  }
  
  private final Boolean zza(String paramString, zzfx paramzzfx)
  {
    if (!zzfg.zzcp(paramString)) {
      return null;
    }
    try
    {
      paramString = zza(new BigDecimal(paramString), paramzzfx, 0.0D);
      return paramString;
    }
    catch (NumberFormatException paramString) {}
    return null;
  }
  
  @VisibleForTesting
  private final Boolean zza(String paramString, zzfz paramzzfz)
  {
    int i = 0;
    Object localObject = null;
    Preconditions.checkNotNull(paramzzfz);
    if (paramString == null) {}
    do
    {
      do
      {
        return null;
      } while ((paramzzfz.zzavw == null) || (paramzzfz.zzavw.intValue() == 0));
      if (paramzzfz.zzavw.intValue() != 6) {
        break;
      }
    } while ((paramzzfz.zzavz == null) || (paramzzfz.zzavz.length == 0));
    int j = paramzzfz.zzavw.intValue();
    boolean bool;
    label89:
    String str;
    if ((paramzzfz.zzavy != null) && (paramzzfz.zzavy.booleanValue()))
    {
      bool = true;
      if ((!bool) && (j != 1) && (j != 6)) {
        break label162;
      }
      str = paramzzfz.zzavx;
      label113:
      if (paramzzfz.zzavz != null) {
        break label177;
      }
    }
    label162:
    label177:
    String[] arrayOfString;
    for (paramzzfz = null;; paramzzfz = Arrays.asList(arrayOfString))
    {
      if (j == 1) {
        localObject = str;
      }
      return zza(paramString, j, bool, str, paramzzfz, (String)localObject);
      if (paramzzfz.zzavx != null) {
        break;
      }
      return null;
      bool = false;
      break label89;
      str = paramzzfz.zzavx.toUpperCase(Locale.ENGLISH);
      break label113;
      arrayOfString = paramzzfz.zzavz;
      if (!bool) {
        break label197;
      }
    }
    label197:
    ArrayList localArrayList = new ArrayList();
    int k = arrayOfString.length;
    for (;;)
    {
      paramzzfz = localArrayList;
      if (i >= k) {
        break;
      }
      localArrayList.add(arrayOfString[i].toUpperCase(Locale.ENGLISH));
      i += 1;
    }
  }
  
  @VisibleForTesting
  private static Boolean zza(BigDecimal paramBigDecimal, zzfx paramzzfx, double paramDouble)
  {
    boolean bool2 = true;
    boolean bool3 = true;
    boolean bool4 = true;
    boolean bool5 = true;
    boolean bool1 = true;
    Preconditions.checkNotNull(paramzzfx);
    if ((paramzzfx.zzavo == null) || (paramzzfx.zzavo.intValue() == 0)) {
      return null;
    }
    if (paramzzfx.zzavo.intValue() == 4)
    {
      if ((paramzzfx.zzavr == null) || (paramzzfx.zzavs == null)) {
        return null;
      }
    }
    else if (paramzzfx.zzavq == null) {
      return null;
    }
    int i = paramzzfx.zzavo.intValue();
    if (paramzzfx.zzavo.intValue() == 4) {
      if ((!zzfg.zzcp(paramzzfx.zzavr)) || (!zzfg.zzcp(paramzzfx.zzavs))) {
        return null;
      }
    }
    BigDecimal localBigDecimal1;
    BigDecimal localBigDecimal2;
    for (;;)
    {
      try
      {
        localBigDecimal1 = new BigDecimal(paramzzfx.zzavr);
        paramzzfx = new BigDecimal(paramzzfx.zzavs);
        localBigDecimal2 = null;
        if (i != 4) {
          break;
        }
        if (localBigDecimal1 != null) {
          break label202;
        }
        return null;
      }
      catch (NumberFormatException paramBigDecimal)
      {
        return null;
      }
      if (!zzfg.zzcp(paramzzfx.zzavq)) {
        return null;
      }
      try
      {
        localBigDecimal2 = new BigDecimal(paramzzfx.zzavq);
        localBigDecimal1 = null;
        paramzzfx = null;
      }
      catch (NumberFormatException paramBigDecimal)
      {
        return null;
      }
    }
    if (localBigDecimal2 != null) {}
    switch (i)
    {
    default: 
      return null;
    case 1: 
      if (paramBigDecimal.compareTo(localBigDecimal2) == -1) {}
      for (;;)
      {
        return Boolean.valueOf(bool1);
        bool1 = false;
      }
    case 2: 
      if (paramBigDecimal.compareTo(localBigDecimal2) == 1) {}
      for (bool1 = bool2;; bool1 = false) {
        return Boolean.valueOf(bool1);
      }
    case 3: 
      label202:
      if (paramDouble != 0.0D)
      {
        if ((paramBigDecimal.compareTo(localBigDecimal2.subtract(new BigDecimal(paramDouble).multiply(new BigDecimal(2)))) == 1) && (paramBigDecimal.compareTo(localBigDecimal2.add(new BigDecimal(paramDouble).multiply(new BigDecimal(2)))) == -1)) {}
        for (bool1 = bool3;; bool1 = false) {
          return Boolean.valueOf(bool1);
        }
      }
      if (paramBigDecimal.compareTo(localBigDecimal2) == 0) {}
      for (bool1 = bool4;; bool1 = false) {
        return Boolean.valueOf(bool1);
      }
    }
    if ((paramBigDecimal.compareTo(localBigDecimal1) != -1) && (paramBigDecimal.compareTo(paramzzfx) != 1)) {}
    for (bool1 = bool5;; bool1 = false) {
      return Boolean.valueOf(bool1);
    }
  }
  
  private static void zza(Map<Integer, Long> paramMap, int paramInt, long paramLong)
  {
    Long localLong = (Long)paramMap.get(Integer.valueOf(paramInt));
    paramLong /= 1000L;
    if ((localLong == null) || (paramLong > localLong.longValue())) {
      paramMap.put(Integer.valueOf(paramInt), Long.valueOf(paramLong));
    }
  }
  
  private static void zzb(Map<Integer, List<Long>> paramMap, int paramInt, long paramLong)
  {
    List localList = (List)paramMap.get(Integer.valueOf(paramInt));
    Object localObject = localList;
    if (localList == null)
    {
      localObject = new ArrayList();
      paramMap.put(Integer.valueOf(paramInt), localObject);
    }
    ((List)localObject).add(Long.valueOf(paramLong / 1000L));
  }
  
  private static zzge[] zzd(Map<Integer, Long> paramMap)
  {
    if (paramMap == null) {
      return null;
    }
    zzge[] arrayOfzzge = new zzge[paramMap.size()];
    Iterator localIterator = paramMap.keySet().iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      Integer localInteger = (Integer)localIterator.next();
      zzge localzzge = new zzge();
      localzzge.zzawq = localInteger;
      localzzge.zzawr = ((Long)paramMap.get(localInteger));
      arrayOfzzge[i] = localzzge;
      i += 1;
    }
    return arrayOfzzge;
  }
  
  @WorkerThread
  final zzgd[] zza(String paramString, zzgf[] paramArrayOfzzgf, zzgl[] paramArrayOfzzgl)
  {
    Preconditions.checkNotEmpty(paramString);
    HashSet localHashSet = new HashSet();
    ArrayMap localArrayMap1 = new ArrayMap();
    ArrayMap localArrayMap2 = new ArrayMap();
    ArrayMap localArrayMap3 = new ArrayMap();
    ArrayMap localArrayMap4 = new ArrayMap();
    ArrayMap localArrayMap5 = new ArrayMap();
    boolean bool = zzgq().zzd(paramString, zzaf.zzakw);
    Object localObject5 = zzjq().zzbo(paramString);
    Object localObject6;
    int m;
    Object localObject8;
    Object localObject4;
    Object localObject3;
    Object localObject1;
    if (localObject5 != null)
    {
      localObject6 = ((Map)localObject5).keySet().iterator();
      if (((Iterator)localObject6).hasNext())
      {
        m = ((Integer)((Iterator)localObject6).next()).intValue();
        localObject8 = (zzgj)((Map)localObject5).get(Integer.valueOf(m));
        localObject4 = (BitSet)localArrayMap2.get(Integer.valueOf(m));
        localObject3 = (BitSet)localArrayMap3.get(Integer.valueOf(m));
        if (!bool) {
          break label4154;
        }
        localObject1 = new ArrayMap();
        if ((localObject8 == null) || (((zzgj)localObject8).zzayg == null)) {
          localArrayMap4.put(Integer.valueOf(m), localObject1);
        }
      }
    }
    for (;;)
    {
      Object localObject2 = localObject4;
      if (localObject4 == null)
      {
        localObject2 = new BitSet();
        localArrayMap2.put(Integer.valueOf(m), localObject2);
        localObject3 = new BitSet();
        localArrayMap3.put(Integer.valueOf(m), localObject3);
      }
      int i = 0;
      int k;
      int j;
      Object localObject9;
      for (;;)
      {
        if (i < ((zzgj)localObject8).zzaye.length << 6)
        {
          k = 0;
          j = k;
          if (zzfg.zza(((zzgj)localObject8).zzaye, i))
          {
            zzgo().zzjl().zze("Filter already evaluated. audience ID, filter ID", Integer.valueOf(m), Integer.valueOf(i));
            ((BitSet)localObject3).set(i);
            j = k;
            if (zzfg.zza(((zzgj)localObject8).zzayf, i))
            {
              ((BitSet)localObject2).set(i);
              j = 1;
            }
          }
          if ((localObject1 != null) && (j == 0)) {
            ((Map)localObject1).remove(Integer.valueOf(i));
          }
          i += 1;
          continue;
          localObject2 = ((zzgj)localObject8).zzayg;
          j = localObject2.length;
          i = 0;
          while (i < j)
          {
            localObject9 = localObject2[i];
            if (((zzge)localObject9).zzawq != null) {
              ((Map)localObject1).put(((zzge)localObject9).zzawq, ((zzge)localObject9).zzawr);
            }
            i += 1;
          }
          break;
        }
      }
      localObject4 = new zzgd();
      localArrayMap1.put(Integer.valueOf(m), localObject4);
      ((zzgd)localObject4).zzawo = Boolean.valueOf(false);
      ((zzgd)localObject4).zzawn = ((zzgj)localObject8);
      ((zzgd)localObject4).zzawm = new zzgj();
      ((zzgd)localObject4).zzawm.zzayf = zzfg.zza((BitSet)localObject2);
      ((zzgd)localObject4).zzawm.zzaye = zzfg.zza((BitSet)localObject3);
      if (!bool) {
        break;
      }
      ((zzgd)localObject4).zzawm.zzayg = zzd((Map)localObject1);
      localArrayMap5.put(Integer.valueOf(m), new ArrayMap());
      break;
      long l1;
      ArrayMap localArrayMap6;
      int n;
      Object localObject10;
      if (paramArrayOfzzgf != null)
      {
        localObject2 = null;
        l1 = 0L;
        localObject3 = null;
        localArrayMap6 = new ArrayMap();
        n = paramArrayOfzzgf.length;
        j = 0;
        if (j < n)
        {
          localObject10 = paramArrayOfzzgf[j];
          localObject4 = ((zzgf)localObject10).name;
          localObject1 = ((zzgf)localObject10).zzawt;
          if (!zzgq().zzd(paramString, zzaf.zzakq)) {
            break label4105;
          }
          zzjo();
          localObject5 = (Long)zzfg.zzb((zzgf)localObject10, "_eid");
          if (localObject5 != null)
          {
            i = 1;
            label681:
            if ((i == 0) || (!((String)localObject4).equals("_ep"))) {
              break label761;
            }
            k = 1;
            label700:
            if (k == 0) {
              break label1411;
            }
            zzjo();
            localObject4 = (String)zzfg.zzb((zzgf)localObject10, "_en");
            if (!TextUtils.isEmpty((CharSequence)localObject4)) {
              break label767;
            }
            zzgo().zzjd().zzg("Extra parameter without an event name. eventId", localObject5);
          }
          for (;;)
          {
            j += 1;
            break;
            i = 0;
            break label681;
            label761:
            k = 0;
            break label700;
            label767:
            if ((localObject2 != null) && (localObject3 != null) && (((Long)localObject5).longValue() == ((Long)localObject3).longValue())) {
              break label4120;
            }
            localObject6 = zzjq().zza(paramString, (Long)localObject5);
            if ((localObject6 != null) && (((Pair)localObject6).first != null)) {
              break label836;
            }
            zzgo().zzjd().zze("Extra parameter without existing main event. eventName, eventId", localObject4, localObject5);
          }
          label836:
          localObject2 = (zzgf)((Pair)localObject6).first;
          l1 = ((Long)((Pair)localObject6).second).longValue();
          zzjo();
          localObject3 = (Long)zzfg.zzb((zzgf)localObject2, "_eid");
          l1 -= 1L;
          if (l1 <= 0L)
          {
            localObject5 = zzjq();
            ((zzco)localObject5).zzaf();
            ((zzco)localObject5).zzgo().zzjl().zzg("Clearing complex main event info. appId", paramString);
          }
        }
      }
      label1065:
      label1134:
      label1210:
      label1305:
      label1411:
      label1496:
      label1526:
      label1981:
      label2066:
      label2072:
      label2143:
      label2361:
      label2515:
      label3029:
      label3114:
      label3120:
      label3187:
      label3405:
      label3440:
      label3843:
      label4081:
      label4084:
      label4099:
      label4102:
      label4105:
      label4120:
      label4148:
      label4151:
      for (;;)
      {
        try
        {
          ((zzq)localObject5).getWritableDatabase().execSQL("delete from main_event_params where app_id=?", new String[] { paramString });
          localObject5 = new zzgg[((zzgf)localObject2).zzawt.length + localObject1.length];
          i = 0;
          localObject6 = ((zzgf)localObject2).zzawt;
          int i1 = localObject6.length;
          k = 0;
          if (k >= i1) {
            break label1065;
          }
          localObject8 = localObject6[k];
          zzjo();
          if (zzfg.zza((zzgf)localObject10, ((zzgg)localObject8).name) != null) {
            break label4151;
          }
          m = i + 1;
          localObject5[i] = localObject8;
          i = m;
          k += 1;
          continue;
        }
        catch (SQLiteException localSQLiteException)
        {
          ((zzco)localObject5).zzgo().zzjd().zzg("Error clearing complex main event", localSQLiteException);
          continue;
        }
        zzjq().zza(paramString, (Long)localObject5, l1, (zzgf)localObject2);
        continue;
        long l2;
        Object localObject11;
        Object localObject7;
        if (i > 0)
        {
          m = localObject1.length;
          k = 0;
          while (k < m)
          {
            localObject5[i] = localObject1[k];
            k += 1;
            i += 1;
          }
          if (i == localObject5.length)
          {
            localObject1 = localObject5;
            localObject5 = localObject1;
            localObject1 = localObject3;
            localObject3 = localObject5;
            localObject5 = zzjq().zzg(paramString, ((zzgf)localObject10).name);
            if (localObject5 != null) {
              break label1526;
            }
            zzgo().zzjg().zze("Event aggregate wasn't created during raw event logging. appId, event", zzap.zzbv(paramString), zzgl().zzbs((String)localObject4));
            localObject5 = new zzz(paramString, ((zzgf)localObject10).name, 1L, 1L, ((zzgf)localObject10).zzawu.longValue(), 0L, null, null, null, null);
            zzjq().zza((zzz)localObject5);
            l2 = ((zzz)localObject5).zzaie;
            localObject11 = (Map)localArrayMap6.get(localObject4);
            if (localObject11 != null) {
              break label4148;
            }
            localObject7 = zzjq().zzl(paramString, (String)localObject4);
            localObject5 = localObject7;
            if (localObject7 == null) {
              localObject5 = new ArrayMap();
            }
            localArrayMap6.put(localObject4, localObject5);
            localObject11 = localObject5;
          }
        }
        for (;;)
        {
          Iterator localIterator1 = ((Map)localObject11).keySet().iterator();
          for (;;)
          {
            if (localIterator1.hasNext())
            {
              m = ((Integer)localIterator1.next()).intValue();
              if (localHashSet.contains(Integer.valueOf(m)))
              {
                zzgo().zzjl().zzg("Skipping failed audience ID", Integer.valueOf(m));
                continue;
                localObject1 = (zzgg[])Arrays.copyOf((Object[])localObject5, i);
                break;
                zzgo().zzjg().zzg("No unique parameters in main event. eventName", localObject4);
                localObject5 = localObject3;
                localObject3 = localObject1;
                localObject1 = localObject5;
                break label1134;
                if (i == 0) {
                  break label4105;
                }
                zzjo();
                localObject2 = Long.valueOf(0L);
                localObject3 = zzfg.zzb((zzgf)localObject10, "_epc");
                if (localObject3 == null) {}
                for (;;)
                {
                  l1 = ((Long)localObject2).longValue();
                  if (l1 > 0L) {
                    break label1496;
                  }
                  zzgo().zzjg().zzg("Complex event with zero extra param count. eventName", localObject4);
                  localObject2 = localObject10;
                  localObject3 = localObject1;
                  localObject1 = localObject5;
                  break;
                  localObject2 = localObject3;
                }
                zzjq().zza(paramString, (Long)localObject5, l1, (zzgf)localObject10);
                localObject2 = localObject10;
                localObject3 = localObject1;
                localObject1 = localObject5;
                break label1134;
                localObject5 = ((zzz)localObject5).zziu();
                break label1210;
              }
              Object localObject12 = (zzgd)localArrayMap1.get(Integer.valueOf(m));
              localObject9 = (BitSet)localArrayMap2.get(Integer.valueOf(m));
              localObject8 = (BitSet)localArrayMap3.get(Integer.valueOf(m));
              localObject7 = null;
              localObject5 = null;
              if (bool)
              {
                localObject7 = (Map)localArrayMap4.get(Integer.valueOf(m));
                localObject5 = (Map)localArrayMap5.get(Integer.valueOf(m));
              }
              if (localObject12 == null)
              {
                localObject8 = new zzgd();
                localArrayMap1.put(Integer.valueOf(m), localObject8);
                ((zzgd)localObject8).zzawo = Boolean.valueOf(true);
                localObject8 = new BitSet();
                localArrayMap2.put(Integer.valueOf(m), localObject8);
                localObject9 = new BitSet();
                localArrayMap3.put(Integer.valueOf(m), localObject9);
                if (bool)
                {
                  localObject12 = new ArrayMap();
                  localArrayMap4.put(Integer.valueOf(m), localObject12);
                  localObject5 = new ArrayMap();
                  localArrayMap5.put(Integer.valueOf(m), localObject5);
                  localObject7 = localObject9;
                  localObject9 = localObject8;
                  localObject8 = localObject7;
                  localObject7 = localObject12;
                }
              }
              for (;;)
              {
                Iterator localIterator2 = ((List)((Map)localObject11).get(Integer.valueOf(m))).iterator();
                Object localObject13;
                while (localIterator2.hasNext())
                {
                  zzfv localzzfv = (zzfv)localIterator2.next();
                  if (zzgo().isLoggable(2))
                  {
                    zzgo().zzjl().zzd("Evaluating filter. audience, filter, event", Integer.valueOf(m), localzzfv.zzave, zzgl().zzbs(localzzfv.zzavf));
                    zzgo().zzjl().zzg("Filter definition", zzjo().zza(localzzfv));
                  }
                  if ((localzzfv.zzave == null) || (localzzfv.zzave.intValue() > 256))
                  {
                    zzgo().zzjg().zze("Invalid event filter ID. appId, id", zzap.zzbv(paramString), String.valueOf(localzzfv.zzave));
                  }
                  else
                  {
                    zzar localzzar;
                    if (bool)
                    {
                      if ((localzzfv != null) && (localzzfv.zzavb != null) && (localzzfv.zzavb.booleanValue()))
                      {
                        i = 1;
                        if ((localzzfv == null) || (localzzfv.zzavc == null) || (!localzzfv.zzavc.booleanValue())) {
                          break label2066;
                        }
                      }
                      for (k = 1;; k = 0)
                      {
                        if ((!((BitSet)localObject9).get(localzzfv.zzave.intValue())) || (i != 0) || (k != 0)) {
                          break label2072;
                        }
                        zzgo().zzjl().zze("Event filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID", Integer.valueOf(m), localzzfv.zzave);
                        break;
                        i = 0;
                        break label1981;
                      }
                      localObject13 = zza(localzzfv, (String)localObject4, (zzgg[])localObject3, l2);
                      localzzar = zzgo().zzjl();
                      if (localObject13 == null) {}
                      for (localObject12 = "null";; localObject12 = localObject13)
                      {
                        localzzar.zzg("Event filter result", localObject12);
                        if (localObject13 != null) {
                          break label2143;
                        }
                        localHashSet.add(Integer.valueOf(m));
                        break;
                      }
                      ((BitSet)localObject8).set(localzzfv.zzave.intValue());
                      if (((Boolean)localObject13).booleanValue())
                      {
                        ((BitSet)localObject9).set(localzzfv.zzave.intValue());
                        if (((i != 0) || (k != 0)) && (((zzgf)localObject10).zzawu != null)) {
                          if (k != 0) {
                            zzb((Map)localObject5, localzzfv.zzave.intValue(), ((zzgf)localObject10).zzawu.longValue());
                          } else {
                            zza((Map)localObject7, localzzfv.zzave.intValue(), ((zzgf)localObject10).zzawu.longValue());
                          }
                        }
                      }
                    }
                    else if (((BitSet)localObject9).get(localzzfv.zzave.intValue()))
                    {
                      zzgo().zzjl().zze("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(m), localzzfv.zzave);
                    }
                    else
                    {
                      localObject13 = zza(localzzfv, (String)localObject4, (zzgg[])localObject3, l2);
                      localzzar = zzgo().zzjl();
                      if (localObject13 == null) {}
                      for (localObject12 = "null";; localObject12 = localObject13)
                      {
                        localzzar.zzg("Event filter result", localObject12);
                        if (localObject13 != null) {
                          break label2361;
                        }
                        localHashSet.add(Integer.valueOf(m));
                        break;
                      }
                      ((BitSet)localObject8).set(localzzfv.zzave.intValue());
                      if (((Boolean)localObject13).booleanValue()) {
                        ((BitSet)localObject9).set(localzzfv.zzave.intValue());
                      }
                    }
                  }
                }
                break label1305;
                if (paramArrayOfzzgl != null)
                {
                  localObject8 = new ArrayMap();
                  m = paramArrayOfzzgl.length;
                  i = 0;
                  if (i < m)
                  {
                    localObject9 = paramArrayOfzzgl[i];
                    localObject4 = (Map)((Map)localObject8).get(((zzgl)localObject9).name);
                    if (localObject4 != null) {
                      break label4102;
                    }
                    localObject1 = zzjq().zzm(paramString, ((zzgl)localObject9).name);
                    paramArrayOfzzgf = (zzgf[])localObject1;
                    if (localObject1 == null) {
                      paramArrayOfzzgf = new ArrayMap();
                    }
                    ((Map)localObject8).put(((zzgl)localObject9).name, paramArrayOfzzgf);
                    localObject4 = paramArrayOfzzgf;
                  }
                }
                for (;;)
                {
                  localObject10 = ((Map)localObject4).keySet().iterator();
                  while (((Iterator)localObject10).hasNext())
                  {
                    n = ((Integer)((Iterator)localObject10).next()).intValue();
                    if (localHashSet.contains(Integer.valueOf(n)))
                    {
                      zzgo().zzjl().zzg("Skipping failed audience ID", Integer.valueOf(n));
                    }
                    else
                    {
                      localObject5 = (zzgd)localArrayMap1.get(Integer.valueOf(n));
                      localObject3 = (BitSet)localArrayMap2.get(Integer.valueOf(n));
                      localObject2 = (BitSet)localArrayMap3.get(Integer.valueOf(n));
                      localObject1 = null;
                      paramArrayOfzzgf = null;
                      if (bool)
                      {
                        localObject1 = (Map)localArrayMap4.get(Integer.valueOf(n));
                        paramArrayOfzzgf = (Map)localArrayMap5.get(Integer.valueOf(n));
                      }
                      if (localObject5 != null) {
                        break label4099;
                      }
                      localObject2 = new zzgd();
                      localArrayMap1.put(Integer.valueOf(n), localObject2);
                      ((zzgd)localObject2).zzawo = Boolean.valueOf(true);
                      localObject2 = new BitSet();
                      localArrayMap2.put(Integer.valueOf(n), localObject2);
                      localObject3 = new BitSet();
                      localArrayMap3.put(Integer.valueOf(n), localObject3);
                      if (!bool) {
                        break label4084;
                      }
                      localObject1 = new ArrayMap();
                      localArrayMap4.put(Integer.valueOf(n), localObject1);
                      localObject5 = new ArrayMap();
                      localArrayMap5.put(Integer.valueOf(n), localObject5);
                      paramArrayOfzzgf = (zzgf[])localObject3;
                      localObject3 = localObject2;
                      localObject2 = paramArrayOfzzgf;
                      paramArrayOfzzgf = (zzgf[])localObject5;
                    }
                  }
                  for (;;)
                  {
                    localObject11 = ((List)((Map)localObject4).get(Integer.valueOf(n))).iterator();
                    for (;;)
                    {
                      if (!((Iterator)localObject11).hasNext()) {
                        break label3440;
                      }
                      localObject12 = (zzfy)((Iterator)localObject11).next();
                      if (zzgo().isLoggable(2))
                      {
                        zzgo().zzjl().zzd("Evaluating filter. audience, filter, property", Integer.valueOf(n), ((zzfy)localObject12).zzave, zzgl().zzbu(((zzfy)localObject12).zzavu));
                        zzgo().zzjl().zzg("Filter definition", zzjo().zza((zzfy)localObject12));
                      }
                      if ((((zzfy)localObject12).zzave == null) || (((zzfy)localObject12).zzave.intValue() > 256))
                      {
                        zzgo().zzjg().zze("Invalid property filter ID. appId, id", zzap.zzbv(paramString), String.valueOf(((zzfy)localObject12).zzave));
                        localHashSet.add(Integer.valueOf(n));
                        break;
                      }
                      if (bool)
                      {
                        if ((localObject12 != null) && (((zzfy)localObject12).zzavb != null) && (((zzfy)localObject12).zzavb.booleanValue()))
                        {
                          j = 1;
                          if ((localObject12 == null) || (((zzfy)localObject12).zzavc == null) || (!((zzfy)localObject12).zzavc.booleanValue())) {
                            break label3114;
                          }
                        }
                        for (k = 1;; k = 0)
                        {
                          if ((!((BitSet)localObject3).get(((zzfy)localObject12).zzave.intValue())) || (j != 0) || (k != 0)) {
                            break label3120;
                          }
                          zzgo().zzjl().zze("Property filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID", Integer.valueOf(n), ((zzfy)localObject12).zzave);
                          break;
                          j = 0;
                          break label3029;
                        }
                        localObject7 = zza((zzfy)localObject12, (zzgl)localObject9);
                        localObject13 = zzgo().zzjl();
                        if (localObject7 == null) {}
                        for (localObject5 = "null";; localObject5 = localObject7)
                        {
                          ((zzar)localObject13).zzg("Property filter result", localObject5);
                          if (localObject7 != null) {
                            break label3187;
                          }
                          localHashSet.add(Integer.valueOf(n));
                          break;
                        }
                        ((BitSet)localObject2).set(((zzfy)localObject12).zzave.intValue());
                        ((BitSet)localObject3).set(((zzfy)localObject12).zzave.intValue(), ((Boolean)localObject7).booleanValue());
                        if ((((Boolean)localObject7).booleanValue()) && ((j != 0) || (k != 0)) && (((zzgl)localObject9).zzayl != null)) {
                          if (k != 0) {
                            zzb(paramArrayOfzzgf, ((zzfy)localObject12).zzave.intValue(), ((zzgl)localObject9).zzayl.longValue());
                          } else {
                            zza((Map)localObject1, ((zzfy)localObject12).zzave.intValue(), ((zzgl)localObject9).zzayl.longValue());
                          }
                        }
                      }
                      else if (((BitSet)localObject3).get(((zzfy)localObject12).zzave.intValue()))
                      {
                        zzgo().zzjl().zze("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(n), ((zzfy)localObject12).zzave);
                      }
                      else
                      {
                        localObject7 = zza((zzfy)localObject12, (zzgl)localObject9);
                        localObject13 = zzgo().zzjl();
                        if (localObject7 == null) {}
                        for (localObject5 = "null";; localObject5 = localObject7)
                        {
                          ((zzar)localObject13).zzg("Property filter result", localObject5);
                          if (localObject7 != null) {
                            break label3405;
                          }
                          localHashSet.add(Integer.valueOf(n));
                          break;
                        }
                        ((BitSet)localObject2).set(((zzfy)localObject12).zzave.intValue());
                        if (((Boolean)localObject7).booleanValue()) {
                          ((BitSet)localObject3).set(((zzfy)localObject12).zzave.intValue());
                        }
                      }
                    }
                    break label2515;
                    i += 1;
                    break;
                    localObject1 = new zzgd[localArrayMap2.size()];
                    localObject2 = localArrayMap2.keySet().iterator();
                    i = 0;
                    while (((Iterator)localObject2).hasNext())
                    {
                      m = ((Integer)((Iterator)localObject2).next()).intValue();
                      if (!localHashSet.contains(Integer.valueOf(m)))
                      {
                        paramArrayOfzzgf = (zzgd)localArrayMap1.get(Integer.valueOf(m));
                        if (paramArrayOfzzgf != null) {
                          break label4081;
                        }
                        paramArrayOfzzgf = new zzgd();
                      }
                    }
                    for (;;)
                    {
                      j = i + 1;
                      localObject1[i] = paramArrayOfzzgf;
                      paramArrayOfzzgf.zzauy = Integer.valueOf(m);
                      paramArrayOfzzgf.zzawm = new zzgj();
                      paramArrayOfzzgf.zzawm.zzayf = zzfg.zza((BitSet)localArrayMap2.get(Integer.valueOf(m)));
                      paramArrayOfzzgf.zzawm.zzaye = zzfg.zza((BitSet)localArrayMap3.get(Integer.valueOf(m)));
                      if (bool)
                      {
                        paramArrayOfzzgf.zzawm.zzayg = zzd((Map)localArrayMap4.get(Integer.valueOf(m)));
                        localObject3 = paramArrayOfzzgf.zzawm;
                        localObject4 = (Map)localArrayMap5.get(Integer.valueOf(m));
                        if (localObject4 != null) {
                          break label3843;
                        }
                        paramArrayOfzzgl = new zzgk[0];
                      }
                      for (;;)
                      {
                        ((zzgj)localObject3).zzayh = paramArrayOfzzgl;
                        paramArrayOfzzgl = zzjq();
                        localObject3 = paramArrayOfzzgf.zzawm;
                        paramArrayOfzzgl.zzcl();
                        paramArrayOfzzgl.zzaf();
                        Preconditions.checkNotEmpty(paramString);
                        Preconditions.checkNotNull(localObject3);
                        try
                        {
                          paramArrayOfzzgf = new byte[((zzzg)localObject3).zzvu()];
                          localObject4 = zzyy.zzk(paramArrayOfzzgf, 0, paramArrayOfzzgf.length);
                          ((zzzg)localObject3).zza((zzyy)localObject4);
                          ((zzyy)localObject4).zzyt();
                          localObject3 = new ContentValues();
                          ((ContentValues)localObject3).put("app_id", paramString);
                          ((ContentValues)localObject3).put("audience_id", Integer.valueOf(m));
                          ((ContentValues)localObject3).put("current_results", paramArrayOfzzgf);
                        }
                        catch (IOException paramArrayOfzzgf)
                        {
                          paramArrayOfzzgl.zzgo().zzjd().zze("Configuration loss. Failed to serialize filter results. appId", zzap.zzbv(paramString), paramArrayOfzzgf);
                          i = j;
                        }
                        try
                        {
                          if (paramArrayOfzzgl.getWritableDatabase().insertWithOnConflict("audience_filter_values", null, (ContentValues)localObject3, 5) == -1L) {
                            paramArrayOfzzgl.zzgo().zzjd().zzg("Failed to insert filter results (got -1). appId", zzap.zzbv(paramString));
                          }
                          i = j;
                        }
                        catch (SQLiteException paramArrayOfzzgf)
                        {
                          paramArrayOfzzgl.zzgo().zzjd().zze("Error storing filter results. appId", zzap.zzbv(paramString), paramArrayOfzzgf);
                          i = j;
                        }
                        paramArrayOfzzgl = new zzgk[((Map)localObject4).size()];
                        localObject5 = ((Map)localObject4).keySet().iterator();
                        i = 0;
                        while (((Iterator)localObject5).hasNext())
                        {
                          localObject8 = (Integer)((Iterator)localObject5).next();
                          localObject7 = new zzgk();
                          ((zzgk)localObject7).zzawq = ((Integer)localObject8);
                          localObject9 = (List)((Map)localObject4).get(localObject8);
                          if (localObject9 != null)
                          {
                            Collections.sort((List)localObject9);
                            localObject8 = new long[((List)localObject9).size()];
                            k = 0;
                            localObject9 = ((List)localObject9).iterator();
                            while (((Iterator)localObject9).hasNext())
                            {
                              localObject8[k] = ((Long)((Iterator)localObject9).next()).longValue();
                              k += 1;
                            }
                            ((zzgk)localObject7).zzayj = ((long[])localObject8);
                          }
                          paramArrayOfzzgl[i] = localObject7;
                          i += 1;
                        }
                      }
                      break;
                      break;
                      return (zzgd[])Arrays.copyOf((Object[])localObject1, i);
                    }
                    localObject5 = localObject2;
                    localObject2 = localObject3;
                    localObject3 = localObject5;
                  }
                }
                localObject5 = localObject3;
                localObject3 = localObject1;
                localObject1 = localObject5;
                break label1134;
                break;
                localObject12 = localObject8;
                localObject8 = localObject9;
                localObject9 = localObject12;
              }
            }
          }
          localObject3 = localObject1;
          break;
        }
      }
      label4154:
      localObject1 = null;
    }
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */