package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.measurement.AppMeasurement.Event;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.google.android.gms.measurement.AppMeasurement.UserProperty;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public final class zzan
  extends zzcp
{
  private static final AtomicReference<String[]> zzalt = new AtomicReference();
  private static final AtomicReference<String[]> zzalu = new AtomicReference();
  private static final AtomicReference<String[]> zzalv = new AtomicReference();
  
  zzan(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  @Nullable
  private static String zza(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, AtomicReference<String[]> paramAtomicReference)
  {
    int i = 0;
    Preconditions.checkNotNull(paramArrayOfString1);
    Preconditions.checkNotNull(paramArrayOfString2);
    Preconditions.checkNotNull(paramAtomicReference);
    if (paramArrayOfString1.length == paramArrayOfString2.length) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool);
      while (i < paramArrayOfString1.length)
      {
        if (zzfk.zzu(paramString, paramArrayOfString1[i])) {
          try
          {
            Object localObject = (String[])paramAtomicReference.get();
            paramString = (String)localObject;
            if (localObject == null)
            {
              paramString = new String[paramArrayOfString2.length];
              paramAtomicReference.set(paramString);
            }
            if (paramString[i] == null)
            {
              localObject = new StringBuilder();
              ((StringBuilder)localObject).append(paramArrayOfString2[i]);
              ((StringBuilder)localObject).append("(");
              ((StringBuilder)localObject).append(paramArrayOfString1[i]);
              ((StringBuilder)localObject).append(")");
              paramString[i] = ((StringBuilder)localObject).toString();
            }
            paramString = paramString[i];
            return paramString;
          }
          finally {}
        }
        i += 1;
      }
      return paramString;
    }
  }
  
  @Nullable
  private final String zzb(zzaa paramzzaa)
  {
    if (paramzzaa == null) {
      return null;
    }
    if (!zzjc()) {
      return paramzzaa.toString();
    }
    return zzd(paramzzaa.zziv());
  }
  
  private final boolean zzjc()
  {
    zzgr();
    return (this.zzadj.zzkj()) && (this.zzadj.zzgo().isLoggable(3));
  }
  
  @Nullable
  protected final String zza(zzy paramzzy)
  {
    if (paramzzy == null) {
      return null;
    }
    if (!zzjc()) {
      return paramzzy.toString();
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Event{appId='");
    localStringBuilder.append(paramzzy.zztt);
    localStringBuilder.append("', name='");
    localStringBuilder.append(zzbs(paramzzy.name));
    localStringBuilder.append("', params=");
    localStringBuilder.append(zzb(paramzzy.zzaid));
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  @Nullable
  protected final String zzb(zzad paramzzad)
  {
    if (paramzzad == null) {
      return null;
    }
    if (!zzjc()) {
      return paramzzad.toString();
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("origin=");
    localStringBuilder.append(paramzzad.origin);
    localStringBuilder.append(",name=");
    localStringBuilder.append(zzbs(paramzzad.name));
    localStringBuilder.append(",params=");
    localStringBuilder.append(zzb(paramzzad.zzaid));
    return localStringBuilder.toString();
  }
  
  @Nullable
  protected final String zzbs(String paramString)
  {
    String str;
    if (paramString == null) {
      str = null;
    }
    do
    {
      return str;
      str = paramString;
    } while (!zzjc());
    return zza(paramString, AppMeasurement.Event.zzadl, AppMeasurement.Event.zzadk, zzalt);
  }
  
  @Nullable
  protected final String zzbt(String paramString)
  {
    String str;
    if (paramString == null) {
      str = null;
    }
    do
    {
      return str;
      str = paramString;
    } while (!zzjc());
    return zza(paramString, AppMeasurement.Param.zzadn, AppMeasurement.Param.zzadm, zzalu);
  }
  
  @Nullable
  protected final String zzbu(String paramString)
  {
    Object localObject;
    if (paramString == null) {
      localObject = null;
    }
    do
    {
      return (String)localObject;
      localObject = paramString;
    } while (!zzjc());
    if (paramString.startsWith("_exp_"))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append("experiment_id");
      ((StringBuilder)localObject).append("(");
      ((StringBuilder)localObject).append(paramString);
      ((StringBuilder)localObject).append(")");
      return ((StringBuilder)localObject).toString();
    }
    return zza(paramString, AppMeasurement.UserProperty.zzadp, AppMeasurement.UserProperty.zzado, zzalv);
  }
  
  @Nullable
  protected final String zzd(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return null;
    }
    if (!zzjc()) {
      return paramBundle.toString();
    }
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramBundle.keySet().iterator();
    if (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (localStringBuilder.length() != 0) {
        localStringBuilder.append(", ");
      }
      for (;;)
      {
        localStringBuilder.append(zzbt(str));
        localStringBuilder.append("=");
        localStringBuilder.append(paramBundle.get(str));
        break;
        localStringBuilder.append("Bundle[{");
      }
    }
    localStringBuilder.append("}]");
    return localStringBuilder.toString();
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */