package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;

final class zzes
  extends zzv
{
  zzes(zzeq paramzzeq, zzcq paramzzcq)
  {
    super(paramzzcq);
  }
  
  @WorkerThread
  public final void run()
  {
    zzeq.zza(this.zzasz);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */