package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;
import java.util.List;
import java.util.Map;

@WorkerThread
abstract interface zzav
{
  public abstract void zza(String paramString, int paramInt, Throwable paramThrowable, byte[] paramArrayOfByte, Map<String, List<String>> paramMap);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzav.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */