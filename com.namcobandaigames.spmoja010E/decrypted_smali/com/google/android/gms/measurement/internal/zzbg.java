package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.PackageManagerWrapper;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.measurement.zzu;
import java.util.List;

public final class zzbg
{
  final zzbt zzadj;
  
  zzbg(zzbt paramzzbt)
  {
    this.zzadj = paramzzbt;
  }
  
  @VisibleForTesting
  private final boolean zzka()
  {
    try
    {
      PackageManagerWrapper localPackageManagerWrapper = Wrappers.packageManager(this.zzadj.getContext());
      if (localPackageManagerWrapper == null)
      {
        this.zzadj.zzgo().zzjj().zzbx("Failed to retrieve Package Manager to check Play Store compatibility");
        return false;
      }
      int i = localPackageManagerWrapper.getPackageInfo("com.android.vending", 128).versionCode;
      if (i >= 80837300) {
        return true;
      }
    }
    catch (Exception localException)
    {
      this.zzadj.zzgo().zzjj().zzg("Failed to retrieve Play Store version", localException);
    }
    return false;
  }
  
  @Nullable
  @WorkerThread
  @VisibleForTesting
  final Bundle zza(String paramString, zzu paramzzu)
  {
    this.zzadj.zzgn().zzaf();
    if (paramzzu == null)
    {
      this.zzadj.zzgo().zzjg().zzbx("Attempting to use Install Referrer Service while it is not initialized");
      return null;
    }
    Bundle localBundle = new Bundle();
    localBundle.putString("package_name", paramString);
    try
    {
      paramString = paramzzu.zza(localBundle);
      if (paramString == null)
      {
        this.zzadj.zzgo().zzjd().zzbx("Install Referrer Service returned a null response");
        return null;
      }
    }
    catch (Exception paramString)
    {
      this.zzadj.zzgo().zzjd().zzg("Exception occurred while retrieving the Install Referrer", paramString.getMessage());
      return null;
    }
    return paramString;
  }
  
  @WorkerThread
  protected final void zzcd(String paramString)
  {
    if ((paramString == null) || (paramString.isEmpty())) {
      this.zzadj.zzgo().zzjj().zzbx("Install Referrer Reporter was called with invalid app package name");
    }
    Object localObject1;
    Object localObject2;
    do
    {
      return;
      this.zzadj.zzgn().zzaf();
      if (!zzka())
      {
        this.zzadj.zzgo().zzjj().zzbx("Install Referrer Reporter is not available");
        return;
      }
      this.zzadj.zzgo().zzjj().zzbx("Install Referrer Reporter is initializing");
      paramString = new zzbh(this, paramString);
      this.zzadj.zzgn().zzaf();
      localObject1 = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
      ((Intent)localObject1).setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
      localObject2 = this.zzadj.getContext().getPackageManager();
      if (localObject2 == null)
      {
        this.zzadj.zzgo().zzjg().zzbx("Failed to obtain Package Manager to verify binding conditions");
        return;
      }
      localObject2 = ((PackageManager)localObject2).queryIntentServices((Intent)localObject1, 0);
      if ((localObject2 == null) || (((List)localObject2).isEmpty())) {
        break;
      }
      localObject2 = (ResolveInfo)((List)localObject2).get(0);
    } while (((ResolveInfo)localObject2).serviceInfo == null);
    String str = ((ResolveInfo)localObject2).serviceInfo.packageName;
    if ((((ResolveInfo)localObject2).serviceInfo.name != null) && ("com.android.vending".equals(str)) && (zzka()))
    {
      localObject1 = new Intent((Intent)localObject1);
      for (;;)
      {
        try
        {
          boolean bool = ConnectionTracker.getInstance().bindService(this.zzadj.getContext(), (Intent)localObject1, paramString, 1);
          localObject1 = this.zzadj.zzgo().zzjj();
          if (bool)
          {
            paramString = "available";
            ((zzar)localObject1).zzg("Install Referrer Service is", paramString);
            return;
          }
        }
        catch (Exception paramString)
        {
          this.zzadj.zzgo().zzjd().zzg("Exception occurred while binding to Install Referrer Service", paramString.getMessage());
          return;
        }
        paramString = "not available";
      }
    }
    this.zzadj.zzgo().zzjj().zzbx("Play Store missing or incompatible. Version 8.3.73 or later required");
    return;
    this.zzadj.zzgo().zzjj().zzbx("Play Service for fetching Install Referrer is unavailable on device");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */