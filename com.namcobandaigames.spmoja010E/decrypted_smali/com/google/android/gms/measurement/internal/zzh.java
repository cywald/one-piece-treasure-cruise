package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="AppMetadataCreator")
@SafeParcelable.Reserved({1, 20})
public final class zzh
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzh> CREATOR = new zzi();
  @SafeParcelable.Field(id=2)
  public final String packageName;
  @SafeParcelable.Field(id=6)
  public final long zzadt;
  @SafeParcelable.Field(id=3)
  public final String zzafx;
  @SafeParcelable.Field(id=12)
  public final String zzafz;
  @SafeParcelable.Field(defaultValueUnchecked="Integer.MIN_VALUE", id=11)
  public final long zzagd;
  @SafeParcelable.Field(id=5)
  public final String zzage;
  @SafeParcelable.Field(id=7)
  public final long zzagf;
  @SafeParcelable.Field(defaultValue="true", id=9)
  public final boolean zzagg;
  @SafeParcelable.Field(id=13)
  public final long zzagh;
  @SafeParcelable.Field(defaultValue="true", id=16)
  public final boolean zzagi;
  @SafeParcelable.Field(defaultValue="true", id=17)
  public final boolean zzagj;
  @SafeParcelable.Field(id=19)
  public final String zzagk;
  @SafeParcelable.Field(id=8)
  public final String zzagv;
  @SafeParcelable.Field(id=10)
  public final boolean zzagw;
  @SafeParcelable.Field(id=14)
  public final long zzagx;
  @SafeParcelable.Field(id=15)
  public final int zzagy;
  @SafeParcelable.Field(id=18)
  public final boolean zzagz;
  @SafeParcelable.Field(id=4)
  public final String zzts;
  
  zzh(String paramString1, String paramString2, String paramString3, long paramLong1, String paramString4, long paramLong2, long paramLong3, String paramString5, boolean paramBoolean1, boolean paramBoolean2, String paramString6, long paramLong4, long paramLong5, int paramInt, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String paramString7)
  {
    Preconditions.checkNotEmpty(paramString1);
    this.packageName = paramString1;
    paramString1 = paramString2;
    if (TextUtils.isEmpty(paramString2)) {
      paramString1 = null;
    }
    this.zzafx = paramString1;
    this.zzts = paramString3;
    this.zzagd = paramLong1;
    this.zzage = paramString4;
    this.zzadt = paramLong2;
    this.zzagf = paramLong3;
    this.zzagv = paramString5;
    this.zzagg = paramBoolean1;
    this.zzagw = paramBoolean2;
    this.zzafz = paramString6;
    this.zzagh = paramLong4;
    this.zzagx = paramLong5;
    this.zzagy = paramInt;
    this.zzagi = paramBoolean3;
    this.zzagj = paramBoolean4;
    this.zzagz = paramBoolean5;
    this.zzagk = paramString7;
  }
  
  @SafeParcelable.Constructor
  zzh(@SafeParcelable.Param(id=2) String paramString1, @SafeParcelable.Param(id=3) String paramString2, @SafeParcelable.Param(id=4) String paramString3, @SafeParcelable.Param(id=5) String paramString4, @SafeParcelable.Param(id=6) long paramLong1, @SafeParcelable.Param(id=7) long paramLong2, @SafeParcelable.Param(id=8) String paramString5, @SafeParcelable.Param(id=9) boolean paramBoolean1, @SafeParcelable.Param(id=10) boolean paramBoolean2, @SafeParcelable.Param(id=11) long paramLong3, @SafeParcelable.Param(id=12) String paramString6, @SafeParcelable.Param(id=13) long paramLong4, @SafeParcelable.Param(id=14) long paramLong5, @SafeParcelable.Param(id=15) int paramInt, @SafeParcelable.Param(id=16) boolean paramBoolean3, @SafeParcelable.Param(id=17) boolean paramBoolean4, @SafeParcelable.Param(id=18) boolean paramBoolean5, @SafeParcelable.Param(id=19) String paramString7)
  {
    this.packageName = paramString1;
    this.zzafx = paramString2;
    this.zzts = paramString3;
    this.zzagd = paramLong3;
    this.zzage = paramString4;
    this.zzadt = paramLong1;
    this.zzagf = paramLong2;
    this.zzagv = paramString5;
    this.zzagg = paramBoolean1;
    this.zzagw = paramBoolean2;
    this.zzafz = paramString6;
    this.zzagh = paramLong4;
    this.zzagx = paramLong5;
    this.zzagy = paramInt;
    this.zzagi = paramBoolean3;
    this.zzagj = paramBoolean4;
    this.zzagz = paramBoolean5;
    this.zzagk = paramString7;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.packageName, false);
    SafeParcelWriter.writeString(paramParcel, 3, this.zzafx, false);
    SafeParcelWriter.writeString(paramParcel, 4, this.zzts, false);
    SafeParcelWriter.writeString(paramParcel, 5, this.zzage, false);
    SafeParcelWriter.writeLong(paramParcel, 6, this.zzadt);
    SafeParcelWriter.writeLong(paramParcel, 7, this.zzagf);
    SafeParcelWriter.writeString(paramParcel, 8, this.zzagv, false);
    SafeParcelWriter.writeBoolean(paramParcel, 9, this.zzagg);
    SafeParcelWriter.writeBoolean(paramParcel, 10, this.zzagw);
    SafeParcelWriter.writeLong(paramParcel, 11, this.zzagd);
    SafeParcelWriter.writeString(paramParcel, 12, this.zzafz, false);
    SafeParcelWriter.writeLong(paramParcel, 13, this.zzagh);
    SafeParcelWriter.writeLong(paramParcel, 14, this.zzagx);
    SafeParcelWriter.writeInt(paramParcel, 15, this.zzagy);
    SafeParcelWriter.writeBoolean(paramParcel, 16, this.zzagi);
    SafeParcelWriter.writeBoolean(paramParcel, 17, this.zzagj);
    SafeParcelWriter.writeBoolean(paramParcel, 18, this.zzagz);
    SafeParcelWriter.writeString(paramParcel, 19, this.zzagk, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */