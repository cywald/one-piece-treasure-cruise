package com.google.android.gms.measurement.internal;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.internal.measurement.zzu;

final class zzbi
  implements Runnable
{
  zzbi(zzbh paramzzbh, zzu paramzzu, ServiceConnection paramServiceConnection) {}
  
  public final void run()
  {
    zzbg localzzbg = this.zzaoh.zzaoe;
    Object localObject1 = zzbh.zza(this.zzaoh);
    Object localObject2 = this.zzaof;
    ServiceConnection localServiceConnection = this.zzaog;
    localObject2 = localzzbg.zza((String)localObject1, (zzu)localObject2);
    localzzbg.zzadj.zzgn().zzaf();
    long l1;
    if (localObject2 != null)
    {
      l1 = ((Bundle)localObject2).getLong("install_begin_timestamp_seconds", 0L) * 1000L;
      if (l1 != 0L) {
        break label114;
      }
      localzzbg.zzadj.zzgo().zzjd().zzbx("Service response is missing Install Referrer install timestamp");
    }
    for (;;)
    {
      if (localServiceConnection != null) {
        ConnectionTracker.getInstance().unbindService(localzzbg.zzadj.getContext(), localServiceConnection);
      }
      return;
      label114:
      localObject1 = ((Bundle)localObject2).getString("install_referrer");
      if ((localObject1 == null) || (((String)localObject1).isEmpty()))
      {
        localzzbg.zzadj.zzgo().zzjd().zzbx("No referrer defined in install referrer response");
      }
      else
      {
        localzzbg.zzadj.zzgo().zzjl().zzg("InstallReferrer API result", localObject1);
        Object localObject3 = localzzbg.zzadj.zzgm();
        localObject1 = String.valueOf(localObject1);
        if (((String)localObject1).length() != 0) {}
        for (localObject1 = "?".concat((String)localObject1);; localObject1 = new String("?"))
        {
          localObject1 = ((zzfk)localObject3).zza(Uri.parse((String)localObject1));
          if (localObject1 != null) {
            break label257;
          }
          localzzbg.zzadj.zzgo().zzjd().zzbx("No campaign params defined in install referrer result");
          break;
        }
        label257:
        localObject3 = ((Bundle)localObject1).getString("medium");
        int i;
        if ((localObject3 != null) && (!"(not set)".equalsIgnoreCase((String)localObject3)) && (!"organic".equalsIgnoreCase((String)localObject3))) {
          i = 1;
        }
        for (;;)
        {
          if (i != 0)
          {
            long l2 = ((Bundle)localObject2).getLong("referrer_click_timestamp_seconds", 0L) * 1000L;
            if (l2 == 0L)
            {
              localzzbg.zzadj.zzgo().zzjd().zzbx("Install Referrer is missing click timestamp for ad campaign");
              break;
              i = 0;
              continue;
            }
            ((Bundle)localObject1).putLong("click_timestamp", l2);
          }
        }
        if (l1 == localzzbg.zzadj.zzgp().zzank.get())
        {
          localzzbg.zzadj.zzgr();
          localzzbg.zzadj.zzgo().zzjl().zzbx("Campaign has already been logged");
        }
        else
        {
          localzzbg.zzadj.zzgp().zzank.set(l1);
          localzzbg.zzadj.zzgr();
          localzzbg.zzadj.zzgo().zzjl().zzg("Logging Install Referrer campaign from sdk with ", "referrer API");
          ((Bundle)localObject1).putString("_cis", "referrer API");
          localzzbg.zzadj.zzge().logEvent("auto", "_cmp", (Bundle)localObject1);
        }
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */