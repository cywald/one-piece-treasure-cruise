package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;

class zzco
  implements zzcq
{
  protected final zzbt zzadj;
  
  zzco(zzbt paramzzbt)
  {
    Preconditions.checkNotNull(paramzzbt);
    this.zzadj = paramzzbt;
  }
  
  public Context getContext()
  {
    return this.zzadj.getContext();
  }
  
  public void zzaf()
  {
    this.zzadj.zzgn().zzaf();
  }
  
  public Clock zzbx()
  {
    return this.zzadj.zzbx();
  }
  
  public void zzga()
  {
    this.zzadj.zzga();
  }
  
  public void zzgb()
  {
    this.zzadj.zzgb();
  }
  
  public void zzgc()
  {
    this.zzadj.zzgn().zzgc();
  }
  
  public zzx zzgk()
  {
    return this.zzadj.zzgk();
  }
  
  public zzan zzgl()
  {
    return this.zzadj.zzgl();
  }
  
  public zzfk zzgm()
  {
    return this.zzadj.zzgm();
  }
  
  public zzbo zzgn()
  {
    return this.zzadj.zzgn();
  }
  
  public zzap zzgo()
  {
    return this.zzadj.zzgo();
  }
  
  public zzba zzgp()
  {
    return this.zzadj.zzgp();
  }
  
  public zzn zzgq()
  {
    return this.zzadj.zzgq();
  }
  
  public zzk zzgr()
  {
    return this.zzadj.zzgr();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzco.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */