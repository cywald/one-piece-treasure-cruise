package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.GoogleServices;
import com.google.android.gms.common.internal.StringResourceValueReader;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.InstantApps;
import java.lang.reflect.Method;

public final class zzaj
  extends zzf
{
  private String zzafx;
  private String zzage;
  private long zzagh;
  private String zzagk;
  private int zzagy;
  private int zzalo;
  private long zzalp;
  private String zztr;
  private String zzts;
  private String zztt;
  
  zzaj(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  @WorkerThread
  @VisibleForTesting
  private final String zziz()
  {
    try
    {
      Class localClass = getContext().getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
      if (localClass == null) {
        return null;
      }
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      return null;
    }
    Object localObject;
    try
    {
      localObject = localClassNotFoundException.getDeclaredMethod("getInstance", new Class[] { Context.class }).invoke(null, new Object[] { getContext() });
      if (localObject == null) {
        return null;
      }
    }
    catch (Exception localException1)
    {
      zzgo().zzjh().zzbx("Failed to obtain Firebase Analytics instance");
      return null;
    }
    try
    {
      String str = (String)localException1.getDeclaredMethod("getFirebaseInstanceId", new Class[0]).invoke(localObject, new Object[0]);
      return str;
    }
    catch (Exception localException2)
    {
      zzgo().zzji().zzbx("Failed to retrieve Firebase Instance Id");
    }
    return null;
  }
  
  final String getGmpAppId()
  {
    zzcl();
    return this.zzafx;
  }
  
  final String zzal()
  {
    zzcl();
    return this.zztt;
  }
  
  @WorkerThread
  final zzh zzbr(String paramString)
  {
    zzaf();
    zzgb();
    String str2 = zzal();
    String str3 = getGmpAppId();
    zzcl();
    String str4 = this.zzts;
    long l1 = zzja();
    zzcl();
    String str5 = this.zzage;
    long l2 = zzgq().zzhc();
    zzcl();
    zzaf();
    if (this.zzalp == 0L) {
      this.zzalp = this.zzadj.zzgm().zzd(getContext(), getContext().getPackageName());
    }
    long l3 = this.zzalp;
    boolean bool3 = this.zzadj.isEnabled();
    boolean bool1;
    String str1;
    label160:
    long l4;
    long l5;
    int i;
    label220:
    boolean bool4;
    if (!zzgp().zzanv)
    {
      bool1 = true;
      zzaf();
      zzgb();
      if ((!zzgq().zzbc(this.zztt)) || (this.zzadj.isEnabled())) {
        break label326;
      }
      str1 = null;
      zzcl();
      l4 = this.zzagh;
      l5 = this.zzadj.zzkp();
      i = zzjb();
      Object localObject = zzgq();
      ((zzco)localObject).zzgb();
      localObject = ((zzn)localObject).zzau("google_analytics_adid_collection_enabled");
      if ((localObject != null) && (!((Boolean)localObject).booleanValue())) {
        break label335;
      }
      bool2 = true;
      bool4 = Boolean.valueOf(bool2).booleanValue();
      localObject = zzgq();
      ((zzco)localObject).zzgb();
      localObject = ((zzn)localObject).zzau("google_analytics_ssaid_collection_enabled");
      if ((localObject != null) && (!((Boolean)localObject).booleanValue())) {
        break label341;
      }
    }
    label326:
    label335:
    label341:
    for (boolean bool2 = true;; bool2 = false)
    {
      return new zzh(str2, str3, str4, l1, str5, l2, l3, paramString, bool3, bool1, str1, l4, l5, i, bool4, Boolean.valueOf(bool2).booleanValue(), zzgp().zzjx(), zzgw());
      bool1 = false;
      break;
      str1 = zziz();
      break label160;
      bool2 = false;
      break label220;
    }
  }
  
  protected final boolean zzgt()
  {
    return true;
  }
  
  protected final void zzgu()
  {
    int k = 1;
    Object localObject3 = "unknown";
    String str3 = "Unknown";
    int j = Integer.MIN_VALUE;
    String str1 = "Unknown";
    String str4 = getContext().getPackageName();
    Object localObject6 = getContext().getPackageManager();
    Object localObject5;
    String str2;
    int i;
    Object localObject1;
    if (localObject6 == null)
    {
      zzgo().zzjd().zzg("PackageManager is null, app identity information might be inaccurate. appId", zzap.zzbv(str4));
      localObject5 = localObject3;
      str2 = str3;
      i = j;
      localObject3 = str1;
      this.zztt = str4;
      this.zzage = ((String)localObject5);
      this.zzts = str2;
      this.zzalo = i;
      this.zztr = ((String)localObject3);
      this.zzalp = 0L;
      zzgr();
      localObject1 = GoogleServices.initialize(getContext());
      if ((localObject1 == null) || (!((Status)localObject1).isSuccess())) {
        break label655;
      }
      i = 1;
      label139:
      if ((TextUtils.isEmpty(this.zzadj.zzkk())) || (!"am".equals(this.zzadj.zzkl()))) {
        break label660;
      }
      j = 1;
      label170:
      i |= j;
      if (i == 0)
      {
        if (localObject1 != null) {
          break label665;
        }
        zzgo().zzjd().zzbx("GoogleService failed to initialize (no status)");
      }
      label196:
      if (i == 0) {
        break label825;
      }
      localObject1 = zzgq().zzhv();
      if (!zzgq().zzhu()) {
        break label694;
      }
      if (!this.zzadj.zzkj()) {
        break label825;
      }
      zzgo().zzjj().zzbx("Collection disabled with firebase_analytics_collection_deactivated=1");
      i = 0;
    }
    for (;;)
    {
      label244:
      this.zzafx = "";
      this.zzagk = "";
      this.zzagh = 0L;
      zzgr();
      if ((!TextUtils.isEmpty(this.zzadj.zzkk())) && ("am".equals(this.zzadj.zzkl()))) {
        this.zzagk = this.zzadj.zzkk();
      }
      for (;;)
      {
        try
        {
          localObject3 = GoogleServices.getGoogleAppId();
          if (!TextUtils.isEmpty((CharSequence)localObject3)) {
            continue;
          }
          localObject1 = "";
          this.zzafx = ((String)localObject1);
          if (!TextUtils.isEmpty((CharSequence)localObject3)) {
            this.zzagk = new StringResourceValueReader(getContext()).getString("gma_app_id");
          }
          if (i != 0) {
            zzgo().zzjl().zze("App package, google app id", this.zztt, this.zzafx);
          }
        }
        catch (IllegalStateException localIllegalStateException)
        {
          Object localObject2;
          Object localObject4;
          label655:
          label660:
          label665:
          label694:
          zzgo().zzjd().zze("getGoogleAppId or isMeasurementEnabled failed with exception. appId", zzap.zzbv(str4), localIllegalStateException);
          continue;
          i = 0;
          continue;
          this.zzagy = 0;
          return;
        }
        if (Build.VERSION.SDK_INT < 16) {
          continue;
        }
        if (!InstantApps.isInstantApp(getContext())) {
          continue;
        }
        i = k;
        this.zzagy = i;
        return;
        try
        {
          localObject1 = ((PackageManager)localObject6).getInstallerPackageName(str4);
          localObject3 = localObject1;
          if (localObject3 == null)
          {
            localObject1 = "manual_install";
            localObject5 = str1;
            str2 = str3;
          }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          try
          {
            PackageInfo localPackageInfo = ((PackageManager)localObject6).getPackageInfo(getContext().getPackageName(), 0);
            localObject3 = str1;
            i = j;
            str2 = str3;
            localObject5 = localObject1;
            if (localPackageInfo == null) {
              break;
            }
            localObject5 = str1;
            str2 = str3;
            localObject6 = ((PackageManager)localObject6).getApplicationLabel(localPackageInfo.applicationInfo);
            localObject3 = str1;
            localObject5 = str1;
            str2 = str3;
            if (!TextUtils.isEmpty((CharSequence)localObject6))
            {
              localObject5 = str1;
              str2 = str3;
              localObject3 = ((CharSequence)localObject6).toString();
            }
            localObject5 = localObject3;
            str2 = str3;
            str1 = localPackageInfo.versionName;
            localObject5 = localObject3;
            str2 = str1;
            i = localPackageInfo.versionCode;
            str2 = str1;
            localObject5 = localObject1;
          }
          catch (PackageManager.NameNotFoundException localNameNotFoundException)
          {
            zzgo().zzjd().zze("Error retrieving package info. appId, appName", zzap.zzbv(str4), localObject5);
            localObject4 = localObject5;
            i = j;
            localObject5 = localObject2;
          }
          localIllegalArgumentException = localIllegalArgumentException;
          zzgo().zzjd().zzg("Error retrieving app installer package name. appId", zzap.zzbv(str4));
          continue;
          localObject2 = localObject3;
          if (!"com.android.vending".equals(localObject3)) {
            continue;
          }
          localObject2 = "";
          continue;
        }
        break;
        i = 0;
        break label139;
        j = 0;
        break label170;
        zzgo().zzjd().zze("GoogleService failed to initialize, status", Integer.valueOf(((Status)localObject2).getStatusCode()), ((Status)localObject2).getStatusMessage());
        break label196;
        if ((localObject2 != null) && (!((Boolean)localObject2).booleanValue()))
        {
          if (!this.zzadj.zzkj()) {
            break label825;
          }
          zzgo().zzjj().zzbx("Collection disabled with firebase_analytics_collection_enabled=0");
          i = 0;
          break label244;
        }
        if ((localObject2 == null) && (GoogleServices.isMeasurementExplicitlyDisabled()))
        {
          zzgo().zzjj().zzbx("Collection disabled with google_app_measurement_enable=0");
          i = 0;
          break label244;
        }
        zzgo().zzjl().zzbx("Collection enabled");
        i = 1;
        break label244;
        localObject2 = localObject4;
      }
      label825:
      i = 0;
    }
  }
  
  final String zzgw()
  {
    zzcl();
    return this.zzagk;
  }
  
  final int zzja()
  {
    zzcl();
    return this.zzalo;
  }
  
  final int zzjb()
  {
    zzcl();
    return this.zzagy;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzaj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */