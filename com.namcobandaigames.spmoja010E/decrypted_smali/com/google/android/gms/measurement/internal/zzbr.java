package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicLong;

final class zzbr<V>
  extends FutureTask<V>
  implements Comparable<zzbr>
{
  private final String zzapf;
  private final long zzaph;
  final boolean zzapi;
  
  zzbr(zzbo paramzzbo, Runnable paramRunnable, boolean paramBoolean, String paramString)
  {
    super(paramRunnable, null);
    Preconditions.checkNotNull(paramString);
    this.zzaph = zzbo.zzkd().getAndIncrement();
    this.zzapf = paramString;
    this.zzapi = false;
    if (this.zzaph == Long.MAX_VALUE) {
      paramzzbo.zzgo().zzjd().zzbx("Tasks index overflow");
    }
  }
  
  zzbr(Callable<V> paramCallable, boolean paramBoolean, String paramString)
  {
    super(paramBoolean);
    Object localObject;
    Preconditions.checkNotNull(localObject);
    this.zzaph = zzbo.zzkd().getAndIncrement();
    this.zzapf = ((String)localObject);
    this.zzapi = paramString;
    if (this.zzaph == Long.MAX_VALUE) {
      paramCallable.zzgo().zzjd().zzbx("Tasks index overflow");
    }
  }
  
  protected final void setException(Throwable paramThrowable)
  {
    this.zzapg.zzgo().zzjd().zzg(this.zzapf, paramThrowable);
    if ((paramThrowable instanceof zzbp)) {
      Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), paramThrowable);
    }
    super.setException(paramThrowable);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */