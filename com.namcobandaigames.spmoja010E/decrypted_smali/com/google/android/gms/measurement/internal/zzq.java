package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzfu;
import com.google.android.gms.internal.measurement.zzfv;
import com.google.android.gms.internal.measurement.zzfy;
import com.google.android.gms.internal.measurement.zzgf;
import com.google.android.gms.internal.measurement.zzgg;
import com.google.android.gms.internal.measurement.zzgi;
import com.google.android.gms.internal.measurement.zzyy;
import com.google.android.gms.internal.measurement.zzzg;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class zzq
  extends zzez
{
  private static final String[] zzahi = { "last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;" };
  private static final String[] zzahj = { "origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;" };
  private static final String[] zzahk = { "app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;" };
  private static final String[] zzahl = { "realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;" };
  private static final String[] zzahm = { "has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;" };
  private static final String[] zzahn = { "previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;" };
  private final zzt zzaho = new zzt(this, getContext(), "google_app_measurement.db");
  private final zzev zzahp = new zzev(zzbx());
  
  zzq(zzfa paramzzfa)
  {
    super(paramzzfa);
  }
  
  @WorkerThread
  private final long zza(String paramString, String[] paramArrayOfString)
  {
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    Object localObject = null;
    String[] arrayOfString = null;
    try
    {
      paramArrayOfString = localSQLiteDatabase.rawQuery(paramString, paramArrayOfString);
      arrayOfString = paramArrayOfString;
      localObject = paramArrayOfString;
      if (paramArrayOfString.moveToFirst())
      {
        arrayOfString = paramArrayOfString;
        localObject = paramArrayOfString;
        long l = paramArrayOfString.getLong(0);
        if (paramArrayOfString != null) {
          paramArrayOfString.close();
        }
        return l;
      }
      arrayOfString = paramArrayOfString;
      localObject = paramArrayOfString;
      throw new SQLiteException("Database returned empty set");
    }
    catch (SQLiteException paramArrayOfString)
    {
      localObject = arrayOfString;
      zzgo().zzjd().zze("Database error", paramString, paramArrayOfString);
      localObject = arrayOfString;
      throw paramArrayOfString;
    }
    finally
    {
      if (localObject != null) {
        ((Cursor)localObject).close();
      }
    }
  }
  
  @WorkerThread
  private final long zza(String paramString, String[] paramArrayOfString, long paramLong)
  {
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    Object localObject = null;
    String[] arrayOfString = null;
    try
    {
      paramArrayOfString = localSQLiteDatabase.rawQuery(paramString, paramArrayOfString);
      arrayOfString = paramArrayOfString;
      localObject = paramArrayOfString;
      long l;
      if (paramArrayOfString.moveToFirst())
      {
        arrayOfString = paramArrayOfString;
        localObject = paramArrayOfString;
        paramLong = paramArrayOfString.getLong(0);
        l = paramLong;
        if (paramArrayOfString != null)
        {
          paramArrayOfString.close();
          l = paramLong;
        }
      }
      do
      {
        return l;
        l = paramLong;
      } while (paramArrayOfString == null);
      paramArrayOfString.close();
      return paramLong;
    }
    catch (SQLiteException paramArrayOfString)
    {
      localObject = arrayOfString;
      zzgo().zzjd().zze("Database error", paramString, paramArrayOfString);
      localObject = arrayOfString;
      throw paramArrayOfString;
    }
    finally
    {
      if (localObject != null) {
        ((Cursor)localObject).close();
      }
    }
  }
  
  @WorkerThread
  @VisibleForTesting
  private final Object zza(Cursor paramCursor, int paramInt)
  {
    int i = paramCursor.getType(paramInt);
    switch (i)
    {
    default: 
      zzgo().zzjd().zzg("Loaded invalid unknown value type, ignoring it", Integer.valueOf(i));
      return null;
    case 0: 
      zzgo().zzjd().zzbx("Loaded invalid null value from database");
      return null;
    case 1: 
      return Long.valueOf(paramCursor.getLong(paramInt));
    case 2: 
      return Double.valueOf(paramCursor.getDouble(paramInt));
    case 3: 
      return paramCursor.getString(paramInt);
    }
    zzgo().zzjd().zzbx("Loaded invalid blob type value, ignoring it");
    return null;
  }
  
  @WorkerThread
  private static void zza(ContentValues paramContentValues, String paramString, Object paramObject)
  {
    Preconditions.checkNotEmpty(paramString);
    Preconditions.checkNotNull(paramObject);
    if ((paramObject instanceof String))
    {
      paramContentValues.put(paramString, (String)paramObject);
      return;
    }
    if ((paramObject instanceof Long))
    {
      paramContentValues.put(paramString, (Long)paramObject);
      return;
    }
    if ((paramObject instanceof Double))
    {
      paramContentValues.put(paramString, (Double)paramObject);
      return;
    }
    throw new IllegalArgumentException("Invalid value type");
  }
  
  @WorkerThread
  private final boolean zza(String paramString, int paramInt, zzfv paramzzfv)
  {
    zzcl();
    zzaf();
    Preconditions.checkNotEmpty(paramString);
    Preconditions.checkNotNull(paramzzfv);
    if (TextUtils.isEmpty(paramzzfv.zzavf))
    {
      zzgo().zzjg().zzd("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", zzap.zzbv(paramString), Integer.valueOf(paramInt), String.valueOf(paramzzfv.zzave));
      return false;
    }
    try
    {
      byte[] arrayOfByte = new byte[paramzzfv.zzvu()];
      Object localObject = zzyy.zzk(arrayOfByte, 0, arrayOfByte.length);
      paramzzfv.zza((zzyy)localObject);
      ((zzyy)localObject).zzyt();
      localObject = new ContentValues();
      ((ContentValues)localObject).put("app_id", paramString);
      ((ContentValues)localObject).put("audience_id", Integer.valueOf(paramInt));
      ((ContentValues)localObject).put("filter_id", paramzzfv.zzave);
      ((ContentValues)localObject).put("event_name", paramzzfv.zzavf);
      ((ContentValues)localObject).put("data", arrayOfByte);
      return false;
    }
    catch (IOException paramzzfv)
    {
      try
      {
        if (getWritableDatabase().insertWithOnConflict("event_filters", null, (ContentValues)localObject, 5) == -1L) {
          zzgo().zzjd().zzg("Failed to insert event filter (got -1). appId", zzap.zzbv(paramString));
        }
        return true;
      }
      catch (SQLiteException paramzzfv)
      {
        zzgo().zzjd().zze("Error storing event filter. appId", zzap.zzbv(paramString), paramzzfv);
      }
      paramzzfv = paramzzfv;
      zzgo().zzjd().zze("Configuration loss. Failed to serialize event filter. appId", zzap.zzbv(paramString), paramzzfv);
      return false;
    }
  }
  
  @WorkerThread
  private final boolean zza(String paramString, int paramInt, zzfy paramzzfy)
  {
    zzcl();
    zzaf();
    Preconditions.checkNotEmpty(paramString);
    Preconditions.checkNotNull(paramzzfy);
    if (TextUtils.isEmpty(paramzzfy.zzavu))
    {
      zzgo().zzjg().zzd("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", zzap.zzbv(paramString), Integer.valueOf(paramInt), String.valueOf(paramzzfy.zzave));
      return false;
    }
    try
    {
      byte[] arrayOfByte = new byte[paramzzfy.zzvu()];
      Object localObject = zzyy.zzk(arrayOfByte, 0, arrayOfByte.length);
      paramzzfy.zza((zzyy)localObject);
      ((zzyy)localObject).zzyt();
      localObject = new ContentValues();
      ((ContentValues)localObject).put("app_id", paramString);
      ((ContentValues)localObject).put("audience_id", Integer.valueOf(paramInt));
      ((ContentValues)localObject).put("filter_id", paramzzfy.zzave);
      ((ContentValues)localObject).put("property_name", paramzzfy.zzavu);
      ((ContentValues)localObject).put("data", arrayOfByte);
      try
      {
        if (getWritableDatabase().insertWithOnConflict("property_filters", null, (ContentValues)localObject, 5) == -1L)
        {
          zzgo().zzjd().zzg("Failed to insert property filter (got -1). appId", zzap.zzbv(paramString));
          return false;
        }
      }
      catch (SQLiteException paramzzfy)
      {
        zzgo().zzjd().zze("Error storing property filter. appId", zzap.zzbv(paramString), paramzzfy);
        return false;
      }
      return true;
    }
    catch (IOException paramzzfy)
    {
      zzgo().zzjd().zze("Configuration loss. Failed to serialize property filter. appId", zzap.zzbv(paramString), paramzzfy);
      return false;
    }
  }
  
  private final boolean zza(String paramString, List<Integer> paramList)
  {
    Preconditions.checkNotEmpty(paramString);
    zzcl();
    zzaf();
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    int j;
    label160:
    do
    {
      try
      {
        long l = zza("select count(1) from audience_filter_values where app_id=?", new String[] { paramString });
        j = Math.max(0, Math.min(2000, zzgq().zzb(paramString, zzaf.zzaki)));
        if (l <= j) {
          return false;
        }
      }
      catch (SQLiteException paramList)
      {
        zzgo().zzjd().zze("Database error querying filters. appId", zzap.zzbv(paramString), paramList);
        return false;
      }
      ArrayList localArrayList = new ArrayList();
      int i = 0;
      for (;;)
      {
        if (i >= paramList.size()) {
          break label160;
        }
        Integer localInteger = (Integer)paramList.get(i);
        if ((localInteger == null) || (!(localInteger instanceof Integer))) {
          break;
        }
        localArrayList.add(Integer.toString(localInteger.intValue()));
        i += 1;
      }
      paramList = TextUtils.join(",", localArrayList);
      paramList = String.valueOf(paramList).length() + 2 + "(" + paramList + ")";
    } while (localSQLiteDatabase.delete("audience_filter_values", String.valueOf(paramList).length() + 140 + "audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in " + paramList + " order by rowid desc limit -1 offset ?)", new String[] { paramString, Integer.toString(j) }) <= 0);
    return true;
  }
  
  private final boolean zzil()
  {
    return getContext().getDatabasePath("google_app_measurement.db").exists();
  }
  
  @WorkerThread
  public final void beginTransaction()
  {
    zzcl();
    getWritableDatabase().beginTransaction();
  }
  
  @WorkerThread
  public final void endTransaction()
  {
    zzcl();
    getWritableDatabase().endTransaction();
  }
  
  @WorkerThread
  @VisibleForTesting
  final SQLiteDatabase getWritableDatabase()
  {
    zzaf();
    try
    {
      SQLiteDatabase localSQLiteDatabase = this.zzaho.getWritableDatabase();
      return localSQLiteDatabase;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzgo().zzjg().zzg("Error opening database", localSQLiteException);
      throw localSQLiteException;
    }
  }
  
  @WorkerThread
  public final void setTransactionSuccessful()
  {
    zzcl();
    getWritableDatabase().setTransactionSuccessful();
  }
  
  public final long zza(zzgi paramzzgi)
    throws IOException
  {
    zzaf();
    zzcl();
    Preconditions.checkNotNull(paramzzgi);
    Preconditions.checkNotEmpty(paramzzgi.zztt);
    for (;;)
    {
      try
      {
        byte[] arrayOfByte = new byte[paramzzgi.zzvu()];
        Object localObject = zzyy.zzk(arrayOfByte, 0, arrayOfByte.length);
        paramzzgi.zza((zzyy)localObject);
        ((zzyy)localObject).zzyt();
        localObject = zzjo();
        Preconditions.checkNotNull(arrayOfByte);
        ((zzco)localObject).zzgm().zzaf();
        MessageDigest localMessageDigest = zzfk.getMessageDigest();
        if (localMessageDigest == null)
        {
          ((zzco)localObject).zzgo().zzjd().zzbx("Failed to get MD5");
          l = 0L;
          localObject = new ContentValues();
          ((ContentValues)localObject).put("app_id", paramzzgi.zztt);
          ((ContentValues)localObject).put("metadata_fingerprint", Long.valueOf(l));
          ((ContentValues)localObject).put("metadata", arrayOfByte);
        }
        long l = zzfk.zzc(localMessageDigest.digest(localIOException));
      }
      catch (IOException localIOException)
      {
        try
        {
          getWritableDatabase().insertWithOnConflict("raw_events_metadata", null, (ContentValues)localObject, 4);
          return l;
        }
        catch (SQLiteException localSQLiteException)
        {
          zzgo().zzjd().zze("Error storing raw event metadata. appId", zzap.zzbv(paramzzgi.zztt), localSQLiteException);
          throw localSQLiteException;
        }
        localIOException = localIOException;
        zzgo().zzjd().zze("Data loss. Failed to serialize event metadata. appId", zzap.zzbv(paramzzgi.zztt), localIOException);
        throw localIOException;
      }
    }
  }
  
  /* Error */
  public final android.util.Pair<zzgf, Long> zza(String paramString, Long paramLong)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 7
    //   3: aload_0
    //   4: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   7: aload_0
    //   8: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   11: aload_0
    //   12: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   15: ldc_w 598
    //   18: iconst_2
    //   19: anewarray 19	java/lang/String
    //   22: dup
    //   23: iconst_0
    //   24: aload_1
    //   25: aastore
    //   26: dup
    //   27: iconst_1
    //   28: aload_2
    //   29: invokestatic 355	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   32: aastore
    //   33: invokevirtual 209	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   36: astore 5
    //   38: aload 5
    //   40: astore 6
    //   42: aload 5
    //   44: invokeinterface 215 1 0
    //   49: ifne +40 -> 89
    //   52: aload 5
    //   54: astore 6
    //   56: aload_0
    //   57: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   60: invokevirtual 601	com/google/android/gms/measurement/internal/zzap:zzjl	()Lcom/google/android/gms/measurement/internal/zzar;
    //   63: ldc_w 603
    //   66: invokevirtual 270	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   69: aload 7
    //   71: astore_1
    //   72: aload 5
    //   74: ifnull +13 -> 87
    //   77: aload 5
    //   79: invokeinterface 222 1 0
    //   84: aload 7
    //   86: astore_1
    //   87: aload_1
    //   88: areturn
    //   89: aload 5
    //   91: astore 6
    //   93: aload 5
    //   95: iconst_0
    //   96: invokeinterface 607 2 0
    //   101: astore 8
    //   103: aload 5
    //   105: astore 6
    //   107: aload 5
    //   109: iconst_1
    //   110: invokeinterface 219 2 0
    //   115: lstore_3
    //   116: aload 5
    //   118: astore 6
    //   120: aload 8
    //   122: iconst_0
    //   123: aload 8
    //   125: arraylength
    //   126: invokestatic 613	com/google/android/gms/internal/measurement/zzyx:zzj	([BII)Lcom/google/android/gms/internal/measurement/zzyx;
    //   129: astore 8
    //   131: aload 5
    //   133: astore 6
    //   135: new 615	com/google/android/gms/internal/measurement/zzgf
    //   138: dup
    //   139: invokespecial 616	com/google/android/gms/internal/measurement/zzgf:<init>	()V
    //   142: astore 9
    //   144: aload 5
    //   146: astore 6
    //   148: aload 9
    //   150: aload 8
    //   152: invokevirtual 619	com/google/android/gms/internal/measurement/zzzg:zza	(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    //   155: pop
    //   156: aload 5
    //   158: astore 6
    //   160: aload 9
    //   162: lload_3
    //   163: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   166: invokestatic 625	android/util/Pair:create	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    //   169: astore_2
    //   170: aload_2
    //   171: astore_1
    //   172: aload 5
    //   174: ifnull -87 -> 87
    //   177: aload 5
    //   179: invokeinterface 222 1 0
    //   184: aload_2
    //   185: areturn
    //   186: astore 8
    //   188: aload 5
    //   190: astore 6
    //   192: aload_0
    //   193: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   196: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   199: ldc_w 627
    //   202: aload_1
    //   203: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   206: aload_2
    //   207: aload 8
    //   209: invokevirtual 359	com/google/android/gms/measurement/internal/zzar:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   212: aload 7
    //   214: astore_1
    //   215: aload 5
    //   217: ifnull -130 -> 87
    //   220: aload 5
    //   222: invokeinterface 222 1 0
    //   227: aconst_null
    //   228: areturn
    //   229: astore_1
    //   230: aconst_null
    //   231: astore 5
    //   233: aload 5
    //   235: astore 6
    //   237: aload_0
    //   238: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   241: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   244: ldc_w 629
    //   247: aload_1
    //   248: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   251: aload 7
    //   253: astore_1
    //   254: aload 5
    //   256: ifnull -169 -> 87
    //   259: aload 5
    //   261: invokeinterface 222 1 0
    //   266: aconst_null
    //   267: areturn
    //   268: astore_1
    //   269: aconst_null
    //   270: astore 6
    //   272: aload 6
    //   274: ifnull +10 -> 284
    //   277: aload 6
    //   279: invokeinterface 222 1 0
    //   284: aload_1
    //   285: athrow
    //   286: astore_1
    //   287: goto -15 -> 272
    //   290: astore_1
    //   291: goto -58 -> 233
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	294	0	this	zzq
    //   0	294	1	paramString	String
    //   0	294	2	paramLong	Long
    //   115	48	3	l	long
    //   36	224	5	localCursor1	Cursor
    //   40	238	6	localCursor2	Cursor
    //   1	251	7	localObject1	Object
    //   101	50	8	localObject2	Object
    //   186	22	8	localIOException	IOException
    //   142	19	9	localzzgf	zzgf
    // Exception table:
    //   from	to	target	type
    //   148	156	186	java/io/IOException
    //   11	38	229	android/database/sqlite/SQLiteException
    //   11	38	268	finally
    //   42	52	286	finally
    //   56	69	286	finally
    //   93	103	286	finally
    //   107	116	286	finally
    //   120	131	286	finally
    //   135	144	286	finally
    //   148	156	286	finally
    //   160	170	286	finally
    //   192	212	286	finally
    //   237	251	286	finally
    //   42	52	290	android/database/sqlite/SQLiteException
    //   56	69	290	android/database/sqlite/SQLiteException
    //   93	103	290	android/database/sqlite/SQLiteException
    //   107	116	290	android/database/sqlite/SQLiteException
    //   120	131	290	android/database/sqlite/SQLiteException
    //   135	144	290	android/database/sqlite/SQLiteException
    //   148	156	290	android/database/sqlite/SQLiteException
    //   160	170	290	android/database/sqlite/SQLiteException
    //   192	212	290	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  @WorkerThread
  public final zzr zza(long paramLong, String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    // Byte code:
    //   0: aload_3
    //   1: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   9: aload_0
    //   10: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   13: new 633	com/google/android/gms/measurement/internal/zzr
    //   16: dup
    //   17: invokespecial 634	com/google/android/gms/measurement/internal/zzr:<init>	()V
    //   20: astore 12
    //   22: aload_0
    //   23: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   26: astore 11
    //   28: aload 11
    //   30: ldc_w 636
    //   33: bipush 6
    //   35: anewarray 19	java/lang/String
    //   38: dup
    //   39: iconst_0
    //   40: ldc 73
    //   42: aastore
    //   43: dup
    //   44: iconst_1
    //   45: ldc 81
    //   47: aastore
    //   48: dup
    //   49: iconst_2
    //   50: ldc 77
    //   52: aastore
    //   53: dup
    //   54: iconst_3
    //   55: ldc 85
    //   57: aastore
    //   58: dup
    //   59: iconst_4
    //   60: ldc 109
    //   62: aastore
    //   63: dup
    //   64: iconst_5
    //   65: ldc 113
    //   67: aastore
    //   68: ldc_w 638
    //   71: iconst_1
    //   72: anewarray 19	java/lang/String
    //   75: dup
    //   76: iconst_0
    //   77: aload_3
    //   78: aastore
    //   79: aconst_null
    //   80: aconst_null
    //   81: aconst_null
    //   82: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   85: astore 10
    //   87: aload 10
    //   89: astore 9
    //   91: aload 10
    //   93: invokeinterface 215 1 0
    //   98: ifne +39 -> 137
    //   101: aload 10
    //   103: astore 9
    //   105: aload_0
    //   106: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   109: invokevirtual 342	com/google/android/gms/measurement/internal/zzap:zzjg	()Lcom/google/android/gms/measurement/internal/zzar;
    //   112: ldc_w 644
    //   115: aload_3
    //   116: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   119: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   122: aload 10
    //   124: ifnull +10 -> 134
    //   127: aload 10
    //   129: invokeinterface 222 1 0
    //   134: aload 12
    //   136: areturn
    //   137: aload 10
    //   139: astore 9
    //   141: aload 10
    //   143: iconst_0
    //   144: invokeinterface 219 2 0
    //   149: lload_1
    //   150: lcmp
    //   151: ifne +88 -> 239
    //   154: aload 10
    //   156: astore 9
    //   158: aload 12
    //   160: aload 10
    //   162: iconst_1
    //   163: invokeinterface 219 2 0
    //   168: putfield 648	com/google/android/gms/measurement/internal/zzr:zzahr	J
    //   171: aload 10
    //   173: astore 9
    //   175: aload 12
    //   177: aload 10
    //   179: iconst_2
    //   180: invokeinterface 219 2 0
    //   185: putfield 651	com/google/android/gms/measurement/internal/zzr:zzahq	J
    //   188: aload 10
    //   190: astore 9
    //   192: aload 12
    //   194: aload 10
    //   196: iconst_3
    //   197: invokeinterface 219 2 0
    //   202: putfield 654	com/google/android/gms/measurement/internal/zzr:zzahs	J
    //   205: aload 10
    //   207: astore 9
    //   209: aload 12
    //   211: aload 10
    //   213: iconst_4
    //   214: invokeinterface 219 2 0
    //   219: putfield 657	com/google/android/gms/measurement/internal/zzr:zzaht	J
    //   222: aload 10
    //   224: astore 9
    //   226: aload 12
    //   228: aload 10
    //   230: iconst_5
    //   231: invokeinterface 219 2 0
    //   236: putfield 660	com/google/android/gms/measurement/internal/zzr:zzahu	J
    //   239: iload 4
    //   241: ifeq +19 -> 260
    //   244: aload 10
    //   246: astore 9
    //   248: aload 12
    //   250: aload 12
    //   252: getfield 648	com/google/android/gms/measurement/internal/zzr:zzahr	J
    //   255: lconst_1
    //   256: ladd
    //   257: putfield 648	com/google/android/gms/measurement/internal/zzr:zzahr	J
    //   260: iload 5
    //   262: ifeq +19 -> 281
    //   265: aload 10
    //   267: astore 9
    //   269: aload 12
    //   271: aload 12
    //   273: getfield 651	com/google/android/gms/measurement/internal/zzr:zzahq	J
    //   276: lconst_1
    //   277: ladd
    //   278: putfield 651	com/google/android/gms/measurement/internal/zzr:zzahq	J
    //   281: iload 6
    //   283: ifeq +19 -> 302
    //   286: aload 10
    //   288: astore 9
    //   290: aload 12
    //   292: aload 12
    //   294: getfield 654	com/google/android/gms/measurement/internal/zzr:zzahs	J
    //   297: lconst_1
    //   298: ladd
    //   299: putfield 654	com/google/android/gms/measurement/internal/zzr:zzahs	J
    //   302: iload 7
    //   304: ifeq +19 -> 323
    //   307: aload 10
    //   309: astore 9
    //   311: aload 12
    //   313: aload 12
    //   315: getfield 657	com/google/android/gms/measurement/internal/zzr:zzaht	J
    //   318: lconst_1
    //   319: ladd
    //   320: putfield 657	com/google/android/gms/measurement/internal/zzr:zzaht	J
    //   323: iload 8
    //   325: ifeq +19 -> 344
    //   328: aload 10
    //   330: astore 9
    //   332: aload 12
    //   334: aload 12
    //   336: getfield 660	com/google/android/gms/measurement/internal/zzr:zzahu	J
    //   339: lconst_1
    //   340: ladd
    //   341: putfield 660	com/google/android/gms/measurement/internal/zzr:zzahu	J
    //   344: aload 10
    //   346: astore 9
    //   348: new 303	android/content/ContentValues
    //   351: dup
    //   352: invokespecial 379	android/content/ContentValues:<init>	()V
    //   355: astore 13
    //   357: aload 10
    //   359: astore 9
    //   361: aload 13
    //   363: ldc 73
    //   365: lload_1
    //   366: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   369: invokevirtual 310	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   372: aload 10
    //   374: astore 9
    //   376: aload 13
    //   378: ldc 77
    //   380: aload 12
    //   382: getfield 651	com/google/android/gms/measurement/internal/zzr:zzahq	J
    //   385: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   388: invokevirtual 310	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   391: aload 10
    //   393: astore 9
    //   395: aload 13
    //   397: ldc 81
    //   399: aload 12
    //   401: getfield 648	com/google/android/gms/measurement/internal/zzr:zzahr	J
    //   404: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   407: invokevirtual 310	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   410: aload 10
    //   412: astore 9
    //   414: aload 13
    //   416: ldc 85
    //   418: aload 12
    //   420: getfield 654	com/google/android/gms/measurement/internal/zzr:zzahs	J
    //   423: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   426: invokevirtual 310	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   429: aload 10
    //   431: astore 9
    //   433: aload 13
    //   435: ldc 109
    //   437: aload 12
    //   439: getfield 657	com/google/android/gms/measurement/internal/zzr:zzaht	J
    //   442: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   445: invokevirtual 310	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   448: aload 10
    //   450: astore 9
    //   452: aload 13
    //   454: ldc 113
    //   456: aload 12
    //   458: getfield 660	com/google/android/gms/measurement/internal/zzr:zzahu	J
    //   461: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   464: invokevirtual 310	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   467: aload 10
    //   469: astore 9
    //   471: aload 11
    //   473: ldc_w 636
    //   476: aload 13
    //   478: ldc_w 638
    //   481: iconst_1
    //   482: anewarray 19	java/lang/String
    //   485: dup
    //   486: iconst_0
    //   487: aload_3
    //   488: aastore
    //   489: invokevirtual 664	android/database/sqlite/SQLiteDatabase:update	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   492: pop
    //   493: aload 10
    //   495: ifnull +10 -> 505
    //   498: aload 10
    //   500: invokeinterface 222 1 0
    //   505: aload 12
    //   507: areturn
    //   508: astore 11
    //   510: aconst_null
    //   511: astore 10
    //   513: aload 10
    //   515: astore 9
    //   517: aload_0
    //   518: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   521: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   524: ldc_w 666
    //   527: aload_3
    //   528: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   531: aload 11
    //   533: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   536: aload 10
    //   538: ifnull +10 -> 548
    //   541: aload 10
    //   543: invokeinterface 222 1 0
    //   548: aload 12
    //   550: areturn
    //   551: astore_3
    //   552: aconst_null
    //   553: astore 9
    //   555: aload 9
    //   557: ifnull +10 -> 567
    //   560: aload 9
    //   562: invokeinterface 222 1 0
    //   567: aload_3
    //   568: athrow
    //   569: astore_3
    //   570: goto -15 -> 555
    //   573: astore 11
    //   575: goto -62 -> 513
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	578	0	this	zzq
    //   0	578	1	paramLong	long
    //   0	578	3	paramString	String
    //   0	578	4	paramBoolean1	boolean
    //   0	578	5	paramBoolean2	boolean
    //   0	578	6	paramBoolean3	boolean
    //   0	578	7	paramBoolean4	boolean
    //   0	578	8	paramBoolean5	boolean
    //   89	472	9	localCursor1	Cursor
    //   85	457	10	localCursor2	Cursor
    //   26	446	11	localSQLiteDatabase	SQLiteDatabase
    //   508	24	11	localSQLiteException1	SQLiteException
    //   573	1	11	localSQLiteException2	SQLiteException
    //   20	529	12	localzzr	zzr
    //   355	122	13	localContentValues	ContentValues
    // Exception table:
    //   from	to	target	type
    //   22	87	508	android/database/sqlite/SQLiteException
    //   22	87	551	finally
    //   91	101	569	finally
    //   105	122	569	finally
    //   141	154	569	finally
    //   158	171	569	finally
    //   175	188	569	finally
    //   192	205	569	finally
    //   209	222	569	finally
    //   226	239	569	finally
    //   248	260	569	finally
    //   269	281	569	finally
    //   290	302	569	finally
    //   311	323	569	finally
    //   332	344	569	finally
    //   348	357	569	finally
    //   361	372	569	finally
    //   376	391	569	finally
    //   395	410	569	finally
    //   414	429	569	finally
    //   433	448	569	finally
    //   452	467	569	finally
    //   471	493	569	finally
    //   517	536	569	finally
    //   91	101	573	android/database/sqlite/SQLiteException
    //   105	122	573	android/database/sqlite/SQLiteException
    //   141	154	573	android/database/sqlite/SQLiteException
    //   158	171	573	android/database/sqlite/SQLiteException
    //   175	188	573	android/database/sqlite/SQLiteException
    //   192	205	573	android/database/sqlite/SQLiteException
    //   209	222	573	android/database/sqlite/SQLiteException
    //   226	239	573	android/database/sqlite/SQLiteException
    //   248	260	573	android/database/sqlite/SQLiteException
    //   269	281	573	android/database/sqlite/SQLiteException
    //   290	302	573	android/database/sqlite/SQLiteException
    //   311	323	573	android/database/sqlite/SQLiteException
    //   332	344	573	android/database/sqlite/SQLiteException
    //   348	357	573	android/database/sqlite/SQLiteException
    //   361	372	573	android/database/sqlite/SQLiteException
    //   376	391	573	android/database/sqlite/SQLiteException
    //   395	410	573	android/database/sqlite/SQLiteException
    //   414	429	573	android/database/sqlite/SQLiteException
    //   433	448	573	android/database/sqlite/SQLiteException
    //   452	467	573	android/database/sqlite/SQLiteException
    //   471	493	573	android/database/sqlite/SQLiteException
  }
  
  @WorkerThread
  public final void zza(zzg paramzzg)
  {
    Preconditions.checkNotNull(paramzzg);
    zzaf();
    zzcl();
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("app_id", paramzzg.zzal());
    localContentValues.put("app_instance_id", paramzzg.getAppInstanceId());
    localContentValues.put("gmp_app_id", paramzzg.getGmpAppId());
    localContentValues.put("resettable_device_id_hash", paramzzg.zzgx());
    localContentValues.put("last_bundle_index", Long.valueOf(paramzzg.zzhe()));
    localContentValues.put("last_bundle_start_timestamp", Long.valueOf(paramzzg.zzgy()));
    localContentValues.put("last_bundle_end_timestamp", Long.valueOf(paramzzg.zzgz()));
    localContentValues.put("app_version", paramzzg.zzak());
    localContentValues.put("app_store", paramzzg.zzhb());
    localContentValues.put("gmp_version", Long.valueOf(paramzzg.zzhc()));
    localContentValues.put("dev_cert_hash", Long.valueOf(paramzzg.zzhd()));
    localContentValues.put("measurement_enabled", Boolean.valueOf(paramzzg.isMeasurementEnabled()));
    localContentValues.put("day", Long.valueOf(paramzzg.zzhi()));
    localContentValues.put("daily_public_events_count", Long.valueOf(paramzzg.zzhj()));
    localContentValues.put("daily_events_count", Long.valueOf(paramzzg.zzhk()));
    localContentValues.put("daily_conversions_count", Long.valueOf(paramzzg.zzhl()));
    localContentValues.put("config_fetched_time", Long.valueOf(paramzzg.zzhf()));
    localContentValues.put("failed_config_fetch_time", Long.valueOf(paramzzg.zzhg()));
    localContentValues.put("app_version_int", Long.valueOf(paramzzg.zzha()));
    localContentValues.put("firebase_instance_id", paramzzg.getFirebaseInstanceId());
    localContentValues.put("daily_error_events_count", Long.valueOf(paramzzg.zzhn()));
    localContentValues.put("daily_realtime_events_count", Long.valueOf(paramzzg.zzhm()));
    localContentValues.put("health_monitor_sample", paramzzg.zzho());
    localContentValues.put("android_id", Long.valueOf(paramzzg.zzhq()));
    localContentValues.put("adid_reporting_enabled", Boolean.valueOf(paramzzg.zzhr()));
    localContentValues.put("ssaid_reporting_enabled", Boolean.valueOf(paramzzg.zzhs()));
    localContentValues.put("admob_app_id", paramzzg.zzgw());
    try
    {
      SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
      if ((localSQLiteDatabase.update("apps", localContentValues, "app_id = ?", new String[] { paramzzg.zzal() }) == 0L) && (localSQLiteDatabase.insertWithOnConflict("apps", null, localContentValues, 5) == -1L)) {
        zzgo().zzjd().zzg("Failed to insert/update app (got -1). appId", zzap.zzbv(paramzzg.zzal()));
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzgo().zzjd().zze("Error storing app. appId", zzap.zzbv(paramzzg.zzal()), localSQLiteException);
    }
  }
  
  @WorkerThread
  public final void zza(zzz paramzzz)
  {
    Object localObject2 = null;
    Preconditions.checkNotNull(paramzzz);
    zzaf();
    zzcl();
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("app_id", paramzzz.zztt);
    localContentValues.put("name", paramzzz.name);
    localContentValues.put("lifetime_count", Long.valueOf(paramzzz.zzaie));
    localContentValues.put("current_bundle_count", Long.valueOf(paramzzz.zzaif));
    localContentValues.put("last_fire_timestamp", Long.valueOf(paramzzz.zzaig));
    localContentValues.put("last_bundled_timestamp", Long.valueOf(paramzzz.zzaih));
    localContentValues.put("last_bundled_day", paramzzz.zzaii);
    localContentValues.put("last_sampled_complex_event_id", paramzzz.zzaij);
    localContentValues.put("last_sampling_rate", paramzzz.zzaik);
    Object localObject1 = localObject2;
    if (paramzzz.zzail != null)
    {
      localObject1 = localObject2;
      if (paramzzz.zzail.booleanValue()) {
        localObject1 = Long.valueOf(1L);
      }
    }
    localContentValues.put("last_exempt_from_sampling", (Long)localObject1);
    try
    {
      if (getWritableDatabase().insertWithOnConflict("events", null, localContentValues, 5) == -1L) {
        zzgo().zzjd().zzg("Failed to insert/update event aggregates (got -1). appId", zzap.zzbv(paramzzz.zztt));
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzgo().zzjd().zze("Error storing event aggregates. appId", zzap.zzbv(paramzzz.zztt), localSQLiteException);
    }
  }
  
  @WorkerThread
  final void zza(String paramString, zzfu[] paramArrayOfzzfu)
  {
    int n = 0;
    zzcl();
    zzaf();
    Preconditions.checkNotEmpty(paramString);
    Preconditions.checkNotNull(paramArrayOfzzfu);
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    localSQLiteDatabase.beginTransaction();
    Object localObject1;
    int j;
    int i2;
    for (;;)
    {
      try
      {
        zzcl();
        zzaf();
        Preconditions.checkNotEmpty(paramString);
        localObject1 = getWritableDatabase();
        ((SQLiteDatabase)localObject1).delete("property_filters", "app_id=?", new String[] { paramString });
        ((SQLiteDatabase)localObject1).delete("event_filters", "app_id=?", new String[] { paramString });
        int i1 = paramArrayOfzzfu.length;
        j = 0;
        if (j >= i1) {
          break label480;
        }
        localObject1 = paramArrayOfzzfu[j];
        zzcl();
        zzaf();
        Preconditions.checkNotEmpty(paramString);
        Preconditions.checkNotNull(localObject1);
        Preconditions.checkNotNull(((zzfu)localObject1).zzava);
        Preconditions.checkNotNull(((zzfu)localObject1).zzauz);
        if (((zzfu)localObject1).zzauy == null)
        {
          zzgo().zzjg().zzg("Audience with no ID. appId", zzap.zzbv(paramString));
        }
        else
        {
          i2 = ((zzfu)localObject1).zzauy.intValue();
          localObject2 = ((zzfu)localObject1).zzava;
          k = localObject2.length;
          i = 0;
          if (i < k) {
            if (localObject2[i].zzave == null) {
              zzgo().zzjg().zze("Event filter with no ID. Audience definition ignored. appId, audienceId", zzap.zzbv(paramString), ((zzfu)localObject1).zzauy);
            }
          }
        }
      }
      finally
      {
        localSQLiteDatabase.endTransaction();
      }
      i += 1;
    }
    Object localObject2 = ((zzfu)localObject1).zzauz;
    int k = localObject2.length;
    int i = 0;
    label270:
    label325:
    label347:
    int m;
    int i3;
    if (i < k)
    {
      if (localObject2[i].zzave != null) {
        break label556;
      }
      zzgo().zzjg().zze("Property filter with no ID. Audience definition ignored. appId, audienceId", zzap.zzbv(paramString), ((zzfu)localObject1).zzauy);
    }
    else
    {
      localObject2 = ((zzfu)localObject1).zzava;
      k = localObject2.length;
      i = 0;
      if (i < k)
      {
        if (zza(paramString, i2, localObject2[i])) {
          break label563;
        }
        i = 0;
        m = i;
        if (i != 0)
        {
          localObject1 = ((zzfu)localObject1).zzauz;
          i3 = localObject1.length;
          k = 0;
        }
      }
    }
    for (;;)
    {
      m = i;
      if (k < i3)
      {
        if (!zza(paramString, i2, localObject1[k])) {
          m = 0;
        }
      }
      else
      {
        if (m == 0)
        {
          zzcl();
          zzaf();
          Preconditions.checkNotEmpty(paramString);
          localObject1 = getWritableDatabase();
          ((SQLiteDatabase)localObject1).delete("property_filters", "app_id=? and audience_id=?", new String[] { paramString, String.valueOf(i2) });
          ((SQLiteDatabase)localObject1).delete("event_filters", "app_id=? and audience_id=?", new String[] { paramString, String.valueOf(i2) });
          break label547;
          label480:
          localObject1 = new ArrayList();
          j = paramArrayOfzzfu.length;
          i = n;
          while (i < j)
          {
            ((List)localObject1).add(paramArrayOfzzfu[i].zzauy);
            i += 1;
          }
          zza(paramString, (List)localObject1);
          localSQLiteDatabase.setTransactionSuccessful();
          localSQLiteDatabase.endTransaction();
          return;
          i = 1;
          break label347;
        }
        label547:
        j += 1;
        break;
        label556:
        i += 1;
        break label270;
        label563:
        i += 1;
        break label325;
      }
      k += 1;
    }
  }
  
  @WorkerThread
  public final boolean zza(zzgi paramzzgi, boolean paramBoolean)
  {
    zzaf();
    zzcl();
    Preconditions.checkNotNull(paramzzgi);
    Preconditions.checkNotEmpty(paramzzgi.zztt);
    Preconditions.checkNotNull(paramzzgi.zzaxf);
    zzif();
    long l = zzbx().currentTimeMillis();
    if ((paramzzgi.zzaxf.longValue() < l - zzn.zzhw()) || (paramzzgi.zzaxf.longValue() > zzn.zzhw() + l)) {
      zzgo().zzjg().zzd("Storing bundle outside of the max uploading time span. appId, now, timestamp", zzap.zzbv(paramzzgi.zztt), Long.valueOf(l), paramzzgi.zzaxf);
    }
    for (;;)
    {
      try
      {
        byte[] arrayOfByte = new byte[paramzzgi.zzvu()];
        Object localObject = zzyy.zzk(arrayOfByte, 0, arrayOfByte.length);
        paramzzgi.zza((zzyy)localObject);
        ((zzyy)localObject).zzyt();
        arrayOfByte = zzjo().zzb(arrayOfByte);
        zzgo().zzjl().zzg("Saving bundle, size", Integer.valueOf(arrayOfByte.length));
        localObject = new ContentValues();
        ((ContentValues)localObject).put("app_id", paramzzgi.zztt);
        ((ContentValues)localObject).put("bundle_end_timestamp", paramzzgi.zzaxf);
        ((ContentValues)localObject).put("data", arrayOfByte);
        if (paramBoolean)
        {
          i = 1;
          ((ContentValues)localObject).put("has_realtime", Integer.valueOf(i));
          if (paramzzgi.zzayc != null) {
            ((ContentValues)localObject).put("retry_count", paramzzgi.zzayc);
          }
        }
        int i = 0;
      }
      catch (IOException localIOException)
      {
        try
        {
          if (getWritableDatabase().insert("queue", null, (ContentValues)localObject) != -1L) {
            break;
          }
          zzgo().zzjd().zzg("Failed to insert bundle (got -1). appId", zzap.zzbv(paramzzgi.zztt));
          return false;
        }
        catch (SQLiteException localSQLiteException)
        {
          zzgo().zzjd().zze("Error storing bundle. appId", zzap.zzbv(paramzzgi.zztt), localSQLiteException);
          return false;
        }
        localIOException = localIOException;
        zzgo().zzjd().zze("Data loss. Failed to serialize bundle. appId", zzap.zzbv(paramzzgi.zztt), localIOException);
        return false;
      }
    }
    return true;
  }
  
  @WorkerThread
  public final boolean zza(zzfj paramzzfj)
  {
    Preconditions.checkNotNull(paramzzfj);
    zzaf();
    zzcl();
    long l;
    if (zzi(paramzzfj.zztt, paramzzfj.name) == null) {
      if (zzfk.zzcq(paramzzfj.name))
      {
        if (zza("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[] { paramzzfj.zztt }) < 25L) {}
      }
      else {
        do
        {
          return false;
          l = zza("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[] { paramzzfj.zztt, paramzzfj.origin });
          if (!zzgq().zze(paramzzfj.zztt, zzaf.zzalj)) {
            break;
          }
        } while ((!"_ap".equals(paramzzfj.name)) && (l >= 25L));
      }
    }
    for (;;)
    {
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("app_id", paramzzfj.zztt);
      localContentValues.put("origin", paramzzfj.origin);
      localContentValues.put("name", paramzzfj.name);
      localContentValues.put("set_timestamp", Long.valueOf(paramzzfj.zzaue));
      zza(localContentValues, "value", paramzzfj.value);
      try
      {
        if (getWritableDatabase().insertWithOnConflict("user_attributes", null, localContentValues, 5) == -1L) {
          zzgo().zzjd().zzg("Failed to insert/update user property (got -1). appId", zzap.zzbv(paramzzfj.zztt));
        }
        return true;
        if (l < 25L) {
          continue;
        }
        return false;
      }
      catch (SQLiteException localSQLiteException)
      {
        for (;;)
        {
          zzgo().zzjd().zze("Error storing user property. appId", zzap.zzbv(paramzzfj.zztt), localSQLiteException);
        }
      }
    }
  }
  
  @WorkerThread
  public final boolean zza(zzl paramzzl)
  {
    Preconditions.checkNotNull(paramzzl);
    zzaf();
    zzcl();
    if (zzi(paramzzl.packageName, paramzzl.zzahb.name) == null) {
      if (zza("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[] { paramzzl.packageName }) >= 1000L) {
        return false;
      }
    }
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("app_id", paramzzl.packageName);
    localContentValues.put("origin", paramzzl.origin);
    localContentValues.put("name", paramzzl.zzahb.name);
    zza(localContentValues, "value", paramzzl.zzahb.getValue());
    localContentValues.put("active", Boolean.valueOf(paramzzl.active));
    localContentValues.put("trigger_event_name", paramzzl.triggerEventName);
    localContentValues.put("trigger_timeout", Long.valueOf(paramzzl.triggerTimeout));
    zzgm();
    localContentValues.put("timed_out_event", zzfk.zza(paramzzl.zzahc));
    localContentValues.put("creation_timestamp", Long.valueOf(paramzzl.creationTimestamp));
    zzgm();
    localContentValues.put("triggered_event", zzfk.zza(paramzzl.zzahd));
    localContentValues.put("triggered_timestamp", Long.valueOf(paramzzl.zzahb.zzaue));
    localContentValues.put("time_to_live", Long.valueOf(paramzzl.timeToLive));
    zzgm();
    localContentValues.put("expired_event", zzfk.zza(paramzzl.zzahe));
    try
    {
      if (getWritableDatabase().insertWithOnConflict("conditional_properties", null, localContentValues, 5) == -1L) {
        zzgo().zzjd().zzg("Failed to insert/update conditional user property (got -1)", zzap.zzbv(paramzzl.packageName));
      }
      return true;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        zzgo().zzjd().zze("Error storing conditional user property", zzap.zzbv(paramzzl.packageName), localSQLiteException);
      }
    }
  }
  
  public final boolean zza(zzy paramzzy, long paramLong, boolean paramBoolean)
  {
    zzaf();
    zzcl();
    Preconditions.checkNotNull(paramzzy);
    Preconditions.checkNotEmpty(paramzzy.zztt);
    Object localObject1 = new zzgf();
    ((zzgf)localObject1).zzawv = Long.valueOf(paramzzy.zzaic);
    ((zzgf)localObject1).zzawt = new zzgg[paramzzy.zzaid.size()];
    Object localObject2 = paramzzy.zzaid.iterator();
    int i = 0;
    Object localObject3;
    while (((Iterator)localObject2).hasNext())
    {
      Object localObject4 = (String)((Iterator)localObject2).next();
      localObject3 = new zzgg();
      ((zzgf)localObject1).zzawt[i] = localObject3;
      ((zzgg)localObject3).name = ((String)localObject4);
      localObject4 = paramzzy.zzaid.get((String)localObject4);
      zzjo().zza((zzgg)localObject3, localObject4);
      i += 1;
    }
    for (;;)
    {
      try
      {
        localObject2 = new byte[((zzzg)localObject1).zzvu()];
        localObject3 = zzyy.zzk((byte[])localObject2, 0, localObject2.length);
        ((zzzg)localObject1).zza((zzyy)localObject3);
        ((zzyy)localObject3).zzyt();
        zzgo().zzjl().zze("Saving event, name, data size", zzgl().zzbs(paramzzy.name), Integer.valueOf(localObject2.length));
        localObject1 = new ContentValues();
        ((ContentValues)localObject1).put("app_id", paramzzy.zztt);
        ((ContentValues)localObject1).put("name", paramzzy.name);
        ((ContentValues)localObject1).put("timestamp", Long.valueOf(paramzzy.timestamp));
        ((ContentValues)localObject1).put("metadata_fingerprint", Long.valueOf(paramLong));
        ((ContentValues)localObject1).put("data", (byte[])localObject2);
        if (paramBoolean)
        {
          i = 1;
          ((ContentValues)localObject1).put("realtime", Integer.valueOf(i));
        }
        i = 0;
      }
      catch (IOException localIOException)
      {
        try
        {
          if (getWritableDatabase().insert("raw_events", null, (ContentValues)localObject1) != -1L) {
            break;
          }
          zzgo().zzjd().zzg("Failed to insert raw event (got -1). appId", zzap.zzbv(paramzzy.zztt));
          return false;
        }
        catch (SQLiteException localSQLiteException)
        {
          zzgo().zzjd().zze("Error storing raw event. appId", zzap.zzbv(paramzzy.zztt), localSQLiteException);
          return false;
        }
        localIOException = localIOException;
        zzgo().zzjd().zze("Data loss. Failed to serialize event params/data. appId", zzap.zzbv(paramzzy.zztt), localIOException);
        return false;
      }
    }
    return true;
  }
  
  public final boolean zza(String paramString, Long paramLong, long paramLong1, zzgf paramzzgf)
  {
    zzaf();
    zzcl();
    Preconditions.checkNotNull(paramzzgf);
    Preconditions.checkNotEmpty(paramString);
    Preconditions.checkNotNull(paramLong);
    try
    {
      byte[] arrayOfByte = new byte[paramzzgf.zzvu()];
      zzyy localzzyy = zzyy.zzk(arrayOfByte, 0, arrayOfByte.length);
      paramzzgf.zza(localzzyy);
      localzzyy.zzyt();
      zzgo().zzjl().zze("Saving complex main event, appId, data size", zzgl().zzbs(paramString), Integer.valueOf(arrayOfByte.length));
      paramzzgf = new ContentValues();
      paramzzgf.put("app_id", paramString);
      paramzzgf.put("event_id", paramLong);
      paramzzgf.put("children_to_process", Long.valueOf(paramLong1));
      paramzzgf.put("main_event", arrayOfByte);
      return true;
    }
    catch (IOException paramzzgf)
    {
      try
      {
        if (getWritableDatabase().insertWithOnConflict("main_event_params", null, paramzzgf, 5) != -1L) {
          break label217;
        }
        zzgo().zzjd().zzg("Failed to insert complex main event (got -1). appId", zzap.zzbv(paramString));
        return false;
      }
      catch (SQLiteException paramLong)
      {
        zzgo().zzjd().zze("Error storing complex main event. appId", zzap.zzbv(paramString), paramLong);
        return false;
      }
      paramzzgf = paramzzgf;
      zzgo().zzjd().zzd("Data loss. Failed to serialize event params/data. appId, eventId", zzap.zzbv(paramString), paramLong, paramzzgf);
      return false;
    }
  }
  
  /* Error */
  public final String zzah(long paramLong)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aload_0
    //   4: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   7: aload_0
    //   8: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   11: aload_0
    //   12: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   15: ldc_w 1109
    //   18: iconst_1
    //   19: anewarray 19	java/lang/String
    //   22: dup
    //   23: iconst_0
    //   24: lload_1
    //   25: invokestatic 1111	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   28: aastore
    //   29: invokevirtual 209	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   32: astore_3
    //   33: aload_3
    //   34: astore 4
    //   36: aload_3
    //   37: invokeinterface 215 1 0
    //   42: ifne +40 -> 82
    //   45: aload_3
    //   46: astore 4
    //   48: aload_0
    //   49: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   52: invokevirtual 601	com/google/android/gms/measurement/internal/zzap:zzjl	()Lcom/google/android/gms/measurement/internal/zzar;
    //   55: ldc_w 1113
    //   58: invokevirtual 270	com/google/android/gms/measurement/internal/zzar:zzbx	(Ljava/lang/String;)V
    //   61: aload 6
    //   63: astore 4
    //   65: aload_3
    //   66: ifnull +13 -> 79
    //   69: aload_3
    //   70: invokeinterface 222 1 0
    //   75: aload 6
    //   77: astore 4
    //   79: aload 4
    //   81: areturn
    //   82: aload_3
    //   83: astore 4
    //   85: aload_3
    //   86: iconst_0
    //   87: invokeinterface 288 2 0
    //   92: astore 5
    //   94: aload 5
    //   96: astore 4
    //   98: aload_3
    //   99: ifnull -20 -> 79
    //   102: aload_3
    //   103: invokeinterface 222 1 0
    //   108: aload 5
    //   110: areturn
    //   111: astore 5
    //   113: aconst_null
    //   114: astore_3
    //   115: aload_3
    //   116: astore 4
    //   118: aload_0
    //   119: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   122: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   125: ldc_w 1115
    //   128: aload 5
    //   130: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   133: aload 6
    //   135: astore 4
    //   137: aload_3
    //   138: ifnull -59 -> 79
    //   141: aload_3
    //   142: invokeinterface 222 1 0
    //   147: aconst_null
    //   148: areturn
    //   149: astore_3
    //   150: aconst_null
    //   151: astore 4
    //   153: aload 4
    //   155: ifnull +10 -> 165
    //   158: aload 4
    //   160: invokeinterface 222 1 0
    //   165: aload_3
    //   166: athrow
    //   167: astore_3
    //   168: goto -15 -> 153
    //   171: astore 5
    //   173: goto -58 -> 115
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	176	0	this	zzq
    //   0	176	1	paramLong	long
    //   32	110	3	localCursor	Cursor
    //   149	17	3	localObject1	Object
    //   167	1	3	localObject2	Object
    //   34	125	4	localObject3	Object
    //   92	17	5	str	String
    //   111	18	5	localSQLiteException1	SQLiteException
    //   171	1	5	localSQLiteException2	SQLiteException
    //   1	133	6	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   11	33	111	android/database/sqlite/SQLiteException
    //   11	33	149	finally
    //   36	45	167	finally
    //   48	61	167	finally
    //   85	94	167	finally
    //   118	133	167	finally
    //   36	45	171	android/database/sqlite/SQLiteException
    //   48	61	171	android/database/sqlite/SQLiteException
    //   85	94	171	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  @WorkerThread
  public final List<android.util.Pair<zzgi, Long>> zzb(String paramString, int paramInt1, int paramInt2)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore 5
    //   3: aload_0
    //   4: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   7: aload_0
    //   8: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   11: iload_2
    //   12: ifle +117 -> 129
    //   15: iconst_1
    //   16: istore 4
    //   18: iload 4
    //   20: invokestatic 1120	com/google/android/gms/common/internal/Preconditions:checkArgument	(Z)V
    //   23: iload_3
    //   24: ifle +111 -> 135
    //   27: iload 5
    //   29: istore 4
    //   31: iload 4
    //   33: invokestatic 1120	com/google/android/gms/common/internal/Preconditions:checkArgument	(Z)V
    //   36: aload_1
    //   37: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   40: pop
    //   41: aload_0
    //   42: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   45: ldc_w 887
    //   48: iconst_3
    //   49: anewarray 19	java/lang/String
    //   52: dup
    //   53: iconst_0
    //   54: ldc_w 1122
    //   57: aastore
    //   58: dup
    //   59: iconst_1
    //   60: ldc_w 392
    //   63: aastore
    //   64: dup
    //   65: iconst_2
    //   66: ldc -103
    //   68: aastore
    //   69: ldc_w 638
    //   72: iconst_1
    //   73: anewarray 19	java/lang/String
    //   76: dup
    //   77: iconst_0
    //   78: aload_1
    //   79: aastore
    //   80: aconst_null
    //   81: aconst_null
    //   82: ldc_w 1122
    //   85: iload_2
    //   86: invokestatic 852	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   89: invokevirtual 1125	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   92: astore 8
    //   94: aload 8
    //   96: invokeinterface 215 1 0
    //   101: ifne +40 -> 141
    //   104: invokestatic 1131	java/util/Collections:emptyList	()Ljava/util/List;
    //   107: astore 9
    //   109: aload 9
    //   111: astore_1
    //   112: aload 8
    //   114: ifnull +13 -> 127
    //   117: aload 8
    //   119: invokeinterface 222 1 0
    //   124: aload 9
    //   126: astore_1
    //   127: aload_1
    //   128: areturn
    //   129: iconst_0
    //   130: istore 4
    //   132: goto -114 -> 18
    //   135: iconst_0
    //   136: istore 4
    //   138: goto -107 -> 31
    //   141: new 462	java/util/ArrayList
    //   144: dup
    //   145: invokespecial 463	java/util/ArrayList:<init>	()V
    //   148: astore 9
    //   150: iconst_0
    //   151: istore_2
    //   152: aload 8
    //   154: iconst_0
    //   155: invokeinterface 219 2 0
    //   160: lstore 6
    //   162: aload 8
    //   164: iconst_1
    //   165: invokeinterface 607 2 0
    //   170: astore 10
    //   172: aload_0
    //   173: invokevirtual 562	com/google/android/gms/measurement/internal/zzey:zzjo	()Lcom/google/android/gms/measurement/internal/zzfg;
    //   176: aload 10
    //   178: invokevirtual 1133	com/google/android/gms/measurement/internal/zzfg:zza	([B)[B
    //   181: astore 10
    //   183: aload 9
    //   185: invokeinterface 1135 1 0
    //   190: ifne +12 -> 202
    //   193: aload 10
    //   195: arraylength
    //   196: iload_2
    //   197: iadd
    //   198: iload_3
    //   199: if_icmpgt +101 -> 300
    //   202: aload 10
    //   204: iconst_0
    //   205: aload 10
    //   207: arraylength
    //   208: invokestatic 613	com/google/android/gms/internal/measurement/zzyx:zzj	([BII)Lcom/google/android/gms/internal/measurement/zzyx;
    //   211: astore 11
    //   213: new 553	com/google/android/gms/internal/measurement/zzgi
    //   216: dup
    //   217: invokespecial 1136	com/google/android/gms/internal/measurement/zzgi:<init>	()V
    //   220: astore 12
    //   222: aload 12
    //   224: aload 11
    //   226: invokevirtual 619	com/google/android/gms/internal/measurement/zzzg:zza	(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    //   229: pop
    //   230: aload 8
    //   232: iconst_2
    //   233: invokeinterface 1140 2 0
    //   238: ifne +19 -> 257
    //   241: aload 12
    //   243: aload 8
    //   245: iconst_2
    //   246: invokeinterface 1143 2 0
    //   251: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   254: putfield 885	com/google/android/gms/internal/measurement/zzgi:zzayc	Ljava/lang/Integer;
    //   257: aload 10
    //   259: arraylength
    //   260: iload_2
    //   261: iadd
    //   262: istore_2
    //   263: aload 9
    //   265: aload 12
    //   267: lload 6
    //   269: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   272: invokestatic 625	android/util/Pair:create	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    //   275: invokeinterface 482 2 0
    //   280: pop
    //   281: aload 8
    //   283: invokeinterface 1146 1 0
    //   288: istore 4
    //   290: iload 4
    //   292: ifeq +8 -> 300
    //   295: iload_2
    //   296: iload_3
    //   297: if_icmple +147 -> 444
    //   300: aload 9
    //   302: astore_1
    //   303: aload 8
    //   305: ifnull -178 -> 127
    //   308: aload 8
    //   310: invokeinterface 222 1 0
    //   315: aload 9
    //   317: areturn
    //   318: astore 10
    //   320: aload_0
    //   321: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   324: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   327: ldc_w 1148
    //   330: aload_1
    //   331: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   334: aload 10
    //   336: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   339: goto -58 -> 281
    //   342: astore 10
    //   344: aload_0
    //   345: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   348: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   351: ldc_w 1150
    //   354: aload_1
    //   355: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   358: aload 10
    //   360: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   363: goto -82 -> 281
    //   366: astore 9
    //   368: aconst_null
    //   369: astore 8
    //   371: aload_0
    //   372: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   375: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   378: ldc_w 1152
    //   381: aload_1
    //   382: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   385: aload 9
    //   387: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   390: invokestatic 1131	java/util/Collections:emptyList	()Ljava/util/List;
    //   393: astore 9
    //   395: aload 9
    //   397: astore_1
    //   398: aload 8
    //   400: ifnull -273 -> 127
    //   403: aload 8
    //   405: invokeinterface 222 1 0
    //   410: aload 9
    //   412: areturn
    //   413: astore_1
    //   414: aconst_null
    //   415: astore 8
    //   417: aload 8
    //   419: ifnull +10 -> 429
    //   422: aload 8
    //   424: invokeinterface 222 1 0
    //   429: aload_1
    //   430: athrow
    //   431: astore_1
    //   432: goto -15 -> 417
    //   435: astore_1
    //   436: goto -19 -> 417
    //   439: astore 9
    //   441: goto -70 -> 371
    //   444: goto -292 -> 152
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	447	0	this	zzq
    //   0	447	1	paramString	String
    //   0	447	2	paramInt1	int
    //   0	447	3	paramInt2	int
    //   16	275	4	bool1	boolean
    //   1	27	5	bool2	boolean
    //   160	108	6	l	long
    //   92	331	8	localCursor	Cursor
    //   107	209	9	localObject	Object
    //   366	20	9	localSQLiteException1	SQLiteException
    //   393	18	9	localList	List
    //   439	1	9	localSQLiteException2	SQLiteException
    //   170	88	10	arrayOfByte	byte[]
    //   318	17	10	localIOException1	IOException
    //   342	17	10	localIOException2	IOException
    //   211	14	11	localzzyx	com.google.android.gms.internal.measurement.zzyx
    //   220	46	12	localzzgi	zzgi
    // Exception table:
    //   from	to	target	type
    //   162	183	318	java/io/IOException
    //   222	230	342	java/io/IOException
    //   41	94	366	android/database/sqlite/SQLiteException
    //   41	94	413	finally
    //   94	109	431	finally
    //   141	150	431	finally
    //   152	162	431	finally
    //   162	183	431	finally
    //   183	202	431	finally
    //   202	222	431	finally
    //   222	230	431	finally
    //   230	257	431	finally
    //   257	281	431	finally
    //   281	290	431	finally
    //   320	339	431	finally
    //   344	363	431	finally
    //   371	395	435	finally
    //   94	109	439	android/database/sqlite/SQLiteException
    //   141	150	439	android/database/sqlite/SQLiteException
    //   152	162	439	android/database/sqlite/SQLiteException
    //   162	183	439	android/database/sqlite/SQLiteException
    //   183	202	439	android/database/sqlite/SQLiteException
    //   202	222	439	android/database/sqlite/SQLiteException
    //   222	230	439	android/database/sqlite/SQLiteException
    //   230	257	439	android/database/sqlite/SQLiteException
    //   257	281	439	android/database/sqlite/SQLiteException
    //   281	290	439	android/database/sqlite/SQLiteException
    //   320	339	439	android/database/sqlite/SQLiteException
    //   344	363	439	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  @WorkerThread
  public final List<zzfj> zzb(String paramString1, String paramString2, String paramString3)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 8
    //   3: aload_1
    //   4: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   7: pop
    //   8: aload_0
    //   9: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   12: aload_0
    //   13: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   16: new 462	java/util/ArrayList
    //   19: dup
    //   20: invokespecial 463	java/util/ArrayList:<init>	()V
    //   23: astore 9
    //   25: new 462	java/util/ArrayList
    //   28: dup
    //   29: iconst_3
    //   30: invokespecial 1155	java/util/ArrayList:<init>	(I)V
    //   33: astore 10
    //   35: aload 10
    //   37: aload_1
    //   38: invokeinterface 482 2 0
    //   43: pop
    //   44: new 490	java/lang/StringBuilder
    //   47: dup
    //   48: ldc_w 638
    //   51: invokespecial 1156	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   54: astore 7
    //   56: aload_2
    //   57: invokestatic 339	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   60: ifne +21 -> 81
    //   63: aload 10
    //   65: aload_2
    //   66: invokeinterface 482 2 0
    //   71: pop
    //   72: aload 7
    //   74: ldc_w 1158
    //   77: invokevirtual 502	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   80: pop
    //   81: aload_3
    //   82: invokestatic 339	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   85: ifne +30 -> 115
    //   88: aload 10
    //   90: aload_3
    //   91: invokestatic 355	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   94: ldc_w 1160
    //   97: invokevirtual 1163	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   100: invokeinterface 482 2 0
    //   105: pop
    //   106: aload 7
    //   108: ldc_w 1165
    //   111: invokevirtual 502	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: aload 10
    //   117: aload 10
    //   119: invokeinterface 468 1 0
    //   124: anewarray 19	java/lang/String
    //   127: invokeinterface 1169 2 0
    //   132: checkcast 1170	[Ljava/lang/String;
    //   135: astore 10
    //   137: aload_0
    //   138: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   141: astore 11
    //   143: aload 7
    //   145: invokevirtual 507	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   148: astore 7
    //   150: aload 11
    //   152: ldc_w 943
    //   155: iconst_4
    //   156: anewarray 19	java/lang/String
    //   159: dup
    //   160: iconst_0
    //   161: ldc_w 781
    //   164: aastore
    //   165: dup
    //   166: iconst_1
    //   167: ldc_w 931
    //   170: aastore
    //   171: dup
    //   172: iconst_2
    //   173: ldc_w 936
    //   176: aastore
    //   177: dup
    //   178: iconst_3
    //   179: ldc 43
    //   181: aastore
    //   182: aload 7
    //   184: aload 10
    //   186: aconst_null
    //   187: aconst_null
    //   188: ldc_w 1122
    //   191: ldc_w 1172
    //   194: invokevirtual 1125	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   197: astore 7
    //   199: aload_2
    //   200: astore 8
    //   202: aload 7
    //   204: invokeinterface 215 1 0
    //   209: istore 4
    //   211: iload 4
    //   213: ifne +18 -> 231
    //   216: aload 7
    //   218: ifnull +10 -> 228
    //   221: aload 7
    //   223: invokeinterface 222 1 0
    //   228: aload 9
    //   230: areturn
    //   231: aload_2
    //   232: astore 8
    //   234: aload 9
    //   236: invokeinterface 468 1 0
    //   241: sipush 1000
    //   244: if_icmplt +40 -> 284
    //   247: aload_2
    //   248: astore 8
    //   250: aload_0
    //   251: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   254: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   257: ldc_w 1174
    //   260: sipush 1000
    //   263: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   266: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   269: aload 7
    //   271: ifnull +10 -> 281
    //   274: aload 7
    //   276: invokeinterface 222 1 0
    //   281: aload 9
    //   283: areturn
    //   284: aload_2
    //   285: astore 8
    //   287: aload 7
    //   289: iconst_0
    //   290: invokeinterface 288 2 0
    //   295: astore 10
    //   297: aload_2
    //   298: astore 8
    //   300: aload 7
    //   302: iconst_1
    //   303: invokeinterface 219 2 0
    //   308: lstore 5
    //   310: aload_2
    //   311: astore 8
    //   313: aload_0
    //   314: aload 7
    //   316: iconst_2
    //   317: invokespecial 1176	com/google/android/gms/measurement/internal/zzq:zza	(Landroid/database/Cursor;I)Ljava/lang/Object;
    //   320: astore 11
    //   322: aload_2
    //   323: astore 8
    //   325: aload 7
    //   327: iconst_3
    //   328: invokeinterface 288 2 0
    //   333: astore_2
    //   334: aload 11
    //   336: ifnonnull +35 -> 371
    //   339: aload_0
    //   340: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   343: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   346: ldc_w 1178
    //   349: aload_1
    //   350: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   353: aload_2
    //   354: aload_3
    //   355: invokevirtual 359	com/google/android/gms/measurement/internal/zzar:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   358: aload 7
    //   360: invokeinterface 1146 1 0
    //   365: ifne -134 -> 231
    //   368: goto -99 -> 269
    //   371: aload 9
    //   373: new 900	com/google/android/gms/measurement/internal/zzfj
    //   376: dup
    //   377: aload_1
    //   378: aload_2
    //   379: aload 10
    //   381: lload 5
    //   383: aload 11
    //   385: invokespecial 1181	com/google/android/gms/measurement/internal/zzfj:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V
    //   388: invokeinterface 482 2 0
    //   393: pop
    //   394: goto -36 -> 358
    //   397: astore 8
    //   399: aload 7
    //   401: astore_3
    //   402: aload 8
    //   404: astore 7
    //   406: aload_0
    //   407: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   410: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   413: ldc_w 1183
    //   416: aload_1
    //   417: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   420: aload_2
    //   421: aload 7
    //   423: invokevirtual 359	com/google/android/gms/measurement/internal/zzar:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   426: aload_3
    //   427: ifnull +9 -> 436
    //   430: aload_3
    //   431: invokeinterface 222 1 0
    //   436: aconst_null
    //   437: areturn
    //   438: astore_1
    //   439: aload 8
    //   441: astore_2
    //   442: aload_2
    //   443: ifnull +9 -> 452
    //   446: aload_2
    //   447: invokeinterface 222 1 0
    //   452: aload_1
    //   453: athrow
    //   454: astore_1
    //   455: aload 7
    //   457: astore_2
    //   458: goto -16 -> 442
    //   461: astore_1
    //   462: aload_3
    //   463: astore_2
    //   464: goto -22 -> 442
    //   467: astore 7
    //   469: aconst_null
    //   470: astore_3
    //   471: goto -65 -> 406
    //   474: astore_2
    //   475: aload 7
    //   477: astore_3
    //   478: aload_2
    //   479: astore 7
    //   481: aload 8
    //   483: astore_2
    //   484: goto -78 -> 406
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	487	0	this	zzq
    //   0	487	1	paramString1	String
    //   0	487	2	paramString2	String
    //   0	487	3	paramString3	String
    //   209	3	4	bool	boolean
    //   308	74	5	l	long
    //   54	402	7	localObject1	Object
    //   467	9	7	localSQLiteException1	SQLiteException
    //   479	1	7	str1	String
    //   1	323	8	str2	String
    //   397	85	8	localSQLiteException2	SQLiteException
    //   23	349	9	localArrayList	ArrayList
    //   33	347	10	localObject2	Object
    //   141	243	11	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   339	358	397	android/database/sqlite/SQLiteException
    //   358	368	397	android/database/sqlite/SQLiteException
    //   371	394	397	android/database/sqlite/SQLiteException
    //   25	81	438	finally
    //   81	115	438	finally
    //   115	199	438	finally
    //   202	211	454	finally
    //   234	247	454	finally
    //   250	269	454	finally
    //   287	297	454	finally
    //   300	310	454	finally
    //   313	322	454	finally
    //   325	334	454	finally
    //   339	358	454	finally
    //   358	368	454	finally
    //   371	394	454	finally
    //   406	426	461	finally
    //   25	81	467	android/database/sqlite/SQLiteException
    //   81	115	467	android/database/sqlite/SQLiteException
    //   115	199	467	android/database/sqlite/SQLiteException
    //   202	211	474	android/database/sqlite/SQLiteException
    //   234	247	474	android/database/sqlite/SQLiteException
    //   250	269	474	android/database/sqlite/SQLiteException
    //   287	297	474	android/database/sqlite/SQLiteException
    //   300	310	474	android/database/sqlite/SQLiteException
    //   313	322	474	android/database/sqlite/SQLiteException
    //   325	334	474	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  public final List<zzl> zzb(String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   4: aload_0
    //   5: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   8: new 462	java/util/ArrayList
    //   11: dup
    //   12: invokespecial 463	java/util/ArrayList:<init>	()V
    //   15: astore 12
    //   17: aload_0
    //   18: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   21: ldc_w 1018
    //   24: bipush 13
    //   26: anewarray 19	java/lang/String
    //   29: dup
    //   30: iconst_0
    //   31: ldc_w 381
    //   34: aastore
    //   35: dup
    //   36: iconst_1
    //   37: ldc 43
    //   39: aastore
    //   40: dup
    //   41: iconst_2
    //   42: ldc_w 781
    //   45: aastore
    //   46: dup
    //   47: iconst_3
    //   48: ldc_w 936
    //   51: aastore
    //   52: dup
    //   53: iconst_4
    //   54: ldc_w 971
    //   57: aastore
    //   58: dup
    //   59: iconst_5
    //   60: ldc_w 976
    //   63: aastore
    //   64: dup
    //   65: bipush 6
    //   67: ldc_w 981
    //   70: aastore
    //   71: dup
    //   72: bipush 7
    //   74: ldc_w 986
    //   77: aastore
    //   78: dup
    //   79: bipush 8
    //   81: ldc_w 995
    //   84: aastore
    //   85: dup
    //   86: bipush 9
    //   88: ldc_w 1000
    //   91: aastore
    //   92: dup
    //   93: bipush 10
    //   95: ldc_w 1005
    //   98: aastore
    //   99: dup
    //   100: bipush 11
    //   102: ldc_w 1008
    //   105: aastore
    //   106: dup
    //   107: bipush 12
    //   109: ldc_w 1013
    //   112: aastore
    //   113: aload_1
    //   114: aload_2
    //   115: aconst_null
    //   116: aconst_null
    //   117: ldc_w 1122
    //   120: ldc_w 1172
    //   123: invokevirtual 1125	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   126: astore_1
    //   127: aload_1
    //   128: invokeinterface 215 1 0
    //   133: istore_3
    //   134: iload_3
    //   135: ifne +18 -> 153
    //   138: aload_1
    //   139: ifnull +9 -> 148
    //   142: aload_1
    //   143: invokeinterface 222 1 0
    //   148: aload 12
    //   150: astore_2
    //   151: aload_2
    //   152: areturn
    //   153: aload 12
    //   155: invokeinterface 468 1 0
    //   160: sipush 1000
    //   163: if_icmplt +35 -> 198
    //   166: aload_0
    //   167: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   170: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   173: ldc_w 1187
    //   176: sipush 1000
    //   179: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   182: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   185: aload_1
    //   186: ifnull +9 -> 195
    //   189: aload_1
    //   190: invokeinterface 222 1 0
    //   195: aload 12
    //   197: areturn
    //   198: aload_1
    //   199: iconst_0
    //   200: invokeinterface 288 2 0
    //   205: astore_2
    //   206: aload_1
    //   207: iconst_1
    //   208: invokeinterface 288 2 0
    //   213: astore 13
    //   215: aload_1
    //   216: iconst_2
    //   217: invokeinterface 288 2 0
    //   222: astore 14
    //   224: aload_0
    //   225: aload_1
    //   226: iconst_3
    //   227: invokespecial 1176	com/google/android/gms/measurement/internal/zzq:zza	(Landroid/database/Cursor;I)Ljava/lang/Object;
    //   230: astore 15
    //   232: aload_1
    //   233: iconst_4
    //   234: invokeinterface 1143 2 0
    //   239: ifeq +185 -> 424
    //   242: iconst_1
    //   243: istore_3
    //   244: aload_1
    //   245: iconst_5
    //   246: invokeinterface 288 2 0
    //   251: astore 16
    //   253: aload_1
    //   254: bipush 6
    //   256: invokeinterface 219 2 0
    //   261: lstore 4
    //   263: aload_0
    //   264: invokevirtual 562	com/google/android/gms/measurement/internal/zzey:zzjo	()Lcom/google/android/gms/measurement/internal/zzfg;
    //   267: aload_1
    //   268: bipush 7
    //   270: invokeinterface 607 2 0
    //   275: getstatic 1193	com/google/android/gms/measurement/internal/zzad:CREATOR	Landroid/os/Parcelable$Creator;
    //   278: invokevirtual 1196	com/google/android/gms/measurement/internal/zzfg:zza	([BLandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    //   281: checkcast 1189	com/google/android/gms/measurement/internal/zzad
    //   284: astore 17
    //   286: aload_1
    //   287: bipush 8
    //   289: invokeinterface 219 2 0
    //   294: lstore 6
    //   296: aload_0
    //   297: invokevirtual 562	com/google/android/gms/measurement/internal/zzey:zzjo	()Lcom/google/android/gms/measurement/internal/zzfg;
    //   300: aload_1
    //   301: bipush 9
    //   303: invokeinterface 607 2 0
    //   308: getstatic 1193	com/google/android/gms/measurement/internal/zzad:CREATOR	Landroid/os/Parcelable$Creator;
    //   311: invokevirtual 1196	com/google/android/gms/measurement/internal/zzfg:zza	([BLandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    //   314: checkcast 1189	com/google/android/gms/measurement/internal/zzad
    //   317: astore 18
    //   319: aload_1
    //   320: bipush 10
    //   322: invokeinterface 219 2 0
    //   327: lstore 8
    //   329: aload_1
    //   330: bipush 11
    //   332: invokeinterface 219 2 0
    //   337: lstore 10
    //   339: aload_0
    //   340: invokevirtual 562	com/google/android/gms/measurement/internal/zzey:zzjo	()Lcom/google/android/gms/measurement/internal/zzfg;
    //   343: aload_1
    //   344: bipush 12
    //   346: invokeinterface 607 2 0
    //   351: getstatic 1193	com/google/android/gms/measurement/internal/zzad:CREATOR	Landroid/os/Parcelable$Creator;
    //   354: invokevirtual 1196	com/google/android/gms/measurement/internal/zzfg:zza	([BLandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    //   357: checkcast 1189	com/google/android/gms/measurement/internal/zzad
    //   360: astore 19
    //   362: aload 12
    //   364: new 950	com/google/android/gms/measurement/internal/zzl
    //   367: dup
    //   368: aload_2
    //   369: aload 13
    //   371: new 959	com/google/android/gms/measurement/internal/zzfh
    //   374: dup
    //   375: aload 14
    //   377: lload 8
    //   379: aload 15
    //   381: aload 13
    //   383: invokespecial 1199	com/google/android/gms/measurement/internal/zzfh:<init>	(Ljava/lang/String;JLjava/lang/Object;Ljava/lang/String;)V
    //   386: lload 6
    //   388: iload_3
    //   389: aload 16
    //   391: aload 17
    //   393: lload 4
    //   395: aload 18
    //   397: lload 10
    //   399: aload 19
    //   401: invokespecial 1202	com/google/android/gms/measurement/internal/zzl:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzfh;JZLjava/lang/String;Lcom/google/android/gms/measurement/internal/zzad;JLcom/google/android/gms/measurement/internal/zzad;JLcom/google/android/gms/measurement/internal/zzad;)V
    //   404: invokeinterface 482 2 0
    //   409: pop
    //   410: aload_1
    //   411: invokeinterface 1146 1 0
    //   416: istore_3
    //   417: iload_3
    //   418: ifne -265 -> 153
    //   421: goto -236 -> 185
    //   424: iconst_0
    //   425: istore_3
    //   426: goto -182 -> 244
    //   429: astore_2
    //   430: aconst_null
    //   431: astore_1
    //   432: aload_0
    //   433: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   436: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   439: ldc_w 1204
    //   442: aload_2
    //   443: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   446: invokestatic 1131	java/util/Collections:emptyList	()Ljava/util/List;
    //   449: astore 12
    //   451: aload 12
    //   453: astore_2
    //   454: aload_1
    //   455: ifnull -304 -> 151
    //   458: aload_1
    //   459: invokeinterface 222 1 0
    //   464: aload 12
    //   466: areturn
    //   467: astore_2
    //   468: aconst_null
    //   469: astore_1
    //   470: aload_1
    //   471: ifnull +9 -> 480
    //   474: aload_1
    //   475: invokeinterface 222 1 0
    //   480: aload_2
    //   481: athrow
    //   482: astore_2
    //   483: goto -13 -> 470
    //   486: astore_2
    //   487: goto -17 -> 470
    //   490: astore_2
    //   491: goto -59 -> 432
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	494	0	this	zzq
    //   0	494	1	paramString	String
    //   0	494	2	paramArrayOfString	String[]
    //   133	293	3	bool	boolean
    //   261	133	4	l1	long
    //   294	93	6	l2	long
    //   327	51	8	l3	long
    //   337	61	10	l4	long
    //   15	450	12	localObject1	Object
    //   213	169	13	str1	String
    //   222	154	14	str2	String
    //   230	150	15	localObject2	Object
    //   251	139	16	str3	String
    //   284	108	17	localzzad1	zzad
    //   317	79	18	localzzad2	zzad
    //   360	40	19	localzzad3	zzad
    // Exception table:
    //   from	to	target	type
    //   17	127	429	android/database/sqlite/SQLiteException
    //   17	127	467	finally
    //   127	134	482	finally
    //   153	185	482	finally
    //   198	242	482	finally
    //   244	417	482	finally
    //   432	451	486	finally
    //   127	134	490	android/database/sqlite/SQLiteException
    //   153	185	490	android/database/sqlite/SQLiteException
    //   198	242	490	android/database/sqlite/SQLiteException
    //   244	417	490	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  @WorkerThread
  public final List<zzfj> zzbk(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aload_1
    //   4: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   7: pop
    //   8: aload_0
    //   9: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   12: aload_0
    //   13: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   16: new 462	java/util/ArrayList
    //   19: dup
    //   20: invokespecial 463	java/util/ArrayList:<init>	()V
    //   23: astore 8
    //   25: aload_0
    //   26: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   29: ldc_w 943
    //   32: iconst_4
    //   33: anewarray 19	java/lang/String
    //   36: dup
    //   37: iconst_0
    //   38: ldc_w 781
    //   41: aastore
    //   42: dup
    //   43: iconst_1
    //   44: ldc 43
    //   46: aastore
    //   47: dup
    //   48: iconst_2
    //   49: ldc_w 931
    //   52: aastore
    //   53: dup
    //   54: iconst_3
    //   55: ldc_w 936
    //   58: aastore
    //   59: ldc_w 638
    //   62: iconst_1
    //   63: anewarray 19	java/lang/String
    //   66: dup
    //   67: iconst_0
    //   68: aload_1
    //   69: aastore
    //   70: aconst_null
    //   71: aconst_null
    //   72: ldc_w 1122
    //   75: ldc_w 1209
    //   78: invokevirtual 1125	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   81: astore 5
    //   83: aload 5
    //   85: invokeinterface 215 1 0
    //   90: istore_2
    //   91: iload_2
    //   92: ifne +18 -> 110
    //   95: aload 5
    //   97: ifnull +10 -> 107
    //   100: aload 5
    //   102: invokeinterface 222 1 0
    //   107: aload 8
    //   109: areturn
    //   110: aload 5
    //   112: iconst_0
    //   113: invokeinterface 288 2 0
    //   118: astore 9
    //   120: aload 5
    //   122: iconst_1
    //   123: invokeinterface 288 2 0
    //   128: astore 7
    //   130: aload 7
    //   132: astore 6
    //   134: aload 7
    //   136: ifnonnull +8 -> 144
    //   139: ldc_w 1211
    //   142: astore 6
    //   144: aload 5
    //   146: iconst_2
    //   147: invokeinterface 219 2 0
    //   152: lstore_3
    //   153: aload_0
    //   154: aload 5
    //   156: iconst_3
    //   157: invokespecial 1176	com/google/android/gms/measurement/internal/zzq:zza	(Landroid/database/Cursor;I)Ljava/lang/Object;
    //   160: astore 7
    //   162: aload 7
    //   164: ifnonnull +47 -> 211
    //   167: aload_0
    //   168: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   171: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   174: ldc_w 1213
    //   177: aload_1
    //   178: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   181: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   184: aload 5
    //   186: invokeinterface 1146 1 0
    //   191: istore_2
    //   192: iload_2
    //   193: ifne -83 -> 110
    //   196: aload 5
    //   198: ifnull +10 -> 208
    //   201: aload 5
    //   203: invokeinterface 222 1 0
    //   208: aload 8
    //   210: areturn
    //   211: aload 8
    //   213: new 900	com/google/android/gms/measurement/internal/zzfj
    //   216: dup
    //   217: aload_1
    //   218: aload 6
    //   220: aload 9
    //   222: lload_3
    //   223: aload 7
    //   225: invokespecial 1181	com/google/android/gms/measurement/internal/zzfj:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V
    //   228: invokeinterface 482 2 0
    //   233: pop
    //   234: goto -50 -> 184
    //   237: astore 6
    //   239: aload_0
    //   240: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   243: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   246: ldc_w 1215
    //   249: aload_1
    //   250: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   253: aload 6
    //   255: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   258: aload 5
    //   260: ifnull +10 -> 270
    //   263: aload 5
    //   265: invokeinterface 222 1 0
    //   270: aconst_null
    //   271: areturn
    //   272: astore_1
    //   273: aload 6
    //   275: astore 5
    //   277: aload 5
    //   279: ifnull +10 -> 289
    //   282: aload 5
    //   284: invokeinterface 222 1 0
    //   289: aload_1
    //   290: athrow
    //   291: astore_1
    //   292: goto -15 -> 277
    //   295: astore_1
    //   296: goto -19 -> 277
    //   299: astore 6
    //   301: aconst_null
    //   302: astore 5
    //   304: goto -65 -> 239
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	307	0	this	zzq
    //   0	307	1	paramString	String
    //   90	103	2	bool	boolean
    //   152	71	3	l	long
    //   81	222	5	localObject1	Object
    //   1	218	6	localObject2	Object
    //   237	37	6	localSQLiteException1	SQLiteException
    //   299	1	6	localSQLiteException2	SQLiteException
    //   128	96	7	localObject3	Object
    //   23	189	8	localArrayList	ArrayList
    //   118	103	9	str	String
    // Exception table:
    //   from	to	target	type
    //   83	91	237	android/database/sqlite/SQLiteException
    //   110	130	237	android/database/sqlite/SQLiteException
    //   144	162	237	android/database/sqlite/SQLiteException
    //   167	184	237	android/database/sqlite/SQLiteException
    //   184	192	237	android/database/sqlite/SQLiteException
    //   211	234	237	android/database/sqlite/SQLiteException
    //   25	83	272	finally
    //   83	91	291	finally
    //   110	130	291	finally
    //   144	162	291	finally
    //   167	184	291	finally
    //   184	192	291	finally
    //   211	234	291	finally
    //   239	258	295	finally
    //   25	83	299	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  @WorkerThread
  public final zzg zzbl(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   9: aload_0
    //   10: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   13: aload_0
    //   14: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   17: ldc_w 636
    //   20: bipush 26
    //   22: anewarray 19	java/lang/String
    //   25: dup
    //   26: iconst_0
    //   27: ldc_w 674
    //   30: aastore
    //   31: dup
    //   32: iconst_1
    //   33: ldc_w 679
    //   36: aastore
    //   37: dup
    //   38: iconst_2
    //   39: ldc_w 684
    //   42: aastore
    //   43: dup
    //   44: iconst_3
    //   45: ldc_w 689
    //   48: aastore
    //   49: dup
    //   50: iconst_4
    //   51: ldc 69
    //   53: aastore
    //   54: dup
    //   55: iconst_5
    //   56: ldc_w 698
    //   59: aastore
    //   60: dup
    //   61: bipush 6
    //   63: ldc 49
    //   65: aastore
    //   66: dup
    //   67: bipush 7
    //   69: ldc 53
    //   71: aastore
    //   72: dup
    //   73: bipush 8
    //   75: ldc 57
    //   77: aastore
    //   78: dup
    //   79: bipush 9
    //   81: ldc 61
    //   83: aastore
    //   84: dup
    //   85: bipush 10
    //   87: ldc 65
    //   89: aastore
    //   90: dup
    //   91: bipush 11
    //   93: ldc 73
    //   95: aastore
    //   96: dup
    //   97: bipush 12
    //   99: ldc 77
    //   101: aastore
    //   102: dup
    //   103: bipush 13
    //   105: ldc 81
    //   107: aastore
    //   108: dup
    //   109: bipush 14
    //   111: ldc 85
    //   113: aastore
    //   114: dup
    //   115: bipush 15
    //   117: ldc 93
    //   119: aastore
    //   120: dup
    //   121: bipush 16
    //   123: ldc 97
    //   125: aastore
    //   126: dup
    //   127: bipush 17
    //   129: ldc 101
    //   131: aastore
    //   132: dup
    //   133: bipush 18
    //   135: ldc 105
    //   137: aastore
    //   138: dup
    //   139: bipush 19
    //   141: ldc 109
    //   143: aastore
    //   144: dup
    //   145: bipush 20
    //   147: ldc 113
    //   149: aastore
    //   150: dup
    //   151: bipush 21
    //   153: ldc 117
    //   155: aastore
    //   156: dup
    //   157: bipush 22
    //   159: ldc 121
    //   161: aastore
    //   162: dup
    //   163: bipush 23
    //   165: ldc 125
    //   167: aastore
    //   168: dup
    //   169: bipush 24
    //   171: ldc -127
    //   173: aastore
    //   174: dup
    //   175: bipush 25
    //   177: ldc -123
    //   179: aastore
    //   180: ldc_w 638
    //   183: iconst_1
    //   184: anewarray 19	java/lang/String
    //   187: dup
    //   188: iconst_0
    //   189: aload_1
    //   190: aastore
    //   191: aconst_null
    //   192: aconst_null
    //   193: aconst_null
    //   194: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   197: astore 6
    //   199: aload 6
    //   201: astore 5
    //   203: aload 6
    //   205: invokeinterface 215 1 0
    //   210: istore_2
    //   211: iload_2
    //   212: ifne +19 -> 231
    //   215: aload 6
    //   217: ifnull +10 -> 227
    //   220: aload 6
    //   222: invokeinterface 222 1 0
    //   227: aconst_null
    //   228: astore_1
    //   229: aload_1
    //   230: areturn
    //   231: aload 6
    //   233: astore 5
    //   235: new 669	com/google/android/gms/measurement/internal/zzg
    //   238: dup
    //   239: aload_0
    //   240: getfield 1222	com/google/android/gms/measurement/internal/zzq:zzamz	Lcom/google/android/gms/measurement/internal/zzfa;
    //   243: invokevirtual 1228	com/google/android/gms/measurement/internal/zzfa:zzmb	()Lcom/google/android/gms/measurement/internal/zzbt;
    //   246: aload_1
    //   247: invokespecial 1231	com/google/android/gms/measurement/internal/zzg:<init>	(Lcom/google/android/gms/measurement/internal/zzbt;Ljava/lang/String;)V
    //   250: astore 7
    //   252: aload 6
    //   254: astore 5
    //   256: aload 7
    //   258: aload 6
    //   260: iconst_0
    //   261: invokeinterface 288 2 0
    //   266: invokevirtual 1234	com/google/android/gms/measurement/internal/zzg:zzam	(Ljava/lang/String;)V
    //   269: aload 6
    //   271: astore 5
    //   273: aload 7
    //   275: aload 6
    //   277: iconst_1
    //   278: invokeinterface 288 2 0
    //   283: invokevirtual 1237	com/google/android/gms/measurement/internal/zzg:zzan	(Ljava/lang/String;)V
    //   286: aload 6
    //   288: astore 5
    //   290: aload 7
    //   292: aload 6
    //   294: iconst_2
    //   295: invokeinterface 288 2 0
    //   300: invokevirtual 1240	com/google/android/gms/measurement/internal/zzg:zzap	(Ljava/lang/String;)V
    //   303: aload 6
    //   305: astore 5
    //   307: aload 7
    //   309: aload 6
    //   311: iconst_3
    //   312: invokeinterface 219 2 0
    //   317: invokevirtual 1244	com/google/android/gms/measurement/internal/zzg:zzx	(J)V
    //   320: aload 6
    //   322: astore 5
    //   324: aload 7
    //   326: aload 6
    //   328: iconst_4
    //   329: invokeinterface 219 2 0
    //   334: invokevirtual 1247	com/google/android/gms/measurement/internal/zzg:zzs	(J)V
    //   337: aload 6
    //   339: astore 5
    //   341: aload 7
    //   343: aload 6
    //   345: iconst_5
    //   346: invokeinterface 219 2 0
    //   351: invokevirtual 1250	com/google/android/gms/measurement/internal/zzg:zzt	(J)V
    //   354: aload 6
    //   356: astore 5
    //   358: aload 7
    //   360: aload 6
    //   362: bipush 6
    //   364: invokeinterface 288 2 0
    //   369: invokevirtual 1253	com/google/android/gms/measurement/internal/zzg:setAppVersion	(Ljava/lang/String;)V
    //   372: aload 6
    //   374: astore 5
    //   376: aload 7
    //   378: aload 6
    //   380: bipush 7
    //   382: invokeinterface 288 2 0
    //   387: invokevirtual 1256	com/google/android/gms/measurement/internal/zzg:zzar	(Ljava/lang/String;)V
    //   390: aload 6
    //   392: astore 5
    //   394: aload 7
    //   396: aload 6
    //   398: bipush 8
    //   400: invokeinterface 219 2 0
    //   405: invokevirtual 1259	com/google/android/gms/measurement/internal/zzg:zzv	(J)V
    //   408: aload 6
    //   410: astore 5
    //   412: aload 7
    //   414: aload 6
    //   416: bipush 9
    //   418: invokeinterface 219 2 0
    //   423: invokevirtual 1262	com/google/android/gms/measurement/internal/zzg:zzw	(J)V
    //   426: aload 6
    //   428: astore 5
    //   430: aload 6
    //   432: bipush 10
    //   434: invokeinterface 1140 2 0
    //   439: ifne +559 -> 998
    //   442: aload 6
    //   444: astore 5
    //   446: aload 6
    //   448: bipush 10
    //   450: invokeinterface 1143 2 0
    //   455: ifeq +424 -> 879
    //   458: goto +540 -> 998
    //   461: aload 6
    //   463: astore 5
    //   465: aload 7
    //   467: iload_2
    //   468: invokevirtual 1265	com/google/android/gms/measurement/internal/zzg:setMeasurementEnabled	(Z)V
    //   471: aload 6
    //   473: astore 5
    //   475: aload 7
    //   477: aload 6
    //   479: bipush 11
    //   481: invokeinterface 219 2 0
    //   486: invokevirtual 1268	com/google/android/gms/measurement/internal/zzg:zzaa	(J)V
    //   489: aload 6
    //   491: astore 5
    //   493: aload 7
    //   495: aload 6
    //   497: bipush 12
    //   499: invokeinterface 219 2 0
    //   504: invokevirtual 1271	com/google/android/gms/measurement/internal/zzg:zzab	(J)V
    //   507: aload 6
    //   509: astore 5
    //   511: aload 7
    //   513: aload 6
    //   515: bipush 13
    //   517: invokeinterface 219 2 0
    //   522: invokevirtual 1274	com/google/android/gms/measurement/internal/zzg:zzac	(J)V
    //   525: aload 6
    //   527: astore 5
    //   529: aload 7
    //   531: aload 6
    //   533: bipush 14
    //   535: invokeinterface 219 2 0
    //   540: invokevirtual 1277	com/google/android/gms/measurement/internal/zzg:zzad	(J)V
    //   543: aload 6
    //   545: astore 5
    //   547: aload 7
    //   549: aload 6
    //   551: bipush 15
    //   553: invokeinterface 219 2 0
    //   558: invokevirtual 1280	com/google/android/gms/measurement/internal/zzg:zzy	(J)V
    //   561: aload 6
    //   563: astore 5
    //   565: aload 7
    //   567: aload 6
    //   569: bipush 16
    //   571: invokeinterface 219 2 0
    //   576: invokevirtual 1283	com/google/android/gms/measurement/internal/zzg:zzz	(J)V
    //   579: aload 6
    //   581: astore 5
    //   583: aload 6
    //   585: bipush 17
    //   587: invokeinterface 1140 2 0
    //   592: ifeq +292 -> 884
    //   595: ldc2_w 1284
    //   598: lstore_3
    //   599: aload 6
    //   601: astore 5
    //   603: aload 7
    //   605: lload_3
    //   606: invokevirtual 1288	com/google/android/gms/measurement/internal/zzg:zzu	(J)V
    //   609: aload 6
    //   611: astore 5
    //   613: aload 7
    //   615: aload 6
    //   617: bipush 18
    //   619: invokeinterface 288 2 0
    //   624: invokevirtual 1291	com/google/android/gms/measurement/internal/zzg:zzaq	(Ljava/lang/String;)V
    //   627: aload 6
    //   629: astore 5
    //   631: aload 7
    //   633: aload 6
    //   635: bipush 19
    //   637: invokeinterface 219 2 0
    //   642: invokevirtual 1293	com/google/android/gms/measurement/internal/zzg:zzaf	(J)V
    //   645: aload 6
    //   647: astore 5
    //   649: aload 7
    //   651: aload 6
    //   653: bipush 20
    //   655: invokeinterface 219 2 0
    //   660: invokevirtual 1296	com/google/android/gms/measurement/internal/zzg:zzae	(J)V
    //   663: aload 6
    //   665: astore 5
    //   667: aload 7
    //   669: aload 6
    //   671: bipush 21
    //   673: invokeinterface 288 2 0
    //   678: invokevirtual 1299	com/google/android/gms/measurement/internal/zzg:zzas	(Ljava/lang/String;)V
    //   681: aload 6
    //   683: astore 5
    //   685: aload 6
    //   687: bipush 22
    //   689: invokeinterface 1140 2 0
    //   694: ifeq +208 -> 902
    //   697: lconst_0
    //   698: lstore_3
    //   699: aload 6
    //   701: astore 5
    //   703: aload 7
    //   705: lload_3
    //   706: invokevirtual 1302	com/google/android/gms/measurement/internal/zzg:zzag	(J)V
    //   709: aload 6
    //   711: astore 5
    //   713: aload 6
    //   715: bipush 23
    //   717: invokeinterface 1140 2 0
    //   722: ifne +281 -> 1003
    //   725: aload 6
    //   727: astore 5
    //   729: aload 6
    //   731: bipush 23
    //   733: invokeinterface 1143 2 0
    //   738: ifeq +181 -> 919
    //   741: goto +262 -> 1003
    //   744: aload 6
    //   746: astore 5
    //   748: aload 7
    //   750: iload_2
    //   751: invokevirtual 1304	com/google/android/gms/measurement/internal/zzg:zze	(Z)V
    //   754: aload 6
    //   756: astore 5
    //   758: aload 6
    //   760: bipush 24
    //   762: invokeinterface 1140 2 0
    //   767: ifne +241 -> 1008
    //   770: aload 6
    //   772: astore 5
    //   774: aload 6
    //   776: bipush 24
    //   778: invokeinterface 1143 2 0
    //   783: ifeq +141 -> 924
    //   786: goto +222 -> 1008
    //   789: aload 6
    //   791: astore 5
    //   793: aload 7
    //   795: iload_2
    //   796: invokevirtual 1307	com/google/android/gms/measurement/internal/zzg:zzf	(Z)V
    //   799: aload 6
    //   801: astore 5
    //   803: aload 7
    //   805: aload 6
    //   807: bipush 25
    //   809: invokeinterface 288 2 0
    //   814: invokevirtual 1310	com/google/android/gms/measurement/internal/zzg:zzao	(Ljava/lang/String;)V
    //   817: aload 6
    //   819: astore 5
    //   821: aload 7
    //   823: invokevirtual 1313	com/google/android/gms/measurement/internal/zzg:zzgv	()V
    //   826: aload 6
    //   828: astore 5
    //   830: aload 6
    //   832: invokeinterface 1146 1 0
    //   837: ifeq +24 -> 861
    //   840: aload 6
    //   842: astore 5
    //   844: aload_0
    //   845: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   848: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   851: ldc_w 1315
    //   854: aload_1
    //   855: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   858: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   861: aload 7
    //   863: astore_1
    //   864: aload 6
    //   866: ifnull -637 -> 229
    //   869: aload 6
    //   871: invokeinterface 222 1 0
    //   876: aload 7
    //   878: areturn
    //   879: iconst_0
    //   880: istore_2
    //   881: goto -420 -> 461
    //   884: aload 6
    //   886: astore 5
    //   888: aload 6
    //   890: bipush 17
    //   892: invokeinterface 1143 2 0
    //   897: i2l
    //   898: lstore_3
    //   899: goto -300 -> 599
    //   902: aload 6
    //   904: astore 5
    //   906: aload 6
    //   908: bipush 22
    //   910: invokeinterface 219 2 0
    //   915: lstore_3
    //   916: goto -217 -> 699
    //   919: iconst_0
    //   920: istore_2
    //   921: goto -177 -> 744
    //   924: iconst_0
    //   925: istore_2
    //   926: goto -137 -> 789
    //   929: astore 7
    //   931: aconst_null
    //   932: astore 6
    //   934: aload 6
    //   936: astore 5
    //   938: aload_0
    //   939: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   942: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   945: ldc_w 1317
    //   948: aload_1
    //   949: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   952: aload 7
    //   954: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   957: aload 6
    //   959: ifnull +10 -> 969
    //   962: aload 6
    //   964: invokeinterface 222 1 0
    //   969: aconst_null
    //   970: areturn
    //   971: astore_1
    //   972: aconst_null
    //   973: astore 5
    //   975: aload 5
    //   977: ifnull +10 -> 987
    //   980: aload 5
    //   982: invokeinterface 222 1 0
    //   987: aload_1
    //   988: athrow
    //   989: astore_1
    //   990: goto -15 -> 975
    //   993: astore 7
    //   995: goto -61 -> 934
    //   998: iconst_1
    //   999: istore_2
    //   1000: goto -539 -> 461
    //   1003: iconst_1
    //   1004: istore_2
    //   1005: goto -261 -> 744
    //   1008: iconst_1
    //   1009: istore_2
    //   1010: goto -221 -> 789
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1013	0	this	zzq
    //   0	1013	1	paramString	String
    //   210	800	2	bool	boolean
    //   598	318	3	l	long
    //   201	780	5	localCursor1	Cursor
    //   197	766	6	localCursor2	Cursor
    //   250	627	7	localzzg	zzg
    //   929	24	7	localSQLiteException1	SQLiteException
    //   993	1	7	localSQLiteException2	SQLiteException
    // Exception table:
    //   from	to	target	type
    //   13	199	929	android/database/sqlite/SQLiteException
    //   13	199	971	finally
    //   203	211	989	finally
    //   235	252	989	finally
    //   256	269	989	finally
    //   273	286	989	finally
    //   290	303	989	finally
    //   307	320	989	finally
    //   324	337	989	finally
    //   341	354	989	finally
    //   358	372	989	finally
    //   376	390	989	finally
    //   394	408	989	finally
    //   412	426	989	finally
    //   430	442	989	finally
    //   446	458	989	finally
    //   465	471	989	finally
    //   475	489	989	finally
    //   493	507	989	finally
    //   511	525	989	finally
    //   529	543	989	finally
    //   547	561	989	finally
    //   565	579	989	finally
    //   583	595	989	finally
    //   603	609	989	finally
    //   613	627	989	finally
    //   631	645	989	finally
    //   649	663	989	finally
    //   667	681	989	finally
    //   685	697	989	finally
    //   703	709	989	finally
    //   713	725	989	finally
    //   729	741	989	finally
    //   748	754	989	finally
    //   758	770	989	finally
    //   774	786	989	finally
    //   793	799	989	finally
    //   803	817	989	finally
    //   821	826	989	finally
    //   830	840	989	finally
    //   844	861	989	finally
    //   888	899	989	finally
    //   906	916	989	finally
    //   938	957	989	finally
    //   203	211	993	android/database/sqlite/SQLiteException
    //   235	252	993	android/database/sqlite/SQLiteException
    //   256	269	993	android/database/sqlite/SQLiteException
    //   273	286	993	android/database/sqlite/SQLiteException
    //   290	303	993	android/database/sqlite/SQLiteException
    //   307	320	993	android/database/sqlite/SQLiteException
    //   324	337	993	android/database/sqlite/SQLiteException
    //   341	354	993	android/database/sqlite/SQLiteException
    //   358	372	993	android/database/sqlite/SQLiteException
    //   376	390	993	android/database/sqlite/SQLiteException
    //   394	408	993	android/database/sqlite/SQLiteException
    //   412	426	993	android/database/sqlite/SQLiteException
    //   430	442	993	android/database/sqlite/SQLiteException
    //   446	458	993	android/database/sqlite/SQLiteException
    //   465	471	993	android/database/sqlite/SQLiteException
    //   475	489	993	android/database/sqlite/SQLiteException
    //   493	507	993	android/database/sqlite/SQLiteException
    //   511	525	993	android/database/sqlite/SQLiteException
    //   529	543	993	android/database/sqlite/SQLiteException
    //   547	561	993	android/database/sqlite/SQLiteException
    //   565	579	993	android/database/sqlite/SQLiteException
    //   583	595	993	android/database/sqlite/SQLiteException
    //   603	609	993	android/database/sqlite/SQLiteException
    //   613	627	993	android/database/sqlite/SQLiteException
    //   631	645	993	android/database/sqlite/SQLiteException
    //   649	663	993	android/database/sqlite/SQLiteException
    //   667	681	993	android/database/sqlite/SQLiteException
    //   685	697	993	android/database/sqlite/SQLiteException
    //   703	709	993	android/database/sqlite/SQLiteException
    //   713	725	993	android/database/sqlite/SQLiteException
    //   729	741	993	android/database/sqlite/SQLiteException
    //   748	754	993	android/database/sqlite/SQLiteException
    //   758	770	993	android/database/sqlite/SQLiteException
    //   774	786	993	android/database/sqlite/SQLiteException
    //   793	799	993	android/database/sqlite/SQLiteException
    //   803	817	993	android/database/sqlite/SQLiteException
    //   821	826	993	android/database/sqlite/SQLiteException
    //   830	840	993	android/database/sqlite/SQLiteException
    //   844	861	993	android/database/sqlite/SQLiteException
    //   888	899	993	android/database/sqlite/SQLiteException
    //   906	916	993	android/database/sqlite/SQLiteException
  }
  
  public final long zzbm(String paramString)
  {
    Preconditions.checkNotEmpty(paramString);
    zzaf();
    zzcl();
    try
    {
      int i = getWritableDatabase().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[] { paramString, String.valueOf(Math.max(0, Math.min(1000000, zzgq().zzb(paramString, zzaf.zzajs)))) });
      return i;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzgo().zzjd().zze("Error deleting over the limit events. appId", zzap.zzbv(paramString), localSQLiteException);
    }
    return 0L;
  }
  
  /* Error */
  @WorkerThread
  public final byte[] zzbn(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   9: aload_0
    //   10: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   13: aload_0
    //   14: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   17: ldc_w 636
    //   20: iconst_1
    //   21: anewarray 19	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: ldc 89
    //   28: aastore
    //   29: ldc_w 638
    //   32: iconst_1
    //   33: anewarray 19	java/lang/String
    //   36: dup
    //   37: iconst_0
    //   38: aload_1
    //   39: aastore
    //   40: aconst_null
    //   41: aconst_null
    //   42: aconst_null
    //   43: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   46: astore 4
    //   48: aload 4
    //   50: astore_3
    //   51: aload 4
    //   53: invokeinterface 215 1 0
    //   58: istore_2
    //   59: iload_2
    //   60: ifne +19 -> 79
    //   63: aload 4
    //   65: ifnull +10 -> 75
    //   68: aload 4
    //   70: invokeinterface 222 1 0
    //   75: aconst_null
    //   76: astore_1
    //   77: aload_1
    //   78: areturn
    //   79: aload 4
    //   81: astore_3
    //   82: aload 4
    //   84: iconst_0
    //   85: invokeinterface 607 2 0
    //   90: astore 5
    //   92: aload 4
    //   94: astore_3
    //   95: aload 4
    //   97: invokeinterface 1146 1 0
    //   102: ifeq +23 -> 125
    //   105: aload 4
    //   107: astore_3
    //   108: aload_0
    //   109: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   112: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   115: ldc_w 1331
    //   118: aload_1
    //   119: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   122: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   125: aload 5
    //   127: astore_1
    //   128: aload 4
    //   130: ifnull -53 -> 77
    //   133: aload 4
    //   135: invokeinterface 222 1 0
    //   140: aload 5
    //   142: areturn
    //   143: astore 5
    //   145: aconst_null
    //   146: astore 4
    //   148: aload 4
    //   150: astore_3
    //   151: aload_0
    //   152: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   155: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   158: ldc_w 1333
    //   161: aload_1
    //   162: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   165: aload 5
    //   167: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   170: aload 4
    //   172: ifnull +10 -> 182
    //   175: aload 4
    //   177: invokeinterface 222 1 0
    //   182: aconst_null
    //   183: areturn
    //   184: astore_1
    //   185: aconst_null
    //   186: astore_3
    //   187: aload_3
    //   188: ifnull +9 -> 197
    //   191: aload_3
    //   192: invokeinterface 222 1 0
    //   197: aload_1
    //   198: athrow
    //   199: astore_1
    //   200: goto -13 -> 187
    //   203: astore 5
    //   205: goto -57 -> 148
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	208	0	this	zzq
    //   0	208	1	paramString	String
    //   58	2	2	bool	boolean
    //   50	142	3	localCursor1	Cursor
    //   46	130	4	localCursor2	Cursor
    //   90	51	5	arrayOfByte	byte[]
    //   143	23	5	localSQLiteException1	SQLiteException
    //   203	1	5	localSQLiteException2	SQLiteException
    // Exception table:
    //   from	to	target	type
    //   13	48	143	android/database/sqlite/SQLiteException
    //   13	48	184	finally
    //   51	59	199	finally
    //   82	92	199	finally
    //   95	105	199	finally
    //   108	125	199	finally
    //   151	170	199	finally
    //   51	59	203	android/database/sqlite/SQLiteException
    //   82	92	203	android/database/sqlite/SQLiteException
    //   95	105	203	android/database/sqlite/SQLiteException
    //   108	125	203	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  final java.util.Map<Integer, com.google.android.gms.internal.measurement.zzgj> zzbo(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   4: aload_0
    //   5: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   8: aload_1
    //   9: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   12: pop
    //   13: aload_0
    //   14: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   17: astore 4
    //   19: aload 4
    //   21: ldc_w 509
    //   24: iconst_2
    //   25: anewarray 19	java/lang/String
    //   28: dup
    //   29: iconst_0
    //   30: ldc_w 383
    //   33: aastore
    //   34: dup
    //   35: iconst_1
    //   36: ldc_w 1337
    //   39: aastore
    //   40: ldc_w 638
    //   43: iconst_1
    //   44: anewarray 19	java/lang/String
    //   47: dup
    //   48: iconst_0
    //   49: aload_1
    //   50: aastore
    //   51: aconst_null
    //   52: aconst_null
    //   53: aconst_null
    //   54: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   57: astore 5
    //   59: aload 5
    //   61: astore 4
    //   63: aload 5
    //   65: invokeinterface 215 1 0
    //   70: istore_3
    //   71: iload_3
    //   72: ifne +19 -> 91
    //   75: aload 5
    //   77: ifnull +10 -> 87
    //   80: aload 5
    //   82: invokeinterface 222 1 0
    //   87: aconst_null
    //   88: astore_1
    //   89: aload_1
    //   90: areturn
    //   91: aload 5
    //   93: astore 4
    //   95: new 1339	android/support/v4/util/ArrayMap
    //   98: dup
    //   99: invokespecial 1340	android/support/v4/util/ArrayMap:<init>	()V
    //   102: astore 6
    //   104: aload 5
    //   106: astore 4
    //   108: aload 5
    //   110: iconst_0
    //   111: invokeinterface 1143 2 0
    //   116: istore_2
    //   117: aload 5
    //   119: astore 4
    //   121: aload 5
    //   123: iconst_1
    //   124: invokeinterface 607 2 0
    //   129: astore 7
    //   131: aload 5
    //   133: astore 4
    //   135: aload 7
    //   137: iconst_0
    //   138: aload 7
    //   140: arraylength
    //   141: invokestatic 613	com/google/android/gms/internal/measurement/zzyx:zzj	([BII)Lcom/google/android/gms/internal/measurement/zzyx;
    //   144: astore 7
    //   146: aload 5
    //   148: astore 4
    //   150: new 1342	com/google/android/gms/internal/measurement/zzgj
    //   153: dup
    //   154: invokespecial 1343	com/google/android/gms/internal/measurement/zzgj:<init>	()V
    //   157: astore 8
    //   159: aload 5
    //   161: astore 4
    //   163: aload 8
    //   165: aload 7
    //   167: invokevirtual 619	com/google/android/gms/internal/measurement/zzzg:zza	(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    //   170: pop
    //   171: aload 5
    //   173: astore 4
    //   175: aload 6
    //   177: iload_2
    //   178: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   181: aload 8
    //   183: invokeinterface 1348 3 0
    //   188: pop
    //   189: aload 5
    //   191: astore 4
    //   193: aload 5
    //   195: invokeinterface 1146 1 0
    //   200: istore_3
    //   201: iload_3
    //   202: ifne -98 -> 104
    //   205: aload 6
    //   207: astore_1
    //   208: aload 5
    //   210: ifnull -121 -> 89
    //   213: aload 5
    //   215: invokeinterface 222 1 0
    //   220: aload 6
    //   222: areturn
    //   223: astore 7
    //   225: aload 5
    //   227: astore 4
    //   229: aload_0
    //   230: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   233: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   236: ldc_w 1350
    //   239: aload_1
    //   240: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   243: iload_2
    //   244: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   247: aload 7
    //   249: invokevirtual 359	com/google/android/gms/measurement/internal/zzar:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   252: goto -63 -> 189
    //   255: astore 6
    //   257: aload 5
    //   259: astore 4
    //   261: aload_0
    //   262: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   265: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   268: ldc_w 1352
    //   271: aload_1
    //   272: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   275: aload 6
    //   277: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   280: aload 5
    //   282: ifnull +10 -> 292
    //   285: aload 5
    //   287: invokeinterface 222 1 0
    //   292: aconst_null
    //   293: areturn
    //   294: astore_1
    //   295: aconst_null
    //   296: astore 4
    //   298: aload 4
    //   300: ifnull +10 -> 310
    //   303: aload 4
    //   305: invokeinterface 222 1 0
    //   310: aload_1
    //   311: athrow
    //   312: astore_1
    //   313: goto -15 -> 298
    //   316: astore 6
    //   318: aconst_null
    //   319: astore 5
    //   321: goto -64 -> 257
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	324	0	this	zzq
    //   0	324	1	paramString	String
    //   116	128	2	i	int
    //   70	132	3	bool	boolean
    //   17	287	4	localObject1	Object
    //   57	263	5	localCursor	Cursor
    //   102	119	6	localArrayMap	android.support.v4.util.ArrayMap
    //   255	21	6	localSQLiteException1	SQLiteException
    //   316	1	6	localSQLiteException2	SQLiteException
    //   129	37	7	localObject2	Object
    //   223	25	7	localIOException	IOException
    //   157	25	8	localzzgj	com.google.android.gms.internal.measurement.zzgj
    // Exception table:
    //   from	to	target	type
    //   163	171	223	java/io/IOException
    //   63	71	255	android/database/sqlite/SQLiteException
    //   95	104	255	android/database/sqlite/SQLiteException
    //   108	117	255	android/database/sqlite/SQLiteException
    //   121	131	255	android/database/sqlite/SQLiteException
    //   135	146	255	android/database/sqlite/SQLiteException
    //   150	159	255	android/database/sqlite/SQLiteException
    //   163	171	255	android/database/sqlite/SQLiteException
    //   175	189	255	android/database/sqlite/SQLiteException
    //   193	201	255	android/database/sqlite/SQLiteException
    //   229	252	255	android/database/sqlite/SQLiteException
    //   19	59	294	finally
    //   63	71	312	finally
    //   95	104	312	finally
    //   108	117	312	finally
    //   121	131	312	finally
    //   135	146	312	finally
    //   150	159	312	finally
    //   163	171	312	finally
    //   175	189	312	finally
    //   193	201	312	finally
    //   229	252	312	finally
    //   261	280	312	finally
    //   19	59	316	android/database/sqlite/SQLiteException
  }
  
  public final long zzbp(String paramString)
  {
    Preconditions.checkNotEmpty(paramString);
    return zza("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[] { paramString }, 0L);
  }
  
  @WorkerThread
  public final List<zzl> zzc(String paramString1, String paramString2, String paramString3)
  {
    Preconditions.checkNotEmpty(paramString1);
    zzaf();
    zzcl();
    ArrayList localArrayList = new ArrayList(3);
    localArrayList.add(paramString1);
    paramString1 = new StringBuilder("app_id=?");
    if (!TextUtils.isEmpty(paramString2))
    {
      localArrayList.add(paramString2);
      paramString1.append(" and origin=?");
    }
    if (!TextUtils.isEmpty(paramString3))
    {
      localArrayList.add(String.valueOf(paramString3).concat("*"));
      paramString1.append(" and name glob ?");
    }
    paramString2 = (String[])localArrayList.toArray(new String[localArrayList.size()]);
    return zzb(paramString1.toString(), paramString2);
  }
  
  @WorkerThread
  @VisibleForTesting
  final void zzc(List<Long> paramList)
  {
    zzaf();
    zzcl();
    Preconditions.checkNotNull(paramList);
    Preconditions.checkNotZero(paramList.size());
    if (!zzil()) {
      return;
    }
    paramList = TextUtils.join(",", paramList);
    paramList = String.valueOf(paramList).length() + 2 + "(" + paramList + ")";
    if (zza(String.valueOf(paramList).length() + 80 + "SELECT COUNT(1) FROM queue WHERE rowid IN " + paramList + " AND retry_count =  2147483647 LIMIT 1", null) > 0L) {
      zzgo().zzjg().zzbx("The number of upload retries exceeds the limit. Will remain unchanged.");
    }
    try
    {
      getWritableDatabase().execSQL(String.valueOf(paramList).length() + 127 + "UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN " + paramList + " AND (retry_count IS NULL OR retry_count < 2147483647)");
      return;
    }
    catch (SQLiteException paramList)
    {
      zzgo().zzjd().zzg("Error incrementing retry count. error", paramList);
    }
  }
  
  /* Error */
  @WorkerThread
  public final zzz zzg(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   4: pop
    //   5: aload_2
    //   6: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   9: pop
    //   10: aload_0
    //   11: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   14: aload_0
    //   15: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   18: aload_0
    //   19: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   22: ldc_w 820
    //   25: bipush 8
    //   27: anewarray 19	java/lang/String
    //   30: dup
    //   31: iconst_0
    //   32: ldc_w 785
    //   35: aastore
    //   36: dup
    //   37: iconst_1
    //   38: ldc_w 790
    //   41: aastore
    //   42: dup
    //   43: iconst_2
    //   44: ldc_w 795
    //   47: aastore
    //   48: dup
    //   49: iconst_3
    //   50: ldc 21
    //   52: aastore
    //   53: dup
    //   54: iconst_4
    //   55: ldc 25
    //   57: aastore
    //   58: dup
    //   59: iconst_5
    //   60: ldc 29
    //   62: aastore
    //   63: dup
    //   64: bipush 6
    //   66: ldc 33
    //   68: aastore
    //   69: dup
    //   70: bipush 7
    //   72: ldc 37
    //   74: aastore
    //   75: ldc_w 1386
    //   78: iconst_2
    //   79: anewarray 19	java/lang/String
    //   82: dup
    //   83: iconst_0
    //   84: aload_1
    //   85: aastore
    //   86: dup
    //   87: iconst_1
    //   88: aload_2
    //   89: aastore
    //   90: aconst_null
    //   91: aconst_null
    //   92: aconst_null
    //   93: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   96: astore 14
    //   98: aload 14
    //   100: invokeinterface 215 1 0
    //   105: istore_3
    //   106: iload_3
    //   107: ifne +19 -> 126
    //   110: aload 14
    //   112: ifnull +10 -> 122
    //   115: aload 14
    //   117: invokeinterface 222 1 0
    //   122: aconst_null
    //   123: astore_1
    //   124: aload_1
    //   125: areturn
    //   126: aload 14
    //   128: iconst_0
    //   129: invokeinterface 219 2 0
    //   134: lstore 6
    //   136: aload 14
    //   138: iconst_1
    //   139: invokeinterface 219 2 0
    //   144: lstore 8
    //   146: aload 14
    //   148: iconst_2
    //   149: invokeinterface 219 2 0
    //   154: lstore 10
    //   156: aload 14
    //   158: iconst_3
    //   159: invokeinterface 1140 2 0
    //   164: ifeq +158 -> 322
    //   167: lconst_0
    //   168: lstore 4
    //   170: aload 14
    //   172: iconst_4
    //   173: invokeinterface 1140 2 0
    //   178: ifeq +157 -> 335
    //   181: aconst_null
    //   182: astore 15
    //   184: aload 14
    //   186: iconst_5
    //   187: invokeinterface 1140 2 0
    //   192: ifeq +159 -> 351
    //   195: aconst_null
    //   196: astore 16
    //   198: aload 14
    //   200: bipush 6
    //   202: invokeinterface 1140 2 0
    //   207: ifeq +160 -> 367
    //   210: aconst_null
    //   211: astore 17
    //   213: aconst_null
    //   214: astore 18
    //   216: aload 14
    //   218: bipush 7
    //   220: invokeinterface 1140 2 0
    //   225: ifne +25 -> 250
    //   228: aload 14
    //   230: bipush 7
    //   232: invokeinterface 219 2 0
    //   237: lconst_1
    //   238: lcmp
    //   239: ifne +149 -> 388
    //   242: iconst_1
    //   243: istore_3
    //   244: iload_3
    //   245: invokestatic 721	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   248: astore 18
    //   250: new 778	com/google/android/gms/measurement/internal/zzz
    //   253: dup
    //   254: aload_1
    //   255: aload_2
    //   256: lload 6
    //   258: lload 8
    //   260: lload 10
    //   262: lload 4
    //   264: aload 15
    //   266: aload 16
    //   268: aload 17
    //   270: aload 18
    //   272: invokespecial 1389	com/google/android/gms/measurement/internal/zzz:<init>	(Ljava/lang/String;Ljava/lang/String;JJJJLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V
    //   275: astore 15
    //   277: aload 14
    //   279: invokeinterface 1146 1 0
    //   284: ifeq +20 -> 304
    //   287: aload_0
    //   288: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   291: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   294: ldc_w 1391
    //   297: aload_1
    //   298: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   301: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   304: aload 15
    //   306: astore_1
    //   307: aload 14
    //   309: ifnull -185 -> 124
    //   312: aload 14
    //   314: invokeinterface 222 1 0
    //   319: aload 15
    //   321: areturn
    //   322: aload 14
    //   324: iconst_3
    //   325: invokeinterface 219 2 0
    //   330: lstore 4
    //   332: goto -162 -> 170
    //   335: aload 14
    //   337: iconst_4
    //   338: invokeinterface 219 2 0
    //   343: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   346: astore 15
    //   348: goto -164 -> 184
    //   351: aload 14
    //   353: iconst_5
    //   354: invokeinterface 219 2 0
    //   359: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   362: astore 16
    //   364: goto -166 -> 198
    //   367: aload 14
    //   369: bipush 6
    //   371: invokeinterface 219 2 0
    //   376: lstore 12
    //   378: lload 12
    //   380: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   383: astore 17
    //   385: goto -172 -> 213
    //   388: iconst_0
    //   389: istore_3
    //   390: goto -146 -> 244
    //   393: astore 15
    //   395: aconst_null
    //   396: astore 14
    //   398: aload_0
    //   399: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   402: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   405: ldc_w 1393
    //   408: aload_1
    //   409: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   412: aload_0
    //   413: invokevirtual 1070	com/google/android/gms/measurement/internal/zzco:zzgl	()Lcom/google/android/gms/measurement/internal/zzan;
    //   416: aload_2
    //   417: invokevirtual 1076	com/google/android/gms/measurement/internal/zzan:zzbs	(Ljava/lang/String;)Ljava/lang/String;
    //   420: aload 15
    //   422: invokevirtual 359	com/google/android/gms/measurement/internal/zzar:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   425: aload 14
    //   427: ifnull +10 -> 437
    //   430: aload 14
    //   432: invokeinterface 222 1 0
    //   437: aconst_null
    //   438: areturn
    //   439: astore_1
    //   440: aconst_null
    //   441: astore 14
    //   443: aload 14
    //   445: ifnull +10 -> 455
    //   448: aload 14
    //   450: invokeinterface 222 1 0
    //   455: aload_1
    //   456: athrow
    //   457: astore_1
    //   458: goto -15 -> 443
    //   461: astore_1
    //   462: goto -19 -> 443
    //   465: astore 15
    //   467: goto -69 -> 398
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	470	0	this	zzq
    //   0	470	1	paramString1	String
    //   0	470	2	paramString2	String
    //   105	285	3	bool	boolean
    //   168	163	4	l1	long
    //   134	123	6	l2	long
    //   144	115	8	l3	long
    //   154	107	10	l4	long
    //   376	3	12	l5	long
    //   96	353	14	localCursor	Cursor
    //   182	165	15	localObject	Object
    //   393	28	15	localSQLiteException1	SQLiteException
    //   465	1	15	localSQLiteException2	SQLiteException
    //   196	167	16	localLong1	Long
    //   211	173	17	localLong2	Long
    //   214	57	18	localBoolean	Boolean
    // Exception table:
    //   from	to	target	type
    //   18	98	393	android/database/sqlite/SQLiteException
    //   18	98	439	finally
    //   98	106	457	finally
    //   126	167	457	finally
    //   170	181	457	finally
    //   184	195	457	finally
    //   198	210	457	finally
    //   216	242	457	finally
    //   244	250	457	finally
    //   250	304	457	finally
    //   322	332	457	finally
    //   335	348	457	finally
    //   351	364	457	finally
    //   367	378	457	finally
    //   398	425	461	finally
    //   98	106	465	android/database/sqlite/SQLiteException
    //   126	167	465	android/database/sqlite/SQLiteException
    //   170	181	465	android/database/sqlite/SQLiteException
    //   184	195	465	android/database/sqlite/SQLiteException
    //   198	210	465	android/database/sqlite/SQLiteException
    //   216	242	465	android/database/sqlite/SQLiteException
    //   244	250	465	android/database/sqlite/SQLiteException
    //   250	304	465	android/database/sqlite/SQLiteException
    //   322	332	465	android/database/sqlite/SQLiteException
    //   335	348	465	android/database/sqlite/SQLiteException
    //   351	364	465	android/database/sqlite/SQLiteException
    //   367	378	465	android/database/sqlite/SQLiteException
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  @WorkerThread
  public final void zzh(String paramString1, String paramString2)
  {
    Preconditions.checkNotEmpty(paramString1);
    Preconditions.checkNotEmpty(paramString2);
    zzaf();
    zzcl();
    try
    {
      int i = getWritableDatabase().delete("user_attributes", "app_id=? and name=?", new String[] { paramString1, paramString2 });
      zzgo().zzjl().zzg("Deleted user attribute rows", Integer.valueOf(i));
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzgo().zzjd().zzd("Error deleting user attribute. appId", zzap.zzbv(paramString1), zzgl().zzbu(paramString2), localSQLiteException);
    }
  }
  
  /* Error */
  @WorkerThread
  public final zzfj zzi(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 7
    //   3: aload_1
    //   4: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   7: pop
    //   8: aload_2
    //   9: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   12: pop
    //   13: aload_0
    //   14: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   17: aload_0
    //   18: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   21: aload_0
    //   22: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   25: ldc_w 943
    //   28: iconst_3
    //   29: anewarray 19	java/lang/String
    //   32: dup
    //   33: iconst_0
    //   34: ldc_w 931
    //   37: aastore
    //   38: dup
    //   39: iconst_1
    //   40: ldc_w 936
    //   43: aastore
    //   44: dup
    //   45: iconst_2
    //   46: ldc 43
    //   48: aastore
    //   49: ldc_w 1386
    //   52: iconst_2
    //   53: anewarray 19	java/lang/String
    //   56: dup
    //   57: iconst_0
    //   58: aload_1
    //   59: aastore
    //   60: dup
    //   61: iconst_1
    //   62: aload_2
    //   63: aastore
    //   64: aconst_null
    //   65: aconst_null
    //   66: aconst_null
    //   67: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   70: astore 6
    //   72: aload 6
    //   74: invokeinterface 215 1 0
    //   79: istore_3
    //   80: iload_3
    //   81: ifne +19 -> 100
    //   84: aload 6
    //   86: ifnull +10 -> 96
    //   89: aload 6
    //   91: invokeinterface 222 1 0
    //   96: aconst_null
    //   97: astore_1
    //   98: aload_1
    //   99: areturn
    //   100: aload 6
    //   102: iconst_0
    //   103: invokeinterface 219 2 0
    //   108: lstore 4
    //   110: aload_0
    //   111: aload 6
    //   113: iconst_1
    //   114: invokespecial 1176	com/google/android/gms/measurement/internal/zzq:zza	(Landroid/database/Cursor;I)Ljava/lang/Object;
    //   117: astore 7
    //   119: new 900	com/google/android/gms/measurement/internal/zzfj
    //   122: dup
    //   123: aload_1
    //   124: aload 6
    //   126: iconst_2
    //   127: invokeinterface 288 2 0
    //   132: aload_2
    //   133: lload 4
    //   135: aload 7
    //   137: invokespecial 1181	com/google/android/gms/measurement/internal/zzfj:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V
    //   140: astore 7
    //   142: aload 6
    //   144: invokeinterface 1146 1 0
    //   149: ifeq +20 -> 169
    //   152: aload_0
    //   153: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   156: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   159: ldc_w 1404
    //   162: aload_1
    //   163: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   166: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   169: aload 7
    //   171: astore_1
    //   172: aload 6
    //   174: ifnull -76 -> 98
    //   177: aload 6
    //   179: invokeinterface 222 1 0
    //   184: aload 7
    //   186: areturn
    //   187: astore 7
    //   189: aconst_null
    //   190: astore 6
    //   192: aload_0
    //   193: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   196: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   199: ldc_w 1406
    //   202: aload_1
    //   203: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   206: aload_0
    //   207: invokevirtual 1070	com/google/android/gms/measurement/internal/zzco:zzgl	()Lcom/google/android/gms/measurement/internal/zzan;
    //   210: aload_2
    //   211: invokevirtual 1402	com/google/android/gms/measurement/internal/zzan:zzbu	(Ljava/lang/String;)Ljava/lang/String;
    //   214: aload 7
    //   216: invokevirtual 359	com/google/android/gms/measurement/internal/zzar:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   219: aload 6
    //   221: ifnull +10 -> 231
    //   224: aload 6
    //   226: invokeinterface 222 1 0
    //   231: aconst_null
    //   232: areturn
    //   233: astore_1
    //   234: aload 7
    //   236: astore_2
    //   237: aload_2
    //   238: ifnull +9 -> 247
    //   241: aload_2
    //   242: invokeinterface 222 1 0
    //   247: aload_1
    //   248: athrow
    //   249: astore_1
    //   250: aload 6
    //   252: astore_2
    //   253: goto -16 -> 237
    //   256: astore_1
    //   257: aload 6
    //   259: astore_2
    //   260: goto -23 -> 237
    //   263: astore 7
    //   265: goto -73 -> 192
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	268	0	this	zzq
    //   0	268	1	paramString1	String
    //   0	268	2	paramString2	String
    //   79	2	3	bool	boolean
    //   108	26	4	l	long
    //   70	188	6	localCursor	Cursor
    //   1	184	7	localObject	Object
    //   187	48	7	localSQLiteException1	SQLiteException
    //   263	1	7	localSQLiteException2	SQLiteException
    // Exception table:
    //   from	to	target	type
    //   21	72	187	android/database/sqlite/SQLiteException
    //   21	72	233	finally
    //   72	80	249	finally
    //   100	169	249	finally
    //   192	219	256	finally
    //   72	80	263	android/database/sqlite/SQLiteException
    //   100	169	263	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  @WorkerThread
  public final String zzid()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   7: astore_1
    //   8: aload_1
    //   9: ldc_w 1409
    //   12: aconst_null
    //   13: invokevirtual 209	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   16: astore_1
    //   17: aload_1
    //   18: astore_2
    //   19: aload_1
    //   20: invokeinterface 215 1 0
    //   25: ifeq +29 -> 54
    //   28: aload_1
    //   29: astore_2
    //   30: aload_1
    //   31: iconst_0
    //   32: invokeinterface 288 2 0
    //   37: astore_3
    //   38: aload_3
    //   39: astore_2
    //   40: aload_1
    //   41: ifnull +11 -> 52
    //   44: aload_1
    //   45: invokeinterface 222 1 0
    //   50: aload_3
    //   51: astore_2
    //   52: aload_2
    //   53: areturn
    //   54: aload 4
    //   56: astore_2
    //   57: aload_1
    //   58: ifnull -6 -> 52
    //   61: aload_1
    //   62: invokeinterface 222 1 0
    //   67: aconst_null
    //   68: areturn
    //   69: astore_3
    //   70: aconst_null
    //   71: astore_1
    //   72: aload_1
    //   73: astore_2
    //   74: aload_0
    //   75: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   78: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   81: ldc_w 1411
    //   84: aload_3
    //   85: invokevirtual 266	com/google/android/gms/measurement/internal/zzar:zzg	(Ljava/lang/String;Ljava/lang/Object;)V
    //   88: aload 4
    //   90: astore_2
    //   91: aload_1
    //   92: ifnull -40 -> 52
    //   95: aload_1
    //   96: invokeinterface 222 1 0
    //   101: aconst_null
    //   102: areturn
    //   103: astore_1
    //   104: aconst_null
    //   105: astore_2
    //   106: aload_2
    //   107: ifnull +9 -> 116
    //   110: aload_2
    //   111: invokeinterface 222 1 0
    //   116: aload_1
    //   117: athrow
    //   118: astore_1
    //   119: goto -13 -> 106
    //   122: astore_3
    //   123: goto -51 -> 72
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	126	0	this	zzq
    //   7	89	1	localObject1	Object
    //   103	14	1	localObject2	Object
    //   118	1	1	localObject3	Object
    //   18	93	2	localObject4	Object
    //   37	14	3	str	String
    //   69	16	3	localSQLiteException1	SQLiteException
    //   122	1	3	localSQLiteException2	SQLiteException
    //   1	88	4	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   8	17	69	android/database/sqlite/SQLiteException
    //   8	17	103	finally
    //   19	28	118	finally
    //   30	38	118	finally
    //   74	88	118	finally
    //   19	28	122	android/database/sqlite/SQLiteException
    //   30	38	122	android/database/sqlite/SQLiteException
  }
  
  public final boolean zzie()
  {
    return zza("select count(1) > 0 from queue where has_realtime = 1", null) != 0L;
  }
  
  @WorkerThread
  final void zzif()
  {
    zzaf();
    zzcl();
    if (!zzil()) {}
    int i;
    do
    {
      do
      {
        long l1;
        long l2;
        do
        {
          return;
          l1 = zzgp().zzanh.get();
          l2 = zzbx().elapsedRealtime();
        } while (Math.abs(l2 - l1) <= ((Long)zzaf.zzakb.get()).longValue());
        zzgp().zzanh.set(l2);
        zzaf();
        zzcl();
      } while (!zzil());
      i = getWritableDatabase().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[] { String.valueOf(zzbx().currentTimeMillis()), String.valueOf(zzn.zzhw()) });
    } while (i <= 0);
    zzgo().zzjl().zzg("Deleted stale rows. rowsDeleted", Integer.valueOf(i));
  }
  
  @WorkerThread
  public final long zzig()
  {
    return zza("select max(bundle_end_timestamp) from queue", null, 0L);
  }
  
  @WorkerThread
  public final long zzih()
  {
    return zza("select max(timestamp) from raw_events", null, 0L);
  }
  
  public final boolean zzii()
  {
    return zza("select count(1) > 0 from raw_events", null) != 0L;
  }
  
  public final boolean zzij()
  {
    return zza("select count(1) > 0 from raw_events where realtime = 1", null) != 0L;
  }
  
  public final long zzik()
  {
    l2 = -1L;
    localObject3 = null;
    localObject1 = null;
    label63:
    do
    {
      try
      {
        localCursor = getWritableDatabase().rawQuery("select rowid from raw_events order by rowid desc limit 1;", null);
        localObject1 = localCursor;
        localObject3 = localCursor;
        boolean bool = localCursor.moveToFirst();
        if (bool) {
          break label63;
        }
        l1 = l2;
        if (localCursor != null)
        {
          localCursor.close();
          l1 = l2;
        }
      }
      catch (SQLiteException localSQLiteException)
      {
        Cursor localCursor;
        localObject3 = localObject1;
        zzgo().zzjd().zzg("Error querying raw events", localSQLiteException);
        long l1 = l2;
        return -1L;
      }
      finally
      {
        if (localObject3 == null) {
          break;
        }
        ((Cursor)localObject3).close();
      }
      return l1;
      localObject1 = localCursor;
      localObject3 = localCursor;
      l1 = localCursor.getLong(0);
      l2 = l1;
      l1 = l2;
    } while (localCursor == null);
    localCursor.close();
    return l2;
  }
  
  /* Error */
  @WorkerThread
  public final zzl zzj(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   4: pop
    //   5: aload_2
    //   6: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   9: pop
    //   10: aload_0
    //   11: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   14: aload_0
    //   15: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   18: aload_0
    //   19: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   22: ldc_w 1018
    //   25: bipush 11
    //   27: anewarray 19	java/lang/String
    //   30: dup
    //   31: iconst_0
    //   32: ldc 43
    //   34: aastore
    //   35: dup
    //   36: iconst_1
    //   37: ldc_w 936
    //   40: aastore
    //   41: dup
    //   42: iconst_2
    //   43: ldc_w 971
    //   46: aastore
    //   47: dup
    //   48: iconst_3
    //   49: ldc_w 976
    //   52: aastore
    //   53: dup
    //   54: iconst_4
    //   55: ldc_w 981
    //   58: aastore
    //   59: dup
    //   60: iconst_5
    //   61: ldc_w 986
    //   64: aastore
    //   65: dup
    //   66: bipush 6
    //   68: ldc_w 995
    //   71: aastore
    //   72: dup
    //   73: bipush 7
    //   75: ldc_w 1000
    //   78: aastore
    //   79: dup
    //   80: bipush 8
    //   82: ldc_w 1005
    //   85: aastore
    //   86: dup
    //   87: bipush 9
    //   89: ldc_w 1008
    //   92: aastore
    //   93: dup
    //   94: bipush 10
    //   96: ldc_w 1013
    //   99: aastore
    //   100: ldc_w 1386
    //   103: iconst_2
    //   104: anewarray 19	java/lang/String
    //   107: dup
    //   108: iconst_0
    //   109: aload_1
    //   110: aastore
    //   111: dup
    //   112: iconst_1
    //   113: aload_2
    //   114: aastore
    //   115: aconst_null
    //   116: aconst_null
    //   117: aconst_null
    //   118: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   121: astore 12
    //   123: aload 12
    //   125: invokeinterface 215 1 0
    //   130: istore_3
    //   131: iload_3
    //   132: ifne +19 -> 151
    //   135: aload 12
    //   137: ifnull +10 -> 147
    //   140: aload 12
    //   142: invokeinterface 222 1 0
    //   147: aconst_null
    //   148: astore_1
    //   149: aload_1
    //   150: areturn
    //   151: aload 12
    //   153: iconst_0
    //   154: invokeinterface 288 2 0
    //   159: astore 13
    //   161: aload_0
    //   162: aload 12
    //   164: iconst_1
    //   165: invokespecial 1176	com/google/android/gms/measurement/internal/zzq:zza	(Landroid/database/Cursor;I)Ljava/lang/Object;
    //   168: astore 14
    //   170: aload 12
    //   172: iconst_2
    //   173: invokeinterface 1143 2 0
    //   178: ifeq +223 -> 401
    //   181: iconst_1
    //   182: istore_3
    //   183: aload 12
    //   185: iconst_3
    //   186: invokeinterface 288 2 0
    //   191: astore 15
    //   193: aload 12
    //   195: iconst_4
    //   196: invokeinterface 219 2 0
    //   201: lstore 4
    //   203: aload_0
    //   204: invokevirtual 562	com/google/android/gms/measurement/internal/zzey:zzjo	()Lcom/google/android/gms/measurement/internal/zzfg;
    //   207: aload 12
    //   209: iconst_5
    //   210: invokeinterface 607 2 0
    //   215: getstatic 1193	com/google/android/gms/measurement/internal/zzad:CREATOR	Landroid/os/Parcelable$Creator;
    //   218: invokevirtual 1196	com/google/android/gms/measurement/internal/zzfg:zza	([BLandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    //   221: checkcast 1189	com/google/android/gms/measurement/internal/zzad
    //   224: astore 16
    //   226: aload 12
    //   228: bipush 6
    //   230: invokeinterface 219 2 0
    //   235: lstore 6
    //   237: aload_0
    //   238: invokevirtual 562	com/google/android/gms/measurement/internal/zzey:zzjo	()Lcom/google/android/gms/measurement/internal/zzfg;
    //   241: aload 12
    //   243: bipush 7
    //   245: invokeinterface 607 2 0
    //   250: getstatic 1193	com/google/android/gms/measurement/internal/zzad:CREATOR	Landroid/os/Parcelable$Creator;
    //   253: invokevirtual 1196	com/google/android/gms/measurement/internal/zzfg:zza	([BLandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    //   256: checkcast 1189	com/google/android/gms/measurement/internal/zzad
    //   259: astore 17
    //   261: aload 12
    //   263: bipush 8
    //   265: invokeinterface 219 2 0
    //   270: lstore 8
    //   272: aload 12
    //   274: bipush 9
    //   276: invokeinterface 219 2 0
    //   281: lstore 10
    //   283: aload_0
    //   284: invokevirtual 562	com/google/android/gms/measurement/internal/zzey:zzjo	()Lcom/google/android/gms/measurement/internal/zzfg;
    //   287: aload 12
    //   289: bipush 10
    //   291: invokeinterface 607 2 0
    //   296: getstatic 1193	com/google/android/gms/measurement/internal/zzad:CREATOR	Landroid/os/Parcelable$Creator;
    //   299: invokevirtual 1196	com/google/android/gms/measurement/internal/zzfg:zza	([BLandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    //   302: checkcast 1189	com/google/android/gms/measurement/internal/zzad
    //   305: astore 18
    //   307: new 950	com/google/android/gms/measurement/internal/zzl
    //   310: dup
    //   311: aload_1
    //   312: aload 13
    //   314: new 959	com/google/android/gms/measurement/internal/zzfh
    //   317: dup
    //   318: aload_2
    //   319: lload 8
    //   321: aload 14
    //   323: aload 13
    //   325: invokespecial 1199	com/google/android/gms/measurement/internal/zzfh:<init>	(Ljava/lang/String;JLjava/lang/Object;Ljava/lang/String;)V
    //   328: lload 6
    //   330: iload_3
    //   331: aload 15
    //   333: aload 16
    //   335: lload 4
    //   337: aload 17
    //   339: lload 10
    //   341: aload 18
    //   343: invokespecial 1202	com/google/android/gms/measurement/internal/zzl:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzfh;JZLjava/lang/String;Lcom/google/android/gms/measurement/internal/zzad;JLcom/google/android/gms/measurement/internal/zzad;JLcom/google/android/gms/measurement/internal/zzad;)V
    //   346: astore 13
    //   348: aload 12
    //   350: invokeinterface 1146 1 0
    //   355: ifeq +28 -> 383
    //   358: aload_0
    //   359: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   362: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   365: ldc_w 1469
    //   368: aload_1
    //   369: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   372: aload_0
    //   373: invokevirtual 1070	com/google/android/gms/measurement/internal/zzco:zzgl	()Lcom/google/android/gms/measurement/internal/zzan;
    //   376: aload_2
    //   377: invokevirtual 1402	com/google/android/gms/measurement/internal/zzan:zzbu	(Ljava/lang/String;)Ljava/lang/String;
    //   380: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   383: aload 13
    //   385: astore_1
    //   386: aload 12
    //   388: ifnull -239 -> 149
    //   391: aload 12
    //   393: invokeinterface 222 1 0
    //   398: aload 13
    //   400: areturn
    //   401: iconst_0
    //   402: istore_3
    //   403: goto -220 -> 183
    //   406: astore 13
    //   408: aconst_null
    //   409: astore 12
    //   411: aload_0
    //   412: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   415: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   418: ldc_w 1471
    //   421: aload_1
    //   422: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   425: aload_0
    //   426: invokevirtual 1070	com/google/android/gms/measurement/internal/zzco:zzgl	()Lcom/google/android/gms/measurement/internal/zzan;
    //   429: aload_2
    //   430: invokevirtual 1402	com/google/android/gms/measurement/internal/zzan:zzbu	(Ljava/lang/String;)Ljava/lang/String;
    //   433: aload 13
    //   435: invokevirtual 359	com/google/android/gms/measurement/internal/zzar:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   438: aload 12
    //   440: ifnull +10 -> 450
    //   443: aload 12
    //   445: invokeinterface 222 1 0
    //   450: aconst_null
    //   451: areturn
    //   452: astore_1
    //   453: aconst_null
    //   454: astore 12
    //   456: aload 12
    //   458: ifnull +10 -> 468
    //   461: aload 12
    //   463: invokeinterface 222 1 0
    //   468: aload_1
    //   469: athrow
    //   470: astore_1
    //   471: goto -15 -> 456
    //   474: astore_1
    //   475: goto -19 -> 456
    //   478: astore 13
    //   480: goto -69 -> 411
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	483	0	this	zzq
    //   0	483	1	paramString1	String
    //   0	483	2	paramString2	String
    //   130	273	3	bool	boolean
    //   201	135	4	l1	long
    //   235	94	6	l2	long
    //   270	50	8	l3	long
    //   281	59	10	l4	long
    //   121	341	12	localCursor	Cursor
    //   159	240	13	localObject1	Object
    //   406	28	13	localSQLiteException1	SQLiteException
    //   478	1	13	localSQLiteException2	SQLiteException
    //   168	154	14	localObject2	Object
    //   191	141	15	str	String
    //   224	110	16	localzzad1	zzad
    //   259	79	17	localzzad2	zzad
    //   305	37	18	localzzad3	zzad
    // Exception table:
    //   from	to	target	type
    //   18	123	406	android/database/sqlite/SQLiteException
    //   18	123	452	finally
    //   123	131	470	finally
    //   151	181	470	finally
    //   183	383	470	finally
    //   411	438	474	finally
    //   123	131	478	android/database/sqlite/SQLiteException
    //   151	181	478	android/database/sqlite/SQLiteException
    //   183	383	478	android/database/sqlite/SQLiteException
  }
  
  @WorkerThread
  public final int zzk(String paramString1, String paramString2)
  {
    Preconditions.checkNotEmpty(paramString1);
    Preconditions.checkNotEmpty(paramString2);
    zzaf();
    zzcl();
    try
    {
      int i = getWritableDatabase().delete("conditional_properties", "app_id=? and name=?", new String[] { paramString1, paramString2 });
      return i;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzgo().zzjd().zzd("Error deleting conditional property", zzap.zzbv(paramString1), zzgl().zzbu(paramString2), localSQLiteException);
    }
    return 0;
  }
  
  /* Error */
  final java.util.Map<Integer, List<zzfv>> zzl(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   4: aload_0
    //   5: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   8: aload_1
    //   9: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   12: pop
    //   13: aload_2
    //   14: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   17: pop
    //   18: new 1339	android/support/v4/util/ArrayMap
    //   21: dup
    //   22: invokespecial 1340	android/support/v4/util/ArrayMap:<init>	()V
    //   25: astore 8
    //   27: aload_0
    //   28: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   31: astore 5
    //   33: aload 5
    //   35: ldc_w 397
    //   38: iconst_2
    //   39: anewarray 19	java/lang/String
    //   42: dup
    //   43: iconst_0
    //   44: ldc_w 383
    //   47: aastore
    //   48: dup
    //   49: iconst_1
    //   50: ldc_w 392
    //   53: aastore
    //   54: ldc_w 1478
    //   57: iconst_2
    //   58: anewarray 19	java/lang/String
    //   61: dup
    //   62: iconst_0
    //   63: aload_1
    //   64: aastore
    //   65: dup
    //   66: iconst_1
    //   67: aload_2
    //   68: aastore
    //   69: aconst_null
    //   70: aconst_null
    //   71: aconst_null
    //   72: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   75: astore 5
    //   77: aload 5
    //   79: astore_2
    //   80: aload 5
    //   82: invokeinterface 215 1 0
    //   87: ifne +26 -> 113
    //   90: aload 5
    //   92: astore_2
    //   93: invokestatic 1482	java/util/Collections:emptyMap	()Ljava/util/Map;
    //   96: astore 6
    //   98: aload 5
    //   100: ifnull +10 -> 110
    //   103: aload 5
    //   105: invokeinterface 222 1 0
    //   110: aload 6
    //   112: areturn
    //   113: aload 5
    //   115: astore_2
    //   116: aload 5
    //   118: iconst_1
    //   119: invokeinterface 607 2 0
    //   124: astore 6
    //   126: aload 5
    //   128: astore_2
    //   129: aload 6
    //   131: iconst_0
    //   132: aload 6
    //   134: arraylength
    //   135: invokestatic 613	com/google/android/gms/internal/measurement/zzyx:zzj	([BII)Lcom/google/android/gms/internal/measurement/zzyx;
    //   138: astore 6
    //   140: aload 5
    //   142: astore_2
    //   143: new 329	com/google/android/gms/internal/measurement/zzfv
    //   146: dup
    //   147: invokespecial 1483	com/google/android/gms/internal/measurement/zzfv:<init>	()V
    //   150: astore 9
    //   152: aload 5
    //   154: astore_2
    //   155: aload 9
    //   157: aload 6
    //   159: invokevirtual 619	com/google/android/gms/internal/measurement/zzzg:zza	(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    //   162: pop
    //   163: aload 5
    //   165: astore_2
    //   166: aload 5
    //   168: iconst_0
    //   169: invokeinterface 1143 2 0
    //   174: istore_3
    //   175: aload 5
    //   177: astore_2
    //   178: aload 8
    //   180: iload_3
    //   181: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   184: invokeinterface 1485 2 0
    //   189: checkcast 465	java/util/List
    //   192: astore 7
    //   194: aload 7
    //   196: astore 6
    //   198: aload 7
    //   200: ifnonnull +32 -> 232
    //   203: aload 5
    //   205: astore_2
    //   206: new 462	java/util/ArrayList
    //   209: dup
    //   210: invokespecial 463	java/util/ArrayList:<init>	()V
    //   213: astore 6
    //   215: aload 5
    //   217: astore_2
    //   218: aload 8
    //   220: iload_3
    //   221: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   224: aload 6
    //   226: invokeinterface 1348 3 0
    //   231: pop
    //   232: aload 5
    //   234: astore_2
    //   235: aload 6
    //   237: aload 9
    //   239: invokeinterface 482 2 0
    //   244: pop
    //   245: aload 5
    //   247: astore_2
    //   248: aload 5
    //   250: invokeinterface 1146 1 0
    //   255: istore 4
    //   257: iload 4
    //   259: ifne -146 -> 113
    //   262: aload 5
    //   264: ifnull +10 -> 274
    //   267: aload 5
    //   269: invokeinterface 222 1 0
    //   274: aload 8
    //   276: areturn
    //   277: astore 6
    //   279: aload 5
    //   281: astore_2
    //   282: aload_0
    //   283: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   286: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   289: ldc_w 1487
    //   292: aload_1
    //   293: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   296: aload 6
    //   298: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   301: goto -56 -> 245
    //   304: astore 6
    //   306: aload 5
    //   308: astore_2
    //   309: aload_0
    //   310: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   313: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   316: ldc_w 460
    //   319: aload_1
    //   320: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   323: aload 6
    //   325: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   328: aload 5
    //   330: ifnull +10 -> 340
    //   333: aload 5
    //   335: invokeinterface 222 1 0
    //   340: aconst_null
    //   341: areturn
    //   342: astore_1
    //   343: aconst_null
    //   344: astore_2
    //   345: aload_2
    //   346: ifnull +9 -> 355
    //   349: aload_2
    //   350: invokeinterface 222 1 0
    //   355: aload_1
    //   356: athrow
    //   357: astore_1
    //   358: goto -13 -> 345
    //   361: astore 6
    //   363: aconst_null
    //   364: astore 5
    //   366: goto -60 -> 306
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	369	0	this	zzq
    //   0	369	1	paramString1	String
    //   0	369	2	paramString2	String
    //   174	47	3	i	int
    //   255	3	4	bool	boolean
    //   31	334	5	localObject1	Object
    //   96	140	6	localObject2	Object
    //   277	20	6	localIOException	IOException
    //   304	20	6	localSQLiteException1	SQLiteException
    //   361	1	6	localSQLiteException2	SQLiteException
    //   192	7	7	localList	List
    //   25	250	8	localArrayMap	android.support.v4.util.ArrayMap
    //   150	88	9	localzzfv	zzfv
    // Exception table:
    //   from	to	target	type
    //   155	163	277	java/io/IOException
    //   80	90	304	android/database/sqlite/SQLiteException
    //   93	98	304	android/database/sqlite/SQLiteException
    //   116	126	304	android/database/sqlite/SQLiteException
    //   129	140	304	android/database/sqlite/SQLiteException
    //   143	152	304	android/database/sqlite/SQLiteException
    //   155	163	304	android/database/sqlite/SQLiteException
    //   166	175	304	android/database/sqlite/SQLiteException
    //   178	194	304	android/database/sqlite/SQLiteException
    //   206	215	304	android/database/sqlite/SQLiteException
    //   218	232	304	android/database/sqlite/SQLiteException
    //   235	245	304	android/database/sqlite/SQLiteException
    //   248	257	304	android/database/sqlite/SQLiteException
    //   282	301	304	android/database/sqlite/SQLiteException
    //   33	77	342	finally
    //   80	90	357	finally
    //   93	98	357	finally
    //   116	126	357	finally
    //   129	140	357	finally
    //   143	152	357	finally
    //   155	163	357	finally
    //   166	175	357	finally
    //   178	194	357	finally
    //   206	215	357	finally
    //   218	232	357	finally
    //   235	245	357	finally
    //   248	257	357	finally
    //   282	301	357	finally
    //   309	328	357	finally
    //   33	77	361	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  final java.util.Map<Integer, List<zzfy>> zzm(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   4: aload_0
    //   5: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   8: aload_1
    //   9: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   12: pop
    //   13: aload_2
    //   14: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   17: pop
    //   18: new 1339	android/support/v4/util/ArrayMap
    //   21: dup
    //   22: invokespecial 1340	android/support/v4/util/ArrayMap:<init>	()V
    //   25: astore 8
    //   27: aload_0
    //   28: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   31: astore 5
    //   33: aload 5
    //   35: ldc_w 422
    //   38: iconst_2
    //   39: anewarray 19	java/lang/String
    //   42: dup
    //   43: iconst_0
    //   44: ldc_w 383
    //   47: aastore
    //   48: dup
    //   49: iconst_1
    //   50: ldc_w 392
    //   53: aastore
    //   54: ldc_w 1491
    //   57: iconst_2
    //   58: anewarray 19	java/lang/String
    //   61: dup
    //   62: iconst_0
    //   63: aload_1
    //   64: aastore
    //   65: dup
    //   66: iconst_1
    //   67: aload_2
    //   68: aastore
    //   69: aconst_null
    //   70: aconst_null
    //   71: aconst_null
    //   72: invokevirtual 642	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   75: astore 5
    //   77: aload 5
    //   79: astore_2
    //   80: aload 5
    //   82: invokeinterface 215 1 0
    //   87: ifne +26 -> 113
    //   90: aload 5
    //   92: astore_2
    //   93: invokestatic 1482	java/util/Collections:emptyMap	()Ljava/util/Map;
    //   96: astore 6
    //   98: aload 5
    //   100: ifnull +10 -> 110
    //   103: aload 5
    //   105: invokeinterface 222 1 0
    //   110: aload 6
    //   112: areturn
    //   113: aload 5
    //   115: astore_2
    //   116: aload 5
    //   118: iconst_1
    //   119: invokeinterface 607 2 0
    //   124: astore 6
    //   126: aload 5
    //   128: astore_2
    //   129: aload 6
    //   131: iconst_0
    //   132: aload 6
    //   134: arraylength
    //   135: invokestatic 613	com/google/android/gms/internal/measurement/zzyx:zzj	([BII)Lcom/google/android/gms/internal/measurement/zzyx;
    //   138: astore 6
    //   140: aload 5
    //   142: astore_2
    //   143: new 412	com/google/android/gms/internal/measurement/zzfy
    //   146: dup
    //   147: invokespecial 1492	com/google/android/gms/internal/measurement/zzfy:<init>	()V
    //   150: astore 9
    //   152: aload 5
    //   154: astore_2
    //   155: aload 9
    //   157: aload 6
    //   159: invokevirtual 619	com/google/android/gms/internal/measurement/zzzg:zza	(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    //   162: pop
    //   163: aload 5
    //   165: astore_2
    //   166: aload 5
    //   168: iconst_0
    //   169: invokeinterface 1143 2 0
    //   174: istore_3
    //   175: aload 5
    //   177: astore_2
    //   178: aload 8
    //   180: iload_3
    //   181: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   184: invokeinterface 1485 2 0
    //   189: checkcast 465	java/util/List
    //   192: astore 7
    //   194: aload 7
    //   196: astore 6
    //   198: aload 7
    //   200: ifnonnull +32 -> 232
    //   203: aload 5
    //   205: astore_2
    //   206: new 462	java/util/ArrayList
    //   209: dup
    //   210: invokespecial 463	java/util/ArrayList:<init>	()V
    //   213: astore 6
    //   215: aload 5
    //   217: astore_2
    //   218: aload 8
    //   220: iload_3
    //   221: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   224: aload 6
    //   226: invokeinterface 1348 3 0
    //   231: pop
    //   232: aload 5
    //   234: astore_2
    //   235: aload 6
    //   237: aload 9
    //   239: invokeinterface 482 2 0
    //   244: pop
    //   245: aload 5
    //   247: astore_2
    //   248: aload 5
    //   250: invokeinterface 1146 1 0
    //   255: istore 4
    //   257: iload 4
    //   259: ifne -146 -> 113
    //   262: aload 5
    //   264: ifnull +10 -> 274
    //   267: aload 5
    //   269: invokeinterface 222 1 0
    //   274: aload 8
    //   276: areturn
    //   277: astore 6
    //   279: aload 5
    //   281: astore_2
    //   282: aload_0
    //   283: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   286: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   289: ldc_w 1494
    //   292: aload_1
    //   293: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   296: aload 6
    //   298: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   301: goto -56 -> 245
    //   304: astore 6
    //   306: aload 5
    //   308: astore_2
    //   309: aload_0
    //   310: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   313: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   316: ldc_w 460
    //   319: aload_1
    //   320: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   323: aload 6
    //   325: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   328: aload 5
    //   330: ifnull +10 -> 340
    //   333: aload 5
    //   335: invokeinterface 222 1 0
    //   340: aconst_null
    //   341: areturn
    //   342: astore_1
    //   343: aconst_null
    //   344: astore_2
    //   345: aload_2
    //   346: ifnull +9 -> 355
    //   349: aload_2
    //   350: invokeinterface 222 1 0
    //   355: aload_1
    //   356: athrow
    //   357: astore_1
    //   358: goto -13 -> 345
    //   361: astore 6
    //   363: aconst_null
    //   364: astore 5
    //   366: goto -60 -> 306
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	369	0	this	zzq
    //   0	369	1	paramString1	String
    //   0	369	2	paramString2	String
    //   174	47	3	i	int
    //   255	3	4	bool	boolean
    //   31	334	5	localObject1	Object
    //   96	140	6	localObject2	Object
    //   277	20	6	localIOException	IOException
    //   304	20	6	localSQLiteException1	SQLiteException
    //   361	1	6	localSQLiteException2	SQLiteException
    //   192	7	7	localList	List
    //   25	250	8	localArrayMap	android.support.v4.util.ArrayMap
    //   150	88	9	localzzfy	zzfy
    // Exception table:
    //   from	to	target	type
    //   155	163	277	java/io/IOException
    //   80	90	304	android/database/sqlite/SQLiteException
    //   93	98	304	android/database/sqlite/SQLiteException
    //   116	126	304	android/database/sqlite/SQLiteException
    //   129	140	304	android/database/sqlite/SQLiteException
    //   143	152	304	android/database/sqlite/SQLiteException
    //   155	163	304	android/database/sqlite/SQLiteException
    //   166	175	304	android/database/sqlite/SQLiteException
    //   178	194	304	android/database/sqlite/SQLiteException
    //   206	215	304	android/database/sqlite/SQLiteException
    //   218	232	304	android/database/sqlite/SQLiteException
    //   235	245	304	android/database/sqlite/SQLiteException
    //   248	257	304	android/database/sqlite/SQLiteException
    //   282	301	304	android/database/sqlite/SQLiteException
    //   33	77	342	finally
    //   80	90	357	finally
    //   93	98	357	finally
    //   116	126	357	finally
    //   129	140	357	finally
    //   143	152	357	finally
    //   155	163	357	finally
    //   166	175	357	finally
    //   178	194	357	finally
    //   206	215	357	finally
    //   218	232	357	finally
    //   235	245	357	finally
    //   248	257	357	finally
    //   282	301	357	finally
    //   309	328	357	finally
    //   33	77	361	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  @WorkerThread
  @VisibleForTesting
  protected final long zzn(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   4: pop
    //   5: aload_2
    //   6: invokestatic 297	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   9: pop
    //   10: aload_0
    //   11: invokevirtual 327	com/google/android/gms/measurement/internal/zzco:zzaf	()V
    //   14: aload_0
    //   15: invokevirtual 324	com/google/android/gms/measurement/internal/zzez:zzcl	()V
    //   18: aload_0
    //   19: invokevirtual 203	com/google/android/gms/measurement/internal/zzq:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   22: astore 8
    //   24: aload 8
    //   26: invokevirtual 541	android/database/sqlite/SQLiteDatabase:beginTransaction	()V
    //   29: aload_0
    //   30: new 490	java/lang/StringBuilder
    //   33: dup
    //   34: aload_2
    //   35: invokestatic 355	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   38: invokevirtual 493	java/lang/String:length	()I
    //   41: bipush 32
    //   43: iadd
    //   44: invokespecial 496	java/lang/StringBuilder:<init>	(I)V
    //   47: ldc_w 1499
    //   50: invokevirtual 502	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   53: aload_2
    //   54: invokevirtual 502	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: ldc_w 1501
    //   60: invokevirtual 502	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   63: invokevirtual 507	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   66: iconst_1
    //   67: anewarray 19	java/lang/String
    //   70: dup
    //   71: iconst_0
    //   72: aload_1
    //   73: aastore
    //   74: ldc2_w 402
    //   77: invokespecial 1358	com/google/android/gms/measurement/internal/zzq:zza	(Ljava/lang/String;[Ljava/lang/String;J)J
    //   80: lstore 5
    //   82: lload 5
    //   84: lstore_3
    //   85: lload 5
    //   87: ldc2_w 402
    //   90: lcmp
    //   91: ifne +92 -> 183
    //   94: new 303	android/content/ContentValues
    //   97: dup
    //   98: invokespecial 379	android/content/ContentValues:<init>	()V
    //   101: astore 7
    //   103: aload 7
    //   105: ldc_w 381
    //   108: aload_1
    //   109: invokevirtual 307	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   112: aload 7
    //   114: ldc_w 1503
    //   117: iconst_0
    //   118: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   121: invokevirtual 386	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   124: aload 7
    //   126: ldc -97
    //   128: iconst_0
    //   129: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   132: invokevirtual 386	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   135: aload 8
    //   137: ldc_w 1505
    //   140: aconst_null
    //   141: aload 7
    //   143: iconst_5
    //   144: invokevirtual 401	android/database/sqlite/SQLiteDatabase:insertWithOnConflict	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    //   147: ldc2_w 402
    //   150: lcmp
    //   151: ifne +30 -> 181
    //   154: aload_0
    //   155: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   158: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   161: ldc_w 1507
    //   164: aload_1
    //   165: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   168: aload_2
    //   169: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   172: aload 8
    //   174: invokevirtual 544	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   177: ldc2_w 402
    //   180: lreturn
    //   181: lconst_0
    //   182: lstore_3
    //   183: new 303	android/content/ContentValues
    //   186: dup
    //   187: invokespecial 379	android/content/ContentValues:<init>	()V
    //   190: astore 7
    //   192: aload 7
    //   194: ldc_w 381
    //   197: aload_1
    //   198: invokevirtual 307	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   201: aload 7
    //   203: aload_2
    //   204: lconst_1
    //   205: lload_3
    //   206: ladd
    //   207: invokestatic 275	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   210: invokevirtual 310	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   213: aload 8
    //   215: ldc_w 1505
    //   218: aload 7
    //   220: ldc_w 771
    //   223: iconst_1
    //   224: anewarray 19	java/lang/String
    //   227: dup
    //   228: iconst_0
    //   229: aload_1
    //   230: aastore
    //   231: invokevirtual 664	android/database/sqlite/SQLiteDatabase:update	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   234: i2l
    //   235: lconst_0
    //   236: lcmp
    //   237: ifne +30 -> 267
    //   240: aload_0
    //   241: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   244: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   247: ldc_w 1509
    //   250: aload_1
    //   251: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   254: aload_2
    //   255: invokevirtual 245	com/google/android/gms/measurement/internal/zzar:zze	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   258: aload 8
    //   260: invokevirtual 544	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   263: ldc2_w 402
    //   266: lreturn
    //   267: aload 8
    //   269: invokevirtual 550	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   272: aload 8
    //   274: invokevirtual 544	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   277: lload_3
    //   278: lreturn
    //   279: astore 7
    //   281: lconst_0
    //   282: lstore_3
    //   283: aload_0
    //   284: invokevirtual 231	com/google/android/gms/measurement/internal/zzco:zzgo	()Lcom/google/android/gms/measurement/internal/zzap;
    //   287: invokevirtual 237	com/google/android/gms/measurement/internal/zzap:zzjd	()Lcom/google/android/gms/measurement/internal/zzar;
    //   290: ldc_w 1511
    //   293: aload_1
    //   294: invokestatic 348	com/google/android/gms/measurement/internal/zzap:zzbv	(Ljava/lang/String;)Ljava/lang/Object;
    //   297: aload_2
    //   298: aload 7
    //   300: invokevirtual 359	com/google/android/gms/measurement/internal/zzar:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   303: aload 8
    //   305: invokevirtual 544	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   308: lload_3
    //   309: lreturn
    //   310: astore_1
    //   311: aload 8
    //   313: invokevirtual 544	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   316: aload_1
    //   317: athrow
    //   318: astore 7
    //   320: goto -37 -> 283
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	323	0	this	zzq
    //   0	323	1	paramString1	String
    //   0	323	2	paramString2	String
    //   84	225	3	l1	long
    //   80	6	5	l2	long
    //   101	118	7	localContentValues	ContentValues
    //   279	20	7	localSQLiteException1	SQLiteException
    //   318	1	7	localSQLiteException2	SQLiteException
    //   22	290	8	localSQLiteDatabase	SQLiteDatabase
    // Exception table:
    //   from	to	target	type
    //   29	82	279	android/database/sqlite/SQLiteException
    //   94	172	279	android/database/sqlite/SQLiteException
    //   29	82	310	finally
    //   94	172	310	finally
    //   183	258	310	finally
    //   267	272	310	finally
    //   283	303	310	finally
    //   183	258	318	android/database/sqlite/SQLiteException
    //   267	272	318	android/database/sqlite/SQLiteException
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */