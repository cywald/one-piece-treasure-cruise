package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;

final class zzz
{
  final String name;
  final long zzaie;
  final long zzaif;
  final long zzaig;
  final long zzaih;
  final Long zzaii;
  final Long zzaij;
  final Long zzaik;
  final Boolean zzail;
  final String zztt;
  
  zzz(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, Long paramLong5, Long paramLong6, Long paramLong7, Boolean paramBoolean)
  {
    Preconditions.checkNotEmpty(paramString1);
    Preconditions.checkNotEmpty(paramString2);
    if (paramLong1 >= 0L)
    {
      bool = true;
      Preconditions.checkArgument(bool);
      if (paramLong2 < 0L) {
        break label122;
      }
      bool = true;
      label38:
      Preconditions.checkArgument(bool);
      if (paramLong4 < 0L) {
        break label128;
      }
    }
    label122:
    label128:
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool);
      this.zztt = paramString1;
      this.name = paramString2;
      this.zzaie = paramLong1;
      this.zzaif = paramLong2;
      this.zzaig = paramLong3;
      this.zzaih = paramLong4;
      this.zzaii = paramLong5;
      this.zzaij = paramLong6;
      this.zzaik = paramLong7;
      this.zzail = paramBoolean;
      return;
      bool = false;
      break;
      bool = false;
      break label38;
    }
  }
  
  final zzz zza(long paramLong1, long paramLong2)
  {
    return new zzz(this.zztt, this.name, this.zzaie, this.zzaif, this.zzaig, paramLong1, Long.valueOf(paramLong2), this.zzaij, this.zzaik, this.zzail);
  }
  
  final zzz zza(Long paramLong1, Long paramLong2, Boolean paramBoolean)
  {
    if ((paramBoolean != null) && (!paramBoolean.booleanValue())) {
      paramBoolean = null;
    }
    for (;;)
    {
      return new zzz(this.zztt, this.name, this.zzaie, this.zzaif, this.zzaig, this.zzaih, this.zzaii, paramLong1, paramLong2, paramBoolean);
    }
  }
  
  final zzz zzai(long paramLong)
  {
    return new zzz(this.zztt, this.name, this.zzaie, this.zzaif, paramLong, this.zzaih, this.zzaii, this.zzaij, this.zzaik, this.zzail);
  }
  
  final zzz zziu()
  {
    return new zzz(this.zztt, this.name, this.zzaie + 1L, this.zzaif + 1L, this.zzaig, this.zzaih, this.zzaii, this.zzaij, this.zzaik, this.zzail);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */