package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.Preconditions;

public final class zzbd
{
  private long value;
  private boolean zzanx;
  private final long zzanz;
  private final String zzoj;
  
  public zzbd(zzba paramzzba, String paramString, long paramLong)
  {
    Preconditions.checkNotEmpty(paramString);
    this.zzoj = paramString;
    this.zzanz = paramLong;
  }
  
  @WorkerThread
  public final long get()
  {
    if (!this.zzanx)
    {
      this.zzanx = true;
      this.value = zzba.zza(this.zzany).getLong(this.zzoj, this.zzanz);
    }
    return this.value;
  }
  
  @WorkerThread
  public final void set(long paramLong)
  {
    SharedPreferences.Editor localEditor = zzba.zza(this.zzany).edit();
    localEditor.putLong(this.zzoj, paramLong);
    localEditor.apply();
    this.value = paramLong;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzbd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */