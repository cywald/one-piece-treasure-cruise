package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.api.internal.GoogleServices;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.CollectionUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.measurement.AppMeasurement.ConditionalUserProperty;
import com.google.android.gms.measurement.AppMeasurement.Event;
import com.google.android.gms.measurement.AppMeasurement.EventInterceptor;
import com.google.android.gms.measurement.AppMeasurement.OnEventListener;
import com.google.android.gms.measurement.AppMeasurement.UserProperty;
import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

public final class zzcs
  extends zzf
{
  @VisibleForTesting
  protected zzdm zzaqv;
  private AppMeasurement.EventInterceptor zzaqw;
  private final Set<AppMeasurement.OnEventListener> zzaqx = new CopyOnWriteArraySet();
  private boolean zzaqy;
  private final AtomicReference<String> zzaqz = new AtomicReference();
  @VisibleForTesting
  protected boolean zzara = true;
  
  protected zzcs(zzbt paramzzbt)
  {
    super(paramzzbt);
  }
  
  private final void zza(AppMeasurement.ConditionalUserProperty paramConditionalUserProperty)
  {
    long l = zzbx().currentTimeMillis();
    Preconditions.checkNotNull(paramConditionalUserProperty);
    Preconditions.checkNotEmpty(paramConditionalUserProperty.mName);
    Preconditions.checkNotEmpty(paramConditionalUserProperty.mOrigin);
    Preconditions.checkNotNull(paramConditionalUserProperty.mValue);
    paramConditionalUserProperty.mCreationTimestamp = l;
    String str = paramConditionalUserProperty.mName;
    Object localObject1 = paramConditionalUserProperty.mValue;
    if (zzgm().zzcs(str) != 0)
    {
      zzgo().zzjd().zzg("Invalid conditional user property name", zzgl().zzbu(str));
      return;
    }
    if (zzgm().zzi(str, localObject1) != 0)
    {
      zzgo().zzjd().zze("Invalid conditional user property value", zzgl().zzbu(str), localObject1);
      return;
    }
    Object localObject2 = zzgm().zzj(str, localObject1);
    if (localObject2 == null)
    {
      zzgo().zzjd().zze("Unable to normalize conditional user property value", zzgl().zzbu(str), localObject1);
      return;
    }
    paramConditionalUserProperty.mValue = localObject2;
    l = paramConditionalUserProperty.mTriggerTimeout;
    if ((!TextUtils.isEmpty(paramConditionalUserProperty.mTriggerEventName)) && ((l > 15552000000L) || (l < 1L)))
    {
      zzgo().zzjd().zze("Invalid conditional user property timeout", zzgl().zzbu(str), Long.valueOf(l));
      return;
    }
    l = paramConditionalUserProperty.mTimeToLive;
    if ((l > 15552000000L) || (l < 1L))
    {
      zzgo().zzjd().zze("Invalid conditional user property time to live", zzgl().zzbu(str), Long.valueOf(l));
      return;
    }
    zzgn().zzc(new zzda(this, paramConditionalUserProperty));
  }
  
  @WorkerThread
  private final void zza(String paramString1, String paramString2, long paramLong, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString3)
  {
    Preconditions.checkNotEmpty(paramString1);
    Preconditions.checkNotEmpty(paramString2);
    Preconditions.checkNotNull(paramBundle);
    zzaf();
    zzcl();
    if (!this.zzadj.isEnabled()) {
      zzgo().zzjk().zzbx("Event not sent since app measurement is disabled");
    }
    label98:
    int i;
    Object localObject3;
    boolean bool1;
    do
    {
      return;
      if (!this.zzaqy) {
        this.zzaqy = true;
      }
      try
      {
        localObject1 = Class.forName("com.google.android.gms.tagmanager.TagManagerService");
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        for (;;)
        {
          Object localObject1;
          zzgo().zzjj().zzbx("Tag Manager is not found and thus will not be used");
          continue;
          if (!localClassNotFoundException.zza("event", AppMeasurement.Event.zzadk, paramString2))
          {
            i = 13;
          }
          else if (!localClassNotFoundException.zza("event", 40, paramString2))
          {
            i = 2;
          }
          else
          {
            i = 0;
            continue;
            j = 0;
          }
        }
        zzgr();
        localObject3 = zzgh().zzla();
        if ((localObject3 == null) || (paramBundle.containsKey("_sc"))) {
          continue;
        }
        ((zzdn)localObject3).zzarn = true;
        if ((!paramBoolean1) || (!paramBoolean3)) {
          continue;
        }
        bool1 = true;
        zzdo.zza((zzdn)localObject3, paramBundle, bool1);
        bool1 = "am".equals(paramString1);
        boolean bool2 = zzfk.zzcv(paramString2);
        if ((!paramBoolean1) || (this.zzaqw == null) || (bool2) || (bool1)) {
          continue;
        }
        zzgo().zzjk().zze("Passing event to registered event handler (FE)", zzgl().zzbs(paramString2), zzgl().zzd(paramBundle));
        this.zzaqw.interceptEvent(paramString1, paramString2, paramBundle, paramLong);
        return;
        bool1 = false;
        continue;
      }
      try
      {
        ((Class)localObject1).getDeclaredMethod("initialize", new Class[] { Context.class }).invoke(null, new Object[] { getContext() });
        if (paramBoolean3)
        {
          zzgr();
          if (!"_iap".equals(paramString2))
          {
            localObject1 = this.zzadj.zzgm();
            if (!((zzfk)localObject1).zzr("event", paramString2))
            {
              i = 2;
              if (i == 0) {
                continue;
              }
              zzgo().zzjf().zzg("Invalid public event name. Event will not be logged (FE)", zzgl().zzbs(paramString2));
              this.zzadj.zzgm();
              paramString1 = zzfk.zza(paramString2, 40, true);
              if (paramString2 == null) {
                continue;
              }
              j = paramString2.length();
              this.zzadj.zzgm().zza(i, "_ev", paramString1, j);
            }
          }
        }
      }
      catch (Exception localException)
      {
        zzgo().zzjg().zzg("Failed to invoke Tag Manager's initialize() method", localException);
        break label98;
      }
    } while (!this.zzadj.zzkr());
    int j = zzgm().zzcr(paramString2);
    if (j != 0)
    {
      zzgo().zzjf().zzg("Invalid event name. Event will not be logged (FE)", zzgl().zzbs(paramString2));
      zzgm();
      paramString1 = zzfk.zza(paramString2, 40, true);
      if (paramString2 != null) {}
      for (i = paramString2.length();; i = 0)
      {
        this.zzadj.zzgm().zza(paramString3, j, "_ev", paramString1, i);
        return;
      }
    }
    List localList = CollectionUtils.listOf(new String[] { "_o", "_sn", "_sc", "_si" });
    Bundle localBundle1 = zzgm().zza(paramString3, paramString2, paramBundle, localList, paramBoolean3, true);
    Object localObject2;
    if ((localBundle1 == null) || (!localBundle1.containsKey("_sc")) || (!localBundle1.containsKey("_si")))
    {
      localObject2 = null;
      if (localObject2 != null) {
        break label1210;
      }
      localObject2 = localObject3;
    }
    label707:
    label1004:
    label1154:
    label1160:
    label1163:
    label1207:
    label1210:
    for (;;)
    {
      localObject3 = new ArrayList();
      ((List)localObject3).add(localBundle1);
      long l = zzgm().zzmd().nextLong();
      i = 0;
      paramBundle = (String[])localBundle1.keySet().toArray(new String[paramBundle.size()]);
      Arrays.sort(paramBundle);
      int m = paramBundle.length;
      j = 0;
      if (j < m)
      {
        String str = paramBundle[j];
        Object localObject4 = localBundle1.get(str);
        zzgm();
        localObject4 = zzfk.zze(localObject4);
        if (localObject4 == null) {
          break label1207;
        }
        localBundle1.putInt(str, localObject4.length);
        int k = 0;
        for (;;)
        {
          if (k < localObject4.length)
          {
            Bundle localBundle2 = localObject4[k];
            zzdo.zza((zzdn)localObject2, localBundle2, true);
            localBundle2 = zzgm().zza(paramString3, "_ep", localBundle2, localList, paramBoolean3, false);
            localBundle2.putString("_en", paramString2);
            localBundle2.putLong("_eid", l);
            localBundle2.putString("_gn", str);
            localBundle2.putInt("_ll", localObject4.length);
            localBundle2.putInt("_i", k);
            ((List)localObject3).add(localBundle2);
            k += 1;
            continue;
            localObject2 = new zzdn(localBundle1.getString("_sn"), localBundle1.getString("_sc"), Long.valueOf(localBundle1.getLong("_si")).longValue());
            break;
          }
        }
        i = localObject4.length + i;
      }
      for (;;)
      {
        j += 1;
        break label707;
        if (i != 0)
        {
          localBundle1.putLong("_eid", l);
          localBundle1.putInt("_epc", i);
        }
        i = 0;
        while (i < ((List)localObject3).size())
        {
          localObject2 = (Bundle)((List)localObject3).get(i);
          if (i != 0)
          {
            j = 1;
            if (j == 0) {
              break label1154;
            }
            paramBundle = "_ep";
            ((Bundle)localObject2).putString("_o", paramString1);
            if (!paramBoolean2) {
              break label1160;
            }
            localObject2 = zzgm().zze((Bundle)localObject2);
          }
          for (;;)
          {
            zzgo().zzjk().zze("Logging event (FE)", zzgl().zzbs(paramString2), zzgl().zzd((Bundle)localObject2));
            paramBundle = new zzad(paramBundle, new zzaa((Bundle)localObject2), paramString1, paramLong);
            zzgg().zzb(paramBundle, paramString3);
            if (bool1) {
              break label1163;
            }
            paramBundle = this.zzaqx.iterator();
            while (paramBundle.hasNext()) {
              ((AppMeasurement.OnEventListener)paramBundle.next()).onEvent(paramString1, paramString2, new Bundle((Bundle)localObject2), paramLong);
            }
            j = 0;
            break;
            paramBundle = paramString2;
            break label1004;
          }
          i += 1;
        }
        zzgr();
        if ((zzgh().zzla() == null) || (!"_ae".equals(paramString2))) {
          break;
        }
        zzgj().zzn(true);
        return;
      }
    }
  }
  
  private final void zza(String paramString1, String paramString2, long paramLong, Object paramObject)
  {
    zzgn().zzc(new zzcv(this, paramString1, paramString2, paramObject, paramLong));
  }
  
  private final void zza(String paramString1, String paramString2, String paramString3, Bundle paramBundle)
  {
    long l = zzbx().currentTimeMillis();
    Preconditions.checkNotEmpty(paramString2);
    AppMeasurement.ConditionalUserProperty localConditionalUserProperty = new AppMeasurement.ConditionalUserProperty();
    localConditionalUserProperty.mAppId = paramString1;
    localConditionalUserProperty.mName = paramString2;
    localConditionalUserProperty.mCreationTimestamp = l;
    if (paramString3 != null)
    {
      localConditionalUserProperty.mExpiredEventName = paramString3;
      localConditionalUserProperty.mExpiredEventParams = paramBundle;
    }
    zzgn().zzc(new zzdb(this, localConditionalUserProperty));
  }
  
  @Nullable
  private final String zzak(long paramLong)
  {
    synchronized (new AtomicReference())
    {
      zzgn().zzc(new zzcy(this, ???));
      try
      {
        ???.wait(paramLong);
        return (String)???.get();
      }
      catch (InterruptedException localInterruptedException)
      {
        zzgo().zzjg().zzbx("Interrupted waiting for app instance id");
        return null;
      }
    }
  }
  
  @VisibleForTesting
  private final Map<String, Object> zzb(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    if (zzgn().zzkb())
    {
      zzgo().zzjd().zzbx("Cannot get user properties from analytics worker thread");
      return Collections.emptyMap();
    }
    if (zzk.isMainThread())
    {
      zzgo().zzjd().zzbx("Cannot get user properties from main thread");
      return Collections.emptyMap();
    }
    synchronized (new AtomicReference())
    {
      this.zzadj.zzgn().zzc(new zzde(this, ???, paramString1, paramString2, paramString3, paramBoolean));
      try
      {
        ???.wait(5000L);
        paramString2 = (List)???.get();
        if (paramString2 == null)
        {
          zzgo().zzjg().zzbx("Timed out waiting for get user properties");
          return Collections.emptyMap();
        }
      }
      catch (InterruptedException paramString1)
      {
        for (;;)
        {
          zzgo().zzjg().zzg("Interrupted waiting for get user properties", paramString1);
        }
      }
    }
    paramString1 = new ArrayMap(paramString2.size());
    paramString2 = paramString2.iterator();
    while (paramString2.hasNext())
    {
      paramString3 = (zzfh)paramString2.next();
      paramString1.put(paramString3.name, paramString3.getValue());
    }
    return paramString1;
  }
  
  @WorkerThread
  private final void zzb(AppMeasurement.ConditionalUserProperty paramConditionalUserProperty)
  {
    zzaf();
    zzcl();
    Preconditions.checkNotNull(paramConditionalUserProperty);
    Preconditions.checkNotEmpty(paramConditionalUserProperty.mName);
    Preconditions.checkNotEmpty(paramConditionalUserProperty.mOrigin);
    Preconditions.checkNotNull(paramConditionalUserProperty.mValue);
    if (!this.zzadj.isEnabled())
    {
      zzgo().zzjk().zzbx("Conditional property not sent since collection is disabled");
      return;
    }
    zzfh localzzfh = new zzfh(paramConditionalUserProperty.mName, paramConditionalUserProperty.mTriggeredTimestamp, paramConditionalUserProperty.mValue, paramConditionalUserProperty.mOrigin);
    try
    {
      zzad localzzad1 = zzgm().zza(paramConditionalUserProperty.mAppId, paramConditionalUserProperty.mTriggeredEventName, paramConditionalUserProperty.mTriggeredEventParams, paramConditionalUserProperty.mOrigin, 0L, true, false);
      zzad localzzad2 = zzgm().zza(paramConditionalUserProperty.mAppId, paramConditionalUserProperty.mTimedOutEventName, paramConditionalUserProperty.mTimedOutEventParams, paramConditionalUserProperty.mOrigin, 0L, true, false);
      zzad localzzad3 = zzgm().zza(paramConditionalUserProperty.mAppId, paramConditionalUserProperty.mExpiredEventName, paramConditionalUserProperty.mExpiredEventParams, paramConditionalUserProperty.mOrigin, 0L, true, false);
      paramConditionalUserProperty = new zzl(paramConditionalUserProperty.mAppId, paramConditionalUserProperty.mOrigin, localzzfh, paramConditionalUserProperty.mCreationTimestamp, false, paramConditionalUserProperty.mTriggerEventName, localzzad2, paramConditionalUserProperty.mTriggerTimeout, localzzad1, paramConditionalUserProperty.mTimeToLive, localzzad3);
      zzgg().zzd(paramConditionalUserProperty);
      return;
    }
    catch (IllegalArgumentException paramConditionalUserProperty) {}
  }
  
  private final void zzb(String paramString1, String paramString2, long paramLong, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString3)
  {
    paramBundle = zzfk.zzf(paramBundle);
    zzgn().zzc(new zzcu(this, paramString1, paramString2, paramLong, paramBundle, paramBoolean1, paramBoolean2, paramBoolean3, paramString3));
  }
  
  @WorkerThread
  private final void zzc(AppMeasurement.ConditionalUserProperty paramConditionalUserProperty)
  {
    zzaf();
    zzcl();
    Preconditions.checkNotNull(paramConditionalUserProperty);
    Preconditions.checkNotEmpty(paramConditionalUserProperty.mName);
    if (!this.zzadj.isEnabled())
    {
      zzgo().zzjk().zzbx("Conditional property not cleared since collection is disabled");
      return;
    }
    zzfh localzzfh = new zzfh(paramConditionalUserProperty.mName, 0L, null, null);
    try
    {
      zzad localzzad = zzgm().zza(paramConditionalUserProperty.mAppId, paramConditionalUserProperty.mExpiredEventName, paramConditionalUserProperty.mExpiredEventParams, paramConditionalUserProperty.mOrigin, paramConditionalUserProperty.mCreationTimestamp, true, false);
      paramConditionalUserProperty = new zzl(paramConditionalUserProperty.mAppId, paramConditionalUserProperty.mOrigin, localzzfh, paramConditionalUserProperty.mCreationTimestamp, paramConditionalUserProperty.mActive, paramConditionalUserProperty.mTriggerEventName, null, paramConditionalUserProperty.mTriggerTimeout, null, paramConditionalUserProperty.mTimeToLive, localzzad);
      zzgg().zzd(paramConditionalUserProperty);
      return;
    }
    catch (IllegalArgumentException paramConditionalUserProperty) {}
  }
  
  @VisibleForTesting
  private final List<AppMeasurement.ConditionalUserProperty> zzf(String paramString1, String paramString2, String paramString3)
  {
    if (zzgn().zzkb())
    {
      zzgo().zzjd().zzbx("Cannot get conditional user properties from analytics worker thread");
      return Collections.emptyList();
    }
    if (zzk.isMainThread())
    {
      zzgo().zzjd().zzbx("Cannot get conditional user properties from main thread");
      return Collections.emptyList();
    }
    synchronized (new AtomicReference())
    {
      this.zzadj.zzgn().zzc(new zzdc(this, (AtomicReference)???, paramString1, paramString2, paramString3));
      try
      {
        ???.wait(5000L);
        paramString2 = (List)((AtomicReference)???).get();
        if (paramString2 == null)
        {
          zzgo().zzjg().zzg("Timed out waiting for get conditional user properties", paramString1);
          return Collections.emptyList();
        }
      }
      catch (InterruptedException paramString2)
      {
        for (;;)
        {
          zzgo().zzjg().zze("Interrupted waiting for get conditional user properties", paramString1, paramString2);
        }
      }
    }
    paramString1 = new ArrayList(paramString2.size());
    paramString2 = paramString2.iterator();
    while (paramString2.hasNext())
    {
      paramString3 = (zzl)paramString2.next();
      ??? = new AppMeasurement.ConditionalUserProperty();
      ((AppMeasurement.ConditionalUserProperty)???).mAppId = paramString3.packageName;
      ((AppMeasurement.ConditionalUserProperty)???).mOrigin = paramString3.origin;
      ((AppMeasurement.ConditionalUserProperty)???).mCreationTimestamp = paramString3.creationTimestamp;
      ((AppMeasurement.ConditionalUserProperty)???).mName = paramString3.zzahb.name;
      ((AppMeasurement.ConditionalUserProperty)???).mValue = paramString3.zzahb.getValue();
      ((AppMeasurement.ConditionalUserProperty)???).mActive = paramString3.active;
      ((AppMeasurement.ConditionalUserProperty)???).mTriggerEventName = paramString3.triggerEventName;
      if (paramString3.zzahc != null)
      {
        ((AppMeasurement.ConditionalUserProperty)???).mTimedOutEventName = paramString3.zzahc.name;
        if (paramString3.zzahc.zzaid != null) {
          ((AppMeasurement.ConditionalUserProperty)???).mTimedOutEventParams = paramString3.zzahc.zzaid.zziv();
        }
      }
      ((AppMeasurement.ConditionalUserProperty)???).mTriggerTimeout = paramString3.triggerTimeout;
      if (paramString3.zzahd != null)
      {
        ((AppMeasurement.ConditionalUserProperty)???).mTriggeredEventName = paramString3.zzahd.name;
        if (paramString3.zzahd.zzaid != null) {
          ((AppMeasurement.ConditionalUserProperty)???).mTriggeredEventParams = paramString3.zzahd.zzaid.zziv();
        }
      }
      ((AppMeasurement.ConditionalUserProperty)???).mTriggeredTimestamp = paramString3.zzahb.zzaue;
      ((AppMeasurement.ConditionalUserProperty)???).mTimeToLive = paramString3.timeToLive;
      if (paramString3.zzahe != null)
      {
        ((AppMeasurement.ConditionalUserProperty)???).mExpiredEventName = paramString3.zzahe.name;
        if (paramString3.zzahe.zzaid != null) {
          ((AppMeasurement.ConditionalUserProperty)???).mExpiredEventParams = paramString3.zzahe.zzaid.zziv();
        }
      }
      paramString1.add(???);
    }
    return paramString1;
  }
  
  @WorkerThread
  private final void zzk(boolean paramBoolean)
  {
    zzaf();
    zzgb();
    zzcl();
    zzgo().zzjk().zzg("Setting app measurement enabled (FE)", Boolean.valueOf(paramBoolean));
    zzgp().setMeasurementEnabled(paramBoolean);
    zzky();
  }
  
  @WorkerThread
  private final void zzky()
  {
    if (zzgq().zze(zzgf().zzal(), zzaf.zzalj)) {
      this.zzadj.zzj(false);
    }
    if ((zzgq().zzbd(zzgf().zzal())) && (this.zzadj.isEnabled()) && (this.zzara))
    {
      zzgo().zzjk().zzbx("Recording app launch after enabling measurement for the first time (FE)");
      zzkz();
      return;
    }
    zzgo().zzjk().zzbx("Updating Scion state (FE)");
    zzgg().zzlc();
  }
  
  public final void clearConditionalUserProperty(String paramString1, String paramString2, Bundle paramBundle)
  {
    zzgb();
    zza(null, paramString1, paramString2, paramBundle);
  }
  
  public final void clearConditionalUserPropertyAs(String paramString1, String paramString2, String paramString3, Bundle paramBundle)
  {
    Preconditions.checkNotEmpty(paramString1);
    zzga();
    zza(paramString1, paramString2, paramString3, paramBundle);
  }
  
  public final List<AppMeasurement.ConditionalUserProperty> getConditionalUserProperties(String paramString1, String paramString2)
  {
    zzgb();
    return zzf(null, paramString1, paramString2);
  }
  
  public final List<AppMeasurement.ConditionalUserProperty> getConditionalUserPropertiesAs(String paramString1, String paramString2, String paramString3)
  {
    Preconditions.checkNotEmpty(paramString1);
    zzga();
    return zzf(paramString1, paramString2, paramString3);
  }
  
  @Nullable
  public final String getCurrentScreenClass()
  {
    zzdn localzzdn = this.zzadj.zzgh().zzlb();
    if (localzzdn != null) {
      return localzzdn.zzarl;
    }
    return null;
  }
  
  @Nullable
  public final String getCurrentScreenName()
  {
    zzdn localzzdn = this.zzadj.zzgh().zzlb();
    if (localzzdn != null) {
      return localzzdn.zzuw;
    }
    return null;
  }
  
  @Nullable
  public final String getGmpAppId()
  {
    if (this.zzadj.zzkk() != null) {
      return this.zzadj.zzkk();
    }
    try
    {
      String str = GoogleServices.getGoogleAppId();
      return str;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      this.zzadj.zzgo().zzjd().zzg("getGoogleAppId failed with exception", localIllegalStateException);
    }
    return null;
  }
  
  public final Map<String, Object> getUserProperties(String paramString1, String paramString2, boolean paramBoolean)
  {
    zzgb();
    return zzb(null, paramString1, paramString2, paramBoolean);
  }
  
  public final Map<String, Object> getUserPropertiesAs(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    Preconditions.checkNotEmpty(paramString1);
    zzga();
    return zzb(paramString1, paramString2, paramString3, paramBoolean);
  }
  
  public final void logEvent(String paramString1, String paramString2, Bundle paramBundle)
  {
    logEvent(paramString1, paramString2, paramBundle, true, true, zzbx().currentTimeMillis());
  }
  
  public final void logEvent(String paramString1, String paramString2, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2, long paramLong)
  {
    zzgb();
    if (paramString1 == null) {
      paramString1 = "app";
    }
    for (;;)
    {
      if (paramBundle == null) {
        paramBundle = new Bundle();
      }
      for (;;)
      {
        boolean bool;
        if ((!paramBoolean2) || (this.zzaqw == null) || (zzfk.zzcv(paramString2)))
        {
          bool = true;
          if (paramBoolean1) {
            break label77;
          }
        }
        label77:
        for (paramBoolean1 = true;; paramBoolean1 = false)
        {
          zzb(paramString1, paramString2, paramLong, paramBundle, paramBoolean2, bool, paramBoolean1, null);
          return;
          bool = false;
          break;
        }
      }
    }
  }
  
  public final void registerOnMeasurementEventListener(AppMeasurement.OnEventListener paramOnEventListener)
  {
    zzgb();
    zzcl();
    Preconditions.checkNotNull(paramOnEventListener);
    if (!this.zzaqx.add(paramOnEventListener)) {
      zzgo().zzjg().zzbx("OnEventListener already registered");
    }
  }
  
  public final void resetAnalyticsData(long paramLong)
  {
    if (zzgq().zza(zzaf.zzalk)) {
      zzcm(null);
    }
    zzgn().zzc(new zzcz(this, paramLong));
  }
  
  public final void setConditionalUserProperty(AppMeasurement.ConditionalUserProperty paramConditionalUserProperty)
  {
    Preconditions.checkNotNull(paramConditionalUserProperty);
    zzgb();
    paramConditionalUserProperty = new AppMeasurement.ConditionalUserProperty(paramConditionalUserProperty);
    if (!TextUtils.isEmpty(paramConditionalUserProperty.mAppId)) {
      zzgo().zzjg().zzbx("Package name should be null when calling setConditionalUserProperty");
    }
    paramConditionalUserProperty.mAppId = null;
    zza(paramConditionalUserProperty);
  }
  
  public final void setConditionalUserPropertyAs(AppMeasurement.ConditionalUserProperty paramConditionalUserProperty)
  {
    Preconditions.checkNotNull(paramConditionalUserProperty);
    Preconditions.checkNotEmpty(paramConditionalUserProperty.mAppId);
    zzga();
    zza(new AppMeasurement.ConditionalUserProperty(paramConditionalUserProperty));
  }
  
  @WorkerThread
  public final void setEventInterceptor(AppMeasurement.EventInterceptor paramEventInterceptor)
  {
    zzaf();
    zzgb();
    zzcl();
    if ((paramEventInterceptor != null) && (paramEventInterceptor != this.zzaqw)) {
      if (this.zzaqw != null) {
        break label46;
      }
    }
    label46:
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "EventInterceptor already set.");
      this.zzaqw = paramEventInterceptor;
      return;
    }
  }
  
  public final void setMeasurementEnabled(boolean paramBoolean)
  {
    zzcl();
    zzgb();
    zzgn().zzc(new zzdi(this, paramBoolean));
  }
  
  public final void setMinimumSessionDuration(long paramLong)
  {
    zzgb();
    zzgn().zzc(new zzdk(this, paramLong));
  }
  
  public final void setSessionTimeoutDuration(long paramLong)
  {
    zzgb();
    zzgn().zzc(new zzdl(this, paramLong));
  }
  
  public final void unregisterOnMeasurementEventListener(AppMeasurement.OnEventListener paramOnEventListener)
  {
    zzgb();
    zzcl();
    Preconditions.checkNotNull(paramOnEventListener);
    if (!this.zzaqx.remove(paramOnEventListener)) {
      zzgo().zzjg().zzbx("OnEventListener had not been registered");
    }
  }
  
  @WorkerThread
  final void zza(String paramString1, String paramString2, long paramLong, Bundle paramBundle)
  {
    zzgb();
    zzaf();
    if ((this.zzaqw == null) || (zzfk.zzcv(paramString2))) {}
    for (boolean bool = true;; bool = false)
    {
      zza(paramString1, paramString2, paramLong, paramBundle, true, bool, false, null);
      return;
    }
  }
  
  @WorkerThread
  final void zza(String paramString1, String paramString2, Bundle paramBundle)
  {
    zzgb();
    zzaf();
    zza(paramString1, paramString2, zzbx().currentTimeMillis(), paramBundle);
  }
  
  public final void zza(String paramString1, String paramString2, Bundle paramBundle, boolean paramBoolean)
  {
    logEvent(paramString1, paramString2, paramBundle, false, true, zzbx().currentTimeMillis());
  }
  
  @WorkerThread
  final void zza(String paramString1, String paramString2, Object paramObject, long paramLong)
  {
    Preconditions.checkNotEmpty(paramString1);
    Preconditions.checkNotEmpty(paramString2);
    zzaf();
    zzgb();
    zzcl();
    long l;
    if (zzgq().zze(zzgf().zzal(), zzaf.zzalj))
    {
      if ((!"_ap".equals(paramString2)) || ("auto".equals(paramString1))) {
        break label235;
      }
      if (((paramObject instanceof String)) && (!TextUtils.isEmpty((String)paramObject))) {
        if (("true".equals(((String)paramObject).toLowerCase(Locale.ENGLISH))) || ("1".equals(paramObject)))
        {
          l = 1L;
          Long localLong = Long.valueOf(l);
          zzbf localzzbf = zzgp().zzans;
          if (((Long)localLong).longValue() != 1L) {
            break label183;
          }
          paramObject = "true";
          label144:
          localzzbf.zzcc((String)paramObject);
          paramObject = localLong;
          label153:
          if (this.zzadj.isEnabled()) {
            break label238;
          }
          zzgo().zzjk().zzbx("User property not set since app measurement is disabled");
        }
      }
    }
    label183:
    label235:
    label238:
    while (!this.zzadj.zzkr())
    {
      do
      {
        return;
        l = 0L;
        break;
        paramObject = "false";
        break label144;
        if (paramObject != null) {
          break label235;
        }
        zzgp().zzans.zzcc("unset");
        zzgn().zzc(new zzcw(this));
        break label153;
      } while ("_ap".equals(paramString2));
      break label153;
    }
    zzgo().zzjk().zze("Setting user property (FE)", zzgl().zzbs(paramString2), paramObject);
    paramString1 = new zzfh(paramString2, paramLong, paramObject, paramString1);
    zzgg().zzb(paramString1);
  }
  
  public final void zza(String paramString1, String paramString2, Object paramObject, boolean paramBoolean, long paramLong)
  {
    int m = 6;
    int j = 0;
    int k = 0;
    if (paramString1 == null) {
      paramString1 = "app";
    }
    for (;;)
    {
      int i;
      if ((paramBoolean) || ("_ap".equals(paramString2)))
      {
        i = zzgm().zzcs(paramString2);
        if (i == 0) {
          break label162;
        }
        zzgm();
        paramString1 = zzfk.zza(paramString2, 24, true);
        j = k;
        if (paramString2 != null) {
          j = paramString2.length();
        }
        this.zzadj.zzgm().zza(i, "_ev", paramString1, j);
      }
      label162:
      do
      {
        return;
        zzfk localzzfk = zzgm();
        i = m;
        if (!localzzfk.zzr("user property", paramString2)) {
          break;
        }
        if (!localzzfk.zza("user property", AppMeasurement.UserProperty.zzado, paramString2))
        {
          i = 15;
          break;
        }
        i = m;
        if (!localzzfk.zza("user property", 24, paramString2)) {
          break;
        }
        i = 0;
        break;
        if (paramObject == null) {
          break label265;
        }
        k = zzgm().zzi(paramString2, paramObject);
        if (k != 0)
        {
          zzgm();
          paramString1 = zzfk.zza(paramString2, 24, true);
          if (!(paramObject instanceof String))
          {
            i = j;
            if (!(paramObject instanceof CharSequence)) {}
          }
          else
          {
            i = String.valueOf(paramObject).length();
          }
          this.zzadj.zzgm().zza(k, "_ev", paramString1, i);
          return;
        }
        paramObject = zzgm().zzj(paramString2, paramObject);
      } while (paramObject == null);
      zza(paramString1, paramString2, paramLong, paramObject);
      return;
      label265:
      zza(paramString1, paramString2, paramLong, null);
      return;
    }
  }
  
  @Nullable
  public final String zzaj(long paramLong)
  {
    Object localObject = null;
    if (zzgn().zzkb()) {
      zzgo().zzjd().zzbx("Cannot retrieve app instance id from analytics worker thread");
    }
    do
    {
      String str;
      do
      {
        return (String)localObject;
        if (zzk.isMainThread())
        {
          zzgo().zzjd().zzbx("Cannot retrieve app instance id from main thread");
          return null;
        }
        paramLong = zzbx().elapsedRealtime();
        str = zzak(120000L);
        paramLong = zzbx().elapsedRealtime() - paramLong;
        localObject = str;
      } while (str != null);
      localObject = str;
    } while (paramLong >= 120000L);
    return zzak(120000L - paramLong);
  }
  
  public final void zzb(String paramString1, String paramString2, Object paramObject, boolean paramBoolean)
  {
    zza(paramString1, paramString2, paramObject, paramBoolean, zzbx().currentTimeMillis());
  }
  
  final void zzcm(@Nullable String paramString)
  {
    this.zzaqz.set(paramString);
  }
  
  public final void zzd(boolean paramBoolean)
  {
    zzcl();
    zzgb();
    zzgn().zzc(new zzdj(this, paramBoolean));
  }
  
  @Nullable
  public final String zzfx()
  {
    zzgb();
    return (String)this.zzaqz.get();
  }
  
  protected final boolean zzgt()
  {
    return false;
  }
  
  public final void zzks()
  {
    if ((getContext().getApplicationContext() instanceof Application)) {
      ((Application)getContext().getApplicationContext()).unregisterActivityLifecycleCallbacks(this.zzaqv);
    }
  }
  
  public final Boolean zzkt()
  {
    AtomicReference localAtomicReference = new AtomicReference();
    return (Boolean)zzgn().zza(localAtomicReference, 15000L, "boolean test flag value", new zzct(this, localAtomicReference));
  }
  
  public final String zzku()
  {
    AtomicReference localAtomicReference = new AtomicReference();
    return (String)zzgn().zza(localAtomicReference, 15000L, "String test flag value", new zzdd(this, localAtomicReference));
  }
  
  public final Long zzkv()
  {
    AtomicReference localAtomicReference = new AtomicReference();
    return (Long)zzgn().zza(localAtomicReference, 15000L, "long test flag value", new zzdf(this, localAtomicReference));
  }
  
  public final Integer zzkw()
  {
    AtomicReference localAtomicReference = new AtomicReference();
    return (Integer)zzgn().zza(localAtomicReference, 15000L, "int test flag value", new zzdg(this, localAtomicReference));
  }
  
  public final Double zzkx()
  {
    AtomicReference localAtomicReference = new AtomicReference();
    return (Double)zzgn().zza(localAtomicReference, 15000L, "double test flag value", new zzdh(this, localAtomicReference));
  }
  
  @WorkerThread
  public final void zzkz()
  {
    zzaf();
    zzgb();
    zzcl();
    if (!this.zzadj.zzkr()) {}
    String str;
    do
    {
      do
      {
        return;
        zzgg().zzkz();
        this.zzara = false;
        str = zzgp().zzjw();
      } while (TextUtils.isEmpty(str));
      zzgk().zzcl();
    } while (str.equals(Build.VERSION.RELEASE));
    Bundle localBundle = new Bundle();
    localBundle.putString("_po", str);
    logEvent("auto", "_ou", localBundle);
  }
  
  public final List<zzfh> zzl(boolean paramBoolean)
  {
    zzgb();
    zzcl();
    zzgo().zzjk().zzbx("Fetching user attributes (FE)");
    if (zzgn().zzkb())
    {
      zzgo().zzjd().zzbx("Cannot get all user properties from analytics worker thread");
      ??? = Collections.emptyList();
    }
    for (;;)
    {
      return (List<zzfh>)???;
      if (zzk.isMainThread())
      {
        zzgo().zzjd().zzbx("Cannot get all user properties from main thread");
        return Collections.emptyList();
      }
      synchronized (new AtomicReference())
      {
        this.zzadj.zzgn().zzc(new zzcx(this, (AtomicReference)???, paramBoolean));
        try
        {
          ???.wait(5000L);
          List localList = (List)((AtomicReference)???).get();
          ??? = localList;
          if (localList != null) {
            continue;
          }
          zzgo().zzjg().zzbx("Timed out waiting for get user properties");
          return Collections.emptyList();
        }
        catch (InterruptedException localInterruptedException)
        {
          for (;;)
          {
            zzgo().zzjg().zzg("Interrupted waiting for get user properties", localInterruptedException);
          }
        }
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzcs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */