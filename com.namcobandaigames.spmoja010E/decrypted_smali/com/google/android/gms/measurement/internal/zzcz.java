package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

final class zzcz
  implements Runnable
{
  zzcz(zzcs paramzzcs, long paramLong) {}
  
  public final void run()
  {
    boolean bool2 = true;
    zzcs localzzcs = this.zzarc;
    long l = this.zzari;
    localzzcs.zzaf();
    localzzcs.zzgb();
    localzzcs.zzcl();
    localzzcs.zzgo().zzjk().zzbx("Resetting analytics data (FE)");
    localzzcs.zzgj().zzlj();
    if (localzzcs.zzgq().zzbe(localzzcs.zzgf().zzal())) {
      localzzcs.zzgp().zzanj.set(l);
    }
    boolean bool3 = localzzcs.zzadj.isEnabled();
    if (!localzzcs.zzgq().zzhu())
    {
      zzba localzzba = localzzcs.zzgp();
      if (!bool3)
      {
        bool1 = true;
        localzzba.zzi(bool1);
      }
    }
    else
    {
      localzzcs.zzgg().resetAnalyticsData();
      if (bool3) {
        break label180;
      }
    }
    label180:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      localzzcs.zzara = bool1;
      if (this.zzarc.zzgq().zza(zzaf.zzalk)) {
        this.zzarc.zzgg().zza(new AtomicReference());
      }
      return;
      bool1 = false;
      break;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\measurement\internal\zzcz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */