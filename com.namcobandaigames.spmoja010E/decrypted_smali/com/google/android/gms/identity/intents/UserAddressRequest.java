package com.google.android.gms.identity.intents;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.identity.intents.model.CountrySpecification;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@SafeParcelable.Class(creator="UserAddressRequestCreator")
@SafeParcelable.Reserved({1})
public final class UserAddressRequest
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<UserAddressRequest> CREATOR = new zzd();
  @SafeParcelable.Field(id=2)
  List<CountrySpecification> zzf;
  
  UserAddressRequest() {}
  
  @SafeParcelable.Constructor
  UserAddressRequest(@SafeParcelable.Param(id=2) List<CountrySpecification> paramList)
  {
    this.zzf = paramList;
  }
  
  public static Builder newBuilder()
  {
    return new Builder(new UserAddressRequest(), null);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeTypedList(paramParcel, 2, this.zzf, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
  
  public final class Builder
  {
    private Builder() {}
    
    public final Builder addAllowedCountrySpecification(CountrySpecification paramCountrySpecification)
    {
      if (UserAddressRequest.this.zzf == null) {
        UserAddressRequest.this.zzf = new ArrayList();
      }
      UserAddressRequest.this.zzf.add(paramCountrySpecification);
      return this;
    }
    
    public final Builder addAllowedCountrySpecifications(Collection<CountrySpecification> paramCollection)
    {
      if (UserAddressRequest.this.zzf == null) {
        UserAddressRequest.this.zzf = new ArrayList();
      }
      UserAddressRequest.this.zzf.addAll(paramCollection);
      return this;
    }
    
    public final UserAddressRequest build()
    {
      if (UserAddressRequest.this.zzf != null) {
        UserAddressRequest.this.zzf = Collections.unmodifiableList(UserAddressRequest.this.zzf);
      }
      return UserAddressRequest.this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\identity\intents\UserAddressRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */