package com.google.android.gms.identity.intents.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="UserAddressCreator")
@SafeParcelable.Reserved({1})
public final class UserAddress
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<UserAddress> CREATOR = new zzb();
  @SafeParcelable.Field(id=2)
  private String name;
  @SafeParcelable.Field(id=10)
  private String zzk;
  @SafeParcelable.Field(id=3)
  private String zzl;
  @SafeParcelable.Field(id=4)
  private String zzm;
  @SafeParcelable.Field(id=5)
  private String zzn;
  @SafeParcelable.Field(id=6)
  private String zzo;
  @SafeParcelable.Field(id=7)
  private String zzp;
  @SafeParcelable.Field(id=8)
  private String zzq;
  @SafeParcelable.Field(id=9)
  private String zzr;
  @SafeParcelable.Field(id=11)
  private String zzs;
  @SafeParcelable.Field(id=12)
  private String zzt;
  @SafeParcelable.Field(id=13)
  private String zzu;
  @SafeParcelable.Field(id=14)
  private boolean zzv;
  @SafeParcelable.Field(id=15)
  private String zzw;
  @SafeParcelable.Field(id=16)
  private String zzx;
  
  UserAddress() {}
  
  @SafeParcelable.Constructor
  UserAddress(@SafeParcelable.Param(id=2) String paramString1, @SafeParcelable.Param(id=3) String paramString2, @SafeParcelable.Param(id=4) String paramString3, @SafeParcelable.Param(id=5) String paramString4, @SafeParcelable.Param(id=6) String paramString5, @SafeParcelable.Param(id=7) String paramString6, @SafeParcelable.Param(id=8) String paramString7, @SafeParcelable.Param(id=9) String paramString8, @SafeParcelable.Param(id=10) String paramString9, @SafeParcelable.Param(id=11) String paramString10, @SafeParcelable.Param(id=12) String paramString11, @SafeParcelable.Param(id=13) String paramString12, @SafeParcelable.Param(id=14) boolean paramBoolean, @SafeParcelable.Param(id=15) String paramString13, @SafeParcelable.Param(id=16) String paramString14)
  {
    this.name = paramString1;
    this.zzl = paramString2;
    this.zzm = paramString3;
    this.zzn = paramString4;
    this.zzo = paramString5;
    this.zzp = paramString6;
    this.zzq = paramString7;
    this.zzr = paramString8;
    this.zzk = paramString9;
    this.zzs = paramString10;
    this.zzt = paramString11;
    this.zzu = paramString12;
    this.zzv = paramBoolean;
    this.zzw = paramString13;
    this.zzx = paramString14;
  }
  
  public static UserAddress fromIntent(Intent paramIntent)
  {
    if ((paramIntent == null) || (!paramIntent.hasExtra("com.google.android.gms.identity.intents.EXTRA_ADDRESS"))) {
      return null;
    }
    return (UserAddress)paramIntent.getParcelableExtra("com.google.android.gms.identity.intents.EXTRA_ADDRESS");
  }
  
  public final String getAddress1()
  {
    return this.zzl;
  }
  
  public final String getAddress2()
  {
    return this.zzm;
  }
  
  public final String getAddress3()
  {
    return this.zzn;
  }
  
  public final String getAddress4()
  {
    return this.zzo;
  }
  
  public final String getAddress5()
  {
    return this.zzp;
  }
  
  public final String getAdministrativeArea()
  {
    return this.zzq;
  }
  
  public final String getCompanyName()
  {
    return this.zzw;
  }
  
  public final String getCountryCode()
  {
    return this.zzk;
  }
  
  public final String getEmailAddress()
  {
    return this.zzx;
  }
  
  public final String getLocality()
  {
    return this.zzr;
  }
  
  public final String getName()
  {
    return this.name;
  }
  
  public final String getPhoneNumber()
  {
    return this.zzu;
  }
  
  public final String getPostalCode()
  {
    return this.zzs;
  }
  
  public final String getSortingCode()
  {
    return this.zzt;
  }
  
  public final boolean isPostBox()
  {
    return this.zzv;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.name, false);
    SafeParcelWriter.writeString(paramParcel, 3, this.zzl, false);
    SafeParcelWriter.writeString(paramParcel, 4, this.zzm, false);
    SafeParcelWriter.writeString(paramParcel, 5, this.zzn, false);
    SafeParcelWriter.writeString(paramParcel, 6, this.zzo, false);
    SafeParcelWriter.writeString(paramParcel, 7, this.zzp, false);
    SafeParcelWriter.writeString(paramParcel, 8, this.zzq, false);
    SafeParcelWriter.writeString(paramParcel, 9, this.zzr, false);
    SafeParcelWriter.writeString(paramParcel, 10, this.zzk, false);
    SafeParcelWriter.writeString(paramParcel, 11, this.zzs, false);
    SafeParcelWriter.writeString(paramParcel, 12, this.zzt, false);
    SafeParcelWriter.writeString(paramParcel, 13, this.zzu, false);
    SafeParcelWriter.writeBoolean(paramParcel, 14, this.zzv);
    SafeParcelWriter.writeString(paramParcel, 15, this.zzw, false);
    SafeParcelWriter.writeString(paramParcel, 16, this.zzx, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\identity\intents\model\UserAddress.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */