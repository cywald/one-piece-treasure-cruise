package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@VisibleForTesting
final class zzam
  extends zzbq
{
  private static final String ID = zza.zzs.toString();
  private static final String zzayz = zzb.zzds.toString();
  private static final String zzban = zzb.zzia.toString();
  private final zzan zzbao;
  
  public zzam(zzan paramzzan)
  {
    super(ID, new String[] { zzban });
    this.zzbao = paramzzan;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    String str = zzgj.zzc((zzp)paramMap.get(zzban));
    HashMap localHashMap = new HashMap();
    paramMap = (zzp)paramMap.get(zzayz);
    if (paramMap != null)
    {
      paramMap = zzgj.zzh(paramMap);
      if (!(paramMap instanceof Map))
      {
        zzdi.zzab("FunctionCallMacro: expected ADDITIONAL_PARAMS to be a map.");
        return zzgj.zzqg();
      }
      paramMap = ((Map)paramMap).entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        localHashMap.put(localEntry.getKey().toString(), localEntry.getValue());
      }
    }
    try
    {
      paramMap = zzgj.zzj(this.zzbao.zza(str, localHashMap));
      return paramMap;
    }
    catch (Exception paramMap)
    {
      paramMap = paramMap.getMessage();
      zzdi.zzab(String.valueOf(str).length() + 34 + String.valueOf(paramMap).length() + "Custom macro/tag " + str + " threw exception " + paramMap);
    }
    return zzgj.zzqg();
  }
  
  public final boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzam.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */