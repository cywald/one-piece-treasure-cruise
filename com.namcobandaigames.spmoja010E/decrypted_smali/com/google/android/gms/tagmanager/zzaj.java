package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

@VisibleForTesting
final class zzaj
  extends zzbq
{
  private static final String ID = zza.zzj.toString();
  private final String version;
  
  public zzaj(String paramString)
  {
    super(ID, new String[0]);
    this.version = paramString;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    if (this.version == null) {
      return zzgj.zzqg();
    }
    return zzgj.zzj(this.version);
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzaj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */