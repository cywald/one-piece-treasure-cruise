package com.google.android.gms.tagmanager;

import android.os.Build.VERSION;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzfk
  extends zzbq
{
  private static final String ID = zza.zzad.toString();
  
  public zzfk()
  {
    super(ID, new String[0]);
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    return zzgj.zzj(Integer.valueOf(Build.VERSION.SDK_INT));
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */