package com.google.android.gms.tagmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
final class zzfn
  extends zzfm
{
  private static final Object zzbfn = new Object();
  private static zzfn zzbfy;
  private boolean connected = true;
  private zzcc zzbdh = new zzfo(this);
  private Context zzbfo;
  private zzcb zzbfp;
  private volatile zzby zzbfq;
  private int zzbfr = 1800000;
  private boolean zzbfs = true;
  private boolean zzbft = false;
  private boolean zzbfu = true;
  private zzfq zzbfv;
  private zzdn zzbfw;
  private boolean zzbfx = false;
  
  private final boolean isPowerSaveMode()
  {
    return (this.zzbfx) || (!this.connected) || (this.zzbfr <= 0);
  }
  
  public static zzfn zzpu()
  {
    if (zzbfy == null) {
      zzbfy = new zzfn();
    }
    return zzbfy;
  }
  
  /* Error */
  public final void dispatch()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 47	com/google/android/gms/tagmanager/zzfn:zzbft	Z
    //   6: ifne +16 -> 22
    //   9: ldc 87
    //   11: invokestatic 93	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   14: aload_0
    //   15: iconst_1
    //   16: putfield 45	com/google/android/gms/tagmanager/zzfn:zzbfs	Z
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: aload_0
    //   23: getfield 95	com/google/android/gms/tagmanager/zzfn:zzbfq	Lcom/google/android/gms/tagmanager/zzby;
    //   26: new 97	com/google/android/gms/tagmanager/zzfp
    //   29: dup
    //   30: aload_0
    //   31: invokespecial 98	com/google/android/gms/tagmanager/zzfp:<init>	(Lcom/google/android/gms/tagmanager/zzfn;)V
    //   34: invokeinterface 104 2 0
    //   39: goto -20 -> 19
    //   42: astore_1
    //   43: aload_0
    //   44: monitorexit
    //   45: aload_1
    //   46: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	47	0	this	zzfn
    //   42	4	1	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	19	42	finally
    //   22	39	42	finally
  }
  
  /* Error */
  final void zza(Context paramContext, zzby paramzzby)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 66	com/google/android/gms/tagmanager/zzfn:zzbfo	Landroid/content/Context;
    //   6: astore_3
    //   7: aload_3
    //   8: ifnull +6 -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: aload_0
    //   15: aload_1
    //   16: invokevirtual 111	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   19: putfield 66	com/google/android/gms/tagmanager/zzfn:zzbfo	Landroid/content/Context;
    //   22: aload_0
    //   23: getfield 95	com/google/android/gms/tagmanager/zzfn:zzbfq	Lcom/google/android/gms/tagmanager/zzby;
    //   26: ifnonnull -15 -> 11
    //   29: aload_0
    //   30: aload_2
    //   31: putfield 95	com/google/android/gms/tagmanager/zzfn:zzbfq	Lcom/google/android/gms/tagmanager/zzby;
    //   34: goto -23 -> 11
    //   37: astore_1
    //   38: aload_0
    //   39: monitorexit
    //   40: aload_1
    //   41: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	42	0	this	zzfn
    //   0	42	1	paramContext	Context
    //   0	42	2	paramzzby	zzby
    //   6	2	3	localContext	Context
    // Exception table:
    //   from	to	target	type
    //   2	7	37	finally
    //   14	34	37	finally
  }
  
  @VisibleForTesting
  final void zza(boolean paramBoolean1, boolean paramBoolean2)
  {
    for (;;)
    {
      try
      {
        boolean bool = isPowerSaveMode();
        this.zzbfx = paramBoolean1;
        this.connected = paramBoolean2;
        paramBoolean1 = isPowerSaveMode();
        if (paramBoolean1 == bool) {
          return;
        }
        if (isPowerSaveMode())
        {
          this.zzbfv.cancel();
          zzdi.v("PowerSaveMode initiated.");
          continue;
        }
        this.zzbfv.zzh(this.zzbfr);
      }
      finally {}
      zzdi.v("PowerSaveMode terminated.");
    }
  }
  
  public final void zzpt()
  {
    try
    {
      if (!isPowerSaveMode()) {
        this.zzbfv.zzpx();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final zzcb zzpv()
  {
    try
    {
      if (this.zzbfp != null) {
        break label50;
      }
      if (this.zzbfo == null) {
        throw new IllegalStateException("Cant get a store unless we have a context");
      }
    }
    finally {}
    this.zzbfp = new zzeb(this.zzbdh, this.zzbfo);
    label50:
    if (this.zzbfv == null)
    {
      this.zzbfv = new zzfr(this, null);
      if (this.zzbfr > 0) {
        this.zzbfv.zzh(this.zzbfr);
      }
    }
    this.zzbft = true;
    if (this.zzbfs)
    {
      dispatch();
      this.zzbfs = false;
    }
    if ((this.zzbfw == null) && (this.zzbfu))
    {
      this.zzbfw = new zzdn(this);
      localObject2 = this.zzbfw;
      Context localContext = this.zzbfo;
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
      localContext.registerReceiver((BroadcastReceiver)localObject2, localIntentFilter);
      localIntentFilter = new IntentFilter();
      localIntentFilter.addAction("com.google.analytics.RADIO_POWERED");
      localIntentFilter.addCategory(localContext.getPackageName());
      localContext.registerReceiver((BroadcastReceiver)localObject2, localIntentFilter);
    }
    Object localObject2 = this.zzbfp;
    return (zzcb)localObject2;
  }
  
  public final void zzr(boolean paramBoolean)
  {
    try
    {
      zza(this.zzbfx, paramBoolean);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */