package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Process;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zza
{
  private static Object zzayt = new Object();
  private static zza zzayu;
  private volatile boolean closed = false;
  private volatile long zzaym = 900000L;
  private volatile long zzayn = 30000L;
  private volatile long zzayo;
  private volatile long zzayp;
  private final Thread zzayq;
  private final Object zzayr = new Object();
  private zzd zzays = new zzb(this);
  private final Context zzri;
  private final Clock zzrz;
  private volatile AdvertisingIdClient.Info zzvl;
  
  private zza(Context paramContext)
  {
    this(paramContext, null, DefaultClock.getInstance());
  }
  
  @VisibleForTesting
  private zza(Context paramContext, zzd paramzzd, Clock paramClock)
  {
    this.zzrz = paramClock;
    if (paramContext != null) {}
    for (this.zzri = paramContext.getApplicationContext();; this.zzri = paramContext)
    {
      this.zzayo = this.zzrz.currentTimeMillis();
      this.zzayq = new Thread(new zzc(this));
      return;
    }
  }
  
  /* Error */
  private final void zzmw()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 55	com/google/android/gms/tagmanager/zza:closed	Z
    //   6: ifne +14 -> 20
    //   9: aload_0
    //   10: invokespecial 105	com/google/android/gms/tagmanager/zza:zzmx	()V
    //   13: aload_0
    //   14: ldc2_w 106
    //   17: invokevirtual 111	java/lang/Object:wait	(J)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    //   28: astore_1
    //   29: goto -9 -> 20
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	32	0	this	zza
    //   23	4	1	localObject	Object
    //   28	1	1	localInterruptedException	InterruptedException
    // Exception table:
    //   from	to	target	type
    //   2	20	23	finally
    //   20	22	23	finally
    //   24	26	23	finally
    //   2	20	28	java/lang/InterruptedException
  }
  
  private final void zzmx()
  {
    if (this.zzrz.currentTimeMillis() - this.zzayo > this.zzayn) {}
    synchronized (this.zzayr)
    {
      this.zzayr.notify();
      this.zzayo = this.zzrz.currentTimeMillis();
      return;
    }
  }
  
  private final void zzmy()
  {
    if (this.zzrz.currentTimeMillis() - this.zzayp > 3600000L) {
      this.zzvl = null;
    }
  }
  
  private final void zzmz()
  {
    Process.setThreadPriority(10);
    while (!this.closed)
    {
      ??? = this.zzays.zzna();
      if (??? != null)
      {
        this.zzvl = ((AdvertisingIdClient.Info)???);
        this.zzayp = this.zzrz.currentTimeMillis();
        zzdi.zzdi("Obtained fresh AdvertisingId info from GmsCore.");
      }
      try
      {
        notifyAll();
        try
        {
          synchronized (this.zzayr)
          {
            this.zzayr.wait(this.zzaym);
          }
        }
        catch (InterruptedException localInterruptedException)
        {
          zzdi.zzdi("sleep interrupted in AdvertiserDataPoller thread; continuing");
        }
        return;
      }
      finally {}
    }
  }
  
  public static zza zzn(Context paramContext)
  {
    if (zzayu == null) {}
    synchronized (zzayt)
    {
      if (zzayu == null)
      {
        paramContext = new zza(paramContext);
        zzayu = paramContext;
        paramContext.zzayq.start();
      }
      return zzayu;
    }
  }
  
  @VisibleForTesting
  public final void close()
  {
    this.closed = true;
    this.zzayq.interrupt();
  }
  
  public final boolean isLimitAdTrackingEnabled()
  {
    if (this.zzvl == null) {
      zzmw();
    }
    for (;;)
    {
      zzmy();
      if (this.zzvl != null) {
        break;
      }
      return true;
      zzmx();
    }
    return this.zzvl.isLimitAdTrackingEnabled();
  }
  
  public final String zzmv()
  {
    if (this.zzvl == null) {
      zzmw();
    }
    for (;;)
    {
      zzmy();
      if (this.zzvl != null) {
        break;
      }
      return null;
      zzmx();
    }
    return this.zzvl.getId();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */