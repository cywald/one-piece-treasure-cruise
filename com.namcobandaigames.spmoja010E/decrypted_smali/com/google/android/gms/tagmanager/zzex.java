package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzl;
import com.google.android.gms.internal.measurement.zzrq;
import com.google.android.gms.internal.measurement.zzrs;
import com.google.android.gms.internal.measurement.zzrw;
import com.google.android.gms.internal.measurement.zzsa;
import com.google.android.gms.internal.measurement.zzzf;
import com.google.android.gms.internal.measurement.zzzg;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;

final class zzex
  implements zzah
{
  private final ExecutorService zzaea;
  private final String zzazf;
  private zzdh<zzrq> zzbeg;
  private final Context zzri;
  
  zzex(Context paramContext, String paramString)
  {
    this.zzri = paramContext;
    this.zzazf = paramString;
    this.zzaea = Executors.newSingleThreadExecutor();
  }
  
  private static zzrw zza(ByteArrayOutputStream paramByteArrayOutputStream)
  {
    try
    {
      paramByteArrayOutputStream = zzda.zzdr(paramByteArrayOutputStream.toString("UTF-8"));
      return paramByteArrayOutputStream;
    }
    catch (UnsupportedEncodingException paramByteArrayOutputStream)
    {
      zzdi.zzdj("Failed to convert binary resource to string for JSON parsing; the file format is not UTF-8 format.");
      return null;
    }
    catch (JSONException paramByteArrayOutputStream)
    {
      zzdi.zzab("Failed to extract the container from the resource file. Resource is a UTF-8 encoded string but doesn't contain a JSON container");
    }
    return null;
  }
  
  private static zzrw zze(byte[] paramArrayOfByte)
  {
    try
    {
      paramArrayOfByte = zzrs.zza((zzl)zzzg.zza(new zzl(), paramArrayOfByte));
      if (paramArrayOfByte != null) {
        zzdi.v("The container was successfully loaded from the resource (using binary file)");
      }
      return paramArrayOfByte;
    }
    catch (zzzf paramArrayOfByte)
    {
      zzdi.e("The resource file is corrupted. The container cannot be extracted from the binary file");
      return null;
    }
    catch (zzsa paramArrayOfByte)
    {
      zzdi.zzab("The resource file is invalid. The container from the binary file is invalid");
    }
    return null;
  }
  
  @VisibleForTesting
  private final File zzpi()
  {
    String str1 = String.valueOf("resource_");
    String str2 = String.valueOf(this.zzazf);
    if (str2.length() != 0) {}
    for (str1 = str1.concat(str2);; str1 = new String(str1)) {
      return new File(this.zzri.getDir("google_tagmanager", 0), str1);
    }
  }
  
  public final void release()
  {
    try
    {
      this.zzaea.shutdown();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void zza(zzrq paramzzrq)
  {
    this.zzaea.execute(new zzez(this, paramzzrq));
  }
  
  public final void zza(zzdh<zzrq> paramzzdh)
  {
    this.zzbeg = paramzzdh;
  }
  
  /* Error */
  @VisibleForTesting
  final boolean zzb(zzrq paramzzrq)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 160	com/google/android/gms/tagmanager/zzex:zzpi	()Ljava/io/File;
    //   4: astore_3
    //   5: new 162	java/io/FileOutputStream
    //   8: dup
    //   9: aload_3
    //   10: invokespecial 165	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   13: astore_2
    //   14: aload_1
    //   15: invokevirtual 168	com/google/android/gms/internal/measurement/zzzg:zzvu	()I
    //   18: newarray <illegal type>
    //   20: astore 4
    //   22: aload_1
    //   23: aload 4
    //   25: iconst_0
    //   26: aload 4
    //   28: arraylength
    //   29: invokestatic 171	com/google/android/gms/internal/measurement/zzzg:zza	(Lcom/google/android/gms/internal/measurement/zzzg;[BII)V
    //   32: aload_2
    //   33: aload 4
    //   35: invokevirtual 175	java/io/FileOutputStream:write	([B)V
    //   38: aload_2
    //   39: invokevirtual 178	java/io/FileOutputStream:close	()V
    //   42: iconst_1
    //   43: ireturn
    //   44: astore_1
    //   45: ldc -76
    //   47: invokestatic 95	com/google/android/gms/tagmanager/zzdi:e	(Ljava/lang/String;)V
    //   50: iconst_0
    //   51: ireturn
    //   52: astore_1
    //   53: ldc -74
    //   55: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   58: goto -16 -> 42
    //   61: astore_1
    //   62: ldc -72
    //   64: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   67: aload_3
    //   68: invokevirtual 188	java/io/File:delete	()Z
    //   71: pop
    //   72: aload_2
    //   73: invokevirtual 178	java/io/FileOutputStream:close	()V
    //   76: iconst_0
    //   77: ireturn
    //   78: astore_1
    //   79: ldc -74
    //   81: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   84: iconst_0
    //   85: ireturn
    //   86: astore_1
    //   87: aload_2
    //   88: invokevirtual 178	java/io/FileOutputStream:close	()V
    //   91: aload_1
    //   92: athrow
    //   93: astore_2
    //   94: ldc -74
    //   96: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   99: goto -8 -> 91
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	102	0	this	zzex
    //   0	102	1	paramzzrq	zzrq
    //   13	75	2	localFileOutputStream	java.io.FileOutputStream
    //   93	1	2	localIOException	java.io.IOException
    //   4	64	3	localFile	File
    //   20	14	4	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   5	14	44	java/io/FileNotFoundException
    //   38	42	52	java/io/IOException
    //   14	38	61	java/io/IOException
    //   72	76	78	java/io/IOException
    //   14	38	86	finally
    //   62	72	86	finally
    //   87	91	93	java/io/IOException
  }
  
  public final void zznp()
  {
    this.zzaea.execute(new zzey(this));
  }
  
  /* Error */
  @VisibleForTesting
  final void zzph()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 150	com/google/android/gms/tagmanager/zzex:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   4: ifnonnull +13 -> 17
    //   7: new 199	java/lang/IllegalStateException
    //   10: dup
    //   11: ldc -55
    //   13: invokespecial 202	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   16: athrow
    //   17: aload_0
    //   18: getfield 150	com/google/android/gms/tagmanager/zzex:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   21: invokeinterface 207 1 0
    //   26: ldc -47
    //   28: invokestatic 90	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   31: invokestatic 215	com/google/android/gms/tagmanager/zzeh:zzpc	()Lcom/google/android/gms/tagmanager/zzeh;
    //   34: invokevirtual 219	com/google/android/gms/tagmanager/zzeh:zzpd	()Lcom/google/android/gms/tagmanager/zzeh$zza;
    //   37: getstatic 225	com/google/android/gms/tagmanager/zzeh$zza:zzbdv	Lcom/google/android/gms/tagmanager/zzeh$zza;
    //   40: if_acmpeq +15 -> 55
    //   43: invokestatic 215	com/google/android/gms/tagmanager/zzeh:zzpc	()Lcom/google/android/gms/tagmanager/zzeh;
    //   46: invokevirtual 219	com/google/android/gms/tagmanager/zzeh:zzpd	()Lcom/google/android/gms/tagmanager/zzeh$zza;
    //   49: getstatic 228	com/google/android/gms/tagmanager/zzeh$zza:zzbdw	Lcom/google/android/gms/tagmanager/zzeh$zza;
    //   52: if_acmpne +32 -> 84
    //   55: aload_0
    //   56: getfield 24	com/google/android/gms/tagmanager/zzex:zzazf	Ljava/lang/String;
    //   59: invokestatic 215	com/google/android/gms/tagmanager/zzeh:zzpc	()Lcom/google/android/gms/tagmanager/zzeh;
    //   62: invokevirtual 232	com/google/android/gms/tagmanager/zzeh:getContainerId	()Ljava/lang/String;
    //   65: invokevirtual 236	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   68: ifeq +16 -> 84
    //   71: aload_0
    //   72: getfield 150	com/google/android/gms/tagmanager/zzex:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   75: getstatic 242	com/google/android/gms/tagmanager/zzcz:zzbcu	I
    //   78: invokeinterface 246 2 0
    //   83: return
    //   84: new 248	java/io/FileInputStream
    //   87: dup
    //   88: aload_0
    //   89: invokespecial 160	com/google/android/gms/tagmanager/zzex:zzpi	()Ljava/io/File;
    //   92: invokespecial 249	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   95: astore_1
    //   96: new 43	java/io/ByteArrayOutputStream
    //   99: dup
    //   100: invokespecial 250	java/io/ByteArrayOutputStream:<init>	()V
    //   103: astore_2
    //   104: aload_1
    //   105: aload_2
    //   106: invokestatic 253	com/google/android/gms/internal/measurement/zzrs:zza	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   109: aload_2
    //   110: invokevirtual 257	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   113: astore_2
    //   114: new 259	com/google/android/gms/internal/measurement/zzrq
    //   117: dup
    //   118: invokespecial 260	com/google/android/gms/internal/measurement/zzrq:<init>	()V
    //   121: aload_2
    //   122: invokestatic 80	com/google/android/gms/internal/measurement/zzzg:zza	(Lcom/google/android/gms/internal/measurement/zzzg;[B)Lcom/google/android/gms/internal/measurement/zzzg;
    //   125: checkcast 259	com/google/android/gms/internal/measurement/zzrq
    //   128: astore_2
    //   129: aload_2
    //   130: getfield 264	com/google/android/gms/internal/measurement/zzrq:zzqg	Lcom/google/android/gms/internal/measurement/zzl;
    //   133: ifnonnull +71 -> 204
    //   136: aload_2
    //   137: getfield 268	com/google/android/gms/internal/measurement/zzrq:zzbpt	Lcom/google/android/gms/internal/measurement/zzo;
    //   140: ifnonnull +64 -> 204
    //   143: new 197	java/lang/IllegalArgumentException
    //   146: dup
    //   147: ldc_w 270
    //   150: invokespecial 271	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   153: athrow
    //   154: astore_2
    //   155: aload_0
    //   156: getfield 150	com/google/android/gms/tagmanager/zzex:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   159: getstatic 274	com/google/android/gms/tagmanager/zzcz:zzbcv	I
    //   162: invokeinterface 246 2 0
    //   167: ldc_w 276
    //   170: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   173: aload_1
    //   174: invokevirtual 277	java/io/FileInputStream:close	()V
    //   177: ldc_w 279
    //   180: invokestatic 90	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   183: return
    //   184: astore_1
    //   185: ldc_w 281
    //   188: invokestatic 61	com/google/android/gms/tagmanager/zzdi:zzdj	(Ljava/lang/String;)V
    //   191: aload_0
    //   192: getfield 150	com/google/android/gms/tagmanager/zzex:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   195: getstatic 242	com/google/android/gms/tagmanager/zzcz:zzbcu	I
    //   198: invokeinterface 246 2 0
    //   203: return
    //   204: aload_0
    //   205: getfield 150	com/google/android/gms/tagmanager/zzex:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   208: aload_2
    //   209: invokeinterface 285 2 0
    //   214: aload_1
    //   215: invokevirtual 277	java/io/FileInputStream:close	()V
    //   218: goto -41 -> 177
    //   221: astore_1
    //   222: ldc_w 287
    //   225: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   228: goto -51 -> 177
    //   231: astore_1
    //   232: ldc_w 287
    //   235: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   238: goto -61 -> 177
    //   241: astore_2
    //   242: aload_0
    //   243: getfield 150	com/google/android/gms/tagmanager/zzex:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   246: getstatic 274	com/google/android/gms/tagmanager/zzcz:zzbcv	I
    //   249: invokeinterface 246 2 0
    //   254: ldc_w 289
    //   257: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   260: aload_1
    //   261: invokevirtual 277	java/io/FileInputStream:close	()V
    //   264: goto -87 -> 177
    //   267: astore_1
    //   268: ldc_w 287
    //   271: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   274: goto -97 -> 177
    //   277: astore_2
    //   278: aload_1
    //   279: invokevirtual 277	java/io/FileInputStream:close	()V
    //   282: aload_2
    //   283: athrow
    //   284: astore_1
    //   285: ldc_w 287
    //   288: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   291: goto -9 -> 282
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	294	0	this	zzex
    //   95	79	1	localFileInputStream	java.io.FileInputStream
    //   184	31	1	localFileNotFoundException	java.io.FileNotFoundException
    //   221	1	1	localIOException1	java.io.IOException
    //   231	30	1	localIOException2	java.io.IOException
    //   267	12	1	localIOException3	java.io.IOException
    //   284	1	1	localIOException4	java.io.IOException
    //   103	34	2	localObject1	Object
    //   154	55	2	localIOException5	java.io.IOException
    //   241	1	2	localIllegalArgumentException	IllegalArgumentException
    //   277	6	2	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   96	154	154	java/io/IOException
    //   204	214	154	java/io/IOException
    //   84	96	184	java/io/FileNotFoundException
    //   214	218	221	java/io/IOException
    //   173	177	231	java/io/IOException
    //   96	154	241	java/lang/IllegalArgumentException
    //   204	214	241	java/lang/IllegalArgumentException
    //   260	264	267	java/io/IOException
    //   96	154	277	finally
    //   155	173	277	finally
    //   204	214	277	finally
    //   242	260	277	finally
    //   278	282	284	java/io/IOException
  }
  
  /* Error */
  public final zzrw zzu(int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 22	com/google/android/gms/tagmanager/zzex:zzri	Landroid/content/Context;
    //   4: invokevirtual 297	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   7: iload_1
    //   8: invokevirtual 303	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
    //   11: astore_2
    //   12: aload_0
    //   13: getfield 22	com/google/android/gms/tagmanager/zzex:zzri	Landroid/content/Context;
    //   16: invokevirtual 297	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   19: iload_1
    //   20: invokevirtual 307	android/content/res/Resources:getResourceName	(I)Ljava/lang/String;
    //   23: astore_3
    //   24: new 309	java/lang/StringBuilder
    //   27: dup
    //   28: aload_3
    //   29: invokestatic 108	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   32: invokevirtual 112	java/lang/String:length	()I
    //   35: bipush 66
    //   37: iadd
    //   38: invokespecial 311	java/lang/StringBuilder:<init>	(I)V
    //   41: ldc_w 313
    //   44: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: iload_1
    //   48: invokevirtual 320	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   51: ldc_w 322
    //   54: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: aload_3
    //   58: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: ldc_w 324
    //   64: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: invokevirtual 326	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   70: invokestatic 90	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   73: new 43	java/io/ByteArrayOutputStream
    //   76: dup
    //   77: invokespecial 250	java/io/ByteArrayOutputStream:<init>	()V
    //   80: astore_3
    //   81: aload_2
    //   82: aload_3
    //   83: invokestatic 253	com/google/android/gms/internal/measurement/zzrs:zza	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   86: aload_3
    //   87: invokestatic 328	com/google/android/gms/tagmanager/zzex:zza	(Ljava/io/ByteArrayOutputStream;)Lcom/google/android/gms/internal/measurement/zzrw;
    //   90: astore_2
    //   91: aload_2
    //   92: ifnull +39 -> 131
    //   95: ldc_w 330
    //   98: invokestatic 90	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   101: aload_2
    //   102: areturn
    //   103: astore_2
    //   104: new 309	java/lang/StringBuilder
    //   107: dup
    //   108: bipush 98
    //   110: invokespecial 311	java/lang/StringBuilder:<init>	(I)V
    //   113: ldc_w 332
    //   116: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   119: iload_1
    //   120: invokevirtual 320	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   123: invokevirtual 326	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   126: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   129: aconst_null
    //   130: areturn
    //   131: aload_3
    //   132: invokevirtual 257	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   135: invokestatic 334	com/google/android/gms/tagmanager/zzex:zze	([B)Lcom/google/android/gms/internal/measurement/zzrw;
    //   138: astore_2
    //   139: aload_2
    //   140: areturn
    //   141: astore_2
    //   142: aload_0
    //   143: getfield 22	com/google/android/gms/tagmanager/zzex:zzri	Landroid/content/Context;
    //   146: invokevirtual 297	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   149: iload_1
    //   150: invokevirtual 307	android/content/res/Resources:getResourceName	(I)Ljava/lang/String;
    //   153: astore_2
    //   154: new 309	java/lang/StringBuilder
    //   157: dup
    //   158: aload_2
    //   159: invokestatic 108	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   162: invokevirtual 112	java/lang/String:length	()I
    //   165: bipush 67
    //   167: iadd
    //   168: invokespecial 311	java/lang/StringBuilder:<init>	(I)V
    //   171: ldc_w 336
    //   174: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   177: iload_1
    //   178: invokevirtual 320	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   181: ldc_w 322
    //   184: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   187: aload_2
    //   188: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   191: ldc_w 324
    //   194: invokevirtual 317	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   197: invokevirtual 326	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   200: invokestatic 66	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   203: aconst_null
    //   204: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	205	0	this	zzex
    //   0	205	1	paramInt	int
    //   11	91	2	localObject1	Object
    //   103	1	2	localNotFoundException	android.content.res.Resources.NotFoundException
    //   138	2	2	localzzrw	zzrw
    //   141	1	2	localIOException	java.io.IOException
    //   153	35	2	str	String
    //   23	109	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   0	12	103	android/content/res/Resources$NotFoundException
    //   73	91	141	java/io/IOException
    //   95	101	141	java/io/IOException
    //   131	139	141	java/io/IOException
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzex.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */