package com.google.android.gms.tagmanager;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class zzx
  extends Handler
{
  private final ContainerHolder.ContainerAvailableListener zzazu;
  
  public zzx(zzv paramzzv, ContainerHolder.ContainerAvailableListener paramContainerAvailableListener, Looper paramLooper)
  {
    super(paramLooper);
    this.zzazu = paramContainerAvailableListener;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default: 
      zzdi.e("Don't know how to handle this message.");
      return;
    }
    paramMessage = (String)paramMessage.obj;
    this.zzazu.onContainerAvailable(this.zzazv, paramMessage);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */