package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@VisibleForTesting
public class DataLayer
{
  public static final String EVENT_KEY = "event";
  public static final Object OBJECT_NOT_PRESENT = new Object();
  private static final String[] zzbap = "gtm.lifetime".toString().split("\\.");
  private static final Pattern zzbaq = Pattern.compile("(\\d+)\\s*([smhd]?)");
  private final ConcurrentHashMap<zzb, Integer> zzbar;
  private final Map<String, Object> zzbas;
  private final ReentrantLock zzbat;
  private final LinkedList<Map<String, Object>> zzbau;
  private final zzc zzbav;
  private final CountDownLatch zzbaw;
  
  @VisibleForTesting
  DataLayer()
  {
    this(new zzao());
  }
  
  DataLayer(zzc paramzzc)
  {
    this.zzbav = paramzzc;
    this.zzbar = new ConcurrentHashMap();
    this.zzbas = new HashMap();
    this.zzbat = new ReentrantLock();
    this.zzbau = new LinkedList();
    this.zzbaw = new CountDownLatch(1);
    this.zzbav.zza(new zzap(this));
  }
  
  @VisibleForTesting
  public static List<Object> listOf(Object... paramVarArgs)
  {
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    while (i < paramVarArgs.length)
    {
      localArrayList.add(paramVarArgs[i]);
      i += 1;
    }
    return localArrayList;
  }
  
  @VisibleForTesting
  public static Map<String, Object> mapOf(Object... paramVarArgs)
  {
    if (paramVarArgs.length % 2 != 0) {
      throw new IllegalArgumentException("expected even number of key-value pairs");
    }
    HashMap localHashMap = new HashMap();
    int i = 0;
    while (i < paramVarArgs.length)
    {
      if (!(paramVarArgs[i] instanceof String))
      {
        paramVarArgs = String.valueOf(paramVarArgs[i]);
        throw new IllegalArgumentException(String.valueOf(paramVarArgs).length() + 21 + "key is not a string: " + paramVarArgs);
      }
      localHashMap.put((String)paramVarArgs[i], paramVarArgs[(i + 1)]);
      i += 2;
    }
    return localHashMap;
  }
  
  @VisibleForTesting
  private final void zza(List<Object> paramList1, List<Object> paramList2)
  {
    while (paramList2.size() < paramList1.size()) {
      paramList2.add(null);
    }
    int i = 0;
    if (i < paramList1.size())
    {
      Object localObject = paramList1.get(i);
      if ((localObject instanceof List))
      {
        if (!(paramList2.get(i) instanceof List)) {
          paramList2.set(i, new ArrayList());
        }
        zza((List)localObject, (List)paramList2.get(i));
      }
      for (;;)
      {
        i += 1;
        break;
        if ((localObject instanceof Map))
        {
          if (!(paramList2.get(i) instanceof Map)) {
            paramList2.set(i, new HashMap());
          }
          zzb((Map)localObject, (Map)paramList2.get(i));
        }
        else if (localObject != OBJECT_NOT_PRESENT)
        {
          paramList2.set(i, localObject);
        }
      }
    }
  }
  
  private final void zza(Map<String, Object> paramMap, String paramString, Collection<zza> paramCollection)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (paramString.length() == 0) {}
      for (paramMap = "";; paramMap = ".")
      {
        String str = (String)localEntry.getKey();
        paramMap = String.valueOf(paramString).length() + String.valueOf(paramMap).length() + String.valueOf(str).length() + paramString + paramMap + str;
        if (!(localEntry.getValue() instanceof Map)) {
          break label143;
        }
        zza((Map)localEntry.getValue(), paramMap, paramCollection);
        break;
      }
      label143:
      if (!paramMap.equals("gtm.lifetime")) {
        paramCollection.add(new zza(paramMap, localEntry.getValue()));
      }
    }
  }
  
  @VisibleForTesting
  private final void zzb(Map<String, Object> paramMap1, Map<String, Object> paramMap2)
  {
    Iterator localIterator = paramMap1.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Object localObject = paramMap1.get(str);
      if ((localObject instanceof List))
      {
        if (!(paramMap2.get(str) instanceof List)) {
          paramMap2.put(str, new ArrayList());
        }
        zza((List)localObject, (List)paramMap2.get(str));
      }
      else if ((localObject instanceof Map))
      {
        if (!(paramMap2.get(str) instanceof Map)) {
          paramMap2.put(str, new HashMap());
        }
        zzb((Map)localObject, (Map)paramMap2.get(str));
      }
      else
      {
        paramMap2.put(str, localObject);
      }
    }
  }
  
  @VisibleForTesting
  private static Long zzde(String paramString)
  {
    Matcher localMatcher = zzbaq.matcher(paramString);
    if (!localMatcher.matches())
    {
      paramString = String.valueOf(paramString);
      if (paramString.length() != 0) {}
      for (paramString = "unknown _lifetime: ".concat(paramString);; paramString = new String("unknown _lifetime: "))
      {
        zzdi.zzdi(paramString);
        return null;
      }
    }
    long l;
    try
    {
      l = Long.parseLong(localMatcher.group(1));
      if (l <= 0L)
      {
        paramString = String.valueOf(paramString);
        if (paramString.length() != 0)
        {
          paramString = "non-positive _lifetime: ".concat(paramString);
          zzdi.zzdi(paramString);
          return null;
        }
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;)
      {
        str = String.valueOf(paramString);
        if (str.length() != 0) {}
        for (str = "illegal number in _lifetime value: ".concat(str);; str = new String("illegal number in _lifetime value: "))
        {
          zzdi.zzab(str);
          l = 0L;
          break;
        }
        paramString = new String("non-positive _lifetime: ");
      }
      String str = localMatcher.group(2);
      if (str.length() == 0) {
        return Long.valueOf(l);
      }
      switch (str.charAt(0))
      {
      default: 
        paramString = String.valueOf(paramString);
        if (paramString.length() == 0) {}
        break;
      }
    }
    for (paramString = "unknown units in _lifetime: ".concat(paramString);; paramString = new String("unknown units in _lifetime: "))
    {
      zzdi.zzab(paramString);
      return null;
      return Long.valueOf(l * 1000L);
      return Long.valueOf(l * 1000L * 60L);
      return Long.valueOf(l * 1000L * 60L * 60L);
      return Long.valueOf(l * 1000L * 60L * 60L * 24L);
    }
  }
  
  private final void zzh(Map<String, Object> paramMap)
  {
    this.zzbat.lock();
    int i;
    Object localObject1;
    Object localObject3;
    try
    {
      this.zzbau.offer(paramMap);
      if (this.zzbat.getHoldCount() != 1) {
        break label208;
      }
      i = 0;
      localObject1 = (Map)this.zzbau.poll();
      if (localObject1 == null) {
        break label208;
      }
      synchronized (this.zzbas)
      {
        localObject3 = ((Map)localObject1).keySet().iterator();
        if (((Iterator)localObject3).hasNext())
        {
          String str = (String)((Iterator)localObject3).next();
          zzb(zzk(str, ((Map)localObject1).get(str)), this.zzbas);
        }
      }
    }
    finally
    {
      this.zzbat.unlock();
    }
    ??? = this.zzbar.keySet().iterator();
    while (((Iterator)???).hasNext()) {
      ((zzb)((Iterator)???).next()).zzf((Map)localObject1);
    }
    i += 1;
    if (i > 500)
    {
      this.zzbau.clear();
      throw new RuntimeException("Seems like an infinite loop of pushing to the data layer");
      label208:
      localObject3 = zzbap;
      int j = localObject3.length;
      i = 0;
      localObject1 = paramMap;
      label222:
      ??? = localObject1;
      if (i >= j) {
        break label335;
      }
      ??? = localObject3[i];
      if (!(localObject1 instanceof Map))
      {
        ??? = null;
        break label335;
      }
    }
    for (;;)
    {
      label251:
      if (localObject1 != null)
      {
        ??? = new ArrayList();
        zza(paramMap, "", (Collection)???);
        this.zzbav.zza((List)???, ((Long)localObject1).longValue());
      }
      this.zzbat.unlock();
      return;
      localObject1 = ((Map)localObject1).get(???);
      i += 1;
      break label222;
      label335:
      do
      {
        localObject1 = zzde(???.toString());
        break label251;
        break;
      } while (??? != null);
      localObject1 = null;
    }
  }
  
  static Map<String, Object> zzk(String paramString, Object paramObject)
  {
    HashMap localHashMap1 = new HashMap();
    String[] arrayOfString = paramString.toString().split("\\.");
    int i = 0;
    HashMap localHashMap2;
    for (paramString = localHashMap1; i < arrayOfString.length - 1; paramString = localHashMap2)
    {
      localHashMap2 = new HashMap();
      paramString.put(arrayOfString[i], localHashMap2);
      i += 1;
    }
    paramString.put(arrayOfString[(arrayOfString.length - 1)], paramObject);
    return localHashMap1;
  }
  
  public Object get(String paramString)
  {
    for (;;)
    {
      int i;
      synchronized (this.zzbas)
      {
        Map localMap1 = this.zzbas;
        String[] arrayOfString = paramString.split("\\.");
        int j = arrayOfString.length;
        paramString = localMap1;
        i = 0;
        if (i < j)
        {
          localMap1 = arrayOfString[i];
          if (!(paramString instanceof Map)) {
            return null;
          }
          paramString = ((Map)paramString).get(localMap1);
          if (paramString == null) {
            return null;
          }
        }
        else
        {
          return paramString;
        }
      }
      i += 1;
    }
  }
  
  public void push(String paramString, Object paramObject)
  {
    push(zzk(paramString, paramObject));
  }
  
  public void push(Map<String, Object> paramMap)
  {
    try
    {
      this.zzbaw.await();
      zzh(paramMap);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        zzdi.zzab("DataLayer.push: unexpected InterruptedException");
      }
    }
  }
  
  public void pushEvent(String paramString, Map<String, Object> paramMap)
  {
    paramMap = new HashMap(paramMap);
    paramMap.put("event", paramString);
    push(paramMap);
  }
  
  public String toString()
  {
    synchronized (this.zzbas)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      Iterator localIterator = this.zzbas.entrySet().iterator();
      if (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        localStringBuilder.append(String.format("{\n\tKey: %s\n\tValue: %s\n}\n", new Object[] { localEntry.getKey(), localEntry.getValue() }));
      }
    }
    String str = ((StringBuilder)localObject).toString();
    return str;
  }
  
  final void zza(zzb paramzzb)
  {
    this.zzbar.put(paramzzb, Integer.valueOf(0));
  }
  
  final void zzdd(String paramString)
  {
    push(paramString, null);
    this.zzbav.zzdf(paramString);
  }
  
  static final class zza
  {
    public final String mKey;
    public final Object mValue;
    
    zza(String paramString, Object paramObject)
    {
      this.mKey = paramString;
      this.mValue = paramObject;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof zza)) {}
      do
      {
        return false;
        paramObject = (zza)paramObject;
      } while ((!this.mKey.equals(((zza)paramObject).mKey)) || (!this.mValue.equals(((zza)paramObject).mValue)));
      return true;
    }
    
    public final int hashCode()
    {
      return Arrays.hashCode(new Integer[] { Integer.valueOf(this.mKey.hashCode()), Integer.valueOf(this.mValue.hashCode()) });
    }
    
    public final String toString()
    {
      String str1 = this.mKey;
      String str2 = this.mValue.toString();
      return String.valueOf(str1).length() + 13 + String.valueOf(str2).length() + "Key: " + str1 + " value: " + str2;
    }
  }
  
  static abstract interface zzb
  {
    public abstract void zzf(Map<String, Object> paramMap);
  }
  
  static abstract interface zzc
  {
    public abstract void zza(zzaq paramzzaq);
    
    public abstract void zza(List<DataLayer.zza> paramList, long paramLong);
    
    public abstract void zzdf(String paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\DataLayer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */