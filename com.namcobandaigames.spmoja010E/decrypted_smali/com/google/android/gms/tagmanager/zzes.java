package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzo;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class zzes
  implements zzag
{
  private boolean closed;
  private final String zzazf;
  private String zzbaf;
  private zzdh<zzo> zzbeg;
  private zzal zzbeh;
  private final ScheduledExecutorService zzbej;
  private final zzev zzbek;
  private ScheduledFuture<?> zzbel;
  private final Context zzri;
  
  public zzes(Context paramContext, String paramString, zzal paramzzal)
  {
    this(paramContext, paramString, paramzzal, null, null);
  }
  
  @VisibleForTesting
  private zzes(Context paramContext, String paramString, zzal paramzzal, zzew paramzzew, zzev paramzzev)
  {
    this.zzbeh = paramzzal;
    this.zzri = paramContext;
    this.zzazf = paramString;
    this.zzbej = new zzet(this).zzpg();
    this.zzbek = new zzeu(this);
  }
  
  private final void zzpf()
  {
    try
    {
      if (this.closed) {
        throw new IllegalStateException("called method after closed");
      }
    }
    finally {}
  }
  
  public final void release()
  {
    try
    {
      zzpf();
      if (this.zzbel != null) {
        this.zzbel.cancel(false);
      }
      this.zzbej.shutdown();
      this.closed = true;
      return;
    }
    finally {}
  }
  
  public final void zza(long paramLong, String paramString)
  {
    try
    {
      localObject = this.zzazf;
      zzdi.v(String.valueOf(localObject).length() + 55 + "loadAfterDelay: containerId=" + (String)localObject + " delay=" + paramLong);
      zzpf();
      if (this.zzbeg == null) {
        throw new IllegalStateException("callback must be set before loadAfterDelay() is called.");
      }
    }
    finally {}
    if (this.zzbel != null) {
      this.zzbel.cancel(false);
    }
    Object localObject = this.zzbej;
    zzer localzzer = this.zzbek.zza(this.zzbeh);
    localzzer.zza(this.zzbeg);
    localzzer.zzdc(this.zzbaf);
    localzzer.zzdu(paramString);
    this.zzbel = ((ScheduledExecutorService)localObject).schedule(localzzer, paramLong, TimeUnit.MILLISECONDS);
  }
  
  public final void zza(zzdh<zzo> paramzzdh)
  {
    try
    {
      zzpf();
      this.zzbeg = paramzzdh;
      return;
    }
    finally
    {
      paramzzdh = finally;
      throw paramzzdh;
    }
  }
  
  public final void zzdc(String paramString)
  {
    try
    {
      zzpf();
      this.zzbaf = paramString;
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */