package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzn;
import com.google.android.gms.internal.measurement.zzrs;
import com.google.android.gms.internal.measurement.zzru;
import com.google.android.gms.internal.measurement.zzrw;
import com.google.android.gms.internal.measurement.zzry;
import com.google.android.gms.internal.measurement.zzzg;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@VisibleForTesting
final class zzfb
{
  private static final zzdz<com.google.android.gms.internal.measurement.zzp> zzbep = new zzdz(zzgj.zzqg(), true);
  private final DataLayer zzazg;
  private final zzrw zzbeq;
  private final zzbo zzber;
  private final Map<String, zzbq> zzbes;
  private final Map<String, zzbq> zzbet;
  private final Map<String, zzbq> zzbeu;
  private final zzp<zzru, zzdz<com.google.android.gms.internal.measurement.zzp>> zzbev;
  private final zzp<String, zzfh> zzbew;
  private final Set<zzry> zzbex;
  private final Map<String, zzfi> zzbey;
  private volatile String zzbez;
  private int zzbfa;
  
  public zzfb(Context paramContext, zzrw paramzzrw, DataLayer paramDataLayer, zzan paramzzan1, zzan paramzzan2, zzbo paramzzbo)
  {
    if (paramzzrw == null) {
      throw new NullPointerException("resource cannot be null");
    }
    this.zzbeq = paramzzrw;
    this.zzbex = new HashSet(paramzzrw.zzrw());
    this.zzazg = paramDataLayer;
    this.zzber = paramzzbo;
    paramzzrw = new zzfc(this);
    new zzq();
    this.zzbev = zzq.zza(1048576, paramzzrw);
    paramzzrw = new zzfd(this);
    new zzq();
    this.zzbew = zzq.zza(1048576, paramzzrw);
    this.zzbes = new HashMap();
    zzb(new zzm(paramContext));
    zzb(new zzam(paramzzan2));
    zzb(new zzaz(paramDataLayer));
    zzb(new zzgk(paramContext, paramDataLayer));
    this.zzbet = new HashMap();
    zzc(new zzak());
    zzc(new zzbl());
    zzc(new zzbm());
    zzc(new zzbs());
    zzc(new zzbt());
    zzc(new zzde());
    zzc(new zzdf());
    zzc(new zzel());
    zzc(new zzfy());
    this.zzbeu = new HashMap();
    zza(new zze(paramContext));
    zza(new zzf(paramContext));
    zza(new zzh(paramContext));
    zza(new zzi(paramContext));
    zza(new zzj(paramContext));
    zza(new zzk(paramContext));
    zza(new zzl(paramContext));
    zza(new zzt());
    zza(new zzaj(this.zzbeq.getVersion()));
    zza(new zzam(paramzzan1));
    zza(new zzas(paramDataLayer));
    zza(new zzbc(paramContext));
    zza(new zzbd());
    zza(new zzbk());
    zza(new zzbp(this));
    zza(new zzbu());
    zza(new zzbv());
    zza(new zzcv(paramContext));
    zza(new zzcx());
    zza(new zzdd());
    zza(new zzdk());
    zza(new zzdm(paramContext));
    zza(new zzea());
    zza(new zzee());
    zza(new zzei());
    zza(new zzek());
    zza(new zzem(paramContext));
    zza(new zzfj());
    zza(new zzfk());
    zza(new zzge());
    zza(new zzgl());
    this.zzbey = new HashMap();
    paramContext = this.zzbex.iterator();
    while (paramContext.hasNext())
    {
      paramzzrw = (zzry)paramContext.next();
      int i = 0;
      while (i < paramzzrw.zzsu().size())
      {
        paramDataLayer = (zzru)paramzzrw.zzsu().get(i);
        paramzzan1 = zzb(this.zzbey, zza(paramDataLayer));
        paramzzan1.zza(paramzzrw);
        paramzzan1.zza(paramzzrw, paramDataLayer);
        paramzzan1.zza(paramzzrw, "Unknown");
        i += 1;
      }
      i = 0;
      while (i < paramzzrw.zzsv().size())
      {
        paramDataLayer = (zzru)paramzzrw.zzsv().get(i);
        paramzzan1 = zzb(this.zzbey, zza(paramDataLayer));
        paramzzan1.zza(paramzzrw);
        paramzzan1.zzb(paramzzrw, paramDataLayer);
        paramzzan1.zzb(paramzzrw, "Unknown");
        i += 1;
      }
    }
    paramContext = this.zzbeq.zzss().entrySet().iterator();
    while (paramContext.hasNext())
    {
      paramzzrw = (Map.Entry)paramContext.next();
      paramDataLayer = ((List)paramzzrw.getValue()).iterator();
      while (paramDataLayer.hasNext())
      {
        paramzzan1 = (zzru)paramDataLayer.next();
        if (!zzgj.zzg((com.google.android.gms.internal.measurement.zzp)paramzzan1.zzry().get(zzb.zzjq.toString())).booleanValue()) {
          zzb(this.zzbey, (String)paramzzrw.getKey()).zzb(paramzzan1);
        }
      }
    }
  }
  
  private final zzdz<com.google.android.gms.internal.measurement.zzp> zza(com.google.android.gms.internal.measurement.zzp paramzzp, Set<String> paramSet, zzgm paramzzgm)
  {
    if (!paramzzp.zzqs) {
      return new zzdz(paramzzp, true);
    }
    zzdz localzzdz1;
    switch (paramzzp.type)
    {
    case 5: 
    case 6: 
    default: 
      i = paramzzp.type;
      zzdi.e(25 + "Unknown type: " + i);
      return zzbep;
    case 2: 
      localzzp = zzrs.zzk(paramzzp);
      localzzp.zzqj = new com.google.android.gms.internal.measurement.zzp[paramzzp.zzqj.length];
      i = 0;
      while (i < paramzzp.zzqj.length)
      {
        localzzdz1 = zza(paramzzp.zzqj[i], paramSet, paramzzgm.zzw(i));
        if (localzzdz1 == zzbep) {
          return zzbep;
        }
        localzzp.zzqj[i] = ((com.google.android.gms.internal.measurement.zzp)localzzdz1.getObject());
        i += 1;
      }
      return new zzdz(localzzp, false);
    case 3: 
      localzzp = zzrs.zzk(paramzzp);
      if (paramzzp.zzqk.length != paramzzp.zzql.length)
      {
        paramzzp = String.valueOf(paramzzp.toString());
        if (paramzzp.length() != 0) {}
        for (paramzzp = "Invalid serving value: ".concat(paramzzp);; paramzzp = new String("Invalid serving value: "))
        {
          zzdi.e(paramzzp);
          return zzbep;
        }
      }
      localzzp.zzqk = new com.google.android.gms.internal.measurement.zzp[paramzzp.zzqk.length];
      localzzp.zzql = new com.google.android.gms.internal.measurement.zzp[paramzzp.zzqk.length];
      i = 0;
      while (i < paramzzp.zzqk.length)
      {
        localzzdz1 = zza(paramzzp.zzqk[i], paramSet, paramzzgm.zzx(i));
        zzdz localzzdz2 = zza(paramzzp.zzql[i], paramSet, paramzzgm.zzy(i));
        if ((localzzdz1 == zzbep) || (localzzdz2 == zzbep)) {
          return zzbep;
        }
        localzzp.zzqk[i] = ((com.google.android.gms.internal.measurement.zzp)localzzdz1.getObject());
        localzzp.zzql[i] = ((com.google.android.gms.internal.measurement.zzp)localzzdz2.getObject());
        i += 1;
      }
      return new zzdz(localzzp, false);
    case 4: 
      if (paramSet.contains(paramzzp.zzqm))
      {
        paramzzp = paramzzp.zzqm;
        paramSet = paramSet.toString();
        zzdi.e(String.valueOf(paramzzp).length() + 79 + String.valueOf(paramSet).length() + "Macro cycle detected.  Current macro reference: " + paramzzp + ".  Previous macro references: " + paramSet + ".");
        return zzbep;
      }
      paramSet.add(paramzzp.zzqm);
      paramzzgm = zzgn.zza(zza(paramzzp.zzqm, paramSet, paramzzgm.zzox()), paramzzp.zzqr);
      paramSet.remove(paramzzp.zzqm);
      return paramzzgm;
    }
    com.google.android.gms.internal.measurement.zzp localzzp = zzrs.zzk(paramzzp);
    localzzp.zzqq = new com.google.android.gms.internal.measurement.zzp[paramzzp.zzqq.length];
    int i = 0;
    while (i < paramzzp.zzqq.length)
    {
      localzzdz1 = zza(paramzzp.zzqq[i], paramSet, paramzzgm.zzz(i));
      if (localzzdz1 == zzbep) {
        return zzbep;
      }
      localzzp.zzqq[i] = ((com.google.android.gms.internal.measurement.zzp)localzzdz1.getObject());
      i += 1;
    }
    return new zzdz(localzzp, false);
  }
  
  @VisibleForTesting
  private final zzdz<Boolean> zza(zzru paramzzru, Set<String> paramSet, zzen paramzzen)
  {
    paramzzru = zza(this.zzbet, paramzzru, paramSet, paramzzen);
    paramSet = zzgj.zzg((com.google.android.gms.internal.measurement.zzp)paramzzru.getObject());
    paramzzen.zza(zzgj.zzj(paramSet));
    return new zzdz(paramSet, paramzzru.zzoy());
  }
  
  private final zzdz<com.google.android.gms.internal.measurement.zzp> zza(String paramString, Set<String> paramSet, zzdl paramzzdl)
  {
    this.zzbfa += 1;
    Object localObject1 = (zzfh)this.zzbew.get(paramString);
    if (localObject1 != null)
    {
      this.zzber.zzoj();
      zza(((zzfh)localObject1).zzpm(), paramSet);
      this.zzbfa -= 1;
      return ((zzfh)localObject1).zzpl();
    }
    localObject1 = (zzfi)this.zzbey.get(paramString);
    if (localObject1 == null)
    {
      paramSet = zzpk();
      zzdi.e(String.valueOf(paramSet).length() + 15 + String.valueOf(paramString).length() + paramSet + "Invalid macro: " + paramString);
      this.zzbfa -= 1;
      return zzbep;
    }
    Object localObject2 = ((zzfi)localObject1).zzpn();
    Map localMap1 = ((zzfi)localObject1).zzpo();
    Map localMap2 = ((zzfi)localObject1).zzpp();
    Map localMap3 = ((zzfi)localObject1).zzpr();
    Map localMap4 = ((zzfi)localObject1).zzpq();
    zzfa localzzfa = paramzzdl.zznx();
    localObject2 = zza((Set)localObject2, paramSet, new zzfe(this, localMap1, localMap2, localMap3, localMap4), localzzfa);
    if (((Set)((zzdz)localObject2).getObject()).isEmpty()) {}
    for (localObject1 = ((zzfi)localObject1).zzps(); localObject1 == null; localObject1 = (zzru)((Set)((zzdz)localObject2).getObject()).iterator().next())
    {
      this.zzbfa -= 1;
      return zzbep;
      if (((Set)((zzdz)localObject2).getObject()).size() > 1)
      {
        localObject1 = zzpk();
        zzdi.zzab(String.valueOf(localObject1).length() + 37 + String.valueOf(paramString).length() + (String)localObject1 + "Multiple macros active for macroName " + paramString);
      }
    }
    paramzzdl = zza(this.zzbeu, (zzru)localObject1, paramSet, paramzzdl.zzop());
    boolean bool;
    if ((((zzdz)localObject2).zzoy()) && (paramzzdl.zzoy()))
    {
      bool = true;
      if (paramzzdl != zzbep) {
        break label464;
      }
    }
    label464:
    for (paramzzdl = zzbep;; paramzzdl = new zzdz((com.google.android.gms.internal.measurement.zzp)paramzzdl.getObject(), bool))
    {
      localObject1 = ((zzru)localObject1).zzpm();
      if (paramzzdl.zzoy()) {
        this.zzbew.zza(paramString, new zzfh(paramzzdl, (com.google.android.gms.internal.measurement.zzp)localObject1));
      }
      zza((com.google.android.gms.internal.measurement.zzp)localObject1, paramSet);
      this.zzbfa -= 1;
      return paramzzdl;
      bool = false;
      break;
    }
  }
  
  private final zzdz<com.google.android.gms.internal.measurement.zzp> zza(Map<String, zzbq> paramMap, zzru paramzzru, Set<String> paramSet, zzen paramzzen)
  {
    boolean bool = true;
    Object localObject1 = (com.google.android.gms.internal.measurement.zzp)paramzzru.zzry().get(zzb.zzhz.toString());
    if (localObject1 == null)
    {
      zzdi.e("No function id in properties");
      return zzbep;
    }
    localObject1 = ((com.google.android.gms.internal.measurement.zzp)localObject1).zzqn;
    paramMap = (zzbq)paramMap.get(localObject1);
    if (paramMap == null)
    {
      zzdi.e(String.valueOf(localObject1).concat(" has no backing implementation."));
      return zzbep;
    }
    Object localObject2 = (zzdz)this.zzbev.get(paramzzru);
    if (localObject2 != null)
    {
      this.zzber.zzoj();
      return (zzdz<com.google.android.gms.internal.measurement.zzp>)localObject2;
    }
    localObject2 = new HashMap();
    Iterator localIterator = paramzzru.zzry().entrySet().iterator();
    int i = 1;
    if (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      Object localObject3 = paramzzen.zzds((String)localEntry.getKey());
      localObject3 = zza((com.google.android.gms.internal.measurement.zzp)localEntry.getValue(), paramSet, ((zzep)localObject3).zzb((com.google.android.gms.internal.measurement.zzp)localEntry.getValue()));
      if (localObject3 == zzbep) {
        return zzbep;
      }
      if (((zzdz)localObject3).zzoy()) {
        paramzzru.zza((String)localEntry.getKey(), (com.google.android.gms.internal.measurement.zzp)((zzdz)localObject3).getObject());
      }
      for (;;)
      {
        ((Map)localObject2).put((String)localEntry.getKey(), (com.google.android.gms.internal.measurement.zzp)((zzdz)localObject3).getObject());
        break;
        i = 0;
      }
    }
    if (!paramMap.zza(((Map)localObject2).keySet()))
    {
      paramMap = String.valueOf(paramMap.zzol());
      paramzzru = String.valueOf(((Map)localObject2).keySet());
      zzdi.e(String.valueOf(localObject1).length() + 43 + String.valueOf(paramMap).length() + String.valueOf(paramzzru).length() + "Incorrect keys for function " + (String)localObject1 + " required " + paramMap + " had " + paramzzru);
      return zzbep;
    }
    if ((i != 0) && (paramMap.zznb())) {}
    for (;;)
    {
      paramMap = new zzdz(paramMap.zze((Map)localObject2), bool);
      if (bool) {
        this.zzbev.zza(paramzzru, paramMap);
      }
      paramzzen.zza((com.google.android.gms.internal.measurement.zzp)paramMap.getObject());
      return paramMap;
      bool = false;
    }
  }
  
  private final zzdz<Set<zzru>> zza(Set<zzry> paramSet, Set<String> paramSet1, zzfg paramzzfg, zzfa paramzzfa)
  {
    HashSet localHashSet1 = new HashSet();
    HashSet localHashSet2 = new HashSet();
    Iterator localIterator = paramSet.iterator();
    boolean bool2 = true;
    if (localIterator.hasNext())
    {
      zzry localzzry = (zzry)localIterator.next();
      zzeq localzzeq = paramzzfa.zzow();
      paramSet = localzzry.zzsb().iterator();
      boolean bool1 = true;
      zzdz localzzdz;
      if (paramSet.hasNext())
      {
        localzzdz = zza((zzru)paramSet.next(), paramSet1, localzzeq.zzoq());
        if (((Boolean)localzzdz.getObject()).booleanValue())
        {
          zzgj.zzj(Boolean.valueOf(false));
          paramSet = new zzdz(Boolean.valueOf(false), localzzdz.zzoy());
          label145:
          if (((Boolean)paramSet.getObject()).booleanValue()) {
            paramzzfg.zza(localzzry, localHashSet1, localHashSet2, localzzeq);
          }
          if ((!bool2) || (!paramSet.zzoy())) {
            break label354;
          }
        }
      }
      label329:
      label354:
      for (bool1 = true;; bool1 = false)
      {
        bool2 = bool1;
        break;
        if ((bool1) && (localzzdz.zzoy())) {}
        for (bool1 = true;; bool1 = false) {
          break;
        }
        paramSet = localzzry.zzsa().iterator();
        for (;;)
        {
          if (!paramSet.hasNext()) {
            break label329;
          }
          localzzdz = zza((zzru)paramSet.next(), paramSet1, localzzeq.zzor());
          if (!((Boolean)localzzdz.getObject()).booleanValue())
          {
            zzgj.zzj(Boolean.valueOf(false));
            paramSet = new zzdz(Boolean.valueOf(false), localzzdz.zzoy());
            break;
          }
          if ((bool1) && (localzzdz.zzoy())) {
            bool1 = true;
          } else {
            bool1 = false;
          }
        }
        zzgj.zzj(Boolean.valueOf(true));
        paramSet = new zzdz(Boolean.valueOf(true), bool1);
        break label145;
      }
    }
    localHashSet1.removeAll(localHashSet2);
    paramzzfa.zzb(localHashSet1);
    return new zzdz(localHashSet1, bool2);
  }
  
  private static String zza(zzru paramzzru)
  {
    return zzgj.zzc((com.google.android.gms.internal.measurement.zzp)paramzzru.zzry().get(zzb.zzil.toString()));
  }
  
  private final void zza(com.google.android.gms.internal.measurement.zzp paramzzp, Set<String> paramSet)
  {
    if (paramzzp == null) {}
    for (;;)
    {
      return;
      paramzzp = zza(paramzzp, paramSet, new zzdx());
      if (paramzzp != zzbep)
      {
        paramzzp = zzgj.zzh((com.google.android.gms.internal.measurement.zzp)paramzzp.getObject());
        if ((paramzzp instanceof Map))
        {
          paramzzp = (Map)paramzzp;
          this.zzazg.push(paramzzp);
          return;
        }
        if (!(paramzzp instanceof List)) {
          break;
        }
        paramzzp = ((List)paramzzp).iterator();
        while (paramzzp.hasNext())
        {
          paramSet = paramzzp.next();
          if ((paramSet instanceof Map))
          {
            paramSet = (Map)paramSet;
            this.zzazg.push(paramSet);
          }
          else
          {
            zzdi.zzab("pushAfterEvaluate: value not a Map");
          }
        }
      }
    }
    zzdi.zzab("pushAfterEvaluate: value not a Map or List");
  }
  
  @VisibleForTesting
  private final void zza(zzbq paramzzbq)
  {
    zza(this.zzbeu, paramzzbq);
  }
  
  private static void zza(Map<String, zzbq> paramMap, zzbq paramzzbq)
  {
    if (paramMap.containsKey(paramzzbq.zzok()))
    {
      paramMap = String.valueOf(paramzzbq.zzok());
      if (paramMap.length() != 0) {}
      for (paramMap = "Duplicate function type name: ".concat(paramMap);; paramMap = new String("Duplicate function type name: ")) {
        throw new IllegalArgumentException(paramMap);
      }
    }
    paramMap.put(paramzzbq.zzok(), paramzzbq);
  }
  
  private static zzfi zzb(Map<String, zzfi> paramMap, String paramString)
  {
    zzfi localzzfi2 = (zzfi)paramMap.get(paramString);
    zzfi localzzfi1 = localzzfi2;
    if (localzzfi2 == null)
    {
      localzzfi1 = new zzfi();
      paramMap.put(paramString, localzzfi1);
    }
    return localzzfi1;
  }
  
  @VisibleForTesting
  private final void zzb(zzbq paramzzbq)
  {
    zza(this.zzbes, paramzzbq);
  }
  
  @VisibleForTesting
  private final void zzc(zzbq paramzzbq)
  {
    zza(this.zzbet, paramzzbq);
  }
  
  @VisibleForTesting
  private final void zzdw(String paramString)
  {
    try
    {
      this.zzbez = paramString;
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  private final String zzpk()
  {
    if (this.zzbfa <= 1) {
      return "";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(Integer.toString(this.zzbfa));
    int i = 2;
    while (i < this.zzbfa)
    {
      localStringBuilder.append(' ');
      i += 1;
    }
    localStringBuilder.append(": ");
    return localStringBuilder.toString();
  }
  
  public final void zzda(String paramString)
  {
    try
    {
      zzdw(paramString);
      paramString = this.zzber.zzdm(paramString).zzoi();
      Object localObject1 = this.zzbex;
      Object localObject2 = paramString.zznx();
      localObject1 = ((Set)zza((Set)localObject1, new HashSet(), new zzff(this), (zzfa)localObject2).getObject()).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (zzru)((Iterator)localObject1).next();
        zza(this.zzbes, (zzru)localObject2, new HashSet(), paramString.zznw());
      }
      zzdw(null);
    }
    finally {}
  }
  
  public final zzdz<com.google.android.gms.internal.measurement.zzp> zzdv(String paramString)
  {
    this.zzbfa = 0;
    zzbn localzzbn = this.zzber.zzdl(paramString);
    return zza(paramString, new HashSet(), localzzbn.zzoh());
  }
  
  public final void zzf(List<zzn> paramList)
  {
    label507:
    label516:
    label569:
    label579:
    label584:
    for (;;)
    {
      Object localObject1;
      try
      {
        Iterator localIterator = paramList.iterator();
        if (!localIterator.hasNext()) {
          break label569;
        }
        localObject1 = (zzn)localIterator.next();
        if ((((zzn)localObject1).name == null) || (!((zzn)localObject1).name.startsWith("gaExperiment:")))
        {
          paramList = String.valueOf(localObject1);
          zzdi.v(String.valueOf(paramList).length() + 22 + "Ignored supplemental: " + paramList);
          continue;
        }
        localDataLayer = this.zzazg;
      }
      finally {}
      DataLayer localDataLayer;
      if (((zzn)localObject1).zzqe == null)
      {
        zzdi.zzab("supplemental missing experimentSupplemental");
      }
      else
      {
        paramList = ((zzn)localObject1).zzqe.zzop;
        int j = paramList.length;
        int i = 0;
        while (i < j)
        {
          localDataLayer.zzdd(zzgj.zzc(paramList[i]));
          i += 1;
        }
        Object localObject2 = ((zzn)localObject1).zzqe.zzoo;
        j = localObject2.length;
        i = 0;
        if (i < j)
        {
          paramList = zzgj.zzh(localObject2[i]);
          if (!(paramList instanceof Map))
          {
            paramList = String.valueOf(paramList);
            zzdi.zzab(String.valueOf(paramList).length() + 36 + "value: " + paramList + " is not a map value, ignored.");
          }
          for (paramList = null; paramList != null; paramList = (Map)paramList)
          {
            localDataLayer.push(paramList);
            break;
          }
        }
        localObject2 = ((zzn)localObject1).zzqe.zzoq;
        j = localObject2.length;
        i = 0;
        for (;;)
        {
          if (i >= j) {
            break label584;
          }
          Object localObject3 = localObject2[i];
          if (((com.google.android.gms.internal.measurement.zzi)localObject3).zzoj == null)
          {
            zzdi.zzab("GaExperimentRandom: No key");
          }
          else
          {
            localObject1 = localDataLayer.get(((com.google.android.gms.internal.measurement.zzi)localObject3).zzoj);
            if (!(localObject1 instanceof Number))
            {
              paramList = null;
              long l1 = ((com.google.android.gms.internal.measurement.zzi)localObject3).zzok;
              long l2 = ((com.google.android.gms.internal.measurement.zzi)localObject3).zzol;
              if ((!((com.google.android.gms.internal.measurement.zzi)localObject3).zzom) || (paramList == null) || (paramList.longValue() < l1) || (paramList.longValue() > l2))
              {
                if (l1 > l2) {
                  break label507;
                }
                localObject1 = Long.valueOf(Math.round(Math.random() * (l2 - l1) + l1));
              }
              localDataLayer.zzdd(((com.google.android.gms.internal.measurement.zzi)localObject3).zzoj);
              paramList = DataLayer.zzk(((com.google.android.gms.internal.measurement.zzi)localObject3).zzoj, localObject1);
              if (((com.google.android.gms.internal.measurement.zzi)localObject3).zzon > 0L)
              {
                if (paramList.containsKey("gtm")) {
                  break label516;
                }
                paramList.put("gtm", DataLayer.mapOf(new Object[] { "lifetime", Long.valueOf(((com.google.android.gms.internal.measurement.zzi)localObject3).zzon) }));
              }
            }
            for (;;)
            {
              localDataLayer.push(paramList);
              break label579;
              paramList = Long.valueOf(((Number)localObject1).longValue());
              break;
              zzdi.zzab("GaExperimentRandom: random range invalid");
              break label579;
              localObject1 = paramList.get("gtm");
              if ((localObject1 instanceof Map)) {
                ((Map)localObject1).put("lifetime", Long.valueOf(((com.google.android.gms.internal.measurement.zzi)localObject3).zzon));
              } else {
                zzdi.zzab("GaExperimentRandom: gtm not a map");
              }
            }
            return;
            i += 1;
            break;
          }
          i += 1;
        }
      }
    }
  }
  
  final String zzpj()
  {
    try
    {
      String str = this.zzbez;
      return str;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */