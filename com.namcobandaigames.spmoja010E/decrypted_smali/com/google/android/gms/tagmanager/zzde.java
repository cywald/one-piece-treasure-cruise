package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzde
  extends zzdy
{
  private static final String ID = zza.zzbp.toString();
  
  public zzde()
  {
    super(ID);
  }
  
  protected final boolean zza(zzgi paramzzgi1, zzgi paramzzgi2, Map<String, zzp> paramMap)
  {
    return paramzzgi1.zza(paramzzgi2) <= 0;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzde.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */