package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.Uri.Builder;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class zzm
  extends zzgh
{
  private static final String ID = zza.zzbu.toString();
  private static final String URL = zzb.zzmz.toString();
  private static final String zzayz = zzb.zzds.toString();
  private static final String zzaza = zzb.zzmy.toString();
  private static final String zzazb;
  private static final Set<String> zzazc = new HashSet();
  private final zza zzazd;
  private final Context zzri;
  
  static
  {
    String str = ID;
    zzazb = String.valueOf(str).length() + 17 + "gtm_" + str + "_unrepeatable";
  }
  
  public zzm(Context paramContext)
  {
    this(paramContext, new zzn(paramContext));
  }
  
  @VisibleForTesting
  private zzm(Context paramContext, zza paramzza)
  {
    super(ID, new String[] { URL });
    this.zzazd = paramzza;
    this.zzri = paramContext;
  }
  
  private final boolean zzcx(String paramString)
  {
    boolean bool1 = true;
    for (;;)
    {
      try
      {
        boolean bool2 = zzazc.contains(paramString);
        if (bool2) {
          return bool1;
        }
        if (this.zzri.getSharedPreferences(zzazb, 0).contains(paramString)) {
          zzazc.add(paramString);
        } else {
          bool1 = false;
        }
      }
      finally {}
    }
  }
  
  public final void zzg(Map<String, zzp> paramMap)
  {
    if (paramMap.get(zzaza) != null) {}
    for (String str = zzgj.zzc((zzp)paramMap.get(zzaza)); (str != null) && (zzcx(str)); str = null) {
      return;
    }
    Uri.Builder localBuilder = Uri.parse(zzgj.zzc((zzp)paramMap.get(URL))).buildUpon();
    paramMap = (zzp)paramMap.get(zzayz);
    if (paramMap != null)
    {
      paramMap = zzgj.zzh(paramMap);
      if (!(paramMap instanceof List))
      {
        paramMap = String.valueOf(localBuilder.build().toString());
        if (paramMap.length() != 0) {}
        for (paramMap = "ArbitraryPixel: additional params not a list: not sending partial hit: ".concat(paramMap);; paramMap = new String("ArbitraryPixel: additional params not a list: not sending partial hit: "))
        {
          zzdi.e(paramMap);
          return;
        }
      }
      paramMap = ((List)paramMap).iterator();
      while (paramMap.hasNext())
      {
        Object localObject = paramMap.next();
        if (!(localObject instanceof Map))
        {
          paramMap = String.valueOf(localBuilder.build().toString());
          if (paramMap.length() != 0) {}
          for (paramMap = "ArbitraryPixel: additional params contains non-map: not sending partial hit: ".concat(paramMap);; paramMap = new String("ArbitraryPixel: additional params contains non-map: not sending partial hit: "))
          {
            zzdi.e(paramMap);
            return;
          }
        }
        localObject = ((Map)localObject).entrySet().iterator();
        while (((Iterator)localObject).hasNext())
        {
          Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
          localBuilder.appendQueryParameter(localEntry.getKey().toString(), localEntry.getValue().toString());
        }
      }
    }
    paramMap = localBuilder.build().toString();
    this.zzazd.zznc().zzdk(paramMap);
    paramMap = String.valueOf(paramMap);
    if (paramMap.length() != 0) {}
    for (paramMap = "ArbitraryPixel: url = ".concat(paramMap);; paramMap = new String("ArbitraryPixel: url = "))
    {
      zzdi.v(paramMap);
      if (str == null) {
        break;
      }
      try
      {
        zzazc.add(str);
        zzft.zza(this.zzri, zzazb, str, "true");
        return;
      }
      finally {}
    }
  }
  
  public static abstract interface zza
  {
    public abstract zzbx zznc();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */