package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;

public final class zzbb
  implements zzbx
{
  private static final Object zzayt = new Object();
  private static zzbb zzbbk;
  private zzej zzazx;
  private zzby zzbbl;
  
  private zzbb(Context paramContext)
  {
    this(zzbz.zzu(paramContext), new zzfl());
  }
  
  @VisibleForTesting
  private zzbb(zzby paramzzby, zzej paramzzej)
  {
    this.zzbbl = paramzzby;
    this.zzazx = paramzzej;
  }
  
  public static zzbx zzo(Context paramContext)
  {
    synchronized (zzayt)
    {
      if (zzbbk == null) {
        zzbbk = new zzbb(paramContext);
      }
      paramContext = zzbbk;
      return paramContext;
    }
  }
  
  public final boolean zzdk(String paramString)
  {
    if (!this.zzazx.zzew())
    {
      zzdi.zzab("Too many urls sent too quickly with the TagManagerSender, rate limiting invoked.");
      return false;
    }
    this.zzbbl.zzdp(paramString);
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzbb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */