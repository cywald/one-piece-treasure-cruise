package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
final class zzec
  implements zzfw
{
  zzec(zzeb paramzzeb) {}
  
  public final void zza(zzbw paramzzbw)
  {
    zzeb.zza(this.zzbdl, paramzzbw.zzom());
  }
  
  public final void zzb(zzbw paramzzbw)
  {
    zzeb.zza(this.zzbdl, paramzzbw.zzom());
    long l = paramzzbw.zzom();
    zzdi.v(57 + "Permanent failure dispatching hitId: " + l);
  }
  
  public final void zzc(zzbw paramzzbw)
  {
    long l = paramzzbw.zzon();
    if (l == 0L) {
      zzeb.zza(this.zzbdl, paramzzbw.zzom(), zzeb.zza(this.zzbdl).currentTimeMillis());
    }
    while (l + 14400000L >= zzeb.zza(this.zzbdl).currentTimeMillis()) {
      return;
    }
    zzeb.zza(this.zzbdl, paramzzbw.zzom());
    l = paramzzbw.zzom();
    zzdi.v(47 + "Giving up on failed hitId: " + l);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */