package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzi
  extends zzbq
{
  private static final String ID = zza.zzc.toString();
  private final Context zzri;
  
  public zzi(Context paramContext)
  {
    super(ID, new String[0]);
    this.zzri = paramContext;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    return zzgj.zzj(this.zzri.getPackageName());
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */