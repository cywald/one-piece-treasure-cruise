package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzru;
import com.google.android.gms.internal.measurement.zzry;
import java.util.Set;

abstract interface zzfg
{
  public abstract void zza(zzry paramzzry, Set<zzru> paramSet1, Set<zzru> paramSet2, zzeq paramzzeq);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */