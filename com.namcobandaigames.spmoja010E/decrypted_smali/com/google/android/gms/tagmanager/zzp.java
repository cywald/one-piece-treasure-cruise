package com.google.android.gms.tagmanager;

abstract interface zzp<K, V>
{
  public abstract V get(K paramK);
  
  public abstract void zza(K paramK, V paramV);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */