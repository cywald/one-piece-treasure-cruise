package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

abstract class zzgh
  extends zzbq
{
  public zzgh(String paramString, String... paramVarArgs)
  {
    super(paramString, paramVarArgs);
  }
  
  public zzp zze(Map<String, zzp> paramMap)
  {
    zzg(paramMap);
    return zzgj.zzqg();
  }
  
  public abstract void zzg(Map<String, zzp> paramMap);
  
  public boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzgh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */