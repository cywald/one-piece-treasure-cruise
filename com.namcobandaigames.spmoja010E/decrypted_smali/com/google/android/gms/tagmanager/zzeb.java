package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Collections;
import java.util.List;

@VisibleForTesting
final class zzeb
  implements zzcb
{
  private static final String zzxf = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL,'%s' INTEGER NOT NULL);", new Object[] { "gtm_hits", "hit_id", "hit_time", "hit_url", "hit_first_send_time" });
  private final zzed zzbdf;
  private volatile zzbe zzbdg;
  private final zzcc zzbdh;
  private final String zzbdi;
  private long zzbdj;
  private final int zzbdk;
  private final Context zzri;
  private Clock zzrz;
  
  zzeb(zzcc paramzzcc, Context paramContext)
  {
    this(paramzzcc, paramContext, "gtm_urls.db", 2000);
  }
  
  @VisibleForTesting
  private zzeb(zzcc paramzzcc, Context paramContext, String paramString, int paramInt)
  {
    this.zzri = paramContext.getApplicationContext();
    this.zzbdi = paramString;
    this.zzbdh = paramzzcc;
    this.zzrz = DefaultClock.getInstance();
    this.zzbdf = new zzed(this, this.zzri, this.zzbdi);
    this.zzbdg = new zzfu(this.zzri, new zzec(this));
    this.zzbdj = 0L;
    this.zzbdk = 2000;
  }
  
  private final void zza(String[] paramArrayOfString)
  {
    boolean bool = true;
    if ((paramArrayOfString == null) || (paramArrayOfString.length == 0)) {}
    SQLiteDatabase localSQLiteDatabase;
    do
    {
      return;
      localSQLiteDatabase = zzdh("Error opening database for deleteHits.");
    } while (localSQLiteDatabase == null);
    String str = String.format("HIT_ID in (%s)", new Object[] { TextUtils.join(",", Collections.nCopies(paramArrayOfString.length, "?")) });
    for (;;)
    {
      try
      {
        localSQLiteDatabase.delete("gtm_hits", str, paramArrayOfString);
        paramArrayOfString = this.zzbdh;
        if (zzoz() == 0)
        {
          paramArrayOfString.zzq(bool);
          return;
        }
      }
      catch (SQLiteException paramArrayOfString)
      {
        zzdi.zzab("Error deleting hits");
        return;
      }
      bool = false;
    }
  }
  
  /* Error */
  private final List<String> zzaa(int paramInt)
  {
    // Byte code:
    //   0: new 166	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 167	java/util/ArrayList:<init>	()V
    //   7: astore 6
    //   9: iload_1
    //   10: ifgt +11 -> 21
    //   13: ldc -87
    //   15: invokestatic 162	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   18: aload 6
    //   20: areturn
    //   21: aload_0
    //   22: ldc -85
    //   24: invokespecial 120	com/google/android/gms/tagmanager/zzeb:zzdh	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    //   27: astore_3
    //   28: aload_3
    //   29: ifnonnull +6 -> 35
    //   32: aload 6
    //   34: areturn
    //   35: ldc -83
    //   37: iconst_1
    //   38: anewarray 4	java/lang/Object
    //   41: dup
    //   42: iconst_0
    //   43: ldc 32
    //   45: aastore
    //   46: invokestatic 44	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   49: astore 4
    //   51: iload_1
    //   52: invokestatic 179	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   55: astore 5
    //   57: aload_3
    //   58: ldc 30
    //   60: iconst_1
    //   61: anewarray 40	java/lang/String
    //   64: dup
    //   65: iconst_0
    //   66: ldc 32
    //   68: aastore
    //   69: aconst_null
    //   70: aconst_null
    //   71: aconst_null
    //   72: aconst_null
    //   73: aload 4
    //   75: aload 5
    //   77: invokevirtual 183	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   80: astore 4
    //   82: aload 4
    //   84: astore_3
    //   85: aload 4
    //   87: invokeinterface 189 1 0
    //   92: ifeq +40 -> 132
    //   95: aload 4
    //   97: astore_3
    //   98: aload 6
    //   100: aload 4
    //   102: iconst_0
    //   103: invokeinterface 193 2 0
    //   108: invokestatic 197	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   111: invokeinterface 203 2 0
    //   116: pop
    //   117: aload 4
    //   119: astore_3
    //   120: aload 4
    //   122: invokeinterface 206 1 0
    //   127: istore_2
    //   128: iload_2
    //   129: ifne -34 -> 95
    //   132: aload 4
    //   134: ifnull +10 -> 144
    //   137: aload 4
    //   139: invokeinterface 209 1 0
    //   144: aload 6
    //   146: areturn
    //   147: astore 5
    //   149: aconst_null
    //   150: astore 4
    //   152: aload 4
    //   154: astore_3
    //   155: aload 5
    //   157: invokevirtual 213	android/database/sqlite/SQLiteException:getMessage	()Ljava/lang/String;
    //   160: invokestatic 216	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   163: astore 5
    //   165: aload 4
    //   167: astore_3
    //   168: aload 5
    //   170: invokevirtual 219	java/lang/String:length	()I
    //   173: ifeq +38 -> 211
    //   176: aload 4
    //   178: astore_3
    //   179: ldc -35
    //   181: aload 5
    //   183: invokevirtual 225	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   186: astore 5
    //   188: aload 4
    //   190: astore_3
    //   191: aload 5
    //   193: invokestatic 162	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   196: aload 4
    //   198: ifnull -54 -> 144
    //   201: aload 4
    //   203: invokeinterface 209 1 0
    //   208: goto -64 -> 144
    //   211: aload 4
    //   213: astore_3
    //   214: new 40	java/lang/String
    //   217: dup
    //   218: ldc -35
    //   220: invokespecial 227	java/lang/String:<init>	(Ljava/lang/String;)V
    //   223: astore 5
    //   225: goto -37 -> 188
    //   228: astore 5
    //   230: aload_3
    //   231: astore 4
    //   233: aload 5
    //   235: astore_3
    //   236: aload 4
    //   238: ifnull +10 -> 248
    //   241: aload 4
    //   243: invokeinterface 209 1 0
    //   248: aload_3
    //   249: athrow
    //   250: astore_3
    //   251: aconst_null
    //   252: astore 4
    //   254: goto -18 -> 236
    //   257: astore 5
    //   259: goto -107 -> 152
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	262	0	this	zzeb
    //   0	262	1	paramInt	int
    //   127	2	2	bool	boolean
    //   27	222	3	localObject1	Object
    //   250	1	3	localObject2	Object
    //   49	204	4	localObject3	Object
    //   55	21	5	str1	String
    //   147	9	5	localSQLiteException1	SQLiteException
    //   163	61	5	str2	String
    //   228	6	5	localObject4	Object
    //   257	1	5	localSQLiteException2	SQLiteException
    //   7	138	6	localArrayList	java.util.ArrayList
    // Exception table:
    //   from	to	target	type
    //   35	82	147	android/database/sqlite/SQLiteException
    //   85	95	228	finally
    //   98	117	228	finally
    //   120	128	228	finally
    //   155	165	228	finally
    //   168	176	228	finally
    //   179	188	228	finally
    //   191	196	228	finally
    //   214	225	228	finally
    //   35	82	250	finally
    //   85	95	257	android/database/sqlite/SQLiteException
    //   98	117	257	android/database/sqlite/SQLiteException
    //   120	128	257	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  private final List<zzbw> zzab(int paramInt)
  {
    // Byte code:
    //   0: new 166	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 167	java/util/ArrayList:<init>	()V
    //   7: astore 7
    //   9: aload_0
    //   10: ldc -25
    //   12: invokespecial 120	com/google/android/gms/tagmanager/zzeb:zzdh	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    //   15: astore 10
    //   17: aload 10
    //   19: ifnonnull +10 -> 29
    //   22: aload 7
    //   24: astore 8
    //   26: aload 8
    //   28: areturn
    //   29: aconst_null
    //   30: astore 8
    //   32: ldc -83
    //   34: iconst_1
    //   35: anewarray 4	java/lang/Object
    //   38: dup
    //   39: iconst_0
    //   40: ldc 32
    //   42: aastore
    //   43: invokestatic 44	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   46: astore 6
    //   48: bipush 40
    //   50: invokestatic 179	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   53: astore 9
    //   55: aload 10
    //   57: ldc 30
    //   59: iconst_3
    //   60: anewarray 40	java/lang/String
    //   63: dup
    //   64: iconst_0
    //   65: ldc 32
    //   67: aastore
    //   68: dup
    //   69: iconst_1
    //   70: ldc 34
    //   72: aastore
    //   73: dup
    //   74: iconst_2
    //   75: ldc 38
    //   77: aastore
    //   78: aconst_null
    //   79: aconst_null
    //   80: aconst_null
    //   81: aconst_null
    //   82: aload 6
    //   84: aload 9
    //   86: invokevirtual 183	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   89: astore 6
    //   91: new 166	java/util/ArrayList
    //   94: dup
    //   95: invokespecial 167	java/util/ArrayList:<init>	()V
    //   98: astore 9
    //   100: aload 6
    //   102: invokeinterface 189 1 0
    //   107: ifeq +56 -> 163
    //   110: aload 9
    //   112: new 233	com/google/android/gms/tagmanager/zzbw
    //   115: dup
    //   116: aload 6
    //   118: iconst_0
    //   119: invokeinterface 193 2 0
    //   124: aload 6
    //   126: iconst_1
    //   127: invokeinterface 193 2 0
    //   132: aload 6
    //   134: iconst_2
    //   135: invokeinterface 193 2 0
    //   140: invokespecial 236	com/google/android/gms/tagmanager/zzbw:<init>	(JJJ)V
    //   143: invokeinterface 203 2 0
    //   148: pop
    //   149: aload 6
    //   151: invokeinterface 206 1 0
    //   156: istore 5
    //   158: iload 5
    //   160: ifne -50 -> 110
    //   163: aload 6
    //   165: ifnull +10 -> 175
    //   168: aload 6
    //   170: invokeinterface 209 1 0
    //   175: aload 6
    //   177: astore 7
    //   179: ldc -83
    //   181: iconst_1
    //   182: anewarray 4	java/lang/Object
    //   185: dup
    //   186: iconst_0
    //   187: ldc 32
    //   189: aastore
    //   190: invokestatic 44	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   193: astore 8
    //   195: aload 6
    //   197: astore 7
    //   199: bipush 40
    //   201: invokestatic 179	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   204: astore 11
    //   206: aload 6
    //   208: astore 7
    //   210: aload 10
    //   212: ldc 30
    //   214: iconst_2
    //   215: anewarray 40	java/lang/String
    //   218: dup
    //   219: iconst_0
    //   220: ldc 32
    //   222: aastore
    //   223: dup
    //   224: iconst_1
    //   225: ldc 36
    //   227: aastore
    //   228: aconst_null
    //   229: aconst_null
    //   230: aconst_null
    //   231: aconst_null
    //   232: aload 8
    //   234: aload 11
    //   236: invokevirtual 183	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   239: astore 10
    //   241: aload 10
    //   243: invokeinterface 189 1 0
    //   248: ifeq +55 -> 303
    //   251: iconst_0
    //   252: istore_1
    //   253: aload 10
    //   255: checkcast 238	android/database/sqlite/SQLiteCursor
    //   258: invokevirtual 242	android/database/sqlite/SQLiteCursor:getWindow	()Landroid/database/CursorWindow;
    //   261: invokevirtual 247	android/database/CursorWindow:getNumRows	()I
    //   264: ifle +153 -> 417
    //   267: aload 9
    //   269: iload_1
    //   270: invokeinterface 251 2 0
    //   275: checkcast 233	com/google/android/gms/tagmanager/zzbw
    //   278: aload 10
    //   280: iconst_1
    //   281: invokeinterface 254 2 0
    //   286: invokevirtual 257	com/google/android/gms/tagmanager/zzbw:zzdo	(Ljava/lang/String;)V
    //   289: aload 10
    //   291: invokeinterface 206 1 0
    //   296: istore 5
    //   298: iload 5
    //   300: ifne +425 -> 725
    //   303: aload 9
    //   305: astore 8
    //   307: aload 10
    //   309: ifnull -283 -> 26
    //   312: aload 10
    //   314: invokeinterface 209 1 0
    //   319: aload 9
    //   321: areturn
    //   322: astore 8
    //   324: aconst_null
    //   325: astore 6
    //   327: aload 8
    //   329: invokevirtual 213	android/database/sqlite/SQLiteException:getMessage	()Ljava/lang/String;
    //   332: invokestatic 216	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   335: astore 8
    //   337: aload 8
    //   339: invokevirtual 219	java/lang/String:length	()I
    //   342: ifeq +36 -> 378
    //   345: ldc -35
    //   347: aload 8
    //   349: invokevirtual 225	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   352: astore 8
    //   354: aload 8
    //   356: invokestatic 162	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   359: aload 7
    //   361: astore 8
    //   363: aload 6
    //   365: ifnull -339 -> 26
    //   368: aload 6
    //   370: invokeinterface 209 1 0
    //   375: aload 7
    //   377: areturn
    //   378: new 40	java/lang/String
    //   381: dup
    //   382: ldc -35
    //   384: invokespecial 227	java/lang/String:<init>	(Ljava/lang/String;)V
    //   387: astore 8
    //   389: goto -35 -> 354
    //   392: astore 8
    //   394: aload 6
    //   396: astore 7
    //   398: aload 8
    //   400: astore 6
    //   402: aload 7
    //   404: ifnull +10 -> 414
    //   407: aload 7
    //   409: invokeinterface 209 1 0
    //   414: aload 6
    //   416: athrow
    //   417: ldc_w 259
    //   420: iconst_1
    //   421: anewarray 4	java/lang/Object
    //   424: dup
    //   425: iconst_0
    //   426: aload 9
    //   428: iload_1
    //   429: invokeinterface 251 2 0
    //   434: checkcast 233	com/google/android/gms/tagmanager/zzbw
    //   437: invokevirtual 263	com/google/android/gms/tagmanager/zzbw:zzom	()J
    //   440: invokestatic 268	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   443: aastore
    //   444: invokestatic 44	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   447: invokestatic 162	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   450: goto -161 -> 289
    //   453: astore 8
    //   455: aload 10
    //   457: astore 6
    //   459: aload 6
    //   461: astore 7
    //   463: aload 8
    //   465: invokevirtual 213	android/database/sqlite/SQLiteException:getMessage	()Ljava/lang/String;
    //   468: invokestatic 216	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   471: astore 8
    //   473: aload 6
    //   475: astore 7
    //   477: aload 8
    //   479: invokevirtual 219	java/lang/String:length	()I
    //   482: ifeq +159 -> 641
    //   485: aload 6
    //   487: astore 7
    //   489: ldc_w 270
    //   492: aload 8
    //   494: invokevirtual 225	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   497: astore 8
    //   499: aload 6
    //   501: astore 7
    //   503: aload 8
    //   505: invokestatic 162	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   508: aload 6
    //   510: astore 7
    //   512: new 166	java/util/ArrayList
    //   515: dup
    //   516: invokespecial 167	java/util/ArrayList:<init>	()V
    //   519: astore 8
    //   521: iconst_0
    //   522: istore_1
    //   523: aload 6
    //   525: astore 7
    //   527: aload 9
    //   529: checkcast 166	java/util/ArrayList
    //   532: astore 9
    //   534: aload 6
    //   536: astore 7
    //   538: aload 9
    //   540: invokevirtual 273	java/util/ArrayList:size	()I
    //   543: istore 4
    //   545: iconst_0
    //   546: istore_2
    //   547: iload_2
    //   548: iload 4
    //   550: if_icmpge +110 -> 660
    //   553: aload 6
    //   555: astore 7
    //   557: aload 9
    //   559: iload_2
    //   560: invokevirtual 274	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   563: astore 10
    //   565: iload_2
    //   566: iconst_1
    //   567: iadd
    //   568: istore_3
    //   569: aload 6
    //   571: astore 7
    //   573: aload 10
    //   575: checkcast 233	com/google/android/gms/tagmanager/zzbw
    //   578: astore 10
    //   580: iload_1
    //   581: istore_2
    //   582: aload 6
    //   584: astore 7
    //   586: aload 10
    //   588: invokevirtual 277	com/google/android/gms/tagmanager/zzbw:zzoo	()Ljava/lang/String;
    //   591: invokestatic 281	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   594: ifeq +9 -> 603
    //   597: iload_1
    //   598: ifne +62 -> 660
    //   601: iconst_1
    //   602: istore_2
    //   603: aload 6
    //   605: astore 7
    //   607: aload 8
    //   609: aload 10
    //   611: invokeinterface 203 2 0
    //   616: pop
    //   617: iload_2
    //   618: istore_1
    //   619: iload_3
    //   620: istore_2
    //   621: goto -74 -> 547
    //   624: astore 6
    //   626: aload 7
    //   628: ifnull +10 -> 638
    //   631: aload 7
    //   633: invokeinterface 209 1 0
    //   638: aload 6
    //   640: athrow
    //   641: aload 6
    //   643: astore 7
    //   645: new 40	java/lang/String
    //   648: dup
    //   649: ldc_w 270
    //   652: invokespecial 227	java/lang/String:<init>	(Ljava/lang/String;)V
    //   655: astore 8
    //   657: goto -158 -> 499
    //   660: aload 6
    //   662: ifnull +10 -> 672
    //   665: aload 6
    //   667: invokeinterface 209 1 0
    //   672: aload 8
    //   674: areturn
    //   675: astore 6
    //   677: aload 10
    //   679: astore 7
    //   681: goto -55 -> 626
    //   684: astore 8
    //   686: goto -227 -> 459
    //   689: astore 6
    //   691: aload 8
    //   693: astore 7
    //   695: goto -293 -> 402
    //   698: astore 8
    //   700: aload 6
    //   702: astore 7
    //   704: aload 8
    //   706: astore 6
    //   708: goto -306 -> 402
    //   711: astore 8
    //   713: goto -386 -> 327
    //   716: astore 8
    //   718: aload 9
    //   720: astore 7
    //   722: goto -395 -> 327
    //   725: iload_1
    //   726: iconst_1
    //   727: iadd
    //   728: istore_1
    //   729: goto -476 -> 253
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	732	0	this	zzeb
    //   0	732	1	paramInt	int
    //   546	75	2	i	int
    //   568	52	3	j	int
    //   543	8	4	k	int
    //   156	143	5	bool	boolean
    //   46	558	6	localObject1	Object
    //   624	42	6	localObject2	Object
    //   675	1	6	localObject3	Object
    //   689	12	6	localObject4	Object
    //   706	1	6	localObject5	Object
    //   7	714	7	localObject6	Object
    //   24	282	8	localObject7	Object
    //   322	6	8	localSQLiteException1	SQLiteException
    //   335	53	8	localObject8	Object
    //   392	7	8	localObject9	Object
    //   453	11	8	localSQLiteException2	SQLiteException
    //   471	202	8	localObject10	Object
    //   684	8	8	localSQLiteException3	SQLiteException
    //   698	7	8	localObject11	Object
    //   711	1	8	localSQLiteException4	SQLiteException
    //   716	1	8	localSQLiteException5	SQLiteException
    //   53	666	9	localObject12	Object
    //   15	663	10	localObject13	Object
    //   204	31	11	str	String
    // Exception table:
    //   from	to	target	type
    //   32	91	322	android/database/sqlite/SQLiteException
    //   327	354	392	finally
    //   354	359	392	finally
    //   378	389	392	finally
    //   241	251	453	android/database/sqlite/SQLiteException
    //   253	289	453	android/database/sqlite/SQLiteException
    //   289	298	453	android/database/sqlite/SQLiteException
    //   417	450	453	android/database/sqlite/SQLiteException
    //   179	195	624	finally
    //   199	206	624	finally
    //   210	241	624	finally
    //   463	473	624	finally
    //   477	485	624	finally
    //   489	499	624	finally
    //   503	508	624	finally
    //   512	521	624	finally
    //   527	534	624	finally
    //   538	545	624	finally
    //   557	565	624	finally
    //   573	580	624	finally
    //   586	597	624	finally
    //   607	617	624	finally
    //   645	657	624	finally
    //   241	251	675	finally
    //   253	289	675	finally
    //   289	298	675	finally
    //   417	450	675	finally
    //   179	195	684	android/database/sqlite/SQLiteException
    //   199	206	684	android/database/sqlite/SQLiteException
    //   210	241	684	android/database/sqlite/SQLiteException
    //   32	91	689	finally
    //   91	100	698	finally
    //   100	110	698	finally
    //   110	158	698	finally
    //   91	100	711	android/database/sqlite/SQLiteException
    //   100	110	716	android/database/sqlite/SQLiteException
    //   110	158	716	android/database/sqlite/SQLiteException
  }
  
  private final SQLiteDatabase zzdh(String paramString)
  {
    try
    {
      SQLiteDatabase localSQLiteDatabase = this.zzbdf.getWritableDatabase();
      return localSQLiteDatabase;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzdi.zzab(paramString);
    }
    return null;
  }
  
  private final void zze(long paramLong)
  {
    zza(new String[] { String.valueOf(paramLong) });
  }
  
  private final void zze(long paramLong1, long paramLong2)
  {
    SQLiteDatabase localSQLiteDatabase = zzdh("Error opening database for getNumStoredHits.");
    if (localSQLiteDatabase == null) {
      return;
    }
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("hit_first_send_time", Long.valueOf(paramLong2));
    try
    {
      localSQLiteDatabase.update("gtm_hits", localContentValues, "hit_id=?", new String[] { String.valueOf(paramLong1) });
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzdi.zzab(69 + "Error setting HIT_FIRST_DISPATCH_TIME for hitId: " + paramLong1);
      zze(paramLong1);
    }
  }
  
  private final int zzoz()
  {
    Object localObject3 = null;
    Object localObject1 = null;
    int i = 0;
    int j = 0;
    Object localObject5 = zzdh("Error opening database for getNumStoredHits.");
    if (localObject5 == null) {}
    for (;;)
    {
      return j;
      try
      {
        localObject5 = ((SQLiteDatabase)localObject5).rawQuery("SELECT COUNT(*) from gtm_hits", null);
        localObject1 = localObject5;
        localObject3 = localObject5;
        if (((Cursor)localObject5).moveToFirst())
        {
          localObject1 = localObject5;
          localObject3 = localObject5;
          long l = ((Cursor)localObject5).getLong(0);
          i = (int)l;
        }
        j = i;
        return i;
      }
      catch (SQLiteException localSQLiteException)
      {
        localObject4 = localObject1;
        zzdi.zzab("Error getting numStoredHits");
        return 0;
      }
      finally
      {
        Object localObject4;
        if (localObject4 != null) {
          ((Cursor)localObject4).close();
        }
      }
    }
  }
  
  /* Error */
  private final int zzpa()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: ldc_w 294
    //   7: invokespecial 120	com/google/android/gms/tagmanager/zzeb:zzdh	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    //   10: astore_3
    //   11: aload_3
    //   12: ifnonnull +5 -> 17
    //   15: iconst_0
    //   16: ireturn
    //   17: aload_3
    //   18: ldc 30
    //   20: iconst_2
    //   21: anewarray 40	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: ldc 32
    //   28: aastore
    //   29: dup
    //   30: iconst_1
    //   31: ldc 38
    //   33: aastore
    //   34: ldc_w 334
    //   37: aconst_null
    //   38: aconst_null
    //   39: aconst_null
    //   40: aconst_null
    //   41: invokevirtual 337	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   44: astore_3
    //   45: aload_3
    //   46: invokeinterface 340 1 0
    //   51: istore_2
    //   52: iload_2
    //   53: istore_1
    //   54: aload_3
    //   55: ifnull +11 -> 66
    //   58: aload_3
    //   59: invokeinterface 209 1 0
    //   64: iload_2
    //   65: istore_1
    //   66: iload_1
    //   67: ireturn
    //   68: astore_3
    //   69: aconst_null
    //   70: astore_3
    //   71: ldc_w 342
    //   74: invokestatic 162	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   77: aload_3
    //   78: ifnull +56 -> 134
    //   81: aload_3
    //   82: invokeinterface 209 1 0
    //   87: iconst_0
    //   88: istore_1
    //   89: goto -23 -> 66
    //   92: astore_3
    //   93: aload 4
    //   95: ifnull +10 -> 105
    //   98: aload 4
    //   100: invokeinterface 209 1 0
    //   105: aload_3
    //   106: athrow
    //   107: astore 5
    //   109: aload_3
    //   110: astore 4
    //   112: aload 5
    //   114: astore_3
    //   115: goto -22 -> 93
    //   118: astore 5
    //   120: aload_3
    //   121: astore 4
    //   123: aload 5
    //   125: astore_3
    //   126: goto -33 -> 93
    //   129: astore 4
    //   131: goto -60 -> 71
    //   134: iconst_0
    //   135: istore_1
    //   136: goto -70 -> 66
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	139	0	this	zzeb
    //   53	83	1	i	int
    //   51	14	2	j	int
    //   10	49	3	localObject1	Object
    //   68	1	3	localSQLiteException1	SQLiteException
    //   70	12	3	localObject2	Object
    //   92	18	3	localObject3	Object
    //   114	12	3	localObject4	Object
    //   1	121	4	localObject5	Object
    //   129	1	4	localSQLiteException2	SQLiteException
    //   107	6	5	localObject6	Object
    //   118	6	5	localObject7	Object
    // Exception table:
    //   from	to	target	type
    //   17	45	68	android/database/sqlite/SQLiteException
    //   17	45	92	finally
    //   45	52	107	finally
    //   71	77	118	finally
    //   45	52	129	android/database/sqlite/SQLiteException
  }
  
  public final void dispatch()
  {
    zzdi.v("GTM Dispatch running...");
    if (!this.zzbdg.zzod()) {}
    do
    {
      return;
      List localList = zzab(40);
      if (localList.isEmpty())
      {
        zzdi.v("...nothing to dispatch");
        this.zzbdh.zzq(true);
        return;
      }
      this.zzbdg.zze(localList);
    } while (zzpa() <= 0);
    zzfn.zzpu().dispatch();
  }
  
  public final void zzb(long paramLong, String paramString)
  {
    boolean bool = true;
    long l = this.zzrz.currentTimeMillis();
    Object localObject;
    if (l > this.zzbdj + 86400000L)
    {
      this.zzbdj = l;
      localObject = zzdh("Error opening database for deleteStaleHits.");
      if (localObject != null)
      {
        ((SQLiteDatabase)localObject).delete("gtm_hits", "HIT_TIME < ?", new String[] { Long.toString(this.zzrz.currentTimeMillis() - 2592000000L) });
        localObject = this.zzbdh;
        if (zzoz() != 0) {
          break label264;
        }
      }
    }
    for (;;)
    {
      ((zzcc)localObject).zzq(bool);
      int i = zzoz() - this.zzbdk + 1;
      if (i > 0)
      {
        localObject = zzaa(i);
        i = ((List)localObject).size();
        zzdi.v(51 + "Store full, deleting " + i + " hits to make room.");
        zza((String[])((List)localObject).toArray(new String[0]));
      }
      localObject = zzdh("Error opening database for putHit");
      ContentValues localContentValues;
      if (localObject != null)
      {
        localContentValues = new ContentValues();
        localContentValues.put("hit_time", Long.valueOf(paramLong));
        localContentValues.put("hit_url", paramString);
        localContentValues.put("hit_first_send_time", Integer.valueOf(0));
      }
      try
      {
        ((SQLiteDatabase)localObject).insert("gtm_hits", null, localContentValues);
        this.zzbdh.zzq(false);
        return;
      }
      catch (SQLiteException paramString)
      {
        label264:
        zzdi.zzab("Error storing hit");
      }
      bool = false;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzeb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */