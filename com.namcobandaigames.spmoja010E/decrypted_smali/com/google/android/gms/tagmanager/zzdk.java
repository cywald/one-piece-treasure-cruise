package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzdk
  extends zzbq
{
  private static final String ID = zza.zzar.toString();
  private static final String zzbbt = zzb.zzef.toString();
  
  public zzdk()
  {
    super(ID, new String[] { zzbbt });
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    return zzgj.zzj(zzgj.zzc((zzp)paramMap.get(zzbbt)).toLowerCase());
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */