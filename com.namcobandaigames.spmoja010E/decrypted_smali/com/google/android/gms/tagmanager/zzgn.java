package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzp;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

final class zzgn
{
  private static zzdz<zzp> zza(zzdz<zzp> paramzzdz)
  {
    try
    {
      zzdz localzzdz = new zzdz(zzgj.zzj(zzee(zzgj.zzc((zzp)paramzzdz.getObject()))), paramzzdz.zzoy());
      return localzzdz;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      zzdi.zza("Escape URI: unsupported encoding", localUnsupportedEncodingException);
    }
    return paramzzdz;
  }
  
  static zzdz<zzp> zza(zzdz<zzp> paramzzdz, int... paramVarArgs)
  {
    int j = paramVarArgs.length;
    int i = 0;
    if (i < j)
    {
      int k = paramVarArgs[i];
      if (!(zzgj.zzh((zzp)paramzzdz.getObject()) instanceof String)) {
        zzdi.e("Escaping can only be applied to strings.");
      }
      for (;;)
      {
        i += 1;
        break;
        switch (k)
        {
        default: 
          zzdi.e(39 + "Unsupported Value Escaping: " + k);
          break;
        case 12: 
          paramzzdz = zza(paramzzdz);
        }
      }
    }
    return paramzzdz;
  }
  
  static String zzee(String paramString)
    throws UnsupportedEncodingException
  {
    return URLEncoder.encode(paramString, "UTF-8").replaceAll("\\+", "%20");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzgn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */