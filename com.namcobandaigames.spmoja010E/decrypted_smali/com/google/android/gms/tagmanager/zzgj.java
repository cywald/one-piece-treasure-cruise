package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzp;
import com.google.android.gms.internal.measurement.zzzg;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@VisibleForTesting
public final class zzgj
{
  private static final Object zzbgo = null;
  private static Long zzbgp = new Long(0L);
  private static Double zzbgq = new Double(0.0D);
  private static zzgi zzbgr = zzgi.zzar(0L);
  private static String zzbgs = new String("");
  private static Boolean zzbgt = new Boolean(false);
  private static List<Object> zzbgu = new ArrayList(0);
  private static Map<Object, Object> zzbgv = new HashMap();
  private static zzp zzbgw = zzj(zzbgs);
  
  private static double getDouble(Object paramObject)
  {
    if ((paramObject instanceof Number)) {
      return ((Number)paramObject).doubleValue();
    }
    zzdi.e("getDouble received non-Number");
    return 0.0D;
  }
  
  public static String zzc(zzp paramzzp)
  {
    return zzi(zzh(paramzzp));
  }
  
  public static zzgi zzd(zzp paramzzp)
  {
    paramzzp = zzh(paramzzp);
    if ((paramzzp instanceof zzgi)) {
      return (zzgi)paramzzp;
    }
    if (zzl(paramzzp)) {
      return zzgi.zzar(zzm(paramzzp));
    }
    if (zzk(paramzzp)) {
      return zzgi.zza(Double.valueOf(getDouble(paramzzp)));
    }
    return zzec(zzi(paramzzp));
  }
  
  public static Long zze(zzp paramzzp)
  {
    paramzzp = zzh(paramzzp);
    if (zzl(paramzzp)) {
      return Long.valueOf(zzm(paramzzp));
    }
    paramzzp = zzec(zzi(paramzzp));
    if (paramzzp == zzbgr) {
      return zzbgp;
    }
    return Long.valueOf(paramzzp.longValue());
  }
  
  public static zzp zzeb(String paramString)
  {
    zzp localzzp = new zzp();
    localzzp.type = 5;
    localzzp.zzqn = paramString;
    return localzzp;
  }
  
  private static zzgi zzec(String paramString)
  {
    try
    {
      zzgi localzzgi = zzgi.zzea(paramString);
      return localzzgi;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      zzdi.e(String.valueOf(paramString).length() + 33 + "Failed to convert '" + paramString + "' to a number.");
    }
    return zzbgr;
  }
  
  public static Double zzf(zzp paramzzp)
  {
    paramzzp = zzh(paramzzp);
    if (zzk(paramzzp)) {
      return Double.valueOf(getDouble(paramzzp));
    }
    paramzzp = zzec(zzi(paramzzp));
    if (paramzzp == zzbgr) {
      return zzbgq;
    }
    return Double.valueOf(paramzzp.doubleValue());
  }
  
  public static Boolean zzg(zzp paramzzp)
  {
    paramzzp = zzh(paramzzp);
    if ((paramzzp instanceof Boolean)) {
      return (Boolean)paramzzp;
    }
    paramzzp = zzi(paramzzp);
    if ("true".equalsIgnoreCase(paramzzp)) {
      return Boolean.TRUE;
    }
    if ("false".equalsIgnoreCase(paramzzp)) {
      return Boolean.FALSE;
    }
    return zzbgt;
  }
  
  public static Object zzh(zzp paramzzp)
  {
    int k = 0;
    int j = 0;
    int i = 0;
    if (paramzzp == null) {
      return null;
    }
    Object localObject1;
    Object localObject2;
    switch (paramzzp.type)
    {
    default: 
      i = paramzzp.type;
      zzdi.e(46 + "Failed to convert a value of type: " + i);
      return null;
    case 1: 
      return paramzzp.string;
    case 2: 
      localObject1 = new ArrayList(paramzzp.zzqj.length);
      paramzzp = paramzzp.zzqj;
      j = paramzzp.length;
      while (i < j)
      {
        localObject2 = zzh(paramzzp[i]);
        if (localObject2 == null) {
          return null;
        }
        ((ArrayList)localObject1).add(localObject2);
        i += 1;
      }
      return localObject1;
    case 3: 
      if (paramzzp.zzqk.length != paramzzp.zzql.length)
      {
        paramzzp = String.valueOf(paramzzp.toString());
        if (paramzzp.length() != 0) {}
        for (paramzzp = "Converting an invalid value to object: ".concat(paramzzp);; paramzzp = new String("Converting an invalid value to object: "))
        {
          zzdi.e(paramzzp);
          return null;
        }
      }
      localObject1 = new HashMap(paramzzp.zzql.length);
      i = k;
      while (i < paramzzp.zzqk.length)
      {
        localObject2 = zzh(paramzzp.zzqk[i]);
        Object localObject3 = zzh(paramzzp.zzql[i]);
        if ((localObject2 == null) || (localObject3 == null)) {
          return null;
        }
        ((Map)localObject1).put(localObject2, localObject3);
        i += 1;
      }
      return localObject1;
    case 4: 
      zzdi.e("Trying to convert a macro reference to object");
      return null;
    case 5: 
      zzdi.e("Trying to convert a function id to object");
      return null;
    case 6: 
      return Long.valueOf(paramzzp.zzqo);
    case 7: 
      localObject1 = new StringBuilder();
      paramzzp = paramzzp.zzqq;
      k = paramzzp.length;
      i = j;
      while (i < k)
      {
        localObject2 = zzi(zzh(paramzzp[i]));
        if (localObject2 == zzbgs) {
          return null;
        }
        ((StringBuilder)localObject1).append((String)localObject2);
        i += 1;
      }
      return ((StringBuilder)localObject1).toString();
    }
    return Boolean.valueOf(paramzzp.zzqp);
  }
  
  private static String zzi(Object paramObject)
  {
    if (paramObject == null) {
      return zzbgs;
    }
    return paramObject.toString();
  }
  
  public static zzp zzj(Object paramObject)
  {
    boolean bool = false;
    zzp localzzp1 = new zzp();
    if ((paramObject instanceof zzp)) {
      return (zzp)paramObject;
    }
    if ((paramObject instanceof String))
    {
      localzzp1.type = 1;
      localzzp1.string = ((String)paramObject);
    }
    for (;;)
    {
      localzzp1.zzqs = bool;
      return localzzp1;
      Object localObject1;
      Object localObject2;
      if ((paramObject instanceof List))
      {
        localzzp1.type = 2;
        localObject1 = (List)paramObject;
        paramObject = new ArrayList(((List)localObject1).size());
        localObject1 = ((List)localObject1).iterator();
        bool = false;
        if (((Iterator)localObject1).hasNext())
        {
          localObject2 = zzj(((Iterator)localObject1).next());
          if (localObject2 == zzbgw) {
            return zzbgw;
          }
          if ((bool) || (((zzp)localObject2).zzqs)) {}
          for (bool = true;; bool = false)
          {
            ((List)paramObject).add(localObject2);
            break;
          }
        }
        localzzp1.zzqj = ((zzp[])((List)paramObject).toArray(new zzp[0]));
      }
      else if ((paramObject instanceof Map))
      {
        localzzp1.type = 3;
        localObject2 = ((Map)paramObject).entrySet();
        paramObject = new ArrayList(((Set)localObject2).size());
        localObject1 = new ArrayList(((Set)localObject2).size());
        localObject2 = ((Set)localObject2).iterator();
        bool = false;
        if (((Iterator)localObject2).hasNext())
        {
          Object localObject3 = (Map.Entry)((Iterator)localObject2).next();
          zzp localzzp2 = zzj(((Map.Entry)localObject3).getKey());
          localObject3 = zzj(((Map.Entry)localObject3).getValue());
          if ((localzzp2 == zzbgw) || (localObject3 == zzbgw)) {
            return zzbgw;
          }
          if ((bool) || (localzzp2.zzqs) || (((zzp)localObject3).zzqs)) {}
          for (bool = true;; bool = false)
          {
            ((List)paramObject).add(localzzp2);
            ((List)localObject1).add(localObject3);
            break;
          }
        }
        localzzp1.zzqk = ((zzp[])((List)paramObject).toArray(new zzp[0]));
        localzzp1.zzql = ((zzp[])((List)localObject1).toArray(new zzp[0]));
      }
      else if (zzk(paramObject))
      {
        localzzp1.type = 1;
        localzzp1.string = paramObject.toString();
      }
      else if (zzl(paramObject))
      {
        localzzp1.type = 6;
        localzzp1.zzqo = zzm(paramObject);
      }
      else
      {
        if (!(paramObject instanceof Boolean)) {
          break;
        }
        localzzp1.type = 8;
        localzzp1.zzqp = ((Boolean)paramObject).booleanValue();
      }
    }
    if (paramObject == null)
    {
      paramObject = "null";
      paramObject = String.valueOf(paramObject);
      if (((String)paramObject).length() == 0) {
        break label508;
      }
    }
    label508:
    for (paramObject = "Converting to Value from unknown object type: ".concat((String)paramObject);; paramObject = new String("Converting to Value from unknown object type: "))
    {
      zzdi.e((String)paramObject);
      return zzbgw;
      paramObject = paramObject.getClass().toString();
      break;
    }
  }
  
  private static boolean zzk(Object paramObject)
  {
    return ((paramObject instanceof Double)) || ((paramObject instanceof Float)) || (((paramObject instanceof zzgi)) && (((zzgi)paramObject).zzpy()));
  }
  
  private static boolean zzl(Object paramObject)
  {
    return ((paramObject instanceof Byte)) || ((paramObject instanceof Short)) || ((paramObject instanceof Integer)) || ((paramObject instanceof Long)) || (((paramObject instanceof zzgi)) && (((zzgi)paramObject).zzpz()));
  }
  
  private static long zzm(Object paramObject)
  {
    if ((paramObject instanceof Number)) {
      return ((Number)paramObject).longValue();
    }
    zzdi.e("getInt64 received non-Number");
    return 0L;
  }
  
  public static Object zzqa()
  {
    return null;
  }
  
  public static Long zzqb()
  {
    return zzbgp;
  }
  
  public static Double zzqc()
  {
    return zzbgq;
  }
  
  public static Boolean zzqd()
  {
    return zzbgt;
  }
  
  public static zzgi zzqe()
  {
    return zzbgr;
  }
  
  public static String zzqf()
  {
    return zzbgs;
  }
  
  public static zzp zzqg()
  {
    return zzbgw;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzgj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */