package com.google.android.gms.tagmanager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

@VisibleForTesting
final class zzed
  extends SQLiteOpenHelper
{
  private boolean zzbdm;
  private long zzbdn = 0L;
  
  zzed(zzeb paramzzeb, Context paramContext, String paramString)
  {
    super(paramContext, paramString, null, 1);
  }
  
  /* Error */
  private static boolean zza(String paramString, SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_1
    //   3: ldc 27
    //   5: iconst_1
    //   6: anewarray 29	java/lang/String
    //   9: dup
    //   10: iconst_0
    //   11: ldc 31
    //   13: aastore
    //   14: ldc 33
    //   16: iconst_1
    //   17: anewarray 29	java/lang/String
    //   20: dup
    //   21: iconst_0
    //   22: aload_0
    //   23: aastore
    //   24: aconst_null
    //   25: aconst_null
    //   26: aconst_null
    //   27: invokevirtual 39	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   30: astore_1
    //   31: aload_1
    //   32: invokeinterface 45 1 0
    //   37: istore_2
    //   38: aload_1
    //   39: ifnull +9 -> 48
    //   42: aload_1
    //   43: invokeinterface 49 1 0
    //   48: iload_2
    //   49: ireturn
    //   50: astore_1
    //   51: aconst_null
    //   52: astore_1
    //   53: aload_0
    //   54: invokestatic 53	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   57: astore_0
    //   58: aload_0
    //   59: invokevirtual 57	java/lang/String:length	()I
    //   62: ifeq +26 -> 88
    //   65: ldc 59
    //   67: aload_0
    //   68: invokevirtual 63	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   71: astore_0
    //   72: aload_0
    //   73: invokestatic 69	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   76: aload_1
    //   77: ifnull +9 -> 86
    //   80: aload_1
    //   81: invokeinterface 49 1 0
    //   86: iconst_0
    //   87: ireturn
    //   88: new 29	java/lang/String
    //   91: dup
    //   92: ldc 59
    //   94: invokespecial 71	java/lang/String:<init>	(Ljava/lang/String;)V
    //   97: astore_0
    //   98: goto -26 -> 72
    //   101: astore_0
    //   102: aload_1
    //   103: ifnull +9 -> 112
    //   106: aload_1
    //   107: invokeinterface 49 1 0
    //   112: aload_0
    //   113: athrow
    //   114: astore_0
    //   115: aload_3
    //   116: astore_1
    //   117: goto -15 -> 102
    //   120: astore_0
    //   121: goto -19 -> 102
    //   124: astore_3
    //   125: goto -72 -> 53
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	128	0	paramString	String
    //   0	128	1	paramSQLiteDatabase	SQLiteDatabase
    //   37	12	2	bool	boolean
    //   1	115	3	localObject	Object
    //   124	1	3	localSQLiteException	SQLiteException
    // Exception table:
    //   from	to	target	type
    //   2	31	50	android/database/sqlite/SQLiteException
    //   53	72	101	finally
    //   72	76	101	finally
    //   88	98	101	finally
    //   2	31	114	finally
    //   31	38	120	finally
    //   31	38	124	android/database/sqlite/SQLiteException
  }
  
  public final SQLiteDatabase getWritableDatabase()
  {
    if ((this.zzbdm) && (this.zzbdn + 3600000L > zzeb.zza(this.zzbdl).currentTimeMillis())) {
      throw new SQLiteException("Database creation failed");
    }
    Object localObject1 = null;
    this.zzbdm = true;
    this.zzbdn = zzeb.zza(this.zzbdl).currentTimeMillis();
    try
    {
      localObject2 = super.getWritableDatabase();
      localObject1 = localObject2;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        Object localObject2;
        zzeb.zzc(this.zzbdl).getDatabasePath(zzeb.zzb(this.zzbdl)).delete();
      }
    }
    localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = super.getWritableDatabase();
    }
    this.zzbdm = false;
    return (SQLiteDatabase)localObject2;
  }
  
  public final void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    zzbr.zzdn(paramSQLiteDatabase.getPath());
  }
  
  public final void onDowngrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2) {}
  
  public final void onOpen(SQLiteDatabase paramSQLiteDatabase)
  {
    Object localObject1;
    if (Build.VERSION.SDK_INT < 15) {
      localObject1 = paramSQLiteDatabase.rawQuery("PRAGMA journal_mode=memory", null);
    }
    do
    {
      try
      {
        ((Cursor)localObject1).moveToFirst();
        ((Cursor)localObject1).close();
        if (!zza("gtm_hits", paramSQLiteDatabase))
        {
          paramSQLiteDatabase.execSQL(zzeb.zzpb());
          return;
        }
      }
      finally
      {
        ((Cursor)localObject1).close();
      }
      paramSQLiteDatabase = paramSQLiteDatabase.rawQuery("SELECT * FROM gtm_hits WHERE 0", null);
      localObject1 = new HashSet();
      try
      {
        String[] arrayOfString = paramSQLiteDatabase.getColumnNames();
        int i = 0;
        while (i < arrayOfString.length)
        {
          ((Set)localObject1).add(arrayOfString[i]);
          i += 1;
        }
        paramSQLiteDatabase.close();
        if ((!((Set)localObject1).remove("hit_id")) || (!((Set)localObject1).remove("hit_url")) || (!((Set)localObject1).remove("hit_time")) || (!((Set)localObject1).remove("hit_first_send_time"))) {
          throw new SQLiteException("Database column missing");
        }
      }
      finally
      {
        paramSQLiteDatabase.close();
      }
    } while (((Set)localObject2).isEmpty());
    throw new SQLiteException("Database has extra columns");
  }
  
  public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */