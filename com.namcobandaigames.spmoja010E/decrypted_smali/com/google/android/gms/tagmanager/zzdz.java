package com.google.android.gms.tagmanager;

final class zzdz<T>
{
  private final T object;
  private final boolean zzbde;
  
  zzdz(T paramT, boolean paramBoolean)
  {
    this.object = paramT;
    this.zzbde = paramBoolean;
  }
  
  public final T getObject()
  {
    return (T)this.object;
  }
  
  public final boolean zzoy()
  {
    return this.zzbde;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */