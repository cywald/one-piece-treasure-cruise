package com.google.android.gms.tagmanager;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.measurement.zzo;

final class zzae
  implements zzdh<zzo>
{
  private zzae(zzy paramzzy) {}
  
  public final void zzno() {}
  
  public final void zzt(int paramInt)
  {
    if (paramInt == zzcz.zzbcx) {
      zzy.zzc(this.zzbai).zzns();
    }
    synchronized (this.zzbai)
    {
      if (!this.zzbai.isReady())
      {
        if (zzy.zzb(this.zzbai) != null) {
          this.zzbai.setResult(zzy.zzb(this.zzbai));
        }
      }
      else
      {
        long l = zzy.zzc(this.zzbai).zznr();
        zzy.zza(this.zzbai, l);
        return;
      }
      this.zzbai.setResult(this.zzbai.a_(Status.RESULT_TIMEOUT));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */