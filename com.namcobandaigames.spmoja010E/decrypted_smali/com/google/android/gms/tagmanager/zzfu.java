package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

final class zzfu
  implements zzbe
{
  private final String zzabl;
  private final zzfx zzbgb;
  private final zzfw zzbgc;
  private final Context zzri;
  
  @VisibleForTesting
  zzfu(Context paramContext, zzfw paramzzfw)
  {
    this(new zzfv(), paramContext, paramzzfw);
  }
  
  @VisibleForTesting
  private zzfu(zzfx paramzzfx, Context paramContext, zzfw paramzzfw)
  {
    this.zzbgb = paramzzfx;
    this.zzri = paramContext.getApplicationContext();
    this.zzbgc = paramzzfw;
    paramContext = Build.VERSION.RELEASE;
    paramzzfw = Locale.getDefault();
    if (paramzzfw == null) {
      paramzzfx = (zzfx)localObject;
    }
    for (;;)
    {
      this.zzabl = String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", new Object[] { "GoogleTagManager", "4.00", paramContext, paramzzfx, Build.MODEL, Build.ID });
      return;
      paramzzfx = (zzfx)localObject;
      if (paramzzfw.getLanguage() != null)
      {
        paramzzfx = (zzfx)localObject;
        if (paramzzfw.getLanguage().length() != 0)
        {
          paramzzfx = new StringBuilder();
          paramzzfx.append(paramzzfw.getLanguage().toLowerCase());
          if ((paramzzfw.getCountry() != null) && (paramzzfw.getCountry().length() != 0)) {
            paramzzfx.append("-").append(paramzzfw.getCountry().toLowerCase());
          }
          paramzzfx = paramzzfx.toString();
        }
      }
    }
  }
  
  @VisibleForTesting
  private static URL zzd(zzbw paramzzbw)
  {
    paramzzbw = paramzzbw.zzoo();
    try
    {
      paramzzbw = new URL(paramzzbw);
      return paramzzbw;
    }
    catch (MalformedURLException paramzzbw)
    {
      zzdi.e("Error trying to parse the GTM url.");
    }
    return null;
  }
  
  public final void zze(List<zzbw> paramList)
  {
    int n = Math.min(paramList.size(), 40);
    j = 1;
    int m = 0;
    zzbw localzzbw;
    Object localObject1;
    if (m < n)
    {
      localzzbw = (zzbw)paramList.get(m);
      localObject1 = zzd(localzzbw);
      if (localObject1 == null)
      {
        zzdi.zzab("No destination: discarding hit.");
        this.zzbgc.zzb(localzzbw);
        i = j;
      }
    }
    for (;;)
    {
      m += 1;
      j = i;
      break;
      int k = j;
      try
      {
        localHttpURLConnection = this.zzbgb.zzc((URL)localObject1);
        i = j;
        if (j == 0) {}
      }
      catch (IOException localIOException2)
      {
        for (;;)
        {
          HttpURLConnection localHttpURLConnection;
          label185:
          label225:
          String str;
          i = k;
        }
      }
      try
      {
        zzdn.zzv(this.zzri);
        i = 0;
        j = i;
        localHttpURLConnection.setRequestProperty("User-Agent", this.zzabl);
        j = i;
        k = localHttpURLConnection.getResponseCode();
        j = i;
        localObject1 = localHttpURLConnection.getInputStream();
        if (k != 200) {}
        try
        {
          zzdi.zzab(25 + "Bad response: " + k);
          this.zzbgc.zzc(localzzbw);
          if (localObject1 != null)
          {
            k = i;
            ((InputStream)localObject1).close();
          }
          k = i;
          localHttpURLConnection.disconnect();
          continue;
        }
        finally {}
        this.zzbgc.zza(localzzbw);
        break label185;
      }
      finally
      {
        Object localObject2 = null;
        i = j;
        break label225;
      }
    }
    if (localObject1 != null) {}
    try
    {
      ((InputStream)localObject1).close();
      localHttpURLConnection.disconnect();
      throw ((Throwable)localObject3);
    }
    catch (IOException localIOException1) {}
    str = String.valueOf(localIOException1.getClass().getSimpleName());
    if (str.length() != 0) {}
    for (str = "Exception sending hit: ".concat(str);; str = new String("Exception sending hit: "))
    {
      zzdi.zzab(str);
      zzdi.zzab(localIOException1.getMessage());
      this.zzbgc.zzc(localzzbw);
      break;
    }
  }
  
  public final boolean zzod()
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)this.zzri.getSystemService("connectivity")).getActiveNetworkInfo();
    if ((localNetworkInfo == null) || (!localNetworkInfo.isConnected()))
    {
      zzdi.v("...no network connectivity");
      return false;
    }
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */