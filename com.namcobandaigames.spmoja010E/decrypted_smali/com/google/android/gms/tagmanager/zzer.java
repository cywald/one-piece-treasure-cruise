package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzo;
import com.google.android.gms.internal.measurement.zzse;

final class zzer
  implements Runnable
{
  private final String zzazf;
  private volatile String zzbaf;
  private final zzse zzbee;
  private final String zzbef;
  private zzdh<zzo> zzbeg;
  private volatile zzal zzbeh;
  private volatile String zzbei;
  private final Context zzri;
  
  @VisibleForTesting
  private zzer(Context paramContext, String paramString, zzse paramzzse, zzal paramzzal)
  {
    this.zzri = paramContext;
    this.zzbee = paramzzse;
    this.zzazf = paramString;
    this.zzbeh = paramzzal;
    paramContext = String.valueOf("/r?id=");
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {}
    for (paramContext = paramContext.concat(paramString);; paramContext = new String(paramContext))
    {
      this.zzbef = paramContext;
      this.zzbaf = this.zzbef;
      this.zzbei = null;
      return;
    }
  }
  
  public zzer(Context paramContext, String paramString, zzal paramzzal)
  {
    this(paramContext, paramString, new zzse(), paramzzal);
  }
  
  /* Error */
  public final void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 76	com/google/android/gms/tagmanager/zzer:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   4: ifnonnull +13 -> 17
    //   7: new 78	java/lang/IllegalStateException
    //   10: dup
    //   11: ldc 80
    //   13: invokespecial 81	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   16: athrow
    //   17: aload_0
    //   18: getfield 76	com/google/android/gms/tagmanager/zzer:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   21: invokeinterface 86 1 0
    //   26: aload_0
    //   27: getfield 28	com/google/android/gms/tagmanager/zzer:zzri	Landroid/content/Context;
    //   30: ldc 88
    //   32: invokevirtual 94	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   35: checkcast 96	android/net/ConnectivityManager
    //   38: invokevirtual 100	android/net/ConnectivityManager:getActiveNetworkInfo	()Landroid/net/NetworkInfo;
    //   41: astore_2
    //   42: aload_2
    //   43: ifnull +10 -> 53
    //   46: aload_2
    //   47: invokevirtual 106	android/net/NetworkInfo:isConnected	()Z
    //   50: ifne +27 -> 77
    //   53: ldc 108
    //   55: invokestatic 113	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   58: iconst_0
    //   59: istore_1
    //   60: iload_1
    //   61: ifne +21 -> 82
    //   64: aload_0
    //   65: getfield 76	com/google/android/gms/tagmanager/zzer:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   68: getstatic 119	com/google/android/gms/tagmanager/zzcz:zzbcu	I
    //   71: invokeinterface 123 2 0
    //   76: return
    //   77: iconst_1
    //   78: istore_1
    //   79: goto -19 -> 60
    //   82: ldc 125
    //   84: invokestatic 113	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   87: aload_0
    //   88: getfield 34	com/google/android/gms/tagmanager/zzer:zzbeh	Lcom/google/android/gms/tagmanager/zzal;
    //   91: invokevirtual 131	com/google/android/gms/tagmanager/zzal:zznv	()Ljava/lang/String;
    //   94: astore_2
    //   95: aload_0
    //   96: getfield 54	com/google/android/gms/tagmanager/zzer:zzbaf	Ljava/lang/String;
    //   99: astore_3
    //   100: new 133	java/lang/StringBuilder
    //   103: dup
    //   104: aload_2
    //   105: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   108: invokevirtual 46	java/lang/String:length	()I
    //   111: bipush 12
    //   113: iadd
    //   114: aload_3
    //   115: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   118: invokevirtual 46	java/lang/String:length	()I
    //   121: iadd
    //   122: invokespecial 135	java/lang/StringBuilder:<init>	(I)V
    //   125: aload_2
    //   126: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   129: aload_3
    //   130: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: ldc -115
    //   135: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: invokevirtual 144	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   141: astore_3
    //   142: aload_3
    //   143: astore_2
    //   144: aload_0
    //   145: getfield 56	com/google/android/gms/tagmanager/zzer:zzbei	Ljava/lang/String;
    //   148: ifnull +71 -> 219
    //   151: aload_3
    //   152: astore_2
    //   153: aload_0
    //   154: getfield 56	com/google/android/gms/tagmanager/zzer:zzbei	Ljava/lang/String;
    //   157: invokevirtual 147	java/lang/String:trim	()Ljava/lang/String;
    //   160: ldc -107
    //   162: invokevirtual 153	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   165: ifne +54 -> 219
    //   168: aload_3
    //   169: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   172: astore_2
    //   173: aload_0
    //   174: getfield 56	com/google/android/gms/tagmanager/zzer:zzbei	Ljava/lang/String;
    //   177: astore_3
    //   178: new 133	java/lang/StringBuilder
    //   181: dup
    //   182: aload_2
    //   183: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   186: invokevirtual 46	java/lang/String:length	()I
    //   189: iconst_4
    //   190: iadd
    //   191: aload_3
    //   192: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   195: invokevirtual 46	java/lang/String:length	()I
    //   198: iadd
    //   199: invokespecial 135	java/lang/StringBuilder:<init>	(I)V
    //   202: aload_2
    //   203: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: ldc -101
    //   208: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   211: aload_3
    //   212: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   215: invokevirtual 144	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   218: astore_2
    //   219: invokestatic 161	com/google/android/gms/tagmanager/zzeh:zzpc	()Lcom/google/android/gms/tagmanager/zzeh;
    //   222: invokevirtual 165	com/google/android/gms/tagmanager/zzeh:zzpd	()Lcom/google/android/gms/tagmanager/zzeh$zza;
    //   225: getstatic 171	com/google/android/gms/tagmanager/zzeh$zza:zzbdw	Lcom/google/android/gms/tagmanager/zzeh$zza;
    //   228: invokevirtual 172	com/google/android/gms/tagmanager/zzeh$zza:equals	(Ljava/lang/Object;)Z
    //   231: ifeq +515 -> 746
    //   234: aload_2
    //   235: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   238: astore_2
    //   239: ldc -82
    //   241: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   244: astore_3
    //   245: aload_3
    //   246: invokevirtual 46	java/lang/String:length	()I
    //   249: ifeq +168 -> 417
    //   252: aload_2
    //   253: aload_3
    //   254: invokevirtual 50	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   257: astore_2
    //   258: invokestatic 178	com/google/android/gms/internal/measurement/zzse:zzsx	()Lcom/google/android/gms/internal/measurement/zzsd;
    //   261: astore 5
    //   263: aconst_null
    //   264: astore 4
    //   266: aload 5
    //   268: aload_2
    //   269: invokeinterface 184 2 0
    //   274: astore_3
    //   275: new 186	java/io/ByteArrayOutputStream
    //   278: dup
    //   279: invokespecial 187	java/io/ByteArrayOutputStream:<init>	()V
    //   282: astore 4
    //   284: aload_3
    //   285: aload 4
    //   287: invokestatic 193	com/google/android/gms/internal/measurement/zzrs:zza	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   290: aload 4
    //   292: invokevirtual 197	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   295: astore_3
    //   296: new 199	com/google/android/gms/internal/measurement/zzo
    //   299: dup
    //   300: invokespecial 200	com/google/android/gms/internal/measurement/zzo:<init>	()V
    //   303: aload_3
    //   304: invokestatic 205	com/google/android/gms/internal/measurement/zzzg:zza	(Lcom/google/android/gms/internal/measurement/zzzg;[B)Lcom/google/android/gms/internal/measurement/zzzg;
    //   307: checkcast 199	com/google/android/gms/internal/measurement/zzo
    //   310: astore 4
    //   312: aload 4
    //   314: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   317: astore_3
    //   318: new 133	java/lang/StringBuilder
    //   321: dup
    //   322: aload_3
    //   323: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   326: invokevirtual 46	java/lang/String:length	()I
    //   329: bipush 43
    //   331: iadd
    //   332: invokespecial 135	java/lang/StringBuilder:<init>	(I)V
    //   335: ldc -49
    //   337: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: aload_3
    //   341: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   344: invokevirtual 144	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   347: invokestatic 113	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   350: aload 4
    //   352: getfield 211	com/google/android/gms/internal/measurement/zzo:zzqg	Lcom/google/android/gms/internal/measurement/zzl;
    //   355: ifnonnull +38 -> 393
    //   358: aload 4
    //   360: getfield 215	com/google/android/gms/internal/measurement/zzo:zzqf	[Lcom/google/android/gms/internal/measurement/zzn;
    //   363: arraylength
    //   364: ifne +29 -> 393
    //   367: aload_0
    //   368: getfield 32	com/google/android/gms/tagmanager/zzer:zzazf	Ljava/lang/String;
    //   371: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   374: astore_3
    //   375: aload_3
    //   376: invokevirtual 46	java/lang/String:length	()I
    //   379: ifeq +274 -> 653
    //   382: ldc -39
    //   384: aload_3
    //   385: invokevirtual 50	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   388: astore_3
    //   389: aload_3
    //   390: invokestatic 113	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   393: aload_0
    //   394: getfield 76	com/google/android/gms/tagmanager/zzer:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   397: aload 4
    //   399: invokeinterface 221 2 0
    //   404: aload 5
    //   406: invokeinterface 224 1 0
    //   411: ldc -30
    //   413: invokestatic 113	com/google/android/gms/tagmanager/zzdi:v	(Ljava/lang/String;)V
    //   416: return
    //   417: new 38	java/lang/String
    //   420: dup
    //   421: aload_2
    //   422: invokespecial 59	java/lang/String:<init>	(Ljava/lang/String;)V
    //   425: astore_2
    //   426: goto -168 -> 258
    //   429: astore_3
    //   430: aload_0
    //   431: getfield 32	com/google/android/gms/tagmanager/zzer:zzazf	Ljava/lang/String;
    //   434: astore_3
    //   435: new 133	java/lang/StringBuilder
    //   438: dup
    //   439: aload_2
    //   440: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   443: invokevirtual 46	java/lang/String:length	()I
    //   446: bipush 79
    //   448: iadd
    //   449: aload_3
    //   450: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   453: invokevirtual 46	java/lang/String:length	()I
    //   456: iadd
    //   457: invokespecial 135	java/lang/StringBuilder:<init>	(I)V
    //   460: ldc -28
    //   462: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   465: aload_2
    //   466: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   469: ldc -26
    //   471: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   474: aload_3
    //   475: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   478: ldc -24
    //   480: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   483: invokevirtual 144	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   486: invokestatic 235	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   489: aload_0
    //   490: getfield 76	com/google/android/gms/tagmanager/zzer:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   493: getstatic 238	com/google/android/gms/tagmanager/zzcz:zzbcw	I
    //   496: invokeinterface 123 2 0
    //   501: aload 5
    //   503: invokeinterface 224 1 0
    //   508: return
    //   509: astore_3
    //   510: aload_2
    //   511: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   514: astore_3
    //   515: aload_3
    //   516: invokevirtual 46	java/lang/String:length	()I
    //   519: ifeq +42 -> 561
    //   522: ldc -16
    //   524: aload_3
    //   525: invokevirtual 50	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   528: astore_3
    //   529: aload_3
    //   530: invokestatic 235	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   533: aload_0
    //   534: getfield 76	com/google/android/gms/tagmanager/zzer:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   537: getstatic 243	com/google/android/gms/tagmanager/zzcz:zzbcx	I
    //   540: invokeinterface 123 2 0
    //   545: aload 4
    //   547: astore_3
    //   548: goto -273 -> 275
    //   551: astore_2
    //   552: aload 5
    //   554: invokeinterface 224 1 0
    //   559: aload_2
    //   560: athrow
    //   561: new 38	java/lang/String
    //   564: dup
    //   565: ldc -16
    //   567: invokespecial 59	java/lang/String:<init>	(Ljava/lang/String;)V
    //   570: astore_3
    //   571: goto -42 -> 529
    //   574: astore_3
    //   575: aload_3
    //   576: invokevirtual 246	java/io/IOException:getMessage	()Ljava/lang/String;
    //   579: astore 4
    //   581: new 133	java/lang/StringBuilder
    //   584: dup
    //   585: aload_2
    //   586: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   589: invokevirtual 46	java/lang/String:length	()I
    //   592: bipush 40
    //   594: iadd
    //   595: aload 4
    //   597: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   600: invokevirtual 46	java/lang/String:length	()I
    //   603: iadd
    //   604: invokespecial 135	java/lang/StringBuilder:<init>	(I)V
    //   607: ldc -8
    //   609: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   612: aload_2
    //   613: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   616: ldc -6
    //   618: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   621: aload 4
    //   623: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   626: invokevirtual 144	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   629: aload_3
    //   630: invokestatic 254	com/google/android/gms/tagmanager/zzdi:zzb	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   633: aload_0
    //   634: getfield 76	com/google/android/gms/tagmanager/zzer:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   637: getstatic 257	com/google/android/gms/tagmanager/zzcz:zzbcv	I
    //   640: invokeinterface 123 2 0
    //   645: aload 5
    //   647: invokeinterface 224 1 0
    //   652: return
    //   653: new 38	java/lang/String
    //   656: dup
    //   657: ldc -39
    //   659: invokespecial 59	java/lang/String:<init>	(Ljava/lang/String;)V
    //   662: astore_3
    //   663: goto -274 -> 389
    //   666: astore_3
    //   667: aload_3
    //   668: invokevirtual 246	java/io/IOException:getMessage	()Ljava/lang/String;
    //   671: astore 4
    //   673: new 133	java/lang/StringBuilder
    //   676: dup
    //   677: aload_2
    //   678: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   681: invokevirtual 46	java/lang/String:length	()I
    //   684: bipush 51
    //   686: iadd
    //   687: aload 4
    //   689: invokestatic 42	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   692: invokevirtual 46	java/lang/String:length	()I
    //   695: iadd
    //   696: invokespecial 135	java/lang/StringBuilder:<init>	(I)V
    //   699: ldc_w 259
    //   702: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   705: aload_2
    //   706: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   709: ldc -6
    //   711: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   714: aload 4
    //   716: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   719: invokevirtual 144	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   722: aload_3
    //   723: invokestatic 254	com/google/android/gms/tagmanager/zzdi:zzb	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   726: aload_0
    //   727: getfield 76	com/google/android/gms/tagmanager/zzer:zzbeg	Lcom/google/android/gms/tagmanager/zzdh;
    //   730: getstatic 238	com/google/android/gms/tagmanager/zzcz:zzbcw	I
    //   733: invokeinterface 123 2 0
    //   738: aload 5
    //   740: invokeinterface 224 1 0
    //   745: return
    //   746: goto -488 -> 258
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	749	0	this	zzer
    //   59	20	1	i	int
    //   41	470	2	localObject1	Object
    //   551	155	2	localObject2	Object
    //   99	291	3	localObject3	Object
    //   429	1	3	localFileNotFoundException	java.io.FileNotFoundException
    //   434	41	3	str1	String
    //   509	1	3	localzzsf	com.google.android.gms.internal.measurement.zzsf
    //   514	57	3	localObject4	Object
    //   574	56	3	localIOException1	java.io.IOException
    //   662	1	3	str2	String
    //   666	57	3	localIOException2	java.io.IOException
    //   264	451	4	localObject5	Object
    //   261	478	5	localzzsd	com.google.android.gms.internal.measurement.zzsd
    // Exception table:
    //   from	to	target	type
    //   266	275	429	java/io/FileNotFoundException
    //   266	275	509	com/google/android/gms/internal/measurement/zzsf
    //   266	275	551	finally
    //   275	389	551	finally
    //   389	393	551	finally
    //   393	404	551	finally
    //   430	501	551	finally
    //   510	529	551	finally
    //   529	545	551	finally
    //   561	571	551	finally
    //   575	645	551	finally
    //   653	663	551	finally
    //   667	738	551	finally
    //   266	275	574	java/io/IOException
    //   275	389	666	java/io/IOException
    //   389	393	666	java/io/IOException
    //   393	404	666	java/io/IOException
    //   653	663	666	java/io/IOException
  }
  
  final void zza(zzdh<zzo> paramzzdh)
  {
    this.zzbeg = paramzzdh;
  }
  
  @VisibleForTesting
  final void zzdc(String paramString)
  {
    if (paramString == null)
    {
      this.zzbaf = this.zzbef;
      return;
    }
    String str = String.valueOf(paramString);
    if (str.length() != 0) {}
    for (str = "Setting CTFE URL path: ".concat(str);; str = new String("Setting CTFE URL path: "))
    {
      zzdi.zzdj(str);
      this.zzbaf = paramString;
      return;
    }
  }
  
  @VisibleForTesting
  final void zzdu(String paramString)
  {
    String str = String.valueOf(paramString);
    if (str.length() != 0) {}
    for (str = "Setting previous container version: ".concat(str);; str = new String("Setting previous container version: "))
    {
      zzdi.zzdj(str);
      this.zzbei = paramString;
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */