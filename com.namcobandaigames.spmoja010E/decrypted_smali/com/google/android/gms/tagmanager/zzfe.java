package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzru;
import com.google.android.gms.internal.measurement.zzry;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class zzfe
  implements zzfg
{
  zzfe(zzfb paramzzfb, Map paramMap1, Map paramMap2, Map paramMap3, Map paramMap4) {}
  
  public final void zza(zzry paramzzry, Set<zzru> paramSet1, Set<zzru> paramSet2, zzeq paramzzeq)
  {
    List localList = (List)this.zzbfb.get(paramzzry);
    this.zzbfc.get(paramzzry);
    if (localList != null)
    {
      paramSet1.addAll(localList);
      paramzzeq.zzos();
    }
    paramSet1 = (List)this.zzbfd.get(paramzzry);
    this.zzbfe.get(paramzzry);
    if (paramSet1 != null)
    {
      paramSet2.addAll(paramSet1);
      paramzzeq.zzot();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */