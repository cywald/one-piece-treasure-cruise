package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzf
  extends zzbq
{
  private static final String ID = com.google.android.gms.internal.measurement.zza.zzb.toString();
  private final zza zzayw;
  
  public zzf(Context paramContext)
  {
    this(zza.zzn(paramContext));
  }
  
  @VisibleForTesting
  private zzf(zza paramzza)
  {
    super(ID, new String[0]);
    this.zzayw = paramzza;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    if (!this.zzayw.isLimitAdTrackingEnabled()) {}
    for (boolean bool = true;; bool = false) {
      return zzgj.zzj(Boolean.valueOf(bool));
    }
  }
  
  public final boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */