package com.google.android.gms.tagmanager;

final class zzgi
  extends Number
  implements Comparable<zzgi>
{
  private double zzbgl;
  private long zzbgm;
  private boolean zzbgn;
  
  private zzgi(double paramDouble)
  {
    this.zzbgl = paramDouble;
    this.zzbgn = false;
  }
  
  private zzgi(long paramLong)
  {
    this.zzbgm = paramLong;
    this.zzbgn = true;
  }
  
  public static zzgi zza(Double paramDouble)
  {
    return new zzgi(paramDouble.doubleValue());
  }
  
  public static zzgi zzar(long paramLong)
  {
    return new zzgi(paramLong);
  }
  
  public static zzgi zzea(String paramString)
    throws NumberFormatException
  {
    try
    {
      zzgi localzzgi1 = new zzgi(Long.parseLong(paramString));
      return localzzgi1;
    }
    catch (NumberFormatException localNumberFormatException1)
    {
      try
      {
        zzgi localzzgi2 = new zzgi(Double.parseDouble(paramString));
        return localzzgi2;
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        throw new NumberFormatException(String.valueOf(paramString).concat(" is not a valid TypedNumber"));
      }
    }
  }
  
  public final byte byteValue()
  {
    return (byte)(int)longValue();
  }
  
  public final double doubleValue()
  {
    if (this.zzbgn) {
      return this.zzbgm;
    }
    return this.zzbgl;
  }
  
  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof zzgi)) && (zza((zzgi)paramObject) == 0);
  }
  
  public final float floatValue()
  {
    return (float)doubleValue();
  }
  
  public final int hashCode()
  {
    return new Long(longValue()).hashCode();
  }
  
  public final int intValue()
  {
    return (int)longValue();
  }
  
  public final long longValue()
  {
    if (this.zzbgn) {
      return this.zzbgm;
    }
    return this.zzbgl;
  }
  
  public final short shortValue()
  {
    return (short)(int)longValue();
  }
  
  public final String toString()
  {
    if (this.zzbgn) {
      return Long.toString(this.zzbgm);
    }
    return Double.toString(this.zzbgl);
  }
  
  public final int zza(zzgi paramzzgi)
  {
    if ((this.zzbgn) && (paramzzgi.zzbgn)) {
      return new Long(this.zzbgm).compareTo(Long.valueOf(paramzzgi.zzbgm));
    }
    return Double.compare(doubleValue(), paramzzgi.doubleValue());
  }
  
  public final boolean zzpy()
  {
    return !this.zzbgn;
  }
  
  public final boolean zzpz()
  {
    return this.zzbgn;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzgi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */