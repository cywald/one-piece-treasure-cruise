package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class zzek
  extends zzbq
{
  private static final String ID = zza.zzap.toString();
  private static final String zzbea = zzb.zzef.toString();
  private static final String zzbeb = zzb.zzeg.toString();
  private static final String zzbec = zzb.zzii.toString();
  private static final String zzbed = zzb.zzib.toString();
  
  public zzek()
  {
    super(ID, new String[] { zzbea, zzbeb });
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    Object localObject = (zzp)paramMap.get(zzbea);
    zzp localzzp = (zzp)paramMap.get(zzbeb);
    if ((localObject == null) || (localObject == zzgj.zzqg()) || (localzzp == null) || (localzzp == zzgj.zzqg())) {
      return zzgj.zzqg();
    }
    int i = 64;
    if (zzgj.zzg((zzp)paramMap.get(zzbec)).booleanValue()) {
      i = 66;
    }
    paramMap = (zzp)paramMap.get(zzbed);
    int j;
    if (paramMap != null)
    {
      paramMap = zzgj.zze(paramMap);
      if (paramMap == zzgj.zzqb()) {
        return zzgj.zzqg();
      }
      int k = paramMap.intValue();
      j = k;
      if (k < 0) {
        return zzgj.zzqg();
      }
    }
    else
    {
      j = 1;
    }
    try
    {
      paramMap = zzgj.zzc((zzp)localObject);
      localObject = zzgj.zzc(localzzp);
      localzzp = null;
      localObject = Pattern.compile((String)localObject, i).matcher(paramMap);
      paramMap = localzzp;
      if (((Matcher)localObject).find())
      {
        paramMap = localzzp;
        if (((Matcher)localObject).groupCount() >= j) {
          paramMap = ((Matcher)localObject).group(j);
        }
      }
      if (paramMap == null) {
        return zzgj.zzqg();
      }
      paramMap = zzgj.zzj(paramMap);
      return paramMap;
    }
    catch (PatternSyntaxException paramMap) {}
    return zzgj.zzqg();
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzek.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */