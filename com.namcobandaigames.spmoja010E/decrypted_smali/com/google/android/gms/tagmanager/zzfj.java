package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzfj
  extends zzbq
{
  private static final String ID = zza.zzac.toString();
  
  public zzfj()
  {
    super(ID, new String[0]);
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    return zzgj.zzj(Long.valueOf(65833898L));
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */