package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

abstract class zzfz
  extends zzef
{
  public zzfz(String paramString)
  {
    super(paramString);
  }
  
  protected final boolean zza(zzp paramzzp1, zzp paramzzp2, Map<String, zzp> paramMap)
  {
    paramzzp1 = zzgj.zzc(paramzzp1);
    paramzzp2 = zzgj.zzc(paramzzp2);
    if ((paramzzp1 == zzgj.zzqf()) || (paramzzp2 == zzgj.zzqf())) {
      return false;
    }
    return zza(paramzzp1, paramzzp2, paramMap);
  }
  
  protected abstract boolean zza(String paramString1, String paramString2, Map<String, zzp> paramMap);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */