package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

@VisibleForTesting
final class zzas
  extends zzbq
{
  private static final String ID = zza.zzi.toString();
  private static final String NAME = zzb.zzjk.toString();
  private static final String zzbay = zzb.zzgp.toString();
  private final DataLayer zzazg;
  
  public zzas(DataLayer paramDataLayer)
  {
    super(ID, new String[] { NAME });
    this.zzazg = paramDataLayer;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    Object localObject = this.zzazg.get(zzgj.zzc((zzp)paramMap.get(NAME)));
    if (localObject == null)
    {
      paramMap = (zzp)paramMap.get(zzbay);
      if (paramMap != null) {
        return paramMap;
      }
      return zzgj.zzqg();
    }
    return zzgj.zzj(localObject);
  }
  
  public final boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */