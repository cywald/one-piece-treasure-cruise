package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzh
  extends zzbq
{
  private static final String ID = zza.zzah.toString();
  private static final String zzayx = zzb.zzfi.toString();
  private static final String zzayy = zzb.zzfl.toString();
  private final Context zzri;
  
  public zzh(Context paramContext)
  {
    super(ID, new String[] { zzayy });
    this.zzri = paramContext;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    Object localObject = (zzp)paramMap.get(zzayy);
    if (localObject == null) {
      return zzgj.zzqg();
    }
    String str2 = zzgj.zzc((zzp)localObject);
    paramMap = (zzp)paramMap.get(zzayx);
    if (paramMap != null)
    {
      localObject = zzgj.zzc(paramMap);
      Context localContext = this.zzri;
      String str1 = (String)zzcw.zzbcl.get(str2);
      paramMap = str1;
      if (str1 == null)
      {
        paramMap = localContext.getSharedPreferences("gtm_click_referrers", 0);
        if (paramMap == null) {
          break label131;
        }
      }
    }
    label131:
    for (paramMap = paramMap.getString(str2, "");; paramMap = "")
    {
      zzcw.zzbcl.put(str2, paramMap);
      paramMap = zzcw.zzv(paramMap, (String)localObject);
      if (paramMap == null) {
        break label137;
      }
      return zzgj.zzj(paramMap);
      localObject = null;
      break;
    }
    label137:
    return zzgj.zzqg();
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */