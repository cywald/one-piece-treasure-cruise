package com.google.android.gms.tagmanager;

import android.util.LruCache;

final class zzdc
  extends LruCache<K, V>
{
  zzdc(zzdb paramzzdb, int paramInt, zzs paramzzs)
  {
    super(paramInt);
  }
  
  protected final int sizeOf(K paramK, V paramV)
  {
    return this.zzbda.sizeOf(paramK, paramV);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */