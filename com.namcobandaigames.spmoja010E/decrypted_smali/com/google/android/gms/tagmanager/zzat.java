package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

final class zzat
  implements DataLayer.zzc
{
  private static final String zzbaz = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' STRING NOT NULL, '%s' BLOB NOT NULL, '%s' INTEGER NOT NULL);", new Object[] { "datalayer", "ID", "key", "value", "expires" });
  private final Executor zzbba;
  private zzax zzbbb;
  private int zzbbc;
  private final Context zzri;
  private Clock zzrz;
  
  public zzat(Context paramContext)
  {
    this(paramContext, DefaultClock.getInstance(), "google_tagmanager.db", 2000, Executors.newSingleThreadExecutor());
  }
  
  @VisibleForTesting
  private zzat(Context paramContext, Clock paramClock, String paramString, int paramInt, Executor paramExecutor)
  {
    this.zzri = paramContext;
    this.zzrz = paramClock;
    this.zzbbc = 2000;
    this.zzbba = paramExecutor;
    this.zzbbb = new zzax(this, this.zzri, paramString);
  }
  
  private final void zzaq(long paramLong)
  {
    SQLiteDatabase localSQLiteDatabase = zzdh("Error opening database for deleteOlderThan.");
    if (localSQLiteDatabase == null) {
      return;
    }
    try
    {
      int i = localSQLiteDatabase.delete("datalayer", "expires <= ?", new String[] { Long.toString(paramLong) });
      zzdi.v(33 + "Deleted " + i + " expired items");
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzdi.zzab("Error deleting old entries.");
    }
  }
  
  private final void zzb(List<zzay> paramList, long paramLong)
  {
    try
    {
      l = this.zzrz.currentTimeMillis();
      zzaq(l);
      int i = paramList.size() + (zzoa() - this.zzbbc);
      if (i > 0)
      {
        localObject1 = zzv(i);
        i = ((List)localObject1).size();
        zzdi.zzdi(64 + "DataLayer store full, deleting " + i + " entries to make room.");
        localObject1 = (String[])((List)localObject1).toArray(new String[0]);
        if (localObject1 != null) {
          if (localObject1.length != 0) {
            break label229;
          }
        }
      }
    }
    finally
    {
      for (;;)
      {
        Object localObject3;
        try
        {
          long l;
          zzob();
          throw paramList;
        }
        finally {}
        label229:
        Object localObject2 = zzdh("Error opening database for deleteEntries.");
        if (localObject2 != null)
        {
          localObject3 = String.format("%s in (%s)", new Object[] { "ID", TextUtils.join(",", Collections.nCopies(localObject1.length, "?")) });
          try
          {
            ((SQLiteDatabase)localObject2).delete("datalayer", (String)localObject3, (String[])localObject1);
          }
          catch (SQLiteException localSQLiteException)
          {
            localObject1 = String.valueOf(Arrays.toString((Object[])localObject1));
            if (((String)localObject1).length() == 0) {}
          }
        }
      }
      for (Object localObject1 = "Error deleting entries ".concat((String)localObject1);; localObject1 = new String("Error deleting entries "))
      {
        zzdi.zzab((String)localObject1);
        break;
      }
      zzob();
    }
    localObject1 = zzdh("Error opening database for writeEntryToDatabase.");
    if (localObject1 != null)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        localObject2 = (zzay)paramList.next();
        localObject3 = new ContentValues();
        ((ContentValues)localObject3).put("expires", Long.valueOf(l + paramLong));
        ((ContentValues)localObject3).put("key", ((zzay)localObject2).zzoj);
        ((ContentValues)localObject3).put("value", ((zzay)localObject2).zzbbi);
        ((SQLiteDatabase)localObject1).insert("datalayer", null, (ContentValues)localObject3);
      }
    }
  }
  
  /* Error */
  private static Object zzd(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: new 281	java/io/ByteArrayInputStream
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 284	java/io/ByteArrayInputStream:<init>	([B)V
    //   8: astore_2
    //   9: new 286	java/io/ObjectInputStream
    //   12: dup
    //   13: aload_2
    //   14: invokespecial 289	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   17: astore_0
    //   18: aload_0
    //   19: invokevirtual 292	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   22: astore_1
    //   23: aload_0
    //   24: invokevirtual 295	java/io/ObjectInputStream:close	()V
    //   27: aload_2
    //   28: invokevirtual 296	java/io/ByteArrayInputStream:close	()V
    //   31: aload_1
    //   32: areturn
    //   33: astore_0
    //   34: aconst_null
    //   35: astore_0
    //   36: aload_0
    //   37: ifnull +7 -> 44
    //   40: aload_0
    //   41: invokevirtual 295	java/io/ObjectInputStream:close	()V
    //   44: aload_2
    //   45: invokevirtual 296	java/io/ByteArrayInputStream:close	()V
    //   48: aconst_null
    //   49: areturn
    //   50: astore_0
    //   51: aconst_null
    //   52: areturn
    //   53: astore_0
    //   54: aconst_null
    //   55: astore_0
    //   56: aload_0
    //   57: ifnull +7 -> 64
    //   60: aload_0
    //   61: invokevirtual 295	java/io/ObjectInputStream:close	()V
    //   64: aload_2
    //   65: invokevirtual 296	java/io/ByteArrayInputStream:close	()V
    //   68: aconst_null
    //   69: areturn
    //   70: astore_0
    //   71: aconst_null
    //   72: areturn
    //   73: astore_1
    //   74: aconst_null
    //   75: astore_0
    //   76: aload_0
    //   77: ifnull +7 -> 84
    //   80: aload_0
    //   81: invokevirtual 295	java/io/ObjectInputStream:close	()V
    //   84: aload_2
    //   85: invokevirtual 296	java/io/ByteArrayInputStream:close	()V
    //   88: aload_1
    //   89: athrow
    //   90: astore_0
    //   91: goto -3 -> 88
    //   94: astore_1
    //   95: goto -19 -> 76
    //   98: astore_1
    //   99: goto -43 -> 56
    //   102: astore_1
    //   103: goto -67 -> 36
    //   106: astore_0
    //   107: aload_1
    //   108: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	109	0	paramArrayOfByte	byte[]
    //   22	10	1	localObject1	Object
    //   73	16	1	localObject2	Object
    //   94	1	1	localObject3	Object
    //   98	1	1	localClassNotFoundException	ClassNotFoundException
    //   102	6	1	localIOException	java.io.IOException
    //   8	77	2	localByteArrayInputStream	java.io.ByteArrayInputStream
    // Exception table:
    //   from	to	target	type
    //   9	18	33	java/io/IOException
    //   40	44	50	java/io/IOException
    //   44	48	50	java/io/IOException
    //   9	18	53	java/lang/ClassNotFoundException
    //   60	64	70	java/io/IOException
    //   64	68	70	java/io/IOException
    //   9	18	73	finally
    //   80	84	90	java/io/IOException
    //   84	88	90	java/io/IOException
    //   18	23	94	finally
    //   18	23	98	java/lang/ClassNotFoundException
    //   18	23	102	java/io/IOException
    //   23	31	106	java/io/IOException
  }
  
  private final void zzdg(String paramString)
  {
    SQLiteDatabase localSQLiteDatabase = zzdh("Error opening database for clearKeysWithPrefix.");
    if (localSQLiteDatabase == null) {
      return;
    }
    try
    {
      int i = localSQLiteDatabase.delete("datalayer", "key = ? OR key LIKE ?", new String[] { paramString, String.valueOf(paramString).concat(".%") });
      zzdi.v(25 + "Cleared " + i + " items");
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      String str = String.valueOf(localSQLiteException);
      zzdi.zzab(String.valueOf(paramString).length() + 44 + String.valueOf(str).length() + "Error deleting entries with key prefix: " + paramString + " (" + str + ").");
      return;
    }
    finally
    {
      zzob();
    }
  }
  
  private final SQLiteDatabase zzdh(String paramString)
  {
    try
    {
      SQLiteDatabase localSQLiteDatabase = this.zzbbb.getWritableDatabase();
      return localSQLiteDatabase;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzdi.zzab(paramString);
    }
    return null;
  }
  
  /* Error */
  private static byte[] zzg(Object paramObject)
  {
    // Byte code:
    //   0: new 320	java/io/ByteArrayOutputStream
    //   3: dup
    //   4: invokespecial 321	java/io/ByteArrayOutputStream:<init>	()V
    //   7: astore_2
    //   8: new 323	java/io/ObjectOutputStream
    //   11: dup
    //   12: aload_2
    //   13: invokespecial 326	java/io/ObjectOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   16: astore_1
    //   17: aload_1
    //   18: aload_0
    //   19: invokevirtual 330	java/io/ObjectOutputStream:writeObject	(Ljava/lang/Object;)V
    //   22: aload_2
    //   23: invokevirtual 334	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   26: astore_0
    //   27: aload_1
    //   28: invokevirtual 335	java/io/ObjectOutputStream:close	()V
    //   31: aload_2
    //   32: invokevirtual 336	java/io/ByteArrayOutputStream:close	()V
    //   35: aload_0
    //   36: areturn
    //   37: astore_0
    //   38: aconst_null
    //   39: astore_1
    //   40: aload_1
    //   41: ifnull +7 -> 48
    //   44: aload_1
    //   45: invokevirtual 335	java/io/ObjectOutputStream:close	()V
    //   48: aload_2
    //   49: invokevirtual 336	java/io/ByteArrayOutputStream:close	()V
    //   52: aconst_null
    //   53: areturn
    //   54: astore_0
    //   55: aconst_null
    //   56: areturn
    //   57: astore_0
    //   58: aconst_null
    //   59: astore_1
    //   60: aload_1
    //   61: ifnull +7 -> 68
    //   64: aload_1
    //   65: invokevirtual 335	java/io/ObjectOutputStream:close	()V
    //   68: aload_2
    //   69: invokevirtual 336	java/io/ByteArrayOutputStream:close	()V
    //   72: aload_0
    //   73: athrow
    //   74: astore_1
    //   75: goto -3 -> 72
    //   78: astore_0
    //   79: goto -19 -> 60
    //   82: astore_0
    //   83: goto -43 -> 40
    //   86: astore_1
    //   87: aload_0
    //   88: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	89	0	paramObject	Object
    //   16	49	1	localObjectOutputStream	java.io.ObjectOutputStream
    //   74	1	1	localIOException1	java.io.IOException
    //   86	1	1	localIOException2	java.io.IOException
    //   7	62	2	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    // Exception table:
    //   from	to	target	type
    //   8	17	37	java/io/IOException
    //   44	48	54	java/io/IOException
    //   48	52	54	java/io/IOException
    //   8	17	57	finally
    //   64	68	74	java/io/IOException
    //   68	72	74	java/io/IOException
    //   17	27	78	finally
    //   17	27	82	java/io/IOException
    //   27	35	86	java/io/IOException
  }
  
  private final List<DataLayer.zza> zzny()
  {
    try
    {
      zzaq(this.zzrz.currentTimeMillis());
      Object localObject = zznz();
      ArrayList localArrayList = new ArrayList();
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        zzay localzzay = (zzay)((Iterator)localObject).next();
        localArrayList.add(new DataLayer.zza(localzzay.zzoj, zzd(localzzay.zzbbi)));
      }
    }
    finally
    {
      zzob();
    }
    return localList;
  }
  
  private final List<zzay> zznz()
  {
    Object localObject = zzdh("Error opening database for loadSerialized.");
    ArrayList localArrayList = new ArrayList();
    if (localObject == null) {
      return localArrayList;
    }
    localObject = ((SQLiteDatabase)localObject).query("datalayer", new String[] { "key", "value" }, null, null, null, null, "ID", null);
    try
    {
      if (((Cursor)localObject).moveToNext()) {
        localArrayList.add(new zzay(((Cursor)localObject).getString(0), ((Cursor)localObject).getBlob(1)));
      }
      return localList;
    }
    finally
    {
      ((Cursor)localObject).close();
    }
  }
  
  private final int zzoa()
  {
    Object localObject3 = null;
    Object localObject1 = null;
    int i = 0;
    int j = 0;
    Object localObject5 = zzdh("Error opening database for getNumStoredEntries.");
    if (localObject5 == null) {}
    for (;;)
    {
      return j;
      try
      {
        localObject5 = ((SQLiteDatabase)localObject5).rawQuery("SELECT COUNT(*) from datalayer", null);
        localObject1 = localObject5;
        localObject3 = localObject5;
        if (((Cursor)localObject5).moveToFirst())
        {
          localObject1 = localObject5;
          localObject3 = localObject5;
          long l = ((Cursor)localObject5).getLong(0);
          i = (int)l;
        }
        j = i;
        return i;
      }
      catch (SQLiteException localSQLiteException)
      {
        localObject4 = localObject1;
        zzdi.zzab("Error getting numStoredEntries");
        return 0;
      }
      finally
      {
        Object localObject4;
        if (localObject4 != null) {
          ((Cursor)localObject4).close();
        }
      }
    }
  }
  
  private final void zzob()
  {
    try
    {
      this.zzbbb.close();
      return;
    }
    catch (SQLiteException localSQLiteException) {}
  }
  
  /* Error */
  private final List<String> zzv(int paramInt)
  {
    // Byte code:
    //   0: new 341	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 342	java/util/ArrayList:<init>	()V
    //   7: astore 6
    //   9: iload_1
    //   10: ifgt +12 -> 22
    //   13: ldc_w 398
    //   16: invokestatic 148	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   19: aload 6
    //   21: areturn
    //   22: aload_0
    //   23: ldc_w 400
    //   26: invokespecial 105	com/google/android/gms/tagmanager/zzat:zzdh	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    //   29: astore_3
    //   30: aload_3
    //   31: ifnonnull +6 -> 37
    //   34: aload 6
    //   36: areturn
    //   37: ldc_w 402
    //   40: iconst_1
    //   41: anewarray 4	java/lang/Object
    //   44: dup
    //   45: iconst_0
    //   46: ldc 26
    //   48: aastore
    //   49: invokestatic 38	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   52: astore 4
    //   54: iload_1
    //   55: invokestatic 406	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   58: astore 5
    //   60: aload_3
    //   61: ldc 24
    //   63: iconst_1
    //   64: anewarray 34	java/lang/String
    //   67: dup
    //   68: iconst_0
    //   69: ldc 26
    //   71: aastore
    //   72: aconst_null
    //   73: aconst_null
    //   74: aconst_null
    //   75: aconst_null
    //   76: aload 4
    //   78: aload 5
    //   80: invokevirtual 360	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   83: astore 4
    //   85: aload 4
    //   87: astore_3
    //   88: aload 4
    //   90: invokeinterface 388 1 0
    //   95: ifeq +40 -> 135
    //   98: aload 4
    //   100: astore_3
    //   101: aload 6
    //   103: aload 4
    //   105: iconst_0
    //   106: invokeinterface 392 2 0
    //   111: invokestatic 408	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   114: invokeinterface 353 2 0
    //   119: pop
    //   120: aload 4
    //   122: astore_3
    //   123: aload 4
    //   125: invokeinterface 365 1 0
    //   130: istore_2
    //   131: iload_2
    //   132: ifne -34 -> 98
    //   135: aload 4
    //   137: ifnull +10 -> 147
    //   140: aload 4
    //   142: invokeinterface 376 1 0
    //   147: aload 6
    //   149: areturn
    //   150: astore 5
    //   152: aconst_null
    //   153: astore 4
    //   155: aload 4
    //   157: astore_3
    //   158: aload 5
    //   160: invokevirtual 411	android/database/sqlite/SQLiteException:getMessage	()Ljava/lang/String;
    //   163: invokestatic 260	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   166: astore 5
    //   168: aload 4
    //   170: astore_3
    //   171: aload 5
    //   173: invokevirtual 263	java/lang/String:length	()I
    //   176: ifeq +39 -> 215
    //   179: aload 4
    //   181: astore_3
    //   182: ldc_w 413
    //   185: aload 5
    //   187: invokevirtual 269	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   190: astore 5
    //   192: aload 4
    //   194: astore_3
    //   195: aload 5
    //   197: invokestatic 148	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   200: aload 4
    //   202: ifnull -55 -> 147
    //   205: aload 4
    //   207: invokeinterface 376 1 0
    //   212: goto -65 -> 147
    //   215: aload 4
    //   217: astore_3
    //   218: new 34	java/lang/String
    //   221: dup
    //   222: ldc_w 413
    //   225: invokespecial 271	java/lang/String:<init>	(Ljava/lang/String;)V
    //   228: astore 5
    //   230: goto -38 -> 192
    //   233: astore 5
    //   235: aload_3
    //   236: astore 4
    //   238: aload 5
    //   240: astore_3
    //   241: aload 4
    //   243: ifnull +10 -> 253
    //   246: aload 4
    //   248: invokeinterface 376 1 0
    //   253: aload_3
    //   254: athrow
    //   255: astore_3
    //   256: aconst_null
    //   257: astore 4
    //   259: goto -18 -> 241
    //   262: astore 5
    //   264: goto -109 -> 155
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	267	0	this	zzat
    //   0	267	1	paramInt	int
    //   130	2	2	bool	boolean
    //   29	225	3	localObject1	Object
    //   255	1	3	localObject2	Object
    //   52	206	4	localObject3	Object
    //   58	21	5	str1	String
    //   150	9	5	localSQLiteException1	SQLiteException
    //   166	63	5	str2	String
    //   233	6	5	localObject4	Object
    //   262	1	5	localSQLiteException2	SQLiteException
    //   7	141	6	localArrayList	ArrayList
    // Exception table:
    //   from	to	target	type
    //   37	85	150	android/database/sqlite/SQLiteException
    //   88	98	233	finally
    //   101	120	233	finally
    //   123	131	233	finally
    //   158	168	233	finally
    //   171	179	233	finally
    //   182	192	233	finally
    //   195	200	233	finally
    //   218	230	233	finally
    //   37	85	255	finally
    //   88	98	262	android/database/sqlite/SQLiteException
    //   101	120	262	android/database/sqlite/SQLiteException
    //   123	131	262	android/database/sqlite/SQLiteException
  }
  
  public final void zza(zzaq paramzzaq)
  {
    this.zzbba.execute(new zzav(this, paramzzaq));
  }
  
  public final void zza(List<DataLayer.zza> paramList, long paramLong)
  {
    ArrayList localArrayList = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      DataLayer.zza localzza = (DataLayer.zza)paramList.next();
      localArrayList.add(new zzay(localzza.mKey, zzg(localzza.mValue)));
    }
    this.zzbba.execute(new zzau(this, localArrayList, paramLong));
  }
  
  public final void zzdf(String paramString)
  {
    this.zzbba.execute(new zzaw(this, paramString));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */