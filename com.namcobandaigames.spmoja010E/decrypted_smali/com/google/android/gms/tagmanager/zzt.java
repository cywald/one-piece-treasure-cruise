package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzt
  extends zzbq
{
  private static final String ID = zza.zzg.toString();
  private static final String VALUE = zzb.zznh.toString();
  
  public zzt()
  {
    super(ID, new String[] { VALUE });
  }
  
  public static String zznd()
  {
    return ID;
  }
  
  public static String zzne()
  {
    return VALUE;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    return (zzp)paramMap.get(VALUE);
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */