package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzei
  extends zzbq
{
  private static final String ID = zza.zzz.toString();
  private static final String zzbdy = zzb.zzji.toString();
  private static final String zzbdz = zzb.zzjg.toString();
  
  public zzei()
  {
    super(ID, new String[0]);
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    Object localObject = (zzp)paramMap.get(zzbdy);
    paramMap = (zzp)paramMap.get(zzbdz);
    double d2;
    double d1;
    if ((localObject != null) && (localObject != zzgj.zzqg()) && (paramMap != null) && (paramMap != zzgj.zzqg()))
    {
      localObject = zzgj.zzd((zzp)localObject);
      paramMap = zzgj.zzd(paramMap);
      if ((localObject != zzgj.zzqe()) && (paramMap != zzgj.zzqe()))
      {
        d2 = ((zzgi)localObject).doubleValue();
        d1 = paramMap.doubleValue();
        if (d2 > d1) {}
      }
    }
    for (;;)
    {
      return zzgj.zzj(Long.valueOf(Math.round((d1 - d2) * Math.random() + d2)));
      d1 = 2.147483647E9D;
      d2 = 0.0D;
    }
  }
  
  public final boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzei.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */