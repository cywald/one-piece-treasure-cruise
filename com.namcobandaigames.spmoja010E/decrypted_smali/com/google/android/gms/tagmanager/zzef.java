package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

@VisibleForTesting
public abstract class zzef
  extends zzbq
{
  private static final String zzbbt = zzb.zzef.toString();
  private static final String zzbdp = zzb.zzeg.toString();
  
  public zzef(String paramString)
  {
    super(paramString, new String[] { zzbbt, zzbdp });
  }
  
  protected abstract boolean zza(zzp paramzzp1, zzp paramzzp2, Map<String, zzp> paramMap);
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    Object localObject = paramMap.values().iterator();
    while (((Iterator)localObject).hasNext()) {
      if ((zzp)((Iterator)localObject).next() == zzgj.zzqg()) {
        return zzgj.zzj(Boolean.valueOf(false));
      }
    }
    localObject = (zzp)paramMap.get(zzbbt);
    zzp localzzp = (zzp)paramMap.get(zzbdp);
    if ((localObject == null) || (localzzp == null)) {}
    for (boolean bool = false;; bool = zza((zzp)localObject, localzzp, paramMap)) {
      return zzgj.zzj(Boolean.valueOf(bool));
    }
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */