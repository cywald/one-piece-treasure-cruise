package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class zzcx
  extends zzbq
{
  private static final String ID = zza.zzan.toString();
  private static final String zzbbt = zzb.zzef.toString();
  private static final String zzbcm = zzb.zzip.toString();
  private static final String zzbcn = zzb.zzit.toString();
  private static final String zzbco = zzb.zzhj.toString();
  
  public zzcx()
  {
    super(ID, new String[] { zzbbt });
  }
  
  private static String zza(String paramString, Integer paramInteger, Set<Character> paramSet)
  {
    switch (zzcy.zzbcp[(paramInteger - 1)])
    {
    default: 
      return paramString;
    case 1: 
      try
      {
        paramSet = zzgn.zzee(paramString);
        return paramSet;
      }
      catch (UnsupportedEncodingException paramSet)
      {
        zzdi.zza("Joiner: unsupported encoding", paramSet);
        return paramString;
      }
    }
    paramString = paramString.replace("\\", "\\\\");
    Iterator localIterator = paramSet.iterator();
    if (localIterator.hasNext())
    {
      String str = ((Character)localIterator.next()).toString();
      paramSet = String.valueOf(str);
      if (paramSet.length() != 0) {}
      for (paramSet = "\\".concat(paramSet);; paramSet = new String("\\"))
      {
        paramString = paramString.replace(str, paramSet);
        break;
      }
    }
    return paramString;
  }
  
  private static void zza(StringBuilder paramStringBuilder, String paramString, Integer paramInteger, Set<Character> paramSet)
  {
    paramStringBuilder.append(zza(paramString, paramInteger, paramSet));
  }
  
  private static void zza(Set<Character> paramSet, String paramString)
  {
    int i = 0;
    while (i < paramString.length())
    {
      paramSet.add(Character.valueOf(paramString.charAt(i)));
      i += 1;
    }
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    zzp localzzp = (zzp)paramMap.get(zzbbt);
    if (localzzp == null) {
      return zzgj.zzqg();
    }
    Object localObject1 = (zzp)paramMap.get(zzbcm);
    Object localObject2;
    label75:
    int i;
    if (localObject1 != null)
    {
      localObject1 = zzgj.zzc((zzp)localObject1);
      localObject2 = (zzp)paramMap.get(zzbcn);
      if (localObject2 == null) {
        break label180;
      }
      localObject2 = zzgj.zzc((zzp)localObject2);
      i = zzcz.zzbcq;
      paramMap = (zzp)paramMap.get(zzbco);
      if (paramMap == null) {
        break label420;
      }
      paramMap = zzgj.zzc(paramMap);
      if (!"url".equals(paramMap)) {
        break label187;
      }
      i = zzcz.zzbcr;
      paramMap = null;
    }
    for (;;)
    {
      label116:
      StringBuilder localStringBuilder = new StringBuilder();
      switch (localzzp.type)
      {
      default: 
        zza(localStringBuilder, zzgj.zzc(localzzp), i, paramMap);
      }
      for (;;)
      {
        return zzgj.zzj(localStringBuilder.toString());
        localObject1 = "";
        break;
        label180:
        localObject2 = "=";
        break label75;
        label187:
        if ("backslash".equals(paramMap))
        {
          i = zzcz.zzbcs;
          paramMap = new HashSet();
          zza(paramMap, (String)localObject1);
          zza(paramMap, (String)localObject2);
          paramMap.remove(Character.valueOf('\\'));
          break label116;
        }
        paramMap = String.valueOf(paramMap);
        if (paramMap.length() != 0) {}
        for (paramMap = "Joiner: unsupported escape type: ".concat(paramMap);; paramMap = new String("Joiner: unsupported escape type: "))
        {
          zzdi.e(paramMap);
          return zzgj.zzqg();
        }
        int k = 1;
        localObject2 = localzzp.zzqj;
        int m = localObject2.length;
        int j = 0;
        while (j < m)
        {
          localzzp = localObject2[j];
          if (k == 0) {
            localStringBuilder.append((String)localObject1);
          }
          zza(localStringBuilder, zzgj.zzc(localzzp), i, paramMap);
          j += 1;
          k = 0;
        }
        j = 0;
        while (j < localzzp.zzqk.length)
        {
          if (j > 0) {
            localStringBuilder.append((String)localObject1);
          }
          String str1 = zzgj.zzc(localzzp.zzqk[j]);
          String str2 = zzgj.zzc(localzzp.zzql[j]);
          zza(localStringBuilder, str1, i, paramMap);
          localStringBuilder.append((String)localObject2);
          zza(localStringBuilder, str2, i, paramMap);
          j += 1;
        }
      }
      label420:
      paramMap = null;
    }
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzcx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */