package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.measurement.zzsw;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.LinkedBlockingQueue;

final class zzbz
  extends Thread
  implements zzby
{
  private static zzbz zzbce;
  private volatile boolean closed = false;
  private final LinkedBlockingQueue<Runnable> zzbcc = new LinkedBlockingQueue();
  private volatile boolean zzbcd = false;
  private volatile zzcb zzbcf;
  private final Context zzri;
  
  private zzbz(Context paramContext)
  {
    super("GAThread");
    if (paramContext != null) {}
    for (this.zzri = paramContext.getApplicationContext();; this.zzri = paramContext)
    {
      start();
      return;
    }
  }
  
  static zzbz zzu(Context paramContext)
  {
    if (zzbce == null) {
      zzbce = new zzbz(paramContext);
    }
    return zzbce;
  }
  
  public final void run()
  {
    for (;;)
    {
      boolean bool = this.closed;
      try
      {
        Runnable localRunnable = (Runnable)this.zzbcc.take();
        if (!this.zzbcd) {
          localRunnable.run();
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        zzdi.zzdi(localInterruptedException.toString());
      }
      catch (Exception localException)
      {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream localPrintStream = new PrintStream(localByteArrayOutputStream);
        zzsw.zza(localException, localPrintStream);
        localPrintStream.flush();
        str = String.valueOf(new String(localByteArrayOutputStream.toByteArray()));
        if (str.length() == 0) {}
      }
    }
    for (String str = "Error on Google TagManager Thread: ".concat(str);; str = new String("Error on Google TagManager Thread: "))
    {
      zzdi.e(str);
      zzdi.e("Google TagManager is shutting down.");
      this.zzbcd = true;
      break;
    }
  }
  
  public final void zzdp(String paramString)
  {
    zzh(new zzca(this, this, System.currentTimeMillis(), paramString));
  }
  
  public final void zzh(Runnable paramRunnable)
  {
    this.zzbcc.add(paramRunnable);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzbz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */