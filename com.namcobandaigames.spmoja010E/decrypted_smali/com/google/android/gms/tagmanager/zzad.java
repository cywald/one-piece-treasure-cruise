package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzrq;

final class zzad
  implements zzdh<zzrq>
{
  private zzad(zzy paramzzy) {}
  
  public final void zzno() {}
  
  public final void zzt(int paramInt)
  {
    if (!zzy.zze(this.zzbai)) {
      zzy.zza(this.zzbai, 0L);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */