package com.google.android.gms.tagmanager;

import android.annotation.TargetApi;
import android.util.LruCache;

@TargetApi(12)
final class zzdb<K, V>
  implements zzp<K, V>
{
  private LruCache<K, V> zzbcz;
  
  zzdb(int paramInt, zzs<K, V> paramzzs)
  {
    this.zzbcz = new zzdc(this, 1048576, paramzzs);
  }
  
  public final V get(K paramK)
  {
    return (V)this.zzbcz.get(paramK);
  }
  
  public final void zza(K paramK, V paramV)
  {
    this.zzbcz.put(paramK, paramV);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */