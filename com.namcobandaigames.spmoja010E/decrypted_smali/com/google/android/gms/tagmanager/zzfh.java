package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzp;
import com.google.android.gms.internal.measurement.zzzg;

final class zzfh
{
  private zzdz<zzp> zzbff;
  private zzp zzbfg;
  
  public zzfh(zzdz<zzp> paramzzdz, zzp paramzzp)
  {
    this.zzbff = paramzzdz;
    this.zzbfg = paramzzp;
  }
  
  public final int getSize()
  {
    int j = ((zzp)this.zzbff.getObject()).zzza();
    if (this.zzbfg == null) {}
    for (int i = 0;; i = this.zzbfg.zzza()) {
      return i + j;
    }
  }
  
  public final zzdz<zzp> zzpl()
  {
    return this.zzbff;
  }
  
  public final zzp zzpm()
  {
    return this.zzbfg;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */