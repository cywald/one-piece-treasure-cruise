package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RawRes;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@VisibleForTesting
public class TagManager
{
  private static TagManager zzbgg;
  private final DataLayer zzazg;
  private final zzal zzbeh;
  private final zza zzbgd;
  private final zzfm zzbge;
  private final ConcurrentMap<String, zzv> zzbgf;
  private final Context zzri;
  
  @VisibleForTesting
  private TagManager(Context paramContext, zza paramzza, DataLayer paramDataLayer, zzfm paramzzfm)
  {
    if (paramContext == null) {
      throw new NullPointerException("context cannot be null");
    }
    this.zzri = paramContext.getApplicationContext();
    this.zzbge = paramzzfm;
    this.zzbgd = paramzza;
    this.zzbgf = new ConcurrentHashMap();
    this.zzazg = paramDataLayer;
    this.zzazg.zza(new zzga(this));
    this.zzazg.zza(new zzg(this.zzri));
    this.zzbeh = new zzal();
    this.zzri.registerComponentCallbacks(new zzgc(this));
    zza.zzn(this.zzri);
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public static TagManager getInstance(Context paramContext)
  {
    try
    {
      if (zzbgg != null) {
        break label68;
      }
      if (paramContext == null)
      {
        zzdi.e("TagManager.getInstance requires non-null context.");
        throw new NullPointerException();
      }
    }
    finally {}
    zzbgg = new TagManager(paramContext, new zzgb(), new DataLayer(new zzat(paramContext)), zzfn.zzpu());
    label68:
    paramContext = zzbgg;
    return paramContext;
  }
  
  private final void zzdx(String paramString)
  {
    Iterator localIterator = this.zzbgf.values().iterator();
    while (localIterator.hasNext()) {
      ((zzv)localIterator.next()).zzda(paramString);
    }
  }
  
  public void dispatch()
  {
    this.zzbge.dispatch();
  }
  
  public DataLayer getDataLayer()
  {
    return this.zzazg;
  }
  
  public PendingResult<ContainerHolder> loadContainerDefaultOnly(String paramString, @RawRes int paramInt)
  {
    paramString = this.zzbgd.zza(this.zzri, this, null, paramString, paramInt, this.zzbeh);
    paramString.zznk();
    return paramString;
  }
  
  public PendingResult<ContainerHolder> loadContainerDefaultOnly(String paramString, @RawRes int paramInt, Handler paramHandler)
  {
    paramString = this.zzbgd.zza(this.zzri, this, paramHandler.getLooper(), paramString, paramInt, this.zzbeh);
    paramString.zznk();
    return paramString;
  }
  
  public PendingResult<ContainerHolder> loadContainerPreferFresh(String paramString, @RawRes int paramInt)
  {
    paramString = this.zzbgd.zza(this.zzri, this, null, paramString, paramInt, this.zzbeh);
    paramString.zznm();
    return paramString;
  }
  
  public PendingResult<ContainerHolder> loadContainerPreferFresh(String paramString, @RawRes int paramInt, Handler paramHandler)
  {
    paramString = this.zzbgd.zza(this.zzri, this, paramHandler.getLooper(), paramString, paramInt, this.zzbeh);
    paramString.zznm();
    return paramString;
  }
  
  public PendingResult<ContainerHolder> loadContainerPreferNonDefault(String paramString, @RawRes int paramInt)
  {
    paramString = this.zzbgd.zza(this.zzri, this, null, paramString, paramInt, this.zzbeh);
    paramString.zznl();
    return paramString;
  }
  
  public PendingResult<ContainerHolder> loadContainerPreferNonDefault(String paramString, @RawRes int paramInt, Handler paramHandler)
  {
    paramString = this.zzbgd.zza(this.zzri, this, paramHandler.getLooper(), paramString, paramInt, this.zzbeh);
    paramString.zznl();
    return paramString;
  }
  
  public void setVerboseLoggingEnabled(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i = 2;; i = 5)
    {
      zzdi.setLogLevel(i);
      return;
    }
  }
  
  @VisibleForTesting
  public final int zza(zzv paramzzv)
  {
    this.zzbgf.put(paramzzv.getContainerId(), paramzzv);
    return this.zzbgf.size();
  }
  
  final boolean zzb(Uri paramUri)
  {
    for (;;)
    {
      zzeh localzzeh;
      boolean bool;
      try
      {
        localzzeh = zzeh.zzpc();
        if (!localzzeh.zzb(paramUri)) {
          break label208;
        }
        paramUri = localzzeh.getContainerId();
        int i = zzgd.zzbgi[localzzeh.zzpd().ordinal()];
        switch (i)
        {
        default: 
          bool = true;
          return bool;
        }
      }
      finally {}
      paramUri = (zzv)this.zzbgf.get(paramUri);
      if (paramUri != null)
      {
        paramUri.zzdb(null);
        paramUri.refresh();
        continue;
        Iterator localIterator = this.zzbgf.keySet().iterator();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          zzv localzzv = (zzv)this.zzbgf.get(str);
          if (str.equals(paramUri))
          {
            localzzv.zzdb(localzzeh.zzpe());
            localzzv.refresh();
          }
          else if (localzzv.zznh() != null)
          {
            localzzv.zzdb(null);
            localzzv.refresh();
          }
        }
        continue;
        label208:
        bool = false;
      }
    }
  }
  
  @VisibleForTesting
  public final boolean zzb(zzv paramzzv)
  {
    return this.zzbgf.remove(paramzzv.getContainerId()) != null;
  }
  
  @VisibleForTesting
  public static abstract interface zza
  {
    public abstract zzy zza(Context paramContext, TagManager paramTagManager, Looper paramLooper, String paramString, int paramInt, zzal paramzzal);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\TagManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */