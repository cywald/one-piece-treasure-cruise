package com.google.android.gms.tagmanager;

import android.text.TextUtils;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
final class zzbw
{
  private final long zzaax;
  private final long zzbbz;
  private final long zzbca;
  private String zzbcb;
  
  zzbw(long paramLong1, long paramLong2, long paramLong3)
  {
    this.zzbbz = paramLong1;
    this.zzaax = paramLong2;
    this.zzbca = paramLong3;
  }
  
  final void zzdo(String paramString)
  {
    if ((paramString == null) || (TextUtils.isEmpty(paramString.trim()))) {
      return;
    }
    this.zzbcb = paramString;
  }
  
  final long zzom()
  {
    return this.zzbbz;
  }
  
  final long zzon()
  {
    return this.zzbca;
  }
  
  final String zzoo()
  {
    return this.zzbcb;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzbw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */