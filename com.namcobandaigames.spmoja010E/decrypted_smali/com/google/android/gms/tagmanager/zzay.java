package com.google.android.gms.tagmanager;

import java.util.Arrays;

final class zzay
{
  final byte[] zzbbi;
  final String zzoj;
  
  zzay(String paramString, byte[] paramArrayOfByte)
  {
    this.zzoj = paramString;
    this.zzbbi = paramArrayOfByte;
  }
  
  public final String toString()
  {
    String str = this.zzoj;
    int i = Arrays.hashCode(this.zzbbi);
    return String.valueOf(str).length() + 54 + "KeyAndSerialized: key = " + str + " serialized hash = " + i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */