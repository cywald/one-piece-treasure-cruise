package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzl;
import com.google.android.gms.internal.measurement.zzo;
import com.google.android.gms.internal.measurement.zzrq;
import com.google.android.gms.internal.measurement.zzrr;
import com.google.android.gms.internal.measurement.zzrw;

public final class zzy
  extends BasePendingResult<ContainerHolder>
{
  private final String zzazf;
  private long zzazk;
  private final Looper zzazn;
  private final TagManager zzazt;
  private final zzaf zzazw;
  private final zzej zzazx;
  private final int zzazy;
  private final zzai zzazz;
  private zzah zzbaa;
  private zzrr zzbab;
  private volatile zzv zzbac;
  private volatile boolean zzbad;
  private zzo zzbae;
  private String zzbaf;
  private zzag zzbag;
  private zzac zzbah;
  private final Context zzri;
  private final Clock zzrz;
  
  @VisibleForTesting
  private zzy(Context paramContext, TagManager paramTagManager, Looper paramLooper, String paramString, int paramInt, zzah paramzzah, zzag paramzzag, zzrr paramzzrr, Clock paramClock, zzej paramzzej, zzai paramzzai) {}
  
  public zzy(Context paramContext, TagManager paramTagManager, Looper paramLooper, String paramString, int paramInt, zzal paramzzal)
  {
    this(paramContext, paramTagManager, paramLooper, paramString, paramInt, new zzex(paramContext, paramString), new zzes(paramContext, paramString, paramzzal), new zzrr(paramContext), DefaultClock.getInstance(), new zzdg(1, 5, 900000L, 5000L, "refreshing", DefaultClock.getInstance()), new zzai(paramContext, paramString));
    this.zzbab.zzfd(paramzzal.zznv());
  }
  
  private final void zza(zzo paramzzo)
  {
    try
    {
      if (this.zzbaa != null)
      {
        zzrq localzzrq = new zzrq();
        localzzrq.zzbps = this.zzazk;
        localzzrq.zzqg = new zzl();
        localzzrq.zzbpt = paramzzo;
        this.zzbaa.zza(localzzrq);
      }
      return;
    }
    finally
    {
      paramzzo = finally;
      throw paramzzo;
    }
  }
  
  private final void zza(zzo paramzzo, long paramLong, boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (;;)
    {
      try
      {
        paramBoolean = this.zzbad;
        if (isReady())
        {
          zzv localzzv = this.zzbac;
          if (localzzv == null) {
            return;
          }
        }
        this.zzbae = paramzzo;
        this.zzazk = paramLong;
        long l = this.zzazz.zznq();
        zzap(Math.max(0L, Math.min(l, this.zzazk + l - this.zzrz.currentTimeMillis())));
        paramzzo = new Container(this.zzri, this.zzazt.getDataLayer(), this.zzazf, paramLong, paramzzo);
        if (this.zzbac == null)
        {
          this.zzbac = new zzv(this.zzazt, this.zzazn, paramzzo, this.zzazw);
          if ((!isReady()) && (this.zzbah.zzb(paramzzo))) {
            setResult(this.zzbac);
          }
        }
        else
        {
          this.zzbac.zza(paramzzo);
        }
      }
      finally {}
    }
  }
  
  /* Error */
  private final void zzap(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 66	com/google/android/gms/tagmanager/zzy:zzbag	Lcom/google/android/gms/tagmanager/zzag;
    //   6: ifnonnull +11 -> 17
    //   9: ldc -6
    //   11: invokestatic 255	com/google/android/gms/tagmanager/zzdi:zzab	(Ljava/lang/String;)V
    //   14: aload_0
    //   15: monitorexit
    //   16: return
    //   17: aload_0
    //   18: getfield 66	com/google/android/gms/tagmanager/zzy:zzbag	Lcom/google/android/gms/tagmanager/zzag;
    //   21: lload_1
    //   22: aload_0
    //   23: getfield 82	com/google/android/gms/tagmanager/zzy:zzbae	Lcom/google/android/gms/internal/measurement/zzo;
    //   26: getfield 258	com/google/android/gms/internal/measurement/zzo:zzqh	Ljava/lang/String;
    //   29: invokeinterface 263 4 0
    //   34: goto -20 -> 14
    //   37: astore_3
    //   38: aload_0
    //   39: monitorexit
    //   40: aload_3
    //   41: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	42	0	this	zzy
    //   0	42	1	paramLong	long
    //   37	4	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	14	37	finally
    //   17	34	37	finally
  }
  
  private final boolean zznn()
  {
    zzeh localzzeh = zzeh.zzpc();
    return ((localzzeh.zzpd() == zzeh.zza.zzbdv) || (localzzeh.zzpd() == zzeh.zza.zzbdw)) && (this.zzazf.equals(localzzeh.getContainerId()));
  }
  
  private final void zzp(boolean paramBoolean)
  {
    this.zzbaa.zza(new zzad(this, null));
    this.zzbag.zza(new zzae(this, null));
    zzrw localzzrw = this.zzbaa.zzu(this.zzazy);
    if (localzzrw != null) {
      this.zzbac = new zzv(this.zzazt, this.zzazn, new Container(this.zzri, this.zzazt.getDataLayer(), this.zzazf, 0L, localzzrw), this.zzazw);
    }
    this.zzbah = new zzab(this, paramBoolean);
    if (zznn())
    {
      this.zzbag.zza(0L, "");
      return;
    }
    this.zzbaa.zznp();
  }
  
  protected final ContainerHolder a_(Status paramStatus)
  {
    if (this.zzbac != null) {
      return this.zzbac;
    }
    if (paramStatus == Status.RESULT_TIMEOUT) {
      zzdi.e("timer expired: setting result to failure");
    }
    return new zzv(paramStatus);
  }
  
  @VisibleForTesting
  final void zzdb(String paramString)
  {
    try
    {
      this.zzbaf = paramString;
      if (this.zzbag != null) {
        this.zzbag.zzdc(paramString);
      }
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  final String zznh()
  {
    try
    {
      String str = this.zzbaf;
      return str;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void zznk()
  {
    Object localObject = this.zzbaa.zzu(this.zzazy);
    if (localObject != null)
    {
      localObject = new Container(this.zzri, this.zzazt.getDataLayer(), this.zzazf, 0L, (zzrw)localObject);
      setResult(new zzv(this.zzazt, this.zzazn, (Container)localObject, new zzaa(this)));
    }
    for (;;)
    {
      this.zzbag = null;
      this.zzbaa = null;
      return;
      zzdi.e("Default was requested, but no default container was found");
      setResult(a_(new Status(10, "Default was requested, but no default container was found", null)));
    }
  }
  
  public final void zznl()
  {
    zzp(false);
  }
  
  public final void zznm()
  {
    zzp(true);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */