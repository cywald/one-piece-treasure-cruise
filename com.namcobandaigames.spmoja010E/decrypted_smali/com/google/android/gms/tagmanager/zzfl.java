package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;

final class zzfl
  implements zzej
{
  private final long zzabb = 2000L;
  private final int zzabc = 60;
  private double zzabd = this.zzabc;
  private final Object zzabf = new Object();
  private long zzbfm;
  private final Clock zzrz = DefaultClock.getInstance();
  
  public zzfl()
  {
    this(60, 2000L);
  }
  
  private zzfl(int paramInt, long paramLong) {}
  
  public final boolean zzew()
  {
    synchronized (this.zzabf)
    {
      long l = this.zzrz.currentTimeMillis();
      if (this.zzabd < this.zzabc)
      {
        double d = (l - this.zzbfm) / this.zzabb;
        if (d > 0.0D) {
          this.zzabd = Math.min(this.zzabc, d + this.zzabd);
        }
      }
      this.zzbfm = l;
      if (this.zzabd >= 1.0D)
      {
        this.zzabd -= 1.0D;
        return true;
      }
      zzdi.zzab("No more tokens available.");
      return false;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */