package com.google.android.gms.tagmanager;

final class zzaf
  implements zzw
{
  private zzaf(zzy paramzzy) {}
  
  public final void zzdb(String paramString)
  {
    this.zzbai.zzdb(paramString);
  }
  
  public final String zznh()
  {
    return this.zzbai.zznh();
  }
  
  public final void zznj()
  {
    if (zzy.zza(this.zzbai).zzew()) {
      zzy.zza(this.zzbai, 0L);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */