package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class InstallReferrerReceiver
  extends CampaignTrackingReceiver
{
  protected final void zza(Context paramContext, String paramString)
  {
    zzcw.zzdq(paramString);
    zzcw.zzf(paramContext, paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\InstallReferrerReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */