package com.google.android.gms.tagmanager;

import android.os.Handler.Callback;
import android.os.Message;

final class zzfs
  implements Handler.Callback
{
  zzfs(zzfr paramzzfr) {}
  
  public final boolean handleMessage(Message paramMessage)
  {
    if ((1 == paramMessage.what) && (zzfn.zzpw().equals(paramMessage.obj)))
    {
      this.zzbga.zzbfz.dispatch();
      if (!zzfn.zzb(this.zzbga.zzbfz)) {
        this.zzbga.zzh(zzfn.zzc(this.zzbga.zzbfz));
      }
    }
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */