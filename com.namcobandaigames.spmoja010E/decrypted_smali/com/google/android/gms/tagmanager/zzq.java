package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
final class zzq<K, V>
{
  @VisibleForTesting
  private final zzs<K, V> zzaze = new zzr(this);
  
  public static zzp<K, V> zza(int paramInt, zzs<K, V> paramzzs)
  {
    return new zzdb(1048576, paramzzs);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */