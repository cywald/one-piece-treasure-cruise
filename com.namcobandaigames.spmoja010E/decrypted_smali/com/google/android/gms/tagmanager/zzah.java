package com.google.android.gms.tagmanager;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.internal.measurement.zzrq;
import com.google.android.gms.internal.measurement.zzrw;

abstract interface zzah
  extends Releasable
{
  public abstract void zza(zzrq paramzzrq);
  
  public abstract void zza(zzdh<zzrq> paramzzdh);
  
  public abstract void zznp();
  
  public abstract zzrw zzu(int paramInt);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */