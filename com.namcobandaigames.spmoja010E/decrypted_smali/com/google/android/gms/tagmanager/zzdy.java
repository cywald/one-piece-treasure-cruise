package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

abstract class zzdy
  extends zzef
{
  public zzdy(String paramString)
  {
    super(paramString);
  }
  
  protected final boolean zza(zzp paramzzp1, zzp paramzzp2, Map<String, zzp> paramMap)
  {
    paramzzp1 = zzgj.zzd(paramzzp1);
    paramzzp2 = zzgj.zzd(paramzzp2);
    if ((paramzzp1 == zzgj.zzqe()) || (paramzzp2 == zzgj.zzqe())) {
      return false;
    }
    return zza(paramzzp1, paramzzp2, paramMap);
  }
  
  protected abstract boolean zza(zzgi paramzzgi1, zzgi paramzzgi2, Map<String, zzp> paramMap);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */