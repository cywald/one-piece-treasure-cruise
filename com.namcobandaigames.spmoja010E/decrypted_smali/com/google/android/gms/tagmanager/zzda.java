package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import com.google.android.gms.internal.measurement.zzru;
import com.google.android.gms.internal.measurement.zzrv;
import com.google.android.gms.internal.measurement.zzrw;
import com.google.android.gms.internal.measurement.zzrx;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class zzda
{
  public static zzrw zzdr(String paramString)
    throws JSONException
  {
    paramString = zzgj.zzj(zzh(new JSONObject(paramString)));
    zzrx localzzrx = zzrw.zzsr();
    int i = 0;
    while (i < paramString.zzqk.length)
    {
      localzzrx.zzc(zzru.zzsp().zzb(zzb.zzil.toString(), paramString.zzqk[i]).zzb(zzb.zzhz.toString(), zzgj.zzeb(zzt.zznd())).zzb(zzt.zzne(), paramString.zzql[i]).zzsq());
      i += 1;
    }
    return localzzrx.zzst();
  }
  
  @VisibleForTesting
  private static Object zzh(Object paramObject)
    throws JSONException
  {
    if ((paramObject instanceof JSONArray)) {
      throw new RuntimeException("JSONArrays are not supported");
    }
    if (JSONObject.NULL.equals(paramObject)) {
      throw new RuntimeException("JSON nulls are not supported");
    }
    Object localObject = paramObject;
    if ((paramObject instanceof JSONObject))
    {
      paramObject = (JSONObject)paramObject;
      localObject = new HashMap();
      Iterator localIterator = ((JSONObject)paramObject).keys();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        ((Map)localObject).put(str, zzh(((JSONObject)paramObject).get(str)));
      }
    }
    return localObject;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzda.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */