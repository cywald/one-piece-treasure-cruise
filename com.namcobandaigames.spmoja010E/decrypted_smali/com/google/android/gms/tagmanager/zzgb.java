package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Looper;

final class zzgb
  implements TagManager.zza
{
  public final zzy zza(Context paramContext, TagManager paramTagManager, Looper paramLooper, String paramString, int paramInt, zzal paramzzal)
  {
    return new zzy(paramContext, paramTagManager, paramLooper, paramString, paramInt, paramzzal);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzgb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */