package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzcv
  extends zzbq
{
  private static final String ID = zza.zzam.toString();
  private static final String zzayx = zzb.zzfi.toString();
  private final Context zzri;
  
  public zzcv(Context paramContext)
  {
    super(ID, new String[0]);
    this.zzri = paramContext;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    if ((zzp)paramMap.get(zzayx) != null) {}
    for (paramMap = zzgj.zzc((zzp)paramMap.get(zzayx));; paramMap = null)
    {
      paramMap = zzcw.zzg(this.zzri, paramMap);
      if (paramMap == null) {
        break;
      }
      return zzgj.zzj(paramMap);
    }
    return zzgj.zzqg();
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzcv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */