package com.google.android.gms.tagmanager;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzem
  extends zzbq
{
  private static final String ID = zza.zzab.toString();
  private final Context zzri;
  
  public zzem(Context paramContext)
  {
    super(ID, new String[0]);
    this.zzri = paramContext;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    paramMap = new DisplayMetrics();
    ((WindowManager)this.zzri.getSystemService("window")).getDefaultDisplay().getMetrics(paramMap);
    int i = paramMap.widthPixels;
    int j = paramMap.heightPixels;
    return zzgj.zzj(23 + i + "x" + j);
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */