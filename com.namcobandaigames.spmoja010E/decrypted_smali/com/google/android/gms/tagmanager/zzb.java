package com.google.android.gms.tagmanager;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;

final class zzb
  implements zzd
{
  zzb(zza paramzza) {}
  
  public final AdvertisingIdClient.Info zzna()
  {
    try
    {
      AdvertisingIdClient.Info localInfo = AdvertisingIdClient.getAdvertisingIdInfo(zza.zza(this.zzayv));
      return localInfo;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      zzdi.zzb("IllegalStateException getting Advertising Id Info", localIllegalStateException);
      return null;
    }
    catch (GooglePlayServicesRepairableException localGooglePlayServicesRepairableException)
    {
      zzdi.zzb("GooglePlayServicesRepairableException getting Advertising Id Info", localGooglePlayServicesRepairableException);
      return null;
    }
    catch (IOException localIOException)
    {
      zzdi.zzb("IOException getting Ad Id Info", localIOException);
      return null;
    }
    catch (GooglePlayServicesNotAvailableException localGooglePlayServicesNotAvailableException)
    {
      this.zzayv.close();
      zzdi.zzb("GooglePlayServicesNotAvailableException getting Advertising Id Info", localGooglePlayServicesNotAvailableException);
      return null;
    }
    catch (Exception localException)
    {
      zzdi.zzb("Unknown exception. Could not get the Advertising Id Info.", localException);
    }
    return null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */