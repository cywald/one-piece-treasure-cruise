package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public final class zzgf
{
  private Tracker zzrh;
  private Context zzri;
  private GoogleAnalytics zzrk;
  
  public zzgf(Context paramContext)
  {
    this.zzri = paramContext;
  }
  
  private final void zzdz(String paramString)
  {
    try
    {
      if (this.zzrk == null)
      {
        this.zzrk = GoogleAnalytics.getInstance(this.zzri);
        this.zzrk.setLogger(new zzgg());
        this.zzrh = this.zzrk.newTracker(paramString);
      }
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  public final Tracker zzdy(String paramString)
  {
    zzdz(paramString);
    return this.zzrh;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzgf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */