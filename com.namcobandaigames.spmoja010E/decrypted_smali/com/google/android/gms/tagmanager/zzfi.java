package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzru;
import com.google.android.gms.internal.measurement.zzry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class zzfi
{
  private final Set<zzry> zzbex = new HashSet();
  private final Map<zzry, List<zzru>> zzbfh = new HashMap();
  private final Map<zzry, List<zzru>> zzbfi = new HashMap();
  private final Map<zzry, List<String>> zzbfj = new HashMap();
  private final Map<zzry, List<String>> zzbfk = new HashMap();
  private zzru zzbfl;
  
  public final void zza(zzry paramzzry)
  {
    this.zzbex.add(paramzzry);
  }
  
  public final void zza(zzry paramzzry, zzru paramzzru)
  {
    List localList = (List)this.zzbfh.get(paramzzry);
    Object localObject = localList;
    if (localList == null)
    {
      localObject = new ArrayList();
      this.zzbfh.put(paramzzry, localObject);
    }
    ((List)localObject).add(paramzzru);
  }
  
  public final void zza(zzry paramzzry, String paramString)
  {
    List localList = (List)this.zzbfj.get(paramzzry);
    Object localObject = localList;
    if (localList == null)
    {
      localObject = new ArrayList();
      this.zzbfj.put(paramzzry, localObject);
    }
    ((List)localObject).add(paramString);
  }
  
  public final void zzb(zzru paramzzru)
  {
    this.zzbfl = paramzzru;
  }
  
  public final void zzb(zzry paramzzry, zzru paramzzru)
  {
    List localList = (List)this.zzbfi.get(paramzzry);
    Object localObject = localList;
    if (localList == null)
    {
      localObject = new ArrayList();
      this.zzbfi.put(paramzzry, localObject);
    }
    ((List)localObject).add(paramzzru);
  }
  
  public final void zzb(zzry paramzzry, String paramString)
  {
    List localList = (List)this.zzbfk.get(paramzzry);
    Object localObject = localList;
    if (localList == null)
    {
      localObject = new ArrayList();
      this.zzbfk.put(paramzzry, localObject);
    }
    ((List)localObject).add(paramString);
  }
  
  public final Set<zzry> zzpn()
  {
    return this.zzbex;
  }
  
  public final Map<zzry, List<zzru>> zzpo()
  {
    return this.zzbfh;
  }
  
  public final Map<zzry, List<String>> zzpp()
  {
    return this.zzbfj;
  }
  
  public final Map<zzry, List<String>> zzpq()
  {
    return this.zzbfk;
  }
  
  public final Map<zzry, List<zzru>> zzpr()
  {
    return this.zzbfi;
  }
  
  public final zzru zzps()
  {
    return this.zzbfl;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */