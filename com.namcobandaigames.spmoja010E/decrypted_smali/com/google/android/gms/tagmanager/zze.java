package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zze
  extends zzbq
{
  private static final String ID = com.google.android.gms.internal.measurement.zza.zza.toString();
  private final zza zzayw;
  
  public zze(Context paramContext)
  {
    this(zza.zzn(paramContext));
  }
  
  @VisibleForTesting
  private zze(zza paramzza)
  {
    super(ID, new String[0]);
    this.zzayw = paramzza;
    this.zzayw.zzmv();
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    paramMap = this.zzayw.zzmv();
    if (paramMap == null) {
      return zzgj.zzqg();
    }
    return zzgj.zzj(paramMap);
  }
  
  public final boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */