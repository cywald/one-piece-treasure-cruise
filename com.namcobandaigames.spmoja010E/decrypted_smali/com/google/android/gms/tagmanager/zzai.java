package com.google.android.gms.tagmanager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.VisibleForTesting;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Random;

public final class zzai
{
  private final String zzazf;
  private final Random zzbal;
  private final Context zzri;
  
  public zzai(Context paramContext, String paramString)
  {
    this(paramContext, paramString, new Random());
  }
  
  @VisibleForTesting
  private zzai(Context paramContext, String paramString, Random paramRandom)
  {
    this.zzri = ((Context)Preconditions.checkNotNull(paramContext));
    this.zzazf = ((String)Preconditions.checkNotNull(paramString));
    this.zzbal = paramRandom;
  }
  
  private final long zzd(long paramLong1, long paramLong2)
  {
    SharedPreferences localSharedPreferences = zznu();
    long l1 = Math.max(0L, localSharedPreferences.getLong("FORBIDDEN_COUNT", 0L));
    long l2 = Math.max(0L, localSharedPreferences.getLong("SUCCESSFUL_COUNT", 0L));
    paramLong2 = ((float)l1 / (float)(l2 + l1 + 1L) * (float)(paramLong2 - paramLong1));
    float f = this.zzbal.nextFloat();
    return ((float)(paramLong2 + paramLong1) * f);
  }
  
  private final SharedPreferences zznu()
  {
    Context localContext = this.zzri;
    String str1 = String.valueOf("_gtmContainerRefreshPolicy_");
    String str2 = String.valueOf(this.zzazf);
    if (str2.length() != 0) {}
    for (str1 = str1.concat(str2);; str1 = new String(str1)) {
      return localContext.getSharedPreferences(str1, 0);
    }
  }
  
  public final long zznq()
  {
    return 43200000L + zzd(7200000L, 259200000L);
  }
  
  public final long zznr()
  {
    return 3600000L + zzd(600000L, 86400000L);
  }
  
  @SuppressLint({"CommitPrefEdits"})
  public final void zzns()
  {
    Object localObject = zznu();
    long l1 = ((SharedPreferences)localObject).getLong("FORBIDDEN_COUNT", 0L);
    long l2 = ((SharedPreferences)localObject).getLong("SUCCESSFUL_COUNT", 0L);
    localObject = ((SharedPreferences)localObject).edit();
    if (l1 == 0L) {}
    for (l1 = 3L;; l1 = Math.min(10L, 1L + l1))
    {
      l2 = Math.max(0L, Math.min(l2, 10L - l1));
      ((SharedPreferences.Editor)localObject).putLong("FORBIDDEN_COUNT", l1);
      ((SharedPreferences.Editor)localObject).putLong("SUCCESSFUL_COUNT", l2);
      ((SharedPreferences.Editor)localObject).apply();
      return;
    }
  }
  
  @SuppressLint({"CommitPrefEdits"})
  public final void zznt()
  {
    Object localObject = zznu();
    long l2 = ((SharedPreferences)localObject).getLong("SUCCESSFUL_COUNT", 0L);
    long l1 = ((SharedPreferences)localObject).getLong("FORBIDDEN_COUNT", 0L);
    l2 = Math.min(10L, l2 + 1L);
    l1 = Math.max(0L, Math.min(l1, 10L - l2));
    localObject = ((SharedPreferences)localObject).edit();
    ((SharedPreferences.Editor)localObject).putLong("SUCCESSFUL_COUNT", l2);
    ((SharedPreferences.Editor)localObject).putLong("FORBIDDEN_COUNT", l1);
    ((SharedPreferences.Editor)localObject).apply();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */