package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;

public final class zzdi
{
  @VisibleForTesting
  private static zzdj zzbdc = new zzba();
  static int zzyn;
  
  public static void e(String paramString)
  {
    zzbdc.e(paramString);
  }
  
  public static void setLogLevel(int paramInt)
  {
    zzyn = paramInt;
    zzbdc.setLogLevel(paramInt);
  }
  
  public static void v(String paramString)
  {
    zzbdc.v(paramString);
  }
  
  public static void zza(String paramString, Throwable paramThrowable)
  {
    zzbdc.zza(paramString, paramThrowable);
  }
  
  public static void zzab(String paramString)
  {
    zzbdc.zzab(paramString);
  }
  
  public static void zzb(String paramString, Throwable paramThrowable)
  {
    zzbdc.zzb(paramString, paramThrowable);
  }
  
  public static void zzdi(String paramString)
  {
    zzbdc.zzdi(paramString);
  }
  
  public static void zzdj(String paramString)
  {
    zzbdc.zzdj(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */