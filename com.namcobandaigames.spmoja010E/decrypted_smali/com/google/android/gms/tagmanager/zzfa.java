package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzru;
import java.util.Set;

abstract interface zzfa
{
  public abstract void zzb(Set<zzru> paramSet);
  
  public abstract zzeq zzow();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */