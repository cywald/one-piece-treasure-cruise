package com.google.android.gms.tagmanager;

import android.util.Log;

public final class zzba
  implements zzdj
{
  private int zzyn = 5;
  
  public final void e(String paramString)
  {
    if (this.zzyn <= 6) {
      Log.e("GoogleTagManager", paramString);
    }
  }
  
  public final void setLogLevel(int paramInt)
  {
    this.zzyn = paramInt;
  }
  
  public final void v(String paramString)
  {
    if (this.zzyn <= 2) {
      Log.v("GoogleTagManager", paramString);
    }
  }
  
  public final void zza(String paramString, Throwable paramThrowable)
  {
    if (this.zzyn <= 6) {
      Log.e("GoogleTagManager", paramString, paramThrowable);
    }
  }
  
  public final void zzab(String paramString)
  {
    if (this.zzyn <= 5) {
      Log.w("GoogleTagManager", paramString);
    }
  }
  
  public final void zzb(String paramString, Throwable paramThrowable)
  {
    if (this.zzyn <= 5) {
      Log.w("GoogleTagManager", paramString, paramThrowable);
    }
  }
  
  public final void zzdi(String paramString)
  {
    if (this.zzyn <= 4) {
      Log.i("GoogleTagManager", paramString);
    }
  }
  
  public final void zzdj(String paramString)
  {
    if (this.zzyn <= 3) {
      Log.d("GoogleTagManager", paramString);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzba.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */