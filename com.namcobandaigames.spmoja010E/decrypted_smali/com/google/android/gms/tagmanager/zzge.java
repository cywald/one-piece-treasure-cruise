package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzge
  extends zzbq
{
  private static final String ID = zza.zzaf.toString();
  
  public zzge()
  {
    super(ID, new String[0]);
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    return zzgj.zzj(Long.valueOf(System.currentTimeMillis()));
  }
  
  public final boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzge.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */