package com.google.android.gms.tagmanager;

import android.content.Context;
import android.provider.Settings.Secure;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzdm
  extends zzbq
{
  private static final String ID = zza.zzao.toString();
  private final Context zzri;
  
  public zzdm(Context paramContext)
  {
    super(ID, new String[0]);
    this.zzri = paramContext;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    paramMap = Settings.Secure.getString(this.zzri.getContentResolver(), "android_id");
    if (paramMap == null) {
      return zzgj.zzqg();
    }
    return zzgj.zzj(paramMap);
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */