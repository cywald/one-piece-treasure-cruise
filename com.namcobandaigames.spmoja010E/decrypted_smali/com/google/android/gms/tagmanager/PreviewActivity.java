package com.google.android.gms.tagmanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public class PreviewActivity
  extends Activity
{
  public void onCreate(Bundle paramBundle)
  {
    try
    {
      super.onCreate(paramBundle);
      zzdi.zzdi("Preview activity");
      paramBundle = getIntent().getData();
      if (!TagManager.getInstance(this).zzb(paramBundle))
      {
        paramBundle = String.valueOf(paramBundle);
        paramBundle = String.valueOf(paramBundle).length() + 73 + "Cannot preview the app with the uri: " + paramBundle + ". Launching current version instead.";
        zzdi.zzab(paramBundle);
        localObject = new AlertDialog.Builder(this).create();
        ((AlertDialog)localObject).setTitle("Preview failure");
        ((AlertDialog)localObject).setMessage(paramBundle);
        ((AlertDialog)localObject).setButton(-1, "Continue", new zzeg(this));
        ((AlertDialog)localObject).show();
      }
      Object localObject = getPackageManager().getLaunchIntentForPackage(getPackageName());
      if (localObject != null)
      {
        paramBundle = String.valueOf(getPackageName());
        if (paramBundle.length() != 0) {}
        for (paramBundle = "Invoke the launch activity for package name: ".concat(paramBundle);; paramBundle = new String("Invoke the launch activity for package name: "))
        {
          zzdi.zzdi(paramBundle);
          startActivity((Intent)localObject);
          return;
        }
        paramBundle = "Calling preview threw an exception: ".concat(paramBundle);
      }
    }
    catch (Exception paramBundle)
    {
      paramBundle = String.valueOf(paramBundle.getMessage());
      if (paramBundle.length() == 0) {}
    }
    for (;;)
    {
      zzdi.e(paramBundle);
      return;
      paramBundle = String.valueOf(getPackageName());
      if (paramBundle.length() != 0) {}
      for (paramBundle = "No launch activity found for package name: ".concat(paramBundle);; paramBundle = new String("No launch activity found for package name: "))
      {
        zzdi.zzdi(paramBundle);
        return;
      }
      paramBundle = new String("Calling preview threw an exception: ");
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\PreviewActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */