package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzj
  extends zzbq
{
  private static final String ID = zza.zzd.toString();
  private final Context zzri;
  
  public zzj(Context paramContext)
  {
    super(ID, new String[0]);
    this.zzri = paramContext;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    try
    {
      paramMap = this.zzri.getPackageManager();
      paramMap = zzgj.zzj(paramMap.getApplicationLabel(paramMap.getApplicationInfo(this.zzri.getPackageName(), 0)).toString());
      return paramMap;
    }
    catch (PackageManager.NameNotFoundException paramMap)
    {
      zzdi.zza("App name is not found.", paramMap);
    }
    return zzgj.zzqg();
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */