package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Locale;
import java.util.Map;

@VisibleForTesting
public final class zzdd
  extends zzbq
{
  private static final String ID = zza.zzv.toString();
  
  public zzdd()
  {
    super(ID, new String[0]);
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    paramMap = Locale.getDefault();
    if (paramMap == null) {
      return zzgj.zzqg();
    }
    paramMap = paramMap.getLanguage();
    if (paramMap == null) {
      return zzgj.zzqg();
    }
    return zzgj.zzj(paramMap.toLowerCase());
  }
  
  public final boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */