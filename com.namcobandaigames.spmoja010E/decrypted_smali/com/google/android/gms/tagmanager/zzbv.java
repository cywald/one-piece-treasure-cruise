package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

final class zzbv
  extends zzbq
{
  private static final String ID = zza.zzal.toString();
  private static final String zzbbt = zzb.zzef.toString();
  private static final String zzbbv = zzb.zzik.toString();
  private static final String zzbby = zzb.zzdv.toString();
  
  public zzbv()
  {
    super(ID, new String[] { zzbbt });
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    Object localObject1 = (zzp)paramMap.get(zzbbt);
    if ((localObject1 == null) || (localObject1 == zzgj.zzqg())) {
      return zzgj.zzqg();
    }
    Object localObject2 = zzgj.zzc((zzp)localObject1);
    localObject1 = (zzp)paramMap.get(zzbby);
    if (localObject1 == null)
    {
      localObject1 = "MD5";
      paramMap = (zzp)paramMap.get(zzbbv);
      if (paramMap != null) {
        break label118;
      }
      paramMap = "text";
      label73:
      if (!"text".equals(paramMap)) {
        break label126;
      }
      paramMap = ((String)localObject2).getBytes();
    }
    for (;;)
    {
      try
      {
        localObject2 = MessageDigest.getInstance((String)localObject1);
        ((MessageDigest)localObject2).update(paramMap);
        paramMap = zzgj.zzj(zzo.encode(((MessageDigest)localObject2).digest()));
        return paramMap;
      }
      catch (NoSuchAlgorithmException paramMap)
      {
        label118:
        label126:
        paramMap = String.valueOf(localObject1);
        if (paramMap.length() == 0) {
          break label211;
        }
      }
      localObject1 = zzgj.zzc((zzp)localObject1);
      break;
      paramMap = zzgj.zzc(paramMap);
      break label73;
      if ("base16".equals(paramMap))
      {
        paramMap = zzo.decode((String)localObject2);
      }
      else
      {
        paramMap = String.valueOf(paramMap);
        if (paramMap.length() != 0) {}
        for (paramMap = "Hash: unknown input format: ".concat(paramMap);; paramMap = new String("Hash: unknown input format: "))
        {
          zzdi.e(paramMap);
          return zzgj.zzqg();
        }
      }
    }
    label211:
    for (paramMap = "Hash: unknown algorithm: ".concat(paramMap);; paramMap = new String("Hash: unknown algorithm: "))
    {
      zzdi.e(paramMap);
      return zzgj.zzqg();
    }
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzbv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */