package com.google.android.gms.tagmanager;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

final class zzet
  implements zzew
{
  zzet(zzes paramzzes) {}
  
  public final ScheduledExecutorService zzpg()
  {
    return Executors.newSingleThreadScheduledExecutor();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */