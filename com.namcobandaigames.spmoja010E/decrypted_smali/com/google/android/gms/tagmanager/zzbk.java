package com.google.android.gms.tagmanager;

import android.util.Base64;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzbk
  extends zzbq
{
  private static final String ID = zza.zzaj.toString();
  private static final String zzbbt = zzb.zzef.toString();
  private static final String zzbbu = zzb.zzjr.toString();
  private static final String zzbbv = zzb.zzik.toString();
  private static final String zzbbw = zzb.zzkb.toString();
  
  public zzbk()
  {
    super(ID, new String[] { zzbbt });
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    Object localObject = (zzp)paramMap.get(zzbbt);
    if ((localObject == null) || (localObject == zzgj.zzqg())) {
      return zzgj.zzqg();
    }
    String str2 = zzgj.zzc((zzp)localObject);
    localObject = (zzp)paramMap.get(zzbbv);
    String str1;
    label84:
    int i;
    if (localObject == null)
    {
      str1 = "text";
      localObject = (zzp)paramMap.get(zzbbw);
      if (localObject != null) {
        break label165;
      }
      localObject = "base16";
      int j = 2;
      paramMap = (zzp)paramMap.get(zzbbu);
      i = j;
      if (paramMap != null)
      {
        i = j;
        if (zzgj.zzg(paramMap).booleanValue()) {
          i = 3;
        }
      }
    }
    for (;;)
    {
      try
      {
        if ("text".equals(str1))
        {
          paramMap = str2.getBytes();
          if (!"base16".equals(localObject)) {
            break label288;
          }
          paramMap = zzo.encode(paramMap);
          return zzgj.zzj(paramMap);
          str1 = zzgj.zzc((zzp)localObject);
          break;
          label165:
          localObject = zzgj.zzc((zzp)localObject);
          break label84;
        }
        if ("base16".equals(str1))
        {
          paramMap = zzo.decode(str2);
          continue;
        }
        if ("base64".equals(str1))
        {
          paramMap = Base64.decode(str2, i);
          continue;
        }
        if ("base64url".equals(str1))
        {
          paramMap = Base64.decode(str2, i | 0x8);
          continue;
        }
        paramMap = String.valueOf(str1);
        if (paramMap.length() != 0)
        {
          paramMap = "Encode: unknown input format: ".concat(paramMap);
          zzdi.e(paramMap);
          return zzgj.zzqg();
        }
        paramMap = new String("Encode: unknown input format: ");
        continue;
        if (!"base64".equals(localObject)) {
          break label307;
        }
      }
      catch (IllegalArgumentException paramMap)
      {
        zzdi.e("Encode: invalid input:");
        return zzgj.zzqg();
      }
      label288:
      paramMap = Base64.encodeToString(paramMap, i);
      continue;
      label307:
      if (!"base64url".equals(localObject)) {
        break label329;
      }
      paramMap = Base64.encodeToString(paramMap, i | 0x8);
    }
    label329:
    paramMap = String.valueOf(localObject);
    if (paramMap.length() != 0) {}
    for (paramMap = "Encode: unknown output format: ".concat(paramMap);; paramMap = new String("Encode: unknown output format: "))
    {
      zzdi.e(paramMap);
      return zzgj.zzqg();
    }
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzbk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */