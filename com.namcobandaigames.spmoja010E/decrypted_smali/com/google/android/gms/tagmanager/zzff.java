package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzru;
import com.google.android.gms.internal.measurement.zzry;
import java.util.Set;

final class zzff
  implements zzfg
{
  zzff(zzfb paramzzfb) {}
  
  public final void zza(zzry paramzzry, Set<zzru> paramSet1, Set<zzru> paramSet2, zzeq paramzzeq)
  {
    paramSet1.addAll(paramzzry.zzsc());
    paramSet2.addAll(paramzzry.zzsd());
    paramzzeq.zzou();
    paramzzeq.zzov();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzff.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */