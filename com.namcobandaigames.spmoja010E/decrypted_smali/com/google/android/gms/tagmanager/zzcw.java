package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.Uri;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;

@VisibleForTesting
public class zzcw
{
  private static String zzbck;
  @VisibleForTesting
  static Map<String, String> zzbcl = new HashMap();
  
  public static void zzdq(String paramString)
  {
    try
    {
      zzbck = paramString;
      return;
    }
    finally {}
  }
  
  static void zzf(Context paramContext, String paramString)
  {
    zzft.zza(paramContext, "gtm_install_referrer", "referrer", paramString);
    zzh(paramContext, paramString);
  }
  
  /* Error */
  public static String zzg(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: getstatic 25	com/google/android/gms/tagmanager/zzcw:zzbck	Ljava/lang/String;
    //   3: ifnonnull +40 -> 43
    //   6: ldc 2
    //   8: monitorenter
    //   9: getstatic 25	com/google/android/gms/tagmanager/zzcw:zzbck	Ljava/lang/String;
    //   12: ifnonnull +28 -> 40
    //   15: aload_0
    //   16: ldc 29
    //   18: iconst_0
    //   19: invokevirtual 48	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   22: astore_0
    //   23: aload_0
    //   24: ifnull +27 -> 51
    //   27: aload_0
    //   28: ldc 31
    //   30: ldc 50
    //   32: invokeinterface 56 3 0
    //   37: putstatic 25	com/google/android/gms/tagmanager/zzcw:zzbck	Ljava/lang/String;
    //   40: ldc 2
    //   42: monitorexit
    //   43: getstatic 25	com/google/android/gms/tagmanager/zzcw:zzbck	Ljava/lang/String;
    //   46: aload_1
    //   47: invokestatic 59	com/google/android/gms/tagmanager/zzcw:zzv	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   50: areturn
    //   51: ldc 50
    //   53: putstatic 25	com/google/android/gms/tagmanager/zzcw:zzbck	Ljava/lang/String;
    //   56: goto -16 -> 40
    //   59: astore_0
    //   60: ldc 2
    //   62: monitorexit
    //   63: aload_0
    //   64: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	65	0	paramContext	Context
    //   0	65	1	paramString	String
    // Exception table:
    //   from	to	target	type
    //   9	23	59	finally
    //   27	40	59	finally
    //   40	43	59	finally
    //   51	56	59	finally
    //   60	63	59	finally
  }
  
  public static void zzh(Context paramContext, String paramString)
  {
    String str = zzv(paramString, "conv");
    if ((str != null) && (str.length() > 0))
    {
      zzbcl.put(str, paramString);
      zzft.zza(paramContext, "gtm_click_referrers", str, paramString);
    }
  }
  
  public static String zzv(String paramString1, String paramString2)
  {
    if (paramString2 == null)
    {
      if (paramString1.length() > 0) {
        return paramString1;
      }
      return null;
    }
    paramString1 = String.valueOf(paramString1);
    if (paramString1.length() != 0) {}
    for (paramString1 = "http://hostname/?".concat(paramString1);; paramString1 = new String("http://hostname/?")) {
      return Uri.parse(paramString1).getQueryParameter(paramString2);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzcw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */