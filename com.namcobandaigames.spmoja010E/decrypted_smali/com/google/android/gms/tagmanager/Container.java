package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzo;
import com.google.android.gms.internal.measurement.zzp;
import com.google.android.gms.internal.measurement.zzrs;
import com.google.android.gms.internal.measurement.zzrw;
import com.google.android.gms.internal.measurement.zzsa;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@VisibleForTesting
public class Container
{
  private final String zzazf;
  private final DataLayer zzazg;
  private zzfb zzazh;
  private Map<String, FunctionCallMacroCallback> zzazi = new HashMap();
  private Map<String, FunctionCallTagCallback> zzazj = new HashMap();
  private volatile long zzazk;
  private volatile String zzazl = "";
  private final Context zzri;
  
  Container(Context paramContext, DataLayer paramDataLayer, String paramString, long paramLong, zzo paramzzo)
  {
    this.zzri = paramContext;
    this.zzazg = paramDataLayer;
    this.zzazf = paramString;
    this.zzazk = paramLong;
    paramContext = paramzzo.zzqg;
    if (paramContext == null) {
      throw new NullPointerException();
    }
    try
    {
      paramDataLayer = zzrs.zza(paramContext);
      zza(paramDataLayer);
    }
    catch (zzsa paramDataLayer)
    {
      for (;;)
      {
        int j;
        int i;
        paramContext = String.valueOf(paramContext);
        paramDataLayer = paramDataLayer.toString();
        zzdi.e(String.valueOf(paramContext).length() + 46 + String.valueOf(paramDataLayer).length() + "Not loading resource: " + paramContext + " because it is invalid: " + paramDataLayer);
      }
      zzng().zzf(paramDataLayer);
    }
    if (paramzzo.zzqf != null)
    {
      paramContext = paramzzo.zzqf;
      paramDataLayer = new ArrayList();
      j = paramContext.length;
      i = 0;
      while (i < j)
      {
        paramDataLayer.add(paramContext[i]);
        i += 1;
      }
    }
  }
  
  Container(Context paramContext, DataLayer paramDataLayer, String paramString, long paramLong, zzrw paramzzrw)
  {
    this.zzri = paramContext;
    this.zzazg = paramDataLayer;
    this.zzazf = paramString;
    this.zzazk = 0L;
    zza(paramzzrw);
  }
  
  private final void zza(zzrw paramzzrw)
  {
    this.zzazl = paramzzrw.getVersion();
    Object localObject = this.zzazl;
    zzeh.zzpc().zzpd().equals(zzeh.zza.zzbdw);
    localObject = new zzdq();
    zza(new zzfb(this.zzri, paramzzrw, this.zzazg, new zza(null), new zzb(null), (zzbo)localObject));
    if (getBoolean("_gtm.loadEventEnabled")) {
      this.zzazg.pushEvent("gtm.load", DataLayer.mapOf(new Object[] { "gtm.id", this.zzazf }));
    }
  }
  
  private final void zza(zzfb paramzzfb)
  {
    try
    {
      this.zzazh = paramzzfb;
      return;
    }
    finally
    {
      paramzzfb = finally;
      throw paramzzfb;
    }
  }
  
  private final zzfb zzng()
  {
    try
    {
      zzfb localzzfb = this.zzazh;
      return localzzfb;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean getBoolean(String paramString)
  {
    zzfb localzzfb = zzng();
    if (localzzfb == null)
    {
      zzdi.e("getBoolean called for closed container.");
      return zzgj.zzqd().booleanValue();
    }
    try
    {
      boolean bool = zzgj.zzg((zzp)localzzfb.zzdv(paramString).getObject()).booleanValue();
      return bool;
    }
    catch (Exception paramString)
    {
      paramString = paramString.getMessage();
      zzdi.e(String.valueOf(paramString).length() + 66 + "Calling getBoolean() threw an exception: " + paramString + " Returning default value.");
    }
    return zzgj.zzqd().booleanValue();
  }
  
  public String getContainerId()
  {
    return this.zzazf;
  }
  
  public double getDouble(String paramString)
  {
    zzfb localzzfb = zzng();
    if (localzzfb == null)
    {
      zzdi.e("getDouble called for closed container.");
      return zzgj.zzqc().doubleValue();
    }
    try
    {
      double d = zzgj.zzf((zzp)localzzfb.zzdv(paramString).getObject()).doubleValue();
      return d;
    }
    catch (Exception paramString)
    {
      paramString = paramString.getMessage();
      zzdi.e(String.valueOf(paramString).length() + 65 + "Calling getDouble() threw an exception: " + paramString + " Returning default value.");
    }
    return zzgj.zzqc().doubleValue();
  }
  
  public long getLastRefreshTime()
  {
    return this.zzazk;
  }
  
  public long getLong(String paramString)
  {
    zzfb localzzfb = zzng();
    if (localzzfb == null)
    {
      zzdi.e("getLong called for closed container.");
      return zzgj.zzqb().longValue();
    }
    try
    {
      long l = zzgj.zze((zzp)localzzfb.zzdv(paramString).getObject()).longValue();
      return l;
    }
    catch (Exception paramString)
    {
      paramString = paramString.getMessage();
      zzdi.e(String.valueOf(paramString).length() + 63 + "Calling getLong() threw an exception: " + paramString + " Returning default value.");
    }
    return zzgj.zzqb().longValue();
  }
  
  public String getString(String paramString)
  {
    zzfb localzzfb = zzng();
    if (localzzfb == null)
    {
      zzdi.e("getString called for closed container.");
      return zzgj.zzqf();
    }
    try
    {
      paramString = zzgj.zzc((zzp)localzzfb.zzdv(paramString).getObject());
      return paramString;
    }
    catch (Exception paramString)
    {
      paramString = paramString.getMessage();
      zzdi.e(String.valueOf(paramString).length() + 65 + "Calling getString() threw an exception: " + paramString + " Returning default value.");
    }
    return zzgj.zzqf();
  }
  
  public boolean isDefault()
  {
    return getLastRefreshTime() == 0L;
  }
  
  public void registerFunctionCallMacroCallback(String paramString, FunctionCallMacroCallback paramFunctionCallMacroCallback)
  {
    if (paramFunctionCallMacroCallback == null) {
      throw new NullPointerException("Macro handler must be non-null");
    }
    synchronized (this.zzazi)
    {
      this.zzazi.put(paramString, paramFunctionCallMacroCallback);
      return;
    }
  }
  
  public void registerFunctionCallTagCallback(String paramString, FunctionCallTagCallback paramFunctionCallTagCallback)
  {
    if (paramFunctionCallTagCallback == null) {
      throw new NullPointerException("Tag callback must be non-null");
    }
    synchronized (this.zzazj)
    {
      this.zzazj.put(paramString, paramFunctionCallTagCallback);
      return;
    }
  }
  
  final void release()
  {
    this.zzazh = null;
  }
  
  public void unregisterFunctionCallMacroCallback(String paramString)
  {
    synchronized (this.zzazi)
    {
      this.zzazi.remove(paramString);
      return;
    }
  }
  
  public void unregisterFunctionCallTagCallback(String paramString)
  {
    synchronized (this.zzazj)
    {
      this.zzazj.remove(paramString);
      return;
    }
  }
  
  @VisibleForTesting
  final FunctionCallMacroCallback zzcy(String paramString)
  {
    synchronized (this.zzazi)
    {
      paramString = (FunctionCallMacroCallback)this.zzazi.get(paramString);
      return paramString;
    }
  }
  
  @VisibleForTesting
  public final FunctionCallTagCallback zzcz(String paramString)
  {
    synchronized (this.zzazj)
    {
      paramString = (FunctionCallTagCallback)this.zzazj.get(paramString);
      return paramString;
    }
  }
  
  @VisibleForTesting
  public final void zzda(String paramString)
  {
    zzng().zzda(paramString);
  }
  
  @VisibleForTesting
  public final String zznf()
  {
    return this.zzazl;
  }
  
  public static abstract interface FunctionCallMacroCallback
  {
    public abstract Object getValue(String paramString, Map<String, Object> paramMap);
  }
  
  public static abstract interface FunctionCallTagCallback
  {
    public abstract void execute(String paramString, Map<String, Object> paramMap);
  }
  
  final class zza
    implements zzan
  {
    private zza() {}
    
    public final Object zza(String paramString, Map<String, Object> paramMap)
    {
      Container.FunctionCallMacroCallback localFunctionCallMacroCallback = Container.this.zzcy(paramString);
      if (localFunctionCallMacroCallback == null) {
        return null;
      }
      return localFunctionCallMacroCallback.getValue(paramString, paramMap);
    }
  }
  
  final class zzb
    implements zzan
  {
    private zzb() {}
    
    public final Object zza(String paramString, Map<String, Object> paramMap)
    {
      Container.FunctionCallTagCallback localFunctionCallTagCallback = Container.this.zzcz(paramString);
      if (localFunctionCallTagCallback != null) {
        localFunctionCallTagCallback.execute(paramString, paramMap);
      }
      return zzgj.zzqf();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\Container.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */