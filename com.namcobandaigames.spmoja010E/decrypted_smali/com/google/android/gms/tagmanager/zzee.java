package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzee
  extends zzbq
{
  private static final String ID = zza.zzy.toString();
  private static final zzp zzbdo = zzgj.zzj("Android");
  
  public zzee()
  {
    super(ID, new String[0]);
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    return zzbdo;
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */