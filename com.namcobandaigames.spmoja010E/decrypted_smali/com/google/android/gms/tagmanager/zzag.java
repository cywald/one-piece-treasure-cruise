package com.google.android.gms.tagmanager;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.internal.measurement.zzo;

abstract interface zzag
  extends Releasable
{
  public abstract void zza(long paramLong, String paramString);
  
  public abstract void zza(zzdh<zzo> paramzzdh);
  
  public abstract void zzdc(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */