package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

final class zzl
  extends zzbq
{
  private static final String ID = zza.zzdl.toString();
  private final Context zzri;
  
  public zzl(Context paramContext)
  {
    super(ID, new String[0]);
    this.zzri = paramContext;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    try
    {
      paramMap = zzgj.zzj(this.zzri.getPackageManager().getPackageInfo(this.zzri.getPackageName(), 0).versionName);
      return paramMap;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      paramMap = this.zzri.getPackageName();
      String str = localNameNotFoundException.getMessage();
      zzdi.e(String.valueOf(paramMap).length() + 25 + String.valueOf(str).length() + "Package name " + paramMap + " not found. " + str);
    }
    return zzgj.zzqg();
  }
  
  public final boolean zznb()
  {
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */