package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzb;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class zzaz
  extends zzgh
{
  private static final String ID = zza.zzaq.toString();
  private static final String VALUE = zzb.zznh.toString();
  private static final String zzbbj = zzb.zzfd.toString();
  private final DataLayer zzazg;
  
  public zzaz(DataLayer paramDataLayer)
  {
    super(ID, new String[] { VALUE });
    this.zzazg = paramDataLayer;
  }
  
  public final void zzg(Map<String, zzp> paramMap)
  {
    Object localObject1 = (zzp)paramMap.get(VALUE);
    if ((localObject1 == null) || (localObject1 == zzgj.zzqa()))
    {
      paramMap = (zzp)paramMap.get(zzbbj);
      if ((paramMap != null) && (paramMap != zzgj.zzqa())) {
        break label110;
      }
    }
    label110:
    do
    {
      return;
      localObject1 = zzgj.zzh((zzp)localObject1);
      if (!(localObject1 instanceof List)) {
        break;
      }
      localObject1 = ((List)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = ((Iterator)localObject1).next();
        if ((localObject2 instanceof Map))
        {
          localObject2 = (Map)localObject2;
          this.zzazg.push((Map)localObject2);
        }
      }
      break;
      paramMap = zzgj.zzc(paramMap);
    } while (paramMap == zzgj.zzqf());
    this.zzazg.zzdd(paramMap);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzaz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */