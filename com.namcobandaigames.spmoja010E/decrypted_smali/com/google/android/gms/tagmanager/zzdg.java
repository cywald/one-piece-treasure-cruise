package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
final class zzdg
  implements zzej
{
  private final long zzabb = 900000L;
  private final int zzabc = 5;
  private double zzabd = Math.min(1, 5);
  private long zzabe;
  private final Object zzabf = new Object();
  private final long zzbdb = 5000L;
  private final Clock zzrz;
  private final String zzul;
  
  public zzdg(int paramInt1, int paramInt2, long paramLong1, long paramLong2, String paramString, Clock paramClock)
  {
    this.zzul = paramString;
    this.zzrz = paramClock;
  }
  
  public final boolean zzew()
  {
    synchronized (this.zzabf)
    {
      long l = this.zzrz.currentTimeMillis();
      if (l - this.zzabe < this.zzbdb)
      {
        String str1 = this.zzul;
        zzdi.zzab(String.valueOf(str1).length() + 34 + "Excessive " + str1 + " detected; call ignored.");
        return false;
      }
      if (this.zzabd < this.zzabc)
      {
        double d = (l - this.zzabe) / this.zzabb;
        if (d > 0.0D) {
          this.zzabd = Math.min(this.zzabc, d + this.zzabd);
        }
      }
      this.zzabe = l;
      if (this.zzabd >= 1.0D)
      {
        this.zzabd -= 1.0D;
        return true;
      }
    }
    String str2 = this.zzul;
    zzdi.zzab(String.valueOf(str2).length() + 34 + "Excessive " + str2 + " detected; call ignored.");
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzdg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */