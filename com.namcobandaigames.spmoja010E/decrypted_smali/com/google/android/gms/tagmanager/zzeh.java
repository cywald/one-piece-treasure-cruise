package com.google.android.gms.tagmanager;

import android.net.Uri;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@VisibleForTesting
class zzeh
{
  private static zzeh zzbdq;
  private volatile String zzazf = null;
  private volatile zza zzbdr = zza.zzbdu;
  private volatile String zzbds = null;
  private volatile String zzbdt = null;
  
  private static String zzdt(String paramString)
  {
    return paramString.split("&")[0].split("=")[1];
  }
  
  static zzeh zzpc()
  {
    try
    {
      if (zzbdq == null) {
        zzbdq = new zzeh();
      }
      zzeh localzzeh = zzbdq;
      return localzzeh;
    }
    finally {}
  }
  
  final String getContainerId()
  {
    return this.zzazf;
  }
  
  final boolean zzb(Uri paramUri)
  {
    boolean bool = true;
    String str2;
    try
    {
      str2 = URLDecoder.decode(paramUri.toString(), "UTF-8");
      if (!str2.matches("^tagmanager.c.\\S+:\\/\\/preview\\/p\\?id=\\S+&gtm_auth=\\S+&gtm_preview=\\d+(&gtm_debug=x)?$")) {
        break label182;
      }
      str1 = String.valueOf(str2);
      if (str1.length() == 0) {
        break label154;
      }
      str1 = "Container preview url: ".concat(str1);
    }
    catch (UnsupportedEncodingException paramUri)
    {
      for (;;)
      {
        label128:
        bool = false;
        continue;
        label154:
        String str1 = new String("Container preview url: ");
      }
    }
    finally {}
    zzdi.v(str1);
    if (str2.matches(".*?&gtm_debug=x$"))
    {
      this.zzbdr = zza.zzbdw;
      this.zzbdt = paramUri.getQuery().replace("&gtm_debug=x", "");
      if ((this.zzbdr == zza.zzbdv) || (this.zzbdr == zza.zzbdw))
      {
        paramUri = String.valueOf("/r?");
        str1 = String.valueOf(this.zzbdt);
        if (str1.length() == 0) {
          break label305;
        }
        paramUri = paramUri.concat(str1);
        this.zzbds = paramUri;
      }
      this.zzazf = zzdt(this.zzbdt);
    }
    for (;;)
    {
      return bool;
      this.zzbdr = zza.zzbdv;
      break;
      label182:
      if (str2.matches("^tagmanager.c.\\S+:\\/\\/preview\\/p\\?id=\\S+&gtm_preview=$"))
      {
        if (zzdt(paramUri.getQuery()).equals(this.zzazf))
        {
          paramUri = String.valueOf(this.zzazf);
          if (paramUri.length() != 0) {}
          for (paramUri = "Exit preview mode for container: ".concat(paramUri);; paramUri = new String("Exit preview mode for container: "))
          {
            zzdi.v(paramUri);
            this.zzbdr = zza.zzbdu;
            this.zzbds = null;
            break;
          }
        }
      }
      else
      {
        paramUri = String.valueOf(str2);
        if (paramUri.length() != 0) {}
        for (paramUri = "Invalid preview uri: ".concat(paramUri);; paramUri = new String("Invalid preview uri: "))
        {
          zzdi.zzab(paramUri);
          bool = false;
          break;
        }
        label305:
        paramUri = new String(paramUri);
        break label128;
      }
      bool = false;
    }
  }
  
  final zza zzpd()
  {
    return this.zzbdr;
  }
  
  final String zzpe()
  {
    return this.zzbds;
  }
  
  static enum zza
  {
    private zza() {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzeh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */