package com.google.android.gms.tagmanager;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zza;
import com.google.android.gms.internal.measurement.zzp;
import java.util.Map;

@VisibleForTesting
final class zzbp
  extends zzbq
{
  private static final String ID = zza.zzr.toString();
  private final zzfb zzazh;
  
  public zzbp(zzfb paramzzfb)
  {
    super(ID, new String[0]);
    this.zzazh = paramzzfb;
  }
  
  public final zzp zze(Map<String, zzp> paramMap)
  {
    paramMap = this.zzazh.zzpj();
    if (paramMap == null) {
      return zzgj.zzqg();
    }
    return zzgj.zzj(paramMap);
  }
  
  public final boolean zznb()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzbp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */