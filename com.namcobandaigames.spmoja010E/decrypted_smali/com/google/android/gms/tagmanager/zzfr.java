package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

final class zzfr
  implements zzfq
{
  private Handler handler = new Handler(zzfn.zza(this.zzbfz).getMainLooper(), new zzfs(this));
  
  private zzfr(zzfn paramzzfn) {}
  
  private final Message obtainMessage()
  {
    return this.handler.obtainMessage(1, zzfn.zzpw());
  }
  
  public final void cancel()
  {
    this.handler.removeMessages(1, zzfn.zzpw());
  }
  
  public final void zzh(long paramLong)
  {
    this.handler.removeMessages(1, zzfn.zzpw());
    this.handler.sendMessageDelayed(obtainMessage(), paramLong);
  }
  
  public final void zzpx()
  {
    this.handler.removeMessages(1, zzfn.zzpw());
    this.handler.sendMessage(obtainMessage());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzfr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */