package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.zzp;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

abstract class zzbq
{
  private final Set<String> zzbbx;
  private final String zzqn;
  
  public zzbq(String paramString, String... paramVarArgs)
  {
    this.zzqn = paramString;
    this.zzbbx = new HashSet(paramVarArgs.length);
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      paramString = paramVarArgs[i];
      this.zzbbx.add(paramString);
      i += 1;
    }
  }
  
  final boolean zza(Set<String> paramSet)
  {
    return paramSet.containsAll(this.zzbbx);
  }
  
  public abstract zzp zze(Map<String, zzp> paramMap);
  
  public abstract boolean zznb();
  
  public String zzok()
  {
    return this.zzqn;
  }
  
  public Set<String> zzol()
  {
    return this.zzbbx;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\tagmanager\zzbq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */