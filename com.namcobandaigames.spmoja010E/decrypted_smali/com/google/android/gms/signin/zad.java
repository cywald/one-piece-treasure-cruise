package com.google.android.gms.signin;

import com.google.android.gms.common.api.Api.Client;
import com.google.android.gms.common.internal.IAccountAccessor;

public abstract interface zad
  extends Api.Client
{
  public abstract void connect();
  
  public abstract void zaa(IAccountAccessor paramIAccountAccessor, boolean paramBoolean);
  
  public abstract void zaa(com.google.android.gms.signin.internal.zad paramzad);
  
  public abstract void zacv();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\signin\zad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */