package com.google.android.gms.drive;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties.zza;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.drive.zzhp;
import com.google.android.gms.internal.drive.zzic;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public final class MetadataChangeSet
{
  public static final int CUSTOM_PROPERTY_SIZE_LIMIT_BYTES = 124;
  public static final int INDEXABLE_TEXT_SIZE_LIMIT_BYTES = 131072;
  public static final int MAX_PRIVATE_PROPERTIES_PER_RESOURCE_PER_APP = 30;
  public static final int MAX_PUBLIC_PROPERTIES_PER_RESOURCE = 30;
  public static final int MAX_TOTAL_PROPERTIES_PER_RESOURCE = 100;
  public static final MetadataChangeSet zzav = new MetadataChangeSet(MetadataBundle.zzaw());
  private final MetadataBundle zzaw;
  
  public MetadataChangeSet(MetadataBundle paramMetadataBundle)
  {
    this.zzaw = paramMetadataBundle.zzax();
  }
  
  public final Map<CustomPropertyKey, String> getCustomPropertyChangeMap()
  {
    AppVisibleCustomProperties localAppVisibleCustomProperties = (AppVisibleCustomProperties)this.zzaw.zza(zzhp.zzix);
    if (localAppVisibleCustomProperties == null) {
      return Collections.emptyMap();
    }
    return localAppVisibleCustomProperties.zzas();
  }
  
  @Nullable
  public final String getDescription()
  {
    return (String)this.zzaw.zza(zzhp.zziy);
  }
  
  @Nullable
  public final String getIndexableText()
  {
    return (String)this.zzaw.zza(zzhp.zzje);
  }
  
  @Nullable
  public final Date getLastViewedByMeDate()
  {
    return (Date)this.zzaw.zza(zzic.zzko);
  }
  
  @Nullable
  public final String getMimeType()
  {
    return (String)this.zzaw.zza(zzhp.zzjs);
  }
  
  @Nullable
  @KeepForSdk
  public final Bitmap getThumbnail()
  {
    BitmapTeleporter localBitmapTeleporter = (BitmapTeleporter)this.zzaw.zza(zzhp.zzka);
    if (localBitmapTeleporter == null) {
      return null;
    }
    return localBitmapTeleporter.get();
  }
  
  @Nullable
  public final String getTitle()
  {
    return (String)this.zzaw.zza(zzhp.zzkb);
  }
  
  @Nullable
  public final Boolean isPinned()
  {
    return (Boolean)this.zzaw.zza(zzhp.zzjk);
  }
  
  @Nullable
  public final Boolean isStarred()
  {
    return (Boolean)this.zzaw.zza(zzhp.zzjz);
  }
  
  @Nullable
  public final Boolean isViewed()
  {
    return (Boolean)this.zzaw.zza(zzhp.zzjr);
  }
  
  public final MetadataBundle zzp()
  {
    return this.zzaw;
  }
  
  public static class Builder
  {
    private final MetadataBundle zzaw = MetadataBundle.zzaw();
    private AppVisibleCustomProperties.zza zzax;
    
    private static void zza(String paramString, int paramInt1, int paramInt2)
    {
      if (paramInt2 <= paramInt1) {}
      for (boolean bool = true;; bool = false)
      {
        Preconditions.checkArgument(bool, String.format(Locale.US, "%s must be no more than %d bytes, but is %d bytes.", new Object[] { paramString, Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }));
        return;
      }
    }
    
    private static int zzb(@Nullable String paramString)
    {
      if (paramString == null) {
        return 0;
      }
      return paramString.getBytes().length;
    }
    
    private final AppVisibleCustomProperties.zza zzq()
    {
      if (this.zzax == null) {
        this.zzax = new AppVisibleCustomProperties.zza();
      }
      return this.zzax;
    }
    
    public MetadataChangeSet build()
    {
      if (this.zzax != null) {
        this.zzaw.zzb(zzhp.zzix, this.zzax.zzat());
      }
      return new MetadataChangeSet(this.zzaw);
    }
    
    public Builder deleteCustomProperty(CustomPropertyKey paramCustomPropertyKey)
    {
      Preconditions.checkNotNull(paramCustomPropertyKey, "key");
      zzq().zza(paramCustomPropertyKey, null);
      return this;
    }
    
    public Builder setCustomProperty(CustomPropertyKey paramCustomPropertyKey, String paramString)
    {
      Preconditions.checkNotNull(paramCustomPropertyKey, "key");
      Preconditions.checkNotNull(paramString, "value");
      zza("The total size of key string and value string of a custom property", 124, zzb(paramCustomPropertyKey.getKey()) + zzb(paramString));
      zzq().zza(paramCustomPropertyKey, paramString);
      return this;
    }
    
    public Builder setDescription(String paramString)
    {
      this.zzaw.zzb(zzhp.zziy, paramString);
      return this;
    }
    
    public Builder setIndexableText(String paramString)
    {
      zza("Indexable text size", 131072, zzb(paramString));
      this.zzaw.zzb(zzhp.zzje, paramString);
      return this;
    }
    
    public Builder setLastViewedByMeDate(Date paramDate)
    {
      this.zzaw.zzb(zzic.zzko, paramDate);
      return this;
    }
    
    public Builder setMimeType(@NonNull String paramString)
    {
      Preconditions.checkNotNull(paramString);
      this.zzaw.zzb(zzhp.zzjs, paramString);
      return this;
    }
    
    public Builder setPinned(boolean paramBoolean)
    {
      this.zzaw.zzb(zzhp.zzjk, Boolean.valueOf(paramBoolean));
      return this;
    }
    
    public Builder setStarred(boolean paramBoolean)
    {
      this.zzaw.zzb(zzhp.zzjz, Boolean.valueOf(paramBoolean));
      return this;
    }
    
    public Builder setTitle(@NonNull String paramString)
    {
      Preconditions.checkNotNull(paramString, "Title cannot be null.");
      this.zzaw.zzb(zzhp.zzkb, paramString);
      return this;
    }
    
    public Builder setViewed()
    {
      this.zzaw.zzb(zzhp.zzjr, Boolean.valueOf(true));
      return this;
    }
    
    @Deprecated
    public Builder setViewed(boolean paramBoolean)
    {
      if (paramBoolean) {
        this.zzaw.zzb(zzhp.zzjr, Boolean.valueOf(true));
      }
      while (!this.zzaw.zzd(zzhp.zzjr)) {
        return this;
      }
      this.zzaw.zzc(zzhp.zzjr);
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\MetadataChangeSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */