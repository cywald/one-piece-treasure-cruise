package com.google.android.gms.drive;

import android.content.IntentSender;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.drive.query.internal.FilterHolder;
import com.google.android.gms.drive.query.internal.zzk;
import com.google.android.gms.internal.drive.zzaw;
import com.google.android.gms.internal.drive.zzeo;
import com.google.android.gms.internal.drive.zzgg;

@Deprecated
public class OpenFileActivityBuilder
{
  public static final String EXTRA_RESPONSE_DRIVE_ID = "response_drive_id";
  private String zzay;
  private String[] zzaz;
  private Filter zzba;
  private DriveId zzbb;
  
  public IntentSender build(GoogleApiClient paramGoogleApiClient)
  {
    Preconditions.checkState(paramGoogleApiClient.isConnected(), "Client must be connected");
    zzf();
    if (this.zzba == null) {}
    for (FilterHolder localFilterHolder = null;; localFilterHolder = new FilterHolder(this.zzba)) {
      try
      {
        paramGoogleApiClient = ((zzeo)((zzaw)paramGoogleApiClient.getClient(Drive.CLIENT_KEY)).getService()).zza(new zzgg(this.zzay, this.zzaz, this.zzbb, localFilterHolder));
        return paramGoogleApiClient;
      }
      catch (RemoteException paramGoogleApiClient)
      {
        throw new RuntimeException("Unable to connect Drive Play Service", paramGoogleApiClient);
      }
    }
  }
  
  final String getTitle()
  {
    return this.zzay;
  }
  
  public OpenFileActivityBuilder setActivityStartFolder(DriveId paramDriveId)
  {
    this.zzbb = ((DriveId)Preconditions.checkNotNull(paramDriveId));
    return this;
  }
  
  public OpenFileActivityBuilder setActivityTitle(String paramString)
  {
    this.zzay = ((String)Preconditions.checkNotNull(paramString));
    return this;
  }
  
  public OpenFileActivityBuilder setMimeType(String[] paramArrayOfString)
  {
    if (paramArrayOfString != null) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool, "mimeTypes may not be null");
      this.zzaz = paramArrayOfString;
      return this;
    }
  }
  
  public OpenFileActivityBuilder setSelectionFilter(Filter paramFilter)
  {
    boolean bool2 = true;
    if (paramFilter != null)
    {
      bool1 = true;
      Preconditions.checkArgument(bool1, "filter may not be null");
      if (zzk.zza(paramFilter)) {
        break label41;
      }
    }
    label41:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      Preconditions.checkArgument(bool1, "FullTextSearchFilter cannot be used as a selection filter");
      this.zzba = paramFilter;
      return this;
      bool1 = false;
      break;
    }
  }
  
  final void zzf()
  {
    if (this.zzaz == null) {
      this.zzaz = new String[0];
    }
    if ((this.zzaz.length > 0) && (this.zzba != null)) {
      throw new IllegalStateException("Cannot use a selection filter and set mimetypes simultaneously");
    }
  }
  
  final String[] zzr()
  {
    return this.zzaz;
  }
  
  final Filter zzs()
  {
    return this.zzba;
  }
  
  final DriveId zzt()
  {
    return this.zzbb;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\OpenFileActivityBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */