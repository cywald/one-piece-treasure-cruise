package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

@KeepForSdk
@SafeParcelable.Class(creator="ContentsCreator")
@SafeParcelable.Reserved({1})
public class Contents
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Contents> CREATOR = new zzc();
  @SafeParcelable.Field(id=4)
  private final int mode;
  @SafeParcelable.Field(id=2)
  private final ParcelFileDescriptor zzi;
  @SafeParcelable.Field(id=3)
  final int zzj;
  @SafeParcelable.Field(id=5)
  private final DriveId zzk;
  @SafeParcelable.Field(id=7)
  private final boolean zzl;
  @Nullable
  @SafeParcelable.Field(id=8)
  private final String zzm;
  
  @SafeParcelable.Constructor
  public Contents(@SafeParcelable.Param(id=2) ParcelFileDescriptor paramParcelFileDescriptor, @SafeParcelable.Param(id=3) int paramInt1, @SafeParcelable.Param(id=4) int paramInt2, @SafeParcelable.Param(id=5) DriveId paramDriveId, @SafeParcelable.Param(id=7) boolean paramBoolean, @Nullable @SafeParcelable.Param(id=8) String paramString)
  {
    this.zzi = paramParcelFileDescriptor;
    this.zzj = paramInt1;
    this.mode = paramInt2;
    this.zzk = paramDriveId;
    this.zzl = paramBoolean;
    this.zzm = paramString;
  }
  
  public final DriveId getDriveId()
  {
    return this.zzk;
  }
  
  public final InputStream getInputStream()
  {
    return new FileInputStream(this.zzi.getFileDescriptor());
  }
  
  public final int getMode()
  {
    return this.mode;
  }
  
  public final OutputStream getOutputStream()
  {
    return new FileOutputStream(this.zzi.getFileDescriptor());
  }
  
  @KeepForSdk
  public ParcelFileDescriptor getParcelFileDescriptor()
  {
    return this.zzi;
  }
  
  public final int getRequestId()
  {
    return this.zzj;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzi, paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 3, this.zzj);
    SafeParcelWriter.writeInt(paramParcel, 4, this.mode);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzk, paramInt, false);
    SafeParcelWriter.writeBoolean(paramParcel, 7, this.zzl);
    SafeParcelWriter.writeString(paramParcel, 8, this.zzm, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final boolean zza()
  {
    return this.zzl;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\Contents.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */