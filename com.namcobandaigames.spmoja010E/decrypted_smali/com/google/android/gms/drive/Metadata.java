package com.google.android.gms.drive;

import android.support.annotation.Nullable;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.internal.drive.zzhp;
import com.google.android.gms.internal.drive.zzic;
import com.google.android.gms.internal.drive.zzik;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

public abstract class Metadata
  implements Freezable<Metadata>
{
  public static final int CONTENT_AVAILABLE_LOCALLY = 1;
  public static final int CONTENT_NOT_AVAILABLE_LOCALLY = 0;
  
  public String getAlternateLink()
  {
    return (String)zza(zzhp.zziw);
  }
  
  public int getContentAvailability()
  {
    Integer localInteger = (Integer)zza(zzik.zzku);
    if (localInteger == null) {
      return 0;
    }
    return localInteger.intValue();
  }
  
  public Date getCreatedDate()
  {
    return (Date)zza(zzic.zzkn);
  }
  
  public Map<CustomPropertyKey, String> getCustomProperties()
  {
    AppVisibleCustomProperties localAppVisibleCustomProperties = (AppVisibleCustomProperties)zza(zzhp.zzix);
    if (localAppVisibleCustomProperties == null) {
      return Collections.emptyMap();
    }
    return localAppVisibleCustomProperties.zzas();
  }
  
  public String getDescription()
  {
    return (String)zza(zzhp.zziy);
  }
  
  public DriveId getDriveId()
  {
    return (DriveId)zza(zzhp.zziv);
  }
  
  public String getEmbedLink()
  {
    return (String)zza(zzhp.zziz);
  }
  
  public String getFileExtension()
  {
    return (String)zza(zzhp.zzja);
  }
  
  public long getFileSize()
  {
    return ((Long)zza(zzhp.zzjb)).longValue();
  }
  
  @Nullable
  public Date getLastViewedByMeDate()
  {
    return (Date)zza(zzic.zzko);
  }
  
  public String getMimeType()
  {
    return (String)zza(zzhp.zzjs);
  }
  
  @Nullable
  public Date getModifiedByMeDate()
  {
    return (Date)zza(zzic.zzkq);
  }
  
  public Date getModifiedDate()
  {
    return (Date)zza(zzic.zzkp);
  }
  
  public String getOriginalFilename()
  {
    return (String)zza(zzhp.zzjt);
  }
  
  public long getQuotaBytesUsed()
  {
    return ((Long)zza(zzhp.zzjy)).longValue();
  }
  
  @Nullable
  public Date getSharedWithMeDate()
  {
    return (Date)zza(zzic.zzkr);
  }
  
  public String getTitle()
  {
    return (String)zza(zzhp.zzkb);
  }
  
  public String getWebContentLink()
  {
    return (String)zza(zzhp.zzkd);
  }
  
  public String getWebViewLink()
  {
    return (String)zza(zzhp.zzke);
  }
  
  public boolean isEditable()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzjh);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isExplicitlyTrashed()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzji);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isFolder()
  {
    return "application/vnd.google-apps.folder".equals(getMimeType());
  }
  
  public boolean isInAppFolder()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzjf);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isPinnable()
  {
    Boolean localBoolean = (Boolean)zza(zzik.zzkv);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isPinned()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzjk);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isRestricted()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzjm);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isShared()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzjn);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isStarred()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzjz);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isTrashable()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzjq);
    if (localBoolean == null) {
      return true;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isTrashed()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzkc);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isViewed()
  {
    Boolean localBoolean = (Boolean)zza(zzhp.zzjr);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public abstract <T> T zza(MetadataField<T> paramMetadataField);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\Metadata.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */