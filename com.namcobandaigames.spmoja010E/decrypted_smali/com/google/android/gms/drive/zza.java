package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Base64;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.internal.drive.zzhm;
import com.google.android.gms.internal.drive.zzix;

@SafeParcelable.Class(creator="ChangeSequenceNumberCreator")
@SafeParcelable.Reserved({1})
public class zza
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zza> CREATOR = new zzb();
  @SafeParcelable.Field(id=2)
  private final long zze;
  @SafeParcelable.Field(id=3)
  private final long zzf;
  @SafeParcelable.Field(id=4)
  private final long zzg;
  private volatile String zzh = null;
  
  @SafeParcelable.Constructor
  public zza(@SafeParcelable.Param(id=2) long paramLong1, @SafeParcelable.Param(id=3) long paramLong2, @SafeParcelable.Param(id=4) long paramLong3)
  {
    if (paramLong1 != -1L)
    {
      bool1 = true;
      Preconditions.checkArgument(bool1);
      if (paramLong2 == -1L) {
        break label85;
      }
      bool1 = true;
      label39:
      Preconditions.checkArgument(bool1);
      if (paramLong3 == -1L) {
        break label91;
      }
    }
    label85:
    label91:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      Preconditions.checkArgument(bool1);
      this.zze = paramLong1;
      this.zzf = paramLong2;
      this.zzg = paramLong3;
      return;
      bool1 = false;
      break;
      bool1 = false;
      break label39;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != zza.class)) {}
    do
    {
      return false;
      paramObject = (zza)paramObject;
    } while ((((zza)paramObject).zzf != this.zzf) || (((zza)paramObject).zzg != this.zzg) || (((zza)paramObject).zze != this.zze));
    return true;
  }
  
  public int hashCode()
  {
    String str1 = String.valueOf(this.zze);
    String str2 = String.valueOf(this.zzf);
    String str3 = String.valueOf(this.zzg);
    return (String.valueOf(str1).length() + String.valueOf(str2).length() + String.valueOf(str3).length() + str1 + str2 + str3).hashCode();
  }
  
  public String toString()
  {
    String str;
    if (this.zzh == null)
    {
      localObject = new zzhm();
      ((zzhm)localObject).versionCode = 1;
      ((zzhm)localObject).zze = this.zze;
      ((zzhm)localObject).zzf = this.zzf;
      ((zzhm)localObject).zzg = this.zzg;
      str = Base64.encodeToString(zzix.zza((zzix)localObject), 10);
      localObject = String.valueOf("ChangeSequenceNumber:");
      str = String.valueOf(str);
      if (str.length() == 0) {
        break label88;
      }
    }
    label88:
    for (Object localObject = ((String)localObject).concat(str);; localObject = new String((String)localObject))
    {
      this.zzh = ((String)localObject);
      return this.zzh;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeLong(paramParcel, 2, this.zze);
    SafeParcelWriter.writeLong(paramParcel, 3, this.zzf);
    SafeParcelWriter.writeLong(paramParcel, 4, this.zzg);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */