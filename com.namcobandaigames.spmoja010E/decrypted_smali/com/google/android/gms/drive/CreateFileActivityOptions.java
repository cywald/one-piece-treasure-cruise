package com.google.android.gms.drive;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.drive.zzq;

public final class CreateFileActivityOptions
  extends zzq
{
  public static final String EXTRA_RESPONSE_DRIVE_ID = "response_drive_id";
  
  private CreateFileActivityOptions(MetadataBundle paramMetadataBundle, Integer paramInteger, String paramString, DriveId paramDriveId, int paramInt)
  {
    super(paramMetadataBundle, paramInteger, paramString, paramDriveId, paramInt);
  }
  
  public static class Builder
  {
    protected final CreateFileActivityBuilder builder = new CreateFileActivityBuilder();
    
    public CreateFileActivityOptions build()
    {
      this.builder.zzf();
      return new CreateFileActivityOptions(this.builder.zzb().zzp(), Integer.valueOf(this.builder.getRequestId()), this.builder.zzd(), this.builder.zzc(), this.builder.zze(), null);
    }
    
    public Builder setActivityStartFolder(@NonNull DriveId paramDriveId)
    {
      this.builder.setActivityStartFolder(paramDriveId);
      return this;
    }
    
    public Builder setActivityTitle(@NonNull String paramString)
    {
      this.builder.setActivityTitle(paramString);
      return this;
    }
    
    public Builder setInitialDriveContents(@Nullable DriveContents paramDriveContents)
    {
      this.builder.setInitialDriveContents(paramDriveContents);
      return this;
    }
    
    public Builder setInitialMetadata(@NonNull MetadataChangeSet paramMetadataChangeSet)
    {
      this.builder.setInitialMetadata(paramMetadataChangeSet);
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\CreateFileActivityOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */