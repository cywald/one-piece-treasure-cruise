package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="DriveFileRangeCreator")
@SafeParcelable.Reserved({1})
public final class zzh
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzh> CREATOR = new zzi();
  @SafeParcelable.Field(id=3)
  private final long zzaa;
  @SafeParcelable.Field(id=2)
  private final long zzz;
  
  @SafeParcelable.Constructor
  public zzh(@SafeParcelable.Param(id=2) long paramLong1, @SafeParcelable.Param(id=3) long paramLong2)
  {
    this.zzz = paramLong1;
    this.zzaa = paramLong2;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeLong(paramParcel, 2, this.zzz);
    SafeParcelWriter.writeLong(paramParcel, 3, this.zzaa);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */