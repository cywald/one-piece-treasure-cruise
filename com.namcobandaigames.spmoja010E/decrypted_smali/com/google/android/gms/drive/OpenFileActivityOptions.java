package com.google.android.gms.drive;

import android.support.annotation.NonNull;
import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.drive.query.internal.FilterHolder;
import java.util.List;

public final class OpenFileActivityOptions
{
  public static final String EXTRA_RESPONSE_DRIVE_ID = "response_drive_id";
  public final String zzay;
  public final String[] zzaz;
  public final DriveId zzbb;
  public final FilterHolder zzbc;
  
  private OpenFileActivityOptions(String paramString, String[] paramArrayOfString, Filter paramFilter, DriveId paramDriveId)
  {
    this.zzay = paramString;
    this.zzaz = paramArrayOfString;
    if (paramFilter == null) {}
    for (paramString = null;; paramString = new FilterHolder(paramFilter))
    {
      this.zzbc = paramString;
      this.zzbb = paramDriveId;
      return;
    }
  }
  
  public static class Builder
  {
    private final OpenFileActivityBuilder zzbd = new OpenFileActivityBuilder();
    
    public OpenFileActivityOptions build()
    {
      this.zzbd.zzf();
      return new OpenFileActivityOptions(this.zzbd.getTitle(), this.zzbd.zzr(), this.zzbd.zzs(), this.zzbd.zzt(), null);
    }
    
    public Builder setActivityStartFolder(DriveId paramDriveId)
    {
      this.zzbd.setActivityStartFolder(paramDriveId);
      return this;
    }
    
    public Builder setActivityTitle(@NonNull String paramString)
    {
      this.zzbd.setActivityTitle(paramString);
      return this;
    }
    
    public Builder setMimeType(@NonNull List<String> paramList)
    {
      this.zzbd.setMimeType((String[])paramList.toArray(new String[0]));
      return this;
    }
    
    public Builder setSelectionFilter(@NonNull Filter paramFilter)
    {
      this.zzbd.setSelectionFilter(paramFilter);
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\OpenFileActivityOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */