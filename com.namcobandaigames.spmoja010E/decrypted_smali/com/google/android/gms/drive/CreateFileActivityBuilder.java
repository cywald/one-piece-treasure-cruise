package com.google.android.gms.drive;

import android.content.IntentSender;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.drive.zzbi;
import com.google.android.gms.internal.drive.zzt;

@Deprecated
public class CreateFileActivityBuilder
{
  public static final String EXTRA_RESPONSE_DRIVE_ID = "response_drive_id";
  private final zzt zzn = new zzt(0);
  @Nullable
  private DriveContents zzo;
  private boolean zzp;
  
  public IntentSender build(GoogleApiClient paramGoogleApiClient)
  {
    Preconditions.checkState(paramGoogleApiClient.isConnected(), "Client must be connected");
    zzf();
    return this.zzn.build(paramGoogleApiClient);
  }
  
  final int getRequestId()
  {
    return this.zzn.getRequestId();
  }
  
  public CreateFileActivityBuilder setActivityStartFolder(DriveId paramDriveId)
  {
    this.zzn.zza(paramDriveId);
    return this;
  }
  
  public CreateFileActivityBuilder setActivityTitle(String paramString)
  {
    this.zzn.zzc(paramString);
    return this;
  }
  
  public CreateFileActivityBuilder setInitialDriveContents(@Nullable DriveContents paramDriveContents)
  {
    if (paramDriveContents != null)
    {
      if (!(paramDriveContents instanceof zzbi)) {
        throw new IllegalArgumentException("Only DriveContents obtained from the Drive API are accepted.");
      }
      if (paramDriveContents.getDriveId() != null) {
        throw new IllegalArgumentException("Only DriveContents obtained through DriveApi.newDriveContents are accepted for file creation.");
      }
      if (paramDriveContents.zzj()) {
        throw new IllegalArgumentException("DriveContents are already closed.");
      }
      this.zzn.zzd(paramDriveContents.zzh().zzj);
      this.zzo = paramDriveContents;
    }
    for (;;)
    {
      this.zzp = true;
      return this;
      this.zzn.zzd(1);
    }
  }
  
  public CreateFileActivityBuilder setInitialMetadata(MetadataChangeSet paramMetadataChangeSet)
  {
    this.zzn.zza(paramMetadataChangeSet);
    return this;
  }
  
  final MetadataChangeSet zzb()
  {
    return this.zzn.zzb();
  }
  
  final DriveId zzc()
  {
    return this.zzn.zzc();
  }
  
  final String zzd()
  {
    return this.zzn.zzd();
  }
  
  final int zze()
  {
    zzt localzzt = this.zzn;
    return 0;
  }
  
  final void zzf()
  {
    Preconditions.checkState(this.zzp, "Must call setInitialDriveContents.");
    if (this.zzo != null) {
      this.zzo.zzi();
    }
    this.zzn.zzf();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\CreateFileActivityBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */