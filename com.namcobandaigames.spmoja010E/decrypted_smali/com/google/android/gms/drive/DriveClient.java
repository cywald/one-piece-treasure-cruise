package com.google.android.gms.drive;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApi.Settings;
import com.google.android.gms.tasks.Task;

public abstract class DriveClient
  extends GoogleApi<Drive.zza>
{
  public DriveClient(@NonNull Activity paramActivity, @Nullable Drive.zza paramzza)
  {
    super(paramActivity, Drive.zzu, paramzza, GoogleApi.Settings.DEFAULT_SETTINGS);
  }
  
  public DriveClient(@NonNull Context paramContext, @NonNull Drive.zza paramzza)
  {
    super(paramContext, Drive.zzu, paramzza, GoogleApi.Settings.DEFAULT_SETTINGS);
  }
  
  public abstract Task<DriveId> getDriveId(@NonNull String paramString);
  
  public abstract Task<TransferPreferences> getUploadPreferences();
  
  public abstract Task<IntentSender> newCreateFileActivityIntentSender(CreateFileActivityOptions paramCreateFileActivityOptions);
  
  public abstract Task<IntentSender> newOpenFileActivityIntentSender(OpenFileActivityOptions paramOpenFileActivityOptions);
  
  public abstract Task<Void> requestSync();
  
  public abstract Task<Void> setUploadPreferences(@NonNull TransferPreferences paramTransferPreferences);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\DriveClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */