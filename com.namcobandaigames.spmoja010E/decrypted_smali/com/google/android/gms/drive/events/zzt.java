package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="TransferProgressOptionsCreator")
@SafeParcelable.Reserved({1})
public final class zzt
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzt> CREATOR = new zzu();
  @SafeParcelable.Field(id=2)
  private final int zzcr;
  
  @SafeParcelable.Constructor
  public zzt(@SafeParcelable.Param(id=2) int paramInt)
  {
    this.zzcr = paramInt;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 2, this.zzcr);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */