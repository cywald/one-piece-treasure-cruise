package com.google.android.gms.drive.events;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.util.UidVerifier;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.drive.zzet;
import com.google.android.gms.internal.drive.zzfj;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import javax.annotation.concurrent.GuardedBy;

public class DriveEventService
  extends Service
  implements ChangeListener, CompletionListener, zzd, zzi
{
  public static final String ACTION_HANDLE_EVENT = "com.google.android.gms.drive.events.HANDLE_EVENT";
  private static final GmsLogger zzbx = new GmsLogger("DriveEventService", "");
  private final String name;
  @GuardedBy("this")
  private CountDownLatch zzch;
  @VisibleForTesting
  @GuardedBy("this")
  zza zzci;
  @GuardedBy("this")
  boolean zzcj = false;
  @VisibleForTesting
  private int zzck = -1;
  
  protected DriveEventService()
  {
    this("DriveEventService");
  }
  
  protected DriveEventService(String paramString)
  {
    this.name = paramString;
  }
  
  private final void zza(zzfj paramzzfj)
  {
    paramzzfj = paramzzfj.zzak();
    for (;;)
    {
      try
      {
        switch (paramzzfj.getType())
        {
        case 3: 
        case 5: 
        case 6: 
          zzbx.wfmt("DriveEventService", "Unhandled event: %s", new Object[] { paramzzfj });
          return;
        }
      }
      catch (Exception paramzzfj)
      {
        zzbx.e("DriveEventService", String.format("Error handling event in %s", new Object[] { this.name }), paramzzfj);
        return;
      }
      onChange((ChangeEvent)paramzzfj);
      return;
      onCompletion((CompletionEvent)paramzzfj);
      return;
      zza((zzb)paramzzfj);
      return;
      paramzzfj = (zzv)paramzzfj;
      zzbx.wfmt("DriveEventService", "Unhandled transfer state event in %s: %s", new Object[] { this.name, paramzzfj });
      return;
    }
  }
  
  private final void zzv()
    throws SecurityException
  {
    int i = getCallingUid();
    if (i == this.zzck) {
      return;
    }
    if (UidVerifier.isGooglePlayServicesUid(this, i))
    {
      this.zzck = i;
      return;
    }
    throw new SecurityException("Caller is not GooglePlayServices");
  }
  
  @VisibleForTesting
  protected int getCallingUid()
  {
    return Binder.getCallingUid();
  }
  
  /* Error */
  public final android.os.IBinder onBind(Intent paramIntent)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: monitorenter
    //   4: ldc 22
    //   6: aload_1
    //   7: invokevirtual 160	android/content/Intent:getAction	()Ljava/lang/String;
    //   10: invokevirtual 164	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   13: ifeq +91 -> 104
    //   16: aload_0
    //   17: getfield 166	com/google/android/gms/drive/events/DriveEventService:zzci	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   20: ifnonnull +71 -> 91
    //   23: aload_0
    //   24: getfield 59	com/google/android/gms/drive/events/DriveEventService:zzcj	Z
    //   27: ifne +64 -> 91
    //   30: aload_0
    //   31: iconst_1
    //   32: putfield 59	com/google/android/gms/drive/events/DriveEventService:zzcj	Z
    //   35: new 168	java/util/concurrent/CountDownLatch
    //   38: dup
    //   39: iconst_1
    //   40: invokespecial 171	java/util/concurrent/CountDownLatch:<init>	(I)V
    //   43: astore_1
    //   44: aload_0
    //   45: new 168	java/util/concurrent/CountDownLatch
    //   48: dup
    //   49: iconst_1
    //   50: invokespecial 171	java/util/concurrent/CountDownLatch:<init>	(I)V
    //   53: putfield 132	com/google/android/gms/drive/events/DriveEventService:zzch	Ljava/util/concurrent/CountDownLatch;
    //   56: new 173	com/google/android/gms/drive/events/zzh
    //   59: dup
    //   60: aload_0
    //   61: aload_1
    //   62: invokespecial 176	com/google/android/gms/drive/events/zzh:<init>	(Lcom/google/android/gms/drive/events/DriveEventService;Ljava/util/concurrent/CountDownLatch;)V
    //   65: invokevirtual 179	com/google/android/gms/drive/events/zzh:start	()V
    //   68: aload_1
    //   69: ldc2_w 180
    //   72: getstatic 187	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   75: invokevirtual 191	java/util/concurrent/CountDownLatch:await	(JLjava/util/concurrent/TimeUnit;)Z
    //   78: ifne +13 -> 91
    //   81: getstatic 51	com/google/android/gms/drive/events/DriveEventService:zzbx	Lcom/google/android/gms/common/internal/GmsLogger;
    //   84: ldc 43
    //   86: ldc -63
    //   88: invokevirtual 195	com/google/android/gms/common/internal/GmsLogger:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   91: new 17	com/google/android/gms/drive/events/DriveEventService$zzb
    //   94: dup
    //   95: aload_0
    //   96: aconst_null
    //   97: invokespecial 198	com/google/android/gms/drive/events/DriveEventService$zzb:<init>	(Lcom/google/android/gms/drive/events/DriveEventService;Lcom/google/android/gms/drive/events/zzh;)V
    //   100: invokevirtual 204	com/google/android/gms/internal/drive/zzb:asBinder	()Landroid/os/IBinder;
    //   103: astore_2
    //   104: aload_0
    //   105: monitorexit
    //   106: aload_2
    //   107: areturn
    //   108: astore_1
    //   109: new 206	java/lang/RuntimeException
    //   112: dup
    //   113: ldc -48
    //   115: aload_1
    //   116: invokespecial 211	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   119: athrow
    //   120: astore_1
    //   121: aload_0
    //   122: monitorexit
    //   123: aload_1
    //   124: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	125	0	this	DriveEventService
    //   0	125	1	paramIntent	Intent
    //   1	106	2	localIBinder	android.os.IBinder
    // Exception table:
    //   from	to	target	type
    //   68	91	108	java/lang/InterruptedException
    //   4	68	120	finally
    //   68	91	120	finally
    //   91	104	120	finally
    //   109	120	120	finally
  }
  
  public void onChange(ChangeEvent paramChangeEvent)
  {
    zzbx.wfmt("DriveEventService", "Unhandled change event in %s: %s", new Object[] { this.name, paramChangeEvent });
  }
  
  public void onCompletion(CompletionEvent paramCompletionEvent)
  {
    zzbx.wfmt("DriveEventService", "Unhandled completion event in %s: %s", new Object[] { this.name, paramCompletionEvent });
  }
  
  /* Error */
  public void onDestroy()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 166	com/google/android/gms/drive/events/DriveEventService:zzci	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   6: ifnull +56 -> 62
    //   9: aload_0
    //   10: getfield 166	com/google/android/gms/drive/events/DriveEventService:zzci	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   13: invokestatic 219	com/google/android/gms/drive/events/DriveEventService$zza:zza	(Lcom/google/android/gms/drive/events/DriveEventService$zza;)Landroid/os/Message;
    //   16: astore_1
    //   17: aload_0
    //   18: getfield 166	com/google/android/gms/drive/events/DriveEventService:zzci	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   21: aload_1
    //   22: invokevirtual 223	com/google/android/gms/drive/events/DriveEventService$zza:sendMessage	(Landroid/os/Message;)Z
    //   25: pop
    //   26: aload_0
    //   27: aconst_null
    //   28: putfield 166	com/google/android/gms/drive/events/DriveEventService:zzci	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   31: aload_0
    //   32: getfield 132	com/google/android/gms/drive/events/DriveEventService:zzch	Ljava/util/concurrent/CountDownLatch;
    //   35: ldc2_w 180
    //   38: getstatic 187	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   41: invokevirtual 191	java/util/concurrent/CountDownLatch:await	(JLjava/util/concurrent/TimeUnit;)Z
    //   44: ifne +13 -> 57
    //   47: getstatic 51	com/google/android/gms/drive/events/DriveEventService:zzbx	Lcom/google/android/gms/common/internal/GmsLogger;
    //   50: ldc 43
    //   52: ldc -31
    //   54: invokevirtual 228	com/google/android/gms/common/internal/GmsLogger:w	(Ljava/lang/String;Ljava/lang/String;)V
    //   57: aload_0
    //   58: aconst_null
    //   59: putfield 132	com/google/android/gms/drive/events/DriveEventService:zzch	Ljava/util/concurrent/CountDownLatch;
    //   62: aload_0
    //   63: invokespecial 230	android/app/Service:onDestroy	()V
    //   66: aload_0
    //   67: monitorexit
    //   68: return
    //   69: astore_1
    //   70: aload_0
    //   71: monitorexit
    //   72: aload_1
    //   73: athrow
    //   74: astore_1
    //   75: goto -18 -> 57
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	78	0	this	DriveEventService
    //   16	6	1	localMessage	Message
    //   69	4	1	localObject	Object
    //   74	1	1	localInterruptedException	InterruptedException
    // Exception table:
    //   from	to	target	type
    //   2	31	69	finally
    //   31	57	69	finally
    //   57	62	69	finally
    //   62	66	69	finally
    //   31	57	74	java/lang/InterruptedException
  }
  
  public boolean onUnbind(Intent paramIntent)
  {
    return true;
  }
  
  public final void zza(zzb paramzzb)
  {
    zzbx.wfmt("DriveEventService", "Unhandled changes available event in %s: %s", new Object[] { this.name, paramzzb });
  }
  
  static final class zza
    extends Handler
  {
    private final WeakReference<DriveEventService> zzcn;
    
    private zza(DriveEventService paramDriveEventService)
    {
      this.zzcn = new WeakReference(paramDriveEventService);
    }
    
    private final Message zzb(zzfj paramzzfj)
    {
      return obtainMessage(1, paramzzfj);
    }
    
    private final Message zzx()
    {
      return obtainMessage(2);
    }
    
    public final void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        DriveEventService.zzw().wfmt("DriveEventService", "Unexpected message type: %s", new Object[] { Integer.valueOf(paramMessage.what) });
        return;
      case 1: 
        DriveEventService localDriveEventService = (DriveEventService)this.zzcn.get();
        if (localDriveEventService != null)
        {
          DriveEventService.zza(localDriveEventService, (zzfj)paramMessage.obj);
          return;
        }
        getLooper().quit();
        return;
      }
      getLooper().quit();
    }
  }
  
  @VisibleForTesting
  final class zzb
    extends zzet
  {
    private zzb() {}
    
    public final void zzc(zzfj paramzzfj)
      throws RemoteException
    {
      synchronized (DriveEventService.this)
      {
        DriveEventService.zza(DriveEventService.this);
        if (DriveEventService.this.zzci != null)
        {
          paramzzfj = DriveEventService.zza.zza(DriveEventService.this.zzci, paramzzfj);
          DriveEventService.this.zzci.sendMessage(paramzzfj);
          return;
        }
        DriveEventService.zzw().e("DriveEventService", "Receiving event before initialize is completed.");
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\DriveEventService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */