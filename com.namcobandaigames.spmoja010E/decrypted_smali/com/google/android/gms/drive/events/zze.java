package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.DriveSpace;
import java.util.List;

@SafeParcelable.Class(creator="ChangesAvailableOptionsCreator")
@SafeParcelable.Reserved({1})
public final class zze
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zze> CREATOR = new zzf();
  @SafeParcelable.Field(id=2)
  private final int zzbu;
  @SafeParcelable.Field(id=3)
  private final boolean zzbv;
  @SafeParcelable.Field(id=4)
  private final List<DriveSpace> zzbw;
  
  @SafeParcelable.Constructor
  zze(@SafeParcelable.Param(id=2) int paramInt, @SafeParcelable.Param(id=3) boolean paramBoolean, @NonNull @SafeParcelable.Param(id=4) List<DriveSpace> paramList)
  {
    this.zzbu = paramInt;
    this.zzbv = paramBoolean;
    this.zzbw = paramList;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zze)paramObject;
      if ((!Objects.equal(this.zzbw, ((zze)paramObject).zzbw)) || (this.zzbu != ((zze)paramObject).zzbu)) {
        break;
      }
      bool1 = bool2;
    } while (this.zzbv == ((zze)paramObject).zzbv);
    return false;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzbw, Integer.valueOf(this.zzbu), Boolean.valueOf(this.zzbv) });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 2, this.zzbu);
    SafeParcelWriter.writeBoolean(paramParcel, 3, this.zzbv);
    SafeParcelWriter.writeTypedList(paramParcel, 4, this.zzbw, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */