package com.google.android.gms.drive.events;

@Deprecated
public abstract interface ChangeListener
  extends zzi
{
  public abstract void onChange(ChangeEvent paramChangeEvent);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\ChangeListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */