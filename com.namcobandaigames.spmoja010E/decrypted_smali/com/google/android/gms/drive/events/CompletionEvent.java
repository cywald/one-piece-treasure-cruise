package com.google.android.gms.drive.events;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.IOUtils;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.drive.zzeu;
import com.google.android.gms.internal.drive.zzev;
import com.google.android.gms.internal.drive.zzhp;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SafeParcelable.Class(creator="CompletionEventCreator")
@SafeParcelable.Reserved({1})
public final class CompletionEvent
  extends AbstractSafeParcelable
  implements ResourceEvent
{
  public static final Parcelable.Creator<CompletionEvent> CREATOR = new zzg();
  public static final int STATUS_CANCELED = 3;
  public static final int STATUS_CONFLICT = 2;
  public static final int STATUS_FAILURE = 1;
  public static final int STATUS_SUCCESS = 0;
  private static final GmsLogger zzbx = new GmsLogger("CompletionEvent", "");
  @SafeParcelable.Field(id=8)
  private final int status;
  @Nullable
  @SafeParcelable.Field(id=3)
  private final String zzby;
  @Nullable
  @SafeParcelable.Field(id=4)
  private final ParcelFileDescriptor zzbz;
  @Nullable
  @SafeParcelable.Field(id=5)
  private final ParcelFileDescriptor zzca;
  @Nullable
  @SafeParcelable.Field(id=6)
  private final MetadataBundle zzcb;
  @SafeParcelable.Field(id=7)
  private final List<String> zzcc;
  @SafeParcelable.Field(id=9)
  private final IBinder zzcd;
  private boolean zzce = false;
  private boolean zzcf = false;
  private boolean zzcg = false;
  @SafeParcelable.Field(id=2)
  private final DriveId zzk;
  
  @SafeParcelable.Constructor
  CompletionEvent(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) String paramString, @SafeParcelable.Param(id=4) ParcelFileDescriptor paramParcelFileDescriptor1, @SafeParcelable.Param(id=5) ParcelFileDescriptor paramParcelFileDescriptor2, @SafeParcelable.Param(id=6) MetadataBundle paramMetadataBundle, @SafeParcelable.Param(id=7) List<String> paramList, @SafeParcelable.Param(id=8) int paramInt, @SafeParcelable.Param(id=9) IBinder paramIBinder)
  {
    this.zzk = paramDriveId;
    this.zzby = paramString;
    this.zzbz = paramParcelFileDescriptor1;
    this.zzca = paramParcelFileDescriptor2;
    this.zzcb = paramMetadataBundle;
    this.zzcc = paramList;
    this.status = paramInt;
    this.zzcd = paramIBinder;
  }
  
  private final void zza(boolean paramBoolean)
  {
    zzu();
    this.zzcg = true;
    IOUtils.closeQuietly(this.zzbz);
    IOUtils.closeQuietly(this.zzca);
    if ((this.zzcb != null) && (this.zzcb.zzd(zzhp.zzka))) {
      ((BitmapTeleporter)this.zzcb.zza(zzhp.zzka)).release();
    }
    if (this.zzcd == null)
    {
      if (paramBoolean) {}
      for (str = "snooze";; str = "dismiss")
      {
        zzbx.efmt("CompletionEvent", "No callback on %s", new Object[] { str });
        return;
      }
    }
    try
    {
      zzev.zza(this.zzcd).zza(paramBoolean);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      if (!paramBoolean) {}
    }
    for (String str = "snooze";; str = "dismiss")
    {
      zzbx.e("CompletionEvent", String.format("RemoteException on %s", new Object[] { str }), localRemoteException);
      return;
    }
  }
  
  private final void zzu()
  {
    if (this.zzcg) {
      throw new IllegalStateException("Event has already been dismissed or snoozed.");
    }
  }
  
  public final void dismiss()
  {
    zza(false);
  }
  
  @Nullable
  public final String getAccountName()
  {
    zzu();
    return this.zzby;
  }
  
  @Nullable
  public final InputStream getBaseContentsInputStream()
  {
    zzu();
    if (this.zzbz == null) {
      return null;
    }
    if (this.zzce) {
      throw new IllegalStateException("getBaseInputStream() can only be called once per CompletionEvent instance.");
    }
    this.zzce = true;
    return new FileInputStream(this.zzbz.getFileDescriptor());
  }
  
  public final DriveId getDriveId()
  {
    zzu();
    return this.zzk;
  }
  
  @Nullable
  public final InputStream getModifiedContentsInputStream()
  {
    zzu();
    if (this.zzca == null) {
      return null;
    }
    if (this.zzcf) {
      throw new IllegalStateException("getModifiedInputStream() can only be called once per CompletionEvent instance.");
    }
    this.zzcf = true;
    return new FileInputStream(this.zzca.getFileDescriptor());
  }
  
  @Nullable
  public final MetadataChangeSet getModifiedMetadataChangeSet()
  {
    zzu();
    if (this.zzcb != null) {
      return new MetadataChangeSet(this.zzcb);
    }
    return null;
  }
  
  public final int getStatus()
  {
    zzu();
    return this.status;
  }
  
  public final List<String> getTrackingTags()
  {
    zzu();
    return new ArrayList(this.zzcc);
  }
  
  public final int getType()
  {
    return 2;
  }
  
  public final void snooze()
  {
    zza(true);
  }
  
  public final String toString()
  {
    if (this.zzcc == null) {}
    for (String str = "<null>";; str = String.valueOf(str).length() + 2 + "'" + str + "'")
    {
      return String.format(Locale.US, "CompletionEvent [id=%s, status=%s, trackingTag=%s]", new Object[] { this.zzk, Integer.valueOf(this.status), str });
      str = TextUtils.join("','", this.zzcc);
    }
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt |= 0x1;
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzk, paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 3, this.zzby, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzbz, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzca, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 6, this.zzcb, paramInt, false);
    SafeParcelWriter.writeStringList(paramParcel, 7, this.zzcc, false);
    SafeParcelWriter.writeInt(paramParcel, 8, this.status);
    SafeParcelWriter.writeIBinder(paramParcel, 9, this.zzcd, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\CompletionEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */