package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.internal.drive.zzh;

@SafeParcelable.Class(creator="TransferProgressEventCreator")
@SafeParcelable.Reserved({1})
public final class zzr
  extends AbstractSafeParcelable
  implements DriveEvent
{
  public static final Parcelable.Creator<zzr> CREATOR = new zzs();
  @SafeParcelable.Field(id=2)
  private final zzh zzcq;
  
  @SafeParcelable.Constructor
  public zzr(@SafeParcelable.Param(id=2) zzh paramzzh)
  {
    this.zzcq = paramzzh;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (zzr)paramObject;
    return Objects.equal(this.zzcq, ((zzr)paramObject).zzcq);
  }
  
  public final int getType()
  {
    return 8;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzcq });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzcq, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final zzh zzab()
  {
    return this.zzcq;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */