package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import java.util.Locale;

@SafeParcelable.Class(creator="ChangesAvailableEventCreator")
@SafeParcelable.Reserved({1, 2})
public final class zzb
  extends AbstractSafeParcelable
  implements DriveEvent
{
  public static final Parcelable.Creator<zzb> CREATOR = new zzc();
  @SafeParcelable.Field(id=3)
  private final zze zzbt;
  
  @SafeParcelable.Constructor
  public zzb(@SafeParcelable.Param(id=3) zze paramzze)
  {
    this.zzbt = paramzze;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (zzb)paramObject;
    return Objects.equal(this.zzbt, ((zzb)paramObject).zzbt);
  }
  
  public final int getType()
  {
    return 4;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzbt });
  }
  
  public final String toString()
  {
    return String.format(Locale.US, "ChangesAvailableEvent [changesAvailableOptions=%s]", new Object[] { this.zzbt });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 3, this.zzbt, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */