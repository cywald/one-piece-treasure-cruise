package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.internal.drive.zzh;
import java.util.List;

@SafeParcelable.Class(creator="TransferStateEventCreator")
@SafeParcelable.Reserved({1, 2})
public final class zzv
  extends AbstractSafeParcelable
  implements DriveEvent
{
  public static final Parcelable.Creator<zzv> CREATOR = new zzw();
  @SafeParcelable.Field(id=3)
  private final List<zzh> zzcs;
  
  @SafeParcelable.Constructor
  public zzv(@SafeParcelable.Param(id=3) List<zzh> paramList)
  {
    this.zzcs = paramList;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (zzv)paramObject;
    return Objects.equal(this.zzcs, ((zzv)paramObject).zzcs);
  }
  
  public final int getType()
  {
    return 7;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzcs });
  }
  
  public final String toString()
  {
    return String.format("TransferStateEvent[%s]", new Object[] { TextUtils.join("','", this.zzcs) });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeTypedList(paramParcel, 3, this.zzcs, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */