package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.DriveId;
import java.util.Locale;

@SafeParcelable.Class(creator="ChangeEventCreator")
@SafeParcelable.Reserved({1})
public final class ChangeEvent
  extends AbstractSafeParcelable
  implements ResourceEvent
{
  public static final Parcelable.Creator<ChangeEvent> CREATOR = new zza();
  @SafeParcelable.Field(id=3)
  private final int zzbs;
  @SafeParcelable.Field(id=2)
  private final DriveId zzk;
  
  @SafeParcelable.Constructor
  public ChangeEvent(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) int paramInt)
  {
    this.zzk = paramDriveId;
    this.zzbs = paramInt;
  }
  
  public final DriveId getDriveId()
  {
    return this.zzk;
  }
  
  public final int getType()
  {
    return 1;
  }
  
  public final boolean hasBeenDeleted()
  {
    return (this.zzbs & 0x4) != 0;
  }
  
  public final boolean hasContentChanged()
  {
    return (this.zzbs & 0x2) != 0;
  }
  
  public final boolean hasMetadataChanged()
  {
    return (this.zzbs & 0x1) != 0;
  }
  
  public final String toString()
  {
    return String.format(Locale.US, "ChangeEvent [id=%s,changeFlags=%x]", new Object[] { this.zzk, Integer.valueOf(this.zzbs) });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzk, paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 3, this.zzbs);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\ChangeEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */