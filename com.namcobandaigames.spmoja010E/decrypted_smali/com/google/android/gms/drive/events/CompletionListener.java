package com.google.android.gms.drive.events;

public abstract interface CompletionListener
  extends zzi
{
  public abstract void onCompletion(CompletionEvent paramCompletionEvent);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\CompletionListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */