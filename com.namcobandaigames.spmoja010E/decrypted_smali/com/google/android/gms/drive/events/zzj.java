package com.google.android.gms.drive.events;

import android.support.annotation.Nullable;
import com.google.android.gms.drive.DriveId;

public final class zzj
{
  public static boolean zza(int paramInt, @Nullable DriveId paramDriveId)
  {
    boolean bool = true;
    switch (paramInt)
    {
    case 2: 
    case 3: 
    case 5: 
    case 6: 
    default: 
      bool = false;
    }
    do
    {
      do
      {
        return bool;
      } while (paramDriveId != null);
      return false;
    } while (paramDriveId == null);
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */