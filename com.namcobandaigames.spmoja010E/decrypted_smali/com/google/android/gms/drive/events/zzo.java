package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.zzu;

@SafeParcelable.Class(creator="QueryResultEventParcelableCreator")
@SafeParcelable.Reserved({1})
public final class zzo
  extends zzu
  implements DriveEvent
{
  public static final Parcelable.Creator<zzo> CREATOR = new zzp();
  @Nullable
  @SafeParcelable.Field(id=2)
  private final DataHolder zzat;
  @SafeParcelable.Field(id=3)
  private final boolean zzco;
  @SafeParcelable.Field(id=4)
  private final int zzcp;
  
  @SafeParcelable.Constructor
  public zzo(@Nullable @SafeParcelable.Param(id=2) DataHolder paramDataHolder, @SafeParcelable.Param(id=3) boolean paramBoolean, @SafeParcelable.Param(id=4) int paramInt)
  {
    this.zzat = paramDataHolder;
    this.zzco = paramBoolean;
    this.zzcp = paramInt;
  }
  
  public final int getType()
  {
    return 3;
  }
  
  public final void zza(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzat, paramInt, false);
    SafeParcelWriter.writeBoolean(paramParcel, 3, this.zzco);
    SafeParcelWriter.writeInt(paramParcel, 4, this.zzcp);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final int zzaa()
  {
    return this.zzcp;
  }
  
  @Nullable
  public final DataHolder zzy()
  {
    return this.zzat;
  }
  
  public final boolean zzz()
  {
    return this.zzco;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */