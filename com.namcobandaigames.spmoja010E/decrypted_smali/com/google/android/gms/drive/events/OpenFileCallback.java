package com.google.android.gms.drive.events;

import android.support.annotation.NonNull;
import com.google.android.gms.drive.DriveContents;

public abstract class OpenFileCallback
{
  public abstract void onContents(@NonNull DriveContents paramDriveContents);
  
  public abstract void onError(@NonNull Exception paramException);
  
  public abstract void onProgress(long paramLong1, long paramLong2);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\OpenFileCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */