package com.google.android.gms.drive.events;

import android.os.Looper;
import java.util.concurrent.CountDownLatch;

final class zzh
  extends Thread
{
  zzh(DriveEventService paramDriveEventService, CountDownLatch paramCountDownLatch) {}
  
  public final void run()
  {
    try
    {
      Looper.prepare();
      this.zzcm.zzci = new DriveEventService.zza(this.zzcm, null);
      this.zzcm.zzcj = false;
      this.zzcl.countDown();
      Looper.loop();
      return;
    }
    finally
    {
      if (DriveEventService.zzb(this.zzcm) != null) {
        DriveEventService.zzb(this.zzcm).countDown();
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\events\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */