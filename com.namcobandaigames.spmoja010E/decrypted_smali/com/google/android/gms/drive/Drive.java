package com.google.android.gms.drive;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AbstractClientBuilder;
import com.google.android.gms.common.api.Api.ApiOptions.HasGoogleSignInAccountOptions;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.ClientKey;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.drive.zzaf;
import com.google.android.gms.internal.drive.zzaw;
import com.google.android.gms.internal.drive.zzbb;
import com.google.android.gms.internal.drive.zzbr;
import com.google.android.gms.internal.drive.zzcb;
import com.google.android.gms.internal.drive.zzch;
import com.google.android.gms.internal.drive.zzeb;
import java.util.Set;

public final class Drive
{
  @Deprecated
  public static final Api<Api.ApiOptions.NoOptions> API;
  private static final Api.AbstractClientBuilder<zzaw, Api.ApiOptions.NoOptions> CLIENT_BUILDER;
  public static final Api.ClientKey<zzaw> CLIENT_KEY = new Api.ClientKey();
  @Deprecated
  public static final DriveApi DriveApi;
  @Deprecated
  public static final DrivePreferencesApi DrivePreferencesApi = new zzcb();
  private static final Api<zzb> INTERNAL_API;
  public static final Scope SCOPE_APPFOLDER;
  public static final Scope SCOPE_FILE;
  private static final Api.AbstractClientBuilder<zzaw, zzb> zzq;
  private static final Api.AbstractClientBuilder<zzaw, zza> zzr;
  private static final Scope zzs;
  private static final Scope zzt;
  public static final Api<zza> zzu;
  @Deprecated
  private static final zzj zzv;
  private static final zzl zzw;
  
  static
  {
    CLIENT_BUILDER = new zze();
    zzq = new zzf();
    zzr = new zzg();
    SCOPE_FILE = new Scope("https://www.googleapis.com/auth/drive.file");
    SCOPE_APPFOLDER = new Scope("https://www.googleapis.com/auth/drive.appdata");
    zzs = new Scope("https://www.googleapis.com/auth/drive");
    zzt = new Scope("https://www.googleapis.com/auth/drive.apps");
    API = new Api("Drive.API", CLIENT_BUILDER, CLIENT_KEY);
    INTERNAL_API = new Api("Drive.INTERNAL_API", zzq, CLIENT_KEY);
    zzu = new Api("Drive.API_CONNECTIONLESS", zzr, CLIENT_KEY);
    DriveApi = new zzaf();
    zzv = new zzbr();
    zzw = new zzeb();
  }
  
  public static DriveClient getDriveClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    zza(paramGoogleSignInAccount);
    return new zzbb(paramActivity, new zza(paramGoogleSignInAccount));
  }
  
  public static DriveClient getDriveClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    zza(paramGoogleSignInAccount);
    return new zzbb(paramContext, new zza(paramGoogleSignInAccount));
  }
  
  public static DriveResourceClient getDriveResourceClient(@NonNull Activity paramActivity, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    zza(paramGoogleSignInAccount);
    return new zzch(paramActivity, new zza(paramGoogleSignInAccount));
  }
  
  public static DriveResourceClient getDriveResourceClient(@NonNull Context paramContext, @NonNull GoogleSignInAccount paramGoogleSignInAccount)
  {
    zza(paramGoogleSignInAccount);
    return new zzch(paramContext, new zza(paramGoogleSignInAccount));
  }
  
  private static void zza(GoogleSignInAccount paramGoogleSignInAccount)
  {
    Preconditions.checkNotNull(paramGoogleSignInAccount);
    paramGoogleSignInAccount = paramGoogleSignInAccount.getRequestedScopes();
    if ((paramGoogleSignInAccount.contains(SCOPE_FILE)) || (paramGoogleSignInAccount.contains(SCOPE_APPFOLDER)) || (paramGoogleSignInAccount.contains(zzs)) || (paramGoogleSignInAccount.contains(zzt))) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool, "You must request a Drive scope in order to interact with the Drive API.");
      return;
    }
  }
  
  public static final class zza
    implements Api.ApiOptions.HasGoogleSignInAccountOptions
  {
    private final Bundle zzx = new Bundle();
    private final GoogleSignInAccount zzy;
    
    public zza(@NonNull GoogleSignInAccount paramGoogleSignInAccount)
    {
      this.zzy = paramGoogleSignInAccount;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      String str1;
      String str2;
      do
      {
        return true;
        if ((paramObject == null) || (paramObject.getClass() != getClass())) {
          return false;
        }
        paramObject = (zza)paramObject;
        if (!Objects.equal(this.zzy, ((zza)paramObject).getGoogleSignInAccount())) {
          return false;
        }
        str1 = this.zzx.getString("method_trace_filename");
        str2 = ((zza)paramObject).zzx.getString("method_trace_filename");
      } while (((str1 != null) || (str2 != null)) && ((str1 != null) && (str2 != null) && (str1.equals(str2)) && (this.zzx.getBoolean("bypass_initial_sync") == ((zza)paramObject).zzx.getBoolean("bypass_initial_sync")) && (this.zzx.getInt("proxy_type") == ((zza)paramObject).zzx.getInt("proxy_type"))));
      return false;
    }
    
    public final GoogleSignInAccount getGoogleSignInAccount()
    {
      return this.zzy;
    }
    
    public final int hashCode()
    {
      String str = this.zzx.getString("method_trace_filename", "");
      int i = this.zzx.getInt("proxy_type");
      boolean bool = this.zzx.getBoolean("bypass_initial_sync");
      return Objects.hashCode(new Object[] { this.zzy, str, Integer.valueOf(i), Boolean.valueOf(bool) });
    }
    
    public final Bundle zzg()
    {
      return this.zzx;
    }
  }
  
  public static final class zzb
    implements Api.ApiOptions.Optional
  {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\Drive.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */