package com.google.android.gms.drive;

import android.text.TextUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.internal.drive.zzaw;

public class ExecutionOptions
{
  public static final int CONFLICT_STRATEGY_KEEP_REMOTE = 1;
  public static final int CONFLICT_STRATEGY_OVERWRITE_REMOTE = 0;
  public static final int MAX_TRACKING_TAG_STRING_LENGTH = 65536;
  private final String zzal;
  private final boolean zzam;
  private final int zzan;
  
  public ExecutionOptions(String paramString, boolean paramBoolean, int paramInt)
  {
    this.zzal = paramString;
    this.zzam = paramBoolean;
    this.zzan = paramInt;
  }
  
  public static boolean zza(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (ExecutionOptions)paramObject;
      if ((!Objects.equal(this.zzal, ((ExecutionOptions)paramObject).zzal)) || (this.zzan != ((ExecutionOptions)paramObject).zzan)) {
        break;
      }
      bool1 = bool2;
    } while (this.zzam == ((ExecutionOptions)paramObject).zzam);
    return false;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzal, Integer.valueOf(this.zzan), Boolean.valueOf(this.zzam) });
  }
  
  @Deprecated
  public final void zza(GoogleApiClient paramGoogleApiClient)
  {
    zza((zzaw)paramGoogleApiClient.getClient(Drive.CLIENT_KEY));
  }
  
  public final void zza(zzaw paramzzaw)
  {
    if ((this.zzam) && (!paramzzaw.zzag())) {
      throw new IllegalStateException("Application must define an exported DriveEventService subclass in AndroidManifest.xml to be notified on completion");
    }
  }
  
  public final String zzk()
  {
    return this.zzal;
  }
  
  public final boolean zzl()
  {
    return this.zzam;
  }
  
  public final int zzm()
  {
    return this.zzan;
  }
  
  public static class Builder
  {
    protected String zzao;
    protected boolean zzap;
    protected int zzaq = 0;
    
    public ExecutionOptions build()
    {
      zzn();
      return new ExecutionOptions(this.zzao, this.zzap, this.zzaq);
    }
    
    public Builder setConflictStrategy(int paramInt)
    {
      switch (paramInt)
      {
      }
      for (int i = 0; i == 0; i = 1) {
        throw new IllegalArgumentException(53 + "Unrecognized value for conflict strategy: " + paramInt);
      }
      this.zzaq = paramInt;
      return this;
    }
    
    public Builder setNotifyOnCompletion(boolean paramBoolean)
    {
      this.zzap = paramBoolean;
      return this;
    }
    
    public Builder setTrackingTag(String paramString)
    {
      if ((!TextUtils.isEmpty(paramString)) && (paramString.length() <= 65536)) {}
      for (int i = 1; i == 0; i = 0) {
        throw new IllegalArgumentException(String.format("trackingTag must not be null nor empty, and the length must be <= the maximum length (%s)", new Object[] { Integer.valueOf(65536) }));
      }
      this.zzao = paramString;
      return this;
    }
    
    protected final void zzn()
    {
      if ((this.zzaq == 1) && (!this.zzap)) {
        throw new IllegalStateException("Cannot use CONFLICT_STRATEGY_KEEP_REMOTE without requesting completion notifications");
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\ExecutionOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */