package com.google.android.gms.drive;

import com.google.android.gms.common.internal.Objects;

public class TransferPreferencesBuilder
{
  public static final TransferPreferences DEFAULT_PREFERENCES = new zza(1, true, 256);
  private int zzbj;
  private boolean zzbk;
  private int zzbl;
  
  public TransferPreferencesBuilder()
  {
    this(DEFAULT_PREFERENCES);
  }
  
  public TransferPreferencesBuilder(FileUploadPreferences paramFileUploadPreferences)
  {
    this.zzbj = paramFileUploadPreferences.getNetworkTypePreference();
    this.zzbk = paramFileUploadPreferences.isRoamingAllowed();
    this.zzbl = paramFileUploadPreferences.getBatteryUsagePreference();
  }
  
  public TransferPreferencesBuilder(TransferPreferences paramTransferPreferences)
  {
    this.zzbj = paramTransferPreferences.getNetworkPreference();
    this.zzbk = paramTransferPreferences.isRoamingAllowed();
    this.zzbl = paramTransferPreferences.getBatteryUsagePreference();
  }
  
  public TransferPreferences build()
  {
    return new zza(this.zzbj, this.zzbk, this.zzbl);
  }
  
  public TransferPreferencesBuilder setBatteryUsagePreference(int paramInt)
  {
    this.zzbl = paramInt;
    return this;
  }
  
  public TransferPreferencesBuilder setIsRoamingAllowed(boolean paramBoolean)
  {
    this.zzbk = paramBoolean;
    return this;
  }
  
  public TransferPreferencesBuilder setNetworkPreference(int paramInt)
  {
    this.zzbj = paramInt;
    return this;
  }
  
  static final class zza
    implements TransferPreferences
  {
    private final int zzbj;
    private final boolean zzbk;
    private final int zzbl;
    
    zza(int paramInt1, boolean paramBoolean, int paramInt2)
    {
      this.zzbj = paramInt1;
      this.zzbk = paramBoolean;
      this.zzbl = paramInt2;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if ((paramObject == null) || (getClass() != paramObject.getClass())) {
          return false;
        }
        paramObject = (zza)paramObject;
      } while ((((zza)paramObject).zzbj == this.zzbj) && (((zza)paramObject).zzbk == this.zzbk) && (((zza)paramObject).zzbl == this.zzbl));
      return false;
    }
    
    public final int getBatteryUsagePreference()
    {
      return this.zzbl;
    }
    
    public final int getNetworkPreference()
    {
      return this.zzbj;
    }
    
    public final int hashCode()
    {
      return Objects.hashCode(new Object[] { Integer.valueOf(this.zzbj), Boolean.valueOf(this.zzbk), Integer.valueOf(this.zzbl) });
    }
    
    public final boolean isRoamingAllowed()
    {
      return this.zzbk;
    }
    
    public final String toString()
    {
      return String.format("NetworkPreference: %s, IsRoamingAllowed %s, BatteryUsagePreference %s", new Object[] { Integer.valueOf(this.zzbj), Boolean.valueOf(this.zzbk), Integer.valueOf(this.zzbl) });
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\TransferPreferencesBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */