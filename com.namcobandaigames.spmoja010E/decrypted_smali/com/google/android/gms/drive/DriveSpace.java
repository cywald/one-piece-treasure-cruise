package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.CollectionUtils;
import java.util.Set;
import java.util.regex.Pattern;

@SafeParcelable.Class(creator="DriveSpaceCreator")
@SafeParcelable.Reserved({1})
public class DriveSpace
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<DriveSpace> CREATOR = new zzm();
  public static final DriveSpace zzaf = new DriveSpace("DRIVE");
  public static final DriveSpace zzag = new DriveSpace("APP_DATA_FOLDER");
  public static final DriveSpace zzah = new DriveSpace("PHOTOS");
  private static final Set<DriveSpace> zzai = CollectionUtils.setOf(zzaf, zzag, zzah);
  private static final String zzaj = TextUtils.join(",", zzai.toArray());
  private static final Pattern zzak = Pattern.compile("[A-Z0-9_]*");
  @SafeParcelable.Field(getter="getName", id=2)
  private final String name;
  
  @SafeParcelable.Constructor
  DriveSpace(@SafeParcelable.Param(id=2) String paramString)
  {
    this.name = ((String)Preconditions.checkNotNull(paramString));
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != DriveSpace.class)) {
      return false;
    }
    return this.name.equals(((DriveSpace)paramObject).name);
  }
  
  public int hashCode()
  {
    return 0x4A54C0DE ^ this.name.hashCode();
  }
  
  public String toString()
  {
    return this.name;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.name, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\DriveSpace.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */