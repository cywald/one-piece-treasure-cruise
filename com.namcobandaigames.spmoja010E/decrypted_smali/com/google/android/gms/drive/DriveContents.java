package com.google.android.gms.drive;

import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import java.io.InputStream;
import java.io.OutputStream;

public abstract interface DriveContents
{
  @Deprecated
  public abstract PendingResult<Status> commit(GoogleApiClient paramGoogleApiClient, @Nullable MetadataChangeSet paramMetadataChangeSet);
  
  @Deprecated
  public abstract PendingResult<Status> commit(GoogleApiClient paramGoogleApiClient, @Nullable MetadataChangeSet paramMetadataChangeSet, @Nullable ExecutionOptions paramExecutionOptions);
  
  @Deprecated
  public abstract void discard(GoogleApiClient paramGoogleApiClient);
  
  public abstract DriveId getDriveId();
  
  public abstract InputStream getInputStream();
  
  public abstract int getMode();
  
  public abstract OutputStream getOutputStream();
  
  public abstract ParcelFileDescriptor getParcelFileDescriptor();
  
  @Deprecated
  public abstract PendingResult<DriveApi.DriveContentsResult> reopenForWrite(GoogleApiClient paramGoogleApiClient);
  
  public abstract Contents zzh();
  
  public abstract void zzi();
  
  public abstract boolean zzj();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\DriveContents.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */