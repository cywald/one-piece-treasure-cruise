package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="UserMetadataCreator")
@SafeParcelable.Reserved({1})
public class UserMetadata
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<UserMetadata> CREATOR = new zzt();
  @SafeParcelable.Field(id=2)
  private final String zzbm;
  @Nullable
  @SafeParcelable.Field(id=3)
  private final String zzbn;
  @Nullable
  @SafeParcelable.Field(id=4)
  private final String zzbo;
  @SafeParcelable.Field(id=5)
  private final boolean zzbp;
  @Nullable
  @SafeParcelable.Field(id=6)
  private final String zzbq;
  
  @SafeParcelable.Constructor
  public UserMetadata(@SafeParcelable.Param(id=2) String paramString1, @Nullable @SafeParcelable.Param(id=3) String paramString2, @Nullable @SafeParcelable.Param(id=4) String paramString3, @SafeParcelable.Param(id=5) boolean paramBoolean, @Nullable @SafeParcelable.Param(id=6) String paramString4)
  {
    this.zzbm = paramString1;
    this.zzbn = paramString2;
    this.zzbo = paramString3;
    this.zzbp = paramBoolean;
    this.zzbq = paramString4;
  }
  
  public String toString()
  {
    return String.format("Permission ID: '%s', Display Name: '%s', Picture URL: '%s', Authenticated User: %b, Email: '%s'", new Object[] { this.zzbm, this.zzbn, this.zzbo, Boolean.valueOf(this.zzbp), this.zzbq });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.zzbm, false);
    SafeParcelWriter.writeString(paramParcel, 3, this.zzbn, false);
    SafeParcelWriter.writeString(paramParcel, 4, this.zzbo, false);
    SafeParcelWriter.writeBoolean(paramParcel, 5, this.zzbp);
    SafeParcelWriter.writeString(paramParcel, 6, this.zzbq, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\UserMetadata.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */