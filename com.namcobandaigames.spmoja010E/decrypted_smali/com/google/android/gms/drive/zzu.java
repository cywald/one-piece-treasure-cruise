package com.google.android.gms.drive;

import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public abstract class zzu
  extends AbstractSafeParcelable
{
  private volatile transient boolean zzbr = false;
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (!this.zzbr) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool);
      this.zzbr = true;
      zza(paramParcel, paramInt);
      return;
    }
  }
  
  protected abstract void zza(Parcel paramParcel, int paramInt);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */