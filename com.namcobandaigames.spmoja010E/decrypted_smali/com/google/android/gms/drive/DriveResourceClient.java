package com.google.android.gms.drive;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApi.Settings;
import com.google.android.gms.drive.events.ListenerToken;
import com.google.android.gms.drive.events.OnChangeListener;
import com.google.android.gms.drive.events.OpenFileCallback;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.tasks.Task;
import java.util.Set;

public abstract class DriveResourceClient
  extends GoogleApi<Drive.zza>
{
  public DriveResourceClient(@NonNull Activity paramActivity, @Nullable Drive.zza paramzza)
  {
    super(paramActivity, Drive.zzu, paramzza, GoogleApi.Settings.DEFAULT_SETTINGS);
  }
  
  public DriveResourceClient(@NonNull Context paramContext, @Nullable Drive.zza paramzza)
  {
    super(paramContext, Drive.zzu, paramzza, GoogleApi.Settings.DEFAULT_SETTINGS);
  }
  
  public abstract Task<ListenerToken> addChangeListener(@NonNull DriveResource paramDriveResource, @NonNull OnChangeListener paramOnChangeListener);
  
  public abstract Task<Void> addChangeSubscription(@NonNull DriveResource paramDriveResource);
  
  public abstract Task<Boolean> cancelOpenFileCallback(@NonNull ListenerToken paramListenerToken);
  
  public abstract Task<Void> commitContents(@NonNull DriveContents paramDriveContents, @Nullable MetadataChangeSet paramMetadataChangeSet);
  
  public abstract Task<Void> commitContents(@NonNull DriveContents paramDriveContents, @Nullable MetadataChangeSet paramMetadataChangeSet, @NonNull ExecutionOptions paramExecutionOptions);
  
  public abstract Task<DriveContents> createContents();
  
  public abstract Task<DriveFile> createFile(@NonNull DriveFolder paramDriveFolder, @NonNull MetadataChangeSet paramMetadataChangeSet, @Nullable DriveContents paramDriveContents);
  
  public abstract Task<DriveFile> createFile(@NonNull DriveFolder paramDriveFolder, @NonNull MetadataChangeSet paramMetadataChangeSet, @Nullable DriveContents paramDriveContents, @NonNull ExecutionOptions paramExecutionOptions);
  
  public abstract Task<DriveFolder> createFolder(@NonNull DriveFolder paramDriveFolder, @NonNull MetadataChangeSet paramMetadataChangeSet);
  
  public abstract Task<Void> delete(@NonNull DriveResource paramDriveResource);
  
  public abstract Task<Void> discardContents(@NonNull DriveContents paramDriveContents);
  
  public abstract Task<DriveFolder> getAppFolder();
  
  public abstract Task<Metadata> getMetadata(@NonNull DriveResource paramDriveResource);
  
  public abstract Task<DriveFolder> getRootFolder();
  
  public abstract Task<MetadataBuffer> listChildren(@NonNull DriveFolder paramDriveFolder);
  
  public abstract Task<MetadataBuffer> listParents(@NonNull DriveResource paramDriveResource);
  
  public abstract Task<DriveContents> openFile(@NonNull DriveFile paramDriveFile, int paramInt);
  
  public abstract Task<ListenerToken> openFile(@NonNull DriveFile paramDriveFile, int paramInt, @NonNull OpenFileCallback paramOpenFileCallback);
  
  public abstract Task<MetadataBuffer> query(@NonNull Query paramQuery);
  
  public abstract Task<MetadataBuffer> queryChildren(@NonNull DriveFolder paramDriveFolder, @NonNull Query paramQuery);
  
  public abstract Task<Boolean> removeChangeListener(@NonNull ListenerToken paramListenerToken);
  
  public abstract Task<Void> removeChangeSubscription(@NonNull DriveResource paramDriveResource);
  
  public abstract Task<DriveContents> reopenContentsForWrite(@NonNull DriveContents paramDriveContents);
  
  public abstract Task<Void> setParents(@NonNull DriveResource paramDriveResource, @NonNull Set<DriveId> paramSet);
  
  public abstract Task<Void> trash(@NonNull DriveResource paramDriveResource);
  
  public abstract Task<Void> untrash(@NonNull DriveResource paramDriveResource);
  
  public abstract Task<Metadata> updateMetadata(@NonNull DriveResource paramDriveResource, @NonNull MetadataChangeSet paramMetadataChangeSet);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\DriveResourceClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */