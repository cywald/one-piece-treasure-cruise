package com.google.android.gms.drive.metadata;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

@SafeParcelable.Class(creator="CustomPropertyKeyCreator")
@SafeParcelable.Reserved({1})
public class CustomPropertyKey
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CustomPropertyKey> CREATOR = new zzc();
  public static final int PRIVATE = 1;
  public static final int PUBLIC = 0;
  private static final Pattern zzik = Pattern.compile("[\\w.!@$%^&*()/-]+");
  @SafeParcelable.Field(id=3)
  private final int visibility;
  @SafeParcelable.Field(id=2)
  private final String zzij;
  
  @SafeParcelable.Constructor
  public CustomPropertyKey(@SafeParcelable.Param(id=2) String paramString, @SafeParcelable.Param(id=3) int paramInt)
  {
    Preconditions.checkNotNull(paramString, "key");
    Preconditions.checkArgument(zzik.matcher(paramString).matches(), "key name characters must be alphanumeric or one of .!@$%^&*()-_/");
    boolean bool1 = bool2;
    if (paramInt != 0) {
      if (paramInt != 1) {
        break label61;
      }
    }
    label61:
    for (bool1 = bool2;; bool1 = false)
    {
      Preconditions.checkArgument(bool1, "visibility must be either PUBLIC or PRIVATE");
      this.zzij = paramString;
      this.visibility = paramInt;
      return;
    }
  }
  
  public static CustomPropertyKey fromJson(JSONObject paramJSONObject)
    throws JSONException
  {
    return new CustomPropertyKey(paramJSONObject.getString("key"), paramJSONObject.getInt("visibility"));
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if ((paramObject == null) || (paramObject.getClass() != getClass())) {
        return false;
      }
      paramObject = (CustomPropertyKey)paramObject;
    } while ((((CustomPropertyKey)paramObject).getKey().equals(this.zzij)) && (((CustomPropertyKey)paramObject).getVisibility() == this.visibility));
    return false;
  }
  
  public String getKey()
  {
    return this.zzij;
  }
  
  public int getVisibility()
  {
    return this.visibility;
  }
  
  public int hashCode()
  {
    String str = this.zzij;
    int i = this.visibility;
    return (String.valueOf(str).length() + 11 + str + i).hashCode();
  }
  
  public JSONObject toJson()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("key", getKey());
    localJSONObject.put("visibility", getVisibility());
    return localJSONObject;
  }
  
  public String toString()
  {
    String str = this.zzij;
    int i = this.visibility;
    return String.valueOf(str).length() + 31 + "CustomPropertyKey(" + str + "," + i + ")";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.zzij, false);
    SafeParcelWriter.writeInt(paramParcel, 3, this.visibility);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\CustomPropertyKey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */