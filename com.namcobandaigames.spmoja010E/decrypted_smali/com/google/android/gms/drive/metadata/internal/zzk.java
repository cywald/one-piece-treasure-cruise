package com.google.android.gms.drive.metadata.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Locale;

public final class zzk
{
  private String zzis;
  
  private zzk(String paramString)
  {
    this.zzis = paramString.toLowerCase(Locale.US);
  }
  
  @Nullable
  public static zzk zze(@Nullable String paramString)
  {
    if ((paramString == null) || (!paramString.isEmpty())) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool);
      if (paramString != null) {
        break;
      }
      return null;
    }
    return new zzk(paramString);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    paramObject = (zzk)paramObject;
    return this.zzis.equals(((zzk)paramObject).zzis);
  }
  
  public final int hashCode()
  {
    return this.zzis.hashCode();
  }
  
  public final boolean isFolder()
  {
    return this.zzis.equals("application/vnd.google-apps.folder");
  }
  
  public final String toString()
  {
    return this.zzis;
  }
  
  public final boolean zzaz()
  {
    return this.zzis.startsWith("application/vnd.google-apps");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */