package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;

final class zzp
  implements zzg
{
  public final String zzav()
  {
    return "parentsExtraHolder";
  }
  
  public final void zzb(DataHolder paramDataHolder)
  {
    zzo.zzd(paramDataHolder);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */