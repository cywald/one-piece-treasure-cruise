package com.google.android.gms.drive.metadata;

import java.util.Collection;

public abstract interface SearchableCollectionMetadataField<T>
  extends SearchableMetadataField<Collection<T>>
{}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\SearchableCollectionMetadataField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */