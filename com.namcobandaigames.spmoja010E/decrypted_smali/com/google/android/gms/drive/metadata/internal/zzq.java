package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="PartialDriveIdCreator")
@SafeParcelable.Reserved({1})
public final class zzq
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzq> CREATOR = new zzr();
  @Nullable
  @SafeParcelable.Field(id=2)
  final String zzab;
  @SafeParcelable.Field(id=3)
  final long zzac;
  @SafeParcelable.Field(defaultValueUnchecked="com.google.android.gms.drive.DriveId.RESOURCE_TYPE_UNKNOWN", id=4)
  final int zzad;
  
  @SafeParcelable.Constructor
  public zzq(@SafeParcelable.Param(id=2) String paramString, @SafeParcelable.Param(id=3) long paramLong, @SafeParcelable.Param(id=4) int paramInt)
  {
    this.zzab = paramString;
    this.zzac = paramLong;
    this.zzad = paramInt;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.zzab, false);
    SafeParcelWriter.writeLong(paramParcel, 3, this.zzac);
    SafeParcelWriter.writeInt(paramParcel, 4, this.zzad);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */