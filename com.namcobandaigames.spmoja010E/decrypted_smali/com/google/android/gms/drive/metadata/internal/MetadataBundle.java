package com.google.android.gms.drive.metadata.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.internal.drive.zzhp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@SafeParcelable.Class(creator="MetadataBundleCreator")
@SafeParcelable.Reserved({1})
public final class MetadataBundle
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<MetadataBundle> CREATOR = new zzj();
  private static final GmsLogger zzbx = new GmsLogger("MetadataBundle", "");
  @SafeParcelable.Field(id=2)
  private final Bundle zzir;
  
  @SafeParcelable.Constructor
  MetadataBundle(@SafeParcelable.Param(id=2) Bundle paramBundle)
  {
    this.zzir = ((Bundle)Preconditions.checkNotNull(paramBundle));
    this.zzir.setClassLoader(getClass().getClassLoader());
    paramBundle = new ArrayList();
    Object localObject = this.zzir.keySet().iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      if (zzf.zzd(str) == null)
      {
        paramBundle.add(str);
        zzbx.wfmt("MetadataBundle", "Ignored unknown metadata field in bundle: %s", new Object[] { str });
      }
    }
    paramBundle = (ArrayList)paramBundle;
    int j = paramBundle.size();
    while (i < j)
    {
      localObject = paramBundle.get(i);
      i += 1;
      localObject = (String)localObject;
      this.zzir.remove((String)localObject);
    }
  }
  
  public static <T> MetadataBundle zza(MetadataField<T> paramMetadataField, T paramT)
  {
    MetadataBundle localMetadataBundle = zzaw();
    localMetadataBundle.zzb(paramMetadataField, paramT);
    return localMetadataBundle;
  }
  
  public static MetadataBundle zzaw()
  {
    return new MetadataBundle(new Bundle());
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    paramObject = (MetadataBundle)paramObject;
    Object localObject = this.zzir.keySet();
    if (!((Set)localObject).equals(((MetadataBundle)paramObject).zzir.keySet())) {
      return false;
    }
    localObject = ((Set)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      if (!Objects.equal(this.zzir.get(str), ((MetadataBundle)paramObject).zzir.get(str))) {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Iterator localIterator = this.zzir.keySet().iterator();
    String str;
    for (int i = 1; localIterator.hasNext(); i = this.zzir.get(str).hashCode() + i * 31) {
      str = (String)localIterator.next();
    }
    return i;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeBundle(paramParcel, 2, this.zzir, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
  
  @Nullable
  public final <T> T zza(MetadataField<T> paramMetadataField)
  {
    return (T)paramMetadataField.zza(this.zzir);
  }
  
  public final void zza(Context paramContext)
  {
    BitmapTeleporter localBitmapTeleporter = (BitmapTeleporter)zza(zzhp.zzka);
    if (localBitmapTeleporter != null) {
      localBitmapTeleporter.setTempDir(paramContext.getCacheDir());
    }
  }
  
  public final MetadataBundle zzax()
  {
    return new MetadataBundle(new Bundle(this.zzir));
  }
  
  public final Set<MetadataField<?>> zzay()
  {
    HashSet localHashSet = new HashSet();
    Iterator localIterator = this.zzir.keySet().iterator();
    while (localIterator.hasNext()) {
      localHashSet.add(zzf.zzd((String)localIterator.next()));
    }
    return localHashSet;
  }
  
  public final <T> void zzb(MetadataField<T> paramMetadataField, T paramT)
  {
    if (zzf.zzd(paramMetadataField.getName()) == null)
    {
      paramMetadataField = String.valueOf(paramMetadataField.getName());
      if (paramMetadataField.length() != 0) {}
      for (paramMetadataField = "Unregistered field: ".concat(paramMetadataField);; paramMetadataField = new String("Unregistered field: ")) {
        throw new IllegalArgumentException(paramMetadataField);
      }
    }
    paramMetadataField.zza(paramT, this.zzir);
  }
  
  @Nullable
  public final <T> T zzc(MetadataField<T> paramMetadataField)
  {
    Object localObject = zza(paramMetadataField);
    this.zzir.remove(paramMetadataField.getName());
    return (T)localObject;
  }
  
  public final boolean zzd(MetadataField<?> paramMetadataField)
  {
    return this.zzir.containsKey(paramMetadataField.getName());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\MetadataBundle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */