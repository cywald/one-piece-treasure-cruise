package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SafeParcelable.Class(creator="AppVisibleCustomPropertiesCreator")
@SafeParcelable.Reserved({1})
public final class AppVisibleCustomProperties
  extends AbstractSafeParcelable
  implements ReflectedParcelable, Iterable<zzc>
{
  public static final Parcelable.Creator<AppVisibleCustomProperties> CREATOR = new zza();
  public static final AppVisibleCustomProperties zzil = new zza().zzat();
  @SafeParcelable.Field(id=2)
  private final List<zzc> zzim;
  
  @SafeParcelable.Constructor
  AppVisibleCustomProperties(@SafeParcelable.Param(id=2) Collection<zzc> paramCollection)
  {
    Preconditions.checkNotNull(paramCollection);
    this.zzim = new ArrayList(paramCollection);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    return zzas().equals(((AppVisibleCustomProperties)paramObject).zzas());
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzim });
  }
  
  public final Iterator<zzc> iterator()
  {
    return this.zzim.iterator();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeTypedList(paramParcel, 2, this.zzim, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
  
  public final Map<CustomPropertyKey, String> zzas()
  {
    HashMap localHashMap = new HashMap(this.zzim.size());
    Iterator localIterator = this.zzim.iterator();
    while (localIterator.hasNext())
    {
      zzc localzzc = (zzc)localIterator.next();
      localHashMap.put(localzzc.zzio, localzzc.value);
    }
    return Collections.unmodifiableMap(localHashMap);
  }
  
  public static final class zza
  {
    private final Map<CustomPropertyKey, zzc> zzin = new HashMap();
    
    public final zza zza(CustomPropertyKey paramCustomPropertyKey, String paramString)
    {
      Preconditions.checkNotNull(paramCustomPropertyKey, "key");
      this.zzin.put(paramCustomPropertyKey, new zzc(paramCustomPropertyKey, paramString));
      return this;
    }
    
    public final zza zza(zzc paramzzc)
    {
      Preconditions.checkNotNull(paramzzc, "property");
      this.zzin.put(paramzzc.zzio, paramzzc);
      return this;
    }
    
    public final AppVisibleCustomProperties zzat()
    {
      return new AppVisibleCustomProperties(this.zzin.values());
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\AppVisibleCustomProperties.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */