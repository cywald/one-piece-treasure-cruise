package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class zzo
  extends zzl<DriveId>
  implements SearchableCollectionMetadataField<DriveId>
{
  public static final zzg zziu = new zzp();
  
  public zzo(int paramInt)
  {
    super("parents", Collections.emptySet(), Arrays.asList(new String[] { "parentsExtra", "dbInstanceId", "parentsExtraHolder" }), 4100000);
  }
  
  private static void zzc(DataHolder paramDataHolder)
  {
    Bundle localBundle = paramDataHolder.getMetadata();
    if (localBundle == null) {
      return;
    }
    try
    {
      DataHolder localDataHolder = (DataHolder)localBundle.getParcelable("parentsExtraHolder");
      if (localDataHolder != null)
      {
        localDataHolder.close();
        localBundle.remove("parentsExtraHolder");
      }
      return;
    }
    finally {}
  }
  
  protected final Collection<DriveId> zzc(Bundle paramBundle)
  {
    paramBundle = super.zzc(paramBundle);
    if (paramBundle == null) {
      return null;
    }
    return new HashSet(paramBundle);
  }
  
  protected final Collection<DriveId> zzd(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    Bundle localBundle = paramDataHolder.getMetadata();
    Object localObject1 = localBundle.getParcelableArrayList("parentsExtra");
    Object localObject4 = localObject1;
    if (localObject1 == null)
    {
      if (localBundle.getParcelable("parentsExtraHolder") != null) {}
      try
      {
        localObject1 = (DataHolder)paramDataHolder.getMetadata().getParcelable("parentsExtraHolder");
        if (localObject1 != null) {
          break label77;
        }
      }
      finally
      {
        try
        {
          for (;;)
          {
            int i = paramDataHolder.getCount();
            localObject4 = new ArrayList(i);
            HashMap localHashMap = new HashMap(i);
            paramInt2 = 0;
            int j;
            while (paramInt2 < i)
            {
              j = paramDataHolder.getWindowIndex(paramInt2);
              localObject6 = new ParentDriveIdSet();
              ((ArrayList)localObject4).add(localObject6);
              localHashMap.put(Long.valueOf(paramDataHolder.getLong("sqlId", paramInt2, j)), localObject6);
              paramInt2 += 1;
            }
            Object localObject7 = ((DataHolder)localObject1).getMetadata();
            Object localObject6 = ((Bundle)localObject7).getString("childSqlIdColumn");
            String str = ((Bundle)localObject7).getString("parentSqlIdColumn");
            localObject7 = ((Bundle)localObject7).getString("parentResIdColumn");
            i = ((DataHolder)localObject1).getCount();
            paramInt2 = 0;
            while (paramInt2 < i)
            {
              j = ((DataHolder)localObject1).getWindowIndex(paramInt2);
              ParentDriveIdSet localParentDriveIdSet = (ParentDriveIdSet)localHashMap.get(Long.valueOf(((DataHolder)localObject1).getLong((String)localObject6, paramInt2, j)));
              zzq localzzq2 = new zzq(((DataHolder)localObject1).getString((String)localObject7, paramInt2, j), ((DataHolder)localObject1).getLong(str, paramInt2, j), 1);
              localParentDriveIdSet.zzit.add(localzzq2);
              paramInt2 += 1;
            }
            paramDataHolder.getMetadata().putParcelableArrayList("parentsExtra", (ArrayList)localObject4);
            ((DataHolder)localObject1).close();
            paramDataHolder.getMetadata().remove("parentsExtraHolder");
          }
        }
        finally
        {
          ((DataHolder)localObject2).close();
          paramDataHolder.getMetadata().remove("parentsExtraHolder");
        }
        localObject2 = finally;
      }
      localObject1 = localBundle.getParcelableArrayList("parentsExtra");
      localObject4 = localObject1;
      if (localObject1 == null) {
        return null;
      }
    }
    label77:
    long l = localBundle.getLong("dbInstanceId");
    Object localObject3 = (ParentDriveIdSet)((List)localObject5).get(paramInt1);
    paramDataHolder = new HashSet();
    localObject3 = ((ParentDriveIdSet)localObject3).zzit.iterator();
    while (((Iterator)localObject3).hasNext())
    {
      zzq localzzq1 = (zzq)((Iterator)localObject3).next();
      paramDataHolder.add(new DriveId(localzzq1.zzab, localzzq1.zzac, l, localzzq1.zzad));
    }
    return paramDataHolder;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */