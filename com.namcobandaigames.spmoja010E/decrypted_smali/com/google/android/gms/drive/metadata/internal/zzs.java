package com.google.android.gms.drive.metadata.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.zza;
import com.google.android.gms.drive.metadata.zzb;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import org.json.JSONArray;
import org.json.JSONException;

public final class zzs
  extends zzb<String>
{
  public zzs(String paramString, int paramInt)
  {
    super(paramString, Collections.singleton(paramString), Collections.emptySet(), 4300000);
  }
  
  @Nullable
  protected final Collection<String> zzd(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    try
    {
      Object localObject = paramDataHolder.getString(getName(), paramInt1, paramInt2);
      if (localObject == null) {
        return null;
      }
      paramDataHolder = new ArrayList();
      localObject = new JSONArray((String)localObject);
      paramInt1 = 0;
      while (paramInt1 < ((JSONArray)localObject).length())
      {
        paramDataHolder.add(((JSONArray)localObject).getString(paramInt1));
        paramInt1 += 1;
      }
      paramDataHolder = Collections.unmodifiableCollection(paramDataHolder);
      return paramDataHolder;
    }
    catch (JSONException paramDataHolder)
    {
      throw new IllegalStateException("DataHolder supplied invalid JSON", paramDataHolder);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */