package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;

public abstract interface zzg
{
  public abstract String zzav();
  
  public abstract void zzb(DataHolder paramDataHolder);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */