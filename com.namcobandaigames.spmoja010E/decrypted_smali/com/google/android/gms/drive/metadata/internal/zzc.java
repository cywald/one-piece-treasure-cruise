package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.metadata.CustomPropertyKey;

@SafeParcelable.Class(creator="CustomPropertyCreator")
@SafeParcelable.Reserved({1})
public final class zzc
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzc> CREATOR = new zzd();
  @Nullable
  @SafeParcelable.Field(id=3)
  final String value;
  @SafeParcelable.Field(id=2)
  final CustomPropertyKey zzio;
  
  @SafeParcelable.Constructor
  public zzc(@SafeParcelable.Param(id=2) CustomPropertyKey paramCustomPropertyKey, @Nullable @SafeParcelable.Param(id=3) String paramString)
  {
    Preconditions.checkNotNull(paramCustomPropertyKey, "key");
    this.zzio = paramCustomPropertyKey;
    this.value = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (paramObject.getClass() != getClass())) {
        return false;
      }
      paramObject = (zzc)paramObject;
    } while ((Objects.equal(this.zzio, ((zzc)paramObject).zzio)) && (Objects.equal(this.value, ((zzc)paramObject).value)));
    return false;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzio, this.value });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzio, paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 3, this.value, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */