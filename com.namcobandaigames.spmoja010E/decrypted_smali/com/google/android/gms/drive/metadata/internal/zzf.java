package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.internal.drive.zzhp;
import com.google.android.gms.internal.drive.zzia;
import com.google.android.gms.internal.drive.zzic;
import com.google.android.gms.internal.drive.zzik;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class zzf
{
  private static final Map<String, MetadataField<?>> zzip = new HashMap();
  private static final Map<String, zzg> zziq = new HashMap();
  
  static
  {
    zzb(zzhp.zziv);
    zzb(zzhp.zzkb);
    zzb(zzhp.zzjs);
    zzb(zzhp.zzjz);
    zzb(zzhp.zzkc);
    zzb(zzhp.zzji);
    zzb(zzhp.zzjh);
    zzb(zzhp.zzjj);
    zzb(zzhp.zzjk);
    zzb(zzhp.zzjl);
    zzb(zzhp.zzjf);
    zzb(zzhp.zzjn);
    zzb(zzhp.zzjo);
    zzb(zzhp.zzjp);
    zzb(zzhp.zzjx);
    zzb(zzhp.zziw);
    zzb(zzhp.zzju);
    zzb(zzhp.zziy);
    zzb(zzhp.zzjg);
    zzb(zzhp.zziz);
    zzb(zzhp.zzja);
    zzb(zzhp.zzjb);
    zzb(zzhp.zzjc);
    zzb(zzhp.zzjr);
    zzb(zzhp.zzjm);
    zzb(zzhp.zzjt);
    zzb(zzhp.zzjv);
    zzb(zzhp.zzjw);
    zzb(zzhp.zzjy);
    zzb(zzhp.zzkd);
    zzb(zzhp.zzke);
    zzb(zzhp.zzje);
    zzb(zzhp.zzjd);
    zzb(zzhp.zzka);
    zzb(zzhp.zzjq);
    zzb(zzhp.zzix);
    zzb(zzhp.zzkf);
    zzb(zzhp.zzkg);
    zzb(zzhp.zzkh);
    zzb(zzhp.zzki);
    zzb(zzhp.zzkj);
    zzb(zzhp.zzkk);
    zzb(zzhp.zzkl);
    zzb(zzic.zzkn);
    zzb(zzic.zzkp);
    zzb(zzic.zzkq);
    zzb(zzic.zzkr);
    zzb(zzic.zzko);
    zzb(zzic.zzks);
    zzb(zzik.zzku);
    zzb(zzik.zzkv);
    zza(zzo.zziu);
    zza(zzia.zzkm);
  }
  
  public static void zza(DataHolder paramDataHolder)
  {
    Iterator localIterator = zziq.values().iterator();
    while (localIterator.hasNext()) {
      ((zzg)localIterator.next()).zzb(paramDataHolder);
    }
  }
  
  private static void zza(zzg paramzzg)
  {
    if (zziq.put(paramzzg.zzav(), paramzzg) != null)
    {
      paramzzg = paramzzg.zzav();
      throw new IllegalStateException(String.valueOf(paramzzg).length() + 46 + "A cleaner for key " + paramzzg + " has already been registered");
    }
  }
  
  public static Collection<MetadataField<?>> zzau()
  {
    return Collections.unmodifiableCollection(zzip.values());
  }
  
  private static void zzb(MetadataField<?> paramMetadataField)
  {
    if (zzip.containsKey(paramMetadataField.getName()))
    {
      paramMetadataField = String.valueOf(paramMetadataField.getName());
      if (paramMetadataField.length() != 0) {}
      for (paramMetadataField = "Duplicate field name registered: ".concat(paramMetadataField);; paramMetadataField = new String("Duplicate field name registered: ")) {
        throw new IllegalArgumentException(paramMetadataField);
      }
    }
    zzip.put(paramMetadataField.getName(), paramMetadataField);
  }
  
  public static MetadataField<?> zzd(String paramString)
  {
    return (MetadataField)zzip.get(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */