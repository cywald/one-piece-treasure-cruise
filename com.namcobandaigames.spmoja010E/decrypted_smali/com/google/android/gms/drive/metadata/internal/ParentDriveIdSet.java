package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import java.util.ArrayList;
import java.util.List;

@SafeParcelable.Class(creator="ParentDriveIdSetCreator")
@SafeParcelable.Reserved({1})
public class ParentDriveIdSet
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<ParentDriveIdSet> CREATOR = new zzn();
  @SafeParcelable.Field(id=2)
  final List<zzq> zzit;
  
  public ParentDriveIdSet()
  {
    this(new ArrayList());
  }
  
  @SafeParcelable.Constructor
  ParentDriveIdSet(@SafeParcelable.Param(id=2) List<zzq> paramList)
  {
    this.zzit = paramList;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeTypedList(paramParcel, 2, this.zzit, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\metadata\internal\ParentDriveIdSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */