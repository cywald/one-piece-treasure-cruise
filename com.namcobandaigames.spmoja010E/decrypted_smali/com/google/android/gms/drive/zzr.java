package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="PermissionCreator")
@SafeParcelable.Reserved({1})
public final class zzr
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzr> CREATOR = new zzs();
  @SafeParcelable.Field(getter="getAccountType", id=3)
  private int accountType;
  @Nullable
  @SafeParcelable.Field(getter="getAccountIdentifier", id=2)
  private String zzbe;
  @Nullable
  @SafeParcelable.Field(getter="getAccountDisplayName", id=4)
  private String zzbf;
  @Nullable
  @SafeParcelable.Field(getter="getPhotoLink", id=5)
  private String zzbg;
  @SafeParcelable.Field(getter="getRole", id=6)
  private int zzbh;
  @SafeParcelable.Field(getter="isLinkRequiredForAccess", id=7)
  private boolean zzbi;
  
  @SafeParcelable.Constructor
  public zzr(@Nullable @SafeParcelable.Param(id=2) String paramString1, @SafeParcelable.Param(id=3) int paramInt1, @Nullable @SafeParcelable.Param(id=4) String paramString2, @Nullable @SafeParcelable.Param(id=5) String paramString3, @SafeParcelable.Param(id=6) int paramInt2, @SafeParcelable.Param(id=7) boolean paramBoolean)
  {
    this.zzbe = paramString1;
    this.accountType = paramInt1;
    this.zzbf = paramString2;
    this.zzbg = paramString3;
    this.zzbh = paramInt2;
    this.zzbi = paramBoolean;
  }
  
  private static boolean zzb(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zzr)paramObject;
      if ((!Objects.equal(this.zzbe, ((zzr)paramObject).zzbe)) || (this.accountType != ((zzr)paramObject).accountType) || (this.zzbh != ((zzr)paramObject).zzbh)) {
        break;
      }
      bool1 = bool2;
    } while (this.zzbi == ((zzr)paramObject).zzbi);
    return false;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzbe, Integer.valueOf(this.accountType), Integer.valueOf(this.zzbh), Boolean.valueOf(this.zzbi) });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = -1;
    int j = SafeParcelWriter.beginObjectHeader(paramParcel);
    String str;
    if (!zzb(this.accountType))
    {
      str = null;
      SafeParcelWriter.writeString(paramParcel, 2, str, false);
      if (zzb(this.accountType)) {
        break label141;
      }
      paramInt = -1;
      label41:
      SafeParcelWriter.writeInt(paramParcel, 3, paramInt);
      SafeParcelWriter.writeString(paramParcel, 4, this.zzbf, false);
      SafeParcelWriter.writeString(paramParcel, 5, this.zzbg, false);
      switch (this.zzbh)
      {
      default: 
        paramInt = 0;
        label102:
        if (paramInt != 0) {
          break;
        }
      }
    }
    for (paramInt = i;; paramInt = this.zzbh)
    {
      SafeParcelWriter.writeInt(paramParcel, 6, paramInt);
      SafeParcelWriter.writeBoolean(paramParcel, 7, this.zzbi);
      SafeParcelWriter.finishObjectHeader(paramParcel, j);
      return;
      str = this.zzbe;
      break;
      label141:
      paramInt = this.accountType;
      break label41;
      paramInt = 1;
      break label102;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */