package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.drive.zzbn;
import com.google.android.gms.internal.drive.zzbs;
import com.google.android.gms.internal.drive.zzdp;
import com.google.android.gms.internal.drive.zzhn;
import com.google.android.gms.internal.drive.zzho;
import com.google.android.gms.internal.drive.zziw;
import com.google.android.gms.internal.drive.zzix;

@SafeParcelable.Class(creator="DriveIdCreator")
@SafeParcelable.Reserved({1})
public class DriveId
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<DriveId> CREATOR = new zzk();
  public static final int RESOURCE_TYPE_FILE = 0;
  public static final int RESOURCE_TYPE_FOLDER = 1;
  public static final int RESOURCE_TYPE_UNKNOWN = -1;
  @SafeParcelable.Field(id=2)
  private final String zzab;
  @SafeParcelable.Field(id=3)
  private final long zzac;
  @SafeParcelable.Field(defaultValueUnchecked="com.google.android.gms.drive.DriveId.RESOURCE_TYPE_UNKNOWN", id=5)
  private final int zzad;
  private volatile String zzae = null;
  @SafeParcelable.Field(id=4)
  private final long zzf;
  private volatile String zzh = null;
  
  @SafeParcelable.Constructor
  public DriveId(@SafeParcelable.Param(id=2) String paramString, @SafeParcelable.Param(id=3) long paramLong1, @SafeParcelable.Param(id=4) long paramLong2, @SafeParcelable.Param(id=5) int paramInt)
  {
    this.zzab = paramString;
    if (!"".equals(paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      Preconditions.checkArgument(bool1);
      if (paramString == null)
      {
        bool1 = bool2;
        if (paramLong1 == -1L) {}
      }
      else
      {
        bool1 = true;
      }
      Preconditions.checkArgument(bool1);
      this.zzac = paramLong1;
      this.zzf = paramLong2;
      this.zzad = paramInt;
      return;
    }
  }
  
  public static DriveId decodeFromString(String paramString)
  {
    boolean bool = paramString.startsWith("DriveId:");
    String str = String.valueOf(paramString);
    if (str.length() != 0) {}
    for (str = "Invalid DriveId: ".concat(str);; str = new String("Invalid DriveId: "))
    {
      Preconditions.checkArgument(bool, str);
      return zza(Base64.decode(paramString.substring(8), 10));
    }
  }
  
  @VisibleForTesting
  public static DriveId zza(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    return new DriveId(paramString, -1L, -1L, -1);
  }
  
  @VisibleForTesting
  private static DriveId zza(byte[] paramArrayOfByte)
  {
    for (;;)
    {
      zzhn localzzhn;
      try
      {
        localzzhn = (zzhn)zzix.zza(new zzhn(), paramArrayOfByte, 0, paramArrayOfByte.length);
        if ("".equals(localzzhn.zzab))
        {
          paramArrayOfByte = null;
          return new DriveId(paramArrayOfByte, localzzhn.zzac, localzzhn.zzf, localzzhn.zzad);
        }
      }
      catch (zziw paramArrayOfByte)
      {
        throw new IllegalArgumentException();
      }
      paramArrayOfByte = localzzhn.zzab;
    }
  }
  
  public DriveFile asDriveFile()
  {
    if (this.zzad == 1) {
      throw new IllegalStateException("This DriveId corresponds to a folder. Call asDriveFolder instead.");
    }
    return new zzbn(this);
  }
  
  public DriveFolder asDriveFolder()
  {
    if (this.zzad == 0) {
      throw new IllegalStateException("This DriveId corresponds to a file. Call asDriveFile instead.");
    }
    return new zzbs(this);
  }
  
  public DriveResource asDriveResource()
  {
    if (this.zzad == 1) {
      return asDriveFolder();
    }
    if (this.zzad == 0) {
      return asDriveFile();
    }
    return new zzdp(this);
  }
  
  public final String encodeToString()
  {
    Object localObject;
    if (this.zzh == null)
    {
      localObject = new zzhn();
      ((zzhn)localObject).versionCode = 1;
      if (this.zzab != null) {
        break label103;
      }
      str = "";
      ((zzhn)localObject).zzab = str;
      ((zzhn)localObject).zzac = this.zzac;
      ((zzhn)localObject).zzf = this.zzf;
      ((zzhn)localObject).zzad = this.zzad;
      localObject = Base64.encodeToString(zzix.zza((zzix)localObject), 10);
      str = String.valueOf("DriveId:");
      localObject = String.valueOf(localObject);
      if (((String)localObject).length() == 0) {
        break label111;
      }
    }
    label103:
    label111:
    for (String str = str.concat((String)localObject);; str = new String(str))
    {
      this.zzh = str;
      return this.zzh;
      str = this.zzab;
      break;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != DriveId.class)) {}
    do
    {
      do
      {
        do
        {
          return false;
          paramObject = (DriveId)paramObject;
        } while (((DriveId)paramObject).zzf != this.zzf);
        if ((((DriveId)paramObject).zzac == -1L) && (this.zzac == -1L)) {
          return ((DriveId)paramObject).zzab.equals(this.zzab);
        }
        if ((this.zzab != null) && (((DriveId)paramObject).zzab != null)) {
          break;
        }
      } while (((DriveId)paramObject).zzac != this.zzac);
      return true;
    } while ((((DriveId)paramObject).zzac != this.zzac) || (!((DriveId)paramObject).zzab.equals(this.zzab)));
    return true;
  }
  
  @Nullable
  public String getResourceId()
  {
    return this.zzab;
  }
  
  public int getResourceType()
  {
    return this.zzad;
  }
  
  public int hashCode()
  {
    if (this.zzac == -1L) {
      return this.zzab.hashCode();
    }
    String str1 = String.valueOf(String.valueOf(this.zzf));
    String str2 = String.valueOf(String.valueOf(this.zzac));
    if (str2.length() != 0) {}
    for (str1 = str1.concat(str2);; str1 = new String(str1)) {
      return str1.hashCode();
    }
  }
  
  public final String toInvariantString()
  {
    if (this.zzae == null)
    {
      zzho localzzho = new zzho();
      localzzho.zzac = this.zzac;
      localzzho.zzf = this.zzf;
      this.zzae = Base64.encodeToString(zzix.zza(localzzho), 10);
    }
    return this.zzae;
  }
  
  public String toString()
  {
    return encodeToString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.zzab, false);
    SafeParcelWriter.writeLong(paramParcel, 3, this.zzac);
    SafeParcelWriter.writeLong(paramParcel, 4, this.zzf);
    SafeParcelWriter.writeInt(paramParcel, 5, this.zzad);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\DriveId.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */