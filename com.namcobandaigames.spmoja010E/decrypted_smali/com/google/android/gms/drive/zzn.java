package com.google.android.gms.drive;

import android.support.annotation.Nullable;

@Deprecated
public final class zzn
  extends ExecutionOptions
{
  private boolean zzar;
  
  private zzn(@Nullable String paramString, boolean paramBoolean1, int paramInt, boolean paramBoolean2)
  {
    super(paramString, paramBoolean1, paramInt);
    this.zzar = paramBoolean2;
  }
  
  public static zzn zza(@Nullable ExecutionOptions paramExecutionOptions)
  {
    zzp localzzp = new zzp();
    if (paramExecutionOptions != null)
    {
      localzzp.setConflictStrategy(paramExecutionOptions.zzm());
      localzzp.setNotifyOnCompletion(paramExecutionOptions.zzl());
      paramExecutionOptions = paramExecutionOptions.zzk();
      if (paramExecutionOptions != null) {
        localzzp.setTrackingTag(paramExecutionOptions);
      }
    }
    return (zzn)localzzp.build();
  }
  
  public final boolean zzo()
  {
    return this.zzar;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */