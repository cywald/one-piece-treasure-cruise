package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.metadata.zzb;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

@SafeParcelable.Class(creator="InFilterCreator")
@SafeParcelable.Reserved({1000})
public final class zzp<T>
  extends zza
{
  public static final zzq CREATOR = new zzq();
  @SafeParcelable.Field(id=1)
  private final MetadataBundle zzlk;
  private final zzb<T> zzlx;
  
  public zzp(SearchableCollectionMetadataField<T> paramSearchableCollectionMetadataField, T paramT)
  {
    this(MetadataBundle.zza(paramSearchableCollectionMetadataField, Collections.singleton(paramT)));
  }
  
  @SafeParcelable.Constructor
  zzp(@SafeParcelable.Param(id=1) MetadataBundle paramMetadataBundle)
  {
    this.zzlk = paramMetadataBundle;
    this.zzlx = ((zzb)zzi.zza(paramMetadataBundle));
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, this.zzlk, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final <F> F zza(zzj<F> paramzzj)
  {
    return (F)paramzzj.zza(this.zzlx, ((Collection)this.zzlk.zza(this.zzlx)).iterator().next());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */