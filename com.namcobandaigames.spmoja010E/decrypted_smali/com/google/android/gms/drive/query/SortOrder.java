package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.query.internal.zzf;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SafeParcelable.Class(creator="SortOrderCreator")
@SafeParcelable.Reserved({1000})
public class SortOrder
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SortOrder> CREATOR = new zzc();
  @SafeParcelable.Field(id=1)
  private final List<zzf> zzlg;
  @SafeParcelable.Field(defaultValue="false", id=2)
  private final boolean zzlh;
  
  @SafeParcelable.Constructor
  SortOrder(@SafeParcelable.Param(id=1) List<zzf> paramList, @SafeParcelable.Param(id=2) boolean paramBoolean)
  {
    this.zzlg = paramList;
    this.zzlh = paramBoolean;
  }
  
  public String toString()
  {
    return String.format(Locale.US, "SortOrder[%s, %s]", new Object[] { TextUtils.join(",", this.zzlg), Boolean.valueOf(this.zzlh) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeTypedList(paramParcel, 1, this.zzlg, false);
    SafeParcelWriter.writeBoolean(paramParcel, 2, this.zzlh);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private final List<zzf> zzlg = new ArrayList();
    private boolean zzlh = false;
    
    public Builder addSortAscending(SortableMetadataField paramSortableMetadataField)
    {
      this.zzlg.add(new zzf(paramSortableMetadataField.getName(), true));
      return this;
    }
    
    public Builder addSortDescending(SortableMetadataField paramSortableMetadataField)
    {
      this.zzlg.add(new zzf(paramSortableMetadataField.getName(), false));
      return this;
    }
    
    public SortOrder build()
    {
      return new SortOrder(this.zzlg, false);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\SortOrder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */