package com.google.android.gms.drive.query;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SearchableOrderedMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties.zza;
import com.google.android.gms.drive.query.internal.zzb;
import com.google.android.gms.drive.query.internal.zzd;
import com.google.android.gms.drive.query.internal.zzn;
import com.google.android.gms.drive.query.internal.zzp;
import com.google.android.gms.drive.query.internal.zzr;
import com.google.android.gms.drive.query.internal.zzv;
import com.google.android.gms.drive.query.internal.zzx;
import com.google.android.gms.drive.query.internal.zzz;

public class Filters
{
  public static Filter and(@NonNull Filter paramFilter, @NonNull Filter... paramVarArgs)
  {
    Preconditions.checkNotNull(paramFilter, "Filter may not be null.");
    Preconditions.checkNotNull(paramVarArgs, "Additional filters may not be null.");
    return new zzr(zzx.zzmf, paramFilter, paramVarArgs);
  }
  
  public static Filter and(@NonNull Iterable<Filter> paramIterable)
  {
    Preconditions.checkNotNull(paramIterable, "Filters may not be null");
    return new zzr(zzx.zzmf, paramIterable);
  }
  
  public static Filter contains(@NonNull SearchableMetadataField<String> paramSearchableMetadataField, @NonNull String paramString)
  {
    Preconditions.checkNotNull(paramSearchableMetadataField, "Field may not be null.");
    Preconditions.checkNotNull(paramString, "Value may not be null.");
    return new zzb(zzx.zzmi, paramSearchableMetadataField, paramString);
  }
  
  public static Filter eq(@NonNull CustomPropertyKey paramCustomPropertyKey, @NonNull String paramString)
  {
    Preconditions.checkNotNull(paramCustomPropertyKey, "Custom property key may not be null.");
    Preconditions.checkNotNull(paramString, "Custom property value may not be null.");
    return new zzn(SearchableField.zzlf, new AppVisibleCustomProperties.zza().zza(paramCustomPropertyKey, paramString).zzat());
  }
  
  public static <T> Filter eq(@NonNull SearchableMetadataField<T> paramSearchableMetadataField, @NonNull T paramT)
  {
    Preconditions.checkNotNull(paramSearchableMetadataField, "Field may not be null.");
    Preconditions.checkNotNull(paramT, "Value may not be null.");
    return new zzb(zzx.zzma, paramSearchableMetadataField, paramT);
  }
  
  public static <T extends Comparable<T>> Filter greaterThan(@NonNull SearchableOrderedMetadataField<T> paramSearchableOrderedMetadataField, @NonNull T paramT)
  {
    Preconditions.checkNotNull(paramSearchableOrderedMetadataField, "Field may not be null.");
    Preconditions.checkNotNull(paramT, "Value may not be null.");
    return new zzb(zzx.zzmd, paramSearchableOrderedMetadataField, paramT);
  }
  
  public static <T extends Comparable<T>> Filter greaterThanEquals(@NonNull SearchableOrderedMetadataField<T> paramSearchableOrderedMetadataField, @NonNull T paramT)
  {
    Preconditions.checkNotNull(paramSearchableOrderedMetadataField, "Field may not be null.");
    Preconditions.checkNotNull(paramT, "Value may not be null.");
    return new zzb(zzx.zzme, paramSearchableOrderedMetadataField, paramT);
  }
  
  public static <T> Filter in(@NonNull SearchableCollectionMetadataField<T> paramSearchableCollectionMetadataField, @NonNull T paramT)
  {
    Preconditions.checkNotNull(paramSearchableCollectionMetadataField, "Field may not be null.");
    Preconditions.checkNotNull(paramT, "Value may not be null.");
    return new zzp(paramSearchableCollectionMetadataField, paramT);
  }
  
  public static <T extends Comparable<T>> Filter lessThan(@NonNull SearchableOrderedMetadataField<T> paramSearchableOrderedMetadataField, @NonNull T paramT)
  {
    Preconditions.checkNotNull(paramSearchableOrderedMetadataField, "Field may not be null.");
    Preconditions.checkNotNull(paramT, "Value may not be null.");
    return new zzb(zzx.zzmb, paramSearchableOrderedMetadataField, paramT);
  }
  
  public static <T extends Comparable<T>> Filter lessThanEquals(@NonNull SearchableOrderedMetadataField<T> paramSearchableOrderedMetadataField, @NonNull T paramT)
  {
    Preconditions.checkNotNull(paramSearchableOrderedMetadataField, "Field may not be null.");
    Preconditions.checkNotNull(paramT, "Value may not be null.");
    return new zzb(zzx.zzmc, paramSearchableOrderedMetadataField, paramT);
  }
  
  public static Filter not(@NonNull Filter paramFilter)
  {
    Preconditions.checkNotNull(paramFilter, "Filter may not be null");
    return new zzv(paramFilter);
  }
  
  public static Filter openedByMe()
  {
    return new zzd(SearchableField.LAST_VIEWED_BY_ME);
  }
  
  public static Filter or(@NonNull Filter paramFilter, @NonNull Filter... paramVarArgs)
  {
    Preconditions.checkNotNull(paramFilter, "Filter may not be null.");
    Preconditions.checkNotNull(paramVarArgs, "Additional filters may not be null.");
    return new zzr(zzx.zzmg, paramFilter, paramVarArgs);
  }
  
  public static Filter or(@NonNull Iterable<Filter> paramIterable)
  {
    Preconditions.checkNotNull(paramIterable, "Filters may not be null");
    return new zzr(zzx.zzmg, paramIterable);
  }
  
  public static Filter ownedByMe()
  {
    return new zzz();
  }
  
  public static Filter sharedWithMe()
  {
    return new zzd(SearchableField.zzle);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\Filters.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */