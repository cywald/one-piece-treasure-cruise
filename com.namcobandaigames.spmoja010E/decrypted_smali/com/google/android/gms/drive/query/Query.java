package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.drive.query.internal.zzr;
import com.google.android.gms.drive.query.internal.zzt;
import com.google.android.gms.drive.query.internal.zzx;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@SafeParcelable.Class(creator="QueryCreator")
@SafeParcelable.Reserved({1000})
public class Query
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Query> CREATOR = new zzb();
  @SafeParcelable.Field(id=7)
  private final List<DriveSpace> zzbw;
  @SafeParcelable.Field(id=1)
  private final zzr zzkw;
  @SafeParcelable.Field(id=3)
  private final String zzkx;
  @Nullable
  @SafeParcelable.Field(id=4)
  private final SortOrder zzky;
  @SafeParcelable.Field(id=5)
  final List<String> zzkz;
  @SafeParcelable.Field(id=6)
  final boolean zzla;
  @SafeParcelable.Field(id=8)
  final boolean zzlb;
  
  @SafeParcelable.Constructor
  Query(@SafeParcelable.Param(id=1) zzr paramzzr, @SafeParcelable.Param(id=3) String paramString, @Nullable @SafeParcelable.Param(id=4) SortOrder paramSortOrder, @NonNull @SafeParcelable.Param(id=5) List<String> paramList, @SafeParcelable.Param(id=6) boolean paramBoolean1, @NonNull @SafeParcelable.Param(id=7) List<DriveSpace> paramList1, @SafeParcelable.Param(id=8) boolean paramBoolean2)
  {
    this.zzkw = paramzzr;
    this.zzkx = paramString;
    this.zzky = paramSortOrder;
    this.zzkz = paramList;
    this.zzla = paramBoolean1;
    this.zzbw = paramList1;
    this.zzlb = paramBoolean2;
  }
  
  private Query(zzr paramzzr, String paramString, SortOrder paramSortOrder, @NonNull List<String> paramList, boolean paramBoolean1, @NonNull Set<DriveSpace> paramSet, boolean paramBoolean2)
  {
    this(paramzzr, paramString, paramSortOrder, paramList, paramBoolean1, new ArrayList(paramSet), paramBoolean2);
  }
  
  public Filter getFilter()
  {
    return this.zzkw;
  }
  
  @Deprecated
  public String getPageToken()
  {
    return this.zzkx;
  }
  
  @Nullable
  public SortOrder getSortOrder()
  {
    return this.zzky;
  }
  
  public String toString()
  {
    return String.format(Locale.US, "Query[%s,%s,PageToken=%s,Spaces=%s]", new Object[] { this.zzkw, this.zzky, this.zzkx, this.zzbw });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, this.zzkw, paramInt, false);
    SafeParcelWriter.writeString(paramParcel, 3, this.zzkx, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzky, paramInt, false);
    SafeParcelWriter.writeStringList(paramParcel, 5, this.zzkz, false);
    SafeParcelWriter.writeBoolean(paramParcel, 6, this.zzla);
    SafeParcelWriter.writeTypedList(paramParcel, 7, this.zzbw, false);
    SafeParcelWriter.writeBoolean(paramParcel, 8, this.zzlb);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final Set<DriveSpace> zzba()
  {
    return new HashSet(this.zzbw);
  }
  
  @VisibleForTesting
  public static class Builder
  {
    private String zzkx;
    private SortOrder zzky;
    private List<String> zzkz = Collections.emptyList();
    private boolean zzla;
    private boolean zzlb;
    private final List<Filter> zzlc = new ArrayList();
    private Set<DriveSpace> zzld = Collections.emptySet();
    
    public Builder() {}
    
    public Builder(Query paramQuery)
    {
      this.zzlc.add(paramQuery.getFilter());
      this.zzkx = paramQuery.getPageToken();
      this.zzky = paramQuery.getSortOrder();
      this.zzkz = paramQuery.zzkz;
      this.zzla = paramQuery.zzla;
      paramQuery.zzba();
      this.zzld = paramQuery.zzba();
      this.zzlb = paramQuery.zzlb;
    }
    
    public Builder addFilter(@NonNull Filter paramFilter)
    {
      Preconditions.checkNotNull(paramFilter, "Filter may not be null.");
      if (!(paramFilter instanceof zzt)) {
        this.zzlc.add(paramFilter);
      }
      return this;
    }
    
    public Query build()
    {
      return new Query(new zzr(zzx.zzmf, this.zzlc), this.zzkx, this.zzky, this.zzkz, this.zzla, this.zzld, this.zzlb, null);
    }
    
    @Deprecated
    public Builder setPageToken(String paramString)
    {
      this.zzkx = paramString;
      return this;
    }
    
    public Builder setSortOrder(SortOrder paramSortOrder)
    {
      this.zzky = paramSortOrder;
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\Query.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */