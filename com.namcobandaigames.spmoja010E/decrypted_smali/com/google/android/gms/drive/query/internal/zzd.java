package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

@SafeParcelable.Class(creator="FieldOnlyFilterCreator")
@SafeParcelable.Reserved({1000})
public final class zzd
  extends zza
{
  public static final Parcelable.Creator<zzd> CREATOR = new zze();
  @SafeParcelable.Field(id=1)
  private final MetadataBundle zzlk;
  private final MetadataField<?> zzll;
  
  public zzd(SearchableMetadataField<?> paramSearchableMetadataField)
  {
    this(MetadataBundle.zza(paramSearchableMetadataField, null));
  }
  
  @SafeParcelable.Constructor
  zzd(@SafeParcelable.Param(id=1) MetadataBundle paramMetadataBundle)
  {
    this.zzlk = paramMetadataBundle;
    this.zzll = zzi.zza(paramMetadataBundle);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, this.zzlk, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final <T> T zza(zzj<T> paramzzj)
  {
    return (T)paramzzj.zze(this.zzll);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */