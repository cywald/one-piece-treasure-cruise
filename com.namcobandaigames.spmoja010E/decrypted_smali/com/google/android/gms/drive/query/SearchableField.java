package com.google.android.gms.drive.query;

import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SearchableOrderedMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.internal.drive.zzhp;
import com.google.android.gms.internal.drive.zzic;
import java.util.Date;

public class SearchableField
{
  public static final SearchableMetadataField<Boolean> IS_PINNED = zzhp.zzjk;
  public static final SearchableOrderedMetadataField<Date> LAST_VIEWED_BY_ME;
  public static final SearchableMetadataField<String> MIME_TYPE;
  public static final SearchableOrderedMetadataField<Date> MODIFIED_DATE;
  public static final SearchableCollectionMetadataField<DriveId> PARENTS;
  public static final SearchableMetadataField<Boolean> STARRED;
  public static final SearchableMetadataField<String> TITLE = zzhp.zzkb;
  public static final SearchableMetadataField<Boolean> TRASHED;
  public static final SearchableOrderedMetadataField<Date> zzle;
  public static final SearchableMetadataField<AppVisibleCustomProperties> zzlf = zzhp.zzix;
  
  static
  {
    MIME_TYPE = zzhp.zzjs;
    TRASHED = zzhp.zzkc;
    PARENTS = zzhp.zzjx;
    zzle = zzic.zzkr;
    STARRED = zzhp.zzjz;
    MODIFIED_DATE = zzic.zzkp;
    LAST_VIEWED_BY_ME = zzic.zzko;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\SearchableField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */