package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import java.util.Locale;

@SafeParcelable.Class(creator="FieldWithSortOrderCreator")
@SafeParcelable.Reserved({1000})
public final class zzf
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzf> CREATOR = new zzg();
  @SafeParcelable.Field(id=1)
  private final String fieldName;
  @SafeParcelable.Field(id=2)
  private final boolean zzlm;
  
  @SafeParcelable.Constructor
  public zzf(@SafeParcelable.Param(id=1) String paramString, @SafeParcelable.Param(id=2) boolean paramBoolean)
  {
    this.fieldName = paramString;
    this.zzlm = paramBoolean;
  }
  
  public final String toString()
  {
    Locale localLocale = Locale.US;
    String str2 = this.fieldName;
    if (this.zzlm) {}
    for (String str1 = "ASC";; str1 = "DESC") {
      return String.format(localLocale, "FieldWithSortOrder[%s %s]", new Object[] { str2, str1 });
    }
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, this.fieldName, false);
    SafeParcelWriter.writeBoolean(paramParcel, 2, this.zzlm);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */