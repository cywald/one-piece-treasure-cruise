package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

@SafeParcelable.Class(creator="ComparisonFilterCreator")
@SafeParcelable.Reserved({1000})
public final class zzb<T>
  extends zza
{
  public static final zzc CREATOR = new zzc();
  @SafeParcelable.Field(id=1)
  private final zzx zzlj;
  @SafeParcelable.Field(id=2)
  private final MetadataBundle zzlk;
  private final MetadataField<T> zzll;
  
  public zzb(zzx paramzzx, SearchableMetadataField<T> paramSearchableMetadataField, T paramT)
  {
    this(paramzzx, MetadataBundle.zza(paramSearchableMetadataField, paramT));
  }
  
  @SafeParcelable.Constructor
  zzb(@SafeParcelable.Param(id=1) zzx paramzzx, @SafeParcelable.Param(id=2) MetadataBundle paramMetadataBundle)
  {
    this.zzlj = paramzzx;
    this.zzlk = paramMetadataBundle;
    this.zzll = zzi.zza(paramMetadataBundle);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, this.zzlj, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzlk, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final <F> F zza(zzj<F> paramzzj)
  {
    return (F)paramzzj.zza(this.zzlj, this.zzll, this.zzlk.zza(this.zzll));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */