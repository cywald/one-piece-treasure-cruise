package com.google.android.gms.drive.query;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.query.internal.zzj;

public abstract interface Filter
  extends SafeParcelable
{
  public abstract <T> T zza(zzj<T> paramzzj);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\Filter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */