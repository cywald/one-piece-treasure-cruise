package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="FullTextSearchFilterCreator")
@SafeParcelable.Reserved({1000})
public final class zzl
  extends zza
{
  public static final Parcelable.Creator<zzl> CREATOR = new zzm();
  @SafeParcelable.Field(id=1)
  private final String value;
  
  @SafeParcelable.Constructor
  public zzl(@SafeParcelable.Param(id=1) String paramString)
  {
    this.value = paramString;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, this.value, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
  
  public final <F> F zza(zzj<F> paramzzj)
  {
    return (F)paramzzj.zzg(this.value);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */