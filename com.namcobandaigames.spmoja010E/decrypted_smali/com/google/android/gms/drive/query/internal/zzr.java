package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.query.Filter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SafeParcelable.Class(creator="LogicalFilterCreator")
@SafeParcelable.Reserved({1000})
public final class zzr
  extends zza
{
  public static final Parcelable.Creator<zzr> CREATOR = new zzs();
  private List<Filter> zzlc;
  @SafeParcelable.Field(id=1)
  private final zzx zzlj;
  @SafeParcelable.Field(id=2)
  private final List<FilterHolder> zzly;
  
  public zzr(zzx paramzzx, Filter paramFilter, Filter... paramVarArgs)
  {
    this.zzlj = paramzzx;
    this.zzly = new ArrayList(paramVarArgs.length + 1);
    this.zzly.add(new FilterHolder(paramFilter));
    this.zzlc = new ArrayList(paramVarArgs.length + 1);
    this.zzlc.add(paramFilter);
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      paramzzx = paramVarArgs[i];
      this.zzly.add(new FilterHolder(paramzzx));
      this.zzlc.add(paramzzx);
      i += 1;
    }
  }
  
  public zzr(zzx paramzzx, Iterable<Filter> paramIterable)
  {
    this.zzlj = paramzzx;
    this.zzlc = new ArrayList();
    this.zzly = new ArrayList();
    paramzzx = paramIterable.iterator();
    while (paramzzx.hasNext())
    {
      paramIterable = (Filter)paramzzx.next();
      this.zzlc.add(paramIterable);
      this.zzly.add(new FilterHolder(paramIterable));
    }
  }
  
  @SafeParcelable.Constructor
  zzr(@SafeParcelable.Param(id=1) zzx paramzzx, @SafeParcelable.Param(id=2) List<FilterHolder> paramList)
  {
    this.zzlj = paramzzx;
    this.zzly = paramList;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, this.zzlj, paramInt, false);
    SafeParcelWriter.writeTypedList(paramParcel, 2, this.zzly, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final <T> T zza(zzj<T> paramzzj)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.zzly.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(((FilterHolder)localIterator.next()).getFilter().zza(paramzzj));
    }
    return (T)paramzzj.zza(this.zzlj, localArrayList);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */