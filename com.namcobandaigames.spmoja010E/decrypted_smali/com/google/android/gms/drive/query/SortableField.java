package com.google.android.gms.drive.query;

import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.internal.drive.zzhp;
import com.google.android.gms.internal.drive.zzic;
import java.util.Date;

public class SortableField
{
  public static final SortableMetadataField<Date> CREATED_DATE;
  public static final SortableMetadataField<Date> LAST_VIEWED_BY_ME;
  public static final SortableMetadataField<Date> MODIFIED_BY_ME_DATE;
  public static final SortableMetadataField<Date> MODIFIED_DATE;
  public static final SortableMetadataField<Long> QUOTA_USED = zzhp.zzjy;
  public static final SortableMetadataField<Date> SHARED_WITH_ME_DATE;
  public static final SortableMetadataField<String> TITLE = zzhp.zzkb;
  private static final SortableMetadataField<Date> zzli = zzic.zzks;
  
  static
  {
    CREATED_DATE = zzic.zzkn;
    MODIFIED_DATE = zzic.zzkp;
    MODIFIED_BY_ME_DATE = zzic.zzkq;
    LAST_VIEWED_BY_ME = zzic.zzko;
    SHARED_WITH_ME_DATE = zzic.zzkr;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\SortableField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */