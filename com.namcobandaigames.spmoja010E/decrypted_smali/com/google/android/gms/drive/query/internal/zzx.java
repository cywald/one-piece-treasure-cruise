package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="OperatorCreator")
@SafeParcelable.Reserved({1000})
public final class zzx
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzx> CREATOR = new zzy();
  public static final zzx zzma = new zzx("=");
  public static final zzx zzmb = new zzx("<");
  public static final zzx zzmc = new zzx("<=");
  public static final zzx zzmd = new zzx(">");
  public static final zzx zzme = new zzx(">=");
  public static final zzx zzmf = new zzx("and");
  public static final zzx zzmg = new zzx("or");
  private static final zzx zzmh = new zzx("not");
  public static final zzx zzmi = new zzx("contains");
  @SafeParcelable.Field(id=1)
  private final String tag;
  
  @SafeParcelable.Constructor
  zzx(@SafeParcelable.Param(id=1) String paramString)
  {
    this.tag = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      do
      {
        return true;
        if ((paramObject == null) || (getClass() != paramObject.getClass())) {
          return false;
        }
        paramObject = (zzx)paramObject;
        if (this.tag != null) {
          break;
        }
      } while (((zzx)paramObject).tag == null);
      return false;
    } while (this.tag.equals(((zzx)paramObject).tag));
    return false;
  }
  
  public final String getTag()
  {
    return this.tag;
  }
  
  public final int hashCode()
  {
    if (this.tag == null) {}
    for (int i = 0;; i = this.tag.hashCode()) {
      return i + 31;
    }
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 1, this.tag, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */