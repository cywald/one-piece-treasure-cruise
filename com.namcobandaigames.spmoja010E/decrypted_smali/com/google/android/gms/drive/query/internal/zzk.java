package com.google.android.gms.drive.query.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.drive.query.Filter;

public final class zzk
  implements zzj<Boolean>
{
  private Boolean zzlw = Boolean.valueOf(false);
  
  public static boolean zza(@Nullable Filter paramFilter)
  {
    if (paramFilter == null) {
      return false;
    }
    return ((Boolean)paramFilter.zza(new zzk())).booleanValue();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */