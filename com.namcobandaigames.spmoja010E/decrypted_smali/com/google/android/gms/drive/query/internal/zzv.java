package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.query.Filter;

@SafeParcelable.Class(creator="NotFilterCreator")
@SafeParcelable.Reserved({1000})
public final class zzv
  extends zza
{
  public static final Parcelable.Creator<zzv> CREATOR = new zzw();
  @SafeParcelable.Field(id=1)
  private final FilterHolder zzlz;
  
  public zzv(Filter paramFilter)
  {
    this(new FilterHolder(paramFilter));
  }
  
  @SafeParcelable.Constructor
  zzv(@SafeParcelable.Param(id=1) FilterHolder paramFilterHolder)
  {
    this.zzlz = paramFilterHolder;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, this.zzlz, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final <T> T zza(zzj<T> paramzzj)
  {
    return (T)paramzzj.zza(this.zzlz.getFilter().zza(paramzzj));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */