package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.query.Filter;

@SafeParcelable.Class(creator="FilterHolderCreator")
@SafeParcelable.Reserved({1000})
public class FilterHolder
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<FilterHolder> CREATOR = new zzh();
  private final Filter zzba;
  @SafeParcelable.Field(id=1)
  private final zzb<?> zzln;
  @SafeParcelable.Field(id=2)
  private final zzd zzlo;
  @SafeParcelable.Field(id=3)
  private final zzr zzlp;
  @SafeParcelable.Field(id=4)
  private final zzv zzlq;
  @SafeParcelable.Field(id=5)
  private final zzp<?> zzlr;
  @SafeParcelable.Field(id=6)
  private final zzt zzls;
  @SafeParcelable.Field(id=7)
  private final zzn zzlt;
  @SafeParcelable.Field(id=8)
  private final zzl zzlu;
  @SafeParcelable.Field(id=9)
  private final zzz zzlv;
  
  public FilterHolder(Filter paramFilter)
  {
    Preconditions.checkNotNull(paramFilter, "Null filter.");
    if ((paramFilter instanceof zzb))
    {
      localObject = (zzb)paramFilter;
      this.zzln = ((zzb)localObject);
      if (!(paramFilter instanceof zzd)) {
        break label242;
      }
      localObject = (zzd)paramFilter;
      label40:
      this.zzlo = ((zzd)localObject);
      if (!(paramFilter instanceof zzr)) {
        break label247;
      }
      localObject = (zzr)paramFilter;
      label57:
      this.zzlp = ((zzr)localObject);
      if (!(paramFilter instanceof zzv)) {
        break label252;
      }
      localObject = (zzv)paramFilter;
      label74:
      this.zzlq = ((zzv)localObject);
      if (!(paramFilter instanceof zzp)) {
        break label257;
      }
      localObject = (zzp)paramFilter;
      label91:
      this.zzlr = ((zzp)localObject);
      if (!(paramFilter instanceof zzt)) {
        break label262;
      }
      localObject = (zzt)paramFilter;
      label108:
      this.zzls = ((zzt)localObject);
      if (!(paramFilter instanceof zzn)) {
        break label267;
      }
      localObject = (zzn)paramFilter;
      label125:
      this.zzlt = ((zzn)localObject);
      if (!(paramFilter instanceof zzl)) {
        break label272;
      }
      localObject = (zzl)paramFilter;
      label142:
      this.zzlu = ((zzl)localObject);
      if (!(paramFilter instanceof zzz)) {
        break label277;
      }
    }
    label242:
    label247:
    label252:
    label257:
    label262:
    label267:
    label272:
    label277:
    for (Object localObject = (zzz)paramFilter;; localObject = null)
    {
      this.zzlv = ((zzz)localObject);
      if ((this.zzln != null) || (this.zzlo != null) || (this.zzlp != null) || (this.zzlq != null) || (this.zzlr != null) || (this.zzls != null) || (this.zzlt != null) || (this.zzlu != null) || (this.zzlv != null)) {
        break label282;
      }
      throw new IllegalArgumentException("Invalid filter type.");
      localObject = null;
      break;
      localObject = null;
      break label40;
      localObject = null;
      break label57;
      localObject = null;
      break label74;
      localObject = null;
      break label91;
      localObject = null;
      break label108;
      localObject = null;
      break label125;
      localObject = null;
      break label142;
    }
    label282:
    this.zzba = paramFilter;
  }
  
  @SafeParcelable.Constructor
  FilterHolder(@SafeParcelable.Param(id=1) zzb<?> paramzzb, @SafeParcelable.Param(id=2) zzd paramzzd, @SafeParcelable.Param(id=3) zzr paramzzr, @SafeParcelable.Param(id=4) zzv paramzzv, @SafeParcelable.Param(id=5) zzp<?> paramzzp, @SafeParcelable.Param(id=6) zzt paramzzt, @SafeParcelable.Param(id=7) zzn<?> paramzzn, @SafeParcelable.Param(id=8) zzl paramzzl, @SafeParcelable.Param(id=9) zzz paramzzz)
  {
    this.zzln = paramzzb;
    this.zzlo = paramzzd;
    this.zzlp = paramzzr;
    this.zzlq = paramzzv;
    this.zzlr = paramzzp;
    this.zzls = paramzzt;
    this.zzlt = paramzzn;
    this.zzlu = paramzzl;
    this.zzlv = paramzzz;
    if (this.zzln != null)
    {
      this.zzba = this.zzln;
      return;
    }
    if (this.zzlo != null)
    {
      this.zzba = this.zzlo;
      return;
    }
    if (this.zzlp != null)
    {
      this.zzba = this.zzlp;
      return;
    }
    if (this.zzlq != null)
    {
      this.zzba = this.zzlq;
      return;
    }
    if (this.zzlr != null)
    {
      this.zzba = this.zzlr;
      return;
    }
    if (this.zzls != null)
    {
      this.zzba = this.zzls;
      return;
    }
    if (this.zzlt != null)
    {
      this.zzba = this.zzlt;
      return;
    }
    if (this.zzlu != null)
    {
      this.zzba = this.zzlu;
      return;
    }
    if (this.zzlv != null)
    {
      this.zzba = this.zzlv;
      return;
    }
    throw new IllegalArgumentException("At least one filter must be set.");
  }
  
  public final Filter getFilter()
  {
    return this.zzba;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 1, this.zzln, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzlo, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 3, this.zzlp, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzlq, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzlr, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 6, this.zzls, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 7, this.zzlt, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 8, this.zzlu, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 9, this.zzlv, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\drive\query\internal\FilterHolder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */