package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.zzt;

public final class zzhy
  extends zzt
  implements SearchableMetadataField<String>, SortableMetadataField<String>
{
  public zzhy(String paramString, int paramInt)
  {
    super(paramString, 4100000);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */