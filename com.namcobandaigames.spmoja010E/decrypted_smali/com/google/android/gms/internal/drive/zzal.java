package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveContents;

final class zzal
  implements Releasable, DriveApi.DriveContentsResult
{
  private final Status zzdw;
  private final DriveContents zzo;
  
  public zzal(Status paramStatus, DriveContents paramDriveContents)
  {
    this.zzdw = paramStatus;
    this.zzo = paramDriveContents;
  }
  
  public final DriveContents getDriveContents()
  {
    return this.zzo;
  }
  
  public final Status getStatus()
  {
    return this.zzdw;
  }
  
  public final void release()
  {
    if (this.zzo != null) {
      this.zzo.zzi();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */