package com.google.android.gms.internal.drive;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Pair;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.zzi;
import java.util.ArrayList;
import java.util.List;

public final class zzee
  extends zzet
{
  private static final GmsLogger zzbx = new GmsLogger("EventCallback", "");
  private final int zzcy = 1;
  private final zzi zzgr;
  private final zzeg zzgs;
  private final List<Integer> zzgt = new ArrayList();
  
  public zzee(Looper paramLooper, Context paramContext, int paramInt, zzi paramzzi)
  {
    this.zzgr = paramzzi;
    this.zzgs = new zzeg(paramLooper, paramContext, null);
  }
  
  public final void zzc(zzfj paramzzfj)
    throws RemoteException
  {
    paramzzfj = paramzzfj.zzak();
    if (this.zzcy == paramzzfj.getType()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool);
      Preconditions.checkState(this.zzgt.contains(Integer.valueOf(paramzzfj.getType())));
      zzeg localzzeg = this.zzgs;
      localzzeg.sendMessage(localzzeg.obtainMessage(1, new Pair(this.zzgr, paramzzfj)));
      return;
    }
  }
  
  public final void zzf(int paramInt)
  {
    this.zzgt.add(Integer.valueOf(1));
  }
  
  public final boolean zzg(int paramInt)
  {
    return this.zzgt.contains(Integer.valueOf(1));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */