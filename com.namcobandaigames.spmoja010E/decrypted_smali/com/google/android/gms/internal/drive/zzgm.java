package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.zzt;

@SafeParcelable.Class(creator="RemoveEventListenerRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzgm
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzgm> CREATOR = new zzgn();
  @SafeParcelable.Field(id=3)
  private final int zzcy;
  @Nullable
  @SafeParcelable.Field(id=4)
  private final zzt zzda;
  @SafeParcelable.Field(id=2)
  private final DriveId zzk;
  
  @VisibleForTesting
  public zzgm(@Nullable DriveId paramDriveId, int paramInt)
  {
    this(paramDriveId, paramInt, null);
  }
  
  @SafeParcelable.Constructor
  zzgm(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) int paramInt, @SafeParcelable.Param(id=4) zzt paramzzt)
  {
    this.zzk = paramDriveId;
    this.zzcy = paramInt;
    this.zzda = paramzzt;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzk, paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 3, this.zzcy);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzda, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzgm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */