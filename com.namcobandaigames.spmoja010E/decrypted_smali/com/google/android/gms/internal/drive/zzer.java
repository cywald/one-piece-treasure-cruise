package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.zza;

public abstract class zzer
  extends zzb
  implements zzeq
{
  public zzer()
  {
    super("com.google.android.gms.drive.internal.IDriveServiceCallbacks");
  }
  
  protected final boolean dispatchTransaction(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
    throws RemoteException
  {
    switch (paramInt1)
    {
    case 10: 
    case 19: 
    default: 
      return false;
    case 1: 
      zza((zzff)zzc.zza(paramParcel1, zzff.CREATOR));
    }
    for (;;)
    {
      paramParcel2.writeNoException();
      return true;
      zza((zzfn)zzc.zza(paramParcel1, zzfn.CREATOR));
      continue;
      zza((zzfh)zzc.zza(paramParcel1, zzfh.CREATOR));
      continue;
      zza((zzfs)zzc.zza(paramParcel1, zzfs.CREATOR));
      continue;
      zza((zzfb)zzc.zza(paramParcel1, zzfb.CREATOR));
      continue;
      zza((Status)zzc.zza(paramParcel1, Status.CREATOR));
      continue;
      onSuccess();
      continue;
      zza((zzfp)zzc.zza(paramParcel1, zzfp.CREATOR));
      continue;
      zza((zzgb)zzc.zza(paramParcel1, zzgb.CREATOR));
      continue;
      zza((zzfr)zzc.zza(paramParcel1, zzfr.CREATOR), zzim.zzb(paramParcel1.readStrongBinder()));
      continue;
      zza((zzfx)zzc.zza(paramParcel1, zzfx.CREATOR));
      continue;
      zza((zzfu)zzc.zza(paramParcel1, zzfu.CREATOR));
      continue;
      zza((zzfd)zzc.zza(paramParcel1, zzfd.CREATOR));
      continue;
      zzb(zzc.zza(paramParcel1));
      continue;
      zza((zzfl)zzc.zza(paramParcel1, zzfl.CREATOR));
      continue;
      zza((zza)zzc.zza(paramParcel1, zza.CREATOR));
      continue;
      zza((zzez)zzc.zza(paramParcel1, zzez.CREATOR));
      continue;
      zza((zzem)zzc.zza(paramParcel1, zzem.CREATOR));
      continue;
      zza((zzgt)zzc.zza(paramParcel1, zzgt.CREATOR));
      continue;
      zza((zzfz)zzc.zza(paramParcel1, zzfz.CREATOR));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */