package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DriveResource.MetadataResult;

final class zzdy
  extends zzl
{
  private final BaseImplementation.ResultHolder<DriveResource.MetadataResult> zzdv;
  
  public zzdy(BaseImplementation.ResultHolder<DriveResource.MetadataResult> paramResultHolder)
  {
    this.zzdv = paramResultHolder;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzdz(paramStatus, null));
  }
  
  public final void zza(zzfs paramzzfs)
    throws RemoteException
  {
    this.zzdv.setResult(new zzdz(Status.RESULT_SUCCESS, new zzaa(paramzzfs.zzdl)));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzdy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */