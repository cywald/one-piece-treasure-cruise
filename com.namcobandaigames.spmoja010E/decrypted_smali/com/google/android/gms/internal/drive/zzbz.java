package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveFolder.DriveFolderResult;

final class zzbz
  implements DriveFolder.DriveFolderResult
{
  private final Status zzdw;
  private final DriveFolder zzfh;
  
  public zzbz(Status paramStatus, DriveFolder paramDriveFolder)
  {
    this.zzdw = paramStatus;
    this.zzfh = paramDriveFolder;
  }
  
  public final DriveFolder getDriveFolder()
  {
    return this.zzfh;
  }
  
  public final Status getStatus()
  {
    return this.zzdw;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzbz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */