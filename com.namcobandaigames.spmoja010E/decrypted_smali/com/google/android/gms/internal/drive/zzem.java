package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.zzr;
import java.util.List;

@SafeParcelable.Class(creator="GetPermissionsResponseCreator")
@SafeParcelable.Reserved({1})
public final class zzem
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzem> CREATOR = new zzen();
  @SafeParcelable.Field(id=3)
  private final int responseCode;
  @SafeParcelable.Field(id=2)
  private final List<zzr> zzgz;
  
  @SafeParcelable.Constructor
  public zzem(@SafeParcelable.Param(id=2) List<zzr> paramList, @SafeParcelable.Param(id=3) int paramInt)
  {
    this.zzgz = paramList;
    this.responseCode = paramInt;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeTypedList(paramParcel, 2, this.zzgz, false);
    SafeParcelWriter.writeInt(paramParcel, 3, this.responseCode);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */