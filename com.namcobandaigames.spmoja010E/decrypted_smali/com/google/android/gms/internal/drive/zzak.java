package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;

final class zzak
  extends zzl
{
  private final BaseImplementation.ResultHolder<DriveApi.DriveContentsResult> zzdv;
  
  zzak(BaseImplementation.ResultHolder<DriveApi.DriveContentsResult> paramResultHolder)
  {
    this.zzdv = paramResultHolder;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzal(paramStatus, null));
  }
  
  public final void zza(zzfb paramzzfb)
    throws RemoteException
  {
    this.zzdv.setResult(new zzal(Status.RESULT_SUCCESS, new zzbi(paramzzfb.zzeq)));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */