package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.zzu;

@SafeParcelable.Class(creator="OnListParentsResponseCreator")
@SafeParcelable.Reserved({1})
public final class zzfp
  extends zzu
{
  public static final Parcelable.Creator<zzfp> CREATOR = new zzfq();
  @SafeParcelable.Field(id=2)
  final DataHolder zzht;
  
  @SafeParcelable.Constructor
  public zzfp(@SafeParcelable.Param(id=2) DataHolder paramDataHolder)
  {
    this.zzht = paramDataHolder;
  }
  
  protected final void zza(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzht, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final DataHolder zzam()
  {
    return this.zzht;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzfp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */