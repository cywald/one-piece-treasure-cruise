package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.MetadataBuffer;

final class zzas
  extends zzl
{
  private final BaseImplementation.ResultHolder<DriveApi.MetadataBufferResult> zzdv;
  
  zzas(BaseImplementation.ResultHolder<DriveApi.MetadataBufferResult> paramResultHolder)
  {
    this.zzdv = paramResultHolder;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzaq(paramStatus, null, false));
  }
  
  public final void zza(zzfn paramzzfn)
    throws RemoteException
  {
    MetadataBuffer localMetadataBuffer = new MetadataBuffer(paramzzfn.zzhs);
    this.zzdv.setResult(new zzaq(Status.RESULT_SUCCESS, localMetadataBuffer, paramzzfn.zzdy));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */