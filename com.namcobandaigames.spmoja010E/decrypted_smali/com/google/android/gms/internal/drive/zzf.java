package com.google.android.gms.internal.drive;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.drive.DriveId;

public final class zzf
{
  private final int status;
  private final int zzcr;
  private final DriveId zzk;
  
  public zzf(zzh paramzzh)
  {
    this.zzk = paramzzh.zzk;
    this.zzcr = paramzzh.zzcr;
    this.status = paramzzh.status;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zzf)paramObject;
      if ((!Objects.equal(this.zzk, ((zzf)paramObject).zzk)) || (this.zzcr != ((zzf)paramObject).zzcr)) {
        break;
      }
      bool1 = bool2;
    } while (this.status == ((zzf)paramObject).status);
    return false;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzk, Integer.valueOf(this.zzcr), Integer.valueOf(this.status) });
  }
  
  public final String toString()
  {
    return String.format("FileTransferState[TransferType: %d, DriveId: %s, status: %d]", new Object[] { Integer.valueOf(this.zzcr), this.zzk, Integer.valueOf(this.status) });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */