package com.google.android.gms.internal.drive;

import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zzes
  extends IInterface
{
  public abstract void zzc(zzfj paramzzfj)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */