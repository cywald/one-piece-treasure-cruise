package com.google.android.gms.internal.drive;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;

@SafeParcelable.Class(creator="OnStartStreamSessionCreator")
@SafeParcelable.Reserved({1})
public final class zzfz
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzfz> CREATOR = new zzga();
  @SafeParcelable.Field(id=2)
  private final ParcelFileDescriptor zzhx;
  @SafeParcelable.Field(id=3)
  private final IBinder zzhy;
  @SafeParcelable.Field(id=4)
  private final String zzm;
  
  @SafeParcelable.Constructor
  zzfz(@SafeParcelable.Param(id=2) ParcelFileDescriptor paramParcelFileDescriptor, @SafeParcelable.Param(id=3) IBinder paramIBinder, @SafeParcelable.Param(id=4) String paramString)
  {
    this.zzhx = paramParcelFileDescriptor;
    this.zzhy = paramIBinder;
    this.zzm = paramString;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzhx, paramInt | 0x1, false);
    SafeParcelWriter.writeIBinder(paramParcel, 3, this.zzhy, false);
    SafeParcelWriter.writeString(paramParcel, 4, this.zzm, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzfz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */