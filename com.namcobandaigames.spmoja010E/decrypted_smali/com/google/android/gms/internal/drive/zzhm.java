package com.google.android.gms.internal.drive;

import java.io.IOException;

public final class zzhm
  extends zzir<zzhm>
{
  public int versionCode = 1;
  public long zze = -1L;
  public long zzf = -1L;
  public long zzg = -1L;
  
  public zzhm()
  {
    this.zzmw = null;
    this.zznf = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzhm)) {
        return false;
      }
      paramObject = (zzhm)paramObject;
      if (this.versionCode != ((zzhm)paramObject).versionCode) {
        return false;
      }
      if (this.zze != ((zzhm)paramObject).zze) {
        return false;
      }
      if (this.zzf != ((zzhm)paramObject).zzf) {
        return false;
      }
      if (this.zzg != ((zzhm)paramObject).zzg) {
        return false;
      }
      if ((this.zzmw != null) && (!this.zzmw.isEmpty())) {
        break;
      }
    } while ((((zzhm)paramObject).zzmw == null) || (((zzhm)paramObject).zzmw.isEmpty()));
    return false;
    return this.zzmw.equals(((zzhm)paramObject).zzmw);
  }
  
  public final int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = this.versionCode;
    int m = (int)(this.zze ^ this.zze >>> 32);
    int n = (int)(this.zzf ^ this.zzf >>> 32);
    int i1 = (int)(this.zzg ^ this.zzg >>> 32);
    if ((this.zzmw == null) || (this.zzmw.isEmpty())) {}
    for (int i = 0;; i = this.zzmw.hashCode()) {
      return i + (((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31;
    }
  }
  
  public final void zza(zzip paramzzip)
    throws IOException
  {
    paramzzip.zzb(1, this.versionCode);
    paramzzip.zza(2, this.zze);
    paramzzip.zza(3, this.zzf);
    paramzzip.zza(4, this.zzg);
    super.zza(paramzzip);
  }
  
  protected final int zzaq()
  {
    return super.zzaq() + zzip.zzc(1, this.versionCode) + zzip.zzb(2, this.zze) + zzip.zzb(3, this.zzf) + zzip.zzb(4, this.zzg);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */