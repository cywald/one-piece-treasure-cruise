package com.google.android.gms.internal.drive;

import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.util.SparseArray;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties.zza;
import com.google.android.gms.drive.metadata.internal.zzc;
import com.google.android.gms.drive.metadata.internal.zzg;
import com.google.android.gms.drive.metadata.internal.zzm;
import java.util.Arrays;

public class zzia
  extends zzm<AppVisibleCustomProperties>
{
  public static final zzg zzkm = new zzib();
  
  public zzia(int paramInt)
  {
    super("customProperties", Arrays.asList(new String[] { "hasCustomProperties", "sqlId" }), Arrays.asList(new String[] { "customPropertiesExtra", "customPropertiesExtraHolder" }), 5000000);
  }
  
  private static void zzc(DataHolder paramDataHolder)
  {
    Bundle localBundle = paramDataHolder.getMetadata();
    if (localBundle == null) {
      return;
    }
    try
    {
      DataHolder localDataHolder = (DataHolder)localBundle.getParcelable("customPropertiesExtraHolder");
      if (localDataHolder != null)
      {
        localDataHolder.close();
        localBundle.remove("customPropertiesExtraHolder");
      }
      return;
    }
    finally {}
  }
  
  private static AppVisibleCustomProperties zzf(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    Bundle localBundle = paramDataHolder.getMetadata();
    Object localObject1 = localBundle.getSparseParcelableArray("customPropertiesExtra");
    Object localObject4 = localObject1;
    if (localObject1 == null)
    {
      if (localBundle.getParcelable("customPropertiesExtraHolder") != null) {}
      try
      {
        localDataHolder = (DataHolder)paramDataHolder.getMetadata().getParcelable("customPropertiesExtraHolder");
        if (localDataHolder != null) {
          break label79;
        }
      }
      finally
      {
        try
        {
          for (;;)
          {
            localObject1 = localDataHolder.getMetadata();
            str1 = ((Bundle)localObject1).getString("entryIdColumn");
            str2 = ((Bundle)localObject1).getString("keyColumn");
            str3 = ((Bundle)localObject1).getString("visibilityColumn");
            str4 = ((Bundle)localObject1).getString("valueColumn");
            localLongSparseArray = new LongSparseArray();
            paramInt2 = 0;
            while (paramInt2 < localDataHolder.getCount())
            {
              i = localDataHolder.getWindowIndex(paramInt2);
              l = localDataHolder.getLong(str1, paramInt2, i);
              localObject1 = localDataHolder.getString(str2, paramInt2, i);
              j = localDataHolder.getInteger(str3, paramInt2, i);
              localObject4 = localDataHolder.getString(str4, paramInt2, i);
              localzzc = new zzc(new CustomPropertyKey((String)localObject1, j), (String)localObject4);
              localObject4 = (AppVisibleCustomProperties.zza)localLongSparseArray.get(l);
              localObject1 = localObject4;
              if (localObject4 == null)
              {
                localObject1 = new AppVisibleCustomProperties.zza();
                localLongSparseArray.put(l, localObject1);
              }
              ((AppVisibleCustomProperties.zza)localObject1).zza(localzzc);
              paramInt2 += 1;
            }
            localObject1 = new SparseArray();
            paramInt2 = 0;
            if (paramInt2 < paramDataHolder.getCount())
            {
              localObject4 = (AppVisibleCustomProperties.zza)localLongSparseArray.get(paramDataHolder.getLong("sqlId", paramInt2, paramDataHolder.getWindowIndex(paramInt2)));
              if (localObject4 == null) {
                break;
              }
              ((SparseArray)localObject1).append(paramInt2, ((AppVisibleCustomProperties.zza)localObject4).zzat());
              break;
            }
            paramDataHolder.getMetadata().putSparseParcelableArray("customPropertiesExtra", (SparseArray)localObject1);
            localDataHolder.close();
            paramDataHolder.getMetadata().remove("customPropertiesExtraHolder");
          }
        }
        finally
        {
          DataHolder localDataHolder;
          localDataHolder.close();
          paramDataHolder.getMetadata().remove("customPropertiesExtraHolder");
        }
        localObject2 = finally;
      }
      localObject1 = localBundle.getSparseParcelableArray("customPropertiesExtra");
      localObject4 = localObject1;
      if (localObject1 == null) {
        return AppVisibleCustomProperties.zzil;
      }
    }
    for (;;)
    {
      label79:
      String str1;
      String str2;
      String str3;
      String str4;
      LongSparseArray localLongSparseArray;
      int i;
      long l;
      int j;
      zzc localzzc;
      return (AppVisibleCustomProperties)((SparseArray)localObject4).get(paramInt1, AppVisibleCustomProperties.zzil);
      paramInt2 += 1;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzia.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */