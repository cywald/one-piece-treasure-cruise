package com.google.android.gms.internal.drive;

import java.util.Arrays;

final class zziz
{
  final int tag;
  final byte[] zzng;
  
  zziz(int paramInt, byte[] paramArrayOfByte)
  {
    this.tag = paramInt;
    this.zzng = paramArrayOfByte;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zziz)) {
        return false;
      }
      paramObject = (zziz)paramObject;
    } while ((this.tag == ((zziz)paramObject).tag) && (Arrays.equals(this.zzng, ((zziz)paramObject).zzng)));
    return false;
  }
  
  public final int hashCode()
  {
    return (this.tag + 527) * 31 + Arrays.hashCode(this.zzng);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zziz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */