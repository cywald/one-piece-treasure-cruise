package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DrivePreferencesApi.FileUploadPreferencesResult;
import com.google.android.gms.drive.FileUploadPreferences;

final class zzcf
  implements DrivePreferencesApi.FileUploadPreferencesResult
{
  private final Status zzdw;
  private final FileUploadPreferences zzfk;
  
  private zzcf(zzcb paramzzcb, Status paramStatus, FileUploadPreferences paramFileUploadPreferences)
  {
    this.zzdw = paramStatus;
    this.zzfk = paramFileUploadPreferences;
  }
  
  public final FileUploadPreferences getFileUploadPreferences()
  {
    return this.zzfk;
  }
  
  public final Status getStatus()
  {
    return this.zzdw;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzcf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */