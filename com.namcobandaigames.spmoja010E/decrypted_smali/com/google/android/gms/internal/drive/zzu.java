package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

@SafeParcelable.Class(creator="CreateFileIntentSenderRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzu
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzu> CREATOR = new zzv();
  @SafeParcelable.Field(id=4)
  private final String zzay;
  @SafeParcelable.Field(id=5)
  private final DriveId zzbb;
  @SafeParcelable.Field(id=2)
  private final MetadataBundle zzdl;
  @SafeParcelable.Field(id=6)
  private final Integer zzdm;
  @SafeParcelable.Field(id=3)
  private final int zzj;
  
  @SafeParcelable.Constructor
  @VisibleForTesting
  public zzu(@SafeParcelable.Param(id=2) MetadataBundle paramMetadataBundle, @SafeParcelable.Param(id=3) int paramInt, @SafeParcelable.Param(id=4) String paramString, @SafeParcelable.Param(id=5) DriveId paramDriveId, @SafeParcelable.Param(id=6) Integer paramInteger)
  {
    this.zzdl = paramMetadataBundle;
    this.zzj = paramInt;
    this.zzay = paramString;
    this.zzbb = paramDriveId;
    this.zzdm = paramInteger;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzdl, paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 3, this.zzj);
    SafeParcelWriter.writeString(paramParcel, 4, this.zzay, false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzbb, paramInt, false);
    SafeParcelWriter.writeIntegerObject(paramParcel, 6, this.zzdm, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */