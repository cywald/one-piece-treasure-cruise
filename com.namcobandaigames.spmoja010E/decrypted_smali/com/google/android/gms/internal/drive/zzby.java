package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;

abstract class zzby
  extends zzau<DriveFolder.DriveFileResult>
{
  zzby(GoogleApiClient paramGoogleApiClient)
  {
    super(paramGoogleApiClient);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzby.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */