package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.zzi;

public final class zzhv
  extends zzi
  implements SortableMetadataField<Long>
{
  public zzhv(String paramString, int paramInt)
  {
    super(paramString, 4300000);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */