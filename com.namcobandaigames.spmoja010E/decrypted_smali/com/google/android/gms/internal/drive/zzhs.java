package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;

public final class zzhs
  extends zzia
  implements SearchableMetadataField<AppVisibleCustomProperties>
{
  public zzhs(int paramInt)
  {
    super(5000000);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */