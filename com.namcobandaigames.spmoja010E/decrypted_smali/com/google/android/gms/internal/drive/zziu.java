package com.google.android.gms.internal.drive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class zziu
  implements Cloneable
{
  private Object value;
  private zzis<?, ?> zznc;
  private List<zziz> zznd = new ArrayList();
  
  private final byte[] toByteArray()
    throws IOException
  {
    byte[] arrayOfByte = new byte[zzaq()];
    zza(zzip.zzb(arrayOfByte));
    return arrayOfByte;
  }
  
  private final zziu zzbj()
  {
    zziu localzziu = new zziu();
    try
    {
      localzziu.zznc = this.zznc;
      if (this.zznd == null) {
        localzziu.zznd = null;
      }
      for (;;)
      {
        if (this.value == null) {
          return localzziu;
        }
        if (!(this.value instanceof zzix)) {
          break;
        }
        localzziu.value = ((zzix)((zzix)this.value).clone());
        return localzziu;
        localzziu.zznd.addAll(this.zznd);
      }
      if (!(this.value instanceof byte[])) {
        break label117;
      }
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new AssertionError(localCloneNotSupportedException);
    }
    localCloneNotSupportedException.value = ((byte[])this.value).clone();
    return localCloneNotSupportedException;
    label117:
    Object localObject1;
    Object localObject2;
    int i;
    if ((this.value instanceof byte[][]))
    {
      localObject1 = (byte[][])this.value;
      localObject2 = new byte[localObject1.length][];
      localCloneNotSupportedException.value = localObject2;
      i = 0;
      while (i < localObject1.length)
      {
        localObject2[i] = ((byte[])localObject1[i].clone());
        i += 1;
      }
    }
    if ((this.value instanceof boolean[]))
    {
      localCloneNotSupportedException.value = ((boolean[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof int[]))
    {
      localCloneNotSupportedException.value = ((int[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof long[]))
    {
      localCloneNotSupportedException.value = ((long[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof float[]))
    {
      localCloneNotSupportedException.value = ((float[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof double[]))
    {
      localCloneNotSupportedException.value = ((double[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof zzix[]))
    {
      localObject1 = (zzix[])this.value;
      localObject2 = new zzix[localObject1.length];
      localCloneNotSupportedException.value = localObject2;
      i = 0;
      while (i < localObject1.length)
      {
        localObject2[i] = ((zzix)localObject1[i].clone());
        i += 1;
      }
    }
    return localCloneNotSupportedException;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (!(paramObject instanceof zziu));
      paramObject = (zziu)paramObject;
      if ((this.value == null) || (((zziu)paramObject).value == null)) {
        break;
      }
      bool1 = bool2;
    } while (this.zznc != ((zziu)paramObject).zznc);
    if (!this.zznc.zzmx.isArray()) {
      return this.value.equals(((zziu)paramObject).value);
    }
    if ((this.value instanceof byte[])) {
      return Arrays.equals((byte[])this.value, (byte[])((zziu)paramObject).value);
    }
    if ((this.value instanceof int[])) {
      return Arrays.equals((int[])this.value, (int[])((zziu)paramObject).value);
    }
    if ((this.value instanceof long[])) {
      return Arrays.equals((long[])this.value, (long[])((zziu)paramObject).value);
    }
    if ((this.value instanceof float[])) {
      return Arrays.equals((float[])this.value, (float[])((zziu)paramObject).value);
    }
    if ((this.value instanceof double[])) {
      return Arrays.equals((double[])this.value, (double[])((zziu)paramObject).value);
    }
    if ((this.value instanceof boolean[])) {
      return Arrays.equals((boolean[])this.value, (boolean[])((zziu)paramObject).value);
    }
    return Arrays.deepEquals((Object[])this.value, (Object[])((zziu)paramObject).value);
    if ((this.zznd != null) && (((zziu)paramObject).zznd != null)) {
      return this.zznd.equals(((zziu)paramObject).zznd);
    }
    try
    {
      bool1 = Arrays.equals(toByteArray(), ((zziu)paramObject).toByteArray());
      return bool1;
    }
    catch (IOException paramObject)
    {
      throw new IllegalStateException((Throwable)paramObject);
    }
  }
  
  public final int hashCode()
  {
    try
    {
      int i = Arrays.hashCode(toByteArray());
      return i + 527;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException(localIOException);
    }
  }
  
  final void zza(zzip paramzzip)
    throws IOException
  {
    if (this.value != null) {
      throw new NoSuchMethodError();
    }
    Iterator localIterator = this.zznd.iterator();
    while (localIterator.hasNext())
    {
      zziz localzziz = (zziz)localIterator.next();
      paramzzip.zzp(localzziz.tag);
      paramzzip.zzc(localzziz.zzng);
    }
  }
  
  final void zza(zziz paramzziz)
    throws IOException
  {
    if (this.zznd != null)
    {
      this.zznd.add(paramzziz);
      return;
    }
    if ((this.value instanceof zzix))
    {
      paramzziz = paramzziz.zzng;
      zzio localzzio = zzio.zza(paramzziz, 0, paramzziz.length);
      int i = localzzio.zzbe();
      if (i != paramzziz.length - zzip.zzm(i)) {
        throw zziw.zzbk();
      }
      paramzziz = ((zzix)this.value).zza(localzzio);
      this.zznc = this.zznc;
      this.value = paramzziz;
      this.zznd = null;
      return;
    }
    if ((this.value instanceof zzix[]))
    {
      Collections.singletonList(paramzziz);
      throw new NoSuchMethodError();
    }
    Collections.singletonList(paramzziz);
    throw new NoSuchMethodError();
  }
  
  final int zzaq()
  {
    if (this.value != null) {
      throw new NoSuchMethodError();
    }
    Iterator localIterator = this.zznd.iterator();
    zziz localzziz;
    int j;
    for (int i = 0; localIterator.hasNext(); i = localzziz.zzng.length + (j + 0) + i)
    {
      localzziz = (zziz)localIterator.next();
      j = zzip.zzq(localzziz.tag);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zziu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */