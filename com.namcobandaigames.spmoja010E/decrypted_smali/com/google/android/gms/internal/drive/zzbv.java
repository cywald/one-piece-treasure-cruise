package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;

final class zzbv
  extends zzl
{
  private final BaseImplementation.ResultHolder<DriveFolder.DriveFileResult> zzdv;
  
  public zzbv(BaseImplementation.ResultHolder<DriveFolder.DriveFileResult> paramResultHolder)
  {
    this.zzdv = paramResultHolder;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzbx(paramStatus, null));
  }
  
  public final void zza(zzfh paramzzfh)
    throws RemoteException
  {
    this.zzdv.setResult(new zzbx(Status.RESULT_SUCCESS, new zzbn(paramzzfh.zzdb)));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzbv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */