package com.google.android.gms.internal.drive;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.zza;

public abstract interface zzeq
  extends IInterface
{
  public abstract void onSuccess()
    throws RemoteException;
  
  public abstract void zza(Status paramStatus)
    throws RemoteException;
  
  public abstract void zza(zza paramzza)
    throws RemoteException;
  
  public abstract void zza(zzem paramzzem)
    throws RemoteException;
  
  public abstract void zza(zzez paramzzez)
    throws RemoteException;
  
  public abstract void zza(zzfb paramzzfb)
    throws RemoteException;
  
  public abstract void zza(zzfd paramzzfd)
    throws RemoteException;
  
  public abstract void zza(zzff paramzzff)
    throws RemoteException;
  
  public abstract void zza(zzfh paramzzfh)
    throws RemoteException;
  
  public abstract void zza(zzfl paramzzfl)
    throws RemoteException;
  
  public abstract void zza(zzfn paramzzfn)
    throws RemoteException;
  
  public abstract void zza(zzfp paramzzfp)
    throws RemoteException;
  
  public abstract void zza(zzfr paramzzfr, zzil paramzzil)
    throws RemoteException;
  
  public abstract void zza(zzfs paramzzfs)
    throws RemoteException;
  
  public abstract void zza(zzfu paramzzfu)
    throws RemoteException;
  
  public abstract void zza(zzfx paramzzfx)
    throws RemoteException;
  
  public abstract void zza(zzfz paramzzfz)
    throws RemoteException;
  
  public abstract void zza(zzgb paramzzgb)
    throws RemoteException;
  
  public abstract void zza(zzgt paramzzgt)
    throws RemoteException;
  
  public abstract void zzb(boolean paramBoolean)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzeq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */