package com.google.android.gms.internal.drive;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolder.ListenerKey;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.Drive.zza;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.ExecutionOptions.Builder;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.events.ListenerToken;
import com.google.android.gms.drive.events.OnChangeListener;
import com.google.android.gms.drive.events.OpenFileCallback;
import com.google.android.gms.drive.events.zzj;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.zzn;
import com.google.android.gms.drive.zzp;
import com.google.android.gms.tasks.Task;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzch
  extends DriveResourceClient
{
  private static final AtomicInteger zzfl = new AtomicInteger();
  
  public zzch(@NonNull Activity paramActivity, @Nullable Drive.zza paramzza)
  {
    super(paramActivity, paramzza);
  }
  
  public zzch(@NonNull Context paramContext, @Nullable Drive.zza paramzza)
  {
    super(paramContext, paramzza);
  }
  
  private static void zze(int paramInt)
  {
    if ((paramInt != 268435456) && (paramInt != 536870912) && (paramInt != 805306368)) {
      throw new IllegalArgumentException("Invalid openMode provided");
    }
  }
  
  public final Task<ListenerToken> addChangeListener(@NonNull DriveResource paramDriveResource, @NonNull OnChangeListener paramOnChangeListener)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    Preconditions.checkNotNull(paramOnChangeListener, "listener");
    paramOnChangeListener = new zzdi(this, paramOnChangeListener, paramDriveResource.getDriveId());
    int i = zzfl.incrementAndGet();
    ListenerHolder localListenerHolder = registerListener(paramOnChangeListener, 27 + "OnChangeListener" + i);
    return doRegisterEventListener(new zzcp(this, localListenerHolder, paramDriveResource, paramOnChangeListener), new zzcq(this, localListenerHolder.getListenerKey(), paramDriveResource, paramOnChangeListener)).continueWith(new zzci(localListenerHolder));
  }
  
  public final Task<Void> addChangeSubscription(@NonNull DriveResource paramDriveResource)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    Preconditions.checkArgument(zzj.zza(1, paramDriveResource.getDriveId()));
    return doWrite(new zzcr(this, paramDriveResource));
  }
  
  public final Task<Boolean> cancelOpenFileCallback(@NonNull ListenerToken paramListenerToken)
  {
    if (!(paramListenerToken instanceof zzg)) {
      throw new IllegalArgumentException("Unrecognized ListenerToken");
    }
    return doUnregisterEventListener(((zzg)paramListenerToken).zzac());
  }
  
  public final Task<Void> commitContents(@NonNull DriveContents paramDriveContents, @Nullable MetadataChangeSet paramMetadataChangeSet)
  {
    return commitContents(paramDriveContents, paramMetadataChangeSet, (zzn)new zzp().build());
  }
  
  public final Task<Void> commitContents(@NonNull DriveContents paramDriveContents, @Nullable MetadataChangeSet paramMetadataChangeSet, @NonNull ExecutionOptions paramExecutionOptions)
  {
    boolean bool2 = true;
    Preconditions.checkNotNull(paramExecutionOptions, "Execution options cannot be null.");
    if (!paramDriveContents.zzj())
    {
      bool1 = true;
      Preconditions.checkArgument(bool1, "DriveContents is already closed");
      if (paramDriveContents.getMode() == 268435456) {
        break label108;
      }
    }
    zzn localzzn;
    label108:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      Preconditions.checkArgument(bool1, "Cannot commit contents opened in MODE_READ_ONLY.");
      Preconditions.checkNotNull(paramDriveContents.getDriveId(), "Only DriveContents obtained through DriveFile.open can be committed.");
      localzzn = zzn.zza(paramExecutionOptions);
      if ((!ExecutionOptions.zza(localzzn.zzm())) || (paramDriveContents.zzh().zza())) {
        break label114;
      }
      throw new IllegalStateException("DriveContents must be valid for conflict detection.");
      bool1 = false;
      break;
    }
    label114:
    paramExecutionOptions = paramMetadataChangeSet;
    if (paramMetadataChangeSet == null) {
      paramExecutionOptions = MetadataChangeSet.zzav;
    }
    return doWrite(new zzcy(this, localzzn, paramDriveContents, paramExecutionOptions));
  }
  
  public final Task<DriveContents> createContents()
  {
    Preconditions.checkArgument(true, "Contents can only be created in MODE_WRITE_ONLY or MODE_READ_WRITE.");
    return doWrite(new zzcw(this, 536870912));
  }
  
  public final Task<DriveFile> createFile(@NonNull DriveFolder paramDriveFolder, @NonNull MetadataChangeSet paramMetadataChangeSet, @Nullable DriveContents paramDriveContents)
  {
    return createFile(paramDriveFolder, paramMetadataChangeSet, paramDriveContents, new ExecutionOptions.Builder().build());
  }
  
  public final Task<DriveFile> createFile(@NonNull DriveFolder paramDriveFolder, @NonNull MetadataChangeSet paramMetadataChangeSet, @Nullable DriveContents paramDriveContents, @NonNull ExecutionOptions paramExecutionOptions)
  {
    zzbs.zzb(paramMetadataChangeSet);
    return doWrite(new zzdh(paramDriveFolder, paramMetadataChangeSet, paramDriveContents, paramExecutionOptions, null));
  }
  
  public final Task<DriveFolder> createFolder(@NonNull DriveFolder paramDriveFolder, @NonNull MetadataChangeSet paramMetadataChangeSet)
  {
    Preconditions.checkNotNull(paramMetadataChangeSet, "MetadataChangeSet must be provided.");
    if ((paramMetadataChangeSet.getMimeType() != null) && (!paramMetadataChangeSet.getMimeType().equals("application/vnd.google-apps.folder"))) {
      throw new IllegalArgumentException("The mimetype must be of type application/vnd.google-apps.folder");
    }
    return doWrite(new zzdb(this, paramMetadataChangeSet, paramDriveFolder));
  }
  
  public final Task<Void> delete(@NonNull DriveResource paramDriveResource)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    return doWrite(new zzcl(this, paramDriveResource));
  }
  
  public final Task<Void> discardContents(@NonNull DriveContents paramDriveContents)
  {
    if (!paramDriveContents.zzj()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool, "DriveContents is already closed");
      paramDriveContents.zzi();
      return doWrite(new zzda(this, paramDriveContents));
    }
  }
  
  public final Task<DriveFolder> getAppFolder()
  {
    return doRead(new zzco(this));
  }
  
  public final Task<Metadata> getMetadata(@NonNull DriveResource paramDriveResource)
  {
    Preconditions.checkNotNull(paramDriveResource, "DriveResource must not be null");
    Preconditions.checkNotNull(paramDriveResource.getDriveId(), "Resource's DriveId must not be null");
    return doRead(new zzdc(this, paramDriveResource, false));
  }
  
  public final Task<DriveFolder> getRootFolder()
  {
    return doRead(new zzck(this));
  }
  
  public final Task<MetadataBuffer> listChildren(@NonNull DriveFolder paramDriveFolder)
  {
    Preconditions.checkNotNull(paramDriveFolder, "folder cannot be null.");
    return query(zzbs.zza(null, paramDriveFolder.getDriveId()));
  }
  
  public final Task<MetadataBuffer> listParents(@NonNull DriveResource paramDriveResource)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    return doRead(new zzde(this, paramDriveResource));
  }
  
  public final Task<DriveContents> openFile(@NonNull DriveFile paramDriveFile, int paramInt)
  {
    zze(paramInt);
    return doRead(new zzct(this, paramDriveFile, paramInt));
  }
  
  public final Task<ListenerToken> openFile(@NonNull DriveFile paramDriveFile, int paramInt, @NonNull OpenFileCallback paramOpenFileCallback)
  {
    zze(paramInt);
    int i = zzfl.incrementAndGet();
    paramOpenFileCallback = registerListener(paramOpenFileCallback, 27 + "OpenFileCallback" + i);
    ListenerHolder.ListenerKey localListenerKey = paramOpenFileCallback.getListenerKey();
    zzg localzzg = new zzg(localListenerKey);
    return doRegisterEventListener(new zzcu(this, paramOpenFileCallback, paramDriveFile, paramInt, localzzg, paramOpenFileCallback), new zzcv(this, localListenerKey, localzzg)).continueWith(new zzcj(localzzg));
  }
  
  public final Task<MetadataBuffer> query(@NonNull Query paramQuery)
  {
    Preconditions.checkNotNull(paramQuery, "query cannot be null.");
    return doRead(new zzcz(this, paramQuery));
  }
  
  public final Task<MetadataBuffer> queryChildren(@NonNull DriveFolder paramDriveFolder, @NonNull Query paramQuery)
  {
    Preconditions.checkNotNull(paramDriveFolder, "folder cannot be null.");
    Preconditions.checkNotNull(paramQuery, "query cannot be null.");
    return query(zzbs.zza(paramQuery, paramDriveFolder.getDriveId()));
  }
  
  public final Task<Boolean> removeChangeListener(@NonNull ListenerToken paramListenerToken)
  {
    Preconditions.checkNotNull(paramListenerToken, "Token is required to unregister listener.");
    if ((paramListenerToken instanceof zzg)) {
      return doUnregisterEventListener(((zzg)paramListenerToken).zzac());
    }
    throw new IllegalStateException("Could not recover key from ListenerToken");
  }
  
  public final Task<Void> removeChangeSubscription(@NonNull DriveResource paramDriveResource)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    Preconditions.checkArgument(zzj.zza(1, paramDriveResource.getDriveId()));
    return doWrite(new zzcs(this, paramDriveResource));
  }
  
  public final Task<DriveContents> reopenContentsForWrite(@NonNull DriveContents paramDriveContents)
  {
    boolean bool2 = true;
    if (!paramDriveContents.zzj())
    {
      bool1 = true;
      Preconditions.checkArgument(bool1, "DriveContents is already closed");
      if (paramDriveContents.getMode() != 268435456) {
        break label64;
      }
    }
    label64:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      Preconditions.checkArgument(bool1, "This method can only be called on contents that are currently opened in MODE_READ_ONLY.");
      paramDriveContents.zzi();
      return doRead(new zzcx(this, paramDriveContents));
      bool1 = false;
      break;
    }
  }
  
  public final Task<Void> setParents(@NonNull DriveResource paramDriveResource, @NonNull Set<DriveId> paramSet)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    Preconditions.checkNotNull(paramSet);
    return doWrite(new zzdf(this, paramDriveResource, new ArrayList(paramSet)));
  }
  
  public final Task<Void> trash(@NonNull DriveResource paramDriveResource)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    return doWrite(new zzcm(this, paramDriveResource));
  }
  
  public final Task<Void> untrash(@NonNull DriveResource paramDriveResource)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    return doWrite(new zzcn(this, paramDriveResource));
  }
  
  public final Task<Metadata> updateMetadata(@NonNull DriveResource paramDriveResource, @NonNull MetadataChangeSet paramMetadataChangeSet)
  {
    Preconditions.checkNotNull(paramDriveResource.getDriveId());
    Preconditions.checkNotNull(paramMetadataChangeSet);
    return doWrite(new zzdd(this, paramMetadataChangeSet, paramDriveResource));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */