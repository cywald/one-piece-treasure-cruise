package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzaa
  extends Metadata
{
  private final MetadataBundle zzdr;
  
  public zzaa(MetadataBundle paramMetadataBundle)
  {
    this.zzdr = paramMetadataBundle;
  }
  
  public final boolean isDataValid()
  {
    return this.zzdr != null;
  }
  
  public final String toString()
  {
    String str = String.valueOf(this.zzdr);
    return String.valueOf(str).length() + 17 + "Metadata [mImpl=" + str + "]";
  }
  
  public final <T> T zza(MetadataField<T> paramMetadataField)
  {
    return (T)this.zzdr.zza(paramMetadataField);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */