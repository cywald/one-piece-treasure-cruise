package com.google.android.gms.internal.drive;

import android.os.IBinder;
import android.os.IInterface;

public final class zzim
  extends zzb
  implements zzil
{
  public static zzil zzb(IBinder paramIBinder)
  {
    if (paramIBinder == null) {
      return null;
    }
    IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.android.gms.drive.realtime.internal.IRealtimeService");
    if ((localIInterface instanceof zzil)) {
      return (zzil)localIInterface;
    }
    return new zzin(paramIBinder);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzim.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */