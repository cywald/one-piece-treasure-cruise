package com.google.android.gms.internal.drive;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.drive.events.zzk;
import com.google.android.gms.drive.events.zzm;
import java.util.Locale;

public final class zze
  implements zzk
{
  private final zzm zzct;
  private final long zzcu;
  private final long zzcv;
  
  public zze(zzh paramzzh)
  {
    this.zzct = new zzf(paramzzh);
    this.zzcu = paramzzh.zzcu;
    this.zzcv = paramzzh.zzcv;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zze)paramObject;
      if ((!Objects.equal(this.zzct, ((zze)paramObject).zzct)) || (this.zzcu != ((zze)paramObject).zzcu)) {
        break;
      }
      bool1 = bool2;
    } while (this.zzcv == ((zze)paramObject).zzcv);
    return false;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { Long.valueOf(this.zzcv), Long.valueOf(this.zzcu), Long.valueOf(this.zzcv) });
  }
  
  public final String toString()
  {
    return String.format(Locale.US, "FileTransferProgress[FileTransferState: %s, BytesTransferred: %d, TotalBytes: %d]", new Object[] { this.zzct.toString(), Long.valueOf(this.zzcu), Long.valueOf(this.zzcv) });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */