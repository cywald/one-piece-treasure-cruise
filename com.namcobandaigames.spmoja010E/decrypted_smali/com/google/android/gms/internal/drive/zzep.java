package com.google.android.gms.internal.drive;

import android.content.IntentSender;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzep
  extends zza
  implements zzeo
{
  zzep(IBinder paramIBinder)
  {
    super(paramIBinder, "com.google.android.gms.drive.internal.IDriveService");
  }
  
  public final IntentSender zza(zzgg paramzzgg)
    throws RemoteException
  {
    Object localObject = obtainAndWriteInterfaceToken();
    zzc.zza((Parcel)localObject, paramzzgg);
    paramzzgg = transactAndReadException(10, (Parcel)localObject);
    localObject = (IntentSender)zzc.zza(paramzzgg, IntentSender.CREATOR);
    paramzzgg.recycle();
    return (IntentSender)localObject;
  }
  
  public final IntentSender zza(zzu paramzzu)
    throws RemoteException
  {
    Object localObject = obtainAndWriteInterfaceToken();
    zzc.zza((Parcel)localObject, paramzzu);
    paramzzu = transactAndReadException(11, (Parcel)localObject);
    localObject = (IntentSender)zzc.zza(paramzzu, IntentSender.CREATOR);
    paramzzu.recycle();
    return (IntentSender)localObject;
  }
  
  public final zzec zza(zzgd paramzzgd, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzgd);
    zzc.zza(localParcel, paramzzeq);
    paramzzgd = transactAndReadException(7, localParcel);
    paramzzeq = (zzec)zzc.zza(paramzzgd, zzec.CREATOR);
    paramzzgd.recycle();
    return paramzzeq;
  }
  
  public final void zza(zzab paramzzab, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzab);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(24, localParcel);
  }
  
  public final void zza(zzad paramzzad)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzad);
    transactAndReadExceptionReturnVoid(16, localParcel);
  }
  
  public final void zza(zzek paramzzek, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzek);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(1, localParcel);
  }
  
  public final void zza(zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(9, localParcel);
  }
  
  public final void zza(zzex paramzzex, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzex);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(13, localParcel);
  }
  
  public final void zza(zzgk paramzzgk, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzgk);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(2, localParcel);
  }
  
  public final void zza(zzgm paramzzgm, zzes paramzzes, String paramString, zzeq paramzzeq)
    throws RemoteException
  {
    paramString = obtainAndWriteInterfaceToken();
    zzc.zza(paramString, paramzzgm);
    zzc.zza(paramString, paramzzes);
    paramString.writeString(null);
    zzc.zza(paramString, paramzzeq);
    transactAndReadExceptionReturnVoid(15, paramString);
  }
  
  public final void zza(zzgo paramzzgo, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzgo);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(36, localParcel);
  }
  
  public final void zza(zzgq paramzzgq, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzgq);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(28, localParcel);
  }
  
  public final void zza(zzgv paramzzgv, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzgv);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(17, localParcel);
  }
  
  public final void zza(zzgx paramzzgx, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzgx);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(38, localParcel);
  }
  
  public final void zza(zzgz paramzzgz, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzgz);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(3, localParcel);
  }
  
  public final void zza(zzj paramzzj, zzes paramzzes, String paramString, zzeq paramzzeq)
    throws RemoteException
  {
    paramString = obtainAndWriteInterfaceToken();
    zzc.zza(paramString, paramzzj);
    zzc.zza(paramString, paramzzes);
    paramString.writeString(null);
    zzc.zza(paramString, paramzzeq);
    transactAndReadExceptionReturnVoid(14, paramString);
  }
  
  public final void zza(zzm paramzzm, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzm);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(18, localParcel);
  }
  
  public final void zza(zzo paramzzo, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzo);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(8, localParcel);
  }
  
  public final void zza(zzr paramzzr, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzr);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(4, localParcel);
  }
  
  public final void zza(zzw paramzzw, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzw);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(5, localParcel);
  }
  
  public final void zza(zzy paramzzy, zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzy);
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(6, localParcel);
  }
  
  public final void zzb(zzeq paramzzeq)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzeq);
    transactAndReadExceptionReturnVoid(35, localParcel);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzep.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */