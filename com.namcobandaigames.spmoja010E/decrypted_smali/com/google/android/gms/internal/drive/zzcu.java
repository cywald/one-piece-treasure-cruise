package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.events.OpenFileCallback;

final class zzcu
  extends RegisterListenerMethod<zzaw, OpenFileCallback>
{
  zzcu(zzch paramzzch, ListenerHolder paramListenerHolder1, DriveFile paramDriveFile, int paramInt, zzg paramzzg, ListenerHolder paramListenerHolder2)
  {
    super(paramListenerHolder1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzcu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */