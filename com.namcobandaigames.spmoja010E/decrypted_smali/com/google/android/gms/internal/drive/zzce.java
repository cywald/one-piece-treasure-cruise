package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DrivePreferencesApi.FileUploadPreferencesResult;

final class zzce
  extends zzl
{
  private final BaseImplementation.ResultHolder<DrivePreferencesApi.FileUploadPreferencesResult> zzdv;
  
  private zzce(BaseImplementation.ResultHolder<DrivePreferencesApi.FileUploadPreferencesResult> paramResultHolder)
  {
    BaseImplementation.ResultHolder localResultHolder;
    this.zzdv = localResultHolder;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzcf(this.zzfi, paramStatus, null, null));
  }
  
  public final void zza(zzfd paramzzfd)
    throws RemoteException
  {
    this.zzdv.setResult(new zzcf(this.zzfi, Status.RESULT_SUCCESS, paramzzfd.zzhg, null));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzce.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */