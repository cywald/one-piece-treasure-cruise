package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.query.internal.FilterHolder;

@SafeParcelable.Class(creator="OpenFileIntentSenderRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzgg
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzgg> CREATOR = new zzgh();
  @SafeParcelable.Field(id=2)
  private final String zzay;
  @SafeParcelable.Field(id=3)
  private final String[] zzaz;
  @SafeParcelable.Field(id=4)
  private final DriveId zzbb;
  @SafeParcelable.Field(id=5)
  private final FilterHolder zzbc;
  
  @SafeParcelable.Constructor
  @VisibleForTesting
  public zzgg(@SafeParcelable.Param(id=2) String paramString, @SafeParcelable.Param(id=3) String[] paramArrayOfString, @SafeParcelable.Param(id=4) DriveId paramDriveId, @SafeParcelable.Param(id=5) FilterHolder paramFilterHolder)
  {
    this.zzay = paramString;
    this.zzaz = paramArrayOfString;
    this.zzbb = paramDriveId;
    this.zzbc = paramFilterHolder;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeString(paramParcel, 2, this.zzay, false);
    SafeParcelWriter.writeStringArray(paramParcel, 3, this.zzaz, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzbb, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzbc, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzgg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */