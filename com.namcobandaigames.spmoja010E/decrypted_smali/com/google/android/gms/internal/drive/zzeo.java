package com.google.android.gms.internal.drive;

import android.content.IntentSender;
import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zzeo
  extends IInterface
{
  public abstract IntentSender zza(zzgg paramzzgg)
    throws RemoteException;
  
  public abstract IntentSender zza(zzu paramzzu)
    throws RemoteException;
  
  public abstract zzec zza(zzgd paramzzgd, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzab paramzzab, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzad paramzzad)
    throws RemoteException;
  
  public abstract void zza(zzek paramzzek, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzex paramzzex, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzgk paramzzgk, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzgm paramzzgm, zzes paramzzes, String paramString, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzgo paramzzgo, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzgq paramzzgq, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzgv paramzzgv, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzgx paramzzgx, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzgz paramzzgz, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzj paramzzj, zzes paramzzes, String paramString, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzm paramzzm, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzo paramzzo, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzr paramzzr, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzw paramzzw, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zza(zzy paramzzy, zzeq paramzzeq)
    throws RemoteException;
  
  public abstract void zzb(zzeq paramzzeq)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzeo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */