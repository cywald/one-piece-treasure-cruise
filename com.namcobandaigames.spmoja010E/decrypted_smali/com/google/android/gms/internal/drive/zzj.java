package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.zze;
import com.google.android.gms.drive.events.zzt;
import com.google.android.gms.drive.events.zzx;

@SafeParcelable.Class(creator="AddEventListenerRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzj
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzj> CREATOR = new zzk();
  @Nullable
  @SafeParcelable.Field(id=4)
  private final zze zzbt;
  @SafeParcelable.Field(id=3)
  final int zzcy;
  @Nullable
  @SafeParcelable.Field(id=5)
  private final zzx zzcz;
  @Nullable
  @SafeParcelable.Field(id=6)
  private final zzt zzda;
  @Nullable
  @SafeParcelable.Field(id=2)
  final DriveId zzk;
  
  public zzj(int paramInt, DriveId paramDriveId)
  {
    this((DriveId)Preconditions.checkNotNull(paramDriveId), 1, null, null, null);
  }
  
  @SafeParcelable.Constructor
  zzj(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) int paramInt, @SafeParcelable.Param(id=4) zze paramzze, @SafeParcelable.Param(id=5) zzx paramzzx, @SafeParcelable.Param(id=6) zzt paramzzt)
  {
    this.zzk = paramDriveId;
    this.zzcy = paramInt;
    this.zzbt = paramzze;
    this.zzcz = paramzzx;
    this.zzda = paramzzt;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzk, paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 3, this.zzcy);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzbt, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzcz, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 6, this.zzda, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */