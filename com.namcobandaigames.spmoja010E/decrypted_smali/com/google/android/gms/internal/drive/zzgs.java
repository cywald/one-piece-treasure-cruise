package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;

public final class zzgs
  extends zzl
{
  private final BaseImplementation.ResultHolder<Status> zzdv;
  
  public zzgs(BaseImplementation.ResultHolder<Status> paramResultHolder)
  {
    this.zzdv = paramResultHolder;
  }
  
  public final void onSuccess()
    throws RemoteException
  {
    this.zzdv.setResult(Status.RESULT_SUCCESS);
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(paramStatus);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzgs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */