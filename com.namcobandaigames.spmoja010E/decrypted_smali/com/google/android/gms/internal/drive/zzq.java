package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzq
{
  public final String zzay;
  public final DriveId zzbb;
  public final MetadataBundle zzdc;
  public final Integer zzdi;
  public final int zzdj;
  
  protected zzq(MetadataBundle paramMetadataBundle, Integer paramInteger, String paramString, DriveId paramDriveId, int paramInt)
  {
    this.zzdc = paramMetadataBundle;
    this.zzdi = paramInteger;
    this.zzay = paramString;
    this.zzbb = paramDriveId;
    this.zzdj = paramInt;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */