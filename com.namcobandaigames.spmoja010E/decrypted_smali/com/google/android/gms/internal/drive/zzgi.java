package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.TransferPreferences;

@SafeParcelable.Class(creator="ParcelableTransferPreferencesCreator")
@SafeParcelable.Reserved({1})
public final class zzgi
  extends AbstractSafeParcelable
  implements TransferPreferences
{
  public static final Parcelable.Creator<zzgi> CREATOR = new zzgj();
  @SafeParcelable.Field(id=4)
  private final boolean zzbk;
  @SafeParcelable.Field(id=3)
  private final int zzbl;
  @SafeParcelable.Field(id=2)
  private final int zzgw;
  
  @SafeParcelable.Constructor
  zzgi(@SafeParcelable.Param(id=2) int paramInt1, @SafeParcelable.Param(id=3) int paramInt2, @SafeParcelable.Param(id=4) boolean paramBoolean)
  {
    this.zzgw = paramInt1;
    this.zzbl = paramInt2;
    this.zzbk = paramBoolean;
  }
  
  public final int getBatteryUsagePreference()
  {
    return this.zzbl;
  }
  
  public final int getNetworkPreference()
  {
    return this.zzgw;
  }
  
  public final boolean isRoamingAllowed()
  {
    return this.zzbk;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 2, this.zzgw);
    SafeParcelWriter.writeInt(paramParcel, 3, this.zzbl);
    SafeParcelWriter.writeBoolean(paramParcel, 4, this.zzbk);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzgi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */