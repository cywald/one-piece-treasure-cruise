package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.drive.events.ListenerToken;
import com.google.android.gms.drive.events.OpenFileCallback;

final class zzdk
  extends zzl
{
  private final ListenerToken zzgh;
  private final ListenerHolder<OpenFileCallback> zzgi;
  
  zzdk(ListenerToken paramListenerToken, ListenerHolder<OpenFileCallback> paramListenerHolder)
  {
    this.zzgh = paramListenerHolder;
    ListenerHolder localListenerHolder;
    this.zzgi = localListenerHolder;
  }
  
  private final void zza(zzdg<OpenFileCallback> paramzzdg)
  {
    this.zzgi.notifyListener(new zzdo(this, paramzzdg));
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    zza(new zzdl(this, paramStatus));
  }
  
  public final void zza(zzfb paramzzfb)
    throws RemoteException
  {
    zza(new zzdn(this, paramzzfb));
  }
  
  public final void zza(zzff paramzzff)
    throws RemoteException
  {
    zza(new zzdm(paramzzff));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzdk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */