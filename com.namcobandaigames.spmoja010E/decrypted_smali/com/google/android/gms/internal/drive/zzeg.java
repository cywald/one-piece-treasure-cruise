package com.google.android.gms.internal.drive;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Pair;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.drive.events.CompletionListener;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.zzb;
import com.google.android.gms.drive.events.zzd;
import com.google.android.gms.drive.events.zzi;
import com.google.android.gms.drive.events.zzk;
import com.google.android.gms.drive.events.zzl;
import com.google.android.gms.drive.events.zzo;
import com.google.android.gms.drive.events.zzq;
import com.google.android.gms.drive.events.zzr;

final class zzeg
  extends Handler
{
  private final Context zzgu;
  
  private zzeg(Looper paramLooper, Context paramContext)
  {
    super(paramLooper);
    this.zzgu = paramContext;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default: 
      zzee.zzah().efmt("EventCallback", "Don't know how to handle this event in context %s", new Object[] { this.zzgu });
    }
    do
    {
      return;
      localObject = (Pair)paramMessage.obj;
      paramMessage = (zzi)((Pair)localObject).first;
      localObject = (DriveEvent)((Pair)localObject).second;
      switch (((DriveEvent)localObject).getType())
      {
      case 5: 
      case 6: 
      case 7: 
      default: 
        zzee.zzah().wfmt("EventCallback", "Unexpected event: %s", new Object[] { localObject });
        return;
      case 1: 
        ((ChangeListener)paramMessage).onChange((ChangeEvent)localObject);
        return;
      case 2: 
        ((CompletionListener)paramMessage).onCompletion((CompletionEvent)localObject);
        return;
      case 3: 
        paramMessage = (zzq)paramMessage;
        localObject = (zzo)localObject;
        DataHolder localDataHolder = ((zzo)localObject).zzy();
        if (localDataHolder != null) {
          paramMessage.zza(new zzeh(new MetadataBuffer(localDataHolder)));
        }
        break;
      }
    } while (!((zzo)localObject).zzz());
    paramMessage.zzc(((zzo)localObject).zzaa());
    return;
    ((zzd)paramMessage).zza((zzb)localObject);
    return;
    Object localObject = new zze(((zzr)localObject).zzab());
    ((zzl)paramMessage).zza((zzk)localObject);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzeg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */