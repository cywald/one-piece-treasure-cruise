package com.google.android.gms.internal.drive;

import java.io.IOException;

public final class zziw
  extends IOException
{
  public zziw(String paramString)
  {
    super(paramString);
  }
  
  static zziw zzbk()
  {
    return new zziw("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
  }
  
  static zziw zzbl()
  {
    return new zziw("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
  }
  
  static zziw zzbm()
  {
    return new zziw("CodedInputStream encountered a malformed varint.");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zziw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */