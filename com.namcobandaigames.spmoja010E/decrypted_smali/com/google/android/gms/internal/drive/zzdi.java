package com.google.android.gms.internal.drive;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.OnChangeListener;
import com.google.android.gms.drive.events.zzj;

final class zzdi
{
  private OnChangeListener zzge;
  private zzee zzgf;
  private DriveId zzk;
  
  zzdi(zzch paramzzch, OnChangeListener paramOnChangeListener, DriveId paramDriveId)
  {
    Preconditions.checkState(zzj.zza(1, paramDriveId));
    this.zzge = paramOnChangeListener;
    this.zzk = paramDriveId;
    paramDriveId = paramzzch.getLooper();
    paramzzch = paramzzch.getApplicationContext();
    paramOnChangeListener.getClass();
    this.zzgf = new zzee(paramDriveId, paramzzch, 1, zzdj.zza(paramOnChangeListener));
    this.zzgf.zzf(1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzdi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */