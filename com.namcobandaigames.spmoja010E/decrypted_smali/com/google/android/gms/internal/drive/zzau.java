package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl;
import com.google.android.gms.drive.Drive;

public abstract class zzau<R extends Result>
  extends BaseImplementation.ApiMethodImpl<R, zzaw>
{
  public zzau(GoogleApiClient paramGoogleApiClient)
  {
    super(Drive.CLIENT_KEY, paramGoogleApiClient);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzau.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */