package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

@SafeParcelable.Class(creator="CloseContentsAndUpdateMetadataRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzm
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzm> CREATOR = new zzn();
  @SafeParcelable.Field(id=6)
  private final String zzal;
  @SafeParcelable.Field(id=5)
  private final boolean zzam;
  @SafeParcelable.Field(defaultValue="true", id=10)
  private final boolean zzar;
  @SafeParcelable.Field(id=2)
  private final DriveId zzdb;
  @SafeParcelable.Field(id=3)
  private final MetadataBundle zzdc;
  @Nullable
  @SafeParcelable.Field(id=4)
  private final Contents zzdd;
  @SafeParcelable.Field(id=7)
  private final int zzde;
  @SafeParcelable.Field(id=8)
  private final int zzdf;
  @SafeParcelable.Field(id=9)
  private final boolean zzdg;
  
  @VisibleForTesting
  public zzm(DriveId paramDriveId, MetadataBundle paramMetadataBundle, int paramInt, boolean paramBoolean, com.google.android.gms.drive.zzn paramzzn)
  {
    this(paramDriveId, paramMetadataBundle, null, paramzzn.zzl(), paramzzn.zzk(), paramzzn.zzm(), paramInt, paramBoolean, paramzzn.zzo());
  }
  
  @SafeParcelable.Constructor
  zzm(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) MetadataBundle paramMetadataBundle, @SafeParcelable.Param(id=4) Contents paramContents, @SafeParcelable.Param(id=5) boolean paramBoolean1, @SafeParcelable.Param(id=6) String paramString, @SafeParcelable.Param(id=7) int paramInt1, @SafeParcelable.Param(id=8) int paramInt2, @SafeParcelable.Param(id=9) boolean paramBoolean2, @SafeParcelable.Param(id=10) boolean paramBoolean3)
  {
    this.zzdb = paramDriveId;
    this.zzdc = paramMetadataBundle;
    this.zzdd = paramContents;
    this.zzam = paramBoolean1;
    this.zzal = paramString;
    this.zzde = paramInt1;
    this.zzdf = paramInt2;
    this.zzdg = paramBoolean2;
    this.zzar = paramBoolean3;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzdb, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 3, this.zzdc, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzdd, paramInt, false);
    SafeParcelWriter.writeBoolean(paramParcel, 5, this.zzam);
    SafeParcelWriter.writeString(paramParcel, 6, this.zzal, false);
    SafeParcelWriter.writeInt(paramParcel, 7, this.zzde);
    SafeParcelWriter.writeInt(paramParcel, 8, this.zzdf);
    SafeParcelWriter.writeBoolean(paramParcel, 9, this.zzdg);
    SafeParcelWriter.writeBoolean(paramParcel, 10, this.zzar);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */