package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzhd
  extends zzhb<DriveFile>
{
  public zzhd(TaskCompletionSource<DriveFile> paramTaskCompletionSource)
  {
    super(paramTaskCompletionSource);
  }
  
  public final void zza(zzfh paramzzfh)
    throws RemoteException
  {
    zzap().setResult(paramzzfh.getDriveId().asDriveFile());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */