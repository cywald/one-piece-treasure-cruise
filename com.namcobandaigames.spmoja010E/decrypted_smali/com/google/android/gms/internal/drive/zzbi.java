package com.google.android.gms.internal.drive;

import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.IOUtils;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.ExecutionOptions.Builder;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.zzn;
import com.google.android.gms.drive.zzp;
import java.io.InputStream;
import java.io.OutputStream;

public final class zzbi
  implements DriveContents
{
  private static final GmsLogger zzbx = new GmsLogger("DriveContentsImpl", "");
  private boolean closed = false;
  private final Contents zzeq;
  private boolean zzer = false;
  private boolean zzes = false;
  
  public zzbi(Contents paramContents)
  {
    this.zzeq = ((Contents)Preconditions.checkNotNull(paramContents));
  }
  
  private final PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, @Nullable MetadataChangeSet paramMetadataChangeSet, @Nullable zzn paramzzn)
  {
    if (paramzzn == null) {
      paramzzn = (zzn)new zzp().build();
    }
    for (;;)
    {
      if (this.zzeq.getMode() == 268435456) {
        throw new IllegalStateException("Cannot commit contents opened with MODE_READ_ONLY");
      }
      if ((ExecutionOptions.zza(paramzzn.zzm())) && (!this.zzeq.zza())) {
        throw new IllegalStateException("DriveContents must be valid for conflict detection.");
      }
      paramzzn.zza(paramGoogleApiClient);
      if (this.closed) {
        throw new IllegalStateException("DriveContents already closed.");
      }
      if (getDriveId() == null) {
        throw new IllegalStateException("Only DriveContents obtained through DriveFile.open can be committed.");
      }
      if (paramMetadataChangeSet != null) {}
      for (;;)
      {
        zzi();
        return paramGoogleApiClient.execute(new zzbk(this, paramGoogleApiClient, paramMetadataChangeSet, paramzzn));
        paramMetadataChangeSet = MetadataChangeSet.zzav;
      }
    }
  }
  
  public final PendingResult<Status> commit(GoogleApiClient paramGoogleApiClient, @Nullable MetadataChangeSet paramMetadataChangeSet)
  {
    return zza(paramGoogleApiClient, paramMetadataChangeSet, null);
  }
  
  public final PendingResult<Status> commit(GoogleApiClient paramGoogleApiClient, @Nullable MetadataChangeSet paramMetadataChangeSet, @Nullable ExecutionOptions paramExecutionOptions)
  {
    if (paramExecutionOptions == null) {}
    for (paramExecutionOptions = null;; paramExecutionOptions = zzn.zza(paramExecutionOptions)) {
      return zza(paramGoogleApiClient, paramMetadataChangeSet, paramExecutionOptions);
    }
  }
  
  public final void discard(GoogleApiClient paramGoogleApiClient)
  {
    if (this.closed) {
      throw new IllegalStateException("DriveContents already closed.");
    }
    zzi();
    ((zzbm)paramGoogleApiClient.execute(new zzbm(this, paramGoogleApiClient))).setResultCallback(new zzbl(this));
  }
  
  public final DriveId getDriveId()
  {
    return this.zzeq.getDriveId();
  }
  
  public final InputStream getInputStream()
  {
    if (this.closed) {
      throw new IllegalStateException("Contents have been closed, cannot access the input stream.");
    }
    if (this.zzeq.getMode() != 268435456) {
      throw new IllegalStateException("getInputStream() can only be used with contents opened with MODE_READ_ONLY.");
    }
    if (this.zzer) {
      throw new IllegalStateException("getInputStream() can only be called once per Contents instance.");
    }
    this.zzer = true;
    return this.zzeq.getInputStream();
  }
  
  public final int getMode()
  {
    return this.zzeq.getMode();
  }
  
  public final OutputStream getOutputStream()
  {
    if (this.closed) {
      throw new IllegalStateException("Contents have been closed, cannot access the output stream.");
    }
    if (this.zzeq.getMode() != 536870912) {
      throw new IllegalStateException("getOutputStream() can only be used with contents opened with MODE_WRITE_ONLY.");
    }
    if (this.zzes) {
      throw new IllegalStateException("getOutputStream() can only be called once per Contents instance.");
    }
    this.zzes = true;
    return this.zzeq.getOutputStream();
  }
  
  public final ParcelFileDescriptor getParcelFileDescriptor()
  {
    if (this.closed) {
      throw new IllegalStateException("Contents have been closed, cannot access the output stream.");
    }
    return this.zzeq.getParcelFileDescriptor();
  }
  
  public final PendingResult<DriveApi.DriveContentsResult> reopenForWrite(GoogleApiClient paramGoogleApiClient)
  {
    if (this.closed) {
      throw new IllegalStateException("DriveContents already closed.");
    }
    if (this.zzeq.getMode() != 268435456) {
      throw new IllegalStateException("reopenForWrite can only be used with DriveContents opened with MODE_READ_ONLY.");
    }
    zzi();
    return paramGoogleApiClient.enqueue(new zzbj(this, paramGoogleApiClient));
  }
  
  public final Contents zzh()
  {
    return this.zzeq;
  }
  
  public final void zzi()
  {
    IOUtils.closeQuietly(this.zzeq.getParcelFileDescriptor());
    this.closed = true;
  }
  
  public final boolean zzj()
  {
    return this.closed;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzbi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */