package com.google.android.gms.internal.drive;

import android.os.IBinder;

public final class zzin
  extends zza
  implements zzil
{
  zzin(IBinder paramIBinder)
  {
    super(paramIBinder, "com.google.android.gms.drive.realtime.internal.IRealtimeService");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */