package com.google.android.gms.internal.drive;

import java.io.IOException;

public final class zzho
  extends zzir<zzho>
{
  public long zzac = -1L;
  public long zzf = -1L;
  
  public zzho()
  {
    this.zzmw = null;
    this.zznf = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzho)) {
        return false;
      }
      paramObject = (zzho)paramObject;
      if (this.zzac != ((zzho)paramObject).zzac) {
        return false;
      }
      if (this.zzf != ((zzho)paramObject).zzf) {
        return false;
      }
      if ((this.zzmw != null) && (!this.zzmw.isEmpty())) {
        break;
      }
    } while ((((zzho)paramObject).zzmw == null) || (((zzho)paramObject).zzmw.isEmpty()));
    return false;
    return this.zzmw.equals(((zzho)paramObject).zzmw);
  }
  
  public final int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = (int)(this.zzac ^ this.zzac >>> 32);
    int m = (int)(this.zzf ^ this.zzf >>> 32);
    if ((this.zzmw == null) || (this.zzmw.isEmpty())) {}
    for (int i = 0;; i = this.zzmw.hashCode()) {
      return i + (((j + 527) * 31 + k) * 31 + m) * 31;
    }
  }
  
  public final void zza(zzip paramzzip)
    throws IOException
  {
    paramzzip.zza(1, this.zzac);
    paramzzip.zza(2, this.zzf);
    super.zza(paramzzip);
  }
  
  protected final int zzaq()
  {
    return super.zzaq() + zzip.zzb(1, this.zzac) + zzip.zzb(2, this.zzf);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzho.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */