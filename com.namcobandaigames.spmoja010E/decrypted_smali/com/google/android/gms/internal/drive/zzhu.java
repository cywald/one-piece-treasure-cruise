package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.zzt;

public final class zzhu
  extends zzt
  implements SearchableMetadataField<String>
{
  public zzhu(int paramInt)
  {
    super("mimeType", 4100000);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */