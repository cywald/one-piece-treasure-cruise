package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DriveApi.DriveIdResult;
import com.google.android.gms.drive.Metadata;

final class zzan
  extends zzl
{
  private final BaseImplementation.ResultHolder<DriveApi.DriveIdResult> zzdv;
  
  public zzan(BaseImplementation.ResultHolder<DriveApi.DriveIdResult> paramResultHolder)
  {
    this.zzdv = paramResultHolder;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzao(paramStatus, null));
  }
  
  public final void zza(zzfh paramzzfh)
    throws RemoteException
  {
    this.zzdv.setResult(new zzao(Status.RESULT_SUCCESS, paramzzfh.zzdb));
  }
  
  public final void zza(zzfs paramzzfs)
    throws RemoteException
  {
    this.zzdv.setResult(new zzao(Status.RESULT_SUCCESS, new zzaa(paramzzfs.zzdl).getDriveId()));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */