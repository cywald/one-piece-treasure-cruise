package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.internal.ListenerHolder.ListenerKey;
import com.google.android.gms.common.api.internal.UnregisterListenerMethod;
import com.google.android.gms.drive.events.OpenFileCallback;

final class zzcv
  extends UnregisterListenerMethod<zzaw, OpenFileCallback>
{
  zzcv(zzch paramzzch, ListenerHolder.ListenerKey paramListenerKey, zzg paramzzg)
  {
    super(paramListenerKey);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzcv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */