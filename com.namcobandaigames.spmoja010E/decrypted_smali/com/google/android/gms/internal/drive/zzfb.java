package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.Contents;

@SafeParcelable.Class(creator="OnContentsResponseCreator")
@SafeParcelable.Reserved({1})
public final class zzfb
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzfb> CREATOR = new zzfc();
  @SafeParcelable.Field(id=2)
  final Contents zzeq;
  @SafeParcelable.Field(id=3)
  final boolean zzhf;
  
  @SafeParcelable.Constructor
  public zzfb(@SafeParcelable.Param(id=2) Contents paramContents, @SafeParcelable.Param(id=3) boolean paramBoolean)
  {
    this.zzeq = paramContents;
    this.zzhf = paramBoolean;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzeq, paramInt, false);
    SafeParcelWriter.writeBoolean(paramParcel, 3, this.zzhf);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final Contents zzai()
  {
    return this.zzeq;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzfb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */