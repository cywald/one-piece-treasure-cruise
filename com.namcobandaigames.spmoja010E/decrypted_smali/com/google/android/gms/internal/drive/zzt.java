package com.google.android.gms.internal.drive;

import android.content.IntentSender;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

@Deprecated
public final class zzt
{
  private String zzay;
  private DriveId zzbb;
  private Integer zzdi;
  private final int zzdj = 0;
  private MetadataChangeSet zzdk;
  
  public zzt(int paramInt) {}
  
  public final IntentSender build(GoogleApiClient paramGoogleApiClient)
  {
    Preconditions.checkState(paramGoogleApiClient.isConnected(), "Client must be connected");
    zzf();
    paramGoogleApiClient = (zzaw)paramGoogleApiClient.getClient(Drive.CLIENT_KEY);
    this.zzdk.zzp().zza(paramGoogleApiClient.getContext());
    try
    {
      paramGoogleApiClient = ((zzeo)paramGoogleApiClient.getService()).zza(new zzu(this.zzdk.zzp(), this.zzdi.intValue(), this.zzay, this.zzbb, Integer.valueOf(0)));
      return paramGoogleApiClient;
    }
    catch (RemoteException paramGoogleApiClient)
    {
      throw new RuntimeException("Unable to connect Drive Play Service", paramGoogleApiClient);
    }
  }
  
  public final int getRequestId()
  {
    return this.zzdi.intValue();
  }
  
  public final void zza(DriveId paramDriveId)
  {
    this.zzbb = ((DriveId)Preconditions.checkNotNull(paramDriveId));
  }
  
  public final void zza(MetadataChangeSet paramMetadataChangeSet)
  {
    this.zzdk = ((MetadataChangeSet)Preconditions.checkNotNull(paramMetadataChangeSet));
  }
  
  public final MetadataChangeSet zzb()
  {
    return this.zzdk;
  }
  
  public final DriveId zzc()
  {
    return this.zzbb;
  }
  
  public final void zzc(String paramString)
  {
    this.zzay = ((String)Preconditions.checkNotNull(paramString));
  }
  
  public final String zzd()
  {
    return this.zzay;
  }
  
  public final void zzd(int paramInt)
  {
    this.zzdi = Integer.valueOf(paramInt);
  }
  
  public final void zzf()
  {
    Preconditions.checkNotNull(this.zzdk, "Must provide initial metadata via setInitialMetadata.");
    if (this.zzdi == null) {}
    for (int i = 0;; i = this.zzdi.intValue())
    {
      this.zzdi = Integer.valueOf(i);
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */