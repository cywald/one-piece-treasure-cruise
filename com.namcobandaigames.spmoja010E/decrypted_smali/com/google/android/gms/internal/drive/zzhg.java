package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.drive.TransferPreferences;
import com.google.android.gms.drive.TransferPreferencesBuilder;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzhg
  extends zzhb<TransferPreferences>
{
  public zzhg(TaskCompletionSource<TransferPreferences> paramTaskCompletionSource)
  {
    super(paramTaskCompletionSource);
  }
  
  public final void zza(zzfd paramzzfd)
    throws RemoteException
  {
    zzap().setResult(new TransferPreferencesBuilder(paramzzfd.zzaj()).build());
  }
  
  public final void zza(zzfu paramzzfu)
    throws RemoteException
  {
    zzap().setResult(paramzzfu.zzao());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */