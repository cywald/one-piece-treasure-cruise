package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.zzb;
import com.google.android.gms.drive.metadata.zza;

public final class zzhz
  extends zzb
  implements SearchableMetadataField<Boolean>
{
  public zzhz(String paramString, int paramInt)
  {
    super(paramString, 4100000);
  }
  
  protected final Boolean zze(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    if (paramDataHolder.getInteger(getName(), paramInt1, paramInt2) != 0) {}
    for (boolean bool = true;; bool = false) {
      return Boolean.valueOf(bool);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */