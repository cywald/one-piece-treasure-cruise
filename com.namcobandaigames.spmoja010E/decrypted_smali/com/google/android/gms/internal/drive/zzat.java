package com.google.android.gms.internal.drive;

import android.annotation.SuppressLint;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

@SuppressLint({"MissingRemoteException"})
final class zzat
  extends zzav
{
  zzat(GoogleApiClient paramGoogleApiClient, Status paramStatus)
  {
    super(paramGoogleApiClient);
    setResult(paramStatus);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */