package com.google.android.gms.internal.drive;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.zzk;

final class zzdh
  extends TaskApiCall<zzaw, DriveFile>
{
  private final DriveFolder zzfh;
  private final MetadataChangeSet zzga;
  private ExecutionOptions zzgb;
  private String zzgc;
  private zzk zzgd;
  private final DriveContents zzo;
  
  zzdh(@NonNull DriveFolder paramDriveFolder, @NonNull MetadataChangeSet paramMetadataChangeSet, @Nullable DriveContents paramDriveContents, @NonNull ExecutionOptions paramExecutionOptions, @Nullable String paramString)
  {
    this.zzfh = paramDriveFolder;
    this.zzga = paramMetadataChangeSet;
    this.zzo = paramDriveContents;
    this.zzgb = paramExecutionOptions;
    this.zzgc = null;
    Preconditions.checkNotNull(paramDriveFolder, "DriveFolder must not be null");
    Preconditions.checkNotNull(paramDriveFolder.getDriveId(), "Folder's DriveId must not be null");
    Preconditions.checkNotNull(paramMetadataChangeSet, "MetadataChangeSet must not be null");
    Preconditions.checkNotNull(paramExecutionOptions, "ExecutionOptions must not be null");
    this.zzgd = zzk.zze(paramMetadataChangeSet.getMimeType());
    if ((this.zzgd != null) && (this.zzgd.isFolder())) {
      throw new IllegalArgumentException("May not create folders using this method. Use DriveFolderManagerClient#createFolder() instead of mime type application/vnd.google-apps.folder");
    }
    if (paramDriveContents != null)
    {
      if (!(paramDriveContents instanceof zzbi)) {
        throw new IllegalArgumentException("Only DriveContents obtained from the Drive API are accepted.");
      }
      if (paramDriveContents.getDriveId() != null) {
        throw new IllegalArgumentException("Only DriveContents obtained through DriveApi.newDriveContents are accepted for file creation.");
      }
      if (paramDriveContents.zzj()) {
        throw new IllegalArgumentException("DriveContents are already closed.");
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzdh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */