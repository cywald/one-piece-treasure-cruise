package com.google.android.gms.internal.drive;

import java.nio.charset.Charset;

public final class zziv
{
  private static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
  protected static final Charset UTF_8 = Charset.forName("UTF-8");
  private static final Object zzne = new Object();
  
  public static void zza(zzir paramzzir1, zzir paramzzir2)
  {
    if (paramzzir1.zzmw != null) {
      paramzzir2.zzmw = ((zzit)paramzzir1.zzmw.clone());
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zziv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */