package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.Contents;

@SafeParcelable.Class(creator="CloseContentsRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzo> CREATOR = new zzp();
  @Nullable
  @SafeParcelable.Field(id=2)
  private final Contents zzdd;
  @SafeParcelable.Field(id=4)
  private final int zzdf;
  @SafeParcelable.Field(id=3)
  private final Boolean zzdh;
  
  @VisibleForTesting
  public zzo(int paramInt, boolean paramBoolean)
  {
    this(null, Boolean.valueOf(false), paramInt);
  }
  
  @SafeParcelable.Constructor
  zzo(@SafeParcelable.Param(id=2) Contents paramContents, @SafeParcelable.Param(id=3) Boolean paramBoolean, @SafeParcelable.Param(id=4) int paramInt)
  {
    this.zzdd = paramContents;
    this.zzdh = paramBoolean;
    this.zzdf = paramInt;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzdd, paramInt, false);
    SafeParcelWriter.writeBooleanObject(paramParcel, 3, this.zzdh, false);
    SafeParcelWriter.writeInt(paramParcel, 4, this.zzdf);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */