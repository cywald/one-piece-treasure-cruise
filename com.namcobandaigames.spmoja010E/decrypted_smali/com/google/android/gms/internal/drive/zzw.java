package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

@SafeParcelable.Class(creator="CreateFileRequestCreator")
@SafeParcelable.Reserved({1, 10})
public final class zzw
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzw> CREATOR = new zzx();
  @SafeParcelable.Field(id=7)
  private final String zzal;
  @SafeParcelable.Field(id=4)
  private final Contents zzdd;
  @SafeParcelable.Field(id=3)
  private final MetadataBundle zzdl;
  @SafeParcelable.Field(id=5)
  private final Integer zzdm;
  @SafeParcelable.Field(id=2)
  private final DriveId zzdn;
  @SafeParcelable.Field(id=6)
  private final boolean zzdo;
  @SafeParcelable.Field(id=8)
  private final int zzdp;
  @SafeParcelable.Field(id=9)
  private final int zzdq;
  
  @VisibleForTesting
  public zzw(DriveId paramDriveId, MetadataBundle paramMetadataBundle, int paramInt1, int paramInt2, ExecutionOptions paramExecutionOptions)
  {
    this(paramDriveId, paramMetadataBundle, null, paramInt2, paramExecutionOptions.zzl(), paramExecutionOptions.zzk(), paramExecutionOptions.zzm(), paramInt1);
  }
  
  @SafeParcelable.Constructor
  zzw(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) MetadataBundle paramMetadataBundle, @SafeParcelable.Param(id=4) Contents paramContents, @SafeParcelable.Param(id=5) int paramInt1, @SafeParcelable.Param(id=6) boolean paramBoolean, @SafeParcelable.Param(id=7) String paramString, @SafeParcelable.Param(id=8) int paramInt2, @SafeParcelable.Param(id=9) int paramInt3)
  {
    if ((paramContents != null) && (paramInt3 != 0)) {
      if (paramContents.getRequestId() != paramInt3) {
        break label56;
      }
    }
    label56:
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool, "inconsistent contents reference");
      if ((paramInt1 != 0) || (paramContents != null) || (paramInt3 != 0)) {
        break;
      }
      throw new IllegalArgumentException("Need a valid contents");
    }
    this.zzdn = ((DriveId)Preconditions.checkNotNull(paramDriveId));
    this.zzdl = ((MetadataBundle)Preconditions.checkNotNull(paramMetadataBundle));
    this.zzdd = paramContents;
    this.zzdm = Integer.valueOf(paramInt1);
    this.zzal = paramString;
    this.zzdp = paramInt2;
    this.zzdo = paramBoolean;
    this.zzdq = paramInt3;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzdn, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 3, this.zzdl, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzdd, paramInt, false);
    SafeParcelWriter.writeIntegerObject(paramParcel, 5, this.zzdm, false);
    SafeParcelWriter.writeBoolean(paramParcel, 6, this.zzdo);
    SafeParcelWriter.writeString(paramParcel, 7, this.zzal, false);
    SafeParcelWriter.writeInt(paramParcel, 8, this.zzdp);
    SafeParcelWriter.writeInt(paramParcel, 9, this.zzdq);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */