package com.google.android.gms.internal.drive;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.GmsClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.UidVerifier;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.events.zzd;
import com.google.android.gms.drive.events.zzl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.concurrent.GuardedBy;

public final class zzaw
  extends GmsClient<zzeo>
{
  private final String zzdz;
  protected final boolean zzea;
  private volatile DriveId zzeb;
  private volatile DriveId zzec;
  private volatile boolean zzed = false;
  @VisibleForTesting
  @GuardedBy("changeEventCallbackMap")
  private final Map<DriveId, Map<ChangeListener, zzee>> zzee = new HashMap();
  @VisibleForTesting
  @GuardedBy("changesAvailableEventCallbackMap")
  private final Map<zzd, zzee> zzef = new HashMap();
  @VisibleForTesting
  @GuardedBy("uploadProgressEventCallbackMap")
  private final Map<DriveId, Map<zzl, zzee>> zzeg = new HashMap();
  @VisibleForTesting
  @GuardedBy("pinnedDownloadProgressEventCallbackMap")
  private final Map<DriveId, Map<zzl, zzee>> zzeh = new HashMap();
  private final Bundle zzx;
  
  public zzaw(Context paramContext, Looper paramLooper, ClientSettings paramClientSettings, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener, Bundle paramBundle)
  {
    super(paramContext, paramLooper, 11, paramClientSettings, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.zzdz = paramClientSettings.getRealClientPackageName();
    this.zzx = paramBundle;
    paramLooper = new Intent("com.google.android.gms.drive.events.HANDLE_EVENT");
    paramLooper.setPackage(paramContext.getPackageName());
    paramContext = paramContext.getPackageManager().queryIntentServices(paramLooper, 0);
    switch (paramContext.size())
    {
    default: 
      paramContext = paramLooper.getAction();
      throw new IllegalStateException(String.valueOf(paramContext).length() + 72 + "AndroidManifest.xml can only define one service that handles the " + paramContext + " action");
    case 0: 
      this.zzea = false;
      return;
    }
    paramContext = ((ResolveInfo)paramContext.get(0)).serviceInfo;
    if (!paramContext.exported)
    {
      paramContext = paramContext.name;
      throw new IllegalStateException(String.valueOf(paramContext).length() + 60 + "Drive event service " + paramContext + " must be exported in AndroidManifest.xml");
    }
    this.zzea = true;
  }
  
  public final void disconnect()
  {
    if (isConnected()) {}
    try
    {
      ((zzeo)getService()).zza(new zzad());
      super.disconnect();
      synchronized (this.zzee)
      {
        this.zzee.clear();
        synchronized (this.zzef)
        {
          this.zzef.clear();
          synchronized (this.zzeg)
          {
            this.zzeg.clear();
          }
        }
      }
      synchronized (this.zzeh)
      {
        this.zzeh.clear();
        return;
        localObject1 = finally;
        throw ((Throwable)localObject1);
        localObject2 = finally;
        throw ((Throwable)localObject2);
        localObject3 = finally;
        throw ((Throwable)localObject3);
      }
    }
    catch (RemoteException localRemoteException)
    {
      for (;;) {}
    }
  }
  
  protected final Bundle getGetServiceRequestExtraArgs()
  {
    String str = getContext().getPackageName();
    Preconditions.checkNotNull(str);
    if (!getClientSettings().getAllRequestedScopes().isEmpty()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool);
      Bundle localBundle = new Bundle();
      if (!str.equals(this.zzdz)) {
        localBundle.putString("proxy_package_name", this.zzdz);
      }
      localBundle.putAll(this.zzx);
      return localBundle;
    }
  }
  
  public final int getMinApkVersion()
  {
    return 12451000;
  }
  
  protected final String getServiceDescriptor()
  {
    return "com.google.android.gms.drive.internal.IDriveService";
  }
  
  protected final String getStartServiceAction()
  {
    return "com.google.android.gms.drive.ApiService.START";
  }
  
  protected final void onPostInitHandler(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    if (paramBundle != null)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      this.zzeb = ((DriveId)paramBundle.getParcelable("com.google.android.gms.drive.root_id"));
      this.zzec = ((DriveId)paramBundle.getParcelable("com.google.android.gms.drive.appdata_id"));
      this.zzed = true;
    }
    super.onPostInitHandler(paramInt1, paramIBinder, paramBundle, paramInt2);
  }
  
  public final boolean requiresAccount()
  {
    return true;
  }
  
  public final boolean requiresSignIn()
  {
    return (!getContext().getPackageName().equals(this.zzdz)) || (!UidVerifier.isGooglePlayServicesUid(getContext(), Process.myUid()));
  }
  
  final PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, DriveId paramDriveId, ChangeListener paramChangeListener)
  {
    Preconditions.checkArgument(com.google.android.gms.drive.events.zzj.zza(1, paramDriveId));
    Preconditions.checkNotNull(paramChangeListener, "listener");
    Preconditions.checkState(isConnected(), "Client must be connected");
    for (;;)
    {
      synchronized (this.zzee)
      {
        Object localObject = (Map)this.zzee.get(paramDriveId);
        if (localObject == null)
        {
          localObject = new HashMap();
          this.zzee.put(paramDriveId, localObject);
          zzee localzzee = (zzee)((Map)localObject).get(paramChangeListener);
          if (localzzee == null)
          {
            localzzee = new zzee(getLooper(), getContext(), 1, paramChangeListener);
            ((Map)localObject).put(paramChangeListener, localzzee);
            paramChangeListener = localzzee;
            paramChangeListener.zzf(1);
            paramGoogleApiClient = paramGoogleApiClient.execute(new zzax(this, paramGoogleApiClient, new zzj(1, paramDriveId), paramChangeListener));
            return paramGoogleApiClient;
          }
          paramChangeListener = localzzee;
          if (localzzee.zzg(1))
          {
            paramGoogleApiClient = new zzat(paramGoogleApiClient, Status.RESULT_SUCCESS);
            return paramGoogleApiClient;
          }
        }
      }
    }
  }
  
  public final DriveId zzad()
  {
    return this.zzeb;
  }
  
  public final DriveId zzae()
  {
    return this.zzec;
  }
  
  public final boolean zzaf()
  {
    return this.zzed;
  }
  
  public final boolean zzag()
  {
    return this.zzea;
  }
  
  final PendingResult<Status> zzb(GoogleApiClient paramGoogleApiClient, DriveId paramDriveId, ChangeListener paramChangeListener)
  {
    Preconditions.checkArgument(com.google.android.gms.drive.events.zzj.zza(1, paramDriveId));
    Preconditions.checkState(isConnected(), "Client must be connected");
    Preconditions.checkNotNull(paramChangeListener, "listener");
    Map localMap2;
    synchronized (this.zzee)
    {
      localMap2 = (Map)this.zzee.get(paramDriveId);
      if (localMap2 == null)
      {
        paramGoogleApiClient = new zzat(paramGoogleApiClient, Status.RESULT_SUCCESS);
        return paramGoogleApiClient;
      }
      paramChangeListener = (zzee)localMap2.remove(paramChangeListener);
      if (paramChangeListener == null)
      {
        paramGoogleApiClient = new zzat(paramGoogleApiClient, Status.RESULT_SUCCESS);
        return paramGoogleApiClient;
      }
    }
    if (localMap2.isEmpty()) {
      this.zzee.remove(paramDriveId);
    }
    paramGoogleApiClient = paramGoogleApiClient.execute(new zzay(this, paramGoogleApiClient, new zzgm(paramDriveId, 1), paramChangeListener));
    return paramGoogleApiClient;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzaw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */