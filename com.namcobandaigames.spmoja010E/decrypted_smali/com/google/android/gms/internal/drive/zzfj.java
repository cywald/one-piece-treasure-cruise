package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.zzb;
import com.google.android.gms.drive.events.zzo;
import com.google.android.gms.drive.events.zzr;
import com.google.android.gms.drive.events.zzv;

@SafeParcelable.Class(creator="OnEventResponseCreator")
@SafeParcelable.Reserved({1, 4, 8})
public final class zzfj
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzfj> CREATOR = new zzfk();
  @SafeParcelable.Field(id=2)
  private final int zzcy;
  @SafeParcelable.Field(id=3)
  private final ChangeEvent zzhl;
  @SafeParcelable.Field(id=5)
  private final CompletionEvent zzhm;
  @SafeParcelable.Field(id=6)
  private final zzo zzhn;
  @SafeParcelable.Field(id=7)
  private final zzb zzho;
  @SafeParcelable.Field(id=9)
  private final zzv zzhp;
  @SafeParcelable.Field(id=10)
  private final zzr zzhq;
  
  @SafeParcelable.Constructor
  zzfj(@SafeParcelable.Param(id=2) int paramInt, @SafeParcelable.Param(id=3) ChangeEvent paramChangeEvent, @SafeParcelable.Param(id=5) CompletionEvent paramCompletionEvent, @SafeParcelable.Param(id=6) zzo paramzzo, @SafeParcelable.Param(id=7) zzb paramzzb, @SafeParcelable.Param(id=9) zzv paramzzv, @SafeParcelable.Param(id=10) zzr paramzzr)
  {
    this.zzcy = paramInt;
    this.zzhl = paramChangeEvent;
    this.zzhm = paramCompletionEvent;
    this.zzhn = paramzzo;
    this.zzho = paramzzb;
    this.zzhp = paramzzv;
    this.zzhq = paramzzr;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 2, this.zzcy);
    SafeParcelWriter.writeParcelable(paramParcel, 3, this.zzhl, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 5, this.zzhm, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 6, this.zzhn, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 7, this.zzho, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 9, this.zzhp, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 10, this.zzhq, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final DriveEvent zzak()
  {
    switch (this.zzcy)
    {
    case 5: 
    case 6: 
    default: 
      int i = this.zzcy;
      throw new IllegalStateException(33 + "Unexpected event type " + i);
    case 1: 
      return this.zzhl;
    case 2: 
      return this.zzhm;
    case 3: 
      return this.zzhn;
    case 4: 
      return this.zzho;
    case 7: 
      return this.zzhp;
    }
    return this.zzhq;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzfj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */