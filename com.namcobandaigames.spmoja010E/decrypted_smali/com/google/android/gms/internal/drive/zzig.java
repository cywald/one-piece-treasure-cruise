package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.metadata.SearchableOrderedMetadataField;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.zze;
import java.util.Date;

public final class zzig
  extends zze
  implements SearchableOrderedMetadataField<Date>, SortableMetadataField<Date>
{
  public zzig(String paramString, int paramInt)
  {
    super(paramString, 4100000);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */