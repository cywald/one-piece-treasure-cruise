package com.google.android.gms.internal.drive;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class zzip
{
  private final ByteBuffer zzmv;
  
  private zzip(ByteBuffer paramByteBuffer)
  {
    this.zzmv = paramByteBuffer;
    this.zzmv.order(ByteOrder.LITTLE_ENDIAN);
  }
  
  private zzip(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this(ByteBuffer.wrap(paramArrayOfByte, paramInt1, paramInt2));
  }
  
  private static int zza(CharSequence paramCharSequence)
  {
    int k = 0;
    int n = paramCharSequence.length();
    int j = 0;
    while ((j < n) && (paramCharSequence.charAt(j) < '')) {
      j += 1;
    }
    for (;;)
    {
      int i;
      if (j < n)
      {
        int m = paramCharSequence.charAt(j);
        if (m < 2048)
        {
          i += (127 - m >>> 31);
          j += 1;
        }
        else
        {
          int i2 = paramCharSequence.length();
          if (j < i2)
          {
            int i3 = paramCharSequence.charAt(j);
            if (i3 < 2048)
            {
              k += (127 - i3 >>> 31);
              m = j;
            }
            for (;;)
            {
              j = m + 1;
              break;
              int i1 = k + 2;
              m = j;
              k = i1;
              if (55296 <= i3)
              {
                m = j;
                k = i1;
                if (i3 <= 57343)
                {
                  if (Character.codePointAt(paramCharSequence, j) < 65536) {
                    throw new IllegalArgumentException(39 + "Unpaired surrogate at index " + j);
                  }
                  m = j + 1;
                  k = i1;
                }
              }
            }
          }
          i += k;
        }
      }
      else
      {
        for (;;)
        {
          if (i < n)
          {
            long l = i;
            throw new IllegalArgumentException(54 + "UTF-8 length does not fit in int: " + (l + 4294967296L));
          }
          return i;
        }
        i = n;
      }
    }
  }
  
  private final void zza(long paramLong)
    throws IOException
  {
    for (;;)
    {
      if ((0xFFFFFFFFFFFFFF80 & paramLong) == 0L)
      {
        zzn((int)paramLong);
        return;
      }
      zzn((int)paramLong & 0x7F | 0x80);
      paramLong >>>= 7;
    }
  }
  
  private static void zza(CharSequence paramCharSequence, ByteBuffer paramByteBuffer)
  {
    int n = 0;
    int m = 0;
    if (paramByteBuffer.isReadOnly()) {
      throw new ReadOnlyBufferException();
    }
    if (paramByteBuffer.hasArray()) {}
    for (;;)
    {
      byte[] arrayOfByte;
      int i2;
      int i3;
      try
      {
        arrayOfByte = paramByteBuffer.array();
        n = paramByteBuffer.arrayOffset() + paramByteBuffer.position();
        i1 = paramByteBuffer.remaining();
        i2 = paramCharSequence.length();
        i3 = n + i1;
        if ((m >= i2) || (m + n >= i3)) {
          break label915;
        }
        i1 = paramCharSequence.charAt(m);
        if (i1 >= 128) {
          break label915;
        }
        arrayOfByte[(n + m)] = ((byte)i1);
        m += 1;
        continue;
        paramByteBuffer.position(m - paramByteBuffer.arrayOffset());
        return;
      }
      catch (ArrayIndexOutOfBoundsException paramCharSequence)
      {
        paramByteBuffer = new BufferOverflowException();
        paramByteBuffer.initCause(paramCharSequence);
        throw paramByteBuffer;
      }
      char c;
      if (m < i2)
      {
        int j = paramCharSequence.charAt(m);
        if ((j < 128) && (n < i3))
        {
          i1 = n + 1;
          arrayOfByte[n] = ((byte)j);
          n = m;
          m = i1;
        }
        else
        {
          int i4;
          if ((j < 2048) && (n <= i3 - 2))
          {
            i4 = n + 1;
            arrayOfByte[n] = ((byte)(j >>> 6 | 0x3C0));
            i1 = i4 + 1;
            arrayOfByte[i4] = ((byte)(j & 0x3F | 0x80));
            n = m;
            m = i1;
          }
          else if (((j < 55296) || (57343 < j)) && (n <= i3 - 3))
          {
            i1 = n + 1;
            int i = (byte)(j >>> 12 | 0x1E0);
            arrayOfByte[n] = i;
            n = i1 + 1;
            arrayOfByte[i1] = ((byte)(j >>> 6 & 0x3F | 0x80));
            i1 = n + 1;
            arrayOfByte[n] = ((byte)(j & 0x3F | 0x80));
            n = m;
            m = i1;
          }
          else if (n <= i3 - 4)
          {
            i1 = m;
            if (m + 1 != paramCharSequence.length())
            {
              m += 1;
              c = paramCharSequence.charAt(m);
              if (!Character.isSurrogatePair(j, c)) {
                i1 = m;
              }
            }
            else
            {
              throw new IllegalArgumentException(39 + "Unpaired surrogate at index " + (i1 - 1));
            }
            i4 = Character.toCodePoint(j, c);
            i1 = n + 1;
            arrayOfByte[n] = ((byte)(i4 >>> 18 | 0xF0));
            n = i1 + 1;
            arrayOfByte[i1] = ((byte)(i4 >>> 12 & 0x3F | 0x80));
            int i5 = n + 1;
            arrayOfByte[n] = ((byte)(i4 >>> 6 & 0x3F | 0x80));
            i1 = i5 + 1;
            arrayOfByte[i5] = ((byte)(i4 & 0x3F | 0x80));
            n = m;
            m = i1;
          }
          else
          {
            throw new ArrayIndexOutOfBoundsException(37 + "Failed writing " + j + " at index " + n);
          }
        }
      }
      else
      {
        m = n;
        continue;
        i1 = paramCharSequence.length();
        m = n;
        if (m < i1)
        {
          int k = paramCharSequence.charAt(m);
          if (k < 128) {
            paramByteBuffer.put((byte)k);
          }
          for (;;)
          {
            m += 1;
            break;
            if (k < 2048)
            {
              paramByteBuffer.put((byte)(k >>> 6 | 0x3C0));
              paramByteBuffer.put((byte)(k & 0x3F | 0x80));
            }
            else if ((k < 55296) || (57343 < k))
            {
              paramByteBuffer.put((byte)(k >>> 12 | 0x1E0));
              paramByteBuffer.put((byte)(k >>> 6 & 0x3F | 0x80));
              paramByteBuffer.put((byte)(k & 0x3F | 0x80));
            }
            else
            {
              n = m;
              if (m + 1 != paramCharSequence.length())
              {
                m += 1;
                c = paramCharSequence.charAt(m);
                if (!Character.isSurrogatePair(k, c)) {
                  n = m;
                }
              }
              else
              {
                throw new IllegalArgumentException(39 + "Unpaired surrogate at index " + (n - 1));
              }
              n = Character.toCodePoint(k, c);
              paramByteBuffer.put((byte)(n >>> 18 | 0xF0));
              paramByteBuffer.put((byte)(n >>> 12 & 0x3F | 0x80));
              paramByteBuffer.put((byte)(n >>> 6 & 0x3F | 0x80));
              paramByteBuffer.put((byte)(n & 0x3F | 0x80));
            }
          }
          label915:
          if (m == i2) {
            m = n + i2;
          }
        }
        else
        {
          return;
        }
        n += m;
        continue;
      }
      int i1 = n + 1;
      n = m;
      m = i1;
    }
  }
  
  public static int zzb(int paramInt, long paramLong)
  {
    int i = zzo(paramInt);
    paramLong = zzb(paramLong);
    if ((0xFFFFFFFFFFFFFF80 & paramLong) == 0L) {
      paramInt = 1;
    }
    for (;;)
    {
      return paramInt + i;
      if ((0xFFFFFFFFFFFFC000 & paramLong) == 0L) {
        paramInt = 2;
      } else if ((0xFFFFFFFFFFE00000 & paramLong) == 0L) {
        paramInt = 3;
      } else if ((0xFFFFFFFFF0000000 & paramLong) == 0L) {
        paramInt = 4;
      } else if ((0xFFFFFFF800000000 & paramLong) == 0L) {
        paramInt = 5;
      } else if ((0xFFFFFC0000000000 & paramLong) == 0L) {
        paramInt = 6;
      } else if ((0xFFFE000000000000 & paramLong) == 0L) {
        paramInt = 7;
      } else if ((0xFF00000000000000 & paramLong) == 0L) {
        paramInt = 8;
      } else if ((paramLong & 0x8000000000000000) == 0L) {
        paramInt = 9;
      } else {
        paramInt = 10;
      }
    }
  }
  
  private static long zzb(long paramLong)
  {
    return paramLong << 1 ^ paramLong >> 63;
  }
  
  public static zzip zzb(byte[] paramArrayOfByte)
  {
    return zzb(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public static zzip zzb(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return new zzip(paramArrayOfByte, 0, paramInt2);
  }
  
  public static int zzc(int paramInt1, int paramInt2)
  {
    return zzo(paramInt1) + zzm(paramInt2);
  }
  
  public static int zzi(String paramString)
  {
    int i = zza(paramString);
    return i + zzq(i);
  }
  
  public static int zzm(int paramInt)
  {
    if (paramInt >= 0) {
      return zzq(paramInt);
    }
    return 10;
  }
  
  private final void zzn(int paramInt)
    throws IOException
  {
    byte b = (byte)paramInt;
    if (!this.zzmv.hasRemaining()) {
      throw new zziq(this.zzmv.position(), this.zzmv.limit());
    }
    this.zzmv.put(b);
  }
  
  public static int zzo(int paramInt)
  {
    return zzq(paramInt << 3);
  }
  
  public static int zzq(int paramInt)
  {
    if ((paramInt & 0xFFFFFF80) == 0) {
      return 1;
    }
    if ((paramInt & 0xC000) == 0) {
      return 2;
    }
    if ((0xFFE00000 & paramInt) == 0) {
      return 3;
    }
    if ((0xF0000000 & paramInt) == 0) {
      return 4;
    }
    return 5;
  }
  
  public final void zza(int paramInt, long paramLong)
    throws IOException
  {
    zzd(paramInt, 0);
    zza(zzb(paramLong));
  }
  
  public final void zzb(int paramInt1, int paramInt2)
    throws IOException
  {
    zzd(paramInt1, 0);
    if (paramInt2 >= 0)
    {
      zzp(paramInt2);
      return;
    }
    zza(paramInt2);
  }
  
  public final void zzbh()
  {
    if (this.zzmv.remaining() != 0) {
      throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", new Object[] { Integer.valueOf(this.zzmv.remaining()) }));
    }
  }
  
  public final void zzc(byte[] paramArrayOfByte)
    throws IOException
  {
    int i = paramArrayOfByte.length;
    if (this.zzmv.remaining() >= i)
    {
      this.zzmv.put(paramArrayOfByte, 0, i);
      return;
    }
    throw new zziq(this.zzmv.position(), this.zzmv.limit());
  }
  
  public final void zzd(int paramInt1, int paramInt2)
    throws IOException
  {
    zzp(paramInt1 << 3 | paramInt2);
  }
  
  public final void zzh(String paramString)
    throws IOException
  {
    int i;
    int j;
    try
    {
      i = zzq(paramString.length());
      if (i != zzq(paramString.length() * 3)) {
        break label150;
      }
      j = this.zzmv.position();
      if (this.zzmv.remaining() < i) {
        throw new zziq(i + j, this.zzmv.limit());
      }
    }
    catch (BufferOverflowException paramString)
    {
      zziq localzziq = new zziq(this.zzmv.position(), this.zzmv.limit());
      localzziq.initCause(paramString);
      throw localzziq;
    }
    this.zzmv.position(j + i);
    zza(paramString, this.zzmv);
    int k = this.zzmv.position();
    this.zzmv.position(j);
    zzp(k - j - i);
    this.zzmv.position(k);
    return;
    label150:
    zzp(zza(paramString));
    zza(paramString, this.zzmv);
  }
  
  public final void zzp(int paramInt)
    throws IOException
  {
    for (;;)
    {
      if ((paramInt & 0xFFFFFF80) == 0)
      {
        zzn(paramInt);
        return;
      }
      zzn(paramInt & 0x7F | 0x80);
      paramInt >>>= 7;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */