package com.google.android.gms.internal.drive;

import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zzeu
  extends IInterface
{
  public abstract void zza(boolean paramBoolean)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzeu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */