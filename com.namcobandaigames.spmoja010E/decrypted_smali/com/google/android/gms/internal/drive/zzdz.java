package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveResource.MetadataResult;
import com.google.android.gms.drive.Metadata;

final class zzdz
  implements DriveResource.MetadataResult
{
  private final Status zzdw;
  private final Metadata zzgp;
  
  public zzdz(Status paramStatus, Metadata paramMetadata)
  {
    this.zzdw = paramStatus;
    this.zzgp = paramMetadata;
  }
  
  public final Metadata getMetadata()
  {
    return this.zzgp;
  }
  
  public final Status getStatus()
  {
    return this.zzdw;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzdz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */