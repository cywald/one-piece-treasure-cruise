package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.DriveId;
import java.util.List;

@SafeParcelable.Class(creator="SetResourceParentsRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzgq
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzgq> CREATOR = new zzgr();
  @SafeParcelable.Field(id=2)
  private final DriveId zzic;
  @SafeParcelable.Field(id=3)
  private final List<DriveId> zzid;
  
  @SafeParcelable.Constructor
  @VisibleForTesting
  public zzgq(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) List<DriveId> paramList)
  {
    this.zzic = paramDriveId;
    this.zzid = paramList;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzic, paramInt, false);
    SafeParcelWriter.writeTypedList(paramParcel, 3, this.zzid, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzgq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */