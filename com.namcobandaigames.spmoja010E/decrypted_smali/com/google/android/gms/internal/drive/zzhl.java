package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.TaskUtil;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzhl
  extends zzhb<Void>
{
  public zzhl(TaskCompletionSource<Void> paramTaskCompletionSource)
  {
    super(paramTaskCompletionSource);
  }
  
  public final void onSuccess()
    throws RemoteException
  {
    TaskUtil.setResultOrApiException(Status.RESULT_SUCCESS, zzap());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */