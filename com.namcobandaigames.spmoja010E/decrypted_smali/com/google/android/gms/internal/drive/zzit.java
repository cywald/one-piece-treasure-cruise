package com.google.android.gms.internal.drive;

public final class zzit
  implements Cloneable
{
  private static final zziu zzmy = new zziu();
  private int mSize;
  private boolean zzmz = false;
  private int[] zzna;
  private zziu[] zznb;
  
  zzit()
  {
    this(10);
  }
  
  private zzit(int paramInt)
  {
    paramInt = idealIntArraySize(paramInt);
    this.zzna = new int[paramInt];
    this.zznb = new zziu[paramInt];
    this.mSize = 0;
  }
  
  private static int idealIntArraySize(int paramInt)
  {
    int j = paramInt << 2;
    paramInt = 4;
    for (;;)
    {
      int i = j;
      if (paramInt < 32)
      {
        if (j <= (1 << paramInt) - 12) {
          i = (1 << paramInt) - 12;
        }
      }
      else {
        return i / 4;
      }
      paramInt += 1;
    }
  }
  
  private final int zzt(int paramInt)
  {
    int j = this.mSize;
    int i = 0;
    j -= 1;
    while (i <= j)
    {
      int k = i + j >>> 1;
      int m = this.zzna[k];
      if (m < paramInt)
      {
        i = k + 1;
      }
      else
      {
        j = k;
        if (m <= paramInt) {
          return j;
        }
        j = k - 1;
      }
    }
    j = i ^ 0xFFFFFFFF;
    return j;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    label71:
    label93:
    label131:
    label138:
    label141:
    for (;;)
    {
      return true;
      if (!(paramObject instanceof zzit)) {
        return false;
      }
      paramObject = (zzit)paramObject;
      if (this.mSize != ((zzit)paramObject).mSize) {
        return false;
      }
      Object localObject = this.zzna;
      int[] arrayOfInt = ((zzit)paramObject).zzna;
      int j = this.mSize;
      int i = 0;
      if (i < j) {
        if (localObject[i] != arrayOfInt[i])
        {
          i = 0;
          if (i != 0)
          {
            localObject = this.zznb;
            paramObject = ((zzit)paramObject).zznb;
            j = this.mSize;
            i = 0;
            if (i >= j) {
              break label138;
            }
            if (localObject[i].equals(paramObject[i])) {
              break label131;
            }
          }
        }
      }
      for (i = 0;; i = 1)
      {
        if (i != 0) {
          break label141;
        }
        return false;
        i += 1;
        break;
        i = 1;
        break label71;
        i += 1;
        break label93;
      }
    }
  }
  
  public final int hashCode()
  {
    int j = 17;
    int i = 0;
    while (i < this.mSize)
    {
      j = (j * 31 + this.zzna[i]) * 31 + this.zznb[i].hashCode();
      i += 1;
    }
    return j;
  }
  
  public final boolean isEmpty()
  {
    return this.mSize == 0;
  }
  
  final int size()
  {
    return this.mSize;
  }
  
  final void zza(int paramInt, zziu paramzziu)
  {
    int i = zzt(paramInt);
    if (i >= 0)
    {
      this.zznb[i] = paramzziu;
      return;
    }
    i ^= 0xFFFFFFFF;
    if ((i < this.mSize) && (this.zznb[i] == zzmy))
    {
      this.zzna[i] = paramInt;
      this.zznb[i] = paramzziu;
      return;
    }
    if (this.mSize >= this.zzna.length)
    {
      int j = idealIntArraySize(this.mSize + 1);
      int[] arrayOfInt = new int[j];
      zziu[] arrayOfzziu = new zziu[j];
      System.arraycopy(this.zzna, 0, arrayOfInt, 0, this.zzna.length);
      System.arraycopy(this.zznb, 0, arrayOfzziu, 0, this.zznb.length);
      this.zzna = arrayOfInt;
      this.zznb = arrayOfzziu;
    }
    if (this.mSize - i != 0)
    {
      System.arraycopy(this.zzna, i, this.zzna, i + 1, this.mSize - i);
      System.arraycopy(this.zznb, i, this.zznb, i + 1, this.mSize - i);
    }
    this.zzna[i] = paramInt;
    this.zznb[i] = paramzziu;
    this.mSize += 1;
  }
  
  final zziu zzr(int paramInt)
  {
    paramInt = zzt(paramInt);
    if ((paramInt < 0) || (this.zznb[paramInt] == zzmy)) {
      return null;
    }
    return this.zznb[paramInt];
  }
  
  final zziu zzs(int paramInt)
  {
    return this.zznb[paramInt];
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzit.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */