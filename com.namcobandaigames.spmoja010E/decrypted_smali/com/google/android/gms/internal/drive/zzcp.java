package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.drive.DriveResource;

final class zzcp
  extends RegisterListenerMethod<zzaw, zzdi>
{
  zzcp(zzch paramzzch, ListenerHolder paramListenerHolder, DriveResource paramDriveResource, zzdi paramzzdi)
  {
    super(paramListenerHolder);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzcp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */