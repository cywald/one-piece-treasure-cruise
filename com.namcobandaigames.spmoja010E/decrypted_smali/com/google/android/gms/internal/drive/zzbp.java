package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.drive.DriveFile.DownloadProgressListener;

final class zzbp
  implements DriveFile.DownloadProgressListener
{
  private final ListenerHolder<DriveFile.DownloadProgressListener> zzey;
  
  public zzbp(ListenerHolder<DriveFile.DownloadProgressListener> paramListenerHolder)
  {
    this.zzey = paramListenerHolder;
  }
  
  public final void onProgress(long paramLong1, long paramLong2)
  {
    this.zzey.notifyListener(new zzbq(this, paramLong1, paramLong2));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzbp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */