package com.google.android.gms.internal.drive;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;
import com.google.android.gms.drive.DriveFolder.DriveFolderResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.ExecutionOptions.Builder;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.zzk;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.Query.Builder;
import com.google.android.gms.drive.query.SearchableField;

public final class zzbs
  extends zzdp
  implements DriveFolder
{
  public zzbs(DriveId paramDriveId)
  {
    super(paramDriveId);
  }
  
  static int zza(@Nullable DriveContents paramDriveContents, @Nullable zzk paramzzk)
  {
    if (paramDriveContents == null)
    {
      if ((paramzzk != null) && (paramzzk.zzaz())) {
        return 0;
      }
      return 1;
    }
    int i = paramDriveContents.zzh().getRequestId();
    paramDriveContents.zzi();
    return i;
  }
  
  static Query zza(@Nullable Query paramQuery, @NonNull DriveId paramDriveId)
  {
    paramDriveId = new Query.Builder().addFilter(Filters.in(SearchableField.PARENTS, paramDriveId));
    if (paramQuery != null)
    {
      if (paramQuery.getFilter() != null) {
        paramDriveId.addFilter(paramQuery.getFilter());
      }
      paramDriveId.setPageToken(paramQuery.getPageToken());
      paramDriveId.setSortOrder(paramQuery.getSortOrder());
    }
    return paramDriveId.build();
  }
  
  static void zzb(MetadataChangeSet paramMetadataChangeSet)
  {
    if (paramMetadataChangeSet == null) {
      throw new IllegalArgumentException("MetadataChangeSet must be provided.");
    }
    paramMetadataChangeSet = zzk.zze(paramMetadataChangeSet.getMimeType());
    if (paramMetadataChangeSet != null)
    {
      if ((!paramMetadataChangeSet.zzaz()) && (!paramMetadataChangeSet.isFolder())) {}
      for (int i = 1; i == 0; i = 0) {
        throw new IllegalArgumentException("May not create shortcut files using this method. Use DriveFolder.createShortcutFile() instead.");
      }
    }
  }
  
  public final PendingResult<DriveFolder.DriveFileResult> createFile(GoogleApiClient paramGoogleApiClient, MetadataChangeSet paramMetadataChangeSet, @Nullable DriveContents paramDriveContents)
  {
    return createFile(paramGoogleApiClient, paramMetadataChangeSet, paramDriveContents, null);
  }
  
  public final PendingResult<DriveFolder.DriveFileResult> createFile(GoogleApiClient paramGoogleApiClient, MetadataChangeSet paramMetadataChangeSet, @Nullable DriveContents paramDriveContents, @Nullable ExecutionOptions paramExecutionOptions)
  {
    if (paramExecutionOptions == null) {
      paramExecutionOptions = new ExecutionOptions.Builder().build();
    }
    for (;;)
    {
      if (paramExecutionOptions.zzm() != 0) {
        throw new IllegalStateException("May not set a conflict strategy for new file creation.");
      }
      if (paramMetadataChangeSet == null) {
        throw new IllegalArgumentException("MetadataChangeSet must be provided.");
      }
      zzk localzzk = zzk.zze(paramMetadataChangeSet.getMimeType());
      if ((localzzk != null) && (localzzk.isFolder())) {
        throw new IllegalArgumentException("May not create folders using this method. Use DriveFolder.createFolder() instead of mime type application/vnd.google-apps.folder");
      }
      if (paramExecutionOptions == null) {
        throw new IllegalArgumentException("ExecutionOptions must be provided");
      }
      paramExecutionOptions.zza(paramGoogleApiClient);
      if (paramDriveContents != null)
      {
        if (!(paramDriveContents instanceof zzbi)) {
          throw new IllegalArgumentException("Only DriveContents obtained from the Drive API are accepted.");
        }
        if (paramDriveContents.getDriveId() != null) {
          throw new IllegalArgumentException("Only DriveContents obtained through DriveApi.newDriveContents are accepted for file creation.");
        }
        if (paramDriveContents.zzj()) {
          throw new IllegalArgumentException("DriveContents are already closed.");
        }
      }
      zzb(paramMetadataChangeSet);
      int j = zza(paramDriveContents, zzk.zze(paramMetadataChangeSet.getMimeType()));
      paramDriveContents = zzk.zze(paramMetadataChangeSet.getMimeType());
      if ((paramDriveContents != null) && (paramDriveContents.zzaz())) {}
      for (int i = 1;; i = 0) {
        return paramGoogleApiClient.execute(new zzbt(this, paramGoogleApiClient, paramMetadataChangeSet, j, i, paramExecutionOptions));
      }
    }
  }
  
  public final PendingResult<DriveFolder.DriveFolderResult> createFolder(GoogleApiClient paramGoogleApiClient, MetadataChangeSet paramMetadataChangeSet)
  {
    if (paramMetadataChangeSet == null) {
      throw new IllegalArgumentException("MetadataChangeSet must be provided.");
    }
    if ((paramMetadataChangeSet.getMimeType() != null) && (!paramMetadataChangeSet.getMimeType().equals("application/vnd.google-apps.folder"))) {
      throw new IllegalArgumentException("The mimetype must be of type application/vnd.google-apps.folder");
    }
    return paramGoogleApiClient.execute(new zzbu(this, paramGoogleApiClient, paramMetadataChangeSet));
  }
  
  public final PendingResult<DriveApi.MetadataBufferResult> listChildren(GoogleApiClient paramGoogleApiClient)
  {
    return queryChildren(paramGoogleApiClient, null);
  }
  
  public final PendingResult<DriveApi.MetadataBufferResult> queryChildren(GoogleApiClient paramGoogleApiClient, Query paramQuery)
  {
    return new zzaf().query(paramGoogleApiClient, zza(paramQuery, getDriveId()));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzbs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */