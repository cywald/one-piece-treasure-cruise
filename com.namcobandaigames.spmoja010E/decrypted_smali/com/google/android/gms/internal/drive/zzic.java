package com.google.android.gms.internal.drive;

public final class zzic
{
  public static final zzid zzkn = new zzid("created", 4100000);
  public static final zzie zzko = new zzie("lastOpenedTime", 4300000);
  public static final zzig zzkp = new zzig("modified", 4100000);
  public static final zzif zzkq = new zzif("modifiedByMe", 4100000);
  public static final zzii zzkr = new zzii("sharedWithMe", 4100000);
  public static final zzih zzks = new zzih("recency", 8000000);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzic.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */