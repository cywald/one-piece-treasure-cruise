package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;

final class zzbx
  implements DriveFolder.DriveFileResult
{
  private final Status zzdw;
  private final DriveFile zzfg;
  
  public zzbx(Status paramStatus, DriveFile paramDriveFile)
  {
    this.zzdw = paramStatus;
    this.zzfg = paramDriveFile;
  }
  
  public final DriveFile getDriveFile()
  {
    return this.zzfg;
  }
  
  public final Status getStatus()
  {
    return this.zzdw;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzbx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */