package com.google.android.gms.internal.drive;

import java.io.IOException;

public final class zzhn
  extends zzir<zzhn>
{
  public int versionCode = 1;
  public String zzab = "";
  public long zzac = -1L;
  public int zzad = -1;
  public long zzf = -1L;
  
  public zzhn()
  {
    this.zzmw = null;
    this.zznf = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzhn)) {
        return false;
      }
      paramObject = (zzhn)paramObject;
      if (this.versionCode != ((zzhn)paramObject).versionCode) {
        return false;
      }
      if (this.zzab == null)
      {
        if (((zzhn)paramObject).zzab != null) {
          return false;
        }
      }
      else if (!this.zzab.equals(((zzhn)paramObject).zzab)) {
        return false;
      }
      if (this.zzac != ((zzhn)paramObject).zzac) {
        return false;
      }
      if (this.zzf != ((zzhn)paramObject).zzf) {
        return false;
      }
      if (this.zzad != ((zzhn)paramObject).zzad) {
        return false;
      }
      if ((this.zzmw != null) && (!this.zzmw.isEmpty())) {
        break;
      }
    } while ((((zzhn)paramObject).zzmw == null) || (((zzhn)paramObject).zzmw.isEmpty()));
    return false;
    return this.zzmw.equals(((zzhn)paramObject).zzmw);
  }
  
  public final int hashCode()
  {
    int k = 0;
    int m = getClass().getName().hashCode();
    int n = this.versionCode;
    int i;
    int i1;
    int i2;
    int i3;
    if (this.zzab == null)
    {
      i = 0;
      i1 = (int)(this.zzac ^ this.zzac >>> 32);
      i2 = (int)(this.zzf ^ this.zzf >>> 32);
      i3 = this.zzad;
      j = k;
      if (this.zzmw != null) {
        if (!this.zzmw.isEmpty()) {
          break label138;
        }
      }
    }
    label138:
    for (int j = k;; j = this.zzmw.hashCode())
    {
      return ((((i + ((m + 527) * 31 + n) * 31) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + j;
      i = this.zzab.hashCode();
      break;
    }
  }
  
  public final void zza(zzip paramzzip)
    throws IOException
  {
    paramzzip.zzb(1, this.versionCode);
    String str = this.zzab;
    paramzzip.zzd(2, 2);
    paramzzip.zzh(str);
    paramzzip.zza(3, this.zzac);
    paramzzip.zza(4, this.zzf);
    if (this.zzad != -1) {
      paramzzip.zzb(5, this.zzad);
    }
    super.zza(paramzzip);
  }
  
  protected final int zzaq()
  {
    int i = super.zzaq();
    int j = zzip.zzc(1, this.versionCode);
    String str = this.zzab;
    int k = zzip.zzo(2);
    j = i + j + (zzip.zzi(str) + k) + zzip.zzb(3, this.zzac) + zzip.zzb(4, this.zzf);
    i = j;
    if (this.zzad != -1) {
      i = j + zzip.zzc(5, this.zzad);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */