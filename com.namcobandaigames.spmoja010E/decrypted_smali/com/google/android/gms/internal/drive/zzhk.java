package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzhk
  extends zzhb<Boolean>
{
  public zzhk(TaskCompletionSource<Boolean> paramTaskCompletionSource)
  {
    super(paramTaskCompletionSource);
  }
  
  public final void onSuccess()
    throws RemoteException
  {
    zzap().setResult(Boolean.valueOf(true));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */