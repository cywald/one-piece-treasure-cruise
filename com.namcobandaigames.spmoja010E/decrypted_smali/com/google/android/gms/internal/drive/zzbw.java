package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DriveFolder.DriveFolderResult;

final class zzbw
  extends zzl
{
  private final BaseImplementation.ResultHolder<DriveFolder.DriveFolderResult> zzdv;
  
  public zzbw(BaseImplementation.ResultHolder<DriveFolder.DriveFolderResult> paramResultHolder)
  {
    this.zzdv = paramResultHolder;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzbz(paramStatus, null));
  }
  
  public final void zza(zzfh paramzzfh)
    throws RemoteException
  {
    this.zzdv.setResult(new zzbz(Status.RESULT_SUCCESS, new zzbs(paramzzfh.zzdb)));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzbw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */