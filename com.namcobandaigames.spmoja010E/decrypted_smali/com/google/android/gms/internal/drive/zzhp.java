package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.zzi;
import com.google.android.gms.drive.metadata.internal.zzo;
import com.google.android.gms.drive.metadata.internal.zzs;
import com.google.android.gms.drive.metadata.internal.zzt;
import com.google.android.gms.drive.metadata.internal.zzu;
import java.util.Collections;

public final class zzhp
{
  public static final MetadataField<DriveId> zziv = zzij.zzkt;
  public static final MetadataField<String> zziw = new zzt("alternateLink", 4300000);
  public static final zzhs zzix = new zzhs(5000000);
  public static final MetadataField<String> zziy = new zzt("description", 4300000);
  public static final MetadataField<String> zziz = new zzt("embedLink", 4300000);
  public static final MetadataField<String> zzja = new zzt("fileExtension", 4300000);
  public static final MetadataField<Long> zzjb = new zzi("fileSize", 4300000);
  public static final MetadataField<String> zzjc = new zzt("folderColorRgb", 7500000);
  public static final MetadataField<Boolean> zzjd = new com.google.android.gms.drive.metadata.internal.zzb("hasThumbnail", 4300000);
  public static final MetadataField<String> zzje = new zzt("indexableText", 4300000);
  public static final MetadataField<Boolean> zzjf = new com.google.android.gms.drive.metadata.internal.zzb("isAppData", 4300000);
  public static final MetadataField<Boolean> zzjg = new com.google.android.gms.drive.metadata.internal.zzb("isCopyable", 4300000);
  public static final MetadataField<Boolean> zzjh = new com.google.android.gms.drive.metadata.internal.zzb("isEditable", 4100000);
  public static final MetadataField<Boolean> zzji = new zzhq("isExplicitlyTrashed", Collections.singleton("trashed"), Collections.emptySet(), 7000000);
  public static final MetadataField<Boolean> zzjj = new com.google.android.gms.drive.metadata.internal.zzb("isLocalContentUpToDate", 7800000);
  public static final zzht zzjk = new zzht("isPinned", 4100000);
  public static final MetadataField<Boolean> zzjl = new com.google.android.gms.drive.metadata.internal.zzb("isOpenable", 7200000);
  public static final MetadataField<Boolean> zzjm = new com.google.android.gms.drive.metadata.internal.zzb("isRestricted", 4300000);
  public static final MetadataField<Boolean> zzjn = new com.google.android.gms.drive.metadata.internal.zzb("isShared", 4300000);
  public static final MetadataField<Boolean> zzjo = new com.google.android.gms.drive.metadata.internal.zzb("isGooglePhotosFolder", 7000000);
  public static final MetadataField<Boolean> zzjp = new com.google.android.gms.drive.metadata.internal.zzb("isGooglePhotosRootFolder", 7000000);
  public static final MetadataField<Boolean> zzjq = new com.google.android.gms.drive.metadata.internal.zzb("isTrashable", 4400000);
  public static final MetadataField<Boolean> zzjr = new com.google.android.gms.drive.metadata.internal.zzb("isViewed", 4300000);
  public static final zzhu zzjs = new zzhu(4100000);
  public static final MetadataField<String> zzjt = new zzt("originalFilename", 4300000);
  public static final com.google.android.gms.drive.metadata.zzb<String> zzju = new zzs("ownerNames", 4300000);
  public static final zzu zzjv = new zzu("lastModifyingUser", 6000000);
  public static final zzu zzjw = new zzu("sharingUser", 6000000);
  public static final zzo zzjx = new zzo(4100000);
  public static final zzhv zzjy = new zzhv("quotaBytesUsed", 4300000);
  public static final zzhx zzjz = new zzhx("starred", 4100000);
  public static final MetadataField<BitmapTeleporter> zzka = new zzhr("thumbnail", Collections.emptySet(), Collections.emptySet(), 4400000);
  public static final zzhy zzkb = new zzhy("title", 4100000);
  public static final zzhz zzkc = new zzhz("trashed", 4100000);
  public static final MetadataField<String> zzkd = new zzt("webContentLink", 4300000);
  public static final MetadataField<String> zzke = new zzt("webViewLink", 4300000);
  public static final MetadataField<String> zzkf = new zzt("uniqueIdentifier", 5000000);
  public static final com.google.android.gms.drive.metadata.internal.zzb zzkg = new com.google.android.gms.drive.metadata.internal.zzb("writersCanShare", 6000000);
  public static final MetadataField<String> zzkh = new zzt("role", 6000000);
  public static final MetadataField<String> zzki = new zzt("md5Checksum", 7000000);
  public static final zzhw zzkj = new zzhw(7000000);
  public static final MetadataField<String> zzkk = new zzt("recencyReason", 8000000);
  public static final MetadataField<Boolean> zzkl = new com.google.android.gms.drive.metadata.internal.zzb("subscribed", 8000000);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */