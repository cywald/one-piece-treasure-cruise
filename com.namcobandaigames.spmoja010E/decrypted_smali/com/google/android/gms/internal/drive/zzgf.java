package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveFile.DownloadProgressListener;

final class zzgf
  extends zzl
{
  private final BaseImplementation.ResultHolder<DriveApi.DriveContentsResult> zzdv;
  private final DriveFile.DownloadProgressListener zzia;
  
  zzgf(BaseImplementation.ResultHolder<DriveApi.DriveContentsResult> paramResultHolder, DriveFile.DownloadProgressListener paramDownloadProgressListener)
  {
    this.zzdv = paramResultHolder;
    this.zzia = paramDownloadProgressListener;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzal(paramStatus, null));
  }
  
  public final void zza(zzfb paramzzfb)
    throws RemoteException
  {
    if (paramzzfb.zzhf) {}
    for (Status localStatus = new Status(-1);; localStatus = Status.RESULT_SUCCESS)
    {
      this.zzdv.setResult(new zzal(localStatus, new zzbi(paramzzfb.zzeq)));
      return;
    }
  }
  
  public final void zza(zzff paramzzff)
    throws RemoteException
  {
    if (this.zzia != null) {
      this.zzia.onProgress(paramzzff.zzhi, paramzzff.zzhj);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzgf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */