package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.zza;

public class zzl
  extends zzer
{
  public void onSuccess()
    throws RemoteException
  {}
  
  public void zza(Status paramStatus)
    throws RemoteException
  {}
  
  public final void zza(zza paramzza)
    throws RemoteException
  {}
  
  public final void zza(zzem paramzzem)
    throws RemoteException
  {}
  
  public final void zza(zzez paramzzez)
    throws RemoteException
  {}
  
  public void zza(zzfb paramzzfb)
    throws RemoteException
  {}
  
  public void zza(zzfd paramzzfd)
    throws RemoteException
  {}
  
  public void zza(zzff paramzzff)
    throws RemoteException
  {}
  
  public void zza(zzfh paramzzfh)
    throws RemoteException
  {}
  
  public final void zza(zzfl paramzzfl)
    throws RemoteException
  {}
  
  public void zza(zzfn paramzzfn)
    throws RemoteException
  {}
  
  public void zza(zzfp paramzzfp)
    throws RemoteException
  {}
  
  public final void zza(zzfr paramzzfr, zzil paramzzil)
    throws RemoteException
  {}
  
  public void zza(zzfs paramzzfs)
    throws RemoteException
  {}
  
  public void zza(zzfu paramzzfu)
    throws RemoteException
  {}
  
  public final void zza(zzfx paramzzfx)
    throws RemoteException
  {}
  
  public final void zza(zzfz paramzzfz)
    throws RemoteException
  {}
  
  public final void zza(zzgb paramzzgb)
    throws RemoteException
  {}
  
  public final void zza(zzgt paramzzgt)
    throws RemoteException
  {}
  
  public final void zzb(boolean paramBoolean)
    throws RemoteException
  {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */