package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.zze;
import java.util.Date;

public final class zzih
  extends zze
  implements SortableMetadataField<Date>
{
  public zzih(String paramString, int paramInt)
  {
    super(paramString, 8000000);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzih.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */