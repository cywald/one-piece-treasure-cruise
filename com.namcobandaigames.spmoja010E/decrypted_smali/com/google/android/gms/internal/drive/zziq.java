package com.google.android.gms.internal.drive;

import java.io.IOException;

public final class zziq
  extends IOException
{
  zziq(int paramInt1, int paramInt2)
  {
    super(108 + "CodedOutputStream was writing to a flat byte array and ran out of space (pos " + paramInt1 + " limit " + paramInt2 + ").");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zziq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */