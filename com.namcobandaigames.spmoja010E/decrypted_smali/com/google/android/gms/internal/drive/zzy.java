package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

@SafeParcelable.Class(creator="CreateFolderRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzy
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzy> CREATOR = new zzz();
  @SafeParcelable.Field(id=3)
  private final MetadataBundle zzdl;
  @SafeParcelable.Field(id=2)
  private final DriveId zzdn;
  
  @SafeParcelable.Constructor
  @VisibleForTesting
  public zzy(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) MetadataBundle paramMetadataBundle)
  {
    this.zzdn = ((DriveId)Preconditions.checkNotNull(paramDriveId));
    this.zzdl = ((MetadataBundle)Preconditions.checkNotNull(paramMetadataBundle));
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzdn, paramInt, false);
    SafeParcelWriter.writeParcelable(paramParcel, 3, this.zzdl, paramInt, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */