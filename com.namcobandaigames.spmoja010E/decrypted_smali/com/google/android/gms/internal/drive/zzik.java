package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.zzb;
import com.google.android.gms.drive.metadata.internal.zzh;

public final class zzik
{
  public static final MetadataField<Integer> zzku = new zzh("contentAvailability", 4300000);
  public static final MetadataField<Boolean> zzkv = new zzb("isPinnable", 4300000);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzik.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */