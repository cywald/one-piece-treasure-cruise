package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.DriveId;

@SafeParcelable.Class(creator="TransferProgressDataCreator")
@SafeParcelable.Reserved({1})
public final class zzh
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzh> CREATOR = new zzi();
  @SafeParcelable.Field(id=4)
  final int status;
  @SafeParcelable.Field(id=2)
  final int zzcr;
  @SafeParcelable.Field(id=5)
  final long zzcu;
  @SafeParcelable.Field(id=6)
  final long zzcv;
  @SafeParcelable.Field(id=3)
  final DriveId zzk;
  
  @SafeParcelable.Constructor
  public zzh(@SafeParcelable.Param(id=2) int paramInt1, @SafeParcelable.Param(id=3) DriveId paramDriveId, @SafeParcelable.Param(id=4) int paramInt2, @SafeParcelable.Param(id=5) long paramLong1, @SafeParcelable.Param(id=6) long paramLong2)
  {
    this.zzcr = paramInt1;
    this.zzk = paramDriveId;
    this.status = paramInt2;
    this.zzcu = paramLong1;
    this.zzcv = paramLong2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zzh)paramObject;
      if ((this.zzcr != ((zzh)paramObject).zzcr) || (!Objects.equal(this.zzk, ((zzh)paramObject).zzk)) || (this.status != ((zzh)paramObject).status) || (this.zzcu != ((zzh)paramObject).zzcu)) {
        break;
      }
      bool1 = bool2;
    } while (this.zzcv == ((zzh)paramObject).zzcv);
    return false;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(this.zzcr), this.zzk, Integer.valueOf(this.status), Long.valueOf(this.zzcu), Long.valueOf(this.zzcv) });
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 2, this.zzcr);
    SafeParcelWriter.writeParcelable(paramParcel, 3, this.zzk, paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 4, this.status);
    SafeParcelWriter.writeLong(paramParcel, 5, this.zzcu);
    SafeParcelWriter.writeLong(paramParcel, 6, this.zzcv);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */