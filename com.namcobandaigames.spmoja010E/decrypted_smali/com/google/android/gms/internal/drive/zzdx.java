package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.MetadataBuffer;

final class zzdx
  extends zzl
{
  private final BaseImplementation.ResultHolder<DriveApi.MetadataBufferResult> zzdv;
  
  public zzdx(BaseImplementation.ResultHolder<DriveApi.MetadataBufferResult> paramResultHolder)
  {
    this.zzdv = paramResultHolder;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzdv.setResult(new zzaq(paramStatus, null, false));
  }
  
  public final void zza(zzfp paramzzfp)
    throws RemoteException
  {
    paramzzfp = new MetadataBuffer(paramzzfp.zzht);
    this.zzdv.setResult(new zzaq(Status.RESULT_SUCCESS, paramzzfp, false));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzdx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */