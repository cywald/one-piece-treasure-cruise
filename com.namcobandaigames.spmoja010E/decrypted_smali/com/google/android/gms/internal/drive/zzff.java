package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.zzh;
import java.util.Collections;
import java.util.List;

@SafeParcelable.Class(creator="OnDownloadProgressResponseCreator")
@SafeParcelable.Reserved({1})
public final class zzff
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzff> CREATOR = new zzfg();
  private static final List<zzh> zzhh = ;
  @SafeParcelable.Field(id=4)
  private final int status;
  @SafeParcelable.Field(id=2)
  final long zzhi;
  @SafeParcelable.Field(id=3)
  final long zzhj;
  @Nullable
  @SafeParcelable.Field(id=5)
  private final List<zzh> zzhk;
  
  @SafeParcelable.Constructor
  public zzff(@SafeParcelable.Param(id=2) long paramLong1, @SafeParcelable.Param(id=3) long paramLong2, @SafeParcelable.Param(id=4) int paramInt, @SafeParcelable.Param(id=5) List<zzh> paramList)
  {
    this.zzhi = paramLong1;
    this.zzhj = paramLong2;
    this.status = paramInt;
    this.zzhk = paramList;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeLong(paramParcel, 2, this.zzhi);
    SafeParcelWriter.writeLong(paramParcel, 3, this.zzhj);
    SafeParcelWriter.writeInt(paramParcel, 4, this.status);
    SafeParcelWriter.writeTypedList(paramParcel, 5, this.zzhk, false);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzff.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */