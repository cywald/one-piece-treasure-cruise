package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.zza;
import com.google.android.gms.drive.zzu;
import java.util.List;

@SafeParcelable.Class(creator="OnChangesResponseCreator")
@SafeParcelable.Reserved({1})
public final class zzez
  extends zzu
{
  public static final Parcelable.Creator<zzez> CREATOR = new zzfa();
  @SafeParcelable.Field(id=2)
  private final DataHolder zzhb;
  @SafeParcelable.Field(id=3)
  private final List<DriveId> zzhc;
  @SafeParcelable.Field(id=4)
  private final zza zzhd;
  @SafeParcelable.Field(id=5)
  private final boolean zzhe;
  
  @SafeParcelable.Constructor
  public zzez(@SafeParcelable.Param(id=2) DataHolder paramDataHolder, @SafeParcelable.Param(id=3) List<DriveId> paramList, @SafeParcelable.Param(id=4) zza paramzza, @SafeParcelable.Param(id=5) boolean paramBoolean)
  {
    this.zzhb = paramDataHolder;
    this.zzhc = paramList;
    this.zzhd = paramzza;
    this.zzhe = paramBoolean;
  }
  
  protected final void zza(Parcel paramParcel, int paramInt)
  {
    paramInt |= 0x1;
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzhb, paramInt, false);
    SafeParcelWriter.writeTypedList(paramParcel, 3, this.zzhc, false);
    SafeParcelWriter.writeParcelable(paramParcel, 4, this.zzhd, paramInt, false);
    SafeParcelWriter.writeBoolean(paramParcel, 5, this.zzhe);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzez.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */