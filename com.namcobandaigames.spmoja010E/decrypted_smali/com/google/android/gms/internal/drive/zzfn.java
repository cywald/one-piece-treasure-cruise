package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.zzu;

@SafeParcelable.Class(creator="OnListEntriesResponseCreator")
@SafeParcelable.Reserved({1})
public final class zzfn
  extends zzu
{
  public static final Parcelable.Creator<zzfn> CREATOR = new zzfo();
  @SafeParcelable.Field(id=3)
  final boolean zzdy;
  @SafeParcelable.Field(id=2)
  final DataHolder zzhs;
  
  @SafeParcelable.Constructor
  public zzfn(@SafeParcelable.Param(id=2) DataHolder paramDataHolder, @SafeParcelable.Param(id=3) boolean paramBoolean)
  {
    this.zzhs = paramDataHolder;
    this.zzdy = paramBoolean;
  }
  
  protected final void zza(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzhs, paramInt, false);
    SafeParcelWriter.writeBoolean(paramParcel, 3, this.zzdy);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
  
  public final DataHolder zzal()
  {
    return this.zzhs;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzfn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */