package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.internal.zzg;

final class zzib
  implements zzg
{
  public final String zzav()
  {
    return "customPropertiesExtraHolder";
  }
  
  public final void zzb(DataHolder paramDataHolder)
  {
    zzia.zzd(paramDataHolder);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzib.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */