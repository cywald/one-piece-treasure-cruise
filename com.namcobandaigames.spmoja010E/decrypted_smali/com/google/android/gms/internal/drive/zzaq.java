package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.MetadataBuffer;

public final class zzaq
  implements DriveApi.MetadataBufferResult
{
  private final Status zzdw;
  private final MetadataBuffer zzdx;
  private final boolean zzdy;
  
  public zzaq(Status paramStatus, MetadataBuffer paramMetadataBuffer, boolean paramBoolean)
  {
    this.zzdw = paramStatus;
    this.zzdx = paramMetadataBuffer;
    this.zzdy = paramBoolean;
  }
  
  public final MetadataBuffer getMetadataBuffer()
  {
    return this.zzdx;
  }
  
  public final Status getStatus()
  {
    return this.zzdw;
  }
  
  public final void release()
  {
    if (this.zzdx != null) {
      this.zzdx.release();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzaq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */