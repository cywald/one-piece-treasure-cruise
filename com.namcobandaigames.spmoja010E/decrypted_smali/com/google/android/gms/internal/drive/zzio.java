package com.google.android.gms.internal.drive;

import java.io.IOException;

public final class zzio
{
  private final byte[] buffer;
  private final int zzmn;
  private final int zzmo;
  private int zzmp;
  private int zzmq;
  private int zzmr;
  private int zzms = Integer.MAX_VALUE;
  private int zzmt = 64;
  private int zzmu = 67108864;
  
  private zzio(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.buffer = paramArrayOfByte;
    this.zzmn = paramInt1;
    paramInt2 = paramInt1 + paramInt2;
    this.zzmp = paramInt2;
    this.zzmo = paramInt2;
    this.zzmq = paramInt1;
  }
  
  public static zzio zza(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return new zzio(paramArrayOfByte, 0, paramInt2);
  }
  
  private final byte zzbg()
    throws IOException
  {
    if (this.zzmq == this.zzmp) {
      throw zziw.zzbk();
    }
    byte[] arrayOfByte = this.buffer;
    int i = this.zzmq;
    this.zzmq = (i + 1);
    return arrayOfByte[i];
  }
  
  private final void zzl(int paramInt)
    throws IOException
  {
    if (paramInt < 0) {
      throw zziw.zzbl();
    }
    if (this.zzmq + paramInt > this.zzms)
    {
      zzl(this.zzms - this.zzmq);
      throw zziw.zzbk();
    }
    if (paramInt <= this.zzmp - this.zzmq)
    {
      this.zzmq += paramInt;
      return;
    }
    throw zziw.zzbk();
  }
  
  public final int getPosition()
  {
    return this.zzmq - this.zzmn;
  }
  
  public final String readString()
    throws IOException
  {
    int i = zzbe();
    if (i < 0) {
      throw zziw.zzbl();
    }
    if (i > this.zzmp - this.zzmq) {
      throw zziw.zzbk();
    }
    String str = new String(this.buffer, this.zzmq, i, zziv.UTF_8);
    this.zzmq = (i + this.zzmq);
    return str;
  }
  
  public final byte[] zza(int paramInt1, int paramInt2)
  {
    if (paramInt2 == 0) {
      return zzja.zzns;
    }
    byte[] arrayOfByte = new byte[paramInt2];
    int i = this.zzmn;
    System.arraycopy(this.buffer, i + paramInt1, arrayOfByte, 0, paramInt2);
    return arrayOfByte;
  }
  
  public final int zzbd()
    throws IOException
  {
    if (this.zzmq == this.zzmp)
    {
      this.zzmr = 0;
      return 0;
    }
    this.zzmr = zzbe();
    if (this.zzmr == 0) {
      throw new zziw("Protocol message contained an invalid tag (zero).");
    }
    return this.zzmr;
  }
  
  public final int zzbe()
    throws IOException
  {
    int i = zzbg();
    if (i >= 0) {}
    int k;
    do
    {
      return i;
      i &= 0x7F;
      j = zzbg();
      if (j >= 0) {
        return i | j << 7;
      }
      i |= (j & 0x7F) << 7;
      j = zzbg();
      if (j >= 0) {
        return i | j << 14;
      }
      i |= (j & 0x7F) << 14;
      k = zzbg();
      if (k >= 0) {
        return i | k << 21;
      }
      j = zzbg();
      k = i | (k & 0x7F) << 21 | j << 28;
      i = k;
    } while (j >= 0);
    int j = 0;
    for (;;)
    {
      if (j >= 5) {
        break label133;
      }
      i = k;
      if (zzbg() >= 0) {
        break;
      }
      j += 1;
    }
    label133:
    throw zziw.zzbm();
  }
  
  public final long zzbf()
    throws IOException
  {
    int i = 0;
    long l = 0L;
    while (i < 64)
    {
      int j = zzbg();
      l |= (j & 0x7F) << i;
      if ((j & 0x80) == 0) {
        return l;
      }
      i += 7;
    }
    throw zziw.zzbm();
  }
  
  public final void zzj(int paramInt)
    throws zziw
  {
    if (this.zzmr != paramInt) {
      throw new zziw("Protocol message end-group tag did not match expected tag.");
    }
  }
  
  public final boolean zzk(int paramInt)
    throws IOException
  {
    switch (paramInt & 0x7)
    {
    default: 
      throw new zziw("Protocol message tag had invalid wire type.");
    case 0: 
      zzbe();
      return true;
    case 1: 
      zzbg();
      zzbg();
      zzbg();
      zzbg();
      zzbg();
      zzbg();
      zzbg();
      zzbg();
      return true;
    case 2: 
      zzl(zzbe());
      return true;
    case 3: 
      int i;
      do
      {
        i = zzbd();
      } while ((i != 0) && (zzk(i)));
      zzj(paramInt >>> 3 << 3 | 0x4);
      return true;
    case 4: 
      return false;
    }
    zzbg();
    zzbg();
    zzbg();
    zzbg();
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzio.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */