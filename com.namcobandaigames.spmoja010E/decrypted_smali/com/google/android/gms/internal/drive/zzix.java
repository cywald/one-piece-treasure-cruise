package com.google.android.gms.internal.drive;

import java.io.IOException;

public abstract class zzix
{
  protected volatile int zznf = -1;
  
  public static final <T extends zzix> T zza(T paramT, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws zziw
  {
    try
    {
      paramArrayOfByte = zzio.zza(paramArrayOfByte, 0, paramInt2);
      paramT.zza(paramArrayOfByte);
      paramArrayOfByte.zzj(0);
      return paramT;
    }
    catch (zziw paramT)
    {
      throw paramT;
    }
    catch (IOException paramT)
    {
      throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).", paramT);
    }
  }
  
  public static final byte[] zza(zzix paramzzix)
  {
    int i = paramzzix.zzaq();
    paramzzix.zznf = i;
    byte[] arrayOfByte = new byte[i];
    i = arrayOfByte.length;
    try
    {
      zzip localzzip = zzip.zzb(arrayOfByte, 0, i);
      paramzzix.zza(localzzip);
      localzzip.zzbh();
      return arrayOfByte;
    }
    catch (IOException paramzzix)
    {
      throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", paramzzix);
    }
  }
  
  public String toString()
  {
    return zziy.zzb(this);
  }
  
  public abstract zzix zza(zzio paramzzio)
    throws IOException;
  
  public void zza(zzip paramzzip)
    throws IOException
  {}
  
  protected int zzaq()
  {
    return 0;
  }
  
  public zzix zzbi()
    throws CloneNotSupportedException
  {
    return (zzix)super.clone();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzix.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */