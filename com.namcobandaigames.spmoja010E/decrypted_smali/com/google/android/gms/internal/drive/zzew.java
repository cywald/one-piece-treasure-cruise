package com.google.android.gms.internal.drive;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzew
  extends zza
  implements zzeu
{
  zzew(IBinder paramIBinder)
  {
    super(paramIBinder, "com.google.android.gms.drive.internal.IEventReleaseCallback");
  }
  
  public final void zza(boolean paramBoolean)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramBoolean);
    transactOneway(1, localParcel);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzew.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */