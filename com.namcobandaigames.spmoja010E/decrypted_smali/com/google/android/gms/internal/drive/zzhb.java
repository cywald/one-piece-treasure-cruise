package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public class zzhb<T>
  extends zzl
{
  private TaskCompletionSource<T> zzif;
  
  zzhb(TaskCompletionSource<T> paramTaskCompletionSource)
  {
    this.zzif = paramTaskCompletionSource;
  }
  
  public final void zza(Status paramStatus)
    throws RemoteException
  {
    this.zzif.setException(new ApiException(paramStatus));
  }
  
  public final TaskCompletionSource<T> zzap()
  {
    return this.zzif;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */