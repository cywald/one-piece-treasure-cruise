package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder.ListenerKey;
import com.google.android.gms.common.internal.ICancelToken;
import com.google.android.gms.drive.events.ListenerToken;

public final class zzg
  implements ListenerToken
{
  private final ListenerHolder.ListenerKey zzcw;
  private ICancelToken zzcx = null;
  
  public zzg(ListenerHolder.ListenerKey paramListenerKey)
  {
    this.zzcw = paramListenerKey;
  }
  
  public final boolean cancel()
  {
    if (this.zzcx != null) {
      try
      {
        this.zzcx.cancel();
        return true;
      }
      catch (RemoteException localRemoteException) {}
    }
    return false;
  }
  
  public final void setCancelToken(ICancelToken paramICancelToken)
  {
    this.zzcx = paramICancelToken;
  }
  
  public final ListenerHolder.ListenerKey zzac()
  {
    return this.zzcw;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */