package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzhf
  extends zzhb<DriveId>
{
  public zzhf(TaskCompletionSource<DriveId> paramTaskCompletionSource)
  {
    super(paramTaskCompletionSource);
  }
  
  public final void zza(zzfh paramzzfh)
    throws RemoteException
  {
    zzap().setResult(paramzzfh.getDriveId());
  }
  
  public final void zza(zzfs paramzzfs)
    throws RemoteException
  {
    zzap().setResult(new zzaa(paramzzfs.zzan()).getDriveId());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */