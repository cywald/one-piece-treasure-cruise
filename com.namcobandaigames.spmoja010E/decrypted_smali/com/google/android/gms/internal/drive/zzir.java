package com.google.android.gms.internal.drive;

import java.io.IOException;

public abstract class zzir<M extends zzir<M>>
  extends zzix
{
  protected zzit zzmw;
  
  public void zza(zzip paramzzip)
    throws IOException
  {
    if (this.zzmw == null) {}
    for (;;)
    {
      return;
      int i = 0;
      while (i < this.zzmw.size())
      {
        this.zzmw.zzs(i).zza(paramzzip);
        i += 1;
      }
    }
  }
  
  protected final boolean zza(zzio paramzzio, int paramInt)
    throws IOException
  {
    int i = paramzzio.getPosition();
    if (!paramzzio.zzk(paramInt)) {
      return false;
    }
    int j = paramInt >>> 3;
    zziz localzziz = new zziz(paramInt, paramzzio.zza(i, paramzzio.getPosition() - i));
    paramzzio = null;
    if (this.zzmw == null) {
      this.zzmw = new zzit();
    }
    for (;;)
    {
      Object localObject = paramzzio;
      if (paramzzio == null)
      {
        localObject = new zziu();
        this.zzmw.zza(j, (zziu)localObject);
      }
      ((zziu)localObject).zza(localzziz);
      return true;
      paramzzio = this.zzmw.zzr(j);
    }
  }
  
  protected int zzaq()
  {
    int j = 0;
    if (this.zzmw != null)
    {
      int i = 0;
      for (;;)
      {
        k = i;
        if (j >= this.zzmw.size()) {
          break;
        }
        i += this.zzmw.zzs(j).zzaq();
        j += 1;
      }
    }
    int k = 0;
    return k;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzir.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */