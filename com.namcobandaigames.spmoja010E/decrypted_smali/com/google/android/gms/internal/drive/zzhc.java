package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzhc
  extends zzhb<DriveContents>
{
  public zzhc(TaskCompletionSource<DriveContents> paramTaskCompletionSource)
  {
    super(paramTaskCompletionSource);
  }
  
  public final void zza(zzfb paramzzfb)
    throws RemoteException
  {
    zzap().setResult(new zzbi(paramzzfb.zzai()));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzhc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */