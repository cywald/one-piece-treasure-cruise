package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.drive.DriveId;

@SafeParcelable.Class(creator="OpenContentsRequestCreator")
@SafeParcelable.Reserved({1})
public final class zzgd
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<zzgd> CREATOR = new zzge();
  @SafeParcelable.Field(id=3)
  private final int mode;
  @SafeParcelable.Field(id=2)
  private final DriveId zzdb;
  @SafeParcelable.Field(id=4)
  private final int zzhz;
  
  @SafeParcelable.Constructor
  @VisibleForTesting
  public zzgd(@SafeParcelable.Param(id=2) DriveId paramDriveId, @SafeParcelable.Param(id=3) int paramInt1, @SafeParcelable.Param(id=4) int paramInt2)
  {
    this.zzdb = paramDriveId;
    this.mode = paramInt1;
    this.zzhz = paramInt2;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeParcelable(paramParcel, 2, this.zzdb, paramInt, false);
    SafeParcelWriter.writeInt(paramParcel, 3, this.mode);
    SafeParcelWriter.writeInt(paramParcel, 4, this.zzhz);
    SafeParcelWriter.finishObjectHeader(paramParcel, i);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzgd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */