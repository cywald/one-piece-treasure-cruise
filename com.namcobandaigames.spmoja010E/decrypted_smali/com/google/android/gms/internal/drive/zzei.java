package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.google.android.gms.drive.FileUploadPreferences;
import com.google.android.gms.drive.TransferPreferences;

@Deprecated
@SafeParcelable.Class(creator="FileUploadPreferencesImplCreator")
@SafeParcelable.Reserved({1})
public final class zzei
  extends AbstractSafeParcelable
  implements FileUploadPreferences
{
  public static final Parcelable.Creator<zzei> CREATOR = new zzej();
  @SafeParcelable.Field(id=3)
  private int zzbl;
  @SafeParcelable.Field(id=2)
  private int zzgw;
  @SafeParcelable.Field(id=4)
  private boolean zzgx;
  
  @SafeParcelable.Constructor
  public zzei(@SafeParcelable.Param(id=2) int paramInt1, @SafeParcelable.Param(id=3) int paramInt2, @SafeParcelable.Param(id=4) boolean paramBoolean)
  {
    this.zzgw = paramInt1;
    this.zzbl = paramInt2;
    this.zzgx = paramBoolean;
  }
  
  public zzei(TransferPreferences paramTransferPreferences)
  {
    this(paramTransferPreferences.getNetworkPreference(), paramTransferPreferences.getBatteryUsagePreference(), paramTransferPreferences.isRoamingAllowed());
  }
  
  private static boolean zzh(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  private static boolean zzi(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public final int getBatteryUsagePreference()
  {
    if (!zzi(this.zzbl)) {
      return 0;
    }
    return this.zzbl;
  }
  
  public final int getNetworkTypePreference()
  {
    if (!zzh(this.zzgw)) {
      return 0;
    }
    return this.zzgw;
  }
  
  public final boolean isRoamingAllowed()
  {
    return this.zzgx;
  }
  
  public final void setBatteryUsagePreference(int paramInt)
  {
    if (!zzi(paramInt)) {
      throw new IllegalArgumentException("Invalid battery usage preference value.");
    }
    this.zzbl = paramInt;
  }
  
  public final void setNetworkTypePreference(int paramInt)
  {
    if (!zzh(paramInt)) {
      throw new IllegalArgumentException("Invalid data connection preference value.");
    }
    this.zzgw = paramInt;
  }
  
  public final void setRoamingAllowed(boolean paramBoolean)
  {
    this.zzgx = paramBoolean;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = SafeParcelWriter.beginObjectHeader(paramParcel);
    SafeParcelWriter.writeInt(paramParcel, 2, this.zzgw);
    SafeParcelWriter.writeInt(paramParcel, 3, this.zzbl);
    SafeParcelWriter.writeBoolean(paramParcel, 4, this.zzgx);
    SafeParcelWriter.finishObjectHeader(paramParcel, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\drive\zzei.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */