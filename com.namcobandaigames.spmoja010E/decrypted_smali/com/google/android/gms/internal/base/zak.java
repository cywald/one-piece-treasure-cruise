package com.google.android.gms.internal.base;

import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;

public final class zak
  extends LruCache<Object, Drawable>
{
  public zak()
  {
    super(10);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\base\zak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */