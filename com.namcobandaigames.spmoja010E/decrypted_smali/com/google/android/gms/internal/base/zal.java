package com.google.android.gms.internal.base;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;

public class zal
  extends Handler
{
  private static volatile zam zaro = null;
  
  public zal() {}
  
  public zal(Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public zal(Looper paramLooper, Handler.Callback paramCallback)
  {
    super(paramLooper, paramCallback);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\base\zal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */