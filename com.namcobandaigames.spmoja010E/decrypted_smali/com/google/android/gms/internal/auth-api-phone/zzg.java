package com.google.android.gms.internal.auth-api-phone;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;

public abstract interface zzg
  extends IInterface
{
  public abstract void zza(Status paramStatus)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth-api-phone\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */