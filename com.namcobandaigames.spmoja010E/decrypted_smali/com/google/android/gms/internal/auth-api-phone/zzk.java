package com.google.android.gms.internal.auth-api-phone;

import android.os.RemoteException;

final class zzk
  extends zzm
{
  zzk(zzj paramzzj)
  {
    super(null);
  }
  
  protected final void zza(zze paramzze)
    throws RemoteException
  {
    paramzze.zza(new zzl(this));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth-api-phone\zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */