package com.google.android.gms.internal.auth-api-phone;

import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zze
  extends IInterface
{
  public abstract void zza(zzg paramzzg)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth-api-phone\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */