package com.google.android.gms.internal.firebase_messaging;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

final class zzf
  extends WeakReference<Throwable>
{
  private final int zzg;
  
  public zzf(Throwable paramThrowable, ReferenceQueue<Throwable> paramReferenceQueue)
  {
    super(paramThrowable, paramReferenceQueue);
    if (paramThrowable == null) {
      throw new NullPointerException("The referent cannot be null");
    }
    this.zzg = System.identityHashCode(paramThrowable);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (this == paramObject);
      paramObject = (zzf)paramObject;
      if (this.zzg != ((zzf)paramObject).zzg) {
        break;
      }
      bool1 = bool2;
    } while (get() == ((zzf)paramObject).get());
    return false;
  }
  
  public final int hashCode()
  {
    return this.zzg;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\firebase_messaging\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */