package com.google.android.gms.internal.firebase_messaging;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;

public class zza
  extends Handler
{
  private static volatile zzb zza = null;
  
  public zza() {}
  
  public zza(Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public zza(Looper paramLooper, Handler.Callback paramCallback)
  {
    super(paramLooper, paramCallback);
  }
  
  public final void dispatchMessage(Message paramMessage)
  {
    super.dispatchMessage(paramMessage);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\firebase_messaging\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */