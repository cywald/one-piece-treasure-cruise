package com.google.android.gms.internal.firebase_messaging;

import java.util.List;

final class zzg
  extends zzd
{
  private final zze zzh = new zze();
  
  public final void zza(Throwable paramThrowable1, Throwable paramThrowable2)
  {
    if (paramThrowable2 == paramThrowable1) {
      throw new IllegalArgumentException("Self suppression is not allowed.", paramThrowable2);
    }
    if (paramThrowable2 == null) {
      throw new NullPointerException("The suppressed exception cannot be null.");
    }
    this.zzh.zza(paramThrowable1, true).add(paramThrowable2);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\firebase_messaging\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */