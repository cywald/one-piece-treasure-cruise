package com.google.android.gms.internal.firebase_messaging;

import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

final class zze
{
  private final ConcurrentHashMap<zzf, List<Throwable>> zze = new ConcurrentHashMap(16, 0.75F, 10);
  private final ReferenceQueue<Throwable> zzf = new ReferenceQueue();
  
  public final List<Throwable> zza(Throwable paramThrowable, boolean paramBoolean)
  {
    for (Object localObject = this.zzf.poll(); localObject != null; localObject = this.zzf.poll()) {
      this.zze.remove(localObject);
    }
    localObject = new zzf(paramThrowable, null);
    localObject = (List)this.zze.get(localObject);
    if (localObject != null) {
      paramThrowable = (Throwable)localObject;
    }
    Vector localVector;
    do
    {
      return paramThrowable;
      localVector = new Vector(2);
      localObject = (List)this.zze.putIfAbsent(new zzf(paramThrowable, this.zzf), localVector);
      paramThrowable = (Throwable)localObject;
    } while (localObject != null);
    return localVector;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\firebase_messaging\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */