package com.google.android.gms.internal.firebase_messaging;

final class zzh
  extends zzd
{
  public final void zza(Throwable paramThrowable1, Throwable paramThrowable2)
  {
    paramThrowable1.addSuppressed(paramThrowable2);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\firebase_messaging\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */