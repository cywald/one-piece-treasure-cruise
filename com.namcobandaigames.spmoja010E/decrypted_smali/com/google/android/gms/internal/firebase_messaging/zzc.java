package com.google.android.gms.internal.firebase_messaging;

import java.io.PrintStream;
import java.lang.reflect.Field;

public final class zzc
{
  private static final zzd zzb;
  private static final int zzc;
  
  static
  {
    for (;;)
    {
      try
      {
        localInteger = zza();
        if (localInteger != null) {}
        try
        {
          if (localInteger.intValue() >= 19)
          {
            localObject = new zzh();
            zzb = (zzd)localObject;
            if (localInteger != null) {
              continue;
            }
            i = 1;
            zzc = i;
            return;
          }
          if (Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic")) {
            continue;
          }
          i = 1;
          if (i == 0) {
            continue;
          }
          Object localObject = new zzg();
          continue;
          localPrintStream = System.err;
        }
        catch (Throwable localThrowable1) {}
      }
      catch (Throwable localThrowable2)
      {
        int i;
        PrintStream localPrintStream;
        String str;
        zza localzza;
        Integer localInteger = null;
        continue;
      }
      str = zza.class.getName();
      localPrintStream.println(String.valueOf(str).length() + 132 + "An error has occured when initializing the try-with-resources desuguring strategy. The default strategy " + str + "will be used. The error is: ");
      localThrowable1.printStackTrace(System.err);
      localzza = new zza();
      continue;
      i = 0;
      continue;
      localzza = new zza();
      continue;
      i = localInteger.intValue();
    }
  }
  
  private static Integer zza()
  {
    try
    {
      Integer localInteger = (Integer)Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
      return localInteger;
    }
    catch (Exception localException)
    {
      System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
      localException.printStackTrace(System.err);
    }
    return null;
  }
  
  public static void zza(Throwable paramThrowable1, Throwable paramThrowable2)
  {
    zzb.zza(paramThrowable1, paramThrowable2);
  }
  
  static final class zza
    extends zzd
  {
    public final void zza(Throwable paramThrowable1, Throwable paramThrowable2) {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\firebase_messaging\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */