package com.google.android.gms.internal.ads_identifier;

import android.os.Parcel;

public class zzc
{
  private static final ClassLoader zzd = zzc.class.getClassLoader();
  
  public static void zza(Parcel paramParcel, boolean paramBoolean)
  {
    paramParcel.writeInt(1);
  }
  
  public static boolean zza(Parcel paramParcel)
  {
    return paramParcel.readInt() != 0;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\ads_identifier\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */