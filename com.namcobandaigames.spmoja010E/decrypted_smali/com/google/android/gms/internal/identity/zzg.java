package com.google.android.gms.internal.identity;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zzg
  extends IInterface
{
  public abstract void zza(int paramInt, Bundle paramBundle)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\identity\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */