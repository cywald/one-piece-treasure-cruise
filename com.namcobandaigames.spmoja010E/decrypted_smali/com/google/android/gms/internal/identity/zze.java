package com.google.android.gms.internal.identity;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.GmsClient;
import com.google.android.gms.identity.intents.UserAddressRequest;

public final class zze
  extends GmsClient<zzi>
{
  private Activity mActivity;
  private final int mTheme;
  private zzf zzh;
  private final String zzi;
  
  public zze(Activity paramActivity, Looper paramLooper, ClientSettings paramClientSettings, int paramInt, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramActivity, paramLooper, 12, paramClientSettings, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.zzi = paramClientSettings.getAccountName();
    this.mActivity = paramActivity;
    this.mTheme = paramInt;
  }
  
  public final void disconnect()
  {
    super.disconnect();
    if (this.zzh != null)
    {
      zzf.zza(this.zzh, null);
      this.zzh = null;
    }
  }
  
  public final int getMinApkVersion()
  {
    return 12451000;
  }
  
  protected final String getServiceDescriptor()
  {
    return "com.google.android.gms.identity.intents.internal.IAddressService";
  }
  
  protected final String getStartServiceAction()
  {
    return "com.google.android.gms.identity.service.BIND";
  }
  
  public final boolean requiresAccount()
  {
    return true;
  }
  
  public final void zza(UserAddressRequest paramUserAddressRequest, int paramInt)
  {
    super.checkConnected();
    this.zzh = new zzf(paramInt, this.mActivity);
    try
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("com.google.android.gms.identity.intents.EXTRA_CALLING_PACKAGE_NAME", getContext().getPackageName());
      if (!TextUtils.isEmpty(this.zzi)) {
        localBundle.putParcelable("com.google.android.gms.identity.intents.EXTRA_ACCOUNT", new Account(this.zzi, "com.google"));
      }
      localBundle.putInt("com.google.android.gms.identity.intents.EXTRA_THEME", this.mTheme);
      ((zzi)super.getService()).zza(this.zzh, paramUserAddressRequest, localBundle);
      return;
    }
    catch (RemoteException paramUserAddressRequest)
    {
      Log.e("AddressClientImpl", "Exception requesting user address", paramUserAddressRequest);
      paramUserAddressRequest = new Bundle();
      paramUserAddressRequest.putInt("com.google.android.gms.identity.intents.EXTRA_ERROR_CODE", 555);
      this.zzh.zza(1, paramUserAddressRequest);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\identity\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */