package com.google.android.gms.internal.identity;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.identity.intents.UserAddressRequest;

public final class zzj
  extends zza
  implements zzi
{
  zzj(IBinder paramIBinder)
  {
    super(paramIBinder, "com.google.android.gms.identity.intents.internal.IAddressService");
  }
  
  public final void zza(zzg paramzzg, UserAddressRequest paramUserAddressRequest, Bundle paramBundle)
    throws RemoteException
  {
    Parcel localParcel = obtainAndWriteInterfaceToken();
    zzc.zza(localParcel, paramzzg);
    zzc.zza(localParcel, paramUserAddressRequest);
    zzc.zza(localParcel, paramBundle);
    transactAndReadExceptionReturnVoid(2, localParcel);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\identity\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */