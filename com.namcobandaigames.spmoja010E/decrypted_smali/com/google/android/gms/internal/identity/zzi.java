package com.google.android.gms.internal.identity;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.identity.intents.UserAddressRequest;

public abstract interface zzi
  extends IInterface
{
  public abstract void zza(zzg paramzzg, UserAddressRequest paramUserAddressRequest, Bundle paramBundle)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\identity\zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */