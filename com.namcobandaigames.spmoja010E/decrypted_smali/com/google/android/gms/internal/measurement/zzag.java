package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzi;
import java.util.HashMap;
import java.util.Map;

public final class zzag
  extends zzi<zzag>
{
  private String zzuo;
  private String zzup;
  private String zzuq;
  private String zzur;
  private boolean zzus;
  private String zzut;
  private boolean zzuu;
  private double zzuv;
  
  public final void setClientId(String paramString)
  {
    this.zzup = paramString;
  }
  
  public final void setUserId(String paramString)
  {
    this.zzuq = paramString;
  }
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("hitType", this.zzuo);
    localHashMap.put("clientId", this.zzup);
    localHashMap.put("userId", this.zzuq);
    localHashMap.put("androidAdId", this.zzur);
    localHashMap.put("AdTargetingEnabled", Boolean.valueOf(this.zzus));
    localHashMap.put("sessionControl", this.zzut);
    localHashMap.put("nonInteraction", Boolean.valueOf(this.zzuu));
    localHashMap.put("sampleRate", Double.valueOf(this.zzuv));
    return zza(localHashMap);
  }
  
  public final void zza(boolean paramBoolean)
  {
    this.zzus = paramBoolean;
  }
  
  public final void zzb(boolean paramBoolean)
  {
    this.zzuu = true;
  }
  
  public final String zzbc()
  {
    return this.zzuo;
  }
  
  public final String zzbd()
  {
    return this.zzup;
  }
  
  public final String zzbe()
  {
    return this.zzuq;
  }
  
  public final String zzbf()
  {
    return this.zzur;
  }
  
  public final boolean zzbg()
  {
    return this.zzus;
  }
  
  public final String zzbh()
  {
    return this.zzut;
  }
  
  public final boolean zzbi()
  {
    return this.zzuu;
  }
  
  public final double zzbj()
  {
    return this.zzuv;
  }
  
  public final void zzl(String paramString)
  {
    this.zzuo = paramString;
  }
  
  public final void zzm(String paramString)
  {
    this.zzur = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */