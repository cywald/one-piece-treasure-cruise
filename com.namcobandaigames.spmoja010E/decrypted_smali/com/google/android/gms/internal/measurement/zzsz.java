package com.google.android.gms.internal.measurement;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

final class zzsz
  extends WeakReference<Throwable>
{
  private final int zzbry;
  
  public zzsz(Throwable paramThrowable, ReferenceQueue<Throwable> paramReferenceQueue)
  {
    super(paramThrowable, null);
    if (paramThrowable == null) {
      throw new NullPointerException("The referent cannot be null");
    }
    this.zzbry = System.identityHashCode(paramThrowable);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (this == paramObject);
      paramObject = (zzsz)paramObject;
      if (this.zzbry != ((zzsz)paramObject).zzbry) {
        break;
      }
      bool1 = bool2;
    } while (get() == ((zzsz)paramObject).get());
    return false;
  }
  
  public final int hashCode()
  {
    return this.zzbry;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */