package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzrq
  extends zzza<zzrq>
{
  public long zzbps = 0L;
  public zzo zzbpt = null;
  public zzl zzqg = null;
  
  public zzrq()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzrq)) {
        return false;
      }
      paramObject = (zzrq)paramObject;
      if (this.zzbps != ((zzrq)paramObject).zzbps) {
        return false;
      }
      if (this.zzqg == null)
      {
        if (((zzrq)paramObject).zzqg != null) {
          return false;
        }
      }
      else if (!this.zzqg.equals(((zzrq)paramObject).zzqg)) {
        return false;
      }
      if (this.zzbpt == null)
      {
        if (((zzrq)paramObject).zzbpt != null) {
          return false;
        }
      }
      else if (!this.zzbpt.equals(((zzrq)paramObject).zzbpt)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzrq)paramObject).zzcfc == null) || (((zzrq)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzrq)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int m = 0;
    int n = getClass().getName().hashCode();
    int i1 = (int)(this.zzbps ^ this.zzbps >>> 32);
    Object localObject = this.zzqg;
    int i;
    int j;
    if (localObject == null)
    {
      i = 0;
      localObject = this.zzbpt;
      if (localObject != null) {
        break label116;
      }
      j = 0;
      label56:
      k = m;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label125;
        }
      }
    }
    label116:
    label125:
    for (int k = m;; k = this.zzcfc.hashCode())
    {
      return (j + (i + ((n + 527) * 31 + i1) * 31) * 31) * 31 + k;
      i = ((zzl)localObject).hashCode();
      break;
      j = ((zzo)localObject).hashCode();
      break label56;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    paramzzyy.zzi(1, this.zzbps);
    if (this.zzqg != null) {
      paramzzyy.zza(2, this.zzqg);
    }
    if (this.zzbpt != null) {
      paramzzyy.zza(3, this.zzbpt);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf() + zzyy.zzd(1, this.zzbps);
    int i = j;
    if (this.zzqg != null) {
      i = j + zzyy.zzb(2, this.zzqg);
    }
    j = i;
    if (this.zzbpt != null) {
      j = i + zzyy.zzb(3, this.zzbpt);
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzrq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */