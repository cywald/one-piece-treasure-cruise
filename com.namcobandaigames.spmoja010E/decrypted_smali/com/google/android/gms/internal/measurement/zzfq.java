package com.google.android.gms.internal.measurement;

public final class zzfq
{
  public static final class zza
    extends zzvm<zza, zza>
    implements zzwv
  {
    private static final zza zzauq = new zza();
    private static volatile zzxd<zza> zznw;
    private String zzauo = "";
    private long zzaup;
    private int zznr;
    
    static
    {
      zzvm.zza(zza.class, zzauq);
    }
    
    protected final Object zza(int paramInt, Object paramObject1, Object paramObject2)
    {
      switch (zzfr.zznq[(paramInt - 1)])
      {
      default: 
        throw new UnsupportedOperationException();
      case 1: 
        paramObject1 = new zza();
      case 2: 
      case 3: 
      case 4: 
      case 5: 
        do
        {
          return paramObject1;
          return new zza(null);
          return zza(zzauq, "\001\002\000\001\001\002\002\000\000\000\001\b\000\002\002\001", new Object[] { "zznr", "zzauo", "zzaup" });
          return zzauq;
          paramObject2 = zznw;
          paramObject1 = paramObject2;
        } while (paramObject2 != null);
        try
        {
          paramObject2 = zznw;
          paramObject1 = paramObject2;
          if (paramObject2 == null)
          {
            paramObject1 = new zzvm.zzb(zzauq);
            zznw = (zzxd)paramObject1;
          }
          return paramObject1;
        }
        finally {}
      case 6: 
        return Byte.valueOf((byte)1);
      }
      return null;
    }
    
    public static final class zza
      extends zzvm.zza<zzfq.zza, zza>
      implements zzwv
    {
      private zza()
      {
        super();
      }
    }
  }
  
  public static final class zzb
    extends zzvm<zzb, zza>
    implements zzwv
  {
    private static final zzb zzaut = new zzb();
    private static volatile zzxd<zzb> zznw;
    private int zzaur = 1;
    private zzvs<zzfq.zza> zzaus = zzwc();
    private int zznr;
    
    static
    {
      zzvm.zza(zzb.class, zzaut);
    }
    
    public static zzxd<zzb> zza()
    {
      return (zzxd)zzaut.zza(zzvm.zze.zzbyz, null, null);
    }
    
    protected final Object zza(int paramInt, Object paramObject1, Object paramObject2)
    {
      switch (zzfr.zznq[(paramInt - 1)])
      {
      default: 
        throw new UnsupportedOperationException();
      case 1: 
        paramObject1 = new zzb();
      case 2: 
      case 3: 
      case 4: 
      case 5: 
        do
        {
          return paramObject1;
          return new zza(null);
          paramObject1 = zzb.zzd();
          return zza(zzaut, "\001\002\000\001\001\002\002\000\001\000\001\f\000\002\033", new Object[] { "zznr", "zzaur", paramObject1, "zzaus", zzfq.zza.class });
          return zzaut;
          paramObject2 = zznw;
          paramObject1 = paramObject2;
        } while (paramObject2 != null);
        try
        {
          paramObject2 = zznw;
          paramObject1 = paramObject2;
          if (paramObject2 == null)
          {
            paramObject1 = new zzvm.zzb(zzaut);
            zznw = (zzxd)paramObject1;
          }
          return paramObject1;
        }
        finally {}
      case 6: 
        return Byte.valueOf((byte)1);
      }
      return null;
    }
    
    public static final class zza
      extends zzvm.zza<zzfq.zzb, zza>
      implements zzwv
    {
      private zza()
      {
        super();
      }
    }
    
    public static enum zzb
      implements zzvp
    {
      private static final zzvq<zzb> zzoa = new zzfs();
      private final int value;
      
      private zzb(int paramInt)
      {
        this.value = paramInt;
      }
      
      public static zzvr zzd()
      {
        return zzft.zzoc;
      }
      
      public static zzb zzs(int paramInt)
      {
        switch (paramInt)
        {
        default: 
          return null;
        case 1: 
          return zzauu;
        }
        return zzauv;
      }
      
      public final int zzc()
      {
        return this.value;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzfq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */