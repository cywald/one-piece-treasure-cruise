package com.google.android.gms.internal.measurement;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class zzsg
{
  private static final Uri CONTENT_URI = Uri.parse("content://com.google.android.gsf.gservices");
  private static final Uri zzbqd = Uri.parse("content://com.google.android.gsf.gservices/prefix");
  public static final Pattern zzbqe = Pattern.compile("^(1|true|t|on|yes|y)$", 2);
  public static final Pattern zzbqf = Pattern.compile("^(0|false|f|off|no|n)$", 2);
  private static final AtomicBoolean zzbqg = new AtomicBoolean();
  private static HashMap<String, String> zzbqh;
  private static final HashMap<String, Boolean> zzbqi = new HashMap();
  private static final HashMap<String, Integer> zzbqj = new HashMap();
  private static final HashMap<String, Long> zzbqk = new HashMap();
  private static final HashMap<String, Float> zzbql = new HashMap();
  private static Object zzbqm;
  private static boolean zzbqn;
  private static String[] zzbqo = new String[0];
  
  private static <T> T zza(HashMap<String, T> paramHashMap, String paramString, T paramT)
  {
    for (;;)
    {
      try
      {
        if (paramHashMap.containsKey(paramString))
        {
          paramHashMap = paramHashMap.get(paramString);
          if (paramHashMap != null) {
            return paramHashMap;
          }
        }
        else
        {
          return null;
        }
      }
      finally {}
      paramHashMap = paramT;
    }
  }
  
  public static String zza(ContentResolver paramContentResolver, String paramString1, String paramString2)
  {
    String str = null;
    paramString2 = null;
    Object localObject1 = null;
    Object localObject2 = null;
    for (;;)
    {
      Object localObject3;
      int i;
      try
      {
        zza(paramContentResolver);
        localObject3 = zzbqm;
        if (zzbqh.containsKey(paramString1))
        {
          paramString1 = (String)zzbqh.get(paramString1);
          paramContentResolver = (ContentResolver)localObject2;
          if (paramString1 != null) {
            paramContentResolver = paramString1;
          }
          return paramContentResolver;
        }
        localObject2 = zzbqo;
        int j = localObject2.length;
        i = 0;
        if (i >= j) {
          break label167;
        }
        if (!paramString1.startsWith(localObject2[i])) {
          break label326;
        }
        if ((!zzbqn) || (zzbqh.isEmpty()))
        {
          paramString2 = zzbqo;
          zzbqh.putAll(zza(paramContentResolver, paramString2));
          zzbqn = true;
          if (zzbqh.containsKey(paramString1))
          {
            paramString1 = (String)zzbqh.get(paramString1);
            paramContentResolver = str;
            if (paramString1 != null) {
              paramContentResolver = paramString1;
            }
            return paramContentResolver;
          }
        }
      }
      finally {}
      return null;
      label167:
      localObject2 = paramContentResolver.query(CONTENT_URI, null, null, new String[] { paramString1 }, null);
      if (localObject2 == null)
      {
        paramContentResolver = (ContentResolver)localObject1;
        if (localObject2 != null)
        {
          ((Cursor)localObject2).close();
          return null;
        }
      }
      else
      {
        try
        {
          if (!((Cursor)localObject2).moveToFirst())
          {
            zza(localObject3, paramString1, null);
            paramContentResolver = (ContentResolver)localObject1;
            return null;
          }
          str = ((Cursor)localObject2).getString(1);
          paramContentResolver = str;
          if (str != null)
          {
            paramContentResolver = str;
            if (str.equals(null)) {
              paramContentResolver = null;
            }
          }
          zza(localObject3, paramString1, paramContentResolver);
          paramString1 = paramString2;
          if (paramContentResolver != null) {
            paramString1 = paramContentResolver;
          }
          paramContentResolver = paramString1;
          return paramString1;
        }
        finally
        {
          if (localObject2 != null) {
            ((Cursor)localObject2).close();
          }
        }
      }
      return paramContentResolver;
      label326:
      i += 1;
    }
  }
  
  private static Map<String, String> zza(ContentResolver paramContentResolver, String... paramVarArgs)
  {
    paramContentResolver = paramContentResolver.query(zzbqd, null, null, paramVarArgs, null);
    paramVarArgs = new TreeMap();
    if (paramContentResolver == null) {
      return paramVarArgs;
    }
    try
    {
      if (paramContentResolver.moveToNext()) {
        paramVarArgs.put(paramContentResolver.getString(0), paramContentResolver.getString(1));
      }
      return paramVarArgs;
    }
    finally
    {
      paramContentResolver.close();
    }
  }
  
  private static void zza(ContentResolver paramContentResolver)
  {
    if (zzbqh == null)
    {
      zzbqg.set(false);
      zzbqh = new HashMap();
      zzbqm = new Object();
      zzbqn = false;
      paramContentResolver.registerContentObserver(CONTENT_URI, true, new zzsh(null));
    }
    while (!zzbqg.getAndSet(false)) {
      return;
    }
    zzbqh.clear();
    zzbqi.clear();
    zzbqj.clear();
    zzbqk.clear();
    zzbql.clear();
    zzbqm = new Object();
    zzbqn = false;
  }
  
  private static void zza(Object paramObject, String paramString1, String paramString2)
  {
    try
    {
      if (paramObject == zzbqm) {
        zzbqh.put(paramString1, paramString2);
      }
      return;
    }
    finally {}
  }
  
  private static <T> void zza(Object paramObject, HashMap<String, T> paramHashMap, String paramString, T paramT)
  {
    try
    {
      if (paramObject == zzbqm)
      {
        paramHashMap.put(paramString, paramT);
        zzbqh.remove(paramString);
      }
      return;
    }
    finally {}
  }
  
  public static boolean zza(ContentResolver paramContentResolver, String paramString, boolean paramBoolean)
  {
    Object localObject = zzb(paramContentResolver);
    Boolean localBoolean = (Boolean)zza(zzbqi, paramString, Boolean.valueOf(paramBoolean));
    if (localBoolean != null) {
      return localBoolean.booleanValue();
    }
    String str = zza(paramContentResolver, paramString, null);
    paramContentResolver = localBoolean;
    boolean bool = paramBoolean;
    if (str != null)
    {
      if (!str.equals("")) {
        break label78;
      }
      bool = paramBoolean;
      paramContentResolver = localBoolean;
    }
    for (;;)
    {
      zza(localObject, zzbqi, paramString, paramContentResolver);
      return bool;
      label78:
      if (zzbqe.matcher(str).matches())
      {
        paramContentResolver = Boolean.valueOf(true);
        bool = true;
      }
      else if (zzbqf.matcher(str).matches())
      {
        paramContentResolver = Boolean.valueOf(false);
        bool = false;
      }
      else
      {
        Log.w("Gservices", "attempt to read gservices key " + paramString + " (value \"" + str + "\") as boolean");
        paramContentResolver = localBoolean;
        bool = paramBoolean;
      }
    }
  }
  
  private static Object zzb(ContentResolver paramContentResolver)
  {
    try
    {
      zza(paramContentResolver);
      paramContentResolver = zzbqm;
      return paramContentResolver;
    }
    finally {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */