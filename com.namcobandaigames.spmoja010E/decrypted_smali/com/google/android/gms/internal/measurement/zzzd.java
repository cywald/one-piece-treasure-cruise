package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class zzzd
  implements Cloneable
{
  private Object value;
  private zzzb<?, ?> zzcfj;
  private List<zzzi> zzcfk = new ArrayList();
  
  private final byte[] toByteArray()
    throws IOException
  {
    byte[] arrayOfByte = new byte[zzf()];
    zza(zzyy.zzo(arrayOfByte));
    return arrayOfByte;
  }
  
  private final zzzd zzyv()
  {
    zzzd localzzzd = new zzzd();
    try
    {
      localzzzd.zzcfj = this.zzcfj;
      if (this.zzcfk == null) {
        localzzzd.zzcfk = null;
      }
      for (;;)
      {
        if (this.value == null) {
          return localzzzd;
        }
        if (!(this.value instanceof zzzg)) {
          break;
        }
        localzzzd.value = ((zzzg)((zzzg)this.value).clone());
        return localzzzd;
        localzzzd.zzcfk.addAll(this.zzcfk);
      }
      if (!(this.value instanceof byte[])) {
        break label117;
      }
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new AssertionError(localCloneNotSupportedException);
    }
    localCloneNotSupportedException.value = ((byte[])this.value).clone();
    return localCloneNotSupportedException;
    label117:
    Object localObject1;
    Object localObject2;
    int i;
    if ((this.value instanceof byte[][]))
    {
      localObject1 = (byte[][])this.value;
      localObject2 = new byte[localObject1.length][];
      localCloneNotSupportedException.value = localObject2;
      i = 0;
      while (i < localObject1.length)
      {
        localObject2[i] = ((byte[])localObject1[i].clone());
        i += 1;
      }
    }
    if ((this.value instanceof boolean[]))
    {
      localCloneNotSupportedException.value = ((boolean[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof int[]))
    {
      localCloneNotSupportedException.value = ((int[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof long[]))
    {
      localCloneNotSupportedException.value = ((long[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof float[]))
    {
      localCloneNotSupportedException.value = ((float[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof double[]))
    {
      localCloneNotSupportedException.value = ((double[])this.value).clone();
      return localCloneNotSupportedException;
    }
    if ((this.value instanceof zzzg[]))
    {
      localObject1 = (zzzg[])this.value;
      localObject2 = new zzzg[localObject1.length];
      localCloneNotSupportedException.value = localObject2;
      i = 0;
      while (i < localObject1.length)
      {
        localObject2[i] = ((zzzg)localObject1[i].clone());
        i += 1;
      }
    }
    return localCloneNotSupportedException;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (!(paramObject instanceof zzzd));
      paramObject = (zzzd)paramObject;
      if ((this.value == null) || (((zzzd)paramObject).value == null)) {
        break;
      }
      bool1 = bool2;
    } while (this.zzcfj != ((zzzd)paramObject).zzcfj);
    if (!this.zzcfj.zzcfd.isArray()) {
      return this.value.equals(((zzzd)paramObject).value);
    }
    if ((this.value instanceof byte[])) {
      return Arrays.equals((byte[])this.value, (byte[])((zzzd)paramObject).value);
    }
    if ((this.value instanceof int[])) {
      return Arrays.equals((int[])this.value, (int[])((zzzd)paramObject).value);
    }
    if ((this.value instanceof long[])) {
      return Arrays.equals((long[])this.value, (long[])((zzzd)paramObject).value);
    }
    if ((this.value instanceof float[])) {
      return Arrays.equals((float[])this.value, (float[])((zzzd)paramObject).value);
    }
    if ((this.value instanceof double[])) {
      return Arrays.equals((double[])this.value, (double[])((zzzd)paramObject).value);
    }
    if ((this.value instanceof boolean[])) {
      return Arrays.equals((boolean[])this.value, (boolean[])((zzzd)paramObject).value);
    }
    return Arrays.deepEquals((Object[])this.value, (Object[])((zzzd)paramObject).value);
    if ((this.zzcfk != null) && (((zzzd)paramObject).zzcfk != null)) {
      return this.zzcfk.equals(((zzzd)paramObject).zzcfk);
    }
    try
    {
      bool1 = Arrays.equals(toByteArray(), ((zzzd)paramObject).toByteArray());
      return bool1;
    }
    catch (IOException paramObject)
    {
      throw new IllegalStateException((Throwable)paramObject);
    }
  }
  
  public final int hashCode()
  {
    try
    {
      int i = Arrays.hashCode(toByteArray());
      return i + 527;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException(localIOException);
    }
  }
  
  final void zza(zzyy paramzzyy)
    throws IOException
  {
    Object localObject1;
    Object localObject2;
    if (this.value != null)
    {
      localObject1 = this.zzcfj;
      localObject2 = this.value;
      if (((zzzb)localObject1).zzcfe)
      {
        int j = Array.getLength(localObject2);
        int i = 0;
        while (i < j)
        {
          Object localObject3 = Array.get(localObject2, i);
          if (localObject3 != null) {
            ((zzzb)localObject1).zza(localObject3, paramzzyy);
          }
          i += 1;
        }
      }
      ((zzzb)localObject1).zza(localObject2, paramzzyy);
    }
    for (;;)
    {
      return;
      localObject1 = this.zzcfk.iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (zzzi)((Iterator)localObject1).next();
        paramzzyy.zzca(((zzzi)localObject2).tag);
        paramzzyy.zzp(((zzzi)localObject2).zzbug);
      }
    }
  }
  
  final void zza(zzzi paramzzzi)
    throws IOException
  {
    if (this.zzcfk != null)
    {
      this.zzcfk.add(paramzzzi);
      return;
    }
    Object localObject;
    if ((this.value instanceof zzzg))
    {
      paramzzzi = paramzzzi.zzbug;
      localObject = zzyx.zzj(paramzzzi, 0, paramzzzi.length);
      int i = ((zzyx)localObject).zzuy();
      if (i != paramzzzi.length - zzyy.zzbc(i)) {
        throw zzzf.zzyw();
      }
      paramzzzi = ((zzzg)this.value).zza((zzyx)localObject);
    }
    for (;;)
    {
      this.zzcfj = this.zzcfj;
      this.value = paramzzzi;
      this.zzcfk = null;
      return;
      if ((this.value instanceof zzzg[]))
      {
        localObject = (zzzg[])this.zzcfj.zzah(Collections.singletonList(paramzzzi));
        zzzg[] arrayOfzzzg = (zzzg[])this.value;
        paramzzzi = (zzzg[])Arrays.copyOf(arrayOfzzzg, arrayOfzzzg.length + localObject.length);
        System.arraycopy(localObject, 0, paramzzzi, arrayOfzzzg.length, localObject.length);
      }
      else
      {
        paramzzzi = this.zzcfj.zzah(Collections.singletonList(paramzzzi));
      }
    }
  }
  
  final <T> T zzb(zzzb<?, T> paramzzzb)
  {
    if (this.value != null)
    {
      if (!this.zzcfj.equals(paramzzzb)) {
        throw new IllegalStateException("Tried to getExtension with a different Extension.");
      }
    }
    else
    {
      this.zzcfj = paramzzzb;
      this.value = paramzzzb.zzah(this.zzcfk);
      this.zzcfk = null;
    }
    return (T)this.value;
  }
  
  final int zzf()
  {
    int i = 0;
    Object localObject2;
    int j;
    if (this.value != null)
    {
      localObject1 = this.zzcfj;
      localObject2 = this.value;
      if (((zzzb)localObject1).zzcfe)
      {
        int m = Array.getLength(localObject2);
        j = 0;
        for (;;)
        {
          k = i;
          if (j >= m) {
            break;
          }
          k = i;
          if (Array.get(localObject2, j) != null) {
            k = i + ((zzzb)localObject1).zzak(Array.get(localObject2, j));
          }
          j += 1;
          i = k;
        }
      }
      int k = ((zzzb)localObject1).zzak(localObject2);
      return k;
    }
    Object localObject1 = this.zzcfk.iterator();
    for (i = 0; ((Iterator)localObject1).hasNext(); i = ((zzzi)localObject2).zzbug.length + (j + 0) + i)
    {
      localObject2 = (zzzi)((Iterator)localObject1).next();
      j = zzyy.zzbj(((zzzi)localObject2).tag);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */