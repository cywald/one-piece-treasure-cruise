package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzcb
  extends zzau
{
  @VisibleForTesting
  zzcb(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  protected final void zzag() {}
  
  public final zzac zzek()
  {
    zzcl();
    return zzca().zzae();
  }
  
  public final String zzel()
  {
    zzcl();
    zzac localzzac = zzek();
    int i = localzzac.zzuh;
    int j = localzzac.zzui;
    return 23 + i + "x" + j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */