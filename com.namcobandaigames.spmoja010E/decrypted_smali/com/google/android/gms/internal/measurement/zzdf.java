package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;

@VisibleForTesting
public final class zzdf
  implements zzbu
{
  public String zzaci;
  public double zzacj = -1.0D;
  public int zzack = -1;
  public int zzacl = -1;
  public int zzacm = -1;
  public int zzacn = -1;
  public Map<String, String> zzaco = new HashMap();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzdf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */