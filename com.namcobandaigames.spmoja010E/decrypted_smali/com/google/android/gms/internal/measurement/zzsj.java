package com.google.android.gms.internal.measurement;

import android.database.ContentObserver;
import android.os.Handler;

final class zzsj
  extends ContentObserver
{
  zzsj(zzsi paramzzsi, Handler paramHandler)
  {
    super(null);
  }
  
  public final void onChange(boolean paramBoolean)
  {
    this.zzbqx.zzta();
    zzsi.zza(this.zzbqx);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */