package com.google.android.gms.internal.measurement;

import java.util.Iterator;

final class zzxq
{
  private static final Iterator<Object> zzcci = new zzxr();
  private static final Iterable<Object> zzccj = new zzxs();
  
  static <T> Iterable<T> zzyc()
  {
    return zzccj;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */