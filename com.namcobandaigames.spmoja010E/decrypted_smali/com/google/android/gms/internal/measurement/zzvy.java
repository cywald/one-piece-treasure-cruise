package com.google.android.gms.internal.measurement;

import java.util.Map.Entry;

final class zzvy<K>
  implements Map.Entry<K, Object>
{
  private Map.Entry<K, zzvw> zzcab;
  
  private zzvy(Map.Entry<K, zzvw> paramEntry)
  {
    this.zzcab = paramEntry;
  }
  
  public final K getKey()
  {
    return (K)this.zzcab.getKey();
  }
  
  public final Object getValue()
  {
    if ((zzvw)this.zzcab.getValue() == null) {
      return null;
    }
    return zzvw.zzwt();
  }
  
  public final Object setValue(Object paramObject)
  {
    if (!(paramObject instanceof zzwt)) {
      throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
    return ((zzvw)this.zzcab.getValue()).zzi((zzwt)paramObject);
  }
  
  public final zzvw zzwu()
  {
    return (zzvw)this.zzcab.getValue();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */