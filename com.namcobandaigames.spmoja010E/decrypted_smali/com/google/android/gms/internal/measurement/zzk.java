package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzk
  extends zzza<zzk>
{
  private static volatile zzk[] zzor;
  public int key = 0;
  public int value = 0;
  
  public zzk()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzk[] zzh()
  {
    if (zzor == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzor == null) {
        zzor = new zzk[0];
      }
      return zzor;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzk)) {
        return false;
      }
      paramObject = (zzk)paramObject;
      if (this.key != ((zzk)paramObject).key) {
        return false;
      }
      if (this.value != ((zzk)paramObject).value) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzk)paramObject).zzcfc == null) || (((zzk)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzk)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = this.key;
    int m = this.value;
    if ((this.zzcfc == null) || (this.zzcfc.isEmpty())) {}
    for (int i = 0;; i = this.zzcfc.hashCode()) {
      return i + (((j + 527) * 31 + k) * 31 + m) * 31;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    paramzzyy.zzd(1, this.key);
    paramzzyy.zzd(2, this.value);
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    return super.zzf() + zzyy.zzh(1, this.key) + zzyy.zzh(2, this.value);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */