package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzw
  extends zzq
  implements zzu
{
  zzw(IBinder paramIBinder)
  {
    super(paramIBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
  }
  
  public final Bundle zza(Bundle paramBundle)
    throws RemoteException
  {
    Object localObject = obtainAndWriteInterfaceToken();
    zzs.zza((Parcel)localObject, paramBundle);
    paramBundle = transactAndReadException(1, (Parcel)localObject);
    localObject = (Bundle)zzs.zza(paramBundle, Bundle.CREATOR);
    paramBundle.recycle();
    return (Bundle)localObject;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */