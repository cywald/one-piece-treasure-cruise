package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map.Entry;

final class zzxp
  extends zzxv
{
  private zzxp(zzxm paramzzxm)
  {
    super(paramzzxm, null);
  }
  
  public final Iterator<Map.Entry<K, V>> iterator()
  {
    return new zzxo(this.zzcch, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */