package com.google.android.gms.internal.measurement;

public abstract interface zzxd<MessageType>
{
  public abstract MessageType zza(zzuo paramzzuo, zzuz paramzzuz)
    throws zzvt;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */