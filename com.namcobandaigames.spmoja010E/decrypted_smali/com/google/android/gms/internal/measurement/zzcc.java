package com.google.android.gms.internal.measurement;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.PersistableBundle;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;

public final class zzcc
  extends zzau
{
  private boolean zzyr;
  private boolean zzys;
  private final AlarmManager zzyt = (AlarmManager)getContext().getSystemService("alarm");
  private Integer zzyu;
  
  protected zzcc(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  private final int getJobId()
  {
    if (this.zzyu == null)
    {
      str = String.valueOf(getContext().getPackageName());
      if (str.length() == 0) {
        break label51;
      }
    }
    label51:
    for (String str = "analytics".concat(str);; str = new String("analytics"))
    {
      this.zzyu = Integer.valueOf(str.hashCode());
      return this.zzyu.intValue();
    }
  }
  
  private final PendingIntent zzeo()
  {
    Intent localIntent = new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
    localIntent.setComponent(new ComponentName(getContext(), "com.google.android.gms.analytics.AnalyticsReceiver"));
    return PendingIntent.getBroadcast(getContext(), 0, localIntent, 0);
  }
  
  public final void cancel()
  {
    this.zzys = false;
    this.zzyt.cancel(zzeo());
    if (Build.VERSION.SDK_INT >= 24)
    {
      JobScheduler localJobScheduler = (JobScheduler)getContext().getSystemService("jobscheduler");
      zza("Cancelling job. JobID", Integer.valueOf(getJobId()));
      localJobScheduler.cancel(getJobId());
    }
  }
  
  protected final void zzag()
  {
    try
    {
      cancel();
      if (zzbx.zzea() > 0L)
      {
        ActivityInfo localActivityInfo = getContext().getPackageManager().getReceiverInfo(new ComponentName(getContext(), "com.google.android.gms.analytics.AnalyticsReceiver"), 2);
        if ((localActivityInfo != null) && (localActivityInfo.enabled))
        {
          zzq("Receiver registered for local dispatch.");
          this.zzyr = true;
        }
      }
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
  }
  
  public final boolean zzej()
  {
    return this.zzys;
  }
  
  public final boolean zzem()
  {
    return this.zzyr;
  }
  
  public final void zzen()
  {
    zzcl();
    Preconditions.checkState(this.zzyr, "Receiver not registered");
    long l1 = zzbx.zzea();
    long l2;
    if (l1 > 0L)
    {
      cancel();
      l2 = zzbx().elapsedRealtime();
      this.zzys = true;
      ((Boolean)zzcf.zzaam.get()).booleanValue();
      if (Build.VERSION.SDK_INT >= 24)
      {
        zzq("Scheduling upload with JobScheduler");
        Object localObject = new ComponentName(getContext(), "com.google.android.gms.analytics.AnalyticsJobService");
        JobScheduler localJobScheduler = (JobScheduler)getContext().getSystemService("jobscheduler");
        localObject = new JobInfo.Builder(getJobId(), (ComponentName)localObject);
        ((JobInfo.Builder)localObject).setMinimumLatency(l1);
        ((JobInfo.Builder)localObject).setOverrideDeadline(l1 << 1);
        PersistableBundle localPersistableBundle = new PersistableBundle();
        localPersistableBundle.putString("action", "com.google.android.gms.analytics.ANALYTICS_DISPATCH");
        ((JobInfo.Builder)localObject).setExtras(localPersistableBundle);
        localObject = ((JobInfo.Builder)localObject).build();
        zza("Scheduling job. JobID", Integer.valueOf(getJobId()));
        localJobScheduler.schedule((JobInfo)localObject);
      }
    }
    else
    {
      return;
    }
    zzq("Scheduling upload with AlarmManager");
    this.zzyt.setInexactRepeating(2, l2 + l1, l1, zzeo());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */