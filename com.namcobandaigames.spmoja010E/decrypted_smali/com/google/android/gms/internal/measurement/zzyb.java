package com.google.android.gms.internal.measurement;

import java.io.IOException;

abstract class zzyb<T, B>
{
  abstract void zza(B paramB, int paramInt, long paramLong);
  
  abstract void zza(B paramB, int paramInt, zzud paramzzud);
  
  abstract void zza(B paramB, int paramInt, T paramT);
  
  abstract void zza(T paramT, zzyw paramzzyw)
    throws IOException;
  
  abstract boolean zza(zzxi paramzzxi);
  
  final boolean zza(B paramB, zzxi paramzzxi)
    throws IOException
  {
    int i = paramzzxi.getTag();
    int j = i >>> 3;
    switch (i & 0x7)
    {
    default: 
      throw zzvt.zzwo();
    case 0: 
      zza(paramB, j, paramzzxi.zzui());
      return true;
    case 5: 
      zzc(paramB, j, paramzzxi.zzul());
      return true;
    case 1: 
      zzb(paramB, j, paramzzxi.zzuk());
      return true;
    case 2: 
      zza(paramB, j, paramzzxi.zzuo());
      return true;
    case 3: 
      Object localObject = zzye();
      while ((paramzzxi.zzve() != Integer.MAX_VALUE) && (zza(localObject, paramzzxi))) {}
      if ((j << 3 | 0x4) != paramzzxi.getTag()) {
        throw zzvt.zzwn();
      }
      zza(paramB, j, zzab(localObject));
      return true;
    }
    return false;
  }
  
  abstract T zzab(B paramB);
  
  abstract int zzae(T paramT);
  
  abstract T zzah(Object paramObject);
  
  abstract B zzai(Object paramObject);
  
  abstract int zzaj(T paramT);
  
  abstract void zzb(B paramB, int paramInt, long paramLong);
  
  abstract void zzc(B paramB, int paramInt1, int paramInt2);
  
  abstract void zzc(T paramT, zzyw paramzzyw)
    throws IOException;
  
  abstract void zzf(Object paramObject, T paramT);
  
  abstract void zzg(Object paramObject, B paramB);
  
  abstract T zzh(T paramT1, T paramT2);
  
  abstract void zzu(Object paramObject);
  
  abstract B zzye();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */