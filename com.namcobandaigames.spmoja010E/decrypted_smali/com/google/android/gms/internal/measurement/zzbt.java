package com.google.android.gms.internal.measurement;

public enum zzbt
{
  private zzbt() {}
  
  public static zzbt zzaa(String paramString)
  {
    if ("GZIP".equalsIgnoreCase(paramString)) {
      return zzyh;
    }
    return zzyg;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */