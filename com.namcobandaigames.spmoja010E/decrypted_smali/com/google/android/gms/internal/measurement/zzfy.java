package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzfy
  extends zzza<zzfy>
{
  private static volatile zzfy[] zzavt;
  public Boolean zzavb = null;
  public Boolean zzavc = null;
  public Integer zzave = null;
  public String zzavu = null;
  public zzfw zzavv = null;
  
  public zzfy()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzfy[] zzml()
  {
    if (zzavt == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzavt == null) {
        zzavt = new zzfy[0];
      }
      return zzavt;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzfy)) {
        return false;
      }
      paramObject = (zzfy)paramObject;
      if (this.zzave == null)
      {
        if (((zzfy)paramObject).zzave != null) {
          return false;
        }
      }
      else if (!this.zzave.equals(((zzfy)paramObject).zzave)) {
        return false;
      }
      if (this.zzavu == null)
      {
        if (((zzfy)paramObject).zzavu != null) {
          return false;
        }
      }
      else if (!this.zzavu.equals(((zzfy)paramObject).zzavu)) {
        return false;
      }
      if (this.zzavv == null)
      {
        if (((zzfy)paramObject).zzavv != null) {
          return false;
        }
      }
      else if (!this.zzavv.equals(((zzfy)paramObject).zzavv)) {
        return false;
      }
      if (this.zzavb == null)
      {
        if (((zzfy)paramObject).zzavb != null) {
          return false;
        }
      }
      else if (!this.zzavb.equals(((zzfy)paramObject).zzavb)) {
        return false;
      }
      if (this.zzavc == null)
      {
        if (((zzfy)paramObject).zzavc != null) {
          return false;
        }
      }
      else if (!this.zzavc.equals(((zzfy)paramObject).zzavc)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzfy)paramObject).zzcfc == null) || (((zzfy)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzfy)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i2 = 0;
    int i3 = getClass().getName().hashCode();
    int i;
    int j;
    label33:
    zzfw localzzfw;
    int k;
    label46:
    int m;
    label56:
    int n;
    if (this.zzave == null)
    {
      i = 0;
      if (this.zzavu != null) {
        break label142;
      }
      j = 0;
      localzzfw = this.zzavv;
      if (localzzfw != null) {
        break label153;
      }
      k = 0;
      if (this.zzavb != null) {
        break label162;
      }
      m = 0;
      if (this.zzavc != null) {
        break label174;
      }
      n = 0;
      label66:
      i1 = i2;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label186;
        }
      }
    }
    label142:
    label153:
    label162:
    label174:
    label186:
    for (int i1 = i2;; i1 = this.zzcfc.hashCode())
    {
      return (n + (m + (k + (j + (i + (i3 + 527) * 31) * 31) * 31) * 31) * 31) * 31 + i1;
      i = this.zzave.hashCode();
      break;
      j = this.zzavu.hashCode();
      break label33;
      k = localzzfw.hashCode();
      break label46;
      m = this.zzavb.hashCode();
      break label56;
      n = this.zzavc.hashCode();
      break label66;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzave != null) {
      paramzzyy.zzd(1, this.zzave.intValue());
    }
    if (this.zzavu != null) {
      paramzzyy.zzb(2, this.zzavu);
    }
    if (this.zzavv != null) {
      paramzzyy.zza(3, this.zzavv);
    }
    if (this.zzavb != null) {
      paramzzyy.zzb(4, this.zzavb.booleanValue());
    }
    if (this.zzavc != null) {
      paramzzyy.zzb(5, this.zzavc.booleanValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.zzave != null) {
      i = j + zzyy.zzh(1, this.zzave.intValue());
    }
    j = i;
    if (this.zzavu != null) {
      j = i + zzyy.zzc(2, this.zzavu);
    }
    i = j;
    if (this.zzavv != null) {
      i = j + zzyy.zzb(3, this.zzavv);
    }
    j = i;
    if (this.zzavb != null)
    {
      this.zzavb.booleanValue();
      j = i + (zzyy.zzbb(4) + 1);
    }
    i = j;
    if (this.zzavc != null)
    {
      this.zzavc.booleanValue();
      i = j + (zzyy.zzbb(5) + 1);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzfy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */