package com.google.android.gms.internal.measurement;

import java.util.Iterator;

public abstract interface zzuj
  extends Iterator<Byte>
{
  public abstract byte nextByte();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */