package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgf
  extends zzza<zzgf>
{
  private static volatile zzgf[] zzaws;
  public Integer count = null;
  public String name = null;
  public zzgg[] zzawt = zzgg.zzmr();
  public Long zzawu = null;
  public Long zzawv = null;
  
  public zzgf()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzgf[] zzmq()
  {
    if (zzaws == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzaws == null) {
        zzaws = new zzgf[0];
      }
      return zzaws;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgf)) {
        return false;
      }
      paramObject = (zzgf)paramObject;
      if (!zzze.equals(this.zzawt, ((zzgf)paramObject).zzawt)) {
        return false;
      }
      if (this.name == null)
      {
        if (((zzgf)paramObject).name != null) {
          return false;
        }
      }
      else if (!this.name.equals(((zzgf)paramObject).name)) {
        return false;
      }
      if (this.zzawu == null)
      {
        if (((zzgf)paramObject).zzawu != null) {
          return false;
        }
      }
      else if (!this.zzawu.equals(((zzgf)paramObject).zzawu)) {
        return false;
      }
      if (this.zzawv == null)
      {
        if (((zzgf)paramObject).zzawv != null) {
          return false;
        }
      }
      else if (!this.zzawv.equals(((zzgf)paramObject).zzawv)) {
        return false;
      }
      if (this.count == null)
      {
        if (((zzgf)paramObject).count != null) {
          return false;
        }
      }
      else if (!this.count.equals(((zzgf)paramObject).count)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgf)paramObject).zzcfc == null) || (((zzgf)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgf)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i1 = 0;
    int i2 = getClass().getName().hashCode();
    int i3 = zzze.hashCode(this.zzawt);
    int i;
    int j;
    label42:
    int k;
    label51:
    int m;
    if (this.name == null)
    {
      i = 0;
      if (this.zzawu != null) {
        break label137;
      }
      j = 0;
      if (this.zzawv != null) {
        break label148;
      }
      k = 0;
      if (this.count != null) {
        break label159;
      }
      m = 0;
      label61:
      n = i1;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label171;
        }
      }
    }
    label137:
    label148:
    label159:
    label171:
    for (int n = i1;; n = this.zzcfc.hashCode())
    {
      return (m + (k + (j + (i + ((i2 + 527) * 31 + i3) * 31) * 31) * 31) * 31) * 31 + n;
      i = this.name.hashCode();
      break;
      j = this.zzawu.hashCode();
      break label42;
      k = this.zzawv.hashCode();
      break label51;
      m = this.count.hashCode();
      break label61;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if ((this.zzawt != null) && (this.zzawt.length > 0))
    {
      int i = 0;
      while (i < this.zzawt.length)
      {
        zzgg localzzgg = this.zzawt[i];
        if (localzzgg != null) {
          paramzzyy.zza(1, localzzgg);
        }
        i += 1;
      }
    }
    if (this.name != null) {
      paramzzyy.zzb(2, this.name);
    }
    if (this.zzawu != null) {
      paramzzyy.zzi(3, this.zzawu.longValue());
    }
    if (this.zzawv != null) {
      paramzzyy.zzi(4, this.zzawv.longValue());
    }
    if (this.count != null) {
      paramzzyy.zzd(5, this.count.intValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int i = super.zzf();
    int j = i;
    if (this.zzawt != null)
    {
      j = i;
      if (this.zzawt.length > 0)
      {
        int k = 0;
        for (;;)
        {
          j = i;
          if (k >= this.zzawt.length) {
            break;
          }
          zzgg localzzgg = this.zzawt[k];
          j = i;
          if (localzzgg != null) {
            j = i + zzyy.zzb(1, localzzgg);
          }
          k += 1;
          i = j;
        }
      }
    }
    i = j;
    if (this.name != null) {
      i = j + zzyy.zzc(2, this.name);
    }
    j = i;
    if (this.zzawu != null) {
      j = i + zzyy.zzd(3, this.zzawu.longValue());
    }
    i = j;
    if (this.zzawv != null) {
      i = j + zzyy.zzd(4, this.zzawv.longValue());
    }
    j = i;
    if (this.count != null) {
      j = i + zzyy.zzh(5, this.count.intValue());
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */