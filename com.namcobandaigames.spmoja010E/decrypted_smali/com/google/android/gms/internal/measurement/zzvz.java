package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map.Entry;

final class zzvz<K>
  implements Iterator<Map.Entry<K, Object>>
{
  private Iterator<Map.Entry<K, Object>> zzcac;
  
  public zzvz(Iterator<Map.Entry<K, Object>> paramIterator)
  {
    this.zzcac = paramIterator;
  }
  
  public final boolean hasNext()
  {
    return this.zzcac.hasNext();
  }
  
  public final void remove()
  {
    this.zzcac.remove();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */