package com.google.android.gms.internal.measurement;

final class zzua
{
  private static final Class<?> zzbtv = zzfu("libcore.io.Memory");
  private static final boolean zzbtw;
  
  static
  {
    if (zzfu("org.robolectric.Robolectric") != null) {}
    for (boolean bool = true;; bool = false)
    {
      zzbtw = bool;
      return;
    }
  }
  
  private static <T> Class<T> zzfu(String paramString)
  {
    try
    {
      paramString = Class.forName(paramString);
      return paramString;
    }
    catch (Throwable paramString) {}
    return null;
  }
  
  static boolean zzty()
  {
    return (zzbtv != null) && (!zzbtw);
  }
  
  static Class<?> zztz()
  {
    return zzbtv;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzua.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */