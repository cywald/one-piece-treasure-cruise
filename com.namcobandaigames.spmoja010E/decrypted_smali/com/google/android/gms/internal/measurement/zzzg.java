package com.google.android.gms.internal.measurement;

import java.io.IOException;

public abstract class zzzg
{
  protected volatile int zzcfm = -1;
  
  public static final <T extends zzzg> T zza(T paramT, byte[] paramArrayOfByte)
    throws zzzf
  {
    return zzb(paramT, paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public static final void zza(zzzg paramzzzg, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      paramArrayOfByte = zzyy.zzk(paramArrayOfByte, 0, paramInt2);
      paramzzzg.zza(paramArrayOfByte);
      paramArrayOfByte.zzyt();
      return;
    }
    catch (IOException paramzzzg)
    {
      throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", paramzzzg);
    }
  }
  
  private static final <T extends zzzg> T zzb(T paramT, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws zzzf
  {
    try
    {
      paramArrayOfByte = zzyx.zzj(paramArrayOfByte, 0, paramInt2);
      paramT.zza(paramArrayOfByte);
      paramArrayOfByte.zzan(0);
      return paramT;
    }
    catch (zzzf paramT)
    {
      throw paramT;
    }
    catch (IOException paramT)
    {
      throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).", paramT);
    }
  }
  
  public String toString()
  {
    return zzzh.zzc(this);
  }
  
  public abstract zzzg zza(zzyx paramzzyx)
    throws IOException;
  
  public void zza(zzyy paramzzyy)
    throws IOException
  {}
  
  protected int zzf()
  {
    return 0;
  }
  
  public final int zzvu()
  {
    int i = zzf();
    this.zzcfm = i;
    return i;
  }
  
  public zzzg zzyu()
    throws CloneNotSupportedException
  {
    return (zzzg)super.clone();
  }
  
  public final int zzza()
  {
    if (this.zzcfm < 0) {
      zzvu();
    }
    return this.zzcfm;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */