package com.google.android.gms.internal.measurement;

public enum zzb
{
  private final String zzno;
  
  private zzb(String paramString)
  {
    this.zzno = paramString;
  }
  
  public final String toString()
  {
    return this.zzno;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */