package com.google.android.gms.internal.measurement;

final class zzuk
{
  private final byte[] buffer;
  private final zzut zzbuf;
  
  private zzuk(int paramInt)
  {
    this.buffer = new byte[paramInt];
    this.zzbuf = zzut.zzj(this.buffer);
  }
  
  public final zzud zzue()
  {
    if (this.zzbuf.zzvg() != 0) {
      throw new IllegalStateException("Did not write as much data as expected.");
    }
    return new zzum(this.buffer);
  }
  
  public final zzut zzuf()
  {
    return this.zzbuf;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */