package com.google.android.gms.internal.measurement;

public enum zza
{
  private final String name;
  
  private zza(String paramString)
  {
    this.name = paramString;
  }
  
  public final String toString()
  {
    return this.name;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */