package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzl
  extends zzza<zzl>
{
  public String version = "";
  private String[] zzos = zzzj.zzcfv;
  public String[] zzot = zzzj.zzcfv;
  public zzp[] zzou = zzp.zzk();
  public zzk[] zzov = zzk.zzh();
  public zzh[] zzow = zzh.zze();
  public zzh[] zzox = zzh.zze();
  public zzh[] zzoy = zzh.zze();
  public zzm[] zzoz = zzm.zzi();
  private String zzpa = "";
  private String zzpb = "";
  private String zzpc = "0";
  private zzc.zza zzpd = null;
  private float zzpe = 0.0F;
  private boolean zzpf = false;
  private String[] zzpg = zzzj.zzcfv;
  public int zzph = 0;
  
  public zzl()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzl)) {
        return false;
      }
      paramObject = (zzl)paramObject;
      if (!zzze.equals(this.zzos, ((zzl)paramObject).zzos)) {
        return false;
      }
      if (!zzze.equals(this.zzot, ((zzl)paramObject).zzot)) {
        return false;
      }
      if (!zzze.equals(this.zzou, ((zzl)paramObject).zzou)) {
        return false;
      }
      if (!zzze.equals(this.zzov, ((zzl)paramObject).zzov)) {
        return false;
      }
      if (!zzze.equals(this.zzow, ((zzl)paramObject).zzow)) {
        return false;
      }
      if (!zzze.equals(this.zzox, ((zzl)paramObject).zzox)) {
        return false;
      }
      if (!zzze.equals(this.zzoy, ((zzl)paramObject).zzoy)) {
        return false;
      }
      if (!zzze.equals(this.zzoz, ((zzl)paramObject).zzoz)) {
        return false;
      }
      if (this.zzpa == null)
      {
        if (((zzl)paramObject).zzpa != null) {
          return false;
        }
      }
      else if (!this.zzpa.equals(((zzl)paramObject).zzpa)) {
        return false;
      }
      if (this.zzpb == null)
      {
        if (((zzl)paramObject).zzpb != null) {
          return false;
        }
      }
      else if (!this.zzpb.equals(((zzl)paramObject).zzpb)) {
        return false;
      }
      if (this.zzpc == null)
      {
        if (((zzl)paramObject).zzpc != null) {
          return false;
        }
      }
      else if (!this.zzpc.equals(((zzl)paramObject).zzpc)) {
        return false;
      }
      if (this.version == null)
      {
        if (((zzl)paramObject).version != null) {
          return false;
        }
      }
      else if (!this.version.equals(((zzl)paramObject).version)) {
        return false;
      }
      if (this.zzpd == null)
      {
        if (((zzl)paramObject).zzpd != null) {
          return false;
        }
      }
      else if (!this.zzpd.equals(((zzl)paramObject).zzpd)) {
        return false;
      }
      if (Float.floatToIntBits(this.zzpe) != Float.floatToIntBits(((zzl)paramObject).zzpe)) {
        return false;
      }
      if (this.zzpf != ((zzl)paramObject).zzpf) {
        return false;
      }
      if (!zzze.equals(this.zzpg, ((zzl)paramObject).zzpg)) {
        return false;
      }
      if (this.zzph != ((zzl)paramObject).zzph) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzl)paramObject).zzcfc == null) || (((zzl)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzl)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i3 = 0;
    int i4 = getClass().getName().hashCode();
    int i5 = zzze.hashCode(this.zzos);
    int i6 = zzze.hashCode(this.zzot);
    int i7 = zzze.hashCode(this.zzou);
    int i8 = zzze.hashCode(this.zzov);
    int i9 = zzze.hashCode(this.zzow);
    int i10 = zzze.hashCode(this.zzox);
    int i11 = zzze.hashCode(this.zzoy);
    int i12 = zzze.hashCode(this.zzoz);
    int i;
    int j;
    label105:
    int k;
    label114:
    int m;
    label124:
    zzc.zza localzza;
    int n;
    label138:
    int i13;
    int i1;
    label159:
    int i14;
    int i15;
    if (this.zzpa == null)
    {
      i = 0;
      if (this.zzpb != null) {
        break label322;
      }
      j = 0;
      if (this.zzpc != null) {
        break label333;
      }
      k = 0;
      if (this.version != null) {
        break label344;
      }
      m = 0;
      localzza = this.zzpd;
      if (localzza != null) {
        break label356;
      }
      n = 0;
      i13 = Float.floatToIntBits(this.zzpe);
      if (!this.zzpf) {
        break label366;
      }
      i1 = 1231;
      i14 = zzze.hashCode(this.zzpg);
      i15 = this.zzph;
      i2 = i3;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label374;
        }
      }
    }
    label322:
    label333:
    label344:
    label356:
    label366:
    label374:
    for (int i2 = i3;; i2 = this.zzcfc.hashCode())
    {
      return (((i1 + ((n + (m + (k + (j + (i + (((((((((i4 + 527) * 31 + i5) * 31 + i6) * 31 + i7) * 31 + i8) * 31 + i9) * 31 + i10) * 31 + i11) * 31 + i12) * 31) * 31) * 31) * 31) * 31) * 31 + i13) * 31) * 31 + i14) * 31 + i15) * 31 + i2;
      i = this.zzpa.hashCode();
      break;
      j = this.zzpb.hashCode();
      break label105;
      k = this.zzpc.hashCode();
      break label114;
      m = this.version.hashCode();
      break label124;
      n = localzza.hashCode();
      break label138;
      i1 = 1237;
      break label159;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    int j = 0;
    int i;
    Object localObject;
    if ((this.zzot != null) && (this.zzot.length > 0))
    {
      i = 0;
      while (i < this.zzot.length)
      {
        localObject = this.zzot[i];
        if (localObject != null) {
          paramzzyy.zzb(1, (String)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzou != null) && (this.zzou.length > 0))
    {
      i = 0;
      while (i < this.zzou.length)
      {
        localObject = this.zzou[i];
        if (localObject != null) {
          paramzzyy.zza(2, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzov != null) && (this.zzov.length > 0))
    {
      i = 0;
      while (i < this.zzov.length)
      {
        localObject = this.zzov[i];
        if (localObject != null) {
          paramzzyy.zza(3, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzow != null) && (this.zzow.length > 0))
    {
      i = 0;
      while (i < this.zzow.length)
      {
        localObject = this.zzow[i];
        if (localObject != null) {
          paramzzyy.zza(4, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzox != null) && (this.zzox.length > 0))
    {
      i = 0;
      while (i < this.zzox.length)
      {
        localObject = this.zzox[i];
        if (localObject != null) {
          paramzzyy.zza(5, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzoy != null) && (this.zzoy.length > 0))
    {
      i = 0;
      while (i < this.zzoy.length)
      {
        localObject = this.zzoy[i];
        if (localObject != null) {
          paramzzyy.zza(6, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzoz != null) && (this.zzoz.length > 0))
    {
      i = 0;
      while (i < this.zzoz.length)
      {
        localObject = this.zzoz[i];
        if (localObject != null) {
          paramzzyy.zza(7, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzpa != null) && (!this.zzpa.equals(""))) {
      paramzzyy.zzb(9, this.zzpa);
    }
    if ((this.zzpb != null) && (!this.zzpb.equals(""))) {
      paramzzyy.zzb(10, this.zzpb);
    }
    if ((this.zzpc != null) && (!this.zzpc.equals("0"))) {
      paramzzyy.zzb(12, this.zzpc);
    }
    if ((this.version != null) && (!this.version.equals(""))) {
      paramzzyy.zzb(13, this.version);
    }
    if (this.zzpd != null) {
      paramzzyy.zze(14, this.zzpd);
    }
    if (Float.floatToIntBits(this.zzpe) != Float.floatToIntBits(0.0F)) {
      paramzzyy.zza(15, this.zzpe);
    }
    if ((this.zzpg != null) && (this.zzpg.length > 0))
    {
      i = 0;
      while (i < this.zzpg.length)
      {
        localObject = this.zzpg[i];
        if (localObject != null) {
          paramzzyy.zzb(16, (String)localObject);
        }
        i += 1;
      }
    }
    if (this.zzph != 0) {
      paramzzyy.zzd(17, this.zzph);
    }
    if (this.zzpf) {
      paramzzyy.zzb(18, this.zzpf);
    }
    if ((this.zzos != null) && (this.zzos.length > 0))
    {
      i = j;
      while (i < this.zzos.length)
      {
        localObject = this.zzos[i];
        if (localObject != null) {
          paramzzyy.zzb(19, (String)localObject);
        }
        i += 1;
      }
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int i2 = 0;
    int i1 = super.zzf();
    int i;
    int k;
    Object localObject;
    int n;
    int m;
    if ((this.zzot != null) && (this.zzot.length > 0))
    {
      i = 0;
      j = 0;
      for (k = 0; i < this.zzot.length; k = m)
      {
        localObject = this.zzot[i];
        n = j;
        m = k;
        if (localObject != null)
        {
          m = k + 1;
          n = j + zzyy.zzfx((String)localObject);
        }
        i += 1;
        j = n;
      }
    }
    for (int j = i1 + j + k * 1;; j = i1)
    {
      i = j;
      if (this.zzou != null)
      {
        i = j;
        if (this.zzou.length > 0)
        {
          i = j;
          j = 0;
          while (j < this.zzou.length)
          {
            localObject = this.zzou[j];
            k = i;
            if (localObject != null) {
              k = i + zzyy.zzb(2, (zzzg)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      j = i;
      if (this.zzov != null)
      {
        j = i;
        if (this.zzov.length > 0)
        {
          j = 0;
          while (j < this.zzov.length)
          {
            localObject = this.zzov[j];
            k = i;
            if (localObject != null) {
              k = i + zzyy.zzb(3, (zzzg)localObject);
            }
            j += 1;
            i = k;
          }
          j = i;
        }
      }
      i = j;
      if (this.zzow != null)
      {
        i = j;
        if (this.zzow.length > 0)
        {
          i = j;
          j = 0;
          while (j < this.zzow.length)
          {
            localObject = this.zzow[j];
            k = i;
            if (localObject != null) {
              k = i + zzyy.zzb(4, (zzzg)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      j = i;
      if (this.zzox != null)
      {
        j = i;
        if (this.zzox.length > 0)
        {
          j = 0;
          while (j < this.zzox.length)
          {
            localObject = this.zzox[j];
            k = i;
            if (localObject != null) {
              k = i + zzyy.zzb(5, (zzzg)localObject);
            }
            j += 1;
            i = k;
          }
          j = i;
        }
      }
      i = j;
      if (this.zzoy != null)
      {
        i = j;
        if (this.zzoy.length > 0)
        {
          i = j;
          j = 0;
          while (j < this.zzoy.length)
          {
            localObject = this.zzoy[j];
            k = i;
            if (localObject != null) {
              k = i + zzyy.zzb(6, (zzzg)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      j = i;
      if (this.zzoz != null)
      {
        j = i;
        if (this.zzoz.length > 0)
        {
          j = 0;
          while (j < this.zzoz.length)
          {
            localObject = this.zzoz[j];
            k = i;
            if (localObject != null) {
              k = i + zzyy.zzb(7, (zzzg)localObject);
            }
            j += 1;
            i = k;
          }
          j = i;
        }
      }
      i = j;
      if (this.zzpa != null)
      {
        i = j;
        if (!this.zzpa.equals("")) {
          i = j + zzyy.zzc(9, this.zzpa);
        }
      }
      j = i;
      if (this.zzpb != null)
      {
        j = i;
        if (!this.zzpb.equals("")) {
          j = i + zzyy.zzc(10, this.zzpb);
        }
      }
      i = j;
      if (this.zzpc != null)
      {
        i = j;
        if (!this.zzpc.equals("0")) {
          i = j + zzyy.zzc(12, this.zzpc);
        }
      }
      j = i;
      if (this.version != null)
      {
        j = i;
        if (!this.version.equals("")) {
          j = i + zzyy.zzc(13, this.version);
        }
      }
      k = j;
      if (this.zzpd != null) {
        k = j + zzut.zzc(14, this.zzpd);
      }
      i = k;
      if (Float.floatToIntBits(this.zzpe) != Float.floatToIntBits(0.0F)) {
        i = k + (zzyy.zzbb(15) + 4);
      }
      j = i;
      if (this.zzpg != null)
      {
        j = i;
        if (this.zzpg.length > 0)
        {
          j = 0;
          k = 0;
          for (m = 0; j < this.zzpg.length; m = n)
          {
            localObject = this.zzpg[j];
            i1 = k;
            n = m;
            if (localObject != null)
            {
              n = m + 1;
              i1 = k + zzyy.zzfx((String)localObject);
            }
            j += 1;
            k = i1;
          }
          j = i + k + m * 2;
        }
      }
      k = j;
      if (this.zzph != 0) {
        k = j + zzyy.zzh(17, this.zzph);
      }
      i = k;
      if (this.zzpf) {
        i = k + (zzyy.zzbb(18) + 1);
      }
      j = i;
      if (this.zzos != null)
      {
        j = i;
        if (this.zzos.length > 0)
        {
          k = 0;
          m = 0;
          j = i2;
          while (j < this.zzos.length)
          {
            localObject = this.zzos[j];
            i1 = k;
            n = m;
            if (localObject != null)
            {
              n = m + 1;
              i1 = k + zzyy.zzfx((String)localObject);
            }
            j += 1;
            k = i1;
            m = n;
          }
          j = i + k + m * 2;
        }
      }
      return j;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */