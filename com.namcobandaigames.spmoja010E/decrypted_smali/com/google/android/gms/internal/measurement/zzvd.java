package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

final class zzvd<FieldDescriptorType extends zzvf<FieldDescriptorType>>
{
  private static final zzvd zzbvs = new zzvd(true);
  private boolean zzbpo;
  private final zzxm<FieldDescriptorType, Object> zzbvq;
  private boolean zzbvr = false;
  
  private zzvd()
  {
    this.zzbvq = zzxm.zzbt(16);
  }
  
  private zzvd(boolean paramBoolean)
  {
    this.zzbvq = zzxm.zzbt(0);
    zzsm();
  }
  
  static int zza(zzyq paramzzyq, int paramInt, Object paramObject)
  {
    paramInt = zzut.zzbb(paramInt);
    if (paramzzyq == zzyq.zzcdz)
    {
      zzvo.zzf((zzwt)paramObject);
      paramInt <<= 1;
    }
    for (;;)
    {
      return paramInt + zzb(paramzzyq, paramObject);
    }
  }
  
  private final Object zza(FieldDescriptorType paramFieldDescriptorType)
  {
    Object localObject = this.zzbvq.get(paramFieldDescriptorType);
    paramFieldDescriptorType = (FieldDescriptorType)localObject;
    if ((localObject instanceof zzvw)) {
      paramFieldDescriptorType = zzvw.zzwt();
    }
    return paramFieldDescriptorType;
  }
  
  static void zza(zzut paramzzut, zzyq paramzzyq, int paramInt, Object paramObject)
    throws IOException
  {
    if (paramzzyq == zzyq.zzcdz)
    {
      zzvo.zzf((zzwt)paramObject);
      paramzzyq = (zzwt)paramObject;
      paramzzut.zzc(paramInt, 3);
      paramzzyq.zzb(paramzzut);
      paramzzut.zzc(paramInt, 4);
      return;
    }
    paramzzut.zzc(paramInt, paramzzyq.zzyq());
    switch (zzve.zzbuu[paramzzyq.ordinal()])
    {
    default: 
      return;
    case 1: 
      paramzzut.zzb(((Double)paramObject).doubleValue());
      return;
    case 2: 
      paramzzut.zza(((Float)paramObject).floatValue());
      return;
    case 3: 
      paramzzut.zzav(((Long)paramObject).longValue());
      return;
    case 4: 
      paramzzut.zzav(((Long)paramObject).longValue());
      return;
    case 5: 
      paramzzut.zzax(((Integer)paramObject).intValue());
      return;
    case 6: 
      paramzzut.zzax(((Long)paramObject).longValue());
      return;
    case 7: 
      paramzzut.zzba(((Integer)paramObject).intValue());
      return;
    case 8: 
      paramzzut.zzu(((Boolean)paramObject).booleanValue());
      return;
    case 9: 
      ((zzwt)paramObject).zzb(paramzzut);
      return;
    case 10: 
      paramzzut.zzb((zzwt)paramObject);
      return;
    case 11: 
      if ((paramObject instanceof zzud))
      {
        paramzzut.zza((zzud)paramObject);
        return;
      }
      paramzzut.zzfw((String)paramObject);
      return;
    case 12: 
      if ((paramObject instanceof zzud))
      {
        paramzzut.zza((zzud)paramObject);
        return;
      }
      paramzzyq = (byte[])paramObject;
      paramzzut.zze(paramzzyq, 0, paramzzyq.length);
      return;
    case 13: 
      paramzzut.zzay(((Integer)paramObject).intValue());
      return;
    case 14: 
      paramzzut.zzba(((Integer)paramObject).intValue());
      return;
    case 15: 
      paramzzut.zzax(((Long)paramObject).longValue());
      return;
    case 16: 
      paramzzut.zzaz(((Integer)paramObject).intValue());
      return;
    case 17: 
      paramzzut.zzaw(((Long)paramObject).longValue());
      return;
    }
    if ((paramObject instanceof zzvp))
    {
      paramzzut.zzax(((zzvp)paramObject).zzc());
      return;
    }
    paramzzut.zzax(((Integer)paramObject).intValue());
  }
  
  private final void zza(FieldDescriptorType paramFieldDescriptorType, Object paramObject)
  {
    if (paramFieldDescriptorType.zzvy())
    {
      if (!(paramObject instanceof List)) {
        throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
      }
      ArrayList localArrayList1 = new ArrayList();
      localArrayList1.addAll((List)paramObject);
      ArrayList localArrayList2 = (ArrayList)localArrayList1;
      int j = localArrayList2.size();
      int i = 0;
      for (;;)
      {
        paramObject = localArrayList1;
        if (i >= j) {
          break;
        }
        paramObject = localArrayList2.get(i);
        i += 1;
        zza(paramFieldDescriptorType.zzvw(), paramObject);
      }
    }
    zza(paramFieldDescriptorType.zzvw(), paramObject);
    if ((paramObject instanceof zzvw)) {
      this.zzbvr = true;
    }
    this.zzbvq.zza(paramFieldDescriptorType, paramObject);
  }
  
  private static void zza(zzyq paramzzyq, Object paramObject)
  {
    boolean bool = false;
    zzvo.checkNotNull(paramObject);
    switch (zzve.zzbvt[paramzzyq.zzyp().ordinal()])
    {
    }
    while (!bool)
    {
      throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
      bool = paramObject instanceof Integer;
      continue;
      bool = paramObject instanceof Long;
      continue;
      bool = paramObject instanceof Float;
      continue;
      bool = paramObject instanceof Double;
      continue;
      bool = paramObject instanceof Boolean;
      continue;
      bool = paramObject instanceof String;
      continue;
      if (((paramObject instanceof zzud)) || ((paramObject instanceof byte[])))
      {
        bool = true;
        continue;
        if (((paramObject instanceof Integer)) || ((paramObject instanceof zzvp)))
        {
          bool = true;
          continue;
          if (((paramObject instanceof zzwt)) || ((paramObject instanceof zzvw))) {
            bool = true;
          }
        }
      }
    }
  }
  
  private static int zzb(zzvf<?> paramzzvf, Object paramObject)
  {
    int j = 0;
    int i = 0;
    zzyq localzzyq = paramzzvf.zzvw();
    int k = paramzzvf.zzc();
    if (paramzzvf.zzvy())
    {
      if (paramzzvf.zzvz())
      {
        paramzzvf = ((List)paramObject).iterator();
        while (paramzzvf.hasNext()) {
          i += zzb(localzzyq, paramzzvf.next());
        }
        j = zzut.zzbb(k);
        j = zzut.zzbj(i) + (j + i);
        return j;
      }
      paramzzvf = ((List)paramObject).iterator();
      i = j;
      for (;;)
      {
        j = i;
        if (!paramzzvf.hasNext()) {
          break;
        }
        i += zza(localzzyq, k, paramzzvf.next());
      }
    }
    return zza(localzzyq, k, paramObject);
  }
  
  private static int zzb(zzyq paramzzyq, Object paramObject)
  {
    switch (zzve.zzbuu[paramzzyq.ordinal()])
    {
    default: 
      throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
    case 1: 
      return zzut.zzc(((Double)paramObject).doubleValue());
    case 2: 
      return zzut.zzb(((Float)paramObject).floatValue());
    case 3: 
      return zzut.zzay(((Long)paramObject).longValue());
    case 4: 
      return zzut.zzaz(((Long)paramObject).longValue());
    case 5: 
      return zzut.zzbc(((Integer)paramObject).intValue());
    case 6: 
      return zzut.zzbb(((Long)paramObject).longValue());
    case 7: 
      return zzut.zzbf(((Integer)paramObject).intValue());
    case 8: 
      return zzut.zzv(((Boolean)paramObject).booleanValue());
    case 9: 
      return zzut.zzd((zzwt)paramObject);
    case 12: 
      if ((paramObject instanceof zzud)) {
        return zzut.zzb((zzud)paramObject);
      }
      return zzut.zzk((byte[])paramObject);
    case 11: 
      if ((paramObject instanceof zzud)) {
        return zzut.zzb((zzud)paramObject);
      }
      return zzut.zzfx((String)paramObject);
    case 13: 
      return zzut.zzbd(((Integer)paramObject).intValue());
    case 14: 
      return zzut.zzbg(((Integer)paramObject).intValue());
    case 15: 
      return zzut.zzbc(((Long)paramObject).longValue());
    case 16: 
      return zzut.zzbe(((Integer)paramObject).intValue());
    case 17: 
      return zzut.zzba(((Long)paramObject).longValue());
    case 10: 
      if ((paramObject instanceof zzvw)) {
        return zzut.zza((zzvw)paramObject);
      }
      return zzut.zzc((zzwt)paramObject);
    }
    if ((paramObject instanceof zzvp)) {
      return zzut.zzbh(((zzvp)paramObject).zzc());
    }
    return zzut.zzbh(((Integer)paramObject).intValue());
  }
  
  private static boolean zzc(Map.Entry<FieldDescriptorType, Object> paramEntry)
  {
    zzvf localzzvf = (zzvf)paramEntry.getKey();
    if (localzzvf.zzvx() == zzyv.zzcet)
    {
      if (localzzvf.zzvy())
      {
        paramEntry = ((List)paramEntry.getValue()).iterator();
        do
        {
          if (!paramEntry.hasNext()) {
            break;
          }
        } while (((zzwt)paramEntry.next()).isInitialized());
        return false;
      }
      paramEntry = paramEntry.getValue();
      if ((paramEntry instanceof zzwt))
      {
        if (!((zzwt)paramEntry).isInitialized()) {
          return false;
        }
      }
      else
      {
        if ((paramEntry instanceof zzvw)) {
          return true;
        }
        throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
      }
    }
    return true;
  }
  
  private final void zzd(Map.Entry<FieldDescriptorType, Object> paramEntry)
  {
    zzvf localzzvf = (zzvf)paramEntry.getKey();
    Object localObject1 = paramEntry.getValue();
    paramEntry = (Map.Entry<FieldDescriptorType, Object>)localObject1;
    if ((localObject1 instanceof zzvw)) {
      paramEntry = zzvw.zzwt();
    }
    if (localzzvf.zzvy())
    {
      Object localObject2 = zza(localzzvf);
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = new ArrayList();
      }
      paramEntry = ((List)paramEntry).iterator();
      while (paramEntry.hasNext())
      {
        localObject2 = paramEntry.next();
        ((List)localObject1).add(zzv(localObject2));
      }
      this.zzbvq.zza(localzzvf, localObject1);
      return;
    }
    if (localzzvf.zzvx() == zzyv.zzcet)
    {
      localObject1 = zza(localzzvf);
      if (localObject1 == null)
      {
        this.zzbvq.zza(localzzvf, zzv(paramEntry));
        return;
      }
      if ((localObject1 instanceof zzwz)) {}
      for (paramEntry = localzzvf.zza((zzwz)localObject1, (zzwz)paramEntry);; paramEntry = localzzvf.zza(((zzwt)localObject1).zzwd(), (zzwt)paramEntry).zzwj())
      {
        this.zzbvq.zza(localzzvf, paramEntry);
        return;
      }
    }
    this.zzbvq.zza(localzzvf, zzv(paramEntry));
  }
  
  private static int zze(Map.Entry<FieldDescriptorType, Object> paramEntry)
  {
    zzvf localzzvf = (zzvf)paramEntry.getKey();
    Object localObject = paramEntry.getValue();
    if ((localzzvf.zzvx() == zzyv.zzcet) && (!localzzvf.zzvy()) && (!localzzvf.zzvz()))
    {
      if ((localObject instanceof zzvw)) {
        return zzut.zzb(((zzvf)paramEntry.getKey()).zzc(), (zzvw)localObject);
      }
      return zzut.zzd(((zzvf)paramEntry.getKey()).zzc(), (zzwt)localObject);
    }
    return zzb(localzzvf, localObject);
  }
  
  private static Object zzv(Object paramObject)
  {
    if ((paramObject instanceof zzwz)) {
      localObject = ((zzwz)paramObject).zzxj();
    }
    do
    {
      return localObject;
      localObject = paramObject;
    } while (!(paramObject instanceof byte[]));
    paramObject = (byte[])paramObject;
    Object localObject = new byte[paramObject.length];
    System.arraycopy(paramObject, 0, localObject, 0, paramObject.length);
    return localObject;
  }
  
  public static <T extends zzvf<T>> zzvd<T> zzvt()
  {
    return zzbvs;
  }
  
  final Iterator<Map.Entry<FieldDescriptorType, Object>> descendingIterator()
  {
    if (this.zzbvr) {
      return new zzvz(this.zzbvq.zzxy().iterator());
    }
    return this.zzbvq.zzxy().iterator();
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof zzvd)) {
      return false;
    }
    paramObject = (zzvd)paramObject;
    return this.zzbvq.equals(((zzvd)paramObject).zzbvq);
  }
  
  public final int hashCode()
  {
    return this.zzbvq.hashCode();
  }
  
  final boolean isEmpty()
  {
    return this.zzbvq.isEmpty();
  }
  
  public final boolean isImmutable()
  {
    return this.zzbpo;
  }
  
  public final boolean isInitialized()
  {
    int i = 0;
    while (i < this.zzbvq.zzxw())
    {
      if (!zzc(this.zzbvq.zzbu(i))) {
        return false;
      }
      i += 1;
    }
    Iterator localIterator = this.zzbvq.zzxx().iterator();
    while (localIterator.hasNext()) {
      if (!zzc((Map.Entry)localIterator.next())) {
        return false;
      }
    }
    return true;
  }
  
  public final Iterator<Map.Entry<FieldDescriptorType, Object>> iterator()
  {
    if (this.zzbvr) {
      return new zzvz(this.zzbvq.entrySet().iterator());
    }
    return this.zzbvq.entrySet().iterator();
  }
  
  public final void zza(zzvd<FieldDescriptorType> paramzzvd)
  {
    int i = 0;
    while (i < paramzzvd.zzbvq.zzxw())
    {
      zzd(paramzzvd.zzbvq.zzbu(i));
      i += 1;
    }
    paramzzvd = paramzzvd.zzbvq.zzxx().iterator();
    while (paramzzvd.hasNext()) {
      zzd((Map.Entry)paramzzvd.next());
    }
  }
  
  public final void zzsm()
  {
    if (this.zzbpo) {
      return;
    }
    this.zzbvq.zzsm();
    this.zzbpo = true;
  }
  
  public final int zzvu()
  {
    int j = 0;
    int i = 0;
    while (j < this.zzbvq.zzxw())
    {
      localObject = this.zzbvq.zzbu(j);
      i += zzb((zzvf)((Map.Entry)localObject).getKey(), ((Map.Entry)localObject).getValue());
      j += 1;
    }
    Object localObject = this.zzbvq.zzxx().iterator();
    while (((Iterator)localObject).hasNext())
    {
      Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
      i += zzb((zzvf)localEntry.getKey(), localEntry.getValue());
    }
    return i;
  }
  
  public final int zzvv()
  {
    int j = 0;
    int i = 0;
    while (j < this.zzbvq.zzxw())
    {
      i += zze(this.zzbvq.zzbu(j));
      j += 1;
    }
    Iterator localIterator = this.zzbvq.zzxx().iterator();
    while (localIterator.hasNext()) {
      i += zze((Map.Entry)localIterator.next());
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */