package com.google.android.gms.internal.measurement;

import java.io.IOException;

public abstract class zzuo
{
  int zzbuh;
  int zzbui = 100;
  private int zzbuj = Integer.MAX_VALUE;
  zzur zzbuk;
  private boolean zzbul = false;
  
  static zzuo zza(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    paramArrayOfByte = new zzuq(paramArrayOfByte, paramInt1, paramInt2, false, null);
    try
    {
      paramArrayOfByte.zzaq(paramInt2);
      return paramArrayOfByte;
    }
    catch (zzvt paramArrayOfByte)
    {
      throw new IllegalArgumentException(paramArrayOfByte);
    }
  }
  
  public static zzuo zzd(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return zza(paramArrayOfByte, paramInt1, paramInt2, false);
  }
  
  public abstract double readDouble()
    throws IOException;
  
  public abstract float readFloat()
    throws IOException;
  
  public abstract String readString()
    throws IOException;
  
  public abstract <T extends zzwt> T zza(zzxd<T> paramzzxd, zzuz paramzzuz)
    throws IOException;
  
  public abstract void zzan(int paramInt)
    throws zzvt;
  
  public abstract boolean zzao(int paramInt)
    throws IOException;
  
  public final int zzap(int paramInt)
  {
    if (paramInt < 0) {
      throw new IllegalArgumentException(47 + "Recursion limit cannot be negative: " + paramInt);
    }
    int i = this.zzbui;
    this.zzbui = paramInt;
    return i;
  }
  
  public abstract int zzaq(int paramInt)
    throws zzvt;
  
  public abstract void zzar(int paramInt);
  
  public abstract void zzas(int paramInt)
    throws IOException;
  
  public abstract int zzug()
    throws IOException;
  
  public abstract long zzuh()
    throws IOException;
  
  public abstract long zzui()
    throws IOException;
  
  public abstract int zzuj()
    throws IOException;
  
  public abstract long zzuk()
    throws IOException;
  
  public abstract int zzul()
    throws IOException;
  
  public abstract boolean zzum()
    throws IOException;
  
  public abstract String zzun()
    throws IOException;
  
  public abstract zzud zzuo()
    throws IOException;
  
  public abstract int zzup()
    throws IOException;
  
  public abstract int zzuq()
    throws IOException;
  
  public abstract int zzur()
    throws IOException;
  
  public abstract long zzus()
    throws IOException;
  
  public abstract int zzut()
    throws IOException;
  
  public abstract long zzuu()
    throws IOException;
  
  abstract long zzuv()
    throws IOException;
  
  public abstract boolean zzuw()
    throws IOException;
  
  public abstract int zzux();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */