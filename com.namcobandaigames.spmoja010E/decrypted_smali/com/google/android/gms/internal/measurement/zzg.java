package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzg
{
  public static final class zza
    extends zzza<zza>
  {
    public static final zzzb<zzp, zza> zzpt = zzzb.zza(11, zza.class, 810L);
    private static final zza[] zzpu = new zza[0];
    public int[] zzpv = zzzj.zzcax;
    public int[] zzpw = zzzj.zzcax;
    public int[] zzpx = zzzj.zzcax;
    private int zzpy = 0;
    public int[] zzpz = zzzj.zzcax;
    public int zzqa = 0;
    private int zzqb = 0;
    
    public zza()
    {
      this.zzcfc = null;
      this.zzcfm = -1;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof zza)) {
          return false;
        }
        paramObject = (zza)paramObject;
        if (!zzze.equals(this.zzpv, ((zza)paramObject).zzpv)) {
          return false;
        }
        if (!zzze.equals(this.zzpw, ((zza)paramObject).zzpw)) {
          return false;
        }
        if (!zzze.equals(this.zzpx, ((zza)paramObject).zzpx)) {
          return false;
        }
        if (this.zzpy != ((zza)paramObject).zzpy) {
          return false;
        }
        if (!zzze.equals(this.zzpz, ((zza)paramObject).zzpz)) {
          return false;
        }
        if (this.zzqa != ((zza)paramObject).zzqa) {
          return false;
        }
        if (this.zzqb != ((zza)paramObject).zzqb) {
          return false;
        }
        if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
          break;
        }
      } while ((((zza)paramObject).zzcfc == null) || (((zza)paramObject).zzcfc.isEmpty()));
      return false;
      return this.zzcfc.equals(((zza)paramObject).zzcfc);
    }
    
    public final int hashCode()
    {
      int j = getClass().getName().hashCode();
      int k = zzze.hashCode(this.zzpv);
      int m = zzze.hashCode(this.zzpw);
      int n = zzze.hashCode(this.zzpx);
      int i1 = this.zzpy;
      int i2 = zzze.hashCode(this.zzpz);
      int i3 = this.zzqa;
      int i4 = this.zzqb;
      if ((this.zzcfc == null) || (this.zzcfc.isEmpty())) {}
      for (int i = 0;; i = this.zzcfc.hashCode()) {
        return i + ((((((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31;
      }
    }
    
    public final void zza(zzyy paramzzyy)
      throws IOException
    {
      int j = 0;
      int i;
      if ((this.zzpv != null) && (this.zzpv.length > 0))
      {
        i = 0;
        while (i < this.zzpv.length)
        {
          paramzzyy.zzd(1, this.zzpv[i]);
          i += 1;
        }
      }
      if ((this.zzpw != null) && (this.zzpw.length > 0))
      {
        i = 0;
        while (i < this.zzpw.length)
        {
          paramzzyy.zzd(2, this.zzpw[i]);
          i += 1;
        }
      }
      if ((this.zzpx != null) && (this.zzpx.length > 0))
      {
        i = 0;
        while (i < this.zzpx.length)
        {
          paramzzyy.zzd(3, this.zzpx[i]);
          i += 1;
        }
      }
      if (this.zzpy != 0) {
        paramzzyy.zzd(4, this.zzpy);
      }
      if ((this.zzpz != null) && (this.zzpz.length > 0))
      {
        i = j;
        while (i < this.zzpz.length)
        {
          paramzzyy.zzd(5, this.zzpz[i]);
          i += 1;
        }
      }
      if (this.zzqa != 0) {
        paramzzyy.zzd(6, this.zzqa);
      }
      if (this.zzqb != 0) {
        paramzzyy.zzd(7, this.zzqb);
      }
      super.zza(paramzzyy);
    }
    
    protected final int zzf()
    {
      int m = 0;
      int k = super.zzf();
      int i;
      if ((this.zzpv != null) && (this.zzpv.length > 0))
      {
        i = 0;
        j = 0;
        while (i < this.zzpv.length)
        {
          j += zzyy.zzbc(this.zzpv[i]);
          i += 1;
        }
      }
      for (int j = k + j + this.zzpv.length * 1;; j = k)
      {
        i = j;
        if (this.zzpw != null)
        {
          i = j;
          if (this.zzpw.length > 0)
          {
            i = 0;
            k = 0;
            while (i < this.zzpw.length)
            {
              k += zzyy.zzbc(this.zzpw[i]);
              i += 1;
            }
            i = j + k + this.zzpw.length * 1;
          }
        }
        j = i;
        if (this.zzpx != null)
        {
          j = i;
          if (this.zzpx.length > 0)
          {
            j = 0;
            k = 0;
            while (j < this.zzpx.length)
            {
              k += zzyy.zzbc(this.zzpx[j]);
              j += 1;
            }
            j = i + k + this.zzpx.length * 1;
          }
        }
        i = j;
        if (this.zzpy != 0) {
          i = j + zzyy.zzh(4, this.zzpy);
        }
        j = i;
        if (this.zzpz != null)
        {
          j = i;
          if (this.zzpz.length > 0)
          {
            k = 0;
            j = m;
            while (j < this.zzpz.length)
            {
              k += zzyy.zzbc(this.zzpz[j]);
              j += 1;
            }
            j = i + k + this.zzpz.length * 1;
          }
        }
        i = j;
        if (this.zzqa != 0) {
          i = j + zzyy.zzh(6, this.zzqa);
        }
        j = i;
        if (this.zzqb != 0) {
          j = i + zzyy.zzh(7, this.zzqb);
        }
        return j;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */