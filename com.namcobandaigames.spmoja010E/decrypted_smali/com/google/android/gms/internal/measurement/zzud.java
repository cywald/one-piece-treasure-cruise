package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Comparator;

public abstract class zzud
  implements Serializable, Iterable<Byte>
{
  public static final zzud zzbtz = new zzum(zzvo.zzbzj);
  private static final zzui zzbua;
  private static final Comparator<zzud> zzbub;
  private int zzbry = 0;
  
  static
  {
    if (zzua.zzty()) {}
    for (Object localObject = new zzun(null);; localObject = new zzug(null))
    {
      zzbua = (zzui)localObject;
      zzbub = new zzuf();
      return;
    }
  }
  
  private static int zza(byte paramByte)
  {
    return paramByte & 0xFF;
  }
  
  static zzuk zzam(int paramInt)
  {
    return new zzuk(paramInt, null);
  }
  
  static int zzb(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = paramInt2 - paramInt1;
    if ((paramInt1 | paramInt2 | i | paramInt3 - paramInt2) < 0)
    {
      if (paramInt1 < 0) {
        throw new IndexOutOfBoundsException(32 + "Beginning index: " + paramInt1 + " < 0");
      }
      if (paramInt2 < paramInt1) {
        throw new IndexOutOfBoundsException(66 + "Beginning index larger than ending index: " + paramInt1 + ", " + paramInt2);
      }
      throw new IndexOutOfBoundsException(37 + "End index: " + paramInt2 + " >= " + paramInt3);
    }
    return i;
  }
  
  public static zzud zzb(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    zzb(paramInt1, paramInt1 + paramInt2, paramArrayOfByte.length);
    return new zzum(zzbua.zzc(paramArrayOfByte, paramInt1, paramInt2));
  }
  
  public static zzud zzfv(String paramString)
  {
    return new zzum(paramString.getBytes(zzvo.UTF_8));
  }
  
  static zzud zzi(byte[] paramArrayOfByte)
  {
    return new zzum(paramArrayOfByte);
  }
  
  public abstract boolean equals(Object paramObject);
  
  public final int hashCode()
  {
    int j = this.zzbry;
    int i = j;
    if (j == 0)
    {
      i = size();
      j = zza(i, 0, i);
      i = j;
      if (j == 0) {
        i = 1;
      }
      this.zzbry = i;
    }
    return i;
  }
  
  public abstract int size();
  
  public final String toString()
  {
    return String.format("<ByteString@%s size=%d>", new Object[] { Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(size()) });
  }
  
  protected abstract int zza(int paramInt1, int paramInt2, int paramInt3);
  
  protected abstract String zza(Charset paramCharset);
  
  abstract void zza(zzuc paramzzuc)
    throws IOException;
  
  public abstract byte zzal(int paramInt);
  
  public abstract zzud zzb(int paramInt1, int paramInt2);
  
  public final String zzua()
  {
    Charset localCharset = zzvo.UTF_8;
    if (size() == 0) {
      return "";
    }
    return zza(localCharset);
  }
  
  public abstract boolean zzub();
  
  protected final int zzuc()
  {
    return this.zzbry;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzud.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */