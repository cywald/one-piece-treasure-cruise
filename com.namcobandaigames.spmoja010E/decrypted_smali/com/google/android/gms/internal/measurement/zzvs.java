package com.google.android.gms.internal.measurement;

import java.util.List;
import java.util.RandomAccess;

public abstract interface zzvs<E>
  extends List<E>, RandomAccess
{
  public abstract zzvs<E> zzak(int paramInt);
  
  public abstract void zzsm();
  
  public abstract boolean zztw();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */