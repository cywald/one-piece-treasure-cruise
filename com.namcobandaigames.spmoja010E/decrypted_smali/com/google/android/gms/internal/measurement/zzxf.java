package com.google.android.gms.internal.measurement;

import java.lang.reflect.Constructor;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class zzxf
{
  private static final zzxf zzcbs = new zzxf();
  private final zzxk zzcbt;
  private final ConcurrentMap<Class<?>, zzxj<?>> zzcbu = new ConcurrentHashMap();
  
  private zzxf()
  {
    Object localObject1 = null;
    int i = 0;
    while (i <= 0)
    {
      localObject2 = zzgb(new String[] { "com.google.protobuf.AndroidProto3SchemaFactory" }[0]);
      localObject1 = localObject2;
      if (localObject2 != null) {
        break;
      }
      i += 1;
      localObject1 = localObject2;
    }
    Object localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = new zzwi();
    }
    this.zzcbt = ((zzxk)localObject2);
  }
  
  private static zzxk zzgb(String paramString)
  {
    try
    {
      paramString = (zzxk)Class.forName(paramString).getConstructor(new Class[0]).newInstance(new Object[0]);
      return paramString;
    }
    catch (Throwable paramString) {}
    return null;
  }
  
  public static zzxf zzxn()
  {
    return zzcbs;
  }
  
  public final <T> zzxj<T> zzag(T paramT)
  {
    return zzi(paramT.getClass());
  }
  
  public final <T> zzxj<T> zzi(Class<T> paramClass)
  {
    zzvo.zza(paramClass, "messageType");
    zzxj localzzxj2 = (zzxj)this.zzcbu.get(paramClass);
    zzxj localzzxj1 = localzzxj2;
    if (localzzxj2 == null)
    {
      localzzxj2 = this.zzcbt.zzh(paramClass);
      zzvo.zza(paramClass, "messageType");
      zzvo.zza(localzzxj2, "schema");
      localzzxj1 = (zzxj)this.zzcbu.putIfAbsent(paramClass, localzzxj2);
      if (localzzxj1 == null) {}
    }
    else
    {
      return localzzxj1;
    }
    return localzzxj2;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */