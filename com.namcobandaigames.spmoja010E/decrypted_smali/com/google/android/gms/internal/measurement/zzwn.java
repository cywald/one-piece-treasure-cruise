package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class zzwn<K, V>
  extends LinkedHashMap<K, V>
{
  private static final zzwn zzcau;
  private boolean zzbtu = true;
  
  static
  {
    zzwn localzzwn = new zzwn();
    zzcau = localzzwn;
    localzzwn.zzbtu = false;
  }
  
  private zzwn() {}
  
  private zzwn(Map<K, V> paramMap)
  {
    super(paramMap);
  }
  
  private static int zzx(Object paramObject)
  {
    if ((paramObject instanceof byte[])) {
      return zzvo.hashCode((byte[])paramObject);
    }
    if ((paramObject instanceof zzvp)) {
      throw new UnsupportedOperationException();
    }
    return paramObject.hashCode();
  }
  
  public static <K, V> zzwn<K, V> zzxa()
  {
    return zzcau;
  }
  
  private final void zzxc()
  {
    if (!this.zzbtu) {
      throw new UnsupportedOperationException();
    }
  }
  
  public final void clear()
  {
    zzxc();
    super.clear();
  }
  
  public final Set<Map.Entry<K, V>> entrySet()
  {
    if (isEmpty()) {
      return Collections.emptySet();
    }
    return super.entrySet();
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof Map))
    {
      paramObject = (Map)paramObject;
      int i;
      if (this != paramObject) {
        if (size() != ((Map)paramObject).size()) {
          i = 0;
        }
      }
      while (i != 0)
      {
        return true;
        Iterator localIterator = entrySet().iterator();
        label168:
        for (;;)
        {
          if (!localIterator.hasNext()) {
            break label170;
          }
          Object localObject2 = (Map.Entry)localIterator.next();
          if (!((Map)paramObject).containsKey(((Map.Entry)localObject2).getKey()))
          {
            i = 0;
            break;
          }
          Object localObject1 = ((Map.Entry)localObject2).getValue();
          localObject2 = ((Map)paramObject).get(((Map.Entry)localObject2).getKey());
          if (((localObject1 instanceof byte[])) && ((localObject2 instanceof byte[]))) {}
          for (boolean bool = Arrays.equals((byte[])localObject1, (byte[])localObject2);; bool = localObject1.equals(localObject2))
          {
            if (bool) {
              break label168;
            }
            i = 0;
            break;
          }
        }
        label170:
        i = 1;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    Iterator localIterator = entrySet().iterator();
    Map.Entry localEntry;
    int j;
    for (int i = 0; localIterator.hasNext(); i = (zzx(localEntry.getValue()) ^ j) + i)
    {
      localEntry = (Map.Entry)localIterator.next();
      j = zzx(localEntry.getKey());
    }
    return i;
  }
  
  public final boolean isMutable()
  {
    return this.zzbtu;
  }
  
  public final V put(K paramK, V paramV)
  {
    zzxc();
    zzvo.checkNotNull(paramK);
    zzvo.checkNotNull(paramV);
    return (V)super.put(paramK, paramV);
  }
  
  public final void putAll(Map<? extends K, ? extends V> paramMap)
  {
    zzxc();
    Iterator localIterator = paramMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = localIterator.next();
      zzvo.checkNotNull(localObject);
      zzvo.checkNotNull(paramMap.get(localObject));
    }
    super.putAll(paramMap);
  }
  
  public final V remove(Object paramObject)
  {
    zzxc();
    return (V)super.remove(paramObject);
  }
  
  public final void zza(zzwn<K, V> paramzzwn)
  {
    zzxc();
    if (!paramzzwn.isEmpty()) {
      putAll(paramzzwn);
    }
  }
  
  public final void zzsm()
  {
    this.zzbtu = false;
  }
  
  public final zzwn<K, V> zzxb()
  {
    if (isEmpty()) {
      return new zzwn();
    }
    return new zzwn(this);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */