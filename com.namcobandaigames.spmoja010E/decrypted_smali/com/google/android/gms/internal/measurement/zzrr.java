package com.google.android.gms.internal.measurement;

import android.content.Context;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;

public final class zzrr
{
  private String zzbam = null;
  @VisibleForTesting
  private Map<String, Object> zzbnk = new HashMap();
  private final Map<String, Object> zzbnl;
  private final zzsb zzbpu;
  private final Context zzri;
  private final Clock zzrz;
  
  public zzrr(Context paramContext)
  {
    this(paramContext, new HashMap(), new zzsb(paramContext), DefaultClock.getInstance());
  }
  
  @VisibleForTesting
  private zzrr(Context paramContext, Map<String, Object> paramMap, zzsb paramzzsb, Clock paramClock)
  {
    this.zzri = paramContext;
    this.zzrz = paramClock;
    this.zzbpu = paramzzsb;
    this.zzbnl = paramMap;
  }
  
  public final void zzfd(String paramString)
  {
    this.zzbam = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzrr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */