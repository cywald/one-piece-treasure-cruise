package com.google.android.gms.internal.measurement;

import java.io.IOException;

public abstract interface zzwt
  extends zzwv
{
  public abstract void zzb(zzut paramzzut)
    throws IOException;
  
  public abstract zzud zztt();
  
  public abstract int zzvu();
  
  public abstract zzwu zzwd();
  
  public abstract zzwu zzwe();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */