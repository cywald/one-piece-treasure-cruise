package com.google.android.gms.internal.measurement;

import java.util.NoSuchElementException;

final class zzue
  implements zzuj
{
  private final int limit = this.zzbuc.size();
  private int position = 0;
  
  zzue(zzud paramzzud) {}
  
  public final boolean hasNext()
  {
    return this.position < this.limit;
  }
  
  public final byte nextByte()
  {
    try
    {
      zzud localzzud = this.zzbuc;
      int i = this.position;
      this.position = (i + 1);
      byte b = localzzud.zzal(i);
      return b;
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
    {
      throw new NoSuchElementException(localIndexOutOfBoundsException.getMessage());
    }
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */