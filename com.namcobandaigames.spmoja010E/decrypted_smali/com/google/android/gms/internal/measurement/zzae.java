package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzi;
import java.util.HashMap;
import java.util.Map;

public final class zzae
  extends zzi<zzae>
{
  private String category;
  private String label;
  private long value;
  private String zzul;
  
  public final String getAction()
  {
    return this.zzul;
  }
  
  public final String getLabel()
  {
    return this.label;
  }
  
  public final long getValue()
  {
    return this.value;
  }
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("category", this.category);
    localHashMap.put("action", this.zzul);
    localHashMap.put("label", this.label);
    localHashMap.put("value", Long.valueOf(this.value));
    return zza(localHashMap);
  }
  
  public final String zzbb()
  {
    return this.category;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */