package com.google.android.gms.internal.measurement;

import android.util.Log;

final class zzss
  extends zzsl<Double>
{
  zzss(zzsv paramzzsv, String paramString, Double paramDouble)
  {
    super(paramzzsv, paramString, paramDouble, null);
  }
  
  private final Double zzfm(String paramString)
  {
    try
    {
      double d = Double.parseDouble(paramString);
      return Double.valueOf(d);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      String str = this.zzbrc;
      Log.e("PhenotypeFlag", String.valueOf(str).length() + 27 + String.valueOf(paramString).length() + "Invalid double value for " + str + ": " + paramString);
    }
    return null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzss.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */