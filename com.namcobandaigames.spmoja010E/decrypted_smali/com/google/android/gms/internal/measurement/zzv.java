package com.google.android.gms.internal.measurement;

import android.os.IBinder;
import android.os.IInterface;

public abstract class zzv
  extends zzr
  implements zzu
{
  public static zzu zza(IBinder paramIBinder)
  {
    if (paramIBinder == null) {
      return null;
    }
    IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    if ((localIInterface instanceof zzu)) {
      return (zzu)localIInterface;
    }
    return new zzw(paramIBinder);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */