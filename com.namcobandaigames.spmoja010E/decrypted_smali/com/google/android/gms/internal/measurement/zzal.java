package com.google.android.gms.internal.measurement;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class zzal
  extends zzau
{
  private final zzbi zzvq;
  
  public zzal(zzaw paramzzaw, zzay paramzzay)
  {
    super(paramzzaw);
    Preconditions.checkNotNull(paramzzay);
    this.zzvq = new zzbi(paramzzaw, paramzzay);
  }
  
  final void onServiceConnected()
  {
    zzk.zzaf();
    this.zzvq.onServiceConnected();
  }
  
  public final void setLocalDispatchPeriod(int paramInt)
  {
    zzcl();
    zzb("setLocalDispatchPeriod (sec)", Integer.valueOf(paramInt));
    zzca().zza(new zzam(this, paramInt));
  }
  
  public final void start()
  {
    this.zzvq.start();
  }
  
  public final long zza(zzaz paramzzaz)
  {
    zzcl();
    Preconditions.checkNotNull(paramzzaz);
    zzk.zzaf();
    long l = this.zzvq.zza(paramzzaz, true);
    if (l == 0L) {
      this.zzvq.zzb(paramzzaz);
    }
    return l;
  }
  
  public final void zza(zzcd paramzzcd)
  {
    zzcl();
    zzca().zza(new zzar(this, paramzzcd));
  }
  
  public final void zza(zzck paramzzck)
  {
    Preconditions.checkNotNull(paramzzck);
    zzcl();
    zzb("Hit delivery requested", paramzzck);
    zzca().zza(new zzap(this, paramzzck));
  }
  
  public final void zza(String paramString, Runnable paramRunnable)
  {
    Preconditions.checkNotEmpty(paramString, "campaign param can't be empty");
    zzca().zza(new zzao(this, paramString, paramRunnable));
  }
  
  protected final void zzag()
  {
    this.zzvq.zzq();
  }
  
  public final void zzbr()
  {
    zzcl();
    zzca().zza(new zzaq(this));
  }
  
  public final void zzbs()
  {
    zzcl();
    Context localContext = getContext();
    if ((zzcw.zza(localContext)) && (zzcx.zze(localContext)))
    {
      Intent localIntent = new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
      localIntent.setComponent(new ComponentName(localContext, "com.google.android.gms.analytics.AnalyticsService"));
      localContext.startService(localIntent);
      return;
    }
    zza(null);
  }
  
  public final boolean zzbt()
  {
    zzcl();
    Future localFuture = zzca().zza(new zzas(this));
    try
    {
      localFuture.get(4L, TimeUnit.SECONDS);
      return true;
    }
    catch (InterruptedException localInterruptedException)
    {
      zzd("syncDispatchLocalHits interrupted", localInterruptedException);
      return false;
    }
    catch (ExecutionException localExecutionException)
    {
      zze("syncDispatchLocalHits failed", localExecutionException);
      return false;
    }
    catch (TimeoutException localTimeoutException)
    {
      zzd("syncDispatchLocalHits timed out", localTimeoutException);
    }
    return false;
  }
  
  public final void zzbu()
  {
    zzcl();
    zzk.zzaf();
    zzbi localzzbi = this.zzvq;
    zzk.zzaf();
    localzzbi.zzcl();
    localzzbi.zzq("Service disconnected");
  }
  
  final void zzbv()
  {
    zzk.zzaf();
    this.zzvq.zzbv();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */