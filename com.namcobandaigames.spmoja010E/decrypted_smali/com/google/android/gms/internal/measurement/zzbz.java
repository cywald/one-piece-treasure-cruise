package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;

abstract class zzbz
{
  private static volatile Handler handler;
  private final zzaw zzvy;
  private final Runnable zzyo;
  private volatile long zzyp;
  
  zzbz(zzaw paramzzaw)
  {
    Preconditions.checkNotNull(paramzzaw);
    this.zzvy = paramzzaw;
    this.zzyo = new zzca(this);
  }
  
  private final Handler getHandler()
  {
    if (handler != null) {
      return handler;
    }
    try
    {
      if (handler == null) {
        handler = new zzdx(this.zzvy.getContext().getMainLooper());
      }
      Handler localHandler = handler;
      return localHandler;
    }
    finally {}
  }
  
  public final void cancel()
  {
    this.zzyp = 0L;
    getHandler().removeCallbacks(this.zzyo);
  }
  
  public abstract void run();
  
  public final long zzei()
  {
    if (this.zzyp == 0L) {
      return 0L;
    }
    return Math.abs(this.zzvy.zzbx().currentTimeMillis() - this.zzyp);
  }
  
  public final boolean zzej()
  {
    return this.zzyp != 0L;
  }
  
  public final void zzh(long paramLong)
  {
    cancel();
    if (paramLong >= 0L)
    {
      this.zzyp = this.zzvy.zzbx().currentTimeMillis();
      if (!getHandler().postDelayed(this.zzyo, paramLong)) {
        this.zzvy.zzby().zze("Failed to schedule delayed post. time", Long.valueOf(paramLong));
      }
    }
  }
  
  public final void zzi(long paramLong)
  {
    long l = 0L;
    if (!zzej()) {
      return;
    }
    if (paramLong < 0L)
    {
      cancel();
      return;
    }
    paramLong -= Math.abs(this.zzvy.zzbx().currentTimeMillis() - this.zzyp);
    if (paramLong < 0L) {
      paramLong = l;
    }
    for (;;)
    {
      getHandler().removeCallbacks(this.zzyo);
      if (getHandler().postDelayed(this.zzyo, paramLong)) {
        break;
      }
      this.zzvy.zzby().zze("Failed to adjust delayed post. time", Long.valueOf(paramLong));
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */