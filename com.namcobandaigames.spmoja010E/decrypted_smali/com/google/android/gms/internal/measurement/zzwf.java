package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

final class zzwf
  extends zzwd
{
  private static final Class<?> zzcal = Collections.unmodifiableList(Collections.emptyList()).getClass();
  
  private zzwf()
  {
    super(null);
  }
  
  private static <L> List<L> zza(Object paramObject, long paramLong, int paramInt)
  {
    List localList = zzc(paramObject, paramLong);
    if (localList.isEmpty()) {
      if ((localList instanceof zzwc))
      {
        localObject = new zzwb(paramInt);
        zzyh.zza(paramObject, paramLong, localObject);
      }
    }
    do
    {
      do
      {
        do
        {
          return (List<L>)localObject;
          if (((localList instanceof zzxe)) && ((localList instanceof zzvs)))
          {
            localObject = ((zzvs)localList).zzak(paramInt);
            break;
          }
          localObject = new ArrayList(paramInt);
          break;
          if (zzcal.isAssignableFrom(localList.getClass()))
          {
            localObject = new ArrayList(localList.size() + paramInt);
            ((ArrayList)localObject).addAll(localList);
            zzyh.zza(paramObject, paramLong, localObject);
            return (List<L>)localObject;
          }
          if ((localList instanceof zzye))
          {
            localObject = new zzwb(localList.size() + paramInt);
            ((zztz)localObject).addAll((zzye)localList);
            zzyh.zza(paramObject, paramLong, localObject);
            return (List<L>)localObject;
          }
          localObject = localList;
        } while (!(localList instanceof zzxe));
        localObject = localList;
      } while (!(localList instanceof zzvs));
      localObject = localList;
    } while (((zzvs)localList).zztw());
    Object localObject = ((zzvs)localList).zzak(localList.size() + paramInt);
    zzyh.zza(paramObject, paramLong, localObject);
    return (List<L>)localObject;
  }
  
  private static <E> List<E> zzc(Object paramObject, long paramLong)
  {
    return (List)zzyh.zzp(paramObject, paramLong);
  }
  
  final <L> List<L> zza(Object paramObject, long paramLong)
  {
    return zza(paramObject, paramLong, 10);
  }
  
  final <E> void zza(Object paramObject1, Object paramObject2, long paramLong)
  {
    paramObject2 = zzc(paramObject2, paramLong);
    List localList = zza(paramObject1, paramLong, ((List)paramObject2).size());
    int i = localList.size();
    int j = ((List)paramObject2).size();
    if ((i > 0) && (j > 0)) {
      localList.addAll((Collection)paramObject2);
    }
    if (i > 0) {
      paramObject2 = localList;
    }
    for (;;)
    {
      zzyh.zza(paramObject1, paramLong, paramObject2);
      return;
    }
  }
  
  final void zzb(Object paramObject, long paramLong)
  {
    Object localObject = (List)zzyh.zzp(paramObject, paramLong);
    if ((localObject instanceof zzwc)) {}
    for (localObject = ((zzwc)localObject).zzww();; localObject = Collections.unmodifiableList((List)localObject))
    {
      zzyh.zza(paramObject, paramLong, localObject);
      do
      {
        do
        {
          return;
        } while (zzcal.isAssignableFrom(localObject.getClass()));
        if ((!(localObject instanceof zzxe)) || (!(localObject instanceof zzvs))) {
          break;
        }
      } while (!((zzvs)localObject).zztw());
      ((zzvs)localObject).zzsm();
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */