package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzfu
  extends zzza<zzfu>
{
  private static volatile zzfu[] zzaux;
  public Integer zzauy = null;
  public zzfy[] zzauz = zzfy.zzml();
  public zzfv[] zzava = zzfv.zzmj();
  private Boolean zzavb = null;
  private Boolean zzavc = null;
  
  public zzfu()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzfu[] zzmi()
  {
    if (zzaux == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzaux == null) {
        zzaux = new zzfu[0];
      }
      return zzaux;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzfu)) {
        return false;
      }
      paramObject = (zzfu)paramObject;
      if (this.zzauy == null)
      {
        if (((zzfu)paramObject).zzauy != null) {
          return false;
        }
      }
      else if (!this.zzauy.equals(((zzfu)paramObject).zzauy)) {
        return false;
      }
      if (!zzze.equals(this.zzauz, ((zzfu)paramObject).zzauz)) {
        return false;
      }
      if (!zzze.equals(this.zzava, ((zzfu)paramObject).zzava)) {
        return false;
      }
      if (this.zzavb == null)
      {
        if (((zzfu)paramObject).zzavb != null) {
          return false;
        }
      }
      else if (!this.zzavb.equals(((zzfu)paramObject).zzavb)) {
        return false;
      }
      if (this.zzavc == null)
      {
        if (((zzfu)paramObject).zzavc != null) {
          return false;
        }
      }
      else if (!this.zzavc.equals(((zzfu)paramObject).zzavc)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzfu)paramObject).zzcfc == null) || (((zzfu)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzfu)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int n = 0;
    int i1 = getClass().getName().hashCode();
    int i;
    int i2;
    int i3;
    int j;
    label51:
    int k;
    if (this.zzauy == null)
    {
      i = 0;
      i2 = zzze.hashCode(this.zzauz);
      i3 = zzze.hashCode(this.zzava);
      if (this.zzavb != null) {
        break label136;
      }
      j = 0;
      if (this.zzavc != null) {
        break label147;
      }
      k = 0;
      label60:
      m = n;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label158;
        }
      }
    }
    label136:
    label147:
    label158:
    for (int m = n;; m = this.zzcfc.hashCode())
    {
      return (k + (j + (((i + (i1 + 527) * 31) * 31 + i2) * 31 + i3) * 31) * 31) * 31 + m;
      i = this.zzauy.hashCode();
      break;
      j = this.zzavb.hashCode();
      break label51;
      k = this.zzavc.hashCode();
      break label60;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    int j = 0;
    if (this.zzauy != null) {
      paramzzyy.zzd(1, this.zzauy.intValue());
    }
    int i;
    Object localObject;
    if ((this.zzauz != null) && (this.zzauz.length > 0))
    {
      i = 0;
      while (i < this.zzauz.length)
      {
        localObject = this.zzauz[i];
        if (localObject != null) {
          paramzzyy.zza(2, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzava != null) && (this.zzava.length > 0))
    {
      i = j;
      while (i < this.zzava.length)
      {
        localObject = this.zzava[i];
        if (localObject != null) {
          paramzzyy.zza(3, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if (this.zzavb != null) {
      paramzzyy.zzb(4, this.zzavb.booleanValue());
    }
    if (this.zzavc != null) {
      paramzzyy.zzb(5, this.zzavc.booleanValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int m = 0;
    int i = super.zzf();
    int j = i;
    if (this.zzauy != null) {
      j = i + zzyy.zzh(1, this.zzauy.intValue());
    }
    i = j;
    Object localObject;
    int k;
    if (this.zzauz != null)
    {
      i = j;
      if (this.zzauz.length > 0)
      {
        i = j;
        j = 0;
        while (j < this.zzauz.length)
        {
          localObject = this.zzauz[j];
          k = i;
          if (localObject != null) {
            k = i + zzyy.zzb(2, (zzzg)localObject);
          }
          j += 1;
          i = k;
        }
      }
    }
    j = i;
    if (this.zzava != null)
    {
      j = i;
      if (this.zzava.length > 0)
      {
        k = m;
        for (;;)
        {
          j = i;
          if (k >= this.zzava.length) {
            break;
          }
          localObject = this.zzava[k];
          j = i;
          if (localObject != null) {
            j = i + zzyy.zzb(3, (zzzg)localObject);
          }
          k += 1;
          i = j;
        }
      }
    }
    i = j;
    if (this.zzavb != null)
    {
      this.zzavb.booleanValue();
      i = j + (zzyy.zzbb(4) + 1);
    }
    j = i;
    if (this.zzavc != null)
    {
      this.zzavc.booleanValue();
      j = i + (zzyy.zzbb(5) + 1);
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzfu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */