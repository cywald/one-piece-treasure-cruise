package com.google.android.gms.internal.measurement;

import java.util.Arrays;

final class zzzi
{
  final int tag;
  final byte[] zzbug;
  
  zzzi(int paramInt, byte[] paramArrayOfByte)
  {
    this.tag = paramInt;
    this.zzbug = paramArrayOfByte;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzzi)) {
        return false;
      }
      paramObject = (zzzi)paramObject;
    } while ((this.tag == ((zzzi)paramObject).tag) && (Arrays.equals(this.zzbug, ((zzzi)paramObject).zzbug)));
    return false;
  }
  
  public final int hashCode()
  {
    return (this.tag + 527) * 31 + Arrays.hashCode(this.zzbug);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */