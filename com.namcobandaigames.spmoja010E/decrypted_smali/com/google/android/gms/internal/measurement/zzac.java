package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzi;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;

@VisibleForTesting
public final class zzac
  extends zzi<zzac>
{
  private String zzuf;
  public int zzug;
  public int zzuh;
  public int zzui;
  public int zzuj;
  public int zzuk;
  
  public final String getLanguage()
  {
    return this.zzuf;
  }
  
  public final void setLanguage(String paramString)
  {
    this.zzuf = paramString;
  }
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("language", this.zzuf);
    localHashMap.put("screenColors", Integer.valueOf(this.zzug));
    localHashMap.put("screenWidth", Integer.valueOf(this.zzuh));
    localHashMap.put("screenHeight", Integer.valueOf(this.zzui));
    localHashMap.put("viewportWidth", Integer.valueOf(this.zzuj));
    localHashMap.put("viewportHeight", Integer.valueOf(this.zzuk));
    return zza(localHashMap);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */