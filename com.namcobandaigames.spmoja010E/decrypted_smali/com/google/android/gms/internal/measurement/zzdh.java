package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import java.util.Locale;

public final class zzdh
  extends zzau
{
  private String zzaaq;
  private String zzaar;
  protected int zzaat;
  private int zzacq;
  protected boolean zzacr;
  private boolean zzacs;
  private boolean zzact;
  
  public zzdh(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  protected final void zzag()
  {
    Object localObject1 = getContext();
    Object localObject2;
    int i;
    String str;
    label135:
    label168:
    label196:
    label224:
    do
    {
      try
      {
        localObject1 = ((Context)localObject1).getPackageManager().getApplicationInfo(((Context)localObject1).getPackageName(), 128);
        if (localObject1 == null)
        {
          zzt("Couldn't get ApplicationInfo to load global config");
          return;
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        do
        {
          do
          {
            do
            {
              for (;;)
              {
                zzd("PackageManager doesn't know about the app package", localNameNotFoundException);
                localObject2 = null;
              }
              localObject2 = ((ApplicationInfo)localObject2).metaData;
            } while (localObject2 == null);
            i = ((Bundle)localObject2).getInt("com.google.android.gms.analytics.globalConfigResource");
          } while (i <= 0);
          localObject2 = (zzcj)new zzch(zzbw()).zzq(i);
        } while (localObject2 == null);
        zzq("Loading global XML config values");
        if (((zzcj)localObject2).zzaaq == null) {
          break;
        }
      }
      i = 1;
      if (i != 0)
      {
        str = ((zzcj)localObject2).zzaaq;
        this.zzaaq = str;
        zzb("XML config - app name", str);
      }
      if (((zzcj)localObject2).zzaar == null) {
        break label297;
      }
      i = 1;
      if (i != 0)
      {
        str = ((zzcj)localObject2).zzaar;
        this.zzaar = str;
        zzb("XML config - app version", str);
      }
      if (((zzcj)localObject2).zzaas == null) {
        break label302;
      }
      i = 1;
      if (i != 0)
      {
        str = ((zzcj)localObject2).zzaas.toLowerCase(Locale.US);
        if (!"verbose".equals(str)) {
          break label307;
        }
        i = 0;
        if (i >= 0)
        {
          this.zzacq = i;
          zza("XML config - log level", Integer.valueOf(i));
        }
      }
      if (((zzcj)localObject2).zzaat < 0) {
        break label357;
      }
      i = 1;
      if (i != 0)
      {
        i = ((zzcj)localObject2).zzaat;
        this.zzaat = i;
        this.zzacr = true;
        zzb("XML config - dispatch period (sec)", Integer.valueOf(i));
      }
    } while (((zzcj)localObject2).zzaau == -1);
    if (((zzcj)localObject2).zzaau == 1) {}
    for (boolean bool = true;; bool = false)
    {
      this.zzact = bool;
      this.zzacs = true;
      zzb("XML config - dry run", Boolean.valueOf(bool));
      return;
      i = 0;
      break;
      label297:
      i = 0;
      break label135;
      label302:
      i = 0;
      break label168;
      label307:
      if ("info".equals(str))
      {
        i = 1;
        break label196;
      }
      if ("warning".equals(str))
      {
        i = 2;
        break label196;
      }
      if ("error".equals(str))
      {
        i = 3;
        break label196;
      }
      i = -1;
      break label196;
      label357:
      i = 0;
      break label224;
    }
  }
  
  public final String zzaj()
  {
    zzcl();
    return this.zzaaq;
  }
  
  public final String zzak()
  {
    zzcl();
    return this.zzaar;
  }
  
  public final boolean zzfr()
  {
    zzcl();
    return false;
  }
  
  public final boolean zzfs()
  {
    zzcl();
    return this.zzacs;
  }
  
  public final boolean zzft()
  {
    zzcl();
    return this.zzact;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzdh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */