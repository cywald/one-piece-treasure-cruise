package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.analytics.zzi;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;

@VisibleForTesting
public final class zzx
  extends zzi<zzx>
{
  private String zztr;
  private String zzts;
  private String zztt;
  private String zztu;
  
  public final void setAppId(String paramString)
  {
    this.zztt = paramString;
  }
  
  public final void setAppInstallerId(String paramString)
  {
    this.zztu = paramString;
  }
  
  public final void setAppName(String paramString)
  {
    this.zztr = paramString;
  }
  
  public final void setAppVersion(String paramString)
  {
    this.zzts = paramString;
  }
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("appName", this.zztr);
    localHashMap.put("appVersion", this.zzts);
    localHashMap.put("appId", this.zztt);
    localHashMap.put("appInstallerId", this.zztu);
    return zza(localHashMap);
  }
  
  public final void zza(zzx paramzzx)
  {
    if (!TextUtils.isEmpty(this.zztr)) {
      paramzzx.zztr = this.zztr;
    }
    if (!TextUtils.isEmpty(this.zzts)) {
      paramzzx.zzts = this.zzts;
    }
    if (!TextUtils.isEmpty(this.zztt)) {
      paramzzx.zztt = this.zztt;
    }
    if (!TextUtils.isEmpty(this.zztu)) {
      paramzzx.zztu = this.zztu;
    }
  }
  
  public final String zzaj()
  {
    return this.zztr;
  }
  
  public final String zzak()
  {
    return this.zzts;
  }
  
  public final String zzal()
  {
    return this.zztt;
  }
  
  public final String zzam()
  {
    return this.zztu;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */