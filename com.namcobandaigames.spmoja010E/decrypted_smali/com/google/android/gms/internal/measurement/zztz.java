package com.google.android.gms.internal.measurement;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

abstract class zztz<E>
  extends AbstractList<E>
  implements zzvs<E>
{
  private boolean zzbtu = true;
  
  public void add(int paramInt, E paramE)
  {
    zztx();
    super.add(paramInt, paramE);
  }
  
  public boolean add(E paramE)
  {
    zztx();
    return super.add(paramE);
  }
  
  public boolean addAll(int paramInt, Collection<? extends E> paramCollection)
  {
    zztx();
    return super.addAll(paramInt, paramCollection);
  }
  
  public boolean addAll(Collection<? extends E> paramCollection)
  {
    zztx();
    return super.addAll(paramCollection);
  }
  
  public void clear()
  {
    zztx();
    super.clear();
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    for (;;)
    {
      return true;
      if (!(paramObject instanceof List)) {
        return false;
      }
      if (!(paramObject instanceof RandomAccess)) {
        return super.equals(paramObject);
      }
      paramObject = (List)paramObject;
      int j = size();
      if (j != ((List)paramObject).size()) {
        return false;
      }
      int i = 0;
      while (i < j)
      {
        if (!get(i).equals(((List)paramObject).get(i))) {
          return false;
        }
        i += 1;
      }
    }
  }
  
  public int hashCode()
  {
    int k = size();
    int j = 1;
    int i = 0;
    while (i < k)
    {
      j = j * 31 + get(i).hashCode();
      i += 1;
    }
    return j;
  }
  
  public E remove(int paramInt)
  {
    zztx();
    return (E)super.remove(paramInt);
  }
  
  public boolean remove(Object paramObject)
  {
    zztx();
    return super.remove(paramObject);
  }
  
  public boolean removeAll(Collection<?> paramCollection)
  {
    zztx();
    return super.removeAll(paramCollection);
  }
  
  public boolean retainAll(Collection<?> paramCollection)
  {
    zztx();
    return super.retainAll(paramCollection);
  }
  
  public E set(int paramInt, E paramE)
  {
    zztx();
    return (E)super.set(paramInt, paramE);
  }
  
  public final void zzsm()
  {
    this.zzbtu = false;
  }
  
  public boolean zztw()
  {
    return this.zzbtu;
  }
  
  protected final void zztx()
  {
    if (!this.zzbtu) {
      throw new UnsupportedOperationException();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zztz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */