package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzfw
  extends zzza<zzfw>
{
  private static volatile zzfw[] zzavj;
  public zzfz zzavk = null;
  public zzfx zzavl = null;
  public Boolean zzavm = null;
  public String zzavn = null;
  
  public zzfw()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzfw[] zzmk()
  {
    if (zzavj == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzavj == null) {
        zzavj = new zzfw[0];
      }
      return zzavj;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzfw)) {
        return false;
      }
      paramObject = (zzfw)paramObject;
      if (this.zzavk == null)
      {
        if (((zzfw)paramObject).zzavk != null) {
          return false;
        }
      }
      else if (!this.zzavk.equals(((zzfw)paramObject).zzavk)) {
        return false;
      }
      if (this.zzavl == null)
      {
        if (((zzfw)paramObject).zzavl != null) {
          return false;
        }
      }
      else if (!this.zzavl.equals(((zzfw)paramObject).zzavl)) {
        return false;
      }
      if (this.zzavm == null)
      {
        if (((zzfw)paramObject).zzavm != null) {
          return false;
        }
      }
      else if (!this.zzavm.equals(((zzfw)paramObject).zzavm)) {
        return false;
      }
      if (this.zzavn == null)
      {
        if (((zzfw)paramObject).zzavn != null) {
          return false;
        }
      }
      else if (!this.zzavn.equals(((zzfw)paramObject).zzavn)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzfw)paramObject).zzcfc == null) || (((zzfw)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzfw)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i1 = 0;
    int i2 = getClass().getName().hashCode();
    Object localObject = this.zzavk;
    int i;
    int j;
    label41:
    int k;
    label50:
    int m;
    if (localObject == null)
    {
      i = 0;
      localObject = this.zzavl;
      if (localObject != null) {
        break label128;
      }
      j = 0;
      if (this.zzavm != null) {
        break label137;
      }
      k = 0;
      if (this.zzavn != null) {
        break label148;
      }
      m = 0;
      label60:
      n = i1;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label160;
        }
      }
    }
    label128:
    label137:
    label148:
    label160:
    for (int n = i1;; n = this.zzcfc.hashCode())
    {
      return (m + (k + (j + (i + (i2 + 527) * 31) * 31) * 31) * 31) * 31 + n;
      i = ((zzfz)localObject).hashCode();
      break;
      j = ((zzfx)localObject).hashCode();
      break label41;
      k = this.zzavm.hashCode();
      break label50;
      m = this.zzavn.hashCode();
      break label60;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzavk != null) {
      paramzzyy.zza(1, this.zzavk);
    }
    if (this.zzavl != null) {
      paramzzyy.zza(2, this.zzavl);
    }
    if (this.zzavm != null) {
      paramzzyy.zzb(3, this.zzavm.booleanValue());
    }
    if (this.zzavn != null) {
      paramzzyy.zzb(4, this.zzavn);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.zzavk != null) {
      i = j + zzyy.zzb(1, this.zzavk);
    }
    j = i;
    if (this.zzavl != null) {
      j = i + zzyy.zzb(2, this.zzavl);
    }
    i = j;
    if (this.zzavm != null)
    {
      this.zzavm.booleanValue();
      i = j + (zzyy.zzbb(3) + 1);
    }
    j = i;
    if (this.zzavn != null) {
      j = i + zzyy.zzc(4, this.zzavn);
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzfw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */