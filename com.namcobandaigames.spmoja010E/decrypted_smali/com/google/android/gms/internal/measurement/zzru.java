package com.google.android.gms.internal.measurement;

import java.util.Collections;
import java.util.Map;

public final class zzru
{
  private final zzp zzbfg;
  private final Map<String, zzp> zzbon;
  
  private zzru(Map<String, zzp> paramMap, zzp paramzzp)
  {
    this.zzbon = paramMap;
    this.zzbfg = paramzzp;
  }
  
  public static zzrv zzsp()
  {
    return new zzrv(null);
  }
  
  public final String toString()
  {
    String str1 = String.valueOf(Collections.unmodifiableMap(this.zzbon));
    String str2 = String.valueOf(this.zzbfg);
    return String.valueOf(str1).length() + 32 + String.valueOf(str2).length() + "Properties: " + str1 + " pushAfterEvaluate: " + str2;
  }
  
  public final void zza(String paramString, zzp paramzzp)
  {
    this.zzbon.put(paramString, paramzzp);
  }
  
  public final zzp zzpm()
  {
    return this.zzbfg;
  }
  
  public final Map<String, zzp> zzry()
  {
    return Collections.unmodifiableMap(this.zzbon);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzru.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */