package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class zzwp
  implements zzwo
{
  public final boolean zzaa(Object paramObject)
  {
    return !((zzwn)paramObject).isMutable();
  }
  
  public final Object zzab(Object paramObject)
  {
    ((zzwn)paramObject).zzsm();
    return paramObject;
  }
  
  public final Object zzac(Object paramObject)
  {
    return zzwn.zzxa().zzxb();
  }
  
  public final zzwm<?, ?> zzad(Object paramObject)
  {
    throw new NoSuchMethodError();
  }
  
  public final int zzb(int paramInt, Object paramObject1, Object paramObject2)
  {
    paramObject1 = (zzwn)paramObject1;
    if (((zzwn)paramObject1).isEmpty()) {}
    do
    {
      return 0;
      paramObject1 = ((zzwn)paramObject1).entrySet().iterator();
    } while (!((Iterator)paramObject1).hasNext());
    paramObject1 = (Map.Entry)((Iterator)paramObject1).next();
    ((Map.Entry)paramObject1).getKey();
    ((Map.Entry)paramObject1).getValue();
    throw new NoSuchMethodError();
  }
  
  public final Object zzc(Object paramObject1, Object paramObject2)
  {
    zzwn localzzwn = (zzwn)paramObject1;
    paramObject2 = (zzwn)paramObject2;
    paramObject1 = localzzwn;
    if (!((zzwn)paramObject2).isEmpty())
    {
      paramObject1 = localzzwn;
      if (!localzzwn.isMutable()) {
        paramObject1 = localzzwn.zzxb();
      }
      ((zzwn)paramObject1).zza((zzwn)paramObject2);
    }
    return paramObject1;
  }
  
  public final Map<?, ?> zzy(Object paramObject)
  {
    return (zzwn)paramObject;
  }
  
  public final Map<?, ?> zzz(Object paramObject)
  {
    return (zzwn)paramObject;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */