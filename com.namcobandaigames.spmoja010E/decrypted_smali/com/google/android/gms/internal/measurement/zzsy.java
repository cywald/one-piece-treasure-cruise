package com.google.android.gms.internal.measurement;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

final class zzsy
{
  private final ConcurrentHashMap<zzsz, List<Throwable>> zzbrw = new ConcurrentHashMap(16, 0.75F, 10);
  private final ReferenceQueue<Throwable> zzbrx = new ReferenceQueue();
  
  public final List<Throwable> zza(Throwable paramThrowable, boolean paramBoolean)
  {
    for (Reference localReference = this.zzbrx.poll(); localReference != null; localReference = this.zzbrx.poll()) {
      this.zzbrw.remove(localReference);
    }
    paramThrowable = new zzsz(paramThrowable, null);
    return (List)this.zzbrw.get(paramThrowable);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */