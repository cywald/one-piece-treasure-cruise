package com.google.android.gms.internal.measurement;

import java.util.Arrays;

final class zzug
  implements zzui
{
  public final byte[] zzc(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return Arrays.copyOfRange(paramArrayOfByte, paramInt1, paramInt1 + paramInt2);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzug.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */