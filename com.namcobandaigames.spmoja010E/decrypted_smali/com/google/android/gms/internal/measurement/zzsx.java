package com.google.android.gms.internal.measurement;

import java.io.PrintStream;

abstract class zzsx
{
  private static final Throwable[] zzbrv = new Throwable[0];
  
  public abstract void zza(Throwable paramThrowable, PrintStream paramPrintStream);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */