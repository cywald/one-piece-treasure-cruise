package com.google.android.gms.internal.measurement;

public abstract class zztx<MessageType extends zztw<MessageType, BuilderType>, BuilderType extends zztx<MessageType, BuilderType>>
  implements zzwu
{
  protected abstract BuilderType zza(MessageType paramMessageType);
  
  public abstract BuilderType zztv();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zztx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */