package com.google.android.gms.internal.measurement;

import java.io.IOException;

public abstract class zztw<MessageType extends zztw<MessageType, BuilderType>, BuilderType extends zztx<MessageType, BuilderType>>
  implements zzwt
{
  private static boolean zzbts = false;
  protected int zzbtr = 0;
  
  void zzah(int paramInt)
  {
    throw new UnsupportedOperationException();
  }
  
  public final zzud zztt()
  {
    try
    {
      Object localObject = zzud.zzam(zzvu());
      zzb(((zzuk)localObject).zzuf());
      localObject = ((zzuk)localObject).zzue();
      return (zzud)localObject;
    }
    catch (IOException localIOException)
    {
      String str = getClass().getName();
      throw new RuntimeException(String.valueOf(str).length() + 62 + String.valueOf("ByteString").length() + "Serializing " + str + " to a " + "ByteString" + " threw an IOException (should never happen).", localIOException);
    }
  }
  
  int zztu()
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zztw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */