package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgc
  extends zzza<zzgc>
{
  private static volatile zzgc[] zzawk;
  public String value = null;
  public String zzoj = null;
  
  public zzgc()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzgc[] zzmn()
  {
    if (zzawk == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzawk == null) {
        zzawk = new zzgc[0];
      }
      return zzawk;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgc)) {
        return false;
      }
      paramObject = (zzgc)paramObject;
      if (this.zzoj == null)
      {
        if (((zzgc)paramObject).zzoj != null) {
          return false;
        }
      }
      else if (!this.zzoj.equals(((zzgc)paramObject).zzoj)) {
        return false;
      }
      if (this.value == null)
      {
        if (((zzgc)paramObject).value != null) {
          return false;
        }
      }
      else if (!this.value.equals(((zzgc)paramObject).value)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgc)paramObject).zzcfc == null) || (((zzgc)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgc)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int m = 0;
    int n = getClass().getName().hashCode();
    int i;
    int j;
    if (this.zzoj == null)
    {
      i = 0;
      if (this.value != null) {
        break label89;
      }
      j = 0;
      label33:
      k = m;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label100;
        }
      }
    }
    label89:
    label100:
    for (int k = m;; k = this.zzcfc.hashCode())
    {
      return (j + (i + (n + 527) * 31) * 31) * 31 + k;
      i = this.zzoj.hashCode();
      break;
      j = this.value.hashCode();
      break label33;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzoj != null) {
      paramzzyy.zzb(1, this.zzoj);
    }
    if (this.value != null) {
      paramzzyy.zzb(2, this.value);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.zzoj != null) {
      i = j + zzyy.zzc(1, this.zzoj);
    }
    j = i;
    if (this.value != null) {
      j = i + zzyy.zzc(2, this.value);
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */