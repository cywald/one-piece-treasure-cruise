package com.google.android.gms.internal.measurement;

import android.util.Log;
import com.google.android.gms.analytics.Logger;

final class zzby
  implements Logger
{
  private boolean zzrr;
  private int zzyn = 2;
  
  public final void error(Exception paramException) {}
  
  public final void error(String paramString) {}
  
  public final int getLogLevel()
  {
    return this.zzyn;
  }
  
  public final void info(String paramString) {}
  
  public final void setLogLevel(int paramInt)
  {
    this.zzyn = paramInt;
    if (!this.zzrr)
    {
      String str1 = (String)zzcf.zzyx.get();
      String str2 = (String)zzcf.zzyx.get();
      Log.i(str1, String.valueOf(str2).length() + 91 + "Logger is deprecated. To enable debug logging, please run:\nadb shell setprop log.tag." + str2 + " DEBUG");
      this.zzrr = true;
    }
  }
  
  public final void verbose(String paramString) {}
  
  public final void warn(String paramString) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzby.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */