package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzcp
  extends zzau
{
  private static zzcp zzabh;
  
  public zzcp(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  @VisibleForTesting
  private static String zzd(Object paramObject)
  {
    if (paramObject == null) {
      return null;
    }
    if ((paramObject instanceof Integer)) {
      paramObject = Long.valueOf(((Integer)paramObject).intValue());
    }
    for (;;)
    {
      if ((paramObject instanceof Long))
      {
        if (Math.abs(((Long)paramObject).longValue()) < 100L) {
          return String.valueOf(paramObject);
        }
        if (String.valueOf(paramObject).charAt(0) == '-') {}
        for (String str = "-";; str = "")
        {
          paramObject = String.valueOf(Math.abs(((Long)paramObject).longValue()));
          StringBuilder localStringBuilder = new StringBuilder();
          localStringBuilder.append(str);
          localStringBuilder.append(Math.round(Math.pow(10.0D, ((String)paramObject).length() - 1)));
          localStringBuilder.append("...");
          localStringBuilder.append(str);
          localStringBuilder.append(Math.round(Math.pow(10.0D, ((String)paramObject).length()) - 1.0D));
          return localStringBuilder.toString();
        }
      }
      if ((paramObject instanceof Boolean)) {
        return String.valueOf(paramObject);
      }
      if ((paramObject instanceof Throwable)) {
        return paramObject.getClass().getCanonicalName();
      }
      return "-";
    }
  }
  
  public static zzcp zzex()
  {
    return zzabh;
  }
  
  public final void zza(zzck paramzzck, String paramString)
  {
    if (paramzzck != null)
    {
      paramzzck = paramzzck.toString();
      paramString = String.valueOf(paramString);
      if (paramString.length() == 0) {
        break label41;
      }
    }
    label41:
    for (paramString = "Discarding hit. ".concat(paramString);; paramString = new String("Discarding hit. "))
    {
      zzd(paramString, paramzzck);
      return;
      paramzzck = "no hit data";
      break;
    }
  }
  
  public final void zza(Map<String, String> paramMap, String paramString)
  {
    if (paramMap != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        if (localStringBuilder.length() > 0) {
          localStringBuilder.append(',');
        }
        localStringBuilder.append((String)localEntry.getKey());
        localStringBuilder.append('=');
        localStringBuilder.append((String)localEntry.getValue());
      }
      paramMap = localStringBuilder.toString();
      paramString = String.valueOf(paramString);
      if (paramString.length() == 0) {
        break label135;
      }
    }
    label135:
    for (paramString = "Discarding hit. ".concat(paramString);; paramString = new String("Discarding hit. "))
    {
      zzd(paramString, paramMap);
      return;
      paramMap = "no hit data";
      break;
    }
  }
  
  protected final void zzag()
  {
    try
    {
      zzabh = this;
      return;
    }
    finally {}
  }
  
  public final void zzb(int paramInt, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    int i = 0;
    for (;;)
    {
      try
      {
        Preconditions.checkNotNull(paramString);
        if (paramInt >= 0) {
          break label183;
        }
        paramInt = i;
      }
      finally {}
      if (zzbz().zzdw())
      {
        c1 = 'C';
        char c2 = "01VDIWEA?".charAt(paramInt);
        String str = zzav.VERSION;
        paramString = zzc(paramString, zzd(paramObject1), zzd(paramObject2), zzd(paramObject3));
        paramObject1 = String.valueOf(str).length() + 4 + String.valueOf(paramString).length() + "3" + c2 + c1 + str + ":" + paramString;
        paramString = (String)paramObject1;
        if (((String)paramObject1).length() > 1024) {
          paramString = ((String)paramObject1).substring(0, 1024);
        }
        paramObject1 = zzbw().zzcp();
        if (paramObject1 != null) {
          ((zzct)paramObject1).zzfk().zzad(paramString);
        }
        return;
      }
      char c1 = 'c';
      continue;
      label183:
      while (paramInt < 9) {
        break;
      }
      paramInt = 8;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */