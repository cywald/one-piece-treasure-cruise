package com.google.android.gms.internal.measurement;

import java.lang.reflect.Method;

final class zzuy
{
  private static final Class<?> zzbvi = ;
  
  private static final zzuz zzfz(String paramString)
    throws Exception
  {
    return (zzuz)zzbvi.getDeclaredMethod(paramString, new Class[0]).invoke(null, new Object[0]);
  }
  
  private static Class<?> zzvk()
  {
    try
    {
      Class localClass = Class.forName("com.google.protobuf.ExtensionRegistry");
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException) {}
    return null;
  }
  
  public static zzuz zzvl()
  {
    if (zzbvi != null) {
      try
      {
        zzuz localzzuz = zzfz("getEmptyRegistry");
        return localzzuz;
      }
      catch (Exception localException) {}
    }
    return zzuz.zzbvm;
  }
  
  static zzuz zzvm()
  {
    Object localObject3 = null;
    Object localObject1 = localObject3;
    if (zzbvi != null) {}
    try
    {
      localObject1 = zzfz("loadGeneratedRegistry");
      localObject3 = localObject1;
      if (localObject1 == null) {
        localObject3 = zzuz.zzvm();
      }
      localObject1 = localObject3;
      if (localObject3 == null) {
        localObject1 = zzvl();
      }
      return (zzuz)localObject1;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = localObject3;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */