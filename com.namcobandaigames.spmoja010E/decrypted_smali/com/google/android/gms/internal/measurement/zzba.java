package com.google.android.gms.internal.measurement;

import android.content.ComponentName;
import android.os.RemoteException;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Collections;
import java.util.List;

@VisibleForTesting
public final class zzba
  extends zzau
{
  private final zzbc zzww;
  private zzcl zzwx;
  private final zzbz zzwy;
  private final zzdc zzwz;
  
  protected zzba(zzaw paramzzaw)
  {
    super(paramzzaw);
    this.zzwz = new zzdc(paramzzaw.zzbx());
    this.zzww = new zzbc(this);
    this.zzwy = new zzbb(this, paramzzaw);
  }
  
  private final void onServiceDisconnected(ComponentName paramComponentName)
  {
    
    if (this.zzwx != null)
    {
      this.zzwx = null;
      zza("Disconnected from device AnalyticsService", paramComponentName);
      zzcc().zzbu();
    }
  }
  
  private final void zza(zzcl paramzzcl)
  {
    zzk.zzaf();
    this.zzwx = paramzzcl;
    zzcy();
    zzcc().onServiceConnected();
  }
  
  private final void zzcy()
  {
    this.zzwz.start();
    this.zzwy.zzh(((Long)zzcf.zzaaf.get()).longValue());
  }
  
  private final void zzcz()
  {
    
    if (!isConnected()) {
      return;
    }
    zzq("Inactivity, disconnecting from device AnalyticsService");
    disconnect();
  }
  
  public final boolean connect()
  {
    zzk.zzaf();
    zzcl();
    if (this.zzwx != null) {
      return true;
    }
    zzcl localzzcl = this.zzww.zzda();
    if (localzzcl != null)
    {
      this.zzwx = localzzcl;
      zzcy();
      return true;
    }
    return false;
  }
  
  public final void disconnect()
  {
    zzk.zzaf();
    zzcl();
    try
    {
      ConnectionTracker.getInstance().unbindService(getContext(), this.zzww);
      if (this.zzwx != null)
      {
        this.zzwx = null;
        zzcc().zzbu();
      }
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    catch (IllegalStateException localIllegalStateException)
    {
      for (;;) {}
    }
  }
  
  public final boolean isConnected()
  {
    zzk.zzaf();
    zzcl();
    return this.zzwx != null;
  }
  
  protected final void zzag() {}
  
  public final boolean zzb(zzck paramzzck)
  {
    Preconditions.checkNotNull(paramzzck);
    zzk.zzaf();
    zzcl();
    zzcl localzzcl = this.zzwx;
    if (localzzcl == null) {
      return false;
    }
    if (paramzzck.zzet()) {}
    for (String str = zzbx.zzed();; str = zzbx.zzee())
    {
      List localList = Collections.emptyList();
      try
      {
        localzzcl.zza(paramzzck.zzcw(), paramzzck.zzer(), str, localList);
        zzcy();
        return true;
      }
      catch (RemoteException paramzzck)
      {
        zzq("Failed to send hits to AnalyticsService");
      }
    }
    return false;
  }
  
  public final boolean zzcx()
  {
    zzk.zzaf();
    zzcl();
    zzcl localzzcl = this.zzwx;
    if (localzzcl == null) {
      return false;
    }
    try
    {
      localzzcl.zzbr();
      zzcy();
      return true;
    }
    catch (RemoteException localRemoteException)
    {
      zzq("Failed to clear hits from AnalyticsService");
    }
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzba.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */