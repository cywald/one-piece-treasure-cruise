package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzub
  extends zztz<Boolean>
  implements zzvs<Boolean>, zzxe, RandomAccess
{
  private static final zzub zzbtx;
  private int size;
  private boolean[] zzbty;
  
  static
  {
    zzub localzzub = new zzub();
    zzbtx = localzzub;
    localzzub.zzsm();
  }
  
  zzub()
  {
    this(new boolean[10], 0);
  }
  
  private zzub(boolean[] paramArrayOfBoolean, int paramInt)
  {
    this.zzbty = paramArrayOfBoolean;
    this.size = paramInt;
  }
  
  private final void zza(int paramInt, boolean paramBoolean)
  {
    zztx();
    if ((paramInt < 0) || (paramInt > this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
    if (this.size < this.zzbty.length) {
      System.arraycopy(this.zzbty, paramInt, this.zzbty, paramInt + 1, this.size - paramInt);
    }
    for (;;)
    {
      this.zzbty[paramInt] = paramBoolean;
      this.size += 1;
      this.modCount += 1;
      return;
      boolean[] arrayOfBoolean = new boolean[this.size * 3 / 2 + 1];
      System.arraycopy(this.zzbty, 0, arrayOfBoolean, 0, paramInt);
      System.arraycopy(this.zzbty, paramInt, arrayOfBoolean, paramInt + 1, this.size - paramInt);
      this.zzbty = arrayOfBoolean;
    }
  }
  
  private final void zzai(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
  }
  
  private final String zzaj(int paramInt)
  {
    int i = this.size;
    return 35 + "Index:" + paramInt + ", Size:" + i;
  }
  
  public final boolean addAll(Collection<? extends Boolean> paramCollection)
  {
    boolean bool = false;
    zztx();
    zzvo.checkNotNull(paramCollection);
    if (!(paramCollection instanceof zzub)) {
      bool = super.addAll(paramCollection);
    }
    do
    {
      return bool;
      paramCollection = (zzub)paramCollection;
    } while (paramCollection.size == 0);
    if (Integer.MAX_VALUE - this.size < paramCollection.size) {
      throw new OutOfMemoryError();
    }
    int i = this.size + paramCollection.size;
    if (i > this.zzbty.length) {
      this.zzbty = Arrays.copyOf(this.zzbty, i);
    }
    System.arraycopy(paramCollection.zzbty, 0, this.zzbty, this.size, paramCollection.size);
    this.size = i;
    this.modCount += 1;
    return true;
  }
  
  public final void addBoolean(boolean paramBoolean)
  {
    zza(this.size, paramBoolean);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      return bool1;
      if (!(paramObject instanceof zzub)) {
        return super.equals(paramObject);
      }
      paramObject = (zzub)paramObject;
      bool1 = bool2;
    } while (this.size != ((zzub)paramObject).size);
    paramObject = ((zzub)paramObject).zzbty;
    int i = 0;
    for (;;)
    {
      if (i >= this.size) {
        break label81;
      }
      bool1 = bool2;
      if (this.zzbty[i] != paramObject[i]) {
        break;
      }
      i += 1;
    }
    label81:
    return true;
  }
  
  public final int hashCode()
  {
    int j = 1;
    int i = 0;
    while (i < this.size)
    {
      j = j * 31 + zzvo.zzw(this.zzbty[i]);
      i += 1;
    }
    return j;
  }
  
  public final boolean remove(Object paramObject)
  {
    boolean bool2 = false;
    zztx();
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      if (i < this.size)
      {
        if (paramObject.equals(Boolean.valueOf(this.zzbty[i])))
        {
          System.arraycopy(this.zzbty, i + 1, this.zzbty, i, this.size - i);
          this.size -= 1;
          this.modCount += 1;
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
      i += 1;
    }
  }
  
  protected final void removeRange(int paramInt1, int paramInt2)
  {
    zztx();
    if (paramInt2 < paramInt1) {
      throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }
    System.arraycopy(this.zzbty, paramInt2, this.zzbty, paramInt1, this.size - paramInt2);
    this.size -= paramInt2 - paramInt1;
    this.modCount += 1;
  }
  
  public final int size()
  {
    return this.size;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzub.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */