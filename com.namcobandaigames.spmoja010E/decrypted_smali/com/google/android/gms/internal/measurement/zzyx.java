package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzyx
{
  private final byte[] buffer;
  private int zzbuh;
  private int zzbui = 64;
  private int zzbuj = 67108864;
  private int zzbun;
  private int zzbup;
  private int zzbuq = Integer.MAX_VALUE;
  private final int zzcev;
  private final int zzcew;
  private int zzcex;
  private int zzcey;
  private zzuo zzcez;
  
  private zzyx(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.buffer = paramArrayOfByte;
    this.zzcev = paramInt1;
    paramInt2 = paramInt1 + paramInt2;
    this.zzcex = paramInt2;
    this.zzcew = paramInt2;
    this.zzcey = paramInt1;
  }
  
  private final void zzas(int paramInt)
    throws IOException
  {
    if (paramInt < 0) {
      throw zzzf.zzyx();
    }
    if (this.zzcey + paramInt > this.zzbuq)
    {
      zzas(this.zzbuq - this.zzcey);
      throw zzzf.zzyw();
    }
    if (paramInt <= this.zzcex - this.zzcey)
    {
      this.zzcey += paramInt;
      return;
    }
    throw zzzf.zzyw();
  }
  
  public static zzyx zzj(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return new zzyx(paramArrayOfByte, 0, paramInt2);
  }
  
  public static zzyx zzn(byte[] paramArrayOfByte)
  {
    return zzj(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  private final void zzvc()
  {
    this.zzcex += this.zzbun;
    int i = this.zzcex;
    if (i > this.zzbuq)
    {
      this.zzbun = (i - this.zzbuq);
      this.zzcex -= this.zzbun;
      return;
    }
    this.zzbun = 0;
  }
  
  private final byte zzvd()
    throws IOException
  {
    if (this.zzcey == this.zzcex) {
      throw zzzf.zzyw();
    }
    byte[] arrayOfByte = this.buffer;
    int i = this.zzcey;
    this.zzcey = (i + 1);
    return arrayOfByte[i];
  }
  
  public final int getPosition()
  {
    return this.zzcey - this.zzcev;
  }
  
  public final String readString()
    throws IOException
  {
    int i = zzuy();
    if (i < 0) {
      throw zzzf.zzyx();
    }
    if (i > this.zzcex - this.zzcey) {
      throw zzzf.zzyw();
    }
    String str = new String(this.buffer, this.zzcey, i, zzze.UTF_8);
    this.zzcey = (i + this.zzcey);
    return str;
  }
  
  public final <T extends zzvm<T, ?>> T zza(zzxd<T> paramzzxd)
    throws IOException
  {
    int i;
    int j;
    try
    {
      if (this.zzcez == null) {
        this.zzcez = zzuo.zzd(this.buffer, this.zzcev, this.zzcew);
      }
      i = this.zzcez.zzux();
      j = this.zzcey - this.zzcev;
      if (i > j) {
        throw new IOException(String.format("CodedInputStream read ahead of CodedInputByteBufferNano: %s > %s", new Object[] { Integer.valueOf(i), Integer.valueOf(j) }));
      }
    }
    catch (zzvt paramzzxd)
    {
      throw new zzzf("", paramzzxd);
    }
    this.zzcez.zzas(j - i);
    this.zzcez.zzap(this.zzbui - this.zzbuh);
    paramzzxd = (zzvm)this.zzcez.zza(paramzzxd, zzuz.zzvp());
    zzao(this.zzbup);
    return paramzzxd;
  }
  
  public final void zza(zzzg paramzzzg)
    throws IOException
  {
    int i = zzuy();
    if (this.zzbuh >= this.zzbui) {
      throw zzzf.zzyz();
    }
    i = zzaq(i);
    this.zzbuh += 1;
    paramzzzg.zza(this);
    zzan(0);
    this.zzbuh -= 1;
    zzar(i);
  }
  
  public final void zza(zzzg paramzzzg, int paramInt)
    throws IOException
  {
    if (this.zzbuh >= this.zzbui) {
      throw zzzf.zzyz();
    }
    this.zzbuh += 1;
    paramzzzg.zza(this);
    zzan(paramInt << 3 | 0x4);
    this.zzbuh -= 1;
  }
  
  public final void zzan(int paramInt)
    throws zzzf
  {
    if (this.zzbup != paramInt) {
      throw new zzzf("Protocol message end-group tag did not match expected tag.");
    }
  }
  
  public final boolean zzao(int paramInt)
    throws IOException
  {
    switch (paramInt & 0x7)
    {
    default: 
      throw new zzzf("Protocol message tag had invalid wire type.");
    case 0: 
      zzuy();
      return true;
    case 1: 
      zzvb();
      return true;
    case 2: 
      zzas(zzuy());
      return true;
    case 3: 
      int i;
      do
      {
        i = zzug();
      } while ((i != 0) && (zzao(i)));
      zzan(paramInt >>> 3 << 3 | 0x4);
      return true;
    case 4: 
      return false;
    }
    zzva();
    return true;
  }
  
  public final int zzaq(int paramInt)
    throws zzzf
  {
    if (paramInt < 0) {
      throw zzzf.zzyx();
    }
    paramInt = this.zzcey + paramInt;
    int i = this.zzbuq;
    if (paramInt > i) {
      throw zzzf.zzyw();
    }
    this.zzbuq = paramInt;
    zzvc();
    return i;
  }
  
  public final void zzar(int paramInt)
  {
    this.zzbuq = paramInt;
    zzvc();
  }
  
  public final void zzby(int paramInt)
  {
    zzt(paramInt, this.zzbup);
  }
  
  public final byte[] zzs(int paramInt1, int paramInt2)
  {
    if (paramInt2 == 0) {
      return zzzj.zzcfx;
    }
    byte[] arrayOfByte = new byte[paramInt2];
    int i = this.zzcev;
    System.arraycopy(this.buffer, i + paramInt1, arrayOfByte, 0, paramInt2);
    return arrayOfByte;
  }
  
  final void zzt(int paramInt1, int paramInt2)
  {
    if (paramInt1 > this.zzcey - this.zzcev)
    {
      paramInt2 = this.zzcey;
      int i = this.zzcev;
      throw new IllegalArgumentException(50 + "Position " + paramInt1 + " is beyond current " + (paramInt2 - i));
    }
    if (paramInt1 < 0) {
      throw new IllegalArgumentException(24 + "Bad position " + paramInt1);
    }
    this.zzcey = (this.zzcev + paramInt1);
    this.zzbup = paramInt2;
  }
  
  public final int zzug()
    throws IOException
  {
    if (this.zzcey == this.zzcex)
    {
      this.zzbup = 0;
      return 0;
    }
    this.zzbup = zzuy();
    if (this.zzbup == 0) {
      throw new zzzf("Protocol message contained an invalid tag (zero).");
    }
    return this.zzbup;
  }
  
  public final boolean zzum()
    throws IOException
  {
    return zzuy() != 0;
  }
  
  public final int zzuy()
    throws IOException
  {
    int i = zzvd();
    if (i >= 0) {}
    int k;
    do
    {
      return i;
      i &= 0x7F;
      j = zzvd();
      if (j >= 0) {
        return i | j << 7;
      }
      i |= (j & 0x7F) << 7;
      j = zzvd();
      if (j >= 0) {
        return i | j << 14;
      }
      i |= (j & 0x7F) << 14;
      k = zzvd();
      if (k >= 0) {
        return i | k << 21;
      }
      j = zzvd();
      k = i | (k & 0x7F) << 21 | j << 28;
      i = k;
    } while (j >= 0);
    int j = 0;
    for (;;)
    {
      if (j >= 5) {
        break label133;
      }
      i = k;
      if (zzvd() >= 0) {
        break;
      }
      j += 1;
    }
    label133:
    throw zzzf.zzyy();
  }
  
  public final long zzuz()
    throws IOException
  {
    int i = 0;
    long l = 0L;
    while (i < 64)
    {
      int j = zzvd();
      l |= (j & 0x7F) << i;
      if ((j & 0x80) == 0) {
        return l;
      }
      i += 7;
    }
    throw zzzf.zzyy();
  }
  
  public final int zzva()
    throws IOException
  {
    return zzvd() & 0xFF | (zzvd() & 0xFF) << 8 | (zzvd() & 0xFF) << 16 | (zzvd() & 0xFF) << 24;
  }
  
  public final long zzvb()
    throws IOException
  {
    int i = zzvd();
    int j = zzvd();
    int k = zzvd();
    int m = zzvd();
    int n = zzvd();
    int i1 = zzvd();
    int i2 = zzvd();
    int i3 = zzvd();
    long l = i;
    return (j & 0xFF) << 8 | l & 0xFF | (k & 0xFF) << 16 | (m & 0xFF) << 24 | (n & 0xFF) << 32 | (i1 & 0xFF) << 40 | (i2 & 0xFF) << 48 | (i3 & 0xFF) << 56;
  }
  
  public final int zzyr()
  {
    if (this.zzbuq == Integer.MAX_VALUE) {
      return -1;
    }
    int i = this.zzcey;
    return this.zzbuq - i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */