package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzuw
  extends zztz<Double>
  implements zzvs<Double>, zzxe, RandomAccess
{
  private static final zzuw zzbvg;
  private int size;
  private double[] zzbvh;
  
  static
  {
    zzuw localzzuw = new zzuw();
    zzbvg = localzzuw;
    localzzuw.zzsm();
  }
  
  zzuw()
  {
    this(new double[10], 0);
  }
  
  private zzuw(double[] paramArrayOfDouble, int paramInt)
  {
    this.zzbvh = paramArrayOfDouble;
    this.size = paramInt;
  }
  
  private final void zzai(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
  }
  
  private final String zzaj(int paramInt)
  {
    int i = this.size;
    return 35 + "Index:" + paramInt + ", Size:" + i;
  }
  
  private final void zzc(int paramInt, double paramDouble)
  {
    zztx();
    if ((paramInt < 0) || (paramInt > this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
    if (this.size < this.zzbvh.length) {
      System.arraycopy(this.zzbvh, paramInt, this.zzbvh, paramInt + 1, this.size - paramInt);
    }
    for (;;)
    {
      this.zzbvh[paramInt] = paramDouble;
      this.size += 1;
      this.modCount += 1;
      return;
      double[] arrayOfDouble = new double[this.size * 3 / 2 + 1];
      System.arraycopy(this.zzbvh, 0, arrayOfDouble, 0, paramInt);
      System.arraycopy(this.zzbvh, paramInt, arrayOfDouble, paramInt + 1, this.size - paramInt);
      this.zzbvh = arrayOfDouble;
    }
  }
  
  public final boolean addAll(Collection<? extends Double> paramCollection)
  {
    boolean bool = false;
    zztx();
    zzvo.checkNotNull(paramCollection);
    if (!(paramCollection instanceof zzuw)) {
      bool = super.addAll(paramCollection);
    }
    do
    {
      return bool;
      paramCollection = (zzuw)paramCollection;
    } while (paramCollection.size == 0);
    if (Integer.MAX_VALUE - this.size < paramCollection.size) {
      throw new OutOfMemoryError();
    }
    int i = this.size + paramCollection.size;
    if (i > this.zzbvh.length) {
      this.zzbvh = Arrays.copyOf(this.zzbvh, i);
    }
    System.arraycopy(paramCollection.zzbvh, 0, this.zzbvh, this.size, paramCollection.size);
    this.size = i;
    this.modCount += 1;
    return true;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      return bool1;
      if (!(paramObject instanceof zzuw)) {
        return super.equals(paramObject);
      }
      paramObject = (zzuw)paramObject;
      bool1 = bool2;
    } while (this.size != ((zzuw)paramObject).size);
    paramObject = ((zzuw)paramObject).zzbvh;
    int i = 0;
    for (;;)
    {
      if (i >= this.size) {
        break label82;
      }
      bool1 = bool2;
      if (this.zzbvh[i] != paramObject[i]) {
        break;
      }
      i += 1;
    }
    label82:
    return true;
  }
  
  public final int hashCode()
  {
    int j = 1;
    int i = 0;
    while (i < this.size)
    {
      j = j * 31 + zzvo.zzbf(Double.doubleToLongBits(this.zzbvh[i]));
      i += 1;
    }
    return j;
  }
  
  public final boolean remove(Object paramObject)
  {
    boolean bool2 = false;
    zztx();
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      if (i < this.size)
      {
        if (paramObject.equals(Double.valueOf(this.zzbvh[i])))
        {
          System.arraycopy(this.zzbvh, i + 1, this.zzbvh, i, this.size - i);
          this.size -= 1;
          this.modCount += 1;
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
      i += 1;
    }
  }
  
  protected final void removeRange(int paramInt1, int paramInt2)
  {
    zztx();
    if (paramInt2 < paramInt1) {
      throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }
    System.arraycopy(this.zzbvh, paramInt2, this.zzbvh, paramInt1, this.size - paramInt2);
    this.size -= paramInt2 - paramInt1;
    this.modCount += 1;
  }
  
  public final int size()
  {
    return this.size;
  }
  
  public final void zzd(double paramDouble)
  {
    zzc(this.size, paramDouble);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */