package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class zzyy
{
  private final ByteBuffer zzbva;
  private zzut zzcfa;
  private int zzcfb;
  
  private zzyy(ByteBuffer paramByteBuffer)
  {
    this.zzbva = paramByteBuffer;
    this.zzbva.order(ByteOrder.LITTLE_ENDIAN);
  }
  
  private zzyy(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this(ByteBuffer.wrap(paramArrayOfByte, paramInt1, paramInt2));
  }
  
  private static int zza(CharSequence paramCharSequence)
  {
    int k = 0;
    int n = paramCharSequence.length();
    int j = 0;
    while ((j < n) && (paramCharSequence.charAt(j) < '')) {
      j += 1;
    }
    for (;;)
    {
      int i;
      if (j < n)
      {
        int m = paramCharSequence.charAt(j);
        if (m < 2048)
        {
          i += (127 - m >>> 31);
          j += 1;
        }
        else
        {
          int i2 = paramCharSequence.length();
          if (j < i2)
          {
            int i3 = paramCharSequence.charAt(j);
            if (i3 < 2048)
            {
              k += (127 - i3 >>> 31);
              m = j;
            }
            for (;;)
            {
              j = m + 1;
              break;
              int i1 = k + 2;
              m = j;
              k = i1;
              if (55296 <= i3)
              {
                m = j;
                k = i1;
                if (i3 <= 57343)
                {
                  if (Character.codePointAt(paramCharSequence, j) < 65536) {
                    throw new IllegalArgumentException(39 + "Unpaired surrogate at index " + j);
                  }
                  m = j + 1;
                  k = i1;
                }
              }
            }
          }
          i += k;
        }
      }
      else
      {
        for (;;)
        {
          if (i < n)
          {
            long l = i;
            throw new IllegalArgumentException(54 + "UTF-8 length does not fit in int: " + (l + 4294967296L));
          }
          return i;
        }
        i = n;
      }
    }
  }
  
  public static int zzb(int paramInt, zzzg paramzzzg)
  {
    paramInt = zzbb(paramInt);
    int i = paramzzzg.zzvu();
    return paramInt + (i + zzbj(i));
  }
  
  public static int zzbb(int paramInt)
  {
    return zzbj(paramInt << 3);
  }
  
  public static int zzbc(int paramInt)
  {
    if (paramInt >= 0) {
      return zzbj(paramInt);
    }
    return 10;
  }
  
  private final void zzbh(long paramLong)
    throws IOException
  {
    for (;;)
    {
      if ((0xFFFFFFFFFFFFFF80 & paramLong) == 0L)
      {
        zzbz((int)paramLong);
        return;
      }
      zzbz((int)paramLong & 0x7F | 0x80);
      paramLong >>>= 7;
    }
  }
  
  public static int zzbi(long paramLong)
  {
    if ((0xFFFFFFFFFFFFFF80 & paramLong) == 0L) {
      return 1;
    }
    if ((0xFFFFFFFFFFFFC000 & paramLong) == 0L) {
      return 2;
    }
    if ((0xFFFFFFFFFFE00000 & paramLong) == 0L) {
      return 3;
    }
    if ((0xFFFFFFFFF0000000 & paramLong) == 0L) {
      return 4;
    }
    if ((0xFFFFFFF800000000 & paramLong) == 0L) {
      return 5;
    }
    if ((0xFFFFFC0000000000 & paramLong) == 0L) {
      return 6;
    }
    if ((0xFFFE000000000000 & paramLong) == 0L) {
      return 7;
    }
    if ((0xFF00000000000000 & paramLong) == 0L) {
      return 8;
    }
    if ((0x8000000000000000 & paramLong) == 0L) {
      return 9;
    }
    return 10;
  }
  
  public static int zzbj(int paramInt)
  {
    if ((paramInt & 0xFFFFFF80) == 0) {
      return 1;
    }
    if ((paramInt & 0xC000) == 0) {
      return 2;
    }
    if ((0xFFE00000 & paramInt) == 0) {
      return 3;
    }
    if ((0xF0000000 & paramInt) == 0) {
      return 4;
    }
    return 5;
  }
  
  private final void zzbz(int paramInt)
    throws IOException
  {
    byte b = (byte)paramInt;
    if (!this.zzbva.hasRemaining()) {
      throw new zzyz(this.zzbva.position(), this.zzbva.limit());
    }
    this.zzbva.put(b);
  }
  
  public static int zzc(int paramInt, String paramString)
  {
    return zzbb(paramInt) + zzfx(paramString);
  }
  
  public static int zzd(int paramInt, long paramLong)
  {
    return zzbb(paramInt) + zzbi(paramLong);
  }
  
  private static void zzd(CharSequence paramCharSequence, ByteBuffer paramByteBuffer)
  {
    int n = 0;
    int m = 0;
    if (paramByteBuffer.isReadOnly()) {
      throw new ReadOnlyBufferException();
    }
    if (paramByteBuffer.hasArray()) {}
    for (;;)
    {
      byte[] arrayOfByte;
      int i2;
      int i3;
      try
      {
        arrayOfByte = paramByteBuffer.array();
        n = paramByteBuffer.arrayOffset() + paramByteBuffer.position();
        i1 = paramByteBuffer.remaining();
        i2 = paramCharSequence.length();
        i3 = n + i1;
        if ((m >= i2) || (m + n >= i3)) {
          break label915;
        }
        i1 = paramCharSequence.charAt(m);
        if (i1 >= 128) {
          break label915;
        }
        arrayOfByte[(n + m)] = ((byte)i1);
        m += 1;
        continue;
        paramByteBuffer.position(m - paramByteBuffer.arrayOffset());
        return;
      }
      catch (ArrayIndexOutOfBoundsException paramCharSequence)
      {
        paramByteBuffer = new BufferOverflowException();
        paramByteBuffer.initCause(paramCharSequence);
        throw paramByteBuffer;
      }
      char c;
      if (m < i2)
      {
        int j = paramCharSequence.charAt(m);
        if ((j < 128) && (n < i3))
        {
          i1 = n + 1;
          arrayOfByte[n] = ((byte)j);
          n = m;
          m = i1;
        }
        else
        {
          int i4;
          if ((j < 2048) && (n <= i3 - 2))
          {
            i4 = n + 1;
            arrayOfByte[n] = ((byte)(j >>> 6 | 0x3C0));
            i1 = i4 + 1;
            arrayOfByte[i4] = ((byte)(j & 0x3F | 0x80));
            n = m;
            m = i1;
          }
          else if (((j < 55296) || (57343 < j)) && (n <= i3 - 3))
          {
            i1 = n + 1;
            int i = (byte)(j >>> 12 | 0x1E0);
            arrayOfByte[n] = i;
            n = i1 + 1;
            arrayOfByte[i1] = ((byte)(j >>> 6 & 0x3F | 0x80));
            i1 = n + 1;
            arrayOfByte[n] = ((byte)(j & 0x3F | 0x80));
            n = m;
            m = i1;
          }
          else if (n <= i3 - 4)
          {
            i1 = m;
            if (m + 1 != paramCharSequence.length())
            {
              m += 1;
              c = paramCharSequence.charAt(m);
              if (!Character.isSurrogatePair(j, c)) {
                i1 = m;
              }
            }
            else
            {
              throw new IllegalArgumentException(39 + "Unpaired surrogate at index " + (i1 - 1));
            }
            i4 = Character.toCodePoint(j, c);
            i1 = n + 1;
            arrayOfByte[n] = ((byte)(i4 >>> 18 | 0xF0));
            n = i1 + 1;
            arrayOfByte[i1] = ((byte)(i4 >>> 12 & 0x3F | 0x80));
            int i5 = n + 1;
            arrayOfByte[n] = ((byte)(i4 >>> 6 & 0x3F | 0x80));
            i1 = i5 + 1;
            arrayOfByte[i5] = ((byte)(i4 & 0x3F | 0x80));
            n = m;
            m = i1;
          }
          else
          {
            throw new ArrayIndexOutOfBoundsException(37 + "Failed writing " + j + " at index " + n);
          }
        }
      }
      else
      {
        m = n;
        continue;
        i1 = paramCharSequence.length();
        m = n;
        if (m < i1)
        {
          int k = paramCharSequence.charAt(m);
          if (k < 128) {
            paramByteBuffer.put((byte)k);
          }
          for (;;)
          {
            m += 1;
            break;
            if (k < 2048)
            {
              paramByteBuffer.put((byte)(k >>> 6 | 0x3C0));
              paramByteBuffer.put((byte)(k & 0x3F | 0x80));
            }
            else if ((k < 55296) || (57343 < k))
            {
              paramByteBuffer.put((byte)(k >>> 12 | 0x1E0));
              paramByteBuffer.put((byte)(k >>> 6 & 0x3F | 0x80));
              paramByteBuffer.put((byte)(k & 0x3F | 0x80));
            }
            else
            {
              n = m;
              if (m + 1 != paramCharSequence.length())
              {
                m += 1;
                c = paramCharSequence.charAt(m);
                if (!Character.isSurrogatePair(k, c)) {
                  n = m;
                }
              }
              else
              {
                throw new IllegalArgumentException(39 + "Unpaired surrogate at index " + (n - 1));
              }
              n = Character.toCodePoint(k, c);
              paramByteBuffer.put((byte)(n >>> 18 | 0xF0));
              paramByteBuffer.put((byte)(n >>> 12 & 0x3F | 0x80));
              paramByteBuffer.put((byte)(n >>> 6 & 0x3F | 0x80));
              paramByteBuffer.put((byte)(n & 0x3F | 0x80));
            }
          }
          label915:
          if (m == i2) {
            m = n + i2;
          }
        }
        else
        {
          return;
        }
        n += m;
        continue;
      }
      int i1 = n + 1;
      n = m;
      m = i1;
    }
  }
  
  public static int zzfx(String paramString)
  {
    int i = zza(paramString);
    return i + zzbj(i);
  }
  
  public static int zzh(int paramInt1, int paramInt2)
  {
    return zzbb(paramInt1) + zzbc(paramInt2);
  }
  
  public static zzyy zzk(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return new zzyy(paramArrayOfByte, 0, paramInt2);
  }
  
  public static zzyy zzo(byte[] paramArrayOfByte)
  {
    return zzk(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  private final zzut zzys()
    throws IOException
  {
    if (this.zzcfa == null)
    {
      this.zzcfa = zzut.zza(this.zzbva);
      this.zzcfb = this.zzbva.position();
    }
    for (;;)
    {
      return this.zzcfa;
      if (this.zzcfb != this.zzbva.position())
      {
        this.zzcfa.write(this.zzbva.array(), this.zzcfb, this.zzbva.position() - this.zzcfb);
        this.zzcfb = this.zzbva.position();
      }
    }
  }
  
  public final void zza(int paramInt, double paramDouble)
    throws IOException
  {
    zzc(paramInt, 1);
    long l = Double.doubleToLongBits(paramDouble);
    if (this.zzbva.remaining() < 8) {
      throw new zzyz(this.zzbva.position(), this.zzbva.limit());
    }
    this.zzbva.putLong(l);
  }
  
  public final void zza(int paramInt, float paramFloat)
    throws IOException
  {
    zzc(paramInt, 5);
    paramInt = Float.floatToIntBits(paramFloat);
    if (this.zzbva.remaining() < 4) {
      throw new zzyz(this.zzbva.position(), this.zzbva.limit());
    }
    this.zzbva.putInt(paramInt);
  }
  
  public final void zza(int paramInt, long paramLong)
    throws IOException
  {
    zzc(paramInt, 0);
    zzbh(paramLong);
  }
  
  public final void zza(int paramInt, zzzg paramzzzg)
    throws IOException
  {
    zzc(paramInt, 2);
    zzb(paramzzzg);
  }
  
  public final void zzb(int paramInt, String paramString)
    throws IOException
  {
    zzc(paramInt, 2);
    int i;
    try
    {
      paramInt = zzbj(paramString.length());
      if (paramInt != zzbj(paramString.length() * 3)) {
        break label156;
      }
      i = this.zzbva.position();
      if (this.zzbva.remaining() < paramInt) {
        throw new zzyz(paramInt + i, this.zzbva.limit());
      }
    }
    catch (BufferOverflowException paramString)
    {
      zzyz localzzyz = new zzyz(this.zzbva.position(), this.zzbva.limit());
      localzzyz.initCause(paramString);
      throw localzzyz;
    }
    this.zzbva.position(i + paramInt);
    zzd(paramString, this.zzbva);
    int j = this.zzbva.position();
    this.zzbva.position(i);
    zzca(j - i - paramInt);
    this.zzbva.position(j);
    return;
    label156:
    zzca(zza(paramString));
    zzd(paramString, this.zzbva);
  }
  
  public final void zzb(int paramInt, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    zzc(paramInt, 0);
    paramInt = i;
    if (paramBoolean) {
      paramInt = 1;
    }
    byte b = (byte)paramInt;
    if (!this.zzbva.hasRemaining()) {
      throw new zzyz(this.zzbva.position(), this.zzbva.limit());
    }
    this.zzbva.put(b);
  }
  
  public final void zzb(zzzg paramzzzg)
    throws IOException
  {
    zzca(paramzzzg.zzza());
    paramzzzg.zza(this);
  }
  
  public final void zzc(int paramInt1, int paramInt2)
    throws IOException
  {
    zzca(paramInt1 << 3 | paramInt2);
  }
  
  public final void zzca(int paramInt)
    throws IOException
  {
    for (;;)
    {
      if ((paramInt & 0xFFFFFF80) == 0)
      {
        zzbz(paramInt);
        return;
      }
      zzbz(paramInt & 0x7F | 0x80);
      paramInt >>>= 7;
    }
  }
  
  public final void zzd(int paramInt1, int paramInt2)
    throws IOException
  {
    zzc(paramInt1, 0);
    if (paramInt2 >= 0)
    {
      zzca(paramInt2);
      return;
    }
    zzbh(paramInt2);
  }
  
  public final void zze(int paramInt, zzwt paramzzwt)
    throws IOException
  {
    zzut localzzut = zzys();
    localzzut.zza(paramInt, paramzzwt);
    localzzut.flush();
    this.zzcfb = this.zzbva.position();
  }
  
  public final void zzi(int paramInt, long paramLong)
    throws IOException
  {
    zzc(paramInt, 0);
    zzbh(paramLong);
  }
  
  public final void zzp(byte[] paramArrayOfByte)
    throws IOException
  {
    int i = paramArrayOfByte.length;
    if (this.zzbva.remaining() >= i)
    {
      this.zzbva.put(paramArrayOfByte, 0, i);
      return;
    }
    throw new zzyz(this.zzbva.position(), this.zzbva.limit());
  }
  
  public final void zzyt()
  {
    if (this.zzbva.remaining() != 0) {
      throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", new Object[] { Integer.valueOf(this.zzbva.remaining()) }));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */