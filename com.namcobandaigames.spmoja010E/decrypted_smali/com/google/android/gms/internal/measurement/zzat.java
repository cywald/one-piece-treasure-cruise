package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;

public class zzat
{
  private final zzaw zzvy;
  
  protected zzat(zzaw paramzzaw)
  {
    Preconditions.checkNotNull(paramzzaw);
    this.zzvy = paramzzaw;
  }
  
  private final void zza(int paramInt, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    Object localObject = null;
    if (this.zzvy != null) {
      localObject = this.zzvy.zzcn();
    }
    if (localObject != null)
    {
      String str = (String)zzcf.zzyx.get();
      if (Log.isLoggable(str, paramInt)) {
        Log.println(paramInt, str, zzcp.zzc(paramString, paramObject1, paramObject2, paramObject3));
      }
      if (paramInt >= 5) {
        ((zzcp)localObject).zzb(paramInt, paramString, paramObject1, paramObject2, paramObject3);
      }
    }
    do
    {
      return;
      localObject = (String)zzcf.zzyx.get();
    } while (!Log.isLoggable((String)localObject, paramInt));
    Log.println(paramInt, (String)localObject, zzc(paramString, paramObject1, paramObject2, paramObject3));
  }
  
  private static String zzb(Object paramObject)
  {
    if (paramObject == null) {
      return "";
    }
    if ((paramObject instanceof String)) {
      return (String)paramObject;
    }
    if ((paramObject instanceof Boolean))
    {
      if (paramObject == Boolean.TRUE) {
        return "true";
      }
      return "false";
    }
    if ((paramObject instanceof Throwable)) {
      return ((Throwable)paramObject).toString();
    }
    return paramObject.toString();
  }
  
  protected static String zzc(String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    String str1 = paramString;
    if (paramString == null) {
      str1 = "";
    }
    String str2 = zzb(paramObject1);
    paramObject2 = zzb(paramObject2);
    paramObject3 = zzb(paramObject3);
    StringBuilder localStringBuilder = new StringBuilder();
    paramString = "";
    if (!TextUtils.isEmpty(str1))
    {
      localStringBuilder.append(str1);
      paramString = ": ";
    }
    paramObject1 = paramString;
    if (!TextUtils.isEmpty(str2))
    {
      localStringBuilder.append(paramString);
      localStringBuilder.append(str2);
      paramObject1 = ", ";
    }
    paramString = (String)paramObject1;
    if (!TextUtils.isEmpty((CharSequence)paramObject2))
    {
      localStringBuilder.append((String)paramObject1);
      localStringBuilder.append((String)paramObject2);
      paramString = ", ";
    }
    if (!TextUtils.isEmpty((CharSequence)paramObject3))
    {
      localStringBuilder.append(paramString);
      localStringBuilder.append((String)paramObject3);
    }
    return localStringBuilder.toString();
  }
  
  public static boolean zzck()
  {
    return Log.isLoggable((String)zzcf.zzyx.get(), 2);
  }
  
  protected final Context getContext()
  {
    return this.zzvy.getContext();
  }
  
  public final void zza(String paramString, Object paramObject)
  {
    zza(2, paramString, paramObject, null, null);
  }
  
  public final void zza(String paramString, Object paramObject1, Object paramObject2)
  {
    zza(2, paramString, paramObject1, paramObject2, null);
  }
  
  public final void zza(String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    zza(3, paramString, paramObject1, paramObject2, paramObject3);
  }
  
  public final void zzb(String paramString, Object paramObject)
  {
    zza(3, paramString, paramObject, null, null);
  }
  
  public final void zzb(String paramString, Object paramObject1, Object paramObject2)
  {
    zza(3, paramString, paramObject1, paramObject2, null);
  }
  
  public final void zzb(String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    zza(5, paramString, paramObject1, paramObject2, paramObject3);
  }
  
  public final zzaw zzbw()
  {
    return this.zzvy;
  }
  
  protected final Clock zzbx()
  {
    return this.zzvy.zzbx();
  }
  
  protected final zzcp zzby()
  {
    return this.zzvy.zzby();
  }
  
  protected final zzbx zzbz()
  {
    return this.zzvy.zzbz();
  }
  
  public final void zzc(String paramString, Object paramObject)
  {
    zza(4, paramString, paramObject, null, null);
  }
  
  public final void zzc(String paramString, Object paramObject1, Object paramObject2)
  {
    zza(5, paramString, paramObject1, paramObject2, null);
  }
  
  protected final zzk zzca()
  {
    return this.zzvy.zzca();
  }
  
  public final GoogleAnalytics zzcb()
  {
    return this.zzvy.zzco();
  }
  
  protected final zzal zzcc()
  {
    return this.zzvy.zzcc();
  }
  
  protected final zzcc zzcd()
  {
    return this.zzvy.zzcd();
  }
  
  protected final zzdh zzce()
  {
    return this.zzvy.zzce();
  }
  
  protected final zzct zzcf()
  {
    return this.zzvy.zzcf();
  }
  
  protected final zzbo zzcg()
  {
    return this.zzvy.zzcr();
  }
  
  protected final zzak zzch()
  {
    return this.zzvy.zzcq();
  }
  
  protected final zzbh zzci()
  {
    return this.zzvy.zzci();
  }
  
  protected final zzcb zzcj()
  {
    return this.zzvy.zzcj();
  }
  
  public final void zzd(String paramString, Object paramObject)
  {
    zza(5, paramString, paramObject, null, null);
  }
  
  public final void zzd(String paramString, Object paramObject1, Object paramObject2)
  {
    zza(6, paramString, paramObject1, paramObject2, null);
  }
  
  public final void zze(String paramString, Object paramObject)
  {
    zza(6, paramString, paramObject, null, null);
  }
  
  public final void zzq(String paramString)
  {
    zza(2, paramString, null, null, null);
  }
  
  public final void zzr(String paramString)
  {
    zza(3, paramString, null, null, null);
  }
  
  public final void zzs(String paramString)
  {
    zza(4, paramString, null, null, null);
  }
  
  public final void zzt(String paramString)
  {
    zza(5, paramString, null, null, null);
  }
  
  public final void zzu(String paramString)
  {
    zza(6, paramString, null, null, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */