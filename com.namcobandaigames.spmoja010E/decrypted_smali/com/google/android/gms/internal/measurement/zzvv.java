package com.google.android.gms.internal.measurement;

public enum zzvv
{
  private final Class<?> zzbzx;
  private final Class<?> zzbzy;
  private final Object zzbzz;
  
  private zzvv(Class<?> paramClass1, Class<?> paramClass2, Object paramObject)
  {
    this.zzbzx = paramClass1;
    this.zzbzy = paramClass2;
    this.zzbzz = paramObject;
  }
  
  public final Class<?> zzws()
  {
    return this.zzbzy;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */