package com.google.android.gms.internal.measurement;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public final class zzye
  extends AbstractList<String>
  implements zzwc, RandomAccess
{
  private final zzwc zzccq;
  
  public zzye(zzwc paramzzwc)
  {
    this.zzccq = paramzzwc;
  }
  
  public final Object getRaw(int paramInt)
  {
    return this.zzccq.getRaw(paramInt);
  }
  
  public final Iterator<String> iterator()
  {
    return new zzyg(this);
  }
  
  public final ListIterator<String> listIterator(int paramInt)
  {
    return new zzyf(this, paramInt);
  }
  
  public final int size()
  {
    return this.zzccq.size();
  }
  
  public final void zzc(zzud paramzzud)
  {
    throw new UnsupportedOperationException();
  }
  
  public final List<?> zzwv()
  {
    return this.zzccq.zzwv();
  }
  
  public final zzwc zzww()
  {
    return this;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzye.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */