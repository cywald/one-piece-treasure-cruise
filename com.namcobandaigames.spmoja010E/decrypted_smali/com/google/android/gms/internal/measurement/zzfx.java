package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzfx
  extends zzza<zzfx>
{
  public Integer zzavo = null;
  public Boolean zzavp = null;
  public String zzavq = null;
  public String zzavr = null;
  public String zzavs = null;
  
  public zzfx()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  private final zzfx zzc(zzyx paramzzyx)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzyx.zzug();
      switch (i)
      {
      default: 
        if (super.zza(paramzzyx, i)) {}
        break;
      case 0: 
        return this;
      case 8: 
        int j = paramzzyx.getPosition();
        int k;
        try
        {
          k = paramzzyx.zzuy();
          if ((k < 0) || (k > 4)) {
            break label126;
          }
          this.zzavo = Integer.valueOf(k);
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          paramzzyx.zzby(j);
          zza(paramzzyx, i);
        }
        continue;
        throw new IllegalArgumentException(46 + k + " is not a valid enum ComparisonType");
      case 16: 
        this.zzavp = Boolean.valueOf(paramzzyx.zzum());
        break;
      case 26: 
        this.zzavq = paramzzyx.readString();
        break;
      case 34: 
        this.zzavr = paramzzyx.readString();
        break;
      case 42: 
        label126:
        this.zzavs = paramzzyx.readString();
      }
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzfx)) {
        return false;
      }
      paramObject = (zzfx)paramObject;
      if (this.zzavo == null)
      {
        if (((zzfx)paramObject).zzavo != null) {
          return false;
        }
      }
      else if (!this.zzavo.equals(((zzfx)paramObject).zzavo)) {
        return false;
      }
      if (this.zzavp == null)
      {
        if (((zzfx)paramObject).zzavp != null) {
          return false;
        }
      }
      else if (!this.zzavp.equals(((zzfx)paramObject).zzavp)) {
        return false;
      }
      if (this.zzavq == null)
      {
        if (((zzfx)paramObject).zzavq != null) {
          return false;
        }
      }
      else if (!this.zzavq.equals(((zzfx)paramObject).zzavq)) {
        return false;
      }
      if (this.zzavr == null)
      {
        if (((zzfx)paramObject).zzavr != null) {
          return false;
        }
      }
      else if (!this.zzavr.equals(((zzfx)paramObject).zzavr)) {
        return false;
      }
      if (this.zzavs == null)
      {
        if (((zzfx)paramObject).zzavs != null) {
          return false;
        }
      }
      else if (!this.zzavs.equals(((zzfx)paramObject).zzavs)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzfx)paramObject).zzcfc == null) || (((zzfx)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzfx)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i2 = 0;
    int i3 = getClass().getName().hashCode();
    int i;
    int j;
    label33:
    int k;
    label42:
    int m;
    label52:
    int n;
    if (this.zzavo == null)
    {
      i = 0;
      if (this.zzavp != null) {
        break label138;
      }
      j = 0;
      if (this.zzavq != null) {
        break label149;
      }
      k = 0;
      if (this.zzavr != null) {
        break label160;
      }
      m = 0;
      if (this.zzavs != null) {
        break label172;
      }
      n = 0;
      label62:
      i1 = i2;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label184;
        }
      }
    }
    label138:
    label149:
    label160:
    label172:
    label184:
    for (int i1 = i2;; i1 = this.zzcfc.hashCode())
    {
      return (n + (m + (k + (j + (i + (i3 + 527) * 31) * 31) * 31) * 31) * 31) * 31 + i1;
      i = this.zzavo.intValue();
      break;
      j = this.zzavp.hashCode();
      break label33;
      k = this.zzavq.hashCode();
      break label42;
      m = this.zzavr.hashCode();
      break label52;
      n = this.zzavs.hashCode();
      break label62;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzavo != null) {
      paramzzyy.zzd(1, this.zzavo.intValue());
    }
    if (this.zzavp != null) {
      paramzzyy.zzb(2, this.zzavp.booleanValue());
    }
    if (this.zzavq != null) {
      paramzzyy.zzb(3, this.zzavq);
    }
    if (this.zzavr != null) {
      paramzzyy.zzb(4, this.zzavr);
    }
    if (this.zzavs != null) {
      paramzzyy.zzb(5, this.zzavs);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.zzavo != null) {
      i = j + zzyy.zzh(1, this.zzavo.intValue());
    }
    j = i;
    if (this.zzavp != null)
    {
      this.zzavp.booleanValue();
      j = i + (zzyy.zzbb(2) + 1);
    }
    i = j;
    if (this.zzavq != null) {
      i = j + zzyy.zzc(3, this.zzavq);
    }
    j = i;
    if (this.zzavr != null) {
      j = i + zzyy.zzc(4, this.zzavr);
    }
    i = j;
    if (this.zzavs != null) {
      i = j + zzyy.zzc(5, this.zzavs);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzfx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */