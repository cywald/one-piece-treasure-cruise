package com.google.android.gms.internal.measurement;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

final class zzww
{
  static String zza(zzwt paramzzwt, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("# ").append(paramString);
    zza(paramzzwt, localStringBuilder, 0);
    return localStringBuilder.toString();
  }
  
  private static void zza(zzwt paramzzwt, StringBuilder paramStringBuilder, int paramInt)
  {
    HashMap localHashMap1 = new HashMap();
    HashMap localHashMap2 = new HashMap();
    Object localObject1 = new TreeSet();
    Object localObject2 = paramzzwt.getClass().getDeclaredMethods();
    int j = localObject2.length;
    int i = 0;
    while (i < j)
    {
      localObject3 = localObject2[i];
      localHashMap2.put(((Method)localObject3).getName(), localObject3);
      if (((Method)localObject3).getParameterTypes().length == 0)
      {
        localHashMap1.put(((Method)localObject3).getName(), localObject3);
        if (((Method)localObject3).getName().startsWith("get")) {
          ((Set)localObject1).add(((Method)localObject3).getName());
        }
      }
      i += 1;
    }
    Object localObject3 = ((Set)localObject1).iterator();
    label318:
    label478:
    label502:
    label562:
    label617:
    label641:
    label679:
    label737:
    label777:
    label779:
    label791:
    label793:
    label807:
    label821:
    label835:
    label841:
    label1051:
    label1057:
    label1076:
    while (((Iterator)localObject3).hasNext())
    {
      localObject2 = (String)((Iterator)localObject3).next();
      Object localObject4 = ((String)localObject2).replaceFirst("get", "");
      Object localObject5;
      if ((((String)localObject4).endsWith("List")) && (!((String)localObject4).endsWith("OrBuilderList")) && (!((String)localObject4).equals("List")))
      {
        localObject1 = String.valueOf(((String)localObject4).substring(0, 1).toLowerCase());
        localObject5 = String.valueOf(((String)localObject4).substring(1, ((String)localObject4).length() - 4));
        if (((String)localObject5).length() != 0) {}
        for (localObject1 = ((String)localObject1).concat((String)localObject5);; localObject1 = new String((String)localObject1))
        {
          localObject5 = (Method)localHashMap1.get(localObject2);
          if ((localObject5 == null) || (!((Method)localObject5).getReturnType().equals(List.class))) {
            break label318;
          }
          zzb(paramStringBuilder, paramInt, zzga((String)localObject1), zzvm.zza((Method)localObject5, paramzzwt, new Object[0]));
          break;
        }
      }
      if ((((String)localObject4).endsWith("Map")) && (!((String)localObject4).equals("Map")))
      {
        localObject1 = String.valueOf(((String)localObject4).substring(0, 1).toLowerCase());
        localObject5 = String.valueOf(((String)localObject4).substring(1, ((String)localObject4).length() - 3));
        if (((String)localObject5).length() != 0) {}
        for (localObject1 = ((String)localObject1).concat((String)localObject5);; localObject1 = new String((String)localObject1))
        {
          localObject2 = (Method)localHashMap1.get(localObject2);
          if ((localObject2 == null) || (!((Method)localObject2).getReturnType().equals(Map.class)) || (((Method)localObject2).isAnnotationPresent(Deprecated.class)) || (!Modifier.isPublic(((Method)localObject2).getModifiers()))) {
            break label478;
          }
          zzb(paramStringBuilder, paramInt, zzga((String)localObject1), zzvm.zza((Method)localObject2, paramzzwt, new Object[0]));
          break;
        }
      }
      localObject1 = String.valueOf(localObject4);
      boolean bool;
      if (((String)localObject1).length() != 0)
      {
        localObject1 = "set".concat((String)localObject1);
        if ((Method)localHashMap2.get(localObject1) == null) {
          break label777;
        }
        if (((String)localObject4).endsWith("Bytes"))
        {
          localObject1 = String.valueOf(((String)localObject4).substring(0, ((String)localObject4).length() - 5));
          if (((String)localObject1).length() == 0) {
            break label779;
          }
          localObject1 = "get".concat((String)localObject1);
          if (localHashMap1.containsKey(localObject1)) {
            break label791;
          }
        }
        localObject1 = String.valueOf(((String)localObject4).substring(0, 1).toLowerCase());
        localObject2 = String.valueOf(((String)localObject4).substring(1));
        if (((String)localObject2).length() == 0) {
          break label793;
        }
        localObject1 = ((String)localObject1).concat((String)localObject2);
        localObject2 = String.valueOf(localObject4);
        if (((String)localObject2).length() == 0) {
          break label807;
        }
        localObject2 = "get".concat((String)localObject2);
        localObject5 = (Method)localHashMap1.get(localObject2);
        localObject2 = String.valueOf(localObject4);
        if (((String)localObject2).length() == 0) {
          break label821;
        }
        localObject2 = "has".concat((String)localObject2);
        localObject2 = (Method)localHashMap1.get(localObject2);
        if (localObject5 == null) {
          continue;
        }
        localObject4 = zzvm.zza((Method)localObject5, paramzzwt, new Object[0]);
        if (localObject2 != null) {
          break label1057;
        }
        if (!(localObject4 instanceof Boolean)) {
          break label841;
        }
        if (((Boolean)localObject4).booleanValue()) {
          break label835;
        }
        bool = true;
        if (bool) {
          break label1051;
        }
        bool = true;
      }
      for (;;)
      {
        if (!bool) {
          break label1076;
        }
        zzb(paramStringBuilder, paramInt, zzga((String)localObject1), localObject4);
        break;
        localObject1 = new String("set");
        break label502;
        break;
        localObject1 = new String("get");
        break label562;
        break;
        localObject1 = new String((String)localObject1);
        break label617;
        localObject2 = new String("get");
        break label641;
        localObject2 = new String("has");
        break label679;
        bool = false;
        break label737;
        if ((localObject4 instanceof Integer))
        {
          if (((Integer)localObject4).intValue() == 0)
          {
            bool = true;
            break label737;
          }
          bool = false;
          break label737;
        }
        if ((localObject4 instanceof Float))
        {
          if (((Float)localObject4).floatValue() == 0.0F)
          {
            bool = true;
            break label737;
          }
          bool = false;
          break label737;
        }
        if ((localObject4 instanceof Double))
        {
          if (((Double)localObject4).doubleValue() == 0.0D)
          {
            bool = true;
            break label737;
          }
          bool = false;
          break label737;
        }
        if ((localObject4 instanceof String))
        {
          bool = localObject4.equals("");
          break label737;
        }
        if ((localObject4 instanceof zzud))
        {
          bool = localObject4.equals(zzud.zzbtz);
          break label737;
        }
        if ((localObject4 instanceof zzwt))
        {
          if (localObject4 == ((zzwt)localObject4).zzwf())
          {
            bool = true;
            break label737;
          }
          bool = false;
          break label737;
        }
        if ((localObject4 instanceof Enum))
        {
          if (((Enum)localObject4).ordinal() == 0)
          {
            bool = true;
            break label737;
          }
          bool = false;
          break label737;
        }
        bool = false;
        break label737;
        bool = false;
        continue;
        bool = ((Boolean)zzvm.zza((Method)localObject2, paramzzwt, new Object[0])).booleanValue();
      }
    }
    if ((paramzzwt instanceof zzvm.zzc))
    {
      localObject1 = ((zzvm.zzc)paramzzwt).zzbys.iterator();
      if (((Iterator)localObject1).hasNext())
      {
        ((Map.Entry)((Iterator)localObject1).next()).getKey();
        throw new NoSuchMethodError();
      }
    }
    if (((zzvm)paramzzwt).zzbym != null) {
      ((zzvm)paramzzwt).zzbym.zzb(paramStringBuilder, paramInt);
    }
  }
  
  static final void zzb(StringBuilder paramStringBuilder, int paramInt, String paramString, Object paramObject)
  {
    int k = 0;
    int j = 0;
    if ((paramObject instanceof List))
    {
      paramObject = ((List)paramObject).iterator();
      while (((Iterator)paramObject).hasNext()) {
        zzb(paramStringBuilder, paramInt, paramString, ((Iterator)paramObject).next());
      }
    }
    if ((paramObject instanceof Map))
    {
      paramObject = ((Map)paramObject).entrySet().iterator();
      while (((Iterator)paramObject).hasNext()) {
        zzb(paramStringBuilder, paramInt, paramString, (Map.Entry)((Iterator)paramObject).next());
      }
    }
    paramStringBuilder.append('\n');
    int i = 0;
    while (i < paramInt)
    {
      paramStringBuilder.append(' ');
      i += 1;
    }
    paramStringBuilder.append(paramString);
    if ((paramObject instanceof String))
    {
      paramStringBuilder.append(": \"").append(zzxx.zzd(zzud.zzfv((String)paramObject))).append('"');
      return;
    }
    if ((paramObject instanceof zzud))
    {
      paramStringBuilder.append(": \"").append(zzxx.zzd((zzud)paramObject)).append('"');
      return;
    }
    if ((paramObject instanceof zzvm))
    {
      paramStringBuilder.append(" {");
      zza((zzvm)paramObject, paramStringBuilder, paramInt + 2);
      paramStringBuilder.append("\n");
      i = j;
      while (i < paramInt)
      {
        paramStringBuilder.append(' ');
        i += 1;
      }
      paramStringBuilder.append("}");
      return;
    }
    if ((paramObject instanceof Map.Entry))
    {
      paramStringBuilder.append(" {");
      paramString = (Map.Entry)paramObject;
      zzb(paramStringBuilder, paramInt + 2, "key", paramString.getKey());
      zzb(paramStringBuilder, paramInt + 2, "value", paramString.getValue());
      paramStringBuilder.append("\n");
      i = k;
      while (i < paramInt)
      {
        paramStringBuilder.append(' ');
        i += 1;
      }
      paramStringBuilder.append("}");
      return;
    }
    paramStringBuilder.append(": ").append(paramObject.toString());
  }
  
  private static final String zzga(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    while (i < paramString.length())
    {
      char c = paramString.charAt(i);
      if (Character.isUpperCase(c)) {
        localStringBuilder.append("_");
      }
      localStringBuilder.append(Character.toLowerCase(c));
      i += 1;
    }
    return localStringBuilder.toString();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzww.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */