package com.google.android.gms.internal.measurement;

import java.lang.reflect.Constructor;

final class zzvc
{
  private static final zzva<?> zzbvo = new zzvb();
  private static final zzva<?> zzbvp = zzvq();
  
  private static zzva<?> zzvq()
  {
    try
    {
      zzva localzzva = (zzva)Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
      return localzzva;
    }
    catch (Exception localException) {}
    return null;
  }
  
  static zzva<?> zzvr()
  {
    return zzbvo;
  }
  
  static zzva<?> zzvs()
  {
    if (zzbvp == null) {
      throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
    return zzbvp;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */