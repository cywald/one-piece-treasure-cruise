package com.google.android.gms.internal.measurement;

import android.annotation.SuppressLint;
import android.util.Log;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.common.util.VisibleForTesting;

@Deprecated
@VisibleForTesting
public final class zzco
{
  private static volatile Logger zzabg = new zzby();
  
  @VisibleForTesting
  public static Logger getLogger()
  {
    return zzabg;
  }
  
  private static boolean isLoggable(int paramInt)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (zzabg != null)
    {
      bool1 = bool2;
      if (zzabg.getLogLevel() <= paramInt) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  @VisibleForTesting
  public static void setLogger(Logger paramLogger)
  {
    zzabg = paramLogger;
  }
  
  @SuppressLint({"LogTagMismatch"})
  public static void v(String paramString)
  {
    Object localObject = zzcp.zzex();
    if (localObject != null) {
      ((zzat)localObject).zzq(paramString);
    }
    for (;;)
    {
      localObject = zzabg;
      if (localObject != null) {
        ((Logger)localObject).verbose(paramString);
      }
      return;
      if (isLoggable(0)) {
        Log.v((String)zzcf.zzyx.get(), paramString);
      }
    }
  }
  
  @SuppressLint({"LogTagMismatch"})
  public static void zzab(String paramString)
  {
    Object localObject = zzcp.zzex();
    if (localObject != null) {
      ((zzat)localObject).zzt(paramString);
    }
    for (;;)
    {
      localObject = zzabg;
      if (localObject != null) {
        ((Logger)localObject).warn(paramString);
      }
      return;
      if (isLoggable(2)) {
        Log.w((String)zzcf.zzyx.get(), paramString);
      }
    }
  }
  
  @SuppressLint({"LogTagMismatch"})
  public static void zzf(String paramString, Object paramObject)
  {
    zzcp localzzcp = zzcp.zzex();
    if (localzzcp != null) {
      localzzcp.zze(paramString, paramObject);
    }
    while (!isLoggable(3))
    {
      paramObject = zzabg;
      if (paramObject != null) {
        ((Logger)paramObject).error(paramString);
      }
      return;
    }
    if (paramObject != null) {
      paramObject = String.valueOf(paramObject);
    }
    for (paramObject = String.valueOf(paramString).length() + 1 + String.valueOf(paramObject).length() + paramString + ":" + (String)paramObject;; paramObject = paramString)
    {
      Log.e((String)zzcf.zzyx.get(), (String)paramObject);
      break;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzco.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */