package com.google.android.gms.internal.measurement;

public abstract interface zzwu
  extends zzwv, Cloneable
{
  public abstract zzwu zza(zzwt paramzzwt);
  
  public abstract zzwt zzwi();
  
  public abstract zzwt zzwj();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */