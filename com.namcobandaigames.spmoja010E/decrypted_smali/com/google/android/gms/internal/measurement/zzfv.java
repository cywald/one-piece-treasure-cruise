package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzfv
  extends zzza<zzfv>
{
  private static volatile zzfv[] zzavd;
  public Boolean zzavb = null;
  public Boolean zzavc = null;
  public Integer zzave = null;
  public String zzavf = null;
  public zzfw[] zzavg = zzfw.zzmk();
  private Boolean zzavh = null;
  public zzfx zzavi = null;
  
  public zzfv()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzfv[] zzmj()
  {
    if (zzavd == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzavd == null) {
        zzavd = new zzfv[0];
      }
      return zzavd;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzfv)) {
        return false;
      }
      paramObject = (zzfv)paramObject;
      if (this.zzave == null)
      {
        if (((zzfv)paramObject).zzave != null) {
          return false;
        }
      }
      else if (!this.zzave.equals(((zzfv)paramObject).zzave)) {
        return false;
      }
      if (this.zzavf == null)
      {
        if (((zzfv)paramObject).zzavf != null) {
          return false;
        }
      }
      else if (!this.zzavf.equals(((zzfv)paramObject).zzavf)) {
        return false;
      }
      if (!zzze.equals(this.zzavg, ((zzfv)paramObject).zzavg)) {
        return false;
      }
      if (this.zzavh == null)
      {
        if (((zzfv)paramObject).zzavh != null) {
          return false;
        }
      }
      else if (!this.zzavh.equals(((zzfv)paramObject).zzavh)) {
        return false;
      }
      if (this.zzavi == null)
      {
        if (((zzfv)paramObject).zzavi != null) {
          return false;
        }
      }
      else if (!this.zzavi.equals(((zzfv)paramObject).zzavi)) {
        return false;
      }
      if (this.zzavb == null)
      {
        if (((zzfv)paramObject).zzavb != null) {
          return false;
        }
      }
      else if (!this.zzavb.equals(((zzfv)paramObject).zzavb)) {
        return false;
      }
      if (this.zzavc == null)
      {
        if (((zzfv)paramObject).zzavc != null) {
          return false;
        }
      }
      else if (!this.zzavc.equals(((zzfv)paramObject).zzavc)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzfv)paramObject).zzcfc == null) || (((zzfv)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzfv)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i3 = 0;
    int i4 = getClass().getName().hashCode();
    int i;
    int j;
    label33:
    int i5;
    int k;
    label51:
    zzfx localzzfx;
    int m;
    label65:
    int n;
    label75:
    int i1;
    if (this.zzave == null)
    {
      i = 0;
      if (this.zzavf != null) {
        break label173;
      }
      j = 0;
      i5 = zzze.hashCode(this.zzavg);
      if (this.zzavh != null) {
        break label184;
      }
      k = 0;
      localzzfx = this.zzavi;
      if (localzzfx != null) {
        break label195;
      }
      m = 0;
      if (this.zzavb != null) {
        break label205;
      }
      n = 0;
      if (this.zzavc != null) {
        break label217;
      }
      i1 = 0;
      label85:
      i2 = i3;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label229;
        }
      }
    }
    label173:
    label184:
    label195:
    label205:
    label217:
    label229:
    for (int i2 = i3;; i2 = this.zzcfc.hashCode())
    {
      return (i1 + (n + (m + (k + ((j + (i + (i4 + 527) * 31) * 31) * 31 + i5) * 31) * 31) * 31) * 31) * 31 + i2;
      i = this.zzave.hashCode();
      break;
      j = this.zzavf.hashCode();
      break label33;
      k = this.zzavh.hashCode();
      break label51;
      m = localzzfx.hashCode();
      break label65;
      n = this.zzavb.hashCode();
      break label75;
      i1 = this.zzavc.hashCode();
      break label85;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzave != null) {
      paramzzyy.zzd(1, this.zzave.intValue());
    }
    if (this.zzavf != null) {
      paramzzyy.zzb(2, this.zzavf);
    }
    if ((this.zzavg != null) && (this.zzavg.length > 0))
    {
      int i = 0;
      while (i < this.zzavg.length)
      {
        zzfw localzzfw = this.zzavg[i];
        if (localzzfw != null) {
          paramzzyy.zza(3, localzzfw);
        }
        i += 1;
      }
    }
    if (this.zzavh != null) {
      paramzzyy.zzb(4, this.zzavh.booleanValue());
    }
    if (this.zzavi != null) {
      paramzzyy.zza(5, this.zzavi);
    }
    if (this.zzavb != null) {
      paramzzyy.zzb(6, this.zzavb.booleanValue());
    }
    if (this.zzavc != null) {
      paramzzyy.zzb(7, this.zzavc.booleanValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int i = super.zzf();
    int j = i;
    if (this.zzave != null) {
      j = i + zzyy.zzh(1, this.zzave.intValue());
    }
    i = j;
    if (this.zzavf != null) {
      i = j + zzyy.zzc(2, this.zzavf);
    }
    j = i;
    if (this.zzavg != null)
    {
      j = i;
      if (this.zzavg.length > 0)
      {
        j = 0;
        while (j < this.zzavg.length)
        {
          zzfw localzzfw = this.zzavg[j];
          int k = i;
          if (localzzfw != null) {
            k = i + zzyy.zzb(3, localzzfw);
          }
          j += 1;
          i = k;
        }
        j = i;
      }
    }
    i = j;
    if (this.zzavh != null)
    {
      this.zzavh.booleanValue();
      i = j + (zzyy.zzbb(4) + 1);
    }
    j = i;
    if (this.zzavi != null) {
      j = i + zzyy.zzb(5, this.zzavi);
    }
    i = j;
    if (this.zzavb != null)
    {
      this.zzavb.booleanValue();
      i = j + (zzyy.zzbb(6) + 1);
    }
    j = i;
    if (this.zzavc != null)
    {
      this.zzavc.booleanValue();
      j = i + (zzyy.zzbb(7) + 1);
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzfv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */