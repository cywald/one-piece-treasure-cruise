package com.google.android.gms.internal.measurement;

final class zzuh
  extends zzum
{
  private final int zzbud;
  private final int zzbue;
  
  zzuh(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    super(paramArrayOfByte);
    zzb(paramInt1, paramInt1 + paramInt2, paramArrayOfByte.length);
    this.zzbud = paramInt1;
    this.zzbue = paramInt2;
  }
  
  public final int size()
  {
    return this.zzbue;
  }
  
  public final byte zzal(int paramInt)
  {
    int i = size();
    if ((i - (paramInt + 1) | paramInt) < 0)
    {
      if (paramInt < 0) {
        throw new ArrayIndexOutOfBoundsException(22 + "Index < 0: " + paramInt);
      }
      throw new ArrayIndexOutOfBoundsException(40 + "Index > length: " + paramInt + ", " + i);
    }
    return this.zzbug[(this.zzbud + paramInt)];
  }
  
  protected final int zzud()
  {
    return this.zzbud;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */