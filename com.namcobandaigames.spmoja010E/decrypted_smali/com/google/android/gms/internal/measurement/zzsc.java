package com.google.android.gms.internal.measurement;

import com.google.android.gms.tagmanager.zzdi;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

final class zzsc
  implements zzsd
{
  private HttpURLConnection zzbof;
  private InputStream zzbog = null;
  
  public final void close()
  {
    HttpURLConnection localHttpURLConnection = this.zzbof;
    try
    {
      if (this.zzbog != null) {
        this.zzbog.close();
      }
      if (localHttpURLConnection != null) {
        localHttpURLConnection.disconnect();
      }
      return;
    }
    catch (IOException localIOException)
    {
      str = String.valueOf(localIOException.getMessage());
      if (str.length() == 0) {}
    }
    for (String str = "HttpUrlConnectionNetworkClient: Error when closing http input stream: ".concat(str);; str = new String("HttpUrlConnectionNetworkClient: Error when closing http input stream: "))
    {
      zzdi.zza(str, localIOException);
      break;
    }
  }
  
  public final InputStream zzev(String paramString)
    throws IOException
  {
    paramString = (HttpURLConnection)new URL(paramString).openConnection();
    paramString.setReadTimeout(20000);
    paramString.setConnectTimeout(20000);
    this.zzbof = paramString;
    paramString = this.zzbof;
    int i = paramString.getResponseCode();
    if (i == 200)
    {
      this.zzbog = paramString.getInputStream();
      return this.zzbog;
    }
    paramString = 25 + "Bad response: " + i;
    if (i == 404) {
      throw new FileNotFoundException(paramString);
    }
    if (i == 503) {
      throw new zzsf(paramString);
    }
    throw new IOException(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */