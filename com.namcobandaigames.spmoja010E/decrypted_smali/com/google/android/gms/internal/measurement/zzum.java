package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.nio.charset.Charset;

class zzum
  extends zzul
{
  protected final byte[] zzbug;
  
  zzum(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null) {
      throw new NullPointerException();
    }
    this.zzbug = paramArrayOfByte;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    if (!(paramObject instanceof zzud)) {
      return false;
    }
    if (size() != ((zzud)paramObject).size()) {
      return false;
    }
    if (size() == 0) {
      return true;
    }
    if ((paramObject instanceof zzum))
    {
      zzum localzzum = (zzum)paramObject;
      int i = zzuc();
      int j = localzzum.zzuc();
      if ((i != 0) && (j != 0) && (i != j)) {
        return false;
      }
      return zza((zzum)paramObject, 0, size());
    }
    return paramObject.equals(this);
  }
  
  public int size()
  {
    return this.zzbug.length;
  }
  
  protected final int zza(int paramInt1, int paramInt2, int paramInt3)
  {
    return zzvo.zza(paramInt1, this.zzbug, zzud(), paramInt3);
  }
  
  protected final String zza(Charset paramCharset)
  {
    return new String(this.zzbug, zzud(), size(), paramCharset);
  }
  
  final void zza(zzuc paramzzuc)
    throws IOException
  {
    paramzzuc.zza(this.zzbug, zzud(), size());
  }
  
  final boolean zza(zzud paramzzud, int paramInt1, int paramInt2)
  {
    if (paramInt2 > paramzzud.size())
    {
      paramInt1 = size();
      throw new IllegalArgumentException(40 + "Length too large: " + paramInt2 + paramInt1);
    }
    if (paramInt2 > paramzzud.size())
    {
      paramInt1 = paramzzud.size();
      throw new IllegalArgumentException(59 + "Ran off end of other: 0, " + paramInt2 + ", " + paramInt1);
    }
    if ((paramzzud instanceof zzum))
    {
      paramzzud = (zzum)paramzzud;
      byte[] arrayOfByte1 = this.zzbug;
      byte[] arrayOfByte2 = paramzzud.zzbug;
      int j = zzud();
      int i = zzud();
      paramInt1 = paramzzud.zzud();
      while (i < j + paramInt2)
      {
        if (arrayOfByte1[i] != arrayOfByte2[paramInt1]) {
          return false;
        }
        i += 1;
        paramInt1 += 1;
      }
      return true;
    }
    return paramzzud.zzb(0, paramInt2).equals(zzb(0, paramInt2));
  }
  
  public byte zzal(int paramInt)
  {
    return this.zzbug[paramInt];
  }
  
  public final zzud zzb(int paramInt1, int paramInt2)
  {
    paramInt1 = zzb(0, paramInt2, size());
    if (paramInt1 == 0) {
      return zzud.zzbtz;
    }
    return new zzuh(this.zzbug, zzud(), paramInt1);
  }
  
  public final boolean zzub()
  {
    int i = zzud();
    return zzyj.zzf(this.zzbug, i, size() + i);
  }
  
  protected int zzud()
  {
    return 0;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzum.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */