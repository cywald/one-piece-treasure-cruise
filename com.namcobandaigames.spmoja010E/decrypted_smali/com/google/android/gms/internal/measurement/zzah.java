package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzi;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@VisibleForTesting
public final class zzah
  extends zzi<zzah>
{
  private String zzuw;
  private int zzux;
  private int zzuy;
  private String zzuz;
  private String zzva;
  private boolean zzvb;
  private boolean zzvc;
  
  public zzah()
  {
    this(false);
  }
  
  private zzah(boolean paramBoolean) {}
  
  @VisibleForTesting
  private zzah(boolean paramBoolean, int paramInt)
  {
    Preconditions.checkNotZero(paramInt);
    this.zzux = paramInt;
    this.zzvc = false;
  }
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("screenName", this.zzuw);
    localHashMap.put("interstitial", Boolean.valueOf(this.zzvb));
    localHashMap.put("automatic", Boolean.valueOf(this.zzvc));
    localHashMap.put("screenId", Integer.valueOf(this.zzux));
    localHashMap.put("referrerScreenId", Integer.valueOf(this.zzuy));
    localHashMap.put("referrerScreenName", this.zzuz);
    localHashMap.put("referrerUri", this.zzva);
    return zza(localHashMap);
  }
  
  public final String zzbk()
  {
    return this.zzuw;
  }
  
  public final int zzbl()
  {
    return this.zzux;
  }
  
  public final String zzbm()
  {
    return this.zzva;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */