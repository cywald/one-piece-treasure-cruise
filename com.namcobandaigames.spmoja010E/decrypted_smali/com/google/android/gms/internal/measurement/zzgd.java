package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgd
  extends zzza<zzgd>
{
  private static volatile zzgd[] zzawl;
  public Integer zzauy = null;
  public zzgj zzawm = null;
  public zzgj zzawn = null;
  public Boolean zzawo = null;
  
  public zzgd()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzgd[] zzmo()
  {
    if (zzawl == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzawl == null) {
        zzawl = new zzgd[0];
      }
      return zzawl;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgd)) {
        return false;
      }
      paramObject = (zzgd)paramObject;
      if (this.zzauy == null)
      {
        if (((zzgd)paramObject).zzauy != null) {
          return false;
        }
      }
      else if (!this.zzauy.equals(((zzgd)paramObject).zzauy)) {
        return false;
      }
      if (this.zzawm == null)
      {
        if (((zzgd)paramObject).zzawm != null) {
          return false;
        }
      }
      else if (!this.zzawm.equals(((zzgd)paramObject).zzawm)) {
        return false;
      }
      if (this.zzawn == null)
      {
        if (((zzgd)paramObject).zzawn != null) {
          return false;
        }
      }
      else if (!this.zzawn.equals(((zzgd)paramObject).zzawn)) {
        return false;
      }
      if (this.zzawo == null)
      {
        if (((zzgd)paramObject).zzawo != null) {
          return false;
        }
      }
      else if (!this.zzawo.equals(((zzgd)paramObject).zzawo)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgd)paramObject).zzcfc == null) || (((zzgd)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgd)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i1 = 0;
    int i2 = getClass().getName().hashCode();
    int i;
    zzgj localzzgj;
    int j;
    label37:
    int k;
    label50:
    int m;
    if (this.zzauy == null)
    {
      i = 0;
      localzzgj = this.zzawm;
      if (localzzgj != null) {
        break label130;
      }
      j = 0;
      localzzgj = this.zzawn;
      if (localzzgj != null) {
        break label139;
      }
      k = 0;
      if (this.zzawo != null) {
        break label148;
      }
      m = 0;
      label60:
      n = i1;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label160;
        }
      }
    }
    label130:
    label139:
    label148:
    label160:
    for (int n = i1;; n = this.zzcfc.hashCode())
    {
      return (m + (k + (j + (i + (i2 + 527) * 31) * 31) * 31) * 31) * 31 + n;
      i = this.zzauy.hashCode();
      break;
      j = localzzgj.hashCode();
      break label37;
      k = localzzgj.hashCode();
      break label50;
      m = this.zzawo.hashCode();
      break label60;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzauy != null) {
      paramzzyy.zzd(1, this.zzauy.intValue());
    }
    if (this.zzawm != null) {
      paramzzyy.zza(2, this.zzawm);
    }
    if (this.zzawn != null) {
      paramzzyy.zza(3, this.zzawn);
    }
    if (this.zzawo != null) {
      paramzzyy.zzb(4, this.zzawo.booleanValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.zzauy != null) {
      i = j + zzyy.zzh(1, this.zzauy.intValue());
    }
    j = i;
    if (this.zzawm != null) {
      j = i + zzyy.zzb(2, this.zzawm);
    }
    i = j;
    if (this.zzawn != null) {
      i = j + zzyy.zzb(3, this.zzawn);
    }
    j = i;
    if (this.zzawo != null)
    {
      this.zzawo.booleanValue();
      j = i + (zzyy.zzbb(4) + 1);
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */