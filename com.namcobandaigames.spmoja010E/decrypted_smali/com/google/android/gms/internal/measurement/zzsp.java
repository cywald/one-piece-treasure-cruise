package com.google.android.gms.internal.measurement;

import android.util.Log;

final class zzsp
  extends zzsl<Long>
{
  zzsp(zzsv paramzzsv, String paramString, Long paramLong)
  {
    super(paramzzsv, paramString, paramLong, null);
  }
  
  private final Long zzfk(String paramString)
  {
    try
    {
      long l = Long.parseLong(paramString);
      return Long.valueOf(l);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      String str = this.zzbrc;
      Log.e("PhenotypeFlag", String.valueOf(str).length() + 25 + String.valueOf(paramString).length() + "Invalid long value for " + str + ": " + paramString);
    }
    return null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */