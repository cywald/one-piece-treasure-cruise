package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public final class zzzb<M extends zzza<M>, T>
{
  public final int tag;
  private final int type;
  private final zzvm<?, ?> zzbyp;
  protected final Class<T> zzcfd;
  protected final boolean zzcfe;
  
  private zzzb(int paramInt1, Class<T> paramClass, int paramInt2, boolean paramBoolean)
  {
    this(11, paramClass, null, 810, false);
  }
  
  private zzzb(int paramInt1, Class<T> paramClass, zzvm<?, ?> paramzzvm, int paramInt2, boolean paramBoolean)
  {
    this.type = paramInt1;
    this.zzcfd = paramClass;
    this.tag = paramInt2;
    this.zzcfe = false;
    this.zzbyp = null;
  }
  
  public static <M extends zzza<M>, T extends zzzg> zzzb<M, T> zza(int paramInt, Class<T> paramClass, long paramLong)
  {
    return new zzzb(11, paramClass, 810, false);
  }
  
  private final Object zze(zzyx paramzzyx)
  {
    Object localObject;
    if (this.zzcfe) {
      localObject = this.zzcfd.getComponentType();
    }
    for (;;)
    {
      try
      {
        switch (this.type)
        {
        case 10: 
          int i = this.type;
          throw new IllegalArgumentException(24 + "Unknown type " + i);
        }
      }
      catch (InstantiationException paramzzyx)
      {
        localObject = String.valueOf(localObject);
        throw new IllegalArgumentException(String.valueOf(localObject).length() + 33 + "Error creating instance of class " + (String)localObject, paramzzyx);
        localObject = this.zzcfd;
        continue;
        zzzg localzzzg = (zzzg)((Class)localObject).newInstance();
        paramzzyx.zza(localzzzg, this.tag >>> 3);
        return localzzzg;
        localzzzg = (zzzg)((Class)localObject).newInstance();
        paramzzyx.zza(localzzzg);
        return localzzzg;
      }
      catch (IllegalAccessException paramzzyx)
      {
        localObject = String.valueOf(localObject);
        throw new IllegalArgumentException(String.valueOf(localObject).length() + 33 + "Error creating instance of class " + (String)localObject, paramzzyx);
      }
      catch (IOException paramzzyx)
      {
        throw new IllegalArgumentException("Error reading extension field", paramzzyx);
      }
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzzb)) {
        return false;
      }
      paramObject = (zzzb)paramObject;
    } while ((this.type == ((zzzb)paramObject).type) && (this.zzcfd == ((zzzb)paramObject).zzcfd) && (this.tag == ((zzzb)paramObject).tag) && (this.zzcfe == ((zzzb)paramObject).zzcfe));
    return false;
  }
  
  public final int hashCode()
  {
    int j = this.type;
    int k = this.zzcfd.hashCode();
    int m = this.tag;
    if (this.zzcfe) {}
    for (int i = 1;; i = 0) {
      return i + (((j + 1147) * 31 + k) * 31 + m) * 31;
    }
  }
  
  protected final void zza(Object paramObject, zzyy paramzzyy)
  {
    for (;;)
    {
      try
      {
        paramzzyy.zzca(this.tag);
        switch (this.type)
        {
        case 10: 
          i = this.type;
          throw new IllegalArgumentException(24 + "Unknown type " + i);
        }
      }
      catch (IOException paramObject)
      {
        throw new IllegalStateException((Throwable)paramObject);
      }
      int i = this.tag;
      ((zzzg)paramObject).zza(paramzzyy);
      paramzzyy.zzc(i >>> 3, 4);
      return;
      paramzzyy.zzb((zzzg)paramObject);
      return;
    }
  }
  
  final T zzah(List<zzzi> paramList)
  {
    int j = 0;
    if (paramList == null)
    {
      paramList = null;
      return paramList;
    }
    if (this.zzcfe)
    {
      ArrayList localArrayList = new ArrayList();
      int i = 0;
      while (i < paramList.size())
      {
        localObject = (zzzi)paramList.get(i);
        if (((zzzi)localObject).zzbug.length != 0) {
          localArrayList.add(zze(zzyx.zzn(((zzzi)localObject).zzbug)));
        }
        i += 1;
      }
      int k = localArrayList.size();
      if (k == 0) {
        return null;
      }
      Object localObject = this.zzcfd.cast(Array.newInstance(this.zzcfd.getComponentType(), k));
      i = j;
      for (;;)
      {
        paramList = (List<zzzi>)localObject;
        if (i >= k) {
          break;
        }
        Array.set(localObject, i, localArrayList.get(i));
        i += 1;
      }
    }
    if (paramList.isEmpty()) {
      return null;
    }
    paramList = (zzzi)paramList.get(paramList.size() - 1);
    return (T)this.zzcfd.cast(zze(zzyx.zzn(paramList.zzbug)));
  }
  
  protected final int zzak(Object paramObject)
  {
    int i = this.tag >>> 3;
    switch (this.type)
    {
    default: 
      i = this.type;
      throw new IllegalArgumentException(24 + "Unknown type " + i);
    case 10: 
      paramObject = (zzzg)paramObject;
      return (zzyy.zzbb(i) << 1) + ((zzzg)paramObject).zzvu();
    }
    return zzyy.zzb(i, (zzzg)paramObject);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */