package com.google.android.gms.internal.measurement;

final class zzax
  implements Thread.UncaughtExceptionHandler
{
  zzax(zzaw paramzzaw) {}
  
  public final void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    paramThread = this.zzwp.zzcn();
    if (paramThread != null) {
      paramThread.zze("Job execution failed", paramThrowable);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzax.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */