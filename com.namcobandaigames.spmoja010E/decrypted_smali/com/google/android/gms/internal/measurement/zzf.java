package com.google.android.gms.internal.measurement;

final class zzf
  implements zzvr
{
  static final zzvr zzoc = new zzf();
  
  public final boolean zzb(int paramInt)
  {
    return zzc.zza.zzb.zza(paramInt) != null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */