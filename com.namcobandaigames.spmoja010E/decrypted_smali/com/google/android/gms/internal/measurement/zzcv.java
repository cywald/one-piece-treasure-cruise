package com.google.android.gms.internal.measurement;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Pair;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.UUID;

public final class zzcv
{
  private final String name;
  private final long zzabv;
  
  private zzcv(zzct paramzzct, String paramString, long paramLong)
  {
    Preconditions.checkNotEmpty(paramString);
    if (paramLong > 0L) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool);
      this.name = paramString;
      this.zzabv = paramLong;
      return;
    }
  }
  
  private final void zzfl()
  {
    long l = this.zzabw.zzbx().currentTimeMillis();
    SharedPreferences.Editor localEditor = zzct.zza(this.zzabw).edit();
    localEditor.remove(zzfp());
    localEditor.remove(zzfq());
    localEditor.putLong(zzfo(), l);
    localEditor.commit();
  }
  
  private final long zzfn()
  {
    return zzct.zza(this.zzabw).getLong(zzfo(), 0L);
  }
  
  private final String zzfo()
  {
    return String.valueOf(this.name).concat(":start");
  }
  
  private final String zzfp()
  {
    return String.valueOf(this.name).concat(":count");
  }
  
  @VisibleForTesting
  private final String zzfq()
  {
    return String.valueOf(this.name).concat(":value");
  }
  
  public final void zzad(String paramString)
  {
    if (zzfn() == 0L) {
      zzfl();
    }
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    for (;;)
    {
      try
      {
        long l = zzct.zza(this.zzabw).getLong(zzfp(), 0L);
        if (l <= 0L)
        {
          paramString = zzct.zza(this.zzabw).edit();
          paramString.putString(zzfq(), str);
          paramString.putLong(zzfp(), 1L);
          paramString.apply();
          return;
        }
        if ((UUID.randomUUID().getLeastSignificantBits() & 0x7FFFFFFFFFFFFFFF) < Long.MAX_VALUE / (l + 1L))
        {
          i = 1;
          paramString = zzct.zza(this.zzabw).edit();
          if (i != 0) {
            paramString.putString(zzfq(), str);
          }
          paramString.putLong(zzfp(), l + 1L);
          paramString.apply();
          return;
        }
      }
      finally {}
      int i = 0;
    }
  }
  
  public final Pair<String, Long> zzfm()
  {
    long l = zzfn();
    if (l == 0L) {}
    for (l = 0L; l < this.zzabv; l = Math.abs(l - this.zzabw.zzbx().currentTimeMillis())) {
      return null;
    }
    if (l > this.zzabv << 1)
    {
      zzfl();
      return null;
    }
    String str = zzct.zza(this.zzabw).getString(zzfq(), null);
    l = zzct.zza(this.zzabw).getLong(zzfp(), 0L);
    zzfl();
    if ((str == null) || (l <= 0L)) {
      return null;
    }
    return new Pair(str, Long.valueOf(l));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */