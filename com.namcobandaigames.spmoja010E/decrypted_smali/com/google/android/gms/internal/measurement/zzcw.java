package com.google.android.gms.internal.measurement;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.stats.WakeLock;

public final class zzcw
{
  static Object lock = new Object();
  static WakeLock zzabx;
  private static Boolean zzre;
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public static void onReceive(Context paramContext, Intent arg1)
  {
    localzzcp = zzaw.zzc(paramContext).zzby();
    if (??? == null) {
      localzzcp.zzt("AnalyticsReceiver called with null intent");
    }
    do
    {
      return;
      ??? = ???.getAction();
      localzzcp.zza("Local AnalyticsReceiver got", ???);
    } while (!"com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(???));
    boolean bool = zzcx.zze(paramContext);
    Intent localIntent = new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
    localIntent.setComponent(new ComponentName(paramContext, "com.google.android.gms.analytics.AnalyticsService"));
    localIntent.setAction("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
    synchronized (lock)
    {
      paramContext.startService(localIntent);
      if (!bool) {
        return;
      }
    }
    try
    {
      if (zzabx == null)
      {
        paramContext = new WakeLock(paramContext, 1, "Analytics WakeLock");
        zzabx = paramContext;
        paramContext.setReferenceCounted(false);
      }
      zzabx.acquire(1000L);
    }
    catch (SecurityException paramContext)
    {
      for (;;)
      {
        localzzcp.zzt("Analytics service at risk of not starting. For more reliable analytics, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions.");
      }
    }
  }
  
  public static boolean zza(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    if (zzre != null) {
      return zzre.booleanValue();
    }
    boolean bool = zzdg.zza(paramContext, "com.google.android.gms.analytics.AnalyticsReceiver", false);
    zzre = Boolean.valueOf(bool);
    return bool;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */