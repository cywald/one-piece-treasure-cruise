package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.internal.Preconditions;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class zzaz
{
  private final Map<String, String> zzsy;
  private final String zzup;
  private final long zzws;
  private final String zzwt;
  private final boolean zzwu;
  private long zzwv;
  
  public zzaz(long paramLong1, String paramString1, String paramString2, boolean paramBoolean, long paramLong2, Map<String, String> paramMap)
  {
    Preconditions.checkNotEmpty(paramString1);
    Preconditions.checkNotEmpty(paramString2);
    this.zzws = 0L;
    this.zzup = paramString1;
    this.zzwt = paramString2;
    this.zzwu = paramBoolean;
    this.zzwv = paramLong2;
    if (paramMap != null)
    {
      this.zzsy = new HashMap(paramMap);
      return;
    }
    this.zzsy = Collections.emptyMap();
  }
  
  public final void zzb(long paramLong)
  {
    this.zzwv = paramLong;
  }
  
  public final String zzbd()
  {
    return this.zzup;
  }
  
  public final long zzcs()
  {
    return this.zzws;
  }
  
  public final String zzct()
  {
    return this.zzwt;
  }
  
  public final boolean zzcu()
  {
    return this.zzwu;
  }
  
  public final long zzcv()
  {
    return this.zzwv;
  }
  
  public final Map<String, String> zzcw()
  {
    return this.zzsy;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzaz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */