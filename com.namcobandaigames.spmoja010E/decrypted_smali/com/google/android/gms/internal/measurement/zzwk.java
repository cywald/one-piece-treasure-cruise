package com.google.android.gms.internal.measurement;

final class zzwk
  implements zzws
{
  private zzws[] zzcaq;
  
  zzwk(zzws... paramVarArgs)
  {
    this.zzcaq = paramVarArgs;
  }
  
  public final boolean zze(Class<?> paramClass)
  {
    boolean bool2 = false;
    zzws[] arrayOfzzws = this.zzcaq;
    int j = arrayOfzzws.length;
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      if (i < j)
      {
        if (arrayOfzzws[i].zze(paramClass)) {
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
      i += 1;
    }
  }
  
  public final zzwr zzf(Class<?> paramClass)
  {
    zzws[] arrayOfzzws = this.zzcaq;
    int j = arrayOfzzws.length;
    int i = 0;
    while (i < j)
    {
      zzws localzzws = arrayOfzzws[i];
      if (localzzws.zze(paramClass)) {
        return localzzws.zzf(paramClass);
      }
      i += 1;
    }
    paramClass = String.valueOf(paramClass.getName());
    if (paramClass.length() != 0) {}
    for (paramClass = "No factory is available for message type: ".concat(paramClass);; paramClass = new String("No factory is available for message type: ")) {
      throw new UnsupportedOperationException(paramClass);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */