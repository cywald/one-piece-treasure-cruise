package com.google.android.gms.internal.measurement;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Build.VERSION;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import javax.annotation.Nullable;

public abstract class zzsl<T>
{
  private static final Object zzbqy = new Object();
  private static boolean zzbqz = false;
  private static volatile Boolean zzbra = null;
  @SuppressLint({"StaticFieldLeak"})
  private static Context zzri = null;
  private final zzsv zzbrb;
  final String zzbrc;
  private final String zzbrd;
  private final T zzbre;
  private T zzbrf = null;
  private volatile zzsi zzbrg = null;
  private volatile SharedPreferences zzbrh = null;
  
  private zzsl(zzsv paramzzsv, String paramString, T paramT)
  {
    if (zzsv.zza(paramzzsv) == null) {
      throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }
    this.zzbrb = paramzzsv;
    String str1 = String.valueOf(zzsv.zzb(paramzzsv));
    String str2 = String.valueOf(paramString);
    if (str2.length() != 0)
    {
      str1 = str1.concat(str2);
      this.zzbrd = str1;
      paramzzsv = String.valueOf(zzsv.zzc(paramzzsv));
      paramString = String.valueOf(paramString);
      if (paramString.length() == 0) {
        break label130;
      }
    }
    label130:
    for (paramzzsv = paramzzsv.concat(paramString);; paramzzsv = new String(paramzzsv))
    {
      this.zzbrc = paramzzsv;
      this.zzbre = paramT;
      return;
      str1 = new String(str1);
      break;
    }
  }
  
  public static void init(Context paramContext)
  {
    synchronized (zzbqy)
    {
      if ((Build.VERSION.SDK_INT >= 24) && (paramContext.isDeviceProtectedStorage())) {}
      Context localContext;
      do
      {
        if (zzri != paramContext) {
          zzbra = null;
        }
        zzri = paramContext;
        zzbqz = false;
        return;
        localContext = paramContext.getApplicationContext();
      } while (localContext == null);
      paramContext = localContext;
    }
  }
  
  private static zzsl<Double> zza(zzsv paramzzsv, String paramString, double paramDouble)
  {
    return new zzss(paramzzsv, paramString, Double.valueOf(paramDouble));
  }
  
  private static zzsl<Integer> zza(zzsv paramzzsv, String paramString, int paramInt)
  {
    return new zzsq(paramzzsv, paramString, Integer.valueOf(paramInt));
  }
  
  private static zzsl<Long> zza(zzsv paramzzsv, String paramString, long paramLong)
  {
    return new zzsp(paramzzsv, paramString, Long.valueOf(paramLong));
  }
  
  private static zzsl<String> zza(zzsv paramzzsv, String paramString1, String paramString2)
  {
    return new zzst(paramzzsv, paramString1, paramString2);
  }
  
  private static zzsl<Boolean> zza(zzsv paramzzsv, String paramString, boolean paramBoolean)
  {
    return new zzsr(paramzzsv, paramString, Boolean.valueOf(paramBoolean));
  }
  
  private static <V> V zza(zzsu<V> paramzzsu)
  {
    try
    {
      Object localObject = paramzzsu.zztj();
      return (V)localObject;
    }
    catch (SecurityException localSecurityException)
    {
      long l = Binder.clearCallingIdentity();
      try
      {
        paramzzsu = paramzzsu.zztj();
        return paramzzsu;
      }
      finally
      {
        Binder.restoreCallingIdentity(l);
      }
    }
  }
  
  static boolean zzd(String paramString, boolean paramBoolean)
  {
    try
    {
      if (zzth())
      {
        paramBoolean = ((Boolean)zza(new zzso(paramString, false))).booleanValue();
        return paramBoolean;
      }
      return false;
    }
    catch (SecurityException paramString)
    {
      Log.e("PhenotypeFlag", "Unable to read GServices, returning default value.", paramString);
    }
    return false;
  }
  
  @Nullable
  @TargetApi(24)
  private final T zzte()
  {
    int i = 0;
    Object localObject = this.zzbrb;
    if (zzd("gms:phenotype:phenotype_flag:debug_bypass_phenotype", false)) {
      i = 1;
    }
    if (i == 0)
    {
      if (zzsv.zza(this.zzbrb) != null)
      {
        localObject = zztg();
        if (localObject != null)
        {
          localObject = (String)zza(new zzsm(this, (zzsi)localObject));
          if (localObject != null) {
            return (T)zzfj((String)localObject);
          }
        }
      }
      else
      {
        localObject = this.zzbrb;
      }
      return null;
    }
    localObject = String.valueOf(this.zzbrc);
    if (((String)localObject).length() != 0) {}
    for (localObject = "Bypass reading Phenotype values for flag: ".concat((String)localObject);; localObject = new String("Bypass reading Phenotype values for flag: "))
    {
      Log.w("PhenotypeFlag", (String)localObject);
      break;
    }
  }
  
  @Nullable
  private final T zztf()
  {
    Object localObject = this.zzbrb;
    if (zzth()) {
      try
      {
        localObject = (String)zza(new zzsn(this));
        if (localObject != null)
        {
          localObject = zzfj((String)localObject);
          return (T)localObject;
        }
      }
      catch (SecurityException localSecurityException)
      {
        localObject = String.valueOf(this.zzbrc);
        if (((String)localObject).length() == 0) {}
      }
    }
    for (localObject = "Unable to read GServices for flag: ".concat((String)localObject);; localObject = new String("Unable to read GServices for flag: "))
    {
      Log.e("PhenotypeFlag", (String)localObject, localSecurityException);
      return null;
    }
  }
  
  private final zzsi zztg()
  {
    if (this.zzbrg == null) {}
    try
    {
      this.zzbrg = zzsi.zza(zzri.getContentResolver(), zzsv.zza(this.zzbrb));
      return this.zzbrg;
    }
    catch (SecurityException localSecurityException)
    {
      for (;;) {}
    }
  }
  
  private static boolean zzth()
  {
    boolean bool1 = false;
    boolean bool2 = false;
    if (zzbra == null)
    {
      if (zzri != null)
      {
        bool1 = bool2;
        if (PermissionChecker.checkSelfPermission(zzri, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0) {
          bool1 = true;
        }
        zzbra = Boolean.valueOf(bool1);
      }
    }
    else {
      bool1 = zzbra.booleanValue();
    }
    return bool1;
  }
  
  public final T get()
  {
    if (zzri == null) {
      throw new IllegalStateException("Must call PhenotypeFlag.init() first");
    }
    Object localObject1 = this.zzbrb;
    localObject1 = zzte();
    if (localObject1 != null) {}
    Object localObject2;
    do
    {
      return (T)localObject1;
      localObject2 = zztf();
      localObject1 = localObject2;
    } while (localObject2 != null);
    return (T)this.zzbre;
  }
  
  public final T getDefaultValue()
  {
    return (T)this.zzbre;
  }
  
  protected abstract T zzfj(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */