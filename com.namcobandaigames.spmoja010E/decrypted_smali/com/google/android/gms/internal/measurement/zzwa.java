package com.google.android.gms.internal.measurement;

public class zzwa
{
  private static final zzuz zzbtt = ;
  private zzud zzcad;
  private volatile zzwt zzcae;
  private volatile zzud zzcaf;
  
  /* Error */
  private final zzwt zzh(zzwt paramzzwt)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 31	com/google/android/gms/internal/measurement/zzwa:zzcae	Lcom/google/android/gms/internal/measurement/zzwt;
    //   4: ifnonnull +14 -> 18
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield 31	com/google/android/gms/internal/measurement/zzwa:zzcae	Lcom/google/android/gms/internal/measurement/zzwt;
    //   13: ifnull +10 -> 23
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_0
    //   19: getfield 31	com/google/android/gms/internal/measurement/zzwa:zzcae	Lcom/google/android/gms/internal/measurement/zzwt;
    //   22: areturn
    //   23: aload_0
    //   24: aload_1
    //   25: putfield 31	com/google/android/gms/internal/measurement/zzwa:zzcae	Lcom/google/android/gms/internal/measurement/zzwt;
    //   28: aload_0
    //   29: getstatic 36	com/google/android/gms/internal/measurement/zzud:zzbtz	Lcom/google/android/gms/internal/measurement/zzud;
    //   32: putfield 38	com/google/android/gms/internal/measurement/zzwa:zzcaf	Lcom/google/android/gms/internal/measurement/zzud;
    //   35: aload_0
    //   36: monitorexit
    //   37: goto -19 -> 18
    //   40: astore_1
    //   41: aload_0
    //   42: monitorexit
    //   43: aload_1
    //   44: athrow
    //   45: astore_2
    //   46: aload_0
    //   47: aload_1
    //   48: putfield 31	com/google/android/gms/internal/measurement/zzwa:zzcae	Lcom/google/android/gms/internal/measurement/zzwt;
    //   51: aload_0
    //   52: getstatic 36	com/google/android/gms/internal/measurement/zzud:zzbtz	Lcom/google/android/gms/internal/measurement/zzud;
    //   55: putfield 38	com/google/android/gms/internal/measurement/zzwa:zzcaf	Lcom/google/android/gms/internal/measurement/zzud;
    //   58: goto -23 -> 35
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	61	0	this	zzwa
    //   0	61	1	paramzzwt	zzwt
    //   45	1	2	localzzvt	zzvt
    // Exception table:
    //   from	to	target	type
    //   9	18	40	finally
    //   23	35	40	finally
    //   35	37	40	finally
    //   41	43	40	finally
    //   46	58	40	finally
    //   23	35	45	com/google/android/gms/internal/measurement/zzvt
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof zzwa)) {
      return false;
    }
    paramObject = (zzwa)paramObject;
    zzwt localzzwt1 = this.zzcae;
    zzwt localzzwt2 = ((zzwa)paramObject).zzcae;
    if ((localzzwt1 == null) && (localzzwt2 == null)) {
      return zztt().equals(((zzwa)paramObject).zztt());
    }
    if ((localzzwt1 != null) && (localzzwt2 != null)) {
      return localzzwt1.equals(localzzwt2);
    }
    if (localzzwt1 != null) {
      return localzzwt1.equals(((zzwa)paramObject).zzh(localzzwt1.zzwf()));
    }
    return zzh(localzzwt2.zzwf()).equals(localzzwt2);
  }
  
  public int hashCode()
  {
    return 1;
  }
  
  public final zzwt zzi(zzwt paramzzwt)
  {
    zzwt localzzwt = this.zzcae;
    this.zzcad = null;
    this.zzcaf = null;
    this.zzcae = paramzzwt;
    return localzzwt;
  }
  
  public final zzud zztt()
  {
    if (this.zzcaf != null) {
      return this.zzcaf;
    }
    try
    {
      if (this.zzcaf != null)
      {
        zzud localzzud1 = this.zzcaf;
        return localzzud1;
      }
    }
    finally {}
    if (this.zzcae == null) {}
    for (this.zzcaf = zzud.zzbtz;; this.zzcaf = this.zzcae.zztt())
    {
      zzud localzzud2 = this.zzcaf;
      return localzzud2;
    }
  }
  
  public final int zzvu()
  {
    if (this.zzcaf != null) {
      return this.zzcaf.size();
    }
    if (this.zzcae != null) {
      return this.zzcae.zzvu();
    }
    return 0;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */