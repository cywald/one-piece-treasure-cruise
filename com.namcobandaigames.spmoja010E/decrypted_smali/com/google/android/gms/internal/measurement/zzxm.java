package com.google.android.gms.internal.measurement;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class zzxm<K extends Comparable<K>, V>
  extends AbstractMap<K, V>
{
  private boolean zzbpo;
  private final int zzcca;
  private List<zzxt> zzccb;
  private Map<K, V> zzccc;
  private volatile zzxv zzccd;
  private Map<K, V> zzcce;
  private volatile zzxp zzccf;
  
  private zzxm(int paramInt)
  {
    this.zzcca = paramInt;
    this.zzccb = Collections.emptyList();
    this.zzccc = Collections.emptyMap();
    this.zzcce = Collections.emptyMap();
  }
  
  private final int zza(K paramK)
  {
    int i = this.zzccb.size() - 1;
    int j;
    if (i >= 0)
    {
      j = paramK.compareTo((Comparable)((zzxt)this.zzccb.get(i)).getKey());
      if (j > 0) {
        return -(i + 2);
      }
      if (j == 0) {
        return i;
      }
    }
    for (;;)
    {
      if (j <= i)
      {
        int k = (j + i) / 2;
        int m = paramK.compareTo((Comparable)((zzxt)this.zzccb.get(k)).getKey());
        if (m < 0) {
          i = k - 1;
        } else if (m > 0) {
          j = k + 1;
        } else {
          return k;
        }
      }
      else
      {
        return -(j + 1);
        j = 0;
      }
    }
  }
  
  static <FieldDescriptorType extends zzvf<FieldDescriptorType>> zzxm<FieldDescriptorType, Object> zzbt(int paramInt)
  {
    return new zzxn(paramInt);
  }
  
  private final V zzbv(int paramInt)
  {
    zzxz();
    Object localObject = ((zzxt)this.zzccb.remove(paramInt)).getValue();
    if (!this.zzccc.isEmpty())
    {
      Iterator localIterator = zzya().entrySet().iterator();
      this.zzccb.add(new zzxt(this, (Map.Entry)localIterator.next()));
      localIterator.remove();
    }
    return (V)localObject;
  }
  
  private final void zzxz()
  {
    if (this.zzbpo) {
      throw new UnsupportedOperationException();
    }
  }
  
  private final SortedMap<K, V> zzya()
  {
    zzxz();
    if ((this.zzccc.isEmpty()) && (!(this.zzccc instanceof TreeMap)))
    {
      this.zzccc = new TreeMap();
      this.zzcce = ((TreeMap)this.zzccc).descendingMap();
    }
    return (SortedMap)this.zzccc;
  }
  
  public void clear()
  {
    zzxz();
    if (!this.zzccb.isEmpty()) {
      this.zzccb.clear();
    }
    if (!this.zzccc.isEmpty()) {
      this.zzccc.clear();
    }
  }
  
  public boolean containsKey(Object paramObject)
  {
    paramObject = (Comparable)paramObject;
    return (zza((Comparable)paramObject) >= 0) || (this.zzccc.containsKey(paramObject));
  }
  
  public Set<Map.Entry<K, V>> entrySet()
  {
    if (this.zzccd == null) {
      this.zzccd = new zzxv(this, null);
    }
    return this.zzccd;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    int j;
    int k;
    do
    {
      return true;
      if (!(paramObject instanceof zzxm)) {
        return super.equals(paramObject);
      }
      paramObject = (zzxm)paramObject;
      j = size();
      if (j != ((zzxm)paramObject).size()) {
        return false;
      }
      k = zzxw();
      if (k != ((zzxm)paramObject).zzxw()) {
        return entrySet().equals(((zzxm)paramObject).entrySet());
      }
      int i = 0;
      while (i < k)
      {
        if (!zzbu(i).equals(((zzxm)paramObject).zzbu(i))) {
          return false;
        }
        i += 1;
      }
    } while (k == j);
    return this.zzccc.equals(((zzxm)paramObject).zzccc);
  }
  
  public V get(Object paramObject)
  {
    paramObject = (Comparable)paramObject;
    int i = zza((Comparable)paramObject);
    if (i >= 0) {
      return (V)((zzxt)this.zzccb.get(i)).getValue();
    }
    return (V)this.zzccc.get(paramObject);
  }
  
  public int hashCode()
  {
    int k = zzxw();
    int i = 0;
    int j = 0;
    while (i < k)
    {
      j += ((zzxt)this.zzccb.get(i)).hashCode();
      i += 1;
    }
    if (this.zzccc.size() > 0) {
      return this.zzccc.hashCode() + j;
    }
    return j;
  }
  
  public final boolean isImmutable()
  {
    return this.zzbpo;
  }
  
  public V remove(Object paramObject)
  {
    zzxz();
    paramObject = (Comparable)paramObject;
    int i = zza((Comparable)paramObject);
    if (i >= 0) {
      return (V)zzbv(i);
    }
    if (this.zzccc.isEmpty()) {
      return null;
    }
    return (V)this.zzccc.remove(paramObject);
  }
  
  public int size()
  {
    return this.zzccb.size() + this.zzccc.size();
  }
  
  public final V zza(K paramK, V paramV)
  {
    zzxz();
    int i = zza(paramK);
    if (i >= 0) {
      return (V)((zzxt)this.zzccb.get(i)).setValue(paramV);
    }
    zzxz();
    if ((this.zzccb.isEmpty()) && (!(this.zzccb instanceof ArrayList))) {
      this.zzccb = new ArrayList(this.zzcca);
    }
    i = -(i + 1);
    if (i >= this.zzcca) {
      return (V)zzya().put(paramK, paramV);
    }
    if (this.zzccb.size() == this.zzcca)
    {
      zzxt localzzxt = (zzxt)this.zzccb.remove(this.zzcca - 1);
      zzya().put((Comparable)localzzxt.getKey(), localzzxt.getValue());
    }
    this.zzccb.add(i, new zzxt(this, paramK, paramV));
    return null;
  }
  
  public final Map.Entry<K, V> zzbu(int paramInt)
  {
    return (Map.Entry)this.zzccb.get(paramInt);
  }
  
  public void zzsm()
  {
    if (!this.zzbpo)
    {
      if (!this.zzccc.isEmpty()) {
        break label55;
      }
      localMap = Collections.emptyMap();
      this.zzccc = localMap;
      if (!this.zzcce.isEmpty()) {
        break label66;
      }
    }
    label55:
    label66:
    for (Map localMap = Collections.emptyMap();; localMap = Collections.unmodifiableMap(this.zzcce))
    {
      this.zzcce = localMap;
      this.zzbpo = true;
      return;
      localMap = Collections.unmodifiableMap(this.zzccc);
      break;
    }
  }
  
  public final int zzxw()
  {
    return this.zzccb.size();
  }
  
  public final Iterable<Map.Entry<K, V>> zzxx()
  {
    if (this.zzccc.isEmpty()) {
      return zzxq.zzyc();
    }
    return this.zzccc.entrySet();
  }
  
  final Set<Map.Entry<K, V>> zzxy()
  {
    if (this.zzccf == null) {
      this.zzccf = new zzxp(this, null);
    }
    return this.zzccf;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */