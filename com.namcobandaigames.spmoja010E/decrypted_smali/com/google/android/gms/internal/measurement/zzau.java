package com.google.android.gms.internal.measurement;

public abstract class zzau
  extends zzat
{
  private boolean zzvz;
  
  protected zzau(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  public final boolean isInitialized()
  {
    return this.zzvz;
  }
  
  protected abstract void zzag();
  
  protected final void zzcl()
  {
    if (!isInitialized()) {
      throw new IllegalStateException("Not initialized");
    }
  }
  
  public final void zzq()
  {
    zzag();
    this.zzvz = true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzau.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */