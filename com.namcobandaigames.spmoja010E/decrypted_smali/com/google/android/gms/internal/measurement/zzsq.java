package com.google.android.gms.internal.measurement;

import android.util.Log;

final class zzsq
  extends zzsl<Integer>
{
  zzsq(zzsv paramzzsv, String paramString, Integer paramInteger)
  {
    super(paramzzsv, paramString, paramInteger, null);
  }
  
  private final Integer zzfl(String paramString)
  {
    try
    {
      int i = Integer.parseInt(paramString);
      return Integer.valueOf(i);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      String str = this.zzbrc;
      Log.e("PhenotypeFlag", String.valueOf(str).length() + 28 + String.valueOf(paramString).length() + "Invalid integer value for " + str + ": " + paramString);
    }
    return null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */