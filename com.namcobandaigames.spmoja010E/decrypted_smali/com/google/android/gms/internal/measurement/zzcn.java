package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzcn
{
  private final long zzabb = 2000L;
  private final int zzabc = 60;
  private double zzabd = this.zzabc;
  private long zzabe;
  private final Object zzabf = new Object();
  private final Clock zzrz;
  private final String zzul;
  
  private zzcn(int paramInt, long paramLong, String paramString, Clock paramClock)
  {
    this.zzul = paramString;
    this.zzrz = paramClock;
  }
  
  public zzcn(String paramString, Clock paramClock)
  {
    this(60, 2000L, paramString, paramClock);
  }
  
  public final boolean zzew()
  {
    synchronized (this.zzabf)
    {
      long l = this.zzrz.currentTimeMillis();
      if (this.zzabd < this.zzabc)
      {
        double d = (l - this.zzabe) / this.zzabb;
        if (d > 0.0D) {
          this.zzabd = Math.min(this.zzabc, d + this.zzabd);
        }
      }
      this.zzabe = l;
      if (this.zzabd >= 1.0D)
      {
        this.zzabd -= 1.0D;
        return true;
      }
      String str = this.zzul;
      zzco.zzab(String.valueOf(str).length() + 34 + "Excessive " + str + " detected; call ignored.");
      return false;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */