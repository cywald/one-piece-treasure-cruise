package com.google.android.gms.internal.measurement;

import java.io.PrintStream;
import java.lang.reflect.Field;

public final class zzsw
{
  private static final zzsx zzbrt;
  private static final int zzbru;
  
  static
  {
    for (;;)
    {
      try
      {
        localInteger = zztk();
        if (localInteger != null) {}
        try
        {
          if (localInteger.intValue() >= 19)
          {
            localObject = new zztb();
            zzbrt = (zzsx)localObject;
            if (localInteger != null) {
              continue;
            }
            i = 1;
            zzbru = i;
            return;
          }
          if (Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic")) {
            continue;
          }
          i = 1;
          if (i == 0) {
            continue;
          }
          Object localObject = new zzta();
          continue;
          localPrintStream = System.err;
        }
        catch (Throwable localThrowable1) {}
      }
      catch (Throwable localThrowable2)
      {
        int i;
        PrintStream localPrintStream;
        String str;
        zza localzza;
        Integer localInteger = null;
        continue;
      }
      str = zza.class.getName();
      localPrintStream.println(String.valueOf(str).length() + 132 + "An error has occured when initializing the try-with-resources desuguring strategy. The default strategy " + str + "will be used. The error is: ");
      localThrowable1.printStackTrace(System.err);
      localzza = new zza();
      continue;
      i = 0;
      continue;
      localzza = new zza();
      continue;
      i = localInteger.intValue();
    }
  }
  
  public static void zza(Throwable paramThrowable, PrintStream paramPrintStream)
  {
    zzbrt.zza(paramThrowable, paramPrintStream);
  }
  
  private static Integer zztk()
  {
    try
    {
      Integer localInteger = (Integer)Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
      return localInteger;
    }
    catch (Exception localException)
    {
      System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
      localException.printStackTrace(System.err);
    }
    return null;
  }
  
  static final class zza
    extends zzsx
  {
    public final void zza(Throwable paramThrowable, PrintStream paramPrintStream)
    {
      paramThrowable.printStackTrace(paramPrintStream);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */