package com.google.android.gms.internal.measurement;

import java.util.Iterator;

final class zzxs
  implements Iterable<Object>
{
  public final Iterator<Object> iterator()
  {
    return zzxq.zzyd();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */