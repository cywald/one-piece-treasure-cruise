package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.util.VisibleForTesting;
import java.io.IOException;
import java.io.InputStream;

@VisibleForTesting
public abstract interface zzsd
{
  public abstract void close();
  
  public abstract InputStream zzev(String paramString)
    throws IOException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */