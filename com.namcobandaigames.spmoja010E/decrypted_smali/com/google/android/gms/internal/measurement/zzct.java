package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.util.Clock;

public final class zzct
  extends zzau
{
  private SharedPreferences zzabr;
  private long zzabs;
  private long zzabt = -1L;
  private final zzcv zzabu = new zzcv(this, "monitoring", ((Long)zzcf.zzaak.get()).longValue(), null);
  
  protected zzct(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  public final void zzac(String paramString)
  {
    zzk.zzaf();
    zzcl();
    SharedPreferences.Editor localEditor = this.zzabr.edit();
    if (TextUtils.isEmpty(paramString)) {
      localEditor.remove("installation_campaign");
    }
    for (;;)
    {
      if (!localEditor.commit()) {
        zzt("Failed to commit campaign data");
      }
      return;
      localEditor.putString("installation_campaign", paramString);
    }
  }
  
  protected final void zzag()
  {
    this.zzabr = getContext().getSharedPreferences("com.google.android.gms.analytics.prefs", 0);
  }
  
  public final long zzff()
  {
    zzk.zzaf();
    zzcl();
    long l;
    if (this.zzabs == 0L)
    {
      l = this.zzabr.getLong("first_run", 0L);
      if (l == 0L) {
        break label45;
      }
    }
    for (this.zzabs = l;; this.zzabs = l)
    {
      return this.zzabs;
      label45:
      l = zzbx().currentTimeMillis();
      SharedPreferences.Editor localEditor = this.zzabr.edit();
      localEditor.putLong("first_run", l);
      if (!localEditor.commit()) {
        zzt("Failed to commit first run time");
      }
    }
  }
  
  public final zzdc zzfg()
  {
    return new zzdc(zzbx(), zzff());
  }
  
  public final long zzfh()
  {
    zzk.zzaf();
    zzcl();
    if (this.zzabt == -1L) {
      this.zzabt = this.zzabr.getLong("last_dispatch", 0L);
    }
    return this.zzabt;
  }
  
  public final void zzfi()
  {
    zzk.zzaf();
    zzcl();
    long l = zzbx().currentTimeMillis();
    SharedPreferences.Editor localEditor = this.zzabr.edit();
    localEditor.putLong("last_dispatch", l);
    localEditor.apply();
    this.zzabt = l;
  }
  
  public final String zzfj()
  {
    zzk.zzaf();
    zzcl();
    String str = this.zzabr.getString("installation_campaign", null);
    if (TextUtils.isEmpty(str)) {
      return null;
    }
    return str;
  }
  
  public final zzcv zzfk()
  {
    return this.zzabu;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */