package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzvn
  extends zztz<Integer>
  implements zzvs<Integer>, zzxe, RandomAccess
{
  private static final zzvn zzbzh;
  private int size;
  private int[] zzbzi;
  
  static
  {
    zzvn localzzvn = new zzvn();
    zzbzh = localzzvn;
    localzzvn.zzsm();
  }
  
  zzvn()
  {
    this(new int[10], 0);
  }
  
  private zzvn(int[] paramArrayOfInt, int paramInt)
  {
    this.zzbzi = paramArrayOfInt;
    this.size = paramInt;
  }
  
  private final void zzai(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
  }
  
  private final String zzaj(int paramInt)
  {
    int i = this.size;
    return 35 + "Index:" + paramInt + ", Size:" + i;
  }
  
  private final void zzp(int paramInt1, int paramInt2)
  {
    zztx();
    if ((paramInt1 < 0) || (paramInt1 > this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt1));
    }
    if (this.size < this.zzbzi.length) {
      System.arraycopy(this.zzbzi, paramInt1, this.zzbzi, paramInt1 + 1, this.size - paramInt1);
    }
    for (;;)
    {
      this.zzbzi[paramInt1] = paramInt2;
      this.size += 1;
      this.modCount += 1;
      return;
      int[] arrayOfInt = new int[this.size * 3 / 2 + 1];
      System.arraycopy(this.zzbzi, 0, arrayOfInt, 0, paramInt1);
      System.arraycopy(this.zzbzi, paramInt1, arrayOfInt, paramInt1 + 1, this.size - paramInt1);
      this.zzbzi = arrayOfInt;
    }
  }
  
  public final boolean addAll(Collection<? extends Integer> paramCollection)
  {
    boolean bool = false;
    zztx();
    zzvo.checkNotNull(paramCollection);
    if (!(paramCollection instanceof zzvn)) {
      bool = super.addAll(paramCollection);
    }
    do
    {
      return bool;
      paramCollection = (zzvn)paramCollection;
    } while (paramCollection.size == 0);
    if (Integer.MAX_VALUE - this.size < paramCollection.size) {
      throw new OutOfMemoryError();
    }
    int i = this.size + paramCollection.size;
    if (i > this.zzbzi.length) {
      this.zzbzi = Arrays.copyOf(this.zzbzi, i);
    }
    System.arraycopy(paramCollection.zzbzi, 0, this.zzbzi, this.size, paramCollection.size);
    this.size = i;
    this.modCount += 1;
    return true;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      return bool1;
      if (!(paramObject instanceof zzvn)) {
        return super.equals(paramObject);
      }
      paramObject = (zzvn)paramObject;
      bool1 = bool2;
    } while (this.size != ((zzvn)paramObject).size);
    paramObject = ((zzvn)paramObject).zzbzi;
    int i = 0;
    for (;;)
    {
      if (i >= this.size) {
        break label81;
      }
      bool1 = bool2;
      if (this.zzbzi[i] != paramObject[i]) {
        break;
      }
      i += 1;
    }
    label81:
    return true;
  }
  
  public final int getInt(int paramInt)
  {
    zzai(paramInt);
    return this.zzbzi[paramInt];
  }
  
  public final int hashCode()
  {
    int j = 1;
    int i = 0;
    while (i < this.size)
    {
      j = j * 31 + this.zzbzi[i];
      i += 1;
    }
    return j;
  }
  
  public final boolean remove(Object paramObject)
  {
    boolean bool2 = false;
    zztx();
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      if (i < this.size)
      {
        if (paramObject.equals(Integer.valueOf(this.zzbzi[i])))
        {
          System.arraycopy(this.zzbzi, i + 1, this.zzbzi, i, this.size - i);
          this.size -= 1;
          this.modCount += 1;
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
      i += 1;
    }
  }
  
  protected final void removeRange(int paramInt1, int paramInt2)
  {
    zztx();
    if (paramInt2 < paramInt1) {
      throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }
    System.arraycopy(this.zzbzi, paramInt2, this.zzbzi, paramInt1, this.size - paramInt2);
    this.size -= paramInt2 - paramInt1;
    this.modCount += 1;
  }
  
  public final int size()
  {
    return this.size;
  }
  
  public final void zzbm(int paramInt)
  {
    zzp(this.size, paramInt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */