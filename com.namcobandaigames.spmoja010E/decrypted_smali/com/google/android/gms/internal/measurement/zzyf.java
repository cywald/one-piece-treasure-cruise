package com.google.android.gms.internal.measurement;

import java.util.ListIterator;

final class zzyf
  implements ListIterator<String>
{
  private ListIterator<String> zzccr = zzye.zza(this.zzcct).listIterator(this.zzccs);
  
  zzyf(zzye paramzzye, int paramInt) {}
  
  public final boolean hasNext()
  {
    return this.zzccr.hasNext();
  }
  
  public final boolean hasPrevious()
  {
    return this.zzccr.hasPrevious();
  }
  
  public final int nextIndex()
  {
    return this.zzccr.nextIndex();
  }
  
  public final int previousIndex()
  {
    return this.zzccr.previousIndex();
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */