package com.google.android.gms.internal.measurement;

import java.lang.reflect.Constructor;

final class zzwq
{
  private static final zzwo zzcav = ;
  private static final zzwo zzcaw = new zzwp();
  
  static zzwo zzxd()
  {
    return zzcav;
  }
  
  static zzwo zzxe()
  {
    return zzcaw;
  }
  
  private static zzwo zzxf()
  {
    try
    {
      zzwo localzzwo = (zzwo)Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
      return localzzwo;
    }
    catch (Exception localException) {}
    return null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */