package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzh
  extends zzza<zzh>
{
  private static volatile zzh[] zzod;
  private int name = 0;
  public int[] zzoe = zzzj.zzcax;
  private int zzof = 0;
  private boolean zzog = false;
  private boolean zzoh = false;
  
  public zzh()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzh[] zze()
  {
    if (zzod == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzod == null) {
        zzod = new zzh[0];
      }
      return zzod;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzh)) {
        return false;
      }
      paramObject = (zzh)paramObject;
      if (!zzze.equals(this.zzoe, ((zzh)paramObject).zzoe)) {
        return false;
      }
      if (this.zzof != ((zzh)paramObject).zzof) {
        return false;
      }
      if (this.name != ((zzh)paramObject).name) {
        return false;
      }
      if (this.zzog != ((zzh)paramObject).zzog) {
        return false;
      }
      if (this.zzoh != ((zzh)paramObject).zzoh) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzh)paramObject).zzcfc == null) || (((zzh)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzh)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int j = 1231;
    int m = getClass().getName().hashCode();
    int n = zzze.hashCode(this.zzoe);
    int i1 = this.zzof;
    int i2 = this.name;
    int i;
    if (this.zzog)
    {
      i = 1231;
      if (!this.zzoh) {
        break label121;
      }
      label55:
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break label128;
      }
    }
    label121:
    label128:
    for (int k = 0;; k = this.zzcfc.hashCode())
    {
      return k + ((i + ((((m + 527) * 31 + n) * 31 + i1) * 31 + i2) * 31) * 31 + j) * 31;
      i = 1237;
      break;
      j = 1237;
      break label55;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzoh) {
      paramzzyy.zzb(1, this.zzoh);
    }
    paramzzyy.zzd(2, this.zzof);
    if ((this.zzoe != null) && (this.zzoe.length > 0))
    {
      int i = 0;
      while (i < this.zzoe.length)
      {
        paramzzyy.zzd(3, this.zzoe[i]);
        i += 1;
      }
    }
    if (this.name != 0) {
      paramzzyy.zzd(4, this.name);
    }
    if (this.zzog) {
      paramzzyy.zzb(6, this.zzog);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = 0;
    int k = super.zzf();
    int i = k;
    if (this.zzoh) {
      i = k + (zzyy.zzbb(1) + 1);
    }
    k = zzyy.zzh(2, this.zzof) + i;
    if ((this.zzoe != null) && (this.zzoe.length > 0))
    {
      i = 0;
      while (i < this.zzoe.length)
      {
        j += zzyy.zzbc(this.zzoe[i]);
        i += 1;
      }
    }
    for (j = k + j + this.zzoe.length * 1;; j = k)
    {
      i = j;
      if (this.name != 0) {
        i = j + zzyy.zzh(4, this.name);
      }
      j = i;
      if (this.zzog) {
        j = i + (zzyy.zzbb(6) + 1);
      }
      return j;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */