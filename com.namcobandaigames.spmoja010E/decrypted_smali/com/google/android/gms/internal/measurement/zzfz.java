package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzfz
  extends zzza<zzfz>
{
  public Integer zzavw = null;
  public String zzavx = null;
  public Boolean zzavy = null;
  public String[] zzavz = zzzj.zzcfv;
  
  public zzfz()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  private final zzfz zzd(zzyx paramzzyx)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzyx.zzug();
      int j;
      switch (i)
      {
      default: 
        if (super.zza(paramzzyx, i)) {}
        break;
      case 0: 
        return this;
      case 8: 
        j = paramzzyx.getPosition();
        int k;
        try
        {
          k = paramzzyx.zzuy();
          if ((k < 0) || (k > 6)) {
            break label119;
          }
          this.zzavw = Integer.valueOf(k);
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          paramzzyx.zzby(j);
          zza(paramzzyx, i);
        }
        continue;
        throw new IllegalArgumentException(41 + k + " is not a valid enum MatchType");
      case 18: 
        this.zzavx = paramzzyx.readString();
        break;
      case 24: 
        this.zzavy = Boolean.valueOf(paramzzyx.zzum());
        break;
      case 34: 
        label119:
        j = zzzj.zzb(paramzzyx, 34);
        if (this.zzavz == null) {}
        String[] arrayOfString;
        for (i = 0;; i = this.zzavz.length)
        {
          arrayOfString = new String[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.zzavz, 0, arrayOfString, 0, i);
            j = i;
          }
          while (j < arrayOfString.length - 1)
          {
            arrayOfString[j] = paramzzyx.readString();
            paramzzyx.zzug();
            j += 1;
          }
        }
        arrayOfString[j] = paramzzyx.readString();
        this.zzavz = arrayOfString;
      }
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzfz)) {
        return false;
      }
      paramObject = (zzfz)paramObject;
      if (this.zzavw == null)
      {
        if (((zzfz)paramObject).zzavw != null) {
          return false;
        }
      }
      else if (!this.zzavw.equals(((zzfz)paramObject).zzavw)) {
        return false;
      }
      if (this.zzavx == null)
      {
        if (((zzfz)paramObject).zzavx != null) {
          return false;
        }
      }
      else if (!this.zzavx.equals(((zzfz)paramObject).zzavx)) {
        return false;
      }
      if (this.zzavy == null)
      {
        if (((zzfz)paramObject).zzavy != null) {
          return false;
        }
      }
      else if (!this.zzavy.equals(((zzfz)paramObject).zzavy)) {
        return false;
      }
      if (!zzze.equals(this.zzavz, ((zzfz)paramObject).zzavz)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzfz)paramObject).zzcfc == null) || (((zzfz)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzfz)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int n = 0;
    int i1 = getClass().getName().hashCode();
    int i;
    int j;
    label33:
    int k;
    label42:
    int i2;
    if (this.zzavw == null)
    {
      i = 0;
      if (this.zzavx != null) {
        break label121;
      }
      j = 0;
      if (this.zzavy != null) {
        break label132;
      }
      k = 0;
      i2 = zzze.hashCode(this.zzavz);
      m = n;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label143;
        }
      }
    }
    label121:
    label132:
    label143:
    for (int m = n;; m = this.zzcfc.hashCode())
    {
      return ((k + (j + (i + (i1 + 527) * 31) * 31) * 31) * 31 + i2) * 31 + m;
      i = this.zzavw.intValue();
      break;
      j = this.zzavx.hashCode();
      break label33;
      k = this.zzavy.hashCode();
      break label42;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzavw != null) {
      paramzzyy.zzd(1, this.zzavw.intValue());
    }
    if (this.zzavx != null) {
      paramzzyy.zzb(2, this.zzavx);
    }
    if (this.zzavy != null) {
      paramzzyy.zzb(3, this.zzavy.booleanValue());
    }
    if ((this.zzavz != null) && (this.zzavz.length > 0))
    {
      int i = 0;
      while (i < this.zzavz.length)
      {
        String str = this.zzavz[i];
        if (str != null) {
          paramzzyy.zzb(4, str);
        }
        i += 1;
      }
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int n = 0;
    int j = super.zzf();
    int i = j;
    if (this.zzavw != null) {
      i = j + zzyy.zzh(1, this.zzavw.intValue());
    }
    j = i;
    if (this.zzavx != null) {
      j = i + zzyy.zzc(2, this.zzavx);
    }
    i = j;
    if (this.zzavy != null)
    {
      this.zzavy.booleanValue();
      i = j + (zzyy.zzbb(3) + 1);
    }
    j = i;
    if (this.zzavz != null)
    {
      j = i;
      if (this.zzavz.length > 0)
      {
        int k = 0;
        int m = 0;
        j = n;
        while (j < this.zzavz.length)
        {
          String str = this.zzavz[j];
          int i1 = k;
          n = m;
          if (str != null)
          {
            n = m + 1;
            i1 = k + zzyy.zzfx(str);
          }
          j += 1;
          k = i1;
          m = n;
        }
        j = i + k + m * 1;
      }
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzfz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */