package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.List;

public final class zzrz
{
  private final List<zzru> zzbop = new ArrayList();
  private final List<zzru> zzboq = new ArrayList();
  private final List<zzru> zzbor = new ArrayList();
  private final List<zzru> zzbos = new ArrayList();
  private final List<zzru> zzbpv = new ArrayList();
  private final List<zzru> zzbpw = new ArrayList();
  private final List<String> zzbpx = new ArrayList();
  private final List<String> zzbpy = new ArrayList();
  private final List<String> zzbpz = new ArrayList();
  private final List<String> zzbqa = new ArrayList();
  
  public final zzrz zzd(zzru paramzzru)
  {
    this.zzbop.add(paramzzru);
    return this;
  }
  
  public final zzrz zze(zzru paramzzru)
  {
    this.zzboq.add(paramzzru);
    return this;
  }
  
  public final zzrz zzf(zzru paramzzru)
  {
    this.zzbor.add(paramzzru);
    return this;
  }
  
  public final zzrz zzff(String paramString)
  {
    this.zzbpz.add(paramString);
    return this;
  }
  
  public final zzrz zzfg(String paramString)
  {
    this.zzbqa.add(paramString);
    return this;
  }
  
  public final zzrz zzfh(String paramString)
  {
    this.zzbpx.add(paramString);
    return this;
  }
  
  public final zzrz zzfi(String paramString)
  {
    this.zzbpy.add(paramString);
    return this;
  }
  
  public final zzrz zzg(zzru paramzzru)
  {
    this.zzbos.add(paramzzru);
    return this;
  }
  
  public final zzrz zzh(zzru paramzzru)
  {
    this.zzbpv.add(paramzzru);
    return this;
  }
  
  public final zzrz zzi(zzru paramzzru)
  {
    this.zzbpw.add(paramzzru);
    return this;
  }
  
  public final zzry zzsw()
  {
    return new zzry(this.zzbop, this.zzboq, this.zzbor, this.zzbos, this.zzbpv, this.zzbpw, this.zzbpx, this.zzbpy, this.zzbpz, this.zzbqa, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzrz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */