package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

public final class zzwb
  extends zztz<String>
  implements zzwc, RandomAccess
{
  private static final zzwb zzcag;
  private static final zzwc zzcah = zzcag;
  private final List<Object> zzcai;
  
  static
  {
    zzwb localzzwb = new zzwb();
    zzcag = localzzwb;
    localzzwb.zzsm();
  }
  
  public zzwb()
  {
    this(10);
  }
  
  public zzwb(int paramInt)
  {
    this(new ArrayList(paramInt));
  }
  
  private zzwb(ArrayList<Object> paramArrayList)
  {
    this.zzcai = paramArrayList;
  }
  
  private static String zzw(Object paramObject)
  {
    if ((paramObject instanceof String)) {
      return (String)paramObject;
    }
    if ((paramObject instanceof zzud)) {
      return ((zzud)paramObject).zzua();
    }
    return zzvo.zzm((byte[])paramObject);
  }
  
  public final boolean addAll(int paramInt, Collection<? extends String> paramCollection)
  {
    zztx();
    Object localObject = paramCollection;
    if ((paramCollection instanceof zzwc)) {
      localObject = ((zzwc)paramCollection).zzwv();
    }
    boolean bool = this.zzcai.addAll(paramInt, (Collection)localObject);
    this.modCount += 1;
    return bool;
  }
  
  public final boolean addAll(Collection<? extends String> paramCollection)
  {
    return addAll(size(), paramCollection);
  }
  
  public final void clear()
  {
    zztx();
    this.zzcai.clear();
    this.modCount += 1;
  }
  
  public final Object getRaw(int paramInt)
  {
    return this.zzcai.get(paramInt);
  }
  
  public final int size()
  {
    return this.zzcai.size();
  }
  
  public final void zzc(zzud paramzzud)
  {
    zztx();
    this.zzcai.add(paramzzud);
    this.modCount += 1;
  }
  
  public final List<?> zzwv()
  {
    return Collections.unmodifiableList(this.zzcai);
  }
  
  public final zzwc zzww()
  {
    Object localObject = this;
    if (zztw()) {
      localObject = new zzye(this);
    }
    return (zzwc)localObject;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */