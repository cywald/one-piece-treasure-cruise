package com.google.android.gms.internal.measurement;

import java.lang.reflect.Method;

final class zzwi
  implements zzxk
{
  private static final zzws zzcap = new zzwj();
  private final zzws zzcao;
  
  public zzwi()
  {
    this(new zzwk(new zzws[] { zzvl.zzwb(), zzwz() }));
  }
  
  private zzwi(zzws paramzzws)
  {
    this.zzcao = ((zzws)zzvo.zza(paramzzws, "messageInfoFactory"));
  }
  
  private static boolean zza(zzwr paramzzwr)
  {
    return paramzzwr.zzxg() == zzvm.zze.zzbzb;
  }
  
  private static zzws zzwz()
  {
    try
    {
      zzws localzzws = (zzws)Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
      return localzzws;
    }
    catch (Exception localException) {}
    return zzcap;
  }
  
  public final <T> zzxj<T> zzh(Class<T> paramClass)
  {
    zzxl.zzj(paramClass);
    zzwr localzzwr = this.zzcao.zzf(paramClass);
    if (localzzwr.zzxh())
    {
      if (zzvm.class.isAssignableFrom(paramClass)) {
        return zzwy.zza(zzxl.zzxt(), zzvc.zzvr(), localzzwr.zzxi());
      }
      return zzwy.zza(zzxl.zzxr(), zzvc.zzvs(), localzzwr.zzxi());
    }
    if (zzvm.class.isAssignableFrom(paramClass))
    {
      if (zza(localzzwr)) {
        return zzwx.zza(paramClass, localzzwr, zzxc.zzxl(), zzwd.zzwy(), zzxl.zzxt(), zzvc.zzvr(), zzwq.zzxe());
      }
      return zzwx.zza(paramClass, localzzwr, zzxc.zzxl(), zzwd.zzwy(), zzxl.zzxt(), null, zzwq.zzxe());
    }
    if (zza(localzzwr)) {
      return zzwx.zza(paramClass, localzzwr, zzxc.zzxk(), zzwd.zzwx(), zzxl.zzxr(), zzvc.zzvs(), zzwq.zzxd());
    }
    return zzwx.zza(paramClass, localzzwr, zzxc.zzxk(), zzwd.zzwx(), zzxl.zzxs(), null, zzwq.zzxd());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */