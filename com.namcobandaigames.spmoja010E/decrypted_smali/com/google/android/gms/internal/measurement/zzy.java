package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzi;
import java.util.HashMap;
import java.util.Map;

public final class zzy
  extends zzi<zzy>
{
  private String name;
  private String zzno;
  private String zztv;
  private String zztw;
  private String zztx;
  private String zzty;
  private String zztz;
  private String zzua;
  private String zzub;
  private String zzuc;
  
  public final String getId()
  {
    return this.zzno;
  }
  
  public final String getName()
  {
    return this.name;
  }
  
  public final String getSource()
  {
    return this.zztv;
  }
  
  public final void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("name", this.name);
    localHashMap.put("source", this.zztv);
    localHashMap.put("medium", this.zztw);
    localHashMap.put("keyword", this.zztx);
    localHashMap.put("content", this.zzty);
    localHashMap.put("id", this.zzno);
    localHashMap.put("adNetworkId", this.zztz);
    localHashMap.put("gclid", this.zzua);
    localHashMap.put("dclid", this.zzub);
    localHashMap.put("aclid", this.zzuc);
    return zza(localHashMap);
  }
  
  public final String zzan()
  {
    return this.zztw;
  }
  
  public final String zzao()
  {
    return this.zztx;
  }
  
  public final String zzap()
  {
    return this.zzty;
  }
  
  public final String zzaq()
  {
    return this.zztz;
  }
  
  public final String zzar()
  {
    return this.zzua;
  }
  
  public final String zzas()
  {
    return this.zzub;
  }
  
  public final String zzat()
  {
    return this.zzuc;
  }
  
  public final void zzc(String paramString)
  {
    this.zztv = paramString;
  }
  
  public final void zzd(String paramString)
  {
    this.zztw = paramString;
  }
  
  public final void zze(String paramString)
  {
    this.zztx = paramString;
  }
  
  public final void zzf(String paramString)
  {
    this.zzty = paramString;
  }
  
  public final void zzg(String paramString)
  {
    this.zzno = paramString;
  }
  
  public final void zzh(String paramString)
  {
    this.zztz = paramString;
  }
  
  public final void zzi(String paramString)
  {
    this.zzua = paramString;
  }
  
  public final void zzj(String paramString)
  {
    this.zzub = paramString;
  }
  
  public final void zzk(String paramString)
  {
    this.zzuc = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */