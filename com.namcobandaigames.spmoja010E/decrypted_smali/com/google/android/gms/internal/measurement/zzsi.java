package com.google.android.gms.internal.measurement;

import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.GuardedBy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class zzsi
{
  private static final Object zzbqp = new Object();
  @GuardedBy("loadersLock")
  private static final Map<Uri, zzsi> zzbqq = new HashMap();
  private static final String[] zzbqw = { "key", "value" };
  private final Uri uri;
  private final ContentResolver zzbqr;
  private final Object zzbqs = new Object();
  private volatile Map<String, String> zzbqt;
  private final Object zzbqu = new Object();
  @GuardedBy("listenersLock")
  private final List<zzsk> zzbqv = new ArrayList();
  
  private zzsi(ContentResolver paramContentResolver, Uri paramUri)
  {
    this.zzbqr = paramContentResolver;
    this.uri = paramUri;
    this.zzbqr.registerContentObserver(paramUri, false, new zzsj(this, null));
  }
  
  public static zzsi zza(ContentResolver paramContentResolver, Uri paramUri)
  {
    synchronized (zzbqp)
    {
      zzsi localzzsi2 = (zzsi)zzbqq.get(paramUri);
      zzsi localzzsi1 = localzzsi2;
      if (localzzsi2 == null)
      {
        localzzsi1 = new zzsi(paramContentResolver, paramUri);
        zzbqq.put(paramUri, localzzsi1);
      }
      return localzzsi1;
    }
  }
  
  /* Error */
  private final Map<String, String> zztb()
  {
    // Byte code:
    //   0: new 35	java/util/HashMap
    //   3: dup
    //   4: invokespecial 36	java/util/HashMap:<init>	()V
    //   7: astore_2
    //   8: aload_0
    //   9: getfield 58	com/google/android/gms/internal/measurement/zzsi:zzbqr	Landroid/content/ContentResolver;
    //   12: aload_0
    //   13: getfield 60	com/google/android/gms/internal/measurement/zzsi:uri	Landroid/net/Uri;
    //   16: getstatic 45	com/google/android/gms/internal/measurement/zzsi:zzbqw	[Ljava/lang/String;
    //   19: aconst_null
    //   20: aconst_null
    //   21: aconst_null
    //   22: invokevirtual 99	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   25: astore_1
    //   26: aload_1
    //   27: ifnull +62 -> 89
    //   30: aload_1
    //   31: invokeinterface 105 1 0
    //   36: ifeq +47 -> 83
    //   39: aload_2
    //   40: aload_1
    //   41: iconst_0
    //   42: invokeinterface 109 2 0
    //   47: aload_1
    //   48: iconst_1
    //   49: invokeinterface 109 2 0
    //   54: invokeinterface 85 3 0
    //   59: pop
    //   60: goto -30 -> 30
    //   63: astore_2
    //   64: aload_1
    //   65: invokeinterface 112 1 0
    //   70: aload_2
    //   71: athrow
    //   72: astore_1
    //   73: ldc 114
    //   75: ldc 116
    //   77: invokestatic 122	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   80: pop
    //   81: aconst_null
    //   82: areturn
    //   83: aload_1
    //   84: invokeinterface 112 1 0
    //   89: aload_2
    //   90: areturn
    //   91: astore_1
    //   92: goto -19 -> 73
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	95	0	this	zzsi
    //   25	40	1	localCursor	android.database.Cursor
    //   72	12	1	localSecurityException	SecurityException
    //   91	1	1	localSQLiteException	android.database.sqlite.SQLiteException
    //   7	33	2	localHashMap	HashMap
    //   63	27	2	localMap	Map<String, String>
    // Exception table:
    //   from	to	target	type
    //   30	60	63	finally
    //   0	26	72	java/lang/SecurityException
    //   64	72	72	java/lang/SecurityException
    //   83	89	72	java/lang/SecurityException
    //   0	26	91	android/database/sqlite/SQLiteException
    //   64	72	91	android/database/sqlite/SQLiteException
    //   83	89	91	android/database/sqlite/SQLiteException
  }
  
  private final void zztc()
  {
    synchronized (this.zzbqu)
    {
      Iterator localIterator = this.zzbqv.iterator();
      if (localIterator.hasNext()) {
        ((zzsk)localIterator.next()).zztd();
      }
    }
  }
  
  public final Map<String, String> zzsz()
  {
    Object localObject1;
    if (zzsl.zzd("gms:phenotype:phenotype_flag:debug_disable_caching", false)) {
      localObject1 = zztb();
    }
    for (;;)
    {
      Object localObject3 = localObject1;
      if (localObject1 == null) {}
      synchronized (this.zzbqs)
      {
        localObject3 = this.zzbqt;
        localObject1 = localObject3;
        if (localObject3 == null)
        {
          localObject1 = zztb();
          this.zzbqt = ((Map)localObject1);
        }
        localObject3 = localObject1;
        if (localObject3 != null)
        {
          return (Map<String, String>)localObject3;
          localObject1 = this.zzbqt;
        }
      }
    }
    return Collections.emptyMap();
  }
  
  public final void zzta()
  {
    synchronized (this.zzbqs)
    {
      this.zzbqt = null;
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */