package com.google.android.gms.internal.measurement;

final class zzwj
  implements zzws
{
  public final boolean zze(Class<?> paramClass)
  {
    return false;
  }
  
  public final zzwr zzf(Class<?> paramClass)
  {
    throw new IllegalStateException("This should never be called.");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */