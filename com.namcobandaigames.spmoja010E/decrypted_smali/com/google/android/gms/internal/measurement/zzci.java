package com.google.android.gms.internal.measurement;

final class zzci
  implements zzbw<zzcj>
{
  private final zzcj zzaap;
  private final zzaw zzvy;
  
  public zzci(zzaw paramzzaw)
  {
    this.zzvy = paramzzaw;
    this.zzaap = new zzcj();
  }
  
  public final void zza(String paramString, boolean paramBoolean)
  {
    if ("ga_dryRun".equals(paramString))
    {
      paramString = this.zzaap;
      if (paramBoolean) {}
      for (int i = 1;; i = 0)
      {
        paramString.zzaau = i;
        return;
      }
    }
    this.zzvy.zzby().zzd("Bool xml configuration name not recognized", paramString);
  }
  
  public final void zzb(String paramString, int paramInt)
  {
    if ("ga_dispatchPeriod".equals(paramString))
    {
      this.zzaap.zzaat = paramInt;
      return;
    }
    this.zzvy.zzby().zzd("Int xml configuration name not recognized", paramString);
  }
  
  public final void zzb(String paramString1, String paramString2) {}
  
  public final void zzc(String paramString1, String paramString2)
  {
    if ("ga_appName".equals(paramString1))
    {
      this.zzaap.zzaaq = paramString2;
      return;
    }
    if ("ga_appVersion".equals(paramString1))
    {
      this.zzaap.zzaar = paramString2;
      return;
    }
    if ("ga_logLevel".equals(paramString1))
    {
      this.zzaap.zzaas = paramString2;
      return;
    }
    this.zzvy.zzby().zzd("String xml configuration name not recognized", paramString1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzci.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */