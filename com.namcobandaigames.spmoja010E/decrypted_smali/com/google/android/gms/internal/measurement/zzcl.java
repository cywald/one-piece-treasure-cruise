package com.google.android.gms.internal.measurement;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;
import java.util.Map;

public abstract interface zzcl
  extends IInterface
{
  public abstract void zza(Map paramMap, long paramLong, String paramString, List<zzbr> paramList)
    throws RemoteException;
  
  public abstract void zzbr()
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */