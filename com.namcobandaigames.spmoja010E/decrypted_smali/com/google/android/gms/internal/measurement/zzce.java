package com.google.android.gms.internal.measurement;

import android.os.Build.VERSION;

public final class zzce
{
  public static int version()
  {
    try
    {
      int i = Integer.parseInt(Build.VERSION.SDK);
      return i;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      zzco.zzf("Invalid version number", Build.VERSION.SDK);
    }
    return 0;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzce.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */