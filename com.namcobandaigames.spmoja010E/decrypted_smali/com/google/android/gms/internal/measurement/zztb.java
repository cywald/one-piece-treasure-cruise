package com.google.android.gms.internal.measurement;

import java.io.PrintStream;

final class zztb
  extends zzsx
{
  public final void zza(Throwable paramThrowable, PrintStream paramPrintStream)
  {
    paramThrowable.printStackTrace(paramPrintStream);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zztb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */