package com.google.android.gms.internal.measurement;

public abstract interface zzvf<T extends zzvf<T>>
  extends Comparable<T>
{
  public abstract zzwu zza(zzwu paramzzwu, zzwt paramzzwt);
  
  public abstract zzwz zza(zzwz paramzzwz1, zzwz paramzzwz2);
  
  public abstract int zzc();
  
  public abstract zzyq zzvw();
  
  public abstract zzyv zzvx();
  
  public abstract boolean zzvy();
  
  public abstract boolean zzvz();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */