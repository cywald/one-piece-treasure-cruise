package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Arrays;

public final class zzyc
{
  private static final zzyc zzcco = new zzyc(0, new int[0], new Object[0], false);
  private int count;
  private boolean zzbtu;
  private int zzbyn = -1;
  private Object[] zzcba;
  private int[] zzccp;
  
  private zzyc()
  {
    this(0, new int[8], new Object[8], true);
  }
  
  private zzyc(int paramInt, int[] paramArrayOfInt, Object[] paramArrayOfObject, boolean paramBoolean)
  {
    this.count = paramInt;
    this.zzccp = paramArrayOfInt;
    this.zzcba = paramArrayOfObject;
    this.zzbtu = paramBoolean;
  }
  
  static zzyc zza(zzyc paramzzyc1, zzyc paramzzyc2)
  {
    int i = paramzzyc1.count + paramzzyc2.count;
    int[] arrayOfInt = Arrays.copyOf(paramzzyc1.zzccp, i);
    System.arraycopy(paramzzyc2.zzccp, 0, arrayOfInt, paramzzyc1.count, paramzzyc2.count);
    Object[] arrayOfObject = Arrays.copyOf(paramzzyc1.zzcba, i);
    System.arraycopy(paramzzyc2.zzcba, 0, arrayOfObject, paramzzyc1.count, paramzzyc2.count);
    return new zzyc(i, arrayOfInt, arrayOfObject, true);
  }
  
  private static void zzb(int paramInt, Object paramObject, zzyw paramzzyw)
    throws IOException
  {
    int i = paramInt >>> 3;
    switch (paramInt & 0x7)
    {
    case 4: 
    default: 
      throw new RuntimeException(zzvt.zzwo());
    case 0: 
      paramzzyw.zzi(i, ((Long)paramObject).longValue());
      return;
    case 5: 
      paramzzyw.zzg(i, ((Integer)paramObject).intValue());
      return;
    case 1: 
      paramzzyw.zzc(i, ((Long)paramObject).longValue());
      return;
    case 2: 
      paramzzyw.zza(i, (zzud)paramObject);
      return;
    }
    if (paramzzyw.zzvj() == zzvm.zze.zzbze)
    {
      paramzzyw.zzbk(i);
      ((zzyc)paramObject).zzb(paramzzyw);
      paramzzyw.zzbl(i);
      return;
    }
    paramzzyw.zzbl(i);
    ((zzyc)paramObject).zzb(paramzzyw);
    paramzzyw.zzbk(i);
  }
  
  public static zzyc zzyf()
  {
    return zzcco;
  }
  
  static zzyc zzyg()
  {
    return new zzyc();
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    label75:
    label97:
    label123:
    label130:
    label135:
    label142:
    label145:
    for (;;)
    {
      return true;
      if (paramObject == null) {
        return false;
      }
      if (!(paramObject instanceof zzyc)) {
        return false;
      }
      paramObject = (zzyc)paramObject;
      if (this.count == ((zzyc)paramObject).count)
      {
        Object localObject = this.zzccp;
        int[] arrayOfInt = ((zzyc)paramObject).zzccp;
        int j = this.count;
        i = 0;
        if (i >= j) {
          break label130;
        }
        if (localObject[i] == arrayOfInt[i]) {
          break label123;
        }
        i = 0;
        if (i != 0)
        {
          localObject = this.zzcba;
          paramObject = ((zzyc)paramObject).zzcba;
          j = this.count;
          i = 0;
          if (i >= j) {
            break label142;
          }
          if (localObject[i].equals(paramObject[i])) {
            break label135;
          }
        }
      }
      for (int i = 0;; i = 1)
      {
        if (i != 0) {
          break label145;
        }
        return false;
        i += 1;
        break;
        i = 1;
        break label75;
        i += 1;
        break label97;
      }
    }
  }
  
  public final int hashCode()
  {
    int k = 17;
    int m = 0;
    int n = this.count;
    Object localObject = this.zzccp;
    int i1 = this.count;
    int j = 0;
    int i = 17;
    while (j < i1)
    {
      i = i * 31 + localObject[j];
      j += 1;
    }
    localObject = this.zzcba;
    i1 = this.count;
    j = m;
    while (j < i1)
    {
      k = k * 31 + localObject[j].hashCode();
      j += 1;
    }
    return ((n + 527) * 31 + i) * 31 + k;
  }
  
  final void zza(zzyw paramzzyw)
    throws IOException
  {
    if (paramzzyw.zzvj() == zzvm.zze.zzbzf)
    {
      i = this.count - 1;
      while (i >= 0)
      {
        paramzzyw.zza(this.zzccp[i] >>> 3, this.zzcba[i]);
        i -= 1;
      }
    }
    int i = 0;
    while (i < this.count)
    {
      paramzzyw.zza(this.zzccp[i] >>> 3, this.zzcba[i]);
      i += 1;
    }
  }
  
  final void zzb(int paramInt, Object paramObject)
  {
    if (!this.zzbtu) {
      throw new UnsupportedOperationException();
    }
    if (this.count == this.zzccp.length) {
      if (this.count >= 4) {
        break label100;
      }
    }
    label100:
    for (int i = 8;; i = this.count >> 1)
    {
      i += this.count;
      this.zzccp = Arrays.copyOf(this.zzccp, i);
      this.zzcba = Arrays.copyOf(this.zzcba, i);
      this.zzccp[this.count] = paramInt;
      this.zzcba[this.count] = paramObject;
      this.count += 1;
      return;
    }
  }
  
  public final void zzb(zzyw paramzzyw)
    throws IOException
  {
    if (this.count == 0) {}
    for (;;)
    {
      return;
      int i;
      if (paramzzyw.zzvj() == zzvm.zze.zzbze)
      {
        i = 0;
        while (i < this.count)
        {
          zzb(this.zzccp[i], this.zzcba[i], paramzzyw);
          i += 1;
        }
      }
      else
      {
        i = this.count - 1;
        while (i >= 0)
        {
          zzb(this.zzccp[i], this.zzcba[i], paramzzyw);
          i -= 1;
        }
      }
    }
  }
  
  final void zzb(StringBuilder paramStringBuilder, int paramInt)
  {
    int i = 0;
    while (i < this.count)
    {
      zzww.zzb(paramStringBuilder, paramInt, String.valueOf(this.zzccp[i] >>> 3), this.zzcba[i]);
      i += 1;
    }
  }
  
  public final void zzsm()
  {
    this.zzbtu = false;
  }
  
  public final int zzvu()
  {
    int i = this.zzbyn;
    if (i != -1) {
      return i;
    }
    int j = 0;
    i = 0;
    if (j < this.count)
    {
      int k = this.zzccp[j];
      int m = k >>> 3;
      switch (k & 0x7)
      {
      case 4: 
      default: 
        throw new IllegalStateException(zzvt.zzwo());
      case 0: 
        i += zzut.zze(m, ((Long)this.zzcba[j]).longValue());
      }
      for (;;)
      {
        j += 1;
        break;
        i += zzut.zzk(m, ((Integer)this.zzcba[j]).intValue());
        continue;
        i += zzut.zzg(m, ((Long)this.zzcba[j]).longValue());
        continue;
        i += zzut.zzc(m, (zzud)this.zzcba[j]);
        continue;
        k = zzut.zzbb(m);
        i += ((zzyc)this.zzcba[j]).zzvu() + (k << 1);
      }
    }
    this.zzbyn = i;
    return i;
  }
  
  public final int zzyh()
  {
    int i = this.zzbyn;
    if (i != -1) {
      return i;
    }
    i = 0;
    int j = 0;
    while (i < this.count)
    {
      j += zzut.zzd(this.zzccp[i] >>> 3, (zzud)this.zzcba[i]);
      i += 1;
    }
    this.zzbyn = j;
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */