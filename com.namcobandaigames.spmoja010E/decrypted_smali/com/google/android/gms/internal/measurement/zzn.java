package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzn
  extends zzza<zzn>
{
  private static volatile zzn[] zzqc;
  public String name = "";
  private zzp zzqd = null;
  public zzj zzqe = null;
  
  public zzn()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzn[] zzj()
  {
    if (zzqc == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzqc == null) {
        zzqc = new zzn[0];
      }
      return zzqc;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzn)) {
        return false;
      }
      paramObject = (zzn)paramObject;
      if (this.name == null)
      {
        if (((zzn)paramObject).name != null) {
          return false;
        }
      }
      else if (!this.name.equals(((zzn)paramObject).name)) {
        return false;
      }
      if (this.zzqd == null)
      {
        if (((zzn)paramObject).zzqd != null) {
          return false;
        }
      }
      else if (!this.zzqd.equals(((zzn)paramObject).zzqd)) {
        return false;
      }
      if (this.zzqe == null)
      {
        if (((zzn)paramObject).zzqe != null) {
          return false;
        }
      }
      else if (!this.zzqe.equals(((zzn)paramObject).zzqe)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzn)paramObject).zzcfc == null) || (((zzn)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzn)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int n = 0;
    int i1 = getClass().getName().hashCode();
    int i;
    Object localObject;
    int j;
    label37:
    int k;
    if (this.name == null)
    {
      i = 0;
      localObject = this.zzqd;
      if (localObject != null) {
        break label114;
      }
      j = 0;
      localObject = this.zzqe;
      if (localObject != null) {
        break label123;
      }
      k = 0;
      label50:
      m = n;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label132;
        }
      }
    }
    label114:
    label123:
    label132:
    for (int m = n;; m = this.zzcfc.hashCode())
    {
      return (k + (j + (i + (i1 + 527) * 31) * 31) * 31) * 31 + m;
      i = this.name.hashCode();
      break;
      j = ((zzp)localObject).hashCode();
      break label37;
      k = ((zzj)localObject).hashCode();
      break label50;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if ((this.name != null) && (!this.name.equals(""))) {
      paramzzyy.zzb(1, this.name);
    }
    if (this.zzqd != null) {
      paramzzyy.zza(2, this.zzqd);
    }
    if (this.zzqe != null) {
      paramzzyy.zza(3, this.zzqe);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.name != null)
    {
      i = j;
      if (!this.name.equals("")) {
        i = j + zzyy.zzc(1, this.name);
      }
    }
    j = i;
    if (this.zzqd != null) {
      j = i + zzyy.zzb(2, this.zzqd);
    }
    i = j;
    if (this.zzqe != null) {
      i = j + zzyy.zzb(3, this.zzqe);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */