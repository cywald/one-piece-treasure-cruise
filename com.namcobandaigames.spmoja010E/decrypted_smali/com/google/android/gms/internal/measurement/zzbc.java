package com.google.android.gms.internal.measurement;

import android.content.ComponentName;
import android.content.ServiceConnection;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzbc
  implements ServiceConnection
{
  private volatile zzcl zzxb;
  private volatile boolean zzxc;
  
  protected zzbc(zzba paramzzba) {}
  
  /* Error */
  public final void onServiceConnected(ComponentName paramComponentName, android.os.IBinder paramIBinder)
  {
    // Byte code:
    //   0: ldc 29
    //   2: invokestatic 35	com/google/android/gms/common/internal/Preconditions:checkMainThread	(Ljava/lang/String;)V
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_2
    //   8: ifnonnull +19 -> 27
    //   11: aload_0
    //   12: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   15: ldc 37
    //   17: invokevirtual 42	com/google/android/gms/internal/measurement/zzat:zzu	(Ljava/lang/String;)V
    //   20: aload_0
    //   21: invokevirtual 45	java/lang/Object:notifyAll	()V
    //   24: aload_0
    //   25: monitorexit
    //   26: return
    //   27: aload_2
    //   28: invokeinterface 51 1 0
    //   33: astore_1
    //   34: ldc 53
    //   36: aload_1
    //   37: invokevirtual 59	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   40: istore_3
    //   41: iload_3
    //   42: ifeq +112 -> 154
    //   45: aload_2
    //   46: ifnonnull +50 -> 96
    //   49: aconst_null
    //   50: astore_1
    //   51: aload_0
    //   52: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   55: ldc 61
    //   57: invokevirtual 64	com/google/android/gms/internal/measurement/zzat:zzq	(Ljava/lang/String;)V
    //   60: aload_1
    //   61: ifnonnull +108 -> 169
    //   64: invokestatic 70	com/google/android/gms/common/stats/ConnectionTracker:getInstance	()Lcom/google/android/gms/common/stats/ConnectionTracker;
    //   67: aload_0
    //   68: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   71: invokevirtual 74	com/google/android/gms/internal/measurement/zzat:getContext	()Landroid/content/Context;
    //   74: aload_0
    //   75: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   78: invokestatic 80	com/google/android/gms/internal/measurement/zzba:zza	(Lcom/google/android/gms/internal/measurement/zzba;)Lcom/google/android/gms/internal/measurement/zzbc;
    //   81: invokevirtual 84	com/google/android/gms/common/stats/ConnectionTracker:unbindService	(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    //   84: aload_0
    //   85: invokevirtual 45	java/lang/Object:notifyAll	()V
    //   88: aload_0
    //   89: monitorexit
    //   90: return
    //   91: astore_1
    //   92: aload_0
    //   93: monitorexit
    //   94: aload_1
    //   95: athrow
    //   96: aload_2
    //   97: ldc 53
    //   99: invokeinterface 88 2 0
    //   104: astore_1
    //   105: aload_1
    //   106: instanceof 90
    //   109: ifeq +11 -> 120
    //   112: aload_1
    //   113: checkcast 90	com/google/android/gms/internal/measurement/zzcl
    //   116: astore_1
    //   117: goto -66 -> 51
    //   120: new 92	com/google/android/gms/internal/measurement/zzcm
    //   123: dup
    //   124: aload_2
    //   125: invokespecial 95	com/google/android/gms/internal/measurement/zzcm:<init>	(Landroid/os/IBinder;)V
    //   128: astore_1
    //   129: goto -78 -> 51
    //   132: astore_1
    //   133: aconst_null
    //   134: astore_1
    //   135: aload_0
    //   136: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   139: ldc 97
    //   141: invokevirtual 42	com/google/android/gms/internal/measurement/zzat:zzu	(Ljava/lang/String;)V
    //   144: goto -84 -> 60
    //   147: astore_1
    //   148: aload_0
    //   149: invokevirtual 45	java/lang/Object:notifyAll	()V
    //   152: aload_1
    //   153: athrow
    //   154: aload_0
    //   155: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   158: ldc 99
    //   160: aload_1
    //   161: invokevirtual 103	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   164: aconst_null
    //   165: astore_1
    //   166: goto -106 -> 60
    //   169: aload_0
    //   170: getfield 105	com/google/android/gms/internal/measurement/zzbc:zzxc	Z
    //   173: ifne +34 -> 207
    //   176: aload_0
    //   177: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   180: ldc 107
    //   182: invokevirtual 110	com/google/android/gms/internal/measurement/zzat:zzt	(Ljava/lang/String;)V
    //   185: aload_0
    //   186: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   189: invokevirtual 114	com/google/android/gms/internal/measurement/zzat:zzca	()Lcom/google/android/gms/analytics/zzk;
    //   192: new 116	com/google/android/gms/internal/measurement/zzbd
    //   195: dup
    //   196: aload_0
    //   197: aload_1
    //   198: invokespecial 119	com/google/android/gms/internal/measurement/zzbd:<init>	(Lcom/google/android/gms/internal/measurement/zzbc;Lcom/google/android/gms/internal/measurement/zzcl;)V
    //   201: invokevirtual 124	com/google/android/gms/analytics/zzk:zza	(Ljava/lang/Runnable;)V
    //   204: goto -120 -> 84
    //   207: aload_0
    //   208: aload_1
    //   209: putfield 126	com/google/android/gms/internal/measurement/zzbc:zzxb	Lcom/google/android/gms/internal/measurement/zzcl;
    //   212: goto -128 -> 84
    //   215: astore_1
    //   216: goto -132 -> 84
    //   219: astore_2
    //   220: goto -85 -> 135
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	223	0	this	zzbc
    //   0	223	1	paramComponentName	ComponentName
    //   0	223	2	paramIBinder	android.os.IBinder
    //   40	2	3	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   20	26	91	finally
    //   84	90	91	finally
    //   92	94	91	finally
    //   148	154	91	finally
    //   27	41	132	android/os/RemoteException
    //   96	117	132	android/os/RemoteException
    //   120	129	132	android/os/RemoteException
    //   154	164	132	android/os/RemoteException
    //   11	20	147	finally
    //   27	41	147	finally
    //   51	60	147	finally
    //   64	84	147	finally
    //   96	117	147	finally
    //   120	129	147	finally
    //   135	144	147	finally
    //   154	164	147	finally
    //   169	204	147	finally
    //   207	212	147	finally
    //   64	84	215	java/lang/IllegalArgumentException
    //   51	60	219	android/os/RemoteException
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    Preconditions.checkMainThread("AnalyticsServiceConnection.onServiceDisconnected");
    this.zzxa.zzca().zza(new zzbe(this, paramComponentName));
  }
  
  /* Error */
  public final zzcl zzda()
  {
    // Byte code:
    //   0: invokestatic 142	com/google/android/gms/analytics/zzk:zzaf	()V
    //   3: new 144	android/content/Intent
    //   6: dup
    //   7: ldc -110
    //   9: invokespecial 148	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   12: astore_2
    //   13: aload_2
    //   14: new 150	android/content/ComponentName
    //   17: dup
    //   18: ldc -104
    //   20: ldc -102
    //   22: invokespecial 157	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   25: invokevirtual 161	android/content/Intent:setComponent	(Landroid/content/ComponentName;)Landroid/content/Intent;
    //   28: pop
    //   29: aload_0
    //   30: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   33: invokevirtual 74	com/google/android/gms/internal/measurement/zzat:getContext	()Landroid/content/Context;
    //   36: astore_3
    //   37: aload_2
    //   38: ldc -93
    //   40: aload_3
    //   41: invokevirtual 168	android/content/Context:getPackageName	()Ljava/lang/String;
    //   44: invokevirtual 172	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   47: pop
    //   48: invokestatic 70	com/google/android/gms/common/stats/ConnectionTracker:getInstance	()Lcom/google/android/gms/common/stats/ConnectionTracker;
    //   51: astore 4
    //   53: aload_0
    //   54: monitorenter
    //   55: aload_0
    //   56: aconst_null
    //   57: putfield 126	com/google/android/gms/internal/measurement/zzbc:zzxb	Lcom/google/android/gms/internal/measurement/zzcl;
    //   60: aload_0
    //   61: iconst_1
    //   62: putfield 105	com/google/android/gms/internal/measurement/zzbc:zzxc	Z
    //   65: aload 4
    //   67: aload_3
    //   68: aload_2
    //   69: aload_0
    //   70: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   73: invokestatic 80	com/google/android/gms/internal/measurement/zzba:zza	(Lcom/google/android/gms/internal/measurement/zzba;)Lcom/google/android/gms/internal/measurement/zzbc;
    //   76: sipush 129
    //   79: invokevirtual 176	com/google/android/gms/common/stats/ConnectionTracker:bindService	(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    //   82: istore_1
    //   83: aload_0
    //   84: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   87: ldc -78
    //   89: iload_1
    //   90: invokestatic 184	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   93: invokevirtual 186	com/google/android/gms/internal/measurement/zzat:zza	(Ljava/lang/String;Ljava/lang/Object;)V
    //   96: iload_1
    //   97: ifne +12 -> 109
    //   100: aload_0
    //   101: iconst_0
    //   102: putfield 105	com/google/android/gms/internal/measurement/zzbc:zzxc	Z
    //   105: aload_0
    //   106: monitorexit
    //   107: aconst_null
    //   108: areturn
    //   109: aload_0
    //   110: getstatic 192	com/google/android/gms/internal/measurement/zzcf:zzaag	Lcom/google/android/gms/internal/measurement/zzcg;
    //   113: invokevirtual 198	com/google/android/gms/internal/measurement/zzcg:get	()Ljava/lang/Object;
    //   116: checkcast 200	java/lang/Long
    //   119: invokevirtual 204	java/lang/Long:longValue	()J
    //   122: invokevirtual 208	java/lang/Object:wait	(J)V
    //   125: aload_0
    //   126: iconst_0
    //   127: putfield 105	com/google/android/gms/internal/measurement/zzbc:zzxc	Z
    //   130: aload_0
    //   131: getfield 126	com/google/android/gms/internal/measurement/zzbc:zzxb	Lcom/google/android/gms/internal/measurement/zzcl;
    //   134: astore_2
    //   135: aload_0
    //   136: aconst_null
    //   137: putfield 126	com/google/android/gms/internal/measurement/zzbc:zzxb	Lcom/google/android/gms/internal/measurement/zzcl;
    //   140: aload_2
    //   141: ifnonnull +12 -> 153
    //   144: aload_0
    //   145: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   148: ldc -46
    //   150: invokevirtual 42	com/google/android/gms/internal/measurement/zzat:zzu	(Ljava/lang/String;)V
    //   153: aload_0
    //   154: monitorexit
    //   155: aload_2
    //   156: areturn
    //   157: astore_2
    //   158: aload_0
    //   159: monitorexit
    //   160: aload_2
    //   161: athrow
    //   162: astore_2
    //   163: aload_0
    //   164: getfield 17	com/google/android/gms/internal/measurement/zzbc:zzxa	Lcom/google/android/gms/internal/measurement/zzba;
    //   167: ldc -44
    //   169: invokevirtual 110	com/google/android/gms/internal/measurement/zzat:zzt	(Ljava/lang/String;)V
    //   172: goto -47 -> 125
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	175	0	this	zzbc
    //   82	15	1	bool	boolean
    //   12	144	2	localObject1	Object
    //   157	4	2	localObject2	Object
    //   162	1	2	localInterruptedException	InterruptedException
    //   36	32	3	localContext	android.content.Context
    //   51	15	4	localConnectionTracker	com.google.android.gms.common.stats.ConnectionTracker
    // Exception table:
    //   from	to	target	type
    //   55	96	157	finally
    //   100	107	157	finally
    //   109	125	157	finally
    //   125	140	157	finally
    //   144	153	157	finally
    //   153	155	157	finally
    //   158	160	157	finally
    //   163	172	157	finally
    //   109	125	162	java/lang/InterruptedException
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */