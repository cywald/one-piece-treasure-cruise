package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Map.Entry;

abstract class zzva<T extends zzvf<T>>
{
  abstract Object zza(zzuz paramzzuz, zzwt paramzzwt, int paramInt);
  
  abstract <UT, UB> UB zza(zzxi paramzzxi, Object paramObject, zzuz paramzzuz, zzvd<T> paramzzvd, UB paramUB, zzyb<UT, UB> paramzzyb)
    throws IOException;
  
  abstract void zza(zzud paramzzud, Object paramObject, zzuz paramzzuz, zzvd<T> paramzzvd)
    throws IOException;
  
  abstract void zza(zzxi paramzzxi, Object paramObject, zzuz paramzzuz, zzvd<T> paramzzvd)
    throws IOException;
  
  abstract void zza(zzyw paramzzyw, Map.Entry<?, ?> paramEntry)
    throws IOException;
  
  abstract void zza(Object paramObject, zzvd<T> paramzzvd);
  
  abstract int zzb(Map.Entry<?, ?> paramEntry);
  
  abstract boolean zze(zzwt paramzzwt);
  
  abstract zzvd<T> zzs(Object paramObject);
  
  abstract zzvd<T> zzt(Object paramObject);
  
  abstract void zzu(Object paramObject);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzva.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */