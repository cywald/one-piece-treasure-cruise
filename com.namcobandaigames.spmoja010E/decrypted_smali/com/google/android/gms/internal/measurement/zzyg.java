package com.google.android.gms.internal.measurement;

import java.util.Iterator;

final class zzyg
  implements Iterator<String>
{
  private Iterator<String> zzccu = zzye.zza(this.zzcct).iterator();
  
  zzyg(zzye paramzzye) {}
  
  public final boolean hasNext()
  {
    return this.zzccu.hasNext();
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */