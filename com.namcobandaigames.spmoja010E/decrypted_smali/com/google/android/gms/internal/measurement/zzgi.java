package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgi
  extends zzza<zzgi>
{
  private static volatile zzgi[] zzawz;
  public String zzafw = null;
  public String zzafx = null;
  public String zzafz = null;
  public String zzage = null;
  public String zzagv = null;
  public String zzaia = null;
  public String zzawj = null;
  public Integer zzaxa = null;
  public zzgf[] zzaxb = zzgf.zzmq();
  public zzgl[] zzaxc = zzgl.zzmu();
  public Long zzaxd = null;
  public Long zzaxe = null;
  public Long zzaxf = null;
  public Long zzaxg = null;
  public Long zzaxh = null;
  public String zzaxi = null;
  public String zzaxj = null;
  public String zzaxk = null;
  public Integer zzaxl = null;
  public Long zzaxm = null;
  public Long zzaxn = null;
  public String zzaxo = null;
  public Boolean zzaxp = null;
  public Long zzaxq = null;
  public Integer zzaxr = null;
  public Boolean zzaxs = null;
  public zzgd[] zzaxt = zzgd.zzmo();
  public Integer zzaxu = null;
  private Integer zzaxv = null;
  private Integer zzaxw = null;
  public String zzaxx = null;
  public Long zzaxy = null;
  public Long zzaxz = null;
  public String zzaya = null;
  private String zzayb = null;
  public Integer zzayc = null;
  private zzfq.zzb zzayd = null;
  public String zzts = null;
  public String zztt = null;
  
  public zzgi()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzgi[] zzms()
  {
    if (zzawz == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzawz == null) {
        zzawz = new zzgi[0];
      }
      return zzawz;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgi)) {
        return false;
      }
      paramObject = (zzgi)paramObject;
      if (this.zzaxa == null)
      {
        if (((zzgi)paramObject).zzaxa != null) {
          return false;
        }
      }
      else if (!this.zzaxa.equals(((zzgi)paramObject).zzaxa)) {
        return false;
      }
      if (!zzze.equals(this.zzaxb, ((zzgi)paramObject).zzaxb)) {
        return false;
      }
      if (!zzze.equals(this.zzaxc, ((zzgi)paramObject).zzaxc)) {
        return false;
      }
      if (this.zzaxd == null)
      {
        if (((zzgi)paramObject).zzaxd != null) {
          return false;
        }
      }
      else if (!this.zzaxd.equals(((zzgi)paramObject).zzaxd)) {
        return false;
      }
      if (this.zzaxe == null)
      {
        if (((zzgi)paramObject).zzaxe != null) {
          return false;
        }
      }
      else if (!this.zzaxe.equals(((zzgi)paramObject).zzaxe)) {
        return false;
      }
      if (this.zzaxf == null)
      {
        if (((zzgi)paramObject).zzaxf != null) {
          return false;
        }
      }
      else if (!this.zzaxf.equals(((zzgi)paramObject).zzaxf)) {
        return false;
      }
      if (this.zzaxg == null)
      {
        if (((zzgi)paramObject).zzaxg != null) {
          return false;
        }
      }
      else if (!this.zzaxg.equals(((zzgi)paramObject).zzaxg)) {
        return false;
      }
      if (this.zzaxh == null)
      {
        if (((zzgi)paramObject).zzaxh != null) {
          return false;
        }
      }
      else if (!this.zzaxh.equals(((zzgi)paramObject).zzaxh)) {
        return false;
      }
      if (this.zzaxi == null)
      {
        if (((zzgi)paramObject).zzaxi != null) {
          return false;
        }
      }
      else if (!this.zzaxi.equals(((zzgi)paramObject).zzaxi)) {
        return false;
      }
      if (this.zzaxj == null)
      {
        if (((zzgi)paramObject).zzaxj != null) {
          return false;
        }
      }
      else if (!this.zzaxj.equals(((zzgi)paramObject).zzaxj)) {
        return false;
      }
      if (this.zzaxk == null)
      {
        if (((zzgi)paramObject).zzaxk != null) {
          return false;
        }
      }
      else if (!this.zzaxk.equals(((zzgi)paramObject).zzaxk)) {
        return false;
      }
      if (this.zzaia == null)
      {
        if (((zzgi)paramObject).zzaia != null) {
          return false;
        }
      }
      else if (!this.zzaia.equals(((zzgi)paramObject).zzaia)) {
        return false;
      }
      if (this.zzaxl == null)
      {
        if (((zzgi)paramObject).zzaxl != null) {
          return false;
        }
      }
      else if (!this.zzaxl.equals(((zzgi)paramObject).zzaxl)) {
        return false;
      }
      if (this.zzage == null)
      {
        if (((zzgi)paramObject).zzage != null) {
          return false;
        }
      }
      else if (!this.zzage.equals(((zzgi)paramObject).zzage)) {
        return false;
      }
      if (this.zztt == null)
      {
        if (((zzgi)paramObject).zztt != null) {
          return false;
        }
      }
      else if (!this.zztt.equals(((zzgi)paramObject).zztt)) {
        return false;
      }
      if (this.zzts == null)
      {
        if (((zzgi)paramObject).zzts != null) {
          return false;
        }
      }
      else if (!this.zzts.equals(((zzgi)paramObject).zzts)) {
        return false;
      }
      if (this.zzaxm == null)
      {
        if (((zzgi)paramObject).zzaxm != null) {
          return false;
        }
      }
      else if (!this.zzaxm.equals(((zzgi)paramObject).zzaxm)) {
        return false;
      }
      if (this.zzaxn == null)
      {
        if (((zzgi)paramObject).zzaxn != null) {
          return false;
        }
      }
      else if (!this.zzaxn.equals(((zzgi)paramObject).zzaxn)) {
        return false;
      }
      if (this.zzaxo == null)
      {
        if (((zzgi)paramObject).zzaxo != null) {
          return false;
        }
      }
      else if (!this.zzaxo.equals(((zzgi)paramObject).zzaxo)) {
        return false;
      }
      if (this.zzaxp == null)
      {
        if (((zzgi)paramObject).zzaxp != null) {
          return false;
        }
      }
      else if (!this.zzaxp.equals(((zzgi)paramObject).zzaxp)) {
        return false;
      }
      if (this.zzafw == null)
      {
        if (((zzgi)paramObject).zzafw != null) {
          return false;
        }
      }
      else if (!this.zzafw.equals(((zzgi)paramObject).zzafw)) {
        return false;
      }
      if (this.zzaxq == null)
      {
        if (((zzgi)paramObject).zzaxq != null) {
          return false;
        }
      }
      else if (!this.zzaxq.equals(((zzgi)paramObject).zzaxq)) {
        return false;
      }
      if (this.zzaxr == null)
      {
        if (((zzgi)paramObject).zzaxr != null) {
          return false;
        }
      }
      else if (!this.zzaxr.equals(((zzgi)paramObject).zzaxr)) {
        return false;
      }
      if (this.zzagv == null)
      {
        if (((zzgi)paramObject).zzagv != null) {
          return false;
        }
      }
      else if (!this.zzagv.equals(((zzgi)paramObject).zzagv)) {
        return false;
      }
      if (this.zzafx == null)
      {
        if (((zzgi)paramObject).zzafx != null) {
          return false;
        }
      }
      else if (!this.zzafx.equals(((zzgi)paramObject).zzafx)) {
        return false;
      }
      if (this.zzaxs == null)
      {
        if (((zzgi)paramObject).zzaxs != null) {
          return false;
        }
      }
      else if (!this.zzaxs.equals(((zzgi)paramObject).zzaxs)) {
        return false;
      }
      if (!zzze.equals(this.zzaxt, ((zzgi)paramObject).zzaxt)) {
        return false;
      }
      if (this.zzafz == null)
      {
        if (((zzgi)paramObject).zzafz != null) {
          return false;
        }
      }
      else if (!this.zzafz.equals(((zzgi)paramObject).zzafz)) {
        return false;
      }
      if (this.zzaxu == null)
      {
        if (((zzgi)paramObject).zzaxu != null) {
          return false;
        }
      }
      else if (!this.zzaxu.equals(((zzgi)paramObject).zzaxu)) {
        return false;
      }
      if (this.zzaxv == null)
      {
        if (((zzgi)paramObject).zzaxv != null) {
          return false;
        }
      }
      else if (!this.zzaxv.equals(((zzgi)paramObject).zzaxv)) {
        return false;
      }
      if (this.zzaxw == null)
      {
        if (((zzgi)paramObject).zzaxw != null) {
          return false;
        }
      }
      else if (!this.zzaxw.equals(((zzgi)paramObject).zzaxw)) {
        return false;
      }
      if (this.zzaxx == null)
      {
        if (((zzgi)paramObject).zzaxx != null) {
          return false;
        }
      }
      else if (!this.zzaxx.equals(((zzgi)paramObject).zzaxx)) {
        return false;
      }
      if (this.zzaxy == null)
      {
        if (((zzgi)paramObject).zzaxy != null) {
          return false;
        }
      }
      else if (!this.zzaxy.equals(((zzgi)paramObject).zzaxy)) {
        return false;
      }
      if (this.zzaxz == null)
      {
        if (((zzgi)paramObject).zzaxz != null) {
          return false;
        }
      }
      else if (!this.zzaxz.equals(((zzgi)paramObject).zzaxz)) {
        return false;
      }
      if (this.zzaya == null)
      {
        if (((zzgi)paramObject).zzaya != null) {
          return false;
        }
      }
      else if (!this.zzaya.equals(((zzgi)paramObject).zzaya)) {
        return false;
      }
      if (this.zzayb == null)
      {
        if (((zzgi)paramObject).zzayb != null) {
          return false;
        }
      }
      else if (!this.zzayb.equals(((zzgi)paramObject).zzayb)) {
        return false;
      }
      if (this.zzayc == null)
      {
        if (((zzgi)paramObject).zzayc != null) {
          return false;
        }
      }
      else if (!this.zzayc.equals(((zzgi)paramObject).zzayc)) {
        return false;
      }
      if (this.zzawj == null)
      {
        if (((zzgi)paramObject).zzawj != null) {
          return false;
        }
      }
      else if (!this.zzawj.equals(((zzgi)paramObject).zzawj)) {
        return false;
      }
      if (this.zzayd == null)
      {
        if (((zzgi)paramObject).zzayd != null) {
          return false;
        }
      }
      else if (!this.zzayd.equals(((zzgi)paramObject).zzayd)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgi)paramObject).zzcfc == null) || (((zzgi)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgi)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i33 = 0;
    int i34 = getClass().getName().hashCode();
    int i;
    int i35;
    int i36;
    int j;
    label51:
    int k;
    label60:
    int m;
    label70:
    int n;
    label80:
    int i1;
    label90:
    int i2;
    label100:
    int i3;
    label110:
    int i4;
    label120:
    int i5;
    label130:
    int i6;
    label140:
    int i7;
    label150:
    int i8;
    label160:
    int i9;
    label170:
    int i10;
    label180:
    int i11;
    label190:
    int i12;
    label200:
    int i13;
    label210:
    int i14;
    label220:
    int i15;
    label230:
    int i16;
    label240:
    int i17;
    label250:
    int i18;
    label260:
    int i19;
    label270:
    int i37;
    int i20;
    label289:
    int i21;
    label299:
    int i22;
    label309:
    int i23;
    label319:
    int i24;
    label329:
    int i25;
    label339:
    int i26;
    label349:
    int i27;
    label359:
    int i28;
    label369:
    int i29;
    label379:
    int i30;
    label389:
    zzfq.zzb localzzb;
    int i31;
    if (this.zzaxa == null)
    {
      i = 0;
      i35 = zzze.hashCode(this.zzaxb);
      i36 = zzze.hashCode(this.zzaxc);
      if (this.zzaxd != null) {
        break label683;
      }
      j = 0;
      if (this.zzaxe != null) {
        break label694;
      }
      k = 0;
      if (this.zzaxf != null) {
        break label705;
      }
      m = 0;
      if (this.zzaxg != null) {
        break label717;
      }
      n = 0;
      if (this.zzaxh != null) {
        break label729;
      }
      i1 = 0;
      if (this.zzaxi != null) {
        break label741;
      }
      i2 = 0;
      if (this.zzaxj != null) {
        break label753;
      }
      i3 = 0;
      if (this.zzaxk != null) {
        break label765;
      }
      i4 = 0;
      if (this.zzaia != null) {
        break label777;
      }
      i5 = 0;
      if (this.zzaxl != null) {
        break label789;
      }
      i6 = 0;
      if (this.zzage != null) {
        break label801;
      }
      i7 = 0;
      if (this.zztt != null) {
        break label813;
      }
      i8 = 0;
      if (this.zzts != null) {
        break label825;
      }
      i9 = 0;
      if (this.zzaxm != null) {
        break label837;
      }
      i10 = 0;
      if (this.zzaxn != null) {
        break label849;
      }
      i11 = 0;
      if (this.zzaxo != null) {
        break label861;
      }
      i12 = 0;
      if (this.zzaxp != null) {
        break label873;
      }
      i13 = 0;
      if (this.zzafw != null) {
        break label885;
      }
      i14 = 0;
      if (this.zzaxq != null) {
        break label897;
      }
      i15 = 0;
      if (this.zzaxr != null) {
        break label909;
      }
      i16 = 0;
      if (this.zzagv != null) {
        break label921;
      }
      i17 = 0;
      if (this.zzafx != null) {
        break label933;
      }
      i18 = 0;
      if (this.zzaxs != null) {
        break label945;
      }
      i19 = 0;
      i37 = zzze.hashCode(this.zzaxt);
      if (this.zzafz != null) {
        break label957;
      }
      i20 = 0;
      if (this.zzaxu != null) {
        break label969;
      }
      i21 = 0;
      if (this.zzaxv != null) {
        break label981;
      }
      i22 = 0;
      if (this.zzaxw != null) {
        break label993;
      }
      i23 = 0;
      if (this.zzaxx != null) {
        break label1005;
      }
      i24 = 0;
      if (this.zzaxy != null) {
        break label1017;
      }
      i25 = 0;
      if (this.zzaxz != null) {
        break label1029;
      }
      i26 = 0;
      if (this.zzaya != null) {
        break label1041;
      }
      i27 = 0;
      if (this.zzayb != null) {
        break label1053;
      }
      i28 = 0;
      if (this.zzayc != null) {
        break label1065;
      }
      i29 = 0;
      if (this.zzawj != null) {
        break label1077;
      }
      i30 = 0;
      localzzb = this.zzayd;
      if (localzzb != null) {
        break label1089;
      }
      i31 = 0;
      label403:
      i32 = i33;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label1099;
        }
      }
    }
    label683:
    label694:
    label705:
    label717:
    label729:
    label741:
    label753:
    label765:
    label777:
    label789:
    label801:
    label813:
    label825:
    label837:
    label849:
    label861:
    label873:
    label885:
    label897:
    label909:
    label921:
    label933:
    label945:
    label957:
    label969:
    label981:
    label993:
    label1005:
    label1017:
    label1029:
    label1041:
    label1053:
    label1065:
    label1077:
    label1089:
    label1099:
    for (int i32 = i33;; i32 = this.zzcfc.hashCode())
    {
      return (i31 + (i30 + (i29 + (i28 + (i27 + (i26 + (i25 + (i24 + (i23 + (i22 + (i21 + (i20 + ((i19 + (i18 + (i17 + (i16 + (i15 + (i14 + (i13 + (i12 + (i11 + (i10 + (i9 + (i8 + (i7 + (i6 + (i5 + (i4 + (i3 + (i2 + (i1 + (n + (m + (k + (j + (((i + (i34 + 527) * 31) * 31 + i35) * 31 + i36) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + i37) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + i32;
      i = this.zzaxa.hashCode();
      break;
      j = this.zzaxd.hashCode();
      break label51;
      k = this.zzaxe.hashCode();
      break label60;
      m = this.zzaxf.hashCode();
      break label70;
      n = this.zzaxg.hashCode();
      break label80;
      i1 = this.zzaxh.hashCode();
      break label90;
      i2 = this.zzaxi.hashCode();
      break label100;
      i3 = this.zzaxj.hashCode();
      break label110;
      i4 = this.zzaxk.hashCode();
      break label120;
      i5 = this.zzaia.hashCode();
      break label130;
      i6 = this.zzaxl.hashCode();
      break label140;
      i7 = this.zzage.hashCode();
      break label150;
      i8 = this.zztt.hashCode();
      break label160;
      i9 = this.zzts.hashCode();
      break label170;
      i10 = this.zzaxm.hashCode();
      break label180;
      i11 = this.zzaxn.hashCode();
      break label190;
      i12 = this.zzaxo.hashCode();
      break label200;
      i13 = this.zzaxp.hashCode();
      break label210;
      i14 = this.zzafw.hashCode();
      break label220;
      i15 = this.zzaxq.hashCode();
      break label230;
      i16 = this.zzaxr.hashCode();
      break label240;
      i17 = this.zzagv.hashCode();
      break label250;
      i18 = this.zzafx.hashCode();
      break label260;
      i19 = this.zzaxs.hashCode();
      break label270;
      i20 = this.zzafz.hashCode();
      break label289;
      i21 = this.zzaxu.hashCode();
      break label299;
      i22 = this.zzaxv.hashCode();
      break label309;
      i23 = this.zzaxw.hashCode();
      break label319;
      i24 = this.zzaxx.hashCode();
      break label329;
      i25 = this.zzaxy.hashCode();
      break label339;
      i26 = this.zzaxz.hashCode();
      break label349;
      i27 = this.zzaya.hashCode();
      break label359;
      i28 = this.zzayb.hashCode();
      break label369;
      i29 = this.zzayc.hashCode();
      break label379;
      i30 = this.zzawj.hashCode();
      break label389;
      i31 = localzzb.hashCode();
      break label403;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    int j = 0;
    if (this.zzaxa != null) {
      paramzzyy.zzd(1, this.zzaxa.intValue());
    }
    int i;
    Object localObject;
    if ((this.zzaxb != null) && (this.zzaxb.length > 0))
    {
      i = 0;
      while (i < this.zzaxb.length)
      {
        localObject = this.zzaxb[i];
        if (localObject != null) {
          paramzzyy.zza(2, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzaxc != null) && (this.zzaxc.length > 0))
    {
      i = 0;
      while (i < this.zzaxc.length)
      {
        localObject = this.zzaxc[i];
        if (localObject != null) {
          paramzzyy.zza(3, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if (this.zzaxd != null) {
      paramzzyy.zzi(4, this.zzaxd.longValue());
    }
    if (this.zzaxe != null) {
      paramzzyy.zzi(5, this.zzaxe.longValue());
    }
    if (this.zzaxf != null) {
      paramzzyy.zzi(6, this.zzaxf.longValue());
    }
    if (this.zzaxh != null) {
      paramzzyy.zzi(7, this.zzaxh.longValue());
    }
    if (this.zzaxi != null) {
      paramzzyy.zzb(8, this.zzaxi);
    }
    if (this.zzaxj != null) {
      paramzzyy.zzb(9, this.zzaxj);
    }
    if (this.zzaxk != null) {
      paramzzyy.zzb(10, this.zzaxk);
    }
    if (this.zzaia != null) {
      paramzzyy.zzb(11, this.zzaia);
    }
    if (this.zzaxl != null) {
      paramzzyy.zzd(12, this.zzaxl.intValue());
    }
    if (this.zzage != null) {
      paramzzyy.zzb(13, this.zzage);
    }
    if (this.zztt != null) {
      paramzzyy.zzb(14, this.zztt);
    }
    if (this.zzts != null) {
      paramzzyy.zzb(16, this.zzts);
    }
    if (this.zzaxm != null) {
      paramzzyy.zzi(17, this.zzaxm.longValue());
    }
    if (this.zzaxn != null) {
      paramzzyy.zzi(18, this.zzaxn.longValue());
    }
    if (this.zzaxo != null) {
      paramzzyy.zzb(19, this.zzaxo);
    }
    if (this.zzaxp != null) {
      paramzzyy.zzb(20, this.zzaxp.booleanValue());
    }
    if (this.zzafw != null) {
      paramzzyy.zzb(21, this.zzafw);
    }
    if (this.zzaxq != null) {
      paramzzyy.zzi(22, this.zzaxq.longValue());
    }
    if (this.zzaxr != null) {
      paramzzyy.zzd(23, this.zzaxr.intValue());
    }
    if (this.zzagv != null) {
      paramzzyy.zzb(24, this.zzagv);
    }
    if (this.zzafx != null) {
      paramzzyy.zzb(25, this.zzafx);
    }
    if (this.zzaxg != null) {
      paramzzyy.zzi(26, this.zzaxg.longValue());
    }
    if (this.zzaxs != null) {
      paramzzyy.zzb(28, this.zzaxs.booleanValue());
    }
    if ((this.zzaxt != null) && (this.zzaxt.length > 0))
    {
      i = j;
      while (i < this.zzaxt.length)
      {
        localObject = this.zzaxt[i];
        if (localObject != null) {
          paramzzyy.zza(29, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if (this.zzafz != null) {
      paramzzyy.zzb(30, this.zzafz);
    }
    if (this.zzaxu != null) {
      paramzzyy.zzd(31, this.zzaxu.intValue());
    }
    if (this.zzaxv != null) {
      paramzzyy.zzd(32, this.zzaxv.intValue());
    }
    if (this.zzaxw != null) {
      paramzzyy.zzd(33, this.zzaxw.intValue());
    }
    if (this.zzaxx != null) {
      paramzzyy.zzb(34, this.zzaxx);
    }
    if (this.zzaxy != null) {
      paramzzyy.zzi(35, this.zzaxy.longValue());
    }
    if (this.zzaxz != null) {
      paramzzyy.zzi(36, this.zzaxz.longValue());
    }
    if (this.zzaya != null) {
      paramzzyy.zzb(37, this.zzaya);
    }
    if (this.zzayb != null) {
      paramzzyy.zzb(38, this.zzayb);
    }
    if (this.zzayc != null) {
      paramzzyy.zzd(39, this.zzayc.intValue());
    }
    if (this.zzawj != null) {
      paramzzyy.zzb(41, this.zzawj);
    }
    if (this.zzayd != null) {
      paramzzyy.zze(44, this.zzayd);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int m = 0;
    int j = super.zzf();
    int i = j;
    if (this.zzaxa != null) {
      i = j + zzyy.zzh(1, this.zzaxa.intValue());
    }
    j = i;
    Object localObject;
    if (this.zzaxb != null)
    {
      j = i;
      if (this.zzaxb.length > 0)
      {
        j = 0;
        while (j < this.zzaxb.length)
        {
          localObject = this.zzaxb[j];
          k = i;
          if (localObject != null) {
            k = i + zzyy.zzb(2, (zzzg)localObject);
          }
          j += 1;
          i = k;
        }
        j = i;
      }
    }
    i = j;
    if (this.zzaxc != null)
    {
      i = j;
      if (this.zzaxc.length > 0)
      {
        i = j;
        j = 0;
        while (j < this.zzaxc.length)
        {
          localObject = this.zzaxc[j];
          k = i;
          if (localObject != null) {
            k = i + zzyy.zzb(3, (zzzg)localObject);
          }
          j += 1;
          i = k;
        }
      }
    }
    j = i;
    if (this.zzaxd != null) {
      j = i + zzyy.zzd(4, this.zzaxd.longValue());
    }
    i = j;
    if (this.zzaxe != null) {
      i = j + zzyy.zzd(5, this.zzaxe.longValue());
    }
    j = i;
    if (this.zzaxf != null) {
      j = i + zzyy.zzd(6, this.zzaxf.longValue());
    }
    i = j;
    if (this.zzaxh != null) {
      i = j + zzyy.zzd(7, this.zzaxh.longValue());
    }
    j = i;
    if (this.zzaxi != null) {
      j = i + zzyy.zzc(8, this.zzaxi);
    }
    i = j;
    if (this.zzaxj != null) {
      i = j + zzyy.zzc(9, this.zzaxj);
    }
    j = i;
    if (this.zzaxk != null) {
      j = i + zzyy.zzc(10, this.zzaxk);
    }
    i = j;
    if (this.zzaia != null) {
      i = j + zzyy.zzc(11, this.zzaia);
    }
    j = i;
    if (this.zzaxl != null) {
      j = i + zzyy.zzh(12, this.zzaxl.intValue());
    }
    i = j;
    if (this.zzage != null) {
      i = j + zzyy.zzc(13, this.zzage);
    }
    j = i;
    if (this.zztt != null) {
      j = i + zzyy.zzc(14, this.zztt);
    }
    i = j;
    if (this.zzts != null) {
      i = j + zzyy.zzc(16, this.zzts);
    }
    j = i;
    if (this.zzaxm != null) {
      j = i + zzyy.zzd(17, this.zzaxm.longValue());
    }
    i = j;
    if (this.zzaxn != null) {
      i = j + zzyy.zzd(18, this.zzaxn.longValue());
    }
    j = i;
    if (this.zzaxo != null) {
      j = i + zzyy.zzc(19, this.zzaxo);
    }
    i = j;
    if (this.zzaxp != null)
    {
      this.zzaxp.booleanValue();
      i = j + (zzyy.zzbb(20) + 1);
    }
    j = i;
    if (this.zzafw != null) {
      j = i + zzyy.zzc(21, this.zzafw);
    }
    i = j;
    if (this.zzaxq != null) {
      i = j + zzyy.zzd(22, this.zzaxq.longValue());
    }
    j = i;
    if (this.zzaxr != null) {
      j = i + zzyy.zzh(23, this.zzaxr.intValue());
    }
    i = j;
    if (this.zzagv != null) {
      i = j + zzyy.zzc(24, this.zzagv);
    }
    j = i;
    if (this.zzafx != null) {
      j = i + zzyy.zzc(25, this.zzafx);
    }
    int k = j;
    if (this.zzaxg != null) {
      k = j + zzyy.zzd(26, this.zzaxg.longValue());
    }
    i = k;
    if (this.zzaxs != null)
    {
      this.zzaxs.booleanValue();
      i = k + (zzyy.zzbb(28) + 1);
    }
    j = i;
    if (this.zzaxt != null)
    {
      j = i;
      if (this.zzaxt.length > 0)
      {
        k = m;
        for (;;)
        {
          j = i;
          if (k >= this.zzaxt.length) {
            break;
          }
          localObject = this.zzaxt[k];
          j = i;
          if (localObject != null) {
            j = i + zzyy.zzb(29, (zzzg)localObject);
          }
          k += 1;
          i = j;
        }
      }
    }
    i = j;
    if (this.zzafz != null) {
      i = j + zzyy.zzc(30, this.zzafz);
    }
    j = i;
    if (this.zzaxu != null) {
      j = i + zzyy.zzh(31, this.zzaxu.intValue());
    }
    i = j;
    if (this.zzaxv != null) {
      i = j + zzyy.zzh(32, this.zzaxv.intValue());
    }
    j = i;
    if (this.zzaxw != null) {
      j = i + zzyy.zzh(33, this.zzaxw.intValue());
    }
    i = j;
    if (this.zzaxx != null) {
      i = j + zzyy.zzc(34, this.zzaxx);
    }
    j = i;
    if (this.zzaxy != null) {
      j = i + zzyy.zzd(35, this.zzaxy.longValue());
    }
    i = j;
    if (this.zzaxz != null) {
      i = j + zzyy.zzd(36, this.zzaxz.longValue());
    }
    j = i;
    if (this.zzaya != null) {
      j = i + zzyy.zzc(37, this.zzaya);
    }
    i = j;
    if (this.zzayb != null) {
      i = j + zzyy.zzc(38, this.zzayb);
    }
    j = i;
    if (this.zzayc != null) {
      j = i + zzyy.zzh(39, this.zzayc.intValue());
    }
    i = j;
    if (this.zzawj != null) {
      i = j + zzyy.zzc(41, this.zzawj);
    }
    j = i;
    if (this.zzayd != null) {
      j = i + zzut.zzc(44, this.zzayd);
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */