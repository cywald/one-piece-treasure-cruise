package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzga
  extends zzza<zzga>
{
  private static volatile zzga[] zzawa;
  public String name = null;
  public Boolean zzawb = null;
  public Boolean zzawc = null;
  public Integer zzawd = null;
  
  public zzga()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzga[] zzmm()
  {
    if (zzawa == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzawa == null) {
        zzawa = new zzga[0];
      }
      return zzawa;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzga)) {
        return false;
      }
      paramObject = (zzga)paramObject;
      if (this.name == null)
      {
        if (((zzga)paramObject).name != null) {
          return false;
        }
      }
      else if (!this.name.equals(((zzga)paramObject).name)) {
        return false;
      }
      if (this.zzawb == null)
      {
        if (((zzga)paramObject).zzawb != null) {
          return false;
        }
      }
      else if (!this.zzawb.equals(((zzga)paramObject).zzawb)) {
        return false;
      }
      if (this.zzawc == null)
      {
        if (((zzga)paramObject).zzawc != null) {
          return false;
        }
      }
      else if (!this.zzawc.equals(((zzga)paramObject).zzawc)) {
        return false;
      }
      if (this.zzawd == null)
      {
        if (((zzga)paramObject).zzawd != null) {
          return false;
        }
      }
      else if (!this.zzawd.equals(((zzga)paramObject).zzawd)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzga)paramObject).zzcfc == null) || (((zzga)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzga)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i1 = 0;
    int i2 = getClass().getName().hashCode();
    int i;
    int j;
    label33:
    int k;
    label42:
    int m;
    if (this.name == null)
    {
      i = 0;
      if (this.zzawb != null) {
        break label122;
      }
      j = 0;
      if (this.zzawc != null) {
        break label133;
      }
      k = 0;
      if (this.zzawd != null) {
        break label144;
      }
      m = 0;
      label52:
      n = i1;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label156;
        }
      }
    }
    label122:
    label133:
    label144:
    label156:
    for (int n = i1;; n = this.zzcfc.hashCode())
    {
      return (m + (k + (j + (i + (i2 + 527) * 31) * 31) * 31) * 31) * 31 + n;
      i = this.name.hashCode();
      break;
      j = this.zzawb.hashCode();
      break label33;
      k = this.zzawc.hashCode();
      break label42;
      m = this.zzawd.hashCode();
      break label52;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.name != null) {
      paramzzyy.zzb(1, this.name);
    }
    if (this.zzawb != null) {
      paramzzyy.zzb(2, this.zzawb.booleanValue());
    }
    if (this.zzawc != null) {
      paramzzyy.zzb(3, this.zzawc.booleanValue());
    }
    if (this.zzawd != null) {
      paramzzyy.zzd(4, this.zzawd.intValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.name != null) {
      i = j + zzyy.zzc(1, this.name);
    }
    j = i;
    if (this.zzawb != null)
    {
      this.zzawb.booleanValue();
      j = i + (zzyy.zzbb(2) + 1);
    }
    i = j;
    if (this.zzawc != null)
    {
      this.zzawc.booleanValue();
      i = j + (zzyy.zzbb(3) + 1);
    }
    j = i;
    if (this.zzawd != null) {
      j = i + zzyy.zzh(4, this.zzawd.intValue());
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzga.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */