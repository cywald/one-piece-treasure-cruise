package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@VisibleForTesting
public final class zzrw
{
  private final String version;
  private final List<zzry> zzbol;
  private final Map<String, List<zzru>> zzbom;
  private final int zzph;
  
  private zzrw(List<zzry> paramList, Map<String, List<zzru>> paramMap, String paramString, int paramInt)
  {
    this.zzbol = Collections.unmodifiableList(paramList);
    this.zzbom = Collections.unmodifiableMap(paramMap);
    this.version = paramString;
    this.zzph = paramInt;
  }
  
  public static zzrx zzsr()
  {
    return new zzrx(null);
  }
  
  public final String getVersion()
  {
    return this.version;
  }
  
  public final String toString()
  {
    String str1 = String.valueOf(this.zzbol);
    String str2 = String.valueOf(this.zzbom);
    return String.valueOf(str1).length() + 17 + String.valueOf(str2).length() + "Rules: " + str1 + "  Macros: " + str2;
  }
  
  public final List<zzry> zzrw()
  {
    return this.zzbol;
  }
  
  public final Map<String, List<zzru>> zzss()
  {
    return this.zzbom;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzrw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */