package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgb
  extends zzza<zzgb>
{
  public String zzafx = null;
  public Long zzawe = null;
  private Integer zzawf = null;
  public zzgc[] zzawg = zzgc.zzmn();
  public zzga[] zzawh = zzga.zzmm();
  public zzfu[] zzawi = zzfu.zzmi();
  private String zzawj = null;
  
  public zzgb()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgb)) {
        return false;
      }
      paramObject = (zzgb)paramObject;
      if (this.zzawe == null)
      {
        if (((zzgb)paramObject).zzawe != null) {
          return false;
        }
      }
      else if (!this.zzawe.equals(((zzgb)paramObject).zzawe)) {
        return false;
      }
      if (this.zzafx == null)
      {
        if (((zzgb)paramObject).zzafx != null) {
          return false;
        }
      }
      else if (!this.zzafx.equals(((zzgb)paramObject).zzafx)) {
        return false;
      }
      if (this.zzawf == null)
      {
        if (((zzgb)paramObject).zzawf != null) {
          return false;
        }
      }
      else if (!this.zzawf.equals(((zzgb)paramObject).zzawf)) {
        return false;
      }
      if (!zzze.equals(this.zzawg, ((zzgb)paramObject).zzawg)) {
        return false;
      }
      if (!zzze.equals(this.zzawh, ((zzgb)paramObject).zzawh)) {
        return false;
      }
      if (!zzze.equals(this.zzawi, ((zzgb)paramObject).zzawi)) {
        return false;
      }
      if (this.zzawj == null)
      {
        if (((zzgb)paramObject).zzawj != null) {
          return false;
        }
      }
      else if (!this.zzawj.equals(((zzgb)paramObject).zzawj)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgb)paramObject).zzcfc == null) || (((zzgb)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgb)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i1 = 0;
    int i2 = getClass().getName().hashCode();
    int i;
    int j;
    label33:
    int k;
    label42:
    int i3;
    int i4;
    int i5;
    int m;
    if (this.zzawe == null)
    {
      i = 0;
      if (this.zzafx != null) {
        break label167;
      }
      j = 0;
      if (this.zzawf != null) {
        break label178;
      }
      k = 0;
      i3 = zzze.hashCode(this.zzawg);
      i4 = zzze.hashCode(this.zzawh);
      i5 = zzze.hashCode(this.zzawi);
      if (this.zzawj != null) {
        break label189;
      }
      m = 0;
      label79:
      n = i1;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label201;
        }
      }
    }
    label167:
    label178:
    label189:
    label201:
    for (int n = i1;; n = this.zzcfc.hashCode())
    {
      return (m + ((((k + (j + (i + (i2 + 527) * 31) * 31) * 31) * 31 + i3) * 31 + i4) * 31 + i5) * 31) * 31 + n;
      i = this.zzawe.hashCode();
      break;
      j = this.zzafx.hashCode();
      break label33;
      k = this.zzawf.hashCode();
      break label42;
      m = this.zzawj.hashCode();
      break label79;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    int j = 0;
    if (this.zzawe != null) {
      paramzzyy.zzi(1, this.zzawe.longValue());
    }
    if (this.zzafx != null) {
      paramzzyy.zzb(2, this.zzafx);
    }
    if (this.zzawf != null) {
      paramzzyy.zzd(3, this.zzawf.intValue());
    }
    int i;
    Object localObject;
    if ((this.zzawg != null) && (this.zzawg.length > 0))
    {
      i = 0;
      while (i < this.zzawg.length)
      {
        localObject = this.zzawg[i];
        if (localObject != null) {
          paramzzyy.zza(4, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzawh != null) && (this.zzawh.length > 0))
    {
      i = 0;
      while (i < this.zzawh.length)
      {
        localObject = this.zzawh[i];
        if (localObject != null) {
          paramzzyy.zza(5, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzawi != null) && (this.zzawi.length > 0))
    {
      i = j;
      while (i < this.zzawi.length)
      {
        localObject = this.zzawi[i];
        if (localObject != null) {
          paramzzyy.zza(6, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if (this.zzawj != null) {
      paramzzyy.zzb(7, this.zzawj);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int m = 0;
    int j = super.zzf();
    int i = j;
    if (this.zzawe != null) {
      i = j + zzyy.zzd(1, this.zzawe.longValue());
    }
    j = i;
    if (this.zzafx != null) {
      j = i + zzyy.zzc(2, this.zzafx);
    }
    i = j;
    if (this.zzawf != null) {
      i = j + zzyy.zzh(3, this.zzawf.intValue());
    }
    j = i;
    Object localObject;
    int k;
    if (this.zzawg != null)
    {
      j = i;
      if (this.zzawg.length > 0)
      {
        j = 0;
        while (j < this.zzawg.length)
        {
          localObject = this.zzawg[j];
          k = i;
          if (localObject != null) {
            k = i + zzyy.zzb(4, (zzzg)localObject);
          }
          j += 1;
          i = k;
        }
        j = i;
      }
    }
    i = j;
    if (this.zzawh != null)
    {
      i = j;
      if (this.zzawh.length > 0)
      {
        i = j;
        j = 0;
        while (j < this.zzawh.length)
        {
          localObject = this.zzawh[j];
          k = i;
          if (localObject != null) {
            k = i + zzyy.zzb(5, (zzzg)localObject);
          }
          j += 1;
          i = k;
        }
      }
    }
    j = i;
    if (this.zzawi != null)
    {
      j = i;
      if (this.zzawi.length > 0)
      {
        k = m;
        for (;;)
        {
          j = i;
          if (k >= this.zzawi.length) {
            break;
          }
          localObject = this.zzawi[k];
          j = i;
          if (localObject != null) {
            j = i + zzyy.zzb(6, (zzzg)localObject);
          }
          k += 1;
          i = j;
        }
      }
    }
    i = j;
    if (this.zzawj != null) {
      i = j + zzyy.zzc(7, this.zzawj);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */