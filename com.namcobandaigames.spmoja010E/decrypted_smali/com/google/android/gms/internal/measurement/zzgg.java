package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgg
  extends zzza<zzgg>
{
  private static volatile zzgg[] zzaww;
  public String name = null;
  public String zzamp = null;
  private Float zzaug = null;
  public Double zzauh = null;
  public Long zzawx = null;
  
  public zzgg()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzgg[] zzmr()
  {
    if (zzaww == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzaww == null) {
        zzaww = new zzgg[0];
      }
      return zzaww;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgg)) {
        return false;
      }
      paramObject = (zzgg)paramObject;
      if (this.name == null)
      {
        if (((zzgg)paramObject).name != null) {
          return false;
        }
      }
      else if (!this.name.equals(((zzgg)paramObject).name)) {
        return false;
      }
      if (this.zzamp == null)
      {
        if (((zzgg)paramObject).zzamp != null) {
          return false;
        }
      }
      else if (!this.zzamp.equals(((zzgg)paramObject).zzamp)) {
        return false;
      }
      if (this.zzawx == null)
      {
        if (((zzgg)paramObject).zzawx != null) {
          return false;
        }
      }
      else if (!this.zzawx.equals(((zzgg)paramObject).zzawx)) {
        return false;
      }
      if (this.zzaug == null)
      {
        if (((zzgg)paramObject).zzaug != null) {
          return false;
        }
      }
      else if (!this.zzaug.equals(((zzgg)paramObject).zzaug)) {
        return false;
      }
      if (this.zzauh == null)
      {
        if (((zzgg)paramObject).zzauh != null) {
          return false;
        }
      }
      else if (!this.zzauh.equals(((zzgg)paramObject).zzauh)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgg)paramObject).zzcfc == null) || (((zzgg)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgg)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i2 = 0;
    int i3 = getClass().getName().hashCode();
    int i;
    int j;
    label33:
    int k;
    label42:
    int m;
    label52:
    int n;
    if (this.name == null)
    {
      i = 0;
      if (this.zzamp != null) {
        break label138;
      }
      j = 0;
      if (this.zzawx != null) {
        break label149;
      }
      k = 0;
      if (this.zzaug != null) {
        break label160;
      }
      m = 0;
      if (this.zzauh != null) {
        break label172;
      }
      n = 0;
      label62:
      i1 = i2;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label184;
        }
      }
    }
    label138:
    label149:
    label160:
    label172:
    label184:
    for (int i1 = i2;; i1 = this.zzcfc.hashCode())
    {
      return (n + (m + (k + (j + (i + (i3 + 527) * 31) * 31) * 31) * 31) * 31) * 31 + i1;
      i = this.name.hashCode();
      break;
      j = this.zzamp.hashCode();
      break label33;
      k = this.zzawx.hashCode();
      break label42;
      m = this.zzaug.hashCode();
      break label52;
      n = this.zzauh.hashCode();
      break label62;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.name != null) {
      paramzzyy.zzb(1, this.name);
    }
    if (this.zzamp != null) {
      paramzzyy.zzb(2, this.zzamp);
    }
    if (this.zzawx != null) {
      paramzzyy.zzi(3, this.zzawx.longValue());
    }
    if (this.zzaug != null) {
      paramzzyy.zza(4, this.zzaug.floatValue());
    }
    if (this.zzauh != null) {
      paramzzyy.zza(5, this.zzauh.doubleValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.name != null) {
      i = j + zzyy.zzc(1, this.name);
    }
    j = i;
    if (this.zzamp != null) {
      j = i + zzyy.zzc(2, this.zzamp);
    }
    i = j;
    if (this.zzawx != null) {
      i = j + zzyy.zzd(3, this.zzawx.longValue());
    }
    j = i;
    if (this.zzaug != null)
    {
      this.zzaug.floatValue();
      j = i + (zzyy.zzbb(4) + 4);
    }
    i = j;
    if (this.zzauh != null)
    {
      this.zzauh.doubleValue();
      i = j + (zzyy.zzbb(5) + 8);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */