package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.List;
import java.util.Map;

final class zzur
  implements zzxi
{
  private int tag;
  private final zzuo zzbur;
  private int zzbus;
  private int zzbut = 0;
  
  private zzur(zzuo paramzzuo)
  {
    this.zzbur = ((zzuo)zzvo.zza(paramzzuo, "input"));
    this.zzbur.zzbuk = this;
  }
  
  public static zzur zza(zzuo paramzzuo)
  {
    if (paramzzuo.zzbuk != null) {
      return paramzzuo.zzbuk;
    }
    return new zzur(paramzzuo);
  }
  
  private final Object zza(zzyq paramzzyq, Class<?> paramClass, zzuz paramzzuz)
    throws IOException
  {
    switch (zzus.zzbuu[paramzzyq.ordinal()])
    {
    default: 
      throw new RuntimeException("unsupported field type.");
    case 1: 
      return Boolean.valueOf(zzum());
    case 2: 
      return zzuo();
    case 3: 
      return Double.valueOf(readDouble());
    case 4: 
      return Integer.valueOf(zzuq());
    case 5: 
      return Integer.valueOf(zzul());
    case 6: 
      return Long.valueOf(zzuk());
    case 7: 
      return Float.valueOf(readFloat());
    case 8: 
      return Integer.valueOf(zzuj());
    case 9: 
      return Long.valueOf(zzui());
    case 10: 
      zzat(2);
      return zzc(zzxf.zzxn().zzi(paramClass), paramzzuz);
    case 11: 
      return Integer.valueOf(zzur());
    case 12: 
      return Long.valueOf(zzus());
    case 13: 
      return Integer.valueOf(zzut());
    case 14: 
      return Long.valueOf(zzuu());
    case 15: 
      return zzun();
    case 16: 
      return Integer.valueOf(zzup());
    }
    return Long.valueOf(zzuh());
  }
  
  private final void zza(List<String> paramList, boolean paramBoolean)
    throws IOException
  {
    if ((this.tag & 0x7) != 2) {
      throw zzvt.zzwo();
    }
    int i;
    if (((paramList instanceof zzwc)) && (!paramBoolean))
    {
      paramList = (zzwc)paramList;
      do
      {
        paramList.zzc(zzuo());
        if (this.zzbur.zzuw()) {
          return;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    label74:
    if (paramBoolean) {}
    for (String str = zzun();; str = readString())
    {
      paramList.add(str);
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
      if (i == this.tag) {
        break label74;
      }
      this.zzbut = i;
      return;
    }
  }
  
  private final void zzat(int paramInt)
    throws IOException
  {
    if ((this.tag & 0x7) != paramInt) {
      throw zzvt.zzwo();
    }
  }
  
  private static void zzau(int paramInt)
    throws IOException
  {
    if ((paramInt & 0x7) != 0) {
      throw zzvt.zzwq();
    }
  }
  
  private static void zzav(int paramInt)
    throws IOException
  {
    if ((paramInt & 0x3) != 0) {
      throw zzvt.zzwq();
    }
  }
  
  private final void zzaw(int paramInt)
    throws IOException
  {
    if (this.zzbur.zzux() != paramInt) {
      throw zzvt.zzwk();
    }
  }
  
  private final <T> T zzc(zzxj<T> paramzzxj, zzuz paramzzuz)
    throws IOException
  {
    int i = this.zzbur.zzup();
    if (this.zzbur.zzbuh >= this.zzbur.zzbui) {
      throw zzvt.zzwp();
    }
    i = this.zzbur.zzaq(i);
    Object localObject = paramzzxj.newInstance();
    zzuo localzzuo = this.zzbur;
    localzzuo.zzbuh += 1;
    paramzzxj.zza(localObject, this, paramzzuz);
    paramzzxj.zzu(localObject);
    this.zzbur.zzan(0);
    paramzzxj = this.zzbur;
    paramzzxj.zzbuh -= 1;
    this.zzbur.zzar(i);
    return (T)localObject;
  }
  
  private final <T> T zzd(zzxj<T> paramzzxj, zzuz paramzzuz)
    throws IOException
  {
    int i = this.zzbus;
    this.zzbus = (this.tag >>> 3 << 3 | 0x4);
    Object localObject;
    try
    {
      localObject = paramzzxj.newInstance();
      paramzzxj.zza(localObject, this, paramzzuz);
      paramzzxj.zzu(localObject);
      if (this.tag != this.zzbus) {
        throw zzvt.zzwq();
      }
    }
    finally
    {
      this.zzbus = i;
    }
    this.zzbus = i;
    return (T)localObject;
  }
  
  public final int getTag()
  {
    return this.tag;
  }
  
  public final double readDouble()
    throws IOException
  {
    zzat(1);
    return this.zzbur.readDouble();
  }
  
  public final float readFloat()
    throws IOException
  {
    zzat(5);
    return this.zzbur.readFloat();
  }
  
  public final String readString()
    throws IOException
  {
    zzat(2);
    return this.zzbur.readString();
  }
  
  public final void readStringList(List<String> paramList)
    throws IOException
  {
    zza(paramList, false);
  }
  
  public final <T> T zza(zzxj<T> paramzzxj, zzuz paramzzuz)
    throws IOException
  {
    zzat(2);
    return (T)zzc(paramzzxj, paramzzuz);
  }
  
  public final <T> void zza(List<T> paramList, zzxj<T> paramzzxj, zzuz paramzzuz)
    throws IOException
  {
    if ((this.tag & 0x7) != 2) {
      throw zzvt.zzwo();
    }
    int i = this.tag;
    int j;
    do
    {
      paramList.add(zzc(paramzzxj, paramzzuz));
      if ((this.zzbur.zzuw()) || (this.zzbut != 0)) {
        return;
      }
      j = this.zzbur.zzug();
    } while (j == i);
    this.zzbut = j;
  }
  
  public final <K, V> void zza(Map<K, V> paramMap, zzwm<K, V> paramzzwm, zzuz paramzzuz)
    throws IOException
  {
    zzat(2);
    int i = this.zzbur.zzup();
    i = this.zzbur.zzaq(i);
    Object localObject1 = paramzzwm.zzcas;
    Object localObject3 = paramzzwm.zzbre;
    Object localObject4;
    for (;;)
    {
      localObject4 = localObject1;
      try
      {
        int j = zzve();
        if (j == Integer.MAX_VALUE) {
          break;
        }
        boolean bool = this.zzbur.zzuw();
        if (bool) {
          break;
        }
        switch (j)
        {
        default: 
          localObject1 = localObject4;
          try
          {
            if (zzvf()) {
              continue;
            }
            throw new zzvt("Unable to parse map entry.");
          }
          catch (zzvu localzzvu)
          {
            localObject2 = localObject4;
          }
          if (zzvf()) {
            continue;
          }
          throw new zzvt("Unable to parse map entry.");
        }
      }
      finally
      {
        this.zzbur.zzar(i);
      }
      Object localObject2 = zza(paramzzwm.zzcar, null, null);
      continue;
      localObject2 = zza(paramzzwm.zzcat, paramzzwm.zzbre.getClass(), paramzzuz);
      localObject3 = localObject2;
      localObject2 = localObject4;
    }
    paramMap.put(localObject4, localObject3);
    this.zzbur.zzar(i);
  }
  
  public final <T> T zzb(zzxj<T> paramzzxj, zzuz paramzzuz)
    throws IOException
  {
    zzat(3);
    return (T)zzd(paramzzxj, paramzzuz);
  }
  
  public final <T> void zzb(List<T> paramList, zzxj<T> paramzzxj, zzuz paramzzuz)
    throws IOException
  {
    if ((this.tag & 0x7) != 3) {
      throw zzvt.zzwo();
    }
    int i = this.tag;
    int j;
    do
    {
      paramList.add(zzd(paramzzxj, paramzzuz));
      if ((this.zzbur.zzuw()) || (this.zzbut != 0)) {
        return;
      }
      j = this.zzbur.zzug();
    } while (j == i);
    this.zzbut = j;
  }
  
  public final void zzh(List<Double> paramList)
    throws IOException
  {
    int i;
    int j;
    if ((paramList instanceof zzuw))
    {
      paramList = (zzuw)paramList;
      switch (this.tag & 0x7)
      {
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup();
        zzau(i);
        j = this.zzbur.zzux();
        do
        {
          paramList.zzd(this.zzbur.readDouble());
        } while (this.zzbur.zzux() < i + j);
        return;
      }
      do
      {
        paramList.zzd(this.zzbur.readDouble());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup();
      zzau(i);
      j = this.zzbur.zzux();
      do
      {
        paramList.add(Double.valueOf(this.zzbur.readDouble()));
      } while (this.zzbur.zzux() < i + j);
      return;
    }
    do
    {
      paramList.add(Double.valueOf(this.zzbur.readDouble()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzi(List<Float> paramList)
    throws IOException
  {
    int i;
    int j;
    if ((paramList instanceof zzvj))
    {
      paramList = (zzvj)paramList;
      switch (this.tag & 0x7)
      {
      case 3: 
      case 4: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup();
        zzav(i);
        j = this.zzbur.zzux();
        do
        {
          paramList.zzc(this.zzbur.readFloat());
        } while (this.zzbur.zzux() < i + j);
        return;
      }
      do
      {
        paramList.zzc(this.zzbur.readFloat());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 3: 
    case 4: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup();
      zzav(i);
      j = this.zzbur.zzux();
      do
      {
        paramList.add(Float.valueOf(this.zzbur.readFloat()));
      } while (this.zzbur.zzux() < i + j);
      return;
    }
    do
    {
      paramList.add(Float.valueOf(this.zzbur.readFloat()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzj(List<Long> paramList)
    throws IOException
  {
    int i;
    if ((paramList instanceof zzwh))
    {
      paramList = (zzwh)paramList;
      switch (this.tag & 0x7)
      {
      case 1: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup() + this.zzbur.zzux();
        do
        {
          paramList.zzbg(this.zzbur.zzuh());
        } while (this.zzbur.zzux() < i);
        zzaw(i);
        return;
      }
      do
      {
        paramList.zzbg(this.zzbur.zzuh());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 1: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup() + this.zzbur.zzux();
      do
      {
        paramList.add(Long.valueOf(this.zzbur.zzuh()));
      } while (this.zzbur.zzux() < i);
      zzaw(i);
      return;
    }
    do
    {
      paramList.add(Long.valueOf(this.zzbur.zzuh()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzk(List<Long> paramList)
    throws IOException
  {
    int i;
    if ((paramList instanceof zzwh))
    {
      paramList = (zzwh)paramList;
      switch (this.tag & 0x7)
      {
      case 1: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup() + this.zzbur.zzux();
        do
        {
          paramList.zzbg(this.zzbur.zzui());
        } while (this.zzbur.zzux() < i);
        zzaw(i);
        return;
      }
      do
      {
        paramList.zzbg(this.zzbur.zzui());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 1: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup() + this.zzbur.zzux();
      do
      {
        paramList.add(Long.valueOf(this.zzbur.zzui()));
      } while (this.zzbur.zzux() < i);
      zzaw(i);
      return;
    }
    do
    {
      paramList.add(Long.valueOf(this.zzbur.zzui()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzl(List<Integer> paramList)
    throws IOException
  {
    int i;
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      switch (this.tag & 0x7)
      {
      case 1: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup() + this.zzbur.zzux();
        do
        {
          paramList.zzbm(this.zzbur.zzuj());
        } while (this.zzbur.zzux() < i);
        zzaw(i);
        return;
      }
      do
      {
        paramList.zzbm(this.zzbur.zzuj());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 1: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup() + this.zzbur.zzux();
      do
      {
        paramList.add(Integer.valueOf(this.zzbur.zzuj()));
      } while (this.zzbur.zzux() < i);
      zzaw(i);
      return;
    }
    do
    {
      paramList.add(Integer.valueOf(this.zzbur.zzuj()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzm(List<Long> paramList)
    throws IOException
  {
    int i;
    int j;
    if ((paramList instanceof zzwh))
    {
      paramList = (zzwh)paramList;
      switch (this.tag & 0x7)
      {
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup();
        zzau(i);
        j = this.zzbur.zzux();
        do
        {
          paramList.zzbg(this.zzbur.zzuk());
        } while (this.zzbur.zzux() < i + j);
        return;
      }
      do
      {
        paramList.zzbg(this.zzbur.zzuk());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup();
      zzau(i);
      j = this.zzbur.zzux();
      do
      {
        paramList.add(Long.valueOf(this.zzbur.zzuk()));
      } while (this.zzbur.zzux() < i + j);
      return;
    }
    do
    {
      paramList.add(Long.valueOf(this.zzbur.zzuk()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzn(List<Integer> paramList)
    throws IOException
  {
    int i;
    int j;
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      switch (this.tag & 0x7)
      {
      case 3: 
      case 4: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup();
        zzav(i);
        j = this.zzbur.zzux();
        do
        {
          paramList.zzbm(this.zzbur.zzul());
        } while (this.zzbur.zzux() < i + j);
        return;
      }
      do
      {
        paramList.zzbm(this.zzbur.zzul());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 3: 
    case 4: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup();
      zzav(i);
      j = this.zzbur.zzux();
      do
      {
        paramList.add(Integer.valueOf(this.zzbur.zzul()));
      } while (this.zzbur.zzux() < i + j);
      return;
    }
    do
    {
      paramList.add(Integer.valueOf(this.zzbur.zzul()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzo(List<Boolean> paramList)
    throws IOException
  {
    int i;
    if ((paramList instanceof zzub))
    {
      paramList = (zzub)paramList;
      switch (this.tag & 0x7)
      {
      case 1: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup() + this.zzbur.zzux();
        do
        {
          paramList.addBoolean(this.zzbur.zzum());
        } while (this.zzbur.zzux() < i);
        zzaw(i);
        return;
      }
      do
      {
        paramList.addBoolean(this.zzbur.zzum());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 1: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup() + this.zzbur.zzux();
      do
      {
        paramList.add(Boolean.valueOf(this.zzbur.zzum()));
      } while (this.zzbur.zzux() < i);
      zzaw(i);
      return;
    }
    do
    {
      paramList.add(Boolean.valueOf(this.zzbur.zzum()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzp(List<String> paramList)
    throws IOException
  {
    zza(paramList, true);
  }
  
  public final void zzq(List<zzud> paramList)
    throws IOException
  {
    if ((this.tag & 0x7) != 2) {
      throw zzvt.zzwo();
    }
    int i;
    do
    {
      paramList.add(zzuo());
      if (this.zzbur.zzuw()) {
        return;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzr(List<Integer> paramList)
    throws IOException
  {
    int i;
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      switch (this.tag & 0x7)
      {
      case 1: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup() + this.zzbur.zzux();
        do
        {
          paramList.zzbm(this.zzbur.zzup());
        } while (this.zzbur.zzux() < i);
        zzaw(i);
        return;
      }
      do
      {
        paramList.zzbm(this.zzbur.zzup());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 1: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup() + this.zzbur.zzux();
      do
      {
        paramList.add(Integer.valueOf(this.zzbur.zzup()));
      } while (this.zzbur.zzux() < i);
      zzaw(i);
      return;
    }
    do
    {
      paramList.add(Integer.valueOf(this.zzbur.zzup()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzs(List<Integer> paramList)
    throws IOException
  {
    int i;
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      switch (this.tag & 0x7)
      {
      case 1: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup() + this.zzbur.zzux();
        do
        {
          paramList.zzbm(this.zzbur.zzuq());
        } while (this.zzbur.zzux() < i);
        zzaw(i);
        return;
      }
      do
      {
        paramList.zzbm(this.zzbur.zzuq());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 1: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup() + this.zzbur.zzux();
      do
      {
        paramList.add(Integer.valueOf(this.zzbur.zzuq()));
      } while (this.zzbur.zzux() < i);
      zzaw(i);
      return;
    }
    do
    {
      paramList.add(Integer.valueOf(this.zzbur.zzuq()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzt(List<Integer> paramList)
    throws IOException
  {
    int i;
    int j;
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      switch (this.tag & 0x7)
      {
      case 3: 
      case 4: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup();
        zzav(i);
        j = this.zzbur.zzux();
        do
        {
          paramList.zzbm(this.zzbur.zzur());
        } while (this.zzbur.zzux() < i + j);
        return;
      }
      do
      {
        paramList.zzbm(this.zzbur.zzur());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 3: 
    case 4: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup();
      zzav(i);
      j = this.zzbur.zzux();
      do
      {
        paramList.add(Integer.valueOf(this.zzbur.zzur()));
      } while (this.zzbur.zzux() < i + j);
      return;
    }
    do
    {
      paramList.add(Integer.valueOf(this.zzbur.zzur()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final void zzu(List<Long> paramList)
    throws IOException
  {
    int i;
    int j;
    if ((paramList instanceof zzwh))
    {
      paramList = (zzwh)paramList;
      switch (this.tag & 0x7)
      {
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup();
        zzau(i);
        j = this.zzbur.zzux();
        do
        {
          paramList.zzbg(this.zzbur.zzus());
        } while (this.zzbur.zzux() < i + j);
        return;
      }
      do
      {
        paramList.zzbg(this.zzbur.zzus());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup();
      zzau(i);
      j = this.zzbur.zzux();
      do
      {
        paramList.add(Long.valueOf(this.zzbur.zzus()));
      } while (this.zzbur.zzux() < i + j);
      return;
    }
    do
    {
      paramList.add(Long.valueOf(this.zzbur.zzus()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final long zzuh()
    throws IOException
  {
    zzat(0);
    return this.zzbur.zzuh();
  }
  
  public final long zzui()
    throws IOException
  {
    zzat(0);
    return this.zzbur.zzui();
  }
  
  public final int zzuj()
    throws IOException
  {
    zzat(0);
    return this.zzbur.zzuj();
  }
  
  public final long zzuk()
    throws IOException
  {
    zzat(1);
    return this.zzbur.zzuk();
  }
  
  public final int zzul()
    throws IOException
  {
    zzat(5);
    return this.zzbur.zzul();
  }
  
  public final boolean zzum()
    throws IOException
  {
    zzat(0);
    return this.zzbur.zzum();
  }
  
  public final String zzun()
    throws IOException
  {
    zzat(2);
    return this.zzbur.zzun();
  }
  
  public final zzud zzuo()
    throws IOException
  {
    zzat(2);
    return this.zzbur.zzuo();
  }
  
  public final int zzup()
    throws IOException
  {
    zzat(0);
    return this.zzbur.zzup();
  }
  
  public final int zzuq()
    throws IOException
  {
    zzat(0);
    return this.zzbur.zzuq();
  }
  
  public final int zzur()
    throws IOException
  {
    zzat(5);
    return this.zzbur.zzur();
  }
  
  public final long zzus()
    throws IOException
  {
    zzat(1);
    return this.zzbur.zzus();
  }
  
  public final int zzut()
    throws IOException
  {
    zzat(0);
    return this.zzbur.zzut();
  }
  
  public final long zzuu()
    throws IOException
  {
    zzat(0);
    return this.zzbur.zzuu();
  }
  
  public final void zzv(List<Integer> paramList)
    throws IOException
  {
    int i;
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      switch (this.tag & 0x7)
      {
      case 1: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup() + this.zzbur.zzux();
        do
        {
          paramList.zzbm(this.zzbur.zzut());
        } while (this.zzbur.zzux() < i);
        zzaw(i);
        return;
      }
      do
      {
        paramList.zzbm(this.zzbur.zzut());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 1: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup() + this.zzbur.zzux();
      do
      {
        paramList.add(Integer.valueOf(this.zzbur.zzut()));
      } while (this.zzbur.zzux() < i);
      zzaw(i);
      return;
    }
    do
    {
      paramList.add(Integer.valueOf(this.zzbur.zzut()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
  
  public final int zzve()
    throws IOException
  {
    if (this.zzbut != 0)
    {
      this.tag = this.zzbut;
      this.zzbut = 0;
    }
    while ((this.tag == 0) || (this.tag == this.zzbus))
    {
      return Integer.MAX_VALUE;
      this.tag = this.zzbur.zzug();
    }
    return this.tag >>> 3;
  }
  
  public final boolean zzvf()
    throws IOException
  {
    if ((this.zzbur.zzuw()) || (this.tag == this.zzbus)) {
      return false;
    }
    return this.zzbur.zzao(this.tag);
  }
  
  public final void zzw(List<Long> paramList)
    throws IOException
  {
    int i;
    if ((paramList instanceof zzwh))
    {
      paramList = (zzwh)paramList;
      switch (this.tag & 0x7)
      {
      case 1: 
      default: 
        throw zzvt.zzwo();
      case 2: 
        i = this.zzbur.zzup() + this.zzbur.zzux();
        do
        {
          paramList.zzbg(this.zzbur.zzuu());
        } while (this.zzbur.zzux() < i);
        zzaw(i);
        return;
      }
      do
      {
        paramList.zzbg(this.zzbur.zzuu());
        if (this.zzbur.zzuw()) {
          break;
        }
        i = this.zzbur.zzug();
      } while (i == this.tag);
      this.zzbut = i;
      return;
    }
    switch (this.tag & 0x7)
    {
    case 1: 
    default: 
      throw zzvt.zzwo();
    case 2: 
      i = this.zzbur.zzup() + this.zzbur.zzux();
      do
      {
        paramList.add(Long.valueOf(this.zzbur.zzuu()));
      } while (this.zzbur.zzux() < i);
      zzaw(i);
      return;
    }
    do
    {
      paramList.add(Long.valueOf(this.zzbur.zzuu()));
      if (this.zzbur.zzuw()) {
        break;
      }
      i = this.zzbur.zzug();
    } while (i == this.tag);
    this.zzbut = i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzur.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */