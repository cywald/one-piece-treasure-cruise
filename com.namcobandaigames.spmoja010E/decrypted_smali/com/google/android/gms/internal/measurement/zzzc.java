package com.google.android.gms.internal.measurement;

public final class zzzc
  implements Cloneable
{
  private static final zzzd zzcff = new zzzd();
  private int mSize;
  private boolean zzcfg = false;
  private int[] zzcfh;
  private zzzd[] zzcfi;
  
  zzzc()
  {
    this(10);
  }
  
  private zzzc(int paramInt)
  {
    paramInt = idealIntArraySize(paramInt);
    this.zzcfh = new int[paramInt];
    this.zzcfi = new zzzd[paramInt];
    this.mSize = 0;
  }
  
  private static int idealIntArraySize(int paramInt)
  {
    int j = paramInt << 2;
    paramInt = 4;
    for (;;)
    {
      int i = j;
      if (paramInt < 32)
      {
        if (j <= (1 << paramInt) - 12) {
          i = (1 << paramInt) - 12;
        }
      }
      else {
        return i / 4;
      }
      paramInt += 1;
    }
  }
  
  private final int zzcd(int paramInt)
  {
    int j = this.mSize;
    int i = 0;
    j -= 1;
    while (i <= j)
    {
      int k = i + j >>> 1;
      int m = this.zzcfh[k];
      if (m < paramInt)
      {
        i = k + 1;
      }
      else
      {
        j = k;
        if (m <= paramInt) {
          return j;
        }
        j = k - 1;
      }
    }
    j = i ^ 0xFFFFFFFF;
    return j;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    label71:
    label93:
    label131:
    label138:
    label141:
    for (;;)
    {
      return true;
      if (!(paramObject instanceof zzzc)) {
        return false;
      }
      paramObject = (zzzc)paramObject;
      if (this.mSize != ((zzzc)paramObject).mSize) {
        return false;
      }
      Object localObject = this.zzcfh;
      int[] arrayOfInt = ((zzzc)paramObject).zzcfh;
      int j = this.mSize;
      int i = 0;
      if (i < j) {
        if (localObject[i] != arrayOfInt[i])
        {
          i = 0;
          if (i != 0)
          {
            localObject = this.zzcfi;
            paramObject = ((zzzc)paramObject).zzcfi;
            j = this.mSize;
            i = 0;
            if (i >= j) {
              break label138;
            }
            if (localObject[i].equals(paramObject[i])) {
              break label131;
            }
          }
        }
      }
      for (i = 0;; i = 1)
      {
        if (i != 0) {
          break label141;
        }
        return false;
        i += 1;
        break;
        i = 1;
        break label71;
        i += 1;
        break label93;
      }
    }
  }
  
  public final int hashCode()
  {
    int j = 17;
    int i = 0;
    while (i < this.mSize)
    {
      j = (j * 31 + this.zzcfh[i]) * 31 + this.zzcfi[i].hashCode();
      i += 1;
    }
    return j;
  }
  
  public final boolean isEmpty()
  {
    return this.mSize == 0;
  }
  
  final int size()
  {
    return this.mSize;
  }
  
  final void zza(int paramInt, zzzd paramzzzd)
  {
    int i = zzcd(paramInt);
    if (i >= 0)
    {
      this.zzcfi[i] = paramzzzd;
      return;
    }
    i ^= 0xFFFFFFFF;
    if ((i < this.mSize) && (this.zzcfi[i] == zzcff))
    {
      this.zzcfh[i] = paramInt;
      this.zzcfi[i] = paramzzzd;
      return;
    }
    if (this.mSize >= this.zzcfh.length)
    {
      int j = idealIntArraySize(this.mSize + 1);
      int[] arrayOfInt = new int[j];
      zzzd[] arrayOfzzzd = new zzzd[j];
      System.arraycopy(this.zzcfh, 0, arrayOfInt, 0, this.zzcfh.length);
      System.arraycopy(this.zzcfi, 0, arrayOfzzzd, 0, this.zzcfi.length);
      this.zzcfh = arrayOfInt;
      this.zzcfi = arrayOfzzzd;
    }
    if (this.mSize - i != 0)
    {
      System.arraycopy(this.zzcfh, i, this.zzcfh, i + 1, this.mSize - i);
      System.arraycopy(this.zzcfi, i, this.zzcfi, i + 1, this.mSize - i);
    }
    this.zzcfh[i] = paramInt;
    this.zzcfi[i] = paramzzzd;
    this.mSize += 1;
  }
  
  final zzzd zzcb(int paramInt)
  {
    paramInt = zzcd(paramInt);
    if ((paramInt < 0) || (this.zzcfi[paramInt] == zzcff)) {
      return null;
    }
    return this.zzcfi[paramInt];
  }
  
  final zzzd zzcc(int paramInt)
  {
    return this.zzcfi[paramInt];
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */