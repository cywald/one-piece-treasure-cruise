package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class zzvm<MessageType extends zzvm<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>>
  extends zztw<MessageType, BuilderType>
{
  private static Map<Object, zzvm<?, ?>> zzbyo = new ConcurrentHashMap();
  protected zzyc zzbym = zzyc.zzyf();
  private int zzbyn = -1;
  
  static <T extends zzvm<T, ?>> T zza(T paramT, zzuo paramzzuo, zzuz paramzzuz)
    throws zzvt
  {
    paramT = (zzvm)paramT.zza(zze.zzbyw, null, null);
    try
    {
      zzxf.zzxn().zzag(paramT).zza(paramT, zzur.zza(paramzzuo), paramzzuz);
      zzxf.zzxn().zzag(paramT).zzu(paramT);
      return paramT;
    }
    catch (IOException paramzzuo)
    {
      if ((paramzzuo.getCause() instanceof zzvt)) {
        throw ((zzvt)paramzzuo.getCause());
      }
      throw new zzvt(paramzzuo.getMessage()).zzg(paramT);
    }
    catch (RuntimeException paramT)
    {
      if ((paramT.getCause() instanceof zzvt)) {
        throw ((zzvt)paramT.getCause());
      }
      throw paramT;
    }
  }
  
  protected static Object zza(zzwt paramzzwt, String paramString, Object[] paramArrayOfObject)
  {
    return new zzxh(paramzzwt, paramString, paramArrayOfObject);
  }
  
  static Object zza(Method paramMethod, Object paramObject, Object... paramVarArgs)
  {
    try
    {
      paramMethod = paramMethod.invoke(paramObject, paramVarArgs);
      return paramMethod;
    }
    catch (IllegalAccessException paramMethod)
    {
      throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", paramMethod);
    }
    catch (InvocationTargetException paramMethod)
    {
      paramMethod = paramMethod.getCause();
      if ((paramMethod instanceof RuntimeException)) {
        throw ((RuntimeException)paramMethod);
      }
      if ((paramMethod instanceof Error)) {
        throw ((Error)paramMethod);
      }
      throw new RuntimeException("Unexpected exception thrown by generated accessor method.", paramMethod);
    }
  }
  
  protected static <T extends zzvm<?, ?>> void zza(Class<T> paramClass, T paramT)
  {
    zzbyo.put(paramClass, paramT);
  }
  
  protected static final <T extends zzvm<T, ?>> boolean zza(T paramT, boolean paramBoolean)
  {
    int i = ((Byte)paramT.zza(zze.zzbyt, null, null)).byteValue();
    if (i == 1) {
      return true;
    }
    if (i == 0) {
      return false;
    }
    return zzxf.zzxn().zzag(paramT).zzaf(paramT);
  }
  
  static <T extends zzvm<?, ?>> T zzg(Class<T> paramClass)
  {
    zzvm localzzvm2 = (zzvm)zzbyo.get(paramClass);
    zzvm localzzvm1 = localzzvm2;
    if (localzzvm2 == null) {}
    for (;;)
    {
      try
      {
        Class.forName(paramClass.getName(), true, paramClass.getClassLoader());
        localzzvm1 = (zzvm)zzbyo.get(paramClass);
        if (localzzvm1 != null) {
          break;
        }
        paramClass = String.valueOf(paramClass.getName());
        if (paramClass.length() != 0)
        {
          paramClass = "Unable to get default instance for: ".concat(paramClass);
          throw new IllegalStateException(paramClass);
        }
      }
      catch (ClassNotFoundException paramClass)
      {
        throw new IllegalStateException("Class initialization cannot fail.", paramClass);
      }
      paramClass = new String("Unable to get default instance for: ");
    }
    return localzzvm1;
  }
  
  protected static <E> zzvs<E> zzwc()
  {
    return zzxg.zzxo();
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!((zzvm)zza(zze.zzbyy, null, null)).getClass().isInstance(paramObject)) {
      return false;
    }
    return zzxf.zzxn().zzag(this).equals(this, (zzvm)paramObject);
  }
  
  public int hashCode()
  {
    if (this.zzbtr != 0) {
      return this.zzbtr;
    }
    this.zzbtr = zzxf.zzxn().zzag(this).hashCode(this);
    return this.zzbtr;
  }
  
  public final boolean isInitialized()
  {
    boolean bool1 = Boolean.TRUE.booleanValue();
    int i = ((Byte)zza(zze.zzbyt, null, null)).byteValue();
    if (i == 1) {
      return true;
    }
    if (i == 0) {
      return false;
    }
    boolean bool2 = zzxf.zzxn().zzag(this).zzaf(this);
    if (bool1)
    {
      i = zze.zzbyu;
      if (!bool2) {
        break label76;
      }
    }
    label76:
    for (zzvm localzzvm = this;; localzzvm = null)
    {
      zza(i, localzzvm, null);
      return bool2;
    }
  }
  
  public String toString()
  {
    return zzww.zza(this, super.toString());
  }
  
  protected abstract Object zza(int paramInt, Object paramObject1, Object paramObject2);
  
  final void zzah(int paramInt)
  {
    this.zzbyn = paramInt;
  }
  
  public final void zzb(zzut paramzzut)
    throws IOException
  {
    zzxf.zzxn().zzi(getClass()).zza(this, zzuv.zza(paramzzut));
  }
  
  final int zztu()
  {
    return this.zzbyn;
  }
  
  public final int zzvu()
  {
    if (this.zzbyn == -1) {
      this.zzbyn = zzxf.zzxn().zzag(this).zzae(this);
    }
    return this.zzbyn;
  }
  
  public static class zza<MessageType extends zzvm<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>>
    extends zztx<MessageType, BuilderType>
  {
    private final MessageType zzbyp;
    private MessageType zzbyq;
    private boolean zzbyr;
    
    protected zza(MessageType paramMessageType)
    {
      this.zzbyp = paramMessageType;
      this.zzbyq = ((zzvm)paramMessageType.zza(zzvm.zze.zzbyw, null, null));
      this.zzbyr = false;
    }
    
    private static void zza(MessageType paramMessageType1, MessageType paramMessageType2)
    {
      zzxf.zzxn().zzag(paramMessageType1).zzd(paramMessageType1, paramMessageType2);
    }
    
    public final boolean isInitialized()
    {
      return zzvm.zza(this.zzbyq, false);
    }
    
    public final BuilderType zza(MessageType paramMessageType)
    {
      if (this.zzbyr)
      {
        zzvm localzzvm = (zzvm)this.zzbyq.zza(zzvm.zze.zzbyw, null, null);
        zza(localzzvm, this.zzbyq);
        this.zzbyq = localzzvm;
        this.zzbyr = false;
      }
      zza(this.zzbyq, paramMessageType);
      return this;
    }
    
    public MessageType zzwg()
    {
      if (this.zzbyr) {
        return this.zzbyq;
      }
      zzvm localzzvm = this.zzbyq;
      zzxf.zzxn().zzag(localzzvm).zzu(localzzvm);
      this.zzbyr = true;
      return this.zzbyq;
    }
    
    public final MessageType zzwh()
    {
      zzvm localzzvm2 = (zzvm)zzwi();
      boolean bool2 = Boolean.TRUE.booleanValue();
      int i = ((Byte)localzzvm2.zza(zzvm.zze.zzbyt, null, null)).byteValue();
      boolean bool1;
      if (i == 1) {
        bool1 = true;
      }
      while (!bool1)
      {
        throw new zzya(localzzvm2);
        if (i == 0)
        {
          bool1 = false;
        }
        else
        {
          bool1 = zzxf.zzxn().zzag(localzzvm2).zzaf(localzzvm2);
          if (bool2)
          {
            i = zzvm.zze.zzbyu;
            if (!bool1) {
              break label108;
            }
          }
          label108:
          for (zzvm localzzvm1 = localzzvm2;; localzzvm1 = null)
          {
            localzzvm2.zza(i, localzzvm1, null);
            break;
          }
        }
      }
      return localzzvm2;
    }
  }
  
  public static final class zzb<T extends zzvm<T, ?>>
    extends zzty<T>
  {
    private final T zzbyp;
    
    public zzb(T paramT)
    {
      this.zzbyp = paramT;
    }
  }
  
  public static abstract class zzc<MessageType extends zzc<MessageType, BuilderType>, BuilderType>
    extends zzvm<MessageType, BuilderType>
    implements zzwv
  {
    protected zzvd<Object> zzbys = zzvd.zzvt();
  }
  
  public static final class zzd<ContainingType extends zzwt, Type>
    extends zzux<ContainingType, Type>
  {}
  
  public static enum zze
  {
    public static int[] values$50KLMJ33DTMIUPRFDTJMOP9FE1P6UT3FC9QMCBQ7CLN6ASJ1EHIM8JB5EDPM2PR59HKN8P949LIN8Q3FCHA6UIBEEPNMMP9R0()
    {
      return (int[])zzbza.clone();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */