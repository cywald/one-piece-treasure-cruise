package com.google.android.gms.internal.measurement;

public final class zzc
{
  public static final class zza
    extends zzvm<zza, zza>
    implements zzwv
  {
    private static final zza zznv = new zza();
    private static volatile zzxd<zza> zznw;
    private int zznr;
    private int zzns = 1;
    private int zznt;
    private int zznu;
    
    static
    {
      zzvm.zza(zza.class, zznv);
    }
    
    public static zzxd<zza> zza()
    {
      return (zzxd)zznv.zza(zzvm.zze.zzbyz, null, null);
    }
    
    protected final Object zza(int paramInt, Object paramObject1, Object paramObject2)
    {
      switch (zzd.zznq[(paramInt - 1)])
      {
      default: 
        throw new UnsupportedOperationException();
      case 1: 
        paramObject1 = new zza();
      case 2: 
      case 3: 
      case 4: 
      case 5: 
        do
        {
          return paramObject1;
          return new zza(null);
          paramObject1 = zzb.zzd();
          return zza(zznv, "\001\003\000\001\001\003\003\000\000\000\001\f\000\002\004\001\003\004\002", new Object[] { "zznr", "zzns", paramObject1, "zznt", "zznu" });
          return zznv;
          paramObject2 = zznw;
          paramObject1 = paramObject2;
        } while (paramObject2 != null);
        try
        {
          paramObject2 = zznw;
          paramObject1 = paramObject2;
          if (paramObject2 == null)
          {
            paramObject1 = new zzvm.zzb(zznv);
            zznw = (zzxd)paramObject1;
          }
          return paramObject1;
        }
        finally {}
      case 6: 
        return Byte.valueOf((byte)1);
      }
      return null;
    }
    
    public static final class zza
      extends zzvm.zza<zzc.zza, zza>
      implements zzwv
    {
      private zza()
      {
        super();
      }
    }
    
    public static enum zzb
      implements zzvp
    {
      private static final zzvq<zzb> zzoa = new zze();
      private final int value;
      
      private zzb(int paramInt)
      {
        this.value = paramInt;
      }
      
      public static zzb zza(int paramInt)
      {
        switch (paramInt)
        {
        default: 
          return null;
        case 1: 
          return zznx;
        case 2: 
          return zzny;
        }
        return zznz;
      }
      
      public static zzvr zzd()
      {
        return zzf.zzoc;
      }
      
      public final int zzc()
      {
        return this.value;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */