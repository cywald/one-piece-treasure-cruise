package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzj
  extends zzza<zzj>
{
  public zzp[] zzoo = zzp.zzk();
  public zzp[] zzop = zzp.zzk();
  public zzi[] zzoq = zzi.zzg();
  
  public zzj()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzj)) {
        return false;
      }
      paramObject = (zzj)paramObject;
      if (!zzze.equals(this.zzoo, ((zzj)paramObject).zzoo)) {
        return false;
      }
      if (!zzze.equals(this.zzop, ((zzj)paramObject).zzop)) {
        return false;
      }
      if (!zzze.equals(this.zzoq, ((zzj)paramObject).zzoq)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzj)paramObject).zzcfc == null) || (((zzj)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzj)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = zzze.hashCode(this.zzoo);
    int m = zzze.hashCode(this.zzop);
    int n = zzze.hashCode(this.zzoq);
    if ((this.zzcfc == null) || (this.zzcfc.isEmpty())) {}
    for (int i = 0;; i = this.zzcfc.hashCode()) {
      return i + ((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    int j = 0;
    int i;
    Object localObject;
    if ((this.zzoo != null) && (this.zzoo.length > 0))
    {
      i = 0;
      while (i < this.zzoo.length)
      {
        localObject = this.zzoo[i];
        if (localObject != null) {
          paramzzyy.zza(1, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzop != null) && (this.zzop.length > 0))
    {
      i = 0;
      while (i < this.zzop.length)
      {
        localObject = this.zzop[i];
        if (localObject != null) {
          paramzzyy.zza(2, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzoq != null) && (this.zzoq.length > 0))
    {
      i = j;
      while (i < this.zzoq.length)
      {
        localObject = this.zzoq[i];
        if (localObject != null) {
          paramzzyy.zza(3, (zzzg)localObject);
        }
        i += 1;
      }
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int m = 0;
    int i = super.zzf();
    int j = i;
    Object localObject;
    if (this.zzoo != null)
    {
      j = i;
      if (this.zzoo.length > 0)
      {
        j = 0;
        while (j < this.zzoo.length)
        {
          localObject = this.zzoo[j];
          k = i;
          if (localObject != null) {
            k = i + zzyy.zzb(1, (zzzg)localObject);
          }
          j += 1;
          i = k;
        }
        j = i;
      }
    }
    i = j;
    if (this.zzop != null)
    {
      i = j;
      if (this.zzop.length > 0)
      {
        i = j;
        j = 0;
        while (j < this.zzop.length)
        {
          localObject = this.zzop[j];
          k = i;
          if (localObject != null) {
            k = i + zzyy.zzb(2, (zzzg)localObject);
          }
          j += 1;
          i = k;
        }
      }
    }
    int k = i;
    if (this.zzoq != null)
    {
      k = i;
      if (this.zzoq.length > 0)
      {
        j = m;
        for (;;)
        {
          k = i;
          if (j >= this.zzoq.length) {
            break;
          }
          localObject = this.zzoq[j];
          k = i;
          if (localObject != null) {
            k = i + zzyy.zzb(3, (zzzg)localObject);
          }
          j += 1;
          i = k;
        }
      }
    }
    return k;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */