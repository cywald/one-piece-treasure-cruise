package com.google.android.gms.internal.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;

class zzcq
  extends BroadcastReceiver
{
  @VisibleForTesting
  private static final String zzabi = zzcq.class.getName();
  private boolean zzabj;
  private boolean zzabk;
  private final zzaw zzvy;
  
  zzcq(zzaw paramzzaw)
  {
    Preconditions.checkNotNull(paramzzaw);
    this.zzvy = paramzzaw;
  }
  
  private final void zzez()
  {
    this.zzvy.zzby();
    this.zzvy.zzcc();
  }
  
  @VisibleForTesting
  private final boolean zzfb()
  {
    Object localObject = (ConnectivityManager)this.zzvy.getContext().getSystemService("connectivity");
    try
    {
      localObject = ((ConnectivityManager)localObject).getActiveNetworkInfo();
      if (localObject != null)
      {
        boolean bool = ((NetworkInfo)localObject).isConnected();
        if (bool) {
          return true;
        }
      }
      return false;
    }
    catch (SecurityException localSecurityException) {}
    return false;
  }
  
  public final boolean isConnected()
  {
    if (!this.zzabj) {
      this.zzvy.zzby().zzt("Connectivity unknown. Receiver not registered");
    }
    return this.zzabk;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    zzez();
    paramContext = paramIntent.getAction();
    this.zzvy.zzby().zza("NetworkBroadcastReceiver received action", paramContext);
    if ("android.net.conn.CONNECTIVITY_CHANGE".equals(paramContext))
    {
      boolean bool = zzfb();
      if (this.zzabk != bool)
      {
        this.zzabk = bool;
        paramContext = this.zzvy.zzcc();
        paramContext.zza("Network connectivity status changed", Boolean.valueOf(bool));
        paramContext.zzca().zza(new zzan(paramContext, bool));
      }
    }
    do
    {
      return;
      if (!"com.google.analytics.RADIO_POWERED".equals(paramContext)) {
        break;
      }
    } while (paramIntent.hasExtra(zzabi));
    paramContext = this.zzvy.zzcc();
    paramContext.zzq("Radio powered up");
    paramContext.zzbs();
    return;
    this.zzvy.zzby().zzd("NetworkBroadcastReceiver received unknown action", paramContext);
  }
  
  public final void unregister()
  {
    if (!this.zzabj) {
      return;
    }
    this.zzvy.zzby().zzq("Unregistering connectivity change receiver");
    this.zzabj = false;
    this.zzabk = false;
    Context localContext = this.zzvy.getContext();
    try
    {
      localContext.unregisterReceiver(this);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      this.zzvy.zzby().zze("Failed to unregister the network broadcast receiver", localIllegalArgumentException);
    }
  }
  
  public final void zzey()
  {
    zzez();
    if (this.zzabj) {
      return;
    }
    Context localContext = this.zzvy.getContext();
    localContext.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    IntentFilter localIntentFilter = new IntentFilter("com.google.analytics.RADIO_POWERED");
    localIntentFilter.addCategory(localContext.getPackageName());
    localContext.registerReceiver(this, localIntentFilter);
    this.zzabk = zzfb();
    this.zzvy.zzby().zza("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.zzabk));
    this.zzabj = true;
  }
  
  @VisibleForTesting
  public final void zzfa()
  {
    Context localContext = this.zzvy.getContext();
    Intent localIntent = new Intent("com.google.analytics.RADIO_POWERED");
    localIntent.addCategory(localContext.getPackageName());
    localIntent.putExtra(zzabi, true);
    localContext.sendOrderedBroadcast(localIntent, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */