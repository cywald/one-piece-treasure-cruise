package com.google.android.gms.internal.measurement;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;

final class zzta
  extends zzsx
{
  private final zzsy zzbrz = new zzsy();
  
  public final void zza(Throwable paramThrowable, PrintStream paramPrintStream)
  {
    paramThrowable.printStackTrace(paramPrintStream);
    paramThrowable = this.zzbrz.zza(paramThrowable, false);
    if (paramThrowable == null) {
      return;
    }
    try
    {
      Iterator localIterator = paramThrowable.iterator();
      while (localIterator.hasNext())
      {
        Throwable localThrowable = (Throwable)localIterator.next();
        paramPrintStream.print("Suppressed: ");
        localThrowable.printStackTrace(paramPrintStream);
      }
    }
    finally {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzta.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */