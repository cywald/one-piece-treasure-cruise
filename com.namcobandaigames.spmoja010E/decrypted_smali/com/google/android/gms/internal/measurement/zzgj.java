package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgj
  extends zzza<zzgj>
{
  public long[] zzaye = zzzj.zzcfr;
  public long[] zzayf = zzzj.zzcfr;
  public zzge[] zzayg = zzge.zzmp();
  public zzgk[] zzayh = zzgk.zzmt();
  
  public zzgj()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgj)) {
        return false;
      }
      paramObject = (zzgj)paramObject;
      if (!zzze.equals(this.zzaye, ((zzgj)paramObject).zzaye)) {
        return false;
      }
      if (!zzze.equals(this.zzayf, ((zzgj)paramObject).zzayf)) {
        return false;
      }
      if (!zzze.equals(this.zzayg, ((zzgj)paramObject).zzayg)) {
        return false;
      }
      if (!zzze.equals(this.zzayh, ((zzgj)paramObject).zzayh)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgj)paramObject).zzcfc == null) || (((zzgj)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgj)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = zzze.hashCode(this.zzaye);
    int m = zzze.hashCode(this.zzayf);
    int n = zzze.hashCode(this.zzayg);
    int i1 = zzze.hashCode(this.zzayh);
    if ((this.zzcfc == null) || (this.zzcfc.isEmpty())) {}
    for (int i = 0;; i = this.zzcfc.hashCode()) {
      return i + (((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    int j = 0;
    int i;
    if ((this.zzaye != null) && (this.zzaye.length > 0))
    {
      i = 0;
      while (i < this.zzaye.length)
      {
        paramzzyy.zza(1, this.zzaye[i]);
        i += 1;
      }
    }
    if ((this.zzayf != null) && (this.zzayf.length > 0))
    {
      i = 0;
      while (i < this.zzayf.length)
      {
        paramzzyy.zza(2, this.zzayf[i]);
        i += 1;
      }
    }
    Object localObject;
    if ((this.zzayg != null) && (this.zzayg.length > 0))
    {
      i = 0;
      while (i < this.zzayg.length)
      {
        localObject = this.zzayg[i];
        if (localObject != null) {
          paramzzyy.zza(3, (zzzg)localObject);
        }
        i += 1;
      }
    }
    if ((this.zzayh != null) && (this.zzayh.length > 0))
    {
      i = j;
      while (i < this.zzayh.length)
      {
        localObject = this.zzayh[i];
        if (localObject != null) {
          paramzzyy.zza(4, (zzzg)localObject);
        }
        i += 1;
      }
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int m = 0;
    int k = super.zzf();
    int j;
    if ((this.zzaye != null) && (this.zzaye.length > 0))
    {
      i = 0;
      j = 0;
      while (i < this.zzaye.length)
      {
        j += zzyy.zzbi(this.zzaye[i]);
        i += 1;
      }
    }
    for (int i = k + j + this.zzaye.length * 1;; i = k)
    {
      j = i;
      if (this.zzayf != null)
      {
        j = i;
        if (this.zzayf.length > 0)
        {
          j = 0;
          k = 0;
          while (j < this.zzayf.length)
          {
            k += zzyy.zzbi(this.zzayf[j]);
            j += 1;
          }
          j = i + k + this.zzayf.length * 1;
        }
      }
      i = j;
      Object localObject;
      if (this.zzayg != null)
      {
        i = j;
        if (this.zzayg.length > 0)
        {
          i = j;
          j = 0;
          while (j < this.zzayg.length)
          {
            localObject = this.zzayg[j];
            k = i;
            if (localObject != null) {
              k = i + zzyy.zzb(3, (zzzg)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      k = i;
      if (this.zzayh != null)
      {
        k = i;
        if (this.zzayh.length > 0)
        {
          j = m;
          for (;;)
          {
            k = i;
            if (j >= this.zzayh.length) {
              break;
            }
            localObject = this.zzayh[j];
            k = i;
            if (localObject != null) {
              k = i + zzyy.zzb(4, (zzzg)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      return k;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */