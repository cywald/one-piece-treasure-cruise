package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

final class zzxl
{
  private static final Class<?> zzcbw = ;
  private static final zzyb<?, ?> zzcbx = zzx(false);
  private static final zzyb<?, ?> zzcby = zzx(true);
  private static final zzyb<?, ?> zzcbz = new zzyd();
  
  static <UT, UB> UB zza(int paramInt1, int paramInt2, UB paramUB, zzyb<UT, UB> paramzzyb)
  {
    Object localObject = paramUB;
    if (paramUB == null) {
      localObject = paramzzyb.zzye();
    }
    paramzzyb.zza(localObject, paramInt1, paramInt2);
    return (UB)localObject;
  }
  
  static <UT, UB> UB zza(int paramInt, List<Integer> paramList, zzvr paramzzvr, UB paramUB, zzyb<UT, UB> paramzzyb)
  {
    if (paramzzvr == null) {
      return paramUB;
    }
    int i;
    if ((paramList instanceof RandomAccess))
    {
      int k = paramList.size();
      int j = 0;
      i = 0;
      if (j < k)
      {
        int m = ((Integer)paramList.get(j)).intValue();
        if (paramzzvr.zzb(m))
        {
          if (j != i) {
            paramList.set(i, Integer.valueOf(m));
          }
          i += 1;
        }
        for (;;)
        {
          j += 1;
          break;
          paramUB = zza(paramInt, m, paramUB, paramzzyb);
        }
      }
      paramzzvr = paramUB;
      if (i != k) {
        paramList.subList(i, k).clear();
      }
    }
    for (paramzzvr = paramUB;; paramzzvr = paramUB)
    {
      return paramzzvr;
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        i = ((Integer)paramList.next()).intValue();
        if (!paramzzvr.zzb(i))
        {
          paramUB = zza(paramInt, i, paramUB, paramzzyb);
          paramList.remove();
        }
      }
    }
  }
  
  public static void zza(int paramInt, List<String> paramList, zzyw paramzzyw)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zza(paramInt, paramList);
    }
  }
  
  public static void zza(int paramInt, List<?> paramList, zzyw paramzzyw, zzxj paramzzxj)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zza(paramInt, paramList, paramzzxj);
    }
  }
  
  public static void zza(int paramInt, List<Double> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzg(paramInt, paramList, paramBoolean);
    }
  }
  
  static <T, FT extends zzvf<FT>> void zza(zzva<FT> paramzzva, T paramT1, T paramT2)
  {
    paramT2 = paramzzva.zzs(paramT2);
    if (!paramT2.isEmpty()) {
      paramzzva.zzt(paramT1).zza(paramT2);
    }
  }
  
  static <T> void zza(zzwo paramzzwo, T paramT1, T paramT2, long paramLong)
  {
    zzyh.zza(paramT1, paramLong, paramzzwo.zzc(zzyh.zzp(paramT1, paramLong), zzyh.zzp(paramT2, paramLong)));
  }
  
  static <T, UT, UB> void zza(zzyb<UT, UB> paramzzyb, T paramT1, T paramT2)
  {
    paramzzyb.zzf(paramT1, paramzzyb.zzh(paramzzyb.zzah(paramT1), paramzzyb.zzah(paramT2)));
  }
  
  static int zzaa(List<Integer> paramList)
  {
    int i = 0;
    int k = 0;
    int m = paramList.size();
    if (m == 0) {
      return k;
    }
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      j = 0;
      for (;;)
      {
        k = i;
        if (j >= m) {
          break;
        }
        k = zzut.zzbh(paramList.getInt(j));
        j += 1;
        i = k + i;
      }
    }
    i = 0;
    int j = 0;
    while (i < m)
    {
      j += zzut.zzbh(((Integer)paramList.get(i)).intValue());
      i += 1;
    }
    return j;
  }
  
  static int zzab(List<Integer> paramList)
  {
    int i = 0;
    int k = 0;
    int m = paramList.size();
    if (m == 0) {
      return k;
    }
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      j = 0;
      for (;;)
      {
        k = i;
        if (j >= m) {
          break;
        }
        k = zzut.zzbc(paramList.getInt(j));
        j += 1;
        i = k + i;
      }
    }
    i = 0;
    int j = 0;
    while (i < m)
    {
      j += zzut.zzbc(((Integer)paramList.get(i)).intValue());
      i += 1;
    }
    return j;
  }
  
  static int zzac(List<Integer> paramList)
  {
    int i = 0;
    int k = 0;
    int m = paramList.size();
    if (m == 0) {
      return k;
    }
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      j = 0;
      for (;;)
      {
        k = i;
        if (j >= m) {
          break;
        }
        k = zzut.zzbd(paramList.getInt(j));
        j += 1;
        i = k + i;
      }
    }
    i = 0;
    int j = 0;
    while (i < m)
    {
      j += zzut.zzbd(((Integer)paramList.get(i)).intValue());
      i += 1;
    }
    return j;
  }
  
  static int zzad(List<Integer> paramList)
  {
    int i = 0;
    int k = 0;
    int m = paramList.size();
    if (m == 0) {
      return k;
    }
    if ((paramList instanceof zzvn))
    {
      paramList = (zzvn)paramList;
      j = 0;
      for (;;)
      {
        k = i;
        if (j >= m) {
          break;
        }
        k = zzut.zzbe(paramList.getInt(j));
        j += 1;
        i = k + i;
      }
    }
    i = 0;
    int j = 0;
    while (i < m)
    {
      j += zzut.zzbe(((Integer)paramList.get(i)).intValue());
      i += 1;
    }
    return j;
  }
  
  static int zzae(List<?> paramList)
  {
    return paramList.size() << 2;
  }
  
  static int zzaf(List<?> paramList)
  {
    return paramList.size() << 3;
  }
  
  static int zzag(List<?> paramList)
  {
    return paramList.size();
  }
  
  public static void zzb(int paramInt, List<zzud> paramList, zzyw paramzzyw)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzb(paramInt, paramList);
    }
  }
  
  public static void zzb(int paramInt, List<?> paramList, zzyw paramzzyw, zzxj paramzzxj)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzb(paramInt, paramList, paramzzxj);
    }
  }
  
  public static void zzb(int paramInt, List<Float> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzf(paramInt, paramList, paramBoolean);
    }
  }
  
  static int zzc(int paramInt, Object paramObject, zzxj paramzzxj)
  {
    if ((paramObject instanceof zzwa)) {
      return zzut.zza(paramInt, (zzwa)paramObject);
    }
    return zzut.zzb(paramInt, (zzwt)paramObject, paramzzxj);
  }
  
  static int zzc(int paramInt, List<?> paramList)
  {
    int j = paramList.size();
    if (j == 0) {
      return 0;
    }
    paramInt = zzut.zzbb(paramInt) * j;
    Object localObject;
    if ((paramList instanceof zzwc))
    {
      paramList = (zzwc)paramList;
      i = 0;
      if (i < j)
      {
        localObject = paramList.getRaw(i);
        if ((localObject instanceof zzud)) {}
        for (paramInt = zzut.zzb((zzud)localObject) + paramInt;; paramInt = zzut.zzfx((String)localObject) + paramInt)
        {
          i += 1;
          break;
        }
      }
      return paramInt;
    }
    int i = 0;
    if (i < j)
    {
      localObject = paramList.get(i);
      if ((localObject instanceof zzud)) {}
      for (paramInt = zzut.zzb((zzud)localObject) + paramInt;; paramInt = zzut.zzfx((String)localObject) + paramInt)
      {
        i += 1;
        break;
      }
    }
    return paramInt;
  }
  
  static int zzc(int paramInt, List<?> paramList, zzxj paramzzxj)
  {
    int j = paramList.size();
    if (j == 0) {
      return 0;
    }
    paramInt = zzut.zzbb(paramInt) * j;
    int i = 0;
    if (i < j)
    {
      Object localObject = paramList.get(i);
      if ((localObject instanceof zzwa)) {}
      for (paramInt = zzut.zza((zzwa)localObject) + paramInt;; paramInt = zzut.zzb((zzwt)localObject, paramzzxj) + paramInt)
      {
        i += 1;
        break;
      }
    }
    return paramInt;
  }
  
  public static void zzc(int paramInt, List<Long> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzc(paramInt, paramList, paramBoolean);
    }
  }
  
  static int zzd(int paramInt, List<zzud> paramList)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    i *= zzut.zzbb(paramInt);
    paramInt = 0;
    while (paramInt < paramList.size())
    {
      i += zzut.zzb((zzud)paramList.get(paramInt));
      paramInt += 1;
    }
    return i;
  }
  
  static int zzd(int paramInt, List<zzwt> paramList, zzxj paramzzxj)
  {
    int k = paramList.size();
    if (k == 0) {
      return 0;
    }
    int i = 0;
    int j = 0;
    while (i < k)
    {
      j += zzut.zzc(paramInt, (zzwt)paramList.get(i), paramzzxj);
      i += 1;
    }
    return j;
  }
  
  public static void zzd(int paramInt, List<Long> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzd(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zze(int paramInt, List<Long> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzn(paramInt, paramList, paramBoolean);
    }
  }
  
  static boolean zze(Object paramObject1, Object paramObject2)
  {
    return (paramObject1 == paramObject2) || ((paramObject1 != null) && (paramObject1.equals(paramObject2)));
  }
  
  public static void zzf(int paramInt, List<Long> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zze(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zzg(int paramInt, List<Long> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzl(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zzh(int paramInt, List<Integer> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zza(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zzi(int paramInt, List<Integer> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzj(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zzj(int paramInt, List<Integer> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzm(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zzj(Class<?> paramClass)
  {
    if ((!zzvm.class.isAssignableFrom(paramClass)) && (zzcbw != null) && (!zzcbw.isAssignableFrom(paramClass))) {
      throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
    }
  }
  
  public static void zzk(int paramInt, List<Integer> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzb(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zzl(int paramInt, List<Integer> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzk(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zzm(int paramInt, List<Integer> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzh(paramInt, paramList, paramBoolean);
    }
  }
  
  public static void zzn(int paramInt, List<Boolean> paramList, zzyw paramzzyw, boolean paramBoolean)
    throws IOException
  {
    if ((paramList != null) && (!paramList.isEmpty())) {
      paramzzyw.zzi(paramInt, paramList, paramBoolean);
    }
  }
  
  static int zzo(int paramInt, List<Long> paramList, boolean paramBoolean)
  {
    if (paramList.size() == 0) {
      return 0;
    }
    return zzx(paramList) + paramList.size() * zzut.zzbb(paramInt);
  }
  
  static int zzp(int paramInt, List<Long> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    int j = zzy(paramList);
    return i * zzut.zzbb(paramInt) + j;
  }
  
  static int zzq(int paramInt, List<Long> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    int j = zzz(paramList);
    return i * zzut.zzbb(paramInt) + j;
  }
  
  static int zzr(int paramInt, List<Integer> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    int j = zzaa(paramList);
    return i * zzut.zzbb(paramInt) + j;
  }
  
  static int zzs(int paramInt, List<Integer> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    int j = zzab(paramList);
    return i * zzut.zzbb(paramInt) + j;
  }
  
  static int zzt(int paramInt, List<Integer> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    int j = zzac(paramList);
    return i * zzut.zzbb(paramInt) + j;
  }
  
  static int zzu(int paramInt, List<Integer> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    int j = zzad(paramList);
    return i * zzut.zzbb(paramInt) + j;
  }
  
  static int zzv(int paramInt, List<?> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    return zzut.zzk(paramInt, 0) * i;
  }
  
  static int zzw(int paramInt, List<?> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    return i * zzut.zzg(paramInt, 0L);
  }
  
  static int zzx(int paramInt, List<?> paramList, boolean paramBoolean)
  {
    int i = paramList.size();
    if (i == 0) {
      return 0;
    }
    return i * zzut.zzc(paramInt, true);
  }
  
  static int zzx(List<Long> paramList)
  {
    int i = 0;
    int k = 0;
    int m = paramList.size();
    if (m == 0) {
      return k;
    }
    if ((paramList instanceof zzwh))
    {
      paramList = (zzwh)paramList;
      j = 0;
      for (;;)
      {
        k = i;
        if (j >= m) {
          break;
        }
        k = zzut.zzay(paramList.getLong(j));
        j += 1;
        i = k + i;
      }
    }
    i = 0;
    int j = 0;
    while (i < m)
    {
      j += zzut.zzay(((Long)paramList.get(i)).longValue());
      i += 1;
    }
    return j;
  }
  
  private static zzyb<?, ?> zzx(boolean paramBoolean)
  {
    try
    {
      Object localObject = zzxv();
      if (localObject == null) {
        return null;
      }
      localObject = (zzyb)((Class)localObject).getConstructor(new Class[] { Boolean.TYPE }).newInstance(new Object[] { Boolean.valueOf(paramBoolean) });
      return (zzyb<?, ?>)localObject;
    }
    catch (Throwable localThrowable) {}
    return null;
  }
  
  public static zzyb<?, ?> zzxr()
  {
    return zzcbx;
  }
  
  public static zzyb<?, ?> zzxs()
  {
    return zzcby;
  }
  
  public static zzyb<?, ?> zzxt()
  {
    return zzcbz;
  }
  
  private static Class<?> zzxu()
  {
    try
    {
      Class localClass = Class.forName("com.google.protobuf.GeneratedMessage");
      return localClass;
    }
    catch (Throwable localThrowable) {}
    return null;
  }
  
  private static Class<?> zzxv()
  {
    try
    {
      Class localClass = Class.forName("com.google.protobuf.UnknownFieldSetSchema");
      return localClass;
    }
    catch (Throwable localThrowable) {}
    return null;
  }
  
  static int zzy(List<Long> paramList)
  {
    int i = 0;
    int k = 0;
    int m = paramList.size();
    if (m == 0) {
      return k;
    }
    if ((paramList instanceof zzwh))
    {
      paramList = (zzwh)paramList;
      j = 0;
      for (;;)
      {
        k = i;
        if (j >= m) {
          break;
        }
        k = zzut.zzaz(paramList.getLong(j));
        j += 1;
        i = k + i;
      }
    }
    i = 0;
    int j = 0;
    while (i < m)
    {
      j += zzut.zzaz(((Long)paramList.get(i)).longValue());
      i += 1;
    }
    return j;
  }
  
  static int zzz(List<Long> paramList)
  {
    int i = 0;
    int k = 0;
    int m = paramList.size();
    if (m == 0) {
      return k;
    }
    if ((paramList instanceof zzwh))
    {
      paramList = (zzwh)paramList;
      j = 0;
      for (;;)
      {
        k = i;
        if (j >= m) {
          break;
        }
        k = zzut.zzba(paramList.getLong(j));
        j += 1;
        i = k + i;
      }
    }
    i = 0;
    int j = 0;
    while (i < m)
    {
      j += zzut.zzba(((Long)paramList.get(i)).longValue());
      i += 1;
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */