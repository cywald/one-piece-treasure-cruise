package com.google.android.gms.internal.measurement;

import android.os.Looper;
import com.google.android.gms.analytics.zzk;

final class zzca
  implements Runnable
{
  zzca(zzbz paramzzbz) {}
  
  public final void run()
  {
    if (Looper.myLooper() == Looper.getMainLooper()) {
      zzbz.zza(this.zzyq).zzca().zza(this);
    }
    boolean bool;
    do
    {
      return;
      bool = this.zzyq.zzej();
      zzbz.zza(this.zzyq, 0L);
    } while (!bool);
    this.zzyq.run();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzca.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */