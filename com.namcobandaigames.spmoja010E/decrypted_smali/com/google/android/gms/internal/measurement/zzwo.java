package com.google.android.gms.internal.measurement;

import java.util.Map;

abstract interface zzwo
{
  public abstract boolean zzaa(Object paramObject);
  
  public abstract Object zzab(Object paramObject);
  
  public abstract Object zzac(Object paramObject);
  
  public abstract zzwm<?, ?> zzad(Object paramObject);
  
  public abstract int zzb(int paramInt, Object paramObject1, Object paramObject2);
  
  public abstract Object zzc(Object paramObject1, Object paramObject2);
  
  public abstract Map<?, ?> zzy(Object paramObject);
  
  public abstract Map<?, ?> zzz(Object paramObject);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */