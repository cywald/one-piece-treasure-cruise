package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

final class zzwy<T>
  implements zzxj<T>
{
  private final zzwt zzcbd;
  private final boolean zzcbe;
  private final zzyb<?, ?> zzcbn;
  private final zzva<?> zzcbo;
  
  private zzwy(zzyb<?, ?> paramzzyb, zzva<?> paramzzva, zzwt paramzzwt)
  {
    this.zzcbn = paramzzyb;
    this.zzcbe = paramzzva.zze(paramzzwt);
    this.zzcbo = paramzzva;
    this.zzcbd = paramzzwt;
  }
  
  static <T> zzwy<T> zza(zzyb<?, ?> paramzzyb, zzva<?> paramzzva, zzwt paramzzwt)
  {
    return new zzwy(paramzzyb, paramzzva, paramzzwt);
  }
  
  public final boolean equals(T paramT1, T paramT2)
  {
    if (!this.zzcbn.zzah(paramT1).equals(this.zzcbn.zzah(paramT2))) {
      return false;
    }
    if (this.zzcbe) {
      return this.zzcbo.zzs(paramT1).equals(this.zzcbo.zzs(paramT2));
    }
    return true;
  }
  
  public final int hashCode(T paramT)
  {
    int j = this.zzcbn.zzah(paramT).hashCode();
    int i = j;
    if (this.zzcbe) {
      i = j * 53 + this.zzcbo.zzs(paramT).hashCode();
    }
    return i;
  }
  
  public final T newInstance()
  {
    return this.zzcbd.zzwe().zzwi();
  }
  
  public final void zza(T paramT, zzxi paramzzxi, zzuz paramzzuz)
    throws IOException
  {
    zzyb localzzyb = this.zzcbn;
    zzva localzzva = this.zzcbo;
    Object localObject2 = localzzyb.zzai(paramT);
    zzvd localzzvd = localzzva.zzt(paramT);
    for (;;)
    {
      int i;
      Object localObject1;
      try
      {
        i = paramzzxi.zzve();
        if (i == Integer.MAX_VALUE) {
          return;
        }
        i = paramzzxi.getTag();
        if (i == 11) {
          break label311;
        }
        if ((i & 0x7) == 2)
        {
          localObject1 = localzzva.zza(paramzzuz, this.zzcbd, i >>> 3);
          if (localObject1 != null)
          {
            localzzva.zza(paramzzxi, localObject1, paramzzuz, localzzvd);
            bool = true;
            if (bool) {
              continue;
            }
            return;
          }
          bool = localzzyb.zza(localObject2, paramzzxi);
          continue;
        }
        boolean bool = paramzzxi.zzvf();
        continue;
        if (paramzzxi.zzve() == Integer.MAX_VALUE) {
          break label257;
        }
        int j = paramzzxi.getTag();
        if (j == 16)
        {
          i = paramzzxi.zzup();
          localObject1 = localzzva.zza(paramzzuz, this.zzcbd, i);
          continue;
        }
        if (j != 26) {
          break label248;
        }
        if (localObject1 != null)
        {
          localzzva.zza(paramzzxi, localObject1, paramzzuz, localzzvd);
          continue;
        }
        localzzud = paramzzxi.zzuo();
      }
      finally
      {
        localzzyb.zzg(paramT, localObject2);
      }
      zzud localzzud;
      continue;
      label248:
      if (!paramzzxi.zzvf())
      {
        label257:
        if (paramzzxi.getTag() != 12) {
          throw zzvt.zzwn();
        }
        if (localzzud != null) {
          if (localObject1 != null)
          {
            localzzva.zza(localzzud, localObject1, paramzzuz, localzzvd);
          }
          else
          {
            localzzyb.zza(localObject2, i, localzzud);
            continue;
            label311:
            localzzud = null;
            i = 0;
            localObject1 = null;
          }
        }
      }
    }
  }
  
  public final void zza(T paramT, zzyw paramzzyw)
    throws IOException
  {
    Object localObject = this.zzcbo.zzs(paramT).iterator();
    while (((Iterator)localObject).hasNext())
    {
      Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
      zzvf localzzvf = (zzvf)localEntry.getKey();
      if ((localzzvf.zzvx() != zzyv.zzcet) || (localzzvf.zzvy()) || (localzzvf.zzvz())) {
        throw new IllegalStateException("Found invalid MessageSet item.");
      }
      if ((localEntry instanceof zzvy)) {
        paramzzyw.zza(localzzvf.zzc(), ((zzvy)localEntry).zzwu().zztt());
      } else {
        paramzzyw.zza(localzzvf.zzc(), localEntry.getValue());
      }
    }
    localObject = this.zzcbn;
    ((zzyb)localObject).zzc(((zzyb)localObject).zzah(paramT), paramzzyw);
  }
  
  public final int zzae(T paramT)
  {
    zzyb localzzyb = this.zzcbn;
    int j = localzzyb.zzaj(localzzyb.zzah(paramT)) + 0;
    int i = j;
    if (this.zzcbe) {
      i = j + this.zzcbo.zzs(paramT).zzvv();
    }
    return i;
  }
  
  public final boolean zzaf(T paramT)
  {
    return this.zzcbo.zzs(paramT).isInitialized();
  }
  
  public final void zzd(T paramT1, T paramT2)
  {
    zzxl.zza(this.zzcbn, paramT1, paramT2);
    if (this.zzcbe) {
      zzxl.zza(this.zzcbo, paramT1, paramT2);
    }
  }
  
  public final void zzu(T paramT)
  {
    this.zzcbn.zzu(paramT);
    this.zzcbo.zzu(paramT);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */