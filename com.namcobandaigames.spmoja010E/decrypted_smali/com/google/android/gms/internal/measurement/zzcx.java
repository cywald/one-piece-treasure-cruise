package com.google.android.gms.internal.measurement;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.internal.Preconditions;

public final class zzcx<T extends Context,  extends zzdb>
{
  private static Boolean zzabz;
  private final Handler handler;
  private final T zzaby;
  
  public zzcx(T paramT)
  {
    Preconditions.checkNotNull(paramT);
    this.zzaby = paramT;
    this.handler = new zzdx();
  }
  
  private final void zzb(Runnable paramRunnable)
  {
    zzaw.zzc(this.zzaby).zzcc().zza(new zzda(this, paramRunnable));
  }
  
  public static boolean zze(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    if (zzabz != null) {
      return zzabz.booleanValue();
    }
    boolean bool = zzdg.zzc(paramContext, "com.google.android.gms.analytics.AnalyticsService");
    zzabz = Boolean.valueOf(bool);
    return bool;
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public final void onCreate()
  {
    zzaw.zzc(this.zzaby).zzby().zzq("Local AnalyticsService is starting up");
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public final void onDestroy()
  {
    zzaw.zzc(this.zzaby).zzby().zzq("Local AnalyticsService is shutting down");
  }
  
  /* Error */
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public final int onStartCommand(android.content.Intent paramIntent, int paramInt1, int paramInt2)
  {
    // Byte code:
    //   0: getstatic 109	com/google/android/gms/internal/measurement/zzcw:lock	Ljava/lang/Object;
    //   3: astore 4
    //   5: aload 4
    //   7: monitorenter
    //   8: getstatic 113	com/google/android/gms/internal/measurement/zzcw:zzabx	Lcom/google/android/gms/stats/WakeLock;
    //   11: astore 5
    //   13: aload 5
    //   15: ifnull +16 -> 31
    //   18: aload 5
    //   20: invokevirtual 118	com/google/android/gms/stats/WakeLock:isHeld	()Z
    //   23: ifeq +8 -> 31
    //   26: aload 5
    //   28: invokevirtual 121	com/google/android/gms/stats/WakeLock:release	()V
    //   31: aload 4
    //   33: monitorexit
    //   34: aload_0
    //   35: getfield 25	com/google/android/gms/internal/measurement/zzcx:zzaby	Landroid/content/Context;
    //   38: invokestatic 43	com/google/android/gms/internal/measurement/zzaw:zzc	(Landroid/content/Context;)Lcom/google/android/gms/internal/measurement/zzaw;
    //   41: invokevirtual 87	com/google/android/gms/internal/measurement/zzaw:zzby	()Lcom/google/android/gms/internal/measurement/zzcp;
    //   44: astore 4
    //   46: aload_1
    //   47: ifnonnull +25 -> 72
    //   50: aload 4
    //   52: ldc 123
    //   54: invokevirtual 126	com/google/android/gms/internal/measurement/zzat:zzt	(Ljava/lang/String;)V
    //   57: iconst_2
    //   58: ireturn
    //   59: astore 5
    //   61: aload 4
    //   63: monitorexit
    //   64: aload 5
    //   66: athrow
    //   67: astore 4
    //   69: goto -35 -> 34
    //   72: aload_1
    //   73: invokevirtual 132	android/content/Intent:getAction	()Ljava/lang/String;
    //   76: astore_1
    //   77: aload 4
    //   79: ldc -122
    //   81: iload_3
    //   82: invokestatic 139	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   85: aload_1
    //   86: invokevirtual 142	com/google/android/gms/internal/measurement/zzat:zza	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   89: ldc -112
    //   91: aload_1
    //   92: invokevirtual 150	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   95: ifeq -38 -> 57
    //   98: aload_0
    //   99: new 152	com/google/android/gms/internal/measurement/zzcy
    //   102: dup
    //   103: aload_0
    //   104: iload_3
    //   105: aload 4
    //   107: invokespecial 155	com/google/android/gms/internal/measurement/zzcy:<init>	(Lcom/google/android/gms/internal/measurement/zzcx;ILcom/google/android/gms/internal/measurement/zzcp;)V
    //   110: invokespecial 157	com/google/android/gms/internal/measurement/zzcx:zzb	(Ljava/lang/Runnable;)V
    //   113: iconst_2
    //   114: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	115	0	this	zzcx
    //   0	115	1	paramIntent	android.content.Intent
    //   0	115	2	paramInt1	int
    //   0	115	3	paramInt2	int
    //   67	39	4	localSecurityException	SecurityException
    //   11	16	5	localWakeLock	com.google.android.gms.stats.WakeLock
    //   59	6	5	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   8	13	59	finally
    //   18	31	59	finally
    //   31	34	59	finally
    //   61	64	59	finally
    //   0	8	67	java/lang/SecurityException
    //   64	67	67	java/lang/SecurityException
  }
  
  @TargetApi(24)
  public final boolean onStartJob(JobParameters paramJobParameters)
  {
    zzcp localzzcp = zzaw.zzc(this.zzaby).zzby();
    String str = paramJobParameters.getExtras().getString("action");
    localzzcp.zza("Local AnalyticsJobService called. action", str);
    if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(str)) {
      zzb(new zzcz(this, localzzcp, paramJobParameters));
    }
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */