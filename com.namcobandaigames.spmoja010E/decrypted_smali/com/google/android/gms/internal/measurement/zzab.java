package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzi;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class zzab
  extends zzi<zzab>
{
  private final Map<String, Object> zzsy = new HashMap();
  
  public final void set(String paramString1, String paramString2)
  {
    Preconditions.checkNotEmpty(paramString1);
    String str = paramString1;
    if (paramString1 != null)
    {
      str = paramString1;
      if (paramString1.startsWith("&")) {
        str = paramString1.substring(1);
      }
    }
    Preconditions.checkNotEmpty(str, "Name can not be empty or \"&\"");
    this.zzsy.put(str, paramString2);
  }
  
  public final String toString()
  {
    return zza(this.zzsy);
  }
  
  public final Map<String, Object> zzaw()
  {
    return Collections.unmodifiableMap(this.zzsy);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */