package com.google.android.gms.internal.measurement;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.HttpUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.Closeable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class zzbf
  extends zzau
  implements Closeable
{
  private static final String zzxf = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", new Object[] { "hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id" });
  private static final String zzxg = String.format("SELECT MAX(%s) FROM %s WHERE 1;", new Object[] { "hit_time", "hits2" });
  private final zzbg zzxh;
  private final zzdc zzxi = new zzdc(zzbx());
  private final zzdc zzxj = new zzdc(zzbx());
  
  zzbf(zzaw paramzzaw)
  {
    super(paramzzaw);
    this.zzxh = new zzbg(this, paramzzaw.getContext(), "google_analytics_v4.db");
  }
  
  private final long zza(String paramString, String[] paramArrayOfString)
  {
    Object localObject1 = null;
    paramArrayOfString = null;
    Object localObject2 = getWritableDatabase();
    try
    {
      localObject2 = ((SQLiteDatabase)localObject2).rawQuery(paramString, null);
      paramArrayOfString = (String[])localObject2;
      localObject1 = localObject2;
      if (((Cursor)localObject2).moveToFirst())
      {
        paramArrayOfString = (String[])localObject2;
        localObject1 = localObject2;
        long l = ((Cursor)localObject2).getLong(0);
        return l;
      }
      paramArrayOfString = (String[])localObject2;
      localObject1 = localObject2;
      throw new SQLiteException("Database returned empty set");
    }
    catch (SQLiteException localSQLiteException)
    {
      localObject1 = paramArrayOfString;
      zzd("Database error", paramString, localSQLiteException);
      localObject1 = paramArrayOfString;
      throw localSQLiteException;
    }
    finally
    {
      if (localObject1 != null) {
        ((Cursor)localObject1).close();
      }
    }
  }
  
  /* Error */
  private final long zza(String paramString, String[] paramArrayOfString, long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 87	com/google/android/gms/internal/measurement/zzbf:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   4: astore 7
    //   6: aconst_null
    //   7: astore 5
    //   9: aconst_null
    //   10: astore 6
    //   12: aload 7
    //   14: aload_1
    //   15: aload_2
    //   16: invokevirtual 93	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   19: astore_2
    //   20: aload_2
    //   21: invokeinterface 99 1 0
    //   26: ifeq +23 -> 49
    //   29: aload_2
    //   30: iconst_0
    //   31: invokeinterface 103 2 0
    //   36: lstore_3
    //   37: aload_2
    //   38: ifnull +9 -> 47
    //   41: aload_2
    //   42: invokeinterface 106 1 0
    //   47: lload_3
    //   48: lreturn
    //   49: aload_2
    //   50: ifnull +9 -> 59
    //   53: aload_2
    //   54: invokeinterface 106 1 0
    //   59: lconst_0
    //   60: lreturn
    //   61: astore 5
    //   63: aload 6
    //   65: astore_2
    //   66: aload 5
    //   68: astore 6
    //   70: aload_2
    //   71: astore 5
    //   73: aload_0
    //   74: ldc 113
    //   76: aload_1
    //   77: aload 6
    //   79: invokevirtual 117	com/google/android/gms/internal/measurement/zzat:zzd	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   82: aload_2
    //   83: astore 5
    //   85: aload 6
    //   87: athrow
    //   88: astore_1
    //   89: aload 5
    //   91: ifnull +10 -> 101
    //   94: aload 5
    //   96: invokeinterface 106 1 0
    //   101: aload_1
    //   102: athrow
    //   103: astore_1
    //   104: aload_2
    //   105: astore 5
    //   107: goto -18 -> 89
    //   110: astore 6
    //   112: goto -42 -> 70
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	115	0	this	zzbf
    //   0	115	1	paramString	String
    //   0	115	2	paramArrayOfString	String[]
    //   0	115	3	paramLong	long
    //   7	1	5	localObject	Object
    //   61	6	5	localSQLiteException1	SQLiteException
    //   71	35	5	arrayOfString	String[]
    //   10	76	6	localSQLiteException2	SQLiteException
    //   110	1	6	localSQLiteException3	SQLiteException
    //   4	9	7	localSQLiteDatabase	SQLiteDatabase
    // Exception table:
    //   from	to	target	type
    //   12	20	61	android/database/sqlite/SQLiteException
    //   12	20	88	finally
    //   73	82	88	finally
    //   85	88	88	finally
    //   20	37	103	finally
    //   20	37	110	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  private final List<Long> zzc(long paramLong)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: invokestatic 132	com/google/android/gms/analytics/zzk:zzaf	()V
    //   6: aload_0
    //   7: invokevirtual 135	com/google/android/gms/internal/measurement/zzau:zzcl	()V
    //   10: lload_1
    //   11: lconst_0
    //   12: lcmp
    //   13: ifgt +7 -> 20
    //   16: invokestatic 141	java/util/Collections:emptyList	()Ljava/util/List;
    //   19: areturn
    //   20: aload_0
    //   21: invokevirtual 87	com/google/android/gms/internal/measurement/zzbf:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   24: astore 4
    //   26: new 143	java/util/ArrayList
    //   29: dup
    //   30: invokespecial 145	java/util/ArrayList:<init>	()V
    //   33: astore 7
    //   35: ldc -109
    //   37: iconst_1
    //   38: anewarray 20	java/lang/Object
    //   41: dup
    //   42: iconst_0
    //   43: ldc 24
    //   45: aastore
    //   46: invokestatic 38	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   49: astore 6
    //   51: lload_1
    //   52: invokestatic 153	java/lang/Long:toString	(J)Ljava/lang/String;
    //   55: astore 8
    //   57: aload 4
    //   59: ldc 22
    //   61: iconst_1
    //   62: anewarray 34	java/lang/String
    //   65: dup
    //   66: iconst_0
    //   67: ldc 24
    //   69: aastore
    //   70: aconst_null
    //   71: aconst_null
    //   72: aconst_null
    //   73: aconst_null
    //   74: aload 6
    //   76: aload 8
    //   78: invokevirtual 157	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   81: astore 4
    //   83: aload 4
    //   85: astore 5
    //   87: aload 5
    //   89: astore 4
    //   91: aload 5
    //   93: invokeinterface 99 1 0
    //   98: ifeq +42 -> 140
    //   101: aload 5
    //   103: astore 4
    //   105: aload 7
    //   107: aload 5
    //   109: iconst_0
    //   110: invokeinterface 103 2 0
    //   115: invokestatic 161	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   118: invokeinterface 167 2 0
    //   123: pop
    //   124: aload 5
    //   126: astore 4
    //   128: aload 5
    //   130: invokeinterface 170 1 0
    //   135: istore_3
    //   136: iload_3
    //   137: ifne -36 -> 101
    //   140: aload 5
    //   142: ifnull +10 -> 152
    //   145: aload 5
    //   147: invokeinterface 106 1 0
    //   152: aload 7
    //   154: areturn
    //   155: astore 6
    //   157: aconst_null
    //   158: astore 5
    //   160: aload 5
    //   162: astore 4
    //   164: aload_0
    //   165: ldc -84
    //   167: aload 6
    //   169: invokevirtual 175	com/google/android/gms/internal/measurement/zzat:zzd	(Ljava/lang/String;Ljava/lang/Object;)V
    //   172: aload 5
    //   174: ifnull -22 -> 152
    //   177: aload 5
    //   179: invokeinterface 106 1 0
    //   184: goto -32 -> 152
    //   187: astore 4
    //   189: aload 5
    //   191: ifnull +10 -> 201
    //   194: aload 5
    //   196: invokeinterface 106 1 0
    //   201: aload 4
    //   203: athrow
    //   204: astore 6
    //   206: aload 4
    //   208: astore 5
    //   210: aload 6
    //   212: astore 4
    //   214: goto -25 -> 189
    //   217: astore 6
    //   219: goto -59 -> 160
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	222	0	this	zzbf
    //   0	222	1	paramLong	long
    //   135	2	3	bool	boolean
    //   24	139	4	localObject1	Object
    //   187	20	4	localObject2	Object
    //   212	1	4	localObject3	Object
    //   1	208	5	localObject4	Object
    //   49	26	6	str1	String
    //   155	13	6	localSQLiteException1	SQLiteException
    //   204	7	6	localObject5	Object
    //   217	1	6	localSQLiteException2	SQLiteException
    //   33	120	7	localArrayList	ArrayList
    //   55	22	8	str2	String
    // Exception table:
    //   from	to	target	type
    //   35	83	155	android/database/sqlite/SQLiteException
    //   35	83	187	finally
    //   91	101	204	finally
    //   105	124	204	finally
    //   128	136	204	finally
    //   164	172	204	finally
    //   91	101	217	android/database/sqlite/SQLiteException
    //   105	124	217	android/database/sqlite/SQLiteException
    //   128	136	217	android/database/sqlite/SQLiteException
  }
  
  private final long zzcv()
  {
    zzk.zzaf();
    zzcl();
    return zza("SELECT COUNT(*) FROM hits2", null);
  }
  
  private static String zzdd()
  {
    return "google_analytics_v4.db";
  }
  
  @VisibleForTesting
  private final Map<String, String> zzv(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return new HashMap(0);
    }
    try
    {
      if (paramString.startsWith("?")) {}
      for (;;)
      {
        return HttpUtils.parse(new URI(paramString), "UTF-8");
        paramString = String.valueOf(paramString);
        if (paramString.length() != 0) {
          paramString = "?".concat(paramString);
        } else {
          paramString = new String("?");
        }
      }
      return new HashMap(0);
    }
    catch (URISyntaxException paramString)
    {
      zze("Error parsing hit parameters", paramString);
    }
  }
  
  @VisibleForTesting
  private final Map<String, String> zzw(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return new HashMap(0);
    }
    try
    {
      paramString = String.valueOf(paramString);
      if (paramString.length() != 0) {}
      for (paramString = "?".concat(paramString);; paramString = new String("?")) {
        return HttpUtils.parse(new URI(paramString), "UTF-8");
      }
      return new HashMap(0);
    }
    catch (URISyntaxException paramString)
    {
      zze("Error parsing property parameters", paramString);
    }
  }
  
  public final void beginTransaction()
  {
    zzcl();
    getWritableDatabase().beginTransaction();
  }
  
  public final void close()
  {
    try
    {
      this.zzxh.close();
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      zze("Sql error closing database", localSQLiteException);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      zze("Error closing database", localIllegalStateException);
    }
  }
  
  public final void endTransaction()
  {
    zzcl();
    getWritableDatabase().endTransaction();
  }
  
  @VisibleForTesting
  final SQLiteDatabase getWritableDatabase()
  {
    try
    {
      SQLiteDatabase localSQLiteDatabase = this.zzxh.getWritableDatabase();
      return localSQLiteDatabase;
    }
    catch (SQLiteException localSQLiteException)
    {
      zzd("Error opening database", localSQLiteException);
      throw localSQLiteException;
    }
  }
  
  final boolean isEmpty()
  {
    return zzcv() == 0L;
  }
  
  public final void setTransactionSuccessful()
  {
    zzcl();
    getWritableDatabase().setTransactionSuccessful();
  }
  
  public final long zza(long paramLong, String paramString1, String paramString2)
  {
    Preconditions.checkNotEmpty(paramString1);
    Preconditions.checkNotEmpty(paramString2);
    zzcl();
    zzk.zzaf();
    return zza("SELECT hits_count FROM properties WHERE app_uid=? AND cid=? AND tid=?", new String[] { String.valueOf(paramLong), paramString1, paramString2 }, 0L);
  }
  
  public final void zza(List<Long> paramList)
  {
    Preconditions.checkNotNull(paramList);
    zzk.zzaf();
    zzcl();
    if (paramList.isEmpty()) {}
    for (;;)
    {
      return;
      Object localObject1 = new StringBuilder("hit_id");
      ((StringBuilder)localObject1).append(" in (");
      int i = 0;
      Object localObject2;
      while (i < paramList.size())
      {
        localObject2 = (Long)paramList.get(i);
        if ((localObject2 == null) || (((Long)localObject2).longValue() == 0L)) {
          throw new SQLiteException("Invalid hit id");
        }
        if (i > 0) {
          ((StringBuilder)localObject1).append(",");
        }
        ((StringBuilder)localObject1).append(localObject2);
        i += 1;
      }
      ((StringBuilder)localObject1).append(")");
      localObject1 = ((StringBuilder)localObject1).toString();
      try
      {
        localObject2 = getWritableDatabase();
        zza("Deleting dispatched hits. count", Integer.valueOf(paramList.size()));
        i = ((SQLiteDatabase)localObject2).delete("hits2", (String)localObject1, null);
        if (i == paramList.size()) {
          continue;
        }
        zzb("Deleted fewer hits then expected", Integer.valueOf(paramList.size()), Integer.valueOf(i), localObject1);
        return;
      }
      catch (SQLiteException paramList)
      {
        zze("Error deleting hits", paramList);
        throw paramList;
      }
    }
  }
  
  protected final void zzag() {}
  
  public final void zzc(zzck paramzzck)
  {
    Preconditions.checkNotNull(paramzzck);
    zzk.zzaf();
    zzcl();
    Preconditions.checkNotNull(paramzzck);
    Object localObject1 = new Uri.Builder();
    Object localObject2 = paramzzck.zzcw().entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      String str = (String)((Map.Entry)localObject3).getKey();
      if ((!"ht".equals(str)) && (!"qt".equals(str)) && (!"AppUID".equals(str))) {
        ((Uri.Builder)localObject1).appendQueryParameter(str, (String)((Map.Entry)localObject3).getValue());
      }
    }
    localObject1 = ((Uri.Builder)localObject1).build().getEncodedQuery();
    if (localObject1 == null) {
      localObject1 = "";
    }
    while (((String)localObject1).length() > 8192)
    {
      zzby().zza(paramzzck, "Hit length exceeds the maximum allowed size");
      return;
    }
    int i = ((Integer)zzcf.zzza.get()).intValue();
    long l = zzcv();
    if (l > i - 1)
    {
      localObject2 = zzc(l - i + 1L);
      zzd("Store full, deleting hits to make room, count", Integer.valueOf(((List)localObject2).size()));
      zza((List)localObject2);
    }
    localObject2 = getWritableDatabase();
    Object localObject3 = new ContentValues();
    ((ContentValues)localObject3).put("hit_string", (String)localObject1);
    ((ContentValues)localObject3).put("hit_time", Long.valueOf(paramzzck.zzer()));
    ((ContentValues)localObject3).put("hit_app_id", Integer.valueOf(paramzzck.zzep()));
    if (paramzzck.zzet()) {}
    for (localObject1 = zzbx.zzed();; localObject1 = zzbx.zzee())
    {
      ((ContentValues)localObject3).put("hit_url", (String)localObject1);
      try
      {
        l = ((SQLiteDatabase)localObject2).insert("hits2", null, (ContentValues)localObject3);
        if (l != -1L) {
          break;
        }
        zzu("Failed to insert a hit (got -1)");
        return;
      }
      catch (SQLiteException paramzzck)
      {
        zze("Error storing a hit", paramzzck);
        return;
      }
    }
    zzb("Hit saved to database. db-id, hit", Long.valueOf(l), paramzzck);
  }
  
  public final List<zzck> zzd(long paramLong)
  {
    boolean bool = true;
    Cursor localCursor = null;
    if (paramLong >= 0L) {}
    for (;;)
    {
      Preconditions.checkArgument(bool);
      zzk.zzaf();
      zzcl();
      Object localObject5 = getWritableDatabase();
      localObject1 = localCursor;
      for (;;)
      {
        try
        {
          str1 = String.format("%s ASC", new Object[] { "hit_id" });
          localObject1 = localCursor;
          str2 = Long.toString(paramLong);
          localObject1 = localCursor;
          localCursor = ((SQLiteDatabase)localObject5).query("hits2", new String[] { "hit_id", "hit_time", "hit_string", "hit_url", "hit_app_id" }, null, null, null, null, str1, str2);
          localObject1 = localCursor;
        }
        catch (SQLiteException localSQLiteException1)
        {
          String str1;
          String str2;
          localObject1 = null;
          try
          {
            zze("Error loading hits from the database", localSQLiteException1);
            throw localSQLiteException1;
          }
          finally
          {
            if (localObject1 != null) {
              ((Cursor)localObject1).close();
            }
          }
        }
        finally
        {
          continue;
        }
        try
        {
          localObject5 = new ArrayList();
          localObject1 = localCursor;
          if (localCursor.moveToFirst())
          {
            localObject1 = localCursor;
            paramLong = localCursor.getLong(0);
            localObject1 = localCursor;
            long l = localCursor.getLong(1);
            localObject1 = localCursor;
            str1 = localCursor.getString(2);
            localObject1 = localCursor;
            str2 = localCursor.getString(3);
            localObject1 = localCursor;
            int i = localCursor.getInt(4);
            localObject1 = localCursor;
            ((List)localObject5).add(new zzck(this, zzv(str1), l, zzdg.zzah(str2), paramLong, i));
            localObject1 = localCursor;
            bool = localCursor.moveToNext();
            if (bool) {
              continue;
            }
          }
          if (localCursor != null) {
            localCursor.close();
          }
          return (List<zzck>)localObject5;
        }
        catch (SQLiteException localSQLiteException2)
        {
          localObject1 = localObject3;
          Object localObject4 = localSQLiteException2;
        }
      }
      bool = false;
    }
  }
  
  public final int zzdb()
  {
    zzk.zzaf();
    zzcl();
    if (!this.zzxi.zzj(86400000L)) {
      return 0;
    }
    this.zzxi.start();
    zzq("Deleting stale hits (if any)");
    int i = getWritableDatabase().delete("hits2", "hit_time < ?", new String[] { Long.toString(zzbx().currentTimeMillis() - 2592000000L) });
    zza("Deleted stale hits, count", Integer.valueOf(i));
    return i;
  }
  
  public final long zzdc()
  {
    zzk.zzaf();
    zzcl();
    return zza(zzxg, null, 0L);
  }
  
  public final void zze(long paramLong)
  {
    zzk.zzaf();
    zzcl();
    ArrayList localArrayList = new ArrayList(1);
    localArrayList.add(Long.valueOf(paramLong));
    zza("Deleting hit, id", Long.valueOf(paramLong));
    zza(localArrayList);
  }
  
  /* Error */
  public final List<zzaz> zzf(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 135	com/google/android/gms/internal/measurement/zzau:zzcl	()V
    //   4: invokestatic 132	com/google/android/gms/analytics/zzk:zzaf	()V
    //   7: aload_0
    //   8: invokevirtual 87	com/google/android/gms/internal/measurement/zzbf:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   11: astore 7
    //   13: aconst_null
    //   14: astore 6
    //   16: aload 6
    //   18: astore 5
    //   20: getstatic 531	com/google/android/gms/internal/measurement/zzcf:zzzc	Lcom/google/android/gms/internal/measurement/zzcg;
    //   23: invokevirtual 415	com/google/android/gms/internal/measurement/zzcg:get	()Ljava/lang/Object;
    //   26: checkcast 313	java/lang/Integer
    //   29: invokevirtual 418	java/lang/Integer:intValue	()I
    //   32: istore_3
    //   33: aload 6
    //   35: astore 5
    //   37: aload 7
    //   39: ldc_w 533
    //   42: iconst_5
    //   43: anewarray 34	java/lang/String
    //   46: dup
    //   47: iconst_0
    //   48: ldc_w 535
    //   51: aastore
    //   52: dup
    //   53: iconst_1
    //   54: ldc_w 537
    //   57: aastore
    //   58: dup
    //   59: iconst_2
    //   60: ldc_w 539
    //   63: aastore
    //   64: dup
    //   65: iconst_3
    //   66: ldc_w 541
    //   69: aastore
    //   70: dup
    //   71: iconst_4
    //   72: ldc_w 543
    //   75: aastore
    //   76: ldc_w 545
    //   79: iconst_1
    //   80: anewarray 34	java/lang/String
    //   83: dup
    //   84: iconst_0
    //   85: ldc_w 547
    //   88: aastore
    //   89: aconst_null
    //   90: aconst_null
    //   91: aconst_null
    //   92: iload_3
    //   93: invokestatic 549	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   96: invokevirtual 157	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   99: astore 6
    //   101: aload 6
    //   103: astore 5
    //   105: new 143	java/util/ArrayList
    //   108: dup
    //   109: invokespecial 145	java/util/ArrayList:<init>	()V
    //   112: astore 7
    //   114: aload 6
    //   116: astore 5
    //   118: aload 6
    //   120: invokeinterface 99 1 0
    //   125: ifeq +134 -> 259
    //   128: aload 6
    //   130: astore 5
    //   132: aload 6
    //   134: iconst_0
    //   135: invokeinterface 479 2 0
    //   140: astore 8
    //   142: aload 6
    //   144: astore 5
    //   146: aload 6
    //   148: iconst_1
    //   149: invokeinterface 479 2 0
    //   154: astore 9
    //   156: aload 6
    //   158: astore 5
    //   160: aload 6
    //   162: iconst_2
    //   163: invokeinterface 483 2 0
    //   168: ifeq +132 -> 300
    //   171: iconst_1
    //   172: istore 4
    //   174: aload 6
    //   176: astore 5
    //   178: aload 6
    //   180: iconst_3
    //   181: invokeinterface 483 2 0
    //   186: i2l
    //   187: lstore_1
    //   188: aload 6
    //   190: astore 5
    //   192: aload_0
    //   193: aload 6
    //   195: iconst_4
    //   196: invokeinterface 479 2 0
    //   201: invokespecial 551	com/google/android/gms/internal/measurement/zzbf:zzw	(Ljava/lang/String;)Ljava/util/Map;
    //   204: astore 10
    //   206: aload 6
    //   208: astore 5
    //   210: aload 8
    //   212: invokestatic 195	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   215: ifne +15 -> 230
    //   218: aload 6
    //   220: astore 5
    //   222: aload 9
    //   224: invokestatic 195	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   227: ifeq +79 -> 306
    //   230: aload 6
    //   232: astore 5
    //   234: aload_0
    //   235: ldc_w 553
    //   238: aload 8
    //   240: aload 9
    //   242: invokevirtual 555	com/google/android/gms/internal/measurement/zzat:zzc	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   245: aload 6
    //   247: astore 5
    //   249: aload 6
    //   251: invokeinterface 170 1 0
    //   256: ifne -128 -> 128
    //   259: aload 6
    //   261: astore 5
    //   263: aload 7
    //   265: invokeinterface 291 1 0
    //   270: iload_3
    //   271: if_icmplt +14 -> 285
    //   274: aload 6
    //   276: astore 5
    //   278: aload_0
    //   279: ldc_w 557
    //   282: invokevirtual 560	com/google/android/gms/internal/measurement/zzat:zzt	(Ljava/lang/String;)V
    //   285: aload 6
    //   287: ifnull +10 -> 297
    //   290: aload 6
    //   292: invokeinterface 106 1 0
    //   297: aload 7
    //   299: areturn
    //   300: iconst_0
    //   301: istore 4
    //   303: goto -129 -> 174
    //   306: aload 6
    //   308: astore 5
    //   310: aload 7
    //   312: new 562	com/google/android/gms/internal/measurement/zzaz
    //   315: dup
    //   316: lconst_0
    //   317: aload 8
    //   319: aload 9
    //   321: iload 4
    //   323: lload_1
    //   324: aload 10
    //   326: invokespecial 565	com/google/android/gms/internal/measurement/zzaz:<init>	(JLjava/lang/String;Ljava/lang/String;ZJLjava/util/Map;)V
    //   329: invokeinterface 167 2 0
    //   334: pop
    //   335: goto -90 -> 245
    //   338: astore 7
    //   340: aload 6
    //   342: astore 5
    //   344: aload 7
    //   346: astore 6
    //   348: aload_0
    //   349: ldc_w 495
    //   352: aload 6
    //   354: invokevirtual 234	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   357: aload 6
    //   359: athrow
    //   360: astore 6
    //   362: aload 5
    //   364: ifnull +10 -> 374
    //   367: aload 5
    //   369: invokeinterface 106 1 0
    //   374: aload 6
    //   376: athrow
    //   377: astore 6
    //   379: goto -17 -> 362
    //   382: astore 6
    //   384: aconst_null
    //   385: astore 5
    //   387: goto -39 -> 348
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	390	0	this	zzbf
    //   0	390	1	paramLong	long
    //   32	240	3	i	int
    //   172	150	4	bool	boolean
    //   18	368	5	localObject1	Object
    //   14	344	6	localObject2	Object
    //   360	15	6	localObject3	Object
    //   377	1	6	localObject4	Object
    //   382	1	6	localSQLiteException1	SQLiteException
    //   11	300	7	localObject5	Object
    //   338	7	7	localSQLiteException2	SQLiteException
    //   140	178	8	str1	String
    //   154	166	9	str2	String
    //   204	121	10	localMap	Map
    // Exception table:
    //   from	to	target	type
    //   105	114	338	android/database/sqlite/SQLiteException
    //   118	128	338	android/database/sqlite/SQLiteException
    //   132	142	338	android/database/sqlite/SQLiteException
    //   146	156	338	android/database/sqlite/SQLiteException
    //   160	171	338	android/database/sqlite/SQLiteException
    //   178	188	338	android/database/sqlite/SQLiteException
    //   192	206	338	android/database/sqlite/SQLiteException
    //   210	218	338	android/database/sqlite/SQLiteException
    //   222	230	338	android/database/sqlite/SQLiteException
    //   234	245	338	android/database/sqlite/SQLiteException
    //   249	259	338	android/database/sqlite/SQLiteException
    //   263	274	338	android/database/sqlite/SQLiteException
    //   278	285	338	android/database/sqlite/SQLiteException
    //   310	335	338	android/database/sqlite/SQLiteException
    //   348	360	360	finally
    //   20	33	377	finally
    //   37	101	377	finally
    //   105	114	377	finally
    //   118	128	377	finally
    //   132	142	377	finally
    //   146	156	377	finally
    //   160	171	377	finally
    //   178	188	377	finally
    //   192	206	377	finally
    //   210	218	377	finally
    //   222	230	377	finally
    //   234	245	377	finally
    //   249	259	377	finally
    //   263	274	377	finally
    //   278	285	377	finally
    //   310	335	377	finally
    //   20	33	382	android/database/sqlite/SQLiteException
    //   37	101	382	android/database/sqlite/SQLiteException
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */