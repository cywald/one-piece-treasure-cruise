package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.internal.Preconditions;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

final class zzcs
{
  private int zzabo;
  private ByteArrayOutputStream zzabp = new ByteArrayOutputStream();
  
  public zzcs(zzcr paramzzcr) {}
  
  public final byte[] getPayload()
  {
    return this.zzabp.toByteArray();
  }
  
  public final boolean zze(zzck paramzzck)
  {
    Preconditions.checkNotNull(paramzzck);
    if (this.zzabo + 1 > zzbx.zzec()) {
      return false;
    }
    Object localObject = this.zzabq.zza(paramzzck, false);
    if (localObject == null)
    {
      this.zzabq.zzby().zza(paramzzck, "Error formatting hit");
      return true;
    }
    localObject = ((String)localObject).getBytes();
    int j = localObject.length;
    if (j > zzbx.zzdy())
    {
      this.zzabq.zzby().zza(paramzzck, "Hit size exceeds the maximum size limit");
      return true;
    }
    int i = j;
    if (this.zzabp.size() > 0) {
      i = j + 1;
    }
    if (this.zzabp.size() + i > ((Integer)zzcf.zzzv.get()).intValue()) {
      return false;
    }
    try
    {
      if (this.zzabp.size() > 0) {
        this.zzabp.write(zzcr.zzfd());
      }
      this.zzabp.write((byte[])localObject);
      this.zzabo += 1;
      return true;
    }
    catch (IOException paramzzck)
    {
      this.zzabq.zze("Failed to write payload when batching hits", paramzzck);
    }
    return true;
  }
  
  public final int zzfe()
  {
    return this.zzabo;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */