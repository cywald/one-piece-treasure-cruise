package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzo
  extends zzza<zzo>
{
  public zzn[] zzqf = zzn.zzj();
  public zzl zzqg = null;
  public String zzqh = "";
  
  public zzo()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzo)) {
        return false;
      }
      paramObject = (zzo)paramObject;
      if (!zzze.equals(this.zzqf, ((zzo)paramObject).zzqf)) {
        return false;
      }
      if (this.zzqg == null)
      {
        if (((zzo)paramObject).zzqg != null) {
          return false;
        }
      }
      else if (!this.zzqg.equals(((zzo)paramObject).zzqg)) {
        return false;
      }
      if (this.zzqh == null)
      {
        if (((zzo)paramObject).zzqh != null) {
          return false;
        }
      }
      else if (!this.zzqh.equals(((zzo)paramObject).zzqh)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzo)paramObject).zzcfc == null) || (((zzo)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzo)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int m = 0;
    int n = getClass().getName().hashCode();
    int i1 = zzze.hashCode(this.zzqf);
    zzl localzzl = this.zzqg;
    int i;
    int j;
    if (localzzl == null)
    {
      i = 0;
      if (this.zzqh != null) {
        break label106;
      }
      j = 0;
      label46:
      k = m;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label117;
        }
      }
    }
    label106:
    label117:
    for (int k = m;; k = this.zzcfc.hashCode())
    {
      return (j + (i + ((n + 527) * 31 + i1) * 31) * 31) * 31 + k;
      i = localzzl.hashCode();
      break;
      j = this.zzqh.hashCode();
      break label46;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if ((this.zzqf != null) && (this.zzqf.length > 0))
    {
      int i = 0;
      while (i < this.zzqf.length)
      {
        zzn localzzn = this.zzqf[i];
        if (localzzn != null) {
          paramzzyy.zza(1, localzzn);
        }
        i += 1;
      }
    }
    if (this.zzqg != null) {
      paramzzyy.zza(2, this.zzqg);
    }
    if ((this.zzqh != null) && (!this.zzqh.equals(""))) {
      paramzzyy.zzb(3, this.zzqh);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int i = super.zzf();
    int j = i;
    if (this.zzqf != null)
    {
      j = i;
      if (this.zzqf.length > 0)
      {
        int k = 0;
        for (;;)
        {
          j = i;
          if (k >= this.zzqf.length) {
            break;
          }
          zzn localzzn = this.zzqf[k];
          j = i;
          if (localzzn != null) {
            j = i + zzyy.zzb(1, localzzn);
          }
          k += 1;
          i = j;
        }
      }
    }
    i = j;
    if (this.zzqg != null) {
      i = j + zzyy.zzb(2, this.zzqg);
    }
    j = i;
    if (this.zzqh != null)
    {
      j = i;
      if (!this.zzqh.equals("")) {
        j = i + zzyy.zzc(3, this.zzqh);
      }
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */