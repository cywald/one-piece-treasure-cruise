package com.google.android.gms.internal.measurement;

import java.io.IOException;

abstract interface zzxj<T>
{
  public abstract boolean equals(T paramT1, T paramT2);
  
  public abstract int hashCode(T paramT);
  
  public abstract T newInstance();
  
  public abstract void zza(T paramT, zzxi paramzzxi, zzuz paramzzuz)
    throws IOException;
  
  public abstract void zza(T paramT, zzyw paramzzyw)
    throws IOException;
  
  public abstract int zzae(T paramT);
  
  public abstract boolean zzaf(T paramT);
  
  public abstract void zzd(T paramT1, T paramT2);
  
  public abstract void zzu(T paramT);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */