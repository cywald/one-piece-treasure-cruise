package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzwl<K, V>
{
  static <K, V> int zza(zzwm<K, V> paramzzwm, K paramK, V paramV)
  {
    return zzvd.zza(paramzzwm.zzcar, 1, paramK) + zzvd.zza(paramzzwm.zzcat, 2, paramV);
  }
  
  static <K, V> void zza(zzut paramzzut, zzwm<K, V> paramzzwm, K paramK, V paramV)
    throws IOException
  {
    zzvd.zza(paramzzut, paramzzwm.zzcar, 1, paramK);
    zzvd.zza(paramzzut, paramzzwm.zzcat, 2, paramV);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */