package com.google.android.gms.internal.measurement;

import java.util.List;

final class zzwg
  extends zzwd
{
  private zzwg()
  {
    super(null);
  }
  
  private static <E> zzvs<E> zzd(Object paramObject, long paramLong)
  {
    return (zzvs)zzyh.zzp(paramObject, paramLong);
  }
  
  final <L> List<L> zza(Object paramObject, long paramLong)
  {
    zzvs localzzvs = zzd(paramObject, paramLong);
    if (!localzzvs.zztw())
    {
      int i = localzzvs.size();
      if (i == 0) {
        i = 10;
      }
      for (;;)
      {
        localzzvs = localzzvs.zzak(i);
        zzyh.zza(paramObject, paramLong, localzzvs);
        return localzzvs;
        i <<= 1;
      }
    }
    return localzzvs;
  }
  
  final <E> void zza(Object paramObject1, Object paramObject2, long paramLong)
  {
    zzvs localzzvs1 = zzd(paramObject1, paramLong);
    zzvs localzzvs2 = zzd(paramObject2, paramLong);
    int i = localzzvs1.size();
    int j = localzzvs2.size();
    paramObject2 = localzzvs1;
    if (i > 0)
    {
      paramObject2 = localzzvs1;
      if (j > 0)
      {
        paramObject2 = localzzvs1;
        if (!localzzvs1.zztw()) {
          paramObject2 = localzzvs1.zzak(j + i);
        }
        ((zzvs)paramObject2).addAll(localzzvs2);
      }
    }
    if (i > 0) {}
    for (;;)
    {
      zzyh.zza(paramObject1, paramLong, paramObject2);
      return;
      paramObject2 = localzzvs2;
    }
  }
  
  final void zzb(Object paramObject, long paramLong)
  {
    zzd(paramObject, paramLong).zzsm();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */