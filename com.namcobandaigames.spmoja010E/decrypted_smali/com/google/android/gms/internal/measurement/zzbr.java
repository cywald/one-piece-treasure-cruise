package com.google.android.gms.internal.measurement;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class zzbr
  implements Parcelable
{
  @Deprecated
  public static final Parcelable.Creator<zzbr> CREATOR = new zzbs();
  private String value;
  private String zzno;
  private String zzyf;
  
  @Deprecated
  public zzbr() {}
  
  @Deprecated
  zzbr(Parcel paramParcel)
  {
    this.zzno = paramParcel.readString();
    this.zzyf = paramParcel.readString();
    this.value = paramParcel.readString();
  }
  
  @Deprecated
  public final int describeContents()
  {
    return 0;
  }
  
  public final String getId()
  {
    return this.zzno;
  }
  
  public final String getValue()
  {
    return this.value;
  }
  
  @Deprecated
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.zzno);
    paramParcel.writeString(this.zzyf);
    paramParcel.writeString(this.value);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */