package com.google.android.gms.internal.measurement;

import android.annotation.SuppressLint;
import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;

@SuppressLint({"StaticFieldLeak"})
public class zzaw
{
  private static volatile zzaw zzwb;
  private final Context zzri;
  private final Clock zzrz;
  private final Context zzwc;
  private final zzbx zzwd;
  private final zzcp zzwe;
  private final zzk zzwf;
  private final zzal zzwg;
  private final zzcc zzwh;
  private final zzdh zzwi;
  private final zzct zzwj;
  private final GoogleAnalytics zzwk;
  private final zzbo zzwl;
  private final zzak zzwm;
  private final zzbh zzwn;
  private final zzcb zzwo;
  
  private zzaw(zzay paramzzay)
  {
    Object localObject1 = paramzzay.getApplicationContext();
    Preconditions.checkNotNull(localObject1, "Application context can't be null");
    Object localObject2 = paramzzay.zzcm();
    Preconditions.checkNotNull(localObject2);
    this.zzri = ((Context)localObject1);
    this.zzwc = ((Context)localObject2);
    this.zzrz = DefaultClock.getInstance();
    this.zzwd = new zzbx(this);
    localObject2 = new zzcp(this);
    ((zzau)localObject2).zzq();
    this.zzwe = ((zzcp)localObject2);
    localObject2 = zzby();
    Object localObject3 = zzav.VERSION;
    ((zzat)localObject2).zzs(String.valueOf(localObject3).length() + 134 + "Google Analytics " + (String)localObject3 + " is starting up. To enable debug logging on a device run:\n  adb shell setprop log.tag.GAv4 DEBUG\n  adb logcat -s GAv4");
    localObject2 = new zzct(this);
    ((zzau)localObject2).zzq();
    this.zzwj = ((zzct)localObject2);
    localObject2 = new zzdh(this);
    ((zzau)localObject2).zzq();
    this.zzwi = ((zzdh)localObject2);
    paramzzay = new zzal(this, paramzzay);
    localObject2 = new zzbo(this);
    localObject3 = new zzak(this);
    zzbh localzzbh = new zzbh(this);
    zzcb localzzcb = new zzcb(this);
    localObject1 = zzk.zzb((Context)localObject1);
    ((zzk)localObject1).zza(new zzax(this));
    this.zzwf = ((zzk)localObject1);
    localObject1 = new GoogleAnalytics(this);
    ((zzau)localObject2).zzq();
    this.zzwl = ((zzbo)localObject2);
    ((zzau)localObject3).zzq();
    this.zzwm = ((zzak)localObject3);
    localzzbh.zzq();
    this.zzwn = localzzbh;
    localzzcb.zzq();
    this.zzwo = localzzcb;
    localObject2 = new zzcc(this);
    ((zzau)localObject2).zzq();
    this.zzwh = ((zzcc)localObject2);
    paramzzay.zzq();
    this.zzwg = paramzzay;
    ((GoogleAnalytics)localObject1).zzq();
    this.zzwk = ((GoogleAnalytics)localObject1);
    paramzzay.start();
  }
  
  private static void zza(zzau paramzzau)
  {
    Preconditions.checkNotNull(paramzzau, "Analytics service not created/initialized");
    Preconditions.checkArgument(paramzzau.isInitialized(), "Analytics service not initialized");
  }
  
  public static zzaw zzc(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    if (zzwb == null) {}
    try
    {
      if (zzwb == null)
      {
        Clock localClock = DefaultClock.getInstance();
        long l1 = localClock.elapsedRealtime();
        paramContext = new zzaw(new zzay(paramContext));
        zzwb = paramContext;
        GoogleAnalytics.zzr();
        l1 = localClock.elapsedRealtime() - l1;
        long l2 = ((Long)zzcf.zzaal.get()).longValue();
        if (l1 > l2) {
          paramContext.zzby().zzc("Slow initialization (ms)", Long.valueOf(l1), Long.valueOf(l2));
        }
      }
      return zzwb;
    }
    finally {}
  }
  
  public final Context getContext()
  {
    return this.zzri;
  }
  
  public final Clock zzbx()
  {
    return this.zzrz;
  }
  
  public final zzcp zzby()
  {
    zza(this.zzwe);
    return this.zzwe;
  }
  
  public final zzbx zzbz()
  {
    return this.zzwd;
  }
  
  public final zzk zzca()
  {
    Preconditions.checkNotNull(this.zzwf);
    return this.zzwf;
  }
  
  public final zzal zzcc()
  {
    zza(this.zzwg);
    return this.zzwg;
  }
  
  public final zzcc zzcd()
  {
    zza(this.zzwh);
    return this.zzwh;
  }
  
  public final zzdh zzce()
  {
    zza(this.zzwi);
    return this.zzwi;
  }
  
  public final zzct zzcf()
  {
    zza(this.zzwj);
    return this.zzwj;
  }
  
  public final zzbh zzci()
  {
    zza(this.zzwn);
    return this.zzwn;
  }
  
  public final zzcb zzcj()
  {
    return this.zzwo;
  }
  
  public final Context zzcm()
  {
    return this.zzwc;
  }
  
  public final zzcp zzcn()
  {
    return this.zzwe;
  }
  
  public final GoogleAnalytics zzco()
  {
    Preconditions.checkNotNull(this.zzwk);
    Preconditions.checkArgument(this.zzwk.isInitialized(), "Analytics instance not initialized");
    return this.zzwk;
  }
  
  public final zzct zzcp()
  {
    if ((this.zzwj == null) || (!this.zzwj.isInitialized())) {
      return null;
    }
    return this.zzwj;
  }
  
  public final zzak zzcq()
  {
    zza(this.zzwm);
    return this.zzwm;
  }
  
  public final zzbo zzcr()
  {
    zza(this.zzwl);
    return this.zzwl;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzaw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */