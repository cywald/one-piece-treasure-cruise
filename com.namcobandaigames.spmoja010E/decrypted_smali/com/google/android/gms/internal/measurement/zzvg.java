package com.google.android.gms.internal.measurement;

import java.lang.reflect.Type;

public enum zzvg
{
  private static final zzvg[] zzbxx;
  private static final Type[] zzbxy;
  private final int id;
  private final zzvv zzbxt;
  private final zzvi zzbxu;
  private final Class<?> zzbxv;
  private final boolean zzbxw;
  
  static
  {
    int i = 0;
    zzbvu = new zzvg("DOUBLE", 0, 0, zzvi.zzbyc, zzvv.zzbzr);
    zzbvv = new zzvg("FLOAT", 1, 1, zzvi.zzbyc, zzvv.zzbzq);
    zzbvw = new zzvg("INT64", 2, 2, zzvi.zzbyc, zzvv.zzbzp);
    zzbvx = new zzvg("UINT64", 3, 3, zzvi.zzbyc, zzvv.zzbzp);
    zzbvy = new zzvg("INT32", 4, 4, zzvi.zzbyc, zzvv.zzbzo);
    zzbvz = new zzvg("FIXED64", 5, 5, zzvi.zzbyc, zzvv.zzbzp);
    zzbwa = new zzvg("FIXED32", 6, 6, zzvi.zzbyc, zzvv.zzbzo);
    zzbwb = new zzvg("BOOL", 7, 7, zzvi.zzbyc, zzvv.zzbzs);
    zzbwc = new zzvg("STRING", 8, 8, zzvi.zzbyc, zzvv.zzbzt);
    zzbwd = new zzvg("MESSAGE", 9, 9, zzvi.zzbyc, zzvv.zzbzw);
    zzbwe = new zzvg("BYTES", 10, 10, zzvi.zzbyc, zzvv.zzbzu);
    zzbwf = new zzvg("UINT32", 11, 11, zzvi.zzbyc, zzvv.zzbzo);
    zzbwg = new zzvg("ENUM", 12, 12, zzvi.zzbyc, zzvv.zzbzv);
    zzbwh = new zzvg("SFIXED32", 13, 13, zzvi.zzbyc, zzvv.zzbzo);
    zzbwi = new zzvg("SFIXED64", 14, 14, zzvi.zzbyc, zzvv.zzbzp);
    zzbwj = new zzvg("SINT32", 15, 15, zzvi.zzbyc, zzvv.zzbzo);
    zzbwk = new zzvg("SINT64", 16, 16, zzvi.zzbyc, zzvv.zzbzp);
    zzbwl = new zzvg("GROUP", 17, 17, zzvi.zzbyc, zzvv.zzbzw);
    zzbwm = new zzvg("DOUBLE_LIST", 18, 18, zzvi.zzbyd, zzvv.zzbzr);
    zzbwn = new zzvg("FLOAT_LIST", 19, 19, zzvi.zzbyd, zzvv.zzbzq);
    zzbwo = new zzvg("INT64_LIST", 20, 20, zzvi.zzbyd, zzvv.zzbzp);
    zzbwp = new zzvg("UINT64_LIST", 21, 21, zzvi.zzbyd, zzvv.zzbzp);
    zzbwq = new zzvg("INT32_LIST", 22, 22, zzvi.zzbyd, zzvv.zzbzo);
    zzbwr = new zzvg("FIXED64_LIST", 23, 23, zzvi.zzbyd, zzvv.zzbzp);
    zzbws = new zzvg("FIXED32_LIST", 24, 24, zzvi.zzbyd, zzvv.zzbzo);
    zzbwt = new zzvg("BOOL_LIST", 25, 25, zzvi.zzbyd, zzvv.zzbzs);
    zzbwu = new zzvg("STRING_LIST", 26, 26, zzvi.zzbyd, zzvv.zzbzt);
    zzbwv = new zzvg("MESSAGE_LIST", 27, 27, zzvi.zzbyd, zzvv.zzbzw);
    zzbww = new zzvg("BYTES_LIST", 28, 28, zzvi.zzbyd, zzvv.zzbzu);
    zzbwx = new zzvg("UINT32_LIST", 29, 29, zzvi.zzbyd, zzvv.zzbzo);
    zzbwy = new zzvg("ENUM_LIST", 30, 30, zzvi.zzbyd, zzvv.zzbzv);
    zzbwz = new zzvg("SFIXED32_LIST", 31, 31, zzvi.zzbyd, zzvv.zzbzo);
    zzbxa = new zzvg("SFIXED64_LIST", 32, 32, zzvi.zzbyd, zzvv.zzbzp);
    zzbxb = new zzvg("SINT32_LIST", 33, 33, zzvi.zzbyd, zzvv.zzbzo);
    zzbxc = new zzvg("SINT64_LIST", 34, 34, zzvi.zzbyd, zzvv.zzbzp);
    zzbxd = new zzvg("DOUBLE_LIST_PACKED", 35, 35, zzvi.zzbye, zzvv.zzbzr);
    zzbxe = new zzvg("FLOAT_LIST_PACKED", 36, 36, zzvi.zzbye, zzvv.zzbzq);
    zzbxf = new zzvg("INT64_LIST_PACKED", 37, 37, zzvi.zzbye, zzvv.zzbzp);
    zzbxg = new zzvg("UINT64_LIST_PACKED", 38, 38, zzvi.zzbye, zzvv.zzbzp);
    zzbxh = new zzvg("INT32_LIST_PACKED", 39, 39, zzvi.zzbye, zzvv.zzbzo);
    zzbxi = new zzvg("FIXED64_LIST_PACKED", 40, 40, zzvi.zzbye, zzvv.zzbzp);
    zzbxj = new zzvg("FIXED32_LIST_PACKED", 41, 41, zzvi.zzbye, zzvv.zzbzo);
    zzbxk = new zzvg("BOOL_LIST_PACKED", 42, 42, zzvi.zzbye, zzvv.zzbzs);
    zzbxl = new zzvg("UINT32_LIST_PACKED", 43, 43, zzvi.zzbye, zzvv.zzbzo);
    zzbxm = new zzvg("ENUM_LIST_PACKED", 44, 44, zzvi.zzbye, zzvv.zzbzv);
    zzbxn = new zzvg("SFIXED32_LIST_PACKED", 45, 45, zzvi.zzbye, zzvv.zzbzo);
    zzbxo = new zzvg("SFIXED64_LIST_PACKED", 46, 46, zzvi.zzbye, zzvv.zzbzp);
    zzbxp = new zzvg("SINT32_LIST_PACKED", 47, 47, zzvi.zzbye, zzvv.zzbzo);
    zzbxq = new zzvg("SINT64_LIST_PACKED", 48, 48, zzvi.zzbye, zzvv.zzbzp);
    zzbxr = new zzvg("GROUP_LIST", 49, 49, zzvi.zzbyd, zzvv.zzbzw);
    zzbxs = new zzvg("MAP", 50, 50, zzvi.zzbyf, zzvv.zzbzn);
    zzbxz = new zzvg[] { zzbvu, zzbvv, zzbvw, zzbvx, zzbvy, zzbvz, zzbwa, zzbwb, zzbwc, zzbwd, zzbwe, zzbwf, zzbwg, zzbwh, zzbwi, zzbwj, zzbwk, zzbwl, zzbwm, zzbwn, zzbwo, zzbwp, zzbwq, zzbwr, zzbws, zzbwt, zzbwu, zzbwv, zzbww, zzbwx, zzbwy, zzbwz, zzbxa, zzbxb, zzbxc, zzbxd, zzbxe, zzbxf, zzbxg, zzbxh, zzbxi, zzbxj, zzbxk, zzbxl, zzbxm, zzbxn, zzbxo, zzbxp, zzbxq, zzbxr, zzbxs };
    zzbxy = new Type[0];
    zzvg[] arrayOfzzvg = values();
    zzbxx = new zzvg[arrayOfzzvg.length];
    int j = arrayOfzzvg.length;
    while (i < j)
    {
      zzvg localzzvg = arrayOfzzvg[i];
      zzbxx[localzzvg.id] = localzzvg;
      i += 1;
    }
  }
  
  private zzvg(int paramInt, zzvi paramzzvi, zzvv paramzzvv)
  {
    this.id = paramInt;
    this.zzbxu = paramzzvi;
    this.zzbxt = paramzzvv;
    switch (zzvh.zzbya[paramzzvi.ordinal()])
    {
    default: 
      this.zzbxv = null;
    }
    for (;;)
    {
      boolean bool2 = false;
      boolean bool1 = bool2;
      if (paramzzvi == zzvi.zzbyc)
      {
        bool1 = bool2;
        switch (zzvh.zzbyb[paramzzvv.ordinal()])
        {
        default: 
          bool1 = true;
        }
      }
      this.zzbxw = bool1;
      return;
      this.zzbxv = paramzzvv.zzws();
      continue;
      this.zzbxv = paramzzvv.zzws();
    }
  }
  
  public final int id()
  {
    return this.id;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */