package com.google.android.gms.internal.measurement;

import java.nio.ByteBuffer;

final class zzyo
  extends zzyl
{
  private static int zza(byte[] paramArrayOfByte, int paramInt1, long paramLong, int paramInt2)
  {
    switch (paramInt2)
    {
    default: 
      throw new AssertionError();
    case 0: 
      return zzyj.zzbx(paramInt1);
    case 1: 
      return zzyj.zzr(paramInt1, zzyh.zza(paramArrayOfByte, paramLong));
    }
    return zzyj.zzd(paramInt1, zzyh.zza(paramArrayOfByte, paramLong), zzyh.zza(paramArrayOfByte, 1L + paramLong));
  }
  
  final int zzb(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    if ((paramInt2 | paramInt3 | paramArrayOfByte.length - paramInt3) < 0) {
      throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", new Object[] { Integer.valueOf(paramArrayOfByte.length), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3) }));
    }
    long l2 = paramInt2;
    paramInt3 = (int)(paramInt3 - l2);
    long l1;
    if (paramInt3 < 16)
    {
      paramInt1 = 0;
      l1 = paramInt1 + l2;
      paramInt1 = paramInt3 - paramInt1;
    }
    label161:
    label216:
    label218:
    label306:
    label308:
    do
    {
      long l3;
      do
      {
        do
        {
          paramInt2 = 0;
          for (;;)
          {
            l2 = l1;
            if (paramInt1 <= 0) {
              break;
            }
            l2 = 1L + l1;
            paramInt2 = zzyh.zza(paramArrayOfByte, l1);
            if (paramInt2 < 0) {
              break;
            }
            paramInt1 -= 1;
            l1 = l2;
          }
          paramInt2 = 0;
          for (l1 = l2;; l1 = 1L + l1)
          {
            if (paramInt2 >= paramInt3) {
              break label161;
            }
            paramInt1 = paramInt2;
            if (zzyh.zza(paramArrayOfByte, l1) < 0) {
              break;
            }
            paramInt2 += 1;
          }
          paramInt1 = paramInt3;
          break;
          if (paramInt1 == 0) {
            return 0;
          }
          paramInt1 -= 1;
          if (paramInt2 >= -32) {
            break label218;
          }
          if (paramInt1 == 0) {
            return paramInt2;
          }
          paramInt1 -= 1;
          if (paramInt2 < -62) {
            break label216;
          }
          l1 = 1L + l2;
        } while (zzyh.zza(paramArrayOfByte, l2) <= -65);
        return -1;
        if (paramInt2 >= -16) {
          break label308;
        }
        if (paramInt1 < 2) {
          return zza(paramArrayOfByte, paramInt2, l2, paramInt1);
        }
        paramInt1 -= 2;
        l3 = l2 + 1L;
        paramInt3 = zzyh.zza(paramArrayOfByte, l2);
        if ((paramInt3 > -65) || ((paramInt2 == -32) && (paramInt3 < -96)) || ((paramInt2 == -19) && (paramInt3 >= -96))) {
          break label306;
        }
        l1 = 1L + l3;
      } while (zzyh.zza(paramArrayOfByte, l3) <= -65);
      return -1;
      if (paramInt1 < 3) {
        return zza(paramArrayOfByte, paramInt2, l2, paramInt1);
      }
      paramInt1 -= 3;
      l1 = 1L + l2;
      paramInt3 = zzyh.zza(paramArrayOfByte, l2);
      if ((paramInt3 > -65) || ((paramInt2 << 28) + (paramInt3 + 112) >> 30 != 0)) {
        break label397;
      }
      l2 = 1L + l1;
      if (zzyh.zza(paramArrayOfByte, l1) > -65) {
        break label397;
      }
      l1 = 1L + l2;
    } while (zzyh.zza(paramArrayOfByte, l2) <= -65);
    label397:
    return -1;
  }
  
  final int zzb(CharSequence paramCharSequence, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    long l1 = paramInt1;
    long l3 = l1 + paramInt2;
    int j = paramCharSequence.length();
    int i;
    if ((j > paramInt2) || (paramArrayOfByte.length - paramInt2 < paramInt1))
    {
      i = paramCharSequence.charAt(j - 1);
      throw new ArrayIndexOutOfBoundsException(37 + "Failed writing " + i + " at index " + (paramInt1 + paramInt2));
    }
    paramInt1 = 0;
    while (paramInt1 < j)
    {
      paramInt2 = paramCharSequence.charAt(paramInt1);
      if (paramInt2 >= 128) {
        break;
      }
      zzyh.zza(paramArrayOfByte, l1, (byte)paramInt2);
      paramInt1 += 1;
      l1 = 1L + l1;
    }
    if (paramInt1 == j) {
      return (int)l1;
    }
    for (;;)
    {
      if (paramInt1 < j)
      {
        i = paramCharSequence.charAt(paramInt1);
        long l2;
        if ((i < 128) && (l1 < l3))
        {
          l2 = 1L + l1;
          zzyh.zza(paramArrayOfByte, l1, (byte)i);
          l1 = l2;
        }
        for (;;)
        {
          paramInt1 += 1;
          break;
          if ((i < 2048) && (l1 <= l3 - 2L))
          {
            l2 = l1 + 1L;
            zzyh.zza(paramArrayOfByte, l1, (byte)(i >>> 6 | 0x3C0));
            l1 = 1L + l2;
            zzyh.zza(paramArrayOfByte, l2, (byte)(i & 0x3F | 0x80));
          }
          else if (((i < 55296) || (57343 < i)) && (l1 <= l3 - 3L))
          {
            l2 = 1L + l1;
            zzyh.zza(paramArrayOfByte, l1, (byte)(i >>> 12 | 0x1E0));
            long l4 = 1L + l2;
            zzyh.zza(paramArrayOfByte, l2, (byte)(i >>> 6 & 0x3F | 0x80));
            l1 = 1L + l4;
            zzyh.zza(paramArrayOfByte, l4, (byte)(i & 0x3F | 0x80));
          }
          else
          {
            if (l1 > l3 - 4L) {
              break label538;
            }
            paramInt2 = paramInt1;
            char c;
            if (paramInt1 + 1 != j)
            {
              paramInt1 += 1;
              c = paramCharSequence.charAt(paramInt1);
              if (!Character.isSurrogatePair(i, c)) {
                paramInt2 = paramInt1;
              }
            }
            else
            {
              throw new zzyn(paramInt2 - 1, j);
            }
            paramInt2 = Character.toCodePoint(i, c);
            l2 = 1L + l1;
            zzyh.zza(paramArrayOfByte, l1, (byte)(paramInt2 >>> 18 | 0xF0));
            l1 = 1L + l2;
            zzyh.zza(paramArrayOfByte, l2, (byte)(paramInt2 >>> 12 & 0x3F | 0x80));
            l2 = l1 + 1L;
            zzyh.zza(paramArrayOfByte, l1, (byte)(paramInt2 >>> 6 & 0x3F | 0x80));
            l1 = 1L + l2;
            zzyh.zza(paramArrayOfByte, l2, (byte)(paramInt2 & 0x3F | 0x80));
          }
        }
        label538:
        if ((55296 <= i) && (i <= 57343) && ((paramInt1 + 1 == j) || (!Character.isSurrogatePair(i, paramCharSequence.charAt(paramInt1 + 1))))) {
          throw new zzyn(paramInt1, j);
        }
        throw new ArrayIndexOutOfBoundsException(46 + "Failed writing " + i + " at index " + l1);
      }
      return (int)l1;
    }
  }
  
  final void zzb(CharSequence paramCharSequence, ByteBuffer paramByteBuffer)
  {
    long l3 = zzyh.zzb(paramByteBuffer);
    long l1 = l3 + paramByteBuffer.position();
    long l4 = l3 + paramByteBuffer.limit();
    int m = paramCharSequence.length();
    int i;
    if (m > l4 - l1)
    {
      i = paramCharSequence.charAt(m - 1);
      j = paramByteBuffer.limit();
      throw new ArrayIndexOutOfBoundsException(37 + "Failed writing " + i + " at index " + j);
    }
    int j = 0;
    int k;
    while (j < m)
    {
      k = paramCharSequence.charAt(j);
      if (k >= 128) {
        break;
      }
      zzyh.zza(l1, (byte)k);
      j += 1;
      l1 = 1L + l1;
    }
    if (j == m)
    {
      paramByteBuffer.position((int)(l1 - l3));
      return;
    }
    for (;;)
    {
      if (j < m)
      {
        i = paramCharSequence.charAt(j);
        long l2;
        if ((i < 128) && (l1 < l4))
        {
          l2 = 1L + l1;
          zzyh.zza(l1, (byte)i);
          l1 = l2;
        }
        for (;;)
        {
          j += 1;
          break;
          if ((i < 2048) && (l1 <= l4 - 2L))
          {
            l2 = l1 + 1L;
            zzyh.zza(l1, (byte)(i >>> 6 | 0x3C0));
            l1 = 1L + l2;
            zzyh.zza(l2, (byte)(i & 0x3F | 0x80));
          }
          else if (((i < 55296) || (57343 < i)) && (l1 <= l4 - 3L))
          {
            l2 = 1L + l1;
            zzyh.zza(l1, (byte)(i >>> 12 | 0x1E0));
            long l5 = 1L + l2;
            zzyh.zza(l2, (byte)(i >>> 6 & 0x3F | 0x80));
            l1 = 1L + l5;
            zzyh.zza(l5, (byte)(i & 0x3F | 0x80));
          }
          else
          {
            if (l1 > l4 - 4L) {
              break label550;
            }
            k = j;
            char c;
            if (j + 1 != m)
            {
              j += 1;
              c = paramCharSequence.charAt(j);
              if (!Character.isSurrogatePair(i, c)) {
                k = j;
              }
            }
            else
            {
              throw new zzyn(k - 1, m);
            }
            k = Character.toCodePoint(i, c);
            l2 = 1L + l1;
            zzyh.zza(l1, (byte)(k >>> 18 | 0xF0));
            l1 = 1L + l2;
            zzyh.zza(l2, (byte)(k >>> 12 & 0x3F | 0x80));
            l2 = l1 + 1L;
            zzyh.zza(l1, (byte)(k >>> 6 & 0x3F | 0x80));
            l1 = 1L + l2;
            zzyh.zza(l2, (byte)(k & 0x3F | 0x80));
          }
        }
        label550:
        if ((55296 <= i) && (i <= 57343) && ((j + 1 == m) || (!Character.isSurrogatePair(i, paramCharSequence.charAt(j + 1))))) {
          throw new zzyn(j, m);
        }
        throw new ArrayIndexOutOfBoundsException(46 + "Failed writing " + i + " at index " + l1);
      }
      paramByteBuffer.position((int)(l1 - l3));
      return;
    }
  }
  
  final String zzh(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws zzvt
  {
    if ((paramInt1 | paramInt2 | paramArrayOfByte.length - paramInt1 - paramInt2) < 0) {
      throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", new Object[] { Integer.valueOf(paramArrayOfByte.length), Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }));
    }
    int k = paramInt1 + paramInt2;
    char[] arrayOfChar = new char[paramInt2];
    int i = 0;
    int j = paramInt1;
    byte b1;
    for (;;)
    {
      paramInt2 = j;
      paramInt1 = i;
      if (j >= k) {
        break;
      }
      b1 = zzyh.zza(paramArrayOfByte, j);
      paramInt2 = j;
      paramInt1 = i;
      if (!zzyk.zzh(b1)) {
        break;
      }
      j += 1;
      zzyk.zzb(b1, arrayOfChar, i);
      i += 1;
    }
    zzyk.zzb(b1, zzyh.zza(paramArrayOfByte, i), arrayOfChar, paramInt1);
    paramInt1 += 1;
    paramInt2 = i + 1;
    for (;;)
    {
      if (paramInt2 >= k) {
        break label403;
      }
      i = paramInt2 + 1;
      b1 = zzyh.zza(paramArrayOfByte, paramInt2);
      if (zzyk.zzh(b1))
      {
        zzyk.zzb(b1, arrayOfChar, paramInt1);
        paramInt2 = paramInt1 + 1;
        paramInt1 = i;
        i = paramInt2;
        for (;;)
        {
          j = i;
          paramInt2 = paramInt1;
          if (paramInt1 >= k) {
            break;
          }
          b1 = zzyh.zza(paramArrayOfByte, paramInt1);
          j = i;
          paramInt2 = paramInt1;
          if (!zzyk.zzh(b1)) {
            break;
          }
          zzyk.zzb(b1, arrayOfChar, i);
          i += 1;
          paramInt1 += 1;
        }
      }
      if (zzyk.zzi(b1))
      {
        if (i < k) {
          break;
        }
        throw zzvt.zzwr();
      }
      if (zzyk.zzj(b1))
      {
        if (i >= k - 1) {
          throw zzvt.zzwr();
        }
        paramInt2 = i + 1;
        zzyk.zzb(b1, zzyh.zza(paramArrayOfByte, i), zzyh.zza(paramArrayOfByte, paramInt2), arrayOfChar, paramInt1);
        paramInt1 += 1;
        paramInt2 += 1;
      }
      else
      {
        if (i >= k - 2) {
          throw zzvt.zzwr();
        }
        paramInt2 = i + 1;
        byte b2 = zzyh.zza(paramArrayOfByte, i);
        i = paramInt2 + 1;
        zzyk.zzb(b1, b2, zzyh.zza(paramArrayOfByte, paramInt2), zzyh.zza(paramArrayOfByte, i), arrayOfChar, paramInt1);
        j = paramInt1 + 1 + 1;
        paramInt2 = i + 1;
        paramInt1 = j;
      }
    }
    label403:
    return new String(arrayOfChar, 0, paramInt1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */