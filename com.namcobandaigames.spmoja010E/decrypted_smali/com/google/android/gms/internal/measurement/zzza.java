package com.google.android.gms.internal.measurement;

import java.io.IOException;

public abstract class zzza<M extends zzza<M>>
  extends zzzg
{
  protected zzzc zzcfc;
  
  public final <T> T zza(zzzb<M, T> paramzzzb)
  {
    if (this.zzcfc == null) {}
    zzzd localzzzd;
    do
    {
      return null;
      localzzzd = this.zzcfc.zzcb(paramzzzb.tag >>> 3);
    } while (localzzzd == null);
    return (T)localzzzd.zzb(paramzzzb);
  }
  
  public void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzcfc == null) {}
    for (;;)
    {
      return;
      int i = 0;
      while (i < this.zzcfc.size())
      {
        this.zzcfc.zzcc(i).zza(paramzzyy);
        i += 1;
      }
    }
  }
  
  protected final boolean zza(zzyx paramzzyx, int paramInt)
    throws IOException
  {
    int i = paramzzyx.getPosition();
    if (!paramzzyx.zzao(paramInt)) {
      return false;
    }
    int j = paramInt >>> 3;
    zzzi localzzzi = new zzzi(paramInt, paramzzyx.zzs(i, paramzzyx.getPosition() - i));
    paramzzyx = null;
    if (this.zzcfc == null) {
      this.zzcfc = new zzzc();
    }
    for (;;)
    {
      Object localObject = paramzzyx;
      if (paramzzyx == null)
      {
        localObject = new zzzd();
        this.zzcfc.zza(j, (zzzd)localObject);
      }
      ((zzzd)localObject).zza(localzzzi);
      return true;
      paramzzyx = this.zzcfc.zzcb(j);
    }
  }
  
  protected int zzf()
  {
    int j = 0;
    if (this.zzcfc != null)
    {
      int i = 0;
      for (;;)
      {
        k = i;
        if (j >= this.zzcfc.size()) {
          break;
        }
        i += this.zzcfc.zzcc(j).zzf();
        j += 1;
      }
    }
    int k = 0;
    return k;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */