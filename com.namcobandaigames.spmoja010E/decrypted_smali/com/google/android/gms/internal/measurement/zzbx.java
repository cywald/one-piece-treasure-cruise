package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashSet;
import java.util.Set;

public final class zzbx
{
  private final zzaw zzqx;
  private volatile Boolean zzyk;
  private String zzyl;
  private Set<Integer> zzym;
  
  protected zzbx(zzaw paramzzaw)
  {
    Preconditions.checkNotNull(paramzzaw);
    this.zzqx = paramzzaw;
  }
  
  public static boolean zzdx()
  {
    return ((Boolean)zzcf.zzyw.get()).booleanValue();
  }
  
  public static int zzdy()
  {
    return ((Integer)zzcf.zzzt.get()).intValue();
  }
  
  public static long zzdz()
  {
    return ((Long)zzcf.zzze.get()).longValue();
  }
  
  public static long zzea()
  {
    return ((Long)zzcf.zzzh.get()).longValue();
  }
  
  public static int zzeb()
  {
    return ((Integer)zzcf.zzzj.get()).intValue();
  }
  
  public static int zzec()
  {
    return ((Integer)zzcf.zzzk.get()).intValue();
  }
  
  @VisibleForTesting
  public static String zzed()
  {
    return (String)zzcf.zzzm.get();
  }
  
  @VisibleForTesting
  public static String zzee()
  {
    return (String)zzcf.zzzl.get();
  }
  
  public static String zzef()
  {
    return (String)zzcf.zzzn.get();
  }
  
  public static long zzeh()
  {
    return ((Long)zzcf.zzaab.get()).longValue();
  }
  
  /* Error */
  public final boolean zzdw()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   4: ifnonnull +119 -> 123
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   13: ifnonnull +108 -> 121
    //   16: aload_0
    //   17: getfield 26	com/google/android/gms/internal/measurement/zzbx:zzqx	Lcom/google/android/gms/internal/measurement/zzaw;
    //   20: invokevirtual 108	com/google/android/gms/internal/measurement/zzaw:getContext	()Landroid/content/Context;
    //   23: invokevirtual 114	android/content/Context:getApplicationInfo	()Landroid/content/pm/ApplicationInfo;
    //   26: astore_3
    //   27: invokestatic 119	com/google/android/gms/common/util/ProcessUtils:getMyProcessName	()Ljava/lang/String;
    //   30: astore_2
    //   31: aload_3
    //   32: ifnull +30 -> 62
    //   35: aload_3
    //   36: getfield 124	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
    //   39: astore_3
    //   40: aload_3
    //   41: ifnull +90 -> 131
    //   44: aload_3
    //   45: aload_2
    //   46: invokevirtual 128	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   49: ifeq +82 -> 131
    //   52: iconst_1
    //   53: istore_1
    //   54: aload_0
    //   55: iload_1
    //   56: invokestatic 132	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   59: putfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   62: aload_0
    //   63: getfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   66: ifnull +13 -> 79
    //   69: aload_0
    //   70: getfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   73: invokevirtual 46	java/lang/Boolean:booleanValue	()Z
    //   76: ifne +19 -> 95
    //   79: ldc -122
    //   81: aload_2
    //   82: invokevirtual 128	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   85: ifeq +10 -> 95
    //   88: aload_0
    //   89: getstatic 137	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   92: putfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   95: aload_0
    //   96: getfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   99: ifnonnull +22 -> 121
    //   102: aload_0
    //   103: getstatic 137	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   106: putfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   109: aload_0
    //   110: getfield 26	com/google/android/gms/internal/measurement/zzbx:zzqx	Lcom/google/android/gms/internal/measurement/zzaw;
    //   113: invokevirtual 141	com/google/android/gms/internal/measurement/zzaw:zzby	()Lcom/google/android/gms/internal/measurement/zzcp;
    //   116: ldc -113
    //   118: invokevirtual 149	com/google/android/gms/internal/measurement/zzat:zzu	(Ljava/lang/String;)V
    //   121: aload_0
    //   122: monitorexit
    //   123: aload_0
    //   124: getfield 102	com/google/android/gms/internal/measurement/zzbx:zzyk	Ljava/lang/Boolean;
    //   127: invokevirtual 46	java/lang/Boolean:booleanValue	()Z
    //   130: ireturn
    //   131: iconst_0
    //   132: istore_1
    //   133: goto -79 -> 54
    //   136: astore_2
    //   137: aload_0
    //   138: monitorexit
    //   139: aload_2
    //   140: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	141	0	this	zzbx
    //   53	80	1	bool	boolean
    //   30	52	2	str	String
    //   136	4	2	localObject1	Object
    //   26	19	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   9	31	136	finally
    //   35	40	136	finally
    //   44	52	136	finally
    //   54	62	136	finally
    //   62	79	136	finally
    //   79	95	136	finally
    //   95	121	136	finally
    //   121	123	136	finally
    //   137	139	136	finally
  }
  
  public final Set<Integer> zzeg()
  {
    String str1 = (String)zzcf.zzzw.get();
    String[] arrayOfString;
    HashSet localHashSet;
    int j;
    int i;
    if ((this.zzym == null) || (this.zzyl == null) || (!this.zzyl.equals(str1)))
    {
      arrayOfString = TextUtils.split(str1, ",");
      localHashSet = new HashSet();
      j = arrayOfString.length;
      i = 0;
    }
    for (;;)
    {
      String str2;
      if (i < j) {
        str2 = arrayOfString[i];
      }
      try
      {
        localHashSet.add(Integer.valueOf(Integer.parseInt(str2)));
        i += 1;
        continue;
        this.zzyl = str1;
        this.zzym = localHashSet;
        return this.zzym;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */