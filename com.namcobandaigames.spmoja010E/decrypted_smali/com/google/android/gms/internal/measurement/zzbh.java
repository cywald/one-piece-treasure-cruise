package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzbh
  extends zzau
{
  private final zzx zzsq = new zzx();
  
  zzbh(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  protected final void zzag()
  {
    zzca().zzad().zza(this.zzsq);
    Object localObject = zzce();
    String str = ((zzdh)localObject).zzaj();
    if (str != null) {
      this.zzsq.setAppName(str);
    }
    localObject = ((zzdh)localObject).zzak();
    if (localObject != null) {
      this.zzsq.setAppVersion((String)localObject);
    }
  }
  
  public final zzx zzdf()
  {
    zzcl();
    return this.zzsq;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */