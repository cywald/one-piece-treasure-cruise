package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Arrays;

final class zzuq
  extends zzuo
{
  private final byte[] buffer;
  private int limit;
  private int pos;
  private final boolean zzbum;
  private int zzbun;
  private int zzbuo;
  private int zzbup;
  private int zzbuq = Integer.MAX_VALUE;
  
  private zzuq(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    super(null);
    this.buffer = paramArrayOfByte;
    this.limit = (paramInt1 + paramInt2);
    this.pos = paramInt1;
    this.zzbuo = this.pos;
    this.zzbum = paramBoolean;
  }
  
  private final int zzuy()
    throws IOException
  {
    int i = this.pos;
    byte[] arrayOfByte;
    int j;
    int k;
    if (this.limit != i)
    {
      arrayOfByte = this.buffer;
      j = i + 1;
      k = arrayOfByte[i];
      if (k >= 0)
      {
        this.pos = j;
        return k;
      }
      if (this.limit - j >= 9)
      {
        i = j + 1;
        k ^= arrayOfByte[j] << 7;
        if (k < 0) {
          j = k ^ 0xFFFFFF80;
        }
      }
    }
    for (;;)
    {
      this.pos = i;
      return j;
      j = i + 1;
      k ^= arrayOfByte[i] << 14;
      if (k >= 0)
      {
        k ^= 0x3F80;
        i = j;
        j = k;
      }
      else
      {
        i = j + 1;
        k ^= arrayOfByte[j] << 21;
        if (k < 0)
        {
          j = k ^ 0xFFE03F80;
        }
        else
        {
          int m = i + 1;
          j = arrayOfByte[i];
          k = k ^ j << 28 ^ 0xFE03F80;
          i = m;
          if (j < 0)
          {
            int n = m + 1;
            j = k;
            i = n;
            if (arrayOfByte[m] >= 0) {
              continue;
            }
            m = n + 1;
            i = m;
            if (arrayOfByte[n] < 0)
            {
              n = m + 1;
              j = k;
              i = n;
              if (arrayOfByte[m] >= 0) {
                continue;
              }
              m = n + 1;
              i = m;
              if (arrayOfByte[n] < 0)
              {
                i = m + 1;
                j = k;
                if (arrayOfByte[m] >= 0) {
                  continue;
                }
                return (int)zzuv();
              }
            }
          }
          j = k;
        }
      }
    }
  }
  
  private final long zzuz()
    throws IOException
  {
    int i = this.pos;
    byte[] arrayOfByte;
    int j;
    int k;
    long l;
    if (this.limit != i)
    {
      arrayOfByte = this.buffer;
      j = i + 1;
      k = arrayOfByte[i];
      if (k >= 0)
      {
        this.pos = j;
        return k;
      }
      if (this.limit - j >= 9)
      {
        i = j + 1;
        k ^= arrayOfByte[j] << 7;
        if (k < 0) {
          l = k ^ 0xFFFFFF80;
        }
      }
    }
    for (;;)
    {
      this.pos = i;
      return l;
      j = i + 1;
      k ^= arrayOfByte[i] << 14;
      if (k >= 0)
      {
        l = k ^ 0x3F80;
        i = j;
      }
      else
      {
        i = j + 1;
        j = k ^ arrayOfByte[j] << 21;
        if (j < 0)
        {
          l = j ^ 0xFFE03F80;
        }
        else
        {
          l = j;
          j = i + 1;
          l ^= arrayOfByte[i] << 28;
          if (l >= 0L)
          {
            l ^= 0xFE03F80;
            i = j;
          }
          else
          {
            i = j + 1;
            l ^= arrayOfByte[j] << 35;
            if (l < 0L)
            {
              l ^= 0xFFFFFFF80FE03F80;
            }
            else
            {
              j = i + 1;
              l ^= arrayOfByte[i] << 42;
              if (l >= 0L)
              {
                l ^= 0x3F80FE03F80;
                i = j;
              }
              else
              {
                i = j + 1;
                l ^= arrayOfByte[j] << 49;
                if (l < 0L)
                {
                  l ^= 0xFFFE03F80FE03F80;
                }
                else
                {
                  j = i + 1;
                  l = l ^ arrayOfByte[i] << 56 ^ 0xFE03F80FE03F80;
                  if (l < 0L)
                  {
                    i = j + 1;
                    if (arrayOfByte[j] < 0L) {
                      return zzuv();
                    }
                  }
                  else
                  {
                    i = j;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  private final int zzva()
    throws IOException
  {
    int i = this.pos;
    if (this.limit - i < 4) {
      throw zzvt.zzwk();
    }
    byte[] arrayOfByte = this.buffer;
    this.pos = (i + 4);
    int j = arrayOfByte[i];
    int k = arrayOfByte[(i + 1)];
    int m = arrayOfByte[(i + 2)];
    return (arrayOfByte[(i + 3)] & 0xFF) << 24 | j & 0xFF | (k & 0xFF) << 8 | (m & 0xFF) << 16;
  }
  
  private final long zzvb()
    throws IOException
  {
    int i = this.pos;
    if (this.limit - i < 8) {
      throw zzvt.zzwk();
    }
    byte[] arrayOfByte = this.buffer;
    this.pos = (i + 8);
    long l1 = arrayOfByte[i];
    long l2 = arrayOfByte[(i + 1)];
    long l3 = arrayOfByte[(i + 2)];
    long l4 = arrayOfByte[(i + 3)];
    long l5 = arrayOfByte[(i + 4)];
    long l6 = arrayOfByte[(i + 5)];
    long l7 = arrayOfByte[(i + 6)];
    return (arrayOfByte[(i + 7)] & 0xFF) << 56 | l1 & 0xFF | (l2 & 0xFF) << 8 | (l3 & 0xFF) << 16 | (l4 & 0xFF) << 24 | (l5 & 0xFF) << 32 | (l6 & 0xFF) << 40 | (l7 & 0xFF) << 48;
  }
  
  private final void zzvc()
  {
    this.limit += this.zzbun;
    int i = this.limit - this.zzbuo;
    if (i > this.zzbuq)
    {
      this.zzbun = (i - this.zzbuq);
      this.limit -= this.zzbun;
      return;
    }
    this.zzbun = 0;
  }
  
  private final byte zzvd()
    throws IOException
  {
    if (this.pos == this.limit) {
      throw zzvt.zzwk();
    }
    byte[] arrayOfByte = this.buffer;
    int i = this.pos;
    this.pos = (i + 1);
    return arrayOfByte[i];
  }
  
  public final double readDouble()
    throws IOException
  {
    return Double.longBitsToDouble(zzvb());
  }
  
  public final float readFloat()
    throws IOException
  {
    return Float.intBitsToFloat(zzva());
  }
  
  public final String readString()
    throws IOException
  {
    int i = zzuy();
    if ((i > 0) && (i <= this.limit - this.pos))
    {
      String str = new String(this.buffer, this.pos, i, zzvo.UTF_8);
      this.pos = (i + this.pos);
      return str;
    }
    if (i == 0) {
      return "";
    }
    if (i < 0) {
      throw zzvt.zzwl();
    }
    throw zzvt.zzwk();
  }
  
  public final <T extends zzwt> T zza(zzxd<T> paramzzxd, zzuz paramzzuz)
    throws IOException
  {
    int i = zzuy();
    if (this.zzbuh >= this.zzbui) {
      throw zzvt.zzwp();
    }
    i = zzaq(i);
    this.zzbuh += 1;
    paramzzxd = (zzwt)paramzzxd.zza(this, paramzzuz);
    zzan(0);
    this.zzbuh -= 1;
    zzar(i);
    return paramzzxd;
  }
  
  public final void zzan(int paramInt)
    throws zzvt
  {
    if (this.zzbup != paramInt) {
      throw zzvt.zzwn();
    }
  }
  
  public final boolean zzao(int paramInt)
    throws IOException
  {
    int j = 0;
    int i = 0;
    switch (paramInt & 0x7)
    {
    default: 
      throw zzvt.zzwo();
    case 0: 
      paramInt = j;
      if (this.limit - this.pos >= 10)
      {
        paramInt = i;
        while (paramInt < 10)
        {
          byte[] arrayOfByte = this.buffer;
          i = this.pos;
          this.pos = (i + 1);
          if (arrayOfByte[i] >= 0) {
            break label142;
          }
          paramInt += 1;
        }
        throw zzvt.zzwm();
      }
      while (paramInt < 10)
      {
        if (zzvd() >= 0) {
          break label142;
        }
        paramInt += 1;
      }
      throw zzvt.zzwm();
    case 1: 
      zzas(8);
      return true;
    case 2: 
      zzas(zzuy());
      return true;
    case 3: 
      do
      {
        i = zzug();
      } while ((i != 0) && (zzao(i)));
      zzan(paramInt >>> 3 << 3 | 0x4);
      return true;
    case 4: 
      label142:
      return false;
    }
    zzas(4);
    return true;
  }
  
  public final int zzaq(int paramInt)
    throws zzvt
  {
    if (paramInt < 0) {
      throw zzvt.zzwl();
    }
    paramInt = zzux() + paramInt;
    int i = this.zzbuq;
    if (paramInt > i) {
      throw zzvt.zzwk();
    }
    this.zzbuq = paramInt;
    zzvc();
    return i;
  }
  
  public final void zzar(int paramInt)
  {
    this.zzbuq = paramInt;
    zzvc();
  }
  
  public final void zzas(int paramInt)
    throws IOException
  {
    if ((paramInt >= 0) && (paramInt <= this.limit - this.pos))
    {
      this.pos += paramInt;
      return;
    }
    if (paramInt < 0) {
      throw zzvt.zzwl();
    }
    throw zzvt.zzwk();
  }
  
  public final int zzug()
    throws IOException
  {
    if (zzuw())
    {
      this.zzbup = 0;
      return 0;
    }
    this.zzbup = zzuy();
    if (this.zzbup >>> 3 == 0) {
      throw new zzvt("Protocol message contained an invalid tag (zero).");
    }
    return this.zzbup;
  }
  
  public final long zzuh()
    throws IOException
  {
    return zzuz();
  }
  
  public final long zzui()
    throws IOException
  {
    return zzuz();
  }
  
  public final int zzuj()
    throws IOException
  {
    return zzuy();
  }
  
  public final long zzuk()
    throws IOException
  {
    return zzvb();
  }
  
  public final int zzul()
    throws IOException
  {
    return zzva();
  }
  
  public final boolean zzum()
    throws IOException
  {
    return zzuz() != 0L;
  }
  
  public final String zzun()
    throws IOException
  {
    int i = zzuy();
    if ((i > 0) && (i <= this.limit - this.pos))
    {
      String str = zzyj.zzh(this.buffer, this.pos, i);
      this.pos = (i + this.pos);
      return str;
    }
    if (i == 0) {
      return "";
    }
    if (i <= 0) {
      throw zzvt.zzwl();
    }
    throw zzvt.zzwk();
  }
  
  public final zzud zzuo()
    throws IOException
  {
    int i = zzuy();
    if ((i > 0) && (i <= this.limit - this.pos))
    {
      localObject = zzud.zzb(this.buffer, this.pos, i);
      this.pos = (i + this.pos);
      return (zzud)localObject;
    }
    if (i == 0) {
      return zzud.zzbtz;
    }
    int j;
    if ((i > 0) && (i <= this.limit - this.pos))
    {
      j = this.pos;
      this.pos = (i + this.pos);
    }
    for (Object localObject = Arrays.copyOfRange(this.buffer, j, this.pos);; localObject = zzvo.zzbzj)
    {
      return zzud.zzi((byte[])localObject);
      if (i > 0) {
        break label124;
      }
      if (i != 0) {
        break;
      }
    }
    throw zzvt.zzwl();
    label124:
    throw zzvt.zzwk();
  }
  
  public final int zzup()
    throws IOException
  {
    return zzuy();
  }
  
  public final int zzuq()
    throws IOException
  {
    return zzuy();
  }
  
  public final int zzur()
    throws IOException
  {
    return zzva();
  }
  
  public final long zzus()
    throws IOException
  {
    return zzvb();
  }
  
  public final int zzut()
    throws IOException
  {
    int i = zzuy();
    return -(i & 0x1) ^ i >>> 1;
  }
  
  public final long zzuu()
    throws IOException
  {
    long l = zzuz();
    return -(l & 1L) ^ l >>> 1;
  }
  
  final long zzuv()
    throws IOException
  {
    long l = 0L;
    int i = 0;
    while (i < 64)
    {
      int j = zzvd();
      l |= (j & 0x7F) << i;
      if ((j & 0x80) == 0) {
        return l;
      }
      i += 7;
    }
    throw zzvt.zzwm();
  }
  
  public final boolean zzuw()
    throws IOException
  {
    return this.pos == this.limit;
  }
  
  public final int zzux()
  {
    return this.pos - this.zzbuo;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */