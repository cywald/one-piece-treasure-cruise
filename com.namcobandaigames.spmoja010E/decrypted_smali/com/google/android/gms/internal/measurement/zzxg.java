package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.List;

final class zzxg<E>
  extends zztz<E>
{
  private static final zzxg<Object> zzcbv;
  private final List<E> zzcai;
  
  static
  {
    zzxg localzzxg = new zzxg();
    zzcbv = localzzxg;
    localzzxg.zzsm();
  }
  
  zzxg()
  {
    this(new ArrayList(10));
  }
  
  private zzxg(List<E> paramList)
  {
    this.zzcai = paramList;
  }
  
  public static <E> zzxg<E> zzxo()
  {
    return zzcbv;
  }
  
  public final void add(int paramInt, E paramE)
  {
    zztx();
    this.zzcai.add(paramInt, paramE);
    this.modCount += 1;
  }
  
  public final E get(int paramInt)
  {
    return (E)this.zzcai.get(paramInt);
  }
  
  public final E remove(int paramInt)
  {
    zztx();
    Object localObject = this.zzcai.remove(paramInt);
    this.modCount += 1;
    return (E)localObject;
  }
  
  public final E set(int paramInt, E paramE)
  {
    zztx();
    paramE = this.zzcai.set(paramInt, paramE);
    this.modCount += 1;
    return paramE;
  }
  
  public final int size()
  {
    return this.zzcai.size();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */