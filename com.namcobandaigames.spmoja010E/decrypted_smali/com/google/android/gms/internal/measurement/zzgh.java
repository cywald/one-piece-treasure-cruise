package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgh
  extends zzza<zzgh>
{
  public zzgi[] zzawy = zzgi.zzms();
  
  public zzgh()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgh)) {
        return false;
      }
      paramObject = (zzgh)paramObject;
      if (!zzze.equals(this.zzawy, ((zzgh)paramObject).zzawy)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgh)paramObject).zzcfc == null) || (((zzgh)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgh)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = zzze.hashCode(this.zzawy);
    if ((this.zzcfc == null) || (this.zzcfc.isEmpty())) {}
    for (int i = 0;; i = this.zzcfc.hashCode()) {
      return i + ((j + 527) * 31 + k) * 31;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if ((this.zzawy != null) && (this.zzawy.length > 0))
    {
      int i = 0;
      while (i < this.zzawy.length)
      {
        zzgi localzzgi = this.zzawy[i];
        if (localzzgi != null) {
          paramzzyy.zza(1, localzzgi);
        }
        i += 1;
      }
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int i = super.zzf();
    int k = i;
    if (this.zzawy != null)
    {
      k = i;
      if (this.zzawy.length > 0)
      {
        int j = 0;
        for (;;)
        {
          k = i;
          if (j >= this.zzawy.length) {
            break;
          }
          zzgi localzzgi = this.zzawy[j];
          k = i;
          if (localzzgi != null) {
            k = i + zzyy.zzb(1, localzzgi);
          }
          j += 1;
          i = k;
        }
      }
    }
    return k;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */