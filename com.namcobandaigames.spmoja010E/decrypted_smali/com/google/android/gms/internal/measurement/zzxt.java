package com.google.android.gms.internal.measurement;

import java.util.Map.Entry;

final class zzxt
  implements Comparable<zzxt>, Map.Entry<K, V>
{
  private V value;
  private final K zzcck;
  
  zzxt(K paramK, V paramV)
  {
    this.zzcck = paramV;
    Object localObject;
    this.value = localObject;
  }
  
  zzxt(Map.Entry<K, V> paramEntry)
  {
    this(paramEntry, (Comparable)((Map.Entry)localObject).getKey(), ((Map.Entry)localObject).getValue());
  }
  
  private static boolean equals(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 == null) {
      return paramObject2 == null;
    }
    return paramObject1.equals(paramObject2);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof Map.Entry)) {
        return false;
      }
      paramObject = (Map.Entry)paramObject;
    } while ((equals(this.zzcck, ((Map.Entry)paramObject).getKey())) && (equals(this.value, ((Map.Entry)paramObject).getValue())));
    return false;
  }
  
  public final V getValue()
  {
    return (V)this.value;
  }
  
  public final int hashCode()
  {
    int j = 0;
    int i;
    if (this.zzcck == null)
    {
      i = 0;
      if (this.value != null) {
        break label33;
      }
    }
    for (;;)
    {
      return i ^ j;
      i = this.zzcck.hashCode();
      break;
      label33:
      j = this.value.hashCode();
    }
  }
  
  public final V setValue(V paramV)
  {
    zzxm.zza(this.zzcch);
    Object localObject = this.value;
    this.value = paramV;
    return (V)localObject;
  }
  
  public final String toString()
  {
    String str1 = String.valueOf(this.zzcck);
    String str2 = String.valueOf(this.value);
    return String.valueOf(str1).length() + 1 + String.valueOf(str2).length() + str1 + "=" + str2;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */