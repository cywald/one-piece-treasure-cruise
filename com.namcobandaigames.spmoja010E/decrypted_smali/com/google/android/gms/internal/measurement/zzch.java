package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzch
  extends zzbv<zzcj>
{
  public zzch(zzaw paramzzaw)
  {
    super(paramzzaw, new zzci(paramzzaw));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */