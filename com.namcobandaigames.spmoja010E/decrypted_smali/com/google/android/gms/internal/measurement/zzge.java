package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzge
  extends zzza<zzge>
{
  private static volatile zzge[] zzawp;
  public Integer zzawq = null;
  public Long zzawr = null;
  
  public zzge()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzge[] zzmp()
  {
    if (zzawp == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzawp == null) {
        zzawp = new zzge[0];
      }
      return zzawp;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzge)) {
        return false;
      }
      paramObject = (zzge)paramObject;
      if (this.zzawq == null)
      {
        if (((zzge)paramObject).zzawq != null) {
          return false;
        }
      }
      else if (!this.zzawq.equals(((zzge)paramObject).zzawq)) {
        return false;
      }
      if (this.zzawr == null)
      {
        if (((zzge)paramObject).zzawr != null) {
          return false;
        }
      }
      else if (!this.zzawr.equals(((zzge)paramObject).zzawr)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzge)paramObject).zzcfc == null) || (((zzge)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzge)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int m = 0;
    int n = getClass().getName().hashCode();
    int i;
    int j;
    if (this.zzawq == null)
    {
      i = 0;
      if (this.zzawr != null) {
        break label89;
      }
      j = 0;
      label33:
      k = m;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label100;
        }
      }
    }
    label89:
    label100:
    for (int k = m;; k = this.zzcfc.hashCode())
    {
      return (j + (i + (n + 527) * 31) * 31) * 31 + k;
      i = this.zzawq.hashCode();
      break;
      j = this.zzawr.hashCode();
      break label33;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzawq != null) {
      paramzzyy.zzd(1, this.zzawq.intValue());
    }
    if (this.zzawr != null) {
      paramzzyy.zzi(2, this.zzawr.longValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.zzawq != null) {
      i = j + zzyy.zzh(1, this.zzawq.intValue());
    }
    j = i;
    if (this.zzawr != null) {
      j = i + zzyy.zzd(2, this.zzawr.longValue());
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzge.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */