package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgl
  extends zzza<zzgl>
{
  private static volatile zzgl[] zzayk;
  public String name = null;
  public String zzamp = null;
  private Float zzaug = null;
  public Double zzauh = null;
  public Long zzawx = null;
  public Long zzayl = null;
  
  public zzgl()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzgl[] zzmu()
  {
    if (zzayk == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzayk == null) {
        zzayk = new zzgl[0];
      }
      return zzayk;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgl)) {
        return false;
      }
      paramObject = (zzgl)paramObject;
      if (this.zzayl == null)
      {
        if (((zzgl)paramObject).zzayl != null) {
          return false;
        }
      }
      else if (!this.zzayl.equals(((zzgl)paramObject).zzayl)) {
        return false;
      }
      if (this.name == null)
      {
        if (((zzgl)paramObject).name != null) {
          return false;
        }
      }
      else if (!this.name.equals(((zzgl)paramObject).name)) {
        return false;
      }
      if (this.zzamp == null)
      {
        if (((zzgl)paramObject).zzamp != null) {
          return false;
        }
      }
      else if (!this.zzamp.equals(((zzgl)paramObject).zzamp)) {
        return false;
      }
      if (this.zzawx == null)
      {
        if (((zzgl)paramObject).zzawx != null) {
          return false;
        }
      }
      else if (!this.zzawx.equals(((zzgl)paramObject).zzawx)) {
        return false;
      }
      if (this.zzaug == null)
      {
        if (((zzgl)paramObject).zzaug != null) {
          return false;
        }
      }
      else if (!this.zzaug.equals(((zzgl)paramObject).zzaug)) {
        return false;
      }
      if (this.zzauh == null)
      {
        if (((zzgl)paramObject).zzauh != null) {
          return false;
        }
      }
      else if (!this.zzauh.equals(((zzgl)paramObject).zzauh)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgl)paramObject).zzcfc == null) || (((zzgl)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgl)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int i3 = 0;
    int i4 = getClass().getName().hashCode();
    int i;
    int j;
    label33:
    int k;
    label42:
    int m;
    label52:
    int n;
    label62:
    int i1;
    if (this.zzayl == null)
    {
      i = 0;
      if (this.name != null) {
        break label154;
      }
      j = 0;
      if (this.zzamp != null) {
        break label165;
      }
      k = 0;
      if (this.zzawx != null) {
        break label176;
      }
      m = 0;
      if (this.zzaug != null) {
        break label188;
      }
      n = 0;
      if (this.zzauh != null) {
        break label200;
      }
      i1 = 0;
      label72:
      i2 = i3;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label212;
        }
      }
    }
    label154:
    label165:
    label176:
    label188:
    label200:
    label212:
    for (int i2 = i3;; i2 = this.zzcfc.hashCode())
    {
      return (i1 + (n + (m + (k + (j + (i + (i4 + 527) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + i2;
      i = this.zzayl.hashCode();
      break;
      j = this.name.hashCode();
      break label33;
      k = this.zzamp.hashCode();
      break label42;
      m = this.zzawx.hashCode();
      break label52;
      n = this.zzaug.hashCode();
      break label62;
      i1 = this.zzauh.hashCode();
      break label72;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzayl != null) {
      paramzzyy.zzi(1, this.zzayl.longValue());
    }
    if (this.name != null) {
      paramzzyy.zzb(2, this.name);
    }
    if (this.zzamp != null) {
      paramzzyy.zzb(3, this.zzamp);
    }
    if (this.zzawx != null) {
      paramzzyy.zzi(4, this.zzawx.longValue());
    }
    if (this.zzaug != null) {
      paramzzyy.zza(5, this.zzaug.floatValue());
    }
    if (this.zzauh != null) {
      paramzzyy.zza(6, this.zzauh.doubleValue());
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.zzayl != null) {
      i = j + zzyy.zzd(1, this.zzayl.longValue());
    }
    j = i;
    if (this.name != null) {
      j = i + zzyy.zzc(2, this.name);
    }
    i = j;
    if (this.zzamp != null) {
      i = j + zzyy.zzc(3, this.zzamp);
    }
    j = i;
    if (this.zzawx != null) {
      j = i + zzyy.zzd(4, this.zzawx.longValue());
    }
    i = j;
    if (this.zzaug != null)
    {
      this.zzaug.floatValue();
      i = j + (zzyy.zzbb(5) + 4);
    }
    j = i;
    if (this.zzauh != null)
    {
      this.zzauh.doubleValue();
      j = i + (zzyy.zzbb(6) + 8);
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */