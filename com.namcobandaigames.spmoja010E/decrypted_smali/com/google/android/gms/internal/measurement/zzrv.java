package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;

@VisibleForTesting
public final class zzrv
{
  private zzp zzbfg;
  private final Map<String, zzp> zzbon = new HashMap();
  
  public final zzrv zzb(String paramString, zzp paramzzp)
  {
    this.zzbon.put(paramString, paramzzp);
    return this;
  }
  
  public final zzrv zzm(zzp paramzzp)
  {
    this.zzbfg = paramzzp;
    return this;
  }
  
  public final zzru zzsq()
  {
    return new zzru(this.zzbon, this.zzbfg, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzrv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */