package com.google.android.gms.internal.measurement;

import java.util.Map;

final class zzde
  extends zzat
  implements zzbw<zzdf>
{
  private final zzdf zzach = new zzdf();
  
  public zzde(zzaw paramzzaw)
  {
    super(paramzzaw);
  }
  
  public final void zza(String paramString, boolean paramBoolean)
  {
    int j = 1;
    int k = 1;
    int i = 1;
    if ("ga_autoActivityTracking".equals(paramString))
    {
      paramString = this.zzach;
      if (paramBoolean) {}
      for (;;)
      {
        paramString.zzacl = i;
        return;
        i = 0;
      }
    }
    if ("ga_anonymizeIp".equals(paramString))
    {
      paramString = this.zzach;
      if (paramBoolean) {}
      for (i = j;; i = 0)
      {
        paramString.zzacm = i;
        return;
      }
    }
    if ("ga_reportUncaughtExceptions".equals(paramString))
    {
      paramString = this.zzach;
      if (paramBoolean) {}
      for (i = k;; i = 0)
      {
        paramString.zzacn = i;
        return;
      }
    }
    zzd("bool configuration name not recognized", paramString);
  }
  
  public final void zzb(String paramString, int paramInt)
  {
    if ("ga_sessionTimeout".equals(paramString))
    {
      this.zzach.zzack = paramInt;
      return;
    }
    zzd("int configuration name not recognized", paramString);
  }
  
  public final void zzb(String paramString1, String paramString2)
  {
    this.zzach.zzaco.put(paramString1, paramString2);
  }
  
  public final void zzc(String paramString1, String paramString2)
  {
    if ("ga_trackingId".equals(paramString1))
    {
      this.zzach.zzaci = paramString2;
      return;
    }
    if ("ga_sampleFrequency".equals(paramString1)) {
      try
      {
        this.zzach.zzacj = Double.parseDouble(paramString2);
        return;
      }
      catch (NumberFormatException paramString1)
      {
        zzc("Error parsing ga_sampleFrequency value", paramString2, paramString1);
        return;
      }
    }
    zzd("string configuration name not recognized", paramString1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzde.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */