package com.google.android.gms.internal.measurement;

import com.google.android.gms.tagmanager.zzdi;
import com.google.android.gms.tagmanager.zzgj;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class zzrs
{
  private static zzp zza(int paramInt, zzl paramzzl, zzp[] paramArrayOfzzp, Set<Integer> paramSet)
    throws zzsa
  {
    int k = 0;
    int m = 0;
    int j = 0;
    if (paramSet.contains(Integer.valueOf(paramInt)))
    {
      localObject = String.valueOf(paramSet);
      zzer(String.valueOf(localObject).length() + 90 + "Value cycle detected.  Current value reference: " + paramInt + ".  Previous value references: " + (String)localObject + ".");
    }
    zzp localzzp1 = (zzp)zza(paramzzl.zzou, paramInt, "values");
    if (paramArrayOfzzp[paramInt] != null) {
      return paramArrayOfzzp[paramInt];
    }
    Object localObject = null;
    paramSet.add(Integer.valueOf(paramInt));
    switch (localzzp1.type)
    {
    }
    for (;;)
    {
      if (localObject == null)
      {
        paramzzl = String.valueOf(localzzp1);
        zzer(String.valueOf(paramzzl).length() + 15 + "Invalid value: " + paramzzl);
      }
      paramArrayOfzzp[paramInt] = localObject;
      paramSet.remove(Integer.valueOf(paramInt));
      return (zzp)localObject;
      localObject = zzl(localzzp1);
      zzp localzzp2 = zzk(localzzp1);
      localzzp2.zzqj = new zzp[((zzg.zza)localObject).zzpv.length];
      int[] arrayOfInt = ((zzg.zza)localObject).zzpv;
      k = arrayOfInt.length;
      int i = 0;
      for (;;)
      {
        localObject = localzzp2;
        if (j >= k) {
          break;
        }
        m = arrayOfInt[j];
        localzzp2.zzqj[i] = zza(m, paramzzl, paramArrayOfzzp, paramSet);
        j += 1;
        i += 1;
      }
      localzzp2 = zzk(localzzp1);
      localObject = zzl(localzzp1);
      if (((zzg.zza)localObject).zzpw.length != ((zzg.zza)localObject).zzpx.length)
      {
        i = ((zzg.zza)localObject).zzpw.length;
        j = ((zzg.zza)localObject).zzpx.length;
        zzer(58 + "Uneven map keys (" + i + ") and map values (" + j + ")");
      }
      localzzp2.zzqk = new zzp[((zzg.zza)localObject).zzpw.length];
      localzzp2.zzql = new zzp[((zzg.zza)localObject).zzpw.length];
      arrayOfInt = ((zzg.zza)localObject).zzpw;
      m = arrayOfInt.length;
      j = 0;
      i = 0;
      while (j < m)
      {
        int n = arrayOfInt[j];
        localzzp2.zzqk[i] = zza(n, paramzzl, paramArrayOfzzp, paramSet);
        j += 1;
        i += 1;
      }
      arrayOfInt = ((zzg.zza)localObject).zzpx;
      m = arrayOfInt.length;
      i = 0;
      j = k;
      for (;;)
      {
        localObject = localzzp2;
        if (j >= m) {
          break;
        }
        k = arrayOfInt[j];
        localzzp2.zzql[i] = zza(k, paramzzl, paramArrayOfzzp, paramSet);
        j += 1;
        i += 1;
      }
      localObject = zzk(localzzp1);
      ((zzp)localObject).zzqm = zzgj.zzc(zza(zzl(localzzp1).zzqa, paramzzl, paramArrayOfzzp, paramSet));
      continue;
      localzzp2 = zzk(localzzp1);
      localObject = zzl(localzzp1);
      localzzp2.zzqq = new zzp[((zzg.zza)localObject).zzpz.length];
      arrayOfInt = ((zzg.zza)localObject).zzpz;
      k = arrayOfInt.length;
      i = 0;
      j = m;
      for (;;)
      {
        localObject = localzzp2;
        if (j >= k) {
          break;
        }
        m = arrayOfInt[j];
        localzzp2.zzqq[i] = zza(m, paramzzl, paramArrayOfzzp, paramSet);
        j += 1;
        i += 1;
      }
      localObject = localzzp1;
    }
  }
  
  private static zzru zza(zzh paramzzh, zzl paramzzl, zzp[] paramArrayOfzzp, int paramInt)
    throws zzsa
  {
    zzrv localzzrv = zzru.zzsp();
    paramzzh = paramzzh.zzoe;
    int i = paramzzh.length;
    paramInt = 0;
    if (paramInt < i)
    {
      int j = paramzzh[paramInt];
      Object localObject = (zzk)zza(paramzzl.zzov, Integer.valueOf(j).intValue(), "properties");
      String str = (String)zza(paramzzl.zzot, ((zzk)localObject).key, "keys");
      localObject = (zzp)zza(paramArrayOfzzp, ((zzk)localObject).value, "values");
      if (zzb.zzks.toString().equals(str)) {
        localzzrv.zzm((zzp)localObject);
      }
      for (;;)
      {
        paramInt += 1;
        break;
        localzzrv.zzb(str, (zzp)localObject);
      }
    }
    return localzzrv.zzsq();
  }
  
  public static zzrw zza(zzl paramzzl)
    throws zzsa
  {
    Object localObject1 = new zzp[paramzzl.zzou.length];
    int i = 0;
    while (i < paramzzl.zzou.length)
    {
      zza(i, paramzzl, (zzp[])localObject1, new HashSet(0));
      i += 1;
    }
    zzrx localzzrx = zzrw.zzsr();
    ArrayList localArrayList1 = new ArrayList();
    i = 0;
    while (i < paramzzl.zzox.length)
    {
      localArrayList1.add(zza(paramzzl.zzox[i], paramzzl, (zzp[])localObject1, i));
      i += 1;
    }
    ArrayList localArrayList2 = new ArrayList();
    i = 0;
    while (i < paramzzl.zzoy.length)
    {
      localArrayList2.add(zza(paramzzl.zzoy[i], paramzzl, (zzp[])localObject1, i));
      i += 1;
    }
    ArrayList localArrayList3 = new ArrayList();
    i = 0;
    Object localObject2;
    while (i < paramzzl.zzow.length)
    {
      localObject2 = zza(paramzzl.zzow[i], paramzzl, (zzp[])localObject1, i);
      localzzrx.zzc((zzru)localObject2);
      localArrayList3.add(localObject2);
      i += 1;
    }
    localObject1 = paramzzl.zzoz;
    int k = localObject1.length;
    i = 0;
    while (i < k)
    {
      int[] arrayOfInt1 = localObject1[i];
      localObject2 = new zzrz(null);
      int[] arrayOfInt2 = arrayOfInt1.zzpj;
      int m = arrayOfInt2.length;
      int j = 0;
      while (j < m)
      {
        ((zzrz)localObject2).zzd((zzru)localArrayList2.get(Integer.valueOf(arrayOfInt2[j]).intValue()));
        j += 1;
      }
      arrayOfInt2 = arrayOfInt1.zzpk;
      m = arrayOfInt2.length;
      j = 0;
      while (j < m)
      {
        ((zzrz)localObject2).zze((zzru)localArrayList2.get(Integer.valueOf(arrayOfInt2[j]).intValue()));
        j += 1;
      }
      arrayOfInt2 = arrayOfInt1.zzpl;
      m = arrayOfInt2.length;
      j = 0;
      while (j < m)
      {
        ((zzrz)localObject2).zzf((zzru)localArrayList1.get(Integer.valueOf(arrayOfInt2[j]).intValue()));
        j += 1;
      }
      arrayOfInt2 = arrayOfInt1.zzpn;
      m = arrayOfInt2.length;
      j = 0;
      int n;
      while (j < m)
      {
        n = arrayOfInt2[j];
        ((zzrz)localObject2).zzff(paramzzl.zzou[Integer.valueOf(n).intValue()].string);
        j += 1;
      }
      arrayOfInt2 = arrayOfInt1.zzpm;
      m = arrayOfInt2.length;
      j = 0;
      while (j < m)
      {
        ((zzrz)localObject2).zzg((zzru)localArrayList1.get(Integer.valueOf(arrayOfInt2[j]).intValue()));
        j += 1;
      }
      arrayOfInt2 = arrayOfInt1.zzpo;
      m = arrayOfInt2.length;
      j = 0;
      while (j < m)
      {
        n = arrayOfInt2[j];
        ((zzrz)localObject2).zzfg(paramzzl.zzou[Integer.valueOf(n).intValue()].string);
        j += 1;
      }
      arrayOfInt2 = arrayOfInt1.zzpp;
      m = arrayOfInt2.length;
      j = 0;
      while (j < m)
      {
        ((zzrz)localObject2).zzh((zzru)localArrayList3.get(Integer.valueOf(arrayOfInt2[j]).intValue()));
        j += 1;
      }
      arrayOfInt2 = arrayOfInt1.zzpr;
      m = arrayOfInt2.length;
      j = 0;
      while (j < m)
      {
        n = arrayOfInt2[j];
        ((zzrz)localObject2).zzfh(paramzzl.zzou[Integer.valueOf(n).intValue()].string);
        j += 1;
      }
      arrayOfInt2 = arrayOfInt1.zzpq;
      m = arrayOfInt2.length;
      j = 0;
      while (j < m)
      {
        ((zzrz)localObject2).zzi((zzru)localArrayList3.get(Integer.valueOf(arrayOfInt2[j]).intValue()));
        j += 1;
      }
      arrayOfInt1 = arrayOfInt1.zzps;
      m = arrayOfInt1.length;
      j = 0;
      while (j < m)
      {
        n = arrayOfInt1[j];
        ((zzrz)localObject2).zzfi(paramzzl.zzou[Integer.valueOf(n).intValue()].string);
        j += 1;
      }
      localzzrx.zzb(((zzrz)localObject2).zzsw());
      i += 1;
    }
    localzzrx.zzfe(paramzzl.version);
    localzzrx.zzag(paramzzl.zzph);
    return localzzrx.zzst();
  }
  
  private static <T> T zza(T[] paramArrayOfT, int paramInt, String paramString)
    throws zzsa
  {
    if ((paramInt < 0) || (paramInt >= paramArrayOfT.length)) {
      zzer(String.valueOf(paramString).length() + 45 + "Index out of bounds detected: " + paramInt + " in " + paramString);
    }
    return paramArrayOfT[paramInt];
  }
  
  public static void zza(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    byte[] arrayOfByte = new byte['Ѐ'];
    for (;;)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (i == -1) {
        return;
      }
      paramOutputStream.write(arrayOfByte, 0, i);
    }
  }
  
  private static void zzer(String paramString)
    throws zzsa
  {
    zzdi.e(paramString);
    throw new zzsa(paramString);
  }
  
  public static zzp zzk(zzp paramzzp)
  {
    zzp localzzp = new zzp();
    localzzp.type = paramzzp.type;
    localzzp.zzqr = ((int[])paramzzp.zzqr.clone());
    if (paramzzp.zzqs) {
      localzzp.zzqs = paramzzp.zzqs;
    }
    return localzzp;
  }
  
  private static zzg.zza zzl(zzp paramzzp)
    throws zzsa
  {
    if ((zzg.zza)paramzzp.zza(zzg.zza.zzpt) == null)
    {
      String str = String.valueOf(paramzzp);
      zzer(String.valueOf(str).length() + 54 + "Expected a ServingValue and didn't get one. Value is: " + str);
    }
    return (zzg.zza)paramzzp.zza(zzg.zza.zzpt);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzrs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */