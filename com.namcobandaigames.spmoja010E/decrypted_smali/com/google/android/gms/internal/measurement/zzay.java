package com.google.android.gms.internal.measurement;

import android.content.Context;
import com.google.android.gms.common.internal.Preconditions;

public final class zzay
{
  private final Context zzwq;
  private final Context zzwr;
  
  public zzay(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    paramContext = paramContext.getApplicationContext();
    Preconditions.checkNotNull(paramContext, "Application context can't be null");
    this.zzwq = paramContext;
    this.zzwr = paramContext;
  }
  
  public final Context getApplicationContext()
  {
    return this.zzwq;
  }
  
  public final Context zzcm()
  {
    return this.zzwr;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */