package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzwh
  extends zztz<Long>
  implements zzvs<Long>, zzxe, RandomAccess
{
  private static final zzwh zzcam;
  private int size;
  private long[] zzcan;
  
  static
  {
    zzwh localzzwh = new zzwh();
    zzcam = localzzwh;
    localzzwh.zzsm();
  }
  
  zzwh()
  {
    this(new long[10], 0);
  }
  
  private zzwh(long[] paramArrayOfLong, int paramInt)
  {
    this.zzcan = paramArrayOfLong;
    this.size = paramInt;
  }
  
  private final void zzai(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
  }
  
  private final String zzaj(int paramInt)
  {
    int i = this.size;
    return 35 + "Index:" + paramInt + ", Size:" + i;
  }
  
  private final void zzk(int paramInt, long paramLong)
  {
    zztx();
    if ((paramInt < 0) || (paramInt > this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
    if (this.size < this.zzcan.length) {
      System.arraycopy(this.zzcan, paramInt, this.zzcan, paramInt + 1, this.size - paramInt);
    }
    for (;;)
    {
      this.zzcan[paramInt] = paramLong;
      this.size += 1;
      this.modCount += 1;
      return;
      long[] arrayOfLong = new long[this.size * 3 / 2 + 1];
      System.arraycopy(this.zzcan, 0, arrayOfLong, 0, paramInt);
      System.arraycopy(this.zzcan, paramInt, arrayOfLong, paramInt + 1, this.size - paramInt);
      this.zzcan = arrayOfLong;
    }
  }
  
  public final boolean addAll(Collection<? extends Long> paramCollection)
  {
    boolean bool = false;
    zztx();
    zzvo.checkNotNull(paramCollection);
    if (!(paramCollection instanceof zzwh)) {
      bool = super.addAll(paramCollection);
    }
    do
    {
      return bool;
      paramCollection = (zzwh)paramCollection;
    } while (paramCollection.size == 0);
    if (Integer.MAX_VALUE - this.size < paramCollection.size) {
      throw new OutOfMemoryError();
    }
    int i = this.size + paramCollection.size;
    if (i > this.zzcan.length) {
      this.zzcan = Arrays.copyOf(this.zzcan, i);
    }
    System.arraycopy(paramCollection.zzcan, 0, this.zzcan, this.size, paramCollection.size);
    this.size = i;
    this.modCount += 1;
    return true;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      return bool1;
      if (!(paramObject instanceof zzwh)) {
        return super.equals(paramObject);
      }
      paramObject = (zzwh)paramObject;
      bool1 = bool2;
    } while (this.size != ((zzwh)paramObject).size);
    paramObject = ((zzwh)paramObject).zzcan;
    int i = 0;
    for (;;)
    {
      if (i >= this.size) {
        break label82;
      }
      bool1 = bool2;
      if (this.zzcan[i] != paramObject[i]) {
        break;
      }
      i += 1;
    }
    label82:
    return true;
  }
  
  public final long getLong(int paramInt)
  {
    zzai(paramInt);
    return this.zzcan[paramInt];
  }
  
  public final int hashCode()
  {
    int j = 1;
    int i = 0;
    while (i < this.size)
    {
      j = j * 31 + zzvo.zzbf(this.zzcan[i]);
      i += 1;
    }
    return j;
  }
  
  public final boolean remove(Object paramObject)
  {
    boolean bool2 = false;
    zztx();
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      if (i < this.size)
      {
        if (paramObject.equals(Long.valueOf(this.zzcan[i])))
        {
          System.arraycopy(this.zzcan, i + 1, this.zzcan, i, this.size - i);
          this.size -= 1;
          this.modCount += 1;
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
      i += 1;
    }
  }
  
  protected final void removeRange(int paramInt1, int paramInt2)
  {
    zztx();
    if (paramInt2 < paramInt1) {
      throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }
    System.arraycopy(this.zzcan, paramInt2, this.zzcan, paramInt1, this.size - paramInt2);
    this.size -= paramInt2 - paramInt1;
    this.modCount += 1;
  }
  
  public final int size()
  {
    return this.size;
  }
  
  public final void zzbg(long paramLong)
  {
    zzk(this.size, paramLong);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */