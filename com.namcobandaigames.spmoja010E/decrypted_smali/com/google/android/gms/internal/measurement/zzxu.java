package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class zzxu
  implements Iterator<Map.Entry<K, V>>
{
  private int pos = -1;
  private Iterator<Map.Entry<K, V>> zzccg;
  private boolean zzccl;
  
  private zzxu(zzxm paramzzxm) {}
  
  private final Iterator<Map.Entry<K, V>> zzyb()
  {
    if (this.zzccg == null) {
      this.zzccg = zzxm.zzc(this.zzcch).entrySet().iterator();
    }
    return this.zzccg;
  }
  
  public final boolean hasNext()
  {
    return (this.pos + 1 < zzxm.zzb(this.zzcch).size()) || ((!zzxm.zzc(this.zzcch).isEmpty()) && (zzyb().hasNext()));
  }
  
  public final void remove()
  {
    if (!this.zzccl) {
      throw new IllegalStateException("remove() was called before next()");
    }
    this.zzccl = false;
    zzxm.zza(this.zzcch);
    if (this.pos < zzxm.zzb(this.zzcch).size())
    {
      zzxm localzzxm = this.zzcch;
      int i = this.pos;
      this.pos = (i - 1);
      zzxm.zza(localzzxm, i);
      return;
    }
    zzyb().remove();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */