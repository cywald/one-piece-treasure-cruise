package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzi;
import java.util.HashMap;
import java.util.Map;

public final class zzai
  extends zzi<zzai>
{
  public String zzvd;
  public String zzve;
  public String zzvf;
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("network", this.zzvd);
    localHashMap.put("action", this.zzve);
    localHashMap.put("target", this.zzvf);
    return zza(localHashMap);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */