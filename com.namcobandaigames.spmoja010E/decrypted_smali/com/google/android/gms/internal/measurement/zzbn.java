package com.google.android.gms.internal.measurement;

public enum zzbn
{
  private zzbn() {}
  
  public static zzbn zzz(String paramString)
  {
    if ("BATCH_BY_SESSION".equalsIgnoreCase(paramString)) {
      return zzxx;
    }
    if ("BATCH_BY_TIME".equalsIgnoreCase(paramString)) {
      return zzxy;
    }
    if ("BATCH_BY_BRUTE_FORCE".equalsIgnoreCase(paramString)) {
      return zzxz;
    }
    if ("BATCH_BY_COUNT".equalsIgnoreCase(paramString)) {
      return zzya;
    }
    if ("BATCH_BY_SIZE".equalsIgnoreCase(paramString)) {
      return zzyb;
    }
    return zzxw;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */