package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.tagmanager.zzgj;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@VisibleForTesting
public final class zzrx
{
  private String version = "";
  private final List<zzry> zzbol = new ArrayList();
  private final Map<String, List<zzru>> zzbom = new HashMap();
  private int zzph = 0;
  
  public final zzrx zzag(int paramInt)
  {
    this.zzph = paramInt;
    return this;
  }
  
  public final zzrx zzb(zzry paramzzry)
  {
    this.zzbol.add(paramzzry);
    return this;
  }
  
  public final zzrx zzc(zzru paramzzru)
  {
    String str = zzgj.zzc((zzp)paramzzru.zzry().get(zzb.zzil.toString()));
    List localList = (List)this.zzbom.get(str);
    Object localObject = localList;
    if (localList == null)
    {
      localObject = new ArrayList();
      this.zzbom.put(str, localObject);
    }
    ((List)localObject).add(paramzzru);
    return this;
  }
  
  public final zzrx zzfe(String paramString)
  {
    this.version = paramString;
    return this;
  }
  
  public final zzrw zzst()
  {
    return new zzrw(this.zzbol, this.zzbom, this.version, this.zzph, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzrx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */