package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzi
  extends zzza<zzi>
{
  private static volatile zzi[] zzoi;
  public String zzoj = "";
  public long zzok = 0L;
  public long zzol = 2147483647L;
  public boolean zzom = false;
  public long zzon = 0L;
  
  public zzi()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzi[] zzg()
  {
    if (zzoi == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzoi == null) {
        zzoi = new zzi[0];
      }
      return zzoi;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzi)) {
        return false;
      }
      paramObject = (zzi)paramObject;
      if (this.zzoj == null)
      {
        if (((zzi)paramObject).zzoj != null) {
          return false;
        }
      }
      else if (!this.zzoj.equals(((zzi)paramObject).zzoj)) {
        return false;
      }
      if (this.zzok != ((zzi)paramObject).zzok) {
        return false;
      }
      if (this.zzol != ((zzi)paramObject).zzol) {
        return false;
      }
      if (this.zzom != ((zzi)paramObject).zzom) {
        return false;
      }
      if (this.zzon != ((zzi)paramObject).zzon) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzi)paramObject).zzcfc == null) || (((zzi)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzi)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int m = 0;
    int n = getClass().getName().hashCode();
    int i;
    int i1;
    int i2;
    int j;
    label65:
    int i3;
    if (this.zzoj == null)
    {
      i = 0;
      i1 = (int)(this.zzok ^ this.zzok >>> 32);
      i2 = (int)(this.zzol ^ this.zzol >>> 32);
      if (!this.zzom) {
        break label154;
      }
      j = 1231;
      i3 = (int)(this.zzon ^ this.zzon >>> 32);
      k = m;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label161;
        }
      }
    }
    label154:
    label161:
    for (int k = m;; k = this.zzcfc.hashCode())
    {
      return ((j + (((i + (n + 527) * 31) * 31 + i1) * 31 + i2) * 31) * 31 + i3) * 31 + k;
      i = this.zzoj.hashCode();
      break;
      j = 1237;
      break label65;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if ((this.zzoj != null) && (!this.zzoj.equals(""))) {
      paramzzyy.zzb(1, this.zzoj);
    }
    if (this.zzok != 0L) {
      paramzzyy.zzi(2, this.zzok);
    }
    if (this.zzol != 2147483647L) {
      paramzzyy.zzi(3, this.zzol);
    }
    if (this.zzom) {
      paramzzyy.zzb(4, this.zzom);
    }
    if (this.zzon != 0L) {
      paramzzyy.zzi(5, this.zzon);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int j = super.zzf();
    int i = j;
    if (this.zzoj != null)
    {
      i = j;
      if (!this.zzoj.equals("")) {
        i = j + zzyy.zzc(1, this.zzoj);
      }
    }
    j = i;
    if (this.zzok != 0L) {
      j = i + zzyy.zzd(2, this.zzok);
    }
    i = j;
    if (this.zzol != 2147483647L) {
      i = j + zzyy.zzd(3, this.zzol);
    }
    j = i;
    if (this.zzom) {
      j = i + (zzyy.zzbb(4) + 1);
    }
    i = j;
    if (this.zzon != 0L) {
      i = j + zzyy.zzd(5, this.zzon);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */