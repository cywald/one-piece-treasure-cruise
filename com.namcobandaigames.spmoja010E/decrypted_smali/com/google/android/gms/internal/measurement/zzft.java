package com.google.android.gms.internal.measurement;

final class zzft
  implements zzvr
{
  static final zzvr zzoc = new zzft();
  
  public final boolean zzb(int paramInt)
  {
    return zzfq.zzb.zzb.zzs(paramInt) != null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzft.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */