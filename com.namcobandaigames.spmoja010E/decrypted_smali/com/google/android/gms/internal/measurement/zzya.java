package com.google.android.gms.internal.measurement;

import java.util.List;

public final class zzya
  extends RuntimeException
{
  private final List<String> zzccn = null;
  
  public zzya(zzwt paramzzwt)
  {
    super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzya.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */