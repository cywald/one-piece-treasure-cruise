package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class zzck
{
  private final List<zzbr> zzaav;
  private final long zzaaw;
  private final long zzaax;
  private final int zzaay;
  private final boolean zzaaz;
  private final String zzaba;
  private final Map<String, String> zzsy;
  
  public zzck(zzat paramzzat, Map<String, String> paramMap, long paramLong, boolean paramBoolean)
  {
    this(paramzzat, paramMap, paramLong, paramBoolean, 0L, 0, null);
  }
  
  public zzck(zzat paramzzat, Map<String, String> paramMap, long paramLong1, boolean paramBoolean, long paramLong2, int paramInt)
  {
    this(paramzzat, paramMap, paramLong1, paramBoolean, paramLong2, paramInt, null);
  }
  
  public zzck(zzat paramzzat, Map<String, String> paramMap, long paramLong1, boolean paramBoolean, long paramLong2, int paramInt, List<zzbr> paramList)
  {
    Preconditions.checkNotNull(paramzzat);
    Preconditions.checkNotNull(paramMap);
    this.zzaax = paramLong1;
    this.zzaaz = paramBoolean;
    this.zzaaw = paramLong2;
    this.zzaay = paramInt;
    Object localObject1;
    if (paramList != null)
    {
      localObject1 = paramList;
      this.zzaav = ((List)localObject1);
      if (paramList == null) {
        break label391;
      }
      paramList = paramList.iterator();
      do
      {
        if (!paramList.hasNext()) {
          break;
        }
        localObject1 = (zzbr)paramList.next();
      } while (!"appendVersion".equals(((zzbr)localObject1).getId()));
    }
    label391:
    for (paramList = ((zzbr)localObject1).getValue();; paramList = null)
    {
      if (TextUtils.isEmpty(paramList)) {
        paramList = (List<zzbr>)localObject2;
      }
      for (;;)
      {
        this.zzaba = paramList;
        paramList = new HashMap();
        localObject1 = paramMap.entrySet().iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = (Map.Entry)((Iterator)localObject1).next();
          if (zzc(((Map.Entry)localObject2).getKey()))
          {
            String str = zza(paramzzat, ((Map.Entry)localObject2).getKey());
            if (str != null) {
              paramList.put(str, zzb(paramzzat, ((Map.Entry)localObject2).getValue()));
            }
          }
        }
        localObject1 = Collections.emptyList();
        break;
      }
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        localObject1 = (Map.Entry)paramMap.next();
        if (!zzc(((Map.Entry)localObject1).getKey()))
        {
          localObject2 = zza(paramzzat, ((Map.Entry)localObject1).getKey());
          if (localObject2 != null) {
            paramList.put(localObject2, zzb(paramzzat, ((Map.Entry)localObject1).getValue()));
          }
        }
      }
      if (!TextUtils.isEmpty(this.zzaba))
      {
        zzdg.zzb(paramList, "_v", this.zzaba);
        if ((this.zzaba.equals("ma4.0.0")) || (this.zzaba.equals("ma4.0.1"))) {
          paramList.remove("adid");
        }
      }
      this.zzsy = Collections.unmodifiableMap(paramList);
      return;
    }
  }
  
  private static String zza(zzat paramzzat, Object paramObject)
  {
    if (paramObject == null) {
      paramzzat = null;
    }
    Object localObject;
    do
    {
      return paramzzat;
      localObject = paramObject.toString();
      paramObject = localObject;
      if (((String)localObject).startsWith("&")) {
        paramObject = ((String)localObject).substring(1);
      }
      int i = ((String)paramObject).length();
      localObject = paramObject;
      if (i > 256)
      {
        localObject = ((String)paramObject).substring(0, 256);
        paramzzat.zzc("Hit param name is too long and will be trimmed", Integer.valueOf(i), localObject);
      }
      paramzzat = (zzat)localObject;
    } while (!TextUtils.isEmpty((CharSequence)localObject));
    return null;
  }
  
  private static String zzb(zzat paramzzat, Object paramObject)
  {
    if (paramObject == null) {}
    for (paramObject = "";; paramObject = paramObject.toString())
    {
      int i = ((String)paramObject).length();
      Object localObject = paramObject;
      if (i > 8192)
      {
        localObject = ((String)paramObject).substring(0, 8192);
        paramzzat.zzc("Hit param value is too long and will be trimmed", Integer.valueOf(i), localObject);
      }
      return (String)localObject;
    }
  }
  
  private static boolean zzc(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    return paramObject.toString().startsWith("&");
  }
  
  private final String zzd(String paramString1, String paramString2)
  {
    Preconditions.checkNotEmpty(paramString1);
    if (!paramString1.startsWith("&")) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkArgument(bool, "Short param name required");
      paramString1 = (String)this.zzsy.get(paramString1);
      if (paramString1 == null) {
        break;
      }
      return paramString1;
    }
    return paramString2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("ht=").append(this.zzaax);
    if (this.zzaaw != 0L) {
      localStringBuilder.append(", dbId=").append(this.zzaaw);
    }
    if (this.zzaay != 0) {
      localStringBuilder.append(", appUID=").append(this.zzaay);
    }
    ArrayList localArrayList = new ArrayList(this.zzsy.keySet());
    Collections.sort(localArrayList);
    localArrayList = (ArrayList)localArrayList;
    int j = localArrayList.size();
    int i = 0;
    while (i < j)
    {
      Object localObject = localArrayList.get(i);
      i += 1;
      localObject = (String)localObject;
      localStringBuilder.append(", ");
      localStringBuilder.append((String)localObject);
      localStringBuilder.append("=");
      localStringBuilder.append((String)this.zzsy.get(localObject));
    }
    return localStringBuilder.toString();
  }
  
  public final Map<String, String> zzcw()
  {
    return this.zzsy;
  }
  
  public final int zzep()
  {
    return this.zzaay;
  }
  
  public final long zzeq()
  {
    return this.zzaaw;
  }
  
  public final long zzer()
  {
    return this.zzaax;
  }
  
  public final List<zzbr> zzes()
  {
    return this.zzaav;
  }
  
  public final boolean zzet()
  {
    return this.zzaaz;
  }
  
  public final long zzeu()
  {
    return zzdg.zzaf(zzd("_s", "0"));
  }
  
  public final String zzev()
  {
    return zzd("_m", "");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzck.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */