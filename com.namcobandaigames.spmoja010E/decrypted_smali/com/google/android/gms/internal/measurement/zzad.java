package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.analytics.zzi;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class zzad
  extends zzi<zzad>
{
  private ProductAction zzrt;
  private final Map<String, List<Product>> zzru = new HashMap();
  private final List<Promotion> zzrv = new ArrayList();
  private final List<Product> zzrw = new ArrayList();
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    if (!this.zzrw.isEmpty()) {
      localHashMap.put("products", this.zzrw);
    }
    if (!this.zzrv.isEmpty()) {
      localHashMap.put("promotions", this.zzrv);
    }
    if (!this.zzru.isEmpty()) {
      localHashMap.put("impressions", this.zzru);
    }
    localHashMap.put("productAction", this.zzrt);
    return zza(localHashMap);
  }
  
  public final ProductAction zzax()
  {
    return this.zzrt;
  }
  
  public final List<Product> zzay()
  {
    return Collections.unmodifiableList(this.zzrw);
  }
  
  public final Map<String, List<Product>> zzaz()
  {
    return this.zzru;
  }
  
  public final List<Promotion> zzba()
  {
    return Collections.unmodifiableList(this.zzrv);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */