package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzzj
{
  public static final int[] zzcax = new int[0];
  private static final int zzcfn = 11;
  private static final int zzcfo = 12;
  private static final int zzcfp = 16;
  private static final int zzcfq = 26;
  public static final long[] zzcfr = new long[0];
  private static final float[] zzcfs = new float[0];
  private static final double[] zzcft = new double[0];
  private static final boolean[] zzcfu = new boolean[0];
  public static final String[] zzcfv = new String[0];
  private static final byte[][] zzcfw = new byte[0][];
  public static final byte[] zzcfx = new byte[0];
  
  public static final int zzb(zzyx paramzzyx, int paramInt)
    throws IOException
  {
    int i = 1;
    int j = paramzzyx.getPosition();
    paramzzyx.zzao(paramInt);
    while (paramzzyx.zzug() == paramInt)
    {
      paramzzyx.zzao(paramInt);
      i += 1;
    }
    paramzzyx.zzt(j, paramInt);
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */