package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzgk
  extends zzza<zzgk>
{
  private static volatile zzgk[] zzayi;
  public Integer zzawq = null;
  public long[] zzayj = zzzj.zzcfr;
  
  public zzgk()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzgk[] zzmt()
  {
    if (zzayi == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzayi == null) {
        zzayi = new zzgk[0];
      }
      return zzayi;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzgk)) {
        return false;
      }
      paramObject = (zzgk)paramObject;
      if (this.zzawq == null)
      {
        if (((zzgk)paramObject).zzawq != null) {
          return false;
        }
      }
      else if (!this.zzawq.equals(((zzgk)paramObject).zzawq)) {
        return false;
      }
      if (!zzze.equals(this.zzayj, ((zzgk)paramObject).zzayj)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzgk)paramObject).zzcfc == null) || (((zzgk)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzgk)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int k = 0;
    int m = getClass().getName().hashCode();
    int i;
    int n;
    if (this.zzawq == null)
    {
      i = 0;
      n = zzze.hashCode(this.zzayj);
      j = k;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label87;
        }
      }
    }
    label87:
    for (int j = k;; j = this.zzcfc.hashCode())
    {
      return ((i + (m + 527) * 31) * 31 + n) * 31 + j;
      i = this.zzawq.hashCode();
      break;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    if (this.zzawq != null) {
      paramzzyy.zzd(1, this.zzawq.intValue());
    }
    if ((this.zzayj != null) && (this.zzayj.length > 0))
    {
      int i = 0;
      while (i < this.zzayj.length)
      {
        paramzzyy.zzi(2, this.zzayj[i]);
        i += 1;
      }
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int m = 0;
    int j = super.zzf();
    int i = j;
    if (this.zzawq != null) {
      i = j + zzyy.zzh(1, this.zzawq.intValue());
    }
    j = i;
    if (this.zzayj != null)
    {
      j = i;
      if (this.zzayj.length > 0)
      {
        int k = 0;
        j = m;
        while (j < this.zzayj.length)
        {
          k += zzyy.zzbi(this.zzayj[j]);
          j += 1;
        }
        j = i + k + this.zzayj.length * 1;
      }
    }
    return j;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzgk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */