package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class zzxo
  implements Iterator<Map.Entry<K, V>>
{
  private int pos = zzxm.zzb(this.zzcch).size();
  private Iterator<Map.Entry<K, V>> zzccg;
  
  private zzxo(zzxm paramzzxm) {}
  
  private final Iterator<Map.Entry<K, V>> zzyb()
  {
    if (this.zzccg == null) {
      this.zzccg = zzxm.zzd(this.zzcch).entrySet().iterator();
    }
    return this.zzccg;
  }
  
  public final boolean hasNext()
  {
    return ((this.pos > 0) && (this.pos <= zzxm.zzb(this.zzcch).size())) || (zzyb().hasNext());
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */