package com.google.android.gms.internal.measurement;

import java.util.List;

abstract class zzwd
{
  private static final zzwd zzcaj = new zzwf(null);
  private static final zzwd zzcak = new zzwg(null);
  
  static zzwd zzwx()
  {
    return zzcaj;
  }
  
  static zzwd zzwy()
  {
    return zzcak;
  }
  
  abstract <L> List<L> zza(Object paramObject, long paramLong);
  
  abstract <L> void zza(Object paramObject1, Object paramObject2, long paramLong);
  
  abstract void zzb(Object paramObject, long paramLong);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzwd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */