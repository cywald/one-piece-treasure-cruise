package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class zzcr
  extends zzau
{
  private static final byte[] zzabn = "\n".getBytes();
  private final String zzabl = String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", new Object[] { "GoogleAnalytics", zzav.VERSION, Build.VERSION.RELEASE, zzdg.zza(Locale.getDefault()), Build.MODEL, Build.ID });
  private final zzdc zzabm;
  
  zzcr(zzaw paramzzaw)
  {
    super(paramzzaw);
    this.zzabm = new zzdc(paramzzaw.zzbx());
  }
  
  private final int zza(URL paramURL)
  {
    Preconditions.checkNotNull(paramURL);
    zzb("GET request", paramURL);
    Object localObject = null;
    URL localURL = null;
    try
    {
      paramURL = zzb(paramURL);
      localURL = paramURL;
      localObject = paramURL;
      paramURL.connect();
      localURL = paramURL;
      localObject = paramURL;
      zza(paramURL);
      localURL = paramURL;
      localObject = paramURL;
      int i = paramURL.getResponseCode();
      if (i == 200)
      {
        localURL = paramURL;
        localObject = paramURL;
        zzcc().zzbv();
      }
      localURL = paramURL;
      localObject = paramURL;
      zzb("GET status", Integer.valueOf(i));
      if (paramURL != null) {
        paramURL.disconnect();
      }
      return i;
    }
    catch (IOException paramURL)
    {
      localObject = localURL;
      zzd("Network GET connection error", paramURL);
      if (localURL != null) {
        localURL.disconnect();
      }
      return 0;
    }
    finally
    {
      if (localObject != null) {
        ((HttpURLConnection)localObject).disconnect();
      }
    }
  }
  
  /* Error */
  private final int zza(URL paramURL, byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aconst_null
    //   4: astore 7
    //   6: aconst_null
    //   7: astore 8
    //   9: aconst_null
    //   10: astore 6
    //   12: aload_1
    //   13: invokestatic 91	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   16: pop
    //   17: aload_2
    //   18: invokestatic 91	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   21: pop
    //   22: aload_0
    //   23: ldc -114
    //   25: aload_2
    //   26: arraylength
    //   27: invokestatic 131	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   30: aload_1
    //   31: invokevirtual 145	com/google/android/gms/internal/measurement/zzat:zzb	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   34: invokestatic 149	com/google/android/gms/internal/measurement/zzcr:zzck	()Z
    //   37: ifeq +17 -> 54
    //   40: aload_0
    //   41: ldc -105
    //   43: new 16	java/lang/String
    //   46: dup
    //   47: aload_2
    //   48: invokespecial 154	java/lang/String:<init>	([B)V
    //   51: invokevirtual 156	com/google/android/gms/internal/measurement/zzat:zza	(Ljava/lang/String;Ljava/lang/Object;)V
    //   54: aload_0
    //   55: invokevirtual 160	com/google/android/gms/internal/measurement/zzat:getContext	()Landroid/content/Context;
    //   58: invokevirtual 166	android/content/Context:getPackageName	()Ljava/lang/String;
    //   61: pop
    //   62: aload_0
    //   63: aload_1
    //   64: invokespecial 102	com/google/android/gms/internal/measurement/zzcr:zzb	(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    //   67: astore_1
    //   68: aload_1
    //   69: astore 5
    //   71: aload 7
    //   73: astore_1
    //   74: aload 5
    //   76: astore 4
    //   78: aload 8
    //   80: astore 6
    //   82: aload 5
    //   84: iconst_1
    //   85: invokevirtual 170	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   88: aload 7
    //   90: astore_1
    //   91: aload 5
    //   93: astore 4
    //   95: aload 8
    //   97: astore 6
    //   99: aload 5
    //   101: aload_2
    //   102: arraylength
    //   103: invokevirtual 174	java/net/HttpURLConnection:setFixedLengthStreamingMode	(I)V
    //   106: aload 7
    //   108: astore_1
    //   109: aload 5
    //   111: astore 4
    //   113: aload 8
    //   115: astore 6
    //   117: aload 5
    //   119: invokevirtual 107	java/net/HttpURLConnection:connect	()V
    //   122: aload 7
    //   124: astore_1
    //   125: aload 5
    //   127: astore 4
    //   129: aload 8
    //   131: astore 6
    //   133: aload 5
    //   135: invokevirtual 178	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   138: astore 7
    //   140: aload 7
    //   142: astore_1
    //   143: aload 5
    //   145: astore 4
    //   147: aload 7
    //   149: astore 6
    //   151: aload 7
    //   153: aload_2
    //   154: invokevirtual 183	java/io/OutputStream:write	([B)V
    //   157: aload 7
    //   159: astore_1
    //   160: aload 5
    //   162: astore 4
    //   164: aload 7
    //   166: astore 6
    //   168: aload_0
    //   169: aload 5
    //   171: invokespecial 110	com/google/android/gms/internal/measurement/zzcr:zza	(Ljava/net/HttpURLConnection;)V
    //   174: aload 7
    //   176: astore_1
    //   177: aload 5
    //   179: astore 4
    //   181: aload 7
    //   183: astore 6
    //   185: aload 5
    //   187: invokevirtual 114	java/net/HttpURLConnection:getResponseCode	()I
    //   190: istore_3
    //   191: iload_3
    //   192: sipush 200
    //   195: if_icmpne +21 -> 216
    //   198: aload 7
    //   200: astore_1
    //   201: aload 5
    //   203: astore 4
    //   205: aload 7
    //   207: astore 6
    //   209: aload_0
    //   210: invokevirtual 118	com/google/android/gms/internal/measurement/zzat:zzcc	()Lcom/google/android/gms/internal/measurement/zzal;
    //   213: invokevirtual 123	com/google/android/gms/internal/measurement/zzal:zzbv	()V
    //   216: aload 7
    //   218: astore_1
    //   219: aload 5
    //   221: astore 4
    //   223: aload 7
    //   225: astore 6
    //   227: aload_0
    //   228: ldc -71
    //   230: iload_3
    //   231: invokestatic 131	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   234: invokevirtual 99	com/google/android/gms/internal/measurement/zzat:zzb	(Ljava/lang/String;Ljava/lang/Object;)V
    //   237: aload 7
    //   239: ifnull +8 -> 247
    //   242: aload 7
    //   244: invokevirtual 188	java/io/OutputStream:close	()V
    //   247: aload 5
    //   249: ifnull +8 -> 257
    //   252: aload 5
    //   254: invokevirtual 134	java/net/HttpURLConnection:disconnect	()V
    //   257: iload_3
    //   258: ireturn
    //   259: astore_1
    //   260: aload_0
    //   261: ldc -66
    //   263: aload_1
    //   264: invokevirtual 193	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   267: goto -20 -> 247
    //   270: astore_2
    //   271: aconst_null
    //   272: astore 5
    //   274: aload 6
    //   276: astore_1
    //   277: aload 5
    //   279: astore 4
    //   281: aload_0
    //   282: ldc -61
    //   284: aload_2
    //   285: invokevirtual 139	com/google/android/gms/internal/measurement/zzat:zzd	(Ljava/lang/String;Ljava/lang/Object;)V
    //   288: aload 6
    //   290: ifnull +8 -> 298
    //   293: aload 6
    //   295: invokevirtual 188	java/io/OutputStream:close	()V
    //   298: aload 5
    //   300: ifnull +8 -> 308
    //   303: aload 5
    //   305: invokevirtual 134	java/net/HttpURLConnection:disconnect	()V
    //   308: iconst_0
    //   309: ireturn
    //   310: astore_1
    //   311: aload_0
    //   312: ldc -66
    //   314: aload_1
    //   315: invokevirtual 193	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   318: goto -20 -> 298
    //   321: astore_2
    //   322: aconst_null
    //   323: astore 4
    //   325: aload 5
    //   327: astore_1
    //   328: aload_1
    //   329: ifnull +7 -> 336
    //   332: aload_1
    //   333: invokevirtual 188	java/io/OutputStream:close	()V
    //   336: aload 4
    //   338: ifnull +8 -> 346
    //   341: aload 4
    //   343: invokevirtual 134	java/net/HttpURLConnection:disconnect	()V
    //   346: aload_2
    //   347: athrow
    //   348: astore_1
    //   349: aload_0
    //   350: ldc -66
    //   352: aload_1
    //   353: invokevirtual 193	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   356: goto -20 -> 336
    //   359: astore_2
    //   360: goto -32 -> 328
    //   363: astore_2
    //   364: goto -90 -> 274
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	367	0	this	zzcr
    //   0	367	1	paramURL	URL
    //   0	367	2	paramArrayOfByte	byte[]
    //   190	68	3	i	int
    //   76	266	4	localURL1	URL
    //   1	325	5	localURL2	URL
    //   10	284	6	localObject1	Object
    //   4	239	7	localOutputStream	java.io.OutputStream
    //   7	123	8	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   242	247	259	java/io/IOException
    //   54	68	270	java/io/IOException
    //   293	298	310	java/io/IOException
    //   54	68	321	finally
    //   332	336	348	java/io/IOException
    //   82	88	359	finally
    //   99	106	359	finally
    //   117	122	359	finally
    //   133	140	359	finally
    //   151	157	359	finally
    //   168	174	359	finally
    //   185	191	359	finally
    //   209	216	359	finally
    //   227	237	359	finally
    //   281	288	359	finally
    //   82	88	363	java/io/IOException
    //   99	106	363	java/io/IOException
    //   117	122	363	java/io/IOException
    //   133	140	363	java/io/IOException
    //   151	157	363	java/io/IOException
    //   168	174	363	java/io/IOException
    //   185	191	363	java/io/IOException
    //   209	216	363	java/io/IOException
    //   227	237	363	java/io/IOException
  }
  
  private static void zza(StringBuilder paramStringBuilder, String paramString1, String paramString2)
    throws UnsupportedEncodingException
  {
    if (paramStringBuilder.length() != 0) {
      paramStringBuilder.append('&');
    }
    paramStringBuilder.append(URLEncoder.encode(paramString1, "UTF-8"));
    paramStringBuilder.append('=');
    paramStringBuilder.append(URLEncoder.encode(paramString2, "UTF-8"));
  }
  
  private final void zza(HttpURLConnection paramHttpURLConnection)
    throws IOException
  {
    localHttpURLConnection = null;
    try
    {
      paramHttpURLConnection = paramHttpURLConnection.getInputStream();
      localHttpURLConnection = paramHttpURLConnection;
      byte[] arrayOfByte = new byte['Ѐ'];
      int i;
      do
      {
        localHttpURLConnection = paramHttpURLConnection;
        i = paramHttpURLConnection.read(arrayOfByte);
      } while (i > 0);
      if (paramHttpURLConnection != null) {}
      try
      {
        paramHttpURLConnection.close();
        return;
      }
      catch (IOException paramHttpURLConnection)
      {
        zze("Error closing http connection input stream", paramHttpURLConnection);
        return;
      }
      try
      {
        localHttpURLConnection.close();
        throw paramHttpURLConnection;
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          zze("Error closing http connection input stream", localIOException);
        }
      }
    }
    finally
    {
      if (localHttpURLConnection == null) {}
    }
  }
  
  /* Error */
  private final int zzb(URL paramURL, byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aconst_null
    //   4: astore 4
    //   6: aload_1
    //   7: invokestatic 91	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   10: pop
    //   11: aload_2
    //   12: invokestatic 91	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   15: pop
    //   16: aload_0
    //   17: invokevirtual 160	com/google/android/gms/internal/measurement/zzat:getContext	()Landroid/content/Context;
    //   20: invokevirtual 166	android/content/Context:getPackageName	()Ljava/lang/String;
    //   23: pop
    //   24: new 234	java/io/ByteArrayOutputStream
    //   27: dup
    //   28: invokespecial 236	java/io/ByteArrayOutputStream:<init>	()V
    //   31: astore 6
    //   33: new 238	java/util/zip/GZIPOutputStream
    //   36: dup
    //   37: aload 6
    //   39: invokespecial 241	java/util/zip/GZIPOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   42: astore 7
    //   44: aload 7
    //   46: aload_2
    //   47: invokevirtual 242	java/util/zip/GZIPOutputStream:write	([B)V
    //   50: aload 7
    //   52: invokevirtual 243	java/util/zip/GZIPOutputStream:close	()V
    //   55: aload 6
    //   57: invokevirtual 244	java/io/ByteArrayOutputStream:close	()V
    //   60: aload 6
    //   62: invokevirtual 247	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   65: astore 6
    //   67: aload_0
    //   68: ldc -7
    //   70: aload 6
    //   72: arraylength
    //   73: invokestatic 131	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   76: ldc2_w 250
    //   79: aload 6
    //   81: arraylength
    //   82: i2l
    //   83: lmul
    //   84: aload_2
    //   85: arraylength
    //   86: i2l
    //   87: ldiv
    //   88: invokestatic 256	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   91: aload_1
    //   92: invokevirtual 259	com/google/android/gms/internal/measurement/zzat:zza	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   95: aload 6
    //   97: arraylength
    //   98: aload_2
    //   99: arraylength
    //   100: if_icmple +21 -> 121
    //   103: aload_0
    //   104: ldc_w 261
    //   107: aload 6
    //   109: arraylength
    //   110: invokestatic 131	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   113: aload_2
    //   114: arraylength
    //   115: invokestatic 131	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   118: invokevirtual 264	com/google/android/gms/internal/measurement/zzat:zzc	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   121: invokestatic 149	com/google/android/gms/internal/measurement/zzcr:zzck	()Z
    //   124: ifeq +37 -> 161
    //   127: new 16	java/lang/String
    //   130: dup
    //   131: aload_2
    //   132: invokespecial 154	java/lang/String:<init>	([B)V
    //   135: invokestatic 267	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   138: astore_2
    //   139: aload_2
    //   140: invokevirtual 268	java/lang/String:length	()I
    //   143: ifeq +109 -> 252
    //   146: ldc 14
    //   148: aload_2
    //   149: invokevirtual 272	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   152: astore_2
    //   153: aload_0
    //   154: ldc_w 274
    //   157: aload_2
    //   158: invokevirtual 156	com/google/android/gms/internal/measurement/zzat:zza	(Ljava/lang/String;Ljava/lang/Object;)V
    //   161: aload_0
    //   162: aload_1
    //   163: invokespecial 102	com/google/android/gms/internal/measurement/zzcr:zzb	(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    //   166: astore_1
    //   167: aload_1
    //   168: iconst_1
    //   169: invokevirtual 170	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   172: aload_1
    //   173: ldc_w 276
    //   176: ldc_w 278
    //   179: invokevirtual 282	java/net/HttpURLConnection:addRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   182: aload_1
    //   183: aload 6
    //   185: arraylength
    //   186: invokevirtual 174	java/net/HttpURLConnection:setFixedLengthStreamingMode	(I)V
    //   189: aload_1
    //   190: invokevirtual 107	java/net/HttpURLConnection:connect	()V
    //   193: aload_1
    //   194: invokevirtual 178	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   197: astore_2
    //   198: aload_2
    //   199: aload 6
    //   201: invokevirtual 183	java/io/OutputStream:write	([B)V
    //   204: aload_2
    //   205: invokevirtual 188	java/io/OutputStream:close	()V
    //   208: aload_0
    //   209: aload_1
    //   210: invokespecial 110	com/google/android/gms/internal/measurement/zzcr:zza	(Ljava/net/HttpURLConnection;)V
    //   213: aload_1
    //   214: invokevirtual 114	java/net/HttpURLConnection:getResponseCode	()I
    //   217: istore_3
    //   218: iload_3
    //   219: sipush 200
    //   222: if_icmpne +10 -> 232
    //   225: aload_0
    //   226: invokevirtual 118	com/google/android/gms/internal/measurement/zzat:zzcc	()Lcom/google/android/gms/internal/measurement/zzal;
    //   229: invokevirtual 123	com/google/android/gms/internal/measurement/zzal:zzbv	()V
    //   232: aload_0
    //   233: ldc -71
    //   235: iload_3
    //   236: invokestatic 131	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   239: invokevirtual 99	com/google/android/gms/internal/measurement/zzat:zzb	(Ljava/lang/String;Ljava/lang/Object;)V
    //   242: aload_1
    //   243: ifnull +7 -> 250
    //   246: aload_1
    //   247: invokevirtual 134	java/net/HttpURLConnection:disconnect	()V
    //   250: iload_3
    //   251: ireturn
    //   252: new 16	java/lang/String
    //   255: dup
    //   256: ldc 14
    //   258: invokespecial 285	java/lang/String:<init>	(Ljava/lang/String;)V
    //   261: astore_2
    //   262: goto -109 -> 153
    //   265: astore_2
    //   266: aconst_null
    //   267: astore_1
    //   268: aload_0
    //   269: ldc_w 287
    //   272: aload_2
    //   273: invokevirtual 139	com/google/android/gms/internal/measurement/zzat:zzd	(Ljava/lang/String;Ljava/lang/Object;)V
    //   276: aload 4
    //   278: ifnull +8 -> 286
    //   281: aload 4
    //   283: invokevirtual 188	java/io/OutputStream:close	()V
    //   286: aload_1
    //   287: ifnull +7 -> 294
    //   290: aload_1
    //   291: invokevirtual 134	java/net/HttpURLConnection:disconnect	()V
    //   294: iconst_0
    //   295: ireturn
    //   296: astore_2
    //   297: aload_0
    //   298: ldc_w 289
    //   301: aload_2
    //   302: invokevirtual 193	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   305: goto -19 -> 286
    //   308: astore_2
    //   309: aconst_null
    //   310: astore_1
    //   311: aload 5
    //   313: astore 4
    //   315: aload 4
    //   317: ifnull +8 -> 325
    //   320: aload 4
    //   322: invokevirtual 188	java/io/OutputStream:close	()V
    //   325: aload_1
    //   326: ifnull +7 -> 333
    //   329: aload_1
    //   330: invokevirtual 134	java/net/HttpURLConnection:disconnect	()V
    //   333: aload_2
    //   334: athrow
    //   335: astore 4
    //   337: aload_0
    //   338: ldc_w 289
    //   341: aload 4
    //   343: invokevirtual 193	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   346: goto -21 -> 325
    //   349: astore_2
    //   350: aload 5
    //   352: astore 4
    //   354: goto -39 -> 315
    //   357: astore 5
    //   359: aload_2
    //   360: astore 4
    //   362: aload 5
    //   364: astore_2
    //   365: goto -50 -> 315
    //   368: astore_2
    //   369: goto -54 -> 315
    //   372: astore_2
    //   373: goto -105 -> 268
    //   376: astore 5
    //   378: aload_2
    //   379: astore 4
    //   381: aload 5
    //   383: astore_2
    //   384: goto -116 -> 268
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	387	0	this	zzcr
    //   0	387	1	paramURL	URL
    //   0	387	2	paramArrayOfByte	byte[]
    //   217	34	3	i	int
    //   4	317	4	localObject1	Object
    //   335	7	4	localIOException1	IOException
    //   352	28	4	localObject2	Object
    //   1	350	5	localObject3	Object
    //   357	6	5	localObject4	Object
    //   376	6	5	localIOException2	IOException
    //   31	169	6	localObject5	Object
    //   42	9	7	localGZIPOutputStream	java.util.zip.GZIPOutputStream
    // Exception table:
    //   from	to	target	type
    //   16	121	265	java/io/IOException
    //   121	153	265	java/io/IOException
    //   153	161	265	java/io/IOException
    //   161	167	265	java/io/IOException
    //   252	262	265	java/io/IOException
    //   281	286	296	java/io/IOException
    //   16	121	308	finally
    //   121	153	308	finally
    //   153	161	308	finally
    //   161	167	308	finally
    //   252	262	308	finally
    //   320	325	335	java/io/IOException
    //   167	198	349	finally
    //   208	218	349	finally
    //   225	232	349	finally
    //   232	242	349	finally
    //   198	208	357	finally
    //   268	276	368	finally
    //   167	198	372	java/io/IOException
    //   208	218	372	java/io/IOException
    //   225	232	372	java/io/IOException
    //   232	242	372	java/io/IOException
    //   198	208	376	java/io/IOException
  }
  
  @VisibleForTesting
  private final HttpURLConnection zzb(URL paramURL)
    throws IOException
  {
    paramURL = paramURL.openConnection();
    if (!(paramURL instanceof HttpURLConnection)) {
      throw new IOException("Failed to obtain http connection");
    }
    paramURL = (HttpURLConnection)paramURL;
    paramURL.setDefaultUseCaches(false);
    paramURL.setConnectTimeout(((Integer)zzcf.zzzz.get()).intValue());
    paramURL.setReadTimeout(((Integer)zzcf.zzaaa.get()).intValue());
    paramURL.setInstanceFollowRedirects(false);
    paramURL.setRequestProperty("User-Agent", this.zzabl);
    paramURL.setDoInput(true);
    return paramURL;
  }
  
  private final URL zzb(zzck paramzzck, String paramString)
  {
    String str;
    if (paramzzck.zzet())
    {
      paramzzck = zzbx.zzed();
      str = zzbx.zzef();
    }
    for (paramzzck = String.valueOf(paramzzck).length() + 1 + String.valueOf(str).length() + String.valueOf(paramString).length() + paramzzck + str + "?" + paramString;; paramzzck = String.valueOf(paramzzck).length() + 1 + String.valueOf(str).length() + String.valueOf(paramString).length() + paramzzck + str + "?" + paramString)
    {
      try
      {
        paramzzck = new URL(paramzzck);
        return paramzzck;
      }
      catch (MalformedURLException paramzzck)
      {
        zze("Error trying to parse the hardcoded host url", paramzzck);
      }
      paramzzck = zzbx.zzee();
      str = zzbx.zzef();
    }
    return null;
  }
  
  private final URL zzd(zzck paramzzck)
  {
    String str;
    if (paramzzck.zzet())
    {
      paramzzck = String.valueOf(zzbx.zzed());
      str = String.valueOf(zzbx.zzef());
      if (str.length() != 0) {}
      for (paramzzck = paramzzck.concat(str);; paramzzck = new String(paramzzck)) {
        try
        {
          paramzzck = new URL(paramzzck);
          return paramzzck;
        }
        catch (MalformedURLException paramzzck)
        {
          zze("Error trying to parse the hardcoded host url", paramzzck);
        }
      }
    }
    else
    {
      paramzzck = String.valueOf(zzbx.zzee());
      str = String.valueOf(zzbx.zzef());
      if (str.length() != 0) {}
      for (paramzzck = paramzzck.concat(str);; paramzzck = new String(paramzzck)) {
        break;
      }
    }
    return null;
  }
  
  private final URL zzfc()
  {
    Object localObject = String.valueOf(zzbx.zzed());
    String str = String.valueOf((String)zzcf.zzzo.get());
    if (str.length() != 0) {}
    for (localObject = ((String)localObject).concat(str);; localObject = new String((String)localObject)) {
      try
      {
        localObject = new URL((String)localObject);
        return (URL)localObject;
      }
      catch (MalformedURLException localMalformedURLException)
      {
        zze("Error trying to parse the hardcoded host url", localMalformedURLException);
      }
    }
    return null;
  }
  
  @VisibleForTesting
  final String zza(zzck paramzzck, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramzzck);
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      Iterator localIterator = paramzzck.zzcw().entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        String str = (String)localEntry.getKey();
        if ((!"ht".equals(str)) && (!"qt".equals(str)) && (!"AppUID".equals(str)) && (!"z".equals(str)) && (!"_gmsv".equals(str))) {
          zza(localStringBuilder, str, (String)localEntry.getValue());
        }
      }
      zza(localStringBuilder, "ht", String.valueOf(paramzzck.zzer()));
    }
    catch (UnsupportedEncodingException paramzzck)
    {
      zze("Failed to encode name or value", paramzzck);
      return null;
    }
    zza(localStringBuilder, "qt", String.valueOf(zzbx().currentTimeMillis() - paramzzck.zzer()));
    long l;
    if (paramBoolean)
    {
      l = paramzzck.zzeu();
      if (l == 0L) {
        break label225;
      }
    }
    for (paramzzck = String.valueOf(l);; paramzzck = String.valueOf(l))
    {
      zza(localStringBuilder, "z", paramzzck);
      return localStringBuilder.toString();
      label225:
      l = paramzzck.zzeq();
    }
  }
  
  protected final void zzag()
  {
    zza("Network initialized. User agent", this.zzabl);
  }
  
  public final List<Long> zzb(List<zzck> paramList)
  {
    boolean bool2 = true;
    zzk.zzaf();
    zzcl();
    Preconditions.checkNotNull(paramList);
    int j;
    boolean bool1;
    int i;
    if ((zzbz().zzeg().isEmpty()) || (!this.zzabm.zzj(((Integer)zzcf.zzzx.get()).intValue() * 1000L)))
    {
      j = 0;
      bool1 = false;
      i = j;
      label64:
      if (i == 0) {
        break label368;
      }
      if (paramList.isEmpty()) {
        break label229;
      }
    }
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      Preconditions.checkArgument(bool2);
      zza("Uploading batched hits. compression, count", Boolean.valueOf(bool1), Integer.valueOf(paramList.size()));
      localObject1 = new zzcs(this);
      localObject2 = new ArrayList();
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        localObject3 = (zzck)paramList.next();
        if (!((zzcs)localObject1).zze((zzck)localObject3)) {
          break;
        }
        ((List)localObject2).add(Long.valueOf(((zzck)localObject3).zzeq()));
      }
      if (zzbn.zzz((String)zzcf.zzzq.get()) != zzbn.zzxw) {}
      for (i = 1;; i = 0)
      {
        j = i;
        if (zzbt.zzaa((String)zzcf.zzzr.get()) != zzbt.zzyh) {
          break;
        }
        bool1 = true;
        break label64;
      }
      label229:
      bool2 = false;
    }
    if (((zzcs)localObject1).zzfe() == 0) {
      return (List<Long>)localObject2;
    }
    paramList = zzfc();
    if (paramList == null) {
      zzu("Failed to build batching endpoint url");
    }
    for (;;)
    {
      return Collections.emptyList();
      if (bool1) {}
      for (i = zzb(paramList, ((zzcs)localObject1).getPayload()); 200 == i; i = zza(paramList, ((zzcs)localObject1).getPayload()))
      {
        zza("Batched upload completed. Hits batched", Integer.valueOf(((zzcs)localObject1).zzfe()));
        return (List<Long>)localObject2;
      }
      zza("Network error uploading hits. status code", Integer.valueOf(i));
      if (zzbz().zzeg().contains(Integer.valueOf(i)))
      {
        zzt("Server instructed the client to stop batching");
        this.zzabm.start();
      }
    }
    label368:
    Object localObject1 = new ArrayList(paramList.size());
    paramList = paramList.iterator();
    if (paramList.hasNext())
    {
      localObject2 = (zzck)paramList.next();
      Preconditions.checkNotNull(localObject2);
      if (((zzck)localObject2).zzet()) {
        break label492;
      }
      bool1 = true;
      label427:
      localObject3 = zza((zzck)localObject2, bool1);
      if (localObject3 != null) {
        break label498;
      }
      zzby().zza((zzck)localObject2, "Error formatting hit for upload");
      i = 1;
    }
    for (;;)
    {
      if (i != 0)
      {
        ((List)localObject1).add(Long.valueOf(((zzck)localObject2).zzeq()));
        if (((List)localObject1).size() < zzbx.zzeb()) {
          break;
        }
      }
      return (List<Long>)localObject1;
      label492:
      bool1 = false;
      break label427;
      label498:
      if (((String)localObject3).length() <= ((Integer)zzcf.zzzp.get()).intValue())
      {
        localObject3 = zzb((zzck)localObject2, (String)localObject3);
        if (localObject3 == null) {
          zzu("Failed to build collect GET endpoint url");
        }
      }
      URL localURL;
      label663:
      do
      {
        for (;;)
        {
          i = 0;
          break;
          if (zza((URL)localObject3) == 200)
          {
            i = 1;
            break;
          }
          i = 0;
          break;
          localObject3 = zza((zzck)localObject2, false);
          if (localObject3 == null)
          {
            zzby().zza((zzck)localObject2, "Error formatting hit for POST upload");
            i = 1;
            break;
          }
          localObject3 = ((String)localObject3).getBytes();
          if (localObject3.length > ((Integer)zzcf.zzzu.get()).intValue())
          {
            zzby().zza((zzck)localObject2, "Hit payload exceeds size limit");
            i = 1;
            break;
          }
          localURL = zzd((zzck)localObject2);
          if (localURL != null) {
            break label663;
          }
          zzu("Failed to build collect POST endpoint url");
        }
      } while (zza(localURL, (byte[])localObject3) != 200);
      i = 1;
    }
  }
  
  public final boolean zzfb()
  {
    zzk.zzaf();
    zzcl();
    Object localObject1 = (ConnectivityManager)getContext().getSystemService("connectivity");
    try
    {
      localObject1 = ((ConnectivityManager)localObject1).getActiveNetworkInfo();
      if ((localObject1 == null) || (!((NetworkInfo)localObject1).isConnected()))
      {
        zzq("No network connectivity");
        return false;
      }
    }
    catch (SecurityException localSecurityException)
    {
      for (;;)
      {
        Object localObject2 = null;
      }
    }
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */