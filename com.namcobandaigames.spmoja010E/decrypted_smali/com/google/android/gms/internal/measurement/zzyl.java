package com.google.android.gms.internal.measurement;

import java.nio.ByteBuffer;

abstract class zzyl
{
  static void zzc(CharSequence paramCharSequence, ByteBuffer paramByteBuffer)
  {
    int i3 = paramCharSequence.length();
    int n = paramByteBuffer.position();
    int j = 0;
    int k;
    int m;
    if (j < i3)
    {
      k = j;
      m = n;
    }
    int i1;
    int i;
    for (;;)
    {
      try
      {
        i1 = paramCharSequence.charAt(j);
        if (i1 < 128)
        {
          k = j;
          m = n;
          paramByteBuffer.put(n + j, (byte)i1);
          j += 1;
          break;
        }
        if (j != i3) {
          break label667;
        }
        k = j;
        m = n;
        paramByteBuffer.position(n + j);
        return;
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException1)
      {
        label154:
        j = k;
        k = paramByteBuffer.position();
        m = Math.max(j, m - paramByteBuffer.position() + 1);
        i = paramCharSequence.charAt(j);
        throw new ArrayIndexOutOfBoundsException(37 + "Failed writing " + i + " at index " + (m + k));
      }
      if (j >= i3) {
        break label629;
      }
      k = j;
      m = n;
      i = paramCharSequence.charAt(j);
      if (i >= 128) {
        continue;
      }
      k = j;
      m = n;
      paramByteBuffer.put(n, (byte)i);
      j += 1;
      n += 1;
    }
    byte b;
    if (i < 2048)
    {
      i1 = n + 1;
      b = (byte)(i >>> 6 | 0xC0);
      m = j;
      k = i1;
    }
    for (;;)
    {
      try
      {
        paramByteBuffer.put(n, b);
        m = j;
        k = i1;
        paramByteBuffer.put(i1, (byte)(i & 0x3F | 0x80));
        n = i1;
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException2)
      {
        char c;
        int i4;
        int i2;
        label629:
        j = m;
        m = k;
        continue;
        continue;
      }
      i1 = n + 1;
      m = j;
      k = i1;
      paramByteBuffer.put(n, (byte)(i >>> 12 | 0xE0));
      n = i1 + 1;
      b = (byte)(i >>> 6 & 0x3F | 0x80);
      k = j;
      m = n;
      paramByteBuffer.put(i1, b);
      k = j;
      m = n;
      paramByteBuffer.put(n, (byte)(i & 0x3F | 0x80));
      break label154;
      i1 = j;
      if (j + 1 != i3)
      {
        j += 1;
        k = j;
        m = n;
        c = paramCharSequence.charAt(j);
        k = j;
        m = n;
        if (!Character.isSurrogatePair(i, c)) {
          i1 = j;
        }
      }
      else
      {
        k = i1;
        m = n;
        throw new zzyn(i1, i3);
      }
      k = j;
      m = n;
      i4 = Character.toCodePoint(i, c);
      i2 = n + 1;
      b = (byte)(i4 >>> 18 | 0xF0);
      try
      {
        paramByteBuffer.put(n, b);
        i1 = i2 + 1;
        b = (byte)(i4 >>> 12 & 0x3F | 0x80);
        m = j;
        k = i1;
        paramByteBuffer.put(i2, b);
        n = i1 + 1;
        b = (byte)(i4 >>> 6 & 0x3F | 0x80);
        k = j;
        m = n;
        paramByteBuffer.put(i1, b);
        k = j;
        m = n;
        paramByteBuffer.put(n, (byte)(i4 & 0x3F | 0x80));
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException3)
      {
        m = i2;
      }
      k = j;
      m = n;
      paramByteBuffer.position(n);
      return;
      label667:
      n += j;
      break;
      if (i >= 55296) {
        if (57343 >= i) {}
      }
    }
  }
  
  abstract int zzb(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3);
  
  abstract int zzb(CharSequence paramCharSequence, byte[] paramArrayOfByte, int paramInt1, int paramInt2);
  
  abstract void zzb(CharSequence paramCharSequence, ByteBuffer paramByteBuffer);
  
  final boolean zzf(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    boolean bool = false;
    if (zzb(0, paramArrayOfByte, paramInt1, paramInt2) == 0) {
      bool = true;
    }
    return bool;
  }
  
  abstract String zzh(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws zzvt;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */