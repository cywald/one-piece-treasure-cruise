package com.google.android.gms.internal.measurement;

final class zzyn
  extends IllegalArgumentException
{
  zzyn(int paramInt1, int paramInt2)
  {
    super(54 + "Unpaired surrogate at index " + paramInt1 + " of " + paramInt2);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */