package com.google.android.gms.internal.measurement;

public abstract interface zzbw<U extends zzbu>
{
  public abstract void zza(String paramString, boolean paramBoolean);
  
  public abstract void zzb(String paramString, int paramInt);
  
  public abstract void zzb(String paramString1, String paramString2);
  
  public abstract void zzc(String paramString1, String paramString2);
  
  public abstract U zzdv();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */