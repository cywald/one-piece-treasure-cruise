package com.google.android.gms.internal.measurement;

final class zzvl
  implements zzws
{
  private static final zzvl zzbyl = new zzvl();
  
  public static zzvl zzwb()
  {
    return zzbyl;
  }
  
  public final boolean zze(Class<?> paramClass)
  {
    return zzvm.class.isAssignableFrom(paramClass);
  }
  
  public final zzwr zzf(Class<?> paramClass)
  {
    if (!zzvm.class.isAssignableFrom(paramClass))
    {
      paramClass = String.valueOf(paramClass.getName());
      if (paramClass.length() != 0) {}
      for (paramClass = "Unsupported message type: ".concat(paramClass);; paramClass = new String("Unsupported message type: ")) {
        throw new IllegalArgumentException(paramClass);
      }
    }
    try
    {
      zzwr localzzwr = (zzwr)zzvm.zzg(paramClass.asSubclass(zzvm.class)).zza(zzvm.zze.zzbyv, null, null);
      return localzzwr;
    }
    catch (Exception localException)
    {
      paramClass = String.valueOf(paramClass.getName());
      if (paramClass.length() == 0) {}
    }
    for (paramClass = "Unable to get message info for ".concat(paramClass);; paramClass = new String("Unable to get message info for ")) {
      throw new RuntimeException(paramClass, localException);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */