package com.google.android.gms.internal.measurement;

import java.nio.ByteBuffer;

final class zzym
  extends zzyl
{
  final int zzb(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    while ((paramInt2 < paramInt3) && (paramArrayOfByte[paramInt2] >= 0)) {
      paramInt2 += 1;
    }
    paramInt1 = paramInt2;
    if (paramInt2 >= paramInt3)
    {
      paramInt1 = 0;
      return paramInt1;
      label31:
      paramInt1 = paramInt2;
    }
    label90:
    label92:
    label170:
    label172:
    do
    {
      int i;
      int j;
      do
      {
        do
        {
          if (paramInt1 >= paramInt3) {
            return 0;
          }
          paramInt2 = paramInt1 + 1;
          i = paramArrayOfByte[paramInt1];
          if (i >= 0) {
            break label31;
          }
          if (i >= -32) {
            break label92;
          }
          paramInt1 = i;
          if (paramInt2 >= paramInt3) {
            break;
          }
          if (i < -62) {
            break label90;
          }
          paramInt1 = paramInt2 + 1;
        } while (paramArrayOfByte[paramInt2] <= -65);
        return -1;
        if (i >= -16) {
          break label172;
        }
        if (paramInt2 >= paramInt3 - 1) {
          return zzyj.zzi(paramArrayOfByte, paramInt2, paramInt3);
        }
        j = paramInt2 + 1;
        paramInt1 = paramArrayOfByte[paramInt2];
        if ((paramInt1 > -65) || ((i == -32) && (paramInt1 < -96)) || ((i == -19) && (paramInt1 >= -96))) {
          break label170;
        }
        paramInt1 = j + 1;
      } while (paramArrayOfByte[j] <= -65);
      return -1;
      if (paramInt2 >= paramInt3 - 2) {
        return zzyj.zzi(paramArrayOfByte, paramInt2, paramInt3);
      }
      paramInt1 = paramInt2 + 1;
      paramInt2 = paramArrayOfByte[paramInt2];
      if ((paramInt2 > -65) || ((i << 28) + (paramInt2 + 112) >> 30 != 0)) {
        break label242;
      }
      paramInt2 = paramInt1 + 1;
      if (paramArrayOfByte[paramInt1] > -65) {
        break label242;
      }
      paramInt1 = paramInt2 + 1;
    } while (paramArrayOfByte[paramInt2] <= -65);
    label242:
    return -1;
  }
  
  final int zzb(CharSequence paramCharSequence, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int k = paramCharSequence.length();
    int j = 0;
    int m = paramInt1 + paramInt2;
    paramInt2 = j;
    while ((paramInt2 < k) && (paramInt2 + paramInt1 < m))
    {
      j = paramCharSequence.charAt(paramInt2);
      if (j >= 128) {
        break;
      }
      paramArrayOfByte[(paramInt1 + paramInt2)] = ((byte)j);
      paramInt2 += 1;
    }
    if (paramInt2 == k) {
      return paramInt1 + k;
    }
    paramInt1 += paramInt2;
    if (paramInt2 < k)
    {
      int i = paramCharSequence.charAt(paramInt2);
      if ((i < 128) && (paramInt1 < m))
      {
        j = paramInt1 + 1;
        paramArrayOfByte[paramInt1] = ((byte)i);
        paramInt1 = j;
      }
      for (;;)
      {
        paramInt2 += 1;
        break;
        if ((i < 2048) && (paramInt1 <= m - 2))
        {
          j = paramInt1 + 1;
          paramArrayOfByte[paramInt1] = ((byte)(i >>> 6 | 0x3C0));
          paramInt1 = j + 1;
          paramArrayOfByte[j] = ((byte)(i & 0x3F | 0x80));
        }
        else
        {
          int n;
          if (((i < 55296) || (57343 < i)) && (paramInt1 <= m - 3))
          {
            j = paramInt1 + 1;
            paramArrayOfByte[paramInt1] = ((byte)(i >>> 12 | 0x1E0));
            n = j + 1;
            paramArrayOfByte[j] = ((byte)(i >>> 6 & 0x3F | 0x80));
            paramInt1 = n + 1;
            paramArrayOfByte[n] = ((byte)(i & 0x3F | 0x80));
          }
          else
          {
            if (paramInt1 > m - 4) {
              break label446;
            }
            j = paramInt2;
            char c;
            if (paramInt2 + 1 != paramCharSequence.length())
            {
              paramInt2 += 1;
              c = paramCharSequence.charAt(paramInt2);
              if (!Character.isSurrogatePair(i, c)) {
                j = paramInt2;
              }
            }
            else
            {
              throw new zzyn(j - 1, k);
            }
            j = Character.toCodePoint(i, c);
            n = paramInt1 + 1;
            paramArrayOfByte[paramInt1] = ((byte)(j >>> 18 | 0xF0));
            paramInt1 = n + 1;
            paramArrayOfByte[n] = ((byte)(j >>> 12 & 0x3F | 0x80));
            n = paramInt1 + 1;
            paramArrayOfByte[paramInt1] = ((byte)(j >>> 6 & 0x3F | 0x80));
            paramInt1 = n + 1;
            paramArrayOfByte[n] = ((byte)(j & 0x3F | 0x80));
          }
        }
      }
      label446:
      if ((55296 <= i) && (i <= 57343) && ((paramInt2 + 1 == paramCharSequence.length()) || (!Character.isSurrogatePair(i, paramCharSequence.charAt(paramInt2 + 1))))) {
        throw new zzyn(paramInt2, k);
      }
      throw new ArrayIndexOutOfBoundsException(37 + "Failed writing " + i + " at index " + paramInt1);
    }
    return paramInt1;
  }
  
  final void zzb(CharSequence paramCharSequence, ByteBuffer paramByteBuffer)
  {
    zzc(paramCharSequence, paramByteBuffer);
  }
  
  final String zzh(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws zzvt
  {
    if ((paramInt1 | paramInt2 | paramArrayOfByte.length - paramInt1 - paramInt2) < 0) {
      throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", new Object[] { Integer.valueOf(paramArrayOfByte.length), Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }));
    }
    int k = paramInt1 + paramInt2;
    char[] arrayOfChar = new char[paramInt2];
    int i = 0;
    int j = paramInt1;
    byte b1;
    for (;;)
    {
      paramInt2 = j;
      paramInt1 = i;
      if (j >= k) {
        break;
      }
      b1 = paramArrayOfByte[j];
      paramInt2 = j;
      paramInt1 = i;
      if (!zzyk.zzh(b1)) {
        break;
      }
      j += 1;
      zzyk.zzb(b1, arrayOfChar, i);
      i += 1;
    }
    zzyk.zzb(b1, paramArrayOfByte[i], arrayOfChar, paramInt1);
    paramInt1 += 1;
    paramInt2 = i + 1;
    for (;;)
    {
      if (paramInt2 >= k) {
        break label376;
      }
      i = paramInt2 + 1;
      b1 = paramArrayOfByte[paramInt2];
      if (zzyk.zzh(b1))
      {
        zzyk.zzb(b1, arrayOfChar, paramInt1);
        paramInt2 = paramInt1 + 1;
        paramInt1 = i;
        i = paramInt2;
        for (;;)
        {
          j = i;
          paramInt2 = paramInt1;
          if (paramInt1 >= k) {
            break;
          }
          b1 = paramArrayOfByte[paramInt1];
          j = i;
          paramInt2 = paramInt1;
          if (!zzyk.zzh(b1)) {
            break;
          }
          zzyk.zzb(b1, arrayOfChar, i);
          i += 1;
          paramInt1 += 1;
        }
      }
      if (zzyk.zzi(b1))
      {
        if (i < k) {
          break;
        }
        throw zzvt.zzwr();
      }
      if (zzyk.zzj(b1))
      {
        if (i >= k - 1) {
          throw zzvt.zzwr();
        }
        paramInt2 = i + 1;
        zzyk.zzb(b1, paramArrayOfByte[i], paramArrayOfByte[paramInt2], arrayOfChar, paramInt1);
        paramInt1 += 1;
        paramInt2 += 1;
      }
      else
      {
        if (i >= k - 2) {
          throw zzvt.zzwr();
        }
        paramInt2 = i + 1;
        byte b2 = paramArrayOfByte[i];
        i = paramInt2 + 1;
        zzyk.zzb(b1, b2, paramArrayOfByte[paramInt2], paramArrayOfByte[i], arrayOfChar, paramInt1);
        j = paramInt1 + 1 + 1;
        paramInt2 = i + 1;
        paramInt1 = j;
      }
    }
    label376:
    return new String(arrayOfChar, 0, paramInt1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzym.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */