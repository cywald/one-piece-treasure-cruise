package com.google.android.gms.internal.measurement;

final class zzun
  implements zzui
{
  public final byte[] zzc(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    byte[] arrayOfByte = new byte[paramInt2];
    System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, 0, paramInt2);
    return arrayOfByte;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzun.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */