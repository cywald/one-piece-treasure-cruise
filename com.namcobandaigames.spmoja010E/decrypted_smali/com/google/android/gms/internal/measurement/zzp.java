package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzp
  extends zzza<zzp>
{
  private static volatile zzp[] zzqi;
  public String string = "";
  public int type = 1;
  public zzp[] zzqj = zzk();
  public zzp[] zzqk = zzk();
  public zzp[] zzql = zzk();
  public String zzqm = "";
  public String zzqn = "";
  public long zzqo = 0L;
  public boolean zzqp = false;
  public zzp[] zzqq = zzk();
  public int[] zzqr = zzzj.zzcax;
  public boolean zzqs = false;
  
  public zzp()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  private final zzp zzb(zzyx paramzzyx)
    throws IOException
  {
    label190:
    Object localObject;
    for (;;)
    {
      k = paramzzyx.zzug();
      switch (k)
      {
      default: 
        if (super.zza(paramzzyx, k)) {}
        break;
      case 0: 
        return this;
      case 8: 
        i = paramzzyx.getPosition();
        try
        {
          j = paramzzyx.zzuy();
          if ((j <= 0) || (j > 8)) {
            break label190;
          }
          this.type = j;
        }
        catch (IllegalArgumentException localIllegalArgumentException1)
        {
          paramzzyx.zzby(i);
          zza(paramzzyx, k);
        }
        continue;
        throw new IllegalArgumentException(36 + j + " is not a valid enum Type");
      case 18: 
        this.string = paramzzyx.readString();
        break;
      case 26: 
        j = zzzj.zzb(paramzzyx, 26);
        if (this.zzqj == null) {}
        for (i = 0;; i = this.zzqj.length)
        {
          localObject = new zzp[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.zzqj, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = new zzp();
            paramzzyx.zza(localObject[j]);
            paramzzyx.zzug();
            j += 1;
          }
        }
        localObject[j] = new zzp();
        paramzzyx.zza(localObject[j]);
        this.zzqj = ((zzp[])localObject);
        break;
      case 34: 
        j = zzzj.zzb(paramzzyx, 34);
        if (this.zzqk == null) {}
        for (i = 0;; i = this.zzqk.length)
        {
          localObject = new zzp[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.zzqk, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = new zzp();
            paramzzyx.zza(localObject[j]);
            paramzzyx.zzug();
            j += 1;
          }
        }
        localObject[j] = new zzp();
        paramzzyx.zza(localObject[j]);
        this.zzqk = ((zzp[])localObject);
        break;
      case 42: 
        j = zzzj.zzb(paramzzyx, 42);
        if (this.zzql == null) {}
        for (i = 0;; i = this.zzql.length)
        {
          localObject = new zzp[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.zzql, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = new zzp();
            paramzzyx.zza(localObject[j]);
            paramzzyx.zzug();
            j += 1;
          }
        }
        localObject[j] = new zzp();
        paramzzyx.zza(localObject[j]);
        this.zzql = ((zzp[])localObject);
        break;
      case 50: 
        this.zzqm = paramzzyx.readString();
        break;
      case 58: 
        this.zzqn = paramzzyx.readString();
        break;
      case 64: 
        this.zzqo = paramzzyx.zzuz();
        break;
      case 72: 
        this.zzqs = paramzzyx.zzum();
        break;
      case 80: 
        int m = zzzj.zzb(paramzzyx, 80);
        localObject = new int[m];
        j = 0;
        i = 0;
        int n;
        for (;;)
        {
          if (j >= m) {
            break label717;
          }
          if (j != 0) {
            paramzzyx.zzug();
          }
          n = paramzzyx.getPosition();
          try
          {
            localObject[i] = zzc(paramzzyx.zzuy());
            i += 1;
          }
          catch (IllegalArgumentException localIllegalArgumentException3)
          {
            for (;;)
            {
              paramzzyx.zzby(n);
              zza(paramzzyx, k);
            }
          }
          j += 1;
        }
        label717:
        if (i != 0)
        {
          if (this.zzqr == null) {}
          for (j = 0;; j = this.zzqr.length)
          {
            if ((j != 0) || (i != localObject.length)) {
              break label759;
            }
            this.zzqr = ((int[])localObject);
            break;
          }
          label759:
          int[] arrayOfInt = new int[j + i];
          if (j != 0) {
            System.arraycopy(this.zzqr, 0, arrayOfInt, 0, j);
          }
          System.arraycopy(localObject, 0, arrayOfInt, j, i);
          this.zzqr = arrayOfInt;
        }
        break;
      }
    }
    int k = paramzzyx.zzaq(paramzzyx.zzuy());
    int i = paramzzyx.getPosition();
    int j = 0;
    for (;;)
    {
      if (paramzzyx.zzyr() > 0) {}
      label942:
      try
      {
        zzc(paramzzyx.zzuy());
        j += 1;
      }
      catch (IllegalArgumentException localIllegalArgumentException2) {}
      if (j != 0)
      {
        paramzzyx.zzby(i);
        if (this.zzqr == null)
        {
          i = 0;
          localObject = new int[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.zzqr, 0, localObject, 0, i);
            j = i;
          }
        }
        for (;;)
        {
          if (paramzzyx.zzyr() <= 0) {
            break label942;
          }
          i = paramzzyx.getPosition();
          try
          {
            localObject[j] = zzc(paramzzyx.zzuy());
            j += 1;
          }
          catch (IllegalArgumentException localIllegalArgumentException4)
          {
            paramzzyx.zzby(i);
            zza(paramzzyx, 80);
          }
          i = this.zzqr.length;
          break;
        }
        this.zzqr = ((int[])localObject);
      }
      paramzzyx.zzar(k);
      break;
      j = zzzj.zzb(paramzzyx, 90);
      if (this.zzqq == null) {}
      for (i = 0;; i = this.zzqq.length)
      {
        localObject = new zzp[j + i];
        j = i;
        if (i != 0)
        {
          System.arraycopy(this.zzqq, 0, localObject, 0, i);
          j = i;
        }
        while (j < localObject.length - 1)
        {
          localObject[j] = new zzp();
          paramzzyx.zza(localObject[j]);
          paramzzyx.zzug();
          j += 1;
        }
      }
      localObject[j] = new zzp();
      paramzzyx.zza(localObject[j]);
      this.zzqq = ((zzp[])localObject);
      break;
      this.zzqp = paramzzyx.zzum();
      break;
    }
  }
  
  private static int zzc(int paramInt)
  {
    if ((paramInt > 0) && (paramInt <= 17)) {
      return paramInt;
    }
    throw new IllegalArgumentException(40 + paramInt + " is not a valid enum Escaping");
  }
  
  public static zzp[] zzk()
  {
    if (zzqi == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzqi == null) {
        zzqi = new zzp[0];
      }
      return zzqi;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzp)) {
        return false;
      }
      paramObject = (zzp)paramObject;
      if (this.type != ((zzp)paramObject).type) {
        return false;
      }
      if (this.string == null)
      {
        if (((zzp)paramObject).string != null) {
          return false;
        }
      }
      else if (!this.string.equals(((zzp)paramObject).string)) {
        return false;
      }
      if (!zzze.equals(this.zzqj, ((zzp)paramObject).zzqj)) {
        return false;
      }
      if (!zzze.equals(this.zzqk, ((zzp)paramObject).zzqk)) {
        return false;
      }
      if (!zzze.equals(this.zzql, ((zzp)paramObject).zzql)) {
        return false;
      }
      if (this.zzqm == null)
      {
        if (((zzp)paramObject).zzqm != null) {
          return false;
        }
      }
      else if (!this.zzqm.equals(((zzp)paramObject).zzqm)) {
        return false;
      }
      if (this.zzqn == null)
      {
        if (((zzp)paramObject).zzqn != null) {
          return false;
        }
      }
      else if (!this.zzqn.equals(((zzp)paramObject).zzqn)) {
        return false;
      }
      if (this.zzqo != ((zzp)paramObject).zzqo) {
        return false;
      }
      if (this.zzqp != ((zzp)paramObject).zzqp) {
        return false;
      }
      if (!zzze.equals(this.zzqq, ((zzp)paramObject).zzqq)) {
        return false;
      }
      if (!zzze.equals(this.zzqr, ((zzp)paramObject).zzqr)) {
        return false;
      }
      if (this.zzqs != ((zzp)paramObject).zzqs) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzp)paramObject).zzcfc == null) || (((zzp)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzp)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int n = 1231;
    int i2 = 0;
    int i3 = getClass().getName().hashCode();
    int i4 = this.type;
    int i;
    int i5;
    int i6;
    int i7;
    int j;
    label71:
    int k;
    label80:
    int i8;
    int m;
    label107:
    int i9;
    int i10;
    if (this.string == null)
    {
      i = 0;
      i5 = zzze.hashCode(this.zzqj);
      i6 = zzze.hashCode(this.zzqk);
      i7 = zzze.hashCode(this.zzql);
      if (this.zzqm != null) {
        break label250;
      }
      j = 0;
      if (this.zzqn != null) {
        break label261;
      }
      k = 0;
      i8 = (int)(this.zzqo ^ this.zzqo >>> 32);
      if (!this.zzqp) {
        break label272;
      }
      m = 1231;
      i9 = zzze.hashCode(this.zzqq);
      i10 = zzze.hashCode(this.zzqr);
      if (!this.zzqs) {
        break label280;
      }
      label132:
      i1 = i2;
      if (this.zzcfc != null) {
        if (!this.zzcfc.isEmpty()) {
          break label288;
        }
      }
    }
    label250:
    label261:
    label272:
    label280:
    label288:
    for (int i1 = i2;; i1 = this.zzcfc.hashCode())
    {
      return ((((m + ((k + (j + ((((i + ((i3 + 527) * 31 + i4) * 31) * 31 + i5) * 31 + i6) * 31 + i7) * 31) * 31) * 31 + i8) * 31) * 31 + i9) * 31 + i10) * 31 + n) * 31 + i1;
      i = this.string.hashCode();
      break;
      j = this.zzqm.hashCode();
      break label71;
      k = this.zzqn.hashCode();
      break label80;
      m = 1237;
      break label107;
      n = 1237;
      break label132;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    int j = 0;
    paramzzyy.zzd(1, this.type);
    if ((this.string != null) && (!this.string.equals(""))) {
      paramzzyy.zzb(2, this.string);
    }
    int i;
    zzp localzzp;
    if ((this.zzqj != null) && (this.zzqj.length > 0))
    {
      i = 0;
      while (i < this.zzqj.length)
      {
        localzzp = this.zzqj[i];
        if (localzzp != null) {
          paramzzyy.zza(3, localzzp);
        }
        i += 1;
      }
    }
    if ((this.zzqk != null) && (this.zzqk.length > 0))
    {
      i = 0;
      while (i < this.zzqk.length)
      {
        localzzp = this.zzqk[i];
        if (localzzp != null) {
          paramzzyy.zza(4, localzzp);
        }
        i += 1;
      }
    }
    if ((this.zzql != null) && (this.zzql.length > 0))
    {
      i = 0;
      while (i < this.zzql.length)
      {
        localzzp = this.zzql[i];
        if (localzzp != null) {
          paramzzyy.zza(5, localzzp);
        }
        i += 1;
      }
    }
    if ((this.zzqm != null) && (!this.zzqm.equals(""))) {
      paramzzyy.zzb(6, this.zzqm);
    }
    if ((this.zzqn != null) && (!this.zzqn.equals(""))) {
      paramzzyy.zzb(7, this.zzqn);
    }
    if (this.zzqo != 0L) {
      paramzzyy.zzi(8, this.zzqo);
    }
    if (this.zzqs) {
      paramzzyy.zzb(9, this.zzqs);
    }
    if ((this.zzqr != null) && (this.zzqr.length > 0))
    {
      i = 0;
      while (i < this.zzqr.length)
      {
        paramzzyy.zzd(10, this.zzqr[i]);
        i += 1;
      }
    }
    if ((this.zzqq != null) && (this.zzqq.length > 0))
    {
      i = j;
      while (i < this.zzqq.length)
      {
        localzzp = this.zzqq[i];
        if (localzzp != null) {
          paramzzyy.zza(11, localzzp);
        }
        i += 1;
      }
    }
    if (this.zzqp) {
      paramzzyy.zzb(12, this.zzqp);
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int m = 0;
    int j = super.zzf() + zzyy.zzh(1, this.type);
    int i = j;
    if (this.string != null)
    {
      i = j;
      if (!this.string.equals("")) {
        i = j + zzyy.zzc(2, this.string);
      }
    }
    j = i;
    zzp localzzp;
    int k;
    if (this.zzqj != null)
    {
      j = i;
      if (this.zzqj.length > 0)
      {
        j = 0;
        while (j < this.zzqj.length)
        {
          localzzp = this.zzqj[j];
          k = i;
          if (localzzp != null) {
            k = i + zzyy.zzb(3, localzzp);
          }
          j += 1;
          i = k;
        }
        j = i;
      }
    }
    i = j;
    if (this.zzqk != null)
    {
      i = j;
      if (this.zzqk.length > 0)
      {
        i = j;
        j = 0;
        while (j < this.zzqk.length)
        {
          localzzp = this.zzqk[j];
          k = i;
          if (localzzp != null) {
            k = i + zzyy.zzb(4, localzzp);
          }
          j += 1;
          i = k;
        }
      }
    }
    j = i;
    if (this.zzql != null)
    {
      j = i;
      if (this.zzql.length > 0)
      {
        j = 0;
        while (j < this.zzql.length)
        {
          localzzp = this.zzql[j];
          k = i;
          if (localzzp != null) {
            k = i + zzyy.zzb(5, localzzp);
          }
          j += 1;
          i = k;
        }
        j = i;
      }
    }
    i = j;
    if (this.zzqm != null)
    {
      i = j;
      if (!this.zzqm.equals("")) {
        i = j + zzyy.zzc(6, this.zzqm);
      }
    }
    j = i;
    if (this.zzqn != null)
    {
      j = i;
      if (!this.zzqn.equals("")) {
        j = i + zzyy.zzc(7, this.zzqn);
      }
    }
    i = j;
    if (this.zzqo != 0L) {
      i = j + zzyy.zzd(8, this.zzqo);
    }
    j = i;
    if (this.zzqs) {
      j = i + (zzyy.zzbb(9) + 1);
    }
    i = j;
    if (this.zzqr != null)
    {
      i = j;
      if (this.zzqr.length > 0)
      {
        i = 0;
        k = 0;
        while (i < this.zzqr.length)
        {
          k += zzyy.zzbc(this.zzqr[i]);
          i += 1;
        }
        i = j + k + this.zzqr.length * 1;
      }
    }
    j = i;
    if (this.zzqq != null)
    {
      j = i;
      if (this.zzqq.length > 0)
      {
        k = m;
        for (;;)
        {
          j = i;
          if (k >= this.zzqq.length) {
            break;
          }
          localzzp = this.zzqq[k];
          j = i;
          if (localzzp != null) {
            j = i + zzyy.zzb(11, localzzp);
          }
          k += 1;
          i = j;
        }
      }
    }
    i = j;
    if (this.zzqp) {
      i = j + (zzyy.zzbb(12) + 1);
    }
    return i;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */