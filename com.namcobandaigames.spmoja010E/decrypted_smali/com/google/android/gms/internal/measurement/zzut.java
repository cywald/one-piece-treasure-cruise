package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class zzut
  extends zzuc
{
  private static final Logger logger = Logger.getLogger(zzut.class.getName());
  private static final boolean zzbuv = zzyh.zzyi();
  zzuv zzbuw;
  
  public static int zza(int paramInt, zzwa paramzzwa)
  {
    paramInt = zzbb(paramInt);
    int i = paramzzwa.zzvu();
    return paramInt + (i + zzbd(i));
  }
  
  public static int zza(zzwa paramzzwa)
  {
    int i = paramzzwa.zzvu();
    return i + zzbd(i);
  }
  
  public static zzut zza(ByteBuffer paramByteBuffer)
  {
    if (paramByteBuffer.hasArray()) {
      return new zzb(paramByteBuffer);
    }
    if ((paramByteBuffer.isDirect()) && (!paramByteBuffer.isReadOnly()))
    {
      if (zzyh.zzyj()) {
        return new zze(paramByteBuffer);
      }
      return new zzd(paramByteBuffer);
    }
    throw new IllegalArgumentException("ByteBuffer is read-only");
  }
  
  public static int zzay(long paramLong)
  {
    return zzaz(paramLong);
  }
  
  public static int zzaz(long paramLong)
  {
    if ((0xFFFFFFFFFFFFFF80 & paramLong) == 0L)
    {
      j = 1;
      return j;
    }
    if (paramLong < 0L) {
      return 10;
    }
    int j = 2;
    if ((0xFFFFFFF800000000 & paramLong) != 0L)
    {
      j = 6;
      paramLong >>>= 28;
    }
    for (;;)
    {
      int i = j;
      long l = paramLong;
      if ((0xFFFFFFFFFFE00000 & paramLong) != 0L)
      {
        i = j + 2;
        l = paramLong >>> 14;
      }
      j = i;
      if ((l & 0xFFFFFFFFFFFFC000) == 0L) {
        break;
      }
      return i + 1;
    }
  }
  
  public static int zzb(float paramFloat)
  {
    return 4;
  }
  
  public static int zzb(int paramInt, double paramDouble)
  {
    return zzbb(paramInt) + 8;
  }
  
  public static int zzb(int paramInt, float paramFloat)
  {
    return zzbb(paramInt) + 4;
  }
  
  public static int zzb(int paramInt, zzwa paramzzwa)
  {
    return (zzbb(1) << 1) + zzi(2, paramInt) + zza(3, paramzzwa);
  }
  
  static int zzb(int paramInt, zzwt paramzzwt, zzxj paramzzxj)
  {
    return zzbb(paramInt) + zzb(paramzzwt, paramzzxj);
  }
  
  public static int zzb(zzud paramzzud)
  {
    int i = paramzzud.size();
    return i + zzbd(i);
  }
  
  static int zzb(zzwt paramzzwt, zzxj paramzzxj)
  {
    paramzzwt = (zztw)paramzzwt;
    int j = paramzzwt.zztu();
    int i = j;
    if (j == -1)
    {
      i = paramzzxj.zzae(paramzzwt);
      paramzzwt.zzah(i);
    }
    return i + zzbd(i);
  }
  
  public static int zzba(long paramLong)
  {
    return zzaz(zzbd(paramLong));
  }
  
  public static int zzbb(int paramInt)
  {
    return zzbd(paramInt << 3);
  }
  
  public static int zzbb(long paramLong)
  {
    return 8;
  }
  
  public static int zzbc(int paramInt)
  {
    if (paramInt >= 0) {
      return zzbd(paramInt);
    }
    return 10;
  }
  
  public static int zzbc(long paramLong)
  {
    return 8;
  }
  
  public static int zzbd(int paramInt)
  {
    if ((paramInt & 0xFFFFFF80) == 0) {
      return 1;
    }
    if ((paramInt & 0xC000) == 0) {
      return 2;
    }
    if ((0xFFE00000 & paramInt) == 0) {
      return 3;
    }
    if ((0xF0000000 & paramInt) == 0) {
      return 4;
    }
    return 5;
  }
  
  private static long zzbd(long paramLong)
  {
    return paramLong << 1 ^ paramLong >> 63;
  }
  
  public static int zzbe(int paramInt)
  {
    return zzbd(zzbi(paramInt));
  }
  
  public static int zzbf(int paramInt)
  {
    return 4;
  }
  
  public static int zzbg(int paramInt)
  {
    return 4;
  }
  
  public static int zzbh(int paramInt)
  {
    return zzbc(paramInt);
  }
  
  private static int zzbi(int paramInt)
  {
    return paramInt << 1 ^ paramInt >> 31;
  }
  
  @Deprecated
  public static int zzbj(int paramInt)
  {
    return zzbd(paramInt);
  }
  
  public static int zzc(double paramDouble)
  {
    return 8;
  }
  
  public static int zzc(int paramInt, zzud paramzzud)
  {
    paramInt = zzbb(paramInt);
    int i = paramzzud.size();
    return paramInt + (i + zzbd(i));
  }
  
  public static int zzc(int paramInt, zzwt paramzzwt)
  {
    return zzbb(paramInt) + zzc(paramzzwt);
  }
  
  @Deprecated
  static int zzc(int paramInt, zzwt paramzzwt, zzxj paramzzxj)
  {
    int j = zzbb(paramInt);
    paramzzwt = (zztw)paramzzwt;
    int i = paramzzwt.zztu();
    paramInt = i;
    if (i == -1)
    {
      paramInt = paramzzxj.zzae(paramzzwt);
      paramzzwt.zzah(paramInt);
    }
    return paramInt + (j << 1);
  }
  
  public static int zzc(int paramInt, String paramString)
  {
    return zzbb(paramInt) + zzfx(paramString);
  }
  
  public static int zzc(int paramInt, boolean paramBoolean)
  {
    return zzbb(paramInt) + 1;
  }
  
  public static int zzc(zzwt paramzzwt)
  {
    int i = paramzzwt.zzvu();
    return i + zzbd(i);
  }
  
  public static int zzd(int paramInt, long paramLong)
  {
    return zzbb(paramInt) + zzaz(paramLong);
  }
  
  public static int zzd(int paramInt, zzud paramzzud)
  {
    return (zzbb(1) << 1) + zzi(2, paramInt) + zzc(3, paramzzud);
  }
  
  public static int zzd(int paramInt, zzwt paramzzwt)
  {
    return (zzbb(1) << 1) + zzi(2, paramInt) + zzc(3, paramzzwt);
  }
  
  @Deprecated
  public static int zzd(zzwt paramzzwt)
  {
    return paramzzwt.zzvu();
  }
  
  public static int zze(int paramInt, long paramLong)
  {
    return zzbb(paramInt) + zzaz(paramLong);
  }
  
  public static int zzf(int paramInt, long paramLong)
  {
    return zzbb(paramInt) + zzaz(zzbd(paramLong));
  }
  
  public static int zzfx(String paramString)
  {
    try
    {
      i = zzyj.zza(paramString);
      return i + zzbd(i);
    }
    catch (zzyn localzzyn)
    {
      for (;;)
      {
        int i = paramString.getBytes(zzvo.UTF_8).length;
      }
    }
  }
  
  public static int zzg(int paramInt, long paramLong)
  {
    return zzbb(paramInt) + 8;
  }
  
  public static int zzh(int paramInt1, int paramInt2)
  {
    return zzbb(paramInt1) + zzbc(paramInt2);
  }
  
  public static int zzh(int paramInt, long paramLong)
  {
    return zzbb(paramInt) + 8;
  }
  
  public static int zzi(int paramInt1, int paramInt2)
  {
    return zzbb(paramInt1) + zzbd(paramInt2);
  }
  
  public static int zzj(int paramInt1, int paramInt2)
  {
    return zzbb(paramInt1) + zzbd(zzbi(paramInt2));
  }
  
  public static zzut zzj(byte[] paramArrayOfByte)
  {
    return new zza(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public static int zzk(int paramInt1, int paramInt2)
  {
    return zzbb(paramInt1) + 4;
  }
  
  public static int zzk(byte[] paramArrayOfByte)
  {
    int i = paramArrayOfByte.length;
    return i + zzbd(i);
  }
  
  public static int zzl(int paramInt1, int paramInt2)
  {
    return zzbb(paramInt1) + 4;
  }
  
  public static int zzm(int paramInt1, int paramInt2)
  {
    return zzbb(paramInt1) + zzbc(paramInt2);
  }
  
  public static int zzv(boolean paramBoolean)
  {
    return 1;
  }
  
  public abstract void flush()
    throws IOException;
  
  public abstract void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException;
  
  public final void zza(float paramFloat)
    throws IOException
  {
    zzba(Float.floatToRawIntBits(paramFloat));
  }
  
  public final void zza(int paramInt, double paramDouble)
    throws IOException
  {
    zzc(paramInt, Double.doubleToRawLongBits(paramDouble));
  }
  
  public final void zza(int paramInt, float paramFloat)
    throws IOException
  {
    zzg(paramInt, Float.floatToRawIntBits(paramFloat));
  }
  
  public abstract void zza(int paramInt, long paramLong)
    throws IOException;
  
  public abstract void zza(int paramInt, zzud paramzzud)
    throws IOException;
  
  public abstract void zza(int paramInt, zzwt paramzzwt)
    throws IOException;
  
  abstract void zza(int paramInt, zzwt paramzzwt, zzxj paramzzxj)
    throws IOException;
  
  public abstract void zza(zzud paramzzud)
    throws IOException;
  
  abstract void zza(zzwt paramzzwt, zzxj paramzzxj)
    throws IOException;
  
  final void zza(String paramString, zzyn paramzzyn)
    throws IOException
  {
    logger.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", paramzzyn);
    paramString = paramString.getBytes(zzvo.UTF_8);
    try
    {
      zzay(paramString.length);
      zza(paramString, 0, paramString.length);
      return;
    }
    catch (IndexOutOfBoundsException paramString)
    {
      throw new zzc(paramString);
    }
    catch (zzc paramString)
    {
      throw paramString;
    }
  }
  
  public abstract void zzav(long paramLong)
    throws IOException;
  
  public final void zzaw(long paramLong)
    throws IOException
  {
    zzav(zzbd(paramLong));
  }
  
  public abstract void zzax(int paramInt)
    throws IOException;
  
  public abstract void zzax(long paramLong)
    throws IOException;
  
  public abstract void zzay(int paramInt)
    throws IOException;
  
  public final void zzaz(int paramInt)
    throws IOException
  {
    zzay(zzbi(paramInt));
  }
  
  public final void zzb(double paramDouble)
    throws IOException
  {
    zzax(Double.doubleToRawLongBits(paramDouble));
  }
  
  public final void zzb(int paramInt, long paramLong)
    throws IOException
  {
    zza(paramInt, zzbd(paramLong));
  }
  
  public abstract void zzb(int paramInt, zzud paramzzud)
    throws IOException;
  
  public abstract void zzb(int paramInt, zzwt paramzzwt)
    throws IOException;
  
  public abstract void zzb(int paramInt, String paramString)
    throws IOException;
  
  public abstract void zzb(int paramInt, boolean paramBoolean)
    throws IOException;
  
  public abstract void zzb(zzwt paramzzwt)
    throws IOException;
  
  public abstract void zzba(int paramInt)
    throws IOException;
  
  public abstract void zzc(byte paramByte)
    throws IOException;
  
  public abstract void zzc(int paramInt1, int paramInt2)
    throws IOException;
  
  public abstract void zzc(int paramInt, long paramLong)
    throws IOException;
  
  public abstract void zzd(int paramInt1, int paramInt2)
    throws IOException;
  
  public abstract void zze(int paramInt1, int paramInt2)
    throws IOException;
  
  abstract void zze(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException;
  
  public final void zzf(int paramInt1, int paramInt2)
    throws IOException
  {
    zze(paramInt1, zzbi(paramInt2));
  }
  
  public abstract void zzfw(String paramString)
    throws IOException;
  
  public abstract void zzg(int paramInt1, int paramInt2)
    throws IOException;
  
  public final void zzu(boolean paramBoolean)
    throws IOException
  {
    if (paramBoolean) {}
    for (int i = 1;; i = 0)
    {
      zzc((byte)i);
      return;
    }
  }
  
  public abstract int zzvg();
  
  static class zza
    extends zzut
  {
    private final byte[] buffer;
    private final int limit;
    private final int offset;
    private int position;
    
    zza(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      super();
      if (paramArrayOfByte == null) {
        throw new NullPointerException("buffer");
      }
      if ((paramInt1 | paramInt2 | paramArrayOfByte.length - (paramInt1 + paramInt2)) < 0) {
        throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", new Object[] { Integer.valueOf(paramArrayOfByte.length), Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }));
      }
      this.buffer = paramArrayOfByte;
      this.offset = paramInt1;
      this.position = paramInt1;
      this.limit = (paramInt1 + paramInt2);
    }
    
    public void flush() {}
    
    public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      try
      {
        System.arraycopy(paramArrayOfByte, paramInt1, this.buffer, this.position, paramInt2);
        this.position += paramInt2;
        return;
      }
      catch (IndexOutOfBoundsException paramArrayOfByte)
      {
        throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(paramInt2) }), paramArrayOfByte);
      }
    }
    
    public final void zza(int paramInt, long paramLong)
      throws IOException
    {
      zzc(paramInt, 0);
      zzav(paramLong);
    }
    
    public final void zza(int paramInt, zzud paramzzud)
      throws IOException
    {
      zzc(paramInt, 2);
      zza(paramzzud);
    }
    
    public final void zza(int paramInt, zzwt paramzzwt)
      throws IOException
    {
      zzc(paramInt, 2);
      zzb(paramzzwt);
    }
    
    final void zza(int paramInt, zzwt paramzzwt, zzxj paramzzxj)
      throws IOException
    {
      zzc(paramInt, 2);
      zztw localzztw = (zztw)paramzzwt;
      int i = localzztw.zztu();
      paramInt = i;
      if (i == -1)
      {
        paramInt = paramzzxj.zzae(localzztw);
        localzztw.zzah(paramInt);
      }
      zzay(paramInt);
      paramzzxj.zza(paramzzwt, this.zzbuw);
    }
    
    public final void zza(zzud paramzzud)
      throws IOException
    {
      zzay(paramzzud.size());
      paramzzud.zza(this);
    }
    
    final void zza(zzwt paramzzwt, zzxj paramzzxj)
      throws IOException
    {
      zztw localzztw = (zztw)paramzzwt;
      int j = localzztw.zztu();
      int i = j;
      if (j == -1)
      {
        i = paramzzxj.zzae(localzztw);
        localzztw.zzah(i);
      }
      zzay(i);
      paramzzxj.zza(paramzzwt, this.zzbuw);
    }
    
    public final void zza(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      write(paramArrayOfByte, paramInt1, paramInt2);
    }
    
    public final void zzav(long paramLong)
      throws IOException
    {
      long l = paramLong;
      byte[] arrayOfByte;
      int i;
      if (zzut.zzvh())
      {
        l = paramLong;
        if (zzvg() >= 10) {
          for (;;)
          {
            if ((paramLong & 0xFFFFFFFFFFFFFF80) == 0L)
            {
              arrayOfByte = this.buffer;
              i = this.position;
              this.position = (i + 1);
              zzyh.zza(arrayOfByte, i, (byte)(int)paramLong);
              return;
            }
            arrayOfByte = this.buffer;
            i = this.position;
            this.position = (i + 1);
            zzyh.zza(arrayOfByte, i, (byte)((int)paramLong & 0x7F | 0x80));
            paramLong >>>= 7;
          }
        }
      }
      try
      {
        do
        {
          arrayOfByte = this.buffer;
          i = this.position;
          this.position = (i + 1);
          arrayOfByte[i] = ((byte)((int)l & 0x7F | 0x80));
          l >>>= 7;
        } while ((l & 0xFFFFFFFFFFFFFF80) != 0L);
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)l);
        return;
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(1) }), localIndexOutOfBoundsException);
      }
    }
    
    public final void zzax(int paramInt)
      throws IOException
    {
      if (paramInt >= 0)
      {
        zzay(paramInt);
        return;
      }
      zzav(paramInt);
    }
    
    public final void zzax(long paramLong)
      throws IOException
    {
      try
      {
        byte[] arrayOfByte = this.buffer;
        int i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)paramLong);
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)(paramLong >> 8));
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)(paramLong >> 16));
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)(paramLong >> 24));
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)(paramLong >> 32));
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)(paramLong >> 40));
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)(paramLong >> 48));
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(int)(paramLong >> 56));
        return;
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(1) }), localIndexOutOfBoundsException);
      }
    }
    
    public final void zzay(int paramInt)
      throws IOException
    {
      int i = paramInt;
      byte[] arrayOfByte;
      if (zzut.zzvh())
      {
        i = paramInt;
        if (zzvg() >= 10) {
          for (;;)
          {
            if ((paramInt & 0xFFFFFF80) == 0)
            {
              arrayOfByte = this.buffer;
              i = this.position;
              this.position = (i + 1);
              zzyh.zza(arrayOfByte, i, (byte)paramInt);
              return;
            }
            arrayOfByte = this.buffer;
            i = this.position;
            this.position = (i + 1);
            zzyh.zza(arrayOfByte, i, (byte)(paramInt & 0x7F | 0x80));
            paramInt >>>= 7;
          }
        }
      }
      try
      {
        do
        {
          arrayOfByte = this.buffer;
          paramInt = this.position;
          this.position = (paramInt + 1);
          arrayOfByte[paramInt] = ((byte)(i & 0x7F | 0x80));
          i >>>= 7;
        } while ((i & 0xFFFFFF80) != 0);
        arrayOfByte = this.buffer;
        paramInt = this.position;
        this.position = (paramInt + 1);
        arrayOfByte[paramInt] = ((byte)i);
        return;
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(1) }), localIndexOutOfBoundsException);
      }
    }
    
    public final void zzb(int paramInt, zzud paramzzud)
      throws IOException
    {
      zzc(1, 3);
      zze(2, paramInt);
      zza(3, paramzzud);
      zzc(1, 4);
    }
    
    public final void zzb(int paramInt, zzwt paramzzwt)
      throws IOException
    {
      zzc(1, 3);
      zze(2, paramInt);
      zza(3, paramzzwt);
      zzc(1, 4);
    }
    
    public final void zzb(int paramInt, String paramString)
      throws IOException
    {
      zzc(paramInt, 2);
      zzfw(paramString);
    }
    
    public final void zzb(int paramInt, boolean paramBoolean)
      throws IOException
    {
      int i = 0;
      zzc(paramInt, 0);
      paramInt = i;
      if (paramBoolean) {
        paramInt = 1;
      }
      zzc((byte)paramInt);
    }
    
    public final void zzb(zzwt paramzzwt)
      throws IOException
    {
      zzay(paramzzwt.zzvu());
      paramzzwt.zzb(this);
    }
    
    public final void zzba(int paramInt)
      throws IOException
    {
      try
      {
        byte[] arrayOfByte = this.buffer;
        int i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)paramInt);
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(paramInt >> 8));
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = ((byte)(paramInt >> 16));
        arrayOfByte = this.buffer;
        i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = (paramInt >> 24);
        return;
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(1) }), localIndexOutOfBoundsException);
      }
    }
    
    public final void zzc(byte paramByte)
      throws IOException
    {
      try
      {
        byte[] arrayOfByte = this.buffer;
        int i = this.position;
        this.position = (i + 1);
        arrayOfByte[i] = paramByte;
        return;
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(1) }), localIndexOutOfBoundsException);
      }
    }
    
    public final void zzc(int paramInt1, int paramInt2)
      throws IOException
    {
      zzay(paramInt1 << 3 | paramInt2);
    }
    
    public final void zzc(int paramInt, long paramLong)
      throws IOException
    {
      zzc(paramInt, 1);
      zzax(paramLong);
    }
    
    public final void zzd(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 0);
      zzax(paramInt2);
    }
    
    public final void zze(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 0);
      zzay(paramInt2);
    }
    
    public final void zze(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      zzay(paramInt2);
      write(paramArrayOfByte, 0, paramInt2);
    }
    
    public final void zzfw(String paramString)
      throws IOException
    {
      int i = this.position;
      try
      {
        int k = zzbd(paramString.length() * 3);
        int j = zzbd(paramString.length());
        if (j == k)
        {
          this.position = (i + j);
          k = zzyj.zza(paramString, this.buffer, this.position, zzvg());
          this.position = i;
          zzay(k - i - j);
          this.position = k;
          return;
        }
        zzay(zzyj.zza(paramString));
        this.position = zzyj.zza(paramString, this.buffer, this.position, zzvg());
        return;
      }
      catch (zzyn localzzyn)
      {
        this.position = i;
        zza(paramString, localzzyn);
        return;
      }
      catch (IndexOutOfBoundsException paramString)
      {
        throw new zzut.zzc(paramString);
      }
    }
    
    public final void zzg(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 5);
      zzba(paramInt2);
    }
    
    public final int zzvg()
    {
      return this.limit - this.position;
    }
    
    public final int zzvi()
    {
      return this.position - this.offset;
    }
  }
  
  static final class zzb
    extends zzut.zza
  {
    private final ByteBuffer zzbux;
    private int zzbuy;
    
    zzb(ByteBuffer paramByteBuffer)
    {
      super(paramByteBuffer.arrayOffset() + paramByteBuffer.position(), paramByteBuffer.remaining());
      this.zzbux = paramByteBuffer;
      this.zzbuy = paramByteBuffer.position();
    }
    
    public final void flush()
    {
      this.zzbux.position(this.zzbuy + zzvi());
    }
  }
  
  public static final class zzc
    extends IOException
  {
    zzc()
    {
      super();
    }
    
    zzc(String paramString) {}
    
    zzc(String paramString, Throwable paramThrowable) {}
    
    zzc(Throwable paramThrowable)
    {
      super(paramThrowable);
    }
  }
  
  static final class zzd
    extends zzut
  {
    private final int zzbuy;
    private final ByteBuffer zzbuz;
    private final ByteBuffer zzbva;
    
    zzd(ByteBuffer paramByteBuffer)
    {
      super();
      this.zzbuz = paramByteBuffer;
      this.zzbva = paramByteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
      this.zzbuy = paramByteBuffer.position();
    }
    
    private final void zzfy(String paramString)
      throws IOException
    {
      try
      {
        zzyj.zza(paramString, this.zzbva);
        return;
      }
      catch (IndexOutOfBoundsException paramString)
      {
        throw new zzut.zzc(paramString);
      }
    }
    
    public final void flush()
    {
      this.zzbuz.position(this.zzbva.position());
    }
    
    public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      try
      {
        this.zzbva.put(paramArrayOfByte, paramInt1, paramInt2);
        return;
      }
      catch (IndexOutOfBoundsException paramArrayOfByte)
      {
        throw new zzut.zzc(paramArrayOfByte);
      }
      catch (BufferOverflowException paramArrayOfByte)
      {
        throw new zzut.zzc(paramArrayOfByte);
      }
    }
    
    public final void zza(int paramInt, long paramLong)
      throws IOException
    {
      zzc(paramInt, 0);
      zzav(paramLong);
    }
    
    public final void zza(int paramInt, zzud paramzzud)
      throws IOException
    {
      zzc(paramInt, 2);
      zza(paramzzud);
    }
    
    public final void zza(int paramInt, zzwt paramzzwt)
      throws IOException
    {
      zzc(paramInt, 2);
      zzb(paramzzwt);
    }
    
    final void zza(int paramInt, zzwt paramzzwt, zzxj paramzzxj)
      throws IOException
    {
      zzc(paramInt, 2);
      zza(paramzzwt, paramzzxj);
    }
    
    public final void zza(zzud paramzzud)
      throws IOException
    {
      zzay(paramzzud.size());
      paramzzud.zza(this);
    }
    
    final void zza(zzwt paramzzwt, zzxj paramzzxj)
      throws IOException
    {
      zztw localzztw = (zztw)paramzzwt;
      int j = localzztw.zztu();
      int i = j;
      if (j == -1)
      {
        i = paramzzxj.zzae(localzztw);
        localzztw.zzah(i);
      }
      zzay(i);
      paramzzxj.zza(paramzzwt, this.zzbuw);
    }
    
    public final void zza(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      write(paramArrayOfByte, paramInt1, paramInt2);
    }
    
    public final void zzav(long paramLong)
      throws IOException
    {
      for (;;)
      {
        if ((0xFFFFFFFFFFFFFF80 & paramLong) == 0L) {}
        try
        {
          this.zzbva.put((byte)(int)paramLong);
          return;
        }
        catch (BufferOverflowException localBufferOverflowException)
        {
          throw new zzut.zzc(localBufferOverflowException);
        }
        this.zzbva.put((byte)((int)paramLong & 0x7F | 0x80));
        paramLong >>>= 7;
      }
    }
    
    public final void zzax(int paramInt)
      throws IOException
    {
      if (paramInt >= 0)
      {
        zzay(paramInt);
        return;
      }
      zzav(paramInt);
    }
    
    public final void zzax(long paramLong)
      throws IOException
    {
      try
      {
        this.zzbva.putLong(paramLong);
        return;
      }
      catch (BufferOverflowException localBufferOverflowException)
      {
        throw new zzut.zzc(localBufferOverflowException);
      }
    }
    
    public final void zzay(int paramInt)
      throws IOException
    {
      for (;;)
      {
        if ((paramInt & 0xFFFFFF80) == 0) {}
        try
        {
          this.zzbva.put((byte)paramInt);
          return;
        }
        catch (BufferOverflowException localBufferOverflowException)
        {
          throw new zzut.zzc(localBufferOverflowException);
        }
        this.zzbva.put((byte)(paramInt & 0x7F | 0x80));
        paramInt >>>= 7;
      }
    }
    
    public final void zzb(int paramInt, zzud paramzzud)
      throws IOException
    {
      zzc(1, 3);
      zze(2, paramInt);
      zza(3, paramzzud);
      zzc(1, 4);
    }
    
    public final void zzb(int paramInt, zzwt paramzzwt)
      throws IOException
    {
      zzc(1, 3);
      zze(2, paramInt);
      zza(3, paramzzwt);
      zzc(1, 4);
    }
    
    public final void zzb(int paramInt, String paramString)
      throws IOException
    {
      zzc(paramInt, 2);
      zzfw(paramString);
    }
    
    public final void zzb(int paramInt, boolean paramBoolean)
      throws IOException
    {
      int i = 0;
      zzc(paramInt, 0);
      paramInt = i;
      if (paramBoolean) {
        paramInt = 1;
      }
      zzc((byte)paramInt);
    }
    
    public final void zzb(zzwt paramzzwt)
      throws IOException
    {
      zzay(paramzzwt.zzvu());
      paramzzwt.zzb(this);
    }
    
    public final void zzba(int paramInt)
      throws IOException
    {
      try
      {
        this.zzbva.putInt(paramInt);
        return;
      }
      catch (BufferOverflowException localBufferOverflowException)
      {
        throw new zzut.zzc(localBufferOverflowException);
      }
    }
    
    public final void zzc(byte paramByte)
      throws IOException
    {
      try
      {
        this.zzbva.put(paramByte);
        return;
      }
      catch (BufferOverflowException localBufferOverflowException)
      {
        throw new zzut.zzc(localBufferOverflowException);
      }
    }
    
    public final void zzc(int paramInt1, int paramInt2)
      throws IOException
    {
      zzay(paramInt1 << 3 | paramInt2);
    }
    
    public final void zzc(int paramInt, long paramLong)
      throws IOException
    {
      zzc(paramInt, 1);
      zzax(paramLong);
    }
    
    public final void zzd(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 0);
      zzax(paramInt2);
    }
    
    public final void zze(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 0);
      zzay(paramInt2);
    }
    
    public final void zze(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      zzay(paramInt2);
      write(paramArrayOfByte, 0, paramInt2);
    }
    
    public final void zzfw(String paramString)
      throws IOException
    {
      int i = this.zzbva.position();
      try
      {
        int j = zzbd(paramString.length() * 3);
        int k = zzbd(paramString.length());
        if (k == j)
        {
          j = this.zzbva.position() + k;
          this.zzbva.position(j);
          zzfy(paramString);
          k = this.zzbva.position();
          this.zzbva.position(i);
          zzay(k - j);
          this.zzbva.position(k);
          return;
        }
        zzay(zzyj.zza(paramString));
        zzfy(paramString);
        return;
      }
      catch (zzyn localzzyn)
      {
        this.zzbva.position(i);
        zza(paramString, localzzyn);
        return;
      }
      catch (IllegalArgumentException paramString)
      {
        throw new zzut.zzc(paramString);
      }
    }
    
    public final void zzg(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 5);
      zzba(paramInt2);
    }
    
    public final int zzvg()
    {
      return this.zzbva.remaining();
    }
  }
  
  static final class zze
    extends zzut
  {
    private final ByteBuffer zzbuz;
    private final ByteBuffer zzbva;
    private final long zzbvb;
    private final long zzbvc;
    private final long zzbvd;
    private final long zzbve;
    private long zzbvf;
    
    zze(ByteBuffer paramByteBuffer)
    {
      super();
      this.zzbuz = paramByteBuffer;
      this.zzbva = paramByteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
      this.zzbvb = zzyh.zzb(paramByteBuffer);
      this.zzbvc = (this.zzbvb + paramByteBuffer.position());
      this.zzbvd = (this.zzbvb + paramByteBuffer.limit());
      this.zzbve = (this.zzbvd - 10L);
      this.zzbvf = this.zzbvc;
    }
    
    private final void zzbe(long paramLong)
    {
      this.zzbva.position((int)(paramLong - this.zzbvb));
    }
    
    public final void flush()
    {
      this.zzbuz.position((int)(this.zzbvf - this.zzbvb));
    }
    
    public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      if ((paramArrayOfByte == null) || (paramInt1 < 0) || (paramInt2 < 0) || (paramArrayOfByte.length - paramInt2 < paramInt1) || (this.zzbvd - paramInt2 < this.zzbvf))
      {
        if (paramArrayOfByte == null) {
          throw new NullPointerException("value");
        }
        throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Long.valueOf(this.zzbvf), Long.valueOf(this.zzbvd), Integer.valueOf(paramInt2) }));
      }
      zzyh.zza(paramArrayOfByte, paramInt1, this.zzbvf, paramInt2);
      this.zzbvf += paramInt2;
    }
    
    public final void zza(int paramInt, long paramLong)
      throws IOException
    {
      zzc(paramInt, 0);
      zzav(paramLong);
    }
    
    public final void zza(int paramInt, zzud paramzzud)
      throws IOException
    {
      zzc(paramInt, 2);
      zza(paramzzud);
    }
    
    public final void zza(int paramInt, zzwt paramzzwt)
      throws IOException
    {
      zzc(paramInt, 2);
      zzb(paramzzwt);
    }
    
    final void zza(int paramInt, zzwt paramzzwt, zzxj paramzzxj)
      throws IOException
    {
      zzc(paramInt, 2);
      zza(paramzzwt, paramzzxj);
    }
    
    public final void zza(zzud paramzzud)
      throws IOException
    {
      zzay(paramzzud.size());
      paramzzud.zza(this);
    }
    
    final void zza(zzwt paramzzwt, zzxj paramzzxj)
      throws IOException
    {
      zztw localzztw = (zztw)paramzzwt;
      int j = localzztw.zztu();
      int i = j;
      if (j == -1)
      {
        i = paramzzxj.zzae(localzztw);
        localzztw.zzah(i);
      }
      zzay(i);
      paramzzxj.zza(paramzzwt, this.zzbuw);
    }
    
    public final void zza(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      write(paramArrayOfByte, paramInt1, paramInt2);
    }
    
    public final void zzav(long paramLong)
      throws IOException
    {
      long l = paramLong;
      if (this.zzbvf <= this.zzbve) {
        for (;;)
        {
          if ((paramLong & 0xFFFFFFFFFFFFFF80) == 0L)
          {
            l = this.zzbvf;
            this.zzbvf = (l + 1L);
            zzyh.zza(l, (byte)(int)paramLong);
            return;
          }
          l = this.zzbvf;
          this.zzbvf = (l + 1L);
          zzyh.zza(l, (byte)((int)paramLong & 0x7F | 0x80));
          paramLong >>>= 7;
        }
      }
      do
      {
        paramLong = this.zzbvf;
        this.zzbvf = (paramLong + 1L);
        zzyh.zza(paramLong, (byte)((int)l & 0x7F | 0x80));
        l >>>= 7;
        if (this.zzbvf >= this.zzbvd) {
          break;
        }
      } while ((l & 0xFFFFFFFFFFFFFF80) != 0L);
      paramLong = this.zzbvf;
      this.zzbvf = (paramLong + 1L);
      zzyh.zza(paramLong, (byte)(int)l);
      return;
      throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Long.valueOf(this.zzbvf), Long.valueOf(this.zzbvd), Integer.valueOf(1) }));
    }
    
    public final void zzax(int paramInt)
      throws IOException
    {
      if (paramInt >= 0)
      {
        zzay(paramInt);
        return;
      }
      zzav(paramInt);
    }
    
    public final void zzax(long paramLong)
      throws IOException
    {
      this.zzbva.putLong((int)(this.zzbvf - this.zzbvb), paramLong);
      this.zzbvf += 8L;
    }
    
    public final void zzay(int paramInt)
      throws IOException
    {
      int i = paramInt;
      if (this.zzbvf <= this.zzbve) {
        for (;;)
        {
          if ((paramInt & 0xFFFFFF80) == 0)
          {
            l = this.zzbvf;
            this.zzbvf = (l + 1L);
            zzyh.zza(l, (byte)paramInt);
            return;
          }
          l = this.zzbvf;
          this.zzbvf = (l + 1L);
          zzyh.zza(l, (byte)(paramInt & 0x7F | 0x80));
          paramInt >>>= 7;
        }
      }
      do
      {
        l = this.zzbvf;
        this.zzbvf = (l + 1L);
        zzyh.zza(l, (byte)(i & 0x7F | 0x80));
        i >>>= 7;
        if (this.zzbvf >= this.zzbvd) {
          break;
        }
      } while ((i & 0xFFFFFF80) != 0);
      long l = this.zzbvf;
      this.zzbvf = (l + 1L);
      zzyh.zza(l, (byte)i);
      return;
      throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Long.valueOf(this.zzbvf), Long.valueOf(this.zzbvd), Integer.valueOf(1) }));
    }
    
    public final void zzb(int paramInt, zzud paramzzud)
      throws IOException
    {
      zzc(1, 3);
      zze(2, paramInt);
      zza(3, paramzzud);
      zzc(1, 4);
    }
    
    public final void zzb(int paramInt, zzwt paramzzwt)
      throws IOException
    {
      zzc(1, 3);
      zze(2, paramInt);
      zza(3, paramzzwt);
      zzc(1, 4);
    }
    
    public final void zzb(int paramInt, String paramString)
      throws IOException
    {
      zzc(paramInt, 2);
      zzfw(paramString);
    }
    
    public final void zzb(int paramInt, boolean paramBoolean)
      throws IOException
    {
      int i = 0;
      zzc(paramInt, 0);
      paramInt = i;
      if (paramBoolean) {
        paramInt = 1;
      }
      zzc((byte)paramInt);
    }
    
    public final void zzb(zzwt paramzzwt)
      throws IOException
    {
      zzay(paramzzwt.zzvu());
      paramzzwt.zzb(this);
    }
    
    public final void zzba(int paramInt)
      throws IOException
    {
      this.zzbva.putInt((int)(this.zzbvf - this.zzbvb), paramInt);
      this.zzbvf += 4L;
    }
    
    public final void zzc(byte paramByte)
      throws IOException
    {
      if (this.zzbvf >= this.zzbvd) {
        throw new zzut.zzc(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Long.valueOf(this.zzbvf), Long.valueOf(this.zzbvd), Integer.valueOf(1) }));
      }
      long l = this.zzbvf;
      this.zzbvf = (1L + l);
      zzyh.zza(l, paramByte);
    }
    
    public final void zzc(int paramInt1, int paramInt2)
      throws IOException
    {
      zzay(paramInt1 << 3 | paramInt2);
    }
    
    public final void zzc(int paramInt, long paramLong)
      throws IOException
    {
      zzc(paramInt, 1);
      zzax(paramLong);
    }
    
    public final void zzd(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 0);
      zzax(paramInt2);
    }
    
    public final void zze(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 0);
      zzay(paramInt2);
    }
    
    public final void zze(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      zzay(paramInt2);
      write(paramArrayOfByte, 0, paramInt2);
    }
    
    public final void zzfw(String paramString)
      throws IOException
    {
      long l1 = this.zzbvf;
      try
      {
        int i = zzbd(paramString.length() * 3);
        int j = zzbd(paramString.length());
        if (j == i)
        {
          i = (int)(this.zzbvf - this.zzbvb) + j;
          this.zzbva.position(i);
          zzyj.zza(paramString, this.zzbva);
          i = this.zzbva.position() - i;
          zzay(i);
          l2 = this.zzbvf;
          this.zzbvf = (i + l2);
          return;
        }
        i = zzyj.zza(paramString);
        zzay(i);
        zzbe(this.zzbvf);
        zzyj.zza(paramString, this.zzbva);
        long l2 = this.zzbvf;
        this.zzbvf = (i + l2);
        return;
      }
      catch (zzyn localzzyn)
      {
        this.zzbvf = l1;
        zzbe(this.zzbvf);
        zza(paramString, localzzyn);
        return;
      }
      catch (IllegalArgumentException paramString)
      {
        throw new zzut.zzc(paramString);
      }
      catch (IndexOutOfBoundsException paramString)
      {
        throw new zzut.zzc(paramString);
      }
    }
    
    public final void zzg(int paramInt1, int paramInt2)
      throws IOException
    {
      zzc(paramInt1, 5);
      zzba(paramInt2);
    }
    
    public final int zzvg()
    {
      return (int)(this.zzbvd - this.zzbvf);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzut.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */