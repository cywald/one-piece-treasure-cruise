package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.zzi;
import java.util.HashMap;
import java.util.Map;

public final class zzaj
  extends zzi<zzaj>
{
  public String mCategory;
  public String zzvg;
  public long zzvh;
  public String zzvi;
  
  public final String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("variableName", this.zzvg);
    localHashMap.put("timeInMillis", Long.valueOf(this.zzvh));
    localHashMap.put("category", this.mCategory);
    localHashMap.put("label", this.zzvi);
    return zza(localHashMap);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzaj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */