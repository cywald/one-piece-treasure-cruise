package com.google.android.gms.internal.measurement;

import android.app.job.JobParameters;

public abstract interface zzdb
{
  public abstract boolean callServiceStopSelfResult(int paramInt);
  
  public abstract void zza(JobParameters paramJobParameters, boolean paramBoolean);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzdb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */