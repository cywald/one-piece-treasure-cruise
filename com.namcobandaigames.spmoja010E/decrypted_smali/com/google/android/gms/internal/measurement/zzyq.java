package com.google.android.gms.internal.measurement;

public enum zzyq
{
  private final zzyv zzcei;
  private final int zzcej;
  
  private zzyq(zzyv paramzzyv, int paramInt)
  {
    this.zzcei = paramzzyv;
    this.zzcej = paramInt;
  }
  
  public final zzyv zzyp()
  {
    return this.zzcei;
  }
  
  public final int zzyq()
  {
    return this.zzcej;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzyq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */