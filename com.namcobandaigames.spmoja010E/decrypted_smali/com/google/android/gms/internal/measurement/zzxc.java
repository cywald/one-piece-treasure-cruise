package com.google.android.gms.internal.measurement;

import java.lang.reflect.Constructor;

final class zzxc
{
  private static final zzxa zzcbq = ;
  private static final zzxa zzcbr = new zzxb();
  
  static zzxa zzxk()
  {
    return zzcbq;
  }
  
  static zzxa zzxl()
  {
    return zzcbr;
  }
  
  private static zzxa zzxm()
  {
    try
    {
      zzxa localzzxa = (zzxa)Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
      return localzzxa;
    }
    catch (Exception localException) {}
    return null;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */