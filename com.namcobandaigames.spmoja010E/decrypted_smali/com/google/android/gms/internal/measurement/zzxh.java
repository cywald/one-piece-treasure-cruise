package com.google.android.gms.internal.measurement;

final class zzxh
  implements zzwr
{
  private final int flags;
  private final String info;
  private final Object[] zzcba;
  private final zzwt zzcbd;
  
  zzxh(zzwt paramzzwt, String paramString, Object[] paramArrayOfObject)
  {
    this.zzcbd = paramzzwt;
    this.info = paramString;
    this.zzcba = paramArrayOfObject;
    int i = 1;
    int j = paramString.charAt(0);
    if (j < 55296)
    {
      this.flags = j;
      return;
    }
    int k = j & 0x1FFF;
    j = 13;
    int m;
    for (;;)
    {
      m = paramString.charAt(i);
      if (m < 55296) {
        break;
      }
      k |= (m & 0x1FFF) << j;
      j += 13;
      i += 1;
    }
    this.flags = (m << j | k);
  }
  
  public final int zzxg()
  {
    if ((this.flags & 0x1) == 1) {
      return zzvm.zze.zzbzb;
    }
    return zzvm.zze.zzbzc;
  }
  
  public final boolean zzxh()
  {
    return (this.flags & 0x2) == 2;
  }
  
  public final zzwt zzxi()
  {
    return this.zzcbd;
  }
  
  final String zzxp()
  {
    return this.info;
  }
  
  final Object[] zzxq()
  {
    return this.zzcba;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzxh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */