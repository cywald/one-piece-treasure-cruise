package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.GoogleApiAvailabilityLight;

public final class zzav
{
  public static final String VERSION = String.valueOf(GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE / 1000).replaceAll("(\\d+)(\\d)(\\d\\d)", "$1.$2.$3");
  public static final String zzwa;
  
  static
  {
    String str = String.valueOf(VERSION);
    if (str.length() != 0) {}
    for (str = "ma".concat(str);; str = new String("ma"))
    {
      zzwa = str;
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzav.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */