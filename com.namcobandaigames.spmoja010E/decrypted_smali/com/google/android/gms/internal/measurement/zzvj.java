package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzvj
  extends zztz<Float>
  implements zzvs<Float>, zzxe, RandomAccess
{
  private static final zzvj zzbyi;
  private int size;
  private float[] zzbyj;
  
  static
  {
    zzvj localzzvj = new zzvj();
    zzbyi = localzzvj;
    localzzvj.zzsm();
  }
  
  zzvj()
  {
    this(new float[10], 0);
  }
  
  private zzvj(float[] paramArrayOfFloat, int paramInt)
  {
    this.zzbyj = paramArrayOfFloat;
    this.size = paramInt;
  }
  
  private final void zzai(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
  }
  
  private final String zzaj(int paramInt)
  {
    int i = this.size;
    return 35 + "Index:" + paramInt + ", Size:" + i;
  }
  
  private final void zzc(int paramInt, float paramFloat)
  {
    zztx();
    if ((paramInt < 0) || (paramInt > this.size)) {
      throw new IndexOutOfBoundsException(zzaj(paramInt));
    }
    if (this.size < this.zzbyj.length) {
      System.arraycopy(this.zzbyj, paramInt, this.zzbyj, paramInt + 1, this.size - paramInt);
    }
    for (;;)
    {
      this.zzbyj[paramInt] = paramFloat;
      this.size += 1;
      this.modCount += 1;
      return;
      float[] arrayOfFloat = new float[this.size * 3 / 2 + 1];
      System.arraycopy(this.zzbyj, 0, arrayOfFloat, 0, paramInt);
      System.arraycopy(this.zzbyj, paramInt, arrayOfFloat, paramInt + 1, this.size - paramInt);
      this.zzbyj = arrayOfFloat;
    }
  }
  
  public final boolean addAll(Collection<? extends Float> paramCollection)
  {
    boolean bool = false;
    zztx();
    zzvo.checkNotNull(paramCollection);
    if (!(paramCollection instanceof zzvj)) {
      bool = super.addAll(paramCollection);
    }
    do
    {
      return bool;
      paramCollection = (zzvj)paramCollection;
    } while (paramCollection.size == 0);
    if (Integer.MAX_VALUE - this.size < paramCollection.size) {
      throw new OutOfMemoryError();
    }
    int i = this.size + paramCollection.size;
    if (i > this.zzbyj.length) {
      this.zzbyj = Arrays.copyOf(this.zzbyj, i);
    }
    System.arraycopy(paramCollection.zzbyj, 0, this.zzbyj, this.size, paramCollection.size);
    this.size = i;
    this.modCount += 1;
    return true;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      return bool1;
      if (!(paramObject instanceof zzvj)) {
        return super.equals(paramObject);
      }
      paramObject = (zzvj)paramObject;
      bool1 = bool2;
    } while (this.size != ((zzvj)paramObject).size);
    paramObject = ((zzvj)paramObject).zzbyj;
    int i = 0;
    for (;;)
    {
      if (i >= this.size) {
        break label82;
      }
      bool1 = bool2;
      if (this.zzbyj[i] != paramObject[i]) {
        break;
      }
      i += 1;
    }
    label82:
    return true;
  }
  
  public final int hashCode()
  {
    int j = 1;
    int i = 0;
    while (i < this.size)
    {
      j = j * 31 + Float.floatToIntBits(this.zzbyj[i]);
      i += 1;
    }
    return j;
  }
  
  public final boolean remove(Object paramObject)
  {
    boolean bool2 = false;
    zztx();
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      if (i < this.size)
      {
        if (paramObject.equals(Float.valueOf(this.zzbyj[i])))
        {
          System.arraycopy(this.zzbyj, i + 1, this.zzbyj, i, this.size - i);
          this.size -= 1;
          this.modCount += 1;
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
      i += 1;
    }
  }
  
  protected final void removeRange(int paramInt1, int paramInt2)
  {
    zztx();
    if (paramInt2 < paramInt1) {
      throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }
    System.arraycopy(this.zzbyj, paramInt2, this.zzbyj, paramInt1, this.size - paramInt2);
    this.size -= paramInt2 - paramInt1;
    this.modCount += 1;
  }
  
  public final int size()
  {
    return this.size;
  }
  
  public final void zzc(float paramFloat)
  {
    zzc(this.size, paramFloat);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */