package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.analytics.zza;
import com.google.android.gms.analytics.zzg;
import com.google.android.gms.analytics.zzj;
import com.google.android.gms.analytics.zzk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.wrappers.PackageManagerWrapper;
import com.google.android.gms.common.wrappers.Wrappers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class zzbi
  extends zzau
{
  private boolean started;
  private final zzbf zzxl;
  private final zzcr zzxm;
  private final zzcq zzxn;
  private final zzba zzxo;
  private long zzxp;
  private final zzbz zzxq;
  private final zzbz zzxr;
  private final zzdc zzxs;
  private long zzxt;
  private boolean zzxu;
  
  protected zzbi(zzaw paramzzaw, zzay paramzzay)
  {
    super(paramzzaw);
    Preconditions.checkNotNull(paramzzay);
    this.zzxp = Long.MIN_VALUE;
    this.zzxn = new zzcq(paramzzaw);
    this.zzxl = new zzbf(paramzzaw);
    this.zzxm = new zzcr(paramzzaw);
    this.zzxo = new zzba(paramzzaw);
    this.zzxs = new zzdc(zzbx());
    this.zzxq = new zzbj(this, paramzzaw);
    this.zzxr = new zzbk(this, paramzzaw);
  }
  
  private final void zza(zzaz paramzzaz, zzy paramzzy)
  {
    Preconditions.checkNotNull(paramzzaz);
    Preconditions.checkNotNull(paramzzy);
    Object localObject1 = new zza(zzbw());
    ((zza)localObject1).zza(paramzzaz.zzct());
    ((zza)localObject1).enableAdvertisingIdCollection(paramzzaz.zzcu());
    localObject1 = ((zzj)localObject1).zzm();
    zzag localzzag = (zzag)((zzg)localObject1).zzb(zzag.class);
    localzzag.zzl("data");
    localzzag.zzb(true);
    ((zzg)localObject1).zza(paramzzy);
    zzab localzzab = (zzab)((zzg)localObject1).zzb(zzab.class);
    zzx localzzx = (zzx)((zzg)localObject1).zzb(zzx.class);
    Iterator localIterator = paramzzaz.zzcw().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject2 = (Map.Entry)localIterator.next();
      String str = (String)((Map.Entry)localObject2).getKey();
      localObject2 = (String)((Map.Entry)localObject2).getValue();
      if ("an".equals(str)) {
        localzzx.setAppName((String)localObject2);
      } else if ("av".equals(str)) {
        localzzx.setAppVersion((String)localObject2);
      } else if ("aid".equals(str)) {
        localzzx.setAppId((String)localObject2);
      } else if ("aiid".equals(str)) {
        localzzx.setAppInstallerId((String)localObject2);
      } else if ("uid".equals(str)) {
        localzzag.setUserId((String)localObject2);
      } else {
        localzzab.set(str, (String)localObject2);
      }
    }
    zzb("Sending installation campaign to", paramzzaz.zzct(), paramzzy);
    ((zzg)localObject1).zza(zzcf().zzff());
    ((zzg)localObject1).zzw();
  }
  
  private final long zzdc()
  {
    zzk.zzaf();
    zzcl();
    try
    {
      long l = this.zzxl.zzdc();
      return l;
    }
    catch (SQLiteException localSQLiteException)
    {
      zze("Failed to get min/max hit times from local store", localSQLiteException);
    }
    return 0L;
  }
  
  private final void zzdh()
  {
    zzb(new zzbm(this));
  }
  
  private final void zzdi()
  {
    try
    {
      this.zzxl.zzdb();
      zzdm();
      this.zzxr.zzh(86400000L);
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        zzd("Failed to delete stale hits", localSQLiteException);
      }
    }
  }
  
  private final void zzdj()
  {
    if (this.zzxu) {}
    do
    {
      long l;
      do
      {
        do
        {
          return;
        } while ((!zzbx.zzdx()) || (this.zzxo.isConnected()));
        l = ((Long)zzcf.zzaaj.get()).longValue();
      } while (!this.zzxs.zzj(l));
      this.zzxs.start();
      zzq("Connecting to service");
    } while (!this.zzxo.connect());
    zzq("Connected to service");
    this.zzxs.clear();
    onServiceConnected();
  }
  
  private final boolean zzdk()
  {
    int j = 1;
    zzk.zzaf();
    zzcl();
    zzq("Dispatching a batch of local hits");
    int i;
    if (!this.zzxo.isConnected())
    {
      i = 1;
      if (this.zzxm.zzfb()) {
        break label60;
      }
    }
    for (;;)
    {
      if ((i == 0) || (j == 0)) {
        break label65;
      }
      zzq("No network or service available. Will retry later");
      return false;
      i = 0;
      break;
      label60:
      j = 0;
    }
    label65:
    long l3 = Math.max(zzbx.zzeb(), zzbx.zzec());
    ArrayList localArrayList = new ArrayList();
    long l1 = 0L;
    try
    {
      for (;;)
      {
        this.zzxl.beginTransaction();
        localArrayList.clear();
        try
        {
          localList = this.zzxl.zzd(l3);
          if (localList.isEmpty())
          {
            zzq("Store is empty, nothing to dispatch");
            zzdo();
            try
            {
              this.zzxl.setTransactionSuccessful();
              this.zzxl.endTransaction();
              return false;
            }
            catch (SQLiteException localSQLiteException1)
            {
              zze("Failed to commit local dispatch transaction", localSQLiteException1);
              zzdo();
              return false;
            }
          }
          zza("Hits loaded from store. count", Integer.valueOf(localList.size()));
          localObject2 = localList.iterator();
          for (;;)
          {
            if (((Iterator)localObject2).hasNext()) {
              if (((zzck)((Iterator)localObject2).next()).zzeq() == l1)
              {
                zzd("Database contains successfully uploaded hit", Long.valueOf(l1), Integer.valueOf(localList.size()));
                zzdo();
                try
                {
                  this.zzxl.setTransactionSuccessful();
                  this.zzxl.endTransaction();
                  return false;
                }
                catch (SQLiteException localSQLiteException2)
                {
                  zze("Failed to commit local dispatch transaction", localSQLiteException2);
                  zzdo();
                  return false;
                }
              }
            }
          }
        }
        catch (SQLiteException localSQLiteException3)
        {
          List localList;
          Object localObject2;
          zzd("Failed to read hits from persisted store", localSQLiteException3);
          zzdo();
          try
          {
            this.zzxl.setTransactionSuccessful();
            this.zzxl.endTransaction();
            return false;
          }
          catch (SQLiteException localSQLiteException4)
          {
            zze("Failed to commit local dispatch transaction", localSQLiteException4);
            zzdo();
            return false;
          }
          long l2 = l1;
          if (this.zzxo.isConnected())
          {
            zzq("Service connected, sending hits to the service");
            for (;;)
            {
              l2 = l1;
              if (!localList.isEmpty())
              {
                localObject2 = (zzck)localList.get(0);
                l2 = l1;
                if (this.zzxo.zzb((zzck)localObject2))
                {
                  l1 = Math.max(l1, ((zzck)localObject2).zzeq());
                  localList.remove(localObject2);
                  zzb("Hit sent do device AnalyticsService for delivery", localObject2);
                  try
                  {
                    this.zzxl.zze(((zzck)localObject2).zzeq());
                    localSQLiteException4.add(Long.valueOf(((zzck)localObject2).zzeq()));
                  }
                  catch (SQLiteException localSQLiteException5)
                  {
                    zze("Failed to remove hit that was send for delivery", localSQLiteException5);
                    zzdo();
                    try
                    {
                      this.zzxl.setTransactionSuccessful();
                      this.zzxl.endTransaction();
                      return false;
                    }
                    catch (SQLiteException localSQLiteException6)
                    {
                      zze("Failed to commit local dispatch transaction", localSQLiteException6);
                      zzdo();
                      return false;
                    }
                  }
                }
              }
            }
          }
          l1 = l2;
          if (this.zzxm.zzfb())
          {
            localList = this.zzxm.zzb(localList);
            localObject2 = localList.iterator();
            for (l1 = l2; ((Iterator)localObject2).hasNext(); l1 = Math.max(l1, ((Long)((Iterator)localObject2).next()).longValue())) {}
          }
          try
          {
            this.zzxl.zza(localList);
            localSQLiteException6.addAll(localList);
            boolean bool = localSQLiteException6.isEmpty();
            if (bool) {
              try
              {
                this.zzxl.setTransactionSuccessful();
                this.zzxl.endTransaction();
                return false;
              }
              catch (SQLiteException localSQLiteException7)
              {
                zze("Failed to commit local dispatch transaction", localSQLiteException7);
                zzdo();
                return false;
              }
            }
          }
          catch (SQLiteException localSQLiteException8)
          {
            zze("Failed to remove successfully uploaded hits", localSQLiteException8);
            zzdo();
            try
            {
              this.zzxl.setTransactionSuccessful();
              this.zzxl.endTransaction();
              return false;
            }
            catch (SQLiteException localSQLiteException9)
            {
              zze("Failed to commit local dispatch transaction", localSQLiteException9);
              zzdo();
              return false;
            }
            try
            {
              this.zzxl.setTransactionSuccessful();
              this.zzxl.endTransaction();
            }
            catch (SQLiteException localSQLiteException10)
            {
              zze("Failed to commit local dispatch transaction", localSQLiteException10);
              zzdo();
              return false;
            }
          }
        }
      }
      return false;
    }
    finally
    {
      try
      {
        this.zzxl.setTransactionSuccessful();
        this.zzxl.endTransaction();
        throw ((Throwable)localObject1);
      }
      catch (SQLiteException localSQLiteException11)
      {
        zze("Failed to commit local dispatch transaction", localSQLiteException11);
        zzdo();
      }
    }
  }
  
  private final void zzdn()
  {
    zzcc localzzcc = zzcd();
    if (!localzzcc.zzem()) {}
    long l;
    do
    {
      do
      {
        return;
      } while (localzzcc.zzej());
      l = zzdc();
    } while ((l == 0L) || (Math.abs(zzbx().currentTimeMillis() - l) > ((Long)zzcf.zzzi.get()).longValue()));
    zza("Dispatch alarm scheduled (ms)", Long.valueOf(zzbx.zzea()));
    localzzcc.zzen();
  }
  
  private final void zzdo()
  {
    if (this.zzxq.zzej()) {
      zzq("All hits dispatched or no network/service. Going to power save mode");
    }
    this.zzxq.cancel();
    zzcc localzzcc = zzcd();
    if (localzzcc.zzej()) {
      localzzcc.cancel();
    }
  }
  
  private final long zzdp()
  {
    long l;
    if (this.zzxp != Long.MIN_VALUE) {
      l = this.zzxp;
    }
    do
    {
      return l;
      l = ((Long)zzcf.zzzd.get()).longValue();
      localzzdh = zzce();
      localzzdh.zzcl();
    } while (!localzzdh.zzacr);
    zzdh localzzdh = zzce();
    localzzdh.zzcl();
    return localzzdh.zzaat * 1000L;
  }
  
  private final void zzdq()
  {
    zzcl();
    zzk.zzaf();
    this.zzxu = true;
    this.zzxo.disconnect();
    zzdm();
  }
  
  private final boolean zzx(String paramString)
  {
    return Wrappers.packageManager(getContext()).checkCallingOrSelfPermission(paramString) == 0;
  }
  
  protected final void onServiceConnected()
  {
    zzk.zzaf();
    zzk.zzaf();
    zzcl();
    if (!zzbx.zzdx()) {
      zzt("Service client disabled. Can't dispatch local hits to device AnalyticsService");
    }
    if (!this.zzxo.isConnected()) {
      zzq("Service not connected");
    }
    while (this.zzxl.isEmpty()) {
      return;
    }
    zzq("Dispatching local hits to device AnalyticsService");
    for (;;)
    {
      try
      {
        List localList = this.zzxl.zzd(zzbx.zzeb());
        if (!localList.isEmpty()) {
          break label117;
        }
        zzdm();
        return;
      }
      catch (SQLiteException localSQLiteException1)
      {
        zze("Failed to read hits from store", localSQLiteException1);
        zzdo();
        return;
      }
      label98:
      Object localObject;
      localSQLiteException1.remove(localObject);
      try
      {
        this.zzxl.zze(((zzck)localObject).zzeq());
        label117:
        if (!localSQLiteException1.isEmpty())
        {
          localObject = (zzck)localSQLiteException1.get(0);
          if (this.zzxo.zzb((zzck)localObject)) {
            break label98;
          }
          zzdm();
          return;
        }
      }
      catch (SQLiteException localSQLiteException2)
      {
        zze("Failed to remove hit that was send for delivery", localSQLiteException2);
        zzdo();
      }
    }
  }
  
  final void start()
  {
    zzcl();
    if (!this.started) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "Analytics backend already started");
      this.started = true;
      zzca().zza(new zzbl(this));
      return;
    }
  }
  
  /* Error */
  public final long zza(zzaz paramzzaz, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 34	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual 246	com/google/android/gms/internal/measurement/zzau:zzcl	()V
    //   9: invokestatic 243	com/google/android/gms/analytics/zzk:zzaf	()V
    //   12: aload_0
    //   13: getfield 48	com/google/android/gms/internal/measurement/zzbi:zzxl	Lcom/google/android/gms/internal/measurement/zzbf;
    //   16: invokevirtual 357	com/google/android/gms/internal/measurement/zzbf:beginTransaction	()V
    //   19: aload_0
    //   20: getfield 48	com/google/android/gms/internal/measurement/zzbi:zzxl	Lcom/google/android/gms/internal/measurement/zzbf;
    //   23: astore 6
    //   25: aload_1
    //   26: invokevirtual 560	com/google/android/gms/internal/measurement/zzaz:zzcs	()J
    //   29: lstore 4
    //   31: aload_1
    //   32: invokevirtual 563	com/google/android/gms/internal/measurement/zzaz:zzbd	()Ljava/lang/String;
    //   35: astore 7
    //   37: aload 7
    //   39: invokestatic 567	com/google/android/gms/common/internal/Preconditions:checkNotEmpty	(Ljava/lang/String;)Ljava/lang/String;
    //   42: pop
    //   43: aload 6
    //   45: invokevirtual 246	com/google/android/gms/internal/measurement/zzau:zzcl	()V
    //   48: invokestatic 243	com/google/android/gms/analytics/zzk:zzaf	()V
    //   51: aload 6
    //   53: invokevirtual 571	com/google/android/gms/internal/measurement/zzbf:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   56: ldc_w 573
    //   59: ldc_w 575
    //   62: iconst_2
    //   63: anewarray 170	java/lang/String
    //   66: dup
    //   67: iconst_0
    //   68: lload 4
    //   70: invokestatic 578	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   73: aastore
    //   74: dup
    //   75: iconst_1
    //   76: aload 7
    //   78: aastore
    //   79: invokevirtual 584	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   82: istore_3
    //   83: iload_3
    //   84: ifle +15 -> 99
    //   87: aload 6
    //   89: ldc_w 586
    //   92: iload_3
    //   93: invokestatic 390	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   96: invokevirtual 392	com/google/android/gms/internal/measurement/zzat:zza	(Ljava/lang/String;Ljava/lang/Object;)V
    //   99: aload_0
    //   100: getfield 48	com/google/android/gms/internal/measurement/zzbi:zzxl	Lcom/google/android/gms/internal/measurement/zzbf;
    //   103: aload_1
    //   104: invokevirtual 560	com/google/android/gms/internal/measurement/zzaz:zzcs	()J
    //   107: aload_1
    //   108: invokevirtual 563	com/google/android/gms/internal/measurement/zzaz:zzbd	()Ljava/lang/String;
    //   111: aload_1
    //   112: invokevirtual 99	com/google/android/gms/internal/measurement/zzaz:zzct	()Ljava/lang/String;
    //   115: invokevirtual 589	com/google/android/gms/internal/measurement/zzbf:zza	(JLjava/lang/String;Ljava/lang/String;)J
    //   118: lstore 4
    //   120: aload_1
    //   121: lconst_1
    //   122: lload 4
    //   124: ladd
    //   125: invokevirtual 591	com/google/android/gms/internal/measurement/zzaz:zzb	(J)V
    //   128: aload_0
    //   129: getfield 48	com/google/android/gms/internal/measurement/zzbi:zzxl	Lcom/google/android/gms/internal/measurement/zzbf;
    //   132: astore 7
    //   134: aload_1
    //   135: invokestatic 34	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   138: pop
    //   139: aload 7
    //   141: invokevirtual 246	com/google/android/gms/internal/measurement/zzau:zzcl	()V
    //   144: invokestatic 243	com/google/android/gms/analytics/zzk:zzaf	()V
    //   147: aload 7
    //   149: invokevirtual 571	com/google/android/gms/internal/measurement/zzbf:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   152: astore 8
    //   154: aload_1
    //   155: invokevirtual 142	com/google/android/gms/internal/measurement/zzaz:zzcw	()Ljava/util/Map;
    //   158: astore 9
    //   160: aload 9
    //   162: invokestatic 34	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   165: pop
    //   166: new 593	android/net/Uri$Builder
    //   169: dup
    //   170: invokespecial 594	android/net/Uri$Builder:<init>	()V
    //   173: astore 6
    //   175: aload 9
    //   177: invokeinterface 148 1 0
    //   182: invokeinterface 154 1 0
    //   187: astore 9
    //   189: aload 9
    //   191: invokeinterface 159 1 0
    //   196: ifeq +64 -> 260
    //   199: aload 9
    //   201: invokeinterface 163 1 0
    //   206: checkcast 165	java/util/Map$Entry
    //   209: astore 10
    //   211: aload 6
    //   213: aload 10
    //   215: invokeinterface 168 1 0
    //   220: checkcast 170	java/lang/String
    //   223: aload 10
    //   225: invokeinterface 173 1 0
    //   230: checkcast 170	java/lang/String
    //   233: invokevirtual 598	android/net/Uri$Builder:appendQueryParameter	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   236: pop
    //   237: goto -48 -> 189
    //   240: astore_1
    //   241: aload_0
    //   242: ldc_w 600
    //   245: aload_1
    //   246: invokevirtual 254	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   249: aload_0
    //   250: getfield 48	com/google/android/gms/internal/measurement/zzbi:zzxl	Lcom/google/android/gms/internal/measurement/zzbf;
    //   253: invokevirtual 377	com/google/android/gms/internal/measurement/zzbf:endTransaction	()V
    //   256: ldc2_w 601
    //   259: lreturn
    //   260: aload 6
    //   262: invokevirtual 606	android/net/Uri$Builder:build	()Landroid/net/Uri;
    //   265: invokevirtual 611	android/net/Uri:getEncodedQuery	()Ljava/lang/String;
    //   268: astore 6
    //   270: aload 6
    //   272: ifnonnull +146 -> 418
    //   275: ldc_w 613
    //   278: astore 6
    //   280: new 615	android/content/ContentValues
    //   283: dup
    //   284: invokespecial 616	android/content/ContentValues:<init>	()V
    //   287: astore 9
    //   289: aload 9
    //   291: ldc_w 618
    //   294: aload_1
    //   295: invokevirtual 560	com/google/android/gms/internal/measurement/zzaz:zzcs	()J
    //   298: invokestatic 403	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   301: invokevirtual 622	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   304: aload 9
    //   306: ldc_w 624
    //   309: aload_1
    //   310: invokevirtual 563	com/google/android/gms/internal/measurement/zzaz:zzbd	()Ljava/lang/String;
    //   313: invokevirtual 626	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   316: aload 9
    //   318: ldc_w 628
    //   321: aload_1
    //   322: invokevirtual 99	com/google/android/gms/internal/measurement/zzaz:zzct	()Ljava/lang/String;
    //   325: invokevirtual 626	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   328: aload_1
    //   329: invokevirtual 106	com/google/android/gms/internal/measurement/zzaz:zzcu	()Z
    //   332: ifeq +89 -> 421
    //   335: iconst_1
    //   336: istore_3
    //   337: aload 9
    //   339: ldc_w 630
    //   342: iload_3
    //   343: invokestatic 390	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   346: invokevirtual 633	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   349: aload 9
    //   351: ldc_w 635
    //   354: aload_1
    //   355: invokevirtual 638	com/google/android/gms/internal/measurement/zzaz:zzcv	()J
    //   358: invokestatic 403	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   361: invokevirtual 622	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   364: aload 9
    //   366: ldc_w 640
    //   369: aload 6
    //   371: invokevirtual 626	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   374: aload 8
    //   376: ldc_w 573
    //   379: aconst_null
    //   380: aload 9
    //   382: iconst_5
    //   383: invokevirtual 644	android/database/sqlite/SQLiteDatabase:insertWithOnConflict	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    //   386: ldc2_w 601
    //   389: lcmp
    //   390: ifne +11 -> 401
    //   393: aload 7
    //   395: ldc_w 646
    //   398: invokevirtual 649	com/google/android/gms/internal/measurement/zzat:zzu	(Ljava/lang/String;)V
    //   401: aload_0
    //   402: getfield 48	com/google/android/gms/internal/measurement/zzbi:zzxl	Lcom/google/android/gms/internal/measurement/zzbf;
    //   405: invokevirtual 374	com/google/android/gms/internal/measurement/zzbf:setTransactionSuccessful	()V
    //   408: aload_0
    //   409: getfield 48	com/google/android/gms/internal/measurement/zzbi:zzxl	Lcom/google/android/gms/internal/measurement/zzbf;
    //   412: invokevirtual 377	com/google/android/gms/internal/measurement/zzbf:endTransaction	()V
    //   415: lload 4
    //   417: lreturn
    //   418: goto -138 -> 280
    //   421: iconst_0
    //   422: istore_3
    //   423: goto -86 -> 337
    //   426: astore_1
    //   427: aload 7
    //   429: ldc_w 651
    //   432: aload_1
    //   433: invokevirtual 254	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   436: goto -35 -> 401
    //   439: astore_1
    //   440: aload_0
    //   441: getfield 48	com/google/android/gms/internal/measurement/zzbi:zzxl	Lcom/google/android/gms/internal/measurement/zzbf;
    //   444: invokevirtual 377	com/google/android/gms/internal/measurement/zzbf:endTransaction	()V
    //   447: aload_1
    //   448: athrow
    //   449: astore_1
    //   450: aload_0
    //   451: ldc_w 653
    //   454: aload_1
    //   455: invokevirtual 254	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   458: goto -43 -> 415
    //   461: astore_1
    //   462: aload_0
    //   463: ldc_w 653
    //   466: aload_1
    //   467: invokevirtual 254	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   470: goto -214 -> 256
    //   473: astore 6
    //   475: aload_0
    //   476: ldc_w 653
    //   479: aload 6
    //   481: invokevirtual 254	com/google/android/gms/internal/measurement/zzat:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   484: goto -37 -> 447
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	487	0	this	zzbi
    //   0	487	1	paramzzaz	zzaz
    //   0	487	2	paramBoolean	boolean
    //   82	341	3	i	int
    //   29	387	4	l	long
    //   23	347	6	localObject1	Object
    //   473	7	6	localSQLiteException	SQLiteException
    //   35	393	7	localObject2	Object
    //   152	223	8	localSQLiteDatabase	SQLiteDatabase
    //   158	223	9	localObject3	Object
    //   209	15	10	localEntry	Map.Entry
    // Exception table:
    //   from	to	target	type
    //   12	83	240	android/database/sqlite/SQLiteException
    //   87	99	240	android/database/sqlite/SQLiteException
    //   99	189	240	android/database/sqlite/SQLiteException
    //   189	237	240	android/database/sqlite/SQLiteException
    //   260	270	240	android/database/sqlite/SQLiteException
    //   280	335	240	android/database/sqlite/SQLiteException
    //   337	374	240	android/database/sqlite/SQLiteException
    //   401	408	240	android/database/sqlite/SQLiteException
    //   427	436	240	android/database/sqlite/SQLiteException
    //   374	401	426	android/database/sqlite/SQLiteException
    //   12	83	439	finally
    //   87	99	439	finally
    //   99	189	439	finally
    //   189	237	439	finally
    //   241	249	439	finally
    //   260	270	439	finally
    //   280	335	439	finally
    //   337	374	439	finally
    //   374	401	439	finally
    //   401	408	439	finally
    //   427	436	439	finally
    //   408	415	449	android/database/sqlite/SQLiteException
    //   249	256	461	android/database/sqlite/SQLiteException
    //   440	447	473	android/database/sqlite/SQLiteException
  }
  
  public final void zza(zzck paramzzck)
  {
    Preconditions.checkNotNull(paramzzck);
    zzk.zzaf();
    zzcl();
    Object localObject1;
    if (this.zzxu)
    {
      zzr("Hit delivery not possible. Missing network permissions. See http://goo.gl/8Rd3yj for instructions");
      if (TextUtils.isEmpty(paramzzck.zzev())) {
        break label72;
      }
      localObject1 = paramzzck;
    }
    for (;;)
    {
      zzdj();
      if (!this.zzxo.zzb((zzck)localObject1)) {
        break label208;
      }
      zzr("Hit sent to the device AnalyticsService for delivery");
      return;
      zza("Delivering hit", paramzzck);
      break;
      label72:
      Object localObject2 = zzcf().zzfk().zzfm();
      localObject1 = paramzzck;
      if (localObject2 != null)
      {
        localObject1 = (Long)((Pair)localObject2).second;
        localObject2 = (String)((Pair)localObject2).first;
        localObject1 = String.valueOf(localObject1);
        localObject1 = String.valueOf(localObject1).length() + 1 + String.valueOf(localObject2).length() + (String)localObject1 + ":" + (String)localObject2;
        localObject2 = new HashMap(paramzzck.zzcw());
        ((Map)localObject2).put("_m", localObject1);
        localObject1 = new zzck(this, (Map)localObject2, paramzzck.zzer(), paramzzck.zzet(), paramzzck.zzeq(), paramzzck.zzep(), paramzzck.zzes());
      }
    }
    try
    {
      label208:
      this.zzxl.zzc((zzck)localObject1);
      zzdm();
      return;
    }
    catch (SQLiteException paramzzck)
    {
      zze("Delivery failed to save hit to a database", paramzzck);
      zzby().zza((zzck)localObject1, "deliver: failed to insert hit to database");
    }
  }
  
  protected final void zzag()
  {
    this.zzxl.zzq();
    this.zzxm.zzq();
    this.zzxo.zzq();
  }
  
  protected final void zzb(zzaz paramzzaz)
  {
    zzk.zzaf();
    zzb("Sending first hit to property", paramzzaz.zzct());
    if (zzcf().zzfg().zzj(zzbx.zzeh())) {}
    do
    {
      return;
      localObject = zzcf().zzfj();
    } while (TextUtils.isEmpty((CharSequence)localObject));
    Object localObject = zzdg.zza(zzby(), (String)localObject);
    zzb("Found relevant installation campaign", localObject);
    zza(paramzzaz, (zzy)localObject);
  }
  
  public final void zzb(zzcd paramzzcd)
  {
    long l2 = this.zzxt;
    zzk.zzaf();
    zzcl();
    long l1 = -1L;
    long l3 = zzcf().zzfh();
    if (l3 != 0L) {
      l1 = Math.abs(zzbx().currentTimeMillis() - l3);
    }
    zzb("Dispatching local hits. Elapsed time since last dispatch (ms)", Long.valueOf(l1));
    zzdj();
    try
    {
      zzdk();
      zzcf().zzfi();
      zzdm();
      if (paramzzcd != null) {
        paramzzcd.zza(null);
      }
      if (this.zzxt != l2) {
        this.zzxn.zzfa();
      }
      return;
    }
    catch (Exception localException)
    {
      do
      {
        zze("Local dispatch failed", localException);
        zzcf().zzfi();
        zzdm();
      } while (paramzzcd == null);
      paramzzcd.zza(localException);
    }
  }
  
  public final void zzbr()
  {
    zzk.zzaf();
    zzcl();
    zzq("Delete all hits from local store");
    try
    {
      zzbf localzzbf = this.zzxl;
      zzk.zzaf();
      localzzbf.zzcl();
      localzzbf.getWritableDatabase().delete("hits2", null, null);
      localzzbf = this.zzxl;
      zzk.zzaf();
      localzzbf.zzcl();
      localzzbf.getWritableDatabase().delete("properties", null, null);
      zzdm();
      zzdj();
      if (this.zzxo.zzcx()) {
        zzq("Device service unavailable. Can't clear hits stored on the device service.");
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        zzd("Failed to delete hits from store", localSQLiteException);
      }
    }
  }
  
  final void zzbv()
  {
    zzk.zzaf();
    this.zzxt = zzbx().currentTimeMillis();
  }
  
  protected final void zzdg()
  {
    zzcl();
    zzk.zzaf();
    Context localContext = zzbw().getContext();
    if (!zzcw.zza(localContext))
    {
      zzt("AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions.");
      if (!CampaignTrackingReceiver.zza(localContext)) {
        zzt("CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
      }
      zzcf().zzff();
      if (!zzx("android.permission.ACCESS_NETWORK_STATE"))
      {
        zzu("Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
        zzdq();
      }
      if (!zzx("android.permission.INTERNET"))
      {
        zzu("Missing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
        zzdq();
      }
      if (!zzcx.zze(getContext())) {
        break label153;
      }
      zzq("AnalyticsService registered in the app manifest and enabled");
    }
    for (;;)
    {
      if ((!this.zzxu) && (!this.zzxl.isEmpty())) {
        zzdj();
      }
      zzdm();
      return;
      if (zzcx.zze(localContext)) {
        break;
      }
      zzu("AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions.");
      break;
      label153:
      zzt("AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions.");
    }
  }
  
  public final void zzdl()
  {
    zzk.zzaf();
    zzcl();
    zzr("Sync dispatching local hits");
    long l = this.zzxt;
    zzdj();
    try
    {
      zzdk();
      zzcf().zzfi();
      zzdm();
      if (this.zzxt != l) {
        this.zzxn.zzfa();
      }
      return;
    }
    catch (Exception localException)
    {
      zze("Sync local dispatch failed", localException);
      zzdm();
    }
  }
  
  public final void zzdm()
  {
    zzk.zzaf();
    zzcl();
    if ((!this.zzxu) && (zzdp() > 0L)) {}
    for (int i = 1; i == 0; i = 0)
    {
      this.zzxn.unregister();
      zzdo();
      return;
    }
    if (this.zzxl.isEmpty())
    {
      this.zzxn.unregister();
      zzdo();
      return;
    }
    boolean bool;
    long l2;
    long l1;
    if (!((Boolean)zzcf.zzaae.get()).booleanValue())
    {
      this.zzxn.zzey();
      bool = this.zzxn.isConnected();
      if (!bool) {
        break label232;
      }
      zzdn();
      l2 = zzdp();
      l1 = zzcf().zzfh();
      if (l1 == 0L) {
        break label211;
      }
      l1 = l2 - Math.abs(zzbx().currentTimeMillis() - l1);
      if (l1 <= 0L) {
        break label199;
      }
    }
    for (;;)
    {
      zza("Dispatch scheduled (ms)", Long.valueOf(l1));
      if (!this.zzxq.zzej()) {
        break label223;
      }
      l1 = Math.max(1L, l1 + this.zzxq.zzei());
      this.zzxq.zzi(l1);
      return;
      bool = true;
      break;
      label199:
      l1 = Math.min(zzbx.zzdz(), l2);
      continue;
      label211:
      l1 = Math.min(zzbx.zzdz(), l2);
    }
    label223:
    this.zzxq.zzh(l1);
    return;
    label232:
    zzdo();
    zzdn();
  }
  
  public final void zzg(long paramLong)
  {
    zzk.zzaf();
    zzcl();
    long l = paramLong;
    if (paramLong < 0L) {
      l = 0L;
    }
    this.zzxp = l;
    zzdm();
  }
  
  public final void zzy(String paramString)
  {
    Preconditions.checkNotEmpty(paramString);
    zzk.zzaf();
    zzy localzzy = zzdg.zza(zzby(), paramString);
    if (localzzy == null) {
      zzd("Parsing failed. Ignoring invalid campaign data", paramString);
    }
    for (;;)
    {
      return;
      String str = zzcf().zzfj();
      if (paramString.equals(str))
      {
        zzt("Ignoring duplicate install campaign");
        return;
      }
      if (!TextUtils.isEmpty(str))
      {
        zzd("Ignoring multiple install campaigns. original, new", str, paramString);
        return;
      }
      zzcf().zzac(paramString);
      if (zzcf().zzfg().zzj(zzbx.zzeh()))
      {
        zzd("Campaign received too late, ignoring", localzzy);
        return;
      }
      zzb("Received installation campaign", localzzy);
      paramString = this.zzxl.zzf(0L).iterator();
      while (paramString.hasNext()) {
        zza((zzaz)paramString.next(), localzzy);
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzbi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */