package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

public abstract interface zzu
  extends IInterface
{
  public abstract Bundle zza(Bundle paramBundle)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */