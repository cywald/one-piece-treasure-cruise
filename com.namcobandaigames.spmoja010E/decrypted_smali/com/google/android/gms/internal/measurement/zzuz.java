package com.google.android.gms.internal.measurement;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class zzuz
{
  private static volatile boolean zzbvj = false;
  private static final Class<?> zzbvk = zzvn();
  private static volatile zzuz zzbvl;
  static final zzuz zzbvm = new zzuz(true);
  private final Map<zza, zzvm.zzd<?, ?>> zzbvn;
  
  zzuz()
  {
    this.zzbvn = new HashMap();
  }
  
  private zzuz(boolean paramBoolean)
  {
    this.zzbvn = Collections.emptyMap();
  }
  
  static zzuz zzvm()
  {
    return zzvk.zzd(zzuz.class);
  }
  
  private static Class<?> zzvn()
  {
    try
    {
      Class localClass = Class.forName("com.google.protobuf.Extension");
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException) {}
    return null;
  }
  
  public static zzuz zzvo()
  {
    return zzuy.zzvl();
  }
  
  public static zzuz zzvp()
  {
    Object localObject = zzbvl;
    if (localObject == null) {
      try
      {
        zzuz localzzuz2 = zzbvl;
        localObject = localzzuz2;
        if (localzzuz2 == null)
        {
          localObject = zzuy.zzvm();
          zzbvl = (zzuz)localObject;
        }
        return (zzuz)localObject;
      }
      finally {}
    }
    return localzzuz1;
  }
  
  public final <ContainingType extends zzwt> zzvm.zzd<ContainingType, ?> zza(ContainingType paramContainingType, int paramInt)
  {
    return (zzvm.zzd)this.zzbvn.get(new zza(paramContainingType, paramInt));
  }
  
  static final class zza
  {
    private final int number;
    private final Object object;
    
    zza(Object paramObject, int paramInt)
    {
      this.object = paramObject;
      this.number = paramInt;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof zza)) {}
      do
      {
        return false;
        paramObject = (zza)paramObject;
      } while ((this.object != ((zza)paramObject).object) || (this.number != ((zza)paramObject).number));
      return true;
    }
    
    public final int hashCode()
    {
      return System.identityHashCode(this.object) * 65535 + this.number;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */