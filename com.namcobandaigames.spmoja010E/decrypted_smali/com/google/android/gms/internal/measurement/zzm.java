package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzm
  extends zzza<zzm>
{
  private static volatile zzm[] zzpi;
  public int[] zzpj = zzzj.zzcax;
  public int[] zzpk = zzzj.zzcax;
  public int[] zzpl = zzzj.zzcax;
  public int[] zzpm = zzzj.zzcax;
  public int[] zzpn = zzzj.zzcax;
  public int[] zzpo = zzzj.zzcax;
  public int[] zzpp = zzzj.zzcax;
  public int[] zzpq = zzzj.zzcax;
  public int[] zzpr = zzzj.zzcax;
  public int[] zzps = zzzj.zzcax;
  
  public zzm()
  {
    this.zzcfc = null;
    this.zzcfm = -1;
  }
  
  public static zzm[] zzi()
  {
    if (zzpi == null) {}
    synchronized (zzze.zzcfl)
    {
      if (zzpi == null) {
        zzpi = new zzm[0];
      }
      return zzpi;
    }
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzm)) {
        return false;
      }
      paramObject = (zzm)paramObject;
      if (!zzze.equals(this.zzpj, ((zzm)paramObject).zzpj)) {
        return false;
      }
      if (!zzze.equals(this.zzpk, ((zzm)paramObject).zzpk)) {
        return false;
      }
      if (!zzze.equals(this.zzpl, ((zzm)paramObject).zzpl)) {
        return false;
      }
      if (!zzze.equals(this.zzpm, ((zzm)paramObject).zzpm)) {
        return false;
      }
      if (!zzze.equals(this.zzpn, ((zzm)paramObject).zzpn)) {
        return false;
      }
      if (!zzze.equals(this.zzpo, ((zzm)paramObject).zzpo)) {
        return false;
      }
      if (!zzze.equals(this.zzpp, ((zzm)paramObject).zzpp)) {
        return false;
      }
      if (!zzze.equals(this.zzpq, ((zzm)paramObject).zzpq)) {
        return false;
      }
      if (!zzze.equals(this.zzpr, ((zzm)paramObject).zzpr)) {
        return false;
      }
      if (!zzze.equals(this.zzps, ((zzm)paramObject).zzps)) {
        return false;
      }
      if ((this.zzcfc != null) && (!this.zzcfc.isEmpty())) {
        break;
      }
    } while ((((zzm)paramObject).zzcfc == null) || (((zzm)paramObject).zzcfc.isEmpty()));
    return false;
    return this.zzcfc.equals(((zzm)paramObject).zzcfc);
  }
  
  public final int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = zzze.hashCode(this.zzpj);
    int m = zzze.hashCode(this.zzpk);
    int n = zzze.hashCode(this.zzpl);
    int i1 = zzze.hashCode(this.zzpm);
    int i2 = zzze.hashCode(this.zzpn);
    int i3 = zzze.hashCode(this.zzpo);
    int i4 = zzze.hashCode(this.zzpp);
    int i5 = zzze.hashCode(this.zzpq);
    int i6 = zzze.hashCode(this.zzpr);
    int i7 = zzze.hashCode(this.zzps);
    if ((this.zzcfc == null) || (this.zzcfc.isEmpty())) {}
    for (int i = 0;; i = this.zzcfc.hashCode()) {
      return i + (((((((((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6) * 31 + i7) * 31;
    }
  }
  
  public final void zza(zzyy paramzzyy)
    throws IOException
  {
    int j = 0;
    int i;
    if ((this.zzpj != null) && (this.zzpj.length > 0))
    {
      i = 0;
      while (i < this.zzpj.length)
      {
        paramzzyy.zzd(1, this.zzpj[i]);
        i += 1;
      }
    }
    if ((this.zzpk != null) && (this.zzpk.length > 0))
    {
      i = 0;
      while (i < this.zzpk.length)
      {
        paramzzyy.zzd(2, this.zzpk[i]);
        i += 1;
      }
    }
    if ((this.zzpl != null) && (this.zzpl.length > 0))
    {
      i = 0;
      while (i < this.zzpl.length)
      {
        paramzzyy.zzd(3, this.zzpl[i]);
        i += 1;
      }
    }
    if ((this.zzpm != null) && (this.zzpm.length > 0))
    {
      i = 0;
      while (i < this.zzpm.length)
      {
        paramzzyy.zzd(4, this.zzpm[i]);
        i += 1;
      }
    }
    if ((this.zzpn != null) && (this.zzpn.length > 0))
    {
      i = 0;
      while (i < this.zzpn.length)
      {
        paramzzyy.zzd(5, this.zzpn[i]);
        i += 1;
      }
    }
    if ((this.zzpo != null) && (this.zzpo.length > 0))
    {
      i = 0;
      while (i < this.zzpo.length)
      {
        paramzzyy.zzd(6, this.zzpo[i]);
        i += 1;
      }
    }
    if ((this.zzpp != null) && (this.zzpp.length > 0))
    {
      i = 0;
      while (i < this.zzpp.length)
      {
        paramzzyy.zzd(7, this.zzpp[i]);
        i += 1;
      }
    }
    if ((this.zzpq != null) && (this.zzpq.length > 0))
    {
      i = 0;
      while (i < this.zzpq.length)
      {
        paramzzyy.zzd(8, this.zzpq[i]);
        i += 1;
      }
    }
    if ((this.zzpr != null) && (this.zzpr.length > 0))
    {
      i = 0;
      while (i < this.zzpr.length)
      {
        paramzzyy.zzd(9, this.zzpr[i]);
        i += 1;
      }
    }
    if ((this.zzps != null) && (this.zzps.length > 0))
    {
      i = j;
      while (i < this.zzps.length)
      {
        paramzzyy.zzd(10, this.zzps[i]);
        i += 1;
      }
    }
    super.zza(paramzzyy);
  }
  
  protected final int zzf()
  {
    int m = 0;
    int k = super.zzf();
    int i;
    if ((this.zzpj != null) && (this.zzpj.length > 0))
    {
      i = 0;
      j = 0;
      while (i < this.zzpj.length)
      {
        j += zzyy.zzbc(this.zzpj[i]);
        i += 1;
      }
    }
    for (int j = k + j + this.zzpj.length * 1;; j = k)
    {
      i = j;
      if (this.zzpk != null)
      {
        i = j;
        if (this.zzpk.length > 0)
        {
          i = 0;
          k = 0;
          while (i < this.zzpk.length)
          {
            k += zzyy.zzbc(this.zzpk[i]);
            i += 1;
          }
          i = j + k + this.zzpk.length * 1;
        }
      }
      j = i;
      if (this.zzpl != null)
      {
        j = i;
        if (this.zzpl.length > 0)
        {
          j = 0;
          k = 0;
          while (j < this.zzpl.length)
          {
            k += zzyy.zzbc(this.zzpl[j]);
            j += 1;
          }
          j = i + k + this.zzpl.length * 1;
        }
      }
      i = j;
      if (this.zzpm != null)
      {
        i = j;
        if (this.zzpm.length > 0)
        {
          i = 0;
          k = 0;
          while (i < this.zzpm.length)
          {
            k += zzyy.zzbc(this.zzpm[i]);
            i += 1;
          }
          i = j + k + this.zzpm.length * 1;
        }
      }
      j = i;
      if (this.zzpn != null)
      {
        j = i;
        if (this.zzpn.length > 0)
        {
          j = 0;
          k = 0;
          while (j < this.zzpn.length)
          {
            k += zzyy.zzbc(this.zzpn[j]);
            j += 1;
          }
          j = i + k + this.zzpn.length * 1;
        }
      }
      i = j;
      if (this.zzpo != null)
      {
        i = j;
        if (this.zzpo.length > 0)
        {
          i = 0;
          k = 0;
          while (i < this.zzpo.length)
          {
            k += zzyy.zzbc(this.zzpo[i]);
            i += 1;
          }
          i = j + k + this.zzpo.length * 1;
        }
      }
      j = i;
      if (this.zzpp != null)
      {
        j = i;
        if (this.zzpp.length > 0)
        {
          j = 0;
          k = 0;
          while (j < this.zzpp.length)
          {
            k += zzyy.zzbc(this.zzpp[j]);
            j += 1;
          }
          j = i + k + this.zzpp.length * 1;
        }
      }
      i = j;
      if (this.zzpq != null)
      {
        i = j;
        if (this.zzpq.length > 0)
        {
          i = 0;
          k = 0;
          while (i < this.zzpq.length)
          {
            k += zzyy.zzbc(this.zzpq[i]);
            i += 1;
          }
          i = j + k + this.zzpq.length * 1;
        }
      }
      j = i;
      if (this.zzpr != null)
      {
        j = i;
        if (this.zzpr.length > 0)
        {
          j = 0;
          k = 0;
          while (j < this.zzpr.length)
          {
            k += zzyy.zzbc(this.zzpr[j]);
            j += 1;
          }
          j = i + k + this.zzpr.length * 1;
        }
      }
      i = j;
      if (this.zzps != null)
      {
        i = j;
        if (this.zzps.length > 0)
        {
          k = 0;
          i = m;
          while (i < this.zzps.length)
          {
            k += zzyy.zzbc(this.zzps[i]);
            i += 1;
          }
          i = j + k + this.zzps.length * 1;
        }
      }
      return i;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */