package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Map.Entry;

final class zzvb
  extends zzva<Object>
{
  final Object zza(zzuz paramzzuz, zzwt paramzzwt, int paramInt)
  {
    return paramzzuz.zza(paramzzwt, paramInt);
  }
  
  final <UT, UB> UB zza(zzxi paramzzxi, Object paramObject, zzuz paramzzuz, zzvd<Object> paramzzvd, UB paramUB, zzyb<UT, UB> paramzzyb)
    throws IOException
  {
    throw new NoSuchMethodError();
  }
  
  final void zza(zzud paramzzud, Object paramObject, zzuz paramzzuz, zzvd<Object> paramzzvd)
    throws IOException
  {
    throw new NoSuchMethodError();
  }
  
  final void zza(zzxi paramzzxi, Object paramObject, zzuz paramzzuz, zzvd<Object> paramzzvd)
    throws IOException
  {
    throw new NoSuchMethodError();
  }
  
  final void zza(zzyw paramzzyw, Map.Entry<?, ?> paramEntry)
    throws IOException
  {
    paramEntry.getKey();
    throw new NoSuchMethodError();
  }
  
  final void zza(Object paramObject, zzvd<Object> paramzzvd)
  {
    ((zzvm.zzc)paramObject).zzbys = paramzzvd;
  }
  
  final int zzb(Map.Entry<?, ?> paramEntry)
  {
    paramEntry.getKey();
    throw new NoSuchMethodError();
  }
  
  final boolean zze(zzwt paramzzwt)
  {
    return paramzzwt instanceof zzvm.zzc;
  }
  
  final zzvd<Object> zzs(Object paramObject)
  {
    return ((zzvm.zzc)paramObject).zzbys;
  }
  
  final zzvd<Object> zzt(Object paramObject)
  {
    zzvd localzzvd2 = zzs(paramObject);
    zzvd localzzvd1 = localzzvd2;
    if (localzzvd2.isImmutable())
    {
      localzzvd1 = (zzvd)localzzvd2.clone();
      zza(paramObject, localzzvd1);
    }
    return localzzvd1;
  }
  
  final void zzu(Object paramObject)
  {
    zzs(paramObject).zzsm();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzvb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */