package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class zzuv
  implements zzyw
{
  private final zzut zzbuf;
  
  private zzuv(zzut paramzzut)
  {
    this.zzbuf = ((zzut)zzvo.zza(paramzzut, "output"));
    this.zzbuf.zzbuw = this;
  }
  
  public static zzuv zza(zzut paramzzut)
  {
    if (paramzzut.zzbuw != null) {
      return paramzzut.zzbuw;
    }
    return new zzuv(paramzzut);
  }
  
  public final void zza(int paramInt, double paramDouble)
    throws IOException
  {
    this.zzbuf.zza(paramInt, paramDouble);
  }
  
  public final void zza(int paramInt, float paramFloat)
    throws IOException
  {
    this.zzbuf.zza(paramInt, paramFloat);
  }
  
  public final void zza(int paramInt, long paramLong)
    throws IOException
  {
    this.zzbuf.zza(paramInt, paramLong);
  }
  
  public final void zza(int paramInt, zzud paramzzud)
    throws IOException
  {
    this.zzbuf.zza(paramInt, paramzzud);
  }
  
  public final <K, V> void zza(int paramInt, zzwm<K, V> paramzzwm, Map<K, V> paramMap)
    throws IOException
  {
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      this.zzbuf.zzc(paramInt, 2);
      this.zzbuf.zzay(zzwl.zza(paramzzwm, localEntry.getKey(), localEntry.getValue()));
      zzwl.zza(this.zzbuf, paramzzwm, localEntry.getKey(), localEntry.getValue());
    }
  }
  
  public final void zza(int paramInt, Object paramObject)
    throws IOException
  {
    if ((paramObject instanceof zzud))
    {
      this.zzbuf.zzb(paramInt, (zzud)paramObject);
      return;
    }
    this.zzbuf.zzb(paramInt, (zzwt)paramObject);
  }
  
  public final void zza(int paramInt, Object paramObject, zzxj paramzzxj)
    throws IOException
  {
    this.zzbuf.zza(paramInt, (zzwt)paramObject, paramzzxj);
  }
  
  public final void zza(int paramInt, List<String> paramList)
    throws IOException
  {
    int i = 0;
    if ((paramList instanceof zzwc))
    {
      zzwc localzzwc = (zzwc)paramList;
      i = 0;
      if (i < paramList.size())
      {
        Object localObject = localzzwc.getRaw(i);
        if ((localObject instanceof String)) {
          this.zzbuf.zzb(paramInt, (String)localObject);
        }
        for (;;)
        {
          i += 1;
          break;
          this.zzbuf.zza(paramInt, (zzud)localObject);
        }
      }
    }
    else
    {
      while (i < paramList.size())
      {
        this.zzbuf.zzb(paramInt, (String)paramList.get(i));
        i += 1;
      }
    }
  }
  
  public final void zza(int paramInt, List<?> paramList, zzxj paramzzxj)
    throws IOException
  {
    int i = 0;
    while (i < paramList.size())
    {
      zza(paramInt, paramList.get(i), paramzzxj);
      i += 1;
    }
  }
  
  public final void zza(int paramInt, List<Integer> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzbc(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzax(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzd(paramInt, ((Integer)paramList.get(i)).intValue());
      i += 1;
    }
  }
  
  public final void zzb(int paramInt, long paramLong)
    throws IOException
  {
    this.zzbuf.zzb(paramInt, paramLong);
  }
  
  public final void zzb(int paramInt, Object paramObject, zzxj paramzzxj)
    throws IOException
  {
    zzut localzzut = this.zzbuf;
    paramObject = (zzwt)paramObject;
    localzzut.zzc(paramInt, 3);
    paramzzxj.zza(paramObject, localzzut.zzbuw);
    localzzut.zzc(paramInt, 4);
  }
  
  public final void zzb(int paramInt, String paramString)
    throws IOException
  {
    this.zzbuf.zzb(paramInt, paramString);
  }
  
  public final void zzb(int paramInt, List<zzud> paramList)
    throws IOException
  {
    int i = 0;
    while (i < paramList.size())
    {
      this.zzbuf.zza(paramInt, (zzud)paramList.get(i));
      i += 1;
    }
  }
  
  public final void zzb(int paramInt, List<?> paramList, zzxj paramzzxj)
    throws IOException
  {
    int i = 0;
    while (i < paramList.size())
    {
      zzb(paramInt, paramList.get(i), paramzzxj);
      i += 1;
    }
  }
  
  public final void zzb(int paramInt, List<Integer> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzbf(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzba(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzg(paramInt, ((Integer)paramList.get(i)).intValue());
      i += 1;
    }
  }
  
  public final void zzb(int paramInt, boolean paramBoolean)
    throws IOException
  {
    this.zzbuf.zzb(paramInt, paramBoolean);
  }
  
  public final void zzbk(int paramInt)
    throws IOException
  {
    this.zzbuf.zzc(paramInt, 3);
  }
  
  public final void zzbl(int paramInt)
    throws IOException
  {
    this.zzbuf.zzc(paramInt, 4);
  }
  
  public final void zzc(int paramInt, long paramLong)
    throws IOException
  {
    this.zzbuf.zzc(paramInt, paramLong);
  }
  
  public final void zzc(int paramInt, List<Long> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzay(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzav(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zza(paramInt, ((Long)paramList.get(i)).longValue());
      i += 1;
    }
  }
  
  public final void zzd(int paramInt1, int paramInt2)
    throws IOException
  {
    this.zzbuf.zzd(paramInt1, paramInt2);
  }
  
  public final void zzd(int paramInt, List<Long> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzaz(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzav(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zza(paramInt, ((Long)paramList.get(i)).longValue());
      i += 1;
    }
  }
  
  public final void zze(int paramInt1, int paramInt2)
    throws IOException
  {
    this.zzbuf.zze(paramInt1, paramInt2);
  }
  
  public final void zze(int paramInt, List<Long> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzbb(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzax(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzc(paramInt, ((Long)paramList.get(i)).longValue());
      i += 1;
    }
  }
  
  public final void zzf(int paramInt1, int paramInt2)
    throws IOException
  {
    this.zzbuf.zzf(paramInt1, paramInt2);
  }
  
  public final void zzf(int paramInt, List<Float> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzb(((Float)paramList.get(paramInt)).floatValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zza(((Float)paramList.get(paramInt)).floatValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zza(paramInt, ((Float)paramList.get(i)).floatValue());
      i += 1;
    }
  }
  
  public final void zzg(int paramInt1, int paramInt2)
    throws IOException
  {
    this.zzbuf.zzg(paramInt1, paramInt2);
  }
  
  public final void zzg(int paramInt, List<Double> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzc(((Double)paramList.get(paramInt)).doubleValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzb(((Double)paramList.get(paramInt)).doubleValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zza(paramInt, ((Double)paramList.get(i)).doubleValue());
      i += 1;
    }
  }
  
  public final void zzh(int paramInt, List<Integer> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzbh(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzax(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzd(paramInt, ((Integer)paramList.get(i)).intValue());
      i += 1;
    }
  }
  
  public final void zzi(int paramInt, long paramLong)
    throws IOException
  {
    this.zzbuf.zza(paramInt, paramLong);
  }
  
  public final void zzi(int paramInt, List<Boolean> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzv(((Boolean)paramList.get(paramInt)).booleanValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzu(((Boolean)paramList.get(paramInt)).booleanValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzb(paramInt, ((Boolean)paramList.get(i)).booleanValue());
      i += 1;
    }
  }
  
  public final void zzj(int paramInt, long paramLong)
    throws IOException
  {
    this.zzbuf.zzc(paramInt, paramLong);
  }
  
  public final void zzj(int paramInt, List<Integer> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzbd(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzay(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zze(paramInt, ((Integer)paramList.get(i)).intValue());
      i += 1;
    }
  }
  
  public final void zzk(int paramInt, List<Integer> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzbg(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzba(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzg(paramInt, ((Integer)paramList.get(i)).intValue());
      i += 1;
    }
  }
  
  public final void zzl(int paramInt, List<Long> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzbc(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzax(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzc(paramInt, ((Long)paramList.get(i)).longValue());
      i += 1;
    }
  }
  
  public final void zzm(int paramInt, List<Integer> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzbe(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzaz(((Integer)paramList.get(paramInt)).intValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzf(paramInt, ((Integer)paramList.get(i)).intValue());
      i += 1;
    }
  }
  
  public final void zzn(int paramInt1, int paramInt2)
    throws IOException
  {
    this.zzbuf.zzg(paramInt1, paramInt2);
  }
  
  public final void zzn(int paramInt, List<Long> paramList, boolean paramBoolean)
    throws IOException
  {
    int i = 0;
    int j = 0;
    if (paramBoolean)
    {
      this.zzbuf.zzc(paramInt, 2);
      paramInt = 0;
      i = 0;
      while (paramInt < paramList.size())
      {
        i += zzut.zzba(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
      this.zzbuf.zzay(i);
      paramInt = j;
      while (paramInt < paramList.size())
      {
        this.zzbuf.zzaw(((Long)paramList.get(paramInt)).longValue());
        paramInt += 1;
      }
    }
    while (i < paramList.size())
    {
      this.zzbuf.zzb(paramInt, ((Long)paramList.get(i)).longValue());
      i += 1;
    }
  }
  
  public final void zzo(int paramInt1, int paramInt2)
    throws IOException
  {
    this.zzbuf.zzd(paramInt1, paramInt2);
  }
  
  public final int zzvj()
  {
    return zzvm.zze.zzbze;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzuv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */