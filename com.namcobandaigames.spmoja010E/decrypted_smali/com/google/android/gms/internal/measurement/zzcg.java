package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.config.GservicesValue;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzcg<V>
{
  private final V zzaan;
  private final GservicesValue<V> zzaao;
  
  private zzcg(GservicesValue<V> paramGservicesValue, V paramV)
  {
    Preconditions.checkNotNull(paramGservicesValue);
    this.zzaao = paramGservicesValue;
    this.zzaan = paramV;
  }
  
  static zzcg<Float> zza(String paramString, float paramFloat1, float paramFloat2)
  {
    return new zzcg(GservicesValue.value(paramString, Float.valueOf(0.5F)), Float.valueOf(0.5F));
  }
  
  static zzcg<Integer> zza(String paramString, int paramInt1, int paramInt2)
  {
    return new zzcg(GservicesValue.value(paramString, Integer.valueOf(paramInt2)), Integer.valueOf(paramInt1));
  }
  
  static zzcg<Long> zza(String paramString, long paramLong1, long paramLong2)
  {
    return new zzcg(GservicesValue.value(paramString, Long.valueOf(paramLong2)), Long.valueOf(paramLong1));
  }
  
  static zzcg<String> zza(String paramString1, String paramString2, String paramString3)
  {
    return new zzcg(GservicesValue.value(paramString1, paramString3), paramString2);
  }
  
  static zzcg<Boolean> zza(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    return new zzcg(GservicesValue.value(paramString, paramBoolean2), Boolean.valueOf(paramBoolean1));
  }
  
  public final V get()
  {
    return (V)this.zzaan;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzcg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */