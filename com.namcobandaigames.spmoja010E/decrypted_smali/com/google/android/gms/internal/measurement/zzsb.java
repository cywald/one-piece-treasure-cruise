package com.google.android.gms.internal.measurement;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class zzsb
{
  private static final Integer zzbqb = Integer.valueOf(0);
  private static final Integer zzbqc = Integer.valueOf(1);
  private final ExecutorService zzaea;
  private final Context zzri;
  
  public zzsb(Context paramContext)
  {
    this(paramContext, Executors.newSingleThreadExecutor());
  }
  
  @VisibleForTesting
  private zzsb(Context paramContext, ExecutorService paramExecutorService)
  {
    this.zzri = paramContext;
    this.zzaea = paramExecutorService;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\measurement\zzsb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */