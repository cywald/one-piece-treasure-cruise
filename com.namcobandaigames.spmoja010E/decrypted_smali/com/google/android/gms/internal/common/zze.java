package com.google.android.gms.internal.common;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;

public class zze
  extends Handler
{
  private static volatile zzf zzit = null;
  
  public zze() {}
  
  public zze(Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public zze(Looper paramLooper, Handler.Callback paramCallback)
  {
    super(paramLooper, paramCallback);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\common\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */