package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos.CaptureCapabilitiesResult;

final class zzef
  implements Videos.CaptureCapabilitiesResult
{
  zzef(zzee paramzzee, Status paramStatus) {}
  
  public final VideoCapabilities getCapabilities()
  {
    return null;
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */