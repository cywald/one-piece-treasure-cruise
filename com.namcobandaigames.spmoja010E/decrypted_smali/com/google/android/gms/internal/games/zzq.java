package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;

final class zzq
  implements Achievements.LoadAchievementsResult
{
  zzq(zzp paramzzp, Status paramStatus) {}
  
  public final AchievementBuffer getAchievements()
  {
    return new AchievementBuffer(DataHolder.empty(14));
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
  
  public final void release() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */