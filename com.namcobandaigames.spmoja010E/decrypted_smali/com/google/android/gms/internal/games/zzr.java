package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games.zza;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;

abstract class zzr
  extends Games.zza<Achievements.UpdateAchievementResult>
{
  private final String zzji;
  
  public zzr(String paramString, GoogleApiClient paramGoogleApiClient)
  {
    super(paramGoogleApiClient);
    this.zzji = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */