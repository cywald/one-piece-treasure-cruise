package com.google.android.gms.internal.games;

import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;
import com.google.android.gms.games.internal.zze;

public final class zzf
  implements Achievements
{
  public final Intent getAchievementsIntent(GoogleApiClient paramGoogleApiClient)
  {
    return Games.zza(paramGoogleApiClient).zzx();
  }
  
  public final void increment(GoogleApiClient paramGoogleApiClient, String paramString, int paramInt)
  {
    paramGoogleApiClient.execute(new zzl(this, paramString, paramGoogleApiClient, paramString, paramInt));
  }
  
  public final PendingResult<Achievements.UpdateAchievementResult> incrementImmediate(GoogleApiClient paramGoogleApiClient, String paramString, int paramInt)
  {
    return paramGoogleApiClient.execute(new zzm(this, paramString, paramGoogleApiClient, paramString, paramInt));
  }
  
  public final PendingResult<Achievements.LoadAchievementsResult> load(GoogleApiClient paramGoogleApiClient, boolean paramBoolean)
  {
    return paramGoogleApiClient.enqueue(new zzg(this, paramGoogleApiClient, paramBoolean));
  }
  
  public final void reveal(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    paramGoogleApiClient.execute(new zzh(this, paramString, paramGoogleApiClient, paramString));
  }
  
  public final PendingResult<Achievements.UpdateAchievementResult> revealImmediate(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    return paramGoogleApiClient.execute(new zzi(this, paramString, paramGoogleApiClient, paramString));
  }
  
  public final void setSteps(GoogleApiClient paramGoogleApiClient, String paramString, int paramInt)
  {
    paramGoogleApiClient.execute(new zzn(this, paramString, paramGoogleApiClient, paramString, paramInt));
  }
  
  public final PendingResult<Achievements.UpdateAchievementResult> setStepsImmediate(GoogleApiClient paramGoogleApiClient, String paramString, int paramInt)
  {
    return paramGoogleApiClient.execute(new zzo(this, paramString, paramGoogleApiClient, paramString, paramInt));
  }
  
  public final void unlock(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    paramGoogleApiClient.execute(new zzj(this, paramString, paramGoogleApiClient, paramString));
  }
  
  public final PendingResult<Achievements.UpdateAchievementResult> unlockImmediate(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    return paramGoogleApiClient.execute(new zzk(this, paramString, paramGoogleApiClient, paramString));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */