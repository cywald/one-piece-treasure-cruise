package com.google.android.gms.internal.games;

import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.multiplayer.Invitations;
import com.google.android.gms.games.multiplayer.Invitations.LoadInvitationsResult;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;

public final class zzai
  implements Invitations
{
  public final Intent getInvitationInboxIntent(GoogleApiClient paramGoogleApiClient)
  {
    return Games.zza(paramGoogleApiClient).zzz();
  }
  
  public final PendingResult<Invitations.LoadInvitationsResult> loadInvitations(GoogleApiClient paramGoogleApiClient)
  {
    return loadInvitations(paramGoogleApiClient, 0);
  }
  
  public final PendingResult<Invitations.LoadInvitationsResult> loadInvitations(GoogleApiClient paramGoogleApiClient, int paramInt)
  {
    return paramGoogleApiClient.enqueue(new zzaj(this, paramGoogleApiClient, paramInt));
  }
  
  public final void registerInvitationListener(GoogleApiClient paramGoogleApiClient, OnInvitationReceivedListener paramOnInvitationReceivedListener)
  {
    zze localzze = Games.zza(paramGoogleApiClient, false);
    if (localzze != null) {
      localzze.zzb(paramGoogleApiClient.registerListener(paramOnInvitationReceivedListener));
    }
  }
  
  public final void unregisterInvitationListener(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient = Games.zza(paramGoogleApiClient, false);
    if (paramGoogleApiClient != null) {
      paramGoogleApiClient.zzab();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */