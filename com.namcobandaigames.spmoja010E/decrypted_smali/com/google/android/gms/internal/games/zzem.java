package com.google.android.gms.internal.games;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.HashMap;
import java.util.Set;

public final class zzem
{
  private static final String[] zzmt = { "requestId", "outcome" };
  private final int statusCode;
  private final HashMap<String, Integer> zzmu;
  
  private zzem(int paramInt, HashMap<String, Integer> paramHashMap)
  {
    this.statusCode = paramInt;
    this.zzmu = paramHashMap;
  }
  
  @VisibleForTesting
  public static zzem zzbd(DataHolder paramDataHolder)
  {
    zzeo localzzeo = new zzeo();
    localzzeo.zzo(paramDataHolder.getStatusCode());
    int j = paramDataHolder.getCount();
    int i = 0;
    while (i < j)
    {
      int k = paramDataHolder.getWindowIndex(i);
      localzzeo.zzh(paramDataHolder.getString("requestId", i, k), paramDataHolder.getInteger("outcome", i, k));
      i += 1;
    }
    return localzzeo.zzca();
  }
  
  @VisibleForTesting
  public final Set<String> getRequestIds()
  {
    return this.zzmu.keySet();
  }
  
  @VisibleForTesting
  public final int getRequestOutcome(String paramString)
  {
    Preconditions.checkArgument(this.zzmu.containsKey(paramString), String.valueOf(paramString).length() + 46 + "Request " + paramString + " was not part of the update operation!");
    return ((Integer)this.zzmu.get(paramString)).intValue();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */