package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.request.Requests.UpdateRequestsResult;
import java.util.Set;

final class zzch
  implements Requests.UpdateRequestsResult
{
  zzch(zzcg paramzzcg, Status paramStatus) {}
  
  public final Set<String> getRequestIds()
  {
    return null;
  }
  
  public final int getRequestOutcome(String paramString)
  {
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {}
    for (paramString = "Unknown request ID ".concat(paramString);; paramString = new String("Unknown request ID ")) {
      throw new IllegalArgumentException(paramString);
    }
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
  
  public final void release() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */