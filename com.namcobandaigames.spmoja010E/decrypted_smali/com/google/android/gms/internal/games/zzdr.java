package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LeaveMatchResult;

final class zzdr
  implements TurnBasedMultiplayer.LeaveMatchResult
{
  zzdr(zzdq paramzzdq, Status paramStatus) {}
  
  public final TurnBasedMatch getMatch()
  {
    return null;
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzdr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */