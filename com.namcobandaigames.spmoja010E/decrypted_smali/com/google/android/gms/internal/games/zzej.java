package com.google.android.gms.internal.games;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class zzej
{
  private final Object zzkq = new Object();
  private Handler zzkr;
  private boolean zzks;
  private HashMap<String, AtomicInteger> zzkt;
  private int zzku;
  
  public zzej(Looper paramLooper, int paramInt)
  {
    this.zzkr = new Handler(paramLooper);
    this.zzkt = new HashMap();
    this.zzku = 1000;
  }
  
  private final void zzbl()
  {
    synchronized (this.zzkq)
    {
      this.zzks = false;
      flush();
      return;
    }
  }
  
  public final void flush()
  {
    synchronized (this.zzkq)
    {
      Iterator localIterator = this.zzkt.entrySet().iterator();
      if (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        zzf((String)localEntry.getKey(), ((AtomicInteger)localEntry.getValue()).get());
      }
    }
    this.zzkt.clear();
  }
  
  protected abstract void zzf(String paramString, int paramInt);
  
  public final void zzg(String paramString, int paramInt)
  {
    synchronized (this.zzkq)
    {
      if (!this.zzks)
      {
        this.zzks = true;
        this.zzkr.postDelayed(new zzek(this), this.zzku);
      }
      AtomicInteger localAtomicInteger2 = (AtomicInteger)this.zzkt.get(paramString);
      AtomicInteger localAtomicInteger1 = localAtomicInteger2;
      if (localAtomicInteger2 == null)
      {
        localAtomicInteger1 = new AtomicInteger();
        this.zzkt.put(paramString, localAtomicInteger1);
      }
      localAtomicInteger1.addAndGet(paramInt);
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzej.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */