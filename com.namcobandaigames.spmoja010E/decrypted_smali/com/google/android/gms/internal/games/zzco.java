package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games.zza;
import com.google.android.gms.games.snapshot.Snapshots.CommitSnapshotResult;

abstract class zzco
  extends Games.zza<Snapshots.CommitSnapshotResult>
{
  private zzco(GoogleApiClient paramGoogleApiClient)
  {
    super(paramGoogleApiClient);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzco.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */