package com.google.android.gms.internal.games;

import java.util.HashMap;

public final class zzeo
{
  private int statusCode = 0;
  private HashMap<String, Integer> zzmu = new HashMap();
  
  public final zzem zzca()
  {
    return new zzem(this.statusCode, this.zzmu, null);
  }
  
  public final zzeo zzh(String paramString, int paramInt)
  {
    switch (paramInt)
    {
    }
    for (int i = 0;; i = 1)
    {
      if (i != 0) {
        this.zzmu.put(paramString, Integer.valueOf(paramInt));
      }
      return this;
    }
  }
  
  public final zzeo zzo(int paramInt)
  {
    this.statusCode = paramInt;
    return this;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzeo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */