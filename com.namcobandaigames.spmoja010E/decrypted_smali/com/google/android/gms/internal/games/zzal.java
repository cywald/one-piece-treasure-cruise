package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.Invitations.LoadInvitationsResult;

final class zzal
  implements Invitations.LoadInvitationsResult
{
  zzal(zzak paramzzak, Status paramStatus) {}
  
  public final InvitationBuffer getInvitations()
  {
    return new InvitationBuffer(DataHolder.empty(14));
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
  
  public final void release() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */