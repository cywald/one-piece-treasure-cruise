package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.Quests.AcceptQuestResult;

final class zzbu
  implements Quests.AcceptQuestResult
{
  zzbu(zzbt paramzzbt, Status paramStatus) {}
  
  public final Quest getQuest()
  {
    return null;
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzbu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */