package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesMetadata;
import com.google.android.gms.games.GamesMetadata.LoadGamesResult;
import com.google.android.gms.games.internal.zze;

public final class zzad
  implements GamesMetadata
{
  public final Game getCurrentGame(GoogleApiClient paramGoogleApiClient)
  {
    return Games.zza(paramGoogleApiClient).zzu();
  }
  
  public final PendingResult<GamesMetadata.LoadGamesResult> loadGame(GoogleApiClient paramGoogleApiClient)
  {
    return paramGoogleApiClient.enqueue(new zzae(this, paramGoogleApiClient));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */