package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadScoresResult;

final class zzaz
  implements Leaderboards.LoadScoresResult
{
  zzaz(zzay paramzzay, Status paramStatus) {}
  
  public final Leaderboard getLeaderboard()
  {
    return null;
  }
  
  public final LeaderboardScoreBuffer getScores()
  {
    return new LeaderboardScoreBuffer(DataHolder.empty(14));
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
  
  public final void release() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzaz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */