package com.google.android.gms.internal.games;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApi.Settings;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.GamesOptions;

public class zzu
  extends GoogleApi<Games.GamesOptions>
{
  protected zzu(@NonNull Activity paramActivity, @Nullable Games.GamesOptions paramGamesOptions)
  {
    super(paramActivity, Games.API, paramGamesOptions, GoogleApi.Settings.DEFAULT_SETTINGS);
  }
  
  protected zzu(@NonNull Context paramContext, @Nullable Games.GamesOptions paramGamesOptions)
  {
    super(paramContext, Games.API, paramGamesOptions, GoogleApi.Settings.DEFAULT_SETTINGS);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */