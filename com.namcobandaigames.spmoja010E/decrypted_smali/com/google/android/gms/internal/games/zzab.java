package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games.zza;

abstract class zzab
  extends Games.zza<Result>
{
  private zzab(GoogleApiClient paramGoogleApiClient)
  {
    super(paramGoogleApiClient);
  }
  
  public Result createFailedResult(Status paramStatus)
  {
    return new zzac(this, paramStatus);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */