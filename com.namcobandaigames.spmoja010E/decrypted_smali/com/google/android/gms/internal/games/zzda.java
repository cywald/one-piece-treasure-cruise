package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.Stats.LoadPlayerStatsResult;

final class zzda
  implements Stats.LoadPlayerStatsResult
{
  zzda(zzcz paramzzcz, Status paramStatus) {}
  
  public final PlayerStats getPlayerStats()
  {
    return null;
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
  
  public final void release() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzda.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */