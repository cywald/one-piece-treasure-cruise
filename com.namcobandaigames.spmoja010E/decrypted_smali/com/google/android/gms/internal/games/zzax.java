package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;

final class zzax
  implements Leaderboards.LoadPlayerScoreResult
{
  zzax(zzaw paramzzaw, Status paramStatus) {}
  
  public final LeaderboardScore getScore()
  {
    return null;
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzax.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */