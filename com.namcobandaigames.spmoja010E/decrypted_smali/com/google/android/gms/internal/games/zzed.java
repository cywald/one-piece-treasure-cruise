package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.video.Videos.CaptureAvailableResult;

final class zzed
  implements Videos.CaptureAvailableResult
{
  zzed(zzec paramzzec, Status paramStatus) {}
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
  
  public final boolean isAvailable()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */