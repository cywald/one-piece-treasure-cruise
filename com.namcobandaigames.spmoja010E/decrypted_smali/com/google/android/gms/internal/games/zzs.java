package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;

final class zzs
  implements Achievements.UpdateAchievementResult
{
  zzs(zzr paramzzr, Status paramStatus) {}
  
  public final String getAchievementId()
  {
    return zzr.zza(this.zzjj);
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */