package com.google.android.gms.internal.games;

import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.games.video.Videos.CaptureAvailableResult;
import com.google.android.gms.games.video.Videos.CaptureCapabilitiesResult;
import com.google.android.gms.games.video.Videos.CaptureOverlayStateListener;
import com.google.android.gms.games.video.Videos.CaptureStateResult;

public final class zzdy
  implements Videos
{
  public final PendingResult<Videos.CaptureCapabilitiesResult> getCaptureCapabilities(GoogleApiClient paramGoogleApiClient)
  {
    return paramGoogleApiClient.enqueue(new zzdz(this, paramGoogleApiClient));
  }
  
  public final Intent getCaptureOverlayIntent(GoogleApiClient paramGoogleApiClient)
  {
    return Games.zza(paramGoogleApiClient).zzay();
  }
  
  public final PendingResult<Videos.CaptureStateResult> getCaptureState(GoogleApiClient paramGoogleApiClient)
  {
    return paramGoogleApiClient.enqueue(new zzea(this, paramGoogleApiClient));
  }
  
  public final PendingResult<Videos.CaptureAvailableResult> isCaptureAvailable(GoogleApiClient paramGoogleApiClient, int paramInt)
  {
    return paramGoogleApiClient.enqueue(new zzeb(this, paramGoogleApiClient, paramInt));
  }
  
  public final boolean isCaptureSupported(GoogleApiClient paramGoogleApiClient)
  {
    return Games.zza(paramGoogleApiClient).zzba();
  }
  
  public final void registerCaptureOverlayStateChangedListener(GoogleApiClient paramGoogleApiClient, Videos.CaptureOverlayStateListener paramCaptureOverlayStateListener)
  {
    zze localzze = Games.zza(paramGoogleApiClient, false);
    if (localzze != null) {
      localzze.zzh(paramGoogleApiClient.registerListener(paramCaptureOverlayStateListener));
    }
  }
  
  public final void unregisterCaptureOverlayStateChangedListener(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient = Games.zza(paramGoogleApiClient, false);
    if (paramGoogleApiClient != null) {
      paramGoogleApiClient.zzbc();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzdy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */