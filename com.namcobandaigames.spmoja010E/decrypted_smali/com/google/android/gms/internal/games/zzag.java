package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GamesMetadata.LoadGamesResult;

final class zzag
  implements GamesMetadata.LoadGamesResult
{
  zzag(zzaf paramzzaf, Status paramStatus) {}
  
  public final GameBuffer getGames()
  {
    return new GameBuffer(DataHolder.empty(14));
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
  
  public final void release() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */