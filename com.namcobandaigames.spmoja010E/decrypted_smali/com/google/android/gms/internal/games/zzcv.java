package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.Snapshots.OpenSnapshotResult;

final class zzcv
  implements Snapshots.OpenSnapshotResult
{
  zzcv(zzcu paramzzcu, Status paramStatus) {}
  
  public final String getConflictId()
  {
    return null;
  }
  
  public final Snapshot getConflictingSnapshot()
  {
    return null;
  }
  
  public final SnapshotContents getResolutionSnapshotContents()
  {
    return null;
  }
  
  public final Snapshot getSnapshot()
  {
    return null;
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzcv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */