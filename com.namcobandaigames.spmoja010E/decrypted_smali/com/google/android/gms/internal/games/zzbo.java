package com.google.android.gms.internal.games;

import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.quest.QuestUpdateListener;
import com.google.android.gms.games.quest.Quests;
import com.google.android.gms.games.quest.Quests.AcceptQuestResult;
import com.google.android.gms.games.quest.Quests.ClaimMilestoneResult;
import com.google.android.gms.games.quest.Quests.LoadQuestsResult;

public final class zzbo
  implements Quests
{
  public final PendingResult<Quests.AcceptQuestResult> accept(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    return paramGoogleApiClient.execute(new zzbp(this, paramGoogleApiClient, paramString));
  }
  
  public final PendingResult<Quests.ClaimMilestoneResult> claim(GoogleApiClient paramGoogleApiClient, String paramString1, String paramString2)
  {
    return paramGoogleApiClient.execute(new zzbq(this, paramGoogleApiClient, paramString1, paramString2));
  }
  
  public final Intent getQuestIntent(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    return Games.zza(paramGoogleApiClient).zzd(paramString);
  }
  
  public final Intent getQuestsIntent(GoogleApiClient paramGoogleApiClient, int[] paramArrayOfInt)
  {
    return Games.zza(paramGoogleApiClient).zza(paramArrayOfInt);
  }
  
  public final PendingResult<Quests.LoadQuestsResult> load(GoogleApiClient paramGoogleApiClient, int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
  {
    return paramGoogleApiClient.enqueue(new zzbr(this, paramGoogleApiClient, paramArrayOfInt, paramInt, paramBoolean));
  }
  
  public final PendingResult<Quests.LoadQuestsResult> loadByIds(GoogleApiClient paramGoogleApiClient, boolean paramBoolean, String... paramVarArgs)
  {
    return paramGoogleApiClient.enqueue(new zzbs(this, paramGoogleApiClient, paramBoolean, paramVarArgs));
  }
  
  public final void registerQuestUpdateListener(GoogleApiClient paramGoogleApiClient, QuestUpdateListener paramQuestUpdateListener)
  {
    zze localzze = Games.zza(paramGoogleApiClient, false);
    if (localzze != null) {
      localzze.zze(paramGoogleApiClient.registerListener(paramQuestUpdateListener));
    }
  }
  
  public final void showStateChangedPopup(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    paramGoogleApiClient = Games.zza(paramGoogleApiClient, false);
    if (paramGoogleApiClient != null) {
      paramGoogleApiClient.zze(paramString);
    }
  }
  
  public final void unregisterQuestUpdateListener(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient = Games.zza(paramGoogleApiClient, false);
    if (paramGoogleApiClient != null) {
      paramGoogleApiClient.zzae();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzbo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */