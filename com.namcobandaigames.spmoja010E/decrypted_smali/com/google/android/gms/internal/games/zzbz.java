package com.google.android.gms.internal.games;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer.ReliableMessageSentCallback;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import java.util.List;

public final class zzbz
  implements RealTimeMultiplayer
{
  private static ListenerHolder<RoomUpdateListener> zza(@NonNull GoogleApiClient paramGoogleApiClient, @NonNull RoomConfig paramRoomConfig)
  {
    if (paramRoomConfig.getRoomUpdateCallback() != null) {
      return paramGoogleApiClient.registerListener(paramRoomConfig.getRoomUpdateCallback());
    }
    return paramGoogleApiClient.registerListener(paramRoomConfig.getRoomUpdateListener());
  }
  
  private static <L> ListenerHolder<L> zza(GoogleApiClient paramGoogleApiClient, L paramL)
  {
    if (paramL == null) {
      return null;
    }
    return paramGoogleApiClient.registerListener(paramL);
  }
  
  @Nullable
  private static ListenerHolder<RoomStatusUpdateListener> zzb(@NonNull GoogleApiClient paramGoogleApiClient, @NonNull RoomConfig paramRoomConfig)
  {
    if (paramRoomConfig.getRoomStatusUpdateCallback() != null) {
      return zza(paramGoogleApiClient, paramRoomConfig.getRoomStatusUpdateCallback());
    }
    return zza(paramGoogleApiClient, paramRoomConfig.getRoomStatusUpdateListener());
  }
  
  @Nullable
  private static ListenerHolder<RealTimeMessageReceivedListener> zzc(@NonNull GoogleApiClient paramGoogleApiClient, @NonNull RoomConfig paramRoomConfig)
  {
    if (paramRoomConfig.getOnMessageReceivedListener() != null) {
      return paramGoogleApiClient.registerListener(paramRoomConfig.getOnMessageReceivedListener());
    }
    return paramGoogleApiClient.registerListener(paramRoomConfig.getMessageReceivedListener());
  }
  
  public final void create(GoogleApiClient paramGoogleApiClient, RoomConfig paramRoomConfig)
  {
    zze localzze = Games.zza(paramGoogleApiClient, false);
    if (localzze == null) {
      return;
    }
    localzze.zzb(zza(paramGoogleApiClient, paramRoomConfig), zzb(paramGoogleApiClient, paramRoomConfig), zzc(paramGoogleApiClient, paramRoomConfig), paramRoomConfig);
  }
  
  public final void declineInvitation(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    paramGoogleApiClient = Games.zza(paramGoogleApiClient, false);
    if (paramGoogleApiClient != null) {
      paramGoogleApiClient.zze(paramString, 0);
    }
  }
  
  public final void dismissInvitation(GoogleApiClient paramGoogleApiClient, String paramString)
  {
    paramGoogleApiClient = Games.zza(paramGoogleApiClient, false);
    if (paramGoogleApiClient != null) {
      paramGoogleApiClient.zzc(paramString, 0);
    }
  }
  
  public final Intent getSelectOpponentsIntent(GoogleApiClient paramGoogleApiClient, int paramInt1, int paramInt2)
  {
    return Games.zza(paramGoogleApiClient).zzd(paramInt1, paramInt2, true);
  }
  
  public final Intent getSelectOpponentsIntent(GoogleApiClient paramGoogleApiClient, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    return Games.zza(paramGoogleApiClient).zzd(paramInt1, paramInt2, paramBoolean);
  }
  
  public final Intent getWaitingRoomIntent(GoogleApiClient paramGoogleApiClient, Room paramRoom, int paramInt)
  {
    return Games.zza(paramGoogleApiClient).zzb(paramRoom, paramInt);
  }
  
  public final void join(GoogleApiClient paramGoogleApiClient, RoomConfig paramRoomConfig)
  {
    zze localzze = Games.zza(paramGoogleApiClient, false);
    if (localzze == null) {
      return;
    }
    localzze.zzd(zza(paramGoogleApiClient, paramRoomConfig), zzb(paramGoogleApiClient, paramRoomConfig), zzc(paramGoogleApiClient, paramRoomConfig), paramRoomConfig);
  }
  
  public final void leave(GoogleApiClient paramGoogleApiClient, RoomUpdateListener paramRoomUpdateListener, String paramString)
  {
    zze localzze = Games.zza(paramGoogleApiClient, false);
    if (localzze != null) {
      localzze.zza(paramGoogleApiClient.registerListener(paramRoomUpdateListener), paramString);
    }
  }
  
  public final int sendReliableMessage(GoogleApiClient paramGoogleApiClient, RealTimeMultiplayer.ReliableMessageSentCallback paramReliableMessageSentCallback, byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    paramReliableMessageSentCallback = zza(paramGoogleApiClient, paramReliableMessageSentCallback);
    return Games.zza(paramGoogleApiClient).zzb(paramReliableMessageSentCallback, paramArrayOfByte, paramString1, paramString2);
  }
  
  public final int sendUnreliableMessage(GoogleApiClient paramGoogleApiClient, byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    return Games.zza(paramGoogleApiClient).zza(paramArrayOfByte, paramString1, new String[] { paramString2 });
  }
  
  public final int sendUnreliableMessage(GoogleApiClient paramGoogleApiClient, byte[] paramArrayOfByte, String paramString, List<String> paramList)
  {
    paramList = (String[])paramList.toArray(new String[paramList.size()]);
    return Games.zza(paramGoogleApiClient).zza(paramArrayOfByte, paramString, paramList);
  }
  
  public final int sendUnreliableMessageToOthers(GoogleApiClient paramGoogleApiClient, byte[] paramArrayOfByte, String paramString)
  {
    return Games.zza(paramGoogleApiClient).zzb(paramArrayOfByte, paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzbz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */