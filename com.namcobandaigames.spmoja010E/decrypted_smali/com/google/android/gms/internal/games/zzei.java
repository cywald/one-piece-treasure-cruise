package com.google.android.gms.internal.games;

public final class zzei
{
  public static String zzn(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException(29 + "Unknown time span " + paramInt);
    case 0: 
      return "DAILY";
    case 1: 
      return "WEEKLY";
    }
    return "ALL_TIME";
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzei.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */