package com.google.android.gms.internal.games;

import android.annotation.SuppressLint;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.event.Events;
import com.google.android.gms.games.event.Events.LoadEventsResult;
import com.google.android.gms.games.internal.zze;

public final class zzv
  implements Events
{
  @SuppressLint({"MissingRemoteException"})
  public final void increment(GoogleApiClient paramGoogleApiClient, String paramString, int paramInt)
  {
    zze localzze = Games.zzb(paramGoogleApiClient, false);
    if (localzze == null) {
      return;
    }
    if (localzze.isConnected())
    {
      localzze.zza(paramString, paramInt);
      return;
    }
    paramGoogleApiClient.execute(new zzy(this, paramGoogleApiClient, paramString, paramInt));
  }
  
  public final PendingResult<Events.LoadEventsResult> load(GoogleApiClient paramGoogleApiClient, boolean paramBoolean)
  {
    return paramGoogleApiClient.enqueue(new zzx(this, paramGoogleApiClient, paramBoolean));
  }
  
  public final PendingResult<Events.LoadEventsResult> loadByIds(GoogleApiClient paramGoogleApiClient, boolean paramBoolean, String... paramVarArgs)
  {
    return paramGoogleApiClient.enqueue(new zzw(this, paramGoogleApiClient, paramBoolean, paramVarArgs));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */