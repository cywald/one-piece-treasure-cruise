package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.request.GameRequestBuffer;
import com.google.android.gms.games.request.Requests.LoadRequestsResult;

final class zzcf
  implements Requests.LoadRequestsResult
{
  zzcf(zzce paramzzce, Status paramStatus) {}
  
  public final GameRequestBuffer getRequests(int paramInt)
  {
    return new GameRequestBuffer(DataHolder.empty(this.zzbc.getStatusCode()));
  }
  
  public final Status getStatus()
  {
    return this.zzbc;
  }
  
  public final void release() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzcf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */