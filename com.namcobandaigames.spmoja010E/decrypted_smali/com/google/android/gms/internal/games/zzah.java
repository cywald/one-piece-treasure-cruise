package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzah<TResult>
  extends TaskApiCall<zze, TResult>
{
  protected abstract void zza(zze paramzze, TaskCompletionSource<TResult> paramTaskCompletionSource)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */