package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games.zza;
import com.google.android.gms.games.multiplayer.Invitations.LoadInvitationsResult;

abstract class zzak
  extends Games.zza<Invitations.LoadInvitationsResult>
{
  private zzak(GoogleApiClient paramGoogleApiClient)
  {
    super(paramGoogleApiClient);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */