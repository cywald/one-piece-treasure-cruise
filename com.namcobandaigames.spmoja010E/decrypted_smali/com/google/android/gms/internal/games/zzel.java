package com.google.android.gms.internal.games;

import java.util.concurrent.atomic.AtomicReference;

public abstract class zzel
{
  private final AtomicReference<zzej> zzkw = new AtomicReference();
  
  public final void flush()
  {
    zzej localzzej = (zzej)this.zzkw.get();
    if (localzzej != null) {
      localzzej.flush();
    }
  }
  
  public final void zza(String paramString, int paramInt)
  {
    zzej localzzej2 = (zzej)this.zzkw.get();
    zzej localzzej1 = localzzej2;
    if (localzzej2 == null)
    {
      localzzej2 = zzbe();
      localzzej1 = localzzej2;
      if (!this.zzkw.compareAndSet(null, localzzej2)) {
        localzzej1 = (zzej)this.zzkw.get();
      }
    }
    localzzej1.zzg(paramString, paramInt);
  }
  
  protected abstract zzej zzbe();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */