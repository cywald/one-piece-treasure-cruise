package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games.zza;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadScoresResult;

abstract class zzay
  extends Games.zza<Leaderboards.LoadScoresResult>
{
  private zzay(GoogleApiClient paramGoogleApiClient)
  {
    super(paramGoogleApiClient);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\games\zzay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */