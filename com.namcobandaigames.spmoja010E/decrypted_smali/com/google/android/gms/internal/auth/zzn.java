package com.google.android.gms.internal.auth;

import android.accounts.Account;
import com.google.android.gms.auth.account.zzb;

class zzn
  extends zzb
{
  public void zza(boolean paramBoolean)
  {
    throw new UnsupportedOperationException();
  }
  
  public void zzc(Account paramAccount)
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth\zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */