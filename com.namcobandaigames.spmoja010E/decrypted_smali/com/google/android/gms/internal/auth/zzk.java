package com.google.android.gms.internal.auth;

import android.accounts.Account;
import com.google.android.gms.common.api.Status;

final class zzk
  extends zzn
{
  zzk(zzj paramzzj) {}
  
  public final void zzc(Account paramAccount)
  {
    zzj localzzj = this.zzaf;
    if (paramAccount != null) {}
    for (Status localStatus = Status.RESULT_SUCCESS;; localStatus = zzh.zzc())
    {
      localzzj.setResult(new zzo(localStatus, paramAccount));
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth\zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */