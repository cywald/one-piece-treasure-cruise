package com.google.android.gms.internal.auth;

import com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl;

final class zzj
  extends BaseImplementation.ApiMethodImpl<WorkAccountApi.AddAccountResult, zzr>
{
  zzj(zzh paramzzh, Api paramApi, GoogleApiClient paramGoogleApiClient, String paramString)
  {
    super(paramApi, paramGoogleApiClient);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */