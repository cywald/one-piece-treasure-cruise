package com.google.android.gms.internal.auth;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyResponse;

public abstract class zzam
  extends zzb
  implements zzal
{
  public zzam()
  {
    super("com.google.android.gms.auth.api.internal.IAuthCallbacks");
  }
  
  protected final boolean dispatchTransaction(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
    throws RemoteException
  {
    switch (paramInt1)
    {
    default: 
      return false;
    case 1: 
      zza((ProxyResponse)zzc.zza(paramParcel1, ProxyResponse.CREATOR));
    }
    for (;;)
    {
      paramParcel2.writeNoException();
      return true;
      zzb(paramParcel1.readString());
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth\zzam.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */