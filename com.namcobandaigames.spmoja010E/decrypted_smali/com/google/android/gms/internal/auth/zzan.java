package com.google.android.gms.internal.auth;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyRequest;

public abstract interface zzan
  extends IInterface
{
  public abstract void zza(zzal paramzzal)
    throws RemoteException;
  
  public abstract void zza(zzal paramzzal, ProxyRequest paramProxyRequest)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth\zzan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */