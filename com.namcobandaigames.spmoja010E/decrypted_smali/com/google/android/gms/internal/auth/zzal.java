package com.google.android.gms.internal.auth;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyResponse;

public abstract interface zzal
  extends IInterface
{
  public abstract void zza(ProxyResponse paramProxyResponse)
    throws RemoteException;
  
  public abstract void zzb(String paramString)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth\zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */