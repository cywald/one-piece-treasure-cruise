package com.google.android.gms.internal.auth;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

final class zzq
  implements Result
{
  private final Status mStatus;
  
  public zzq(Status paramStatus)
  {
    this.mStatus = paramStatus;
  }
  
  public final Status getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth\zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */