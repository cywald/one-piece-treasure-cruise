package com.google.android.gms.internal.auth;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl;

final class zzi
  extends BaseImplementation.ApiMethodImpl<Result, zzr>
{
  zzi(zzh paramzzh, Api paramApi, GoogleApiClient paramGoogleApiClient, boolean paramBoolean)
  {
    super(paramApi, paramGoogleApiClient);
  }
  
  protected final Result createFailedResult(Status paramStatus)
  {
    return new zzp(paramStatus);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth\zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */