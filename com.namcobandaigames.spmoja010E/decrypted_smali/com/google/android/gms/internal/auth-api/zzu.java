package com.google.android.gms.internal.auth-api;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.api.Status;

public abstract interface zzu
  extends IInterface
{
  public abstract void zzc(Status paramStatus)
    throws RemoteException;
  
  public abstract void zzc(Status paramStatus, Credential paramCredential)
    throws RemoteException;
  
  public abstract void zzc(Status paramStatus, String paramString)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth-api\zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */