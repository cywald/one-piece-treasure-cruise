package com.google.android.gms.internal.auth-api;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.credentials.CredentialRequest;

public abstract interface zzw
  extends IInterface
{
  public abstract void zzc(zzu paramzzu)
    throws RemoteException;
  
  public abstract void zzc(zzu paramzzu, CredentialRequest paramCredentialRequest)
    throws RemoteException;
  
  public abstract void zzc(zzu paramzzu, zzs paramzzs)
    throws RemoteException;
  
  public abstract void zzc(zzu paramzzu, zzy paramzzy)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth-api\zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */