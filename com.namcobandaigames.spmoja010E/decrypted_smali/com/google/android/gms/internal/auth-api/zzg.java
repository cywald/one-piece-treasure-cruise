package com.google.android.gms.internal.auth-api;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.api.Status;

public class zzg
  extends zzv
{
  public void zzc(Status paramStatus)
  {
    throw new UnsupportedOperationException();
  }
  
  public void zzc(Status paramStatus, Credential paramCredential)
  {
    throw new UnsupportedOperationException();
  }
  
  public final void zzc(Status paramStatus, String paramString)
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\internal\auth-api\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */