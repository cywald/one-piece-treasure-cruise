package com.google.android.gms.dynamic;

import android.app.Activity;
import android.os.Bundle;

final class zab
  implements DeferredLifecycleHelper.zaa
{
  zab(DeferredLifecycleHelper paramDeferredLifecycleHelper, Activity paramActivity, Bundle paramBundle1, Bundle paramBundle2) {}
  
  public final int getState()
  {
    return 0;
  }
  
  public final void zaa(LifecycleDelegate paramLifecycleDelegate)
  {
    DeferredLifecycleHelper.zab(this.zari).onInflate(this.val$activity, this.zarj, this.zark);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\dynamic\zab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */