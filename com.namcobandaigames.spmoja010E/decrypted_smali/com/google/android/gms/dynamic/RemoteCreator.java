package com.google.android.gms.dynamic;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;

@KeepForSdk
public abstract class RemoteCreator<T>
{
  private final String zzia;
  private T zzib;
  
  @KeepForSdk
  protected RemoteCreator(String paramString)
  {
    this.zzia = paramString;
  }
  
  @KeepForSdk
  protected abstract T getRemoteCreator(IBinder paramIBinder);
  
  @KeepForSdk
  protected final T getRemoteCreatorInstance(Context paramContext)
    throws RemoteCreator.RemoteCreatorException
  {
    if (this.zzib == null)
    {
      Preconditions.checkNotNull(paramContext);
      paramContext = GooglePlayServicesUtilLight.getRemoteContext(paramContext);
      if (paramContext == null) {
        throw new RemoteCreatorException("Could not get remote context.");
      }
      paramContext = paramContext.getClassLoader();
    }
    try
    {
      this.zzib = getRemoteCreator((IBinder)paramContext.loadClass(this.zzia).newInstance());
      return (T)this.zzib;
    }
    catch (ClassNotFoundException paramContext)
    {
      throw new RemoteCreatorException("Could not load creator class.", paramContext);
    }
    catch (InstantiationException paramContext)
    {
      throw new RemoteCreatorException("Could not instantiate creator.", paramContext);
    }
    catch (IllegalAccessException paramContext)
    {
      throw new RemoteCreatorException("Could not access creator.", paramContext);
    }
  }
  
  @KeepForSdk
  public static class RemoteCreatorException
    extends Exception
  {
    public RemoteCreatorException(String paramString)
    {
      super();
    }
    
    public RemoteCreatorException(String paramString, Throwable paramThrowable)
    {
      super(paramThrowable);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\dynamic\RemoteCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */