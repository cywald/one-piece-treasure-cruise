package com.google.android.gms.dynamic;

import java.util.Iterator;
import java.util.LinkedList;

final class zaa
  implements OnDelegateCreatedListener<T>
{
  zaa(DeferredLifecycleHelper paramDeferredLifecycleHelper) {}
  
  public final void onDelegateCreated(T paramT)
  {
    DeferredLifecycleHelper.zaa(this.zari, paramT);
    paramT = DeferredLifecycleHelper.zaa(this.zari).iterator();
    while (paramT.hasNext()) {
      ((DeferredLifecycleHelper.zaa)paramT.next()).zaa(DeferredLifecycleHelper.zab(this.zari));
    }
    DeferredLifecycleHelper.zaa(this.zari).clear();
    DeferredLifecycleHelper.zaa(this.zari, null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\android\gms\dynamic\zaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */