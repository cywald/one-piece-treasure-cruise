package com.google.firebase.inject;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract interface Provider<T>
{
  @KeepForSdk
  public abstract T get();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\inject\Provider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */