package com.google.firebase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.StringResourceValueReader;
import com.google.android.gms.common.util.Strings;
import com.google.firebase.annotations.PublicApi;

@PublicApi
public final class FirebaseOptions
{
  private final String zza;
  private final String zzb;
  private final String zzc;
  private final String zzd;
  private final String zze;
  private final String zzf;
  private final String zzg;
  
  private FirebaseOptions(@NonNull String paramString1, @NonNull String paramString2, @Nullable String paramString3, @Nullable String paramString4, @Nullable String paramString5, @Nullable String paramString6, @Nullable String paramString7)
  {
    if (!Strings.isEmptyOrWhitespace(paramString1)) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "ApplicationId must be set.");
      this.zzb = paramString1;
      this.zza = paramString2;
      this.zzc = paramString3;
      this.zzd = paramString4;
      this.zze = paramString5;
      this.zzf = paramString6;
      this.zzg = paramString7;
      return;
    }
  }
  
  @PublicApi
  public static FirebaseOptions fromResource(Context paramContext)
  {
    paramContext = new StringResourceValueReader(paramContext);
    String str = paramContext.getString("google_app_id");
    if (TextUtils.isEmpty(str)) {
      return null;
    }
    return new FirebaseOptions(str, paramContext.getString("google_api_key"), paramContext.getString("firebase_database_url"), paramContext.getString("ga_trackingId"), paramContext.getString("gcm_defaultSenderId"), paramContext.getString("google_storage_bucket"), paramContext.getString("project_id"));
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof FirebaseOptions)) {}
    do
    {
      return false;
      paramObject = (FirebaseOptions)paramObject;
    } while ((!Objects.equal(this.zzb, ((FirebaseOptions)paramObject).zzb)) || (!Objects.equal(this.zza, ((FirebaseOptions)paramObject).zza)) || (!Objects.equal(this.zzc, ((FirebaseOptions)paramObject).zzc)) || (!Objects.equal(this.zzd, ((FirebaseOptions)paramObject).zzd)) || (!Objects.equal(this.zze, ((FirebaseOptions)paramObject).zze)) || (!Objects.equal(this.zzf, ((FirebaseOptions)paramObject).zzf)) || (!Objects.equal(this.zzg, ((FirebaseOptions)paramObject).zzg)));
    return true;
  }
  
  @PublicApi
  public final String getApiKey()
  {
    return this.zza;
  }
  
  @PublicApi
  public final String getApplicationId()
  {
    return this.zzb;
  }
  
  @PublicApi
  public final String getDatabaseUrl()
  {
    return this.zzc;
  }
  
  @KeepForSdk
  public final String getGaTrackingId()
  {
    return this.zzd;
  }
  
  @PublicApi
  public final String getGcmSenderId()
  {
    return this.zze;
  }
  
  @PublicApi
  public final String getProjectId()
  {
    return this.zzg;
  }
  
  @PublicApi
  public final String getStorageBucket()
  {
    return this.zzf;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzb, this.zza, this.zzc, this.zzd, this.zze, this.zzf, this.zzg });
  }
  
  public final String toString()
  {
    return Objects.toStringHelper(this).add("applicationId", this.zzb).add("apiKey", this.zza).add("databaseUrl", this.zzc).add("gcmSenderId", this.zze).add("storageBucket", this.zzf).add("projectId", this.zzg).toString();
  }
  
  @PublicApi
  public static final class Builder
  {
    private String zza;
    private String zzb;
    private String zzc;
    private String zzd;
    private String zze;
    private String zzf;
    private String zzg;
    
    @PublicApi
    public Builder() {}
    
    @PublicApi
    public Builder(FirebaseOptions paramFirebaseOptions)
    {
      this.zzb = FirebaseOptions.zza(paramFirebaseOptions);
      this.zza = FirebaseOptions.zzb(paramFirebaseOptions);
      this.zzc = FirebaseOptions.zzc(paramFirebaseOptions);
      this.zzd = FirebaseOptions.zzd(paramFirebaseOptions);
      this.zze = FirebaseOptions.zze(paramFirebaseOptions);
      this.zzf = FirebaseOptions.zzf(paramFirebaseOptions);
      this.zzg = FirebaseOptions.zzg(paramFirebaseOptions);
    }
    
    @PublicApi
    public final FirebaseOptions build()
    {
      return new FirebaseOptions(this.zzb, this.zza, this.zzc, this.zzd, this.zze, this.zzf, this.zzg, (byte)0);
    }
    
    @PublicApi
    public final Builder setApiKey(@NonNull String paramString)
    {
      this.zza = Preconditions.checkNotEmpty(paramString, "ApiKey must be set.");
      return this;
    }
    
    @PublicApi
    public final Builder setApplicationId(@NonNull String paramString)
    {
      this.zzb = Preconditions.checkNotEmpty(paramString, "ApplicationId must be set.");
      return this;
    }
    
    @PublicApi
    public final Builder setDatabaseUrl(@Nullable String paramString)
    {
      this.zzc = paramString;
      return this;
    }
    
    @KeepForSdk
    public final Builder setGaTrackingId(@Nullable String paramString)
    {
      this.zzd = paramString;
      return this;
    }
    
    @PublicApi
    public final Builder setGcmSenderId(@Nullable String paramString)
    {
      this.zze = paramString;
      return this;
    }
    
    @PublicApi
    public final Builder setProjectId(@Nullable String paramString)
    {
      this.zzg = paramString;
      return this;
    }
    
    @PublicApi
    public final Builder setStorageBucket(@Nullable String paramString)
    {
      this.zzf = paramString;
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\FirebaseOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */