package com.google.firebase.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;

@KeepForSdk
public class InternalTokenResult
{
  private String zza;
  
  @KeepForSdk
  public InternalTokenResult(@Nullable String paramString)
  {
    this.zza = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof InternalTokenResult)) {
      return false;
    }
    paramObject = (InternalTokenResult)paramObject;
    return Objects.equal(this.zza, ((InternalTokenResult)paramObject).zza);
  }
  
  @Nullable
  @KeepForSdk
  public String getToken()
  {
    return this.zza;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zza });
  }
  
  public String toString()
  {
    return Objects.toStringHelper(this).add("token", this.zza).toString();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\internal\InternalTokenResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */