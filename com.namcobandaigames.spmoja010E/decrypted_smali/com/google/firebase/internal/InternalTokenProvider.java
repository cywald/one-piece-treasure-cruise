package com.google.firebase.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.GetTokenResult;

@Deprecated
@KeepForSdk
public abstract interface InternalTokenProvider
{
  @KeepForSdk
  public abstract Task<GetTokenResult> getAccessToken(boolean paramBoolean);
  
  @Nullable
  @KeepForSdk
  public abstract String getUid();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\internal\InternalTokenProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */