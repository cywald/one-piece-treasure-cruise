package com.google.firebase.events;

import com.google.android.gms.common.annotation.KeepForSdk;
import java.util.concurrent.Executor;

@KeepForSdk
public abstract interface Subscriber
{
  @KeepForSdk
  public abstract <T> void subscribe(Class<T> paramClass, EventHandler<? super T> paramEventHandler);
  
  @KeepForSdk
  public abstract <T> void subscribe(Class<T> paramClass, Executor paramExecutor, EventHandler<? super T> paramEventHandler);
  
  @KeepForSdk
  public abstract <T> void unsubscribe(Class<T> paramClass, EventHandler<? super T> paramEventHandler);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\events\Subscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */