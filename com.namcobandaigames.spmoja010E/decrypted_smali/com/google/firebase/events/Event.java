package com.google.firebase.events;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;

@KeepForSdk
public class Event<T>
{
  private final Class<T> zza;
  private final T zzb;
  
  @KeepForSdk
  public Event(Class<T> paramClass, T paramT)
  {
    this.zza = ((Class)Preconditions.checkNotNull(paramClass));
    this.zzb = Preconditions.checkNotNull(paramT);
  }
  
  @KeepForSdk
  public T getPayload()
  {
    return (T)this.zzb;
  }
  
  @KeepForSdk
  public Class<T> getType()
  {
    return this.zza;
  }
  
  public String toString()
  {
    return String.format("Event{type: %s, payload: %s}", new Object[] { this.zza, this.zzb });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\events\Event.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */