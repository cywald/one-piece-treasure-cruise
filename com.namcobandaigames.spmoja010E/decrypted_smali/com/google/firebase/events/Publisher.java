package com.google.firebase.events;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract interface Publisher
{
  @KeepForSdk
  public abstract void publish(Event<?> paramEvent);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\events\Publisher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */