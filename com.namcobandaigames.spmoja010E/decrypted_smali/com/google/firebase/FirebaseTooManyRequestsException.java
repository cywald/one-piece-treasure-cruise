package com.google.firebase;

import com.google.firebase.annotations.PublicApi;

@PublicApi
public class FirebaseTooManyRequestsException
  extends FirebaseException
{
  @PublicApi
  public FirebaseTooManyRequestsException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\FirebaseTooManyRequestsException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */