package com.google.firebase.analytics.connector.internal;

import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement.OnEventListener;
import com.google.firebase.analytics.connector.AnalyticsConnector.AnalyticsConnectorListener;
import java.util.Set;

final class zze
  implements AppMeasurement.OnEventListener
{
  public zze(zzd paramzzd) {}
  
  public final void onEvent(String paramString1, String paramString2, Bundle paramBundle, long paramLong)
  {
    if (!this.zzbsv.zzbss.contains(paramString2)) {
      return;
    }
    paramString1 = new Bundle();
    paramString1.putString("events", zzc.zzfs(paramString2));
    zzd.zza(this.zzbsv).onMessageTriggered(2, paramString1);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\analytics\connector\internal\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */