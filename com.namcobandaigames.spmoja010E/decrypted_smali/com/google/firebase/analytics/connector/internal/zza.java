package com.google.firebase.analytics.connector.internal;

import com.google.firebase.analytics.connector.AnalyticsConnector.AnalyticsConnectorListener;
import java.util.Set;

public abstract interface zza
{
  public abstract void registerEventNames(Set<String> paramSet);
  
  public abstract void unregisterEventNames();
  
  public abstract AnalyticsConnector.AnalyticsConnectorListener zztl();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\analytics\connector\internal\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */