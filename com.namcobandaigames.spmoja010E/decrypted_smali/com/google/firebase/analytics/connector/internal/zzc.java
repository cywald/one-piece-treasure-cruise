package com.google.firebase.analytics.connector.internal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.measurement.AppMeasurement.ConditionalUserProperty;
import com.google.android.gms.measurement.AppMeasurement.Event;
import com.google.android.gms.measurement.AppMeasurement.UserProperty;
import com.google.android.gms.measurement.internal.zzfk;
import com.google.firebase.analytics.connector.AnalyticsConnector.ConditionalUserProperty;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class zzc
{
  private static final Set<String> zzbsm = new HashSet(Arrays.asList(new String[] { "_in", "_xa", "_xu", "_aq", "_aa", "_ai", "_ac", "campaign_details", "_ug", "_iapx", "_exp_set", "_exp_clear", "_exp_activate", "_exp_timeout", "_exp_expire" }));
  private static final List<String> zzbsn = Arrays.asList(new String[] { "_e", "_f", "_iap", "_s", "_au", "_ui", "_cd", "app_open" });
  private static final List<String> zzbso = Arrays.asList(new String[] { "auto", "app", "am" });
  private static final List<String> zzbsp = Arrays.asList(new String[] { "_r", "_dbg" });
  private static final List<String> zzbsq = Arrays.asList((String[])ArrayUtils.concat(new String[][] { AppMeasurement.UserProperty.zzado, AppMeasurement.UserProperty.zzadp }));
  private static final List<String> zzbsr = Arrays.asList(new String[] { "^_ltv_[A-Z]{3}$", "^_cc[1-5]{1}$" });
  
  public static boolean zza(AnalyticsConnector.ConditionalUserProperty paramConditionalUserProperty)
  {
    if (paramConditionalUserProperty == null) {}
    String str;
    do
    {
      return false;
      str = paramConditionalUserProperty.origin;
    } while ((str == null) || (str.isEmpty()) || ((paramConditionalUserProperty.value != null) && (zzfk.zzf(paramConditionalUserProperty.value) == null)) || (!zzfo(str)) || (!zzy(str, paramConditionalUserProperty.name)) || ((paramConditionalUserProperty.expiredEventName != null) && ((!zza(paramConditionalUserProperty.expiredEventName, paramConditionalUserProperty.expiredEventParams)) || (!zzb(str, paramConditionalUserProperty.expiredEventName, paramConditionalUserProperty.expiredEventParams)))) || ((paramConditionalUserProperty.triggeredEventName != null) && ((!zza(paramConditionalUserProperty.triggeredEventName, paramConditionalUserProperty.triggeredEventParams)) || (!zzb(str, paramConditionalUserProperty.triggeredEventName, paramConditionalUserProperty.triggeredEventParams)))) || ((paramConditionalUserProperty.timedOutEventName != null) && ((!zza(paramConditionalUserProperty.timedOutEventName, paramConditionalUserProperty.timedOutEventParams)) || (!zzb(str, paramConditionalUserProperty.timedOutEventName, paramConditionalUserProperty.timedOutEventParams)))));
    return true;
  }
  
  public static boolean zza(@NonNull String paramString, @Nullable Bundle paramBundle)
  {
    if (zzbsn.contains(paramString)) {
      return false;
    }
    if (paramBundle != null)
    {
      paramString = zzbsp.iterator();
      while (paramString.hasNext()) {
        if (paramBundle.containsKey((String)paramString.next())) {
          return false;
        }
      }
    }
    return true;
  }
  
  public static AppMeasurement.ConditionalUserProperty zzb(AnalyticsConnector.ConditionalUserProperty paramConditionalUserProperty)
  {
    AppMeasurement.ConditionalUserProperty localConditionalUserProperty = new AppMeasurement.ConditionalUserProperty();
    localConditionalUserProperty.mOrigin = paramConditionalUserProperty.origin;
    localConditionalUserProperty.mActive = paramConditionalUserProperty.active;
    localConditionalUserProperty.mCreationTimestamp = paramConditionalUserProperty.creationTimestamp;
    localConditionalUserProperty.mExpiredEventName = paramConditionalUserProperty.expiredEventName;
    if (paramConditionalUserProperty.expiredEventParams != null) {
      localConditionalUserProperty.mExpiredEventParams = new Bundle(paramConditionalUserProperty.expiredEventParams);
    }
    localConditionalUserProperty.mName = paramConditionalUserProperty.name;
    localConditionalUserProperty.mTimedOutEventName = paramConditionalUserProperty.timedOutEventName;
    if (paramConditionalUserProperty.timedOutEventParams != null) {
      localConditionalUserProperty.mTimedOutEventParams = new Bundle(paramConditionalUserProperty.timedOutEventParams);
    }
    localConditionalUserProperty.mTimeToLive = paramConditionalUserProperty.timeToLive;
    localConditionalUserProperty.mTriggeredEventName = paramConditionalUserProperty.triggeredEventName;
    if (paramConditionalUserProperty.triggeredEventParams != null) {
      localConditionalUserProperty.mTriggeredEventParams = new Bundle(paramConditionalUserProperty.triggeredEventParams);
    }
    localConditionalUserProperty.mTriggeredTimestamp = paramConditionalUserProperty.triggeredTimestamp;
    localConditionalUserProperty.mTriggerEventName = paramConditionalUserProperty.triggerEventName;
    localConditionalUserProperty.mTriggerTimeout = paramConditionalUserProperty.triggerTimeout;
    if (paramConditionalUserProperty.value != null) {
      localConditionalUserProperty.mValue = zzfk.zzf(paramConditionalUserProperty.value);
    }
    return localConditionalUserProperty;
  }
  
  public static boolean zzb(@NonNull String paramString1, @NonNull String paramString2, @Nullable Bundle paramBundle)
  {
    if (!"_cmp".equals(paramString2)) {
      return true;
    }
    if (!zzfo(paramString1)) {
      return false;
    }
    if (paramBundle == null) {
      return false;
    }
    paramString2 = zzbsp.iterator();
    while (paramString2.hasNext()) {
      if (paramBundle.containsKey((String)paramString2.next())) {
        return false;
      }
    }
    int i = -1;
    switch (paramString1.hashCode())
    {
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        return false;
        if (paramString1.equals("fcm"))
        {
          i = 0;
          continue;
          if (paramString1.equals("fdl"))
          {
            i = 1;
            continue;
            if (paramString1.equals("fiam")) {
              i = 2;
            }
          }
        }
        break;
      }
    }
    paramBundle.putString("_cis", "fcm_integration");
    return true;
    paramBundle.putString("_cis", "fdl_integration");
    return true;
    paramBundle.putString("_cis", "fiam_integration");
    return true;
  }
  
  public static AnalyticsConnector.ConditionalUserProperty zzd(AppMeasurement.ConditionalUserProperty paramConditionalUserProperty)
  {
    AnalyticsConnector.ConditionalUserProperty localConditionalUserProperty = new AnalyticsConnector.ConditionalUserProperty();
    localConditionalUserProperty.origin = paramConditionalUserProperty.mOrigin;
    localConditionalUserProperty.active = paramConditionalUserProperty.mActive;
    localConditionalUserProperty.creationTimestamp = paramConditionalUserProperty.mCreationTimestamp;
    localConditionalUserProperty.expiredEventName = paramConditionalUserProperty.mExpiredEventName;
    if (paramConditionalUserProperty.mExpiredEventParams != null) {
      localConditionalUserProperty.expiredEventParams = new Bundle(paramConditionalUserProperty.mExpiredEventParams);
    }
    localConditionalUserProperty.name = paramConditionalUserProperty.mName;
    localConditionalUserProperty.timedOutEventName = paramConditionalUserProperty.mTimedOutEventName;
    if (paramConditionalUserProperty.mTimedOutEventParams != null) {
      localConditionalUserProperty.timedOutEventParams = new Bundle(paramConditionalUserProperty.mTimedOutEventParams);
    }
    localConditionalUserProperty.timeToLive = paramConditionalUserProperty.mTimeToLive;
    localConditionalUserProperty.triggeredEventName = paramConditionalUserProperty.mTriggeredEventName;
    if (paramConditionalUserProperty.mTriggeredEventParams != null) {
      localConditionalUserProperty.triggeredEventParams = new Bundle(paramConditionalUserProperty.mTriggeredEventParams);
    }
    localConditionalUserProperty.triggeredTimestamp = paramConditionalUserProperty.mTriggeredTimestamp;
    localConditionalUserProperty.triggerEventName = paramConditionalUserProperty.mTriggerEventName;
    localConditionalUserProperty.triggerTimeout = paramConditionalUserProperty.mTriggerTimeout;
    if (paramConditionalUserProperty.mValue != null) {
      localConditionalUserProperty.value = zzfk.zzf(paramConditionalUserProperty.mValue);
    }
    return localConditionalUserProperty;
  }
  
  public static boolean zzfo(@NonNull String paramString)
  {
    return !zzbso.contains(paramString);
  }
  
  public static boolean zzfp(@NonNull String paramString)
  {
    return !zzbsm.contains(paramString);
  }
  
  public static boolean zzfq(String paramString)
  {
    if (paramString == null) {}
    do
    {
      do
      {
        return false;
      } while (paramString.length() == 0);
      i = paramString.codePointAt(0);
    } while (!Character.isLetter(i));
    int j = paramString.length();
    int i = Character.charCount(i);
    for (;;)
    {
      if (i >= j) {
        break label70;
      }
      int k = paramString.codePointAt(i);
      if ((k != 95) && (!Character.isLetterOrDigit(k))) {
        break;
      }
      i += Character.charCount(k);
    }
    label70:
    return true;
  }
  
  public static boolean zzfr(String paramString)
  {
    if (paramString == null) {}
    do
    {
      do
      {
        return false;
      } while (paramString.length() == 0);
      i = paramString.codePointAt(0);
    } while ((!Character.isLetter(i)) && (i != 95));
    int j = paramString.length();
    int i = Character.charCount(i);
    for (;;)
    {
      if (i >= j) {
        break label76;
      }
      int k = paramString.codePointAt(i);
      if ((k != 95) && (!Character.isLetterOrDigit(k))) {
        break;
      }
      i += Character.charCount(k);
    }
    label76:
    return true;
  }
  
  public static String zzfs(String paramString)
  {
    String str = AppMeasurement.Event.zzak(paramString);
    if (str != null) {
      paramString = str;
    }
    return paramString;
  }
  
  public static String zzft(String paramString)
  {
    String str = AppMeasurement.Event.zzal(paramString);
    if (str != null) {
      paramString = str;
    }
    return paramString;
  }
  
  public static boolean zzy(@NonNull String paramString1, @NonNull String paramString2)
  {
    if (("_ce1".equals(paramString2)) || ("_ce2".equals(paramString2))) {
      return (paramString1.equals("fcm")) || (paramString1.equals("frc"));
    }
    if ("_ln".equals(paramString2)) {
      return (paramString1.equals("fcm")) || (paramString1.equals("fiam"));
    }
    if (zzbsq.contains(paramString2)) {
      return false;
    }
    paramString1 = zzbsr.iterator();
    while (paramString1.hasNext()) {
      if (paramString2.matches((String)paramString1.next())) {
        return false;
      }
    }
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\analytics\connector\internal\zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */