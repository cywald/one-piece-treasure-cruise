package com.google.firebase.analytics.connector.internal;

import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.connector.AnalyticsConnector.AnalyticsConnectorListener;
import java.util.Set;

public final class zzf
  implements zza
{
  private AppMeasurement zzbsg;
  private AnalyticsConnector.AnalyticsConnectorListener zzbst;
  private zzg zzbsw;
  
  public zzf(AppMeasurement paramAppMeasurement, AnalyticsConnector.AnalyticsConnectorListener paramAnalyticsConnectorListener)
  {
    this.zzbst = paramAnalyticsConnectorListener;
    this.zzbsg = paramAppMeasurement;
    this.zzbsw = new zzg(this);
    this.zzbsg.registerOnMeasurementEventListener(this.zzbsw);
  }
  
  public final void registerEventNames(Set<String> paramSet) {}
  
  public final void unregisterEventNames() {}
  
  public final AnalyticsConnector.AnalyticsConnectorListener zztl()
  {
    return this.zzbst;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\analytics\connector\internal\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */