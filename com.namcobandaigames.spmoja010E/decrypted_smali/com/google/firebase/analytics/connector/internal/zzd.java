package com.google.firebase.analytics.connector.internal;

import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.connector.AnalyticsConnector.AnalyticsConnectorListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class zzd
  implements zza
{
  private AppMeasurement zzbsg;
  Set<String> zzbss;
  private AnalyticsConnector.AnalyticsConnectorListener zzbst;
  private zze zzbsu;
  
  public zzd(AppMeasurement paramAppMeasurement, AnalyticsConnector.AnalyticsConnectorListener paramAnalyticsConnectorListener)
  {
    this.zzbst = paramAnalyticsConnectorListener;
    this.zzbsg = paramAppMeasurement;
    this.zzbsu = new zze(this);
    this.zzbsg.registerOnMeasurementEventListener(this.zzbsu);
    this.zzbss = new HashSet();
  }
  
  public final void registerEventNames(Set<String> paramSet)
  {
    this.zzbss.clear();
    Set localSet = this.zzbss;
    HashSet localHashSet = new HashSet();
    paramSet = paramSet.iterator();
    while (paramSet.hasNext())
    {
      String str = (String)paramSet.next();
      if (localHashSet.size() >= 50) {
        break;
      }
      if ((zzc.zzfr(str)) && (zzc.zzfq(str))) {
        localHashSet.add(zzc.zzft(str));
      }
    }
    localSet.addAll(localHashSet);
  }
  
  public final void unregisterEventNames()
  {
    this.zzbss.clear();
  }
  
  public final AnalyticsConnector.AnalyticsConnectorListener zztl()
  {
    return this.zzbst;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\analytics\connector\internal\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */