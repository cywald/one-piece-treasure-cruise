package com.google.firebase;

import android.support.annotation.NonNull;
import com.google.firebase.annotations.PublicApi;

@PublicApi
public class FirebaseNetworkException
  extends FirebaseException
{
  @PublicApi
  public FirebaseNetworkException(@NonNull String paramString)
  {
    super(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\FirebaseNetworkException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */