package com.google.firebase.auth;

import android.support.annotation.Nullable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.firebase.annotations.PublicApi;
import java.util.Map;

@PublicApi
public class GetTokenResult
{
  private String zza;
  private Map<String, Object> zzb;
  
  @KeepForSdk
  public GetTokenResult(String paramString, Map<String, Object> paramMap)
  {
    this.zza = paramString;
    this.zzb = paramMap;
  }
  
  private long zza(String paramString)
  {
    paramString = (Integer)this.zzb.get(paramString);
    if (paramString == null) {
      return 0L;
    }
    return paramString.longValue();
  }
  
  @PublicApi
  public long getAuthTimestamp()
  {
    return zza("auth_time");
  }
  
  @PublicApi
  public Map<String, Object> getClaims()
  {
    return this.zzb;
  }
  
  @PublicApi
  public long getExpirationTimestamp()
  {
    return zza("exp");
  }
  
  @PublicApi
  public long getIssuedAtTimestamp()
  {
    return zza("iat");
  }
  
  @Nullable
  @PublicApi
  public String getSignInProvider()
  {
    Map localMap = (Map)this.zzb.get("firebase");
    if (localMap != null) {
      return (String)localMap.get("sign_in_provider");
    }
    return null;
  }
  
  @Nullable
  @PublicApi
  public String getToken()
  {
    return this.zza;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\auth\GetTokenResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */