package com.google.firebase.messaging;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirebaseMessaging
{
  public static final String INSTANCE_ID_SCOPE = "FCM";
  private static final Pattern zzdp = Pattern.compile("[a-zA-Z0-9-_.~%]{1,900}");
  private static FirebaseMessaging zzdq;
  private final FirebaseInstanceId zzdj;
  
  private FirebaseMessaging(FirebaseInstanceId paramFirebaseInstanceId)
  {
    this.zzdj = paramFirebaseInstanceId;
  }
  
  public static FirebaseMessaging getInstance()
  {
    try
    {
      if (zzdq == null) {
        zzdq = new FirebaseMessaging(FirebaseInstanceId.getInstance());
      }
      FirebaseMessaging localFirebaseMessaging = zzdq;
      return localFirebaseMessaging;
    }
    finally {}
  }
  
  public boolean isAutoInitEnabled()
  {
    return this.zzdj.zzr();
  }
  
  public void send(RemoteMessage paramRemoteMessage)
  {
    if (TextUtils.isEmpty(paramRemoteMessage.getTo())) {
      throw new IllegalArgumentException("Missing 'to'");
    }
    Context localContext = FirebaseApp.getInstance().getApplicationContext();
    Intent localIntent1 = new Intent("com.google.android.gcm.intent.SEND");
    Intent localIntent2 = new Intent();
    localIntent2.setPackage("com.google.example.invalidpackage");
    localIntent1.putExtra("app", PendingIntent.getBroadcast(localContext, 0, localIntent2, 0));
    localIntent1.setPackage("com.google.android.gms");
    localIntent1.putExtras(paramRemoteMessage.zzds);
    localContext.sendOrderedBroadcast(localIntent1, "com.google.android.gtalkservice.permission.GTALK_SERVICE");
  }
  
  public void setAutoInitEnabled(boolean paramBoolean)
  {
    this.zzdj.zzb(paramBoolean);
  }
  
  public Task<Void> subscribeToTopic(String paramString)
  {
    String str = paramString;
    if (paramString != null)
    {
      str = paramString;
      if (paramString.startsWith("/topics/"))
      {
        Log.w("FirebaseMessaging", "Format /topics/topic-name is deprecated. Only 'topic-name' should be used in subscribeToTopic.");
        str = paramString.substring(8);
      }
    }
    if ((str == null) || (!zzdp.matcher(str).matches())) {
      throw new IllegalArgumentException(String.valueOf(str).length() + 78 + "Invalid topic name: " + str + " does not match the allowed format [a-zA-Z0-9-_.~%]{1,900}");
    }
    FirebaseInstanceId localFirebaseInstanceId = this.zzdj;
    paramString = String.valueOf("S!");
    str = String.valueOf(str);
    if (str.length() != 0) {}
    for (paramString = paramString.concat(str);; paramString = new String(paramString)) {
      return localFirebaseInstanceId.zza(paramString);
    }
  }
  
  public Task<Void> unsubscribeFromTopic(String paramString)
  {
    String str = paramString;
    if (paramString != null)
    {
      str = paramString;
      if (paramString.startsWith("/topics/"))
      {
        Log.w("FirebaseMessaging", "Format /topics/topic-name is deprecated. Only 'topic-name' should be used in unsubscribeFromTopic.");
        str = paramString.substring(8);
      }
    }
    if ((str == null) || (!zzdp.matcher(str).matches())) {
      throw new IllegalArgumentException(String.valueOf(str).length() + 78 + "Invalid topic name: " + str + " does not match the allowed format [a-zA-Z0-9-_.~%]{1,900}");
    }
    FirebaseInstanceId localFirebaseInstanceId = this.zzdj;
    paramString = String.valueOf("U!");
    str = String.valueOf(str);
    if (str.length() != 0) {}
    for (paramString = paramString.concat(str);; paramString = new String(paramString)) {
      return localFirebaseInstanceId.zza(paramString);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\messaging\FirebaseMessaging.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */