package com.google.firebase.messaging;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.connector.AnalyticsConnector;

@KeepForSdk
public class MessagingAnalytics
{
  @KeepForSdk
  public static void logNotificationDismiss(Intent paramIntent)
  {
    zza("_nd", paramIntent);
  }
  
  @KeepForSdk
  public static void logNotificationForeground(Intent paramIntent)
  {
    zza("_nf", paramIntent);
  }
  
  @KeepForSdk
  public static void logNotificationOpen(Intent paramIntent)
  {
    if (paramIntent != null)
    {
      if (!"1".equals(paramIntent.getStringExtra("google.c.a.tc"))) {
        break label129;
      }
      AnalyticsConnector localAnalyticsConnector = (AnalyticsConnector)FirebaseApp.getInstance().get(AnalyticsConnector.class);
      if (Log.isLoggable("FirebaseMessaging", 3)) {
        Log.d("FirebaseMessaging", "Received event with track-conversion=true. Setting user property and reengagement event");
      }
      if (localAnalyticsConnector == null) {
        break label118;
      }
      String str = paramIntent.getStringExtra("google.c.a.c_id");
      localAnalyticsConnector.setUserProperty("fcm", "_ln", str);
      Bundle localBundle = new Bundle();
      localBundle.putString("source", "Firebase");
      localBundle.putString("medium", "notification");
      localBundle.putString("campaign", str);
      localAnalyticsConnector.logEvent("fcm", "_cmp", localBundle);
    }
    for (;;)
    {
      zza("_no", paramIntent);
      return;
      label118:
      Log.w("FirebaseMessaging", "Unable to set user property for conversion tracking:  analytics library is missing");
      continue;
      label129:
      if (Log.isLoggable("FirebaseMessaging", 3)) {
        Log.d("FirebaseMessaging", "Received event with track-conversion=false. Do not set user property");
      }
    }
  }
  
  @KeepForSdk
  public static void logNotificationReceived(Intent paramIntent)
  {
    zza("_nr", paramIntent);
  }
  
  @KeepForSdk
  public static boolean shouldUploadMetrics(Intent paramIntent)
  {
    if (paramIntent == null) {}
    while ("com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(paramIntent.getAction())) {
      return false;
    }
    return "1".equals(paramIntent.getStringExtra("google.c.a.e"));
  }
  
  private static void zza(String paramString, Intent paramIntent)
  {
    Bundle localBundle = new Bundle();
    String str = paramIntent.getStringExtra("google.c.a.c_id");
    if (str != null) {
      localBundle.putString("_nmid", str);
    }
    str = paramIntent.getStringExtra("google.c.a.c_l");
    if (str != null) {
      localBundle.putString("_nmn", str);
    }
    str = paramIntent.getStringExtra("google.c.a.m_l");
    if (!TextUtils.isEmpty(str)) {
      localBundle.putString("label", str);
    }
    str = paramIntent.getStringExtra("google.c.a.m_c");
    if (!TextUtils.isEmpty(str)) {
      localBundle.putString("message_channel", str);
    }
    str = paramIntent.getStringExtra("from");
    if ((str != null) && (str.startsWith("/topics/"))) {}
    for (;;)
    {
      if (str != null) {
        localBundle.putString("_nt", str);
      }
      if (paramIntent.hasExtra("google.c.a.ts")) {}
      try
      {
        localBundle.putInt("_nmt", Integer.parseInt(paramIntent.getStringExtra("google.c.a.ts")));
        if (!paramIntent.hasExtra("google.c.a.udt")) {}
      }
      catch (NumberFormatException localNumberFormatException)
      {
        try
        {
          localBundle.putInt("_ndt", Integer.parseInt(paramIntent.getStringExtra("google.c.a.udt")));
          if (Log.isLoggable("FirebaseMessaging", 3))
          {
            paramIntent = String.valueOf(localBundle);
            Log.d("FirebaseMessaging", String.valueOf(paramString).length() + 22 + String.valueOf(paramIntent).length() + "Sending event=" + paramString + " params=" + paramIntent);
          }
          paramIntent = (AnalyticsConnector)FirebaseApp.getInstance().get(AnalyticsConnector.class);
          if (paramIntent != null)
          {
            paramIntent.logEvent("fcm", paramString, localBundle);
            return;
            str = null;
            continue;
            localNumberFormatException = localNumberFormatException;
            Log.w("FirebaseMessaging", "Error while parsing timestamp in GCM event", localNumberFormatException);
          }
        }
        catch (NumberFormatException paramIntent)
        {
          for (;;)
          {
            Log.w("FirebaseMessaging", "Error while parsing use_device_time in GCM event", paramIntent);
          }
          Log.w("FirebaseMessaging", "Unable to log event: analytics library is missing");
        }
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\messaging\MessagingAnalytics.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */