package com.google.firebase.messaging;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Process;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.firebase.iid.zzav;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.MissingFormatArgumentException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;

final class zza
{
  private static final AtomicInteger zzdn = new AtomicInteger((int)SystemClock.elapsedRealtime());
  private Bundle zzdo;
  private final Context zzx;
  
  public zza(Context paramContext)
  {
    this.zzx = paramContext.getApplicationContext();
  }
  
  static String zza(Bundle paramBundle, String paramString)
  {
    String str2 = paramBundle.getString(paramString);
    String str1 = str2;
    if (str2 == null) {
      str1 = paramBundle.getString(paramString.replace("gcm.n.", "gcm.notification."));
    }
    return str1;
  }
  
  private static void zza(Intent paramIntent, Bundle paramBundle)
  {
    Iterator localIterator = paramBundle.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if ((str.startsWith("google.c.a.")) || (str.equals("from"))) {
        paramIntent.putExtra(str, paramBundle.getString(str));
      }
    }
  }
  
  private final Bundle zzas()
  {
    if (this.zzdo != null) {
      return this.zzdo;
    }
    Object localObject = null;
    try
    {
      ApplicationInfo localApplicationInfo = this.zzx.getPackageManager().getApplicationInfo(this.zzx.getPackageName(), 128);
      localObject = localApplicationInfo;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;) {}
    }
    if ((localObject != null) && (((ApplicationInfo)localObject).metaData != null))
    {
      this.zzdo = ((ApplicationInfo)localObject).metaData;
      return this.zzdo;
    }
    return Bundle.EMPTY;
  }
  
  static String zzb(Bundle paramBundle, String paramString)
  {
    paramString = String.valueOf(paramString);
    String str = String.valueOf("_loc_key");
    if (str.length() != 0) {}
    for (paramString = paramString.concat(str);; paramString = new String(paramString)) {
      return zza(paramBundle, paramString);
    }
  }
  
  @TargetApi(26)
  private final boolean zzb(int paramInt)
  {
    if (Build.VERSION.SDK_INT != 26) {}
    for (;;)
    {
      return true;
      try
      {
        if ((this.zzx.getResources().getDrawable(paramInt, null) instanceof AdaptiveIconDrawable))
        {
          Log.e("FirebaseMessaging", 77 + "Adaptive icons cannot be used in notifications. Ignoring icon id: " + paramInt);
          return false;
        }
      }
      catch (Resources.NotFoundException localNotFoundException) {}
    }
    return false;
  }
  
  static Object[] zzc(Bundle paramBundle, String paramString)
  {
    Object localObject = String.valueOf(paramString);
    String str = String.valueOf("_loc_args");
    if (str.length() != 0)
    {
      localObject = ((String)localObject).concat(str);
      str = zza(paramBundle, (String)localObject);
      if (!TextUtils.isEmpty(str)) {
        break label58;
      }
      paramBundle = null;
    }
    for (;;)
    {
      return paramBundle;
      localObject = new String((String)localObject);
      break;
      try
      {
        label58:
        JSONArray localJSONArray = new JSONArray(str);
        localObject = new String[localJSONArray.length()];
        int i = 0;
        for (;;)
        {
          paramBundle = (Bundle)localObject;
          if (i >= localObject.length) {
            break;
          }
          localObject[i] = localJSONArray.opt(i);
          i += 1;
        }
        paramBundle = paramBundle.concat(paramString);
      }
      catch (JSONException paramBundle)
      {
        paramBundle = String.valueOf(paramString);
        paramString = String.valueOf("_loc_args");
        if (paramString.length() == 0) {}
      }
    }
    for (;;)
    {
      paramBundle = paramBundle.substring(6);
      Log.w("FirebaseMessaging", String.valueOf(paramBundle).length() + 41 + String.valueOf(str).length() + "Malformed " + paramBundle + ": " + str + "  Default value will be used.");
      return null;
      paramBundle = new String(paramBundle);
    }
  }
  
  private final String zzd(Bundle paramBundle, String paramString)
  {
    String str = zza(paramBundle, paramString);
    if (!TextUtils.isEmpty(str)) {
      return str;
    }
    str = zzb(paramBundle, paramString);
    if (TextUtils.isEmpty(str)) {
      return null;
    }
    Resources localResources = this.zzx.getResources();
    int i = localResources.getIdentifier(str, "string", this.zzx.getPackageName());
    if (i == 0)
    {
      paramBundle = String.valueOf(paramString);
      paramString = String.valueOf("_loc_key");
      if (paramString.length() != 0) {}
      for (paramBundle = paramBundle.concat(paramString);; paramBundle = new String(paramBundle))
      {
        paramBundle = paramBundle.substring(6);
        Log.w("FirebaseMessaging", String.valueOf(paramBundle).length() + 49 + String.valueOf(str).length() + paramBundle + " resource not found: " + str + " Default value will be used.");
        return null;
      }
    }
    paramBundle = zzc(paramBundle, paramString);
    if (paramBundle == null) {
      return localResources.getString(i);
    }
    try
    {
      paramString = localResources.getString(i, paramBundle);
      return paramString;
    }
    catch (MissingFormatArgumentException paramString)
    {
      paramBundle = Arrays.toString(paramBundle);
      Log.w("FirebaseMessaging", String.valueOf(str).length() + 58 + String.valueOf(paramBundle).length() + "Missing format argument for " + str + ": " + paramBundle + " Default value will be used.", paramString);
    }
    return null;
  }
  
  static boolean zzf(Bundle paramBundle)
  {
    return ("1".equals(zza(paramBundle, "gcm.n.e"))) || (zza(paramBundle, "gcm.n.icon") != null);
  }
  
  @Nullable
  static Uri zzg(@NonNull Bundle paramBundle)
  {
    String str2 = zza(paramBundle, "gcm.n.link_android");
    String str1 = str2;
    if (TextUtils.isEmpty(str2)) {
      str1 = zza(paramBundle, "gcm.n.link");
    }
    if (!TextUtils.isEmpty(str1)) {
      return Uri.parse(str1);
    }
    return null;
  }
  
  static String zzi(Bundle paramBundle)
  {
    String str2 = zza(paramBundle, "gcm.n.sound2");
    String str1 = str2;
    if (TextUtils.isEmpty(str2)) {
      str1 = zza(paramBundle, "gcm.n.sound");
    }
    return str1;
  }
  
  private final Integer zzl(String paramString)
  {
    if (Build.VERSION.SDK_INT < 21) {}
    int i;
    do
    {
      return null;
      if (!TextUtils.isEmpty(paramString)) {
        try
        {
          i = Color.parseColor(paramString);
          return Integer.valueOf(i);
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          Log.w("FirebaseMessaging", String.valueOf(paramString).length() + 54 + "Color " + paramString + " not valid. Notification will use default color.");
        }
      }
      i = zzas().getInt("com.google.firebase.messaging.default_notification_color", 0);
    } while (i == 0);
    try
    {
      i = ContextCompat.getColor(this.zzx, i);
      return Integer.valueOf(i);
    }
    catch (Resources.NotFoundException paramString)
    {
      Log.w("FirebaseMessaging", "Cannot find the color resource referenced in AndroidManifest.");
    }
    return null;
  }
  
  final boolean zzh(Bundle paramBundle)
  {
    if ("1".equals(zza(paramBundle, "gcm.n.noui"))) {
      return true;
    }
    int i;
    Object localObject1;
    Object localObject2;
    if (!((KeyguardManager)this.zzx.getSystemService("keyguard")).inKeyguardRestrictedInputMode())
    {
      if (!PlatformVersion.isAtLeastLollipop()) {
        SystemClock.sleep(10L);
      }
      i = Process.myPid();
      localObject1 = ((ActivityManager)this.zzx.getSystemService("activity")).getRunningAppProcesses();
      if (localObject1 != null)
      {
        localObject1 = ((List)localObject1).iterator();
        for (;;)
        {
          if (((Iterator)localObject1).hasNext())
          {
            localObject2 = (ActivityManager.RunningAppProcessInfo)((Iterator)localObject1).next();
            if (((ActivityManager.RunningAppProcessInfo)localObject2).pid == i) {
              if (((ActivityManager.RunningAppProcessInfo)localObject2).importance == 100) {
                i = 1;
              }
            }
          }
        }
      }
    }
    while (i != 0)
    {
      return false;
      i = 0;
      continue;
      i = 0;
    }
    Object localObject3 = zzd(paramBundle, "gcm.n.title");
    if (TextUtils.isEmpty((CharSequence)localObject3)) {
      localObject3 = this.zzx.getApplicationInfo().loadLabel(this.zzx.getPackageManager());
    }
    for (;;)
    {
      String str = zzd(paramBundle, "gcm.n.body");
      localObject1 = zza(paramBundle, "gcm.n.icon");
      Integer localInteger;
      label277:
      label327:
      label335:
      boolean bool;
      label342:
      Object localObject4;
      Object localObject5;
      if (!TextUtils.isEmpty((CharSequence)localObject1))
      {
        localObject2 = this.zzx.getResources();
        i = ((Resources)localObject2).getIdentifier((String)localObject1, "drawable", this.zzx.getPackageName());
        if ((i != 0) && (zzb(i)))
        {
          localInteger = zzl(zza(paramBundle, "gcm.n.color"));
          localObject1 = zzi(paramBundle);
          if (!TextUtils.isEmpty((CharSequence)localObject1)) {
            break label843;
          }
          localObject2 = null;
          localObject1 = zza(paramBundle, "gcm.n.click_action");
          if (TextUtils.isEmpty((CharSequence)localObject1)) {
            break label957;
          }
          localObject1 = new Intent((String)localObject1);
          ((Intent)localObject1).setPackage(this.zzx.getPackageName());
          ((Intent)localObject1).setFlags(268435456);
          if (localObject1 != null) {
            break label1040;
          }
          localObject1 = null;
          if (paramBundle != null) {
            break label1159;
          }
          bool = false;
          if (!bool) {
            break label1356;
          }
          localObject4 = new Intent("com.google.firebase.messaging.NOTIFICATION_OPEN");
          zza((Intent)localObject4, paramBundle);
          ((Intent)localObject4).putExtra("pending_intent", (Parcelable)localObject1);
          localObject5 = zzav.zza(this.zzx, zzdn.incrementAndGet(), (Intent)localObject4, 1073741824);
          localObject1 = new Intent("com.google.firebase.messaging.NOTIFICATION_DISMISS");
          zza((Intent)localObject1, paramBundle);
          localObject4 = zzav.zza(this.zzx, zzdn.incrementAndGet(), (Intent)localObject1, 1073741824);
        }
      }
      for (;;)
      {
        localObject1 = zza(paramBundle, "gcm.n.android_channel_id");
        if ((!PlatformVersion.isAtLeastO()) || (this.zzx.getApplicationInfo().targetSdkVersion < 26)) {
          localObject1 = null;
        }
        Object localObject6;
        label843:
        label957:
        label1040:
        label1159:
        label1257:
        do
        {
          do
          {
            localObject6 = new NotificationCompat.Builder(this.zzx).setAutoCancel(true).setSmallIcon(i);
            if (!TextUtils.isEmpty((CharSequence)localObject3)) {
              ((NotificationCompat.Builder)localObject6).setContentTitle((CharSequence)localObject3);
            }
            if (!TextUtils.isEmpty(str))
            {
              ((NotificationCompat.Builder)localObject6).setContentText(str);
              ((NotificationCompat.Builder)localObject6).setStyle(new NotificationCompat.BigTextStyle().bigText(str));
            }
            if (localInteger != null) {
              ((NotificationCompat.Builder)localObject6).setColor(localInteger.intValue());
            }
            if (localObject2 != null) {
              ((NotificationCompat.Builder)localObject6).setSound((Uri)localObject2);
            }
            if (localObject5 != null) {
              ((NotificationCompat.Builder)localObject6).setContentIntent((PendingIntent)localObject5);
            }
            if (localObject4 != null) {
              ((NotificationCompat.Builder)localObject6).setDeleteIntent((PendingIntent)localObject4);
            }
            if (localObject1 != null) {
              ((NotificationCompat.Builder)localObject6).setChannelId((String)localObject1);
            }
            localObject2 = ((NotificationCompat.Builder)localObject6).build();
            localObject1 = zza(paramBundle, "gcm.n.tag");
            if (Log.isLoggable("FirebaseMessaging", 3)) {
              Log.d("FirebaseMessaging", "Showing notification");
            }
            localObject3 = (NotificationManager)this.zzx.getSystemService("notification");
            paramBundle = (Bundle)localObject1;
            if (TextUtils.isEmpty((CharSequence)localObject1))
            {
              long l = SystemClock.uptimeMillis();
              paramBundle = 37 + "FCM-Notification:" + l;
            }
            ((NotificationManager)localObject3).notify(paramBundle, 0, (Notification)localObject2);
            return true;
            i = ((Resources)localObject2).getIdentifier((String)localObject1, "mipmap", this.zzx.getPackageName());
            if ((i != 0) && (zzb(i))) {
              break;
            }
            Log.w("FirebaseMessaging", String.valueOf(localObject1).length() + 61 + "Icon resource " + (String)localObject1 + " not found. Notification will use default icon.");
            int j = zzas().getInt("com.google.firebase.messaging.default_notification_icon", 0);
            if (j != 0)
            {
              i = j;
              if (zzb(j)) {}
            }
            else
            {
              i = this.zzx.getApplicationInfo().icon;
            }
            if (i != 0)
            {
              j = i;
              if (zzb(i)) {}
            }
            else
            {
              j = 17301651;
            }
            i = j;
            break;
            if ((!"default".equals(localObject1)) && (this.zzx.getResources().getIdentifier((String)localObject1, "raw", this.zzx.getPackageName()) != 0))
            {
              localObject2 = this.zzx.getPackageName();
              localObject2 = Uri.parse(String.valueOf(localObject2).length() + 24 + String.valueOf(localObject1).length() + "android.resource://" + (String)localObject2 + "/raw/" + (String)localObject1);
              break label277;
            }
            localObject2 = RingtoneManager.getDefaultUri(2);
            break label277;
            localObject4 = zzg(paramBundle);
            if (localObject4 != null)
            {
              localObject1 = new Intent("android.intent.action.VIEW");
              ((Intent)localObject1).setPackage(this.zzx.getPackageName());
              ((Intent)localObject1).setData((Uri)localObject4);
              break label327;
            }
            localObject1 = this.zzx.getPackageManager().getLaunchIntentForPackage(this.zzx.getPackageName());
            if (localObject1 == null) {
              Log.w("FirebaseMessaging", "No activity found to launch app");
            }
            break label327;
            ((Intent)localObject1).addFlags(67108864);
            localObject4 = new Bundle(paramBundle);
            FirebaseMessagingService.zzj((Bundle)localObject4);
            ((Intent)localObject1).putExtras((Bundle)localObject4);
            localObject4 = ((Bundle)localObject4).keySet().iterator();
            while (((Iterator)localObject4).hasNext())
            {
              localObject5 = (String)((Iterator)localObject4).next();
              if ((((String)localObject5).startsWith("gcm.n.")) || (((String)localObject5).startsWith("gcm.notification."))) {
                ((Intent)localObject1).removeExtra((String)localObject5);
              }
            }
            localObject1 = PendingIntent.getActivity(this.zzx, zzdn.incrementAndGet(), (Intent)localObject1, 1073741824);
            break label335;
            bool = "1".equals(paramBundle.getString("google.c.a.e"));
            break label342;
            localObject6 = (NotificationManager)this.zzx.getSystemService(NotificationManager.class);
            if (TextUtils.isEmpty((CharSequence)localObject1)) {
              break label1257;
            }
          } while (((NotificationManager)localObject6).getNotificationChannel((String)localObject1) != null);
          Log.w("FirebaseMessaging", String.valueOf(localObject1).length() + 122 + "Notification Channel requested (" + (String)localObject1 + ") has not been created by the app. Manifest configuration, or default, value will be used.");
          localObject1 = zzas().getString("com.google.firebase.messaging.default_notification_channel_id");
          if (TextUtils.isEmpty((CharSequence)localObject1)) {
            break label1344;
          }
        } while (((NotificationManager)localObject6).getNotificationChannel((String)localObject1) != null);
        Log.w("FirebaseMessaging", "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.");
        for (;;)
        {
          if (((NotificationManager)localObject6).getNotificationChannel("fcm_fallback_notification_channel") == null) {
            ((NotificationManager)localObject6).createNotificationChannel(new NotificationChannel("fcm_fallback_notification_channel", this.zzx.getString(R.string.fcm_fallback_notification_channel_label), 3));
          }
          localObject1 = "fcm_fallback_notification_channel";
          break;
          label1344:
          Log.w("FirebaseMessaging", "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.");
        }
        label1356:
        localObject4 = null;
        localObject5 = localObject1;
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\messaging\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */