package com.google.firebase.messaging;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.iid.zzab;
import com.google.firebase.iid.zzav;
import com.google.firebase.iid.zzb;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class FirebaseMessagingService
  extends zzb
{
  private static final Queue<String> zzdr = new ArrayDeque(10);
  
  static void zzj(Bundle paramBundle)
  {
    paramBundle = paramBundle.keySet().iterator();
    while (paramBundle.hasNext())
    {
      String str = (String)paramBundle.next();
      if ((str != null) && (str.startsWith("google.c."))) {
        paramBundle.remove();
      }
    }
  }
  
  @WorkerThread
  public void onDeletedMessages() {}
  
  @WorkerThread
  public void onMessageReceived(RemoteMessage paramRemoteMessage) {}
  
  @WorkerThread
  public void onMessageSent(String paramString) {}
  
  @WorkerThread
  public void onNewToken(String paramString) {}
  
  @WorkerThread
  public void onSendError(String paramString, Exception paramException) {}
  
  protected final Intent zzb(Intent paramIntent)
  {
    return zzav.zzai().zzaj();
  }
  
  public final boolean zzc(Intent paramIntent)
  {
    if ("com.google.firebase.messaging.NOTIFICATION_OPEN".equals(paramIntent.getAction()))
    {
      PendingIntent localPendingIntent = (PendingIntent)paramIntent.getParcelableExtra("pending_intent");
      if (localPendingIntent != null) {}
      try
      {
        localPendingIntent.send();
        if (MessagingAnalytics.shouldUploadMetrics(paramIntent)) {
          MessagingAnalytics.logNotificationOpen(paramIntent);
        }
        return true;
      }
      catch (PendingIntent.CanceledException localCanceledException)
      {
        for (;;)
        {
          Log.e("FirebaseMessaging", "Notification pending intent canceled");
        }
      }
    }
    return false;
  }
  
  public final void zzd(Intent paramIntent)
  {
    int j = 0;
    Object localObject1 = paramIntent.getAction();
    Object localObject2;
    if (("com.google.android.c2dm.intent.RECEIVE".equals(localObject1)) || ("com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(localObject1)))
    {
      localObject2 = paramIntent.getStringExtra("google.message_id");
      if (TextUtils.isEmpty((CharSequence)localObject2)) {
        localObject1 = Tasks.forResult(null);
      }
    }
    for (;;)
    {
      int i;
      label60:
      Object localObject3;
      if (TextUtils.isEmpty((CharSequence)localObject2))
      {
        i = 0;
        if (i == 0)
        {
          localObject3 = paramIntent.getStringExtra("message_type");
          localObject2 = localObject3;
          if (localObject3 == null) {
            localObject2 = "gcm";
          }
          switch (((String)localObject2).hashCode())
          {
          default: 
            label132:
            i = -1;
            switch (i)
            {
            default: 
              label134:
              paramIntent = String.valueOf(localObject2);
              if (paramIntent.length() != 0)
              {
                paramIntent = "Received message with unknown type: ".concat(paramIntent);
                Log.w("FirebaseMessaging", paramIntent);
              }
              break;
            }
            break;
          }
        }
      }
      try
      {
        for (;;)
        {
          Tasks.await((Task)localObject1, 1L, TimeUnit.SECONDS);
          return;
          localObject1 = new Bundle();
          ((Bundle)localObject1).putString("google.message_id", (String)localObject2);
          localObject1 = zzab.zzc(this).zza(2, (Bundle)localObject1);
          break;
          if (zzdr.contains(localObject2))
          {
            if (Log.isLoggable("FirebaseMessaging", 3))
            {
              localObject2 = String.valueOf(localObject2);
              if (((String)localObject2).length() == 0) {
                break label294;
              }
            }
            label294:
            for (localObject2 = "Received duplicate message: ".concat((String)localObject2);; localObject2 = new String("Received duplicate message: "))
            {
              Log.d("FirebaseMessaging", (String)localObject2);
              i = 1;
              break;
            }
          }
          if (zzdr.size() >= 10) {
            zzdr.remove();
          }
          zzdr.add(localObject2);
          i = 0;
          break label60;
          if (!((String)localObject2).equals("gcm")) {
            break label132;
          }
          i = j;
          break label134;
          if (!((String)localObject2).equals("deleted_messages")) {
            break label132;
          }
          i = 1;
          break label134;
          if (!((String)localObject2).equals("send_event")) {
            break label132;
          }
          i = 2;
          break label134;
          if (!((String)localObject2).equals("send_error")) {
            break label132;
          }
          i = 3;
          break label134;
          if (MessagingAnalytics.shouldUploadMetrics(paramIntent)) {
            MessagingAnalytics.logNotificationReceived(paramIntent);
          }
          localObject3 = paramIntent.getExtras();
          localObject2 = localObject3;
          if (localObject3 == null) {
            localObject2 = new Bundle();
          }
          ((Bundle)localObject2).remove("android.support.content.wakelockid");
          if (zza.zzf((Bundle)localObject2))
          {
            if (new zza(this).zzh((Bundle)localObject2)) {
              continue;
            }
            if (MessagingAnalytics.shouldUploadMetrics(paramIntent)) {
              MessagingAnalytics.logNotificationForeground(paramIntent);
            }
          }
          onMessageReceived(new RemoteMessage((Bundle)localObject2));
          continue;
          onDeletedMessages();
          continue;
          onMessageSent(paramIntent.getStringExtra("google.message_id"));
          continue;
          localObject3 = paramIntent.getStringExtra("google.message_id");
          localObject2 = localObject3;
          if (localObject3 == null) {
            localObject2 = paramIntent.getStringExtra("message_id");
          }
          onSendError((String)localObject2, new SendException(paramIntent.getStringExtra("error")));
        }
        paramIntent = new String("Received message with unknown type: ");
      }
      catch (InterruptedException paramIntent)
      {
        do
        {
          paramIntent = String.valueOf(paramIntent);
          Log.w("FirebaseMessaging", String.valueOf(paramIntent).length() + 20 + "Message ack failed: " + paramIntent);
          return;
          if (!"com.google.firebase.messaging.NOTIFICATION_DISMISS".equals(localObject1)) {
            break;
          }
        } while (!MessagingAnalytics.shouldUploadMetrics(paramIntent));
        MessagingAnalytics.logNotificationDismiss(paramIntent);
        return;
        if ("com.google.firebase.messaging.NEW_TOKEN".equals(localObject1))
        {
          onNewToken(paramIntent.getStringExtra("token"));
          return;
        }
        paramIntent = String.valueOf(paramIntent.getAction());
        if (paramIntent.length() != 0) {}
        for (paramIntent = "Unknown intent action: ".concat(paramIntent);; paramIntent = new String("Unknown intent action: "))
        {
          Log.d("FirebaseMessaging", paramIntent);
          return;
        }
      }
      catch (ExecutionException paramIntent)
      {
        for (;;) {}
      }
      catch (TimeoutException paramIntent)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\messaging\FirebaseMessagingService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */