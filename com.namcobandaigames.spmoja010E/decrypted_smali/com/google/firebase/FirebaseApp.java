package com.google.firebase;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.internal.BackgroundDetector;
import com.google.android.gms.common.api.internal.BackgroundDetector.BackgroundStateChangeListener;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Objects.ToStringHelper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.gms.common.util.ProcessUtils;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.annotations.PublicApi;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.components.Component;
import com.google.firebase.components.Component.1;
import com.google.firebase.components.zzf;
import com.google.firebase.events.Event;
import com.google.firebase.events.Publisher;
import com.google.firebase.internal.InternalTokenProvider;
import com.google.firebase.internal.InternalTokenResult;
import com.google.firebase.internal.zza;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.concurrent.GuardedBy;

@PublicApi
public class FirebaseApp
{
  @GuardedBy("LOCK")
  static final Map<String, FirebaseApp> zza = new ArrayMap();
  private static final List<String> zzb = Arrays.asList(new String[] { "com.google.firebase.auth.FirebaseAuth", "com.google.firebase.iid.FirebaseInstanceId" });
  private static final List<String> zzc = Collections.singletonList("com.google.firebase.crash.FirebaseCrash");
  private static final List<String> zzd = Arrays.asList(new String[] { "com.google.android.gms.measurement.AppMeasurement" });
  private static final List<String> zze = Arrays.asList(new String[0]);
  private static final Set<String> zzf = Collections.emptySet();
  private static final Object zzg = new Object();
  private static final Executor zzh = new zzb((byte)0);
  private final Context zzi;
  private final String zzj;
  private final FirebaseOptions zzk;
  private final zzf zzl;
  private final SharedPreferences zzm;
  private final Publisher zzn;
  private final AtomicBoolean zzo = new AtomicBoolean(false);
  private final AtomicBoolean zzp = new AtomicBoolean();
  private final AtomicBoolean zzq;
  private final List<IdTokenListener> zzr = new CopyOnWriteArrayList();
  private final List<BackgroundStateChangeListener> zzs = new CopyOnWriteArrayList();
  private final List<FirebaseAppLifecycleListener> zzt = new CopyOnWriteArrayList();
  private InternalTokenProvider zzu;
  private IdTokenListenersCountChangedListener zzv;
  
  private FirebaseApp(Context paramContext, String paramString, FirebaseOptions paramFirebaseOptions)
  {
    this.zzi = ((Context)Preconditions.checkNotNull(paramContext));
    this.zzj = Preconditions.checkNotEmpty(paramString);
    this.zzk = ((FirebaseOptions)Preconditions.checkNotNull(paramFirebaseOptions));
    this.zzv = new zza();
    this.zzm = paramContext.getSharedPreferences("com.google.firebase.common.prefs", 0);
    this.zzq = new AtomicBoolean(zzb());
    paramString = Component.1.zza(paramContext).zza();
    this.zzl = new zzf(zzh, paramString, new Component[] { Component.of(paramContext, Context.class, new Class[0]), Component.of(this, FirebaseApp.class, new Class[0]), Component.of(paramFirebaseOptions, FirebaseOptions.class, new Class[0]) });
    this.zzn = ((Publisher)this.zzl.get(Publisher.class));
  }
  
  @PublicApi
  public static List<FirebaseApp> getApps(Context arg0)
  {
    synchronized (zzg)
    {
      ArrayList localArrayList = new ArrayList(zza.values());
      return localArrayList;
    }
  }
  
  @Nullable
  @PublicApi
  public static FirebaseApp getInstance()
  {
    synchronized (zzg)
    {
      FirebaseApp localFirebaseApp1 = (FirebaseApp)zza.get("[DEFAULT]");
      if (localFirebaseApp1 == null) {
        throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + ProcessUtils.getMyProcessName() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
      }
    }
    return localFirebaseApp2;
  }
  
  @PublicApi
  public static FirebaseApp getInstance(@NonNull String paramString)
  {
    for (;;)
    {
      synchronized (zzg)
      {
        localObject1 = (FirebaseApp)zza.get(paramString.trim());
        if (localObject1 != null) {
          return (FirebaseApp)localObject1;
        }
        localObject1 = zzd();
        if (((List)localObject1).isEmpty())
        {
          localObject1 = "";
          throw new IllegalStateException(String.format("FirebaseApp with name %s doesn't exist. %s", new Object[] { paramString, localObject1 }));
        }
      }
      Object localObject1 = "Available app names: " + TextUtils.join(", ", (Iterable)localObject1);
    }
  }
  
  @KeepForSdk
  public static String getPersistenceKey(String paramString, FirebaseOptions paramFirebaseOptions)
  {
    return Base64Utils.encodeUrlSafeNoPadding(paramString.getBytes(Charset.defaultCharset())) + "+" + Base64Utils.encodeUrlSafeNoPadding(paramFirebaseOptions.getApplicationId().getBytes(Charset.defaultCharset()));
  }
  
  @Nullable
  @PublicApi
  public static FirebaseApp initializeApp(Context paramContext)
  {
    FirebaseOptions localFirebaseOptions;
    synchronized (zzg)
    {
      if (zza.containsKey("[DEFAULT]"))
      {
        paramContext = getInstance();
        return paramContext;
      }
      localFirebaseOptions = FirebaseOptions.fromResource(paramContext);
      if (localFirebaseOptions == null)
      {
        Log.d("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
        return null;
      }
    }
    paramContext = initializeApp(paramContext, localFirebaseOptions);
    return paramContext;
  }
  
  @PublicApi
  public static FirebaseApp initializeApp(Context paramContext, FirebaseOptions paramFirebaseOptions)
  {
    return initializeApp(paramContext, paramFirebaseOptions, "[DEFAULT]");
  }
  
  @PublicApi
  public static FirebaseApp initializeApp(Context paramContext, FirebaseOptions paramFirebaseOptions, String paramString)
  {
    zza.zza(paramContext);
    paramString = paramString.trim();
    if (paramContext.getApplicationContext() == null) {}
    synchronized (zzg)
    {
      while (!zza.containsKey(paramString))
      {
        bool = true;
        Preconditions.checkState(bool, "FirebaseApp name " + paramString + " already exists!");
        Preconditions.checkNotNull(paramContext, "Application context cannot be null.");
        paramContext = new FirebaseApp(paramContext, paramString, paramFirebaseOptions);
        zza.put(paramString, paramContext);
        paramContext.zze();
        return paramContext;
        paramContext = paramContext.getApplicationContext();
      }
      boolean bool = false;
    }
  }
  
  private static <T> void zza(Class<T> paramClass, T paramT, Iterable<String> paramIterable, boolean paramBoolean)
  {
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext())
    {
      String str = (String)paramIterable.next();
      if (paramBoolean) {}
      try
      {
        if (zze.contains(str))
        {
          Method localMethod = Class.forName(str).getMethod("getInstance", new Class[] { paramClass });
          int i = localMethod.getModifiers();
          if ((Modifier.isPublic(i)) && (Modifier.isStatic(i))) {
            localMethod.invoke(null, new Object[] { paramT });
          }
        }
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        if (zzf.contains(str)) {
          throw new IllegalStateException(str + " is missing, but is required. Check if it has been removed by Proguard.");
        }
        Log.d("FirebaseApp", str + " is not linked. Skipping initialization.");
      }
      catch (NoSuchMethodException paramClass)
      {
        throw new IllegalStateException(str + "#getInstance has been removed by Proguard. Add keep rule to prevent it.");
      }
      catch (InvocationTargetException localInvocationTargetException)
      {
        Log.wtf("FirebaseApp", "Firebase API initialization failure.", localInvocationTargetException);
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        Log.wtf("FirebaseApp", "Failed to initialize " + localInvocationTargetException, localIllegalAccessException);
      }
    }
  }
  
  private void zza(boolean paramBoolean)
  {
    Log.d("FirebaseApp", "Notifying background state change listeners.");
    Iterator localIterator = this.zzs.iterator();
    while (localIterator.hasNext()) {
      ((BackgroundStateChangeListener)localIterator.next()).onBackgroundStateChanged(paramBoolean);
    }
  }
  
  private boolean zzb()
  {
    boolean bool2 = true;
    boolean bool1;
    if (this.zzm.contains("firebase_data_collection_default_enabled")) {
      bool1 = this.zzm.getBoolean("firebase_data_collection_default_enabled", true);
    }
    for (;;)
    {
      return bool1;
      try
      {
        Object localObject = this.zzi.getPackageManager();
        bool1 = bool2;
        if (localObject != null)
        {
          localObject = ((PackageManager)localObject).getApplicationInfo(this.zzi.getPackageName(), 128);
          bool1 = bool2;
          if (localObject != null)
          {
            bool1 = bool2;
            if (((ApplicationInfo)localObject).metaData != null)
            {
              bool1 = bool2;
              if (((ApplicationInfo)localObject).metaData.containsKey("firebase_data_collection_default_enabled"))
              {
                bool1 = ((ApplicationInfo)localObject).metaData.getBoolean("firebase_data_collection_default_enabled");
                return bool1;
              }
            }
          }
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    }
    return true;
  }
  
  private void zzc()
  {
    if (!this.zzp.get()) {}
    for (boolean bool = true;; bool = false)
    {
      Preconditions.checkState(bool, "FirebaseApp was deleted");
      return;
    }
  }
  
  private static List<String> zzd()
  {
    ArrayList localArrayList = new ArrayList();
    synchronized (zzg)
    {
      Iterator localIterator = zza.values().iterator();
      if (localIterator.hasNext()) {
        localArrayList.add(((FirebaseApp)localIterator.next()).getName());
      }
    }
    Collections.sort(localList);
    return localList;
  }
  
  private void zze()
  {
    boolean bool = ContextCompat.isDeviceProtectedStorage(this.zzi);
    if (bool) {
      zzc.zza(this.zzi);
    }
    for (;;)
    {
      zza(FirebaseApp.class, this, zzb, bool);
      if (isDefaultApp())
      {
        zza(FirebaseApp.class, this, zzc, bool);
        zza(Context.class, this.zzi, zzd, bool);
      }
      return;
      this.zzl.zza(isDefaultApp());
    }
  }
  
  @KeepForSdk
  public void addBackgroundStateChangeListener(BackgroundStateChangeListener paramBackgroundStateChangeListener)
  {
    zzc();
    if ((this.zzo.get()) && (BackgroundDetector.getInstance().isInBackground())) {
      paramBackgroundStateChangeListener.onBackgroundStateChanged(true);
    }
    this.zzs.add(paramBackgroundStateChangeListener);
  }
  
  @Deprecated
  @KeepForSdk
  public void addIdTokenListener(@NonNull IdTokenListener paramIdTokenListener)
  {
    zzc();
    Preconditions.checkNotNull(paramIdTokenListener);
    this.zzr.add(paramIdTokenListener);
    this.zzv.onListenerCountChanged(this.zzr.size());
  }
  
  @KeepForSdk
  public void addLifecycleEventListener(@NonNull FirebaseAppLifecycleListener paramFirebaseAppLifecycleListener)
  {
    zzc();
    Preconditions.checkNotNull(paramFirebaseAppLifecycleListener);
    this.zzt.add(paramFirebaseAppLifecycleListener);
  }
  
  @PublicApi
  public void delete()
  {
    if (!this.zzp.compareAndSet(false, true)) {}
    for (;;)
    {
      return;
      synchronized (zzg)
      {
        zza.remove(this.zzj);
        ??? = this.zzt.iterator();
        if (!((Iterator)???).hasNext()) {
          continue;
        }
        ((Iterator)???).next();
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof FirebaseApp)) {
      return false;
    }
    return this.zzj.equals(((FirebaseApp)paramObject).getName());
  }
  
  @KeepForSdk
  public <T> T get(Class<T> paramClass)
  {
    zzc();
    return (T)this.zzl.get(paramClass);
  }
  
  @NonNull
  @PublicApi
  public Context getApplicationContext()
  {
    zzc();
    return this.zzi;
  }
  
  @Deprecated
  @KeepForSdk
  public List<IdTokenListener> getListeners()
  {
    zzc();
    return this.zzr;
  }
  
  @NonNull
  @PublicApi
  public String getName()
  {
    zzc();
    return this.zzj;
  }
  
  @NonNull
  @PublicApi
  public FirebaseOptions getOptions()
  {
    zzc();
    return this.zzk;
  }
  
  @KeepForSdk
  public String getPersistenceKey()
  {
    return Base64Utils.encodeUrlSafeNoPadding(getName().getBytes(Charset.defaultCharset())) + "+" + Base64Utils.encodeUrlSafeNoPadding(getOptions().getApplicationId().getBytes(Charset.defaultCharset()));
  }
  
  @Deprecated
  @KeepForSdk
  public Task<GetTokenResult> getToken(boolean paramBoolean)
  {
    zzc();
    if (this.zzu == null) {
      return Tasks.forException(new FirebaseApiNotAvailableException("firebase-auth is not linked, please fall back to unauthenticated mode."));
    }
    return this.zzu.getAccessToken(paramBoolean);
  }
  
  @Deprecated
  @Nullable
  @KeepForSdk
  public String getUid()
    throws FirebaseApiNotAvailableException
  {
    zzc();
    if (this.zzu == null) {
      throw new FirebaseApiNotAvailableException("firebase-auth is not linked, please fall back to unauthenticated mode.");
    }
    return this.zzu.getUid();
  }
  
  public int hashCode()
  {
    return this.zzj.hashCode();
  }
  
  @KeepForSdk
  public boolean isDataCollectionDefaultEnabled()
  {
    zzc();
    return this.zzq.get();
  }
  
  @VisibleForTesting
  @KeepForSdk
  public boolean isDefaultApp()
  {
    return "[DEFAULT]".equals(getName());
  }
  
  @Deprecated
  @UiThread
  @KeepForSdk
  public void notifyIdTokenListeners(@NonNull InternalTokenResult paramInternalTokenResult)
  {
    Log.d("FirebaseApp", "Notifying auth state listeners.");
    Iterator localIterator = this.zzr.iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      ((IdTokenListener)localIterator.next()).onIdTokenChanged(paramInternalTokenResult);
      i += 1;
    }
    Log.d("FirebaseApp", String.format("Notified %d auth state listeners.", new Object[] { Integer.valueOf(i) }));
  }
  
  @KeepForSdk
  public void removeBackgroundStateChangeListener(BackgroundStateChangeListener paramBackgroundStateChangeListener)
  {
    zzc();
    this.zzs.remove(paramBackgroundStateChangeListener);
  }
  
  @Deprecated
  @KeepForSdk
  public void removeIdTokenListener(@NonNull IdTokenListener paramIdTokenListener)
  {
    zzc();
    Preconditions.checkNotNull(paramIdTokenListener);
    this.zzr.remove(paramIdTokenListener);
    this.zzv.onListenerCountChanged(this.zzr.size());
  }
  
  @KeepForSdk
  public void removeLifecycleEventListener(@NonNull FirebaseAppLifecycleListener paramFirebaseAppLifecycleListener)
  {
    zzc();
    Preconditions.checkNotNull(paramFirebaseAppLifecycleListener);
    this.zzt.remove(paramFirebaseAppLifecycleListener);
  }
  
  @PublicApi
  public void setAutomaticResourceManagementEnabled(boolean paramBoolean)
  {
    zzc();
    AtomicBoolean localAtomicBoolean = this.zzo;
    boolean bool;
    if (!paramBoolean)
    {
      bool = true;
      if (localAtomicBoolean.compareAndSet(bool, paramBoolean))
      {
        bool = BackgroundDetector.getInstance().isInBackground();
        if ((!paramBoolean) || (!bool)) {
          break label50;
        }
        zza(true);
      }
    }
    label50:
    while ((paramBoolean) || (!bool))
    {
      return;
      bool = false;
      break;
    }
    zza(false);
  }
  
  @KeepForSdk
  public void setDataCollectionDefaultEnabled(boolean paramBoolean)
  {
    zzc();
    AtomicBoolean localAtomicBoolean = this.zzq;
    if (!paramBoolean) {}
    for (boolean bool = true;; bool = false)
    {
      if (localAtomicBoolean.compareAndSet(bool, paramBoolean))
      {
        this.zzm.edit().putBoolean("firebase_data_collection_default_enabled", paramBoolean).commit();
        this.zzn.publish(new Event(DataCollectionDefaultChange.class, new DataCollectionDefaultChange(paramBoolean)));
      }
      return;
    }
  }
  
  @Deprecated
  @KeepForSdk
  public void setIdTokenListenersCountChangedListener(@NonNull IdTokenListenersCountChangedListener paramIdTokenListenersCountChangedListener)
  {
    this.zzv = ((IdTokenListenersCountChangedListener)Preconditions.checkNotNull(paramIdTokenListenersCountChangedListener));
    this.zzv.onListenerCountChanged(this.zzr.size());
  }
  
  @Deprecated
  @KeepForSdk
  public void setTokenProvider(@NonNull InternalTokenProvider paramInternalTokenProvider)
  {
    this.zzu = ((InternalTokenProvider)Preconditions.checkNotNull(paramInternalTokenProvider));
  }
  
  public String toString()
  {
    return Objects.toStringHelper(this).add("name", this.zzj).add("options", this.zzk).toString();
  }
  
  @KeepForSdk
  public static abstract interface BackgroundStateChangeListener
  {
    @KeepForSdk
    public abstract void onBackgroundStateChanged(boolean paramBoolean);
  }
  
  @Deprecated
  @KeepForSdk
  public static abstract interface IdTokenListener
  {
    @KeepForSdk
    public abstract void onIdTokenChanged(@NonNull InternalTokenResult paramInternalTokenResult);
  }
  
  @Deprecated
  @KeepForSdk
  public static abstract interface IdTokenListenersCountChangedListener
  {
    @KeepForSdk
    public abstract void onListenerCountChanged(int paramInt);
  }
  
  @TargetApi(14)
  static final class zza
    implements BackgroundDetector.BackgroundStateChangeListener
  {
    private static AtomicReference<zza> zza = new AtomicReference();
    
    public final void onBackgroundStateChanged(boolean paramBoolean)
    {
      synchronized ()
      {
        Iterator localIterator = new ArrayList(FirebaseApp.zza.values()).iterator();
        while (localIterator.hasNext())
        {
          FirebaseApp localFirebaseApp = (FirebaseApp)localIterator.next();
          if (FirebaseApp.zzb(localFirebaseApp).get()) {
            FirebaseApp.zza(localFirebaseApp, paramBoolean);
          }
        }
      }
    }
  }
  
  static final class zzb
    implements Executor
  {
    private static final Handler zza = new Handler(Looper.getMainLooper());
    
    public final void execute(@NonNull Runnable paramRunnable)
    {
      zza.post(paramRunnable);
    }
  }
  
  @TargetApi(24)
  static final class zzc
    extends BroadcastReceiver
  {
    private static AtomicReference<zzc> zza = new AtomicReference();
    private final Context zzb;
    
    private zzc(Context paramContext)
    {
      this.zzb = paramContext;
    }
    
    public final void onReceive(Context arg1, Intent paramIntent)
    {
      synchronized ()
      {
        paramIntent = FirebaseApp.zza.values().iterator();
        if (paramIntent.hasNext()) {
          FirebaseApp.zza((FirebaseApp)paramIntent.next());
        }
      }
      this.zzb.unregisterReceiver(this);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\FirebaseApp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */