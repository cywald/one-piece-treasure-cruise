package com.google.firebase.iid;

import android.support.v4.util.ArrayMap;
import android.util.Pair;
import com.google.android.gms.tasks.Task;
import java.util.Map;
import java.util.concurrent.Executor;
import javax.annotation.concurrent.GuardedBy;

final class zzaq
{
  private final Executor zzbj;
  @GuardedBy("this")
  private final Map<Pair<String, String>, Task<String>> zzco = new ArrayMap();
  
  zzaq(Executor paramExecutor)
  {
    this.zzbj = paramExecutor;
  }
  
  /* Error */
  final Task<String> zza(String paramString1, String paramString2, zzas paramzzas)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 39	android/util/Pair
    //   5: dup
    //   6: aload_1
    //   7: aload_2
    //   8: invokespecial 42	android/util/Pair:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   11: astore 4
    //   13: aload_0
    //   14: getfield 22	com/google/firebase/iid/zzaq:zzco	Ljava/util/Map;
    //   17: aload 4
    //   19: invokeinterface 45 2 0
    //   24: checkcast 47	com/google/android/gms/tasks/Task
    //   27: astore_2
    //   28: aload_2
    //   29: ifnull +61 -> 90
    //   32: aload_2
    //   33: astore_1
    //   34: ldc 49
    //   36: iconst_3
    //   37: invokestatic 55	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   40: ifeq +46 -> 86
    //   43: aload 4
    //   45: invokestatic 61	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   48: astore_1
    //   49: ldc 49
    //   51: new 63	java/lang/StringBuilder
    //   54: dup
    //   55: aload_1
    //   56: invokestatic 61	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   59: invokevirtual 67	java/lang/String:length	()I
    //   62: bipush 29
    //   64: iadd
    //   65: invokespecial 70	java/lang/StringBuilder:<init>	(I)V
    //   68: ldc 72
    //   70: invokevirtual 76	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: aload_1
    //   74: invokevirtual 76	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: invokevirtual 80	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   80: invokestatic 84	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   83: pop
    //   84: aload_2
    //   85: astore_1
    //   86: aload_0
    //   87: monitorexit
    //   88: aload_1
    //   89: areturn
    //   90: ldc 49
    //   92: iconst_3
    //   93: invokestatic 55	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   96: ifeq +44 -> 140
    //   99: aload 4
    //   101: invokestatic 61	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   104: astore_1
    //   105: ldc 49
    //   107: new 63	java/lang/StringBuilder
    //   110: dup
    //   111: aload_1
    //   112: invokestatic 61	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   115: invokevirtual 67	java/lang/String:length	()I
    //   118: bipush 24
    //   120: iadd
    //   121: invokespecial 70	java/lang/StringBuilder:<init>	(I)V
    //   124: ldc 86
    //   126: invokevirtual 76	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   129: aload_1
    //   130: invokevirtual 76	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: invokevirtual 80	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   136: invokestatic 84	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   139: pop
    //   140: aload_3
    //   141: invokeinterface 92 1 0
    //   146: aload_0
    //   147: getfield 24	com/google/firebase/iid/zzaq:zzbj	Ljava/util/concurrent/Executor;
    //   150: new 94	com/google/firebase/iid/zzar
    //   153: dup
    //   154: aload_0
    //   155: aload 4
    //   157: invokespecial 97	com/google/firebase/iid/zzar:<init>	(Lcom/google/firebase/iid/zzaq;Landroid/util/Pair;)V
    //   160: invokevirtual 101	com/google/android/gms/tasks/Task:continueWithTask	(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;
    //   163: astore_1
    //   164: aload_0
    //   165: getfield 22	com/google/firebase/iid/zzaq:zzco	Ljava/util/Map;
    //   168: aload 4
    //   170: aload_1
    //   171: invokeinterface 105 3 0
    //   176: pop
    //   177: goto -91 -> 86
    //   180: astore_1
    //   181: aload_0
    //   182: monitorexit
    //   183: aload_1
    //   184: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	185	0	this	zzaq
    //   0	185	1	paramString1	String
    //   0	185	2	paramString2	String
    //   0	185	3	paramzzas	zzas
    //   11	158	4	localPair	Pair
    // Exception table:
    //   from	to	target	type
    //   2	28	180	finally
    //   34	84	180	finally
    //   90	140	180	finally
    //   140	177	180	finally
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzaq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */