package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.google.android.gms.common.util.VisibleForTesting;
import javax.annotation.Nullable;

@VisibleForTesting
final class zzaz
  extends BroadcastReceiver
{
  @Nullable
  private zzay zzdk;
  
  public zzaz(zzay paramzzay)
  {
    this.zzdk = paramzzay;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (this.zzdk == null) {}
    while (!this.zzdk.zzao()) {
      return;
    }
    if (FirebaseInstanceId.zzl()) {
      Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
    }
    FirebaseInstanceId.zza(this.zzdk, 0L);
    this.zzdk.getContext().unregisterReceiver(this);
    this.zzdk = null;
  }
  
  public final void zzap()
  {
    if (FirebaseInstanceId.zzl()) {
      Log.d("FirebaseInstanceId", "Connectivity change received registered");
    }
    IntentFilter localIntentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
    this.zzdk.getContext().registerReceiver(this, localIntentFilter);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzaz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */