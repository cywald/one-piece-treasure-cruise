package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.GuardedBy;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class zzaw
{
  private final SharedPreferences zzdc;
  private final zzy zzdd;
  @GuardedBy("this")
  private final Map<String, zzz> zzde = new ArrayMap();
  private final Context zzx;
  
  public zzaw(Context paramContext)
  {
    this(paramContext, new zzy());
  }
  
  private zzaw(Context paramContext, zzy paramzzy)
  {
    this.zzx = paramContext;
    this.zzdc = paramContext.getSharedPreferences("com.google.android.gms.appid", 0);
    this.zzdd = paramzzy;
    paramContext = new File(ContextCompat.getNoBackupFilesDir(this.zzx), "com.google.android.gms.appid-no-backup");
    if (!paramContext.exists()) {}
    try
    {
      if ((paramContext.createNewFile()) && (!isEmpty()))
      {
        Log.i("FirebaseInstanceId", "App restored, clearing state");
        zzal();
        FirebaseInstanceId.getInstance().zzm();
      }
      return;
    }
    catch (IOException paramContext)
    {
      while (!Log.isLoggable("FirebaseInstanceId", 3)) {}
      paramContext = String.valueOf(paramContext.getMessage());
      if (paramContext.length() == 0) {}
    }
    for (paramContext = "Error creating file in no backup dir: ".concat(paramContext);; paramContext = new String("Error creating file in no backup dir: "))
    {
      Log.d("FirebaseInstanceId", paramContext);
      return;
    }
  }
  
  private final boolean isEmpty()
  {
    try
    {
      boolean bool = this.zzdc.getAll().isEmpty();
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private static String zza(String paramString1, String paramString2, String paramString3)
  {
    return String.valueOf(paramString1).length() + 4 + String.valueOf(paramString2).length() + String.valueOf(paramString3).length() + paramString1 + "|T|" + paramString2 + "|" + paramString3;
  }
  
  static String zzd(String paramString1, String paramString2)
  {
    return String.valueOf(paramString1).length() + 3 + String.valueOf(paramString2).length() + paramString1 + "|S|" + paramString2;
  }
  
  /* Error */
  public final void zza(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload 4
    //   4: aload 5
    //   6: invokestatic 162	java/lang/System:currentTimeMillis	()J
    //   9: invokestatic 167	com/google/firebase/iid/zzax:zza	(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    //   12: astore 4
    //   14: aload 4
    //   16: ifnonnull +6 -> 22
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: aload_0
    //   23: getfield 47	com/google/firebase/iid/zzaw:zzdc	Landroid/content/SharedPreferences;
    //   26: invokeinterface 171 1 0
    //   31: astore 5
    //   33: aload 5
    //   35: aload_1
    //   36: aload_2
    //   37: aload_3
    //   38: invokestatic 173	com/google/firebase/iid/zzaw:zza	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   41: aload 4
    //   43: invokeinterface 179 3 0
    //   48: pop
    //   49: aload 5
    //   51: invokeinterface 182 1 0
    //   56: pop
    //   57: goto -38 -> 19
    //   60: astore_1
    //   61: aload_0
    //   62: monitorexit
    //   63: aload_1
    //   64: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	65	0	this	zzaw
    //   0	65	1	paramString1	String
    //   0	65	2	paramString2	String
    //   0	65	3	paramString3	String
    //   0	65	4	paramString4	String
    //   0	65	5	paramString5	String
    // Exception table:
    //   from	to	target	type
    //   2	14	60	finally
    //   22	57	60	finally
  }
  
  public final String zzak()
  {
    try
    {
      String str = this.zzdc.getString("topic_operaion_queue", "");
      return str;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void zzal()
  {
    try
    {
      this.zzde.clear();
      zzy.zza(this.zzx);
      this.zzdc.edit().clear().commit();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final zzax zzb(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      paramString1 = zzax.zzi(this.zzdc.getString(zza(paramString1, paramString2, paramString3), null));
      return paramString1;
    }
    finally
    {
      paramString1 = finally;
      throw paramString1;
    }
  }
  
  public final void zzc(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      paramString1 = zza(paramString1, paramString2, paramString3);
      paramString2 = this.zzdc.edit();
      paramString2.remove(paramString1);
      paramString2.commit();
      return;
    }
    finally
    {
      paramString1 = finally;
      throw paramString1;
    }
  }
  
  public final void zzf(String paramString)
  {
    try
    {
      this.zzdc.edit().putString("topic_operaion_queue", paramString).apply();
      return;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  /* Error */
  public final zzz zzg(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 35	com/google/firebase/iid/zzaw:zzde	Ljava/util/Map;
    //   6: aload_1
    //   7: invokeinterface 221 2 0
    //   12: checkcast 223	com/google/firebase/iid/zzz
    //   15: astore_2
    //   16: aload_2
    //   17: ifnull +7 -> 24
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: areturn
    //   24: aload_0
    //   25: getfield 49	com/google/firebase/iid/zzaw:zzdd	Lcom/google/firebase/iid/zzy;
    //   28: aload_0
    //   29: getfield 37	com/google/firebase/iid/zzaw:zzx	Landroid/content/Context;
    //   32: aload_1
    //   33: invokevirtual 226	com/google/firebase/iid/zzy:zzb	(Landroid/content/Context;Ljava/lang/String;)Lcom/google/firebase/iid/zzz;
    //   36: astore_2
    //   37: aload_0
    //   38: getfield 35	com/google/firebase/iid/zzaw:zzde	Ljava/util/Map;
    //   41: aload_1
    //   42: aload_2
    //   43: invokeinterface 230 3 0
    //   48: pop
    //   49: goto -29 -> 20
    //   52: astore_1
    //   53: aload_0
    //   54: monitorexit
    //   55: aload_1
    //   56: athrow
    //   57: astore_2
    //   58: ldc 74
    //   60: ldc -24
    //   62: invokestatic 235	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   65: pop
    //   66: invokestatic 91	com/google/firebase/iid/FirebaseInstanceId:getInstance	()Lcom/google/firebase/iid/FirebaseInstanceId;
    //   69: invokevirtual 94	com/google/firebase/iid/FirebaseInstanceId:zzm	()V
    //   72: aload_0
    //   73: getfield 49	com/google/firebase/iid/zzaw:zzdd	Lcom/google/firebase/iid/zzy;
    //   76: aload_0
    //   77: getfield 37	com/google/firebase/iid/zzaw:zzx	Landroid/content/Context;
    //   80: aload_1
    //   81: invokevirtual 237	com/google/firebase/iid/zzy:zzc	(Landroid/content/Context;Ljava/lang/String;)Lcom/google/firebase/iid/zzz;
    //   84: astore_2
    //   85: goto -48 -> 37
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	88	0	this	zzaw
    //   0	88	1	paramString	String
    //   15	28	2	localzzz1	zzz
    //   57	1	2	localzzaa	zzaa
    //   84	1	2	localzzz2	zzz
    // Exception table:
    //   from	to	target	type
    //   2	16	52	finally
    //   24	37	52	finally
    //   37	49	52	finally
    //   58	85	52	finally
    //   24	37	57	com/google/firebase/iid/zzaa
  }
  
  public final void zzh(String paramString)
  {
    try
    {
      paramString = String.valueOf(paramString).concat("|T|");
      SharedPreferences.Editor localEditor = this.zzdc.edit();
      Iterator localIterator = this.zzdc.getAll().keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if (str.startsWith(paramString)) {
          localEditor.remove(str);
        }
      }
      localEditor.commit();
    }
    finally {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzaw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */