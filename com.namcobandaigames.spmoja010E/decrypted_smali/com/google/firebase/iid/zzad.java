package com.google.firebase.iid;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.internal.firebase_messaging.zza;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.concurrent.GuardedBy;

final class zzad
  implements ServiceConnection
{
  @GuardedBy("this")
  int state = 0;
  final Messenger zzbx = new Messenger(new zza(Looper.getMainLooper(), new zzae(this)));
  zzai zzby;
  @GuardedBy("this")
  final Queue<zzak<?>> zzbz = new ArrayDeque();
  @GuardedBy("this")
  final SparseArray<zzak<?>> zzca = new SparseArray();
  
  private zzad(zzab paramzzab) {}
  
  private final void zzy()
  {
    zzab.zzb(this.zzcb).execute(new zzag(this));
  }
  
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    for (;;)
    {
      try
      {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
          Log.v("MessengerIpcClient", "Service connected");
        }
        if (paramIBinder != null) {
          continue;
        }
        zza(0, "Null service connection");
      }
      finally
      {
        try
        {
          this.zzby = new zzai(paramIBinder);
          this.state = 2;
          zzy();
        }
        catch (RemoteException paramComponentName)
        {
          zza(0, paramComponentName.getMessage());
        }
        paramComponentName = finally;
      }
      return;
    }
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    try
    {
      if (Log.isLoggable("MessengerIpcClient", 2)) {
        Log.v("MessengerIpcClient", "Service disconnected");
      }
      zza(2, "Service disconnected");
      return;
    }
    finally {}
  }
  
  final void zza(int paramInt)
  {
    try
    {
      zzak localzzak = (zzak)this.zzca.get(paramInt);
      if (localzzak != null)
      {
        Log.w("MessengerIpcClient", 31 + "Timing out request: " + paramInt);
        this.zzca.remove(paramInt);
        localzzak.zza(new zzal(3, "Timed out waiting for response"));
        zzz();
      }
      return;
    }
    finally {}
  }
  
  final void zza(int paramInt, String paramString)
  {
    for (;;)
    {
      try
      {
        if (Log.isLoggable("MessengerIpcClient", 3))
        {
          localObject = String.valueOf(paramString);
          if (((String)localObject).length() != 0)
          {
            localObject = "Disconnected: ".concat((String)localObject);
            Log.d("MessengerIpcClient", (String)localObject);
          }
        }
        else
        {
          switch (this.state)
          {
          case 0: 
            paramInt = this.state;
            throw new IllegalStateException(26 + "Unknown state: " + paramInt);
          }
        }
      }
      finally {}
      Object localObject = new String("Disconnected: ");
      continue;
      throw new IllegalStateException();
      if (Log.isLoggable("MessengerIpcClient", 2)) {
        Log.v("MessengerIpcClient", "Unbinding service");
      }
      this.state = 4;
      ConnectionTracker.getInstance().unbindService(zzab.zza(this.zzcb), this);
      paramString = new zzal(paramInt, paramString);
      localObject = this.zzbz.iterator();
      while (((Iterator)localObject).hasNext()) {
        ((zzak)((Iterator)localObject).next()).zza(paramString);
      }
      this.zzbz.clear();
      paramInt = 0;
      while (paramInt < this.zzca.size())
      {
        ((zzak)this.zzca.valueAt(paramInt)).zza(paramString);
        paramInt += 1;
      }
      this.zzca.clear();
      for (;;)
      {
        return;
        this.state = 4;
      }
    }
  }
  
  final boolean zza(Message paramMessage)
  {
    int i = paramMessage.arg1;
    if (Log.isLoggable("MessengerIpcClient", 3)) {
      Log.d("MessengerIpcClient", 41 + "Received response to request: " + i);
    }
    zzak localzzak;
    try
    {
      localzzak = (zzak)this.zzca.get(i);
      if (localzzak == null)
      {
        Log.w("MessengerIpcClient", 50 + "Received response for unknown request: " + i);
        return true;
      }
      this.zzca.remove(i);
      zzz();
      paramMessage = paramMessage.getData();
      if (paramMessage.getBoolean("unsupported", false))
      {
        localzzak.zza(new zzal(4, "Not supported by GmsCore"));
        return true;
      }
    }
    finally {}
    localzzak.zzb(paramMessage);
    return true;
  }
  
  final void zzaa()
  {
    try
    {
      if (this.state == 1) {
        zza(1, "Timed out while binding");
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final boolean zzb(zzak paramzzak)
  {
    boolean bool1 = false;
    boolean bool2 = true;
    for (;;)
    {
      try
      {
        switch (this.state)
        {
        case 0: 
          int i = this.state;
          throw new IllegalStateException(26 + "Unknown state: " + i);
        }
      }
      finally {}
      this.zzbz.add(paramzzak);
      if (this.state == 0) {
        bool1 = true;
      }
      Preconditions.checkState(bool1);
      if (Log.isLoggable("MessengerIpcClient", 2)) {
        Log.v("MessengerIpcClient", "Starting bind to GmsCore");
      }
      this.state = 1;
      paramzzak = new Intent("com.google.android.c2dm.intent.REGISTER");
      paramzzak.setPackage("com.google.android.gms");
      if (!ConnectionTracker.getInstance().bindService(zzab.zza(this.zzcb), paramzzak, this, 1))
      {
        zza(0, "Unable to bind to service");
        bool1 = bool2;
      }
      for (;;)
      {
        return bool1;
        zzab.zzb(this.zzcb).schedule(new zzaf(this), 30L, TimeUnit.SECONDS);
        bool1 = bool2;
        continue;
        this.zzbz.add(paramzzak);
        bool1 = bool2;
        continue;
        this.zzbz.add(paramzzak);
        zzy();
        bool1 = bool2;
        continue;
        bool1 = false;
      }
    }
  }
  
  final void zzz()
  {
    try
    {
      if ((this.state == 2) && (this.zzbz.isEmpty()) && (this.zzca.size() == 0))
      {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
          Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
        }
        this.state = 3;
        ConnectionTracker.getInstance().unbindService(zzab.zza(this.zzcb), this);
      }
      return;
    }
    finally {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */