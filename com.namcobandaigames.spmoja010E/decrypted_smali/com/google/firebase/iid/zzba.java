package com.google.firebase.iid;

import android.support.annotation.GuardedBy;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.io.IOException;
import java.util.Map;

final class zzba
{
  @GuardedBy("itself")
  private final zzaw zzaj;
  @GuardedBy("this")
  private int zzdl = 0;
  @GuardedBy("this")
  private final Map<Integer, TaskCompletionSource<Void>> zzdm = new ArrayMap();
  
  zzba(zzaw paramzzaw)
  {
    this.zzaj = paramzzaw;
  }
  
  @WorkerThread
  private static boolean zza(FirebaseInstanceId paramFirebaseInstanceId, String paramString)
  {
    Object localObject = paramString.split("!");
    int j;
    int i;
    if (localObject.length == 2)
    {
      paramString = localObject[0];
      localObject = localObject[1];
      j = -1;
      i = j;
      try
      {
        switch (paramString.hashCode())
        {
        case 83: 
          i = j;
          if (!paramString.equals("S")) {
            break label183;
          }
          i = 0;
        }
      }
      catch (IOException paramFirebaseInstanceId)
      {
        paramFirebaseInstanceId = String.valueOf(paramFirebaseInstanceId.getMessage());
        if (paramFirebaseInstanceId.length() == 0) {
          break label168;
        }
        for (paramFirebaseInstanceId = "Topic sync failed: ".concat(paramFirebaseInstanceId);; paramFirebaseInstanceId = new String("Topic sync failed: "))
        {
          Log.e("FirebaseInstanceId", paramFirebaseInstanceId);
          return false;
          paramFirebaseInstanceId.zzc((String)localObject);
          if (!FirebaseInstanceId.zzl()) {
            break;
          }
          Log.d("FirebaseInstanceId", "unsubscribe operation succeeded");
          return true;
        }
        i = j;
        switch (i)
        {
        }
      }
      i = j;
      if (paramString.equals("U"))
      {
        i = 1;
        break label183;
        paramFirebaseInstanceId.zzb((String)localObject);
        if (!FirebaseInstanceId.zzl()) {
          break label208;
        }
        Log.d("FirebaseInstanceId", "subscribe operation succeeded");
        return true;
      }
    }
    label168:
    label183:
    label208:
    return true;
  }
  
  @GuardedBy("this")
  @Nullable
  private final String zzar()
  {
    synchronized (this.zzaj)
    {
      String str = this.zzaj.zzak();
      if (!TextUtils.isEmpty(str))
      {
        ??? = str.split(",");
        if ((???.length > 1) && (!TextUtils.isEmpty(???[1]))) {
          return ???[1];
        }
      }
    }
    return null;
  }
  
  private final boolean zzk(String paramString)
  {
    for (;;)
    {
      try
      {
        String str1;
        synchronized (this.zzaj)
        {
          String str2 = this.zzaj.zzak();
          str1 = String.valueOf(",");
          String str3 = String.valueOf(paramString);
          if (str3.length() != 0)
          {
            str1 = str1.concat(str3);
            if (!str2.startsWith(str1)) {
              break label142;
            }
            str1 = String.valueOf(",");
            paramString = String.valueOf(paramString);
            if (paramString.length() != 0)
            {
              paramString = str1.concat(paramString);
              paramString = str2.substring(paramString.length());
              this.zzaj.zzf(paramString);
              bool = true;
              return bool;
            }
          }
          else
          {
            str1 = new String(str1);
          }
        }
        paramString = new String(str1);
      }
      finally {}
      continue;
      label142:
      boolean bool = false;
    }
  }
  
  final Task<Void> zza(String paramString)
  {
    for (;;)
    {
      int i;
      try
      {
        String str;
        synchronized (this.zzaj)
        {
          str = this.zzaj.zzak();
          this.zzaj.zzf(String.valueOf(str).length() + 1 + String.valueOf(paramString).length() + str + "," + paramString);
          paramString = new TaskCompletionSource();
          ??? = this.zzdm;
          if (TextUtils.isEmpty(str))
          {
            i = 0;
            ((Map)???).put(Integer.valueOf(i + this.zzdl), paramString);
            paramString = paramString.getTask();
            return paramString;
          }
        }
        i = str.split(",").length;
      }
      finally {}
      i -= 1;
    }
  }
  
  /* Error */
  final boolean zzaq()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial 166	com/google/firebase/iid/zzba:zzar	()Ljava/lang/String;
    //   6: astore_2
    //   7: aload_2
    //   8: ifnull +9 -> 17
    //   11: iconst_1
    //   12: istore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: iload_1
    //   16: ireturn
    //   17: iconst_0
    //   18: istore_1
    //   19: goto -6 -> 13
    //   22: astore_2
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_2
    //   26: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	27	0	this	zzba
    //   12	7	1	bool	boolean
    //   6	2	2	str	String
    //   22	4	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	7	22	finally
  }
  
  @WorkerThread
  final boolean zzc(FirebaseInstanceId paramFirebaseInstanceId)
  {
    for (;;)
    {
      String str;
      try
      {
        str = zzar();
        if (str == null)
        {
          if (FirebaseInstanceId.zzl()) {
            Log.d("FirebaseInstanceId", "topic sync succeeded");
          }
          return true;
        }
        if (!zza(paramFirebaseInstanceId, str)) {
          return false;
        }
      }
      finally {}
      try
      {
        TaskCompletionSource localTaskCompletionSource = (TaskCompletionSource)this.zzdm.remove(Integer.valueOf(this.zzdl));
        zzk(str);
        this.zzdl += 1;
        if (localTaskCompletionSource == null) {
          continue;
        }
        localTaskCompletionSource.setResult(null);
      }
      finally {}
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzba.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */