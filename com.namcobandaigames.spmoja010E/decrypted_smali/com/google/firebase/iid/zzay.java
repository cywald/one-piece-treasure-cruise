package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.firebase.FirebaseApp;
import java.io.IOException;

final class zzay
  implements Runnable
{
  private final zzan zzan;
  private final zzba zzaq;
  private final long zzdh;
  private final PowerManager.WakeLock zzdi;
  private final FirebaseInstanceId zzdj;
  
  @VisibleForTesting
  zzay(FirebaseInstanceId paramFirebaseInstanceId, zzan paramzzan, zzba paramzzba, long paramLong)
  {
    this.zzdj = paramFirebaseInstanceId;
    this.zzan = paramzzan;
    this.zzaq = paramzzba;
    this.zzdh = paramLong;
    this.zzdi = ((PowerManager)getContext().getSystemService("power")).newWakeLock(1, "fiid-sync");
    this.zzdi.setReferenceCounted(false);
  }
  
  @VisibleForTesting
  private final boolean zzam()
  {
    try
    {
      if (!this.zzdj.zzo()) {
        this.zzdj.zzp();
      }
      return true;
    }
    catch (IOException localIOException)
    {
      str = String.valueOf(localIOException.getMessage());
      if (str.length() == 0) {}
    }
    for (String str = "Build channel failed: ".concat(str);; str = new String("Build channel failed: "))
    {
      Log.e("FirebaseInstanceId", str);
      return false;
    }
  }
  
  @VisibleForTesting
  private final boolean zzan()
  {
    Object localObject = this.zzdj.zzj();
    if ((localObject != null) && (!((zzax)localObject).zzj(this.zzan.zzad()))) {}
    for (;;)
    {
      return true;
      try
      {
        String str1 = this.zzdj.zzk();
        if (str1 == null)
        {
          Log.e("FirebaseInstanceId", "Token retrieval failed: null");
          return false;
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
          Log.d("FirebaseInstanceId", "Token successfully retrieved");
        }
        if ((localObject != null) && ((localObject == null) || (str1.equals(((zzax)localObject).zzbq)))) {
          continue;
        }
        localObject = getContext();
        Intent localIntent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
        localIntent.putExtra("token", str1);
        zzav.zzc((Context)localObject, localIntent);
        zzav.zzb((Context)localObject, new Intent("com.google.firebase.iid.TOKEN_REFRESH"));
        return true;
      }
      catch (IOException localIOException)
      {
        String str2 = String.valueOf(localIOException.getMessage());
        if (str2.length() != 0) {}
        for (str2 = "Token retrieval failed: ".concat(str2);; str2 = new String("Token retrieval failed: "))
        {
          Log.e("FirebaseInstanceId", str2);
          return false;
        }
      }
      catch (SecurityException localSecurityException)
      {
        for (;;) {}
      }
    }
  }
  
  final Context getContext()
  {
    return this.zzdj.zzh().getApplicationContext();
  }
  
  /* Error */
  public final void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 52	com/google/firebase/iid/zzay:zzdi	Landroid/os/PowerManager$WakeLock;
    //   4: invokevirtual 181	android/os/PowerManager$WakeLock:acquire	()V
    //   7: aload_0
    //   8: getfield 24	com/google/firebase/iid/zzay:zzdj	Lcom/google/firebase/iid/FirebaseInstanceId;
    //   11: iconst_1
    //   12: invokevirtual 184	com/google/firebase/iid/FirebaseInstanceId:zza	(Z)V
    //   15: aload_0
    //   16: getfield 24	com/google/firebase/iid/zzay:zzdj	Lcom/google/firebase/iid/FirebaseInstanceId;
    //   19: invokevirtual 187	com/google/firebase/iid/FirebaseInstanceId:zzn	()Z
    //   22: ifne +19 -> 41
    //   25: aload_0
    //   26: getfield 24	com/google/firebase/iid/zzay:zzdj	Lcom/google/firebase/iid/FirebaseInstanceId;
    //   29: iconst_0
    //   30: invokevirtual 184	com/google/firebase/iid/FirebaseInstanceId:zza	(Z)V
    //   33: aload_0
    //   34: getfield 52	com/google/firebase/iid/zzay:zzdi	Landroid/os/PowerManager$WakeLock;
    //   37: invokevirtual 190	android/os/PowerManager$WakeLock:release	()V
    //   40: return
    //   41: aload_0
    //   42: invokevirtual 193	com/google/firebase/iid/zzay:zzao	()Z
    //   45: ifne +22 -> 67
    //   48: new 195	com/google/firebase/iid/zzaz
    //   51: dup
    //   52: aload_0
    //   53: invokespecial 198	com/google/firebase/iid/zzaz:<init>	(Lcom/google/firebase/iid/zzay;)V
    //   56: invokevirtual 201	com/google/firebase/iid/zzaz:zzap	()V
    //   59: aload_0
    //   60: getfield 52	com/google/firebase/iid/zzay:zzdi	Landroid/os/PowerManager$WakeLock;
    //   63: invokevirtual 190	android/os/PowerManager$WakeLock:release	()V
    //   66: return
    //   67: aload_0
    //   68: invokespecial 203	com/google/firebase/iid/zzay:zzam	()Z
    //   71: ifeq +40 -> 111
    //   74: aload_0
    //   75: invokespecial 205	com/google/firebase/iid/zzay:zzan	()Z
    //   78: ifeq +33 -> 111
    //   81: aload_0
    //   82: getfield 28	com/google/firebase/iid/zzay:zzaq	Lcom/google/firebase/iid/zzba;
    //   85: aload_0
    //   86: getfield 24	com/google/firebase/iid/zzay:zzdj	Lcom/google/firebase/iid/FirebaseInstanceId;
    //   89: invokevirtual 210	com/google/firebase/iid/zzba:zzc	(Lcom/google/firebase/iid/FirebaseInstanceId;)Z
    //   92: ifeq +19 -> 111
    //   95: aload_0
    //   96: getfield 24	com/google/firebase/iid/zzay:zzdj	Lcom/google/firebase/iid/FirebaseInstanceId;
    //   99: iconst_0
    //   100: invokevirtual 184	com/google/firebase/iid/FirebaseInstanceId:zza	(Z)V
    //   103: aload_0
    //   104: getfield 52	com/google/firebase/iid/zzay:zzdi	Landroid/os/PowerManager$WakeLock;
    //   107: invokevirtual 190	android/os/PowerManager$WakeLock:release	()V
    //   110: return
    //   111: aload_0
    //   112: getfield 24	com/google/firebase/iid/zzay:zzdj	Lcom/google/firebase/iid/FirebaseInstanceId;
    //   115: aload_0
    //   116: getfield 30	com/google/firebase/iid/zzay:zzdh	J
    //   119: invokevirtual 213	com/google/firebase/iid/FirebaseInstanceId:zza	(J)V
    //   122: goto -19 -> 103
    //   125: astore_1
    //   126: aload_0
    //   127: getfield 52	com/google/firebase/iid/zzay:zzdi	Landroid/os/PowerManager$WakeLock;
    //   130: invokevirtual 190	android/os/PowerManager$WakeLock:release	()V
    //   133: aload_1
    //   134: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	135	0	this	zzay
    //   125	9	1	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   7	33	125	finally
    //   41	59	125	finally
    //   67	103	125	finally
    //   111	122	125	finally
  }
  
  final boolean zzao()
  {
    Object localObject = (ConnectivityManager)getContext().getSystemService("connectivity");
    if (localObject != null) {}
    for (localObject = ((ConnectivityManager)localObject).getActiveNetworkInfo(); (localObject != null) && (((NetworkInfo)localObject).isConnected()); localObject = null) {
      return true;
    }
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */