package com.google.firebase.iid;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build.VERSION;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.util.PlatformVersion;
import javax.annotation.concurrent.GuardedBy;

public final class FirebaseInstanceIdReceiver
  extends WakefulBroadcastReceiver
{
  private static boolean zzbf = false;
  @GuardedBy("FirebaseInstanceIdReceiver.class")
  private static zzh zzbg;
  @GuardedBy("FirebaseInstanceIdReceiver.class")
  private static zzh zzbh;
  
  @SuppressLint({"InlinedApi"})
  public static int zza(BroadcastReceiver paramBroadcastReceiver, Context paramContext, String paramString, Intent paramIntent)
  {
    int j = 1;
    int i;
    if ((PlatformVersion.isAtLeastO()) && (paramContext.getApplicationInfo().targetSdkVersion >= 26))
    {
      i = 1;
      if ((paramIntent.getFlags() & 0x10000000) == 0) {
        break label62;
      }
      label34:
      if ((i == 0) || (j != 0)) {
        break label68;
      }
      i = zzb(paramBroadcastReceiver, paramContext, paramString, paramIntent);
    }
    label62:
    label68:
    do
    {
      do
      {
        return i;
        i = 0;
        break;
        j = 0;
        break label34;
        j = zzav.zzai().zzb(paramContext, paramString, paramIntent);
        i = j;
      } while (!PlatformVersion.isAtLeastO());
      i = j;
    } while (j != 402);
    zzb(paramBroadcastReceiver, paramContext, paramString, paramIntent);
    return 403;
  }
  
  /* Error */
  private static zzh zza(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: ldc 65
    //   5: aload_1
    //   6: invokevirtual 71	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   9: ifeq +30 -> 39
    //   12: getstatic 73	com/google/firebase/iid/FirebaseInstanceIdReceiver:zzbh	Lcom/google/firebase/iid/zzh;
    //   15: ifnonnull +15 -> 30
    //   18: new 75	com/google/firebase/iid/zzh
    //   21: dup
    //   22: aload_0
    //   23: aload_1
    //   24: invokespecial 78	com/google/firebase/iid/zzh:<init>	(Landroid/content/Context;Ljava/lang/String;)V
    //   27: putstatic 73	com/google/firebase/iid/FirebaseInstanceIdReceiver:zzbh	Lcom/google/firebase/iid/zzh;
    //   30: getstatic 73	com/google/firebase/iid/FirebaseInstanceIdReceiver:zzbh	Lcom/google/firebase/iid/zzh;
    //   33: astore_0
    //   34: ldc 2
    //   36: monitorexit
    //   37: aload_0
    //   38: areturn
    //   39: getstatic 80	com/google/firebase/iid/FirebaseInstanceIdReceiver:zzbg	Lcom/google/firebase/iid/zzh;
    //   42: ifnonnull +15 -> 57
    //   45: new 75	com/google/firebase/iid/zzh
    //   48: dup
    //   49: aload_0
    //   50: aload_1
    //   51: invokespecial 78	com/google/firebase/iid/zzh:<init>	(Landroid/content/Context;Ljava/lang/String;)V
    //   54: putstatic 80	com/google/firebase/iid/FirebaseInstanceIdReceiver:zzbg	Lcom/google/firebase/iid/zzh;
    //   57: getstatic 80	com/google/firebase/iid/FirebaseInstanceIdReceiver:zzbg	Lcom/google/firebase/iid/zzh;
    //   60: astore_0
    //   61: goto -27 -> 34
    //   64: astore_0
    //   65: ldc 2
    //   67: monitorexit
    //   68: aload_0
    //   69: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	70	0	paramContext	Context
    //   0	70	1	paramString	String
    // Exception table:
    //   from	to	target	type
    //   3	30	64	finally
    //   30	34	64	finally
    //   39	57	64	finally
    //   57	61	64	finally
  }
  
  private final void zza(Context paramContext, Intent paramIntent, String paramString)
  {
    paramIntent.setComponent(null);
    paramIntent.setPackage(paramContext.getPackageName());
    if (Build.VERSION.SDK_INT <= 18) {
      paramIntent.removeCategory(paramContext.getPackageName());
    }
    String str = paramIntent.getStringExtra("gcm.rawData64");
    if (str != null)
    {
      paramIntent.putExtra("rawData", Base64.decode(str, 0));
      paramIntent.removeExtra("gcm.rawData64");
    }
    if (("google.com/iid".equals(paramIntent.getStringExtra("from"))) || ("com.google.firebase.INSTANCE_ID_EVENT".equals(paramString))) {
      paramString = "com.google.firebase.INSTANCE_ID_EVENT";
    }
    for (;;)
    {
      int i = -1;
      if (paramString != null) {
        i = zza(this, paramContext, paramString, paramIntent);
      }
      if (isOrderedBroadcast()) {
        setResultCode(i);
      }
      return;
      if (("com.google.android.c2dm.intent.RECEIVE".equals(paramString)) || ("com.google.firebase.MESSAGING_EVENT".equals(paramString)))
      {
        paramString = "com.google.firebase.MESSAGING_EVENT";
      }
      else
      {
        Log.d("FirebaseInstanceId", "Unexpected intent");
        paramString = null;
      }
    }
  }
  
  private static int zzb(BroadcastReceiver paramBroadcastReceiver, Context paramContext, String paramString, Intent paramIntent)
  {
    if (Log.isLoggable("FirebaseInstanceId", 3))
    {
      str = String.valueOf(paramString);
      if (str.length() == 0) {
        break label67;
      }
    }
    label67:
    for (String str = "Binding to service: ".concat(str);; str = new String("Binding to service: "))
    {
      Log.d("FirebaseInstanceId", str);
      if (paramBroadcastReceiver.isOrderedBroadcast()) {
        paramBroadcastReceiver.setResultCode(-1);
      }
      zza(paramContext, paramString).zza(paramIntent, paramBroadcastReceiver.goAsync());
      return -1;
    }
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    Object localObject = paramIntent.getParcelableExtra("wrapped_intent");
    if ((localObject instanceof Intent)) {}
    for (localObject = (Intent)localObject; localObject != null; localObject = null)
    {
      zza(paramContext, (Intent)localObject, paramIntent.getAction());
      return;
    }
    zza(paramContext, paramIntent, paramIntent.getAction());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\FirebaseInstanceIdReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */