package com.google.firebase.iid;

import android.content.BroadcastReceiver.PendingResult;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.support.annotation.GuardedBy;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public final class zzh
  implements ServiceConnection
{
  private final Queue<zzd> zzaa = new ArrayDeque();
  private zzf zzab;
  @GuardedBy("this")
  private boolean zzac = false;
  private final Context zzx;
  private final Intent zzy;
  private final ScheduledExecutorService zzz;
  
  public zzh(Context paramContext, String paramString)
  {
    this(paramContext, paramString, new ScheduledThreadPoolExecutor(0, new NamedThreadFactory("Firebase-FirebaseInstanceIdServiceConnection")));
  }
  
  @VisibleForTesting
  private zzh(Context paramContext, String paramString, ScheduledExecutorService paramScheduledExecutorService)
  {
    this.zzx = paramContext.getApplicationContext();
    this.zzy = new Intent(paramString).setPackage(this.zzx.getPackageName());
    this.zzz = paramScheduledExecutorService;
  }
  
  private final void zzc()
  {
    try
    {
      if (Log.isLoggable("EnhancedIntentService", 3)) {
        Log.d("EnhancedIntentService", "flush queue called");
      }
      for (;;)
      {
        if (this.zzaa.isEmpty()) {
          break label190;
        }
        if (Log.isLoggable("EnhancedIntentService", 3)) {
          Log.d("EnhancedIntentService", "found intent to be delivered");
        }
        if ((this.zzab == null) || (!this.zzab.isBinderAlive())) {
          break;
        }
        if (Log.isLoggable("EnhancedIntentService", 3)) {
          Log.d("EnhancedIntentService", "binder is alive, sending the intent.");
        }
        zzd localzzd = (zzd)this.zzaa.poll();
        this.zzab.zza(localzzd);
      }
      if (!Log.isLoggable("EnhancedIntentService", 3)) {
        break label156;
      }
    }
    finally {}
    boolean bool;
    if (!this.zzac)
    {
      bool = true;
      Log.d("EnhancedIntentService", 39 + "binder is dead. start connection? " + bool);
      label156:
      if (!this.zzac) {
        this.zzac = true;
      }
    }
    for (;;)
    {
      try
      {
        bool = ConnectionTracker.getInstance().bindService(this.zzx, this.zzy, this, 65);
        if (bool)
        {
          label190:
          return;
          bool = false;
          break;
        }
        Log.e("EnhancedIntentService", "binding to the service failed");
      }
      catch (SecurityException localSecurityException)
      {
        Log.e("EnhancedIntentService", "Exception while binding the service", localSecurityException);
        continue;
      }
      this.zzac = false;
      zzd();
    }
  }
  
  @GuardedBy("this")
  private final void zzd()
  {
    while (!this.zzaa.isEmpty()) {
      ((zzd)this.zzaa.poll()).finish();
    }
  }
  
  /* Error */
  public final void onServiceConnected(ComponentName paramComponentName, android.os.IBinder paramIBinder)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_0
    //   4: putfield 51	com/google/firebase/iid/zzh:zzac	Z
    //   7: aload_0
    //   8: aload_2
    //   9: checkcast 104	com/google/firebase/iid/zzf
    //   12: putfield 102	com/google/firebase/iid/zzh:zzab	Lcom/google/firebase/iid/zzf;
    //   15: ldc 80
    //   17: iconst_3
    //   18: invokestatic 86	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   21: ifeq +43 -> 64
    //   24: aload_1
    //   25: invokestatic 170	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   28: astore_1
    //   29: ldc 80
    //   31: new 121	java/lang/StringBuilder
    //   34: dup
    //   35: aload_1
    //   36: invokestatic 170	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   39: invokevirtual 174	java/lang/String:length	()I
    //   42: bipush 20
    //   44: iadd
    //   45: invokespecial 124	java/lang/StringBuilder:<init>	(I)V
    //   48: ldc -80
    //   50: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   53: aload_1
    //   54: invokevirtual 130	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: invokestatic 92	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   63: pop
    //   64: aload_2
    //   65: ifnonnull +18 -> 83
    //   68: ldc 80
    //   70: ldc -78
    //   72: invokestatic 151	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   75: pop
    //   76: aload_0
    //   77: invokespecial 154	com/google/firebase/iid/zzh:zzd	()V
    //   80: aload_0
    //   81: monitorexit
    //   82: return
    //   83: aload_0
    //   84: invokespecial 180	com/google/firebase/iid/zzh:zzc	()V
    //   87: goto -7 -> 80
    //   90: astore_1
    //   91: aload_0
    //   92: monitorexit
    //   93: aload_1
    //   94: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	95	0	this	zzh
    //   0	95	1	paramComponentName	ComponentName
    //   0	95	2	paramIBinder	android.os.IBinder
    // Exception table:
    //   from	to	target	type
    //   2	64	90	finally
    //   68	80	90	finally
    //   80	82	90	finally
    //   83	87	90	finally
    //   91	93	90	finally
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    if (Log.isLoggable("EnhancedIntentService", 3))
    {
      paramComponentName = String.valueOf(paramComponentName);
      Log.d("EnhancedIntentService", String.valueOf(paramComponentName).length() + 23 + "onServiceDisconnected: " + paramComponentName);
    }
    zzc();
  }
  
  public final void zza(Intent paramIntent, BroadcastReceiver.PendingResult paramPendingResult)
  {
    try
    {
      if (Log.isLoggable("EnhancedIntentService", 3)) {
        Log.d("EnhancedIntentService", "new intent queued in the bind-strategy delivery");
      }
      this.zzaa.add(new zzd(paramIntent, paramPendingResult, this.zzz));
      zzc();
      return;
    }
    finally {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */