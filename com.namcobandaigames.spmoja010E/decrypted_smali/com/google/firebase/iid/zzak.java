package com.google.firebase.iid;

import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.tasks.TaskCompletionSource;

abstract class zzak<T>
{
  final int what;
  final int zzcf;
  final TaskCompletionSource<T> zzcg = new TaskCompletionSource();
  final Bundle zzch;
  
  zzak(int paramInt1, int paramInt2, Bundle paramBundle)
  {
    this.zzcf = paramInt1;
    this.what = paramInt2;
    this.zzch = paramBundle;
  }
  
  final void finish(T paramT)
  {
    if (Log.isLoggable("MessengerIpcClient", 3))
    {
      String str1 = String.valueOf(this);
      String str2 = String.valueOf(paramT);
      Log.d("MessengerIpcClient", String.valueOf(str1).length() + 16 + String.valueOf(str2).length() + "Finishing " + str1 + " with " + str2);
    }
    this.zzcg.setResult(paramT);
  }
  
  public String toString()
  {
    int i = this.what;
    int j = this.zzcf;
    boolean bool = zzab();
    return 55 + "Request { what=" + i + " id=" + j + " oneWay=" + bool + "}";
  }
  
  final void zza(zzal paramzzal)
  {
    if (Log.isLoggable("MessengerIpcClient", 3))
    {
      String str1 = String.valueOf(this);
      String str2 = String.valueOf(paramzzal);
      Log.d("MessengerIpcClient", String.valueOf(str1).length() + 14 + String.valueOf(str2).length() + "Failing " + str1 + " with " + str2);
    }
    this.zzcg.setException(paramzzal);
  }
  
  abstract boolean zzab();
  
  abstract void zzb(Bundle paramBundle);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */