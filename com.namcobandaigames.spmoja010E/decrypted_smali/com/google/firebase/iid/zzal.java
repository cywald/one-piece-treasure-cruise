package com.google.firebase.iid;

public final class zzal
  extends Exception
{
  private final int errorCode;
  
  public zzal(int paramInt, String paramString)
  {
    super(paramString);
    this.errorCode = paramInt;
  }
  
  public final int getErrorCode()
  {
    return this.errorCode;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */