package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.util.Properties;

final class zzy
{
  /* Error */
  @Nullable
  private final zzz zza(Context paramContext, String paramString, zzz paramzzz, boolean paramBoolean)
  {
    // Byte code:
    //   0: ldc 20
    //   2: iconst_3
    //   3: invokestatic 26	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   6: ifeq +11 -> 17
    //   9: ldc 20
    //   11: ldc 28
    //   13: invokestatic 32	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: new 34	java/util/Properties
    //   20: dup
    //   21: invokespecial 35	java/util/Properties:<init>	()V
    //   24: astore 9
    //   26: aload 9
    //   28: ldc 37
    //   30: aload_3
    //   31: invokestatic 42	com/google/firebase/iid/zzz:zza	(Lcom/google/firebase/iid/zzz;)Ljava/lang/String;
    //   34: invokevirtual 46	java/util/Properties:setProperty	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    //   37: pop
    //   38: aload 9
    //   40: ldc 48
    //   42: aload_3
    //   43: invokestatic 51	com/google/firebase/iid/zzz:zzb	(Lcom/google/firebase/iid/zzz;)Ljava/lang/String;
    //   46: invokevirtual 46	java/util/Properties:setProperty	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    //   49: pop
    //   50: aload 9
    //   52: ldc 53
    //   54: aload_3
    //   55: invokestatic 57	com/google/firebase/iid/zzz:zzc	(Lcom/google/firebase/iid/zzz;)J
    //   58: invokestatic 63	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   61: invokevirtual 46	java/util/Properties:setProperty	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    //   64: pop
    //   65: aload_1
    //   66: aload_2
    //   67: invokestatic 67	com/google/firebase/iid/zzy:zzf	(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    //   70: astore_1
    //   71: aload_1
    //   72: invokevirtual 73	java/io/File:createNewFile	()Z
    //   75: pop
    //   76: new 75	java/io/RandomAccessFile
    //   79: dup
    //   80: aload_1
    //   81: ldc 77
    //   83: invokespecial 80	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   86: astore 7
    //   88: aload 7
    //   90: invokevirtual 84	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   93: astore 8
    //   95: aload 8
    //   97: invokevirtual 90	java/nio/channels/FileChannel:lock	()Ljava/nio/channels/FileLock;
    //   100: pop
    //   101: iload 4
    //   103: ifeq +99 -> 202
    //   106: aload 8
    //   108: invokevirtual 94	java/nio/channels/FileChannel:size	()J
    //   111: lstore 5
    //   113: lload 5
    //   115: lconst_0
    //   116: lcmp
    //   117: ifle +85 -> 202
    //   120: aload 8
    //   122: lconst_0
    //   123: invokevirtual 98	java/nio/channels/FileChannel:position	(J)Ljava/nio/channels/FileChannel;
    //   126: pop
    //   127: aload 8
    //   129: invokestatic 101	com/google/firebase/iid/zzy:zza	(Ljava/nio/channels/FileChannel;)Lcom/google/firebase/iid/zzz;
    //   132: astore_1
    //   133: aload 8
    //   135: ifnull +9 -> 144
    //   138: aconst_null
    //   139: aload 8
    //   141: invokestatic 104	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/nio/channels/FileChannel;)V
    //   144: aconst_null
    //   145: aload 7
    //   147: invokestatic 107	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/io/RandomAccessFile;)V
    //   150: aload_1
    //   151: areturn
    //   152: astore_1
    //   153: ldc 20
    //   155: iconst_3
    //   156: invokestatic 26	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   159: ifeq +43 -> 202
    //   162: aload_1
    //   163: invokestatic 110	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   166: astore_1
    //   167: ldc 20
    //   169: new 112	java/lang/StringBuilder
    //   172: dup
    //   173: aload_1
    //   174: invokestatic 110	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   177: invokevirtual 116	java/lang/String:length	()I
    //   180: bipush 64
    //   182: iadd
    //   183: invokespecial 119	java/lang/StringBuilder:<init>	(I)V
    //   186: ldc 121
    //   188: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   191: aload_1
    //   192: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   195: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   198: invokestatic 32	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   201: pop
    //   202: aload 8
    //   204: lconst_0
    //   205: invokevirtual 98	java/nio/channels/FileChannel:position	(J)Ljava/nio/channels/FileChannel;
    //   208: pop
    //   209: aload 9
    //   211: aload 8
    //   213: invokestatic 135	java/nio/channels/Channels:newOutputStream	(Ljava/nio/channels/WritableByteChannel;)Ljava/io/OutputStream;
    //   216: aconst_null
    //   217: invokevirtual 139	java/util/Properties:store	(Ljava/io/OutputStream;Ljava/lang/String;)V
    //   220: aload 8
    //   222: ifnull +9 -> 231
    //   225: aconst_null
    //   226: aload 8
    //   228: invokestatic 104	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/nio/channels/FileChannel;)V
    //   231: aconst_null
    //   232: aload 7
    //   234: invokestatic 107	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/io/RandomAccessFile;)V
    //   237: aload_3
    //   238: areturn
    //   239: astore_1
    //   240: aload_1
    //   241: invokestatic 110	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   244: astore_1
    //   245: ldc 20
    //   247: new 112	java/lang/StringBuilder
    //   250: dup
    //   251: aload_1
    //   252: invokestatic 110	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   255: invokevirtual 116	java/lang/String:length	()I
    //   258: bipush 21
    //   260: iadd
    //   261: invokespecial 119	java/lang/StringBuilder:<init>	(I)V
    //   264: ldc -115
    //   266: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   269: aload_1
    //   270: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   273: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   276: invokestatic 144	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   279: pop
    //   280: aconst_null
    //   281: areturn
    //   282: astore_1
    //   283: aload_1
    //   284: athrow
    //   285: astore_2
    //   286: aload 8
    //   288: ifnull +9 -> 297
    //   291: aload_1
    //   292: aload 8
    //   294: invokestatic 104	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/nio/channels/FileChannel;)V
    //   297: aload_2
    //   298: athrow
    //   299: astore_1
    //   300: aload_1
    //   301: athrow
    //   302: astore_2
    //   303: aload_1
    //   304: aload 7
    //   306: invokestatic 107	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/io/RandomAccessFile;)V
    //   309: aload_2
    //   310: athrow
    //   311: astore_2
    //   312: aconst_null
    //   313: astore_1
    //   314: goto -11 -> 303
    //   317: astore_2
    //   318: aconst_null
    //   319: astore_1
    //   320: goto -34 -> 286
    //   323: astore_1
    //   324: goto -171 -> 153
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	327	0	this	zzy
    //   0	327	1	paramContext	Context
    //   0	327	2	paramString	String
    //   0	327	3	paramzzz	zzz
    //   0	327	4	paramBoolean	boolean
    //   111	3	5	l	long
    //   86	219	7	localRandomAccessFile	java.io.RandomAccessFile
    //   93	200	8	localFileChannel	FileChannel
    //   24	186	9	localProperties	Properties
    // Exception table:
    //   from	to	target	type
    //   120	133	152	com/google/firebase/iid/zzaa
    //   71	88	239	java/io/IOException
    //   144	150	239	java/io/IOException
    //   231	237	239	java/io/IOException
    //   303	311	239	java/io/IOException
    //   95	101	282	java/lang/Throwable
    //   106	113	282	java/lang/Throwable
    //   120	133	282	java/lang/Throwable
    //   153	202	282	java/lang/Throwable
    //   202	220	282	java/lang/Throwable
    //   283	285	285	finally
    //   88	95	299	java/lang/Throwable
    //   138	144	299	java/lang/Throwable
    //   225	231	299	java/lang/Throwable
    //   291	297	299	java/lang/Throwable
    //   297	299	299	java/lang/Throwable
    //   300	302	302	finally
    //   88	95	311	finally
    //   138	144	311	finally
    //   225	231	311	finally
    //   291	297	311	finally
    //   297	299	311	finally
    //   95	101	317	finally
    //   106	113	317	finally
    //   120	133	317	finally
    //   153	202	317	finally
    //   202	220	317	finally
    //   120	133	323	java/io/IOException
  }
  
  @Nullable
  private static zzz zza(SharedPreferences paramSharedPreferences, String paramString)
    throws zzaa
  {
    String str1 = paramSharedPreferences.getString(zzaw.zzd(paramString, "|P|"), null);
    String str2 = paramSharedPreferences.getString(zzaw.zzd(paramString, "|K|"), null);
    if ((str1 == null) || (str2 == null)) {
      return null;
    }
    return new zzz(zzc(str1, str2), zzb(paramSharedPreferences, paramString));
  }
  
  /* Error */
  private final zzz zza(File paramFile)
    throws zzaa, IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: new 174	java/io/FileInputStream
    //   5: dup
    //   6: aload_1
    //   7: invokespecial 177	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   10: astore 4
    //   12: aload 4
    //   14: invokevirtual 178	java/io/FileInputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   17: astore 5
    //   19: aload 5
    //   21: lconst_0
    //   22: ldc2_w 179
    //   25: iconst_1
    //   26: invokevirtual 183	java/nio/channels/FileChannel:lock	(JJZ)Ljava/nio/channels/FileLock;
    //   29: pop
    //   30: aload 5
    //   32: invokestatic 101	com/google/firebase/iid/zzy:zza	(Ljava/nio/channels/FileChannel;)Lcom/google/firebase/iid/zzz;
    //   35: astore_1
    //   36: aload 5
    //   38: ifnull +9 -> 47
    //   41: aconst_null
    //   42: aload 5
    //   44: invokestatic 104	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/nio/channels/FileChannel;)V
    //   47: aconst_null
    //   48: aload 4
    //   50: invokestatic 186	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/io/FileInputStream;)V
    //   53: aload_1
    //   54: areturn
    //   55: astore_1
    //   56: aload_1
    //   57: athrow
    //   58: astore_2
    //   59: aload 5
    //   61: ifnull +9 -> 70
    //   64: aload_1
    //   65: aload 5
    //   67: invokestatic 104	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/nio/channels/FileChannel;)V
    //   70: aload_2
    //   71: athrow
    //   72: astore_1
    //   73: aload_1
    //   74: athrow
    //   75: astore_2
    //   76: aload_1
    //   77: aload 4
    //   79: invokestatic 186	com/google/firebase/iid/zzy:zza	(Ljava/lang/Throwable;Ljava/io/FileInputStream;)V
    //   82: aload_2
    //   83: athrow
    //   84: astore_2
    //   85: aload_3
    //   86: astore_1
    //   87: goto -11 -> 76
    //   90: astore_2
    //   91: aconst_null
    //   92: astore_1
    //   93: goto -34 -> 59
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	this	zzy
    //   0	96	1	paramFile	File
    //   58	13	2	localObject1	Object
    //   75	8	2	localObject2	Object
    //   84	1	2	localObject3	Object
    //   90	1	2	localObject4	Object
    //   1	85	3	localObject5	Object
    //   10	68	4	localFileInputStream	java.io.FileInputStream
    //   17	49	5	localFileChannel	FileChannel
    // Exception table:
    //   from	to	target	type
    //   19	36	55	java/lang/Throwable
    //   56	58	58	finally
    //   12	19	72	java/lang/Throwable
    //   41	47	72	java/lang/Throwable
    //   64	70	72	java/lang/Throwable
    //   70	72	72	java/lang/Throwable
    //   73	75	75	finally
    //   12	19	84	finally
    //   41	47	84	finally
    //   64	70	84	finally
    //   70	72	84	finally
    //   19	36	90	finally
  }
  
  private static zzz zza(FileChannel paramFileChannel)
    throws zzaa, IOException
  {
    Properties localProperties = new Properties();
    localProperties.load(Channels.newInputStream(paramFileChannel));
    paramFileChannel = localProperties.getProperty("pub");
    String str = localProperties.getProperty("pri");
    if ((paramFileChannel == null) || (str == null)) {
      throw new zzaa("Invalid properties file");
    }
    paramFileChannel = zzc(paramFileChannel, str);
    try
    {
      long l = Long.parseLong(localProperties.getProperty("cre"));
      return new zzz(paramFileChannel, l);
    }
    catch (NumberFormatException paramFileChannel)
    {
      throw new zzaa(paramFileChannel);
    }
  }
  
  static void zza(Context paramContext)
  {
    paramContext = zzb(paramContext).listFiles();
    int j = paramContext.length;
    int i = 0;
    while (i < j)
    {
      Object localObject = paramContext[i];
      if (((File)localObject).getName().startsWith("com.google.InstanceId")) {
        ((File)localObject).delete();
      }
      i += 1;
    }
  }
  
  private final void zza(Context paramContext, String paramString, zzz paramzzz)
  {
    paramContext = paramContext.getSharedPreferences("com.google.android.gms.appid", 0);
    try
    {
      boolean bool = paramzzz.equals(zza(paramContext, paramString));
      if (bool) {
        return;
      }
    }
    catch (zzaa localzzaa)
    {
      if (Log.isLoggable("FirebaseInstanceId", 3)) {
        Log.d("FirebaseInstanceId", "Writing key to shared preferences");
      }
      paramContext = paramContext.edit();
      paramContext.putString(zzaw.zzd(paramString, "|P|"), zzz.zza(paramzzz));
      paramContext.putString(zzaw.zzd(paramString, "|K|"), zzz.zzb(paramzzz));
      paramContext.putString(zzaw.zzd(paramString, "cre"), String.valueOf(zzz.zzc(paramzzz)));
      paramContext.commit();
    }
  }
  
  private static long zzb(SharedPreferences paramSharedPreferences, String paramString)
  {
    paramSharedPreferences = paramSharedPreferences.getString(zzaw.zzd(paramString, "cre"), null);
    if (paramSharedPreferences != null) {
      try
      {
        long l = Long.parseLong(paramSharedPreferences);
        return l;
      }
      catch (NumberFormatException paramSharedPreferences) {}
    }
    return 0L;
  }
  
  private static File zzb(Context paramContext)
  {
    File localFile = ContextCompat.getNoBackupFilesDir(paramContext);
    if ((localFile != null) && (localFile.isDirectory())) {
      return localFile;
    }
    Log.w("FirebaseInstanceId", "noBackupFilesDir doesn't exist, using regular files directory instead");
    return paramContext.getFilesDir();
  }
  
  /* Error */
  private static java.security.KeyPair zzc(String paramString1, String paramString2)
    throws zzaa
  {
    // Byte code:
    //   0: aload_0
    //   1: bipush 8
    //   3: invokestatic 300	android/util/Base64:decode	(Ljava/lang/String;I)[B
    //   6: astore_0
    //   7: aload_1
    //   8: bipush 8
    //   10: invokestatic 300	android/util/Base64:decode	(Ljava/lang/String;I)[B
    //   13: astore_1
    //   14: ldc_w 302
    //   17: invokestatic 308	java/security/KeyFactory:getInstance	(Ljava/lang/String;)Ljava/security/KeyFactory;
    //   20: astore_2
    //   21: new 310	java/security/KeyPair
    //   24: dup
    //   25: aload_2
    //   26: new 312	java/security/spec/X509EncodedKeySpec
    //   29: dup
    //   30: aload_0
    //   31: invokespecial 315	java/security/spec/X509EncodedKeySpec:<init>	([B)V
    //   34: invokevirtual 319	java/security/KeyFactory:generatePublic	(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    //   37: aload_2
    //   38: new 321	java/security/spec/PKCS8EncodedKeySpec
    //   41: dup
    //   42: aload_1
    //   43: invokespecial 322	java/security/spec/PKCS8EncodedKeySpec:<init>	([B)V
    //   46: invokevirtual 326	java/security/KeyFactory:generatePrivate	(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
    //   49: invokespecial 329	java/security/KeyPair:<init>	(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V
    //   52: astore_0
    //   53: aload_0
    //   54: areturn
    //   55: astore_0
    //   56: new 18	com/google/firebase/iid/zzaa
    //   59: dup
    //   60: aload_0
    //   61: invokespecial 214	com/google/firebase/iid/zzaa:<init>	(Ljava/lang/Exception;)V
    //   64: athrow
    //   65: astore_0
    //   66: aload_0
    //   67: invokestatic 110	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   70: astore_1
    //   71: ldc 20
    //   73: new 112	java/lang/StringBuilder
    //   76: dup
    //   77: aload_1
    //   78: invokestatic 110	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   81: invokevirtual 116	java/lang/String:length	()I
    //   84: bipush 19
    //   86: iadd
    //   87: invokespecial 119	java/lang/StringBuilder:<init>	(I)V
    //   90: ldc_w 331
    //   93: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: aload_1
    //   97: invokevirtual 125	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   100: invokevirtual 129	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   103: invokestatic 144	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   106: pop
    //   107: new 18	com/google/firebase/iid/zzaa
    //   110: dup
    //   111: aload_0
    //   112: invokespecial 214	com/google/firebase/iid/zzaa:<init>	(Ljava/lang/Exception;)V
    //   115: athrow
    //   116: astore_0
    //   117: goto -51 -> 66
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	120	0	paramString1	String
    //   0	120	1	paramString2	String
    //   20	18	2	localKeyFactory	java.security.KeyFactory
    // Exception table:
    //   from	to	target	type
    //   0	14	55	java/lang/IllegalArgumentException
    //   14	53	65	java/security/NoSuchAlgorithmException
    //   14	53	116	java/security/spec/InvalidKeySpecException
  }
  
  @Nullable
  private final zzz zzd(Context paramContext, String paramString)
    throws zzaa
  {
    for (;;)
    {
      try
      {
        zzz localzzz1 = zze(paramContext, paramString);
        if (localzzz1 != null)
        {
          zza(paramContext, paramString, localzzz1);
          return localzzz1;
        }
        localzzz1 = null;
      }
      catch (zzaa localzzaa)
      {
        zzz localzzz2;
        continue;
        paramContext = localzzaa;
        continue;
      }
      try
      {
        localzzz2 = zza(paramContext.getSharedPreferences("com.google.android.gms.appid", 0), paramString);
        if (localzzz2 == null) {
          continue;
        }
        zza(paramContext, paramString, localzzz2, false);
        return localzzz2;
      }
      catch (zzaa paramContext)
      {
        if (paramContext == null) {
          break label69;
        }
      }
    }
    throw paramContext;
    label69:
    return null;
  }
  
  @Nullable
  private final zzz zze(Context paramContext, String paramString)
    throws zzaa
  {
    paramString = zzf(paramContext, paramString);
    if (!paramString.exists()) {
      return null;
    }
    try
    {
      paramContext = zza(paramString);
      return paramContext;
    }
    catch (zzaa paramContext)
    {
      if (Log.isLoggable("FirebaseInstanceId", 3))
      {
        paramContext = String.valueOf(paramContext);
        Log.d("FirebaseInstanceId", String.valueOf(paramContext).length() + 40 + "Failed to read key from file, retrying: " + paramContext);
      }
      try
      {
        paramContext = zza(paramString);
        return paramContext;
      }
      catch (IOException paramContext)
      {
        paramString = String.valueOf(paramContext);
        Log.w("FirebaseInstanceId", String.valueOf(paramString).length() + 45 + "IID file exists, but failed to read from it: " + paramString);
        throw new zzaa(paramContext);
      }
    }
    catch (IOException paramContext)
    {
      for (;;) {}
    }
  }
  
  private static File zzf(Context paramContext, String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      paramString = "com.google.InstanceId.properties";
    }
    for (;;)
    {
      return new File(zzb(paramContext), paramString);
      try
      {
        paramString = Base64.encodeToString(paramString.getBytes("UTF-8"), 11);
        paramString = String.valueOf(paramString).length() + 33 + "com.google.InstanceId_" + paramString + ".properties";
      }
      catch (UnsupportedEncodingException paramContext)
      {
        throw new AssertionError(paramContext);
      }
    }
  }
  
  @WorkerThread
  final zzz zzb(Context paramContext, String paramString)
    throws zzaa
  {
    zzz localzzz = zzd(paramContext, paramString);
    if (localzzz != null) {
      return localzzz;
    }
    return zzc(paramContext, paramString);
  }
  
  @WorkerThread
  final zzz zzc(Context paramContext, String paramString)
  {
    zzz localzzz1 = new zzz(zza.zzb(), System.currentTimeMillis());
    zzz localzzz2 = zza(paramContext, paramString, localzzz1, true);
    if ((localzzz2 != null) && (!localzzz2.equals(localzzz1)))
    {
      if (Log.isLoggable("FirebaseInstanceId", 3)) {
        Log.d("FirebaseInstanceId", "Loaded key after generating new one, using loaded one");
      }
      return localzzz2;
    }
    if (Log.isLoggable("FirebaseInstanceId", 3)) {
      Log.d("FirebaseInstanceId", "Generated new key");
    }
    zza(paramContext, paramString, localzzz1);
    return localzzz1;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */