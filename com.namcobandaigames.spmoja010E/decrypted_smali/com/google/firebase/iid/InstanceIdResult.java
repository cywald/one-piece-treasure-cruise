package com.google.firebase.iid;

import android.support.annotation.NonNull;

public abstract interface InstanceIdResult
{
  @NonNull
  public abstract String getId();
  
  @NonNull
  public abstract String getToken();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\InstanceIdResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */