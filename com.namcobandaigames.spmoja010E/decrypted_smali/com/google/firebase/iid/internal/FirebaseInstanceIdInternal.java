package com.google.firebase.iid.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract interface FirebaseInstanceIdInternal
{
  @KeepForSdk
  public abstract String getId();
  
  @Nullable
  @KeepForSdk
  public abstract String getToken();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\internal\FirebaseInstanceIdInternal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */