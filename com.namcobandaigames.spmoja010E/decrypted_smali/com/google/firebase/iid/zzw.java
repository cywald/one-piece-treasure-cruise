package com.google.firebase.iid;

import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzw
  implements zzv
{
  private final IBinder zzbo;
  
  zzw(IBinder paramIBinder)
  {
    this.zzbo = paramIBinder;
  }
  
  public final IBinder asBinder()
  {
    return this.zzbo;
  }
  
  public final void send(Message paramMessage)
    throws RemoteException
  {
    Parcel localParcel = Parcel.obtain();
    localParcel.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
    localParcel.writeInt(1);
    paramMessage.writeToParcel(localParcel, 0);
    try
    {
      this.zzbo.transact(1, localParcel, null, 1);
      return;
    }
    finally
    {
      localParcel.recycle();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */