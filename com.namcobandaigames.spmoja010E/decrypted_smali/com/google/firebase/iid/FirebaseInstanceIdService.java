package com.google.firebase.iid;

import android.content.Intent;
import android.support.annotation.WorkerThread;
import android.util.Log;
import java.util.Queue;

@Deprecated
public class FirebaseInstanceIdService
  extends zzb
{
  @Deprecated
  @WorkerThread
  public void onTokenRefresh() {}
  
  protected final Intent zzb(Intent paramIntent)
  {
    return (Intent)zzav.zzai().zzda.poll();
  }
  
  public final void zzd(Intent paramIntent)
  {
    if ("com.google.firebase.iid.TOKEN_REFRESH".equals(paramIntent.getAction())) {
      onTokenRefresh();
    }
    String str;
    do
    {
      do
      {
        return;
        str = paramIntent.getStringExtra("CMD");
      } while (str == null);
      if (Log.isLoggable("FirebaseInstanceId", 3))
      {
        paramIntent = String.valueOf(paramIntent.getExtras());
        Log.d("FirebaseInstanceId", String.valueOf(str).length() + 21 + String.valueOf(paramIntent).length() + "Received command: " + str + " - " + paramIntent);
      }
      if (("RST".equals(str)) || ("RST_FULL".equals(str)))
      {
        FirebaseInstanceId.getInstance().zzm();
        return;
      }
    } while (!"SYNC".equals(str));
    FirebaseInstanceId.getInstance().zzq();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\FirebaseInstanceIdService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */