package com.google.firebase.iid;

import android.os.Bundle;

final class zzam
  extends zzak<Bundle>
{
  zzam(int paramInt1, int paramInt2, Bundle paramBundle)
  {
    super(paramInt1, 1, paramBundle);
  }
  
  final boolean zzab()
  {
    return false;
  }
  
  final void zzb(Bundle paramBundle)
  {
    Bundle localBundle = paramBundle.getBundle("data");
    paramBundle = localBundle;
    if (localBundle == null) {
      paramBundle = Bundle.EMPTY;
    }
    finish(paramBundle);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzam.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */