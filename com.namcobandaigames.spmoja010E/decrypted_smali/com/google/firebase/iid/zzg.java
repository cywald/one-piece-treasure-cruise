package com.google.firebase.iid;

import android.util.Log;

final class zzg
  implements Runnable
{
  zzg(zzf paramzzf, zzd paramzzd) {}
  
  public final void run()
  {
    if (Log.isLoggable("EnhancedIntentService", 3)) {
      Log.d("EnhancedIntentService", "bg processing of the intent starting now");
    }
    zzf.zza(this.zzw).zzd(this.zzv.intent);
    this.zzv.finish();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */