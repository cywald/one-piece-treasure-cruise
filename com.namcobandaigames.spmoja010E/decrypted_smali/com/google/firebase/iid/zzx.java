package com.google.firebase.iid;

final class zzx
  implements InstanceIdResult
{
  private final String zzbp;
  private final String zzbq;
  
  zzx(String paramString1, String paramString2)
  {
    this.zzbp = paramString1;
    this.zzbq = paramString2;
  }
  
  public final String getId()
  {
    return this.zzbp;
  }
  
  public final String getToken()
  {
    return this.zzbq;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */