package com.google.firebase.iid;

import android.util.Base64;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.util.VisibleForTesting;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

final class zzz
{
  private final KeyPair zzbr;
  private final long zzbs;
  
  @VisibleForTesting
  zzz(KeyPair paramKeyPair, long paramLong)
  {
    this.zzbr = paramKeyPair;
    this.zzbs = paramLong;
  }
  
  private final String zzv()
  {
    return Base64.encodeToString(this.zzbr.getPublic().getEncoded(), 11);
  }
  
  private final String zzw()
  {
    return Base64.encodeToString(this.zzbr.getPrivate().getEncoded(), 11);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof zzz)) {}
    do
    {
      return false;
      paramObject = (zzz)paramObject;
    } while ((this.zzbs != ((zzz)paramObject).zzbs) || (!this.zzbr.getPublic().equals(((zzz)paramObject).zzbr.getPublic())) || (!this.zzbr.getPrivate().equals(((zzz)paramObject).zzbr.getPrivate())));
    return true;
  }
  
  final long getCreationTime()
  {
    return this.zzbs;
  }
  
  final KeyPair getKeyPair()
  {
    return this.zzbr;
  }
  
  public final int hashCode()
  {
    return Objects.hashCode(new Object[] { this.zzbr.getPublic(), this.zzbr.getPrivate(), Long.valueOf(this.zzbs) });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */