package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.DataCollectionDefaultChange;
import com.google.firebase.FirebaseApp;
import com.google.firebase.events.EventHandler;
import com.google.firebase.events.Subscriber;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.annotation.concurrent.GuardedBy;

public class FirebaseInstanceId
{
  private static final long zzai = TimeUnit.HOURS.toSeconds(8L);
  private static zzaw zzaj;
  @VisibleForTesting
  @GuardedBy("FirebaseInstanceId.class")
  private static ScheduledThreadPoolExecutor zzak;
  private final Executor zzal;
  private final FirebaseApp zzam;
  private final zzan zzan;
  private MessagingChannel zzao;
  private final zzaq zzap;
  private final zzba zzaq;
  @GuardedBy("this")
  private boolean zzar = false;
  private final zza zzas;
  
  FirebaseInstanceId(FirebaseApp paramFirebaseApp, Subscriber paramSubscriber)
  {
    this(paramFirebaseApp, new zzan(paramFirebaseApp.getApplicationContext()), zzi.zzf(), zzi.zzf(), paramSubscriber);
  }
  
  private FirebaseInstanceId(FirebaseApp paramFirebaseApp, zzan paramzzan, Executor paramExecutor1, Executor paramExecutor2, Subscriber paramSubscriber)
  {
    if (zzan.zza(paramFirebaseApp) == null) {
      throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
    }
    for (;;)
    {
      try
      {
        if (zzaj == null) {
          zzaj = new zzaw(paramFirebaseApp.getApplicationContext());
        }
        this.zzam = paramFirebaseApp;
        this.zzan = paramzzan;
        if (this.zzao == null)
        {
          MessagingChannel localMessagingChannel = (MessagingChannel)paramFirebaseApp.get(MessagingChannel.class);
          if ((localMessagingChannel != null) && (localMessagingChannel.isAvailable())) {
            this.zzao = localMessagingChannel;
          }
        }
        else
        {
          this.zzao = this.zzao;
          this.zzal = paramExecutor2;
          this.zzaq = new zzba(zzaj);
          this.zzas = new zza(paramSubscriber);
          this.zzap = new zzaq(paramExecutor1);
          if (this.zzas.isEnabled()) {
            zzg();
          }
          return;
        }
      }
      finally {}
      this.zzao = new zzr(paramFirebaseApp, paramzzan, paramExecutor1);
    }
  }
  
  public static FirebaseInstanceId getInstance()
  {
    return getInstance(FirebaseApp.getInstance());
  }
  
  @Keep
  public static FirebaseInstanceId getInstance(@NonNull FirebaseApp paramFirebaseApp)
  {
    try
    {
      paramFirebaseApp = (FirebaseInstanceId)paramFirebaseApp.get(FirebaseInstanceId.class);
      return paramFirebaseApp;
    }
    finally
    {
      paramFirebaseApp = finally;
      throw paramFirebaseApp;
    }
  }
  
  private final void startSync()
  {
    try
    {
      if (!this.zzar) {
        zza(0L);
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private final Task<InstanceIdResult> zza(String paramString1, String paramString2)
  {
    String str = zzd(paramString2);
    TaskCompletionSource localTaskCompletionSource = new TaskCompletionSource();
    this.zzal.execute(new zzn(this, paramString1, paramString2, localTaskCompletionSource, str));
    return localTaskCompletionSource.getTask();
  }
  
  private final <T> T zza(Task<T> paramTask)
    throws IOException
  {
    try
    {
      paramTask = Tasks.await(paramTask, 30000L, TimeUnit.MILLISECONDS);
      return paramTask;
    }
    catch (ExecutionException paramTask)
    {
      Throwable localThrowable = paramTask.getCause();
      if ((localThrowable instanceof IOException))
      {
        if ("INSTANCE_ID_RESET".equals(localThrowable.getMessage())) {
          zzm();
        }
        throw ((IOException)localThrowable);
      }
      if ((localThrowable instanceof RuntimeException)) {
        throw ((RuntimeException)localThrowable);
      }
      throw new IOException(paramTask);
    }
    catch (InterruptedException paramTask)
    {
      throw new IOException("SERVICE_NOT_AVAILABLE");
    }
    catch (TimeoutException paramTask)
    {
      for (;;) {}
    }
  }
  
  static void zza(Runnable paramRunnable, long paramLong)
  {
    try
    {
      if (zzak == null) {
        zzak = new ScheduledThreadPoolExecutor(1, new NamedThreadFactory("FirebaseInstanceId"));
      }
      zzak.schedule(paramRunnable, paramLong, TimeUnit.SECONDS);
      return;
    }
    finally {}
  }
  
  @Nullable
  @VisibleForTesting
  private static zzax zzb(String paramString1, String paramString2)
  {
    return zzaj.zzb("", paramString1, paramString2);
  }
  
  private static String zzd(String paramString)
  {
    String str;
    if ((!paramString.isEmpty()) && (!paramString.equalsIgnoreCase("fcm")))
    {
      str = paramString;
      if (!paramString.equalsIgnoreCase("gcm")) {}
    }
    else
    {
      str = "*";
    }
    return str;
  }
  
  private final void zzg()
  {
    zzax localzzax = zzj();
    if ((!zzo()) || (localzzax == null) || (localzzax.zzj(this.zzan.zzad())) || (this.zzaq.zzaq())) {
      startSync();
    }
  }
  
  private static String zzi()
  {
    return zzan.zza(zzaj.zzg("").getKeyPair());
  }
  
  static boolean zzl()
  {
    return (Log.isLoggable("FirebaseInstanceId", 3)) || ((Build.VERSION.SDK_INT == 23) && (Log.isLoggable("FirebaseInstanceId", 3)));
  }
  
  @WorkerThread
  public void deleteInstanceId()
    throws IOException
  {
    if (Looper.getMainLooper() == Looper.myLooper()) {
      throw new IOException("MAIN_THREAD");
    }
    String str = zzi();
    zza(this.zzao.deleteInstanceId(str));
    zzm();
  }
  
  @WorkerThread
  public void deleteToken(String paramString1, String paramString2)
    throws IOException
  {
    if (Looper.getMainLooper() == Looper.myLooper()) {
      throw new IOException("MAIN_THREAD");
    }
    paramString2 = zzd(paramString2);
    String str1 = zzi();
    String str2 = zzax.zza(zzb(paramString1, paramString2));
    zza(this.zzao.deleteToken(str1, str2, paramString1, paramString2));
    zzaj.zzc("", paramString1, paramString2);
  }
  
  public long getCreationTime()
  {
    return zzaj.zzg("").getCreationTime();
  }
  
  @WorkerThread
  public String getId()
  {
    zzg();
    return zzi();
  }
  
  @NonNull
  public Task<InstanceIdResult> getInstanceId()
  {
    return zza(zzan.zza(this.zzam), "*");
  }
  
  @Deprecated
  @Nullable
  public String getToken()
  {
    zzax localzzax = zzj();
    if ((localzzax == null) || (localzzax.zzj(this.zzan.zzad()))) {
      startSync();
    }
    if (localzzax != null) {
      return localzzax.zzbq;
    }
    return null;
  }
  
  @WorkerThread
  public String getToken(String paramString1, String paramString2)
    throws IOException
  {
    if (Looper.getMainLooper() == Looper.myLooper()) {
      throw new IOException("MAIN_THREAD");
    }
    return ((InstanceIdResult)zza(zza(paramString1, paramString2))).getToken();
  }
  
  public final Task<Void> zza(String paramString)
  {
    try
    {
      paramString = this.zzaq.zza(paramString);
      startSync();
      return paramString;
    }
    finally
    {
      paramString = finally;
      throw paramString;
    }
  }
  
  final void zza(long paramLong)
  {
    try
    {
      long l = Math.min(Math.max(30L, paramLong << 1), zzai);
      zza(new zzay(this, this.zzan, this.zzaq, l), paramLong);
      this.zzar = true;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final void zza(boolean paramBoolean)
  {
    try
    {
      this.zzar = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final void zzb(String paramString)
    throws IOException
  {
    Object localObject = zzj();
    if ((localObject == null) || (((zzax)localObject).zzj(this.zzan.zzad()))) {
      throw new IOException("token not available");
    }
    String str = zzi();
    localObject = ((zzax)localObject).zzbq;
    zza(this.zzao.subscribeToTopic(str, (String)localObject, paramString));
  }
  
  @VisibleForTesting
  public final void zzb(boolean paramBoolean)
  {
    this.zzas.setEnabled(paramBoolean);
  }
  
  final void zzc(String paramString)
    throws IOException
  {
    zzax localzzax = zzj();
    if ((localzzax == null) || (localzzax.zzj(this.zzan.zzad()))) {
      throw new IOException("token not available");
    }
    String str = zzi();
    zza(this.zzao.unsubscribeFromTopic(str, localzzax.zzbq, paramString));
  }
  
  final FirebaseApp zzh()
  {
    return this.zzam;
  }
  
  @Nullable
  final zzax zzj()
  {
    return zzb(zzan.zza(this.zzam), "*");
  }
  
  final String zzk()
    throws IOException
  {
    return getToken(zzan.zza(this.zzam), "*");
  }
  
  final void zzm()
  {
    try
    {
      zzaj.zzal();
      if (this.zzas.isEnabled()) {
        startSync();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final boolean zzn()
  {
    return this.zzao.isAvailable();
  }
  
  final boolean zzo()
  {
    return this.zzao.isChannelBuilt();
  }
  
  final void zzp()
    throws IOException
  {
    String str1 = zzi();
    String str2 = zzax.zza(zzj());
    zza(this.zzao.buildChannel(str1, str2));
  }
  
  final void zzq()
  {
    zzaj.zzh("");
    startSync();
  }
  
  @VisibleForTesting
  public final boolean zzr()
  {
    return this.zzas.isEnabled();
  }
  
  private final class zza
  {
    private final boolean zzaz;
    private final Subscriber zzba;
    @Nullable
    @GuardedBy("this")
    private EventHandler<DataCollectionDefaultChange> zzbb;
    @Nullable
    @GuardedBy("this")
    private Boolean zzbc;
    
    zza(Subscriber paramSubscriber)
    {
      this.zzba = paramSubscriber;
      this.zzaz = zzu();
      this.zzbc = zzt();
      if ((this.zzbc == null) && (this.zzaz))
      {
        this.zzbb = new zzq(this);
        paramSubscriber.subscribe(DataCollectionDefaultChange.class, this.zzbb);
      }
    }
    
    @Nullable
    private final Boolean zzt()
    {
      Object localObject1 = FirebaseInstanceId.zza(FirebaseInstanceId.this).getApplicationContext();
      Object localObject2 = ((Context)localObject1).getSharedPreferences("com.google.firebase.messaging", 0);
      if (((SharedPreferences)localObject2).contains("auto_init")) {
        return Boolean.valueOf(((SharedPreferences)localObject2).getBoolean("auto_init", false));
      }
      try
      {
        localObject2 = ((Context)localObject1).getPackageManager();
        if (localObject2 != null)
        {
          localObject1 = ((PackageManager)localObject2).getApplicationInfo(((Context)localObject1).getPackageName(), 128);
          if ((localObject1 != null) && (((ApplicationInfo)localObject1).metaData != null) && (((ApplicationInfo)localObject1).metaData.containsKey("firebase_messaging_auto_init_enabled")))
          {
            boolean bool = ((ApplicationInfo)localObject1).metaData.getBoolean("firebase_messaging_auto_init_enabled");
            return Boolean.valueOf(bool);
          }
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
      return null;
    }
    
    private final boolean zzu()
    {
      try
      {
        Class.forName("com.google.firebase.messaging.FirebaseMessaging");
        return true;
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        Object localObject;
        do
        {
          localObject = FirebaseInstanceId.zza(FirebaseInstanceId.this).getApplicationContext();
          Intent localIntent = new Intent("com.google.firebase.MESSAGING_EVENT");
          localIntent.setPackage(((Context)localObject).getPackageName());
          localObject = ((Context)localObject).getPackageManager().resolveService(localIntent, 0);
          if (localObject == null) {
            break;
          }
        } while (((ResolveInfo)localObject).serviceInfo != null);
      }
      return false;
    }
    
    /* Error */
    final boolean isEnabled()
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield 43	com/google/firebase/iid/FirebaseInstanceId$zza:zzbc	Ljava/lang/Boolean;
      //   6: ifnull +15 -> 21
      //   9: aload_0
      //   10: getfield 43	com/google/firebase/iid/FirebaseInstanceId$zza:zzbc	Ljava/lang/Boolean;
      //   13: invokevirtual 161	java/lang/Boolean:booleanValue	()Z
      //   16: istore_1
      //   17: aload_0
      //   18: monitorexit
      //   19: iload_1
      //   20: ireturn
      //   21: aload_0
      //   22: getfield 37	com/google/firebase/iid/FirebaseInstanceId$zza:zzaz	Z
      //   25: ifeq +23 -> 48
      //   28: aload_0
      //   29: getfield 26	com/google/firebase/iid/FirebaseInstanceId$zza:zzbd	Lcom/google/firebase/iid/FirebaseInstanceId;
      //   32: invokestatic 64	com/google/firebase/iid/FirebaseInstanceId:zza	(Lcom/google/firebase/iid/FirebaseInstanceId;)Lcom/google/firebase/FirebaseApp;
      //   35: invokevirtual 164	com/google/firebase/FirebaseApp:isDataCollectionDefaultEnabled	()Z
      //   38: istore_1
      //   39: iload_1
      //   40: ifeq +8 -> 48
      //   43: iconst_1
      //   44: istore_1
      //   45: goto -28 -> 17
      //   48: iconst_0
      //   49: istore_1
      //   50: goto -33 -> 17
      //   53: astore_2
      //   54: aload_0
      //   55: monitorexit
      //   56: aload_2
      //   57: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	58	0	this	zza
      //   16	34	1	bool	boolean
      //   53	4	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   2	17	53	finally
      //   21	39	53	finally
    }
    
    final void setEnabled(boolean paramBoolean)
    {
      try
      {
        if (this.zzbb != null)
        {
          this.zzba.unsubscribe(DataCollectionDefaultChange.class, this.zzbb);
          this.zzbb = null;
        }
        SharedPreferences.Editor localEditor = FirebaseInstanceId.zza(FirebaseInstanceId.this).getApplicationContext().getSharedPreferences("com.google.firebase.messaging", 0).edit();
        localEditor.putBoolean("auto_init", paramBoolean);
        localEditor.apply();
        if (paramBoolean) {
          FirebaseInstanceId.zzb(FirebaseInstanceId.this);
        }
        this.zzbc = Boolean.valueOf(paramBoolean);
        return;
      }
      finally {}
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\FirebaseInstanceId.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */