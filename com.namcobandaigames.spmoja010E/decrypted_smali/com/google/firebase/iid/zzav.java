package com.google.firebase.iid;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Queue;
import javax.annotation.concurrent.GuardedBy;

public final class zzav
{
  private static zzav zzcx;
  @GuardedBy("serviceClassNames")
  private final SimpleArrayMap<String, String> zzcy = new SimpleArrayMap();
  private Boolean zzcz = null;
  @VisibleForTesting
  final Queue<Intent> zzda = new ArrayDeque();
  @VisibleForTesting
  private final Queue<Intent> zzdb = new ArrayDeque();
  
  public static PendingIntent zza(Context paramContext, int paramInt1, Intent paramIntent, int paramInt2)
  {
    return PendingIntent.getBroadcast(paramContext, paramInt1, zza(paramContext, "com.google.firebase.MESSAGING_EVENT", paramIntent), 1073741824);
  }
  
  private static Intent zza(Context paramContext, String paramString, Intent paramIntent)
  {
    paramContext = new Intent(paramContext, FirebaseInstanceIdReceiver.class);
    paramContext.setAction(paramString);
    paramContext.putExtra("wrapped_intent", paramIntent);
    return paramContext;
  }
  
  public static zzav zzai()
  {
    try
    {
      if (zzcx == null) {
        zzcx = new zzav();
      }
      zzav localzzav = zzcx;
      return localzzav;
    }
    finally {}
  }
  
  public static void zzb(Context paramContext, Intent paramIntent)
  {
    paramContext.sendBroadcast(zza(paramContext, "com.google.firebase.INSTANCE_ID_EVENT", paramIntent));
  }
  
  public static void zzc(Context paramContext, Intent paramIntent)
  {
    paramContext.sendBroadcast(zza(paramContext, "com.google.firebase.MESSAGING_EVENT", paramIntent));
  }
  
  private final int zzd(Context paramContext, Intent paramIntent)
  {
    synchronized (this.zzcy)
    {
      ??? = (String)this.zzcy.get(paramIntent.getAction());
      ??? = ???;
      if (??? == null)
      {
        ??? = paramContext.getPackageManager().resolveService(paramIntent, 0);
        if ((??? == null) || (((ResolveInfo)???).serviceInfo == null)) {
          Log.e("FirebaseInstanceId", "Failed to resolve target intent service, skipping classname enforcement");
        }
      }
    }
    try
    {
      boolean bool;
      if (this.zzcz == null)
      {
        if (paramContext.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0)
        {
          bool = true;
          label87:
          this.zzcz = Boolean.valueOf(bool);
        }
      }
      else
      {
        if (!this.zzcz.booleanValue()) {
          break label410;
        }
        paramContext = WakefulBroadcastReceiver.startWakefulService(paramContext, paramIntent);
      }
      for (;;)
      {
        if (paramContext != null) {
          break label441;
        }
        Log.e("FirebaseInstanceId", "Error while delivering the message: ServiceIntent not found.");
        return 404;
        paramContext = finally;
        throw paramContext;
        ??? = ((ResolveInfo)???).serviceInfo;
        if ((!paramContext.getPackageName().equals(((ServiceInfo)???).packageName)) || (((ServiceInfo)???).name == null))
        {
          ??? = ((ServiceInfo)???).packageName;
          ??? = ((ServiceInfo)???).name;
          Log.e("FirebaseInstanceId", String.valueOf(???).length() + 94 + String.valueOf(???).length() + "Error resolving target intent service, skipping classname enforcement. Resolved service was: " + (String)??? + "/" + (String)???);
          break;
        }
        ??? = ((ServiceInfo)???).name;
        ??? = ???;
        if (((String)???).startsWith("."))
        {
          ??? = String.valueOf(paramContext.getPackageName());
          ??? = String.valueOf(???);
          if (((String)???).length() == 0) {
            break label371;
          }
          ??? = ((String)???).concat((String)???);
        }
        for (;;)
        {
          synchronized (this.zzcy)
          {
            this.zzcy.put(paramIntent.getAction(), ???);
            if (Log.isLoggable("FirebaseInstanceId", 3))
            {
              ??? = String.valueOf(???);
              if (((String)???).length() != 0)
              {
                ??? = "Restricting intent to a specific service: ".concat((String)???);
                Log.d("FirebaseInstanceId", (String)???);
              }
            }
            else
            {
              paramIntent.setClassName(paramContext.getPackageName(), (String)???);
              break;
              label371:
              ??? = new String((String)???);
            }
          }
          ??? = new String("Restricting intent to a specific service: ");
        }
        bool = false;
        break label87;
        label410:
        paramContext = paramContext.startService(paramIntent);
        Log.d("FirebaseInstanceId", "Missing wake lock permission, service start may be delayed");
      }
      label441:
      return 402;
    }
    catch (SecurityException paramContext)
    {
      Log.e("FirebaseInstanceId", "Error while delivering the message to the serviceIntent", paramContext);
      return 401;
      return -1;
    }
    catch (IllegalStateException paramContext)
    {
      paramContext = String.valueOf(paramContext);
      Log.e("FirebaseInstanceId", String.valueOf(paramContext).length() + 45 + "Failed to start service while in background: " + paramContext);
    }
  }
  
  public final Intent zzaj()
  {
    return (Intent)this.zzdb.poll();
  }
  
  public final int zzb(Context paramContext, String paramString, Intent paramIntent)
  {
    String str;
    int i;
    if (Log.isLoggable("FirebaseInstanceId", 3))
    {
      str = String.valueOf(paramString);
      if (str.length() != 0)
      {
        str = "Starting service: ".concat(str);
        Log.d("FirebaseInstanceId", str);
      }
    }
    else
    {
      i = -1;
      switch (paramString.hashCode())
      {
      default: 
        switch (i)
        {
        default: 
          label72:
          paramContext = String.valueOf(paramString);
          if (paramContext.length() == 0) {}
          break;
        }
        break;
      }
    }
    for (paramContext = "Unknown service action: ".concat(paramContext);; paramContext = new String("Unknown service action: "))
    {
      Log.w("FirebaseInstanceId", paramContext);
      return 500;
      str = new String("Starting service: ");
      break;
      if (!paramString.equals("com.google.firebase.INSTANCE_ID_EVENT")) {
        break label72;
      }
      i = 0;
      break label72;
      if (!paramString.equals("com.google.firebase.MESSAGING_EVENT")) {
        break label72;
      }
      i = 1;
      break label72;
      this.zzda.offer(paramIntent);
      for (;;)
      {
        paramString = new Intent(paramString);
        paramString.setPackage(paramContext.getPackageName());
        return zzd(paramContext, paramString);
        this.zzdb.offer(paramIntent);
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzav.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */