package com.google.firebase.iid;

import android.content.BroadcastReceiver.PendingResult;
import android.content.Intent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class zzd
{
  final Intent intent;
  private final BroadcastReceiver.PendingResult zzp;
  private boolean zzq = false;
  private final ScheduledFuture<?> zzr;
  
  zzd(Intent paramIntent, BroadcastReceiver.PendingResult paramPendingResult, ScheduledExecutorService paramScheduledExecutorService)
  {
    this.intent = paramIntent;
    this.zzp = paramPendingResult;
    this.zzr = paramScheduledExecutorService.schedule(new zze(this, paramIntent), 9000L, TimeUnit.MILLISECONDS);
  }
  
  final void finish()
  {
    try
    {
      if (!this.zzq)
      {
        this.zzp.finish();
        this.zzr.cancel(false);
        this.zzq = true;
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */