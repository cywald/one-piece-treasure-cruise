package com.google.firebase.iid;

import android.os.IInterface;
import android.os.Message;
import android.os.RemoteException;

abstract interface zzv
  extends IInterface
{
  public abstract void send(Message paramMessage)
    throws RemoteException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */