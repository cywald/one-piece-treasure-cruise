package com.google.firebase.iid;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.concurrent.GuardedBy;

final class zzat
{
  private static int zzcf = 0;
  private static PendingIntent zzcr;
  private final zzan zzan;
  @GuardedBy("responseCallbacks")
  private final SimpleArrayMap<String, TaskCompletionSource<Bundle>> zzcs = new SimpleArrayMap();
  private Messenger zzct;
  private Messenger zzcu;
  private zzl zzcv;
  private final Context zzx;
  
  public zzat(Context paramContext, zzan paramzzan)
  {
    this.zzx = paramContext;
    this.zzan = paramzzan;
    this.zzct = new Messenger(new zzau(this, Looper.getMainLooper()));
  }
  
  private static void zza(Context paramContext, Intent paramIntent)
  {
    try
    {
      if (zzcr == null)
      {
        Intent localIntent = new Intent();
        localIntent.setPackage("com.google.example.invalidpackage");
        zzcr = PendingIntent.getBroadcast(paramContext, 0, localIntent, 0);
      }
      paramIntent.putExtra("app", zzcr);
      return;
    }
    finally {}
  }
  
  private final void zza(String paramString, Bundle paramBundle)
  {
    TaskCompletionSource localTaskCompletionSource;
    synchronized (this.zzcs)
    {
      localTaskCompletionSource = (TaskCompletionSource)this.zzcs.remove(paramString);
      if (localTaskCompletionSource == null)
      {
        paramString = String.valueOf(paramString);
        if (paramString.length() != 0)
        {
          paramString = "Missing callback for ".concat(paramString);
          Log.w("FirebaseInstanceId", paramString);
          return;
        }
        paramString = new String("Missing callback for ");
      }
    }
    localTaskCompletionSource.setResult(paramBundle);
  }
  
  private static String zzah()
  {
    try
    {
      int i = zzcf;
      zzcf = i + 1;
      String str = Integer.toString(i);
      return str;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private final void zzb(Message paramMessage)
  {
    if ((paramMessage != null) && ((paramMessage.obj instanceof Intent)))
    {
      Object localObject1 = (Intent)paramMessage.obj;
      ((Intent)localObject1).setExtrasClassLoader(new zzl.zza());
      if (((Intent)localObject1).hasExtra("google.messenger"))
      {
        localObject1 = ((Intent)localObject1).getParcelableExtra("google.messenger");
        if ((localObject1 instanceof zzl)) {
          this.zzcv = ((zzl)localObject1);
        }
        if ((localObject1 instanceof Messenger)) {
          this.zzcu = ((Messenger)localObject1);
        }
      }
      Object localObject4 = (Intent)paramMessage.obj;
      paramMessage = ((Intent)localObject4).getAction();
      if (!"com.google.android.c2dm.intent.REGISTRATION".equals(paramMessage)) {
        if (Log.isLoggable("FirebaseInstanceId", 3))
        {
          paramMessage = String.valueOf(paramMessage);
          if (paramMessage.length() == 0) {
            break label139;
          }
          paramMessage = "Unexpected response action: ".concat(paramMessage);
          Log.d("FirebaseInstanceId", paramMessage);
        }
      }
      label139:
      label343:
      label356:
      label400:
      do
      {
        return;
        paramMessage = new String("Unexpected response action: ");
        break;
        localObject1 = ((Intent)localObject4).getStringExtra("registration_id");
        paramMessage = (Message)localObject1;
        if (localObject1 == null) {
          paramMessage = ((Intent)localObject4).getStringExtra("unregistered");
        }
        if (paramMessage == null)
        {
          localObject1 = ((Intent)localObject4).getStringExtra("error");
          if (localObject1 == null)
          {
            paramMessage = String.valueOf(((Intent)localObject4).getExtras());
            Log.w("FirebaseInstanceId", String.valueOf(paramMessage).length() + 49 + "Unexpected response, no error or registration id " + paramMessage);
            return;
          }
          if (Log.isLoggable("FirebaseInstanceId", 3))
          {
            paramMessage = String.valueOf(localObject1);
            if (paramMessage.length() != 0)
            {
              paramMessage = "Received InstanceID error ".concat(paramMessage);
              Log.d("FirebaseInstanceId", paramMessage);
            }
          }
          else
          {
            if (!((String)localObject1).startsWith("|")) {
              break label400;
            }
            paramMessage = ((String)localObject1).split("\\|");
            if ((paramMessage.length > 2) && ("ID".equals(paramMessage[1]))) {
              break label356;
            }
            paramMessage = String.valueOf(localObject1);
            if (paramMessage.length() == 0) {
              break label343;
            }
          }
          for (paramMessage = "Unexpected structured response ".concat(paramMessage);; paramMessage = new String("Unexpected structured response "))
          {
            Log.w("FirebaseInstanceId", paramMessage);
            return;
            paramMessage = new String("Received InstanceID error ");
            break;
          }
          String str = paramMessage[2];
          localObject1 = paramMessage[3];
          paramMessage = (Message)localObject1;
          if (((String)localObject1).startsWith(":")) {
            paramMessage = ((String)localObject1).substring(1);
          }
          zza(str, ((Intent)localObject4).putExtra("error", paramMessage).getExtras());
          return;
          paramMessage = this.zzcs;
          int i = 0;
          try
          {
            while (i < this.zzcs.size())
            {
              zza((String)this.zzcs.keyAt(i), ((Intent)localObject4).getExtras());
              i += 1;
            }
            return;
          }
          finally {}
        }
        localObject3 = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(paramMessage);
        if (((Matcher)localObject3).matches()) {
          break label523;
        }
      } while (!Log.isLoggable("FirebaseInstanceId", 3));
      paramMessage = String.valueOf(paramMessage);
      if (paramMessage.length() != 0) {}
      for (paramMessage = "Unexpected response string: ".concat(paramMessage);; paramMessage = new String("Unexpected response string: "))
      {
        Log.d("FirebaseInstanceId", paramMessage);
        return;
      }
      label523:
      paramMessage = ((Matcher)localObject3).group(1);
      Object localObject3 = ((Matcher)localObject3).group(2);
      localObject4 = ((Intent)localObject4).getExtras();
      ((Bundle)localObject4).putString("registration_id", (String)localObject3);
      zza(paramMessage, (Bundle)localObject4);
      return;
    }
    Log.w("FirebaseInstanceId", "Dropping invalid message");
  }
  
  private final Bundle zzd(Bundle paramBundle)
    throws IOException
  {
    Bundle localBundle2 = zze(paramBundle);
    Bundle localBundle1 = localBundle2;
    if (localBundle2 != null)
    {
      localBundle1 = localBundle2;
      if (localBundle2.containsKey("google.messenger"))
      {
        paramBundle = zze(paramBundle);
        localBundle1 = paramBundle;
        if (paramBundle != null)
        {
          localBundle1 = paramBundle;
          if (paramBundle.containsKey("google.messenger")) {
            localBundle1 = null;
          }
        }
      }
    }
    return localBundle1;
  }
  
  /* Error */
  private final Bundle zze(Bundle arg1)
    throws IOException
  {
    // Byte code:
    //   0: invokestatic 294	com/google/firebase/iid/zzat:zzah	()Ljava/lang/String;
    //   3: astore_2
    //   4: new 96	com/google/android/gms/tasks/TaskCompletionSource
    //   7: dup
    //   8: invokespecial 295	com/google/android/gms/tasks/TaskCompletionSource:<init>	()V
    //   11: astore_3
    //   12: aload_0
    //   13: getfield 37	com/google/firebase/iid/zzat:zzcs	Landroid/support/v4/util/SimpleArrayMap;
    //   16: astore 4
    //   18: aload 4
    //   20: monitorenter
    //   21: aload_0
    //   22: getfield 37	com/google/firebase/iid/zzat:zzcs	Landroid/support/v4/util/SimpleArrayMap;
    //   25: aload_2
    //   26: aload_3
    //   27: invokevirtual 299	android/support/v4/util/SimpleArrayMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   30: pop
    //   31: aload 4
    //   33: monitorexit
    //   34: aload_0
    //   35: getfield 41	com/google/firebase/iid/zzat:zzan	Lcom/google/firebase/iid/zzan;
    //   38: invokevirtual 304	com/google/firebase/iid/zzan:zzac	()I
    //   41: ifne +20 -> 61
    //   44: new 277	java/io/IOException
    //   47: dup
    //   48: ldc_w 306
    //   51: invokespecial 307	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   54: athrow
    //   55: astore_1
    //   56: aload 4
    //   58: monitorexit
    //   59: aload_1
    //   60: athrow
    //   61: new 65	android/content/Intent
    //   64: dup
    //   65: invokespecial 66	android/content/Intent:<init>	()V
    //   68: astore 4
    //   70: aload 4
    //   72: ldc_w 309
    //   75: invokevirtual 72	android/content/Intent:setPackage	(Ljava/lang/String;)Landroid/content/Intent;
    //   78: pop
    //   79: aload_0
    //   80: getfield 41	com/google/firebase/iid/zzat:zzan	Lcom/google/firebase/iid/zzan;
    //   83: invokevirtual 304	com/google/firebase/iid/zzan:zzac	()I
    //   86: iconst_2
    //   87: if_icmpne +213 -> 300
    //   90: aload 4
    //   92: ldc_w 311
    //   95: invokevirtual 314	android/content/Intent:setAction	(Ljava/lang/String;)Landroid/content/Intent;
    //   98: pop
    //   99: aload 4
    //   101: aload_1
    //   102: invokevirtual 318	android/content/Intent:putExtras	(Landroid/os/Bundle;)Landroid/content/Intent;
    //   105: pop
    //   106: aload_0
    //   107: getfield 39	com/google/firebase/iid/zzat:zzx	Landroid/content/Context;
    //   110: aload 4
    //   112: invokestatic 320	com/google/firebase/iid/zzat:zza	(Landroid/content/Context;Landroid/content/Intent;)V
    //   115: aload 4
    //   117: ldc_w 322
    //   120: new 197	java/lang/StringBuilder
    //   123: dup
    //   124: aload_2
    //   125: invokestatic 102	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   128: invokevirtual 106	java/lang/String:length	()I
    //   131: iconst_5
    //   132: iadd
    //   133: invokespecial 200	java/lang/StringBuilder:<init>	(I)V
    //   136: ldc_w 324
    //   139: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   142: aload_2
    //   143: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   146: ldc -44
    //   148: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   151: invokevirtual 208	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   154: invokevirtual 233	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   157: pop
    //   158: ldc 114
    //   160: iconst_3
    //   161: invokestatic 177	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   164: ifeq +48 -> 212
    //   167: aload 4
    //   169: invokevirtual 195	android/content/Intent:getExtras	()Landroid/os/Bundle;
    //   172: invokestatic 102	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   175: astore_1
    //   176: ldc 114
    //   178: new 197	java/lang/StringBuilder
    //   181: dup
    //   182: aload_1
    //   183: invokestatic 102	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   186: invokevirtual 106	java/lang/String:length	()I
    //   189: bipush 8
    //   191: iadd
    //   192: invokespecial 200	java/lang/StringBuilder:<init>	(I)V
    //   195: ldc_w 326
    //   198: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   201: aload_1
    //   202: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   205: invokevirtual 208	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   208: invokestatic 182	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   211: pop
    //   212: aload 4
    //   214: ldc -106
    //   216: aload_0
    //   217: getfield 59	com/google/firebase/iid/zzat:zzct	Landroid/os/Messenger;
    //   220: invokevirtual 84	android/content/Intent:putExtra	(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    //   223: pop
    //   224: aload_0
    //   225: getfield 164	com/google/firebase/iid/zzat:zzcu	Landroid/os/Messenger;
    //   228: ifnonnull +10 -> 238
    //   231: aload_0
    //   232: getfield 162	com/google/firebase/iid/zzat:zzcv	Lcom/google/firebase/iid/zzl;
    //   235: ifnull +107 -> 342
    //   238: invokestatic 330	android/os/Message:obtain	()Landroid/os/Message;
    //   241: astore_1
    //   242: aload_1
    //   243: aload 4
    //   245: putfield 141	android/os/Message:obj	Ljava/lang/Object;
    //   248: aload_0
    //   249: getfield 164	com/google/firebase/iid/zzat:zzcu	Landroid/os/Messenger;
    //   252: ifnull +60 -> 312
    //   255: aload_0
    //   256: getfield 164	com/google/firebase/iid/zzat:zzcu	Landroid/os/Messenger;
    //   259: aload_1
    //   260: invokevirtual 333	android/os/Messenger:send	(Landroid/os/Message;)V
    //   263: aload_3
    //   264: invokevirtual 337	com/google/android/gms/tasks/TaskCompletionSource:getTask	()Lcom/google/android/gms/tasks/Task;
    //   267: ldc2_w 338
    //   270: getstatic 345	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   273: invokestatic 351	com/google/android/gms/tasks/Tasks:await	(Lcom/google/android/gms/tasks/Task;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    //   276: checkcast 267	android/os/Bundle
    //   279: astore_3
    //   280: aload_0
    //   281: getfield 37	com/google/firebase/iid/zzat:zzcs	Landroid/support/v4/util/SimpleArrayMap;
    //   284: astore_1
    //   285: aload_1
    //   286: monitorenter
    //   287: aload_0
    //   288: getfield 37	com/google/firebase/iid/zzat:zzcs	Landroid/support/v4/util/SimpleArrayMap;
    //   291: aload_2
    //   292: invokevirtual 94	android/support/v4/util/SimpleArrayMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
    //   295: pop
    //   296: aload_1
    //   297: monitorexit
    //   298: aload_3
    //   299: areturn
    //   300: aload 4
    //   302: ldc_w 353
    //   305: invokevirtual 314	android/content/Intent:setAction	(Ljava/lang/String;)Landroid/content/Intent;
    //   308: pop
    //   309: goto -210 -> 99
    //   312: aload_0
    //   313: getfield 162	com/google/firebase/iid/zzat:zzcv	Lcom/google/firebase/iid/zzl;
    //   316: aload_1
    //   317: invokevirtual 354	com/google/firebase/iid/zzl:send	(Landroid/os/Message;)V
    //   320: goto -57 -> 263
    //   323: astore_1
    //   324: ldc 114
    //   326: iconst_3
    //   327: invokestatic 177	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   330: ifeq +12 -> 342
    //   333: ldc 114
    //   335: ldc_w 356
    //   338: invokestatic 182	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   341: pop
    //   342: aload_0
    //   343: getfield 41	com/google/firebase/iid/zzat:zzan	Lcom/google/firebase/iid/zzan;
    //   346: invokevirtual 304	com/google/firebase/iid/zzan:zzac	()I
    //   349: iconst_2
    //   350: if_icmpne +15 -> 365
    //   353: aload_0
    //   354: getfield 39	com/google/firebase/iid/zzat:zzx	Landroid/content/Context;
    //   357: aload 4
    //   359: invokevirtual 362	android/content/Context:sendBroadcast	(Landroid/content/Intent;)V
    //   362: goto -99 -> 263
    //   365: aload_0
    //   366: getfield 39	com/google/firebase/iid/zzat:zzx	Landroid/content/Context;
    //   369: aload 4
    //   371: invokevirtual 366	android/content/Context:startService	(Landroid/content/Intent;)Landroid/content/ComponentName;
    //   374: pop
    //   375: goto -112 -> 263
    //   378: astore_2
    //   379: aload_1
    //   380: monitorexit
    //   381: aload_2
    //   382: athrow
    //   383: astore_1
    //   384: ldc 114
    //   386: ldc_w 368
    //   389: invokestatic 120	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   392: pop
    //   393: new 277	java/io/IOException
    //   396: dup
    //   397: ldc_w 370
    //   400: invokespecial 307	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   403: athrow
    //   404: astore_3
    //   405: aload_0
    //   406: getfield 37	com/google/firebase/iid/zzat:zzcs	Landroid/support/v4/util/SimpleArrayMap;
    //   409: astore_1
    //   410: aload_1
    //   411: monitorenter
    //   412: aload_0
    //   413: getfield 37	com/google/firebase/iid/zzat:zzcs	Landroid/support/v4/util/SimpleArrayMap;
    //   416: aload_2
    //   417: invokevirtual 94	android/support/v4/util/SimpleArrayMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
    //   420: pop
    //   421: aload_1
    //   422: monitorexit
    //   423: aload_3
    //   424: athrow
    //   425: astore_1
    //   426: new 277	java/io/IOException
    //   429: dup
    //   430: aload_1
    //   431: invokespecial 373	java/io/IOException:<init>	(Ljava/lang/Throwable;)V
    //   434: athrow
    //   435: astore_2
    //   436: aload_1
    //   437: monitorexit
    //   438: aload_2
    //   439: athrow
    //   440: astore_1
    //   441: goto -57 -> 384
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	444	0	this	zzat
    //   3	289	2	str	String
    //   378	39	2	localObject1	Object
    //   435	4	2	localObject2	Object
    //   11	288	3	localObject3	Object
    //   404	20	3	localObject4	Object
    //   16	354	4	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   21	34	55	finally
    //   56	59	55	finally
    //   248	263	323	android/os/RemoteException
    //   312	320	323	android/os/RemoteException
    //   287	298	378	finally
    //   379	381	378	finally
    //   263	280	383	java/lang/InterruptedException
    //   263	280	404	finally
    //   384	404	404	finally
    //   426	435	404	finally
    //   263	280	425	java/util/concurrent/ExecutionException
    //   412	423	435	finally
    //   436	438	435	finally
    //   263	280	440	java/util/concurrent/TimeoutException
  }
  
  final Bundle zzc(Bundle paramBundle)
    throws IOException
  {
    Object localObject;
    if (this.zzan.zzaf() >= 12000000) {
      localObject = zzab.zzc(this.zzx).zzb(1, paramBundle);
    }
    try
    {
      localObject = (Bundle)Tasks.await((Task)localObject);
      return (Bundle)localObject;
    }
    catch (InterruptedException localInterruptedException)
    {
      if (Log.isLoggable("FirebaseInstanceId", 3))
      {
        String str = String.valueOf(localInterruptedException);
        Log.d("FirebaseInstanceId", String.valueOf(str).length() + 22 + "Error making request: " + str);
      }
      if (((localInterruptedException.getCause() instanceof zzal)) && (((zzal)localInterruptedException.getCause()).getErrorCode() == 4)) {
        return zzd(paramBundle);
      }
      return null;
      return zzd(paramBundle);
    }
    catch (ExecutionException localExecutionException)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */