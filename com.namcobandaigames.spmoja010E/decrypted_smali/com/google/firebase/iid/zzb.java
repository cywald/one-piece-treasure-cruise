package com.google.firebase.iid;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class zzb
  extends Service
{
  private final Object lock;
  @VisibleForTesting
  final ExecutorService zzi;
  private Binder zzj;
  private int zzk;
  private int zzl;
  
  public zzb()
  {
    String str = String.valueOf(getClass().getSimpleName());
    if (str.length() != 0) {}
    for (str = "Firebase-".concat(str);; str = new String("Firebase-"))
    {
      this.zzi = Executors.newSingleThreadExecutor(new NamedThreadFactory(str));
      this.lock = new Object();
      this.zzl = 0;
      return;
    }
  }
  
  private final void zza(Intent arg1)
  {
    if (??? != null) {
      WakefulBroadcastReceiver.completeWakefulIntent(???);
    }
    synchronized (this.lock)
    {
      this.zzl -= 1;
      if (this.zzl == 0) {
        stopSelfResult(this.zzk);
      }
      return;
    }
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    try
    {
      if (Log.isLoggable("EnhancedIntentService", 3)) {
        Log.d("EnhancedIntentService", "Service received bind request");
      }
      if (this.zzj == null) {
        this.zzj = new zzf(this);
      }
      paramIntent = this.zzj;
      return paramIntent;
    }
    finally {}
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    synchronized (this.lock)
    {
      this.zzk = paramInt2;
      this.zzl += 1;
      ??? = zzb(paramIntent);
      if (??? == null)
      {
        zza(paramIntent);
        return 2;
      }
    }
    if (zzc((Intent)???))
    {
      zza(paramIntent);
      return 2;
    }
    this.zzi.execute(new zzc(this, (Intent)???, paramIntent));
    return 3;
  }
  
  protected Intent zzb(Intent paramIntent)
  {
    return paramIntent;
  }
  
  public boolean zzc(Intent paramIntent)
  {
    return false;
  }
  
  public abstract void zzd(Intent paramIntent);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */