package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.List;
import javax.annotation.concurrent.GuardedBy;

public final class zzan
{
  @GuardedBy("this")
  private String zzci;
  @GuardedBy("this")
  private String zzcj;
  @GuardedBy("this")
  private int zzck;
  @GuardedBy("this")
  private int zzcl = 0;
  private final Context zzx;
  
  public zzan(Context paramContext)
  {
    this.zzx = paramContext;
  }
  
  public static String zza(FirebaseApp paramFirebaseApp)
  {
    String str = paramFirebaseApp.getOptions().getGcmSenderId();
    if (str != null) {
      paramFirebaseApp = str;
    }
    do
    {
      do
      {
        return paramFirebaseApp;
        str = paramFirebaseApp.getOptions().getApplicationId();
        paramFirebaseApp = str;
      } while (!str.startsWith("1:"));
      paramFirebaseApp = str.split(":");
      if (paramFirebaseApp.length < 2) {
        return null;
      }
      str = paramFirebaseApp[1];
      paramFirebaseApp = str;
    } while (!str.isEmpty());
    return null;
  }
  
  public static String zza(KeyPair paramKeyPair)
  {
    paramKeyPair = paramKeyPair.getPublic().getEncoded();
    try
    {
      paramKeyPair = MessageDigest.getInstance("SHA1").digest(paramKeyPair);
      paramKeyPair[0] = ((byte)((paramKeyPair[0] & 0xF) + 112));
      paramKeyPair = Base64.encodeToString(paramKeyPair, 0, 8, 11);
      return paramKeyPair;
    }
    catch (NoSuchAlgorithmException paramKeyPair)
    {
      Log.w("FirebaseInstanceId", "Unexpected error, device missing required algorithms");
    }
    return null;
  }
  
  private final void zzag()
  {
    try
    {
      PackageInfo localPackageInfo = zze(this.zzx.getPackageName());
      if (localPackageInfo != null)
      {
        this.zzci = Integer.toString(localPackageInfo.versionCode);
        this.zzcj = localPackageInfo.versionName;
      }
      return;
    }
    finally {}
  }
  
  private final PackageInfo zze(String paramString)
  {
    try
    {
      paramString = this.zzx.getPackageManager().getPackageInfo(paramString, 0);
      return paramString;
    }
    catch (PackageManager.NameNotFoundException paramString)
    {
      paramString = String.valueOf(paramString);
      Log.w("FirebaseInstanceId", String.valueOf(paramString).length() + 23 + "Failed to find package " + paramString);
    }
    return null;
  }
  
  public final int zzac()
  {
    int i = 0;
    for (;;)
    {
      try
      {
        if (this.zzcl != 0)
        {
          i = this.zzcl;
          return i;
        }
        PackageManager localPackageManager = this.zzx.getPackageManager();
        if (localPackageManager.checkPermission("com.google.android.c2dm.permission.SEND", "com.google.android.gms") == -1)
        {
          Log.e("FirebaseInstanceId", "Google Play services missing or without correct permission.");
          continue;
        }
        if (PlatformVersion.isAtLeastO()) {
          break label112;
        }
      }
      finally {}
      Object localObject2 = new Intent("com.google.android.c2dm.intent.REGISTER");
      ((Intent)localObject2).setPackage("com.google.android.gms");
      localObject2 = ((PackageManager)localObject1).queryIntentServices((Intent)localObject2, 0);
      if ((localObject2 != null) && (((List)localObject2).size() > 0))
      {
        this.zzcl = 1;
        i = this.zzcl;
      }
      else
      {
        label112:
        localObject2 = new Intent("com.google.iid.TOKEN_REQUEST");
        ((Intent)localObject2).setPackage("com.google.android.gms");
        List localList = ((PackageManager)localObject1).queryBroadcastReceivers((Intent)localObject2, 0);
        if ((localList == null) || (localList.size() <= 0)) {
          break;
        }
        this.zzcl = 2;
        i = this.zzcl;
      }
    }
    Log.w("FirebaseInstanceId", "Failed to resolve IID implementation package, falling back");
    if (PlatformVersion.isAtLeastO()) {}
    for (this.zzcl = 2;; this.zzcl = 1)
    {
      i = this.zzcl;
      break;
    }
  }
  
  public final String zzad()
  {
    try
    {
      if (this.zzci == null) {
        zzag();
      }
      String str = this.zzci;
      return str;
    }
    finally {}
  }
  
  public final String zzae()
  {
    try
    {
      if (this.zzcj == null) {
        zzag();
      }
      String str = this.zzcj;
      return str;
    }
    finally {}
  }
  
  public final int zzaf()
  {
    try
    {
      if (this.zzck == 0)
      {
        PackageInfo localPackageInfo = zze("com.google.android.gms");
        if (localPackageInfo != null) {
          this.zzck = localPackageInfo.versionCode;
        }
      }
      int i = this.zzck;
      return i;
    }
    finally {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */