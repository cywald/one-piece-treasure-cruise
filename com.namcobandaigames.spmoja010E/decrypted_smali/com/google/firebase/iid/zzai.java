package com.google.firebase.iid;

import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

final class zzai
{
  private final Messenger zzag;
  private final zzl zzce;
  
  zzai(IBinder paramIBinder)
    throws RemoteException
  {
    String str = paramIBinder.getInterfaceDescriptor();
    if ("android.os.IMessenger".equals(str))
    {
      this.zzag = new Messenger(paramIBinder);
      this.zzce = null;
      return;
    }
    if ("com.google.android.gms.iid.IMessengerCompat".equals(str))
    {
      this.zzce = new zzl(paramIBinder);
      this.zzag = null;
      return;
    }
    paramIBinder = String.valueOf(str);
    if (paramIBinder.length() != 0) {}
    for (paramIBinder = "Invalid interface descriptor: ".concat(paramIBinder);; paramIBinder = new String("Invalid interface descriptor: "))
    {
      Log.w("MessengerIpcClient", paramIBinder);
      throw new RemoteException();
    }
  }
  
  final void send(Message paramMessage)
    throws RemoteException
  {
    if (this.zzag != null)
    {
      this.zzag.send(paramMessage);
      return;
    }
    if (this.zzce != null)
    {
      this.zzce.send(paramMessage);
      return;
    }
    throw new IllegalStateException("Both messengers are null");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */