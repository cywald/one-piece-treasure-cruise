package com.google.firebase.iid;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

final class zzax
{
  private static final long zzdf = TimeUnit.DAYS.toMillis(7L);
  private final long timestamp;
  final String zzbq;
  private final String zzdg;
  
  private zzax(String paramString1, String paramString2, long paramLong)
  {
    this.zzbq = paramString1;
    this.zzdg = paramString2;
    this.timestamp = paramLong;
  }
  
  static String zza(@Nullable zzax paramzzax)
  {
    if (paramzzax == null) {
      return null;
    }
    return paramzzax.zzbq;
  }
  
  static String zza(String paramString1, String paramString2, long paramLong)
  {
    try
    {
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("token", paramString1);
      localJSONObject.put("appVersion", paramString2);
      localJSONObject.put("timestamp", paramLong);
      paramString1 = localJSONObject.toString();
      return paramString1;
    }
    catch (JSONException paramString1)
    {
      paramString1 = String.valueOf(paramString1);
      Log.w("FirebaseInstanceId", String.valueOf(paramString1).length() + 24 + "Failed to encode token: " + paramString1);
    }
    return null;
  }
  
  static zzax zzi(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    if (paramString.startsWith("{")) {
      try
      {
        paramString = new JSONObject(paramString);
        paramString = new zzax(paramString.getString("token"), paramString.getString("appVersion"), paramString.getLong("timestamp"));
        return paramString;
      }
      catch (JSONException paramString)
      {
        paramString = String.valueOf(paramString);
        Log.w("FirebaseInstanceId", String.valueOf(paramString).length() + 23 + "Failed to parse token: " + paramString);
        return null;
      }
    }
    return new zzax(paramString, null, 0L);
  }
  
  final boolean zzj(String paramString)
  {
    return (System.currentTimeMillis() > this.timestamp + zzdf) || (!paramString.equals(this.zzdg));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzax.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */