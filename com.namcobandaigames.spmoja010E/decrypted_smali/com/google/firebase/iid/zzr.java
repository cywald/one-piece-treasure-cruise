package com.google.firebase.iid;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import java.io.IOException;
import java.util.concurrent.Executor;

final class zzr
  implements MessagingChannel
{
  private final FirebaseApp zzam;
  private final zzan zzan;
  private final zzat zzbi;
  private final Executor zzbj;
  
  zzr(FirebaseApp paramFirebaseApp, zzan paramzzan, Executor paramExecutor)
  {
    this(paramFirebaseApp, paramzzan, paramExecutor, new zzat(paramFirebaseApp.getApplicationContext(), paramzzan));
  }
  
  @VisibleForTesting
  private zzr(FirebaseApp paramFirebaseApp, zzan paramzzan, Executor paramExecutor, zzat paramzzat)
  {
    this.zzam = paramFirebaseApp;
    this.zzan = paramzzan;
    this.zzbi = paramzzat;
    this.zzbj = paramExecutor;
  }
  
  private final Task<Bundle> zza(String paramString1, String paramString2, String paramString3, Bundle paramBundle)
  {
    paramBundle.putString("scope", paramString3);
    paramBundle.putString("sender", paramString2);
    paramBundle.putString("subtype", paramString2);
    paramBundle.putString("appid", paramString1);
    paramBundle.putString("gmp_app_id", this.zzam.getOptions().getApplicationId());
    paramBundle.putString("gmsv", Integer.toString(this.zzan.zzaf()));
    paramBundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
    paramBundle.putString("app_ver", this.zzan.zzad());
    paramBundle.putString("app_ver_name", this.zzan.zzae());
    paramBundle.putString("cliv", "fiid-12451000");
    paramString1 = new TaskCompletionSource();
    this.zzbj.execute(new zzs(this, paramBundle, paramString1));
    return paramString1.getTask();
  }
  
  private static String zza(Bundle paramBundle)
    throws IOException
  {
    if (paramBundle == null) {
      throw new IOException("SERVICE_NOT_AVAILABLE");
    }
    Object localObject = paramBundle.getString("registration_id");
    if (localObject != null) {}
    String str;
    do
    {
      return (String)localObject;
      str = paramBundle.getString("unregistered");
      localObject = str;
    } while (str != null);
    localObject = paramBundle.getString("error");
    if ("RST".equals(localObject)) {
      throw new IOException("INSTANCE_ID_RESET");
    }
    if (localObject != null) {
      throw new IOException((String)localObject);
    }
    paramBundle = String.valueOf(paramBundle);
    Log.w("FirebaseInstanceId", String.valueOf(paramBundle).length() + 21 + "Unexpected response: " + paramBundle, new Throwable());
    throw new IOException("SERVICE_NOT_AVAILABLE");
  }
  
  private final <T> Task<Void> zzb(Task<T> paramTask)
  {
    return paramTask.continueWith(zzi.zze(), new zzt(this));
  }
  
  private final Task<String> zzc(Task<Bundle> paramTask)
  {
    return paramTask.continueWith(this.zzbj, new zzu(this));
  }
  
  public final Task<Void> ackMessage(String paramString)
  {
    return null;
  }
  
  public final Task<Void> buildChannel(String paramString1, String paramString2)
  {
    return Tasks.forResult(null);
  }
  
  public final Task<Void> deleteInstanceId(String paramString)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("iid-operation", "delete");
    localBundle.putString("delete", "1");
    return zzb(zzc(zza(paramString, "*", "*", localBundle)));
  }
  
  public final Task<Void> deleteToken(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    paramString2 = new Bundle();
    paramString2.putString("delete", "1");
    return zzb(zzc(zza(paramString1, paramString3, paramString4, paramString2)));
  }
  
  public final Task<String> getToken(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    return zzc(zza(paramString1, paramString3, paramString4, new Bundle()));
  }
  
  public final boolean isAvailable()
  {
    return this.zzan.zzac() != 0;
  }
  
  public final boolean isChannelBuilt()
  {
    return true;
  }
  
  public final Task<Void> subscribeToTopic(String paramString1, String paramString2, String paramString3)
  {
    Bundle localBundle = new Bundle();
    String str1 = String.valueOf("/topics/");
    String str2 = String.valueOf(paramString3);
    if (str2.length() != 0)
    {
      str1 = str1.concat(str2);
      localBundle.putString("gcm.topic", str1);
      str1 = String.valueOf("/topics/");
      paramString3 = String.valueOf(paramString3);
      if (paramString3.length() == 0) {
        break label109;
      }
    }
    label109:
    for (paramString3 = str1.concat(paramString3);; paramString3 = new String(str1))
    {
      return zzb(zzc(zza(paramString1, paramString2, paramString3, localBundle)));
      str1 = new String(str1);
      break;
    }
  }
  
  public final Task<Void> unsubscribeFromTopic(String paramString1, String paramString2, String paramString3)
  {
    Bundle localBundle = new Bundle();
    String str1 = String.valueOf("/topics/");
    String str2 = String.valueOf(paramString3);
    if (str2.length() != 0)
    {
      str1 = str1.concat(str2);
      localBundle.putString("gcm.topic", str1);
      localBundle.putString("delete", "1");
      str1 = String.valueOf("/topics/");
      paramString3 = String.valueOf(paramString3);
      if (paramString3.length() == 0) {
        break label118;
      }
    }
    label118:
    for (paramString3 = str1.concat(paramString3);; paramString3 = new String(str1))
    {
      return zzb(zzc(zza(paramString1, paramString2, paramString3, localBundle)));
      str1 = new String(str1);
      break;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */