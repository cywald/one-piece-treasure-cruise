package com.google.firebase.iid;

import android.os.Binder;
import android.os.Process;
import android.util.Log;
import java.util.concurrent.ExecutorService;

public final class zzf
  extends Binder
{
  private final zzb zzu;
  
  zzf(zzb paramzzb)
  {
    this.zzu = paramzzb;
  }
  
  public final void zza(zzd paramzzd)
  {
    if (Binder.getCallingUid() != Process.myUid()) {
      throw new SecurityException("Binding only allowed within app");
    }
    if (Log.isLoggable("EnhancedIntentService", 3)) {
      Log.d("EnhancedIntentService", "service received new intent via bind strategy");
    }
    if (this.zzu.zzc(paramzzd.intent))
    {
      paramzzd.finish();
      return;
    }
    if (Log.isLoggable("EnhancedIntentService", 3)) {
      Log.d("EnhancedIntentService", "intent being queued for bg execution");
    }
    this.zzu.zzi.execute(new zzg(this, paramzzd));
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */