package com.google.firebase.iid;

import android.os.Looper;
import android.os.Message;
import com.google.android.gms.internal.firebase_messaging.zza;

final class zzau
  extends zza
{
  zzau(zzat paramzzat, Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    zzat.zza(this.zzcw, paramMessage);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\iid\zzau.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */