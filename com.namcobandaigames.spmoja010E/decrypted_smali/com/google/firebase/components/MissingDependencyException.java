package com.google.firebase.components;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class MissingDependencyException
  extends DependencyException
{
  @KeepForSdk
  public MissingDependencyException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\MissingDependencyException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */