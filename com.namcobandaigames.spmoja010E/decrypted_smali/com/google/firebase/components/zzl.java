package com.google.firebase.components;

import com.google.firebase.events.Event;
import com.google.firebase.events.Publisher;
import com.google.firebase.inject.Provider;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

final class zzl
  extends zza
{
  private final Set<Class<?>> zza;
  private final Set<Class<?>> zzb;
  private final Set<Class<?>> zzc;
  private final ComponentContainer zzd;
  
  zzl(Component<?> paramComponent, ComponentContainer paramComponentContainer)
  {
    HashSet localHashSet1 = new HashSet();
    HashSet localHashSet2 = new HashSet();
    Iterator localIterator = paramComponent.zzb().iterator();
    while (localIterator.hasNext())
    {
      Dependency localDependency = (Dependency)localIterator.next();
      if (localDependency.zzc()) {
        localHashSet1.add(localDependency.zza());
      } else {
        localHashSet2.add(localDependency.zza());
      }
    }
    if (!paramComponent.zzd().isEmpty()) {
      localHashSet1.add(Publisher.class);
    }
    this.zza = Collections.unmodifiableSet(localHashSet1);
    this.zzb = Collections.unmodifiableSet(localHashSet2);
    this.zzc = paramComponent.zzd();
    this.zzd = paramComponentContainer;
  }
  
  public final <T> T get(Class<T> paramClass)
  {
    if (!this.zza.contains(paramClass)) {
      throw new IllegalArgumentException(String.format("Requesting %s is not allowed.", new Object[] { paramClass }));
    }
    Object localObject = this.zzd.get(paramClass);
    if (!paramClass.equals(Publisher.class)) {
      return (T)localObject;
    }
    return new zza(this.zzc, (Publisher)localObject);
  }
  
  public final <T> Provider<T> getProvider(Class<T> paramClass)
  {
    if (!this.zzb.contains(paramClass)) {
      throw new IllegalArgumentException(String.format("Requesting Provider<%s> is not allowed.", new Object[] { paramClass }));
    }
    return this.zzd.getProvider(paramClass);
  }
  
  static final class zza
    implements Publisher
  {
    private final Set<Class<?>> zza;
    private final Publisher zzb;
    
    public zza(Set<Class<?>> paramSet, Publisher paramPublisher)
    {
      this.zza = paramSet;
      this.zzb = paramPublisher;
    }
    
    public final void publish(Event<?> paramEvent)
    {
      if (!this.zza.contains(paramEvent.getType())) {
        throw new IllegalArgumentException(String.format("Attempting to publish an undeclared event %s.", new Object[] { paramEvent }));
      }
      this.zzb.publish(paramEvent);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */