package com.google.firebase.components;

import com.google.android.gms.common.annotation.KeepForSdk;
import java.util.Arrays;
import java.util.List;

@KeepForSdk
public class DependencyCycleException
  extends DependencyException
{
  private final List<Component<?>> zza;
  
  @KeepForSdk
  public DependencyCycleException(List<Component<?>> paramList)
  {
    super("Dependency cycle detected: " + Arrays.toString(paramList.toArray()));
    this.zza = paramList;
  }
  
  @KeepForSdk
  public List<Component<?>> getComponentsInCycle()
  {
    return this.zza;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\DependencyCycleException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */