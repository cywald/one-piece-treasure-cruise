package com.google.firebase.components;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.firebase.inject.Provider;

@KeepForSdk
public abstract interface ComponentContainer
{
  @KeepForSdk
  public abstract <T> T get(Class<T> paramClass);
  
  @KeepForSdk
  public abstract <T> Provider<T> getProvider(Class<T> paramClass);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\ComponentContainer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */