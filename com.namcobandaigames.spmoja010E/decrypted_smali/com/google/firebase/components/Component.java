package com.google.firebase.components;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@KeepForSdk
public final class Component<T>
{
  private final Set<Class<? super T>> zza;
  private final Set<Dependency> zzb;
  private final int zzc;
  private final ComponentFactory<T> zzd;
  private final Set<Class<?>> zze;
  
  private Component(Set<Class<? super T>> paramSet, Set<Dependency> paramSet1, int paramInt, ComponentFactory<T> paramComponentFactory, Set<Class<?>> paramSet2)
  {
    this.zza = Collections.unmodifiableSet(paramSet);
    this.zzb = Collections.unmodifiableSet(paramSet1);
    this.zzc = paramInt;
    this.zzd = paramComponentFactory;
    this.zze = Collections.unmodifiableSet(paramSet2);
  }
  
  @KeepForSdk
  public static <T> Builder<T> builder(Class<T> paramClass)
  {
    return new Builder(paramClass, new Class[0], (byte)0);
  }
  
  @KeepForSdk
  public static <T> Builder<T> builder(Class<T> paramClass, Class<? super T>... paramVarArgs)
  {
    return new Builder(paramClass, paramVarArgs, (byte)0);
  }
  
  @Deprecated
  @KeepForSdk
  public static <T> Component<T> of(Class<T> paramClass, T paramT)
  {
    return builder(paramClass).factory(zzb.zza(paramT)).build();
  }
  
  @SafeVarargs
  @KeepForSdk
  public static <T> Component<T> of(T paramT, Class<T> paramClass, Class<? super T>... paramVarArgs)
  {
    return builder(paramClass, paramVarArgs).factory(zzc.zza(paramT)).build();
  }
  
  public final String toString()
  {
    return "Component<" + Arrays.toString(this.zza.toArray()) + ">{" + this.zzc + ", deps=" + Arrays.toString(this.zzb.toArray()) + "}";
  }
  
  public final Set<Class<? super T>> zza()
  {
    return this.zza;
  }
  
  public final Set<Dependency> zzb()
  {
    return this.zzb;
  }
  
  public final ComponentFactory<T> zzc()
  {
    return this.zzd;
  }
  
  public final Set<Class<?>> zzd()
  {
    return this.zze;
  }
  
  public final boolean zze()
  {
    return this.zzc == 1;
  }
  
  public final boolean zzf()
  {
    return this.zzc == 2;
  }
  
  @KeepForSdk
  public static class Builder<T>
  {
    private final Set<Class<? super T>> zza = new HashSet();
    private final Set<Dependency> zzb = new HashSet();
    private int zzc = 0;
    private ComponentFactory<T> zzd;
    private Set<Class<?>> zze = new HashSet();
    
    private Builder(Class<T> paramClass, Class<? super T>... paramVarArgs)
    {
      Preconditions.checkNotNull(paramClass, "Null interface");
      this.zza.add(paramClass);
      int j = paramVarArgs.length;
      while (i < j)
      {
        Preconditions.checkNotNull(paramVarArgs[i], "Null interface");
        i += 1;
      }
      Collections.addAll(this.zza, paramVarArgs);
    }
    
    private Builder<T> zza(int paramInt)
    {
      if (this.zzc == 0) {}
      for (boolean bool = true;; bool = false)
      {
        Preconditions.checkState(bool, "Instantiation type has already been set.");
        this.zzc = paramInt;
        return this;
      }
    }
    
    @KeepForSdk
    public Builder<T> add(Dependency paramDependency)
    {
      Preconditions.checkNotNull(paramDependency, "Null dependency");
      Class localClass = paramDependency.zza();
      if (!this.zza.contains(localClass)) {}
      for (boolean bool = true;; bool = false)
      {
        Preconditions.checkArgument(bool, "Components are not allowed to depend on interfaces they themselves provide.");
        this.zzb.add(paramDependency);
        return this;
      }
    }
    
    @KeepForSdk
    public Builder<T> alwaysEager()
    {
      return zza(1);
    }
    
    @KeepForSdk
    public Component<T> build()
    {
      if (this.zzd != null) {}
      for (boolean bool = true;; bool = false)
      {
        Preconditions.checkState(bool, "Missing required property: factory.");
        return new Component(new HashSet(this.zza), new HashSet(this.zzb), this.zzc, this.zzd, this.zze, (byte)0);
      }
    }
    
    @KeepForSdk
    public Builder<T> eagerInDefaultApp()
    {
      return zza(2);
    }
    
    @KeepForSdk
    public Builder<T> factory(ComponentFactory<T> paramComponentFactory)
    {
      this.zzd = ((ComponentFactory)Preconditions.checkNotNull(paramComponentFactory, "Null factory"));
      return this;
    }
    
    @KeepForSdk
    public Builder<T> publishes(Class<?> paramClass)
    {
      this.zze.add(paramClass);
      return this;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\Component.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */