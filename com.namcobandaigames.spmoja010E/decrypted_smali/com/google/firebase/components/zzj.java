package com.google.firebase.components;

import com.google.firebase.inject.Provider;

final class zzj<T>
  implements Provider<T>
{
  private static final Object zza = new Object();
  private volatile Object zzb = zza;
  private volatile Provider<T> zzc;
  
  zzj(ComponentFactory<T> paramComponentFactory, ComponentContainer paramComponentContainer)
  {
    this.zzc = zzk.zza(paramComponentFactory, paramComponentContainer);
  }
  
  public final T get()
  {
    Object localObject1 = this.zzb;
    if (localObject1 == zza) {
      try
      {
        Object localObject2 = this.zzb;
        localObject1 = localObject2;
        if (localObject2 == zza)
        {
          localObject1 = this.zzc.get();
          this.zzb = localObject1;
          this.zzc = null;
        }
        return (T)localObject1;
      }
      finally {}
    }
    return ?;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */