package com.google.firebase.components;

import android.support.annotation.GuardedBy;
import com.google.android.gms.common.internal.Preconditions;
import com.google.firebase.events.Event;
import com.google.firebase.events.EventHandler;
import com.google.firebase.events.Publisher;
import com.google.firebase.events.Subscriber;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

class zzh
  implements Publisher, Subscriber
{
  @GuardedBy("this")
  private final Map<Class<?>, ConcurrentHashMap<EventHandler<Object>, Executor>> zza = new HashMap();
  @GuardedBy("this")
  private Queue<Event<?>> zzb = new ArrayDeque();
  private final Executor zzc;
  
  zzh(Executor paramExecutor)
  {
    this.zzc = paramExecutor;
  }
  
  /* Error */
  private Set<Map.Entry<EventHandler<Object>, Executor>> zza(Event<?> paramEvent)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 29	com/google/firebase/components/zzh:zza	Ljava/util/Map;
    //   6: aload_1
    //   7: invokevirtual 44	com/google/firebase/events/Event:getType	()Ljava/lang/Class;
    //   10: invokeinterface 50 2 0
    //   15: checkcast 46	java/util/Map
    //   18: astore_1
    //   19: aload_1
    //   20: ifnonnull +11 -> 31
    //   23: invokestatic 56	java/util/Collections:emptySet	()Ljava/util/Set;
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: areturn
    //   31: aload_1
    //   32: invokeinterface 59 1 0
    //   37: astore_1
    //   38: goto -11 -> 27
    //   41: astore_1
    //   42: aload_0
    //   43: monitorexit
    //   44: aload_1
    //   45: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	46	0	this	zzh
    //   0	46	1	paramEvent	Event<?>
    // Exception table:
    //   from	to	target	type
    //   2	19	41	finally
    //   23	27	41	finally
    //   31	38	41	finally
  }
  
  public void publish(Event<?> paramEvent)
  {
    Preconditions.checkNotNull(paramEvent);
    try
    {
      if (this.zzb != null)
      {
        this.zzb.add(paramEvent);
        return;
      }
      Iterator localIterator = zza(paramEvent).iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        ((Executor)localEntry.getValue()).execute(zzi.zza(localEntry, paramEvent));
      }
      return;
    }
    finally {}
  }
  
  public <T> void subscribe(Class<T> paramClass, EventHandler<? super T> paramEventHandler)
  {
    subscribe(paramClass, this.zzc, paramEventHandler);
  }
  
  public <T> void subscribe(Class<T> paramClass, Executor paramExecutor, EventHandler<? super T> paramEventHandler)
  {
    try
    {
      Preconditions.checkNotNull(paramClass);
      Preconditions.checkNotNull(paramEventHandler);
      Preconditions.checkNotNull(paramExecutor);
      if (!this.zza.containsKey(paramClass)) {
        this.zza.put(paramClass, new ConcurrentHashMap());
      }
      ((ConcurrentHashMap)this.zza.get(paramClass)).put(paramEventHandler, paramExecutor);
      return;
    }
    finally {}
  }
  
  /* Error */
  public <T> void unsubscribe(Class<T> paramClass, EventHandler<? super T> paramEventHandler)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: invokestatic 80	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   6: pop
    //   7: aload_2
    //   8: invokestatic 80	com/google/android/gms/common/internal/Preconditions:checkNotNull	(Ljava/lang/Object;)Ljava/lang/Object;
    //   11: pop
    //   12: aload_0
    //   13: getfield 29	com/google/firebase/components/zzh:zza	Ljava/util/Map;
    //   16: aload_1
    //   17: invokeinterface 127 2 0
    //   22: istore_3
    //   23: iload_3
    //   24: ifne +6 -> 30
    //   27: aload_0
    //   28: monitorexit
    //   29: return
    //   30: aload_0
    //   31: getfield 29	com/google/firebase/components/zzh:zza	Ljava/util/Map;
    //   34: aload_1
    //   35: invokeinterface 50 2 0
    //   40: checkcast 129	java/util/concurrent/ConcurrentHashMap
    //   43: astore 4
    //   45: aload 4
    //   47: aload_2
    //   48: invokevirtual 140	java/util/concurrent/ConcurrentHashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
    //   51: pop
    //   52: aload 4
    //   54: invokevirtual 143	java/util/concurrent/ConcurrentHashMap:isEmpty	()Z
    //   57: ifeq -30 -> 27
    //   60: aload_0
    //   61: getfield 29	com/google/firebase/components/zzh:zza	Ljava/util/Map;
    //   64: aload_1
    //   65: invokeinterface 144 2 0
    //   70: pop
    //   71: goto -44 -> 27
    //   74: astore_1
    //   75: aload_0
    //   76: monitorexit
    //   77: aload_1
    //   78: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	79	0	this	zzh
    //   0	79	1	paramClass	Class<T>
    //   0	79	2	paramEventHandler	EventHandler<? super T>
    //   22	2	3	bool	boolean
    //   43	10	4	localConcurrentHashMap	ConcurrentHashMap
    // Exception table:
    //   from	to	target	type
    //   2	23	74	finally
    //   30	71	74	finally
  }
  
  final void zza()
  {
    Object localObject1 = null;
    try
    {
      if (this.zzb != null)
      {
        localObject1 = this.zzb;
        this.zzb = null;
      }
      if (localObject1 != null)
      {
        localObject1 = ((Queue)localObject1).iterator();
        while (((Iterator)localObject1).hasNext()) {
          publish((Event)((Iterator)localObject1).next());
        }
      }
      return;
    }
    finally {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */