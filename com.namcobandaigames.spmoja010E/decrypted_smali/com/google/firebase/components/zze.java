package com.google.firebase.components;

import android.support.annotation.VisibleForTesting;
import java.util.List;

@VisibleForTesting
abstract interface zze<T>
{
  public abstract List<String> zza(T paramT);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */