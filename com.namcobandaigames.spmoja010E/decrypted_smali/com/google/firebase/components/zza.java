package com.google.firebase.components;

import com.google.firebase.inject.Provider;

abstract class zza
  implements ComponentContainer
{
  public <T> T get(Class<T> paramClass)
  {
    paramClass = getProvider(paramClass);
    if (paramClass == null) {
      return null;
    }
    return (T)paramClass.get();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */