package com.google.firebase.components;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;

@KeepForSdk
public final class Dependency
{
  private final Class<?> zza;
  private final int zzb;
  private final int zzc;
  
  private Dependency(Class<?> paramClass, int paramInt1, int paramInt2)
  {
    this.zza = ((Class)Preconditions.checkNotNull(paramClass, "Null dependency anInterface."));
    this.zzb = paramInt1;
    this.zzc = paramInt2;
  }
  
  @KeepForSdk
  public static Dependency optional(Class<?> paramClass)
  {
    return new Dependency(paramClass, 0, 0);
  }
  
  @KeepForSdk
  public static Dependency optionalProvider(Class<?> paramClass)
  {
    return new Dependency(paramClass, 0, 1);
  }
  
  @KeepForSdk
  public static Dependency required(Class<?> paramClass)
  {
    return new Dependency(paramClass, 1, 0);
  }
  
  @KeepForSdk
  public static Dependency requiredProvider(Class<?> paramClass)
  {
    return new Dependency(paramClass, 1, 1);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if ((paramObject instanceof Dependency))
    {
      paramObject = (Dependency)paramObject;
      bool1 = bool2;
      if (this.zza == ((Dependency)paramObject).zza)
      {
        bool1 = bool2;
        if (this.zzb == ((Dependency)paramObject).zzb)
        {
          bool1 = bool2;
          if (this.zzc == ((Dependency)paramObject).zzc) {
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    return ((this.zza.hashCode() ^ 0xF4243) * 1000003 ^ this.zzb) * 1000003 ^ this.zzc;
  }
  
  public final String toString()
  {
    boolean bool2 = true;
    StringBuilder localStringBuilder = new StringBuilder("Dependency{anInterface=").append(this.zza).append(", required=");
    if (this.zzb == 1)
    {
      bool1 = true;
      localStringBuilder = localStringBuilder.append(bool1).append(", direct=");
      if (this.zzc != 0) {
        break label73;
      }
    }
    label73:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1 + "}";
      bool1 = false;
      break;
    }
  }
  
  public final Class<?> zza()
  {
    return this.zza;
  }
  
  public final boolean zzb()
  {
    return this.zzb == 1;
  }
  
  public final boolean zzc()
  {
    return this.zzc == 0;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\components\Dependency.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */