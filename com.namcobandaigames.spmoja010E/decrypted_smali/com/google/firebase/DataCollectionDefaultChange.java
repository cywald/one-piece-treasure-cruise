package com.google.firebase;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class DataCollectionDefaultChange
{
  @KeepForSdk
  public final boolean enabled;
  
  @KeepForSdk
  public DataCollectionDefaultChange(boolean paramBoolean)
  {
    this.enabled = paramBoolean;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\google\firebase\DataCollectionDefaultChange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */