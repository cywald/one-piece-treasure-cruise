package com.adjust.sdk;

import android.content.Context;
import java.io.IOException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class AdjustFactory
{
  private static IActivityHandler activityHandler;
  private static IAttributionHandler attributionHandler;
  private static HttpsURLConnection httpsURLConnection;
  private static ILogger logger;
  private static long maxDelayStart = -1L;
  private static IPackageHandler packageHandler = null;
  private static BackoffStrategy packageHandlerBackoffStrategy;
  private static IRequestHandler requestHandler = null;
  private static BackoffStrategy sdkClickBackoffStrategy;
  private static ISdkClickHandler sdkClickHandler;
  private static long sessionInterval;
  private static long subsessionInterval;
  private static long timerInterval;
  private static long timerStart;
  
  static
  {
    attributionHandler = null;
    activityHandler = null;
    logger = null;
    httpsURLConnection = null;
    sdkClickHandler = null;
    timerInterval = -1L;
    timerStart = -1L;
    sessionInterval = -1L;
    subsessionInterval = -1L;
    sdkClickBackoffStrategy = null;
    packageHandlerBackoffStrategy = null;
  }
  
  public static IActivityHandler getActivityHandler(AdjustConfig paramAdjustConfig)
  {
    if (activityHandler == null) {
      return ActivityHandler.getInstance(paramAdjustConfig);
    }
    activityHandler.init(paramAdjustConfig);
    return activityHandler;
  }
  
  public static IAttributionHandler getAttributionHandler(IActivityHandler paramIActivityHandler, ActivityPackage paramActivityPackage, boolean paramBoolean)
  {
    if (attributionHandler == null) {
      return new AttributionHandler(paramIActivityHandler, paramActivityPackage, paramBoolean);
    }
    attributionHandler.init(paramIActivityHandler, paramActivityPackage, paramBoolean);
    return attributionHandler;
  }
  
  public static HttpsURLConnection getHttpsURLConnection(URL paramURL)
    throws IOException
  {
    if (httpsURLConnection == null) {
      return (HttpsURLConnection)paramURL.openConnection();
    }
    return httpsURLConnection;
  }
  
  public static ILogger getLogger()
  {
    if (logger == null) {
      logger = new Logger();
    }
    return logger;
  }
  
  public static long getMaxDelayStart()
  {
    if (maxDelayStart == -1L) {
      return 10000L;
    }
    return maxDelayStart;
  }
  
  public static IPackageHandler getPackageHandler(ActivityHandler paramActivityHandler, Context paramContext, boolean paramBoolean)
  {
    if (packageHandler == null) {
      return new PackageHandler(paramActivityHandler, paramContext, paramBoolean);
    }
    packageHandler.init(paramActivityHandler, paramContext, paramBoolean);
    return packageHandler;
  }
  
  public static BackoffStrategy getPackageHandlerBackoffStrategy()
  {
    if (packageHandlerBackoffStrategy == null) {
      return BackoffStrategy.LONG_WAIT;
    }
    return packageHandlerBackoffStrategy;
  }
  
  public static IRequestHandler getRequestHandler(IPackageHandler paramIPackageHandler)
  {
    if (requestHandler == null) {
      return new RequestHandler(paramIPackageHandler);
    }
    requestHandler.init(paramIPackageHandler);
    return requestHandler;
  }
  
  public static BackoffStrategy getSdkClickBackoffStrategy()
  {
    if (sdkClickBackoffStrategy == null) {
      return BackoffStrategy.SHORT_WAIT;
    }
    return sdkClickBackoffStrategy;
  }
  
  public static ISdkClickHandler getSdkClickHandler(IActivityHandler paramIActivityHandler, boolean paramBoolean)
  {
    if (sdkClickHandler == null) {
      return new SdkClickHandler(paramIActivityHandler, paramBoolean);
    }
    sdkClickHandler.init(paramIActivityHandler, paramBoolean);
    return sdkClickHandler;
  }
  
  public static long getSessionInterval()
  {
    if (sessionInterval == -1L) {
      return 1800000L;
    }
    return sessionInterval;
  }
  
  public static long getSubsessionInterval()
  {
    if (subsessionInterval == -1L) {
      return 1000L;
    }
    return subsessionInterval;
  }
  
  public static long getTimerInterval()
  {
    if (timerInterval == -1L) {
      return 60000L;
    }
    return timerInterval;
  }
  
  public static long getTimerStart()
  {
    if (timerStart == -1L) {
      return 60000L;
    }
    return timerStart;
  }
  
  public static void setActivityHandler(IActivityHandler paramIActivityHandler)
  {
    activityHandler = paramIActivityHandler;
  }
  
  public static void setAttributionHandler(IAttributionHandler paramIAttributionHandler)
  {
    attributionHandler = paramIAttributionHandler;
  }
  
  public static void setHttpsURLConnection(HttpsURLConnection paramHttpsURLConnection)
  {
    httpsURLConnection = paramHttpsURLConnection;
  }
  
  public static void setLogger(ILogger paramILogger)
  {
    logger = paramILogger;
  }
  
  public static void setPackageHandler(IPackageHandler paramIPackageHandler)
  {
    packageHandler = paramIPackageHandler;
  }
  
  public static void setPackageHandlerBackoffStrategy(BackoffStrategy paramBackoffStrategy)
  {
    packageHandlerBackoffStrategy = paramBackoffStrategy;
  }
  
  public static void setRequestHandler(IRequestHandler paramIRequestHandler)
  {
    requestHandler = paramIRequestHandler;
  }
  
  public static void setSdkClickBackoffStrategy(BackoffStrategy paramBackoffStrategy)
  {
    sdkClickBackoffStrategy = paramBackoffStrategy;
  }
  
  public static void setSdkClickHandler(ISdkClickHandler paramISdkClickHandler)
  {
    sdkClickHandler = paramISdkClickHandler;
  }
  
  public static void setSessionInterval(long paramLong)
  {
    sessionInterval = paramLong;
  }
  
  public static void setSubsessionInterval(long paramLong)
  {
    subsessionInterval = paramLong;
  }
  
  public static void setTimerInterval(long paramLong)
  {
    timerInterval = paramLong;
  }
  
  public static void setTimerStart(long paramLong)
  {
    timerStart = paramLong;
  }
  
  public static class URLGetConnection
  {
    HttpsURLConnection httpsURLConnection;
    URL url;
    
    URLGetConnection(HttpsURLConnection paramHttpsURLConnection, URL paramURL)
    {
      this.httpsURLConnection = paramHttpsURLConnection;
      this.url = paramURL;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\AdjustFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */