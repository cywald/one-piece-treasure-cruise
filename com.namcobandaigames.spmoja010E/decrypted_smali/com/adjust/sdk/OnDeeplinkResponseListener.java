package com.adjust.sdk;

import android.net.Uri;

public abstract interface OnDeeplinkResponseListener
{
  public abstract boolean launchReceivedDeeplink(Uri paramUri);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\OnDeeplinkResponseListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */