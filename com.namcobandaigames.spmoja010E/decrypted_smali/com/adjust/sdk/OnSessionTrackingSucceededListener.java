package com.adjust.sdk;

public abstract interface OnSessionTrackingSucceededListener
{
  public abstract void onFinishedSessionTrackingSucceeded(AdjustSessionSuccess paramAdjustSessionSuccess);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\OnSessionTrackingSucceededListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */