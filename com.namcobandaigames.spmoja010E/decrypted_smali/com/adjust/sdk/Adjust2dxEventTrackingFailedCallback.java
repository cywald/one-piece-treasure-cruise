package com.adjust.sdk;

public class Adjust2dxEventTrackingFailedCallback
  implements OnEventTrackingFailedListener
{
  public native void eventTrackingFailed(Object paramObject);
  
  public void onFinishedEventTrackingFailed(AdjustEventFailure paramAdjustEventFailure)
  {
    eventTrackingFailed(paramAdjustEventFailure);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Adjust2dxEventTrackingFailedCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */