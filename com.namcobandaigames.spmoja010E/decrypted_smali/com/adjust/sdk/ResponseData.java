package com.adjust.sdk;

import java.util.Locale;
import org.json.JSONObject;

public class ResponseData
{
  public ActivityKind activityKind;
  public String adid;
  public AdjustAttribution attribution;
  public JSONObject jsonResponse;
  public String message;
  public boolean success;
  public String timestamp;
  public boolean willRetry;
  
  public static ResponseData buildResponseData(ActivityPackage paramActivityPackage)
  {
    ActivityKind localActivityKind = paramActivityPackage.getActivityKind();
    switch (localActivityKind)
    {
    default: 
      paramActivityPackage = new ResponseData();
    }
    for (;;)
    {
      paramActivityPackage.activityKind = localActivityKind;
      return paramActivityPackage;
      paramActivityPackage = new SessionResponseData();
      continue;
      paramActivityPackage = new SdkClickResponseData();
      continue;
      paramActivityPackage = new AttributionResponseData();
      continue;
      paramActivityPackage = new EventResponseData(paramActivityPackage);
    }
  }
  
  public String toString()
  {
    return String.format(Locale.US, "message:%s timestamp:%s json:%s", new Object[] { this.message, this.timestamp, this.jsonResponse });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\ResponseData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */