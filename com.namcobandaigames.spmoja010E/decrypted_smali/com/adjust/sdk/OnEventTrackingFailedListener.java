package com.adjust.sdk;

public abstract interface OnEventTrackingFailedListener
{
  public abstract void onFinishedEventTrackingFailed(AdjustEventFailure paramAdjustEventFailure);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\OnEventTrackingFailedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */