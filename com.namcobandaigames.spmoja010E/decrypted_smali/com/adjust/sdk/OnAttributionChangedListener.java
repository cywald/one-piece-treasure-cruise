package com.adjust.sdk;

public abstract interface OnAttributionChangedListener
{
  public abstract void onAttributionChanged(AdjustAttribution paramAdjustAttribution);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\OnAttributionChangedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */