package com.adjust.sdk;

public abstract interface IAttributionHandler
{
  public abstract void checkSdkClickResponse(SdkClickResponseData paramSdkClickResponseData);
  
  public abstract void checkSessionResponse(SessionResponseData paramSessionResponseData);
  
  public abstract void getAttribution();
  
  public abstract void init(IActivityHandler paramIActivityHandler, ActivityPackage paramActivityPackage, boolean paramBoolean);
  
  public abstract void pauseSending();
  
  public abstract void resumeSending();
  
  public abstract void teardown();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\IAttributionHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */