package com.adjust.sdk;

import java.util.Locale;
import org.json.JSONObject;

public class AdjustSessionFailure
{
  public String adid;
  public JSONObject jsonResponse;
  public String message;
  public String timestamp;
  public boolean willRetry;
  
  public String toString()
  {
    return String.format(Locale.US, "Session Failure msg:%s time:%s adid:%s retry:%b json:%s", new Object[] { this.message, this.timestamp, this.adid, Boolean.valueOf(this.willRetry), this.jsonResponse });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\AdjustSessionFailure.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */