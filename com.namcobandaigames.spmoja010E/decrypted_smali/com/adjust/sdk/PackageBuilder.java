package com.adjust.sdk;

import android.content.ContentResolver;
import android.content.Context;
import android.text.TextUtils;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONObject;

class PackageBuilder
{
  private static ILogger logger = ;
  private ActivityStateCopy activityStateCopy;
  private AdjustConfig adjustConfig;
  AdjustAttribution attribution;
  long clickTime;
  private long createdAt;
  String deeplink;
  private DeviceInfo deviceInfo;
  Map<String, String> extraParameters;
  String referrer;
  String reftag;
  
  public PackageBuilder(AdjustConfig paramAdjustConfig, DeviceInfo paramDeviceInfo, ActivityState paramActivityState, long paramLong)
  {
    this.adjustConfig = paramAdjustConfig;
    this.deviceInfo = paramDeviceInfo;
    this.activityStateCopy = new ActivityStateCopy(paramActivityState);
    this.createdAt = paramLong;
  }
  
  public static void addBoolean(Map<String, String> paramMap, String paramString, Boolean paramBoolean)
  {
    if (paramBoolean == null) {
      return;
    }
    if (paramBoolean.booleanValue()) {}
    for (int i = 1;; i = 0)
    {
      addInt(paramMap, paramString, i);
      return;
    }
  }
  
  public static void addDate(Map<String, String> paramMap, String paramString, long paramLong)
  {
    if (paramLong < 0L) {
      return;
    }
    addString(paramMap, paramString, Util.dateFormatter.format(Long.valueOf(paramLong)));
  }
  
  public static void addDouble(Map<String, String> paramMap, String paramString, Double paramDouble)
  {
    if (paramDouble == null) {
      return;
    }
    addString(paramMap, paramString, String.format(Locale.US, "%.5f", new Object[] { paramDouble }));
  }
  
  public static void addDuration(Map<String, String> paramMap, String paramString, long paramLong)
  {
    if (paramLong < 0L) {
      return;
    }
    addInt(paramMap, paramString, (500L + paramLong) / 1000L);
  }
  
  public static void addInt(Map<String, String> paramMap, String paramString, long paramLong)
  {
    if (paramLong < 0L) {
      return;
    }
    addString(paramMap, paramString, Long.toString(paramLong));
  }
  
  public static void addMapJson(Map<String, String> paramMap1, String paramString, Map<String, String> paramMap2)
  {
    if (paramMap2 == null) {}
    while (paramMap2.size() == 0) {
      return;
    }
    addString(paramMap1, paramString, new JSONObject(paramMap2).toString());
  }
  
  public static void addString(Map<String, String> paramMap, String paramString1, String paramString2)
  {
    if (TextUtils.isEmpty(paramString2)) {
      return;
    }
    paramMap.put(paramString1, paramString2);
  }
  
  private void checkDeviceIds(Map<String, String> paramMap)
  {
    if ((!paramMap.containsKey("mac_sha1")) && (!paramMap.containsKey("mac_md5")) && (!paramMap.containsKey("android_id")) && (!paramMap.containsKey("gps_adid"))) {
      logger.error("Missing device id's. Please check if Proguard is correctly set with Adjust SDK", new Object[0]);
    }
  }
  
  private void fillPluginKeys(Map<String, String> paramMap)
  {
    if (this.deviceInfo.pluginKeys == null) {}
    for (;;)
    {
      return;
      Iterator localIterator = this.deviceInfo.pluginKeys.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        addString(paramMap, (String)localEntry.getKey(), (String)localEntry.getValue());
      }
    }
  }
  
  private Map<String, String> getAttributableParameters(SessionParameters paramSessionParameters)
  {
    Map localMap = getDefaultParameters();
    addDuration(localMap, "last_interval", this.activityStateCopy.lastInterval);
    addString(localMap, "default_tracker", this.adjustConfig.defaultTracker);
    addString(localMap, "installed_at", this.deviceInfo.appInstallTime);
    addString(localMap, "updated_at", this.deviceInfo.appUpdateTime);
    if (paramSessionParameters != null)
    {
      addMapJson(localMap, "callback_params", paramSessionParameters.callbackParameters);
      addMapJson(localMap, "partner_params", paramSessionParameters.partnerParameters);
    }
    return localMap;
  }
  
  private ActivityPackage getDefaultActivityPackage(ActivityKind paramActivityKind)
  {
    paramActivityKind = new ActivityPackage(paramActivityKind);
    paramActivityKind.setClientSdk(this.deviceInfo.clientSdk);
    return paramActivityKind;
  }
  
  private Map<String, String> getDefaultParameters()
  {
    HashMap localHashMap = new HashMap();
    injectDeviceInfo(localHashMap);
    injectConfig(localHashMap);
    injectActivityState(localHashMap);
    injectCommonParameters(localHashMap);
    checkDeviceIds(localHashMap);
    return localHashMap;
  }
  
  private String getEventSuffix(AdjustEvent paramAdjustEvent)
  {
    if (paramAdjustEvent.revenue == null) {
      return String.format(Locale.US, "'%s'", new Object[] { paramAdjustEvent.eventToken });
    }
    return String.format(Locale.US, "(%.5f %s, '%s')", new Object[] { paramAdjustEvent.revenue, paramAdjustEvent.currency, paramAdjustEvent.eventToken });
  }
  
  private Map<String, String> getIdsParameters()
  {
    HashMap localHashMap = new HashMap();
    injectDeviceInfoIds(localHashMap);
    injectConfig(localHashMap);
    injectCommonParameters(localHashMap);
    checkDeviceIds(localHashMap);
    return localHashMap;
  }
  
  private void injectActivityState(Map<String, String> paramMap)
  {
    addString(paramMap, "android_uuid", this.activityStateCopy.uuid);
    addInt(paramMap, "session_count", this.activityStateCopy.sessionCount);
    addInt(paramMap, "subsession_count", this.activityStateCopy.subsessionCount);
    addDuration(paramMap, "session_length", this.activityStateCopy.sessionLength);
    addDuration(paramMap, "time_spent", this.activityStateCopy.timeSpent);
  }
  
  private void injectAttribution(Map<String, String> paramMap)
  {
    if (this.attribution == null) {
      return;
    }
    addString(paramMap, "tracker", this.attribution.trackerName);
    addString(paramMap, "campaign", this.attribution.campaign);
    addString(paramMap, "adgroup", this.attribution.adgroup);
    addString(paramMap, "creative", this.attribution.creative);
  }
  
  private void injectCommonParameters(Map<String, String> paramMap)
  {
    addDate(paramMap, "created_at", this.createdAt);
    addBoolean(paramMap, "attribution_deeplink", Boolean.valueOf(true));
  }
  
  private void injectConfig(Map<String, String> paramMap)
  {
    addString(paramMap, "app_token", this.adjustConfig.appToken);
    addString(paramMap, "environment", this.adjustConfig.environment);
    addBoolean(paramMap, "device_known", this.adjustConfig.deviceKnown);
    addBoolean(paramMap, "needs_response_details", Boolean.valueOf(true));
    addString(paramMap, "gps_adid", Util.getPlayAdId(this.adjustConfig.context));
    addBoolean(paramMap, "tracking_enabled", Util.isPlayTrackingEnabled(this.adjustConfig.context));
    addBoolean(paramMap, "event_buffering_enabled", Boolean.valueOf(this.adjustConfig.eventBufferingEnabled));
    addString(paramMap, "push_token", this.activityStateCopy.pushToken);
    ContentResolver localContentResolver = this.adjustConfig.context.getContentResolver();
    addString(paramMap, "fire_adid", Util.getFireAdvertisingId(localContentResolver));
    addBoolean(paramMap, "fire_tracking_enabled", Util.getFireTrackingEnabled(localContentResolver));
  }
  
  private void injectDeviceInfo(Map<String, String> paramMap)
  {
    injectDeviceInfoIds(paramMap);
    addString(paramMap, "fb_id", this.deviceInfo.fbAttributionId);
    addString(paramMap, "package_name", this.deviceInfo.packageName);
    addString(paramMap, "app_version", this.deviceInfo.appVersion);
    addString(paramMap, "device_type", this.deviceInfo.deviceType);
    addString(paramMap, "device_name", this.deviceInfo.deviceName);
    addString(paramMap, "device_manufacturer", this.deviceInfo.deviceManufacturer);
    addString(paramMap, "os_name", this.deviceInfo.osName);
    addString(paramMap, "os_version", this.deviceInfo.osVersion);
    addString(paramMap, "api_level", this.deviceInfo.apiLevel);
    addString(paramMap, "language", this.deviceInfo.language);
    addString(paramMap, "country", this.deviceInfo.country);
    addString(paramMap, "screen_size", this.deviceInfo.screenSize);
    addString(paramMap, "screen_format", this.deviceInfo.screenFormat);
    addString(paramMap, "screen_density", this.deviceInfo.screenDensity);
    addString(paramMap, "display_width", this.deviceInfo.displayWidth);
    addString(paramMap, "display_height", this.deviceInfo.displayHeight);
    addString(paramMap, "hardware_name", this.deviceInfo.hardwareName);
    addString(paramMap, "cpu_type", this.deviceInfo.abi);
    addString(paramMap, "os_build", this.deviceInfo.buildName);
    addString(paramMap, "vm_isa", this.deviceInfo.vmInstructionSet);
    fillPluginKeys(paramMap);
  }
  
  private void injectDeviceInfoIds(Map<String, String> paramMap)
  {
    addString(paramMap, "mac_sha1", this.deviceInfo.macSha1);
    addString(paramMap, "mac_md5", this.deviceInfo.macShortMd5);
    addString(paramMap, "android_id", this.deviceInfo.androidId);
  }
  
  public ActivityPackage buildAttributionPackage()
  {
    Map localMap = getIdsParameters();
    ActivityPackage localActivityPackage = getDefaultActivityPackage(ActivityKind.ATTRIBUTION);
    localActivityPackage.setPath("attribution");
    localActivityPackage.setSuffix("");
    localActivityPackage.setParameters(localMap);
    return localActivityPackage;
  }
  
  public ActivityPackage buildClickPackage(String paramString, SessionParameters paramSessionParameters)
  {
    paramSessionParameters = getAttributableParameters(paramSessionParameters);
    addString(paramSessionParameters, "source", paramString);
    addDate(paramSessionParameters, "click_time", this.clickTime);
    addString(paramSessionParameters, "reftag", this.reftag);
    addMapJson(paramSessionParameters, "params", this.extraParameters);
    addString(paramSessionParameters, "referrer", this.referrer);
    addString(paramSessionParameters, "deeplink", this.deeplink);
    injectAttribution(paramSessionParameters);
    paramString = getDefaultActivityPackage(ActivityKind.CLICK);
    paramString.setPath("/sdk_click");
    paramString.setSuffix("");
    paramString.setParameters(paramSessionParameters);
    return paramString;
  }
  
  public ActivityPackage buildEventPackage(AdjustEvent paramAdjustEvent, SessionParameters paramSessionParameters, boolean paramBoolean)
  {
    Map localMap = getDefaultParameters();
    addInt(localMap, "event_count", this.activityStateCopy.eventCount);
    addString(localMap, "event_token", paramAdjustEvent.eventToken);
    addDouble(localMap, "revenue", paramAdjustEvent.revenue);
    addString(localMap, "currency", paramAdjustEvent.currency);
    if (!paramBoolean)
    {
      addMapJson(localMap, "callback_params", Util.mergeParameters(paramSessionParameters.callbackParameters, paramAdjustEvent.callbackParameters, "Callback"));
      addMapJson(localMap, "partner_params", Util.mergeParameters(paramSessionParameters.partnerParameters, paramAdjustEvent.partnerParameters, "Partner"));
    }
    paramSessionParameters = getDefaultActivityPackage(ActivityKind.EVENT);
    paramSessionParameters.setPath("/event");
    paramSessionParameters.setSuffix(getEventSuffix(paramAdjustEvent));
    paramSessionParameters.setParameters(localMap);
    if (paramBoolean)
    {
      paramSessionParameters.setCallbackParameters(paramAdjustEvent.callbackParameters);
      paramSessionParameters.setPartnerParameters(paramAdjustEvent.partnerParameters);
    }
    return paramSessionParameters;
  }
  
  public ActivityPackage buildInfoPackage(String paramString)
  {
    Map localMap = getIdsParameters();
    addString(localMap, "source", paramString);
    paramString = getDefaultActivityPackage(ActivityKind.INFO);
    paramString.setPath("/sdk_info");
    paramString.setSuffix("");
    paramString.setParameters(localMap);
    return paramString;
  }
  
  public ActivityPackage buildSessionPackage(SessionParameters paramSessionParameters, boolean paramBoolean)
  {
    if (!paramBoolean) {}
    for (paramSessionParameters = getAttributableParameters(paramSessionParameters);; paramSessionParameters = getAttributableParameters(null))
    {
      ActivityPackage localActivityPackage = getDefaultActivityPackage(ActivityKind.SESSION);
      localActivityPackage.setPath("/session");
      localActivityPackage.setSuffix("");
      localActivityPackage.setParameters(paramSessionParameters);
      return localActivityPackage;
    }
  }
  
  private class ActivityStateCopy
  {
    int eventCount = -1;
    long lastInterval = -1L;
    String pushToken = null;
    int sessionCount = -1;
    long sessionLength = -1L;
    int subsessionCount = -1;
    long timeSpent = -1L;
    String uuid = null;
    
    ActivityStateCopy(ActivityState paramActivityState)
    {
      if (paramActivityState == null) {
        return;
      }
      this.lastInterval = paramActivityState.lastInterval;
      this.eventCount = paramActivityState.eventCount;
      this.uuid = paramActivityState.uuid;
      this.sessionCount = paramActivityState.sessionCount;
      this.subsessionCount = paramActivityState.subsessionCount;
      this.sessionLength = paramActivityState.sessionLength;
      this.timeSpent = paramActivityState.timeSpent;
      this.pushToken = paramActivityState.pushToken;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\PackageBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */