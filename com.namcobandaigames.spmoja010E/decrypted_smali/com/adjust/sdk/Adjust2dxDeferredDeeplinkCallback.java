package com.adjust.sdk;

import android.net.Uri;

public class Adjust2dxDeferredDeeplinkCallback
  implements OnDeeplinkResponseListener
{
  public native boolean deferredDeeplinkReceived(String paramString);
  
  public boolean launchReceivedDeeplink(Uri paramUri)
  {
    return deferredDeeplinkReceived(paramUri.toString());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Adjust2dxDeferredDeeplinkCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */