package com.adjust.sdk;

public class Adjust2dxSessionTrackingSucceededCallback
  implements OnSessionTrackingSucceededListener
{
  public void onFinishedSessionTrackingSucceeded(AdjustSessionSuccess paramAdjustSessionSuccess)
  {
    sessionTrackingSucceeded(paramAdjustSessionSuccess);
  }
  
  public native void sessionTrackingSucceeded(Object paramObject);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Adjust2dxSessionTrackingSucceededCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */