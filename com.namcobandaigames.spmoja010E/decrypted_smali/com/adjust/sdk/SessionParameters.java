package com.adjust.sdk;

import java.util.HashMap;
import java.util.Map;

public class SessionParameters
{
  Map<String, String> callbackParameters;
  Map<String, String> partnerParameters;
  
  public SessionParameters deepCopy()
  {
    SessionParameters localSessionParameters = new SessionParameters();
    if (this.callbackParameters != null) {
      localSessionParameters.callbackParameters = new HashMap(this.callbackParameters);
    }
    if (this.partnerParameters != null) {
      localSessionParameters.partnerParameters = new HashMap(this.partnerParameters);
    }
    return localSessionParameters;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\SessionParameters.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */