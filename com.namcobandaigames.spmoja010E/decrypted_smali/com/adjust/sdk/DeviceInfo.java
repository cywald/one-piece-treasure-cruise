package com.adjust.sdk;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

class DeviceInfo
{
  String abi;
  String androidId;
  String apiLevel;
  String appInstallTime;
  String appUpdateTime;
  String appVersion;
  String buildName;
  String clientSdk;
  String country;
  String deviceManufacturer;
  String deviceName;
  String deviceType;
  String displayHeight;
  String displayWidth;
  String fbAttributionId;
  String hardwareName;
  String language;
  String macSha1;
  String macShortMd5;
  String osName;
  String osVersion;
  String packageName;
  Map<String, String> pluginKeys;
  String screenDensity;
  String screenFormat;
  String screenSize;
  String vmInstructionSet;
  
  DeviceInfo(Context paramContext, String paramString)
  {
    Object localObject1 = paramContext.getResources();
    DisplayMetrics localDisplayMetrics = ((Resources)localObject1).getDisplayMetrics();
    Object localObject2 = ((Resources)localObject1).getConfiguration();
    localObject1 = Util.getLocale((Configuration)localObject2);
    int i = ((Configuration)localObject2).screenLayout;
    if (Util.getPlayAdId(paramContext) != null) {}
    for (boolean bool = true;; bool = false)
    {
      localObject2 = getMacAddress(paramContext, bool);
      paramContext.getContentResolver();
      this.packageName = getPackageName(paramContext);
      this.appVersion = getAppVersion(paramContext);
      this.deviceType = getDeviceType(i);
      this.deviceName = getDeviceName();
      this.deviceManufacturer = getDeviceManufacturer();
      this.osName = getOsName();
      this.osVersion = getOsVersion();
      this.apiLevel = getApiLevel();
      this.language = getLanguage((Locale)localObject1);
      this.country = getCountry((Locale)localObject1);
      this.screenSize = getScreenSize(i);
      this.screenFormat = getScreenFormat(i);
      this.screenDensity = getScreenDensity(localDisplayMetrics);
      this.displayWidth = getDisplayWidth(localDisplayMetrics);
      this.displayHeight = getDisplayHeight(localDisplayMetrics);
      this.clientSdk = getClientSdk(paramString);
      this.androidId = getAndroidId(paramContext, bool);
      this.fbAttributionId = getFacebookAttributionId(paramContext);
      this.pluginKeys = Util.getPluginKeys(paramContext);
      this.macSha1 = getMacSha1((String)localObject2);
      this.macShortMd5 = getMacShortMd5((String)localObject2);
      this.hardwareName = getHardwareName();
      this.abi = getABI();
      this.buildName = getBuildName();
      this.vmInstructionSet = getVmInstructionSet();
      this.appInstallTime = getAppInstallTime(paramContext);
      this.appUpdateTime = getAppUpdateTime(paramContext);
      return;
    }
  }
  
  private String getABI()
  {
    String[] arrayOfString = Util.getSupportedAbis();
    if ((arrayOfString == null) || (arrayOfString.length == 0)) {
      return Util.getCpuAbi();
    }
    return arrayOfString[0];
  }
  
  private String getAndroidId(Context paramContext, boolean paramBoolean)
  {
    if (!paramBoolean) {
      return Util.getAndroidId(paramContext);
    }
    return null;
  }
  
  private String getApiLevel()
  {
    return "" + Build.VERSION.SDK_INT;
  }
  
  private String getAppInstallTime(Context paramContext)
  {
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 4096);
      paramContext = Util.dateFormatter.format(new Date(paramContext.firstInstallTime));
      return paramContext;
    }
    catch (Exception paramContext) {}
    return null;
  }
  
  private String getAppUpdateTime(Context paramContext)
  {
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 4096);
      paramContext = Util.dateFormatter.format(new Date(paramContext.lastUpdateTime));
      return paramContext;
    }
    catch (Exception paramContext) {}
    return null;
  }
  
  private String getAppVersion(Context paramContext)
  {
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionName;
      return paramContext;
    }
    catch (Exception paramContext) {}
    return null;
  }
  
  private String getBuildName()
  {
    return Build.ID;
  }
  
  private String getClientSdk(String paramString)
  {
    if (paramString == null) {
      return "android4.11.4";
    }
    return String.format(Locale.US, "%s@%s", new Object[] { paramString, "android4.11.4" });
  }
  
  private String getCountry(Locale paramLocale)
  {
    return paramLocale.getCountry();
  }
  
  private String getDeviceManufacturer()
  {
    return Build.MANUFACTURER;
  }
  
  private String getDeviceName()
  {
    return Build.MODEL;
  }
  
  private String getDeviceType(int paramInt)
  {
    switch (paramInt & 0xF)
    {
    default: 
      return null;
    case 1: 
    case 2: 
      return "phone";
    }
    return "tablet";
  }
  
  private String getDisplayHeight(DisplayMetrics paramDisplayMetrics)
  {
    return String.valueOf(paramDisplayMetrics.heightPixels);
  }
  
  private String getDisplayWidth(DisplayMetrics paramDisplayMetrics)
  {
    return String.valueOf(paramDisplayMetrics.widthPixels);
  }
  
  private String getFacebookAttributionId(Context paramContext)
  {
    try
    {
      paramContext = paramContext.getContentResolver().query(Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider"), new String[] { "aid" }, null, null, null);
      if (paramContext == null) {
        return null;
      }
      if (!paramContext.moveToFirst())
      {
        paramContext.close();
        return null;
      }
      String str = paramContext.getString(paramContext.getColumnIndex("aid"));
      paramContext.close();
      return str;
    }
    catch (Exception paramContext) {}
    return null;
  }
  
  private String getHardwareName()
  {
    return Build.DISPLAY;
  }
  
  private String getLanguage(Locale paramLocale)
  {
    return paramLocale.getLanguage();
  }
  
  private String getMacAddress(Context paramContext, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      if (!Util.checkPermission(paramContext, "android.permission.ACCESS_WIFI_STATE")) {
        AdjustFactory.getLogger().warn("Missing permission: ACCESS_WIFI_STATE", new Object[0]);
      }
      return Util.getMacAddress(paramContext);
    }
    return null;
  }
  
  private String getMacSha1(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return Util.sha1(paramString);
  }
  
  private String getMacShortMd5(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return Util.md5(paramString.replaceAll(":", ""));
  }
  
  private String getOsName()
  {
    return "android";
  }
  
  private String getOsVersion()
  {
    return Build.VERSION.RELEASE;
  }
  
  private String getPackageName(Context paramContext)
  {
    return paramContext.getPackageName();
  }
  
  private String getScreenDensity(DisplayMetrics paramDisplayMetrics)
  {
    int i = paramDisplayMetrics.densityDpi;
    if (i == 0) {
      return null;
    }
    if (i < 140) {
      return "low";
    }
    if (i > 200) {
      return "high";
    }
    return "medium";
  }
  
  private String getScreenFormat(int paramInt)
  {
    switch (paramInt & 0x30)
    {
    default: 
      return null;
    case 32: 
      return "long";
    }
    return "normal";
  }
  
  private String getScreenSize(int paramInt)
  {
    switch (paramInt & 0xF)
    {
    default: 
      return null;
    case 1: 
      return "small";
    case 2: 
      return "normal";
    case 3: 
      return "large";
    }
    return "xlarge";
  }
  
  private String getVmInstructionSet()
  {
    return Util.getVmInstructionSet();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\DeviceInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */