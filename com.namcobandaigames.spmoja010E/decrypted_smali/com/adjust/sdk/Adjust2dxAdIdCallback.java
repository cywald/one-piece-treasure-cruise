package com.adjust.sdk;

public class Adjust2dxAdIdCallback
  implements OnDeviceIdsRead
{
  public native void adIdRead(String paramString);
  
  public void onGoogleAdIdRead(String paramString)
  {
    adIdRead(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Adjust2dxAdIdCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */