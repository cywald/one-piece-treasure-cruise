package com.adjust.sdk;

import java.util.Map;

public class EventResponseData
  extends ResponseData
{
  public String eventToken;
  
  public EventResponseData(ActivityPackage paramActivityPackage)
  {
    this.eventToken = ((String)paramActivityPackage.getParameters().get("event_token"));
  }
  
  public AdjustEventFailure getFailureResponseData()
  {
    if (this.success) {
      return null;
    }
    AdjustEventFailure localAdjustEventFailure = new AdjustEventFailure();
    localAdjustEventFailure.message = this.message;
    localAdjustEventFailure.timestamp = this.timestamp;
    localAdjustEventFailure.adid = this.adid;
    localAdjustEventFailure.willRetry = this.willRetry;
    localAdjustEventFailure.jsonResponse = this.jsonResponse;
    localAdjustEventFailure.eventToken = this.eventToken;
    return localAdjustEventFailure;
  }
  
  public AdjustEventSuccess getSuccessResponseData()
  {
    if (!this.success) {
      return null;
    }
    AdjustEventSuccess localAdjustEventSuccess = new AdjustEventSuccess();
    localAdjustEventSuccess.message = this.message;
    localAdjustEventSuccess.timestamp = this.timestamp;
    localAdjustEventSuccess.adid = this.adid;
    localAdjustEventSuccess.jsonResponse = this.jsonResponse;
    localAdjustEventSuccess.eventToken = this.eventToken;
    return localAdjustEventSuccess;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\EventResponseData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */