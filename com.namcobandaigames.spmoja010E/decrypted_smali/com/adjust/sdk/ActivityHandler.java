package com.adjust.sdk;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.net.UrlQuerySanitizer.ParameterValuePair;
import android.os.Handler;
import android.os.Process;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class ActivityHandler
  implements IActivityHandler
{
  private static final String ACTIVITY_STATE_NAME = "Activity state";
  private static final String ADJUST_PREFIX = "adjust_";
  private static final String ATTRIBUTION_NAME = "Attribution";
  private static long BACKGROUND_TIMER_INTERVAL = 0L;
  private static final String BACKGROUND_TIMER_NAME = "Background timer";
  private static final String DELAY_START_TIMER_NAME = "Delay Start timer";
  private static long FOREGROUND_TIMER_INTERVAL = 0L;
  private static final String FOREGROUND_TIMER_NAME = "Foreground timer";
  private static long FOREGROUND_TIMER_START = 0L;
  private static final String SESSION_CALLBACK_PARAMETERS_NAME = "Session Callback parameters";
  private static long SESSION_INTERVAL = 0L;
  private static final String SESSION_PARTNER_PARAMETERS_NAME = "Session Partner parameters";
  private static long SUBSESSION_INTERVAL = 0L;
  private static final String TIME_TRAVEL = "Time travel!";
  private ActivityState activityState;
  private AdjustConfig adjustConfig;
  private AdjustAttribution attribution;
  private IAttributionHandler attributionHandler;
  private TimerOnce backgroundTimer;
  private TimerOnce delayStartTimer;
  private DeviceInfo deviceInfo;
  private TimerCycle foregroundTimer;
  private InternalState internalState;
  private ILogger logger;
  private IPackageHandler packageHandler;
  private CustomScheduledExecutor scheduledExecutor;
  private ISdkClickHandler sdkClickHandler;
  private SessionParameters sessionParameters;
  
  private ActivityHandler(AdjustConfig paramAdjustConfig)
  {
    init(paramAdjustConfig);
    this.logger = AdjustFactory.getLogger();
    this.logger.lockLogLevel();
    this.scheduledExecutor = new CustomScheduledExecutor("ActivityHandler", false);
    this.internalState = new InternalState();
    this.internalState.enabled = true;
    this.internalState.offline = false;
    this.internalState.background = true;
    this.internalState.delayStart = false;
    this.internalState.updatePackages = false;
    this.internalState.sessionResponseProcessed = false;
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.initI();
      }
    });
  }
  
  private void backgroundTimerFiredI()
  {
    if (toSendI()) {
      this.packageHandler.sendFirstPackage();
    }
  }
  
  private boolean checkActivityStateI(ActivityState paramActivityState)
  {
    if (paramActivityState == null)
    {
      this.logger.error("Missing activity state", new Object[0]);
      return false;
    }
    return true;
  }
  
  private void checkAttributionStateI()
  {
    if (!checkActivityStateI(this.activityState)) {}
    while (((this.internalState.isFirstLaunch()) && (!this.internalState.isSessionResponseProcessed())) || ((this.attribution != null) && (!this.activityState.askingAttribution))) {
      return;
    }
    this.attributionHandler.getAttribution();
  }
  
  private boolean checkEventI(AdjustEvent paramAdjustEvent)
  {
    if (paramAdjustEvent == null)
    {
      this.logger.error("Event missing", new Object[0]);
      return false;
    }
    if (!paramAdjustEvent.isValid())
    {
      this.logger.error("Event not initialized correctly", new Object[0]);
      return false;
    }
    return true;
  }
  
  private boolean checkOrderIdI(String paramString)
  {
    if ((paramString == null) || (paramString.isEmpty())) {
      return true;
    }
    if (this.activityState.findOrderId(paramString))
    {
      this.logger.info("Skipping duplicated order ID '%s'", new Object[] { paramString });
      return false;
    }
    this.activityState.addOrderId(paramString);
    this.logger.verbose("Added order ID '%s'", new Object[] { paramString });
    return true;
  }
  
  private Intent createDeeplinkIntentI(Uri paramUri)
  {
    if (this.adjustConfig.deepLinkComponent == null) {}
    for (paramUri = new Intent("android.intent.action.VIEW", paramUri);; paramUri = new Intent("android.intent.action.VIEW", paramUri, this.adjustConfig.context, this.adjustConfig.deepLinkComponent))
    {
      paramUri.setFlags(268435456);
      paramUri.setPackage(this.adjustConfig.context.getPackageName());
      return paramUri;
    }
  }
  
  private void delayStartI()
  {
    if (this.internalState.isToStartNow()) {
      break label10;
    }
    label10:
    while (isToUpdatePackagesI()) {
      return;
    }
    if (this.adjustConfig.delayStart != null) {}
    for (double d1 = this.adjustConfig.delayStart.doubleValue();; d1 = 0.0D)
    {
      long l2 = AdjustFactory.getMaxDelayStart();
      long l3 = (1000.0D * d1);
      long l1 = l3;
      double d2 = d1;
      if (l3 > l2)
      {
        d2 = l2 / 1000L;
        str1 = Util.SecondsDisplayFormat.format(d1);
        String str2 = Util.SecondsDisplayFormat.format(d2);
        this.logger.warn("Delay start of %s seconds bigger than max allowed value of %s seconds", new Object[] { str1, str2 });
        l1 = l2;
      }
      String str1 = Util.SecondsDisplayFormat.format(d2);
      this.logger.info("Waiting %s seconds before starting first session", new Object[] { str1 });
      this.delayStartTimer.startIn(l1);
      this.internalState.updatePackages = true;
      if (this.activityState == null) {
        break;
      }
      this.activityState.updatePackages = true;
      writeActivityStateI();
      return;
    }
  }
  
  public static boolean deleteActivityState(Context paramContext)
  {
    return paramContext.deleteFile("AdjustIoActivityState");
  }
  
  public static boolean deleteAttribution(Context paramContext)
  {
    return paramContext.deleteFile("AdjustAttribution");
  }
  
  public static boolean deleteSessionCallbackParameters(Context paramContext)
  {
    return paramContext.deleteFile("AdjustSessionCallbackParameters");
  }
  
  public static boolean deleteSessionPartnerParameters(Context paramContext)
  {
    return paramContext.deleteFile("AdjustSessionPartnerParameters");
  }
  
  private void endI()
  {
    if (!toSendI()) {
      pauseSendingI();
    }
    if (updateActivityStateI(System.currentTimeMillis())) {
      writeActivityStateI();
    }
  }
  
  private void foregroundTimerFiredI()
  {
    if (!isEnabledI()) {
      stopForegroundTimerI();
    }
    do
    {
      return;
      if (toSendI()) {
        this.packageHandler.sendFirstPackage();
      }
    } while (!updateActivityStateI(System.currentTimeMillis()));
    writeActivityStateI();
  }
  
  public static ActivityHandler getInstance(AdjustConfig paramAdjustConfig)
  {
    if (paramAdjustConfig == null) {
      AdjustFactory.getLogger().error("AdjustConfig missing", new Object[0]);
    }
    int i;
    do
    {
      return null;
      if (!paramAdjustConfig.isValid())
      {
        AdjustFactory.getLogger().error("AdjustConfig not initialized correctly", new Object[0]);
        return null;
      }
      if (paramAdjustConfig.processName == null) {
        break;
      }
      i = Process.myPid();
      localObject = (ActivityManager)paramAdjustConfig.context.getSystemService("activity");
    } while (localObject == null);
    Object localObject = ((ActivityManager)localObject).getRunningAppProcesses().iterator();
    while (((Iterator)localObject).hasNext())
    {
      ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)((Iterator)localObject).next();
      if (localRunningAppProcessInfo.pid == i) {
        if (!localRunningAppProcessInfo.processName.equalsIgnoreCase(paramAdjustConfig.processName))
        {
          AdjustFactory.getLogger().info("Skipping initialization in background process (%s)", new Object[] { localRunningAppProcessInfo.processName });
          return null;
        }
      }
    }
    return new ActivityHandler(paramAdjustConfig);
  }
  
  private boolean hasChangedState(boolean paramBoolean1, boolean paramBoolean2, String paramString1, String paramString2)
  {
    if (paramBoolean1 != paramBoolean2) {
      return true;
    }
    if (paramBoolean1)
    {
      this.logger.debug(paramString1, new Object[0]);
      return false;
    }
    this.logger.debug(paramString2, new Object[0]);
    return false;
  }
  
  private void initI()
  {
    SESSION_INTERVAL = AdjustFactory.getSessionInterval();
    SUBSESSION_INTERVAL = AdjustFactory.getSubsessionInterval();
    FOREGROUND_TIMER_INTERVAL = AdjustFactory.getTimerInterval();
    FOREGROUND_TIMER_START = AdjustFactory.getTimerStart();
    BACKGROUND_TIMER_INTERVAL = AdjustFactory.getTimerInterval();
    readAttributionI(this.adjustConfig.context);
    readActivityStateI(this.adjustConfig.context);
    this.sessionParameters = new SessionParameters();
    readSessionCallbackParametersI(this.adjustConfig.context);
    readSessionPartnerParametersI(this.adjustConfig.context);
    if (this.activityState != null)
    {
      this.internalState.enabled = this.activityState.enabled;
      this.internalState.updatePackages = this.activityState.updatePackages;
      this.internalState.firstLaunch = false;
      readConfigFile(this.adjustConfig.context);
      this.deviceInfo = new DeviceInfo(this.adjustConfig.context, this.adjustConfig.sdkPrefix);
      if (this.adjustConfig.eventBufferingEnabled) {
        this.logger.info("Event buffering is enabled", new Object[0]);
      }
      if (Util.getPlayAdId(this.adjustConfig.context) != null) {
        break label628;
      }
      this.logger.warn("Unable to get Google Play Services Advertising ID at start time", new Object[0]);
      if ((this.deviceInfo.macSha1 == null) && (this.deviceInfo.macShortMd5 == null) && (this.deviceInfo.androidId == null)) {
        this.logger.error("Unable to get any device id's. Please check if Proguard is correctly set with Adjust SDK", new Object[0]);
      }
    }
    for (;;)
    {
      if (this.adjustConfig.defaultTracker != null) {
        this.logger.info("Default tracker: '%s'", new Object[] { this.adjustConfig.defaultTracker });
      }
      if (this.adjustConfig.pushToken != null)
      {
        this.logger.info("Push token: '%s'", new Object[] { this.adjustConfig.pushToken });
        if (this.activityState != null) {
          setPushToken(this.adjustConfig.pushToken);
        }
      }
      this.foregroundTimer = new TimerCycle(new Runnable()
      {
        public void run()
        {
          ActivityHandler.this.foregroundTimerFired();
        }
      }, FOREGROUND_TIMER_START, FOREGROUND_TIMER_INTERVAL, "Foreground timer");
      if (this.adjustConfig.sendInBackground)
      {
        this.logger.info("Send in background configured", new Object[0]);
        this.backgroundTimer = new TimerOnce(new Runnable()
        {
          public void run()
          {
            ActivityHandler.this.backgroundTimerFired();
          }
        }, "Background timer");
      }
      if ((this.activityState == null) && (this.adjustConfig.delayStart != null) && (this.adjustConfig.delayStart.doubleValue() > 0.0D))
      {
        this.logger.info("Delay start configured", new Object[0]);
        this.internalState.delayStart = true;
        this.delayStartTimer = new TimerOnce(new Runnable()
        {
          public void run()
          {
            ActivityHandler.this.sendFirstPackages();
          }
        }, "Delay Start timer");
      }
      UtilNetworking.setUserAgent(this.adjustConfig.userAgent);
      this.packageHandler = AdjustFactory.getPackageHandler(this, this.adjustConfig.context, toSendI(false));
      this.attributionHandler = AdjustFactory.getAttributionHandler(this, getAttributionPackageI(), toSendI(false));
      this.sdkClickHandler = AdjustFactory.getSdkClickHandler(this, toSendI(true));
      if (isToUpdatePackagesI()) {
        updatePackagesI();
      }
      if (this.adjustConfig.referrer != null) {
        sendReferrerI(this.adjustConfig.referrer, this.adjustConfig.referrerClickTime);
      }
      sessionParametersActionsI(this.adjustConfig.sessionParametersActionsArray);
      return;
      this.internalState.firstLaunch = true;
      break;
      label628:
      this.logger.info("Google Play Services Advertising ID read correctly at start time", new Object[0]);
    }
  }
  
  private boolean isEnabledI()
  {
    if (this.activityState != null) {
      return this.activityState.enabled;
    }
    return this.internalState.isEnabled();
  }
  
  private boolean isToUpdatePackagesI()
  {
    if (this.activityState != null) {
      return this.activityState.updatePackages;
    }
    return this.internalState.isToUpdatePackages();
  }
  
  private void launchAttributionListenerI(Handler paramHandler)
  {
    if (this.adjustConfig.onAttributionChangedListener == null) {
      return;
    }
    paramHandler.post(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.adjustConfig.onAttributionChangedListener.onAttributionChanged(ActivityHandler.this.attribution);
      }
    });
  }
  
  private void launchAttributionResponseTasksI(AttributionResponseData paramAttributionResponseData)
  {
    updateAdidI(paramAttributionResponseData.adid);
    Handler localHandler = new Handler(this.adjustConfig.context.getMainLooper());
    if (updateAttributionI(paramAttributionResponseData.attribution)) {
      launchAttributionListenerI(localHandler);
    }
    prepareDeeplinkI(paramAttributionResponseData.deeplink, localHandler);
  }
  
  private void launchDeeplinkMain(Intent paramIntent, Uri paramUri)
  {
    if (this.adjustConfig.context.getPackageManager().queryIntentActivities(paramIntent, 0).size() > 0) {}
    for (int i = 1; i == 0; i = 0)
    {
      this.logger.error("Unable to open deferred deep link (%s)", new Object[] { paramUri });
      return;
    }
    this.logger.info("Open deferred deep link (%s)", new Object[] { paramUri });
    this.adjustConfig.context.startActivity(paramIntent);
  }
  
  private void launchEventResponseTasksI(final EventResponseData paramEventResponseData)
  {
    updateAdidI(paramEventResponseData.adid);
    Handler localHandler = new Handler(this.adjustConfig.context.getMainLooper());
    if ((paramEventResponseData.success) && (this.adjustConfig.onEventTrackingSucceededListener != null))
    {
      this.logger.debug("Launching success event tracking listener", new Object[0]);
      localHandler.post(new Runnable()
      {
        public void run()
        {
          ActivityHandler.this.adjustConfig.onEventTrackingSucceededListener.onFinishedEventTrackingSucceeded(paramEventResponseData.getSuccessResponseData());
        }
      });
    }
    while ((paramEventResponseData.success) || (this.adjustConfig.onEventTrackingFailedListener == null)) {
      return;
    }
    this.logger.debug("Launching failed event tracking listener", new Object[0]);
    localHandler.post(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.adjustConfig.onEventTrackingFailedListener.onFinishedEventTrackingFailed(paramEventResponseData.getFailureResponseData());
      }
    });
  }
  
  private void launchSdkClickResponseTasksI(SdkClickResponseData paramSdkClickResponseData)
  {
    updateAdidI(paramSdkClickResponseData.adid);
    Handler localHandler = new Handler(this.adjustConfig.context.getMainLooper());
    if (updateAttributionI(paramSdkClickResponseData.attribution)) {
      launchAttributionListenerI(localHandler);
    }
  }
  
  private void launchSessionResponseListenerI(final SessionResponseData paramSessionResponseData, Handler paramHandler)
  {
    if ((paramSessionResponseData.success) && (this.adjustConfig.onSessionTrackingSucceededListener != null))
    {
      this.logger.debug("Launching success session tracking listener", new Object[0]);
      paramHandler.post(new Runnable()
      {
        public void run()
        {
          ActivityHandler.this.adjustConfig.onSessionTrackingSucceededListener.onFinishedSessionTrackingSucceeded(paramSessionResponseData.getSuccessResponseData());
        }
      });
    }
    while ((paramSessionResponseData.success) || (this.adjustConfig.onSessionTrackingFailedListener == null)) {
      return;
    }
    this.logger.debug("Launching failed session tracking listener", new Object[0]);
    paramHandler.post(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.adjustConfig.onSessionTrackingFailedListener.onFinishedSessionTrackingFailed(paramSessionResponseData.getFailureResponseData());
      }
    });
  }
  
  private void launchSessionResponseTasksI(SessionResponseData paramSessionResponseData)
  {
    updateAdidI(paramSessionResponseData.adid);
    Handler localHandler = new Handler(this.adjustConfig.context.getMainLooper());
    if (updateAttributionI(paramSessionResponseData.attribution)) {
      launchAttributionListenerI(localHandler);
    }
    launchSessionResponseListenerI(paramSessionResponseData, localHandler);
    this.internalState.sessionResponseProcessed = true;
  }
  
  private void pauseSendingI()
  {
    this.attributionHandler.pauseSending();
    this.packageHandler.pauseSending();
    if (!toSendI(true))
    {
      this.sdkClickHandler.pauseSending();
      return;
    }
    this.sdkClickHandler.resumeSending();
  }
  
  private boolean pausedI()
  {
    return pausedI(false);
  }
  
  private boolean pausedI(boolean paramBoolean)
  {
    boolean bool = false;
    if (paramBoolean) {
      if (!this.internalState.isOffline())
      {
        paramBoolean = bool;
        if (isEnabledI()) {}
      }
      else
      {
        paramBoolean = true;
      }
    }
    do
    {
      return paramBoolean;
      if ((this.internalState.isOffline()) || (!isEnabledI())) {
        break;
      }
      paramBoolean = bool;
    } while (!this.internalState.isDelayStart());
    return true;
  }
  
  private void prepareDeeplinkI(final Uri paramUri, Handler paramHandler)
  {
    if (paramUri == null) {
      return;
    }
    this.logger.info("Deferred deeplink received (%s)", new Object[] { paramUri });
    paramHandler.post(new Runnable()
    {
      public void run()
      {
        boolean bool = true;
        if (ActivityHandler.this.adjustConfig.onDeeplinkResponseListener != null) {
          bool = ActivityHandler.this.adjustConfig.onDeeplinkResponseListener.launchReceivedDeeplink(paramUri);
        }
        if (bool) {
          ActivityHandler.this.launchDeeplinkMain(this.val$deeplinkIntent, paramUri);
        }
      }
    });
  }
  
  private void processSessionI()
  {
    long l1 = System.currentTimeMillis();
    if (this.activityState == null)
    {
      this.activityState = new ActivityState();
      this.activityState.sessionCount = 1;
      this.activityState.pushToken = this.adjustConfig.pushToken;
      transferSessionPackageI(l1);
      this.activityState.resetSessionAttributes(l1);
      this.activityState.enabled = this.internalState.isEnabled();
      this.activityState.updatePackages = this.internalState.isToUpdatePackages();
      writeActivityStateI();
      return;
    }
    long l2 = l1 - this.activityState.lastActivity;
    if (l2 < 0L)
    {
      this.logger.error("Time travel!", new Object[0]);
      this.activityState.lastActivity = l1;
      writeActivityStateI();
      return;
    }
    ActivityState localActivityState;
    if (l2 > SESSION_INTERVAL)
    {
      localActivityState = this.activityState;
      localActivityState.sessionCount += 1;
      this.activityState.lastInterval = l2;
      transferSessionPackageI(l1);
      this.activityState.resetSessionAttributes(l1);
      writeActivityStateI();
      return;
    }
    if (l2 > SUBSESSION_INTERVAL)
    {
      localActivityState = this.activityState;
      localActivityState.subsessionCount += 1;
      localActivityState = this.activityState;
      localActivityState.sessionLength += l2;
      this.activityState.lastActivity = l1;
      this.logger.verbose("Started subsession %d of session %d", new Object[] { Integer.valueOf(this.activityState.subsessionCount), Integer.valueOf(this.activityState.sessionCount) });
      writeActivityStateI();
      return;
    }
    this.logger.verbose("Time span since last activity too short for a new subsession", new Object[0]);
  }
  
  private PackageBuilder queryStringClickPackageBuilderI(List<UrlQuerySanitizer.ParameterValuePair> paramList)
  {
    if (paramList == null) {
      return null;
    }
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    AdjustAttribution localAdjustAttribution = new AdjustAttribution();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      localObject = (UrlQuerySanitizer.ParameterValuePair)paramList.next();
      readQueryStringI(((UrlQuerySanitizer.ParameterValuePair)localObject).mParameter, ((UrlQuerySanitizer.ParameterValuePair)localObject).mValue, localLinkedHashMap, localAdjustAttribution);
    }
    paramList = (String)localLinkedHashMap.remove("reftag");
    long l1 = System.currentTimeMillis();
    if (this.activityState != null)
    {
      long l2 = this.activityState.lastActivity;
      this.activityState.lastInterval = (l1 - l2);
    }
    Object localObject = new PackageBuilder(this.adjustConfig, this.deviceInfo, this.activityState, l1);
    ((PackageBuilder)localObject).extraParameters = localLinkedHashMap;
    ((PackageBuilder)localObject).attribution = localAdjustAttribution;
    ((PackageBuilder)localObject).reftag = paramList;
    return (PackageBuilder)localObject;
  }
  
  private void readActivityStateI(Context paramContext)
  {
    try
    {
      this.activityState = ((ActivityState)Util.readObject(paramContext, "AdjustIoActivityState", "Activity state", ActivityState.class));
      return;
    }
    catch (Exception paramContext)
    {
      this.logger.error("Failed to read %s file (%s)", new Object[] { "Activity state", paramContext.getMessage() });
      this.activityState = null;
    }
  }
  
  private void readAttributionI(Context paramContext)
  {
    try
    {
      this.attribution = ((AdjustAttribution)Util.readObject(paramContext, "AdjustAttribution", "Attribution", AdjustAttribution.class));
      return;
    }
    catch (Exception paramContext)
    {
      this.logger.error("Failed to read %s file (%s)", new Object[] { "Attribution", paramContext.getMessage() });
      this.attribution = null;
    }
  }
  
  private void readConfigFile(Context paramContext)
  {
    try
    {
      paramContext = paramContext.getAssets().open("adjust_config.properties");
      Properties localProperties = new Properties();
      localProperties.load(paramContext);
      this.logger.verbose("adjust_config.properties file read and loaded", new Object[0]);
      paramContext = localProperties.getProperty("defaultTracker");
      if (paramContext != null) {
        this.adjustConfig.defaultTracker = paramContext;
      }
      return;
    }
    catch (Exception paramContext)
    {
      this.logger.debug("%s file not found in this app", new Object[] { paramContext.getMessage() });
    }
  }
  
  private void readOpenUrlI(Uri paramUri, long paramLong)
  {
    if (paramUri == null) {}
    Object localObject;
    do
    {
      do
      {
        return;
        localObject = paramUri.toString();
      } while ((localObject == null) || (((String)localObject).length() == 0));
      this.logger.verbose("Url to parse (%s)", new Object[] { paramUri });
      UrlQuerySanitizer localUrlQuerySanitizer = new UrlQuerySanitizer();
      localUrlQuerySanitizer.setUnregisteredParameterValueSanitizer(UrlQuerySanitizer.getAllButNulLegal());
      localUrlQuerySanitizer.setAllowUnregisteredParamaters(true);
      localUrlQuerySanitizer.parseUrl((String)localObject);
      localObject = queryStringClickPackageBuilderI(localUrlQuerySanitizer.getParameterList());
    } while (localObject == null);
    ((PackageBuilder)localObject).deeplink = paramUri.toString();
    ((PackageBuilder)localObject).clickTime = paramLong;
    paramUri = ((PackageBuilder)localObject).buildClickPackage("deeplink", this.sessionParameters);
    this.sdkClickHandler.sendSdkClick(paramUri);
  }
  
  private boolean readQueryStringI(String paramString1, String paramString2, Map<String, String> paramMap, AdjustAttribution paramAdjustAttribution)
  {
    if ((paramString1 == null) || (paramString2 == null)) {}
    do
    {
      do
      {
        return false;
      } while (!paramString1.startsWith("adjust_"));
      paramString1 = paramString1.substring("adjust_".length());
    } while ((paramString1.length() == 0) || (paramString2.length() == 0));
    if (!trySetAttributionI(paramAdjustAttribution, paramString1, paramString2)) {
      paramMap.put(paramString1, paramString2);
    }
    return true;
  }
  
  private void readSessionCallbackParametersI(Context paramContext)
  {
    try
    {
      this.sessionParameters.callbackParameters = ((Map)Util.readObject(paramContext, "AdjustSessionCallbackParameters", "Session Callback parameters", Map.class));
      return;
    }
    catch (Exception paramContext)
    {
      this.logger.error("Failed to read %s file (%s)", new Object[] { "Session Callback parameters", paramContext.getMessage() });
      this.sessionParameters.callbackParameters = null;
    }
  }
  
  private void readSessionPartnerParametersI(Context paramContext)
  {
    try
    {
      this.sessionParameters.partnerParameters = ((Map)Util.readObject(paramContext, "AdjustSessionPartnerParameters", "Session Partner parameters", Map.class));
      return;
    }
    catch (Exception paramContext)
    {
      this.logger.error("Failed to read %s file (%s)", new Object[] { "Session Partner parameters", paramContext.getMessage() });
      this.sessionParameters.partnerParameters = null;
    }
  }
  
  private void resumeSendingI()
  {
    this.attributionHandler.resumeSending();
    this.packageHandler.resumeSending();
    this.sdkClickHandler.resumeSending();
  }
  
  private void sendFirstPackagesI()
  {
    if (this.internalState.isToStartNow())
    {
      this.logger.info("Start delay expired or never configured", new Object[0]);
      return;
    }
    updatePackagesI();
    this.internalState.delayStart = false;
    this.delayStartTimer.cancel();
    this.delayStartTimer = null;
    updateHandlersStatusAndSendI();
  }
  
  private void sendReferrerI(String paramString, long paramLong)
  {
    if ((paramString == null) || (paramString.length() == 0)) {}
    Object localObject;
    do
    {
      return;
      this.logger.verbose("Referrer to parse (%s)", new Object[] { paramString });
      localObject = new UrlQuerySanitizer();
      ((UrlQuerySanitizer)localObject).setUnregisteredParameterValueSanitizer(UrlQuerySanitizer.getAllButNulLegal());
      ((UrlQuerySanitizer)localObject).setAllowUnregisteredParamaters(true);
      ((UrlQuerySanitizer)localObject).parseQuery(paramString);
      localObject = queryStringClickPackageBuilderI(((UrlQuerySanitizer)localObject).getParameterList());
    } while (localObject == null);
    ((PackageBuilder)localObject).referrer = paramString;
    ((PackageBuilder)localObject).clickTime = paramLong;
    paramString = ((PackageBuilder)localObject).buildClickPackage("reftag", this.sessionParameters);
    this.sdkClickHandler.sendSdkClick(paramString);
  }
  
  private void sessionParametersActionsI(List<IRunActivityHandler> paramList)
  {
    if (paramList == null) {}
    for (;;)
    {
      return;
      paramList = paramList.iterator();
      while (paramList.hasNext()) {
        ((IRunActivityHandler)paramList.next()).run(this);
      }
    }
  }
  
  private void setPushTokenI(String paramString)
  {
    if (paramString == null) {}
    while (paramString.equals(this.activityState.pushToken)) {
      return;
    }
    this.activityState.pushToken = paramString;
    writeActivityStateI();
    long l = System.currentTimeMillis();
    paramString = new PackageBuilder(this.adjustConfig, this.deviceInfo, this.activityState, l).buildInfoPackage("push");
    this.packageHandler.addPackage(paramString);
    this.packageHandler.sendFirstPackage();
  }
  
  private void startBackgroundTimerI()
  {
    if (this.backgroundTimer == null) {}
    while ((!toSendI()) || (this.backgroundTimer.getFireIn() > 0L)) {
      return;
    }
    this.backgroundTimer.startIn(BACKGROUND_TIMER_INTERVAL);
  }
  
  private void startForegroundTimerI()
  {
    if (!isEnabledI()) {
      return;
    }
    this.foregroundTimer.start();
  }
  
  private void startI()
  {
    if ((this.activityState != null) && (!this.activityState.enabled)) {
      return;
    }
    updateHandlersStatusAndSendI();
    processSessionI();
    checkAttributionStateI();
  }
  
  private void stopBackgroundTimerI()
  {
    if (this.backgroundTimer == null) {
      return;
    }
    this.backgroundTimer.cancel();
  }
  
  private void stopForegroundTimerI()
  {
    this.foregroundTimer.suspend();
  }
  
  private void teardownActivityStateS(boolean paramBoolean)
  {
    try
    {
      if (this.activityState == null) {
        return;
      }
      if ((paramBoolean) && (this.adjustConfig != null) && (this.adjustConfig.context != null)) {
        deleteActivityState(this.adjustConfig.context);
      }
      this.activityState = null;
      return;
    }
    finally {}
  }
  
  private void teardownAllSessionParametersS(boolean paramBoolean)
  {
    try
    {
      if (this.sessionParameters == null) {
        return;
      }
      if ((paramBoolean) && (this.adjustConfig != null) && (this.adjustConfig.context != null))
      {
        deleteSessionCallbackParameters(this.adjustConfig.context);
        deleteSessionPartnerParameters(this.adjustConfig.context);
      }
      this.sessionParameters = null;
      return;
    }
    finally {}
  }
  
  private void teardownAttributionS(boolean paramBoolean)
  {
    try
    {
      if (this.attribution == null) {
        return;
      }
      if ((paramBoolean) && (this.adjustConfig != null) && (this.adjustConfig.context != null)) {
        deleteAttribution(this.adjustConfig.context);
      }
      this.attribution = null;
      return;
    }
    finally {}
  }
  
  private boolean toSendI()
  {
    return toSendI(false);
  }
  
  private boolean toSendI(boolean paramBoolean)
  {
    if (pausedI(paramBoolean)) {
      return false;
    }
    if (this.adjustConfig.sendInBackground) {
      return true;
    }
    return this.internalState.isForeground();
  }
  
  private void trackEventI(AdjustEvent paramAdjustEvent)
  {
    if (!checkActivityStateI(this.activityState)) {}
    while ((!isEnabledI()) || (!checkEventI(paramAdjustEvent)) || (!checkOrderIdI(paramAdjustEvent.orderId))) {
      return;
    }
    long l = System.currentTimeMillis();
    ActivityState localActivityState = this.activityState;
    localActivityState.eventCount += 1;
    updateActivityStateI(l);
    paramAdjustEvent = new PackageBuilder(this.adjustConfig, this.deviceInfo, this.activityState, l).buildEventPackage(paramAdjustEvent, this.sessionParameters, this.internalState.isDelayStart());
    this.packageHandler.addPackage(paramAdjustEvent);
    if (this.adjustConfig.eventBufferingEnabled) {
      this.logger.info("Buffered event %s", new Object[] { paramAdjustEvent.getSuffix() });
    }
    for (;;)
    {
      if ((this.adjustConfig.sendInBackground) && (this.internalState.isBackground())) {
        startBackgroundTimerI();
      }
      writeActivityStateI();
      return;
      this.packageHandler.sendFirstPackage();
    }
  }
  
  private void transferSessionPackageI(long paramLong)
  {
    ActivityPackage localActivityPackage = new PackageBuilder(this.adjustConfig, this.deviceInfo, this.activityState, paramLong).buildSessionPackage(this.sessionParameters, this.internalState.isDelayStart());
    this.packageHandler.addPackage(localActivityPackage);
    this.packageHandler.sendFirstPackage();
  }
  
  private boolean trySetAttributionI(AdjustAttribution paramAdjustAttribution, String paramString1, String paramString2)
  {
    if (paramString1.equals("tracker"))
    {
      paramAdjustAttribution.trackerName = paramString2;
      return true;
    }
    if (paramString1.equals("campaign"))
    {
      paramAdjustAttribution.campaign = paramString2;
      return true;
    }
    if (paramString1.equals("adgroup"))
    {
      paramAdjustAttribution.adgroup = paramString2;
      return true;
    }
    if (paramString1.equals("creative"))
    {
      paramAdjustAttribution.creative = paramString2;
      return true;
    }
    return false;
  }
  
  private boolean updateActivityStateI(long paramLong)
  {
    if (!checkActivityStateI(this.activityState)) {}
    long l;
    do
    {
      return false;
      l = paramLong - this.activityState.lastActivity;
    } while (l > SESSION_INTERVAL);
    this.activityState.lastActivity = paramLong;
    if (l < 0L) {
      this.logger.error("Time travel!", new Object[0]);
    }
    for (;;)
    {
      return true;
      ActivityState localActivityState = this.activityState;
      localActivityState.sessionLength += l;
      localActivityState = this.activityState;
      localActivityState.timeSpent += l;
    }
  }
  
  private void updateAdidI(String paramString)
  {
    if (paramString == null) {}
    while (paramString.equals(this.activityState.adid)) {
      return;
    }
    this.activityState.adid = paramString;
    writeActivityStateI();
  }
  
  private void updateHandlersStatusAndSend()
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.updateHandlersStatusAndSendI();
      }
    });
  }
  
  private void updateHandlersStatusAndSendI()
  {
    if (!toSendI()) {
      pauseSendingI();
    }
    do
    {
      return;
      resumeSendingI();
    } while (this.adjustConfig.eventBufferingEnabled);
    this.packageHandler.sendFirstPackage();
  }
  
  private void updatePackagesI()
  {
    this.packageHandler.updatePackages(this.sessionParameters);
    this.internalState.updatePackages = false;
    if (this.activityState != null)
    {
      this.activityState.updatePackages = false;
      writeActivityStateI();
    }
  }
  
  private void updateStatus(boolean paramBoolean, String paramString1, String paramString2, String paramString3)
  {
    if (paramBoolean) {
      this.logger.info(paramString1, new Object[0]);
    }
    for (;;)
    {
      updateHandlersStatusAndSend();
      return;
      if (pausedI(false))
      {
        if (pausedI(true)) {
          this.logger.info(paramString2, new Object[0]);
        } else {
          this.logger.info(paramString2 + ", except the Sdk Click Handler", new Object[0]);
        }
      }
      else {
        this.logger.info(paramString3, new Object[0]);
      }
    }
  }
  
  private void writeActivityStateI()
  {
    writeActivityStateS(null);
  }
  
  private void writeActivityStateS(Runnable paramRunnable)
  {
    try
    {
      if (this.activityState == null) {
        return;
      }
      if (paramRunnable != null) {
        paramRunnable.run();
      }
      Util.writeObject(this.activityState, this.adjustConfig.context, "AdjustIoActivityState", "Activity state");
      return;
    }
    finally {}
  }
  
  private void writeAttributionI()
  {
    try
    {
      if (this.attribution == null) {
        return;
      }
      Util.writeObject(this.attribution, this.adjustConfig.context, "AdjustAttribution", "Attribution");
      return;
    }
    finally {}
  }
  
  private void writeSessionCallbackParametersI()
  {
    try
    {
      if (this.sessionParameters == null) {
        return;
      }
      Util.writeObject(this.sessionParameters.callbackParameters, this.adjustConfig.context, "AdjustSessionCallbackParameters", "Session Callback parameters");
      return;
    }
    finally {}
  }
  
  private void writeSessionPartnerParametersI()
  {
    try
    {
      if (this.sessionParameters == null) {
        return;
      }
      Util.writeObject(this.sessionParameters.partnerParameters, this.adjustConfig.context, "AdjustSessionPartnerParameters", "Session Partner parameters");
      return;
    }
    finally {}
  }
  
  public void addSessionCallbackParameter(final String paramString1, final String paramString2)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.addSessionCallbackParameterI(paramString1, paramString2);
      }
    });
  }
  
  public void addSessionCallbackParameterI(String paramString1, String paramString2)
  {
    if (!Util.isValidParameter(paramString1, "key", "Session Callback")) {}
    while (!Util.isValidParameter(paramString2, "value", "Session Callback")) {
      return;
    }
    if (this.sessionParameters.callbackParameters == null) {
      this.sessionParameters.callbackParameters = new LinkedHashMap();
    }
    String str = (String)this.sessionParameters.callbackParameters.get(paramString1);
    if (paramString2.equals(str))
    {
      this.logger.verbose("Key %s already present with the same value", new Object[] { paramString1 });
      return;
    }
    if (str != null) {
      this.logger.warn("Key %s will be overwritten", new Object[] { paramString1 });
    }
    this.sessionParameters.callbackParameters.put(paramString1, paramString2);
    writeSessionCallbackParametersI();
  }
  
  public void addSessionPartnerParameter(final String paramString1, final String paramString2)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.addSessionPartnerParameterI(paramString1, paramString2);
      }
    });
  }
  
  public void addSessionPartnerParameterI(String paramString1, String paramString2)
  {
    if (!Util.isValidParameter(paramString1, "key", "Session Partner")) {}
    while (!Util.isValidParameter(paramString2, "value", "Session Partner")) {
      return;
    }
    if (this.sessionParameters.partnerParameters == null) {
      this.sessionParameters.partnerParameters = new LinkedHashMap();
    }
    String str = (String)this.sessionParameters.partnerParameters.get(paramString1);
    if (paramString2.equals(str))
    {
      this.logger.verbose("Key %s already present with the same value", new Object[] { paramString1 });
      return;
    }
    if (str != null) {
      this.logger.warn("Key %s will be overwritten", new Object[] { paramString1 });
    }
    this.sessionParameters.partnerParameters.put(paramString1, paramString2);
    writeSessionPartnerParametersI();
  }
  
  public void backgroundTimerFired()
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.backgroundTimerFiredI();
      }
    });
  }
  
  public void finishedTrackingActivity(ResponseData paramResponseData)
  {
    if ((paramResponseData instanceof SessionResponseData)) {
      this.attributionHandler.checkSessionResponse((SessionResponseData)paramResponseData);
    }
    do
    {
      return;
      if ((paramResponseData instanceof SdkClickResponseData))
      {
        this.attributionHandler.checkSdkClickResponse((SdkClickResponseData)paramResponseData);
        return;
      }
    } while (!(paramResponseData instanceof EventResponseData));
    launchEventResponseTasks((EventResponseData)paramResponseData);
  }
  
  public void foregroundTimerFired()
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.foregroundTimerFiredI();
      }
    });
  }
  
  public String getAdid()
  {
    if (this.activityState == null) {
      return null;
    }
    return this.activityState.adid;
  }
  
  public AdjustAttribution getAttribution()
  {
    return this.attribution;
  }
  
  public ActivityPackage getAttributionPackageI()
  {
    long l = System.currentTimeMillis();
    return new PackageBuilder(this.adjustConfig, this.deviceInfo, this.activityState, l).buildAttributionPackage();
  }
  
  public InternalState getInternalState()
  {
    return this.internalState;
  }
  
  public void init(AdjustConfig paramAdjustConfig)
  {
    this.adjustConfig = paramAdjustConfig;
  }
  
  public boolean isEnabled()
  {
    return isEnabledI();
  }
  
  public void launchAttributionResponseTasks(final AttributionResponseData paramAttributionResponseData)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.launchAttributionResponseTasksI(paramAttributionResponseData);
      }
    });
  }
  
  public void launchEventResponseTasks(final EventResponseData paramEventResponseData)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.launchEventResponseTasksI(paramEventResponseData);
      }
    });
  }
  
  public void launchSdkClickResponseTasks(final SdkClickResponseData paramSdkClickResponseData)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.launchSdkClickResponseTasksI(paramSdkClickResponseData);
      }
    });
  }
  
  public void launchSessionResponseTasks(final SessionResponseData paramSessionResponseData)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.launchSessionResponseTasksI(paramSessionResponseData);
      }
    });
  }
  
  public void onPause()
  {
    this.internalState.background = true;
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.stopForegroundTimerI();
        ActivityHandler.this.startBackgroundTimerI();
        ActivityHandler.this.logger.verbose("Subsession end", new Object[0]);
        ActivityHandler.this.endI();
      }
    });
  }
  
  public void onResume()
  {
    this.internalState.background = false;
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.delayStartI();
        ActivityHandler.this.stopBackgroundTimerI();
        ActivityHandler.this.startForegroundTimerI();
        ActivityHandler.this.logger.verbose("Subsession start", new Object[0]);
        ActivityHandler.this.startI();
      }
    });
  }
  
  public void readOpenUrl(final Uri paramUri, final long paramLong)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.readOpenUrlI(paramUri, paramLong);
      }
    });
  }
  
  public void removeSessionCallbackParameter(final String paramString)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.removeSessionCallbackParameterI(paramString);
      }
    });
  }
  
  public void removeSessionCallbackParameterI(String paramString)
  {
    if (!Util.isValidParameter(paramString, "key", "Session Callback")) {
      return;
    }
    if (this.sessionParameters.callbackParameters == null)
    {
      this.logger.warn("Session Callback parameters are not set", new Object[0]);
      return;
    }
    if ((String)this.sessionParameters.callbackParameters.remove(paramString) == null)
    {
      this.logger.warn("Key %s does not exist", new Object[] { paramString });
      return;
    }
    this.logger.debug("Key %s will be removed", new Object[] { paramString });
    writeSessionCallbackParametersI();
  }
  
  public void removeSessionPartnerParameter(final String paramString)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.removeSessionPartnerParameterI(paramString);
      }
    });
  }
  
  public void removeSessionPartnerParameterI(String paramString)
  {
    if (!Util.isValidParameter(paramString, "key", "Session Partner")) {
      return;
    }
    if (this.sessionParameters.partnerParameters == null)
    {
      this.logger.warn("Session Partner parameters are not set", new Object[0]);
      return;
    }
    if ((String)this.sessionParameters.partnerParameters.remove(paramString) == null)
    {
      this.logger.warn("Key %s does not exist", new Object[] { paramString });
      return;
    }
    this.logger.debug("Key %s will be removed", new Object[] { paramString });
    writeSessionPartnerParametersI();
  }
  
  public void resetSessionCallbackParameters()
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.resetSessionCallbackParametersI();
      }
    });
  }
  
  public void resetSessionCallbackParametersI()
  {
    if (this.sessionParameters.callbackParameters == null) {
      this.logger.warn("Session Callback parameters are not set", new Object[0]);
    }
    this.sessionParameters.callbackParameters = null;
    writeSessionCallbackParametersI();
  }
  
  public void resetSessionPartnerParameters()
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.resetSessionPartnerParametersI();
      }
    });
  }
  
  public void resetSessionPartnerParametersI()
  {
    if (this.sessionParameters.partnerParameters == null) {
      this.logger.warn("Session Partner parameters are not set", new Object[0]);
    }
    this.sessionParameters.partnerParameters = null;
    writeSessionPartnerParametersI();
  }
  
  public void sendFirstPackages()
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.sendFirstPackagesI();
      }
    });
  }
  
  public void sendReferrer(final String paramString, final long paramLong)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.sendReferrerI(paramString, paramLong);
      }
    });
  }
  
  public void setAskingAttribution(final boolean paramBoolean)
  {
    writeActivityStateS(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.activityState.askingAttribution = paramBoolean;
      }
    });
  }
  
  public void setEnabled(final boolean paramBoolean)
  {
    boolean bool2 = true;
    boolean bool1 = true;
    if (!hasChangedState(isEnabled(), paramBoolean, "Adjust already enabled", "Adjust already disabled")) {
      return;
    }
    this.internalState.enabled = paramBoolean;
    if (this.activityState == null)
    {
      if (!paramBoolean) {}
      for (paramBoolean = bool1;; paramBoolean = false)
      {
        updateStatus(paramBoolean, "Handlers will start as paused due to the SDK being disabled", "Handlers will still start as paused", "Handlers will start as active due to the SDK being enabled");
        return;
      }
    }
    writeActivityStateS(new Runnable()
    {
      public void run()
      {
        ActivityHandler.this.activityState.enabled = paramBoolean;
      }
    });
    if (!paramBoolean) {}
    for (paramBoolean = bool2;; paramBoolean = false)
    {
      updateStatus(paramBoolean, "Pausing handlers due to SDK being disabled", "Handlers remain paused", "Resuming handlers due to SDK being enabled");
      return;
    }
  }
  
  public void setOfflineMode(boolean paramBoolean)
  {
    if (!hasChangedState(this.internalState.isOffline(), paramBoolean, "Adjust already in offline mode", "Adjust already in online mode")) {
      return;
    }
    this.internalState.offline = paramBoolean;
    if (this.activityState == null)
    {
      updateStatus(paramBoolean, "Handlers will start paused due to SDK being offline", "Handlers will still start as paused", "Handlers will start as active due to SDK being online");
      return;
    }
    updateStatus(paramBoolean, "Pausing handlers to put SDK offline mode", "Handlers remain paused", "Resuming handlers to put SDK in online mode");
  }
  
  public void setPushToken(final String paramString)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        if (ActivityHandler.this.activityState == null) {
          ActivityHandler.this.startI();
        }
        ActivityHandler.this.setPushTokenI(paramString);
      }
    });
  }
  
  public void teardown(boolean paramBoolean)
  {
    if (this.backgroundTimer != null) {
      this.backgroundTimer.teardown();
    }
    if (this.foregroundTimer != null) {
      this.foregroundTimer.teardown();
    }
    if (this.delayStartTimer != null) {
      this.delayStartTimer.teardown();
    }
    if (this.scheduledExecutor != null) {}
    try
    {
      this.scheduledExecutor.shutdownNow();
      if (this.packageHandler != null) {
        this.packageHandler.teardown(paramBoolean);
      }
      if (this.attributionHandler != null) {
        this.attributionHandler.teardown();
      }
      if (this.sdkClickHandler != null) {
        this.sdkClickHandler.teardown();
      }
      if (this.sessionParameters != null)
      {
        if (this.sessionParameters.callbackParameters != null) {
          this.sessionParameters.callbackParameters.clear();
        }
        if (this.sessionParameters.partnerParameters != null) {
          this.sessionParameters.partnerParameters.clear();
        }
      }
      teardownActivityStateS(paramBoolean);
      teardownAttributionS(paramBoolean);
      teardownAllSessionParametersS(paramBoolean);
      this.packageHandler = null;
      this.logger = null;
      this.foregroundTimer = null;
      this.scheduledExecutor = null;
      this.backgroundTimer = null;
      this.delayStartTimer = null;
      this.internalState = null;
      this.deviceInfo = null;
      this.adjustConfig = null;
      this.attributionHandler = null;
      this.sdkClickHandler = null;
      this.sessionParameters = null;
      return;
    }
    catch (SecurityException localSecurityException)
    {
      for (;;) {}
    }
  }
  
  public void trackEvent(final AdjustEvent paramAdjustEvent)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        if (ActivityHandler.this.activityState == null)
        {
          ActivityHandler.this.logger.warn("Event tracked before first activity resumed.\nIf it was triggered in the Application class, it might timestamp or even send an install long before the user opens the app.\nPlease check https://github.com/adjust/android_sdk#can-i-trigger-an-event-at-application-launch for more information.", new Object[0]);
          ActivityHandler.this.startI();
        }
        ActivityHandler.this.trackEventI(paramAdjustEvent);
      }
    });
  }
  
  public boolean updateAttributionI(AdjustAttribution paramAdjustAttribution)
  {
    if (paramAdjustAttribution == null) {}
    while (paramAdjustAttribution.equals(this.attribution)) {
      return false;
    }
    this.attribution = paramAdjustAttribution;
    writeAttributionI();
    return true;
  }
  
  public class InternalState
  {
    boolean background;
    boolean delayStart;
    boolean enabled;
    boolean firstLaunch;
    boolean offline;
    boolean sessionResponseProcessed;
    boolean updatePackages;
    
    public InternalState() {}
    
    public boolean isBackground()
    {
      return this.background;
    }
    
    public boolean isDelayStart()
    {
      return this.delayStart;
    }
    
    public boolean isDisabled()
    {
      return !this.enabled;
    }
    
    public boolean isEnabled()
    {
      return this.enabled;
    }
    
    public boolean isFirstLaunch()
    {
      return this.firstLaunch;
    }
    
    public boolean isForeground()
    {
      return !this.background;
    }
    
    public boolean isOffline()
    {
      return this.offline;
    }
    
    public boolean isOnline()
    {
      return !this.offline;
    }
    
    public boolean isSessionResponseProcessed()
    {
      return this.sessionResponseProcessed;
    }
    
    public boolean isToStartNow()
    {
      return !this.delayStart;
    }
    
    public boolean isToUpdatePackages()
    {
      return this.updatePackages;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\ActivityHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */