package com.adjust.sdk;

public class Adjust2dxAttributionCallback
  implements OnAttributionChangedListener
{
  public native void attributionChanged(Object paramObject);
  
  public void onAttributionChanged(AdjustAttribution paramAdjustAttribution)
  {
    attributionChanged(paramAdjustAttribution);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Adjust2dxAttributionCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */