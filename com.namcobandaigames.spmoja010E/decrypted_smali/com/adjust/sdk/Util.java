package com.adjust.sdk;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Looper;
import android.provider.Settings.Secure;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.NotSerializableException;
import java.io.ObjectInputStream.GetField;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util
{
  private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'Z";
  public static final DecimalFormat SecondsDisplayFormat = new DecimalFormat("0.0");
  public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'Z", Locale.US);
  private static final String fieldReadErrorMessage = "Unable to read '%s' field in migration device with message (%s)";
  
  public static boolean checkPermission(Context paramContext, String paramString)
  {
    return paramContext.checkCallingOrSelfPermission(paramString) == 0;
  }
  
  public static String convertToHex(byte[] paramArrayOfByte)
  {
    BigInteger localBigInteger = new BigInteger(1, paramArrayOfByte);
    paramArrayOfByte = "%0" + (paramArrayOfByte.length << 1) + "x";
    return String.format(Locale.US, paramArrayOfByte, new Object[] { localBigInteger });
  }
  
  protected static String createUuid()
  {
    return UUID.randomUUID().toString();
  }
  
  public static boolean equalBoolean(Boolean paramBoolean1, Boolean paramBoolean2)
  {
    return equalObject(paramBoolean1, paramBoolean2);
  }
  
  public static boolean equalEnum(Enum paramEnum1, Enum paramEnum2)
  {
    return equalObject(paramEnum1, paramEnum2);
  }
  
  public static boolean equalInt(Integer paramInteger1, Integer paramInteger2)
  {
    return equalObject(paramInteger1, paramInteger2);
  }
  
  public static boolean equalLong(Long paramLong1, Long paramLong2)
  {
    return equalObject(paramLong1, paramLong2);
  }
  
  public static boolean equalObject(Object paramObject1, Object paramObject2)
  {
    if ((paramObject1 == null) || (paramObject2 == null)) {
      return (paramObject1 == null) && (paramObject2 == null);
    }
    return paramObject1.equals(paramObject2);
  }
  
  public static boolean equalString(String paramString1, String paramString2)
  {
    return equalObject(paramString1, paramString2);
  }
  
  public static boolean equalsDouble(Double paramDouble1, Double paramDouble2)
  {
    if ((paramDouble1 == null) || (paramDouble2 == null)) {
      if ((paramDouble1 != null) || (paramDouble2 != null)) {}
    }
    while (Double.doubleToLongBits(paramDouble1.doubleValue()) == Double.doubleToLongBits(paramDouble2.doubleValue()))
    {
      return true;
      return false;
    }
    return false;
  }
  
  public static String getAndroidId(Context paramContext)
  {
    return Reflection.getAndroidId(paramContext);
  }
  
  public static String getCpuAbi()
  {
    return Reflection.getCpuAbi();
  }
  
  public static String getFireAdvertisingId(ContentResolver paramContentResolver)
  {
    if (paramContentResolver == null) {
      return null;
    }
    try
    {
      paramContentResolver = Settings.Secure.getString(paramContentResolver, "advertising_id");
      return paramContentResolver;
    }
    catch (Exception paramContentResolver) {}
    return null;
  }
  
  public static Boolean getFireTrackingEnabled(ContentResolver paramContentResolver)
  {
    try
    {
      if (Settings.Secure.getInt(paramContentResolver, "limit_ad_tracking") == 0) {}
      for (boolean bool = true;; bool = false) {
        return Boolean.valueOf(bool);
      }
      return null;
    }
    catch (Exception paramContentResolver) {}
  }
  
  public static void getGoogleAdId(Context paramContext, OnDeviceIdsRead paramOnDeviceIdsRead)
  {
    ILogger localILogger = AdjustFactory.getLogger();
    if (Looper.myLooper() != Looper.getMainLooper())
    {
      localILogger.debug("GoogleAdId being read in the background", new Object[0]);
      paramContext = getPlayAdId(paramContext);
      localILogger.debug("GoogleAdId read " + paramContext, new Object[0]);
      paramOnDeviceIdsRead.onGoogleAdIdRead(paramContext);
      return;
    }
    localILogger.debug("GoogleAdId being read in the foreground", new Object[0]);
    new AsyncTask()
    {
      protected String doInBackground(Context... paramAnonymousVarArgs)
      {
        ILogger localILogger = AdjustFactory.getLogger();
        paramAnonymousVarArgs = Util.getPlayAdId(paramAnonymousVarArgs[0]);
        localILogger.debug("GoogleAdId read " + paramAnonymousVarArgs, new Object[0]);
        return paramAnonymousVarArgs;
      }
      
      protected void onPostExecute(String paramAnonymousString)
      {
        AdjustFactory.getLogger();
        this.val$onDeviceIdRead.onGoogleAdIdRead(paramAnonymousString);
      }
    }.execute(new Context[] { paramContext });
  }
  
  public static Locale getLocale(Configuration paramConfiguration)
  {
    Locale localLocale = Reflection.getLocaleFromLocaleList(paramConfiguration);
    if (localLocale != null) {
      return localLocale;
    }
    return Reflection.getLocaleFromField(paramConfiguration);
  }
  
  private static ILogger getLogger()
  {
    return AdjustFactory.getLogger();
  }
  
  public static String getMacAddress(Context paramContext)
  {
    return Reflection.getMacAddress(paramContext);
  }
  
  public static String getPlayAdId(Context paramContext)
  {
    return Reflection.getPlayAdId(paramContext);
  }
  
  public static Map<String, String> getPluginKeys(Context paramContext)
  {
    return Reflection.getPluginKeys(paramContext);
  }
  
  public static String getReasonString(String paramString, Throwable paramThrowable)
  {
    if (paramThrowable != null) {
      return String.format(Locale.US, "%s: %s", new Object[] { paramString, paramThrowable });
    }
    return String.format(Locale.US, "%s", new Object[] { paramString });
  }
  
  public static String[] getSupportedAbis()
  {
    return Reflection.getSupportedAbis();
  }
  
  public static String getVmInstructionSet()
  {
    return Reflection.getVmInstructionSet();
  }
  
  public static long getWaitingTime(int paramInt, BackoffStrategy paramBackoffStrategy)
  {
    if (paramInt < paramBackoffStrategy.minRetries) {
      return 0L;
    }
    long l = Math.min(Math.pow(2.0D, paramInt - paramBackoffStrategy.minRetries) * paramBackoffStrategy.milliSecondMultiplier, paramBackoffStrategy.maxWait);
    double d = randomInRange(paramBackoffStrategy.minRange, paramBackoffStrategy.maxRange);
    return (l * d);
  }
  
  public static String hash(String paramString1, String paramString2)
  {
    try
    {
      paramString1 = paramString1.getBytes("UTF-8");
      paramString2 = MessageDigest.getInstance(paramString2);
      paramString2.update(paramString1, 0, paramString1.length);
      paramString1 = convertToHex(paramString2.digest());
      return paramString1;
    }
    catch (Exception paramString1) {}
    return null;
  }
  
  public static int hashBoolean(Boolean paramBoolean)
  {
    if (paramBoolean == null) {
      return 0;
    }
    return paramBoolean.hashCode();
  }
  
  public static int hashEnum(Enum paramEnum)
  {
    if (paramEnum == null) {
      return 0;
    }
    return paramEnum.hashCode();
  }
  
  public static int hashLong(Long paramLong)
  {
    if (paramLong == null) {
      return 0;
    }
    return paramLong.hashCode();
  }
  
  public static int hashObject(Object paramObject)
  {
    if (paramObject == null) {
      return 0;
    }
    return paramObject.hashCode();
  }
  
  public static int hashString(String paramString)
  {
    if (paramString == null) {
      return 0;
    }
    return paramString.hashCode();
  }
  
  public static Boolean isPlayTrackingEnabled(Context paramContext)
  {
    return Reflection.isPlayTrackingEnabled(paramContext);
  }
  
  public static boolean isValidParameter(String paramString1, String paramString2, String paramString3)
  {
    if (paramString1 == null)
    {
      getLogger().error("%s parameter %s is missing", new Object[] { paramString3, paramString2 });
      return false;
    }
    if (paramString1.equals(""))
    {
      getLogger().error("%s parameter %s is empty", new Object[] { paramString3, paramString2 });
      return false;
    }
    return true;
  }
  
  public static String md5(String paramString)
  {
    return hash(paramString, "MD5");
  }
  
  public static Map<String, String> mergeParameters(Map<String, String> paramMap1, Map<String, String> paramMap2, String paramString)
  {
    if (paramMap1 == null) {
      return paramMap2;
    }
    if (paramMap2 == null) {
      return paramMap1;
    }
    paramMap1 = new HashMap(paramMap1);
    ILogger localILogger = getLogger();
    paramMap2 = paramMap2.entrySet().iterator();
    while (paramMap2.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap2.next();
      String str = (String)paramMap1.put(localEntry.getKey(), localEntry.getValue());
      if (str != null) {
        localILogger.warn("Key %s with value %s from %s parameter was replaced by value %s", new Object[] { localEntry.getKey(), str, paramString, localEntry.getValue() });
      }
    }
    return paramMap1;
  }
  
  public static String quote(String paramString)
  {
    String str;
    if (paramString == null) {
      str = null;
    }
    do
    {
      return str;
      str = paramString;
    } while (!Pattern.compile("\\s").matcher(paramString).find());
    return String.format(Locale.US, "'%s'", new Object[] { paramString });
  }
  
  private static double randomInRange(double paramDouble1, double paramDouble2)
  {
    return new Random().nextDouble() * (paramDouble2 - paramDouble1) + paramDouble1;
  }
  
  public static boolean readBooleanField(ObjectInputStream.GetField paramGetField, String paramString, boolean paramBoolean)
  {
    try
    {
      boolean bool = paramGetField.get(paramString, paramBoolean);
      return bool;
    }
    catch (Exception paramGetField)
    {
      getLogger().debug("Unable to read '%s' field in migration device with message (%s)", new Object[] { paramString, paramGetField.getMessage() });
    }
    return paramBoolean;
  }
  
  public static int readIntField(ObjectInputStream.GetField paramGetField, String paramString, int paramInt)
  {
    try
    {
      int i = paramGetField.get(paramString, paramInt);
      return i;
    }
    catch (Exception paramGetField)
    {
      getLogger().debug("Unable to read '%s' field in migration device with message (%s)", new Object[] { paramString, paramGetField.getMessage() });
    }
    return paramInt;
  }
  
  public static long readLongField(ObjectInputStream.GetField paramGetField, String paramString, long paramLong)
  {
    try
    {
      long l = paramGetField.get(paramString, paramLong);
      return l;
    }
    catch (Exception paramGetField)
    {
      getLogger().debug("Unable to read '%s' field in migration device with message (%s)", new Object[] { paramString, paramGetField.getMessage() });
    }
    return paramLong;
  }
  
  /* Error */
  public static <T> T readObject(Context paramContext, String paramString1, String paramString2, Class<T> paramClass)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 7
    //   3: aconst_null
    //   4: astore 5
    //   6: aconst_null
    //   7: astore 10
    //   9: aconst_null
    //   10: astore 11
    //   12: aconst_null
    //   13: astore 9
    //   15: aconst_null
    //   16: astore 8
    //   18: aconst_null
    //   19: astore 12
    //   21: aload 10
    //   23: astore 4
    //   25: aload 9
    //   27: astore 6
    //   29: aload_0
    //   30: aload_1
    //   31: invokevirtual 443	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   34: astore_0
    //   35: aload_0
    //   36: astore 7
    //   38: aload 7
    //   40: astore 5
    //   42: aload 10
    //   44: astore 4
    //   46: aload 9
    //   48: astore 6
    //   50: new 445	java/io/BufferedInputStream
    //   53: dup
    //   54: aload_0
    //   55: invokespecial 448	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   58: astore_0
    //   59: aload_0
    //   60: astore 7
    //   62: aload 7
    //   64: astore 5
    //   66: aload 10
    //   68: astore 4
    //   70: aload 9
    //   72: astore 6
    //   74: new 450	java/io/ObjectInputStream
    //   77: dup
    //   78: aload_0
    //   79: invokespecial 451	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   82: astore 13
    //   84: aload 13
    //   86: astore 9
    //   88: aload 12
    //   90: astore_0
    //   91: aload 9
    //   93: astore 5
    //   95: aload 10
    //   97: astore 4
    //   99: aload 11
    //   101: astore_1
    //   102: aload_3
    //   103: aload 13
    //   105: invokevirtual 453	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   108: invokevirtual 459	java/lang/Class:cast	(Ljava/lang/Object;)Ljava/lang/Object;
    //   111: astore_3
    //   112: aload_3
    //   113: astore_0
    //   114: aload 9
    //   116: astore 5
    //   118: aload_3
    //   119: astore 4
    //   121: aload_3
    //   122: astore_1
    //   123: aload_3
    //   124: astore 8
    //   126: invokestatic 321	com/adjust/sdk/Util:getLogger	()Lcom/adjust/sdk/ILogger;
    //   129: ldc_w 461
    //   132: iconst_2
    //   133: anewarray 4	java/lang/Object
    //   136: dup
    //   137: iconst_0
    //   138: aload_2
    //   139: aastore
    //   140: dup
    //   141: iconst_1
    //   142: aload_3
    //   143: aastore
    //   144: invokeinterface 182 3 0
    //   149: aload_3
    //   150: astore 4
    //   152: aload 9
    //   154: astore 5
    //   156: aload 5
    //   158: ifnull +10 -> 168
    //   161: aload 5
    //   163: invokeinterface 466 1 0
    //   168: aload 4
    //   170: areturn
    //   171: astore_1
    //   172: aload 9
    //   174: astore 5
    //   176: aload_0
    //   177: astore 4
    //   179: aload 9
    //   181: astore 7
    //   183: aload_0
    //   184: astore 6
    //   186: invokestatic 321	com/adjust/sdk/Util:getLogger	()Lcom/adjust/sdk/ILogger;
    //   189: ldc_w 468
    //   192: iconst_2
    //   193: anewarray 4	java/lang/Object
    //   196: dup
    //   197: iconst_0
    //   198: aload_2
    //   199: aastore
    //   200: dup
    //   201: iconst_1
    //   202: aload_1
    //   203: invokevirtual 469	java/lang/ClassNotFoundException:getMessage	()Ljava/lang/String;
    //   206: aastore
    //   207: invokeinterface 326 3 0
    //   212: aload 9
    //   214: astore 5
    //   216: aload_0
    //   217: astore 4
    //   219: goto -63 -> 156
    //   222: astore_0
    //   223: invokestatic 321	com/adjust/sdk/Util:getLogger	()Lcom/adjust/sdk/ILogger;
    //   226: ldc_w 471
    //   229: iconst_1
    //   230: anewarray 4	java/lang/Object
    //   233: dup
    //   234: iconst_0
    //   235: aload_2
    //   236: aastore
    //   237: invokeinterface 182 3 0
    //   242: goto -86 -> 156
    //   245: astore_0
    //   246: aload 9
    //   248: astore 5
    //   250: aload_1
    //   251: astore 4
    //   253: aload 9
    //   255: astore 7
    //   257: aload_1
    //   258: astore 6
    //   260: invokestatic 321	com/adjust/sdk/Util:getLogger	()Lcom/adjust/sdk/ILogger;
    //   263: ldc_w 473
    //   266: iconst_2
    //   267: anewarray 4	java/lang/Object
    //   270: dup
    //   271: iconst_0
    //   272: aload_2
    //   273: aastore
    //   274: dup
    //   275: iconst_1
    //   276: aload_0
    //   277: invokevirtual 474	java/lang/ClassCastException:getMessage	()Ljava/lang/String;
    //   280: aastore
    //   281: invokeinterface 326 3 0
    //   286: aload 9
    //   288: astore 5
    //   290: aload_1
    //   291: astore 4
    //   293: goto -137 -> 156
    //   296: astore_0
    //   297: invokestatic 321	com/adjust/sdk/Util:getLogger	()Lcom/adjust/sdk/ILogger;
    //   300: ldc_w 476
    //   303: iconst_2
    //   304: anewarray 4	java/lang/Object
    //   307: dup
    //   308: iconst_0
    //   309: aload_2
    //   310: aastore
    //   311: dup
    //   312: iconst_1
    //   313: aload_0
    //   314: aastore
    //   315: invokeinterface 326 3 0
    //   320: aload 7
    //   322: astore 5
    //   324: aload 6
    //   326: astore 4
    //   328: goto -172 -> 156
    //   331: astore_0
    //   332: aload 9
    //   334: astore 5
    //   336: aload 8
    //   338: astore 4
    //   340: aload 9
    //   342: astore 7
    //   344: aload 8
    //   346: astore 6
    //   348: invokestatic 321	com/adjust/sdk/Util:getLogger	()Lcom/adjust/sdk/ILogger;
    //   351: ldc_w 478
    //   354: iconst_2
    //   355: anewarray 4	java/lang/Object
    //   358: dup
    //   359: iconst_0
    //   360: aload_2
    //   361: aastore
    //   362: dup
    //   363: iconst_1
    //   364: aload_0
    //   365: invokevirtual 421	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   368: aastore
    //   369: invokeinterface 326 3 0
    //   374: aload 9
    //   376: astore 5
    //   378: aload 8
    //   380: astore 4
    //   382: goto -226 -> 156
    //   385: astore_0
    //   386: invokestatic 321	com/adjust/sdk/Util:getLogger	()Lcom/adjust/sdk/ILogger;
    //   389: ldc_w 480
    //   392: iconst_2
    //   393: anewarray 4	java/lang/Object
    //   396: dup
    //   397: iconst_0
    //   398: aload_2
    //   399: aastore
    //   400: dup
    //   401: iconst_1
    //   402: aload_0
    //   403: aastore
    //   404: invokeinterface 326 3 0
    //   409: aload 4
    //   411: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	412	0	paramContext	Context
    //   0	412	1	paramString1	String
    //   0	412	2	paramString2	String
    //   0	412	3	paramClass	Class<T>
    //   23	387	4	localObject1	Object
    //   4	373	5	localObject2	Object
    //   27	320	6	localObject3	Object
    //   1	342	7	localObject4	Object
    //   16	363	8	localClass	Class<T>
    //   13	362	9	localObject5	Object
    //   7	89	10	localObject6	Object
    //   10	90	11	localObject7	Object
    //   19	70	12	localObject8	Object
    //   82	22	13	localObjectInputStream	java.io.ObjectInputStream
    // Exception table:
    //   from	to	target	type
    //   102	112	171	java/lang/ClassNotFoundException
    //   126	149	171	java/lang/ClassNotFoundException
    //   29	35	222	java/io/FileNotFoundException
    //   50	59	222	java/io/FileNotFoundException
    //   74	84	222	java/io/FileNotFoundException
    //   102	112	222	java/io/FileNotFoundException
    //   126	149	222	java/io/FileNotFoundException
    //   186	212	222	java/io/FileNotFoundException
    //   260	286	222	java/io/FileNotFoundException
    //   348	374	222	java/io/FileNotFoundException
    //   102	112	245	java/lang/ClassCastException
    //   126	149	245	java/lang/ClassCastException
    //   29	35	296	java/lang/Exception
    //   50	59	296	java/lang/Exception
    //   74	84	296	java/lang/Exception
    //   186	212	296	java/lang/Exception
    //   260	286	296	java/lang/Exception
    //   348	374	296	java/lang/Exception
    //   102	112	331	java/lang/Exception
    //   126	149	331	java/lang/Exception
    //   161	168	385	java/lang/Exception
  }
  
  public static <T> T readObjectField(ObjectInputStream.GetField paramGetField, String paramString, T paramT)
  {
    try
    {
      paramGetField = paramGetField.get(paramString, paramT);
      return paramGetField;
    }
    catch (Exception paramGetField)
    {
      getLogger().debug("Unable to read '%s' field in migration device with message (%s)", new Object[] { paramString, paramGetField.getMessage() });
    }
    return paramT;
  }
  
  public static String readStringField(ObjectInputStream.GetField paramGetField, String paramString1, String paramString2)
  {
    return (String)readObjectField(paramGetField, paramString1, paramString2);
  }
  
  public static String sha1(String paramString)
  {
    return hash(paramString, "SHA-1");
  }
  
  public static <T> void writeObject(T paramT, Context paramContext, String paramString1, String paramString2)
  {
    Context localContext = null;
    try
    {
      paramContext = paramContext.openFileOutput(paramString1, 0);
      localContext = paramContext;
      paramContext = new BufferedOutputStream(paramContext);
      localContext = paramContext;
      paramString1 = new ObjectOutputStream(paramContext);
      paramContext = paramString1;
      localContext = paramContext;
    }
    catch (Exception paramT)
    {
      for (;;)
      {
        label70:
        getLogger().error("Failed to open %s for writing (%s)", new Object[] { paramString2, paramT });
        paramContext = localContext;
      }
    }
    try
    {
      paramString1.writeObject(paramT);
      localContext = paramContext;
      getLogger().debug("Wrote %s: %s", new Object[] { paramString2, paramT });
    }
    catch (NotSerializableException paramT)
    {
      try
      {
        paramContext.close();
        return;
      }
      catch (Exception paramT)
      {
        getLogger().error("Failed to close %s file for writing (%s)", new Object[] { paramString2, paramT });
      }
      paramT = paramT;
      localContext = paramContext;
      getLogger().error("Failed to serialize %s", new Object[] { paramString2 });
      break label70;
    }
    if (paramContext != null) {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Util.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */