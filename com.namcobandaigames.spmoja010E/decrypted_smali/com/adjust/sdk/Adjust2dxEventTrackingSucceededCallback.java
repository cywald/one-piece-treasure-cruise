package com.adjust.sdk;

public class Adjust2dxEventTrackingSucceededCallback
  implements OnEventTrackingSucceededListener
{
  public native void eventTrackingSucceeded(Object paramObject);
  
  public void onFinishedEventTrackingSucceeded(AdjustEventSuccess paramAdjustEventSuccess)
  {
    eventTrackingSucceeded(paramAdjustEventSuccess);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Adjust2dxEventTrackingSucceededCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */