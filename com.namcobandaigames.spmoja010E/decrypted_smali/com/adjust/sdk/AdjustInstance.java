package com.adjust.sdk;

import android.net.Uri;
import java.util.ArrayList;
import java.util.List;

public class AdjustInstance
{
  private ActivityHandler activityHandler;
  private String pushToken;
  private String referrer;
  private long referrerClickTime;
  private List<IRunActivityHandler> sessionParametersActionsArray;
  
  private boolean checkActivityHandler()
  {
    if (this.activityHandler == null)
    {
      getLogger().error("Adjust not initialized correctly", new Object[0]);
      return false;
    }
    return true;
  }
  
  private static ILogger getLogger()
  {
    return AdjustFactory.getLogger();
  }
  
  public void addSessionCallbackParameter(final String paramString1, final String paramString2)
  {
    if (this.activityHandler != null)
    {
      this.activityHandler.addSessionCallbackParameter(paramString1, paramString2);
      return;
    }
    if (this.sessionParametersActionsArray == null) {
      this.sessionParametersActionsArray = new ArrayList();
    }
    this.sessionParametersActionsArray.add(new IRunActivityHandler()
    {
      public void run(ActivityHandler paramAnonymousActivityHandler)
      {
        paramAnonymousActivityHandler.addSessionCallbackParameterI(paramString1, paramString2);
      }
    });
  }
  
  public void addSessionPartnerParameter(final String paramString1, final String paramString2)
  {
    if (this.activityHandler != null)
    {
      this.activityHandler.addSessionPartnerParameter(paramString1, paramString2);
      return;
    }
    if (this.sessionParametersActionsArray == null) {
      this.sessionParametersActionsArray = new ArrayList();
    }
    this.sessionParametersActionsArray.add(new IRunActivityHandler()
    {
      public void run(ActivityHandler paramAnonymousActivityHandler)
      {
        paramAnonymousActivityHandler.addSessionPartnerParameterI(paramString1, paramString2);
      }
    });
  }
  
  public void appWillOpenUrl(Uri paramUri)
  {
    if (!checkActivityHandler()) {
      return;
    }
    long l = System.currentTimeMillis();
    this.activityHandler.readOpenUrl(paramUri, l);
  }
  
  public String getAdid()
  {
    if (!checkActivityHandler()) {
      return null;
    }
    return this.activityHandler.getAdid();
  }
  
  public AdjustAttribution getAttribution()
  {
    if (!checkActivityHandler()) {
      return null;
    }
    return this.activityHandler.getAttribution();
  }
  
  public boolean isEnabled()
  {
    if (!checkActivityHandler()) {
      return false;
    }
    return this.activityHandler.isEnabled();
  }
  
  public void onCreate(AdjustConfig paramAdjustConfig)
  {
    if (this.activityHandler != null)
    {
      getLogger().error("Adjust already initialized", new Object[0]);
      return;
    }
    paramAdjustConfig.referrer = this.referrer;
    paramAdjustConfig.referrerClickTime = this.referrerClickTime;
    paramAdjustConfig.sessionParametersActionsArray = this.sessionParametersActionsArray;
    paramAdjustConfig.pushToken = this.pushToken;
    this.activityHandler = ActivityHandler.getInstance(paramAdjustConfig);
  }
  
  public void onPause()
  {
    if (!checkActivityHandler()) {
      return;
    }
    this.activityHandler.onPause();
  }
  
  public void onResume()
  {
    if (!checkActivityHandler()) {
      return;
    }
    this.activityHandler.onResume();
  }
  
  public void removeSessionCallbackParameter(final String paramString)
  {
    if (this.activityHandler != null)
    {
      this.activityHandler.removeSessionCallbackParameter(paramString);
      return;
    }
    if (this.sessionParametersActionsArray == null) {
      this.sessionParametersActionsArray = new ArrayList();
    }
    this.sessionParametersActionsArray.add(new IRunActivityHandler()
    {
      public void run(ActivityHandler paramAnonymousActivityHandler)
      {
        paramAnonymousActivityHandler.removeSessionCallbackParameterI(paramString);
      }
    });
  }
  
  public void removeSessionPartnerParameter(final String paramString)
  {
    if (this.activityHandler != null)
    {
      this.activityHandler.removeSessionPartnerParameter(paramString);
      return;
    }
    if (this.sessionParametersActionsArray == null) {
      this.sessionParametersActionsArray = new ArrayList();
    }
    this.sessionParametersActionsArray.add(new IRunActivityHandler()
    {
      public void run(ActivityHandler paramAnonymousActivityHandler)
      {
        paramAnonymousActivityHandler.removeSessionPartnerParameterI(paramString);
      }
    });
  }
  
  public void resetSessionCallbackParameters()
  {
    if (this.activityHandler != null)
    {
      this.activityHandler.resetSessionCallbackParameters();
      return;
    }
    if (this.sessionParametersActionsArray == null) {
      this.sessionParametersActionsArray = new ArrayList();
    }
    this.sessionParametersActionsArray.add(new IRunActivityHandler()
    {
      public void run(ActivityHandler paramAnonymousActivityHandler)
      {
        paramAnonymousActivityHandler.resetSessionCallbackParametersI();
      }
    });
  }
  
  public void resetSessionPartnerParameters()
  {
    if (this.activityHandler != null)
    {
      this.activityHandler.resetSessionPartnerParameters();
      return;
    }
    if (this.sessionParametersActionsArray == null) {
      this.sessionParametersActionsArray = new ArrayList();
    }
    this.sessionParametersActionsArray.add(new IRunActivityHandler()
    {
      public void run(ActivityHandler paramAnonymousActivityHandler)
      {
        paramAnonymousActivityHandler.resetSessionPartnerParametersI();
      }
    });
  }
  
  public void sendFirstPackages()
  {
    if (!checkActivityHandler()) {
      return;
    }
    this.activityHandler.sendFirstPackages();
  }
  
  public void sendReferrer(String paramString)
  {
    long l = System.currentTimeMillis();
    if (this.activityHandler == null)
    {
      this.referrer = paramString;
      this.referrerClickTime = l;
      return;
    }
    this.activityHandler.sendReferrer(paramString, l);
  }
  
  public void setEnabled(boolean paramBoolean)
  {
    if (!checkActivityHandler()) {
      return;
    }
    this.activityHandler.setEnabled(paramBoolean);
  }
  
  public void setOfflineMode(boolean paramBoolean)
  {
    if (!checkActivityHandler()) {
      return;
    }
    this.activityHandler.setOfflineMode(paramBoolean);
  }
  
  public void setPushToken(String paramString)
  {
    this.pushToken = paramString;
    if (this.activityHandler != null) {
      this.activityHandler.setPushToken(paramString);
    }
  }
  
  public void teardown(boolean paramBoolean)
  {
    if (!checkActivityHandler()) {
      return;
    }
    this.activityHandler.teardown(paramBoolean);
    this.activityHandler = null;
  }
  
  public void trackEvent(AdjustEvent paramAdjustEvent)
  {
    if (!checkActivityHandler()) {
      return;
    }
    this.activityHandler.trackEvent(paramAdjustEvent);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\AdjustInstance.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */