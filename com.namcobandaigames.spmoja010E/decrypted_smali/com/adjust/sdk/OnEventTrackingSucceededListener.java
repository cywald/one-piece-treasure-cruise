package com.adjust.sdk;

public abstract interface OnEventTrackingSucceededListener
{
  public abstract void onFinishedEventTrackingSucceeded(AdjustEventSuccess paramAdjustEventSuccess);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\OnEventTrackingSucceededListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */