package com.adjust.sdk;

import android.net.Uri;

public abstract interface IActivityHandler
{
  public abstract void addSessionCallbackParameter(String paramString1, String paramString2);
  
  public abstract void addSessionPartnerParameter(String paramString1, String paramString2);
  
  public abstract void finishedTrackingActivity(ResponseData paramResponseData);
  
  public abstract void init(AdjustConfig paramAdjustConfig);
  
  public abstract boolean isEnabled();
  
  public abstract void launchAttributionResponseTasks(AttributionResponseData paramAttributionResponseData);
  
  public abstract void launchEventResponseTasks(EventResponseData paramEventResponseData);
  
  public abstract void launchSdkClickResponseTasks(SdkClickResponseData paramSdkClickResponseData);
  
  public abstract void launchSessionResponseTasks(SessionResponseData paramSessionResponseData);
  
  public abstract void onPause();
  
  public abstract void onResume();
  
  public abstract void readOpenUrl(Uri paramUri, long paramLong);
  
  public abstract void removeSessionCallbackParameter(String paramString);
  
  public abstract void removeSessionPartnerParameter(String paramString);
  
  public abstract void resetSessionCallbackParameters();
  
  public abstract void resetSessionPartnerParameters();
  
  public abstract void sendFirstPackages();
  
  public abstract void sendReferrer(String paramString, long paramLong);
  
  public abstract void setAskingAttribution(boolean paramBoolean);
  
  public abstract void setEnabled(boolean paramBoolean);
  
  public abstract void setOfflineMode(boolean paramBoolean);
  
  public abstract void setPushToken(String paramString);
  
  public abstract void teardown(boolean paramBoolean);
  
  public abstract void trackEvent(AdjustEvent paramAdjustEvent);
  
  public abstract boolean updateAttributionI(AdjustAttribution paramAdjustAttribution);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\IActivityHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */