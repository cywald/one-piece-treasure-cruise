package com.adjust.sdk;

import android.content.Context;

public abstract interface IPackageHandler
{
  public abstract void addPackage(ActivityPackage paramActivityPackage);
  
  public abstract void closeFirstPackage(ResponseData paramResponseData, ActivityPackage paramActivityPackage);
  
  public abstract void init(IActivityHandler paramIActivityHandler, Context paramContext, boolean paramBoolean);
  
  public abstract void pauseSending();
  
  public abstract void resumeSending();
  
  public abstract void sendFirstPackage();
  
  public abstract void sendNextPackage(ResponseData paramResponseData);
  
  public abstract void teardown(boolean paramBoolean);
  
  public abstract void updatePackages(SessionParameters paramSessionParameters);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\IPackageHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */