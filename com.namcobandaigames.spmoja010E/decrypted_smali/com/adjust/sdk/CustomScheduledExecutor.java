package com.adjust.sdk;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class CustomScheduledExecutor
{
  private ScheduledThreadPoolExecutor executor;
  private String source;
  private final AtomicInteger threadCounter = new AtomicInteger(1);
  
  public CustomScheduledExecutor(final String paramString, boolean paramBoolean)
  {
    this.source = paramString;
    this.executor = new ScheduledThreadPoolExecutor(1, new ThreadFactory()new RejectedExecutionHandler
    {
      public Thread newThread(Runnable paramAnonymousRunnable)
      {
        paramAnonymousRunnable = Executors.defaultThreadFactory().newThread(new CustomScheduledExecutor.RunnableWrapper(CustomScheduledExecutor.this, paramAnonymousRunnable));
        paramAnonymousRunnable.setPriority(1);
        paramAnonymousRunnable.setName("Adjust-" + paramAnonymousRunnable.getName() + "-" + paramString);
        paramAnonymousRunnable.setDaemon(true);
        paramAnonymousRunnable.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
          public void uncaughtException(Thread paramAnonymous2Thread, Throwable paramAnonymous2Throwable)
          {
            AdjustFactory.getLogger().error("Thread %s with error %s", new Object[] { paramAnonymous2Thread.getName(), paramAnonymous2Throwable.getMessage() });
          }
        });
        return paramAnonymousRunnable;
      }
    }, new RejectedExecutionHandler()
    {
      public void rejectedExecution(Runnable paramAnonymousRunnable, ThreadPoolExecutor paramAnonymousThreadPoolExecutor)
      {
        AdjustFactory.getLogger().warn("Runnable %s rejected from %s ", new Object[] { paramAnonymousRunnable.toString(), paramString });
      }
    });
    if (!paramBoolean)
    {
      this.executor.setKeepAliveTime(10L, TimeUnit.MILLISECONDS);
      this.executor.allowCoreThreadTimeOut(true);
    }
  }
  
  public ScheduledFuture<?> schedule(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit)
  {
    return this.executor.schedule(paramRunnable, paramLong, paramTimeUnit);
  }
  
  public ScheduledFuture<?> scheduleWithFixedDelay(Runnable paramRunnable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
  {
    return this.executor.scheduleWithFixedDelay(paramRunnable, paramLong1, paramLong2, paramTimeUnit);
  }
  
  public void shutdownNow()
  {
    this.executor.shutdownNow();
  }
  
  public Future<?> submit(Runnable paramRunnable)
  {
    return this.executor.submit(paramRunnable);
  }
  
  private class RunnableWrapper
    implements Runnable
  {
    private Runnable runnable;
    
    public RunnableWrapper(Runnable paramRunnable)
    {
      this.runnable = paramRunnable;
    }
    
    public void run()
    {
      try
      {
        this.runnable.run();
        return;
      }
      catch (Throwable localThrowable)
      {
        AdjustFactory.getLogger().error("Runnable error %s", new Object[] { localThrowable.getMessage() });
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\CustomScheduledExecutor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */