package com.adjust.sdk;

public class Adjust2dxSessionTrackingFailedCallback
  implements OnSessionTrackingFailedListener
{
  public void onFinishedSessionTrackingFailed(AdjustSessionFailure paramAdjustSessionFailure)
  {
    sessionTrackingFailed(paramAdjustSessionFailure);
  }
  
  public native void sessionTrackingFailed(Object paramObject);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Adjust2dxSessionTrackingFailedCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */