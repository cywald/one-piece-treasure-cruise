package com.adjust.sdk;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SdkClickHandler
  implements ISdkClickHandler
{
  private WeakReference<IActivityHandler> activityHandlerWeakRef;
  private BackoffStrategy backoffStrategy;
  private ILogger logger;
  private List<ActivityPackage> packageQueue;
  private boolean paused;
  private CustomScheduledExecutor scheduledExecutor;
  
  public SdkClickHandler(IActivityHandler paramIActivityHandler, boolean paramBoolean)
  {
    init(paramIActivityHandler, paramBoolean);
    this.logger = AdjustFactory.getLogger();
    this.scheduledExecutor = new CustomScheduledExecutor("SdkClickHandler", false);
    this.backoffStrategy = AdjustFactory.getSdkClickBackoffStrategy();
  }
  
  private void logErrorMessageI(ActivityPackage paramActivityPackage, String paramString, Throwable paramThrowable)
  {
    paramActivityPackage = String.format("%s. (%s)", new Object[] { paramActivityPackage.getFailureMessage(), Util.getReasonString(paramString, paramThrowable) });
    this.logger.error(paramActivityPackage, new Object[0]);
  }
  
  private void retrySendingI(ActivityPackage paramActivityPackage)
  {
    int i = paramActivityPackage.increaseRetries();
    this.logger.error("Retrying sdk_click package for the %d time", new Object[] { Integer.valueOf(i) });
    sendSdkClick(paramActivityPackage);
  }
  
  private void sendNextSdkClick()
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        SdkClickHandler.this.sendNextSdkClickI();
      }
    });
  }
  
  private void sendNextSdkClickI()
  {
    if (this.paused) {}
    while (this.packageQueue.isEmpty()) {
      return;
    }
    final Object localObject = (ActivityPackage)this.packageQueue.remove(0);
    int i = ((ActivityPackage)localObject).getRetries();
    localObject = new Runnable()
    {
      public void run()
      {
        SdkClickHandler.this.sendSdkClickI(localObject);
        SdkClickHandler.this.sendNextSdkClick();
      }
    };
    if (i <= 0)
    {
      ((Runnable)localObject).run();
      return;
    }
    long l = Util.getWaitingTime(i, this.backoffStrategy);
    double d = l / 1000.0D;
    String str = Util.SecondsDisplayFormat.format(d);
    this.logger.verbose("Waiting for %s seconds before retrying sdk_click for the %d time", new Object[] { str, Integer.valueOf(i) });
    this.scheduledExecutor.schedule((Runnable)localObject, l, TimeUnit.MILLISECONDS);
  }
  
  private void sendSdkClickI(ActivityPackage paramActivityPackage)
  {
    Object localObject = "https://app.adjust.com" + paramActivityPackage.getPath();
    try
    {
      localObject = UtilNetworking.createPOSTHttpsURLConnection((String)localObject, paramActivityPackage, this.packageQueue.size() - 1);
      if (((ResponseData)localObject).jsonResponse == null)
      {
        retrySendingI(paramActivityPackage);
        return;
      }
      IActivityHandler localIActivityHandler = (IActivityHandler)this.activityHandlerWeakRef.get();
      if (localIActivityHandler != null)
      {
        localIActivityHandler.finishedTrackingActivity((ResponseData)localObject);
        return;
      }
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      logErrorMessageI(paramActivityPackage, "Sdk_click failed to encode parameters", localUnsupportedEncodingException);
      return;
    }
    catch (SocketTimeoutException localSocketTimeoutException)
    {
      logErrorMessageI(paramActivityPackage, "Sdk_click request timed out. Will retry later", localSocketTimeoutException);
      retrySendingI(paramActivityPackage);
      return;
    }
    catch (IOException localIOException)
    {
      logErrorMessageI(paramActivityPackage, "Sdk_click request failed. Will retry later", localIOException);
      retrySendingI(paramActivityPackage);
      return;
    }
    catch (Throwable localThrowable)
    {
      logErrorMessageI(paramActivityPackage, "Sdk_click runtime exception", localThrowable);
    }
  }
  
  public void init(IActivityHandler paramIActivityHandler, boolean paramBoolean)
  {
    if (!paramBoolean) {}
    for (paramBoolean = true;; paramBoolean = false)
    {
      this.paused = paramBoolean;
      this.packageQueue = new ArrayList();
      this.activityHandlerWeakRef = new WeakReference(paramIActivityHandler);
      return;
    }
  }
  
  public void pauseSending()
  {
    this.paused = true;
  }
  
  public void resumeSending()
  {
    this.paused = false;
    sendNextSdkClick();
  }
  
  public void sendSdkClick(final ActivityPackage paramActivityPackage)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        SdkClickHandler.this.packageQueue.add(paramActivityPackage);
        SdkClickHandler.this.logger.debug("Added sdk_click %d", new Object[] { Integer.valueOf(SdkClickHandler.this.packageQueue.size()) });
        SdkClickHandler.this.logger.verbose("%s", new Object[] { paramActivityPackage.getExtendedString() });
        SdkClickHandler.this.sendNextSdkClick();
      }
    });
  }
  
  public void teardown()
  {
    this.logger.verbose("SdkClickHandler teardown", new Object[0]);
    if (this.scheduledExecutor != null) {}
    try
    {
      this.scheduledExecutor.shutdownNow();
      if (this.packageQueue != null) {
        this.packageQueue.clear();
      }
      if (this.activityHandlerWeakRef != null) {
        this.activityHandlerWeakRef.clear();
      }
      this.scheduledExecutor = null;
      this.logger = null;
      this.packageQueue = null;
      this.backoffStrategy = null;
      return;
    }
    catch (SecurityException localSecurityException)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\SdkClickHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */