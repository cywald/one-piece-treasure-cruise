package com.adjust.sdk;

public class SessionResponseData
  extends ResponseData
{
  public AdjustSessionFailure getFailureResponseData()
  {
    if (this.success) {
      return null;
    }
    AdjustSessionFailure localAdjustSessionFailure = new AdjustSessionFailure();
    localAdjustSessionFailure.message = this.message;
    localAdjustSessionFailure.timestamp = this.timestamp;
    localAdjustSessionFailure.adid = this.adid;
    localAdjustSessionFailure.willRetry = this.willRetry;
    localAdjustSessionFailure.jsonResponse = this.jsonResponse;
    return localAdjustSessionFailure;
  }
  
  public AdjustSessionSuccess getSuccessResponseData()
  {
    if (!this.success) {
      return null;
    }
    AdjustSessionSuccess localAdjustSessionSuccess = new AdjustSessionSuccess();
    localAdjustSessionSuccess.message = this.message;
    localAdjustSessionSuccess.timestamp = this.timestamp;
    localAdjustSessionSuccess.adid = this.adid;
    localAdjustSessionSuccess.jsonResponse = this.jsonResponse;
    return localAdjustSessionSuccess;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\SessionResponseData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */