package com.adjust.sdk;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;

public class RequestHandler
  implements IRequestHandler
{
  private ILogger logger = AdjustFactory.getLogger();
  private WeakReference<IPackageHandler> packageHandlerWeakRef;
  private CustomScheduledExecutor scheduledExecutor = new CustomScheduledExecutor("RequestHandler", false);
  
  public RequestHandler(IPackageHandler paramIPackageHandler)
  {
    init(paramIPackageHandler);
  }
  
  private void closePackageI(ActivityPackage paramActivityPackage, String paramString, Throwable paramThrowable)
  {
    paramThrowable = String.format("%s. (%s) Will retry later", new Object[] { paramActivityPackage.getFailureMessage(), Util.getReasonString(paramString, paramThrowable) });
    this.logger.error(paramThrowable, new Object[0]);
    paramString = ResponseData.buildResponseData(paramActivityPackage);
    paramString.message = paramThrowable;
    paramThrowable = (IPackageHandler)this.packageHandlerWeakRef.get();
    if (paramThrowable == null) {
      return;
    }
    paramThrowable.closeFirstPackage(paramString, paramActivityPackage);
  }
  
  private void sendI(ActivityPackage paramActivityPackage, int paramInt)
  {
    Object localObject = "https://app.adjust.com" + paramActivityPackage.getPath();
    try
    {
      localObject = UtilNetworking.createPOSTHttpsURLConnection((String)localObject, paramActivityPackage, paramInt);
      localIPackageHandler = (IPackageHandler)this.packageHandlerWeakRef.get();
      if (localIPackageHandler == null) {
        return;
      }
      if (((ResponseData)localObject).jsonResponse == null)
      {
        localIPackageHandler.closeFirstPackage((ResponseData)localObject, paramActivityPackage);
        return;
      }
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      IPackageHandler localIPackageHandler;
      sendNextPackageI(paramActivityPackage, "Failed to encode parameters", localUnsupportedEncodingException);
      return;
      localIPackageHandler.sendNextPackage(localUnsupportedEncodingException);
      return;
    }
    catch (SocketTimeoutException localSocketTimeoutException)
    {
      closePackageI(paramActivityPackage, "Request timed out", localSocketTimeoutException);
      return;
    }
    catch (IOException localIOException)
    {
      closePackageI(paramActivityPackage, "Request failed", localIOException);
      return;
    }
    catch (Throwable localThrowable)
    {
      sendNextPackageI(paramActivityPackage, "Runtime exception", localThrowable);
    }
  }
  
  private void sendNextPackageI(ActivityPackage paramActivityPackage, String paramString, Throwable paramThrowable)
  {
    paramString = String.format("%s. (%s)", new Object[] { paramActivityPackage.getFailureMessage(), Util.getReasonString(paramString, paramThrowable) });
    this.logger.error(paramString, new Object[0]);
    paramActivityPackage = ResponseData.buildResponseData(paramActivityPackage);
    paramActivityPackage.message = paramString;
    paramString = (IPackageHandler)this.packageHandlerWeakRef.get();
    if (paramString == null) {
      return;
    }
    paramString.sendNextPackage(paramActivityPackage);
  }
  
  public void init(IPackageHandler paramIPackageHandler)
  {
    this.packageHandlerWeakRef = new WeakReference(paramIPackageHandler);
  }
  
  public void sendPackage(final ActivityPackage paramActivityPackage, final int paramInt)
  {
    this.scheduledExecutor.submit(new Runnable()
    {
      public void run()
      {
        RequestHandler.this.sendI(paramActivityPackage, paramInt);
      }
    });
  }
  
  public void teardown()
  {
    this.logger.verbose("RequestHandler teardown", new Object[0]);
    if (this.scheduledExecutor != null) {}
    try
    {
      this.scheduledExecutor.shutdownNow();
      if (this.packageHandlerWeakRef != null) {
        this.packageHandlerWeakRef.clear();
      }
      this.scheduledExecutor = null;
      this.packageHandlerWeakRef = null;
      this.logger = null;
      return;
    }
    catch (SecurityException localSecurityException)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\RequestHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */