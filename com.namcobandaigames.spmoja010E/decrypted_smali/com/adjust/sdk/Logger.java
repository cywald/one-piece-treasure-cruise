package com.adjust.sdk;

import android.util.Log;
import java.util.Arrays;
import java.util.Locale;

public class Logger
  implements ILogger
{
  private static String formatErrorMessage = "Error formating log message: %s, with params: %s";
  private boolean isProductionEnvironment = false;
  private LogLevel logLevel;
  private boolean logLevelLocked = false;
  
  public Logger()
  {
    setLogLevel(LogLevel.INFO, this.isProductionEnvironment);
  }
  
  public void Assert(String paramString, Object... paramVarArgs)
  {
    if (this.isProductionEnvironment) {}
    while (this.logLevel.androidLogLevel > 7) {
      return;
    }
    try
    {
      Log.println(7, "Adjust", String.format(Locale.US, paramString, paramVarArgs));
      return;
    }
    catch (Exception localException)
    {
      Log.e("Adjust", String.format(Locale.US, formatErrorMessage, new Object[] { paramString, Arrays.toString(paramVarArgs) }));
    }
  }
  
  public void debug(String paramString, Object... paramVarArgs)
  {
    if (this.isProductionEnvironment) {}
    while (this.logLevel.androidLogLevel > 3) {
      return;
    }
    try
    {
      Log.d("Adjust", String.format(Locale.US, paramString, paramVarArgs));
      return;
    }
    catch (Exception localException)
    {
      Log.e("Adjust", String.format(Locale.US, formatErrorMessage, new Object[] { paramString, Arrays.toString(paramVarArgs) }));
    }
  }
  
  public void error(String paramString, Object... paramVarArgs)
  {
    if (this.isProductionEnvironment) {}
    while (this.logLevel.androidLogLevel > 6) {
      return;
    }
    try
    {
      Log.e("Adjust", String.format(Locale.US, paramString, paramVarArgs));
      return;
    }
    catch (Exception localException)
    {
      Log.e("Adjust", String.format(Locale.US, formatErrorMessage, new Object[] { paramString, Arrays.toString(paramVarArgs) }));
    }
  }
  
  public void info(String paramString, Object... paramVarArgs)
  {
    if (this.isProductionEnvironment) {}
    while (this.logLevel.androidLogLevel > 4) {
      return;
    }
    try
    {
      Log.i("Adjust", String.format(Locale.US, paramString, paramVarArgs));
      return;
    }
    catch (Exception localException)
    {
      Log.e("Adjust", String.format(Locale.US, formatErrorMessage, new Object[] { paramString, Arrays.toString(paramVarArgs) }));
    }
  }
  
  public void lockLogLevel()
  {
    this.logLevelLocked = true;
  }
  
  public void setLogLevel(LogLevel paramLogLevel, boolean paramBoolean)
  {
    if (this.logLevelLocked) {
      return;
    }
    this.logLevel = paramLogLevel;
    this.isProductionEnvironment = paramBoolean;
  }
  
  public void setLogLevelString(String paramString, boolean paramBoolean)
  {
    if (paramString != null) {}
    try
    {
      setLogLevel(LogLevel.valueOf(paramString.toUpperCase(Locale.US)), paramBoolean);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      error("Malformed logLevel '%s', falling back to 'info'", new Object[] { paramString });
    }
  }
  
  public void verbose(String paramString, Object... paramVarArgs)
  {
    if (this.isProductionEnvironment) {}
    while (this.logLevel.androidLogLevel > 2) {
      return;
    }
    try
    {
      Log.v("Adjust", String.format(Locale.US, paramString, paramVarArgs));
      return;
    }
    catch (Exception localException)
    {
      Log.e("Adjust", String.format(Locale.US, formatErrorMessage, new Object[] { paramString, Arrays.toString(paramVarArgs) }));
    }
  }
  
  public void warn(String paramString, Object... paramVarArgs)
  {
    if (this.isProductionEnvironment) {}
    while (this.logLevel.androidLogLevel > 5) {
      return;
    }
    try
    {
      Log.w("Adjust", String.format(Locale.US, paramString, paramVarArgs));
      return;
    }
    catch (Exception localException)
    {
      Log.e("Adjust", String.format(Locale.US, formatErrorMessage, new Object[] { paramString, Arrays.toString(paramVarArgs) }));
    }
  }
  
  public void warnInProduction(String paramString, Object... paramVarArgs)
  {
    if (this.logLevel.androidLogLevel <= 5) {}
    try
    {
      Log.w("Adjust", String.format(Locale.US, paramString, paramVarArgs));
      return;
    }
    catch (Exception localException)
    {
      Log.e("Adjust", String.format(Locale.US, formatErrorMessage, new Object[] { paramString, Arrays.toString(paramVarArgs) }));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Logger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */