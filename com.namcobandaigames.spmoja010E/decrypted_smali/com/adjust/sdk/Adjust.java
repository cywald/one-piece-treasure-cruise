package com.adjust.sdk;

import android.content.Context;
import android.net.Uri;

public class Adjust
{
  private static AdjustInstance defaultInstance;
  
  public static void addSessionCallbackParameter(String paramString1, String paramString2)
  {
    getDefaultInstance().addSessionCallbackParameter(paramString1, paramString2);
  }
  
  public static void addSessionPartnerParameter(String paramString1, String paramString2)
  {
    getDefaultInstance().addSessionPartnerParameter(paramString1, paramString2);
  }
  
  public static void appWillOpenUrl(Uri paramUri)
  {
    getDefaultInstance().appWillOpenUrl(paramUri);
  }
  
  public static String getAdid()
  {
    return getDefaultInstance().getAdid();
  }
  
  public static AdjustAttribution getAttribution()
  {
    return getDefaultInstance().getAttribution();
  }
  
  public static AdjustInstance getDefaultInstance()
  {
    try
    {
      if (defaultInstance == null) {
        defaultInstance = new AdjustInstance();
      }
      AdjustInstance localAdjustInstance = defaultInstance;
      return localAdjustInstance;
    }
    finally {}
  }
  
  public static void getGoogleAdId(Context paramContext, OnDeviceIdsRead paramOnDeviceIdsRead)
  {
    Util.getGoogleAdId(paramContext, paramOnDeviceIdsRead);
  }
  
  public static boolean isEnabled()
  {
    return getDefaultInstance().isEnabled();
  }
  
  public static void onCreate(AdjustConfig paramAdjustConfig)
  {
    getDefaultInstance().onCreate(paramAdjustConfig);
  }
  
  public static void onPause()
  {
    getDefaultInstance().onPause();
  }
  
  public static void onResume()
  {
    getDefaultInstance().onResume();
  }
  
  public static void removeSessionCallbackParameter(String paramString)
  {
    getDefaultInstance().removeSessionCallbackParameter(paramString);
  }
  
  public static void removeSessionPartnerParameter(String paramString)
  {
    getDefaultInstance().removeSessionPartnerParameter(paramString);
  }
  
  public static void resetSessionCallbackParameters()
  {
    getDefaultInstance().resetSessionCallbackParameters();
  }
  
  public static void resetSessionPartnerParameters()
  {
    getDefaultInstance().resetSessionPartnerParameters();
  }
  
  public static void sendFirstPackages()
  {
    getDefaultInstance().sendFirstPackages();
  }
  
  public static void setEnabled(boolean paramBoolean)
  {
    getDefaultInstance().setEnabled(paramBoolean);
  }
  
  public static void setOfflineMode(boolean paramBoolean)
  {
    getDefaultInstance().setOfflineMode(paramBoolean);
  }
  
  public static void setPushToken(String paramString)
  {
    getDefaultInstance().setPushToken(paramString);
  }
  
  public static void setReferrer(String paramString)
  {
    getDefaultInstance().sendReferrer(paramString);
  }
  
  public static void trackEvent(AdjustEvent paramAdjustEvent)
  {
    getDefaultInstance().trackEvent(paramAdjustEvent);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\Adjust.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */