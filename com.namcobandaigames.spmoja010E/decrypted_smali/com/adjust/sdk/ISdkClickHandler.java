package com.adjust.sdk;

public abstract interface ISdkClickHandler
{
  public abstract void init(IActivityHandler paramIActivityHandler, boolean paramBoolean);
  
  public abstract void pauseSending();
  
  public abstract void resumeSending();
  
  public abstract void sendSdkClick(ActivityPackage paramActivityPackage);
  
  public abstract void teardown();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\ISdkClickHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */