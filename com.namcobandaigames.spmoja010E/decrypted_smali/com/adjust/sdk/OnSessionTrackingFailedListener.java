package com.adjust.sdk;

public abstract interface OnSessionTrackingFailedListener
{
  public abstract void onFinishedSessionTrackingFailed(AdjustSessionFailure paramAdjustSessionFailure);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\OnSessionTrackingFailedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */