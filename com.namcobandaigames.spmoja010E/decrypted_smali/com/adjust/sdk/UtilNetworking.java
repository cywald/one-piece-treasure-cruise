package com.adjust.sdk;

import android.net.Uri;
import android.net.Uri.Builder;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

public class UtilNetworking
{
  private static String userAgent;
  
  private static Uri buildUri(String paramString, Map<String, String> paramMap)
  {
    Uri.Builder localBuilder = new Uri.Builder();
    localBuilder.scheme("https");
    localBuilder.authority("app.adjust.com");
    localBuilder.appendPath(paramString);
    paramString = paramMap.entrySet().iterator();
    while (paramString.hasNext())
    {
      paramMap = (Map.Entry)paramString.next();
      localBuilder.appendQueryParameter((String)paramMap.getKey(), (String)paramMap.getValue());
    }
    long l = System.currentTimeMillis();
    localBuilder.appendQueryParameter("sent_at", Util.dateFormatter.format(Long.valueOf(l)));
    return localBuilder.build();
  }
  
  public static ResponseData createGETHttpsURLConnection(ActivityPackage paramActivityPackage)
    throws Exception
  {
    try
    {
      Object localObject = new HashMap(paramActivityPackage.getParameters());
      localObject = AdjustFactory.getHttpsURLConnection(new URL(buildUri(paramActivityPackage.getPath(), (Map)localObject).toString()));
      setDefaultHttpsUrlConnectionProperties((HttpsURLConnection)localObject, paramActivityPackage.getClientSdk());
      ((HttpsURLConnection)localObject).setRequestMethod("GET");
      paramActivityPackage = readHttpResponse((HttpsURLConnection)localObject, paramActivityPackage);
      return paramActivityPackage;
    }
    catch (Exception paramActivityPackage)
    {
      throw paramActivityPackage;
    }
  }
  
  /* Error */
  public static ResponseData createPOSTHttpsURLConnection(String paramString, ActivityPackage paramActivityPackage, int paramInt)
    throws Exception
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aconst_null
    //   4: astore 4
    //   6: aload 5
    //   8: astore_3
    //   9: new 115	java/net/URL
    //   12: dup
    //   13: aload_0
    //   14: invokespecial 129	java/net/URL:<init>	(Ljava/lang/String;)V
    //   17: invokestatic 135	com/adjust/sdk/AdjustFactory:getHttpsURLConnection	(Ljava/net/URL;)Ljavax/net/ssl/HttpsURLConnection;
    //   20: astore 6
    //   22: aload 5
    //   24: astore_3
    //   25: new 104	java/util/HashMap
    //   28: dup
    //   29: aload_1
    //   30: invokevirtual 110	com/adjust/sdk/ActivityPackage:getParameters	()Ljava/util/Map;
    //   33: invokespecial 113	java/util/HashMap:<init>	(Ljava/util/Map;)V
    //   36: astore 7
    //   38: aload 5
    //   40: astore_3
    //   41: aload 6
    //   43: aload_1
    //   44: invokevirtual 138	com/adjust/sdk/ActivityPackage:getClientSdk	()Ljava/lang/String;
    //   47: invokestatic 142	com/adjust/sdk/UtilNetworking:setDefaultHttpsUrlConnectionProperties	(Ljavax/net/ssl/HttpsURLConnection;Ljava/lang/String;)V
    //   50: aload 5
    //   52: astore_3
    //   53: aload 6
    //   55: ldc -98
    //   57: invokevirtual 149	javax/net/ssl/HttpsURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   60: aload 5
    //   62: astore_3
    //   63: aload 6
    //   65: iconst_0
    //   66: invokevirtual 162	javax/net/ssl/HttpsURLConnection:setUseCaches	(Z)V
    //   69: aload 5
    //   71: astore_3
    //   72: aload 6
    //   74: iconst_1
    //   75: invokevirtual 165	javax/net/ssl/HttpsURLConnection:setDoInput	(Z)V
    //   78: aload 5
    //   80: astore_3
    //   81: aload 6
    //   83: iconst_1
    //   84: invokevirtual 168	javax/net/ssl/HttpsURLConnection:setDoOutput	(Z)V
    //   87: aload 5
    //   89: astore_3
    //   90: new 170	java/io/DataOutputStream
    //   93: dup
    //   94: aload 6
    //   96: invokevirtual 174	javax/net/ssl/HttpsURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   99: invokespecial 177	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   102: astore_0
    //   103: aload_0
    //   104: aload 7
    //   106: iload_2
    //   107: invokestatic 181	com/adjust/sdk/UtilNetworking:getPostDataString	(Ljava/util/Map;I)Ljava/lang/String;
    //   110: invokevirtual 184	java/io/DataOutputStream:writeBytes	(Ljava/lang/String;)V
    //   113: aload 6
    //   115: aload_1
    //   116: invokestatic 153	com/adjust/sdk/UtilNetworking:readHttpResponse	(Ljavax/net/ssl/HttpsURLConnection;Lcom/adjust/sdk/ActivityPackage;)Lcom/adjust/sdk/ResponseData;
    //   119: astore_1
    //   120: aload_0
    //   121: ifnull +11 -> 132
    //   124: aload_0
    //   125: invokevirtual 187	java/io/DataOutputStream:flush	()V
    //   128: aload_0
    //   129: invokevirtual 190	java/io/DataOutputStream:close	()V
    //   132: aload_1
    //   133: areturn
    //   134: astore_0
    //   135: aload 4
    //   137: astore_3
    //   138: aload_0
    //   139: athrow
    //   140: astore_0
    //   141: aload_3
    //   142: ifnull +11 -> 153
    //   145: aload_3
    //   146: invokevirtual 187	java/io/DataOutputStream:flush	()V
    //   149: aload_3
    //   150: invokevirtual 190	java/io/DataOutputStream:close	()V
    //   153: aload_0
    //   154: athrow
    //   155: astore_1
    //   156: goto -3 -> 153
    //   159: astore_1
    //   160: aload_0
    //   161: astore_3
    //   162: aload_1
    //   163: astore_0
    //   164: goto -23 -> 141
    //   167: astore_1
    //   168: aload_0
    //   169: astore_3
    //   170: aload_1
    //   171: astore_0
    //   172: goto -34 -> 138
    //   175: astore_0
    //   176: aload_1
    //   177: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	178	0	paramString	String
    //   0	178	1	paramActivityPackage	ActivityPackage
    //   0	178	2	paramInt	int
    //   8	162	3	localObject1	Object
    //   4	132	4	localObject2	Object
    //   1	87	5	localObject3	Object
    //   20	94	6	localHttpsURLConnection	HttpsURLConnection
    //   36	69	7	localHashMap	HashMap
    // Exception table:
    //   from	to	target	type
    //   9	22	134	java/lang/Exception
    //   25	38	134	java/lang/Exception
    //   41	50	134	java/lang/Exception
    //   53	60	134	java/lang/Exception
    //   63	69	134	java/lang/Exception
    //   72	78	134	java/lang/Exception
    //   81	87	134	java/lang/Exception
    //   90	103	134	java/lang/Exception
    //   9	22	140	finally
    //   25	38	140	finally
    //   41	50	140	finally
    //   53	60	140	finally
    //   63	69	140	finally
    //   72	78	140	finally
    //   81	87	140	finally
    //   90	103	140	finally
    //   138	140	140	finally
    //   145	153	155	java/lang/Exception
    //   103	120	159	finally
    //   103	120	167	java/lang/Exception
    //   124	132	175	java/lang/Exception
  }
  
  private static ILogger getLogger()
  {
    return AdjustFactory.getLogger();
  }
  
  private static String getPostDataString(Map<String, String> paramMap, int paramInt)
    throws UnsupportedEncodingException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramMap.entrySet().iterator();
    if (localIterator.hasNext())
    {
      paramMap = (Map.Entry)localIterator.next();
      String str = URLEncoder.encode((String)paramMap.getKey(), "UTF-8");
      paramMap = (String)paramMap.getValue();
      if (paramMap != null) {}
      for (paramMap = URLEncoder.encode(paramMap, "UTF-8");; paramMap = "")
      {
        if (localStringBuilder.length() > 0) {
          localStringBuilder.append("&");
        }
        localStringBuilder.append(str);
        localStringBuilder.append("=");
        localStringBuilder.append(paramMap);
        break;
      }
    }
    long l = System.currentTimeMillis();
    paramMap = Util.dateFormatter.format(Long.valueOf(l));
    localStringBuilder.append("&");
    localStringBuilder.append(URLEncoder.encode("sent_at", "UTF-8"));
    localStringBuilder.append("=");
    localStringBuilder.append(URLEncoder.encode(paramMap, "UTF-8"));
    if (paramInt > 0)
    {
      localStringBuilder.append("&");
      localStringBuilder.append(URLEncoder.encode("queue_size", "UTF-8"));
      localStringBuilder.append("=");
      localStringBuilder.append(URLEncoder.encode("" + paramInt, "UTF-8"));
    }
    return localStringBuilder.toString();
  }
  
  private static ResponseData readHttpResponse(HttpsURLConnection paramHttpsURLConnection, ActivityPackage paramActivityPackage)
    throws Exception
  {
    StringBuffer localStringBuffer = new StringBuffer();
    ILogger localILogger = getLogger();
    ResponseData localResponseData = ResponseData.buildResponseData(paramActivityPackage);
    Integer localInteger;
    for (;;)
    {
      try
      {
        paramHttpsURLConnection.connect();
        localInteger = Integer.valueOf(paramHttpsURLConnection.getResponseCode());
        if (localInteger.intValue() >= 400)
        {
          paramActivityPackage = paramHttpsURLConnection.getErrorStream();
          paramActivityPackage = new BufferedReader(new InputStreamReader(paramActivityPackage));
          String str = paramActivityPackage.readLine();
          if (str == null) {
            break;
          }
          localStringBuffer.append(str);
          continue;
        }
        paramActivityPackage = paramHttpsURLConnection.getInputStream();
      }
      catch (Exception paramActivityPackage)
      {
        localILogger.error("Failed to read response. (%s)", new Object[] { paramActivityPackage.getMessage() });
        throw paramActivityPackage;
      }
      finally
      {
        if (paramHttpsURLConnection != null) {
          paramHttpsURLConnection.disconnect();
        }
      }
    }
    if (paramHttpsURLConnection != null) {
      paramHttpsURLConnection.disconnect();
    }
    paramActivityPackage = localStringBuffer.toString();
    localILogger.verbose("Response: %s", new Object[] { paramActivityPackage });
    if ((paramActivityPackage == null) || (paramActivityPackage.length() == 0)) {}
    do
    {
      return localResponseData;
      paramHttpsURLConnection = null;
      try
      {
        paramActivityPackage = new JSONObject(paramActivityPackage);
        paramHttpsURLConnection = paramActivityPackage;
      }
      catch (JSONException paramActivityPackage)
      {
        for (;;)
        {
          paramActivityPackage = String.format("Failed to parse json response. (%s)", new Object[] { paramActivityPackage.getMessage() });
          localILogger.error(paramActivityPackage, new Object[0]);
          localResponseData.message = paramActivityPackage;
        }
        localILogger.error("%s", new Object[] { paramHttpsURLConnection });
      }
    } while (paramHttpsURLConnection == null);
    localResponseData.jsonResponse = paramHttpsURLConnection;
    paramActivityPackage = paramHttpsURLConnection.optString("message", null);
    localResponseData.message = paramActivityPackage;
    localResponseData.timestamp = paramHttpsURLConnection.optString("timestamp", null);
    localResponseData.adid = paramHttpsURLConnection.optString("adid", null);
    paramHttpsURLConnection = paramActivityPackage;
    if (paramActivityPackage == null) {
      paramHttpsURLConnection = "No message found";
    }
    if ((localInteger != null) && (localInteger.intValue() == 200))
    {
      localILogger.info("%s", new Object[] { paramHttpsURLConnection });
      localResponseData.success = true;
      return localResponseData;
    }
    return localResponseData;
  }
  
  private static void setDefaultHttpsUrlConnectionProperties(HttpsURLConnection paramHttpsURLConnection, String paramString)
  {
    paramHttpsURLConnection.setRequestProperty("Client-SDK", paramString);
    paramHttpsURLConnection.setConnectTimeout(60000);
    paramHttpsURLConnection.setReadTimeout(60000);
    if (userAgent != null) {
      paramHttpsURLConnection.setRequestProperty("User-Agent", userAgent);
    }
  }
  
  public static void setUserAgent(String paramString)
  {
    userAgent = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\adjust\sdk\UtilNetworking.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */