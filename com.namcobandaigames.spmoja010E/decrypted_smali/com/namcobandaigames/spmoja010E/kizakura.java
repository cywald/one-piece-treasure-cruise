package com.namcobandaigames.spmoja010E;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import jp.co.drecom.bisque.lib.BQAppPlatformManager;
import jp.co.drecom.bisque.lib.BQClipboard;
import jp.co.drecom.bisque.lib.BQClipboardDispatcher;
import jp.co.drecom.bisque.lib.BQClipboardDispatcherNotify;
import jp.co.drecom.bisque.lib.BQHandler;
import jp.co.drecom.bisque.lib.BQHandlerDispatcher;
import jp.co.drecom.bisque.lib.BQHandlerDispatcherNotify;
import jp.co.drecom.bisque.lib.BQJNIHelper;
import jp.co.drecom.bisque.lib.BQPaymentBridge;
import jp.co.drecom.bisque.lib.BQPaymentDispatchable;
import jp.co.drecom.bisque.lib.BQPermissionHelper;
import jp.co.drecom.bisque.lib.BQPlayGameService;
import jp.co.drecom.bisque.lib.BQSleepSetting;
import jp.co.drecom.bisque.lib.BQSleepSettingDispatcher;
import jp.co.drecom.bisque.lib.BQSleepSettingDispatcherNotify;
import jp.co.drecom.bisque.lib.BQTouchDispatcher;
import jp.co.drecom.bisque.lib.BQTouchDispatcherNotify;
import jp.co.drecom.bisque.lib.BQTwitterHelper;
import jp.co.drecom.bisque.lib.BQTypefaces;
import jp.co.drecom.bisque.lib.BQTypefaces.Implement;
import jp.co.drecom.bisque.lib.BQUpdate;
import jp.co.drecom.bisque.lib.BQUpdateDispatcher;
import jp.co.drecom.bisque.lib.BQUpdateDispatcherNotify;
import jp.co.drecom.bisque.lib.BQUrlScheme;
import jp.co.drecom.bisque.lib.BQUrlSchemeDispatcher;
import jp.co.drecom.bisque.lib.BQUrlSchemeDispatcherNotify;
import jp.co.drecom.bisque.lib.BQWebView;
import jp.co.drecom.bisque.lib.BQWebViewDispatcher;
import jp.co.drecom.bisque.lib.BQWebViewDispatcherNotify;
import jp.co.drecom.bisque.lib.DRMoviePlayerManager;
import jp.co.drecom.bisque.lib.DRToastManager;
import jp.co.drecom.bisque.lib.HandlerJava;
import jp.co.drecom.bisque.lib.Notification.BQNotificationManagerHelper;
import jp.co.drecom.spice.hockeyapp.HockeyApp;
import jp.co.drecom.util.config.AppConfigure;
import net.hockeyapp.android.Constants;
import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.CrashManagerListener;
import net.hockeyapp.android.NativeCrashManager;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
import org.cocos2dx.lib.Cocos2dxRenderer;
import org.cocos2dx.lib.Cocos2dxTypefaces;

public class kizakura
  extends Cocos2dxActivity
  implements BQWebViewDispatcher, BQTouchDispatcher, BQPaymentDispatchable, BQUpdateDispatcher, BQClipboardDispatcher, BQHandlerDispatcher, BQUrlSchemeDispatcher, View.OnTouchListener, BQSleepSettingDispatcher, ActivityCompat.OnRequestPermissionsResultCallback
{
  private static final String LOG_TAG = "kizakura";
  private static final String META_DATA_KEY_ENABLE_MOVIEPLAYER = "jp.co.drecom.bisque.enable.movieplayer";
  private static final String META_DATA_KEY_ENABLE_TRANSPARENT = "jp.co.drecom.bisque.enable.glsurfaceview.transparent";
  private static final String META_DATA_KEY_GMS_APP_ID = "com.google.android.gms.games.APP_ID";
  private static final String META_DATA_KEY_GMS_VERSION = "com.google.android.gms.version";
  private BQHandler bqhandler = new BQHandler();
  private BQUpdate bqupdate;
  private BQUrlScheme bqurlscheme;
  private BQClipboard clipboard = new BQClipboard(this);
  private BQSleepSetting deviceSetting;
  private boolean enableGlSurfaceviewTransparent = true;
  private Handler handler = new Handler();
  private RelativeLayout layout;
  private BQPaymentBridge paymentBridge;
  private BQPlayGameService playGameService;
  
  static
  {
    System.loadLibrary("game");
  }
  
  private boolean canUseAchievement()
  {
    boolean bool2 = false;
    try
    {
      ApplicationInfo localApplicationInfo = getPackageManager().getApplicationInfo(getPackageName(), 128);
      boolean bool1 = bool2;
      if (localApplicationInfo != null)
      {
        bool1 = bool2;
        if (localApplicationInfo.metaData != null)
        {
          if (!localApplicationInfo.metaData.containsKey("com.google.android.gms.games.APP_ID")) {
            break label62;
          }
          bool1 = localApplicationInfo.metaData.containsKey("com.google.android.gms.version");
          if (!bool1) {
            break label62;
          }
          bool1 = true;
        }
      }
      return bool1;
      label62:
      return false;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return false;
  }
  
  private BQWebView getWebView(int paramInt)
  {
    int i = 0;
    while (i < this.layout.getChildCount())
    {
      if (this.layout.getChildAt(i).getClass() == BQWebView.class)
      {
        BQWebView localBQWebView = (BQWebView)this.layout.getChildAt(i);
        if (paramInt == localBQWebView.tag) {
          return localBQWebView;
        }
      }
      i += 1;
    }
    return null;
  }
  
  private void hideSystemUI()
  {
    if (Build.VERSION.SDK_INT >= 19) {
      this.mGLSurfaceView.setSystemUiVisibility(5894);
    }
  }
  
  private void initializeTypefaces()
  {
    BQTypefaces.registImplement(new BQTypefaces.Implement()
    {
      public Typeface getTypeface(String paramAnonymousString)
      {
        return Cocos2dxTypefaces.get(Cocos2dxActivity.getContext(), paramAnonymousString);
      }
    });
  }
  
  private boolean isEnableMoviePlayer()
  {
    boolean bool2 = false;
    try
    {
      ApplicationInfo localApplicationInfo = getPackageManager().getApplicationInfo(getPackageName(), 128);
      boolean bool1 = bool2;
      if (localApplicationInfo != null)
      {
        bool1 = bool2;
        if (localApplicationInfo.metaData != null)
        {
          bool1 = bool2;
          if (localApplicationInfo.metaData.containsKey("jp.co.drecom.bisque.enable.movieplayer")) {
            bool1 = localApplicationInfo.metaData.getBoolean("jp.co.drecom.bisque.enable.movieplayer");
          }
        }
      }
      return bool1;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return false;
  }
  
  private void snsAuthnicationCallbackInternal(Intent paramIntent, boolean paramBoolean)
  {
    if ((paramIntent == null) || (paramIntent.getData() == null)) {}
    String str2;
    String str3;
    Object localObject;
    do
    {
      do
      {
        return;
      } while (!paramIntent.getData().toString().startsWith("onepiecetc.gb://sns-connection"));
      str2 = paramIntent.getData().getQueryParameter("code");
      str3 = paramIntent.getData().getQueryParameter("operation");
      localObject = paramIntent.getData().getQueryParameter("token");
      String str1 = paramIntent.getData().getQueryParameter("name");
      paramIntent = (Intent)localObject;
      if (localObject == null) {
        paramIntent = "";
      }
      localObject = str1;
      if (str1 == null) {
        localObject = "";
      }
    } while ((str2 == null) || (str2.equals("")));
    AppConfigure.callbackSnsAuthnication(str2, str3, paramIntent, (String)localObject);
  }
  
  private void twitterCallbackInternal(Intent paramIntent, boolean paramBoolean)
  {
    if ((paramIntent == null) || (paramIntent.getData() == null)) {}
    do
    {
      do
      {
        return;
        str = getResources().getString(2131361869);
      } while (!paramIntent.getData().toString().startsWith(str));
      String str = paramIntent.getData().getQueryParameter("oauth_verifier");
      paramIntent = str;
      if (str == null) {
        paramIntent = "";
      }
      BQTwitterHelper.setVerifier(paramIntent);
    } while (!paramBoolean);
    BQTwitterHelper.gotOAuthVerifier(paramIntent);
    BQTwitterHelper.setExecutingBrowser(false);
  }
  
  public boolean addWebView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean, String paramString2, String paramString3)
  {
    BQWebView localBQWebView = new BQWebView(this, this, AppConfigure.getWebViewErrorTemplate());
    localBQWebView.setAuthInfo(paramBoolean, paramString2, paramString3);
    localBQWebView.getClass();
    if (paramInt7 == 1) {
      localBQWebView.bouncesOff();
    }
    for (;;)
    {
      localBQWebView.tag = paramInt5;
      paramString2 = new RelativeLayout.LayoutParams(paramInt3, paramInt4);
      paramString2.setMargins(paramInt1, paramInt2, 0, 0);
      this.layout.addView(localBQWebView, paramInt6, paramString2);
      localBQWebView.setOnTouchListener(this);
      localBQWebView.loadUrl(paramString1);
      if (!this.enableGlSurfaceviewTransparent) {
        localBQWebView.enableBringToFront(true);
      }
      return true;
      localBQWebView.getClass();
      if (paramInt7 == 2) {
        localBQWebView.bouncesOn();
      }
    }
  }
  
  public boolean addWebView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, String paramString2, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean, String paramString3, String paramString4)
  {
    BQWebView localBQWebView = new BQWebView(this, this, AppConfigure.getWebViewErrorTemplate());
    localBQWebView.setAuthInfo(paramBoolean, paramString3, paramString4);
    localBQWebView.getClass();
    if (paramInt7 == 1) {
      localBQWebView.bouncesOff();
    }
    for (;;)
    {
      localBQWebView.tag = paramInt5;
      paramString3 = new RelativeLayout.LayoutParams(paramInt3, paramInt4);
      paramString3.setMargins(paramInt1, paramInt2, 0, 0);
      this.layout.addView(localBQWebView, paramInt6, paramString3);
      localBQWebView.setOnTouchListener(this);
      try
      {
        paramString2 = paramString2.getBytes("UTF-8");
        localBQWebView.postUrl(paramString1, paramString2);
        if (!this.enableGlSurfaceviewTransparent) {
          localBQWebView.enableBringToFront(true);
        }
        return true;
      }
      catch (UnsupportedEncodingException paramString1) {}
      localBQWebView.getClass();
      if (paramInt7 == 2) {
        localBQWebView.bouncesOn();
      }
    }
    return false;
  }
  
  public boolean addWebView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, String[] paramArrayOfString1, String[] paramArrayOfString2, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean, String paramString2, String paramString3)
  {
    BQWebView localBQWebView = new BQWebView(this, this, AppConfigure.getWebViewErrorTemplate());
    localBQWebView.setAuthInfo(paramBoolean, paramString2, paramString3);
    localBQWebView.getClass();
    if (paramInt7 == 1) {
      localBQWebView.bouncesOff();
    }
    for (;;)
    {
      localBQWebView.tag = paramInt5;
      paramString2 = new RelativeLayout.LayoutParams(paramInt3, paramInt4);
      paramString2.setMargins(paramInt1, paramInt2, 0, 0);
      this.layout.addView(localBQWebView, paramInt6, paramString2);
      localBQWebView.setOnTouchListener(this);
      paramString2 = new HashMap();
      paramInt1 = 0;
      while (paramInt1 < paramArrayOfString1.length)
      {
        paramString2.put(paramArrayOfString1[paramInt1], paramArrayOfString2[paramInt1]);
        paramInt1 += 1;
      }
      localBQWebView.getClass();
      if (paramInt7 == 2) {
        localBQWebView.bouncesOn();
      }
    }
    localBQWebView.loadUrl(paramString1, paramString2);
    if (!this.enableGlSurfaceviewTransparent) {
      localBQWebView.enableBringToFront(true);
    }
    return true;
  }
  
  public void bouncesWebView(boolean paramBoolean, int paramInt)
  {
    BQWebView localBQWebView = getWebView(paramInt);
    if (localBQWebView == null) {
      return;
    }
    if (paramBoolean)
    {
      localBQWebView.bouncesOn();
      return;
    }
    localBQWebView.bouncesOff();
  }
  
  public void callbackForWebViewDidFailLoadWithError(String paramString)
  {
    BQWebView.nativeCallbackFuncForWebViewDidFailLoadWithError(paramString);
  }
  
  public void callbackForWebViewDidFinishLoad(final int paramInt)
  {
    runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQWebView.nativeCallbackFuncForWebViewDidFinishLoad(paramInt);
      }
    });
  }
  
  /* Error */
  public boolean callbackForWebViewShouldStartLoadWithRequest(final String paramString, final int paramInt)
  {
    // Byte code:
    //   0: new 34	com/namcobandaigames/spmoja010E/kizakura$2
    //   3: dup
    //   4: aload_0
    //   5: aload_1
    //   6: iload_2
    //   7: invokespecial 357	com/namcobandaigames/spmoja010E/kizakura$2:<init>	(Lcom/namcobandaigames/spmoja010E/kizakura;Ljava/lang/String;I)V
    //   10: astore 4
    //   12: aconst_null
    //   13: astore_1
    //   14: invokestatic 363	java/util/concurrent/Executors:newSingleThreadExecutor	()Ljava/util/concurrent/ExecutorService;
    //   17: astore 5
    //   19: aload 5
    //   21: astore_1
    //   22: aload 5
    //   24: aload 4
    //   26: invokeinterface 369 2 0
    //   31: astore 6
    //   33: aconst_null
    //   34: astore 4
    //   36: aload 5
    //   38: astore_1
    //   39: aload 6
    //   41: invokeinterface 375 1 0
    //   46: checkcast 31	com/namcobandaigames/spmoja010E/kizakura$1Result
    //   49: astore 6
    //   51: aload 6
    //   53: astore 4
    //   55: aload 5
    //   57: astore_1
    //   58: aload 4
    //   60: getfield 378	com/namcobandaigames/spmoja010E/kizakura$1Result:ret	Z
    //   63: istore_3
    //   64: aload 5
    //   66: ifnull +10 -> 76
    //   69: aload 5
    //   71: invokeinterface 381 1 0
    //   76: iload_3
    //   77: ireturn
    //   78: astore 6
    //   80: aload 5
    //   82: astore_1
    //   83: aload 6
    //   85: invokevirtual 384	java/lang/InterruptedException:printStackTrace	()V
    //   88: goto -33 -> 55
    //   91: astore 4
    //   93: aload_1
    //   94: ifnull +9 -> 103
    //   97: aload_1
    //   98: invokeinterface 381 1 0
    //   103: aload 4
    //   105: athrow
    //   106: astore 6
    //   108: aload 5
    //   110: astore_1
    //   111: aload 6
    //   113: invokevirtual 385	java/util/concurrent/ExecutionException:printStackTrace	()V
    //   116: goto -61 -> 55
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	119	0	this	kizakura
    //   0	119	1	paramString	String
    //   0	119	2	paramInt	int
    //   63	14	3	bool	boolean
    //   10	49	4	localObject1	Object
    //   91	13	4	localObject2	Object
    //   17	92	5	localExecutorService	java.util.concurrent.ExecutorService
    //   31	21	6	localObject3	Object
    //   78	6	6	localInterruptedException	InterruptedException
    //   106	6	6	localExecutionException	java.util.concurrent.ExecutionException
    // Exception table:
    //   from	to	target	type
    //   39	51	78	java/lang/InterruptedException
    //   14	19	91	finally
    //   22	33	91	finally
    //   39	51	91	finally
    //   58	64	91	finally
    //   83	88	91	finally
    //   111	116	91	finally
    //   39	51	106	java/util/concurrent/ExecutionException
  }
  
  public void checkUpdate(boolean paramBoolean, int paramInt, String paramString)
  {
    this.bqupdate.checkUpdate(paramBoolean, paramInt, paramString);
  }
  
  public void clearCache(int paramInt)
  {
    BQWebView localBQWebView = getWebView(paramInt);
    if (localBQWebView == null) {
      return;
    }
    localBQWebView.clearCache(true);
  }
  
  public void clearCookie(int paramInt)
  {
    if (getWebView(paramInt) == null) {
      return;
    }
    CookieManager localCookieManager = CookieManager.getInstance();
    CookieSyncManager localCookieSyncManager = CookieSyncManager.createInstance(getApplicationContext());
    localCookieSyncManager.startSync();
    localCookieManager.removeAllCookie();
    localCookieManager.removeSessionCookie();
    localCookieSyncManager.stopSync();
    localCookieSyncManager.sync();
  }
  
  public void enableCache(boolean paramBoolean, int paramInt)
  {
    Object localObject = getWebView(paramInt);
    if (localObject == null) {
      return;
    }
    localObject = ((BQWebView)localObject).getSettings();
    if (paramBoolean) {}
    for (paramInt = -1;; paramInt = 2)
    {
      ((WebSettings)localObject).setCacheMode(paramInt);
      return;
    }
  }
  
  public void enableCookie(boolean paramBoolean, int paramInt)
  {
    if (getWebView(paramInt) == null) {
      return;
    }
    CookieManager.getInstance().setAcceptCookie(paramBoolean);
  }
  
  public void enableWebView(boolean paramBoolean, int paramInt)
  {
    BQWebView localBQWebView = getWebView(paramInt);
    if (localBQWebView == null) {
      return;
    }
    if (paramBoolean)
    {
      localBQWebView.setVisibility(0);
      return;
    }
    localBQWebView.setVisibility(4);
  }
  
  public void execOpenApplicationDetailsSettings()
  {
    this.bqurlscheme.execOpenApplicationDetailsSettings();
  }
  
  public void execReview(String paramString)
  {
    this.bqupdate.execReview(paramString);
  }
  
  public void execUpdate(String paramString)
  {
    this.bqupdate.execUpdate(paramString);
  }
  
  public void execUrlScheme(String paramString)
  {
    this.bqurlscheme.execUrlScheme(paramString);
  }
  
  public void executeJsInWebView(String paramString, int paramInt)
  {
    BQWebView localBQWebView = getWebView(paramInt);
    if (localBQWebView == null) {
      return;
    }
    localBQWebView.loadUrl("javascript:" + paramString);
  }
  
  public Handler getHandler()
  {
    return this.handler;
  }
  
  public Point getRealSize()
  {
    Display localDisplay = getWindowManager().getDefaultDisplay();
    Point localPoint = new Point(0, 0);
    if (Build.VERSION.SDK_INT >= 17) {
      localDisplay.getRealSize(localPoint);
    }
    while (Build.VERSION.SDK_INT < 13) {
      return localPoint;
    }
    try
    {
      Method localMethod1 = Display.class.getMethod("getRawWidth", new Class[0]);
      Method localMethod2 = Display.class.getMethod("getRawHeight", new Class[0]);
      localPoint.set(((Integer)localMethod1.invoke(localDisplay, new Object[0])).intValue(), ((Integer)localMethod2.invoke(localDisplay, new Object[0])).intValue());
      return localPoint;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return localPoint;
  }
  
  public String getStringFromClipboard()
  {
    return this.clipboard.getStringFromClipboard();
  }
  
  public void handlerPost(final long paramLong1, long paramLong2, int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return;
    case 0: 
      this.bqhandler.handlerPost(paramLong1, paramLong2);
      return;
    }
    runOnGLThread(new Runnable()
    {
      public void run()
      {
        kizakura.this.bqhandler.handlerPost(paramLong1, this.val$_data);
      }
    });
  }
  
  public boolean isWideScreen()
  {
    Point localPoint = getRealSize();
    return localPoint.x / 3 == localPoint.y / 4;
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (this.playGameService != null) {
      this.playGameService.onActivityResult(paramInt1, paramInt2, paramIntent);
    }
    if ((this.paymentBridge != null) && (this.paymentBridge.handleActivityResult(paramInt1, paramInt2, paramIntent))) {}
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (isWideScreen())
    {
      getWindow().addFlags(1024);
      getWindow().getDecorView().setSystemUiVisibility(4102);
    }
    if ((ExternalServiceStateModel.getState("enable_hockey_app_on_create", this)) && (ExternalServiceStateModel.getState("enable_hockey_app", this)))
    {
      ExternalServiceStateModel.disableState("enable_hockey_app_on_create", this);
      Constants.loadFromContext(this);
      HockeyApp.setUpBreakpad(Constants.FILES_PATH);
    }
    try
    {
      NativeCrashManager.handleDumpFiles(this, getPackageManager().getApplicationInfo(getPackageName(), 128).metaData.getString("HOCKEYAPP_ID"), AppConfigure.getUserId(), null);
      BQPaymentBridge.getBridge().setContext(this);
      BQPaymentBridge.getBridge().setup();
      this.bqupdate = new BQUpdate(this);
      this.bqurlscheme = new BQUrlScheme(this);
      this.deviceSetting = new BQSleepSetting(this);
      if (canUseAchievement()) {
        this.playGameService = new BQPlayGameService(this);
      }
      twitterCallbackInternal(getIntent(), false);
      snsAuthnicationCallbackInternal(getIntent(), false);
      BQPermissionHelper.getInstance().initialize(this);
      initializeTypefaces();
      return;
    }
    catch (PackageManager.NameNotFoundException paramBundle)
    {
      for (;;) {}
    }
  }
  
  public Cocos2dxGLSurfaceView onCreateView()
  {
    this.layout = new RelativeLayout(this);
    try
    {
      localObject = getPackageManager().getApplicationInfo(getPackageName(), 128);
      if ((localObject != null) && (((ApplicationInfo)localObject).metaData != null) && (((ApplicationInfo)localObject).metaData.containsKey("jp.co.drecom.bisque.enable.glsurfaceview.transparent"))) {
        this.enableGlSurfaceviewTransparent = ((ApplicationInfo)localObject).metaData.getBoolean("jp.co.drecom.bisque.enable.glsurfaceview.transparent");
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        Object localObject;
        ViewGroup.LayoutParams localLayoutParams;
        this.enableGlSurfaceviewTransparent = true;
      }
    }
    localObject = new Cocos2dxGLSurfaceView(this, this.enableGlSurfaceviewTransparent);
    localLayoutParams = new ViewGroup.LayoutParams(-1, -1);
    this.layout.addView((View)localObject, 0, localLayoutParams);
    ((Cocos2dxGLSurfaceView)localObject).setCocos2dxRenderer(new Cocos2dxRenderer());
    ((Cocos2dxGLSurfaceView)localObject).setOnTouchListener(this);
    setContentView(this.layout);
    BQAppPlatformManager.setContext(this);
    BQTouchDispatcherNotify.setDispatcher(this, this.handler);
    BQWebViewDispatcherNotify.setDispatcher(this, this.handler);
    BQUpdateDispatcherNotify.setDispatcher(this, this.handler);
    BQClipboardDispatcherNotify.setDispatcher(this, this.handler);
    BQHandlerDispatcherNotify.setDispatcher(this, this.handler);
    BQUrlSchemeDispatcherNotify.setDispatcher(this, this.handler);
    BQSleepSettingDispatcherNotify.setDispatcher(this, this.handler);
    DRToastManager.initialize(this, this.handler);
    if (isEnableMoviePlayer()) {
      DRMoviePlayerManager.initialize(this, this.handler, this.layout);
    }
    HandlerJava.initialize(this, this.handler, (GLSurfaceView)localObject);
    BQTwitterHelper.setHttpTimeout(getResources().getInteger(2131230727), getResources().getInteger(2131230728));
    BQJNIHelper.initNativeContext(this);
    return (Cocos2dxGLSurfaceView)localObject;
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    if (this.paymentBridge != null)
    {
      this.paymentBridge.dispose();
      this.paymentBridge = null;
    }
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) || (paramInt == 82)) {
      return this.mGLSurfaceView.onKeyDown(paramInt, paramKeyEvent);
    }
    return false;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) || (paramInt == 82)) {
      return this.mGLSurfaceView.onKeyUp(paramInt, paramKeyEvent);
    }
    return false;
  }
  
  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    twitterCallbackInternal(paramIntent, true);
    snsAuthnicationCallbackInternal(paramIntent, true);
    setIntent(paramIntent);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    BQPermissionHelper.getInstance().onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  protected void onResume()
  {
    super.onResume();
    if ((ExternalServiceStateModel.getState("enable_hockey_app_on_resume", this)) && (ExternalServiceStateModel.getState("enable_hockey_app", this))) {
      ExternalServiceStateModel.disableState("enable_hockey_app_on_resume", this);
    }
    try
    {
      CrashManager.register(this, getPackageManager().getApplicationInfo(getPackageName(), 128).metaData.getString("HOCKEYAPP_ID"), new CrashManagerListener()
      {
        public String getUserID()
        {
          return AppConfigure.getUserId();
        }
        
        public boolean shouldAutoUploadCrashes()
        {
          return true;
        }
      });
      if ((!BQNotificationManagerHelper.getInstance().handleNotification(getIntent())) || (BQTwitterHelper.isExecutingBrowser()))
      {
        BQTwitterHelper.closeAuthorizeURL();
        BQTwitterHelper.setExecutingBrowser(false);
      }
      if (this.playGameService != null) {
        this.playGameService.onResume();
      }
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;) {}
    }
  }
  
  protected void onStart()
  {
    super.onStart();
    if (this.playGameService != null) {
      this.playGameService.onStart();
    }
  }
  
  protected void onStop()
  {
    if (this.playGameService != null) {
      this.playGameService.onStop();
    }
    super.onStop();
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    int i;
    if (this.enableGlSurfaceviewTransparent)
    {
      this.mGLSurfaceView.onTouchEvent(paramMotionEvent);
      i = 0;
      while (i < this.layout.getChildCount())
      {
        if (this.layout.getChildAt(i).getClass() == BQWebView.class)
        {
          paramView = (BQWebView)this.layout.getChildAt(i);
          if (paramView.getVisibility() != 4) {}
        }
        else
        {
          i += 1;
          continue;
        }
        paramView.onTouchEvent(paramMotionEvent);
      }
    }
    BQWebView localBQWebView;
    for (;;)
    {
      return true;
      if (paramView == this.mGLSurfaceView)
      {
        this.mGLSurfaceView.onTouchEvent(paramMotionEvent);
        return true;
      }
      i = 0;
      while (i < this.layout.getChildCount())
      {
        if (this.layout.getChildAt(i).getClass() == BQWebView.class)
        {
          localBQWebView = (BQWebView)this.layout.getChildAt(i);
          if ((paramView == localBQWebView) && (localBQWebView.getVisibility() != 4)) {
            break label163;
          }
        }
        i += 1;
      }
    }
    label163:
    localBQWebView.onTouchEvent(paramMotionEvent);
    return true;
  }
  
  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if ((paramBoolean) && (isWideScreen())) {
      hideSystemUI();
    }
  }
  
  public void reloadWebView(int paramInt)
  {
    BQWebView localBQWebView = getWebView(paramInt);
    if (localBQWebView == null) {
      return;
    }
    if (localBQWebView.canGoBack())
    {
      localBQWebView.goBack();
      return;
    }
    localBQWebView.reload();
  }
  
  public void removeWebView(int paramInt)
  {
    int i = 0;
    while (i < this.layout.getChildCount())
    {
      if (this.layout.getChildAt(i).getClass() == BQWebView.class)
      {
        BQWebView localBQWebView = (BQWebView)this.layout.getChildAt(i);
        if (paramInt == localBQWebView.tag)
        {
          this.layout.removeViewAt(i);
          localBQWebView.stopLoading();
          localBQWebView.clearCache(false);
          localBQWebView.clearHistory();
          localBQWebView.setWebChromeClient(null);
          localBQWebView.setWebViewClient(null);
          localBQWebView.destroy();
        }
      }
      i += 1;
    }
  }
  
  public void requestWebView(String paramString1, int paramInt, boolean paramBoolean, String paramString2, String paramString3)
  {
    BQWebView localBQWebView = getWebView(paramInt);
    if (localBQWebView == null) {
      return;
    }
    localBQWebView.setAuthInfo(paramBoolean, paramString2, paramString3);
    localBQWebView.loadUrl(paramString1);
  }
  
  public void requestWebView(String paramString1, String paramString2, int paramInt, boolean paramBoolean, String paramString3, String paramString4)
  {
    BQWebView localBQWebView = getWebView(paramInt);
    if (localBQWebView == null) {
      return;
    }
    localBQWebView.setAuthInfo(paramBoolean, paramString3, paramString4);
    try
    {
      paramString2 = paramString2.getBytes("UTF-8");
      localBQWebView.postUrl(paramString1, paramString2);
      return;
    }
    catch (UnsupportedEncodingException paramString1) {}
  }
  
  public void requestWebView(String paramString1, String[] paramArrayOfString1, String[] paramArrayOfString2, int paramInt, boolean paramBoolean, String paramString2, String paramString3)
  {
    BQWebView localBQWebView = getWebView(paramInt);
    if (localBQWebView == null) {
      return;
    }
    localBQWebView.setAuthInfo(paramBoolean, paramString2, paramString3);
    paramString2 = new HashMap();
    paramInt = 0;
    while (paramInt < paramArrayOfString1.length)
    {
      paramString2.put(paramArrayOfString1[paramInt], paramArrayOfString2[paramInt]);
      paramInt += 1;
    }
    localBQWebView.loadUrl(paramString1, paramString2);
  }
  
  public void setBQPaymentBridge(BQPaymentBridge paramBQPaymentBridge)
  {
    this.paymentBridge = paramBQPaymentBridge;
  }
  
  public void setDeviceSleep(boolean paramBoolean)
  {
    this.deviceSetting.setDeviceSleep(paramBoolean);
  }
  
  public void setOrderWebView(int paramInt1, int paramInt2) {}
  
  public void setRectWebView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    BQWebView localBQWebView = getWebView(paramInt5);
    if (localBQWebView == null) {
      return;
    }
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(paramInt3, paramInt4);
    localLayoutParams.setMargins(paramInt1, paramInt2, 0, 0);
    localBQWebView.setLayoutParams(localLayoutParams);
    localBQWebView.requestLayout();
  }
  
  public void setStringToClipboard(String paramString)
  {
    this.clipboard.setStringToClipboard(paramString);
  }
  
  public void setUserInteractionEnabled(boolean paramBoolean, int paramInt)
  {
    if (paramInt == 999) {
      this.mGLSurfaceView.setInteraction(paramBoolean);
    }
    BQWebView localBQWebView;
    do
    {
      return;
      localBQWebView = getWebView(paramInt);
    } while (localBQWebView == null);
    localBQWebView.setInteraction(paramBoolean);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\namcobandaigames\spmoja010E\kizakura.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */