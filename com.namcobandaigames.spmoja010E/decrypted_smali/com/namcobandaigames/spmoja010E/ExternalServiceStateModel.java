package com.namcobandaigames.spmoja010E;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ExternalServiceStateModel
{
  static final String PREFERENCE_KEY = "external_state";
  
  public static void disableState(String paramString, Context paramContext)
  {
    setState(paramString, false, paramContext);
  }
  
  public static void enableState(String paramString, Context paramContext)
  {
    setState(paramString, true, paramContext);
  }
  
  public static boolean getState(String paramString, Context paramContext)
  {
    return paramContext.getSharedPreferences("external_state", 0).getBoolean(paramString, true);
  }
  
  public static void setState(String paramString, boolean paramBoolean, Context paramContext)
  {
    paramContext = paramContext.getSharedPreferences("external_state", 0).edit();
    paramContext.putBoolean(paramString, paramBoolean);
    paramContext.commit();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\com\namcobandaigames\spmoja010E\ExternalServiceStateModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */