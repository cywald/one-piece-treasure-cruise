package org.cocos2dx.lib;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class Cocos2dxEditBoxDialog
  extends Dialog
{
  private Context ctx;
  private final int kCCTextAlignmentCenter = 1;
  private final int kCCTextAlignmentLeft = 0;
  private final int kCCTextAlignmentRight = 2;
  private final int kEditBoxInputFlagInitialCapsAllCharacters = 4;
  private final int kEditBoxInputFlagInitialCapsSentence = 3;
  private final int kEditBoxInputFlagInitialCapsWord = 2;
  private final int kEditBoxInputFlagPassword = 0;
  private final int kEditBoxInputFlagSensitive = 1;
  private final int kEditBoxInputModeAny = 0;
  private final int kEditBoxInputModeDecimal = 5;
  private final int kEditBoxInputModeEmailAddr = 1;
  private final int kEditBoxInputModeNumeric = 2;
  private final int kEditBoxInputModePhoneNumber = 3;
  private final int kEditBoxInputModeSingleLine = 6;
  private final int kEditBoxInputModeUrl = 4;
  private final int kKeyboardReturnTypeDefault = 0;
  private final int kKeyboardReturnTypeDone = 1;
  private final int kKeyboardReturnTypeGo = 4;
  private final int kKeyboardReturnTypeSearch = 3;
  private final int kKeyboardReturnTypeSend = 2;
  private int mAlignment = 3;
  private boolean mDialogCocosDefault = true;
  private int mDialogFontBackgroundColor = -1;
  private int mDialogFontColor = -16777216;
  private boolean mDialogGrayout = true;
  private int mDialogPosx = 0;
  private int mDialogPosy = 0;
  private int mDialogSizeh = 0;
  private int mDialogSizew = 0;
  private int mFontColor = -1;
  private int mFontColorPlaceHolder = -5855578;
  private String mFontName = "";
  private String mFontNamePlaceHolder = "";
  private float mFontSize = 0.0F;
  private float mFontSizePlaceHolder = 0.0F;
  private EditText mInputEditText;
  private final int mInputFlag;
  private int mInputFlagConstraints;
  private final int mInputMode;
  private int mInputModeContraints;
  private boolean mIsMultiline;
  private final int mMaxLength;
  private final String mMessage;
  private final int mReturnType;
  private TextView mTextViewTitle;
  private final String mTitle;
  
  public Cocos2dxEditBoxDialog(Context paramContext, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super(paramContext, 16973841);
    this.mTitle = paramString1;
    this.mMessage = paramString2;
    this.mInputMode = paramInt1;
    this.mInputFlag = paramInt2;
    this.mReturnType = paramInt3;
    this.mMaxLength = paramInt4;
    this.ctx = paramContext;
  }
  
  public Cocos2dxEditBoxDialog(Context paramContext, String paramString1, String paramString2, String paramString3, float paramFloat1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString4, float paramFloat2, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, boolean paramBoolean, int paramInt10, int paramInt11, int paramInt12, int paramInt13)
  {
    super(paramContext);
    super.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    super.getWindow().requestFeature(1);
    super.getWindow().setFlags(0, 2);
    this.ctx = paramContext;
    this.mTitle = paramString1;
    this.mMessage = paramString2;
    this.mFontName = paramString3;
    this.mFontSize = paramFloat1;
    this.mInputMode = paramInt1;
    this.mInputFlag = paramInt2;
    this.mReturnType = paramInt3;
    this.mMaxLength = paramInt4;
    this.mFontNamePlaceHolder = paramString4;
    this.mFontSizePlaceHolder = paramFloat2;
    this.mAlignment = convCCAlignmentToGravity(paramInt5);
    this.mDialogPosx = paramInt6;
    this.mDialogPosy = paramInt7;
    this.mDialogSizew = paramInt8;
    this.mDialogSizeh = paramInt9;
    this.mDialogGrayout = paramBoolean;
    this.mFontColor = convColor3BToColor(paramInt10);
    this.mFontColorPlaceHolder = convColor3BToColor(paramInt11);
    this.mDialogFontColor = convColor3BToColor(paramInt12);
    this.mDialogFontBackgroundColor = convColor3BToColor(paramInt13);
    if ((this.mDialogSizew == 0) && (this.mDialogSizeh == 0)) {}
    for (paramBoolean = true;; paramBoolean = false)
    {
      this.mDialogCocosDefault = paramBoolean;
      return;
    }
  }
  
  private void closeKeyboard()
  {
    ((InputMethodManager)getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.mInputEditText.getWindowToken(), 0);
  }
  
  private int convCCAlignmentToGravity(int paramInt)
  {
    if (paramInt == 0) {}
    do
    {
      return 3;
      if (paramInt == 1) {
        return 17;
      }
    } while (paramInt != 2);
    return 5;
  }
  
  private int convColor3BToColor(int paramInt)
  {
    return 0x0 | 0xFF000000 | 0xFFFFFF & paramInt;
  }
  
  private int convertDipsToPixels(float paramFloat)
  {
    return Math.round(paramFloat * getContext().getResources().getDisplayMetrics().density);
  }
  
  private void openKeyboard()
  {
    ((InputMethodManager)getContext().getSystemService("input_method")).showSoftInput(this.mInputEditText, 0);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if ((this.mDialogCocosDefault) || ((!this.mDialogCocosDefault) && (this.mDialogGrayout))) {
      getWindow().setBackgroundDrawable(new ColorDrawable(Integer.MIN_VALUE));
    }
    LinearLayout localLinearLayout = new LinearLayout(getContext());
    localLinearLayout.setOrientation(1);
    paramBundle = ((WindowManager)this.ctx.getSystemService("window")).getDefaultDisplay();
    LinearLayout.LayoutParams localLayoutParams2 = new LinearLayout.LayoutParams(paramBundle.getWidth(), paramBundle.getHeight());
    this.mTextViewTitle = new TextView(getContext());
    int i;
    if (this.mDialogCocosDefault)
    {
      paramBundle = new LinearLayout.LayoutParams(-2, -2);
      i = convertDipsToPixels(10.0F);
      paramBundle.rightMargin = i;
      paramBundle.leftMargin = i;
      this.mTextViewTitle.setTextSize(1, 20.0F);
      localLinearLayout.addView(this.mTextViewTitle, paramBundle);
    }
    this.mInputEditText = new EditText(getContext());
    if (this.mDialogCocosDefault)
    {
      paramBundle = new LinearLayout.LayoutParams(-1, -2);
      i = convertDipsToPixels(10.0F);
      paramBundle.rightMargin = i;
      paramBundle.leftMargin = i;
      localLinearLayout.addView(this.mInputEditText, paramBundle);
      setContentView(localLinearLayout, localLayoutParams2);
      getWindow().addFlags(1024);
      this.mTextViewTitle.setText(this.mTitle);
      this.mInputEditText.setText(this.mMessage);
      i = this.mInputEditText.getImeOptions();
      this.mInputEditText.setImeOptions(0x10000000 | i);
      i = this.mInputEditText.getImeOptions();
      switch (this.mInputMode)
      {
      default: 
        label348:
        if (this.mIsMultiline) {
          this.mInputModeContraints |= 0x20000;
        }
        this.mInputEditText.setInputType(this.mInputModeContraints | this.mInputFlagConstraints);
        switch (this.mInputFlag)
        {
        default: 
          label420:
          this.mInputEditText.setInputType(this.mInputFlagConstraints | this.mInputModeContraints);
          switch (this.mReturnType)
          {
          default: 
            this.mInputEditText.setImeOptions(i | 0x1);
          }
          break;
        }
        break;
      }
    }
    for (;;)
    {
      if (this.mMaxLength > 0) {
        this.mInputEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(this.mMaxLength) });
      }
      new Handler().postDelayed(new Runnable()
      {
        public void run()
        {
          Cocos2dxEditBoxDialog.this.mInputEditText.requestFocus();
          Cocos2dxEditBoxDialog.this.mInputEditText.setSelection(Cocos2dxEditBoxDialog.this.mInputEditText.length());
          Cocos2dxEditBoxDialog.this.openKeyboard();
        }
      }, 200L);
      this.mInputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener()
      {
        public boolean onEditorAction(TextView paramAnonymousTextView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
        {
          if ((paramAnonymousInt != 0) || ((paramAnonymousInt == 0) && (paramAnonymousKeyEvent != null) && (paramAnonymousKeyEvent.getAction() == 0)))
          {
            Cocos2dxHelper.setEditTextDialogResult(Cocos2dxEditBoxDialog.this.mInputEditText.getText().toString());
            Cocos2dxEditBoxDialog.this.closeKeyboard();
            Cocos2dxEditBoxDialog.this.dismiss();
            return true;
          }
          return false;
        }
      });
      setOnDismissListener(new DialogInterface.OnDismissListener()
      {
        public void onDismiss(DialogInterface paramAnonymousDialogInterface) {}
      });
      return;
      LinearLayout.LayoutParams localLayoutParams1 = new LinearLayout.LayoutParams(-2, -2);
      if (this.mFontName != null) {}
      for (;;)
      {
        try
        {
          if (!this.mFontName.startsWith("/")) {
            continue;
          }
          paramBundle = Typeface.createFromFile(this.mFontName);
          if (paramBundle != null) {
            this.mInputEditText.setTypeface(paramBundle);
          }
        }
        catch (Exception paramBundle)
        {
          paramBundle = Typeface.create(this.mFontName, 0);
          if (paramBundle == null) {
            continue;
          }
          this.mInputEditText.setTypeface(paramBundle);
          continue;
        }
        if (this.mFontSize > 0.0F) {
          this.mInputEditText.setTextSize(0, this.mFontSize);
        }
        this.mInputEditText.setTextColor(this.mDialogFontColor);
        localLayoutParams1.leftMargin = this.mDialogPosx;
        localLayoutParams1.topMargin = this.mDialogPosy;
        localLayoutParams1.width = this.mDialogSizew;
        localLayoutParams1.height = this.mDialogSizeh;
        this.mInputEditText.setBackgroundColor(this.mDialogFontBackgroundColor);
        this.mInputEditText.setGravity(this.mAlignment | 0x10);
        this.mInputEditText.setWidth(this.mDialogSizew);
        this.mInputEditText.setHeight(this.mDialogSizeh);
        this.mInputEditText.setPadding(0, 0, 0, 0);
        this.mInputEditText.setHint(this.mTitle);
        this.mInputEditText.setHintTextColor(this.mFontColorPlaceHolder);
        paramBundle = localLayoutParams1;
        break;
        paramBundle = Typeface.createFromAsset(this.ctx.getAssets(), this.mFontName);
      }
      this.mInputModeContraints = 131073;
      break label348;
      this.mInputModeContraints = 33;
      break label348;
      this.mInputModeContraints = 4098;
      break label348;
      this.mInputModeContraints = 3;
      break label348;
      this.mInputModeContraints = 17;
      break label348;
      this.mInputModeContraints = 12290;
      break label348;
      this.mInputModeContraints = 1;
      break label348;
      this.mInputFlagConstraints = 129;
      break label420;
      this.mInputFlagConstraints = 524288;
      break label420;
      this.mInputFlagConstraints = 8192;
      break label420;
      this.mInputFlagConstraints = 16384;
      break label420;
      this.mInputFlagConstraints = 4096;
      break label420;
      this.mInputEditText.setImeOptions(i | 0x1);
      continue;
      this.mInputEditText.setImeOptions(i | 0x6);
      continue;
      this.mInputEditText.setImeOptions(i | 0x4);
      continue;
      this.mInputEditText.setImeOptions(i | 0x3);
      continue;
      this.mInputEditText.setImeOptions(i | 0x2);
    }
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      Cocos2dxHelper.setEditTextDialogResult(this.mInputEditText.getText().toString());
      closeKeyboard();
      dismiss();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\cocos2dx\lib\Cocos2dxEditBoxDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */