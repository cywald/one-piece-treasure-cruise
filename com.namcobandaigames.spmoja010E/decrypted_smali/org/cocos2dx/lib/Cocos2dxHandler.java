package org.cocos2dx.lib;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;

public class Cocos2dxHandler
  extends Handler
{
  public static final int HANDLER_HIDE_EDITBOX_DIALOG = 3;
  public static final int HANDLER_SHOW_DIALOG = 1;
  public static final int HANDLER_SHOW_EDITBOX_DIALOG = 2;
  private Cocos2dxEditBoxDialog cocos2dxEditBoxDialog = null;
  private WeakReference<Cocos2dxActivity> mActivity;
  
  public Cocos2dxHandler(Cocos2dxActivity paramCocos2dxActivity)
  {
    this.mActivity = new WeakReference(paramCocos2dxActivity);
  }
  
  private void hideEditBoxDialog()
  {
    if (this.cocos2dxEditBoxDialog != null) {
      this.cocos2dxEditBoxDialog.dismiss();
    }
    this.cocos2dxEditBoxDialog = null;
  }
  
  private void showDialog(Message paramMessage)
  {
    Cocos2dxActivity localCocos2dxActivity = (Cocos2dxActivity)this.mActivity.get();
    paramMessage = (DialogMessage)paramMessage.obj;
    new AlertDialog.Builder(localCocos2dxActivity).setTitle(paramMessage.titile).setMessage(paramMessage.message).setPositiveButton("Ok", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {}
    }).create().show();
  }
  
  private void showEditBoxDialog(Message paramMessage)
  {
    paramMessage = (EditBoxMessage)paramMessage.obj;
    this.cocos2dxEditBoxDialog = new Cocos2dxEditBoxDialog((Context)this.mActivity.get(), paramMessage.title, paramMessage.content, paramMessage.fontName, paramMessage.fontSize, paramMessage.inputMode, paramMessage.inputFlag, paramMessage.returnType, paramMessage.maxLength, paramMessage.fontNamePlaceHolder, paramMessage.fontSizePlaceHolder, paramMessage.alignment, paramMessage.dialogPosx, paramMessage.dialogPosy, paramMessage.dialogSizew, paramMessage.dialogSizeh, paramMessage.dialogGrayout, paramMessage.fontColor, paramMessage.fontColorPlaceHolder, paramMessage.dialogFontColor, paramMessage.dialogFontBackgroundColor);
    this.cocos2dxEditBoxDialog.show();
  }
  
  public void handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default: 
      return;
    case 1: 
      showDialog(paramMessage);
      return;
    case 2: 
      showEditBoxDialog(paramMessage);
      return;
    }
    hideEditBoxDialog();
  }
  
  public static class DialogMessage
  {
    public String message;
    public String titile;
    
    public DialogMessage(String paramString1, String paramString2)
    {
      this.titile = paramString1;
      this.message = paramString2;
    }
  }
  
  public static class EditBoxMessage
  {
    public int alignment;
    public String content;
    public int dialogFontBackgroundColor;
    public int dialogFontColor;
    public boolean dialogGrayout;
    public int dialogPosx;
    public int dialogPosy;
    public int dialogSizeh;
    public int dialogSizew;
    public int fontColor;
    public int fontColorPlaceHolder;
    public String fontName;
    public String fontNamePlaceHolder;
    public float fontSize;
    public float fontSizePlaceHolder;
    public int inputFlag;
    public int inputMode;
    public int maxLength;
    public int returnType;
    public String title;
    
    public EditBoxMessage(String paramString1, String paramString2, String paramString3, float paramFloat1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString4, float paramFloat2, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, boolean paramBoolean, int paramInt10, int paramInt11, int paramInt12, int paramInt13)
    {
      this.content = paramString2;
      this.title = paramString1;
      this.fontName = paramString3;
      this.fontSize = paramFloat1;
      this.inputMode = paramInt1;
      this.inputFlag = paramInt2;
      this.returnType = paramInt3;
      this.maxLength = paramInt4;
      this.fontNamePlaceHolder = paramString4;
      this.fontSizePlaceHolder = paramFloat2;
      this.alignment = paramInt5;
      this.dialogPosx = paramInt6;
      this.dialogPosy = paramInt7;
      this.dialogSizew = paramInt8;
      this.dialogSizeh = paramInt9;
      this.dialogGrayout = paramBoolean;
      this.fontColor = paramInt10;
      this.fontColorPlaceHolder = paramInt11;
      this.dialogFontColor = paramInt12;
      this.dialogFontBackgroundColor = paramInt13;
    }
  }
  
  public static class termEditing {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\cocos2dx\lib\Cocos2dxHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */