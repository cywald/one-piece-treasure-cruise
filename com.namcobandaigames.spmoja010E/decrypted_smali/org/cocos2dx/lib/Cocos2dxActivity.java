package org.cocos2dx.lib;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

public abstract class Cocos2dxActivity
  extends Activity
  implements Cocos2dxHelper.Cocos2dxHelperListener
{
  private static final String TAG = Cocos2dxActivity.class.getSimpleName();
  private static Context sContext = null;
  protected Message editBoxMessage;
  protected Cocos2dxGLSurfaceView mGLSurfaceView;
  protected Cocos2dxHandler mHandler;
  
  public static Context getContext()
  {
    return sContext;
  }
  
  public void disposeEditBoxDialog()
  {
    this.editBoxMessage = null;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    sContext = this;
    this.mHandler = new Cocos2dxHandler(this);
    this.mGLSurfaceView = onCreateView();
    this.editBoxMessage = null;
    Cocos2dxHelper.init(this, this);
  }
  
  public Cocos2dxGLSurfaceView onCreateView()
  {
    Object localObject1 = new ViewGroup.LayoutParams(-1, -1);
    FrameLayout localFrameLayout = new FrameLayout(this);
    localFrameLayout.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    Object localObject2 = new ViewGroup.LayoutParams(-1, -2);
    localObject1 = new Cocos2dxEditText(this);
    ((Cocos2dxEditText)localObject1).setLayoutParams((ViewGroup.LayoutParams)localObject2);
    localFrameLayout.addView((View)localObject1);
    localObject2 = new Cocos2dxGLSurfaceView(this, true);
    localFrameLayout.addView((View)localObject2);
    ((Cocos2dxGLSurfaceView)localObject2).setCocos2dxRenderer(new Cocos2dxRenderer());
    ((Cocos2dxGLSurfaceView)localObject2).setCocos2dxEditText((Cocos2dxEditText)localObject1);
    setContentView(localFrameLayout);
    return (Cocos2dxGLSurfaceView)localObject2;
  }
  
  protected void onPause()
  {
    super.onPause();
    Cocos2dxHelper.onPause();
    this.mGLSurfaceView.onPause();
  }
  
  protected void onResume()
  {
    super.onResume();
    Cocos2dxHelper.onResume();
    this.mGLSurfaceView.onResume();
  }
  
  public void runOnGLThread(Runnable paramRunnable)
  {
    this.mGLSurfaceView.queueEvent(paramRunnable);
  }
  
  public void showDialog(String paramString1, String paramString2)
  {
    Message localMessage = new Message();
    localMessage.what = 1;
    localMessage.obj = new Cocos2dxHandler.DialogMessage(paramString1, paramString2);
    this.mHandler.sendMessage(localMessage);
  }
  
  public void showEditTextDialog(String paramString1, String paramString2, String paramString3, float paramFloat1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString4, float paramFloat2, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, boolean paramBoolean, int paramInt10, int paramInt11, int paramInt12, int paramInt13)
  {
    if (this.editBoxMessage != null) {
      return;
    }
    this.editBoxMessage = new Message();
    this.editBoxMessage.what = 2;
    this.editBoxMessage.obj = new Cocos2dxHandler.EditBoxMessage(paramString1, paramString2, paramString3, paramFloat1, paramInt1, paramInt2, paramInt3, paramInt4, paramString4, paramFloat2, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9, paramBoolean, paramInt10, paramInt11, paramInt12, paramInt13);
    this.mHandler.sendMessage(this.editBoxMessage);
  }
  
  public void termEditing()
  {
    if (this.editBoxMessage == null) {
      return;
    }
    Message localMessage = new Message();
    localMessage.what = 3;
    localMessage.obj = new Cocos2dxHandler.termEditing();
    this.mHandler.sendMessage(localMessage);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\cocos2dx\lib\Cocos2dxActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */