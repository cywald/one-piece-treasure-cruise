package org.cocos2dx.lib;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class Cocos2dxHelper
{
  private static boolean sAccelerometerEnabled;
  private static AssetManager sAssetManager;
  private static Cocos2dxMusic sCocos2dMusic;
  private static Cocos2dxSound sCocos2dSound;
  private static Cocos2dxAccelerometer sCocos2dxAccelerometer;
  private static Cocos2dxHelperListener sCocos2dxHelperListener;
  private static String sPackageName;
  
  public static void disableAccelerometer()
  {
    sAccelerometerEnabled = false;
    sCocos2dxAccelerometer.disable();
  }
  
  public static void disposeEditBoxDialog()
  {
    sCocos2dxHelperListener.disposeEditBoxDialog();
  }
  
  public static void enableAccelerometer()
  {
    sAccelerometerEnabled = true;
    sCocos2dxAccelerometer.enable();
  }
  
  public static void end()
  {
    sCocos2dMusic.end();
    sCocos2dSound.end();
  }
  
  private static String getAbsolutePathOnExternalStorage(ApplicationInfo paramApplicationInfo, String paramString)
  {
    return Environment.getExternalStorageDirectory() + "/Android/data/" + paramApplicationInfo.packageName + "/files/" + paramString;
  }
  
  public static AssetManager getAssetManager()
  {
    return sAssetManager;
  }
  
  public static float getBackgroundMusicVolume()
  {
    return sCocos2dMusic.getBackgroundVolume();
  }
  
  public static String getCocos2dxPackageName()
  {
    return sPackageName;
  }
  
  public static String getCurrentLanguage()
  {
    return Locale.getDefault().getLanguage();
  }
  
  public static String getDeviceModel()
  {
    return Build.MODEL;
  }
  
  public static float getEffectsVolume()
  {
    return sCocos2dSound.getEffectsVolume();
  }
  
  public static void init(Context paramContext, Cocos2dxHelperListener paramCocos2dxHelperListener)
  {
    ApplicationInfo localApplicationInfo = paramContext.getApplicationInfo();
    sCocos2dxHelperListener = paramCocos2dxHelperListener;
    sPackageName = localApplicationInfo.packageName;
    nativeSetApkPath(localApplicationInfo.sourceDir);
    nativeSetExternalAssetPath(getAbsolutePathOnExternalStorage(localApplicationInfo, "assets/"));
    sCocos2dxAccelerometer = new Cocos2dxAccelerometer(paramContext);
    sCocos2dMusic = new Cocos2dxMusic(paramContext);
    sCocos2dSound = new Cocos2dxSound(paramContext);
    sAssetManager = paramContext.getAssets();
    Cocos2dxBitmap.setContext(paramContext);
  }
  
  public static boolean isBackgroundMusicPlaying()
  {
    return sCocos2dMusic.isBackgroundMusicPlaying();
  }
  
  private static native void nativeSetApkPath(String paramString);
  
  private static native void nativeSetEditTextDialogResult(byte[] paramArrayOfByte);
  
  private static native void nativeSetExternalAssetPath(String paramString);
  
  public static void onPause()
  {
    if (sAccelerometerEnabled) {
      sCocos2dxAccelerometer.disable();
    }
  }
  
  public static void onResume()
  {
    if (sAccelerometerEnabled) {
      sCocos2dxAccelerometer.enable();
    }
  }
  
  public static void pauseAllEffects()
  {
    sCocos2dSound.pauseAllEffects();
  }
  
  public static void pauseBackgroundMusic()
  {
    sCocos2dMusic.pauseBackgroundMusic();
  }
  
  public static void pauseEffect(int paramInt)
  {
    sCocos2dSound.pauseEffect(paramInt);
  }
  
  public static void playBackgroundMusic(String paramString, boolean paramBoolean)
  {
    sCocos2dMusic.playBackgroundMusic(paramString, paramBoolean);
  }
  
  public static int playEffect(String paramString, boolean paramBoolean)
  {
    return sCocos2dSound.playEffect(paramString, paramBoolean);
  }
  
  public static void preloadBackgroundMusic(String paramString)
  {
    sCocos2dMusic.preloadBackgroundMusic(paramString);
  }
  
  public static void preloadEffect(String paramString)
  {
    sCocos2dSound.preloadEffect(paramString);
  }
  
  public static void resumeAllEffects()
  {
    sCocos2dSound.resumeAllEffects();
  }
  
  public static void resumeBackgroundMusic()
  {
    sCocos2dMusic.resumeBackgroundMusic();
  }
  
  public static void resumeEffect(int paramInt)
  {
    sCocos2dSound.resumeEffect(paramInt);
  }
  
  public static void rewindBackgroundMusic()
  {
    sCocos2dMusic.rewindBackgroundMusic();
  }
  
  public static void setAccelerometerInterval(float paramFloat)
  {
    sCocos2dxAccelerometer.setInterval(paramFloat);
  }
  
  public static void setBackgroundMusicVolume(float paramFloat)
  {
    sCocos2dMusic.setBackgroundVolume(paramFloat);
  }
  
  public static void setEditTextDialogResult(String paramString)
  {
    try
    {
      paramString = paramString.getBytes("UTF8");
      sCocos2dxHelperListener.runOnGLThread(new Runnable()
      {
        public void run()
        {
          Cocos2dxHelper.nativeSetEditTextDialogResult(this.val$bytesUTF8);
        }
      });
      return;
    }
    catch (UnsupportedEncodingException paramString) {}
  }
  
  public static void setEffectsVolume(float paramFloat)
  {
    sCocos2dSound.setEffectsVolume(paramFloat);
  }
  
  private static void showDialog(String paramString1, String paramString2)
  {
    sCocos2dxHelperListener.showDialog(paramString1, paramString2);
  }
  
  private static void showEditTextDialog(String paramString1, String paramString2, String paramString3, float paramFloat1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString4, float paramFloat2, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, boolean paramBoolean, int paramInt10, int paramInt11, int paramInt12, int paramInt13)
  {
    sCocos2dxHelperListener.showEditTextDialog(paramString1, paramString2, paramString3, paramFloat1, paramInt1, paramInt2, paramInt3, paramInt4, paramString4, paramFloat2, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9, paramBoolean, paramInt10, paramInt11, paramInt12, paramInt13);
  }
  
  public static void stopAllEffects()
  {
    sCocos2dSound.stopAllEffects();
  }
  
  public static void stopBackgroundMusic()
  {
    sCocos2dMusic.stopBackgroundMusic();
  }
  
  public static void stopEffect(int paramInt)
  {
    sCocos2dSound.stopEffect(paramInt);
  }
  
  private static void termEditing()
  {
    sCocos2dxHelperListener.termEditing();
  }
  
  public static void terminateProcess()
  {
    Process.killProcess(Process.myPid());
  }
  
  public static void unloadEffect(String paramString)
  {
    sCocos2dSound.unloadEffect(paramString);
  }
  
  public static abstract interface Cocos2dxHelperListener
  {
    public abstract void disposeEditBoxDialog();
    
    public abstract void runOnGLThread(Runnable paramRunnable);
    
    public abstract void showDialog(String paramString1, String paramString2);
    
    public abstract void showEditTextDialog(String paramString1, String paramString2, String paramString3, float paramFloat1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString4, float paramFloat2, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, boolean paramBoolean, int paramInt10, int paramInt11, int paramInt12, int paramInt13);
    
    public abstract void termEditing();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\cocos2dx\lib\Cocos2dxHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */