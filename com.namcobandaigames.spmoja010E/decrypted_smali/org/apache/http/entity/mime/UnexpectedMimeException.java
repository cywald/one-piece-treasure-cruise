package org.apache.http.entity.mime;

import org.apache.james.mime4j.MimeException;

@Deprecated
public class UnexpectedMimeException
  extends RuntimeException
{
  private static final long serialVersionUID = 1316818299528463579L;
  
  public UnexpectedMimeException(MimeException paramMimeException)
  {
    super(paramMimeException.getMessage(), paramMimeException);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\http\entity\mime\UnexpectedMimeException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */