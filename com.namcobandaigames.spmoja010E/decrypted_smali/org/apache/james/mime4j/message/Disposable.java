package org.apache.james.mime4j.message;

public abstract interface Disposable
{
  public abstract void dispose();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\message\Disposable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */