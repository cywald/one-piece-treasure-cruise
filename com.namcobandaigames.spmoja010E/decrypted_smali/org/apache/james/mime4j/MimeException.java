package org.apache.james.mime4j;

public class MimeException
  extends Exception
{
  private static final long serialVersionUID = 8352821278714188542L;
  
  public MimeException(String paramString)
  {
    super(paramString);
  }
  
  public MimeException(String paramString, Throwable paramThrowable)
  {
    super(paramString);
    initCause(paramThrowable);
  }
  
  public MimeException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\MimeException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */