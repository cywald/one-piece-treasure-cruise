package org.apache.james.mime4j.storage;

import java.io.IOException;
import java.io.InputStream;

public abstract interface Storage
{
  public abstract void delete();
  
  public abstract InputStream getInputStream()
    throws IOException;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\storage\Storage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */