package org.apache.james.mime4j.storage;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.codec.CodecUtil;

public abstract class AbstractStorageProvider
  implements StorageProvider
{
  public final Storage store(InputStream paramInputStream)
    throws IOException
  {
    StorageOutputStream localStorageOutputStream = createStorageOutputStream();
    CodecUtil.copy(paramInputStream, localStorageOutputStream);
    return localStorageOutputStream.toStorage();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\storage\AbstractStorageProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */