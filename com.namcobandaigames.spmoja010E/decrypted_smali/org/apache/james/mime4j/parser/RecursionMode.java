package org.apache.james.mime4j.parser;

public abstract interface RecursionMode
{
  public static final int M_FLAT = 3;
  public static final int M_NO_RECURSE = 1;
  public static final int M_RAW = 2;
  public static final int M_RECURSE = 0;
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\parser\RecursionMode.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */