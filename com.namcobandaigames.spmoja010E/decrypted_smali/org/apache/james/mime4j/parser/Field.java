package org.apache.james.mime4j.parser;

import org.apache.james.mime4j.util.ByteSequence;

public abstract interface Field
{
  public abstract String getBody();
  
  public abstract String getName();
  
  public abstract ByteSequence getRaw();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\parser\Field.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */