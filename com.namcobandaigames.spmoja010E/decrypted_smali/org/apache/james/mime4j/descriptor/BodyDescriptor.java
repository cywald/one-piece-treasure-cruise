package org.apache.james.mime4j.descriptor;

public abstract interface BodyDescriptor
  extends ContentDescriptor
{
  public abstract String getBoundary();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\descriptor\BodyDescriptor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */