package org.apache.james.mime4j.io;

public abstract interface LineNumberSource
{
  public abstract int getLineNumber();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\io\LineNumberSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */