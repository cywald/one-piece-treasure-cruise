package org.apache.james.mime4j.field.contenttype.parser;

public class Token
{
  public int beginColumn;
  public int beginLine;
  public int endColumn;
  public int endLine;
  public String image;
  public int kind;
  public Token next;
  public Token specialToken;
  
  public static final Token newToken(int paramInt)
  {
    return new Token();
  }
  
  public String toString()
  {
    return this.image;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\field\contenttype\parser\Token.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */