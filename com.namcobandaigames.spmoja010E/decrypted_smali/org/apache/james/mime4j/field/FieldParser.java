package org.apache.james.mime4j.field;

import org.apache.james.mime4j.util.ByteSequence;

public abstract interface FieldParser
{
  public abstract ParsedField parse(String paramString1, String paramString2, ByteSequence paramByteSequence);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\org\apache\james\mime4j\field\FieldParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */