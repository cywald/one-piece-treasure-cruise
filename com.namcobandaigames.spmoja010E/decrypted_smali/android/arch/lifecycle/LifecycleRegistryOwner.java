package android.arch.lifecycle;

@Deprecated
public abstract interface LifecycleRegistryOwner
  extends LifecycleOwner
{
  public abstract LifecycleRegistry getLifecycle();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\android\arch\lifecycle\LifecycleRegistryOwner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */