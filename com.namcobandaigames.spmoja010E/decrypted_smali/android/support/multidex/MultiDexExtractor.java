package android.support.multidex;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

final class MultiDexExtractor
  implements Closeable
{
  private static final int BUFFER_SIZE = 16384;
  private static final String DEX_PREFIX = "classes";
  static final String DEX_SUFFIX = ".dex";
  private static final String EXTRACTED_NAME_EXT = ".classes";
  static final String EXTRACTED_SUFFIX = ".zip";
  private static final String KEY_CRC = "crc";
  private static final String KEY_DEX_CRC = "dex.crc.";
  private static final String KEY_DEX_NUMBER = "dex.number";
  private static final String KEY_DEX_TIME = "dex.time.";
  private static final String KEY_TIME_STAMP = "timestamp";
  private static final String LOCK_FILENAME = "MultiDex.lock";
  private static final int MAX_EXTRACT_ATTEMPTS = 3;
  private static final long NO_VALUE = -1L;
  private static final String PREFS_FILE = "multidex.version";
  private static final String TAG = "MultiDex";
  private final FileLock cacheLock;
  private final File dexDir;
  private final FileChannel lockChannel;
  private final RandomAccessFile lockRaf;
  private final File sourceApk;
  private final long sourceCrc;
  
  /* Error */
  MultiDexExtractor(File paramFile1, File paramFile2)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 78	java/lang/Object:<init>	()V
    //   4: ldc 57
    //   6: new 80	java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   13: ldc 83
    //   15: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   18: aload_1
    //   19: invokevirtual 93	java/io/File:getPath	()Ljava/lang/String;
    //   22: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   25: ldc 95
    //   27: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: aload_2
    //   31: invokevirtual 93	java/io/File:getPath	()Ljava/lang/String;
    //   34: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: ldc 97
    //   39: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   45: invokestatic 106	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   48: pop
    //   49: aload_0
    //   50: aload_1
    //   51: putfield 108	android/support/multidex/MultiDexExtractor:sourceApk	Ljava/io/File;
    //   54: aload_0
    //   55: aload_2
    //   56: putfield 110	android/support/multidex/MultiDexExtractor:dexDir	Ljava/io/File;
    //   59: aload_0
    //   60: aload_1
    //   61: invokestatic 114	android/support/multidex/MultiDexExtractor:getZipCrc	(Ljava/io/File;)J
    //   64: putfield 116	android/support/multidex/MultiDexExtractor:sourceCrc	J
    //   67: new 89	java/io/File
    //   70: dup
    //   71: aload_2
    //   72: ldc 45
    //   74: invokespecial 119	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   77: astore_1
    //   78: aload_0
    //   79: new 121	java/io/RandomAccessFile
    //   82: dup
    //   83: aload_1
    //   84: ldc 123
    //   86: invokespecial 124	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   89: putfield 126	android/support/multidex/MultiDexExtractor:lockRaf	Ljava/io/RandomAccessFile;
    //   92: aload_0
    //   93: aload_0
    //   94: getfield 126	android/support/multidex/MultiDexExtractor:lockRaf	Ljava/io/RandomAccessFile;
    //   97: invokevirtual 130	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   100: putfield 132	android/support/multidex/MultiDexExtractor:lockChannel	Ljava/nio/channels/FileChannel;
    //   103: ldc 57
    //   105: new 80	java/lang/StringBuilder
    //   108: dup
    //   109: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   112: ldc -122
    //   114: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   117: aload_1
    //   118: invokevirtual 93	java/io/File:getPath	()Ljava/lang/String;
    //   121: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   124: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   127: invokestatic 106	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   130: pop
    //   131: aload_0
    //   132: aload_0
    //   133: getfield 132	android/support/multidex/MultiDexExtractor:lockChannel	Ljava/nio/channels/FileChannel;
    //   136: invokevirtual 140	java/nio/channels/FileChannel:lock	()Ljava/nio/channels/FileLock;
    //   139: putfield 142	android/support/multidex/MultiDexExtractor:cacheLock	Ljava/nio/channels/FileLock;
    //   142: ldc 57
    //   144: new 80	java/lang/StringBuilder
    //   147: dup
    //   148: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   151: aload_1
    //   152: invokevirtual 93	java/io/File:getPath	()Ljava/lang/String;
    //   155: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   158: ldc -112
    //   160: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   163: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   166: invokestatic 106	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   169: pop
    //   170: return
    //   171: aload_0
    //   172: getfield 132	android/support/multidex/MultiDexExtractor:lockChannel	Ljava/nio/channels/FileChannel;
    //   175: invokestatic 148	android/support/multidex/MultiDexExtractor:closeQuietly	(Ljava/io/Closeable;)V
    //   178: aload_1
    //   179: athrow
    //   180: astore_1
    //   181: aload_0
    //   182: getfield 126	android/support/multidex/MultiDexExtractor:lockRaf	Ljava/io/RandomAccessFile;
    //   185: invokestatic 148	android/support/multidex/MultiDexExtractor:closeQuietly	(Ljava/io/Closeable;)V
    //   188: aload_1
    //   189: athrow
    //   190: astore_1
    //   191: goto -20 -> 171
    //   194: astore_1
    //   195: goto -24 -> 171
    //   198: astore_1
    //   199: goto -18 -> 181
    //   202: astore_1
    //   203: goto -22 -> 181
    //   206: astore_1
    //   207: goto -36 -> 171
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	210	0	this	MultiDexExtractor
    //   0	210	1	paramFile1	File
    //   0	210	2	paramFile2	File
    // Exception table:
    //   from	to	target	type
    //   92	103	180	java/io/IOException
    //   142	170	180	java/io/IOException
    //   171	180	180	java/io/IOException
    //   103	142	190	java/lang/RuntimeException
    //   103	142	194	java/lang/Error
    //   92	103	198	java/lang/RuntimeException
    //   142	170	198	java/lang/RuntimeException
    //   171	180	198	java/lang/RuntimeException
    //   92	103	202	java/lang/Error
    //   142	170	202	java/lang/Error
    //   171	180	202	java/lang/Error
    //   103	142	206	java/io/IOException
  }
  
  private void clearDexDir()
  {
    File[] arrayOfFile = this.dexDir.listFiles(new FileFilter()
    {
      public boolean accept(File paramAnonymousFile)
      {
        return !paramAnonymousFile.getName().equals("MultiDex.lock");
      }
    });
    if (arrayOfFile == null)
    {
      Log.w("MultiDex", "Failed to list secondary dex dir content (" + this.dexDir.getPath() + ").");
      return;
    }
    int j = arrayOfFile.length;
    int i = 0;
    label62:
    File localFile;
    if (i < j)
    {
      localFile = arrayOfFile[i];
      Log.i("MultiDex", "Trying to delete old file " + localFile.getPath() + " of size " + localFile.length());
      if (localFile.delete()) {
        break label158;
      }
      Log.w("MultiDex", "Failed to delete old file " + localFile.getPath());
    }
    for (;;)
    {
      i += 1;
      break label62;
      break;
      label158:
      Log.i("MultiDex", "Deleted old file " + localFile.getPath());
    }
  }
  
  private static void closeQuietly(Closeable paramCloseable)
  {
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException paramCloseable)
    {
      Log.w("MultiDex", "Failed to close resource", paramCloseable);
    }
  }
  
  private static void extract(ZipFile paramZipFile, ZipEntry paramZipEntry, File paramFile, String paramString)
    throws IOException, FileNotFoundException
  {
    InputStream localInputStream = paramZipFile.getInputStream(paramZipEntry);
    paramString = File.createTempFile("tmp-" + paramString, ".zip", paramFile.getParentFile());
    Log.i("MultiDex", "Extracting " + paramString.getPath());
    for (;;)
    {
      try
      {
        paramZipFile = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(paramString)));
      }
      finally
      {
        ZipEntry localZipEntry;
        int i;
        continue;
      }
      try
      {
        localZipEntry = new ZipEntry("classes.dex");
        localZipEntry.setTime(paramZipEntry.getTime());
        paramZipFile.putNextEntry(localZipEntry);
        paramZipEntry = new byte['䀀'];
        i = localInputStream.read(paramZipEntry);
        if (i != -1)
        {
          paramZipFile.write(paramZipEntry, 0, i);
          i = localInputStream.read(paramZipEntry);
        }
        else
        {
          paramZipFile.closeEntry();
          try
          {
            paramZipFile.close();
            if (paramString.setReadOnly()) {
              continue;
            }
            throw new IOException("Failed to mark readonly \"" + paramString.getAbsolutePath() + "\" (tmp of \"" + paramFile.getAbsolutePath() + "\")");
          }
          finally {}
          closeQuietly(localInputStream);
          paramString.delete();
          throw paramZipFile;
        }
      }
      finally
      {
        paramZipFile.close();
      }
    }
    Log.i("MultiDex", "Renaming to " + paramFile.getPath());
    if (!paramString.renameTo(paramFile)) {
      throw new IOException("Failed to rename \"" + paramString.getAbsolutePath() + "\" to \"" + paramFile.getAbsolutePath() + "\"");
    }
    closeQuietly(localInputStream);
    paramString.delete();
  }
  
  private static SharedPreferences getMultiDexPreferences(Context paramContext)
  {
    if (Build.VERSION.SDK_INT < 11) {}
    for (int i = 0;; i = 4) {
      return paramContext.getSharedPreferences("multidex.version", i);
    }
  }
  
  private static long getTimeStamp(File paramFile)
  {
    long l2 = paramFile.lastModified();
    long l1 = l2;
    if (l2 == -1L) {
      l1 = l2 - 1L;
    }
    return l1;
  }
  
  private static long getZipCrc(File paramFile)
    throws IOException
  {
    long l2 = ZipUtil.getZipCrc(paramFile);
    long l1 = l2;
    if (l2 == -1L) {
      l1 = l2 - 1L;
    }
    return l1;
  }
  
  private static boolean isModified(Context paramContext, File paramFile, long paramLong, String paramString)
  {
    paramContext = getMultiDexPreferences(paramContext);
    return (paramContext.getLong(paramString + "timestamp", -1L) != getTimeStamp(paramFile)) || (paramContext.getLong(paramString + "crc", -1L) != paramLong);
  }
  
  private List<ExtractedDex> loadExistingExtractions(Context paramContext, String paramString)
    throws IOException
  {
    Log.i("MultiDex", "loading existing secondary dex files");
    String str = this.sourceApk.getName() + ".classes";
    paramContext = getMultiDexPreferences(paramContext);
    int j = paramContext.getInt(paramString + "dex.number", 1);
    ArrayList localArrayList = new ArrayList(j - 1);
    int i = 2;
    while (i <= j)
    {
      Object localObject = str + i + ".zip";
      localObject = new ExtractedDex(this.dexDir, (String)localObject);
      if (((ExtractedDex)localObject).isFile())
      {
        ((ExtractedDex)localObject).crc = getZipCrc((File)localObject);
        long l1 = paramContext.getLong(paramString + "dex.crc." + i, -1L);
        long l2 = paramContext.getLong(paramString + "dex.time." + i, -1L);
        long l3 = ((ExtractedDex)localObject).lastModified();
        if ((l2 != l3) || (l1 != ((ExtractedDex)localObject).crc)) {
          throw new IOException("Invalid extracted dex: " + localObject + " (key \"" + paramString + "\"), expected modification time: " + l2 + ", modification time: " + l3 + ", expected crc: " + l1 + ", file crc: " + ((ExtractedDex)localObject).crc);
        }
        localArrayList.add(localObject);
        i += 1;
      }
      else
      {
        throw new IOException("Missing extracted secondary dex file '" + ((ExtractedDex)localObject).getPath() + "'");
      }
    }
    return localArrayList;
  }
  
  /* Error */
  private List<ExtractedDex> performExtractions()
    throws IOException
  {
    // Byte code:
    //   0: new 80	java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   7: aload_0
    //   8: getfield 108	android/support/multidex/MultiDexExtractor:sourceApk	Ljava/io/File;
    //   11: invokevirtual 323	java/io/File:getName	()Ljava/lang/String;
    //   14: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   17: ldc 24
    //   19: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   22: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   25: astore 10
    //   27: aload_0
    //   28: invokespecial 374	android/support/multidex/MultiDexExtractor:clearDexDir	()V
    //   31: new 329	java/util/ArrayList
    //   34: dup
    //   35: invokespecial 375	java/util/ArrayList:<init>	()V
    //   38: astore 8
    //   40: new 198	java/util/zip/ZipFile
    //   43: dup
    //   44: aload_0
    //   45: getfield 108	android/support/multidex/MultiDexExtractor:sourceApk	Ljava/io/File;
    //   48: invokespecial 376	java/util/zip/ZipFile:<init>	(Ljava/io/File;)V
    //   51: astore 9
    //   53: iconst_2
    //   54: istore_2
    //   55: aload 9
    //   57: new 80	java/lang/StringBuilder
    //   60: dup
    //   61: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   64: ldc 18
    //   66: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: iconst_2
    //   70: invokevirtual 335	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   73: ldc 21
    //   75: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   81: invokevirtual 380	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
    //   84: astore 6
    //   86: aload 6
    //   88: ifnull +420 -> 508
    //   91: new 80	java/lang/StringBuilder
    //   94: dup
    //   95: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   98: aload 10
    //   100: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: iload_2
    //   104: invokevirtual 335	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   107: ldc 27
    //   109: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   115: astore 7
    //   117: new 10	android/support/multidex/MultiDexExtractor$ExtractedDex
    //   120: dup
    //   121: aload_0
    //   122: getfield 110	android/support/multidex/MultiDexExtractor:dexDir	Ljava/io/File;
    //   125: aload 7
    //   127: invokespecial 336	android/support/multidex/MultiDexExtractor$ExtractedDex:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   130: astore 11
    //   132: aload 8
    //   134: aload 11
    //   136: invokeinterface 363 2 0
    //   141: pop
    //   142: ldc 57
    //   144: new 80	java/lang/StringBuilder
    //   147: dup
    //   148: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   151: ldc_w 382
    //   154: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   157: aload 11
    //   159: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   162: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   165: invokestatic 106	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   168: pop
    //   169: iconst_0
    //   170: istore_3
    //   171: iconst_0
    //   172: istore 4
    //   174: iload_3
    //   175: iconst_3
    //   176: if_icmpge +242 -> 418
    //   179: iload 4
    //   181: ifne +237 -> 418
    //   184: iload_3
    //   185: iconst_1
    //   186: iadd
    //   187: istore 5
    //   189: aload 9
    //   191: aload 6
    //   193: aload 11
    //   195: aload 10
    //   197: invokestatic 384	android/support/multidex/MultiDexExtractor:extract	(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/io/File;Ljava/lang/String;)V
    //   200: aload 11
    //   202: aload 11
    //   204: invokestatic 114	android/support/multidex/MultiDexExtractor:getZipCrc	(Ljava/io/File;)J
    //   207: putfield 341	android/support/multidex/MultiDexExtractor$ExtractedDex:crc	J
    //   210: iconst_1
    //   211: istore_1
    //   212: new 80	java/lang/StringBuilder
    //   215: dup
    //   216: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   219: ldc_w 386
    //   222: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   225: astore 12
    //   227: iload_1
    //   228: ifeq +318 -> 546
    //   231: ldc_w 388
    //   234: astore 7
    //   236: ldc 57
    //   238: aload 12
    //   240: aload 7
    //   242: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   245: ldc_w 390
    //   248: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   251: aload 11
    //   253: invokevirtual 391	android/support/multidex/MultiDexExtractor$ExtractedDex:getAbsolutePath	()Ljava/lang/String;
    //   256: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   259: ldc_w 393
    //   262: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   265: aload 11
    //   267: invokevirtual 394	android/support/multidex/MultiDexExtractor$ExtractedDex:length	()J
    //   270: invokevirtual 176	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   273: ldc_w 396
    //   276: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   279: aload 11
    //   281: getfield 341	android/support/multidex/MultiDexExtractor$ExtractedDex:crc	J
    //   284: invokevirtual 176	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   287: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   290: invokestatic 106	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   293: pop
    //   294: iload_1
    //   295: istore 4
    //   297: iload 5
    //   299: istore_3
    //   300: iload_1
    //   301: ifne -127 -> 174
    //   304: aload 11
    //   306: invokevirtual 397	android/support/multidex/MultiDexExtractor$ExtractedDex:delete	()Z
    //   309: pop
    //   310: iload_1
    //   311: istore 4
    //   313: iload 5
    //   315: istore_3
    //   316: aload 11
    //   318: invokevirtual 400	android/support/multidex/MultiDexExtractor$ExtractedDex:exists	()Z
    //   321: ifeq -147 -> 174
    //   324: ldc 57
    //   326: new 80	java/lang/StringBuilder
    //   329: dup
    //   330: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   333: ldc_w 402
    //   336: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   339: aload 11
    //   341: invokevirtual 366	android/support/multidex/MultiDexExtractor$ExtractedDex:getPath	()Ljava/lang/String;
    //   344: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   347: ldc_w 368
    //   350: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   353: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   356: invokestatic 165	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   359: pop
    //   360: iload_1
    //   361: istore 4
    //   363: iload 5
    //   365: istore_3
    //   366: goto -192 -> 174
    //   369: astore 6
    //   371: aload 9
    //   373: invokevirtual 403	java/util/zip/ZipFile:close	()V
    //   376: aload 6
    //   378: athrow
    //   379: astore 7
    //   381: iconst_0
    //   382: istore_1
    //   383: ldc 57
    //   385: new 80	java/lang/StringBuilder
    //   388: dup
    //   389: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   392: ldc_w 405
    //   395: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   398: aload 11
    //   400: invokevirtual 391	android/support/multidex/MultiDexExtractor$ExtractedDex:getAbsolutePath	()Ljava/lang/String;
    //   403: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   406: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   409: aload 7
    //   411: invokestatic 192	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   414: pop
    //   415: goto -203 -> 212
    //   418: iload 4
    //   420: ifne +50 -> 470
    //   423: new 71	java/io/IOException
    //   426: dup
    //   427: new 80	java/lang/StringBuilder
    //   430: dup
    //   431: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   434: ldc_w 407
    //   437: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   440: aload 11
    //   442: invokevirtual 391	android/support/multidex/MultiDexExtractor$ExtractedDex:getAbsolutePath	()Ljava/lang/String;
    //   445: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   448: ldc_w 409
    //   451: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   454: iload_2
    //   455: invokevirtual 335	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   458: ldc 97
    //   460: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   463: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   466: invokespecial 272	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   469: athrow
    //   470: iload_2
    //   471: iconst_1
    //   472: iadd
    //   473: istore_2
    //   474: aload 9
    //   476: new 80	java/lang/StringBuilder
    //   479: dup
    //   480: invokespecial 81	java/lang/StringBuilder:<init>	()V
    //   483: ldc 18
    //   485: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   488: iload_2
    //   489: invokevirtual 335	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   492: ldc 21
    //   494: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   497: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   500: invokevirtual 380	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
    //   503: astore 6
    //   505: goto -419 -> 86
    //   508: aload 9
    //   510: invokevirtual 403	java/util/zip/ZipFile:close	()V
    //   513: aload 8
    //   515: areturn
    //   516: astore 6
    //   518: ldc 57
    //   520: ldc -67
    //   522: aload 6
    //   524: invokestatic 192	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   527: pop
    //   528: aload 8
    //   530: areturn
    //   531: astore 7
    //   533: ldc 57
    //   535: ldc -67
    //   537: aload 7
    //   539: invokestatic 192	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   542: pop
    //   543: goto -167 -> 376
    //   546: ldc_w 411
    //   549: astore 7
    //   551: goto -315 -> 236
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	554	0	this	MultiDexExtractor
    //   211	172	1	i	int
    //   54	435	2	j	int
    //   170	196	3	k	int
    //   172	247	4	m	int
    //   187	177	5	n	int
    //   84	108	6	localZipEntry1	ZipEntry
    //   369	8	6	localObject	Object
    //   503	1	6	localZipEntry2	ZipEntry
    //   516	7	6	localIOException1	IOException
    //   115	126	7	str1	String
    //   379	31	7	localIOException2	IOException
    //   531	7	7	localIOException3	IOException
    //   549	1	7	str2	String
    //   38	491	8	localArrayList	ArrayList
    //   51	458	9	localZipFile	ZipFile
    //   25	171	10	str3	String
    //   130	311	11	localExtractedDex	ExtractedDex
    //   225	14	12	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   55	86	369	finally
    //   91	169	369	finally
    //   189	200	369	finally
    //   200	210	369	finally
    //   212	227	369	finally
    //   236	294	369	finally
    //   304	310	369	finally
    //   316	360	369	finally
    //   383	415	369	finally
    //   423	470	369	finally
    //   474	505	369	finally
    //   200	210	379	java/io/IOException
    //   508	513	516	java/io/IOException
    //   371	376	531	java/io/IOException
  }
  
  private static void putStoredApkInfo(Context paramContext, String paramString, long paramLong1, long paramLong2, List<ExtractedDex> paramList)
  {
    paramContext = getMultiDexPreferences(paramContext).edit();
    paramContext.putLong(paramString + "timestamp", paramLong1);
    paramContext.putLong(paramString + "crc", paramLong2);
    paramContext.putInt(paramString + "dex.number", paramList.size() + 1);
    int i = 2;
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      ExtractedDex localExtractedDex = (ExtractedDex)paramList.next();
      paramContext.putLong(paramString + "dex.crc." + i, localExtractedDex.crc);
      paramContext.putLong(paramString + "dex.time." + i, localExtractedDex.lastModified());
      i += 1;
    }
    paramContext.commit();
  }
  
  public void close()
    throws IOException
  {
    this.cacheLock.release();
    this.lockChannel.close();
    this.lockRaf.close();
  }
  
  List<? extends File> load(Context paramContext, String paramString, boolean paramBoolean)
    throws IOException
  {
    Log.i("MultiDex", "MultiDexExtractor.load(" + this.sourceApk.getPath() + ", " + paramBoolean + ", " + paramString + ")");
    if (!this.cacheLock.isValid()) {
      throw new IllegalStateException("MultiDexExtractor was closed");
    }
    if ((!paramBoolean) && (!isModified(paramContext, this.sourceApk, this.sourceCrc, paramString)))
    {
      try
      {
        List localList1 = loadExistingExtractions(paramContext, paramString);
        paramContext = localList1;
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          Log.w("MultiDex", "Failed to reload existing extracted secondary dex files, falling back to fresh extraction", localIOException);
          localList2 = performExtractions();
          putStoredApkInfo(paramContext, paramString, getTimeStamp(this.sourceApk), this.sourceCrc, localList2);
          paramContext = localList2;
        }
      }
      Log.i("MultiDex", "load found " + paramContext.size() + " secondary dex files");
      return paramContext;
    }
    List localList2;
    if (paramBoolean) {
      Log.i("MultiDex", "Forced extraction must be performed.");
    }
    for (;;)
    {
      localList2 = performExtractions();
      putStoredApkInfo(paramContext, paramString, getTimeStamp(this.sourceApk), this.sourceCrc, localList2);
      paramContext = localList2;
      break;
      Log.i("MultiDex", "Detected that extraction must be performed.");
    }
  }
  
  private static class ExtractedDex
    extends File
  {
    public long crc = -1L;
    
    public ExtractedDex(File paramFile, String paramString)
    {
      super(paramString);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\android\support\multidex\MultiDexExtractor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */