package android.support.v7.view;

public abstract interface CollapsibleActionView
{
  public abstract void onActionViewCollapsed();
  
  public abstract void onActionViewExpanded();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\android\support\v7\view\CollapsibleActionView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */