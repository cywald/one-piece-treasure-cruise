package android.support.v4.view;

import android.view.View;

public abstract interface ViewPropertyAnimatorUpdateListener
{
  public abstract void onAnimationUpdate(View paramView);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\android\support\v4\view\ViewPropertyAnimatorUpdateListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */