package android.support.v4.view;

import android.view.View;

public abstract interface ViewPropertyAnimatorListener
{
  public abstract void onAnimationCancel(View paramView);
  
  public abstract void onAnimationEnd(View paramView);
  
  public abstract void onAnimationStart(View paramView);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\android\support\v4\view\ViewPropertyAnimatorListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */