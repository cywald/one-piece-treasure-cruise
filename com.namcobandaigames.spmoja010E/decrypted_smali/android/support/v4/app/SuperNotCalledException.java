package android.support.v4.app;

import android.util.AndroidRuntimeException;

final class SuperNotCalledException
  extends AndroidRuntimeException
{
  public SuperNotCalledException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\android\support\v4\app\SuperNotCalledException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */