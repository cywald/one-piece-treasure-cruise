package jp.co.drecom.bisque.lib;

import android.os.Handler;

public class BQWebViewDispatcherNotify
{
  private static BQWebViewDispatcher dispatcher;
  private static Handler handler;
  
  public static boolean addWebView(int paramInt1, final int paramInt2, final int paramInt3, final int paramInt4, final String paramString1, final int paramInt5, final int paramInt6, final int paramInt7, final boolean paramBoolean, final String paramString2, final String paramString3)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.addWebView(this.val$_x, paramInt2, paramInt3, paramInt4, paramString1, paramInt5, paramInt6, paramInt7, paramBoolean, paramString2, paramString3);
      }
    });
    return true;
  }
  
  public static boolean addWebView(int paramInt1, final int paramInt2, final int paramInt3, final int paramInt4, final String paramString1, final String paramString2, final int paramInt5, final int paramInt6, final int paramInt7, final boolean paramBoolean, final String paramString3, final String paramString4)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.addWebView(this.val$_x, paramInt2, paramInt3, paramInt4, paramString1, paramString2, paramInt5, paramInt6, paramInt7, paramBoolean, paramString3, paramString4);
      }
    });
    return true;
  }
  
  public static boolean addWebView(int paramInt1, final int paramInt2, final int paramInt3, final int paramInt4, final String paramString1, final String[] paramArrayOfString1, final String[] paramArrayOfString2, final int paramInt5, final int paramInt6, final int paramInt7, final boolean paramBoolean, final String paramString2, final String paramString3)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.addWebView(this.val$_x, paramInt2, paramInt3, paramInt4, paramString1, paramArrayOfString1, paramArrayOfString2, paramInt5, paramInt6, paramInt7, paramBoolean, paramString2, paramString3);
      }
    });
    return true;
  }
  
  public static void bouncesWebView(boolean paramBoolean, final int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.bouncesWebView(this.val$_b, paramInt);
      }
    });
  }
  
  public static void clearCache(int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.clearCache(this.val$_tag);
      }
    });
  }
  
  public static void clearCookie(int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.clearCookie(this.val$_tag);
      }
    });
  }
  
  public static void enableCache(boolean paramBoolean, final int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.enableCache(this.val$_b, paramInt);
      }
    });
  }
  
  public static void enableCookie(boolean paramBoolean, final int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.enableCookie(this.val$_b, paramInt);
      }
    });
  }
  
  public static void enableWebView(boolean paramBoolean, final int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.enableWebView(this.val$_b, paramInt);
      }
    });
  }
  
  public static void executeJsInWebView(String paramString, final int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.executeJsInWebView(this.val$_url, paramInt);
      }
    });
  }
  
  public static void reloadWebView(int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.reloadWebView(this.val$_tag);
      }
    });
  }
  
  public static void removeWebView(int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.removeWebView(this.val$_tag);
      }
    });
  }
  
  public static void requestWebView(String paramString1, final int paramInt, final boolean paramBoolean, final String paramString2, final String paramString3)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.requestWebView(this.val$_url, paramInt, paramBoolean, paramString2, paramString3);
      }
    });
  }
  
  public static void requestWebView(String paramString1, final String paramString2, final int paramInt, final boolean paramBoolean, final String paramString3, final String paramString4)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.requestWebView(this.val$_url, paramString2, paramInt, paramBoolean, paramString3, paramString4);
      }
    });
  }
  
  public static void requestWebView(String paramString1, final String[] paramArrayOfString1, final String[] paramArrayOfString2, final int paramInt, final boolean paramBoolean, final String paramString2, final String paramString3)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.requestWebView(this.val$_url, paramArrayOfString1, paramArrayOfString2, paramInt, paramBoolean, paramString2, paramString3);
      }
    });
  }
  
  public static void setDispatcher(BQWebViewDispatcher paramBQWebViewDispatcher, Handler paramHandler)
  {
    dispatcher = paramBQWebViewDispatcher;
    handler = paramHandler;
  }
  
  public static void setOrderWebView(int paramInt1, final int paramInt2)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.setOrderWebView(this.val$_order, paramInt2);
      }
    });
  }
  
  public static void setRectWebView(int paramInt1, final int paramInt2, final int paramInt3, final int paramInt4, final int paramInt5)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQWebViewDispatcherNotify.dispatcher.setRectWebView(this.val$_x, paramInt2, paramInt3, paramInt4, paramInt5);
      }
    });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQWebViewDispatcherNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */