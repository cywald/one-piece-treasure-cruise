package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class BQPermissionHelper
{
  private static BQPermissionHelper instance = null;
  private static String[] permissionArray = { "android.permission.READ_CALENDAR", "android.permission.WRITE_CALENDAR", "android.permission.CAMERA", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS", "android.permission.GET_ACCOUNTS", "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.RECORD_AUDIO", "android.permission.READ_PHONE_STATE", "android.permission.CALL_PHONE", "com.android.voicemail.permission.ADD_VOICEMAIL", "android.permission.USE_SIP", "android.permission.PROCESS_OUTGOING_CALLS", "android.permission.SEND_SMS", "android.permission.RECEIVE_SMS", "android.permission.READ_SMS", "android.permission.RECEIVE_WAP_PUSH", "android.permission.RECEIVE_MMS", "android.permission.WRITE_EXTERNAL_STORAGE" };
  private Activity m_activity = null;
  private int m_currentRequestCode = 0;
  
  public static void checkPermission(int paramInt)
    throws Exception
  {
    getInstance().checkPermissionInternal(paramInt);
  }
  
  private void checkPermissionInternal(int paramInt)
    throws Exception
  {
    final String str = getPermissionStr(paramInt);
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        final int i = ContextCompat.checkSelfPermission(BQPermissionHelper.this.m_activity, str);
        HandlerJava.getInstance().runOnGLThread(new Runnable()
        {
          public void run()
          {
            BQPermissionHelper.nativeCheckPermission(i);
          }
        });
      }
    });
  }
  
  public static BQPermissionHelper getInstance()
  {
    if (instance == null) {
      instance = new BQPermissionHelper();
    }
    return instance;
  }
  
  private static String getPermissionStr(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= permissionArray.length)) {
      throw new IllegalArgumentException();
    }
    return permissionArray[paramInt];
  }
  
  private static native void nativeCheckPermission(int paramInt);
  
  private static native void nativePermissionResult(boolean paramBoolean);
  
  private static native void nativeShouldShowRequestPermissionRationale(boolean paramBoolean);
  
  public static void requestPermission(int paramInt)
  {
    getInstance().requestPermissionInternal(paramInt);
  }
  
  private void requestPermissionInternal(final int paramInt)
  {
    String str = getPermissionStr(paramInt);
    this.m_currentRequestCode = paramInt;
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        ActivityCompat.requestPermissions(BQPermissionHelper.this.m_activity, this.val$permissions, paramInt);
      }
    });
  }
  
  public static void shouldShowRequestPermissionRationale(int paramInt)
  {
    getInstance().shouldShowRequestPermissionRationaleInternal(paramInt);
  }
  
  private void shouldShowRequestPermissionRationaleInternal(int paramInt)
  {
    final String str = getPermissionStr(paramInt);
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        final boolean bool = ActivityCompat.shouldShowRequestPermissionRationale(BQPermissionHelper.this.m_activity, str);
        HandlerJava.getInstance().runOnGLThread(new Runnable()
        {
          public void run()
          {
            BQPermissionHelper.nativeShouldShowRequestPermissionRationale(bool);
          }
        });
      }
    });
  }
  
  public void initialize(Activity paramActivity)
  {
    this.m_activity = paramActivity;
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    if (paramInt == this.m_currentRequestCode) {
      if ((paramArrayOfInt.length <= 0) || (paramArrayOfInt[0] != 0)) {
        break label39;
      }
    }
    label39:
    for (final boolean bool = true;; bool = false)
    {
      HandlerJava.getInstance().runOnGLThread(new Runnable()
      {
        public void run()
        {
          BQPermissionHelper.nativePermissionResult(bool);
        }
      });
      return;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQPermissionHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */