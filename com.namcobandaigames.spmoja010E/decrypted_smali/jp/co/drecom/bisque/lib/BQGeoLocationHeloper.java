package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BQGeoLocationHeloper
{
  public static final boolean DEFAULT_USE_LAST_KNOWN_LOCATION = true;
  private static BQGeoLocationHeloper self = null;
  private LocationListener gpsLocationListener;
  private boolean m_bGpsUsable = false;
  private boolean m_bInitialized = false;
  private boolean m_bLocationUsable = false;
  private boolean m_bNetUsable = false;
  private Location m_kCurrentLocation;
  private LocationListener m_kGpsListener = null;
  private LocationManager m_kLocationMgr = null;
  private Lock m_kLock;
  private LocationListener m_kNetListener = null;
  private long m_significantlyNewer = 120000L;
  private float m_zMinDistance;
  private long m_zMinTime;
  private LocationListener networkLocationListener;
  private boolean useLastKnownLocation = true;
  
  private boolean checkAndUpdateProviderStatus()
  {
    if (this.m_kLocationMgr == null) {
      return false;
    }
    this.m_bGpsUsable = this.m_kLocationMgr.isProviderEnabled("gps");
    this.m_bNetUsable = this.m_kLocationMgr.isProviderEnabled("network");
    if ((!this.m_bGpsUsable) && (!this.m_bNetUsable))
    {
      this.m_bLocationUsable = false;
      return false;
    }
    this.m_bLocationUsable = true;
    return true;
  }
  
  private boolean checkProviderStatus(boolean paramBoolean)
  {
    if (this.m_kLocationMgr == null) {}
    boolean bool1;
    boolean bool2;
    do
    {
      return false;
      bool1 = this.m_kLocationMgr.isProviderEnabled("gps");
      bool2 = this.m_kLocationMgr.isProviderEnabled("network");
      if ((true == paramBoolean) && ((bool1) || (bool2))) {
        return true;
      }
    } while ((!bool1) || (!bool2));
    return true;
  }
  
  public static BQGeoLocationHeloper getInstance()
  {
    if (self == null) {
      self = new BQGeoLocationHeloper();
    }
    return self;
  }
  
  private boolean initInternal()
  {
    if (true == this.m_bInitialized) {
      return true;
    }
    this.m_kLocationMgr = ((LocationManager)BQJNIHelper.getSystemServiceWXP("location"));
    if (this.m_kLocationMgr == null) {
      return false;
    }
    this.m_bInitialized = true;
    this.m_kLock = new ReentrantLock();
    if (true != checkAndUpdateProviderStatus()) {
      handleProviderNotAvailable();
    }
    return true;
  }
  
  public static boolean initialize()
  {
    if (!BQJNIHelper.isInitialized()) {
      return false;
    }
    return getInstance().initInternal();
  }
  
  public static boolean isAvailable()
  {
    if (!getInstance().m_bInitialized) {
      return false;
    }
    return getInstance().checkProviderStatus(true);
  }
  
  public static boolean isGpsUsable()
  {
    return getInstance().m_bGpsUsable;
  }
  
  public static boolean isNetUsable()
  {
    return getInstance().m_bNetUsable;
  }
  
  private boolean isSameProvider(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return paramString2 == null;
    }
    return paramString1.equals(paramString2);
  }
  
  public static native void lockationChangedKallBacqk(double paramDouble1, double paramDouble2, float paramFloat);
  
  public static boolean pause()
  {
    if (!isAvailable()) {
      return false;
    }
    return getInstance().stopInternal();
  }
  
  public static native void providerDisabledKallBacqk();
  
  public static native void providerEnabledKallBacqk();
  
  public static boolean start()
  {
    if (!isAvailable()) {
      return false;
    }
    return getInstance().startInternal();
  }
  
  public static native void statusChangedKallBacqk();
  
  public static boolean stop()
  {
    return getInstance().stopInternal();
  }
  
  public Location getLastKnownLocation()
  {
    return getLastKnownLocation(new String[] { "gps", "network" });
  }
  
  public Location getLastKnownLocation(String[] paramArrayOfString)
    throws IllegalArgumentException
  {
    Object localObject2;
    if ((this.m_kLocationMgr == null) || (paramArrayOfString == null))
    {
      localObject2 = null;
      return (Location)localObject2;
    }
    Object localObject1 = null;
    int j = paramArrayOfString.length;
    int i = 0;
    for (;;)
    {
      localObject2 = localObject1;
      if (i >= j) {
        break;
      }
      Object localObject3 = paramArrayOfString[i];
      localObject2 = localObject1;
      if (localObject3 != null)
      {
        localObject2 = localObject1;
        if (this.m_kLocationMgr.isProviderEnabled((String)localObject3))
        {
          localObject3 = this.m_kLocationMgr.getLastKnownLocation((String)localObject3);
          localObject2 = localObject1;
          if (isBetterLocation((Location)localObject1, (Location)localObject3)) {
            localObject2 = localObject3;
          }
        }
      }
      i += 1;
      localObject1 = localObject2;
    }
  }
  
  public long getSignificantlyNewer()
  {
    return this.m_significantlyNewer;
  }
  
  public void handleProviderDisabled(String paramString)
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQGeoLocationHeloper.this.checkAndUpdateProviderStatus();
        BQGeoLocationHeloper.providerDisabledKallBacqk();
      }
    });
  }
  
  public void handleProviderEnabled(String paramString)
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQGeoLocationHeloper.this.checkAndUpdateProviderStatus();
        BQGeoLocationHeloper.providerEnabledKallBacqk();
      }
    });
  }
  
  public void handleProviderNotAvailable() {}
  
  public void handleStatusChanged(String paramString, int paramInt, Bundle paramBundle)
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQGeoLocationHeloper.this.checkAndUpdateProviderStatus();
        BQGeoLocationHeloper.statusChangedKallBacqk();
      }
    });
  }
  
  void initListeners()
  {
    if (true != checkAndUpdateProviderStatus()) {}
    do
    {
      return;
      if ((this.m_bGpsUsable) && (this.m_kGpsListener == null)) {
        this.m_kGpsListener = new LocationListener()
        {
          public void onLocationChanged(Location paramAnonymousLocation)
          {
            if (BQGeoLocationHeloper.this.isBetterLocation(paramAnonymousLocation, BQGeoLocationHeloper.this.m_kCurrentLocation)) {
              BQGeoLocationHeloper.this.updateLocation(paramAnonymousLocation, false);
            }
          }
          
          public void onProviderDisabled(String paramAnonymousString)
          {
            BQGeoLocationHeloper.this.handleProviderDisabled(paramAnonymousString);
          }
          
          public void onProviderEnabled(String paramAnonymousString)
          {
            BQGeoLocationHeloper.this.handleProviderEnabled(paramAnonymousString);
          }
          
          public void onStatusChanged(String paramAnonymousString, int paramAnonymousInt, Bundle paramAnonymousBundle)
          {
            BQGeoLocationHeloper.this.handleStatusChanged(paramAnonymousString, paramAnonymousInt, paramAnonymousBundle);
          }
        };
      }
    } while ((!this.m_bNetUsable) || (this.m_kNetListener != null));
    this.m_kNetListener = new LocationListener()
    {
      public void onLocationChanged(Location paramAnonymousLocation)
      {
        if (BQGeoLocationHeloper.this.isBetterLocation(paramAnonymousLocation, BQGeoLocationHeloper.this.m_kCurrentLocation)) {
          BQGeoLocationHeloper.this.updateLocation(paramAnonymousLocation, false);
        }
      }
      
      public void onProviderDisabled(String paramAnonymousString)
      {
        BQGeoLocationHeloper.this.handleProviderDisabled(paramAnonymousString);
      }
      
      public void onProviderEnabled(String paramAnonymousString)
      {
        BQGeoLocationHeloper.this.handleProviderEnabled(paramAnonymousString);
      }
      
      public void onStatusChanged(String paramAnonymousString, int paramAnonymousInt, Bundle paramAnonymousBundle)
      {
        BQGeoLocationHeloper.this.handleStatusChanged(paramAnonymousString, paramAnonymousInt, paramAnonymousBundle);
      }
    };
  }
  
  protected boolean isBetterLocation(Location paramLocation1, Location paramLocation2)
  {
    if (paramLocation1 == null) {
      return false;
    }
    if (paramLocation2 == null) {
      return true;
    }
    long l = paramLocation1.getTime() - paramLocation2.getTime();
    int j;
    int k;
    if (l > this.m_significantlyNewer)
    {
      j = 1;
      if (l >= -this.m_significantlyNewer) {
        break label72;
      }
      k = 1;
      label50:
      if (l <= 0L) {
        break label78;
      }
    }
    label72:
    label78:
    for (int i = 1;; i = 0)
    {
      if (j == 0) {
        break label83;
      }
      return true;
      j = 0;
      break;
      k = 0;
      break label50;
    }
    label83:
    if (k != 0) {
      return false;
    }
    int m = (int)(paramLocation1.getAccuracy() - paramLocation2.getAccuracy());
    if (m > 0)
    {
      j = 1;
      if (m >= 0) {
        break label156;
      }
      k = 1;
      label118:
      if (m <= 200) {
        break label162;
      }
    }
    boolean bool;
    label156:
    label162:
    for (m = 1;; m = 0)
    {
      bool = isSameProvider(paramLocation1.getProvider(), paramLocation2.getProvider());
      if (k == 0) {
        break label168;
      }
      return true;
      j = 0;
      break;
      k = 0;
      break label118;
    }
    label168:
    if ((i != 0) && (j == 0)) {
      return true;
    }
    return (i != 0) && (m == 0) && (bool);
  }
  
  public boolean isUseLastKnownLocation()
  {
    return this.useLastKnownLocation;
  }
  
  public void onLocationChanged(final Location paramLocation)
  {
    try
    {
      this.m_kLock.lock();
      HandlerJava.getInstance().runOnGLThread(new Runnable()
      {
        public void run()
        {
          BQGeoLocationHeloper.lockationChangedKallBacqk(paramLocation.getLatitude(), paramLocation.getLongitude(), paramLocation.getAccuracy());
        }
      });
      return;
    }
    finally
    {
      this.m_kLock.unlock();
    }
  }
  
  public void setSignificantlyNewer(long paramLong)
  {
    this.m_significantlyNewer = paramLong;
  }
  
  public void setUseLastKnownLocation(boolean paramBoolean)
  {
    this.useLastKnownLocation = paramBoolean;
  }
  
  public boolean startInternal()
  {
    if (this.m_kLocationMgr == null) {
      return false;
    }
    this.m_kCurrentLocation = null;
    if (this.useLastKnownLocation)
    {
      Location localLocation = getLastKnownLocation();
      if (localLocation != null) {
        updateLocation(localLocation, true);
      }
    }
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQGeoLocationHeloper.this.initListeners();
        if (BQGeoLocationHeloper.this.m_kNetListener != null) {
          BQGeoLocationHeloper.this.m_kLocationMgr.requestLocationUpdates("network", 5000L, 10.0F, BQGeoLocationHeloper.this.m_kNetListener);
        }
        if (BQGeoLocationHeloper.this.m_kGpsListener != null) {
          BQGeoLocationHeloper.this.m_kLocationMgr.requestLocationUpdates("gps", 5000L, 10.0F, BQGeoLocationHeloper.this.m_kGpsListener);
        }
      }
    });
    return true;
  }
  
  public boolean stopInternal()
  {
    if (this.m_kLocationMgr == null) {
      return false;
    }
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (BQGeoLocationHeloper.this.m_kGpsListener != null) {
          BQGeoLocationHeloper.this.m_kLocationMgr.removeUpdates(BQGeoLocationHeloper.this.m_kGpsListener);
        }
        if (BQGeoLocationHeloper.this.m_kNetListener != null) {
          BQGeoLocationHeloper.this.m_kLocationMgr.removeUpdates(BQGeoLocationHeloper.this.m_kNetListener);
        }
      }
    });
    return true;
  }
  
  void updateLocation(Location paramLocation, boolean paramBoolean)
  {
    this.m_kCurrentLocation = paramLocation;
    onLocationChanged(paramLocation);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQGeoLocationHeloper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */