package jp.co.drecom.bisque.lib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;

public class SoundStateReceiver
  extends BroadcastReceiver
{
  public static String ringerModeToStr(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "UNKNOWN<" + paramInt + ">";
    case 2: 
      return "RINGER_MODE_NORMAL";
    case 0: 
      return "RINGER_MODE_SILENT";
    }
    return "RINGER_MODE_VIBRATE";
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (!BQJNIHelper.isInitialized()) {}
    int i;
    int j;
    do
    {
      do
      {
        do
        {
          return;
          paramContext = (AudioManager)paramContext.getSystemService("audio");
          if (!paramIntent.getAction().equals("android.media.RINGER_MODE_CHANGED")) {
            break;
          }
          i = BQJNIHelper.getLastAudioRingMode();
          j = paramContext.getRingerMode();
          BQJNIHelper.setLastAudioRingMode(j);
          if ((i != 0) && (j == 0))
          {
            BQJNIHelper.enterAudioSilentMode();
            return;
          }
        } while ((i != 0) || (j == 0));
        BQJNIHelper.leaveAudioSilentMode();
        return;
      } while (!paramIntent.getAction().equals("android.media.VOLUME_CHANGED_ACTION"));
      i = BQJNIHelper.getLastAudioVolume();
      j = paramContext.getStreamVolume(3);
    } while (i == j);
    BQJNIHelper.setLastAudioVolume(j);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\SoundStateReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */