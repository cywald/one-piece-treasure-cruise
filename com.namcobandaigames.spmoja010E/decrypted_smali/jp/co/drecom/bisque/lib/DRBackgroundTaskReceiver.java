package jp.co.drecom.bisque.lib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DRBackgroundTaskReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent.getAction().equals("jp.co.drecom.bisque.lib.drbackgroundtask.broadcast.BGTASK"))
    {
      paramContext = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID");
      paramIntent = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.drbackgroundtask.extra.STATUS");
      if ((paramIntent != null) && (paramIntent.equals("jp.co.drecom.bisque.lib.drbackgroundtask.status.FINISH"))) {
        DRBackgroundTaskHelper.finish(paramContext);
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\DRBackgroundTaskReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */