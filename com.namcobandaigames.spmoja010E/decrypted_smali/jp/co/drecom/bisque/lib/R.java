package jp.co.drecom.bisque.lib;

public final class R
{
  public static final class attr
  {
    public static final int buttonSize = 2130837567;
    public static final int circleCrop = 2130837574;
    public static final int colorScheme = 2130837589;
    public static final int font = 2130837618;
    public static final int fontProviderAuthority = 2130837620;
    public static final int fontProviderCerts = 2130837621;
    public static final int fontProviderFetchStrategy = 2130837622;
    public static final int fontProviderFetchTimeout = 2130837623;
    public static final int fontProviderPackage = 2130837624;
    public static final int fontProviderQuery = 2130837625;
    public static final int fontStyle = 2130837626;
    public static final int fontWeight = 2130837627;
    public static final int imageAspectRatio = 2130837638;
    public static final int imageAspectRatioAdjust = 2130837639;
    public static final int scopeUris = 2130837686;
  }
  
  public static final class bool
  {
    public static final int abc_action_bar_embed_tabs = 2130903040;
  }
  
  public static final class color
  {
    public static final int common_google_signin_btn_text_dark = 2130968614;
    public static final int common_google_signin_btn_text_dark_default = 2130968615;
    public static final int common_google_signin_btn_text_dark_disabled = 2130968616;
    public static final int common_google_signin_btn_text_dark_focused = 2130968617;
    public static final int common_google_signin_btn_text_dark_pressed = 2130968618;
    public static final int common_google_signin_btn_text_light = 2130968619;
    public static final int common_google_signin_btn_text_light_default = 2130968620;
    public static final int common_google_signin_btn_text_light_disabled = 2130968621;
    public static final int common_google_signin_btn_text_light_focused = 2130968622;
    public static final int common_google_signin_btn_text_light_pressed = 2130968623;
    public static final int common_google_signin_btn_tint = 2130968624;
    public static final int notification_action_color_filter = 2130968646;
    public static final int notification_icon_bg_color = 2130968647;
    public static final int notification_material_background_media_default_color = 2130968648;
    public static final int primary_text_default_material_dark = 2130968653;
    public static final int ripple_material_light = 2130968658;
    public static final int secondary_text_default_material_dark = 2130968659;
    public static final int secondary_text_default_material_light = 2130968660;
  }
  
  public static final class dimen
  {
    public static final int compat_button_inset_horizontal_material = 2131034186;
    public static final int compat_button_inset_vertical_material = 2131034187;
    public static final int compat_button_padding_horizontal_material = 2131034188;
    public static final int compat_button_padding_vertical_material = 2131034189;
    public static final int compat_control_corner_material = 2131034190;
    public static final int notification_action_icon_size = 2131034200;
    public static final int notification_action_text_size = 2131034201;
    public static final int notification_big_circle_margin = 2131034202;
    public static final int notification_content_margin_start = 2131034203;
    public static final int notification_large_icon_height = 2131034204;
    public static final int notification_large_icon_width = 2131034205;
    public static final int notification_main_column_padding_top = 2131034206;
    public static final int notification_media_narrow_margin = 2131034207;
    public static final int notification_right_icon_size = 2131034208;
    public static final int notification_right_side_padding_top = 2131034209;
    public static final int notification_small_icon_background_padding = 2131034210;
    public static final int notification_small_icon_size_as_large = 2131034211;
    public static final int notification_subtext_size = 2131034212;
    public static final int notification_top_pad = 2131034213;
    public static final int notification_top_pad_large_text = 2131034214;
  }
  
  public static final class drawable
  {
    public static final int common_full_open_on_phone = 2131099731;
    public static final int common_google_signin_btn_icon_dark = 2131099732;
    public static final int common_google_signin_btn_icon_dark_focused = 2131099733;
    public static final int common_google_signin_btn_icon_dark_normal = 2131099734;
    public static final int common_google_signin_btn_icon_dark_normal_background = 2131099735;
    public static final int common_google_signin_btn_icon_disabled = 2131099736;
    public static final int common_google_signin_btn_icon_light = 2131099737;
    public static final int common_google_signin_btn_icon_light_focused = 2131099738;
    public static final int common_google_signin_btn_icon_light_normal = 2131099739;
    public static final int common_google_signin_btn_icon_light_normal_background = 2131099740;
    public static final int common_google_signin_btn_text_dark = 2131099741;
    public static final int common_google_signin_btn_text_dark_focused = 2131099742;
    public static final int common_google_signin_btn_text_dark_normal = 2131099743;
    public static final int common_google_signin_btn_text_dark_normal_background = 2131099744;
    public static final int common_google_signin_btn_text_disabled = 2131099745;
    public static final int common_google_signin_btn_text_light = 2131099746;
    public static final int common_google_signin_btn_text_light_focused = 2131099747;
    public static final int common_google_signin_btn_text_light_normal = 2131099748;
    public static final int common_google_signin_btn_text_light_normal_background = 2131099749;
    public static final int googleg_disabled_color_18 = 2131099750;
    public static final int googleg_standard_color_18 = 2131099751;
    public static final int notification_action_background = 2131099754;
    public static final int notification_bg = 2131099755;
    public static final int notification_bg_low = 2131099756;
    public static final int notification_bg_low_normal = 2131099757;
    public static final int notification_bg_low_pressed = 2131099758;
    public static final int notification_bg_normal = 2131099759;
    public static final int notification_bg_normal_pressed = 2131099760;
    public static final int notification_icon_background = 2131099761;
    public static final int notification_template_icon_bg = 2131099762;
    public static final int notification_template_icon_low_bg = 2131099763;
    public static final int notification_tile_bg = 2131099764;
    public static final int notify_panel_notification_icon_bg = 2131099765;
  }
  
  public static final class id
  {
    public static final int action0 = 2131165190;
    public static final int action_container = 2131165198;
    public static final int action_divider = 2131165200;
    public static final int action_image = 2131165201;
    public static final int action_text = 2131165207;
    public static final int actions = 2131165208;
    public static final int adjust_height = 2131165211;
    public static final int adjust_width = 2131165212;
    public static final int async = 2131165215;
    public static final int auto = 2131165216;
    public static final int blocking = 2131165218;
    public static final int cancel_action = 2131165221;
    public static final int chronometer = 2131165223;
    public static final int dark = 2131165228;
    public static final int end_padder = 2131165234;
    public static final int forever = 2131165237;
    public static final int icon = 2131165240;
    public static final int icon_group = 2131165241;
    public static final int icon_only = 2131165242;
    public static final int info = 2131165245;
    public static final int italic = 2131165246;
    public static final int light = 2131165247;
    public static final int line1 = 2131165248;
    public static final int line3 = 2131165249;
    public static final int media_actions = 2131165252;
    public static final int none = 2131165257;
    public static final int normal = 2131165258;
    public static final int notification_background = 2131165259;
    public static final int notification_main_column = 2131165260;
    public static final int notification_main_column_container = 2131165261;
    public static final int right_icon = 2131165266;
    public static final int right_side = 2131165267;
    public static final int standard = 2131165292;
    public static final int status_bar_latest_event_content = 2131165293;
    public static final int text = 2131165297;
    public static final int text2 = 2131165298;
    public static final int time = 2131165301;
    public static final int title = 2131165302;
    public static final int wide = 2131165310;
  }
  
  public static final class integer
  {
    public static final int cancel_button_image_alpha = 2131230722;
    public static final int google_play_services_version = 2131230724;
    public static final int status_bar_notification_info_maxnum = 2131230726;
  }
  
  public static final class layout
  {
    public static final int notification_action = 2131296283;
    public static final int notification_action_tombstone = 2131296284;
    public static final int notification_media_action = 2131296285;
    public static final int notification_media_cancel_action = 2131296286;
    public static final int notification_template_big_media = 2131296287;
    public static final int notification_template_big_media_custom = 2131296288;
    public static final int notification_template_big_media_narrow = 2131296289;
    public static final int notification_template_big_media_narrow_custom = 2131296290;
    public static final int notification_template_custom_big = 2131296291;
    public static final int notification_template_icon_group = 2131296292;
    public static final int notification_template_lines_media = 2131296293;
    public static final int notification_template_media = 2131296294;
    public static final int notification_template_media_custom = 2131296295;
    public static final int notification_template_part_chronometer = 2131296296;
    public static final int notification_template_part_time = 2131296297;
  }
  
  public static final class string
  {
    public static final int common_google_play_services_enable_button = 2131361825;
    public static final int common_google_play_services_enable_text = 2131361826;
    public static final int common_google_play_services_enable_title = 2131361827;
    public static final int common_google_play_services_install_button = 2131361828;
    public static final int common_google_play_services_install_text = 2131361829;
    public static final int common_google_play_services_install_title = 2131361830;
    public static final int common_google_play_services_notification_channel_name = 2131361831;
    public static final int common_google_play_services_notification_ticker = 2131361832;
    public static final int common_google_play_services_unknown_issue = 2131361833;
    public static final int common_google_play_services_unsupported_text = 2131361834;
    public static final int common_google_play_services_update_button = 2131361835;
    public static final int common_google_play_services_update_text = 2131361836;
    public static final int common_google_play_services_update_title = 2131361837;
    public static final int common_google_play_services_updating_text = 2131361838;
    public static final int common_google_play_services_wear_update_text = 2131361839;
    public static final int common_open_on_phone = 2131361840;
    public static final int common_signin_button_text = 2131361841;
    public static final int common_signin_button_text_long = 2131361842;
    public static final int fcm_fallback_notification_channel_label = 2131361844;
    public static final int gamehelper_app_misconfigured = 2131361846;
    public static final int gamehelper_license_failed = 2131361847;
    public static final int gamehelper_sign_in_failed = 2131361848;
    public static final int gamehelper_unknown_error = 2131361849;
    public static final int jp_co_drecom_bqpayment_notfoundservice_dialog_message = 2131361855;
    public static final int jp_co_drecom_bqpayment_notfoundservice_dialog_title = 2131361856;
    public static final int jp_co_drecom_bqupdate_dialog_message = 2131361857;
    public static final int jp_co_drecom_bqupdate_dialog_title = 2131361858;
    public static final int jp_co_drecom_common_label_Exit = 2131361859;
    public static final int jp_co_drecom_common_label_cancel = 2131361860;
    public static final int jp_co_drecom_common_label_ok = 2131361861;
    public static final int status_bar_notification_info_overflow = 2131361868;
  }
  
  public static final class style
  {
    public static final int TextAppearance_Compat_Notification = 2131427578;
    public static final int TextAppearance_Compat_Notification_Info = 2131427579;
    public static final int TextAppearance_Compat_Notification_Info_Media = 2131427580;
    public static final int TextAppearance_Compat_Notification_Line2 = 2131427581;
    public static final int TextAppearance_Compat_Notification_Line2_Media = 2131427582;
    public static final int TextAppearance_Compat_Notification_Media = 2131427583;
    public static final int TextAppearance_Compat_Notification_Time = 2131427584;
    public static final int TextAppearance_Compat_Notification_Time_Media = 2131427585;
    public static final int TextAppearance_Compat_Notification_Title = 2131427586;
    public static final int TextAppearance_Compat_Notification_Title_Media = 2131427587;
    public static final int Widget_Compat_NotificationActionContainer = 2131427691;
    public static final int Widget_Compat_NotificationActionText = 2131427692;
  }
  
  public static final class styleable
  {
    public static final int[] FontFamily = { 2130837620, 2130837621, 2130837622, 2130837623, 2130837624, 2130837625 };
    public static final int[] FontFamilyFont = { 2130837618, 2130837626, 2130837627 };
    public static final int FontFamilyFont_font = 0;
    public static final int FontFamilyFont_fontStyle = 1;
    public static final int FontFamilyFont_fontWeight = 2;
    public static final int FontFamily_fontProviderAuthority = 0;
    public static final int FontFamily_fontProviderCerts = 1;
    public static final int FontFamily_fontProviderFetchStrategy = 2;
    public static final int FontFamily_fontProviderFetchTimeout = 3;
    public static final int FontFamily_fontProviderPackage = 4;
    public static final int FontFamily_fontProviderQuery = 5;
    public static final int[] LoadingImageView = { 2130837574, 2130837638, 2130837639 };
    public static final int LoadingImageView_circleCrop = 0;
    public static final int LoadingImageView_imageAspectRatio = 1;
    public static final int LoadingImageView_imageAspectRatioAdjust = 2;
    public static final int[] SignInButton = { 2130837567, 2130837589, 2130837686 };
    public static final int SignInButton_buttonSize = 0;
    public static final int SignInButton_colorScheme = 1;
    public static final int SignInButton_scopeUris = 2;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */