package jp.co.drecom.bisque.lib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.cocos2dx.lib.Cocos2dxHelper;

public class LocaleChangedReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (!BQJNIHelper.isInitialized()) {
      return;
    }
    Cocos2dxHelper.terminateProcess();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\LocaleChangedReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */