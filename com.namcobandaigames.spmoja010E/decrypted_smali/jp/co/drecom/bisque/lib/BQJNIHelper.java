package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Vibrator;
import java.lang.reflect.Method;

public class BQJNIHelper
{
  private static BQJNIHelper self = null;
  private Activity activity;
  private int audioSampleBufSize = -1;
  private int audioSampleRate = -1;
  private AudioManager audioSvc;
  private boolean isSSRChecked = false;
  private boolean isSSRMapped = false;
  private int lastAudioRingMode = -1;
  private int lastAudioVolume = -1;
  private Vibrator vibratorSvc;
  
  public static native void abort();
  
  public static native void enterAudioSilentMode();
  
  public static Activity getActivity()
  {
    return getInstance().activity;
  }
  
  public static float getAudioMasterVolume()
  {
    int i = getInstance().audioSvc.getStreamVolume(3);
    int j = getInstance().audioSvc.getStreamMaxVolume(3);
    if (i == 0) {
      return 0.0F;
    }
    return 100.0F * (i / j);
  }
  
  public static int getAudioOutputDevice()
  {
    if ((true == getAudioSvc().isWiredHeadsetOn()) || (getAudioSvc().isBluetoothA2dpOn())) {}
    for (int i = 0x0 | 0x2000000;; i = 0x0 | 0x1000000)
    {
      int j = i;
      if (getAudioSvc().getRingerMode() != 0) {
        j = i | 0x4000;
      }
      return j;
    }
  }
  
  public static int getAudioSampleBufSize()
  {
    if (self == null) {
      return -1;
    }
    return self.audioSampleBufSize;
  }
  
  public static int getAudioSampleRate()
  {
    if (self == null) {
      return -1;
    }
    return self.audioSampleRate;
  }
  
  public static AudioManager getAudioSvc()
  {
    return getInstance().audioSvc;
  }
  
  public static BQJNIHelper getInstance()
  {
    if (self == null) {
      self = new BQJNIHelper();
    }
    return self;
  }
  
  public static int getLastAudioRingMode()
  {
    if (self == null) {
      return -1;
    }
    return self.lastAudioRingMode;
  }
  
  public static int getLastAudioVolume()
  {
    if (self == null) {
      return -1;
    }
    return self.lastAudioVolume;
  }
  
  public static Object getSystemServiceWXP(String paramString)
  {
    if (!isInitialized()) {
      return null;
    }
    return self.activity.getSystemService(paramString);
  }
  
  private void initInstance(Activity paramActivity)
  {
    this.activity = paramActivity;
    this.audioSvc = ((AudioManager)this.activity.getSystemService("audio"));
    try
    {
      this.vibratorSvc = ((Vibrator)this.activity.getSystemService("vibrator"));
      initAudioState();
      return;
    }
    catch (Exception paramActivity)
    {
      for (;;)
      {
        this.vibratorSvc = null;
      }
    }
  }
  
  public static native void initNativeContext(Object paramObject);
  
  public static native void initNativeContextDisableSound(Object paramObject);
  
  public static boolean isAudioSilentMode()
  {
    return getAudioSvc().getRingerMode() == 0;
  }
  
  public static boolean isInitialized()
  {
    if (self == null) {}
    while (self.activity == null) {
      return false;
    }
    return true;
  }
  
  public static boolean isSoundStateRecieverMapped()
  {
    if (self == null) {
      return false;
    }
    if (true == self.isSSRChecked) {
      return self.isSSRMapped;
    }
    self.isSSRChecked = true;
    self.isSSRMapped = false;
    Object localObject = self.activity;
    for (;;)
    {
      int i;
      try
      {
        localObject = ((Activity)localObject).getPackageManager().getPackageInfo(((Activity)localObject).getPackageName(), 2);
        if (localObject == null) {
          break;
        }
        ActivityInfo[] arrayOfActivityInfo = ((PackageInfo)localObject).receivers;
        if (arrayOfActivityInfo == null) {
          break;
        }
        localObject = ((PackageInfo)localObject).receivers;
        int j = localObject.length;
        i = 0;
        if (i >= j) {
          break;
        }
        if (localObject[i].name.equalsIgnoreCase(SoundStateReceiver.class.getName()))
        {
          self.isSSRMapped = true;
          return true;
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        return false;
      }
      i += 1;
    }
  }
  
  public static native void leaveAudioSilentMode();
  
  public static void setContext(Activity paramActivity)
  {
    getInstance().initInstance(paramActivity);
  }
  
  public static void setLastAudioRingMode(int paramInt)
  {
    if (self == null) {
      return;
    }
    self.lastAudioRingMode = paramInt;
  }
  
  public static void setLastAudioVolume(int paramInt)
  {
    if (self == null) {
      return;
    }
    self.lastAudioVolume = paramInt;
  }
  
  public static void vibrate(final int paramInt1, int paramInt2)
  {
    if ((self == null) || (self.vibratorSvc == null)) {
      return;
    }
    self.activity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        Vibrator localVibrator = BQJNIHelper.self.vibratorSvc;
        long l = Math.max(this.val$_Inteval, 100);
        int i = paramInt1;
        localVibrator.vibrate(new long[] { 0L, l }, i);
      }
    });
  }
  
  public void initAudioState()
  {
    Object localObject = getAudioSvc();
    this.lastAudioVolume = ((AudioManager)localObject).getStreamVolume(3);
    this.lastAudioRingMode = ((AudioManager)localObject).getRingerMode();
    if (Build.VERSION.SDK_INT < 17) {}
    for (;;)
    {
      return;
      for (;;)
      {
        try
        {
          localMethod = localObject.getClass().getMethod("getProperty", new Class[] { String.class });
          if (localMethod == null) {
            break;
          }
        }
        catch (Exception localException2)
        {
          Method localMethod;
          String str;
          return;
        }
        try
        {
          str = (String)localMethod.invoke(localObject, new Object[] { "android.media.property.OUTPUT_SAMPLE_RATE" });
          if (str != null) {
            this.audioSampleRate = Integer.parseInt(str);
          }
        }
        catch (Exception localException3)
        {
          this.audioSampleRate = -1;
          continue;
        }
        try
        {
          localObject = (String)localMethod.invoke(localObject, new Object[] { "android.media.property.OUTPUT_FRAMES_PER_BUFFER" });
          if (localObject == null) {
            break;
          }
          this.audioSampleBufSize = Integer.parseInt((String)localObject);
          return;
        }
        catch (Exception localException1)
        {
          this.audioSampleBufSize = -1;
          return;
        }
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQJNIHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */