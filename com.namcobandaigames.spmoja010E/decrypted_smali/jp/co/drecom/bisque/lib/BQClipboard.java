package jp.co.drecom.bisque.lib;

import android.content.Context;
import android.text.ClipboardManager;

public class BQClipboard
{
  private Context context;
  
  public BQClipboard(Context paramContext)
  {
    this.context = paramContext;
  }
  
  public String getStringFromClipboard()
  {
    ClipboardManager localClipboardManager = (ClipboardManager)this.context.getSystemService("clipboard");
    if (localClipboardManager == null) {}
    while (localClipboardManager.getText() == null) {
      return null;
    }
    return localClipboardManager.getText().toString();
  }
  
  public void setStringToClipboard(String paramString)
  {
    ((ClipboardManager)this.context.getSystemService("clipboard")).setText(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQClipboard.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */