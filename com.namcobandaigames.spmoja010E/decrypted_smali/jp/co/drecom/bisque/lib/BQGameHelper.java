package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions.Builder;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.AchievementsClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class BQGameHelper
{
  private static final int RC_ACHIEVEMENT_UI = 9003;
  private static final int RC_SIGN_IN = 9001;
  private static final int RC_UNUSED = 5001;
  private static final String TAG = "BQGameHelper";
  private AchievementsClient mAchievementsClient = null;
  Activity mActivity = null;
  Context mAppContext = null;
  private GoogleSignInClient mGoogleSignInClient = null;
  BQGameHelperListener mListener = null;
  
  public BQGameHelper(Activity paramActivity)
  {
    this.mActivity = paramActivity;
    this.mAppContext = paramActivity.getApplicationContext();
  }
  
  private void onConnected(GoogleSignInAccount paramGoogleSignInAccount)
  {
    this.mAchievementsClient = Games.getAchievementsClient(this.mActivity, paramGoogleSignInAccount);
  }
  
  private void onDisconnected()
  {
    this.mAchievementsClient = null;
    if (this.mListener != null) {
      this.mListener.onDisconnected();
    }
  }
  
  private void signInSilently()
  {
    if (this.mGoogleSignInClient == null) {
      return;
    }
    this.mGoogleSignInClient.silentSignIn().addOnCompleteListener(this.mActivity, new OnCompleteListener()
    {
      public void onComplete(@NonNull Task<GoogleSignInAccount> paramAnonymousTask)
      {
        if (paramAnonymousTask.isSuccessful())
        {
          paramAnonymousTask = (GoogleSignInAccount)paramAnonymousTask.getResult();
          BQGameHelper.this.onConnected(paramAnonymousTask);
          if (BQGameHelper.this.mListener != null) {
            BQGameHelper.this.mListener.onSilentSignInSucceeded(paramAnonymousTask);
          }
          return;
        }
        if (BQGameHelper.this.mListener != null) {
          BQGameHelper.this.mListener.onSilentSignInFailed(paramAnonymousTask.getException());
        }
        BQGameHelper.this.onDisconnected();
      }
    });
  }
  
  public boolean isSignedIn()
  {
    return GoogleSignIn.getLastSignedInAccount(this.mActivity) != null;
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 9001)
    {
      paramIntent = Auth.GoogleSignInApi.getSignInResultFromIntent(paramIntent);
      if ((paramIntent != null) && (paramIntent.isSuccess()))
      {
        paramIntent = paramIntent.getSignInAccount();
        onConnected(paramIntent);
        if (this.mListener != null) {
          this.mListener.onInteractiveSignInSucceeded(paramIntent);
        }
      }
    }
    while (paramInt1 != 9003)
    {
      return;
      if ((paramIntent != null) && (this.mListener != null)) {
        this.mListener.onInteractiveSignInFailed(paramIntent.getStatus());
      }
      onDisconnected();
      return;
    }
  }
  
  public void onResume()
  {
    signInSilently();
  }
  
  public void setup(BQGameHelperListener paramBQGameHelperListener)
  {
    this.mListener = paramBQGameHelperListener;
    this.mGoogleSignInClient = GoogleSignIn.getClient(this.mActivity, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN).build());
  }
  
  public void showAchievements()
  {
    Games.getAchievementsClient(this.mActivity, GoogleSignIn.getLastSignedInAccount(this.mActivity)).getAchievementsIntent().addOnSuccessListener(new OnSuccessListener()
    {
      public void onSuccess(Intent paramAnonymousIntent)
      {
        BQGameHelper.this.mActivity.startActivityForResult(paramAnonymousIntent, 9003);
      }
    });
  }
  
  public void signOut()
  {
    if (this.mGoogleSignInClient == null) {}
    while (!isSignedIn()) {
      return;
    }
    this.mGoogleSignInClient.signOut().addOnCompleteListener(this.mActivity, new OnCompleteListener()
    {
      public void onComplete(@NonNull Task<Void> paramAnonymousTask)
      {
        if (BQGameHelper.this.mListener != null) {
          BQGameHelper.this.mListener.onDisconnected();
        }
      }
    });
  }
  
  public void startSignInIntent()
  {
    if (this.mGoogleSignInClient == null) {
      return;
    }
    Intent localIntent = this.mGoogleSignInClient.getSignInIntent();
    this.mActivity.startActivityForResult(localIntent, 9001);
  }
  
  public void unlockAchievement(String paramString)
  {
    Games.getAchievementsClient(this.mActivity, GoogleSignIn.getLastSignedInAccount(this.mActivity)).unlock(paramString);
  }
  
  public static abstract interface BQGameHelperListener
  {
    public abstract void onDisconnected();
    
    public abstract void onInteractiveSignInFailed(Status paramStatus);
    
    public abstract void onInteractiveSignInSucceeded(GoogleSignInAccount paramGoogleSignInAccount);
    
    public abstract void onSilentSignInFailed(Exception paramException);
    
    public abstract void onSilentSignInSucceeded(GoogleSignInAccount paramGoogleSignInAccount);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQGameHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */