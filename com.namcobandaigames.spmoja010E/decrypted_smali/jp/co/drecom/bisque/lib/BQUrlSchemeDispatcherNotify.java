package jp.co.drecom.bisque.lib;

import android.os.Handler;

public class BQUrlSchemeDispatcherNotify
{
  private static BQUrlSchemeDispatcher dispatcher;
  private static Handler handler;
  
  public static void execOpenApplicationDetailsSettings()
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQUrlSchemeDispatcherNotify.dispatcher.execOpenApplicationDetailsSettings();
      }
    });
  }
  
  public static void execUrlScheme(String paramString)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQUrlSchemeDispatcherNotify.dispatcher.execUrlScheme(this.val$_string);
      }
    });
  }
  
  public static void setDispatcher(BQUrlSchemeDispatcher paramBQUrlSchemeDispatcher, Handler paramHandler)
  {
    dispatcher = paramBQUrlSchemeDispatcher;
    handler = paramHandler;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQUrlSchemeDispatcherNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */