package jp.co.drecom.bisque.lib;

import android.os.Handler;

public class BQUpdateDispatcherNotify
{
  private static BQUpdateDispatcher dispatcher;
  private static Handler handler;
  
  public static void checkUpdate(boolean paramBoolean, final int paramInt, final String paramString)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQUpdateDispatcherNotify.dispatcher.checkUpdate(this.val$_bqnotify, paramInt, paramString);
      }
    });
  }
  
  public static void execReview(String paramString)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQUpdateDispatcherNotify.dispatcher.execReview(this.val$_android_review_uri);
      }
    });
  }
  
  public static void execUpdate(String paramString)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQUpdateDispatcherNotify.dispatcher.execUpdate(this.val$_android_update_uri);
      }
    });
  }
  
  public static void setDispatcher(BQUpdateDispatcher paramBQUpdateDispatcher, Handler paramHandler)
  {
    dispatcher = paramBQUpdateDispatcher;
    handler = paramHandler;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQUpdateDispatcherNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */