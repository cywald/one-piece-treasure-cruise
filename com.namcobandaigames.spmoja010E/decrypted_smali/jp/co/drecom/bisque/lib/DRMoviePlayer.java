package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.VideoView;

public class DRMoviePlayer
{
  private Activity m_context = null;
  private int m_height = 0;
  private boolean m_isPaused = false;
  private boolean m_isPausedOnSuspend = false;
  private boolean m_isSuspendSequence = false;
  private RelativeLayout m_layout = null;
  private int m_posOnPause = -1;
  private String m_source = null;
  private Uri m_uri = null;
  private VideoView m_videoView = null;
  private int m_width = 0;
  private int m_x = 0;
  private int m_y = 0;
  
  public DRMoviePlayer(Activity paramActivity, RelativeLayout paramRelativeLayout)
  {
    this.m_context = paramActivity;
    this.m_layout = paramRelativeLayout;
  }
  
  public static native void nativeCallbackMovieFinished();
  
  public static native void nativeCallbackMovieStarted();
  
  public boolean isPaused()
  {
    return this.m_isPaused;
  }
  
  public boolean isPlaying()
  {
    return this.m_videoView != null;
  }
  
  public void onResume()
  {
    if (this.m_isSuspendSequence) {
      play();
    }
  }
  
  public void onSuspend()
  {
    if (this.m_videoView == null) {
      return;
    }
    this.m_isSuspendSequence = true;
    this.m_isPausedOnSuspend = isPaused();
    this.m_posOnPause = this.m_videoView.getCurrentPosition();
    stop();
  }
  
  public boolean pause()
  {
    if (this.m_videoView == null) {}
    while (!this.m_videoView.canPause()) {
      return false;
    }
    this.m_videoView.pause();
    this.m_isPaused = true;
    return true;
  }
  
  public boolean play()
  {
    if (this.m_videoView != null)
    {
      this.m_videoView.start();
      this.m_isPaused = false;
    }
    do
    {
      return true;
      if ((this.m_source == null) && (this.m_uri == null)) {
        return false;
      }
      this.m_videoView = new VideoView(this.m_context);
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(this.m_width, this.m_height);
      localLayoutParams.leftMargin = this.m_x;
      localLayoutParams.topMargin = this.m_y;
      this.m_videoView.setLayoutParams(localLayoutParams);
      this.m_layout.addView(this.m_videoView, localLayoutParams);
      this.m_videoView.setZOrderOnTop(true);
      this.m_videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
      {
        public void onPrepared(MediaPlayer paramAnonymousMediaPlayer)
        {
          if (DRMoviePlayer.this.m_isSuspendSequence)
          {
            DRMoviePlayer.this.m_videoView.seekTo(DRMoviePlayer.this.m_posOnPause);
            DRMoviePlayer.access$102(DRMoviePlayer.this, -1);
            if (DRMoviePlayer.this.m_isPausedOnSuspend)
            {
              DRMoviePlayer.this.m_videoView.pause();
              DRMoviePlayer.access$402(DRMoviePlayer.this, DRMoviePlayer.this.m_isPausedOnSuspend);
            }
          }
          for (;;)
          {
            if (!DRMoviePlayer.this.m_isSuspendSequence) {
              DRMoviePlayer.nativeCallbackMovieStarted();
            }
            DRMoviePlayer.access$302(DRMoviePlayer.this, false);
            DRMoviePlayer.access$002(DRMoviePlayer.this, false);
            return;
            DRMoviePlayer.this.m_videoView.start();
            break;
            DRMoviePlayer.this.m_videoView.start();
            DRMoviePlayer.access$402(DRMoviePlayer.this, false);
          }
        }
      });
      this.m_videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
      {
        public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
        {
          DRMoviePlayer.this.stop();
        }
      });
      if (this.m_source != null)
      {
        this.m_videoView.setVideoPath(this.m_source);
        return true;
      }
    } while (this.m_uri == null);
    this.m_videoView.setVideoURI(this.m_uri);
    return true;
  }
  
  public boolean setFile(String paramString)
  {
    this.m_source = paramString;
    this.m_uri = null;
    return true;
  }
  
  public boolean setRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.m_x = paramInt1;
    this.m_y = paramInt2;
    this.m_width = paramInt3;
    this.m_height = paramInt4;
    return true;
  }
  
  public boolean setUri(String paramString)
  {
    this.m_source = null;
    if (paramString == null) {
      return false;
    }
    this.m_uri = Uri.parse(paramString);
    return true;
  }
  
  public boolean stop()
  {
    if (this.m_videoView == null) {
      return false;
    }
    this.m_videoView.stopPlayback();
    this.m_layout.removeView(this.m_videoView);
    this.m_videoView = null;
    this.m_isPaused = false;
    if (!this.m_isSuspendSequence) {
      nativeCallbackMovieFinished();
    }
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\DRMoviePlayer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */