package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.util.Log;

public class BQArtilleryJava
{
  private native boolean callbackMethod(long paramLong);
  
  public boolean invoke(final long paramLong)
  {
    Activity localActivity = BQJNIHelper.getActivity();
    if (localActivity == null)
    {
      Log.e("BQArtilleryJava", "NOT initialized!!!");
      return false;
    }
    localActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQArtilleryJava.this.callbackMethod(paramLong);
      }
    });
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQArtilleryJava.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */