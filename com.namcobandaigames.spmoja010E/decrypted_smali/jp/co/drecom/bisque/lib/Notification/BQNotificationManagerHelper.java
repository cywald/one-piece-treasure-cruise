package jp.co.drecom.bisque.lib.Notification;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import jp.co.drecom.bisque.lib.BQAppPlatformManager;
import jp.co.drecom.bisque.lib.BQJNIHelper;

public class BQNotificationManagerHelper
{
  public static final String EXTRA_ACTIVITY_NAME = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME";
  public static final String EXTRA_CHANNEL_ID = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID";
  public static final String EXTRA_ICON_COLOR = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR";
  public static final String EXTRA_ICON_ID = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID";
  public static final String EXTRA_ID = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID";
  public static final String EXTRA_KEY = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY";
  public static final String EXTRA_MESSAGE = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE";
  public static final String EXTRA_TITLE = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE";
  public static final String EXTRA_TYPE = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TYPE";
  private static final String TAG = "BQNotificationManagerHelper";
  public static final String TYPE_LOCAL_NOTIFICATION_PREFIX = "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-";
  public static final String TYPE_REMOTE_NOTIFICATION = "application/vnd.jp.co.drecom.bisque.lib.Notification.remote-notification-key";
  private static BQNotificationManagerHelper mInstance = null;
  private boolean mInitialized = false;
  private String mToken = null;
  
  private void _createNotificationChannel(final String paramString1, final String paramString2, final int paramInt, final boolean paramBoolean)
  {
    if ((!isLessThanAndroidO()) && (BQJNIHelper.isInitialized()))
    {
      BQJNIHelper.getInstance();
      BQJNIHelper.getActivity().runOnUiThread(new Runnable()
      {
        public void run()
        {
          NotificationChannel localNotificationChannel = new NotificationChannel(paramString1, paramString2, paramInt);
          localNotificationChannel.enableVibration(paramBoolean);
          BQNotificationManagerHelper.access$000().createNotificationChannel(localNotificationChannel);
        }
      });
    }
  }
  
  private void _deleteNotificationChannel(final String paramString)
  {
    if ((!isLessThanAndroidO()) && (BQJNIHelper.isInitialized()))
    {
      BQJNIHelper.getInstance();
      BQJNIHelper.getActivity().runOnUiThread(new Runnable()
      {
        public void run()
        {
          BQNotificationManagerHelper.access$000().deleteNotificationChannel(paramString);
        }
      });
    }
  }
  
  private boolean _isAutoInitEnabled()
  {
    return FirebaseMessaging.getInstance().isAutoInitEnabled();
  }
  
  private void _registLocalNotification(final long paramLong1, final String paramString1, final String paramString2, String paramString3, final String paramString4, final long paramLong2)
  {
    if (!BQJNIHelper.isInitialized()) {
      return;
    }
    BQJNIHelper.getInstance();
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQJNIHelper.getInstance();
        Activity localActivity = BQJNIHelper.getActivity();
        String str2 = "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-" + paramString1;
        String str3 = BQNotificationManagerHelper.this.getResString("app_name");
        int i = BQNotificationManagerHelper.this.getResId("app_name", "string");
        BQJNIHelper.getInstance();
        String str4 = BQJNIHelper.getActivity().getClass().getName();
        if (str3 == null) {
          return;
        }
        int j = BQNotificationManagerHelper.this.getResId("ic_statusbar", "drawable");
        String str1 = paramString4;
        Object localObject = str1;
        if (str1.isEmpty()) {
          localObject = BQNotificationManagerHelper.this.getResChannelId();
        }
        long l = 1000L * paramLong1;
        localObject = new BQNotificationManagerHelper.RegistParams(BQNotificationManagerHelper.this, str2, j, paramString2, paramString1, str3, paramLong2, i, str4, (String)localObject);
        if (BQNotificationManagerHelper.this.isLessThanAndroidO())
        {
          BQNotificationIntentService.schedule(localActivity, (BQNotificationManagerHelper.RegistParams)localObject, l);
          return;
        }
        BQNotificationJobService.schedule(localActivity, (BQNotificationManagerHelper.RegistParams)localObject, l - System.currentTimeMillis(), this.val$_deadline);
      }
    });
  }
  
  private void _registRemoteNotification()
  {
    if (this.mToken == null) {
      this.mToken = FirebaseInstanceId.getInstance().getToken();
    }
    if (this.mToken != null) {
      handleRegistered(this.mToken);
    }
  }
  
  private void _setAutoInitEnabled(boolean paramBoolean)
  {
    FirebaseMessaging.getInstance().setAutoInitEnabled(paramBoolean);
  }
  
  public static void cancelLocalNotification(String paramString)
  {
    getInstance()._cancelLocalNotification(paramString);
  }
  
  public static void createNotificationChannel()
  {
    createNotificationChannel(getInstance().getResString("notification_channel_id"), getInstance().getResString("notification_channel_name"), getInstance().getResInteger("notification_channel_importance"), getInstance().getResBoolean("notification_channel_enable_vibration"));
  }
  
  public static void createNotificationChannel(String paramString1, String paramString2)
  {
    createNotificationChannel(paramString1, paramString2, 3, false);
  }
  
  public static void createNotificationChannel(String paramString1, String paramString2, int paramInt, boolean paramBoolean)
  {
    getInstance()._createNotificationChannel(paramString1, paramString2, paramInt, paramBoolean);
  }
  
  public static RegistParams createRegistParams(String paramString1, int paramInt1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt2, String paramString6, String paramString7)
  {
    BQNotificationManagerHelper localBQNotificationManagerHelper = getInstance();
    localBQNotificationManagerHelper.getClass();
    return new RegistParams(localBQNotificationManagerHelper, paramString1, paramInt1, paramString2, paramString3, paramString4, paramString5, paramInt2, paramString6, paramString7);
  }
  
  public static void deleteNotificationChannel()
  {
    deleteNotificationChannel(getInstance().getResChannelId());
  }
  
  public static void deleteNotificationChannel(String paramString)
  {
    getInstance()._deleteNotificationChannel(paramString);
  }
  
  public static void finalization()
  {
    if (mInstance != null) {
      mInstance = null;
    }
  }
  
  public static BQNotificationManagerHelper getInstance()
  {
    if (mInstance == null) {
      mInstance = new BQNotificationManagerHelper();
    }
    return mInstance;
  }
  
  private static NotificationManager getNotificationManager()
  {
    BQJNIHelper.getInstance();
    return (NotificationManager)BQJNIHelper.getActivity().getSystemService("notification");
  }
  
  private boolean getResBoolean(String paramString)
  {
    BQJNIHelper.getInstance();
    Activity localActivity = BQJNIHelper.getActivity();
    Resources localResources = localActivity.getResources();
    return localResources.getBoolean(localResources.getIdentifier(paramString, "bool", localActivity.getPackageName()));
  }
  
  private int getResId(String paramString1, String paramString2)
  {
    BQJNIHelper.getInstance();
    Activity localActivity = BQJNIHelper.getActivity();
    return localActivity.getResources().getIdentifier(paramString1, paramString2, localActivity.getPackageName());
  }
  
  private int getResInteger(String paramString)
  {
    BQJNIHelper.getInstance();
    Activity localActivity = BQJNIHelper.getActivity();
    Resources localResources = localActivity.getResources();
    return localResources.getInteger(localResources.getIdentifier(paramString, "integer", localActivity.getPackageName()));
  }
  
  private String getResString(String paramString)
  {
    BQJNIHelper.getInstance();
    Activity localActivity = BQJNIHelper.getActivity();
    Resources localResources = localActivity.getResources();
    return localResources.getString(localResources.getIdentifier(paramString, "string", localActivity.getPackageName()));
  }
  
  private Resources getResources()
  {
    BQJNIHelper.getInstance();
    return BQJNIHelper.getActivity().getResources();
  }
  
  private void handleError(String paramString)
  {
    receiveRegisterForRemoteNotificationError(paramString);
  }
  
  private void handleRegistered(String paramString)
  {
    receiveRegisterForRemoteNotification(paramString);
  }
  
  private boolean initInternal()
  {
    if (this.mInitialized == true) {
      return true;
    }
    this.mInitialized = true;
    createNotificationChannel();
    return true;
  }
  
  public static boolean initialize()
  {
    if (!BQJNIHelper.isInitialized()) {
      return false;
    }
    return getInstance().initInternal();
  }
  
  public static boolean isAutoInitEnabled()
  {
    return getInstance()._isAutoInitEnabled();
  }
  
  private boolean isLessThanAndroidO()
  {
    return BQAppPlatformManager.getOsVersionCode() < 26;
  }
  
  public static native void nativeCallbackFuncForReceiveLocalNotification(String paramString1, String paramString2);
  
  public static native void nativeCallbackFuncForReceiveRegistRemoteNotification(String paramString);
  
  public static native void nativeCallbackFuncForReceiveRegistRemoteNotificationError(String paramString);
  
  public static native void nativeCallbackFuncForReceiveRemoteNotification(String paramString);
  
  public static native void nativeCallbackFuncForReceiveRemoteNotificationError(String paramString);
  
  public static void registLocalNotification(long paramLong, String paramString1, String paramString2, String paramString3)
  {
    registLocalNotification(paramLong, paramString1, paramString2, paramString3, getInstance().getResChannelId(), 0L);
  }
  
  public static void registLocalNotification(long paramLong1, String paramString1, String paramString2, String paramString3, String paramString4, long paramLong2)
  {
    getInstance()._registLocalNotification(paramLong1, paramString1, paramString2, paramString3, paramString4, paramLong2);
  }
  
  public static void registRemoteNotification()
  {
    getInstance()._registRemoteNotification();
  }
  
  public static void setAutoInitEnabled(boolean paramBoolean)
  {
    getInstance()._setAutoInitEnabled(paramBoolean);
  }
  
  public void _cancelLocalNotification(final String paramString)
  {
    if (!BQJNIHelper.isInitialized()) {
      return;
    }
    BQJNIHelper.getInstance();
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQJNIHelper.getInstance();
        Activity localActivity = BQJNIHelper.getActivity();
        String str = "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-" + paramString;
        if (BQNotificationManagerHelper.this.isLessThanAndroidO())
        {
          BQNotificationIntentService.cancel(localActivity, str);
          return;
        }
        BQNotificationJobService.cancel(localActivity, str);
      }
    });
  }
  
  public String getResChannelId()
  {
    return getResString("notification_channel_id");
  }
  
  public boolean handleNotification(Intent paramIntent)
  {
    String str1 = paramIntent.getType();
    String str2 = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY");
    String str3 = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE");
    int i = paramIntent.getIntExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID", -1);
    if (str2 != null) {
      paramIntent.removeExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY");
    }
    if (str3 != null) {
      paramIntent.removeExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE");
    }
    if (i != -1) {
      paramIntent.removeExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID");
    }
    if ((str1 != null) && (str1.startsWith("application/vnd.jp.co.drecom.bisque.lib.Notification.remote-notification-key"))) {}
    do
    {
      return true;
      if ((str1 == null) || (!str1.startsWith("application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-"))) {
        break;
      }
      BQJNIHelper.getInstance();
      ((NotificationManager)BQJNIHelper.getActivity().getSystemService("notification")).cancel(str2, i);
    } while ((str2 == null) || (str3 == null));
    receiveLocalNotification(str2, str3);
    return true;
    return false;
  }
  
  public void receiveLocalNotification(final String paramString1, final String paramString2)
  {
    if (!BQJNIHelper.isInitialized()) {
      return;
    }
    BQJNIHelper.getInstance();
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQNotificationManagerHelper.nativeCallbackFuncForReceiveLocalNotification(paramString1, paramString2);
      }
    });
  }
  
  public void receiveRegisterForRemoteNotification(final String paramString)
  {
    this.mToken = paramString;
    if (!BQJNIHelper.isInitialized()) {
      return;
    }
    BQJNIHelper.getInstance();
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQNotificationManagerHelper.nativeCallbackFuncForReceiveRegistRemoteNotification(paramString);
      }
    });
  }
  
  public void receiveRegisterForRemoteNotificationError(final String paramString)
  {
    if (!BQJNIHelper.isInitialized()) {
      return;
    }
    BQJNIHelper.getInstance();
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQNotificationManagerHelper.nativeCallbackFuncForReceiveRegistRemoteNotificationError(paramString);
      }
    });
  }
  
  public void receiveRemoteNotification(final String paramString)
  {
    if (!BQJNIHelper.isInitialized()) {
      return;
    }
    BQJNIHelper.getInstance();
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQNotificationManagerHelper.nativeCallbackFuncForReceiveRemoteNotification(paramString);
      }
    });
  }
  
  public void receiveRemoteNotificationError(final String paramString)
  {
    if (!BQJNIHelper.isInitialized()) {
      return;
    }
    BQJNIHelper.getInstance();
    BQJNIHelper.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        BQNotificationManagerHelper.nativeCallbackFuncForReceiveRemoteNotificationError(paramString);
      }
    });
  }
  
  public class RegistParams
  {
    public String activityName;
    public String channelId;
    public String iconColor;
    public int iconId;
    public String key;
    public String message;
    public int notifyId;
    public String title;
    public String type;
    
    public RegistParams(String paramString1, int paramInt1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt2, String paramString6, String paramString7)
    {
      this.type = paramString1;
      this.iconId = paramInt1;
      this.iconColor = paramString2;
      this.key = paramString3;
      this.title = paramString4;
      this.message = paramString5;
      this.notifyId = paramInt2;
      this.activityName = paramString6;
      this.channelId = paramString7;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\Notification\BQNotificationManagerHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */