package jp.co.drecom.bisque.lib.Notification;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat.Builder;
import jp.co.drecom.bisque.lib.BQJNIHelper;

public class BQNotificationIntentService
  extends IntentService
{
  private static final String TAG = "BQNotificationIntentService";
  
  public BQNotificationIntentService()
  {
    super("BQNotificationIntentService");
  }
  
  public BQNotificationIntentService(String paramString)
  {
    super(paramString);
  }
  
  public static void cancel(Context paramContext, String paramString)
  {
    paramString = PendingIntent.getService(paramContext, -1, new Intent(paramContext, BQNotificationIntentService.class).setType(paramString), 134217728);
    ((AlarmManager)paramContext.getSystemService("alarm")).cancel(paramString);
  }
  
  public static void schedule(Context paramContext, BQNotificationManagerHelper.RegistParams paramRegistParams, long paramLong)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TYPE", paramRegistParams.type);
    localBundle.putInt("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID", paramRegistParams.iconId);
    if (paramRegistParams.iconColor != null) {
      localBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR", paramRegistParams.iconColor);
    }
    if (paramRegistParams.key != null) {
      localBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY", paramRegistParams.key);
    }
    localBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE", paramRegistParams.title);
    localBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE", paramRegistParams.message);
    localBundle.putInt("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID", paramRegistParams.notifyId);
    localBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME", paramRegistParams.activityName);
    if (paramRegistParams.channelId != null) {
      localBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID", paramRegistParams.channelId);
    }
    paramRegistParams = PendingIntent.getService(paramContext, -1, new Intent(paramContext, BQNotificationIntentService.class).setType(paramRegistParams.type).putExtras(localBundle), 134217728);
    ((AlarmManager)paramContext.getSystemService("alarm")).set(1, paramLong, paramRegistParams);
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    String str2 = paramIntent.getType();
    int i = paramIntent.getIntExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID", -1);
    String str3 = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR");
    String str1 = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY");
    String str5 = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE");
    String str4 = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE");
    int j = paramIntent.getIntExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID", -1);
    Object localObject2 = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME");
    Object localObject1 = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID");
    if ((str2 == null) || (i == -1) || (str5 == null) || (str4 == null) || (j == -1))
    {
      label95:
      return;
    }
    else
    {
      paramIntent = (Intent)localObject1;
      if (localObject1 == null) {
        paramIntent = "";
      }
      localObject1 = null;
      if (!BQJNIHelper.isInitialized()) {
        break label300;
      }
      BQJNIHelper.getInstance();
      localObject1 = BQJNIHelper.getActivity().getClass();
    }
    for (;;)
    {
      if (localObject1 == null) {
        break label95;
      }
      localObject2 = paramIntent;
      if (paramIntent.isEmpty())
      {
        localObject2 = paramIntent;
        if (BQJNIHelper.isInitialized()) {
          localObject2 = BQNotificationManagerHelper.getInstance().getResChannelId();
        }
      }
      paramIntent = new NotificationCompat.Builder(this, (String)localObject2).setSmallIcon(i).setContentTitle(str5).setContentText(str4).setAutoCancel(true);
      if ((str3 != null) && (!str3.isEmpty())) {
        paramIntent.setColor(Color.parseColor(str3));
      }
      i = 0;
      if (str2.startsWith("application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-")) {
        i = -1;
      }
      paramIntent.setContentIntent(PendingIntent.getActivity(this, i, new Intent(this, (Class)localObject1).setType(str2).putExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY", str1).putExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE", str4).putExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID", j), 134217728));
      localObject1 = (NotificationManager)getSystemService("notification");
      if (str1 == null)
      {
        ((NotificationManager)localObject1).notify(j, paramIntent.build());
        return;
        label300:
        if (localObject2 == null) {
          break;
        }
        try
        {
          localObject1 = Class.forName((String)localObject2);
        }
        catch (ClassNotFoundException paramIntent)
        {
          paramIntent.printStackTrace();
          throw new IllegalStateException(String.format("Failed get startup activity class. class name=%s", new Object[] { localObject2 }));
        }
      }
    }
    ((NotificationManager)localObject1).notify(str1, j, paramIntent.build());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\Notification\BQNotificationIntentService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */