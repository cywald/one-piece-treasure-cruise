package jp.co.drecom.bisque.lib.Notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo.Builder;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.PersistableBundle;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import jp.co.drecom.bisque.lib.BQJNIHelper;

public class BQNotificationJobService
  extends JobService
{
  private static final long OVERRIDE_DEADLINE = 30000L;
  private static final String TAG = "BQNotificationJobService";
  
  public static void cancel(Context paramContext, String paramString)
  {
    ((JobScheduler)paramContext.getSystemService("jobscheduler")).cancel(paramString.hashCode());
  }
  
  private void registRemoteNotification(JobParameters paramJobParameters)
  {
    paramJobParameters = paramJobParameters.getExtras();
    String str2 = paramJobParameters.getString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TYPE");
    int i = paramJobParameters.getInt("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID", -1);
    String str3 = paramJobParameters.getString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR");
    String str1 = paramJobParameters.getString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY");
    String str5 = paramJobParameters.getString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE");
    String str4 = paramJobParameters.getString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE");
    int j = paramJobParameters.getInt("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID", -1);
    Object localObject2 = paramJobParameters.getString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME");
    Object localObject1 = paramJobParameters.getString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID");
    if ((str2 == null) || (i == -1) || (str5 == null) || (str4 == null) || (j == -1))
    {
      Log.w("BQNotificationJobService", "registRemoteNotification() Invalid Extras.");
      return;
    }
    paramJobParameters = (JobParameters)localObject1;
    if (localObject1 == null) {
      paramJobParameters = "";
    }
    localObject1 = null;
    if (BQJNIHelper.isInitialized())
    {
      BQJNIHelper.getInstance();
      localObject1 = BQJNIHelper.getActivity().getClass();
    }
    while (localObject1 == null)
    {
      Log.w("BQNotificationJobService", "registRemoteNotification() Could not get Activity.");
      return;
      if (localObject2 != null) {
        try
        {
          localObject1 = Class.forName((String)localObject2);
        }
        catch (ClassNotFoundException paramJobParameters)
        {
          paramJobParameters.printStackTrace();
          throw new IllegalStateException(String.format("Failed get startup activity class. class name=%s", new Object[] { localObject2 }));
        }
      }
    }
    localObject2 = paramJobParameters;
    if (paramJobParameters.isEmpty())
    {
      localObject2 = paramJobParameters;
      if (BQJNIHelper.isInitialized()) {
        localObject2 = BQNotificationManagerHelper.getInstance().getResChannelId();
      }
    }
    paramJobParameters = new NotificationCompat.Builder(this, (String)localObject2).setSmallIcon(i).setContentTitle(str5).setContentText(str4).setAutoCancel(true);
    if ((str3 != null) && (!str3.isEmpty())) {
      paramJobParameters.setColor(Color.parseColor(str3));
    }
    i = 0;
    if (str2.startsWith("application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-")) {
      i = -1;
    }
    paramJobParameters.setContentIntent(PendingIntent.getActivity(this, i, new Intent(this, (Class)localObject1).setType(str2).putExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY", str1).putExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE", str4).putExtra("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID", j), 134217728));
    localObject1 = (NotificationManager)getSystemService("notification");
    if (str1 == null)
    {
      ((NotificationManager)localObject1).notify(j, paramJobParameters.build());
      return;
    }
    ((NotificationManager)localObject1).notify(str1, j, paramJobParameters.build());
  }
  
  public static void schedule(Context paramContext, BQNotificationManagerHelper.RegistParams paramRegistParams, long paramLong1, long paramLong2)
  {
    PersistableBundle localPersistableBundle = new PersistableBundle();
    localPersistableBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TYPE", paramRegistParams.type);
    localPersistableBundle.putInt("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID", paramRegistParams.iconId);
    if (paramRegistParams.iconColor != null) {
      localPersistableBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR", paramRegistParams.iconColor);
    }
    if (paramRegistParams.key != null) {
      localPersistableBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY", paramRegistParams.key);
    }
    localPersistableBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE", paramRegistParams.title);
    localPersistableBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE", paramRegistParams.message);
    localPersistableBundle.putInt("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID", paramRegistParams.notifyId);
    localPersistableBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME", paramRegistParams.activityName);
    if (paramRegistParams.channelId != null) {
      localPersistableBundle.putString("jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID", paramRegistParams.channelId);
    }
    long l = paramLong1;
    if (paramLong1 <= 0L) {
      l = 0L;
    }
    paramLong1 = paramLong2;
    if (paramLong2 <= 0L) {
      paramLong1 = 30000L;
    }
    ComponentName localComponentName = new ComponentName(paramContext, BQNotificationJobService.class);
    paramRegistParams = new JobInfo.Builder(paramRegistParams.type.hashCode(), localComponentName).setExtras(localPersistableBundle).setMinimumLatency(l).setOverrideDeadline(l + paramLong1).build();
    ((JobScheduler)paramContext.getSystemService("jobscheduler")).schedule(paramRegistParams);
  }
  
  public boolean onStartJob(JobParameters paramJobParameters)
  {
    new Thread(new AlarmRunnable(paramJobParameters)).start();
    return true;
  }
  
  public boolean onStopJob(JobParameters paramJobParameters)
  {
    jobFinished(paramJobParameters, false);
    return false;
  }
  
  private class AlarmRunnable
    implements Runnable
  {
    private static final String TAG = "BQNotificationJobService.AlarmRunnable";
    private JobParameters mParams;
    
    public AlarmRunnable(JobParameters paramJobParameters)
    {
      this.mParams = paramJobParameters;
    }
    
    public void run()
    {
      BQNotificationJobService.this.registRemoteNotification(this.mParams);
      BQNotificationJobService.this.jobFinished(this.mParams, false);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\Notification\BQNotificationJobService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */