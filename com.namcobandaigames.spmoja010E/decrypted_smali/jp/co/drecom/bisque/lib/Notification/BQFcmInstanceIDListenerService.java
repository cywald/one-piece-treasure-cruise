package jp.co.drecom.bisque.lib.Notification;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class BQFcmInstanceIDListenerService
  extends FirebaseInstanceIdService
{
  public void onTokenRefresh()
  {
    String str = FirebaseInstanceId.getInstance().getToken();
    BQNotificationManagerHelper.getInstance().receiveRegisterForRemoteNotification(str);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\Notification\BQFcmInstanceIDListenerService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */