package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

public class DRBackgroundTaskHelper
{
  private static DRBackgroundTaskHelper mInstance = null;
  private IntentFilter mBackgroundTaskIntentFilter;
  private DRBackgroundTaskReceiver mBackgroundTaskReceiver;
  private boolean mInitialized = false;
  
  public static void finalization()
  {
    if (mInstance != null) {
      mInstance = null;
    }
  }
  
  public static void finish(String paramString)
  {
    nativeCallbackFuncForFinish(paramString);
  }
  
  private static Activity getActivity()
  {
    return HandlerJava.getInstance().getActivity();
  }
  
  public static DRBackgroundTaskHelper getInstance()
  {
    if (mInstance == null) {
      mInstance = new DRBackgroundTaskHelper();
    }
    return mInstance;
  }
  
  private boolean initInternal()
  {
    if (this.mInitialized == true) {
      return true;
    }
    this.mBackgroundTaskIntentFilter = new IntentFilter("jp.co.drecom.bisque.lib.drbackgroundtask.broadcast.BGTASK");
    this.mBackgroundTaskReceiver = new DRBackgroundTaskReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(this.mBackgroundTaskReceiver, this.mBackgroundTaskIntentFilter);
    this.mInitialized = true;
    return true;
  }
  
  public static boolean initialize()
  {
    if (!BQJNIHelper.isInitialized()) {
      return false;
    }
    return getInstance().initInternal();
  }
  
  private static native void nativeCallbackFuncForFinish(String paramString);
  
  private static native void nativeCallbackFuncForTask(String paramString);
  
  public static boolean start(String paramString)
  {
    return DRBackgroundTaskIntentService.startAction(getActivity(), paramString);
  }
  
  public static void task(String paramString)
  {
    nativeCallbackFuncForTask(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\DRBackgroundTaskHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */