package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;

public class BQUpdate
{
  private static final int BQUPDATE_CHECKUPDATE_ERROR = 2;
  private static final int BQUPDATE_CHECKUPDATE_NO = 1;
  private static final int BQUPDATE_CHECKUPDATE_YES = 0;
  private int latestVersionCode = 0;
  private String packageName = "";
  private Activity parent;
  private int versionCode = 0;
  private String versionName = "";
  
  public BQUpdate(Context paramContext)
  {
    this.parent = ((Activity)paramContext);
  }
  
  public static native void nativeCallbackFuncForCheckUpdate(int paramInt);
  
  public void checkUpdate(boolean paramBoolean, int paramInt, final String paramString)
  {
    this.latestVersionCode = paramInt;
    Object localObject = this.parent.getPackageManager();
    try
    {
      localObject = ((PackageManager)localObject).getPackageInfo(this.parent.getPackageName(), 1);
      this.packageName = ((PackageInfo)localObject).packageName;
      this.versionCode = ((PackageInfo)localObject).versionCode;
      this.versionName = ((PackageInfo)localObject).versionName;
      if ((paramBoolean != true) || ((paramBoolean) || (this.latestVersionCode == -1))) {
        this.latestVersionCode = 10;
      }
      if (this.versionCode < this.latestVersionCode)
      {
        nativeCallbackFuncForCheckUpdate(0);
        if (paramBoolean == true)
        {
          localObject = new AlertDialog.Builder(this.parent);
          ((AlertDialog.Builder)localObject).setTitle(R.string.jp_co_drecom_bqupdate_dialog_title);
          ((AlertDialog.Builder)localObject).setMessage(R.string.jp_co_drecom_bqupdate_dialog_message);
          ((AlertDialog.Builder)localObject).setPositiveButton(R.string.jp_co_drecom_common_label_ok, new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              BQUpdate.this.execUpdate(paramString);
            }
          });
          ((AlertDialog.Builder)localObject).setCancelable(false);
          ((AlertDialog.Builder)localObject).create().show();
        }
        return;
      }
    }
    catch (PackageManager.NameNotFoundException paramString)
    {
      paramString.printStackTrace();
      nativeCallbackFuncForCheckUpdate(2);
      return;
    }
    nativeCallbackFuncForCheckUpdate(1);
  }
  
  public void execReview(String paramString)
  {
    paramString = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
    this.parent.startActivity(paramString);
  }
  
  public void execUpdate(String paramString)
  {
    paramString = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
    this.parent.startActivity(paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQUpdate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */