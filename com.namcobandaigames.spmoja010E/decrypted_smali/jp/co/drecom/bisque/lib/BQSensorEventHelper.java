package jp.co.drecom.bisque.lib;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class BQSensorEventHelper
  implements SensorEventListener
{
  static BQSensorEventHelper m_kInstance = null;
  private SensorManager m_kManager = null;
  
  private void initInternal()
  {
    this.m_kManager = ((SensorManager)BQJNIHelper.getSystemServiceWXP("sensor"));
  }
  
  public static void initialize()
  {
    if (m_kInstance == null)
    {
      m_kInstance = new BQSensorEventHelper();
      m_kInstance.initInternal();
    }
  }
  
  public static BQSensorEventHelper self()
  {
    return m_kInstance;
  }
  
  public void onAccuracyChanged(Sensor paramSensor, int paramInt) {}
  
  public void onSensorChanged(SensorEvent paramSensorEvent) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQSensorEventHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */