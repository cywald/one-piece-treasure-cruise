package jp.co.drecom.bisque.lib;

import android.os.FileObserver;
import java.util.HashMap;

public class DRFileSystemNotifyHelper
{
  private static HashMap<String, DRFileSystemNotify> mInstMap;
  private static DRFileSystemNotifyHelper mInstance = null;
  private boolean mInitialized = false;
  
  public static void finalization()
  {
    if (mInstance != null)
    {
      mInstMap.clear();
      mInstance = null;
    }
  }
  
  public static DRFileSystemNotifyHelper getInstance()
  {
    if (mInstance == null) {
      mInstance = new DRFileSystemNotifyHelper();
    }
    return mInstance;
  }
  
  private boolean initInternal()
  {
    if (this.mInitialized == true) {
      return true;
    }
    mInstMap = new HashMap();
    this.mInitialized = true;
    return true;
  }
  
  public static boolean initialize()
  {
    if (!BQJNIHelper.isInitialized()) {
      return false;
    }
    return getInstance().initInternal();
  }
  
  private static native void nativeCallbackFuncForReceiveNotify(String paramString1, String paramString2);
  
  public static boolean setPath(String paramString)
  {
    if (mInstMap.containsKey(paramString)) {
      return false;
    }
    return getInstance().setPathInternal(paramString);
  }
  
  private boolean setPathInternal(String paramString)
  {
    DRFileSystemNotify localDRFileSystemNotify = new DRFileSystemNotify(paramString);
    localDRFileSystemNotify.startWatching();
    mInstMap.put(paramString, localDRFileSystemNotify);
    return true;
  }
  
  public static void unsetPath(String paramString)
  {
    paramString = (DRFileSystemNotify)mInstMap.remove(paramString);
    if (paramString != null) {
      paramString.stopWatching();
    }
  }
  
  private static class DRFileSystemNotify
    extends FileObserver
  {
    private static final String NOTIFY_UPDATE = "NOTIFY_UPDATE";
    private String monitoredPath;
    
    public DRFileSystemNotify(String paramString)
    {
      super();
      this.monitoredPath = paramString;
    }
    
    public DRFileSystemNotify(String paramString, int paramInt)
    {
      super(paramInt);
      this.monitoredPath = paramString;
    }
    
    public void onEvent(int paramInt, String paramString)
    {
      if ((paramInt == 256) || (paramInt == 512)) {
        DRFileSystemNotifyHelper.nativeCallbackFuncForReceiveNotify("NOTIFY_UPDATE", this.monitoredPath);
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\DRFileSystemNotifyHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */