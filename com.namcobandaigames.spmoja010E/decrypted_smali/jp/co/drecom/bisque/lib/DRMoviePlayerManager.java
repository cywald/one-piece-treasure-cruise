package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.os.Handler;
import android.widget.RelativeLayout;

public class DRMoviePlayerManager
{
  private static DRMoviePlayerManager m_instance = null;
  private Handler m_handler = null;
  private DRMoviePlayer m_player = null;
  
  public static void initialize(Activity paramActivity, Handler paramHandler, RelativeLayout paramRelativeLayout)
  {
    m_instance = new DRMoviePlayerManager();
    m_instance.m_handler = paramHandler;
    m_instance.m_player = new DRMoviePlayer(paramActivity, paramRelativeLayout);
  }
  
  public static boolean isPaused()
  {
    if (m_instance == null) {
      return false;
    }
    return m_instance.m_player.isPaused();
  }
  
  public static boolean isPlaying()
  {
    if (m_instance == null) {
      return false;
    }
    return m_instance.m_player.isPlaying();
  }
  
  public static void onResume()
  {
    if (m_instance == null) {
      return;
    }
    m_instance.m_handler.post(new Runnable()
    {
      public void run()
      {
        DRMoviePlayerManager.m_instance.m_player.onResume();
      }
    });
  }
  
  public static void onSuspend()
  {
    if (m_instance == null) {
      return;
    }
    m_instance.m_handler.post(new Runnable()
    {
      public void run()
      {
        DRMoviePlayerManager.m_instance.m_player.onSuspend();
      }
    });
  }
  
  public static boolean pause()
  {
    if (m_instance == null) {
      return false;
    }
    m_instance.m_handler.post(new Runnable()
    {
      public void run()
      {
        DRMoviePlayerManager.m_instance.m_player.pause();
      }
    });
    return true;
  }
  
  public static boolean play()
  {
    if (m_instance == null) {
      return false;
    }
    m_instance.m_handler.post(new Runnable()
    {
      public void run()
      {
        DRMoviePlayerManager.m_instance.m_player.play();
      }
    });
    return true;
  }
  
  public static boolean setFile(String paramString)
  {
    if (m_instance == null) {
      return false;
    }
    return m_instance.m_player.setFile(paramString);
  }
  
  public static boolean setRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    if (m_instance == null) {
      return false;
    }
    return m_instance.m_player.setRect((int)paramFloat1, (int)paramFloat2, (int)paramFloat3, (int)paramFloat4);
  }
  
  public static boolean setUri(String paramString)
  {
    if (m_instance == null) {
      return false;
    }
    return m_instance.m_player.setUri(paramString);
  }
  
  public static boolean stop()
  {
    if (m_instance == null) {
      return false;
    }
    m_instance.m_handler.post(new Runnable()
    {
      public void run()
      {
        DRMoviePlayerManager.m_instance.m_player.stop();
      }
    });
    return true;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\DRMoviePlayerManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */