package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Debug;
import android.os.Debug.MemoryInfo;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import java.io.File;
import java.util.Currency;
import java.util.Locale;

public class BQAppPlatformManager
{
  private static final long MOCKLOCATIONSETTING_OFF = 0L;
  private static final long MOCKLOCATIONSETTING_ON = 1L;
  private static final long MOCKLOCATIONSETTING_UNKNOWN = -1L;
  private static ActivityManager m_activityManager = null;
  private static Activity m_context = null;
  
  public static String getAndroidID()
  {
    String str = Settings.Secure.getString(m_context.getContentResolver(), "android_id");
    if (str != null) {
      return str;
    }
    return "";
  }
  
  public static long getAvailMem()
  {
    if (m_activityManager == null) {
      return -1L;
    }
    ActivityManager.MemoryInfo localMemoryInfo = new ActivityManager.MemoryInfo();
    m_activityManager.getMemoryInfo(localMemoryInfo);
    return localMemoryInfo.availMem;
  }
  
  public static float getBatteryLevel()
  {
    if (m_context == null) {
      return -1.0F;
    }
    try
    {
      Intent localIntent = m_context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
      int i = localIntent.getIntExtra("level", 0);
      int j = localIntent.getIntExtra("scale", 100);
      return i / j;
    }
    catch (Exception localException) {}
    return -2.0F;
  }
  
  public static String getCountryCode()
  {
    return Locale.getDefault().getCountry();
  }
  
  public static String getCurrencyCode()
  {
    Object localObject1 = Locale.getDefault();
    try
    {
      localObject1 = Currency.getInstance((Locale)localObject1);
      if (localObject1 == null) {
        return null;
      }
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Object localObject2;
      for (;;)
      {
        localObject2 = null;
      }
      return ((Currency)localObject2).getCurrencyCode();
    }
  }
  
  public static long getDalvikHeapAllocatedSize()
  {
    return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
  }
  
  public static long getDalvikHeapFreeSize()
  {
    return Runtime.getRuntime().freeMemory();
  }
  
  public static long getDalvikHeapMaxSize()
  {
    return Runtime.getRuntime().maxMemory();
  }
  
  public static long getDalvikHeapTotalSize()
  {
    return Runtime.getRuntime().totalMemory();
  }
  
  public static long getDalvikPss()
  {
    if (m_activityManager == null) {}
    Debug.MemoryInfo[] arrayOfMemoryInfo;
    do
    {
      return -1L;
      int i = Process.myPid();
      arrayOfMemoryInfo = m_activityManager.getProcessMemoryInfo(new int[] { i });
    } while (arrayOfMemoryInfo.length == 0);
    return arrayOfMemoryInfo[0].dalvikPss * 1024L;
  }
  
  public static long getDiskFreeSpaceExternal()
  {
    return 0L;
  }
  
  public static long getDiskFreeSpaceInternal()
  {
    StatFs localStatFs = new StatFs(Environment.getDataDirectory().getPath());
    return localStatFs.getAvailableBlocks() * localStatFs.getBlockSize();
  }
  
  public static long getMockLocation()
  {
    if (m_context == null) {
      return -1L;
    }
    try
    {
      int i = Settings.Secure.getInt(m_context.getContentResolver(), "mock_location");
      if (i == 1) {
        return 1L;
      }
      return 0L;
    }
    catch (Settings.SettingNotFoundException localSettingNotFoundException) {}
    return -1L;
  }
  
  public static String getModelName()
  {
    return Build.MODEL;
  }
  
  public static long getNativeHeapAllocatedSize()
  {
    return Debug.getNativeHeapAllocatedSize();
  }
  
  public static long getNativeHeapFreeSize()
  {
    return Debug.getNativeHeapFreeSize();
  }
  
  public static long getNativeHeapSize()
  {
    return Debug.getNativeHeapSize();
  }
  
  public static long getNativePss()
  {
    if (m_activityManager == null) {}
    Debug.MemoryInfo[] arrayOfMemoryInfo;
    do
    {
      return -1L;
      int i = Process.myPid();
      arrayOfMemoryInfo = m_activityManager.getProcessMemoryInfo(new int[] { i });
    } while (arrayOfMemoryInfo.length == 0);
    return arrayOfMemoryInfo[0].nativePss * 1024L;
  }
  
  public static String getOsVersion()
  {
    return Build.VERSION.RELEASE;
  }
  
  public static int getOsVersionCode()
  {
    return Build.VERSION.SDK_INT;
  }
  
  public static long getOtherPss()
  {
    if (m_activityManager == null) {}
    Debug.MemoryInfo[] arrayOfMemoryInfo;
    do
    {
      return -1L;
      int i = Process.myPid();
      arrayOfMemoryInfo = m_activityManager.getProcessMemoryInfo(new int[] { i });
    } while (arrayOfMemoryInfo.length == 0);
    return arrayOfMemoryInfo[0].otherPss * 1024L;
  }
  
  public static long getThreshold()
  {
    if (m_activityManager == null) {
      return -1L;
    }
    ActivityManager.MemoryInfo localMemoryInfo = new ActivityManager.MemoryInfo();
    m_activityManager.getMemoryInfo(localMemoryInfo);
    return localMemoryInfo.threshold;
  }
  
  public static long getTotalPss()
  {
    if (m_activityManager == null) {}
    Debug.MemoryInfo[] arrayOfMemoryInfo;
    do
    {
      return -1L;
      int i = Process.myPid();
      arrayOfMemoryInfo = m_activityManager.getProcessMemoryInfo(new int[] { i });
    } while (arrayOfMemoryInfo.length == 0);
    return 1024L * arrayOfMemoryInfo[0].getTotalPss();
  }
  
  public static String getTrustStorePath()
  {
    return "";
  }
  
  public static String getVersionCode()
  {
    if (m_context == null) {}
    for (;;)
    {
      return null;
      try
      {
        PackageInfo localPackageInfo = m_context.getPackageManager().getPackageInfo(m_context.getPackageName(), 128);
        if (localPackageInfo == null) {
          continue;
        }
        return Integer.toString(localPackageInfo.versionCode);
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        for (;;)
        {
          Object localObject = null;
        }
      }
    }
  }
  
  public static String getVersionName()
  {
    if (m_context == null) {}
    for (;;)
    {
      return null;
      try
      {
        PackageInfo localPackageInfo = m_context.getPackageManager().getPackageInfo(m_context.getPackageName(), 128);
        if (localPackageInfo == null) {
          continue;
        }
        return localPackageInfo.versionName;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        for (;;)
        {
          Object localObject = null;
        }
      }
    }
  }
  
  public static boolean isEmulator()
  {
    return (Build.MODEL.equals("sdk")) || (Build.MODEL.equals("sdk_phone_armv7"));
  }
  
  public static boolean isExternalPowerSupplyConnected()
  {
    if (m_context == null) {
      return false;
    }
    for (;;)
    {
      try
      {
        int i = m_context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED")).getIntExtra("plugged", 0);
        if (i == 0) {
          continue;
        }
        bool = true;
      }
      catch (Exception localException)
      {
        boolean bool = false;
        continue;
      }
      return bool;
      bool = false;
    }
  }
  
  public static boolean isLowMemory()
  {
    if (m_activityManager == null) {
      return false;
    }
    ActivityManager.MemoryInfo localMemoryInfo = new ActivityManager.MemoryInfo();
    m_activityManager.getMemoryInfo(localMemoryInfo);
    return localMemoryInfo.lowMemory;
  }
  
  public static void setContext(Activity paramActivity)
  {
    m_context = paramActivity;
    m_activityManager = (ActivityManager)paramActivity.getSystemService("activity");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQAppPlatformManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */