package jp.co.drecom.bisque.lib;

import android.os.Handler;

public class BQTouchDispatcherNotify
{
  private static BQTouchDispatcher dispatcher;
  private static Handler handler;
  
  public static void setDispatcher(BQTouchDispatcher paramBQTouchDispatcher, Handler paramHandler)
  {
    dispatcher = paramBQTouchDispatcher;
    handler = paramHandler;
  }
  
  public static void setUserInteractionEnabled(boolean paramBoolean, final int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQTouchDispatcherNotify.dispatcher.setUserInteractionEnabled(this.val$_b, paramInt);
      }
    });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQTouchDispatcherNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */