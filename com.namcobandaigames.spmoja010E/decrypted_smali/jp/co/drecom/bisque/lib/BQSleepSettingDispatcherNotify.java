package jp.co.drecom.bisque.lib;

import android.os.Handler;

public class BQSleepSettingDispatcherNotify
{
  private static BQSleepSettingDispatcher dispatcher;
  private static Handler handler;
  
  public static void setDeviceSleep(boolean paramBoolean)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQSleepSettingDispatcherNotify.dispatcher.setDeviceSleep(this.val$_set);
      }
    });
  }
  
  public static void setDispatcher(BQSleepSettingDispatcher paramBQSleepSettingDispatcher, Handler paramHandler)
  {
    dispatcher = paramBQSleepSettingDispatcher;
    handler = paramHandler;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQSleepSettingDispatcherNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */