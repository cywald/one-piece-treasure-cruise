package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Handler;

public class HandlerJava
{
  private static HandlerJava m_instance = null;
  private Activity m_activity = null;
  private Handler m_handler = null;
  private GLSurfaceView m_surfaceview = null;
  
  public static HandlerJava getInstance()
  {
    return m_instance;
  }
  
  public static void initialize(Activity paramActivity, Handler paramHandler, GLSurfaceView paramGLSurfaceView)
  {
    m_instance = new HandlerJava();
    m_instance.m_handler = paramHandler;
    m_instance.m_activity = paramActivity;
    m_instance.m_surfaceview = paramGLSurfaceView;
  }
  
  public boolean enableRunOnGLThread()
  {
    return this.m_surfaceview != null;
  }
  
  public boolean enableRunOnUIThread()
  {
    return this.m_handler != null;
  }
  
  public Activity getActivity()
  {
    return this.m_activity;
  }
  
  public void runOnGLThread(Runnable paramRunnable)
  {
    this.m_surfaceview.queueEvent(paramRunnable);
  }
  
  public void runOnUIThread(Runnable paramRunnable)
  {
    this.m_handler.post(paramRunnable);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\HandlerJava.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */