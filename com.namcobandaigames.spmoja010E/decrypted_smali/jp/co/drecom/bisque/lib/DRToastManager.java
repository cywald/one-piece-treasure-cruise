package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.os.Handler;

public class DRToastManager
{
  private static DRToastManager m_instance = null;
  private Handler m_handler = null;
  private DRToast m_toast = null;
  
  public static void initialize(Activity paramActivity, Handler paramHandler)
  {
    m_instance = new DRToastManager();
    m_instance.m_handler = paramHandler;
    m_instance.m_toast = new DRToast(paramActivity);
  }
  
  public static void showMessage(String paramString, int paramInt1, int paramInt2)
  {
    if (m_instance == null) {
      return;
    }
    final DRToast.ANCHOR_POS localANCHOR_POS = DRToast.ANCHOR_POS.toEnum(paramInt1);
    final DRToast.SHOW_TIME localSHOW_TIME = DRToast.SHOW_TIME.toEnum(paramInt2);
    m_instance.m_handler.post(new Runnable()
    {
      public void run()
      {
        DRToastManager.m_instance.m_toast.showMessage(this.val$messageValue, localANCHOR_POS, localSHOW_TIME);
      }
    });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\DRToastManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */