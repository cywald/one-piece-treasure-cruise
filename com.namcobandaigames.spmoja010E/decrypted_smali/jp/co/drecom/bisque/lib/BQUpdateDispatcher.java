package jp.co.drecom.bisque.lib;

public abstract interface BQUpdateDispatcher
{
  public abstract void checkUpdate(boolean paramBoolean, int paramInt, String paramString);
  
  public abstract void execReview(String paramString);
  
  public abstract void execUpdate(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQUpdateDispatcher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */