package jp.co.drecom.bisque.lib;

import android.graphics.Typeface;

public class BQTypefaces
{
  private static Implement m_implement = null;
  
  public static Typeface get(String paramString)
  {
    if (m_implement != null) {
      return m_implement.getTypeface(paramString);
    }
    return null;
  }
  
  public static void registImplement(Implement paramImplement)
  {
    m_implement = paramImplement;
  }
  
  public static abstract interface Implement
  {
    public abstract Typeface getTypeface(String paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQTypefaces.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */