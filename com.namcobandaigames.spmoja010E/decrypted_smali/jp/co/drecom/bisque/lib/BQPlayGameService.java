package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

public class BQPlayGameService
  implements BQGameHelper.BQGameHelperListener
{
  private static final int REQUEST_ACHIEVEMENTS = 0;
  static final String TAG = "BQPlayGameService";
  static Activity activity;
  static BQGameHelper gameHelper;
  
  public BQPlayGameService(Activity paramActivity)
  {
    activity = paramActivity;
    gameHelper = new BQGameHelper(activity);
    gameHelper.setup(this);
  }
  
  private static native void native_ReportSignInResult(boolean paramBoolean1, boolean paramBoolean2);
  
  private static native void native_ReportSignInState(boolean paramBoolean1, boolean paramBoolean2);
  
  public static void postStartSignIn()
  {
    activity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (!BQPlayGameService.gameHelper.isSignedIn())
        {
          BQPlayGameService.native_ReportSignInState(false, true);
          BQPlayGameService.gameHelper.startSignInIntent();
        }
      }
    });
  }
  
  public static void postStartSignOut()
  {
    activity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (BQPlayGameService.gameHelper.isSignedIn())
        {
          BQPlayGameService.native_ReportSignInState(false, false);
          BQPlayGameService.gameHelper.signOut();
        }
      }
    });
  }
  
  public static void postUnlockAchievement(String paramString)
  {
    activity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (BQPlayGameService.gameHelper.isSignedIn()) {
          BQPlayGameService.gameHelper.unlockAchievement(this.val$achId);
        }
      }
    });
  }
  
  public static void showAchievement()
  {
    activity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (BQPlayGameService.gameHelper.isSignedIn()) {
          BQPlayGameService.gameHelper.showAchievements();
        }
      }
    });
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    gameHelper.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public void onDisconnected()
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQPlayGameService.native_ReportSignInState(false, false);
        BQPlayGameService.native_ReportSignInResult(false, false);
      }
    });
  }
  
  public void onInteractiveSignInFailed(Status paramStatus)
  {
    final boolean bool = false;
    if (paramStatus.getStatusCode() == 12501) {
      bool = true;
    }
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQPlayGameService.native_ReportSignInState(false, false);
        BQPlayGameService.native_ReportSignInResult(false, bool);
      }
    });
  }
  
  public void onInteractiveSignInSucceeded(GoogleSignInAccount paramGoogleSignInAccount)
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQPlayGameService.native_ReportSignInState(true, false);
        BQPlayGameService.native_ReportSignInResult(true, false);
      }
    });
  }
  
  public void onResume()
  {
    gameHelper.onResume();
  }
  
  public void onSilentSignInFailed(Exception paramException)
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQPlayGameService.native_ReportSignInState(false, false);
        BQPlayGameService.native_ReportSignInResult(false, false);
      }
    });
  }
  
  public void onSilentSignInSucceeded(GoogleSignInAccount paramGoogleSignInAccount)
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQPlayGameService.native_ReportSignInState(true, false);
        BQPlayGameService.native_ReportSignInResult(true, false);
      }
    });
  }
  
  public void onStart()
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQPlayGameService.native_ReportSignInState(false, true);
      }
    });
  }
  
  public void onStop()
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQPlayGameService.native_ReportSignInState(false, true);
      }
    });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQPlayGameService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */