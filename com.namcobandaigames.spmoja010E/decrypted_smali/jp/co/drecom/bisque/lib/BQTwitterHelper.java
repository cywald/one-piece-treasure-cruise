package jp.co.drecom.bisque.lib;

import android.app.Activity;
import java.io.File;
import twitter4j.AsyncTwitter;
import twitter4j.AsyncTwitterFactory;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterAdapter;
import twitter4j.TwitterException;
import twitter4j.TwitterMethod;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

public class BQTwitterHelper
{
  private static int http_connection_timeout = 20000;
  private static int http_read_timeout = 120000;
  private static BQTwitterHelper self = null;
  private String mAccessToken = "";
  private String mAccessTokenSecret = "";
  private String mCallbackURL = "";
  private String mConsumerKey = "";
  private String mConsumerSecret = "";
  private boolean mInitialized = false;
  private String mMediaFile = "";
  private String mRequestToken = "";
  private String mRequestTokenSecret = "";
  private AsyncTwitter mTwitter = null;
  private String mVerifier = "";
  private ConfigurationBuilder m_cb = null;
  private boolean m_executing_browser = false;
  private AsyncTwitterFactory m_factory = null;
  
  public static void closeAuthorizeURL()
  {
    getInstance().closeAuthorizeURLInternal();
  }
  
  private void closeAuthorizeURLInternal()
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run() {}
    });
  }
  
  public static void finalization()
  {
    if (self != null)
    {
      self.mTwitter = null;
      self.m_factory = null;
      self = null;
    }
  }
  
  public static BQTwitterHelper getInstance()
  {
    if (self == null) {
      self = new BQTwitterHelper();
    }
    return self;
  }
  
  public static void getOAuthAccessToken(String paramString1, String paramString2, String paramString3)
  {
    getInstance().getOAuthAccessTokenInternal(new String(paramString1), new String(paramString2), new String(paramString3));
  }
  
  private void getOAuthAccessTokenInternal(final String paramString1, final String paramString2, String paramString3)
  {
    paramString2 = new RequestToken(paramString2, paramString3);
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        BQTwitterHelper.this.mTwitter.getOAuthAccessTokenAsync(paramString2, paramString1);
      }
    });
  }
  
  public static void getOAuthRequestToken(String paramString)
  {
    getInstance().getOAuthRequestTokenInternal(new String(paramString));
  }
  
  private void getOAuthRequestTokenInternal(final String paramString)
  {
    this.mCallbackURL = paramString;
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        BQTwitterHelper.this.mTwitter.getOAuthRequestTokenAsync(paramString);
      }
    });
  }
  
  public static String getVerifier()
  {
    return getInstance().getVerifierInternal();
  }
  
  private String getVerifierInternal()
  {
    return this.mVerifier;
  }
  
  public static void gotOAuthVerifier(String paramString)
  {
    getInstance().gotOAuthVerifierInternal(paramString);
  }
  
  private void gotOAuthVerifierInternal(final String paramString)
  {
    HandlerJava.getInstance().runOnGLThread(new Runnable()
    {
      public void run()
      {
        BQTwitterHelper.nativegotOAuthVerifier(paramString);
      }
    });
  }
  
  private boolean initInternal()
  {
    if (this.mInitialized == true) {
      return true;
    }
    this.m_cb = new ConfigurationBuilder();
    this.m_cb.setHttpConnectionTimeout(http_connection_timeout);
    this.m_cb.setHttpReadTimeout(http_read_timeout);
    this.m_factory = new AsyncTwitterFactory(this.m_cb.build());
    this.mTwitter = this.m_factory.getInstance();
    this.mTwitter.addListener(new BQTwitterAdapter(null));
    this.mInitialized = true;
    return true;
  }
  
  public static boolean initialize()
  {
    if (!BQJNIHelper.isInitialized()) {
      return false;
    }
    return getInstance().initInternal();
  }
  
  public static boolean isExecutingBrowser()
  {
    return getInstance().isExecutingBrowserInternal();
  }
  
  private boolean isExecutingBrowserInternal()
  {
    return this.m_executing_browser;
  }
  
  private AccessToken loadAccessToken()
  {
    return new AccessToken(this.mAccessToken, this.mAccessTokenSecret);
  }
  
  public static native void nativecloseAuthorizeURL();
  
  public static native void nativegotOAuthAccessToken(String paramString1, String paramString2);
  
  public static native void nativegotOAuthRequestToken(String paramString1, String paramString2, String paramString3);
  
  public static native void nativegotOAuthVerifier(String paramString);
  
  public static native void nativeonException(String paramString1, int paramInt1, int paramInt2, String paramString2, boolean paramBoolean);
  
  public static native void nativeupdatedStatus();
  
  public static native void nativeupdatedStatusWithMedia();
  
  public static void openAuthorizeURL(String paramString)
  {
    getInstance().openAuthorizeURLInternal(paramString);
  }
  
  private void openAuthorizeURLInternal(final String paramString)
  {
    final Activity localActivity = HandlerJava.getInstance().getActivity();
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        new BQUrlScheme(localActivity).execUrlScheme(paramString);
        BQTwitterHelper.access$202(BQTwitterHelper.this, true);
      }
    });
  }
  
  public static void refreshAuthorization()
  {
    getInstance().refreshAuthorizationInternal();
  }
  
  private void refreshAuthorizationInternal()
  {
    this.mTwitter = this.m_factory.getInstance();
    this.mTwitter.addListener(new BQTwitterAdapter(null));
    if ((!this.mConsumerKey.equals("")) && (!this.mConsumerSecret.equals(""))) {
      this.mTwitter.setOAuthConsumer(this.mConsumerKey, this.mConsumerSecret);
    }
  }
  
  public static void setAccessToken(String paramString1, String paramString2)
  {
    getInstance().setAccessTokenInternal(new String(paramString1), new String(paramString2));
  }
  
  private void setAccessTokenInternal(String paramString1, String paramString2)
  {
    this.mAccessToken = paramString1;
    this.mAccessTokenSecret = paramString2;
    this.mTwitter.setOAuthAccessToken(loadAccessToken());
  }
  
  public static void setConsumerKey(String paramString1, String paramString2)
  {
    getInstance().setConsumerKeyInternal(new String(paramString1), new String(paramString2));
  }
  
  private void setConsumerKeyInternal(String paramString1, String paramString2)
  {
    if ((!this.mConsumerKey.equals(paramString1)) && (!this.mConsumerSecret.equals(paramString2)))
    {
      this.mTwitter.setOAuthConsumer(paramString1, paramString2);
      this.mConsumerKey = paramString1;
      this.mConsumerSecret = paramString2;
    }
  }
  
  public static void setExecutingBrowser(boolean paramBoolean)
  {
    getInstance().setExecutingBrowserInternal(paramBoolean);
  }
  
  private void setExecutingBrowserInternal(boolean paramBoolean)
  {
    this.m_executing_browser = paramBoolean;
  }
  
  public static void setHttpTimeout(int paramInt1, int paramInt2)
  {
    http_connection_timeout = paramInt1;
    http_read_timeout = paramInt2;
  }
  
  public static void setVerifier(String paramString)
  {
    getInstance().setVerifierInternal(new String(paramString));
  }
  
  private void setVerifierInternal(String paramString)
  {
    this.mVerifier = paramString;
  }
  
  public static void updateStatus(String paramString)
  {
    getInstance().updateStatusInternal(false, new String(paramString), "");
  }
  
  private void updateStatusInternal(boolean paramBoolean, final String paramString1, String paramString2)
  {
    if (paramBoolean)
    {
      paramString1 = new StatusUpdate(paramString1);
      this.mMediaFile = paramString2;
      paramString1.media(new File(this.mMediaFile));
      HandlerJava.getInstance().runOnUIThread(new Runnable()
      {
        public void run()
        {
          BQTwitterHelper.this.mTwitter.updateStatus(paramString1);
        }
      });
      return;
    }
    this.mMediaFile = null;
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        BQTwitterHelper.this.mTwitter.updateStatus(paramString1);
      }
    });
  }
  
  public static void updateStatusWithMedia(String paramString1, String paramString2)
  {
    getInstance().updateStatusInternal(true, new String(paramString1), new String(paramString2));
  }
  
  public String getCallbackURL()
  {
    return this.mCallbackURL;
  }
  
  private class BQTwitterAdapter
    extends TwitterAdapter
  {
    private BQTwitterAdapter() {}
    
    public void gotOAuthAccessToken(final AccessToken paramAccessToken)
    {
      final String str = paramAccessToken.getToken();
      paramAccessToken = paramAccessToken.getTokenSecret();
      HandlerJava.getInstance().runOnGLThread(new Runnable()
      {
        public void run()
        {
          BQTwitterHelper.nativegotOAuthAccessToken(str, paramAccessToken);
        }
      });
    }
    
    public void gotOAuthRequestToken(final RequestToken paramRequestToken)
    {
      BQTwitterHelper.access$302(BQTwitterHelper.this, paramRequestToken.getToken());
      BQTwitterHelper.access$402(BQTwitterHelper.this, paramRequestToken.getTokenSecret());
      paramRequestToken = paramRequestToken.getAuthenticationURL();
      HandlerJava.getInstance().runOnGLThread(new Runnable()
      {
        public void run()
        {
          BQTwitterHelper.nativegotOAuthRequestToken(paramRequestToken, BQTwitterHelper.this.mRequestToken, BQTwitterHelper.this.mRequestTokenSecret);
        }
      });
    }
    
    public void onException(TwitterException paramTwitterException, final TwitterMethod paramTwitterMethod)
    {
      if ((paramTwitterMethod == TwitterMethod.UPDATE_STATUS) && (BQTwitterHelper.this.mMediaFile != null)) {}
      for (paramTwitterMethod = "UPDATE_STATUS_WITH_MEDIA";; paramTwitterMethod = paramTwitterMethod.name())
      {
        final int i = paramTwitterException.getStatusCode();
        final int j = paramTwitterException.getErrorCode();
        final String str = paramTwitterException.getMessage();
        final boolean bool = paramTwitterException.isCausedByNetworkIssue();
        HandlerJava.getInstance().runOnGLThread(new Runnable()
        {
          public void run()
          {
            BQTwitterHelper.nativeonException(paramTwitterMethod, i, j, str, bool);
          }
        });
        return;
      }
    }
    
    public void updatedStatus(Status paramStatus)
    {
      if (BQTwitterHelper.this.mMediaFile == null)
      {
        HandlerJava.getInstance().runOnGLThread(new Runnable()
        {
          public void run() {}
        });
        return;
      }
      HandlerJava.getInstance().runOnGLThread(new Runnable()
      {
        public void run() {}
      });
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQTwitterHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */