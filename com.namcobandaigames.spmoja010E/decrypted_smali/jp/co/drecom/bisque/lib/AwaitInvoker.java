package jp.co.drecom.bisque.lib;

import android.os.Handler;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public final class AwaitInvoker
  implements Runnable
{
  private Runnable m_runnable = null;
  private CountDownLatch m_signal = null;
  
  public boolean invokeAndWait(Handler paramHandler, Runnable paramRunnable, int paramInt)
  {
    this.m_runnable = paramRunnable;
    this.m_signal = new CountDownLatch(1);
    paramHandler.post(this);
    try
    {
      boolean bool = this.m_signal.await(paramInt, TimeUnit.MILLISECONDS);
      return bool;
    }
    catch (InterruptedException paramHandler) {}
    return false;
  }
  
  public void run()
  {
    try
    {
      this.m_runnable.run();
      return;
    }
    finally
    {
      this.m_signal.countDown();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\AwaitInvoker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */