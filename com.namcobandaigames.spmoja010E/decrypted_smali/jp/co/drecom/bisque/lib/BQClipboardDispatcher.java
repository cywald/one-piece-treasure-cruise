package jp.co.drecom.bisque.lib;

public abstract interface BQClipboardDispatcher
{
  public abstract String getStringFromClipboard();
  
  public abstract void setStringToClipboard(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQClipboardDispatcher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */