package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.vending.billing.IInAppBillingService;
import com.android.vending.billing.IInAppBillingService.Stub;
import com.ex.android.util.IabResult;
import com.ex.android.util.Purchase;
import com.ex.android.util.SkuDetails;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;

public class BQPaymentBridge
{
  public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
  public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
  public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
  public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
  public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;
  public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
  public static final int BILLING_RESPONSE_RESULT_OK = 0;
  public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
  public static final String GET_SKU_DETAILS_ITEM_LIST = "ITEM_ID_LIST";
  public static final String GET_SKU_DETAILS_ITEM_TYPE_LIST = "ITEM_TYPE_LIST";
  public static final int IABHELPER_BAD_RESPONSE = -1002;
  public static final int IABHELPER_ERROR_BASE = -1000;
  public static final int IABHELPER_INVALID_CONSUMPTION = -1010;
  public static final int IABHELPER_MISSING_TOKEN = -1007;
  public static final int IABHELPER_REMOTE_EXCEPTION = -1001;
  public static final int IABHELPER_SEND_INTENT_FAILED = -1004;
  public static final int IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE = -1009;
  public static final int IABHELPER_UNKNOWN_ERROR = -1008;
  public static final int IABHELPER_UNKNOWN_PURCHASE_RESPONSE = -1006;
  public static final int IABHELPER_USER_CANCELLED = -1005;
  public static final int IABHELPER_VERIFICATION_FAILED = -1003;
  public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";
  public static final String ITEM_TYPE_INAPP = "inapp";
  public static final String ITEM_TYPE_SUBS = "subs";
  private static final String LOG_TAG = "BQPayment";
  public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";
  public static final String RESPONSE_CODE = "RESPONSE_CODE";
  public static final String RESPONSE_GET_SKU_DETAILS_LIST = "DETAILS_LIST";
  public static final String RESPONSE_INAPP_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
  public static final String RESPONSE_INAPP_PURCHASE_DATA = "INAPP_PURCHASE_DATA";
  public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
  public static final String RESPONSE_INAPP_SIGNATURE = "INAPP_DATA_SIGNATURE";
  public static final String RESPONSE_INAPP_SIGNATURE_LIST = "INAPP_DATA_SIGNATURE_LIST";
  private static BQPaymentBridge instance = new BQPaymentBridge();
  private Activity context;
  private long currentRequestIdentifier;
  private Handler handler;
  private IInAppBillingService iabService;
  private Map<String, SkuDetails> productIdentifiers = new HashMap();
  private Map<String, Purchase> purchases = new HashMap();
  private IabResult reason;
  private ServiceConnection serviceConnection;
  private boolean setuped;
  
  public static BQPaymentBridge getBridge()
  {
    return instance;
  }
  
  public static String getResponseDesc(int paramInt)
  {
    String[] arrayOfString1 = "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned".split("/");
    String[] arrayOfString2 = "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt".split("/");
    if (paramInt <= 64536)
    {
      int i = 64536 - paramInt;
      if ((i >= 0) && (i < arrayOfString2.length)) {
        return arrayOfString2[i];
      }
      return String.valueOf(paramInt) + ":Unknown IAB Helper Error";
    }
    if ((paramInt < 0) || (paramInt >= arrayOfString1.length)) {
      return String.valueOf(paramInt) + ":Unknown";
    }
    return arrayOfString1[paramInt];
  }
  
  private static native void nativeCallbackFuncForDidFailConsumePurchase(long paramLong, int paramInt, String paramString);
  
  private static native void nativeCallbackFuncForDidFailLoadingProductList(int paramInt, String paramString);
  
  private static native void nativeCallbackFuncForDidFailLoadingPurchaseList(int paramInt, String paramString);
  
  private static native void nativeCallbackFuncForDidFailPurchaseProduct(long paramLong, String paramString1, int paramInt, String paramString2);
  
  private static native void nativeCallbackFuncForDidFinishConsumePurchase(long paramLong, String paramString1, String paramString2, String paramString3);
  
  private static native void nativeCallbackFuncForDidFinishLoadingProductList();
  
  private static native void nativeCallbackFuncForDidFinishLoadingPurchaseList();
  
  private static native void nativeCallbackFuncForDidFinishPurchaseProduct(long paramLong, String paramString1, String paramString2, String paramString3);
  
  private static native void nativeCallbackFuncForLoadProduct(String paramString1, boolean paramBoolean, String paramString2, float paramFloat, String paramString3, String paramString4, String paramString5, String paramString6);
  
  private static native void nativeCallbackFuncForLoadPurchase(String paramString1, String paramString2, String paramString3);
  
  private void showDialogNotFoundSevice()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.context);
    localBuilder.setTitle(R.string.jp_co_drecom_bqpayment_notfoundservice_dialog_title);
    localBuilder.setMessage(R.string.jp_co_drecom_bqpayment_notfoundservice_dialog_message);
    localBuilder.setPositiveButton(R.string.jp_co_drecom_common_label_Exit, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        BQPaymentBridge.this.context.finish();
      }
    });
    localBuilder.setCancelable(false);
    localBuilder.create().show();
  }
  
  private void syncProductsToNative()
  {
    Iterator localIterator = this.productIdentifiers.keySet().iterator();
    if (localIterator.hasNext())
    {
      String str5 = (String)localIterator.next();
      Object localObject = (SkuDetails)this.productIdentifiers.get(str5);
      boolean bool;
      label61:
      String str1;
      label70:
      float f;
      label77:
      String str2;
      label87:
      String str3;
      label97:
      String str4;
      if (localObject != null)
      {
        bool = true;
        if (localObject != null) {
          break label141;
        }
        str1 = "UNK";
        if (localObject != null) {
          break label150;
        }
        f = 0.0F;
        if (localObject != null) {
          break label159;
        }
        str2 = "";
        if (localObject != null) {
          break label169;
        }
        str3 = "";
        if (localObject != null) {
          break label179;
        }
        str4 = "";
        label107:
        if (localObject != null) {
          break label189;
        }
      }
      label141:
      label150:
      label159:
      label169:
      label179:
      label189:
      for (localObject = "";; localObject = ((SkuDetails)localObject).getDescription())
      {
        nativeCallbackFuncForLoadProduct(str5, bool, str1, f, str2, str3, str4, (String)localObject);
        break;
        bool = false;
        break label61;
        str1 = ((SkuDetails)localObject).getPriceCurrencyCode();
        break label70;
        f = ((SkuDetails)localObject).getPriceValueFloat();
        break label77;
        str2 = ((SkuDetails)localObject).getPriceValueString();
        break label87;
        str3 = ((SkuDetails)localObject).getPrice();
        break label97;
        str4 = ((SkuDetails)localObject).getTitle();
        break label107;
      }
    }
  }
  
  private void syncPurchasesToNative()
  {
    Iterator localIterator = this.purchases.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Purchase localPurchase = (Purchase)((Map.Entry)localIterator.next()).getValue();
      if (localPurchase != null) {
        nativeCallbackFuncForLoadPurchase(localPurchase.getSku(), localPurchase.getToken(), localPurchase.getSignature());
      }
    }
  }
  
  public void consumePurchase(final long paramLong, String paramString)
  {
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        if (BQPaymentBridge.this.iabService == null)
        {
          BQPaymentBridge.nativeCallbackFuncForDidFailConsumePurchase(paramLong, -1, "Can not call consumePurchase()");
          return;
        }
        if ((this.val$token == null) || (this.val$token.equals("")))
        {
          BQPaymentBridge.nativeCallbackFuncForDidFailConsumePurchase(paramLong, -1, "Can't consume. No token.");
          return;
        }
        Purchase localPurchase = (Purchase)BQPaymentBridge.this.purchases.get(this.val$token);
        int i;
        try
        {
          i = BQPaymentBridge.this.iabService.consumePurchase(3, BQPaymentBridge.this.context.getPackageName(), this.val$token);
          if (i == 0)
          {
            String str1 = "";
            String str2 = "";
            String str3 = "";
            if (localPurchase != null)
            {
              str1 = localPurchase.getSku();
              str2 = localPurchase.getOriginalJson();
              str3 = localPurchase.getSignature();
            }
            BQPaymentBridge.nativeCallbackFuncForDidFinishConsumePurchase(paramLong, str1, str2, str3);
            return;
          }
        }
        catch (RemoteException localRemoteException)
        {
          BQPaymentBridge.nativeCallbackFuncForDidFailConsumePurchase(paramLong, 64535, "Remote exception while consuming.");
          return;
        }
        BQPaymentBridge.nativeCallbackFuncForDidFailConsumePurchase(paramLong, i, "Can't consume. No token.");
      }
    });
  }
  
  public void dispose()
  {
    if (this.serviceConnection != null)
    {
      if (this.context != null) {
        this.context.unbindService(this.serviceConnection);
      }
      this.serviceConnection = null;
      this.iabService = null;
    }
    this.setuped = false;
  }
  
  int getResponseCodeFromBundle(Bundle paramBundle)
  {
    paramBundle = paramBundle.get("RESPONSE_CODE");
    if (paramBundle == null) {
      return 0;
    }
    if ((paramBundle instanceof Integer)) {
      return ((Integer)paramBundle).intValue();
    }
    if ((paramBundle instanceof Long)) {
      return (int)((Long)paramBundle).longValue();
    }
    throw new RuntimeException("Unexpected type for bundle response code: " + paramBundle.getClass().getName());
  }
  
  int getResponseCodeFromIntent(Intent paramIntent)
  {
    paramIntent = paramIntent.getExtras().get("RESPONSE_CODE");
    if (paramIntent == null) {
      return 0;
    }
    if ((paramIntent instanceof Integer)) {
      return ((Integer)paramIntent).intValue();
    }
    if ((paramIntent instanceof Long)) {
      return (int)((Long)paramIntent).longValue();
    }
    throw new RuntimeException("Unexpected type for intent response code: " + paramIntent.getClass().getName());
  }
  
  public boolean handleActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 != (int)this.currentRequestIdentifier) {
      return false;
    }
    if (paramIntent == null)
    {
      paramIntent = new IabResult(64534, "Null data in IAB result");
      nativeCallbackFuncForDidFailPurchaseProduct(this.currentRequestIdentifier, "", paramIntent.getResponse(), paramIntent.getMessage());
      return true;
    }
    paramInt1 = getResponseCodeFromIntent(paramIntent);
    String str = paramIntent.getStringExtra("INAPP_PURCHASE_DATA");
    paramIntent = paramIntent.getStringExtra("INAPP_DATA_SIGNATURE");
    if ((paramInt2 == -1) && (paramInt1 == 0))
    {
      if ((str == null) || (paramIntent == null))
      {
        paramIntent = new IabResult(64528, "IAB returned null purchaseData or dataSignature");
        nativeCallbackFuncForDidFailPurchaseProduct(this.currentRequestIdentifier, "", paramIntent.getResponse(), paramIntent.getMessage());
        return true;
      }
      try
      {
        paramIntent = new Purchase("inapp", str, paramIntent);
        nativeCallbackFuncForDidFinishPurchaseProduct(this.currentRequestIdentifier, paramIntent.getSku(), paramIntent.getOriginalJson(), paramIntent.getSignature());
        return true;
      }
      catch (JSONException paramIntent)
      {
        paramIntent = new IabResult(64534, "Failed to parse purchase data.");
        nativeCallbackFuncForDidFailPurchaseProduct(this.currentRequestIdentifier, "", paramIntent.getResponse(), paramIntent.getMessage());
        return true;
      }
    }
    if (paramInt2 == -1)
    {
      paramIntent = new IabResult(paramInt1, "Problem purchashing item.");
      nativeCallbackFuncForDidFailPurchaseProduct(this.currentRequestIdentifier, "", paramIntent.getResponse(), paramIntent.getMessage());
      return true;
    }
    if (paramInt2 == 0)
    {
      paramIntent = new IabResult(64531, "User canceled.");
      nativeCallbackFuncForDidFailPurchaseProduct(this.currentRequestIdentifier, "", paramIntent.getResponse(), paramIntent.getMessage());
      return true;
    }
    paramIntent = new IabResult(64530, "Unknown purchase response.");
    nativeCallbackFuncForDidFailPurchaseProduct(this.currentRequestIdentifier, "", paramIntent.getResponse(), paramIntent.getMessage());
    return true;
  }
  
  public boolean isAvailablePayment()
  {
    return this.setuped;
  }
  
  public void requestPayment(final long paramLong, String paramString, int paramInt)
  {
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        if (BQPaymentBridge.this.iabService == null)
        {
          BQPaymentBridge.nativeCallbackFuncForDidFailPurchaseProduct(paramLong, "", -1, "Can not call getBuyIntent()");
          return;
        }
        try
        {
          Bundle localBundle = BQPaymentBridge.this.iabService.getBuyIntent(3, BQPaymentBridge.this.context.getPackageName(), this.val$productIdentifier, "inapp", null);
          int i = BQPaymentBridge.this.getResponseCodeFromBundle(localBundle);
          if (i != 0)
          {
            BQPaymentBridge.access$702(BQPaymentBridge.this, new IabResult(i, "Unable to buy item"));
            BQPaymentBridge.nativeCallbackFuncForDidFailPurchaseProduct(paramLong, "", BQPaymentBridge.this.reason.getResponse(), BQPaymentBridge.this.reason.getMessage());
            return;
          }
        }
        catch (IntentSender.SendIntentException localSendIntentException)
        {
          BQPaymentBridge.access$702(BQPaymentBridge.this, new IabResult(64532, "Failed to send intent."));
          BQPaymentBridge.nativeCallbackFuncForDidFailPurchaseProduct(paramLong, "", BQPaymentBridge.this.reason.getResponse(), BQPaymentBridge.this.reason.getMessage());
          return;
          PendingIntent localPendingIntent = (PendingIntent)localSendIntentException.getParcelable("BUY_INTENT");
          BQPaymentBridge.access$802(BQPaymentBridge.this, paramLong);
          BQPaymentBridge.this.context.startIntentSenderForResult(localPendingIntent.getIntentSender(), (int)paramLong, new Intent(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue());
          return;
        }
        catch (RemoteException localRemoteException)
        {
          BQPaymentBridge.access$702(BQPaymentBridge.this, new IabResult(64535, "Remote exception while starting purchase flow"));
          BQPaymentBridge.nativeCallbackFuncForDidFailPurchaseProduct(paramLong, "", BQPaymentBridge.this.reason.getResponse(), BQPaymentBridge.this.reason.getMessage());
        }
      }
    });
  }
  
  public void requestPaymentDetails(final String[] paramArrayOfString)
  {
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      public void run()
      {
        if (BQPaymentBridge.this.iabService == null)
        {
          BQPaymentBridge.nativeCallbackFuncForDidFailLoadingProductList(-1, "Can not call getSkuDetails()");
          return;
        }
        BQPaymentBridge.this.productIdentifiers.clear();
        if (paramArrayOfString == null)
        {
          BQPaymentBridge.nativeCallbackFuncForDidFailLoadingProductList(4, "INAPP_PURCHASE_ITEM_LIST");
          return;
        }
        Object localObject1 = new ArrayList(Arrays.asList(paramArrayOfString));
        if (((ArrayList)localObject1).size() == 0)
        {
          BQPaymentBridge.access$300();
          return;
        }
        Object localObject2 = ((ArrayList)localObject1).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          String str = (String)((Iterator)localObject2).next();
          BQPaymentBridge.this.productIdentifiers.put(str, null);
        }
        localObject2 = new Bundle();
        ((Bundle)localObject2).putStringArrayList("ITEM_ID_LIST", (ArrayList)localObject1);
        try
        {
          localObject1 = BQPaymentBridge.this.iabService.getSkuDetails(3, BQPaymentBridge.this.context.getPackageName(), "inapp", (Bundle)localObject2);
          if ((localObject1 == null) || (((Bundle)localObject1).containsKey("DETAILS_LIST"))) {
            break label204;
          }
          if (BQPaymentBridge.this.getResponseCodeFromBundle((Bundle)localObject1) != 0)
          {
            BQPaymentBridge.nativeCallbackFuncForDidFailLoadingProductList(6, "INAPP_PURCHASE_ITEM_LIST");
            return;
          }
        }
        catch (RemoteException localRemoteException)
        {
          BQPaymentBridge.nativeCallbackFuncForDidFailLoadingProductList(4, "INAPP_PURCHASE_ITEM_LIST");
          localRemoteException.printStackTrace();
          return;
        }
        BQPaymentBridge.nativeCallbackFuncForDidFailLoadingProductList(3, "INAPP_PURCHASE_ITEM_LIST");
        return;
        label204:
        Iterator localIterator = localRemoteException.getStringArrayList("DETAILS_LIST").iterator();
        while (localIterator.hasNext())
        {
          localObject2 = (String)localIterator.next();
          try
          {
            localObject2 = new SkuDetails("inapp", (String)localObject2);
            BQPaymentBridge.this.productIdentifiers.put(((SkuDetails)localObject2).getSku(), localObject2);
          }
          catch (JSONException localJSONException) {}
        }
        BQPaymentBridge.this.syncProductsToNative();
        BQPaymentBridge.access$300();
      }
    });
  }
  
  public void requestPurchases()
  {
    HandlerJava.getInstance().runOnUIThread(new Runnable()
    {
      /* Error */
      public void run()
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	jp/co/drecom/bisque/lib/BQPaymentBridge$3:this$0	Ljp/co/drecom/bisque/lib/BQPaymentBridge;
        //   4: invokestatic 29	jp/co/drecom/bisque/lib/BQPaymentBridge:access$900	(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;
        //   7: invokeinterface 34 1 0
        //   12: aconst_null
        //   13: astore_3
        //   14: aload_0
        //   15: getfield 17	jp/co/drecom/bisque/lib/BQPaymentBridge$3:this$0	Ljp/co/drecom/bisque/lib/BQPaymentBridge;
        //   18: invokestatic 38	jp/co/drecom/bisque/lib/BQPaymentBridge:access$000	(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;
        //   21: ifnonnull +10 -> 31
        //   24: iconst_m1
        //   25: ldc 40
        //   27: invokestatic 44	jp/co/drecom/bisque/lib/BQPaymentBridge:access$1000	(ILjava/lang/String;)V
        //   30: return
        //   31: aload_0
        //   32: getfield 17	jp/co/drecom/bisque/lib/BQPaymentBridge$3:this$0	Ljp/co/drecom/bisque/lib/BQPaymentBridge;
        //   35: invokestatic 38	jp/co/drecom/bisque/lib/BQPaymentBridge:access$000	(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;
        //   38: iconst_3
        //   39: aload_0
        //   40: getfield 17	jp/co/drecom/bisque/lib/BQPaymentBridge$3:this$0	Ljp/co/drecom/bisque/lib/BQPaymentBridge;
        //   43: invokestatic 48	jp/co/drecom/bisque/lib/BQPaymentBridge:access$400	(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;
        //   46: invokevirtual 54	android/app/Activity:getPackageName	()Ljava/lang/String;
        //   49: ldc 56
        //   51: aload_3
        //   52: invokeinterface 62 5 0
        //   57: astore_3
        //   58: aload_0
        //   59: getfield 17	jp/co/drecom/bisque/lib/BQPaymentBridge$3:this$0	Ljp/co/drecom/bisque/lib/BQPaymentBridge;
        //   62: aload_3
        //   63: invokevirtual 66	jp/co/drecom/bisque/lib/BQPaymentBridge:getResponseCodeFromBundle	(Landroid/os/Bundle;)I
        //   66: istore_1
        //   67: iload_1
        //   68: ifeq +38 -> 106
        //   71: iload_1
        //   72: new 68	java/lang/StringBuilder
        //   75: dup
        //   76: invokespecial 69	java/lang/StringBuilder:<init>	()V
        //   79: ldc 71
        //   81: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   84: iload_1
        //   85: invokestatic 79	jp/co/drecom/bisque/lib/BQPaymentBridge:getResponseDesc	(I)Ljava/lang/String;
        //   88: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   91: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   94: invokestatic 44	jp/co/drecom/bisque/lib/BQPaymentBridge:access$1000	(ILjava/lang/String;)V
        //   97: return
        //   98: astore_3
        //   99: iconst_m1
        //   100: ldc 40
        //   102: invokestatic 44	jp/co/drecom/bisque/lib/BQPaymentBridge:access$1000	(ILjava/lang/String;)V
        //   105: return
        //   106: aload_3
        //   107: ldc 84
        //   109: invokevirtual 90	android/os/Bundle:containsKey	(Ljava/lang/String;)Z
        //   112: ifeq +21 -> 133
        //   115: aload_3
        //   116: ldc 92
        //   118: invokevirtual 90	android/os/Bundle:containsKey	(Ljava/lang/String;)Z
        //   121: ifeq +12 -> 133
        //   124: aload_3
        //   125: ldc 94
        //   127: invokevirtual 90	android/os/Bundle:containsKey	(Ljava/lang/String;)Z
        //   130: ifne +10 -> 140
        //   133: iload_1
        //   134: ldc 96
        //   136: invokestatic 44	jp/co/drecom/bisque/lib/BQPaymentBridge:access$1000	(ILjava/lang/String;)V
        //   139: return
        //   140: aload_3
        //   141: ldc 92
        //   143: invokevirtual 100	android/os/Bundle:getStringArrayList	(Ljava/lang/String;)Ljava/util/ArrayList;
        //   146: astore 4
        //   148: aload_3
        //   149: ldc 94
        //   151: invokevirtual 100	android/os/Bundle:getStringArrayList	(Ljava/lang/String;)Ljava/util/ArrayList;
        //   154: astore 5
        //   156: iconst_0
        //   157: istore_1
        //   158: iload_1
        //   159: aload 4
        //   161: invokevirtual 106	java/util/ArrayList:size	()I
        //   164: if_icmpge +85 -> 249
        //   167: aload 4
        //   169: iload_1
        //   170: invokevirtual 110	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   173: checkcast 112	java/lang/String
        //   176: astore 6
        //   178: aload 5
        //   180: iload_1
        //   181: invokevirtual 110	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //   184: checkcast 112	java/lang/String
        //   187: astore 7
        //   189: new 114	com/ex/android/util/Purchase
        //   192: dup
        //   193: ldc 56
        //   195: aload 6
        //   197: aload 7
        //   199: invokespecial 117	com/ex/android/util/Purchase:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   202: astore 6
        //   204: aload 6
        //   206: invokevirtual 120	com/ex/android/util/Purchase:getToken	()Ljava/lang/String;
        //   209: invokestatic 126	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //   212: istore_2
        //   213: iload_2
        //   214: ifeq +3 -> 217
        //   217: aload_0
        //   218: getfield 17	jp/co/drecom/bisque/lib/BQPaymentBridge$3:this$0	Ljp/co/drecom/bisque/lib/BQPaymentBridge;
        //   221: invokestatic 29	jp/co/drecom/bisque/lib/BQPaymentBridge:access$900	(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;
        //   224: aload 6
        //   226: invokevirtual 120	com/ex/android/util/Purchase:getToken	()Ljava/lang/String;
        //   229: aload 6
        //   231: invokeinterface 130 3 0
        //   236: pop
        //   237: iload_1
        //   238: iconst_1
        //   239: iadd
        //   240: istore_1
        //   241: goto -83 -> 158
        //   244: astore 6
        //   246: goto -9 -> 237
        //   249: aload_3
        //   250: ldc -124
        //   252: invokevirtual 136	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
        //   255: astore 4
        //   257: aload 4
        //   259: astore_3
        //   260: aload 4
        //   262: invokestatic 126	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //   265: ifeq -234 -> 31
        //   268: aload_0
        //   269: getfield 17	jp/co/drecom/bisque/lib/BQPaymentBridge$3:this$0	Ljp/co/drecom/bisque/lib/BQPaymentBridge;
        //   272: invokestatic 139	jp/co/drecom/bisque/lib/BQPaymentBridge:access$1100	(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V
        //   275: invokestatic 142	jp/co/drecom/bisque/lib/BQPaymentBridge:access$1200	()V
        //   278: return
        //   279: astore 6
        //   281: goto -35 -> 246
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	284	0	this	3
        //   66	175	1	i	int
        //   212	2	2	bool	boolean
        //   13	50	3	localObject1	Object
        //   98	152	3	localRemoteException	RemoteException
        //   259	1	3	localObject2	Object
        //   146	115	4	localObject3	Object
        //   154	25	5	localArrayList	ArrayList
        //   176	54	6	localObject4	Object
        //   244	1	6	localJSONException1	JSONException
        //   279	1	6	localJSONException2	JSONException
        //   187	11	7	str	String
        // Exception table:
        //   from	to	target	type
        //   31	58	98	android/os/RemoteException
        //   189	204	244	org/json/JSONException
        //   204	213	279	org/json/JSONException
      }
    });
  }
  
  public <A extends Activity,  extends BQPaymentDispatchable> void setContext(A paramA)
  {
    this.context = paramA;
    if (paramA == null)
    {
      this.handler = null;
      return;
    }
    ((BQPaymentDispatchable)paramA).setBQPaymentBridge(this);
    this.handler = ((BQPaymentDispatchable)paramA).getHandler();
  }
  
  public void setup()
  {
    if (BQAppPlatformManager.isEmulator()) {}
    boolean bool1;
    do
    {
      do
      {
        return;
      } while (this.setuped);
      this.reason = null;
      this.serviceConnection = new ServiceConnection()
      {
        public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
        {
          BQPaymentBridge.access$002(BQPaymentBridge.this, IInAppBillingService.Stub.asInterface(paramAnonymousIBinder));
          paramAnonymousComponentName = BQPaymentBridge.this.context.getPackageName();
          try
          {
            int i = BQPaymentBridge.this.iabService.isBillingSupported(3, paramAnonymousComponentName, "inapp");
            if (i != 0)
            {
              BQPaymentBridge.access$702(BQPaymentBridge.this, new IabResult(i, "Error checking for billing v3 support."));
              return;
            }
            BQPaymentBridge.access$1502(BQPaymentBridge.this, true);
            BQPaymentBridge.access$702(BQPaymentBridge.this, new IabResult(0, "Setup successful."));
            return;
          }
          catch (RemoteException paramAnonymousComponentName)
          {
            BQPaymentBridge.access$702(BQPaymentBridge.this, new IabResult(64535, "RemoteException while setting up in-app billing."));
          }
        }
        
        public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
        {
          BQPaymentBridge.access$002(BQPaymentBridge.this, null);
          BQPaymentBridge.access$1502(BQPaymentBridge.this, false);
          BQPaymentBridge.this.context.runOnUiThread(new Runnable()
          {
            public void run()
            {
              BQPaymentBridge.this.setup();
            }
          });
        }
      };
      Intent localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
      localIntent.setPackage("com.android.vending");
      boolean bool2 = false;
      List localList = this.context.getPackageManager().queryIntentServices(localIntent, 0);
      bool1 = bool2;
      if (localList != null)
      {
        bool1 = bool2;
        if (!localList.isEmpty()) {
          bool1 = this.context.bindService(localIntent, this.serviceConnection, 1);
        }
      }
    } while (bool1);
    this.reason = new IabResult(3, "Billing service unavailable on device.");
    showDialogNotFoundSevice();
  }
  
  public void unbindService()
  {
    if (this.serviceConnection != null)
    {
      if (this.context != null) {
        this.context.unbindService(this.serviceConnection);
      }
      this.serviceConnection = null;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQPaymentBridge.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */