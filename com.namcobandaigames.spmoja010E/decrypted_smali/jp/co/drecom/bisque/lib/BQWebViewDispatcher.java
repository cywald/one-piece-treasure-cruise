package jp.co.drecom.bisque.lib;

public abstract interface BQWebViewDispatcher
{
  public abstract boolean addWebView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean, String paramString2, String paramString3);
  
  public abstract boolean addWebView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, String paramString2, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean, String paramString3, String paramString4);
  
  public abstract boolean addWebView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, String[] paramArrayOfString1, String[] paramArrayOfString2, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean, String paramString2, String paramString3);
  
  public abstract void bouncesWebView(boolean paramBoolean, int paramInt);
  
  public abstract void callbackForWebViewDidFailLoadWithError(String paramString);
  
  public abstract void callbackForWebViewDidFinishLoad(int paramInt);
  
  public abstract boolean callbackForWebViewShouldStartLoadWithRequest(String paramString, int paramInt);
  
  public abstract void clearCache(int paramInt);
  
  public abstract void clearCookie(int paramInt);
  
  public abstract void enableCache(boolean paramBoolean, int paramInt);
  
  public abstract void enableCookie(boolean paramBoolean, int paramInt);
  
  public abstract void enableWebView(boolean paramBoolean, int paramInt);
  
  public abstract void executeJsInWebView(String paramString, int paramInt);
  
  public abstract void reloadWebView(int paramInt);
  
  public abstract void removeWebView(int paramInt);
  
  public abstract void requestWebView(String paramString1, int paramInt, boolean paramBoolean, String paramString2, String paramString3);
  
  public abstract void requestWebView(String paramString1, String paramString2, int paramInt, boolean paramBoolean, String paramString3, String paramString4);
  
  public abstract void requestWebView(String paramString1, String[] paramArrayOfString1, String[] paramArrayOfString2, int paramInt, boolean paramBoolean, String paramString2, String paramString3);
  
  public abstract void setOrderWebView(int paramInt1, int paramInt2);
  
  public abstract void setRectWebView(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQWebViewDispatcher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */