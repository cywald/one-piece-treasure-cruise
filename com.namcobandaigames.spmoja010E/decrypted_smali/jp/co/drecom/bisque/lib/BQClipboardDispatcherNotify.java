package jp.co.drecom.bisque.lib;

import android.os.Handler;

public class BQClipboardDispatcherNotify
{
  private static final int MAX_INVOKE_WAIT_TIME = 1;
  private static String clipBoardText;
  public static BQClipboardDispatcher dispatcher;
  public static Handler handler;
  
  public static String getStringFromClipboard()
  {
    clipBoardText = null;
    AwaitInvoker localAwaitInvoker = new AwaitInvoker();
    Runnable local2 = new Runnable()
    {
      public void run()
      {
        BQClipboardDispatcherNotify.access$002(BQClipboardDispatcherNotify.dispatcher.getStringFromClipboard());
      }
    };
    if (!localAwaitInvoker.invokeAndWait(handler, local2, 1)) {
      clipBoardText = null;
    }
    return clipBoardText;
  }
  
  public static void setDispatcher(BQClipboardDispatcher paramBQClipboardDispatcher, Handler paramHandler)
  {
    dispatcher = paramBQClipboardDispatcher;
    handler = paramHandler;
  }
  
  public static void setStringToClipboard(String paramString)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQClipboardDispatcherNotify.dispatcher.setStringToClipboard(this.val$_string);
      }
    });
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQClipboardDispatcherNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */