package jp.co.drecom.bisque.lib;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.webkit.HttpAuthHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.File;
import org.json.JSONException;
import org.json.JSONObject;

public class BQWebView
  extends WebView
{
  private static Context wcontext;
  public final int BQ_WEBVIEW_BOUNCES_OFF = 1;
  public final int BQ_WEBVIEW_BOUNCES_ON = 2;
  public final int BQ_WEBVIEW_BOUNCES_OSDEFAULT = 0;
  private final String ERROR_TEMPLATE_CODE = "<html><body style='background: black;'><p style='color: red;'>Unable to load webview error template file.</p></body></html>";
  private String ERROR_TEMPLATE_FILE = "";
  private final String ERROR_TEMPLATE_PATH = "file:///android_asset/";
  private final String META_DATA_KEY_ENABLE_WEBVIEW_ZOOM = "jp.co.drecom.bisque.enable.webview.zoom";
  private final String TAG = "BQWebView";
  private boolean auth = false;
  private String authpassword = "";
  private String authusername = "";
  private boolean bounces;
  private int count;
  private BQWebViewDispatcher dispatcher;
  private boolean enableBackKey = false;
  private boolean enableBringToFront = false;
  private boolean interaction = true;
  private boolean move = false;
  private int old_scrollY;
  private boolean onwv = false;
  private int scrollHeightMax;
  public int tag = 0;
  
  public BQWebView(Context paramContext, AttributeSet paramAttributeSet, BQWebViewDispatcher paramBQWebViewDispatcher)
  {
    super(paramContext, paramAttributeSet);
    initDispatcher(paramBQWebViewDispatcher);
  }
  
  public BQWebView(Context paramContext, BQWebViewDispatcher paramBQWebViewDispatcher, String paramString)
  {
    super(paramContext);
    wcontext = paramContext;
    initDispatcher(paramBQWebViewDispatcher);
    this.old_scrollY = 0;
    this.count = 0;
    this.bounces = false;
    setOverScrollMode(0);
    this.scrollHeightMax = getContentHeight();
    this.ERROR_TEMPLATE_FILE = paramString;
    paramBQWebViewDispatcher = paramContext.getApplicationContext().getDir("localstorage", 0).getPath();
    getSettings().setDomStorageEnabled(true);
    getSettings().setDatabasePath(paramBQWebViewDispatcher);
    bool2 = false;
    try
    {
      paramContext = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
      bool1 = bool2;
      if (paramContext != null)
      {
        bool1 = bool2;
        if ((paramContext.flags & 0x2) == 2)
        {
          bool1 = bool2;
          if (paramContext.metaData != null)
          {
            bool1 = bool2;
            if (paramContext.metaData.containsKey("jp.co.drecom.bisque.enable.webview.zoom")) {
              bool1 = paramContext.metaData.getBoolean("jp.co.drecom.bisque.enable.webview.zoom");
            }
          }
        }
      }
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;)
      {
        boolean bool1 = bool2;
      }
    }
    getSettings().setBuiltInZoomControls(bool1);
  }
  
  public static native void nativeCallbackFuncForWebViewDidFailLoadWithError(String paramString);
  
  public static native void nativeCallbackFuncForWebViewDidFinishLoad(int paramInt);
  
  public static native boolean nativeCallbackFuncForWebViewShouldStartLoadWithRequest(String paramString, int paramInt);
  
  public void backKeyDisable()
  {
    this.enableBackKey = false;
  }
  
  public void backKeyEnable()
  {
    this.enableBackKey = true;
  }
  
  public void bouncesOff()
  {
    this.bounces = false;
  }
  
  public void bouncesOn()
  {
    this.bounces = true;
  }
  
  public void enableBringToFront(boolean paramBoolean)
  {
    this.enableBringToFront = paramBoolean;
    if (this.enableBringToFront) {
      bringToFront();
    }
  }
  
  public void initDispatcher(BQWebViewDispatcher paramBQWebViewDispatcher)
  {
    this.dispatcher = paramBQWebViewDispatcher;
    getSettings().setJavaScriptEnabled(true);
    setVerticalScrollbarOverlay(true);
    getSettings().setCacheMode(2);
    setBackgroundColor(-16777216);
    setWebViewClient(new WebViewClient()
    {
      public void onPageFinished(WebView paramAnonymousWebView, String paramAnonymousString)
      {
        BQWebView.this.requestFocus(130);
        BQWebView.this.dispatcher.callbackForWebViewDidFinishLoad(BQWebView.this.tag);
      }
      
      public void onPageStarted(WebView paramAnonymousWebView, String paramAnonymousString, Bitmap paramAnonymousBitmap) {}
      
      public void onReceivedError(WebView paramAnonymousWebView, int paramAnonymousInt, String paramAnonymousString1, String paramAnonymousString2)
      {
        if (!"".equals(BQWebView.this.ERROR_TEMPLATE_FILE))
        {
          if (!new File("file:///android_asset/" + BQWebView.this.ERROR_TEMPLATE_FILE).exists()) {
            break label159;
          }
          paramAnonymousWebView.loadUrl("file:///android_asset/" + BQWebView.this.ERROR_TEMPLATE_FILE);
        }
        for (;;)
        {
          JSONObject localJSONObject = new JSONObject();
          try
          {
            localJSONObject.put("FailingURL", paramAnonymousString2);
            localJSONObject.put("ErrorDescription", paramAnonymousString1 + "(errorCode " + paramAnonymousInt + ")");
            paramAnonymousWebView.goForward();
            BQWebView.this.dispatcher.callbackForWebViewDidFailLoadWithError(localJSONObject.toString());
            return;
            label159:
            paramAnonymousWebView.loadData("<html><body style='background: black;'><p style='color: red;'>Unable to load webview error template file.</p></body></html>", "text/html", null);
          }
          catch (JSONException paramAnonymousString1)
          {
            for (;;) {}
          }
        }
      }
      
      public void onReceivedHttpAuthRequest(WebView paramAnonymousWebView, HttpAuthHandler paramAnonymousHttpAuthHandler, String paramAnonymousString1, String paramAnonymousString2)
      {
        if (BQWebView.this.auth) {
          paramAnonymousHttpAuthHandler.proceed(BQWebView.this.authusername, BQWebView.this.authpassword);
        }
      }
      
      public boolean shouldOverrideUrlLoading(WebView paramAnonymousWebView, String paramAnonymousString)
      {
        return BQWebView.this.dispatcher.callbackForWebViewShouldStartLoadWithRequest(paramAnonymousString, BQWebView.this.tag);
      }
    });
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) || (paramInt == 82)) {
      return false;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) || (paramInt == 82)) {
      return false;
    }
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i;
    if (this.bounces)
    {
      i = (int)(getHeight() * 0.15D);
      if ((this.old_scrollY <= -i) && (paramInt2 > -i) && (this.count > 3)) {}
      this.old_scrollY = paramInt2;
      if (paramInt2 > -i) {
        break label87;
      }
    }
    label87:
    for (this.count += 1;; this.count = 0)
    {
      super.onOverScrolled(paramInt1, paramInt2, paramBoolean1, paramBoolean2);
      return;
      i = 0;
      this.count = 0;
      break;
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    int i = getLeft();
    int j = getTop();
    int k = getRight();
    int m = getBottom();
    if (((!this.enableBringToFront) && (f1 >= i) && (f1 <= k) && (f2 >= j) && (f2 <= m)) || ((this.enableBringToFront == true) && (f1 >= 0.0F) && (f1 <= k - i) && (f2 >= 0.0F) && (f2 <= m - j)))
    {
      if ((!this.onwv) && ((paramMotionEvent.getAction() & 0xFF) != 0))
      {
        resetOverScroll();
        return false;
      }
      this.onwv = true;
      if ((paramMotionEvent.getAction() & 0xFF) == 2)
      {
        this.move = true;
        label161:
        if (this.enableBringToFront) {
          break label259;
        }
        f1 -= i;
        f2 -= j;
      }
    }
    label259:
    for (;;)
    {
      paramMotionEvent.setLocation(f1, f2);
      if (!this.interaction) {
        break;
      }
      return super.onTouchEvent(paramMotionEvent);
      this.move = false;
      break label161;
      if (this.move == true)
      {
        if ((paramMotionEvent.getAction() & 0xFF) == 2)
        {
          this.move = true;
          break label161;
        }
        this.move = false;
        this.onwv = false;
        break label161;
      }
      this.onwv = false;
      resetOverScroll();
      return false;
    }
  }
  
  protected boolean overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean)
  {
    this.scrollHeightMax = paramInt6;
    if (this.bounces) {}
    for (paramInt1 = (int)(getHeight() * 0.15D);; paramInt1 = 0) {
      return super.overScrollBy(0, paramInt2, 0, paramInt4, 0, paramInt6, 0, paramInt1, paramBoolean);
    }
  }
  
  void resetOverScroll()
  {
    int i = this.scrollHeightMax;
    int j = this.old_scrollY;
    if (this.old_scrollY < 0)
    {
      this.old_scrollY = 0;
      super.onOverScrolled(0, 0, false, false);
    }
    while (this.old_scrollY <= i) {
      return;
    }
    this.old_scrollY = i;
    super.onOverScrolled(0, i, false, false);
  }
  
  public void setAuthInfo(boolean paramBoolean, String paramString1, String paramString2)
  {
    this.auth = paramBoolean;
    if (this.auth)
    {
      this.authusername = paramString1;
      this.authpassword = paramString2;
    }
  }
  
  public void setInteraction(boolean paramBoolean)
  {
    this.interaction = paramBoolean;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQWebView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */