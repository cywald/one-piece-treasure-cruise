package jp.co.drecom.bisque.lib;

public class IndependenceException
  extends RuntimeException
{
  public IndependenceException() {}
  
  public IndependenceException(String paramString)
  {
    super(paramString);
  }
  
  public IndependenceException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
    destruct();
  }
  
  public IndependenceException(Throwable paramThrowable)
  {
    super(paramThrowable);
    destruct();
  }
  
  void destruct() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\IndependenceException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */