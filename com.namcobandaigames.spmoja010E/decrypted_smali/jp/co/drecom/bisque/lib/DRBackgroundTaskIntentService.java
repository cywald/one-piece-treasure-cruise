package jp.co.drecom.bisque.lib;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class DRBackgroundTaskIntentService
  extends IntentService
{
  public static final String ACTION_BGTASK = "jp.co.drecom.bisque.lib.drbackgroundtask.action.BGTASK";
  public static final String BROADCAST_BGTASK = "jp.co.drecom.bisque.lib.drbackgroundtask.broadcast.BGTASK";
  public static final String EXTENDED_STATUS = "jp.co.drecom.bisque.lib.drbackgroundtask.extra.STATUS";
  public static final String EXTENDED_UUID = "jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID";
  public static final String STATUS_FINISH = "jp.co.drecom.bisque.lib.drbackgroundtask.status.FINISH";
  
  public DRBackgroundTaskIntentService()
  {
    super("DRBackgroundTaskIntentService");
  }
  
  private void handleActionBGTask(String paramString)
  {
    DRBackgroundTaskHelper.task(paramString);
    Intent localIntent = new Intent("jp.co.drecom.bisque.lib.drbackgroundtask.broadcast.BGTASK");
    localIntent.putExtra("jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID", paramString);
    localIntent.putExtra("jp.co.drecom.bisque.lib.drbackgroundtask.extra.STATUS", "jp.co.drecom.bisque.lib.drbackgroundtask.status.FINISH");
    LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
  }
  
  public static boolean startAction(Context paramContext, String paramString)
  {
    Intent localIntent = new Intent(paramContext, DRBackgroundTaskIntentService.class);
    localIntent.setAction("jp.co.drecom.bisque.lib.drbackgroundtask.action.BGTASK");
    localIntent.putExtra("jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID", paramString);
    paramContext.startService(localIntent);
    return true;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    if (paramIntent != null)
    {
      String str = paramIntent.getAction();
      paramIntent = paramIntent.getStringExtra("jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID");
      if ("jp.co.drecom.bisque.lib.drbackgroundtask.action.BGTASK".equals(str)) {
        handleActionBGTask(paramIntent);
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\DRBackgroundTaskIntentService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */