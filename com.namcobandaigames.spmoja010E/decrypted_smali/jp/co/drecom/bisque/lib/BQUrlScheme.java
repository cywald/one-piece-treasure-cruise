package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class BQUrlScheme
{
  private Activity parent;
  
  public BQUrlScheme(Context paramContext)
  {
    this.parent = ((Activity)paramContext);
  }
  
  public void execOpenApplicationDetailsSettings()
  {
    Uri localUri = Uri.fromParts("package", this.parent.getPackageName(), null);
    try
    {
      Intent localIntent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
      localIntent.setData(localUri);
      this.parent.startActivity(localIntent);
      return;
    }
    catch (Exception localException) {}
  }
  
  public void execUrlScheme(String paramString)
  {
    paramString = Uri.parse(paramString);
    try
    {
      paramString = new Intent("android.intent.action.VIEW", paramString);
      this.parent.startActivity(paramString);
      return;
    }
    catch (Exception paramString) {}
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQUrlScheme.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */