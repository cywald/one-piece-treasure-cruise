package jp.co.drecom.bisque.lib;

import android.os.Handler;

public abstract interface BQPaymentDispatchable
{
  public abstract Handler getHandler();
  
  public abstract void setBQPaymentBridge(BQPaymentBridge paramBQPaymentBridge);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQPaymentDispatchable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */