package jp.co.drecom.bisque.lib;

import android.os.Handler;

public class BQHandlerDispatcherNotify
{
  public static BQHandlerDispatcher dispatcher;
  public static Handler handler;
  
  public static void handlerPost(long paramLong1, long paramLong2, final int paramInt)
  {
    handler.post(new Runnable()
    {
      public void run()
      {
        BQHandlerDispatcherNotify.dispatcher.handlerPost(this.val$_func, paramInt, this.val$_type);
      }
    });
  }
  
  public static void setDispatcher(BQHandlerDispatcher paramBQHandlerDispatcher, Handler paramHandler)
  {
    dispatcher = paramBQHandlerDispatcher;
    handler = paramHandler;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQHandlerDispatcherNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */