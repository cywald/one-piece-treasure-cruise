package jp.co.drecom.bisque.lib;

public abstract interface BQUrlSchemeDispatcher
{
  public abstract void execOpenApplicationDetailsSettings();
  
  public abstract void execUrlScheme(String paramString);
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQUrlSchemeDispatcher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */