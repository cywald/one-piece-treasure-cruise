package jp.co.drecom.bisque.lib;

public class NilPointerException
  extends RuntimeException
{
  public NilPointerException() {}
  
  public NilPointerException(String paramString)
  {
    super(paramString);
  }
  
  public NilPointerException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
  
  public NilPointerException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\NilPointerException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */