package jp.co.drecom.bisque.lib;

public class BQHandler
{
  public static native void nativeCallbackFuncForHandlerPost(long paramLong1, long paramLong2);
  
  public void handlerPost(long paramLong1, long paramLong2)
  {
    nativeCallbackFuncForHandlerPost(paramLong1, paramLong2);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */