package jp.co.drecom.bisque.lib;

public class IndependenceCrasher
{
  static IndependenceCrasher _instance;
  private IndependenceHandler _target = null;
  
  public IndependenceCrasher(IndependenceHandler paramIndependenceHandler)
  {
    this._target = paramIndependenceHandler;
    initInstance();
  }
  
  public static void crash(String paramString)
    throws Exception
  {
    if (_instance == null) {
      throw new IndependenceException(paramString);
    }
    _instance.crashInternal(paramString);
  }
  
  public static void init(IndependenceHandler paramIndependenceHandler)
  {
    _instance = new IndependenceCrasher(paramIndependenceHandler);
  }
  
  void crashInternal(String paramString)
    throws Exception
  {
    this._target.independence(paramString);
  }
  
  void initInstance() {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\IndependenceCrasher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */