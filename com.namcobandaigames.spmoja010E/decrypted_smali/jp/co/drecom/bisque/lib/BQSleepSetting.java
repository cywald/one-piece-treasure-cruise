package jp.co.drecom.bisque.lib;

import android.app.Activity;
import android.content.Context;
import android.view.Window;

public class BQSleepSetting
{
  private Activity parent;
  
  public BQSleepSetting(Context paramContext)
  {
    this.parent = ((Activity)paramContext);
  }
  
  public void setDeviceSleep(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.parent.getWindow().clearFlags(128);
      return;
    }
    this.parent.getWindow().addFlags(128);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQSleepSetting.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */