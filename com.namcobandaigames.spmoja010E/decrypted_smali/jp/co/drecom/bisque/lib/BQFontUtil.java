package jp.co.drecom.bisque.lib;

import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Typeface;
import android.util.Log;
import java.io.UnsupportedEncodingException;

public class BQFontUtil
{
  private static TextProperty computeTextProperty(String paramString, float paramFloat, Paint paramPaint)
  {
    Paint.FontMetricsInt localFontMetricsInt = paramPaint.getFontMetricsInt();
    int j = (int)Math.ceil(localFontMetricsInt.bottom - localFontMetricsInt.top);
    int i = paramString.length();
    for (;;)
    {
      float f = paramFloat;
      if (i > 0)
      {
        f = (float)Math.ceil(paramPaint.measureText(paramString, 0, i));
        if (f >= paramFloat) {
          break label86;
        }
      }
      try
      {
        i = paramString.substring(0, i).getBytes("UTF-8").length;
        return new TextProperty((int)f, j, i);
        label86:
        i -= 1;
      }
      catch (UnsupportedEncodingException paramString)
      {
        for (;;)
        {
          i = 0;
        }
      }
    }
  }
  
  public static TextProperty getDrawSize(String paramString1, String paramString2, float paramFloat1, float paramFloat2)
  {
    return computeTextProperty(paramString1, paramFloat2, newPaint(paramString2, paramFloat1));
  }
  
  private static Paint newPaint(String paramString, float paramFloat)
  {
    Paint localPaint = new Paint();
    localPaint.setColor(-1);
    localPaint.setTextSize(paramFloat);
    localPaint.setAntiAlias(true);
    localPaint.setHinting(1);
    localPaint.setSubpixelText(true);
    localPaint.setTextAlign(Paint.Align.LEFT);
    if (paramString.endsWith(".ttf")) {
      try
      {
        localPaint.setTypeface(BQTypefaces.get(paramString));
        return localPaint;
      }
      catch (Exception localException)
      {
        Log.e("BQFontUtil", "error to create ttf type face: " + paramString);
        localPaint.setTypeface(Typeface.create(paramString, 0));
        return localPaint;
      }
    }
    localPaint.setTypeface(Typeface.create(paramString, 0));
    return localPaint;
  }
  
  static class TextProperty
  {
    private final int m_height;
    private final int m_stringLength;
    private final int m_width;
    
    TextProperty(int paramInt1, int paramInt2, int paramInt3)
    {
      this.m_width = paramInt1;
      this.m_height = paramInt2;
      this.m_stringLength = paramInt3;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\jp\co\drecom\bisque\lib\BQFontUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */