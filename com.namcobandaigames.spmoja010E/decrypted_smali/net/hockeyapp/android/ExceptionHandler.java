package net.hockeyapp.android;

import android.os.Process;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.UUID;

public class ExceptionHandler
  implements Thread.UncaughtExceptionHandler
{
  private Thread.UncaughtExceptionHandler defaultExceptionHandler;
  private boolean ignoreDefaultHandler = false;
  private CrashManagerListener listener;
  
  public ExceptionHandler(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler, CrashManagerListener paramCrashManagerListener, boolean paramBoolean)
  {
    this.defaultExceptionHandler = paramUncaughtExceptionHandler;
    this.ignoreDefaultHandler = paramBoolean;
    this.listener = paramCrashManagerListener;
  }
  
  private static String limitedString(String paramString)
  {
    String str = paramString;
    if (paramString != null)
    {
      str = paramString;
      if (paramString.length() > 255) {
        str = paramString.substring(0, 255);
      }
    }
    return str;
  }
  
  public static void saveException(Throwable paramThrowable, CrashManagerListener paramCrashManagerListener)
  {
    Date localDate = new Date();
    StringWriter localStringWriter = new StringWriter();
    paramThrowable.printStackTrace(new PrintWriter(localStringWriter));
    try
    {
      paramThrowable = UUID.randomUUID().toString();
      Object localObject = Constants.FILES_PATH + "/" + paramThrowable + ".stacktrace";
      Log.d("HockeyApp", "Writing unhandled exception to: " + (String)localObject);
      localObject = new BufferedWriter(new FileWriter((String)localObject));
      ((BufferedWriter)localObject).write("Package: " + Constants.APP_PACKAGE + "\n");
      ((BufferedWriter)localObject).write("Version Code: " + Constants.APP_VERSION + "\n");
      ((BufferedWriter)localObject).write("Version Name: " + Constants.APP_VERSION_NAME + "\n");
      if ((paramCrashManagerListener == null) || (paramCrashManagerListener.includeDeviceData()))
      {
        ((BufferedWriter)localObject).write("Android: " + Constants.ANDROID_VERSION + "\n");
        ((BufferedWriter)localObject).write("Manufacturer: " + Constants.PHONE_MANUFACTURER + "\n");
        ((BufferedWriter)localObject).write("Model: " + Constants.PHONE_MODEL + "\n");
      }
      if ((Constants.CRASH_IDENTIFIER != null) && ((paramCrashManagerListener == null) || (paramCrashManagerListener.includeDeviceIdentifier()))) {
        ((BufferedWriter)localObject).write("CrashReporter Key: " + Constants.CRASH_IDENTIFIER + "\n");
      }
      ((BufferedWriter)localObject).write("Date: " + localDate + "\n");
      ((BufferedWriter)localObject).write("\n");
      ((BufferedWriter)localObject).write(localStringWriter.toString());
      ((BufferedWriter)localObject).flush();
      ((BufferedWriter)localObject).close();
      if (paramCrashManagerListener != null)
      {
        writeValueToFile(limitedString(paramCrashManagerListener.getUserID()), paramThrowable + ".user");
        writeValueToFile(limitedString(paramCrashManagerListener.getContact()), paramThrowable + ".contact");
        writeValueToFile(paramCrashManagerListener.getDescription(), paramThrowable + ".description");
      }
      return;
    }
    catch (Exception paramThrowable)
    {
      Log.e("HockeyApp", "Error saving exception stacktrace!\n", paramThrowable);
    }
  }
  
  private static void writeValueToFile(String paramString1, String paramString2)
  {
    try
    {
      paramString2 = Constants.FILES_PATH + "/" + paramString2;
      if (paramString1.trim().length() > 0)
      {
        paramString2 = new BufferedWriter(new FileWriter(paramString2));
        paramString2.write(paramString1);
        paramString2.flush();
        paramString2.close();
      }
      return;
    }
    catch (Exception paramString1) {}
  }
  
  public void setListener(CrashManagerListener paramCrashManagerListener)
  {
    this.listener = paramCrashManagerListener;
  }
  
  public void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    if (Constants.FILES_PATH == null)
    {
      this.defaultExceptionHandler.uncaughtException(paramThread, paramThrowable);
      return;
    }
    saveException(paramThrowable, this.listener);
    if (!this.ignoreDefaultHandler)
    {
      this.defaultExceptionHandler.uncaughtException(paramThread, paramThrowable);
      return;
    }
    Process.killProcess(Process.myPid());
    System.exit(10);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\ExceptionHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */