package net.hockeyapp.android;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;

public class LocaleManager
{
  public static void initialize(Context paramContext)
  {
    loadFromResources("hockeyapp_crash_dialog_title", 0, paramContext);
    loadFromResources("hockeyapp_crash_dialog_message", 1, paramContext);
    loadFromResources("hockeyapp_crash_dialog_negative_button", 2, paramContext);
    loadFromResources("hockeyapp_crash_dialog_positive_button", 4, paramContext);
    loadFromResources("hockeyapp_download_failed_dialog_title", 256, paramContext);
    loadFromResources("hockeyapp_download_failed_dialog_message", 257, paramContext);
    loadFromResources("hockeyapp_download_failed_dialog_negative_button", 258, paramContext);
    loadFromResources("hockeyapp_download_failed_dialog_positive_button", 259, paramContext);
    loadFromResources("hockeyapp_update_mandatory_toast", 512, paramContext);
    loadFromResources("hockeyapp_update_dialog_title", 513, paramContext);
    loadFromResources("hockeyapp_update_dialog_message", 514, paramContext);
    loadFromResources("hockeyapp_update_dialog_negative_button", 515, paramContext);
    loadFromResources("hockeyapp_update_dialog_positive_button", 516, paramContext);
    loadFromResources("hockeyapp_expiry_info_title", 768, paramContext);
    loadFromResources("hockeyapp_expiry_info_text", 769, paramContext);
    loadFromResources("hockeyapp_feedback_failed_title", 1024, paramContext);
    loadFromResources("hockeyapp_feedback_failed_text", 1025, paramContext);
    loadFromResources("hockeyapp_feedback_name_hint", 1026, paramContext);
    loadFromResources("hockeyapp_feedback_email_hint", 1027, paramContext);
    loadFromResources("hockeyapp_feedback_subject_hint", 1028, paramContext);
    loadFromResources("hockeyapp_feedback_message_hint", 1029, paramContext);
    loadFromResources("hockeyapp_feedback_last_updated_text", 1030, paramContext);
    loadFromResources("hockeyapp_feedback_attachment_button_text", 1031, paramContext);
    loadFromResources("hockeyapp_feedback_send_button_text", 1032, paramContext);
    loadFromResources("hockeyapp_feedback_response_button_text", 1033, paramContext);
    loadFromResources("hockeyapp_feedback_refresh_button_text", 1034, paramContext);
    loadFromResources("hockeyapp_feedback_title", 1035, paramContext);
    loadFromResources("hockeyapp_feedback_send_generic_error", 1036, paramContext);
    loadFromResources("hockeyapp_feedback_send_network_error", 1037, paramContext);
    loadFromResources("hockeyapp_feedback_validate_subject_error", 1038, paramContext);
    loadFromResources("hockeyapp_feedback_validate_email_error", 1039, paramContext);
    loadFromResources("hockeyapp_feedback_validate_email_empty", 1042, paramContext);
    loadFromResources("hockeyapp_feedback_validate_name_error", 1041, paramContext);
    loadFromResources("hockeyapp_feedback_validate_text_error", 1043, paramContext);
    loadFromResources("hockeyapp_feedback_generic_error", 1040, paramContext);
    loadFromResources("hockeyapp_login_headline_text", 1280, paramContext);
    loadFromResources("hockeyapp_login_missing_credentials_toast", 1281, paramContext);
    loadFromResources("hockeyapp_login_email_hint", 1282, paramContext);
    loadFromResources("hockeyapp_login_password_hint", 1283, paramContext);
    loadFromResources("hockeyapp_login_login_button_text", 1284, paramContext);
    loadFromResources("hockeyapp_paint_indicator_toast", 1536, paramContext);
    loadFromResources("hockeyapp_paint_menu_save", 1537, paramContext);
    loadFromResources("hockeyapp_paint_menu_undo", 1538, paramContext);
    loadFromResources("hockeyapp_paint_menu_clear", 1539, paramContext);
    loadFromResources("hockeyapp_paint_dialog_message", 1540, paramContext);
    loadFromResources("hockeyapp_paint_dialog_negative_button", 1541, paramContext);
    loadFromResources("hockeyapp_paint_dialog_positive_button", 1542, paramContext);
  }
  
  private static void loadFromResources(String paramString, int paramInt, Context paramContext)
  {
    int i = paramContext.getResources().getIdentifier(paramString, "string", paramContext.getPackageName());
    if (i == 0) {}
    do
    {
      return;
      paramString = paramContext.getString(i);
    } while (TextUtils.isEmpty(paramString));
    Strings.set(paramInt, paramString);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\LocaleManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */