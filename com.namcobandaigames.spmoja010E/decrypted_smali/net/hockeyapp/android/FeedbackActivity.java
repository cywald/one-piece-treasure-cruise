package net.hockeyapp.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import net.hockeyapp.android.adapters.MessagesAdapter;
import net.hockeyapp.android.objects.ErrorObject;
import net.hockeyapp.android.objects.Feedback;
import net.hockeyapp.android.objects.FeedbackMessage;
import net.hockeyapp.android.objects.FeedbackResponse;
import net.hockeyapp.android.tasks.ParseFeedbackTask;
import net.hockeyapp.android.tasks.SendFeedbackTask;
import net.hockeyapp.android.utils.AsyncTaskUtils;
import net.hockeyapp.android.utils.PrefsUtil;
import net.hockeyapp.android.utils.Util;
import net.hockeyapp.android.views.AttachmentListView;
import net.hockeyapp.android.views.AttachmentView;
import net.hockeyapp.android.views.FeedbackView;

public class FeedbackActivity
  extends Activity
  implements FeedbackActivityInterface, View.OnClickListener
{
  private static final int MAX_ATTACHMENTS_PER_MSG = 3;
  private final int ATTACH_FILE = 2;
  private final int ATTACH_PICTURE = 1;
  private final int DIALOG_ERROR_ID = 0;
  private final int PAINT_IMAGE = 3;
  private Button addAttachmentButton;
  private Button addResponseButton;
  private Context context;
  private EditText emailInput;
  private ErrorObject error;
  private Handler feedbackHandler;
  private ArrayList<FeedbackMessage> feedbackMessages;
  private ScrollView feedbackScrollView;
  private boolean feedbackViewInitialized;
  private boolean inSendFeedback;
  private TextView lastUpdatedTextView;
  private MessagesAdapter messagesAdapter;
  private ListView messagesListView;
  private EditText nameInput;
  private Handler parseFeedbackHandler;
  private ParseFeedbackTask parseFeedbackTask;
  private Button refreshButton;
  private Button sendFeedbackButton;
  private SendFeedbackTask sendFeedbackTask;
  private EditText subjectInput;
  private EditText textInput;
  private String token;
  private String url;
  private LinearLayout wrapperLayoutFeedbackAndMessages;
  
  private boolean addAttachment(int paramInt)
  {
    Intent localIntent;
    if (paramInt == 2)
    {
      localIntent = new Intent();
      localIntent.setType("*/*");
      localIntent.setAction("android.intent.action.GET_CONTENT");
      startActivityForResult(Intent.createChooser(localIntent, "Select File"), 2);
      return true;
    }
    if (paramInt == 1)
    {
      localIntent = new Intent();
      localIntent.setType("image/*");
      localIntent.setAction("android.intent.action.GET_CONTENT");
      startActivityForResult(Intent.createChooser(localIntent, "Select Picture"), 1);
      return true;
    }
    return false;
  }
  
  private void configureAppropriateView()
  {
    this.token = PrefsUtil.getInstance().getFeedbackTokenFromPrefs(this);
    if ((this.token == null) || (this.inSendFeedback))
    {
      configureFeedbackView(false);
      return;
    }
    configureFeedbackView(true);
    sendFetchFeedback(this.url, null, null, null, null, null, this.token, this.feedbackHandler, true);
  }
  
  private void createParseFeedbackTask(String paramString1, String paramString2)
  {
    this.parseFeedbackTask = new ParseFeedbackTask(this, paramString1, this.parseFeedbackHandler, paramString2);
  }
  
  private void hideKeyboard()
  {
    if (this.textInput != null) {
      ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(this.textInput.getWindowToken(), 0);
    }
  }
  
  private void initFeedbackHandler()
  {
    this.feedbackHandler = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        int i = 0;
        FeedbackActivity.access$002(FeedbackActivity.this, new ErrorObject());
        Object localObject;
        String str;
        if ((paramAnonymousMessage != null) && (paramAnonymousMessage.getData() != null))
        {
          localObject = paramAnonymousMessage.getData();
          paramAnonymousMessage = ((Bundle)localObject).getString("feedback_response");
          str = ((Bundle)localObject).getString("feedback_status");
          localObject = ((Bundle)localObject).getString("request_type");
          if ((((String)localObject).equals("send")) && ((paramAnonymousMessage == null) || (Integer.parseInt(str) != 201))) {
            FeedbackActivity.this.error.setMessage(Strings.get(1036));
          }
        }
        for (;;)
        {
          if (i == 0) {
            FeedbackActivity.this.runOnUiThread(new Runnable()
            {
              public void run()
              {
                FeedbackActivity.this.enableDisableSendFeedbackButton(true);
                FeedbackActivity.this.showDialog(0);
              }
            });
          }
          return;
          if ((((String)localObject).equals("fetch")) && (str != null) && ((Integer.parseInt(str) == 404) || (Integer.parseInt(str) == 422)))
          {
            FeedbackActivity.this.resetFeedbackView();
            i = 1;
          }
          else if (paramAnonymousMessage != null)
          {
            FeedbackActivity.this.startParseFeedbackTask(paramAnonymousMessage, (String)localObject);
            i = 1;
          }
          else
          {
            FeedbackActivity.this.error.setMessage(Strings.get(1037));
            continue;
            FeedbackActivity.this.error.setMessage(Strings.get(1036));
          }
        }
      }
    };
  }
  
  private void initParseFeedbackHandler()
  {
    this.parseFeedbackHandler = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        int j = 0;
        FeedbackActivity.access$002(FeedbackActivity.this, new ErrorObject());
        int i = j;
        if (paramAnonymousMessage != null)
        {
          i = j;
          if (paramAnonymousMessage.getData() != null)
          {
            paramAnonymousMessage = (FeedbackResponse)paramAnonymousMessage.getData().getSerializable("parse_feedback_response");
            i = j;
            if (paramAnonymousMessage != null)
            {
              if (!paramAnonymousMessage.getStatus().equalsIgnoreCase("success")) {
                break label138;
              }
              j = 1;
              i = j;
              if (paramAnonymousMessage.getToken() != null)
              {
                PrefsUtil.getInstance().saveFeedbackTokenToPrefs(FeedbackActivity.this.context, paramAnonymousMessage.getToken());
                FeedbackActivity.this.loadFeedbackMessages(paramAnonymousMessage);
                FeedbackActivity.access$502(FeedbackActivity.this, false);
              }
            }
          }
        }
        label138:
        for (i = j;; i = 0)
        {
          if (i == 0) {
            FeedbackActivity.this.runOnUiThread(new Runnable()
            {
              public void run()
              {
                FeedbackActivity.this.showDialog(0);
              }
            });
          }
          FeedbackActivity.this.enableDisableSendFeedbackButton(true);
          return;
        }
      }
    };
  }
  
  private void loadFeedbackMessages(final FeedbackResponse paramFeedbackResponse)
  {
    runOnUiThread(new Runnable()
    {
      public void run()
      {
        FeedbackActivity.this.configureFeedbackView(true);
        Object localObject = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("d MMM h:mm a");
        if ((paramFeedbackResponse != null) && (paramFeedbackResponse.getFeedback() != null) && (paramFeedbackResponse.getFeedback().getMessages() != null) && (paramFeedbackResponse.getFeedback().getMessages().size() > 0))
        {
          FeedbackActivity.access$602(FeedbackActivity.this, paramFeedbackResponse.getFeedback().getMessages());
          Collections.reverse(FeedbackActivity.this.feedbackMessages);
        }
        try
        {
          localObject = ((SimpleDateFormat)localObject).parse(((FeedbackMessage)FeedbackActivity.this.feedbackMessages.get(0)).getCreatedAt());
          FeedbackActivity.this.lastUpdatedTextView.setText(String.format("Last Updated: %s", new Object[] { localSimpleDateFormat.format((Date)localObject) }));
          if (FeedbackActivity.this.messagesAdapter == null)
          {
            FeedbackActivity.access$802(FeedbackActivity.this, new MessagesAdapter(FeedbackActivity.this.context, FeedbackActivity.this.feedbackMessages));
            FeedbackActivity.this.messagesListView.setAdapter(FeedbackActivity.this.messagesAdapter);
            return;
          }
        }
        catch (ParseException localParseException)
        {
          for (;;)
          {
            localParseException.printStackTrace();
            continue;
            FeedbackActivity.this.messagesAdapter.clear();
            Iterator localIterator = FeedbackActivity.this.feedbackMessages.iterator();
            while (localIterator.hasNext())
            {
              localObject = (FeedbackMessage)localIterator.next();
              FeedbackActivity.this.messagesAdapter.add((FeedbackMessage)localObject);
            }
            FeedbackActivity.this.messagesAdapter.notifyDataSetChanged();
          }
        }
      }
    });
  }
  
  private void resetFeedbackView()
  {
    runOnUiThread(new Runnable()
    {
      public void run()
      {
        PrefsUtil.getInstance().saveFeedbackTokenToPrefs(FeedbackActivity.this, null);
        PrefsUtil.applyChanges(FeedbackActivity.this.getSharedPreferences("net.hockeyapp.android.feedback", 0).edit().remove("idLastMessageSend").remove("idLastMessageProcessed"));
        FeedbackActivity.this.configureFeedbackView(false);
      }
    });
  }
  
  private void sendFeedback()
  {
    enableDisableSendFeedbackButton(false);
    hideKeyboard();
    String str1 = PrefsUtil.getInstance().getFeedbackTokenFromPrefs(this.context);
    String str2 = this.nameInput.getText().toString().trim();
    String str3 = this.emailInput.getText().toString().trim();
    String str4 = this.subjectInput.getText().toString().trim();
    String str5 = this.textInput.getText().toString().trim();
    if (TextUtils.isEmpty(str4))
    {
      this.subjectInput.setVisibility(0);
      setError(this.subjectInput, 1038);
      return;
    }
    if (TextUtils.isEmpty(str2))
    {
      setError(this.nameInput, 1041);
      return;
    }
    if (TextUtils.isEmpty(str3))
    {
      setError(this.emailInput, 1042);
      return;
    }
    if (TextUtils.isEmpty(str5))
    {
      setError(this.textInput, 1043);
      return;
    }
    if (!Util.isValidEmail(str3))
    {
      setError(this.emailInput, 1039);
      return;
    }
    PrefsUtil.getInstance().saveNameEmailSubjectToPrefs(this.context, str2, str3, str4);
    ArrayList localArrayList = ((AttachmentListView)findViewById(8209)).getAttachments();
    sendFetchFeedback(this.url, str2, str3, str4, str5, localArrayList, str1, this.feedbackHandler, false);
  }
  
  private void sendFetchFeedback(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, List<Uri> paramList, String paramString6, Handler paramHandler, boolean paramBoolean)
  {
    this.sendFeedbackTask = new SendFeedbackTask(this.context, paramString1, paramString2, paramString3, paramString4, paramString5, paramList, paramString6, paramHandler, paramBoolean);
    AsyncTaskUtils.execute(this.sendFeedbackTask);
  }
  
  private void setError(EditText paramEditText, int paramInt)
  {
    paramEditText.setError(Strings.get(paramInt));
    enableDisableSendFeedbackButton(true);
  }
  
  private void startParseFeedbackTask(String paramString1, String paramString2)
  {
    createParseFeedbackTask(paramString1, paramString2);
    AsyncTaskUtils.execute(this.parseFeedbackTask);
  }
  
  protected void configureFeedbackView(boolean paramBoolean)
  {
    this.feedbackScrollView = ((ScrollView)findViewById(131095));
    this.wrapperLayoutFeedbackAndMessages = ((LinearLayout)findViewById(131093));
    this.messagesListView = ((ListView)findViewById(131094));
    if (paramBoolean)
    {
      this.wrapperLayoutFeedbackAndMessages.setVisibility(0);
      this.feedbackScrollView.setVisibility(8);
      this.lastUpdatedTextView = ((TextView)findViewById(8192));
      this.addResponseButton = ((Button)findViewById(131088));
      this.addResponseButton.setOnClickListener(this);
      this.refreshButton = ((Button)findViewById(131089));
      this.refreshButton.setOnClickListener(this);
      return;
    }
    this.wrapperLayoutFeedbackAndMessages.setVisibility(8);
    this.feedbackScrollView.setVisibility(0);
    this.nameInput = ((EditText)findViewById(8194));
    this.emailInput = ((EditText)findViewById(8196));
    this.subjectInput = ((EditText)findViewById(8198));
    this.textInput = ((EditText)findViewById(8200));
    if (!this.feedbackViewInitialized)
    {
      Object localObject = PrefsUtil.getInstance().getNameEmailFromPrefs(this.context);
      if (localObject == null) {
        break label393;
      }
      localObject = ((String)localObject).split("\\|");
      if ((localObject != null) && (localObject.length >= 2))
      {
        this.nameInput.setText(localObject[0]);
        this.emailInput.setText(localObject[1]);
        if (localObject.length >= 3)
        {
          this.subjectInput.setText(localObject[2]);
          this.textInput.requestFocus();
        }
      }
      else
      {
        this.feedbackViewInitialized = true;
      }
    }
    else
    {
      this.textInput.setText("");
      if (PrefsUtil.getInstance().getFeedbackTokenFromPrefs(this.context) == null) {
        break label434;
      }
      this.subjectInput.setVisibility(8);
    }
    for (;;)
    {
      ((ViewGroup)findViewById(8209)).removeAllViews();
      this.addAttachmentButton = ((Button)findViewById(8208));
      this.addAttachmentButton.setOnClickListener(this);
      registerForContextMenu(this.addAttachmentButton);
      this.sendFeedbackButton = ((Button)findViewById(8201));
      this.sendFeedbackButton.setOnClickListener(this);
      return;
      this.subjectInput.requestFocus();
      break;
      label393:
      this.nameInput.setText("");
      this.emailInput.setText("");
      this.subjectInput.setText("");
      this.nameInput.requestFocus();
      break;
      label434:
      this.subjectInput.setVisibility(0);
    }
  }
  
  public void enableDisableSendFeedbackButton(boolean paramBoolean)
  {
    if (this.sendFeedbackButton != null) {
      this.sendFeedbackButton.setEnabled(paramBoolean);
    }
  }
  
  public ViewGroup getLayoutView()
  {
    return new FeedbackView(this);
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt2 != -1) {}
    do
    {
      do
      {
        do
        {
          do
          {
            return;
            if (paramInt1 != 2) {
              break;
            }
            paramIntent = paramIntent.getData();
          } while (paramIntent == null);
          localObject = (ViewGroup)findViewById(8209);
          ((ViewGroup)localObject).addView(new AttachmentView(this, (ViewGroup)localObject, paramIntent, true));
          return;
          if (paramInt1 != 1) {
            break;
          }
          paramIntent = paramIntent.getData();
        } while (paramIntent == null);
        try
        {
          localObject = new Intent(this, PaintActivity.class);
          ((Intent)localObject).putExtra("imageUri", paramIntent);
          startActivityForResult((Intent)localObject, 3);
          return;
        }
        catch (ActivityNotFoundException paramIntent)
        {
          Log.e("HockeyApp", "Paint activity not declared!", paramIntent);
          return;
        }
      } while (paramInt1 != 3);
      paramIntent = (Uri)paramIntent.getParcelableExtra("imageUri");
    } while (paramIntent == null);
    Object localObject = (ViewGroup)findViewById(8209);
    ((ViewGroup)localObject).addView(new AttachmentView(this, (ViewGroup)localObject, paramIntent, true));
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default: 
      return;
    case 8201: 
      sendFeedback();
      return;
    case 8208: 
      if (((ViewGroup)findViewById(8209)).getChildCount() >= 3)
      {
        Toast.makeText(this, "Only 3 attachments allowed.", 1000).show();
        return;
      }
      openContextMenu(paramView);
      return;
    case 131088: 
      configureFeedbackView(false);
      this.inSendFeedback = true;
      return;
    }
    sendFetchFeedback(this.url, null, null, null, null, null, PrefsUtil.getInstance().getFeedbackTokenFromPrefs(this.context), this.feedbackHandler, true);
  }
  
  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onContextItemSelected(paramMenuItem);
    }
    return addAttachment(paramMenuItem.getItemId());
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(getLayoutView());
    setTitle(Strings.get(1035));
    this.context = this;
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null) {
      this.url = localBundle.getString("url");
    }
    if (paramBundle != null)
    {
      this.feedbackViewInitialized = paramBundle.getBoolean("feedbackViewInitialized");
      this.inSendFeedback = paramBundle.getBoolean("inSendFeedback");
    }
    for (;;)
    {
      ((NotificationManager)getSystemService("notification")).cancel(2);
      initFeedbackHandler();
      initParseFeedbackHandler();
      configureAppropriateView();
      return;
      this.inSendFeedback = false;
      this.feedbackViewInitialized = false;
    }
  }
  
  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    super.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
    paramContextMenu.add(0, 2, 0, "Attach File");
    paramContextMenu.add(0, 1, 0, "Attach Picture");
  }
  
  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    }
    new AlertDialog.Builder(this).setMessage("An error has occured").setCancelable(false).setTitle("Error").setIcon(17301543).setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        FeedbackActivity.access$002(FeedbackActivity.this, null);
        paramAnonymousDialogInterface.cancel();
      }
    }).create();
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      if (this.inSendFeedback)
      {
        this.inSendFeedback = false;
        configureAppropriateView();
      }
      for (;;)
      {
        return true;
        finish();
      }
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  protected void onPrepareDialog(int paramInt, Dialog paramDialog)
  {
    switch (paramInt)
    {
    default: 
      return;
    }
    paramDialog = (AlertDialog)paramDialog;
    if (this.error != null)
    {
      paramDialog.setMessage(this.error.getMessage());
      return;
    }
    paramDialog.setMessage(Strings.get(1040));
  }
  
  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      ViewGroup localViewGroup = (ViewGroup)findViewById(8209);
      Iterator localIterator = paramBundle.getParcelableArrayList("attachments").iterator();
      while (localIterator.hasNext()) {
        localViewGroup.addView(new AttachmentView(this, localViewGroup, (Uri)localIterator.next(), true));
      }
      this.feedbackViewInitialized = paramBundle.getBoolean("feedbackViewInitialized");
    }
    super.onRestoreInstanceState(paramBundle);
  }
  
  public Object onRetainNonConfigurationInstance()
  {
    if (this.sendFeedbackTask != null) {
      this.sendFeedbackTask.detach();
    }
    return this.sendFeedbackTask;
  }
  
  protected void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putParcelableArrayList("attachments", ((AttachmentListView)findViewById(8209)).getAttachments());
    paramBundle.putBoolean("feedbackViewInitialized", this.feedbackViewInitialized);
    paramBundle.putBoolean("inSendFeedback", this.inSendFeedback);
    super.onSaveInstanceState(paramBundle);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\FeedbackActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */