package net.hockeyapp.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import net.hockeyapp.android.views.ExpiryInfoView;

public class ExpiryInfoActivity
  extends Activity
{
  protected View getLayoutView()
  {
    return new ExpiryInfoView(this, getStringResource(769));
  }
  
  protected String getStringResource(int paramInt)
  {
    return Strings.get(UpdateManager.getLastListener(), paramInt);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setTitle(getStringResource(768));
    setContentView(getLayoutView());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\ExpiryInfoActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */