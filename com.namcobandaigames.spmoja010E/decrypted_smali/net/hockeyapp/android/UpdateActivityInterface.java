package net.hockeyapp.android;

import android.view.View;

public abstract interface UpdateActivityInterface
{
  public abstract View getLayoutView();
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\UpdateActivityInterface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */