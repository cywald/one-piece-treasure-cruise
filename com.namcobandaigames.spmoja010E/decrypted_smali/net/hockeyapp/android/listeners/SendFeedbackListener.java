package net.hockeyapp.android.listeners;

import net.hockeyapp.android.StringListener;
import net.hockeyapp.android.tasks.SendFeedbackTask;

public abstract class SendFeedbackListener
  extends StringListener
{
  public void feedbackFailed(SendFeedbackTask paramSendFeedbackTask, Boolean paramBoolean) {}
  
  public void feedbackSuccessful(SendFeedbackTask paramSendFeedbackTask) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\listeners\SendFeedbackListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */