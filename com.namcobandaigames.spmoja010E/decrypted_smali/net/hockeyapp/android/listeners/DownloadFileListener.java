package net.hockeyapp.android.listeners;

import net.hockeyapp.android.StringListener;
import net.hockeyapp.android.tasks.DownloadFileTask;

public abstract class DownloadFileListener
  extends StringListener
{
  public void downloadFailed(DownloadFileTask paramDownloadFileTask, Boolean paramBoolean) {}
  
  public void downloadSuccessful(DownloadFileTask paramDownloadFileTask) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\listeners\DownloadFileListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */