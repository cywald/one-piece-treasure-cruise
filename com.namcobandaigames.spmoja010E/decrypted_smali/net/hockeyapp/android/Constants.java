package net.hockeyapp.android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.util.Log;
import java.io.File;
import java.lang.reflect.Field;
import java.security.MessageDigest;

public class Constants
{
  public static String ANDROID_VERSION;
  public static String APP_PACKAGE;
  public static String APP_VERSION;
  public static String APP_VERSION_NAME;
  public static final String BASE_URL = "https://sdk.hockeyapp.net/";
  public static String CRASH_IDENTIFIER = null;
  public static String FILES_PATH = null;
  public static String PHONE_MANUFACTURER;
  public static String PHONE_MODEL;
  public static final String SDK_NAME = "HockeySDK";
  public static final String SDK_VERSION = "3.5.0";
  public static final String TAG = "HockeyApp";
  
  static
  {
    APP_VERSION = null;
    APP_VERSION_NAME = null;
    APP_PACKAGE = null;
    ANDROID_VERSION = null;
    PHONE_MODEL = null;
    PHONE_MANUFACTURER = null;
  }
  
  private static String bytesToHex(byte[] paramArrayOfByte)
  {
    char[] arrayOfChar1 = "0123456789ABCDEF".toCharArray();
    char[] arrayOfChar2 = new char[paramArrayOfByte.length * 2];
    int i = 0;
    while (i < paramArrayOfByte.length)
    {
      int j = paramArrayOfByte[i] & 0xFF;
      arrayOfChar2[(i * 2)] = arrayOfChar1[(j >>> 4)];
      arrayOfChar2[(i * 2 + 1)] = arrayOfChar1[(j & 0xF)];
      i += 1;
    }
    return new String(arrayOfChar2).replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5");
  }
  
  private static String createSalt(Context paramContext)
  {
    String str2 = "HA" + Build.BOARD.length() % 10 + Build.BRAND.length() % 10 + Build.CPU_ABI.length() % 10 + Build.PRODUCT.length() % 10;
    String str1 = "";
    paramContext = str1;
    if (Build.VERSION.SDK_INT >= 9) {}
    try
    {
      paramContext = Build.class.getField("SERIAL").get(null).toString();
      return str2 + ":" + paramContext;
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        paramContext = str1;
      }
    }
  }
  
  public static File getHockeyAppStorageDir()
  {
    File localFile = Environment.getExternalStorageDirectory();
    localFile = new File(localFile.getAbsolutePath() + "/" + "HockeyApp");
    localFile.mkdirs();
    return localFile;
  }
  
  private static int loadBuildNumber(Context paramContext, PackageManager paramPackageManager)
  {
    int i = 0;
    try
    {
      paramContext = paramPackageManager.getApplicationInfo(paramContext.getPackageName(), 128).metaData;
      if (paramContext != null) {
        i = paramContext.getInt("buildNumber", 0);
      }
      return i;
    }
    catch (Exception paramContext)
    {
      Log.e("HockeyApp", "Exception thrown when accessing the application info:");
      paramContext.printStackTrace();
    }
    return 0;
  }
  
  private static void loadCrashIdentifier(Context paramContext)
  {
    Object localObject = Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
    if ((APP_PACKAGE != null) && (localObject != null)) {
      localObject = APP_PACKAGE + ":" + (String)localObject + ":" + createSalt(paramContext);
    }
    try
    {
      paramContext = MessageDigest.getInstance("SHA-1");
      localObject = ((String)localObject).getBytes("UTF-8");
      paramContext.update((byte[])localObject, 0, localObject.length);
      CRASH_IDENTIFIER = bytesToHex(paramContext.digest());
      return;
    }
    catch (Throwable paramContext) {}
  }
  
  private static void loadFilesPath(Context paramContext)
  {
    if (paramContext != null) {}
    try
    {
      paramContext = paramContext.getFilesDir();
      if (paramContext != null) {
        FILES_PATH = paramContext.getAbsolutePath();
      }
      return;
    }
    catch (Exception paramContext)
    {
      Log.e("HockeyApp", "Exception thrown when accessing the files dir:");
      paramContext.printStackTrace();
    }
  }
  
  public static void loadFromContext(Context paramContext)
  {
    ANDROID_VERSION = Build.VERSION.RELEASE;
    PHONE_MODEL = Build.MODEL;
    PHONE_MANUFACTURER = Build.MANUFACTURER;
    loadFilesPath(paramContext);
    loadPackageData(paramContext);
    loadCrashIdentifier(paramContext);
  }
  
  private static void loadPackageData(Context paramContext)
  {
    if (paramContext != null) {}
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      PackageInfo localPackageInfo = localPackageManager.getPackageInfo(paramContext.getPackageName(), 0);
      APP_PACKAGE = localPackageInfo.packageName;
      APP_VERSION = "" + localPackageInfo.versionCode;
      APP_VERSION_NAME = localPackageInfo.versionName;
      int i = loadBuildNumber(paramContext, localPackageManager);
      if ((i != 0) && (i > localPackageInfo.versionCode)) {
        APP_VERSION = "" + i;
      }
      return;
    }
    catch (Exception paramContext)
    {
      Log.e("HockeyApp", "Exception thrown when accessing the package info:");
      paramContext.printStackTrace();
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\Constants.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */