package net.hockeyapp.android;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;
import java.io.File;
import java.io.FilenameFilter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.hockeyapp.android.utils.ConnectionManager;
import net.hockeyapp.android.utils.PrefsUtil;
import net.hockeyapp.android.utils.Util;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class CrashManager
{
  private static final String ALWAYS_SEND_KEY = "always_send_crash_reports";
  private static String identifier = null;
  private static boolean submitting = false;
  private static String urlString = null;
  
  /* Error */
  private static String contentsOfFile(WeakReference<Context> paramWeakReference, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnull +148 -> 149
    //   4: aload_0
    //   5: invokevirtual 57	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   8: checkcast 59	android/content/Context
    //   11: astore 5
    //   13: aload 5
    //   15: ifnull +134 -> 149
    //   18: new 61	java/lang/StringBuilder
    //   21: dup
    //   22: invokespecial 62	java/lang/StringBuilder:<init>	()V
    //   25: astore 4
    //   27: aconst_null
    //   28: astore_3
    //   29: aconst_null
    //   30: astore_0
    //   31: aconst_null
    //   32: astore_2
    //   33: new 64	java/io/BufferedReader
    //   36: dup
    //   37: new 66	java/io/InputStreamReader
    //   40: dup
    //   41: aload 5
    //   43: aload_1
    //   44: invokevirtual 70	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   47: invokespecial 73	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   50: invokespecial 76	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   53: astore_1
    //   54: aload_1
    //   55: invokevirtual 80	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   58: astore_0
    //   59: aload_0
    //   60: ifnull +39 -> 99
    //   63: aload 4
    //   65: aload_0
    //   66: invokevirtual 84	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: pop
    //   70: aload 4
    //   72: ldc 86
    //   74: invokestatic 92	java/lang/System:getProperty	(Ljava/lang/String;)Ljava/lang/String;
    //   77: invokevirtual 84	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   80: pop
    //   81: goto -27 -> 54
    //   84: astore_0
    //   85: aload_1
    //   86: ifnull +7 -> 93
    //   89: aload_1
    //   90: invokevirtual 95	java/io/BufferedReader:close	()V
    //   93: aload 4
    //   95: invokevirtual 98	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   98: areturn
    //   99: aload_1
    //   100: ifnull +77 -> 177
    //   103: aload_1
    //   104: invokevirtual 95	java/io/BufferedReader:close	()V
    //   107: goto -14 -> 93
    //   110: astore_0
    //   111: goto -18 -> 93
    //   114: astore_2
    //   115: aload_3
    //   116: astore_1
    //   117: aload_1
    //   118: astore_0
    //   119: aload_2
    //   120: invokevirtual 101	java/io/IOException:printStackTrace	()V
    //   123: aload_1
    //   124: ifnull -31 -> 93
    //   127: aload_1
    //   128: invokevirtual 95	java/io/BufferedReader:close	()V
    //   131: goto -38 -> 93
    //   134: astore_0
    //   135: goto -42 -> 93
    //   138: astore_1
    //   139: aload_0
    //   140: ifnull +7 -> 147
    //   143: aload_0
    //   144: invokevirtual 95	java/io/BufferedReader:close	()V
    //   147: aload_1
    //   148: athrow
    //   149: aconst_null
    //   150: areturn
    //   151: astore_0
    //   152: goto -59 -> 93
    //   155: astore_0
    //   156: goto -9 -> 147
    //   159: astore_2
    //   160: aload_1
    //   161: astore_0
    //   162: aload_2
    //   163: astore_1
    //   164: goto -25 -> 139
    //   167: astore_2
    //   168: goto -51 -> 117
    //   171: astore_0
    //   172: aload_2
    //   173: astore_1
    //   174: goto -89 -> 85
    //   177: goto -84 -> 93
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	180	0	paramWeakReference	WeakReference<Context>
    //   0	180	1	paramString	String
    //   32	1	2	localObject1	Object
    //   114	6	2	localIOException1	java.io.IOException
    //   159	4	2	localObject2	Object
    //   167	6	2	localIOException2	java.io.IOException
    //   28	88	3	localObject3	Object
    //   25	69	4	localStringBuilder	StringBuilder
    //   11	31	5	localContext	Context
    // Exception table:
    //   from	to	target	type
    //   54	59	84	java/io/FileNotFoundException
    //   63	81	84	java/io/FileNotFoundException
    //   103	107	110	java/io/IOException
    //   33	54	114	java/io/IOException
    //   127	131	134	java/io/IOException
    //   33	54	138	finally
    //   119	123	138	finally
    //   89	93	151	java/io/IOException
    //   143	147	155	java/io/IOException
    //   54	59	159	finally
    //   63	81	159	finally
    //   54	59	167	java/io/IOException
    //   63	81	167	java/io/IOException
    //   33	54	171	java/io/FileNotFoundException
  }
  
  private static void deleteStackTrace(WeakReference<Context> paramWeakReference, String paramString)
  {
    if (paramWeakReference != null)
    {
      paramWeakReference = (Context)paramWeakReference.get();
      if (paramWeakReference != null)
      {
        paramWeakReference.deleteFile(paramString);
        paramWeakReference.deleteFile(paramString.replace(".stacktrace", ".user"));
        paramWeakReference.deleteFile(paramString.replace(".stacktrace", ".contact"));
        paramWeakReference.deleteFile(paramString.replace(".stacktrace", ".description"));
      }
    }
  }
  
  public static void deleteStackTraces(WeakReference<Context> paramWeakReference)
  {
    String[] arrayOfString = searchForStackTraces();
    if ((arrayOfString != null) && (arrayOfString.length > 0))
    {
      Log.d("HockeyApp", "Found " + arrayOfString.length + " stacktrace(s).");
      int i = 0;
      while (i < arrayOfString.length)
      {
        if (paramWeakReference != null) {}
        try
        {
          Log.d("HockeyApp", "Delete stacktrace " + arrayOfString[i] + ".");
          deleteStackTrace(paramWeakReference, arrayOfString[i]);
          Context localContext = (Context)paramWeakReference.get();
          if (localContext != null) {
            localContext.deleteFile(arrayOfString[i]);
          }
        }
        catch (Exception localException)
        {
          for (;;)
          {
            localException.printStackTrace();
          }
        }
        i += 1;
      }
    }
  }
  
  public static void execute(Context paramContext, CrashManagerListener paramCrashManagerListener)
  {
    if ((paramCrashManagerListener != null) && (paramCrashManagerListener.ignoreDefaultHandler())) {}
    Boolean localBoolean2;
    WeakReference localWeakReference;
    int i;
    for (boolean bool = true;; bool = false)
    {
      localBoolean2 = Boolean.valueOf(bool);
      localWeakReference = new WeakReference(paramContext);
      i = hasStackTraces(localWeakReference);
      if (i != 1) {
        break label139;
      }
      paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext);
      Boolean localBoolean1 = Boolean.valueOf(Boolean.valueOf(false).booleanValue() | paramContext.getBoolean("always_send_crash_reports", false));
      paramContext = localBoolean1;
      if (paramCrashManagerListener != null)
      {
        paramContext = Boolean.valueOf(Boolean.valueOf(localBoolean1.booleanValue() | paramCrashManagerListener.shouldAutoUploadCrashes()).booleanValue() | paramCrashManagerListener.onCrashesFound());
        paramCrashManagerListener.onNewCrashesFound();
      }
      if (paramContext.booleanValue()) {
        break;
      }
      showDialog(localWeakReference, paramCrashManagerListener, localBoolean2.booleanValue());
      return;
    }
    sendCrashes(localWeakReference, paramCrashManagerListener, localBoolean2.booleanValue());
    return;
    label139:
    if (i == 2)
    {
      if (paramCrashManagerListener != null) {
        paramCrashManagerListener.onConfirmedCrashesFound();
      }
      sendCrashes(localWeakReference, paramCrashManagerListener, localBoolean2.booleanValue());
      return;
    }
    registerHandler(localWeakReference, paramCrashManagerListener, localBoolean2.booleanValue());
  }
  
  private static String getURLString()
  {
    return urlString + "api/2/apps/" + identifier + "/crashes/";
  }
  
  public static int hasStackTraces(WeakReference<Context> paramWeakReference)
  {
    String[] arrayOfString = searchForStackTraces();
    localObject2 = null;
    int j = 0;
    int i = j;
    if (arrayOfString != null)
    {
      i = j;
      if (arrayOfString.length > 0)
      {
        localObject1 = localObject2;
        if (paramWeakReference == null) {}
      }
    }
    try
    {
      paramWeakReference = (Context)paramWeakReference.get();
      localObject1 = localObject2;
      if (paramWeakReference != null) {
        localObject1 = Arrays.asList(paramWeakReference.getSharedPreferences("HockeySDK", 0).getString("ConfirmedFilenames", "").split("\\|"));
      }
    }
    catch (Exception paramWeakReference)
    {
      for (;;)
      {
        int k;
        int m;
        localObject1 = localObject2;
      }
    }
    if (localObject1 != null)
    {
      k = 2;
      m = arrayOfString.length;
      j = 0;
      for (;;)
      {
        i = k;
        if (j < m)
        {
          if (!((List)localObject1).contains(arrayOfString[j])) {
            i = 1;
          }
        }
        else {
          return i;
        }
        j += 1;
      }
    }
    return 1;
  }
  
  public static void initialize(Context paramContext, String paramString1, String paramString2, CrashManagerListener paramCrashManagerListener)
  {
    initialize(paramContext, paramString1, paramString2, paramCrashManagerListener, true);
  }
  
  private static void initialize(Context paramContext, String paramString1, String paramString2, CrashManagerListener paramCrashManagerListener, boolean paramBoolean)
  {
    if (paramContext != null)
    {
      urlString = paramString1;
      identifier = Util.sanitizeAppIdentifier(paramString2);
      Constants.loadFromContext(paramContext);
      if (identifier == null) {
        identifier = Constants.APP_PACKAGE;
      }
      if (paramBoolean) {
        if ((paramCrashManagerListener == null) || (!paramCrashManagerListener.ignoreDefaultHandler())) {
          break label71;
        }
      }
    }
    label71:
    for (paramBoolean = true;; paramBoolean = false)
    {
      registerHandler(new WeakReference(paramContext), paramCrashManagerListener, Boolean.valueOf(paramBoolean).booleanValue());
      return;
    }
  }
  
  public static void initialize(Context paramContext, String paramString, CrashManagerListener paramCrashManagerListener)
  {
    initialize(paramContext, "https://sdk.hockeyapp.net/", paramString, paramCrashManagerListener, true);
  }
  
  private static String joinArray(String[] paramArrayOfString, String paramString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int i = 0;
    while (i < paramArrayOfString.length)
    {
      localStringBuffer.append(paramArrayOfString[i]);
      if (i < paramArrayOfString.length - 1) {
        localStringBuffer.append(paramString);
      }
      i += 1;
    }
    return localStringBuffer.toString();
  }
  
  public static void register(Context paramContext, String paramString)
  {
    register(paramContext, "https://sdk.hockeyapp.net/", paramString, null);
  }
  
  public static void register(Context paramContext, String paramString1, String paramString2, CrashManagerListener paramCrashManagerListener)
  {
    initialize(paramContext, paramString1, paramString2, paramCrashManagerListener, false);
    execute(paramContext, paramCrashManagerListener);
  }
  
  public static void register(Context paramContext, String paramString, CrashManagerListener paramCrashManagerListener)
  {
    register(paramContext, "https://sdk.hockeyapp.net/", paramString, paramCrashManagerListener);
  }
  
  private static void registerHandler(WeakReference<Context> paramWeakReference, CrashManagerListener paramCrashManagerListener, boolean paramBoolean)
  {
    if ((Constants.APP_VERSION != null) && (Constants.APP_PACKAGE != null))
    {
      paramWeakReference = Thread.getDefaultUncaughtExceptionHandler();
      if (paramWeakReference != null) {
        Log.d("HockeyApp", "Current handler class = " + paramWeakReference.getClass().getName());
      }
      if ((paramWeakReference instanceof ExceptionHandler))
      {
        ((ExceptionHandler)paramWeakReference).setListener(paramCrashManagerListener);
        return;
      }
      Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(paramWeakReference, paramCrashManagerListener, paramBoolean));
      return;
    }
    Log.d("HockeyApp", "Exception handler not set because version or package is null.");
  }
  
  private static void saveConfirmedStackTraces(WeakReference<Context> paramWeakReference)
  {
    Object localObject;
    if (paramWeakReference != null)
    {
      localObject = (Context)paramWeakReference.get();
      if (localObject == null) {}
    }
    try
    {
      paramWeakReference = searchForStackTraces();
      localObject = ((Context)localObject).getSharedPreferences("HockeySDK", 0).edit();
      ((SharedPreferences.Editor)localObject).putString("ConfirmedFilenames", joinArray(paramWeakReference, "|"));
      PrefsUtil.applyChanges((SharedPreferences.Editor)localObject);
      return;
    }
    catch (Exception paramWeakReference) {}
  }
  
  private static String[] searchForStackTraces()
  {
    if (Constants.FILES_PATH != null)
    {
      Log.d("HockeyApp", "Looking for exceptions in: " + Constants.FILES_PATH);
      File localFile = new File(Constants.FILES_PATH + "/");
      if ((!localFile.mkdir()) && (!localFile.exists())) {
        return new String[0];
      }
      localFile.list(new FilenameFilter()
      {
        public boolean accept(File paramAnonymousFile, String paramAnonymousString)
        {
          return paramAnonymousString.endsWith(".stacktrace");
        }
      });
    }
    Log.d("HockeyApp", "Can't search for exception as file path is null.");
    return null;
  }
  
  private static void sendCrashes(WeakReference<Context> paramWeakReference, final CrashManagerListener paramCrashManagerListener, boolean paramBoolean)
  {
    saveConfirmedStackTraces(paramWeakReference);
    registerHandler(paramWeakReference, paramCrashManagerListener, paramBoolean);
    if (!submitting)
    {
      submitting = true;
      new Thread()
      {
        public void run()
        {
          CrashManager.submitStackTraces(this.val$weakContext, paramCrashManagerListener);
          CrashManager.access$202(false);
        }
      }.start();
    }
  }
  
  private static void showDialog(final WeakReference<Context> paramWeakReference, final CrashManagerListener paramCrashManagerListener, final boolean paramBoolean)
  {
    Object localObject = null;
    if (paramWeakReference != null) {
      localObject = (Context)paramWeakReference.get();
    }
    if (localObject == null) {
      return;
    }
    localObject = new AlertDialog.Builder((Context)localObject);
    ((AlertDialog.Builder)localObject).setTitle(Strings.get(paramCrashManagerListener, 0));
    ((AlertDialog.Builder)localObject).setMessage(Strings.get(paramCrashManagerListener, 1));
    ((AlertDialog.Builder)localObject).setNegativeButton(Strings.get(paramCrashManagerListener, 2), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        if (this.val$listener != null) {
          this.val$listener.onUserDeniedCrashes();
        }
        CrashManager.deleteStackTraces(paramWeakReference);
        CrashManager.registerHandler(paramWeakReference, this.val$listener, paramBoolean);
      }
    });
    ((AlertDialog.Builder)localObject).setNeutralButton(Strings.get(paramCrashManagerListener, 3), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface = null;
        if (this.val$weakContext != null) {
          paramAnonymousDialogInterface = (Context)this.val$weakContext.get();
        }
        if (paramAnonymousDialogInterface == null) {
          return;
        }
        PreferenceManager.getDefaultSharedPreferences(paramAnonymousDialogInterface).edit().putBoolean("always_send_crash_reports", true).commit();
        CrashManager.sendCrashes(this.val$weakContext, paramCrashManagerListener, paramBoolean);
      }
    });
    ((AlertDialog.Builder)localObject).setPositiveButton(Strings.get(paramCrashManagerListener, 4), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        CrashManager.sendCrashes(this.val$weakContext, paramCrashManagerListener, paramBoolean);
      }
    });
    ((AlertDialog.Builder)localObject).create().show();
  }
  
  public static void submitStackTraces(WeakReference<Context> paramWeakReference, CrashManagerListener paramCrashManagerListener)
  {
    String[] arrayOfString = searchForStackTraces();
    Object localObject1 = Boolean.valueOf(false);
    int i;
    Object localObject4;
    if ((arrayOfString != null) && (arrayOfString.length > 0))
    {
      Log.d("HockeyApp", "Found " + arrayOfString.length + " stacktrace(s).");
      i = 0;
      if (i < arrayOfString.length)
      {
        localObject4 = arrayOfString[i];
        for (;;)
        {
          try
          {
            String str = contentsOfFile(paramWeakReference, (String)localObject4);
            localObject2 = localObject1;
            if (str.length() > 0)
            {
              Log.d("HockeyApp", "Transmitting crash data: \n" + str);
              localObject2 = (DefaultHttpClient)ConnectionManager.getInstance().getHttpClient();
              HttpPost localHttpPost = new HttpPost(getURLString());
              ArrayList localArrayList = new ArrayList();
              localArrayList.add(new BasicNameValuePair("raw", str));
              localArrayList.add(new BasicNameValuePair("userID", contentsOfFile(paramWeakReference, ((String)localObject4).replace(".stacktrace", ".user"))));
              localArrayList.add(new BasicNameValuePair("contact", contentsOfFile(paramWeakReference, ((String)localObject4).replace(".stacktrace", ".contact"))));
              localArrayList.add(new BasicNameValuePair("description", contentsOfFile(paramWeakReference, ((String)localObject4).replace(".stacktrace", ".description"))));
              localArrayList.add(new BasicNameValuePair("sdk", "HockeySDK"));
              localArrayList.add(new BasicNameValuePair("sdk_version", "3.5.0"));
              localHttpPost.setEntity(new UrlEncodedFormEntity(localArrayList, "UTF-8"));
              ((DefaultHttpClient)localObject2).execute(localHttpPost);
              localObject2 = Boolean.valueOf(true);
            }
            if (!((Boolean)localObject2).booleanValue()) {
              continue;
            }
            Log.d("HockeyApp", "Transmission succeeded");
            deleteStackTrace(paramWeakReference, arrayOfString[i]);
            localObject4 = localObject2;
            if (paramCrashManagerListener != null)
            {
              paramCrashManagerListener.onCrashesSent();
              localObject4 = localObject2;
            }
          }
          catch (Exception localException)
          {
            Object localObject2;
            localException.printStackTrace();
            if (!((Boolean)localObject1).booleanValue()) {
              continue;
            }
            Log.d("HockeyApp", "Transmission succeeded");
            deleteStackTrace(paramWeakReference, arrayOfString[i]);
            localObject4 = localObject1;
            if (paramCrashManagerListener == null) {
              continue;
            }
            paramCrashManagerListener.onCrashesSent();
            localObject4 = localObject1;
            continue;
            Log.d("HockeyApp", "Transmission failed, will retry on next register() call");
            localObject4 = localObject1;
            if (paramCrashManagerListener == null) {
              continue;
            }
            paramCrashManagerListener.onCrashesNotSent();
            localObject4 = localObject1;
            continue;
          }
          finally
          {
            if (!((Boolean)localObject1).booleanValue()) {
              continue;
            }
            Log.d("HockeyApp", "Transmission succeeded");
            deleteStackTrace(paramWeakReference, arrayOfString[i]);
            if (paramCrashManagerListener == null) {
              continue;
            }
            paramCrashManagerListener.onCrashesSent();
            throw ((Throwable)localObject3);
            Log.d("HockeyApp", "Transmission failed, will retry on next register() call");
            if (paramCrashManagerListener == null) {
              continue;
            }
            paramCrashManagerListener.onCrashesNotSent();
            continue;
          }
          i += 1;
          localObject1 = localObject4;
          break;
          Log.d("HockeyApp", "Transmission failed, will retry on next register() call");
          localObject4 = localObject2;
          if (paramCrashManagerListener != null)
          {
            paramCrashManagerListener.onCrashesNotSent();
            localObject4 = localObject2;
          }
        }
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\CrashManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */