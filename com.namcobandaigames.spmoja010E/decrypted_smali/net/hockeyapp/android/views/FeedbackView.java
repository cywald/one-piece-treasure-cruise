package net.hockeyapp.android.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build.VERSION;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import net.hockeyapp.android.Strings;

public class FeedbackView
  extends LinearLayout
{
  public static final int ADD_ATTACHMENT_BUTTON_ID = 8208;
  public static final int ADD_RESPONSE_BUTTON_ID = 131088;
  public static final int EMAIL_EDIT_TEXT_ID = 8196;
  public static final int FEEDBACK_SCROLLVIEW_ID = 131095;
  public static final int LAST_UPDATED_TEXT_VIEW_ID = 8192;
  public static final int MESSAGES_LISTVIEW_ID = 131094;
  public static final int NAME_EDIT_TEXT_ID = 8194;
  public static final int REFRESH_BUTTON_ID = 131089;
  public static final int SEND_FEEDBACK_BUTTON_ID = 8201;
  public static final int SUBJECT_EDIT_TEXT_ID = 8198;
  public static final int TEXT_EDIT_TEXT_ID = 8200;
  public static final int WRAPPER_BASE_ID = 131090;
  public static final int WRAPPER_LAYOUT_ATTACHMENTS = 8209;
  public static final int WRAPPER_LAYOUT_BUTTONS_ID = 131092;
  public static final int WRAPPER_LAYOUT_FEEDBACK_AND_MESSAGES_ID = 131093;
  public static final int WRAPPER_LAYOUT_FEEDBACK_ID = 131091;
  private ScrollView feedbackScrollView;
  private ListView messagesListView;
  private LinearLayout wrapperBase;
  private ViewGroup wrapperLayoutAttachments;
  private LinearLayout wrapperLayoutButtons;
  private LinearLayout wrapperLayoutFeedback;
  private LinearLayout wrapperLayoutFeedbackAndMessages;
  
  public FeedbackView(Context paramContext)
  {
    super(paramContext);
    loadLayoutParams(paramContext);
    loadWrapperBase(paramContext);
    loadFeedbackScrollView(paramContext);
    loadWrapperLayoutFeedback(paramContext);
    loadWrapperLayoutFeedbackAndMessages(paramContext);
    loadNameInput(paramContext);
    loadEmailInput(paramContext);
    loadSubjectInput(paramContext);
    loadTextInput(paramContext);
    loadAttachmentList(paramContext);
    loadAddAttachmentButton(paramContext);
    loadSendFeedbackButton(paramContext);
    loadLastUpdatedLabel(paramContext);
    loadWrapperLayoutButtons(paramContext);
    loadAddResponseButton(paramContext);
    loadRefreshButton(paramContext);
    loadMessagesListView(paramContext);
  }
  
  private Drawable getButtonSelector()
  {
    StateListDrawable localStateListDrawable = new StateListDrawable();
    ColorDrawable localColorDrawable = new ColorDrawable(-16777216);
    localStateListDrawable.addState(new int[] { -16842919 }, localColorDrawable);
    localColorDrawable = new ColorDrawable(-12303292);
    localStateListDrawable.addState(new int[] { -16842919, 16842908 }, localColorDrawable);
    localColorDrawable = new ColorDrawable(-7829368);
    localStateListDrawable.addState(new int[] { 16842919 }, localColorDrawable);
    return localStateListDrawable;
  }
  
  private Drawable getEditTextBackground(Context paramContext)
  {
    int i = (int)(paramContext.getResources().getDisplayMetrics().density * 10.0F);
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new RectShape());
    Paint localPaint = localShapeDrawable.getPaint();
    localPaint.setColor(-1);
    localPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    localPaint.setStrokeWidth(1.0F);
    localShapeDrawable.setPadding(i, i, i, i);
    i = (int)(paramContext.getResources().getDisplayMetrics().density * 1.5D);
    paramContext = new ShapeDrawable(new RectShape());
    localPaint = paramContext.getPaint();
    localPaint.setColor(-12303292);
    localPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    localPaint.setStrokeWidth(1.0F);
    paramContext.setPadding(0, 0, 0, i);
    return new LayerDrawable(new Drawable[] { paramContext, localShapeDrawable });
  }
  
  private void loadAddAttachmentButton(Context paramContext)
  {
    paramContext = new Button(paramContext);
    paramContext.setId(8208);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    int j = (int)TypedValue.applyDimension(1, 30.0F, getResources().getDisplayMetrics());
    int k = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, k);
    localLayoutParams.gravity = 1;
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setBackgroundDrawable(getButtonSelector());
    paramContext.setPadding(j, i, j, i);
    paramContext.setText(Strings.get(1031));
    paramContext.setTextColor(-1);
    paramContext.setTextSize(2, 15.0F);
    this.wrapperLayoutFeedback.addView(paramContext);
  }
  
  private void loadAddResponseButton(Context paramContext)
  {
    paramContext = new Button(paramContext);
    paramContext.setId(131088);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    int j = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    int k = (int)TypedValue.applyDimension(1, 5.0F, getResources().getDisplayMetrics());
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, k, j);
    localLayoutParams.gravity = 1;
    localLayoutParams.weight = 1.0F;
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setBackgroundDrawable(getButtonSelector());
    paramContext.setPadding(0, i, 0, i);
    paramContext.setGravity(17);
    paramContext.setText(Strings.get(1033));
    paramContext.setTextColor(-1);
    paramContext.setTextSize(2, 15.0F);
    this.wrapperLayoutButtons.addView(paramContext);
  }
  
  private void loadAttachmentList(Context paramContext)
  {
    this.wrapperLayoutAttachments = new AttachmentListView(paramContext);
    this.wrapperLayoutAttachments.setId(8209);
    paramContext = new LinearLayout.LayoutParams(-1, -1);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    paramContext.gravity = 3;
    this.wrapperLayoutAttachments.setLayoutParams(paramContext);
    this.wrapperLayoutAttachments.setPadding(0, 0, 0, i);
    this.wrapperLayoutFeedback.addView(this.wrapperLayoutAttachments);
  }
  
  private void loadEmailInput(Context paramContext)
  {
    EditText localEditText = new EditText(paramContext);
    localEditText.setId(8196);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics()));
    localEditText.setLayoutParams(localLayoutParams);
    localEditText.setImeOptions(5);
    localEditText.setInputType(33);
    localEditText.setSingleLine(true);
    localEditText.setTextColor(-7829368);
    localEditText.setTextSize(2, 15.0F);
    localEditText.setTypeface(null, 0);
    localEditText.setHint(Strings.get(1027));
    localEditText.setHintTextColor(-3355444);
    setEditTextBackground(paramContext, localEditText);
    this.wrapperLayoutFeedback.addView(localEditText);
  }
  
  private void loadFeedbackScrollView(Context paramContext)
  {
    this.feedbackScrollView = new ScrollView(paramContext);
    this.feedbackScrollView.setId(131095);
    paramContext = new LinearLayout.LayoutParams(-1, -1);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    paramContext.gravity = 17;
    this.feedbackScrollView.setLayoutParams(paramContext);
    this.feedbackScrollView.setPadding(i, 0, i, 0);
    this.wrapperBase.addView(this.feedbackScrollView);
  }
  
  private void loadLastUpdatedLabel(Context paramContext)
  {
    paramContext = new TextView(paramContext);
    paramContext.setId(8192);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-2, -2);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    localLayoutParams.setMargins(0, 0, 0, 0);
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setPadding(0, i, 0, i);
    paramContext.setEllipsize(TextUtils.TruncateAt.END);
    paramContext.setShadowLayer(1.0F, 0.0F, 1.0F, -1);
    paramContext.setSingleLine(true);
    paramContext.setText(Strings.get(1030));
    paramContext.setTextColor(-7829368);
    paramContext.setTextSize(2, 15.0F);
    paramContext.setTypeface(null, 0);
    this.wrapperLayoutFeedbackAndMessages.addView(paramContext);
  }
  
  private void loadLayoutParams(Context paramContext)
  {
    paramContext = new LinearLayout.LayoutParams(-1, -2);
    setBackgroundColor(-1);
    setLayoutParams(paramContext);
  }
  
  private void loadMessagesListView(Context paramContext)
  {
    this.messagesListView = new ListView(paramContext);
    this.messagesListView.setId(131094);
    paramContext = new LinearLayout.LayoutParams(-1, -1);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    this.messagesListView.setLayoutParams(paramContext);
    this.messagesListView.setPadding(0, i, 0, i);
    this.wrapperLayoutFeedbackAndMessages.addView(this.messagesListView);
  }
  
  private void loadNameInput(Context paramContext)
  {
    EditText localEditText = new EditText(paramContext);
    localEditText.setId(8194);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    localLayoutParams.setMargins(0, i / 2, 0, i);
    localEditText.setLayoutParams(localLayoutParams);
    localEditText.setImeOptions(5);
    localEditText.setInputType(16385);
    localEditText.setSingleLine(true);
    localEditText.setTextColor(-7829368);
    localEditText.setTextSize(2, 15.0F);
    localEditText.setTypeface(null, 0);
    localEditText.setHint(Strings.get(1026));
    localEditText.setHintTextColor(-3355444);
    setEditTextBackground(paramContext, localEditText);
    this.wrapperLayoutFeedback.addView(localEditText);
  }
  
  private void loadRefreshButton(Context paramContext)
  {
    paramContext = new Button(paramContext);
    paramContext.setId(131089);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    int j = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    int k = (int)TypedValue.applyDimension(1, 5.0F, getResources().getDisplayMetrics());
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(k, 0, 0, j);
    localLayoutParams.gravity = 1;
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setBackgroundDrawable(getButtonSelector());
    paramContext.setPadding(0, i, 0, i);
    paramContext.setGravity(17);
    paramContext.setText(Strings.get(1034));
    paramContext.setTextColor(-1);
    paramContext.setTextSize(2, 15.0F);
    localLayoutParams.weight = 1.0F;
    this.wrapperLayoutButtons.addView(paramContext);
  }
  
  private void loadSendFeedbackButton(Context paramContext)
  {
    paramContext = new Button(paramContext);
    paramContext.setId(8201);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    int j = (int)TypedValue.applyDimension(1, 30.0F, getResources().getDisplayMetrics());
    int k = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, k);
    localLayoutParams.gravity = 1;
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setBackgroundDrawable(getButtonSelector());
    paramContext.setPadding(j, i, j, i);
    paramContext.setText(Strings.get(1032));
    paramContext.setTextColor(-1);
    paramContext.setTextSize(2, 15.0F);
    this.wrapperLayoutFeedback.addView(paramContext);
  }
  
  private void loadSubjectInput(Context paramContext)
  {
    EditText localEditText = new EditText(paramContext);
    localEditText.setId(8198);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics()));
    localEditText.setLayoutParams(localLayoutParams);
    localEditText.setImeOptions(5);
    localEditText.setInputType(16433);
    localEditText.setSingleLine(true);
    localEditText.setTextColor(-7829368);
    localEditText.setTextSize(2, 15.0F);
    localEditText.setTypeface(null, 0);
    localEditText.setHint(Strings.get(1028));
    localEditText.setHintTextColor(-3355444);
    setEditTextBackground(paramContext, localEditText);
    this.wrapperLayoutFeedback.addView(localEditText);
  }
  
  private void loadTextInput(Context paramContext)
  {
    EditText localEditText = new EditText(paramContext);
    localEditText.setId(8200);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    int j = (int)TypedValue.applyDimension(1, 100.0F, getResources().getDisplayMetrics());
    localLayoutParams.setMargins(0, 0, 0, i);
    localEditText.setLayoutParams(localLayoutParams);
    localEditText.setImeOptions(5);
    localEditText.setInputType(16385);
    localEditText.setSingleLine(false);
    localEditText.setTextColor(-7829368);
    localEditText.setTextSize(2, 15.0F);
    localEditText.setTypeface(null, 0);
    localEditText.setMinimumHeight(j);
    localEditText.setHint(Strings.get(1029));
    localEditText.setHintTextColor(-3355444);
    setEditTextBackground(paramContext, localEditText);
    this.wrapperLayoutFeedback.addView(localEditText);
  }
  
  private void loadWrapperBase(Context paramContext)
  {
    this.wrapperBase = new LinearLayout(paramContext);
    this.wrapperBase.setId(131090);
    paramContext = new LinearLayout.LayoutParams(-1, -1);
    paramContext.gravity = 49;
    this.wrapperBase.setLayoutParams(paramContext);
    this.wrapperBase.setPadding(0, 0, 0, 0);
    this.wrapperBase.setOrientation(1);
    addView(this.wrapperBase);
  }
  
  private void loadWrapperLayoutButtons(Context paramContext)
  {
    this.wrapperLayoutButtons = new LinearLayout(paramContext);
    this.wrapperLayoutButtons.setId(131092);
    paramContext = new LinearLayout.LayoutParams(-1, -2);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    paramContext.gravity = 3;
    this.wrapperLayoutButtons.setLayoutParams(paramContext);
    this.wrapperLayoutButtons.setPadding(0, i, 0, i);
    this.wrapperLayoutButtons.setGravity(48);
    this.wrapperLayoutButtons.setOrientation(0);
    this.wrapperLayoutFeedbackAndMessages.addView(this.wrapperLayoutButtons);
  }
  
  private void loadWrapperLayoutFeedback(Context paramContext)
  {
    this.wrapperLayoutFeedback = new LinearLayout(paramContext);
    this.wrapperLayoutFeedback.setId(131091);
    paramContext = new LinearLayout.LayoutParams(-1, -1);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    paramContext.gravity = 3;
    this.wrapperLayoutFeedback.setLayoutParams(paramContext);
    this.wrapperLayoutFeedback.setPadding(i, i, i, i);
    this.wrapperLayoutFeedback.setGravity(48);
    this.wrapperLayoutFeedback.setOrientation(1);
    this.feedbackScrollView.addView(this.wrapperLayoutFeedback);
  }
  
  private void loadWrapperLayoutFeedbackAndMessages(Context paramContext)
  {
    this.wrapperLayoutFeedbackAndMessages = new LinearLayout(paramContext);
    this.wrapperLayoutFeedbackAndMessages.setId(131093);
    paramContext = new LinearLayout.LayoutParams(-1, -1);
    int i = (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics());
    paramContext.gravity = 17;
    this.wrapperLayoutFeedbackAndMessages.setLayoutParams(paramContext);
    this.wrapperLayoutFeedbackAndMessages.setPadding(i, i, i, 0);
    this.wrapperLayoutFeedbackAndMessages.setGravity(48);
    this.wrapperLayoutFeedbackAndMessages.setOrientation(1);
    this.wrapperBase.addView(this.wrapperLayoutFeedbackAndMessages);
  }
  
  private void setEditTextBackground(Context paramContext, EditText paramEditText)
  {
    if (Build.VERSION.SDK_INT < 11) {
      paramEditText.setBackgroundDrawable(getEditTextBackground(paramContext));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\views\FeedbackView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */