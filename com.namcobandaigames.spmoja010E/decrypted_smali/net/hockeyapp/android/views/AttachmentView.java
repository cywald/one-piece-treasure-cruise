package net.hockeyapp.android.views;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;
import net.hockeyapp.android.Constants;
import net.hockeyapp.android.objects.FeedbackAttachment;
import net.hockeyapp.android.utils.ImageUtils;

public class AttachmentView
  extends FrameLayout
{
  private static final int IMAGES_PER_ROW_LANDSCAPE = 2;
  private static final int IMAGES_PER_ROW_PORTRAIT = 3;
  private final FeedbackAttachment attachment;
  private final Uri attachmentUri;
  private final Context context;
  private final String filename;
  private int gap;
  private ImageView imageView;
  private int maxHeightLandscape;
  private int maxHeightPortrait;
  private int orientation;
  private final ViewGroup parent;
  private TextView textView;
  private int widthLandscape;
  private int widthPortrait;
  
  public AttachmentView(Context paramContext, ViewGroup paramViewGroup, Uri paramUri, boolean paramBoolean)
  {
    super(paramContext);
    this.context = paramContext;
    this.parent = paramViewGroup;
    this.attachment = null;
    this.attachmentUri = paramUri;
    this.filename = paramUri.getLastPathSegment();
    calculateDimensions(20);
    initializeView(paramContext, paramBoolean);
    this.textView.setText(this.filename);
    new AsyncTask()
    {
      protected Bitmap doInBackground(Void... paramAnonymousVarArgs)
      {
        return AttachmentView.this.loadImageThumbnail();
      }
      
      protected void onPostExecute(Bitmap paramAnonymousBitmap)
      {
        if (paramAnonymousBitmap != null)
        {
          AttachmentView.this.configureViewForThumbnail(paramAnonymousBitmap, false);
          return;
        }
        AttachmentView.this.configureViewForPlaceholder(false);
      }
    }.execute(new Void[0]);
  }
  
  public AttachmentView(Context paramContext, ViewGroup paramViewGroup, FeedbackAttachment paramFeedbackAttachment, boolean paramBoolean)
  {
    super(paramContext);
    this.context = paramContext;
    this.parent = paramViewGroup;
    this.attachment = paramFeedbackAttachment;
    this.attachmentUri = Uri.fromFile(new File(Constants.getHockeyAppStorageDir(), paramFeedbackAttachment.getCacheId()));
    this.filename = paramFeedbackAttachment.getFilename();
    calculateDimensions(30);
    initializeView(paramContext, paramBoolean);
    this.orientation = 0;
    this.textView.setText("Loading...");
    configureViewForPlaceholder(false);
  }
  
  private void calculateDimensions(int paramInt)
  {
    DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
    this.gap = Math.round(TypedValue.applyDimension(1, 10.0F, localDisplayMetrics));
    paramInt = Math.round(TypedValue.applyDimension(1, paramInt, localDisplayMetrics));
    int i = localDisplayMetrics.widthPixels;
    int j = this.gap;
    int k = this.gap;
    this.widthPortrait = ((i - paramInt * 2 - j * 2) / 3);
    this.widthLandscape = ((i - paramInt * 2 - k * 1) / 2);
    this.maxHeightPortrait = (this.widthPortrait * 2);
    this.maxHeightLandscape = this.widthLandscape;
  }
  
  private void configureViewForPlaceholder(final boolean paramBoolean)
  {
    this.textView.setMaxWidth(this.widthPortrait);
    this.textView.setMinWidth(this.widthPortrait);
    this.imageView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
    this.imageView.setAdjustViewBounds(false);
    this.imageView.setBackgroundColor(Color.parseColor("#eeeeee"));
    this.imageView.setMinimumHeight((int)(this.widthPortrait * 1.2F));
    this.imageView.setMinimumWidth(this.widthPortrait);
    this.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
    this.imageView.setImageDrawable(getSystemIcon("ic_menu_attachment"));
    this.imageView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (!paramBoolean) {
          return;
        }
        paramAnonymousView = new Intent();
        paramAnonymousView.setAction("android.intent.action.VIEW");
        paramAnonymousView.setDataAndType(AttachmentView.this.attachmentUri, "*/*");
        AttachmentView.this.context.startActivity(paramAnonymousView);
      }
    });
  }
  
  private void configureViewForThumbnail(Bitmap paramBitmap, final boolean paramBoolean)
  {
    int i;
    if (this.orientation == 1)
    {
      i = this.widthLandscape;
      if (this.orientation != 1) {
        break label137;
      }
    }
    label137:
    for (int j = this.maxHeightLandscape;; j = this.maxHeightPortrait)
    {
      this.textView.setMaxWidth(i);
      this.textView.setMinWidth(i);
      this.imageView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
      this.imageView.setAdjustViewBounds(true);
      this.imageView.setMinimumWidth(i);
      this.imageView.setMaxWidth(i);
      this.imageView.setMaxHeight(j);
      this.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
      this.imageView.setImageBitmap(paramBitmap);
      this.imageView.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if (!paramBoolean) {
            return;
          }
          paramAnonymousView = new Intent();
          paramAnonymousView.setAction("android.intent.action.VIEW");
          paramAnonymousView.setDataAndType(AttachmentView.this.attachmentUri, "image/*");
          AttachmentView.this.context.startActivity(paramAnonymousView);
        }
      });
      return;
      i = this.widthPortrait;
      break;
    }
  }
  
  private Drawable getSystemIcon(String paramString)
  {
    return getResources().getDrawable(getResources().getIdentifier(paramString, "drawable", "android"));
  }
  
  private void initializeView(Context paramContext, boolean paramBoolean)
  {
    setLayoutParams(new FrameLayout.LayoutParams(-2, -2, 80));
    setPadding(0, this.gap, 0, 0);
    this.imageView = new ImageView(paramContext);
    LinearLayout localLinearLayout = new LinearLayout(paramContext);
    localLinearLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -2, 80));
    localLinearLayout.setGravity(3);
    localLinearLayout.setOrientation(1);
    localLinearLayout.setBackgroundColor(Color.parseColor("#80262626"));
    this.textView = new TextView(paramContext);
    this.textView.setLayoutParams(new FrameLayout.LayoutParams(-1, -2, 17));
    this.textView.setGravity(17);
    this.textView.setTextColor(Color.parseColor("#FFFFFF"));
    this.textView.setSingleLine();
    this.textView.setEllipsize(TextUtils.TruncateAt.MIDDLE);
    if (paramBoolean)
    {
      paramContext = new ImageButton(paramContext);
      paramContext.setLayoutParams(new FrameLayout.LayoutParams(-1, -2, 80));
      paramContext.setAdjustViewBounds(true);
      paramContext.setImageDrawable(getSystemIcon("ic_menu_delete"));
      paramContext.setBackgroundResource(0);
      paramContext.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          AttachmentView.this.remove();
        }
      });
      localLinearLayout.addView(paramContext);
    }
    localLinearLayout.addView(this.textView);
    addView(this.imageView);
    addView(localLinearLayout);
  }
  
  private Bitmap loadImageThumbnail()
  {
    try
    {
      this.orientation = ImageUtils.determineOrientation(this.context, this.attachmentUri);
      int i;
      if (this.orientation == 1)
      {
        i = this.widthLandscape;
        if (this.orientation != 1) {
          break label63;
        }
      }
      label63:
      for (int j = this.maxHeightLandscape;; j = this.maxHeightPortrait)
      {
        return ImageUtils.decodeSampledBitmap(this.context, this.attachmentUri, i, j);
        i = this.widthPortrait;
        break;
      }
      return null;
    }
    catch (Throwable localThrowable) {}
  }
  
  public FeedbackAttachment getAttachment()
  {
    return this.attachment;
  }
  
  public Uri getAttachmentUri()
  {
    return this.attachmentUri;
  }
  
  public int getEffectiveMaxHeight()
  {
    if (this.orientation == 1) {
      return this.maxHeightLandscape;
    }
    return this.maxHeightPortrait;
  }
  
  public int getGap()
  {
    return this.gap;
  }
  
  public int getMaxHeightLandscape()
  {
    return this.maxHeightLandscape;
  }
  
  public int getMaxHeightPortrait()
  {
    return this.maxHeightPortrait;
  }
  
  public int getWidthLandscape()
  {
    return this.widthLandscape;
  }
  
  public int getWidthPortrait()
  {
    return this.widthPortrait;
  }
  
  public void remove()
  {
    this.parent.removeView(this);
  }
  
  public void setImage(Bitmap paramBitmap, int paramInt)
  {
    this.textView.setText(this.filename);
    this.orientation = paramInt;
    if (paramBitmap == null)
    {
      configureViewForPlaceholder(true);
      return;
    }
    configureViewForThumbnail(paramBitmap, true);
  }
  
  public void signalImageLoadingError()
  {
    this.textView.setText("Error");
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\views\AttachmentView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */