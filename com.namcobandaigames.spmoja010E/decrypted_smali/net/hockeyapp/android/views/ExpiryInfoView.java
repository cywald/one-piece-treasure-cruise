package net.hockeyapp.android.views;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import net.hockeyapp.android.utils.ViewHelper;

public class ExpiryInfoView
  extends RelativeLayout
{
  public ExpiryInfoView(Context paramContext)
  {
    this(paramContext, "");
  }
  
  public ExpiryInfoView(Context paramContext, String paramString)
  {
    super(paramContext);
    loadLayoutParams(paramContext);
    loadShadowView(paramContext);
    loadTextView(paramContext, paramString);
  }
  
  private void loadLayoutParams(Context paramContext)
  {
    paramContext = new RelativeLayout.LayoutParams(-1, -1);
    setBackgroundColor(-1);
    setLayoutParams(paramContext);
  }
  
  private void loadShadowView(Context paramContext)
  {
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, (int)TypedValue.applyDimension(1, 3.0F, getResources().getDisplayMetrics()));
    localLayoutParams.addRule(10, -1);
    paramContext = new ImageView(paramContext);
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setBackgroundDrawable(ViewHelper.getGradient());
    addView(paramContext);
  }
  
  private void loadTextView(Context paramContext, String paramString)
  {
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
    localLayoutParams.addRule(13, -1);
    localLayoutParams.setMargins(i, i, i, i);
    paramContext = new TextView(paramContext);
    paramContext.setGravity(17);
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setText(paramString);
    paramContext.setTextColor(-16777216);
    addView(paramContext);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\views\ExpiryInfoView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */