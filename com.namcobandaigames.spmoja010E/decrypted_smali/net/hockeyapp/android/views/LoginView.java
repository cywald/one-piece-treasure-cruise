package net.hockeyapp.android.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build.VERSION;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import net.hockeyapp.android.Strings;

public class LoginView
  extends LinearLayout
{
  public static final int EMAIL_INPUT_ID = 12291;
  public static final int HEADLINE_TEXT_ID = 12290;
  public static final int LOGIN_BUTTON_ID = 12293;
  public static final int PASSWORD_INPUT_ID = 12292;
  public static final int WRAPPER_BASE_ID = 12289;
  private LinearLayout wrapperBase;
  
  public LoginView(Context paramContext)
  {
    this(paramContext, 0);
  }
  
  public LoginView(Context paramContext, int paramInt)
  {
    super(paramContext);
    loadLayoutParams(paramContext);
    loadWrapperBase(paramContext);
    loadHeadlineTextView(paramContext);
    loadEmailInput(paramContext);
    loadPasswordInput(paramContext);
    loadLoginButton(paramContext);
  }
  
  private Drawable getButtonSelector()
  {
    StateListDrawable localStateListDrawable = new StateListDrawable();
    ColorDrawable localColorDrawable = new ColorDrawable(-16777216);
    localStateListDrawable.addState(new int[] { -16842919 }, localColorDrawable);
    localColorDrawable = new ColorDrawable(-12303292);
    localStateListDrawable.addState(new int[] { -16842919, 16842908 }, localColorDrawable);
    localColorDrawable = new ColorDrawable(-7829368);
    localStateListDrawable.addState(new int[] { 16842919 }, localColorDrawable);
    return localStateListDrawable;
  }
  
  private Drawable getEditTextBackground(Context paramContext)
  {
    int i = (int)(paramContext.getResources().getDisplayMetrics().density * 10.0F);
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new RectShape());
    Paint localPaint = localShapeDrawable.getPaint();
    localPaint.setColor(-1);
    localPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    localPaint.setStrokeWidth(1.0F);
    localShapeDrawable.setPadding(i, i, i, i);
    i = (int)(paramContext.getResources().getDisplayMetrics().density * 1.5D);
    paramContext = new ShapeDrawable(new RectShape());
    localPaint = paramContext.getPaint();
    localPaint.setColor(-12303292);
    localPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    localPaint.setStrokeWidth(1.0F);
    paramContext.setPadding(0, 0, 0, i);
    return new LayerDrawable(new Drawable[] { paramContext, localShapeDrawable });
  }
  
  private void loadEmailInput(Context paramContext)
  {
    EditText localEditText = new EditText(paramContext);
    localEditText.setId(12291);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, (int)TypedValue.applyDimension(1, 30.0F, getResources().getDisplayMetrics()));
    localEditText.setLayoutParams(localLayoutParams);
    localEditText.setHint(Strings.get(1282));
    localEditText.setImeOptions(5);
    localEditText.setInputType(33);
    localEditText.setTextColor(-7829368);
    localEditText.setTextSize(2, 15.0F);
    localEditText.setTypeface(null, 0);
    localEditText.setHintTextColor(-3355444);
    setEditTextBackground(paramContext, localEditText);
    this.wrapperBase.addView(localEditText);
  }
  
  private void loadHeadlineTextView(Context paramContext)
  {
    paramContext = new TextView(paramContext);
    paramContext.setId(12290);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, (int)TypedValue.applyDimension(1, 30.0F, getResources().getDisplayMetrics()));
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setText(Strings.get(1280));
    paramContext.setTextColor(-7829368);
    paramContext.setTextSize(2, 18.0F);
    paramContext.setTypeface(null, 0);
    this.wrapperBase.addView(paramContext);
  }
  
  private void loadLayoutParams(Context paramContext)
  {
    paramContext = new LinearLayout.LayoutParams(-1, -2);
    setBackgroundColor(-1);
    setLayoutParams(paramContext);
  }
  
  private void loadLoginButton(Context paramContext)
  {
    paramContext = new Button(paramContext);
    paramContext.setId(12293);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, (int)TypedValue.applyDimension(1, 30.0F, getResources().getDisplayMetrics()));
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setBackgroundDrawable(getButtonSelector());
    paramContext.setText(Strings.get(1284));
    paramContext.setTextColor(-1);
    paramContext.setTextSize(2, 15.0F);
    this.wrapperBase.addView(paramContext);
  }
  
  private void loadPasswordInput(Context paramContext)
  {
    EditText localEditText = new EditText(paramContext);
    localEditText.setId(12292);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, (int)TypedValue.applyDimension(1, 30.0F, getResources().getDisplayMetrics()));
    localEditText.setLayoutParams(localLayoutParams);
    localEditText.setHint(Strings.get(1283));
    localEditText.setImeOptions(5);
    localEditText.setInputType(128);
    localEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
    localEditText.setTextColor(-7829368);
    localEditText.setTextSize(2, 15.0F);
    localEditText.setTypeface(null, 0);
    localEditText.setHintTextColor(-3355444);
    setEditTextBackground(paramContext, localEditText);
    this.wrapperBase.addView(localEditText);
  }
  
  private void loadWrapperBase(Context paramContext)
  {
    this.wrapperBase = new LinearLayout(paramContext);
    this.wrapperBase.setId(12289);
    paramContext = new LinearLayout.LayoutParams(-1, -1);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    paramContext.gravity = 49;
    this.wrapperBase.setLayoutParams(paramContext);
    this.wrapperBase.setPadding(i, i, i, i);
    this.wrapperBase.setOrientation(1);
    addView(this.wrapperBase);
  }
  
  private void setEditTextBackground(Context paramContext, EditText paramEditText)
  {
    if (Build.VERSION.SDK_INT < 11) {
      paramEditText.setBackgroundDrawable(getEditTextBackground(paramContext));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\views\LoginView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */