package net.hockeyapp.android.views;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import net.hockeyapp.android.utils.ViewHelper;

public class UpdateView
  extends RelativeLayout
{
  public static final int HEADER_VIEW_ID = 4097;
  public static final int NAME_LABEL_ID = 4098;
  public static final int UPDATE_BUTTON_ID = 4100;
  public static final int VERSION_LABEL_ID = 4099;
  public static final int WEB_VIEW_ID = 4101;
  protected RelativeLayout headerView = null;
  protected boolean layoutHorizontally = false;
  protected boolean limitHeight = false;
  
  public UpdateView(Context paramContext)
  {
    this(paramContext, true);
  }
  
  public UpdateView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, true, false);
  }
  
  public UpdateView(Context paramContext, boolean paramBoolean)
  {
    this(paramContext, true, false);
  }
  
  public UpdateView(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramContext);
    if (paramBoolean1) {
      setLayoutHorizontally(paramContext);
    }
    for (;;)
    {
      this.limitHeight = paramBoolean2;
      loadLayoutParams(paramContext);
      loadHeaderView(paramContext);
      loadWebView(paramContext);
      loadShadow(this.headerView, paramContext);
      return;
      this.layoutHorizontally = false;
    }
  }
  
  private Drawable getButtonSelector()
  {
    StateListDrawable localStateListDrawable = new StateListDrawable();
    ColorDrawable localColorDrawable = new ColorDrawable(-16777216);
    localStateListDrawable.addState(new int[] { -16842919 }, localColorDrawable);
    localColorDrawable = new ColorDrawable(-12303292);
    localStateListDrawable.addState(new int[] { -16842919, 16842908 }, localColorDrawable);
    localColorDrawable = new ColorDrawable(-7829368);
    localStateListDrawable.addState(new int[] { 16842919 }, localColorDrawable);
    return localStateListDrawable;
  }
  
  private void loadHeaderView(Context paramContext)
  {
    this.headerView = new RelativeLayout(paramContext);
    this.headerView.setId(4097);
    RelativeLayout.LayoutParams localLayoutParams;
    if (this.layoutHorizontally)
    {
      localLayoutParams = new RelativeLayout.LayoutParams((int)TypedValue.applyDimension(1, 250.0F, getResources().getDisplayMetrics()), -1);
      localLayoutParams.addRule(9, -1);
      this.headerView.setPadding(0, 0, 0, 0);
    }
    for (;;)
    {
      this.headerView.setLayoutParams(localLayoutParams);
      this.headerView.setBackgroundColor(Color.rgb(230, 236, 239));
      loadTitleLabel(this.headerView, paramContext);
      loadVersionLabel(this.headerView, paramContext);
      loadUpdateButton(this.headerView, paramContext);
      addView(this.headerView);
      return;
      localLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
      this.headerView.setPadding(0, 0, 0, (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics()));
    }
  }
  
  private void loadLayoutParams(Context paramContext)
  {
    paramContext = new RelativeLayout.LayoutParams(-1, -2);
    setBackgroundColor(-1);
    setLayoutParams(paramContext);
  }
  
  private void loadShadow(RelativeLayout paramRelativeLayout, Context paramContext)
  {
    int i = (int)TypedValue.applyDimension(1, 3.0F, getResources().getDisplayMetrics());
    ImageView localImageView = new ImageView(paramContext);
    RelativeLayout.LayoutParams localLayoutParams;
    if (this.layoutHorizontally)
    {
      localLayoutParams = new RelativeLayout.LayoutParams(1, -1);
      localLayoutParams.addRule(11, -1);
      localImageView.setBackgroundDrawable(new ColorDrawable(-16777216));
      localImageView.setLayoutParams(localLayoutParams);
      paramRelativeLayout.addView(localImageView);
      paramRelativeLayout = new ImageView(paramContext);
      paramContext = new RelativeLayout.LayoutParams(-1, i);
      if (!this.layoutHorizontally) {
        break label159;
      }
      paramContext.addRule(10, -1);
    }
    for (;;)
    {
      paramRelativeLayout.setLayoutParams(paramContext);
      paramRelativeLayout.setBackgroundDrawable(ViewHelper.getGradient());
      addView(paramRelativeLayout);
      return;
      localLayoutParams = new RelativeLayout.LayoutParams(-1, i);
      localLayoutParams.addRule(10, -1);
      localImageView.setBackgroundDrawable(ViewHelper.getGradient());
      break;
      label159:
      paramContext.addRule(3, 4097);
    }
  }
  
  private void loadTitleLabel(RelativeLayout paramRelativeLayout, Context paramContext)
  {
    paramContext = new TextView(paramContext);
    paramContext.setId(4098);
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    localLayoutParams.setMargins(i, i, i, 0);
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setEllipsize(TextUtils.TruncateAt.END);
    paramContext.setShadowLayer(1.0F, 0.0F, 1.0F, -1);
    paramContext.setSingleLine(true);
    paramContext.setTextColor(-16777216);
    paramContext.setTextSize(2, 20.0F);
    paramContext.setTypeface(null, 1);
    paramRelativeLayout.addView(paramContext);
  }
  
  private void loadUpdateButton(RelativeLayout paramRelativeLayout, Context paramContext)
  {
    paramContext = new Button(paramContext);
    paramContext.setId(4100);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams((int)TypedValue.applyDimension(1, 120.0F, getResources().getDisplayMetrics()), -2);
    localLayoutParams.setMargins(i, i, i, i);
    localLayoutParams.addRule(9, -1);
    localLayoutParams.addRule(3, 4099);
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setBackgroundDrawable(getButtonSelector());
    paramContext.setText("Update");
    paramContext.setTextColor(-1);
    paramContext.setTextSize(2, 16.0F);
    paramRelativeLayout.addView(paramContext);
  }
  
  private void loadVersionLabel(RelativeLayout paramRelativeLayout, Context paramContext)
  {
    paramContext = new TextView(paramContext);
    paramContext.setId(4099);
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    localLayoutParams.setMargins(i, (int)TypedValue.applyDimension(1, 10.0F, getResources().getDisplayMetrics()), i, 0);
    localLayoutParams.addRule(3, 4098);
    paramContext.setLayoutParams(localLayoutParams);
    paramContext.setEllipsize(TextUtils.TruncateAt.END);
    paramContext.setShadowLayer(1.0F, 0.0F, 1.0F, -1);
    paramContext.setLines(2);
    paramContext.setLineSpacing(0.0F, 1.1F);
    paramContext.setTextColor(-16777216);
    paramContext.setTextSize(2, 16.0F);
    paramContext.setTypeface(null, 1);
    paramRelativeLayout.addView(paramContext);
  }
  
  private void loadWebView(Context paramContext)
  {
    WebView localWebView = new WebView(paramContext);
    localWebView.setId(4101);
    int i = (int)TypedValue.applyDimension(1, 400.0F, paramContext.getResources().getDisplayMetrics());
    if (this.limitHeight)
    {
      paramContext = new RelativeLayout.LayoutParams(-1, i);
      if (!this.layoutHorizontally) {
        break label92;
      }
      paramContext.addRule(1, 4097);
    }
    for (;;)
    {
      paramContext.setMargins(0, 0, 0, 0);
      localWebView.setLayoutParams(paramContext);
      localWebView.setBackgroundColor(-1);
      addView(localWebView);
      return;
      i = -1;
      break;
      label92:
      paramContext.addRule(3, 4097);
    }
  }
  
  private void setLayoutHorizontally(Context paramContext)
  {
    if (getResources().getConfiguration().orientation == 2)
    {
      this.layoutHorizontally = true;
      return;
    }
    this.layoutHorizontally = false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\views\UpdateView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */