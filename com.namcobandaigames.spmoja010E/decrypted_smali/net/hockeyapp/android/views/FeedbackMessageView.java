package net.hockeyapp.android.views;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class FeedbackMessageView
  extends LinearLayout
{
  public static final int ATTACHMENT_LIST_VIEW_ID = 12292;
  public static final int AUTHOR_TEXT_VIEW_ID = 12289;
  public static final int DATE_TEXT_VIEW_ID = 12290;
  public static final int MESSAGE_TEXT_VIEW_ID = 12291;
  private AttachmentListView attachmentListView;
  private TextView authorTextView;
  private TextView dateTextView;
  private TextView messageTextView;
  private boolean ownMessage;
  
  public FeedbackMessageView(Context paramContext)
  {
    this(paramContext, true);
  }
  
  public FeedbackMessageView(Context paramContext, boolean paramBoolean)
  {
    super(paramContext);
    this.ownMessage = paramBoolean;
    loadLayoutParams(paramContext);
    loadAuthorLabel(paramContext);
    loadDateLabel(paramContext);
    loadMessageLabel(paramContext);
    loadAttachmentList(paramContext);
  }
  
  private void loadAttachmentList(Context paramContext)
  {
    this.attachmentListView = new AttachmentListView(paramContext);
    this.attachmentListView.setId(12292);
    paramContext = new LinearLayout.LayoutParams(-1, -1);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    paramContext.setMargins(i, 0, i, i);
    this.attachmentListView.setLayoutParams(paramContext);
    addView(this.attachmentListView);
  }
  
  private void loadAuthorLabel(Context paramContext)
  {
    this.authorTextView = new TextView(paramContext);
    this.authorTextView.setId(12289);
    paramContext = new LinearLayout.LayoutParams(-2, -2);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    paramContext.setMargins(i, i, i, 0);
    this.authorTextView.setLayoutParams(paramContext);
    this.authorTextView.setShadowLayer(1.0F, 0.0F, 1.0F, -1);
    this.authorTextView.setSingleLine(true);
    this.authorTextView.setTextColor(-7829368);
    this.authorTextView.setTextSize(2, 15.0F);
    this.authorTextView.setTypeface(null, 0);
    addView(this.authorTextView);
  }
  
  private void loadDateLabel(Context paramContext)
  {
    this.dateTextView = new TextView(paramContext);
    this.dateTextView.setId(12290);
    paramContext = new LinearLayout.LayoutParams(-2, -2);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    paramContext.setMargins(i, 0, i, 0);
    this.dateTextView.setLayoutParams(paramContext);
    this.dateTextView.setShadowLayer(1.0F, 0.0F, 1.0F, -1);
    this.dateTextView.setSingleLine(true);
    this.dateTextView.setTextColor(-7829368);
    this.dateTextView.setTextSize(2, 15.0F);
    this.dateTextView.setTypeface(null, 2);
    addView(this.dateTextView);
  }
  
  private void loadLayoutParams(Context paramContext)
  {
    setOrientation(1);
    setGravity(3);
    setBackgroundColor(-3355444);
  }
  
  private void loadMessageLabel(Context paramContext)
  {
    this.messageTextView = new TextView(paramContext);
    this.messageTextView.setId(12291);
    paramContext = new LinearLayout.LayoutParams(-2, -2);
    int i = (int)TypedValue.applyDimension(1, 20.0F, getResources().getDisplayMetrics());
    paramContext.setMargins(i, 0, i, i);
    this.messageTextView.setLayoutParams(paramContext);
    this.messageTextView.setShadowLayer(1.0F, 0.0F, 1.0F, -1);
    this.messageTextView.setSingleLine(false);
    this.messageTextView.setTextColor(-16777216);
    this.messageTextView.setTextSize(2, 18.0F);
    this.messageTextView.setTypeface(null, 0);
    addView(this.messageTextView);
  }
  
  private void setAuthorLaberColor(int paramInt)
  {
    if (this.authorTextView != null) {
      this.authorTextView.setTextColor(paramInt);
    }
  }
  
  private void setDateLaberColor(int paramInt)
  {
    if (this.dateTextView != null) {
      this.dateTextView.setTextColor(paramInt);
    }
  }
  
  private void setMessageLaberColor(int paramInt)
  {
    if (this.messageTextView != null) {
      this.messageTextView.setTextColor(paramInt);
    }
  }
  
  public void setAuthorLabelText(String paramString)
  {
    if ((this.authorTextView != null) && (paramString != null)) {
      this.authorTextView.setText(paramString);
    }
  }
  
  public void setDateLabelText(String paramString)
  {
    if ((this.dateTextView != null) && (paramString != null)) {
      this.dateTextView.setText(paramString);
    }
  }
  
  public void setFeedbackMessageViewBgAndTextColor(int paramInt)
  {
    if (paramInt == 0)
    {
      setBackgroundColor(-3355444);
      setAuthorLaberColor(-1);
      setDateLaberColor(-1);
    }
    for (;;)
    {
      setMessageLaberColor(-16777216);
      return;
      if (paramInt == 1)
      {
        setBackgroundColor(-1);
        setAuthorLaberColor(-3355444);
        setDateLaberColor(-3355444);
      }
    }
  }
  
  public void setMessageLabelText(String paramString)
  {
    if ((this.messageTextView != null) && (paramString != null)) {
      this.messageTextView.setText(paramString);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\views\FeedbackMessageView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */