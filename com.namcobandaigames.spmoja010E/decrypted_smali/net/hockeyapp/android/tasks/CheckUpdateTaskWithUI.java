package net.hockeyapp.android.tasks;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import net.hockeyapp.android.Strings;
import net.hockeyapp.android.UpdateActivity;
import net.hockeyapp.android.UpdateFragment;
import net.hockeyapp.android.UpdateManagerListener;
import net.hockeyapp.android.utils.Util;
import net.hockeyapp.android.utils.VersionCache;
import org.json.JSONArray;

public class CheckUpdateTaskWithUI
  extends CheckUpdateTask
{
  private Activity activity = null;
  private AlertDialog dialog = null;
  protected boolean isDialogRequired = false;
  
  public CheckUpdateTaskWithUI(WeakReference<Activity> paramWeakReference, String paramString1, String paramString2, UpdateManagerListener paramUpdateManagerListener, boolean paramBoolean)
  {
    super(paramWeakReference, paramString1, paramString2, paramUpdateManagerListener);
    if (paramWeakReference != null) {
      this.activity = ((Activity)paramWeakReference.get());
    }
    this.isDialogRequired = paramBoolean;
  }
  
  @TargetApi(11)
  private void showDialog(final JSONArray paramJSONArray)
  {
    if (getCachingEnabled()) {
      VersionCache.setVersionInfo(this.activity, paramJSONArray.toString());
    }
    if ((this.activity == null) || (this.activity.isFinishing())) {
      return;
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.activity);
    localBuilder.setTitle(Strings.get(this.listener, 513));
    if (!this.mandatory.booleanValue())
    {
      localBuilder.setMessage(Strings.get(this.listener, 514));
      localBuilder.setNegativeButton(Strings.get(this.listener, 515), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          CheckUpdateTaskWithUI.this.cleanUp();
        }
      });
      localBuilder.setPositiveButton(Strings.get(this.listener, 516), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          if (CheckUpdateTaskWithUI.this.getCachingEnabled()) {
            VersionCache.setVersionInfo(CheckUpdateTaskWithUI.this.activity, "[]");
          }
          paramAnonymousDialogInterface = new WeakReference(CheckUpdateTaskWithUI.this.activity);
          if ((Util.fragmentsSupported().booleanValue()) && (Util.runsOnTablet(paramAnonymousDialogInterface).booleanValue()))
          {
            CheckUpdateTaskWithUI.this.showUpdateFragment(paramJSONArray);
            return;
          }
          CheckUpdateTaskWithUI.this.startUpdateIntent(paramJSONArray, Boolean.valueOf(false));
        }
      });
      this.dialog = localBuilder.create();
      this.dialog.show();
      return;
    }
    Toast.makeText(this.activity, Strings.get(this.listener, 512), 1).show();
    startUpdateIntent(paramJSONArray, Boolean.valueOf(true));
  }
  
  @TargetApi(11)
  private void showUpdateFragment(JSONArray paramJSONArray)
  {
    FragmentTransaction localFragmentTransaction;
    Object localObject;
    if (this.activity != null)
    {
      localFragmentTransaction = this.activity.getFragmentManager().beginTransaction();
      localFragmentTransaction.setTransition(4097);
      localObject = this.activity.getFragmentManager().findFragmentByTag("hockey_update_dialog");
      if (localObject != null) {
        localFragmentTransaction.remove((Fragment)localObject);
      }
      localFragmentTransaction.addToBackStack(null);
      localObject = UpdateFragment.class;
      if (this.listener != null) {
        localObject = this.listener.getUpdateFragmentClass();
      }
    }
    try
    {
      ((DialogFragment)((Class)localObject).getMethod("newInstance", new Class[] { JSONArray.class, String.class }).invoke(null, new Object[] { paramJSONArray, getURLString("apk") })).show(localFragmentTransaction, "hockey_update_dialog");
      return;
    }
    catch (Exception localException)
    {
      Log.d("HockeyApp", "An exception happened while showing the update fragment:");
      localException.printStackTrace();
      Log.d("HockeyApp", "Showing update activity instead.");
      startUpdateIntent(paramJSONArray, Boolean.valueOf(false));
    }
  }
  
  @TargetApi(11)
  private void startUpdateIntent(JSONArray paramJSONArray, Boolean paramBoolean)
  {
    Object localObject1 = null;
    if (this.listener != null) {
      localObject1 = this.listener.getUpdateActivityClass();
    }
    Object localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = UpdateActivity.class;
    }
    if (this.activity != null)
    {
      localObject1 = new Intent();
      ((Intent)localObject1).setClass(this.activity, (Class)localObject2);
      ((Intent)localObject1).putExtra("json", paramJSONArray.toString());
      ((Intent)localObject1).putExtra("url", getURLString("apk"));
      this.activity.startActivity((Intent)localObject1);
      if (paramBoolean.booleanValue()) {
        this.activity.finish();
      }
    }
    cleanUp();
  }
  
  protected void cleanUp()
  {
    super.cleanUp();
    this.activity = null;
    this.dialog = null;
  }
  
  public void detach()
  {
    super.detach();
    this.activity = null;
    if (this.dialog != null)
    {
      this.dialog.dismiss();
      this.dialog = null;
    }
  }
  
  protected void onPostExecute(JSONArray paramJSONArray)
  {
    super.onPostExecute(paramJSONArray);
    if ((paramJSONArray != null) && (this.isDialogRequired)) {
      showDialog(paramJSONArray);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\tasks\CheckUpdateTaskWithUI.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */