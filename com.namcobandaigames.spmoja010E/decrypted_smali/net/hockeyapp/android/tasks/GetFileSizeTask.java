package net.hockeyapp.android.tasks;

import android.content.Context;
import java.net.URL;
import java.net.URLConnection;
import net.hockeyapp.android.listeners.DownloadFileListener;

public class GetFileSizeTask
  extends DownloadFileTask
{
  private long size;
  
  public GetFileSizeTask(Context paramContext, String paramString, DownloadFileListener paramDownloadFileListener)
  {
    super(paramContext, paramString, paramDownloadFileListener);
  }
  
  protected Long doInBackground(Void... paramVarArgs)
  {
    try
    {
      long l = createConnection(new URL(getURLString()), 6).getContentLength();
      return Long.valueOf(l);
    }
    catch (Exception paramVarArgs)
    {
      paramVarArgs.printStackTrace();
    }
    return Long.valueOf(0L);
  }
  
  public long getSize()
  {
    return this.size;
  }
  
  protected void onPostExecute(Long paramLong)
  {
    this.size = paramLong.longValue();
    if (this.size > 0L)
    {
      this.notifier.downloadSuccessful(this);
      return;
    }
    this.notifier.downloadFailed(this, Boolean.valueOf(false));
  }
  
  protected void onProgressUpdate(Integer... paramVarArgs) {}
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\tasks\GetFileSizeTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */