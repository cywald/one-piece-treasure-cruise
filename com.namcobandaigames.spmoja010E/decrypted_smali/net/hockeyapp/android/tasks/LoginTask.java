package net.hockeyapp.android.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import net.hockeyapp.android.Constants;
import net.hockeyapp.android.utils.Base64;
import net.hockeyapp.android.utils.ConnectionManager;
import net.hockeyapp.android.utils.PrefsUtil;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginTask
  extends AsyncTask<Void, Void, Boolean>
{
  private Context context;
  private Handler handler;
  private final int mode;
  private final Map<String, String> params;
  private ProgressDialog progressDialog;
  private boolean showProgressDialog;
  private final String urlString;
  
  public LoginTask(Context paramContext, Handler paramHandler, String paramString, int paramInt, Map<String, String> paramMap)
  {
    this.context = paramContext;
    this.handler = paramHandler;
    this.urlString = paramString;
    this.mode = paramInt;
    this.params = paramMap;
    this.showProgressDialog = true;
    if (paramContext != null) {
      Constants.loadFromContext(paramContext);
    }
  }
  
  private boolean handleResponse(String paramString)
  {
    SharedPreferences localSharedPreferences = this.context.getSharedPreferences("net.hockeyapp.android.login", 0);
    try
    {
      paramString = new JSONObject(paramString);
      String str = paramString.getString("status");
      if (TextUtils.isEmpty(str)) {
        return false;
      }
      if (this.mode == 1)
      {
        if (!str.equals("identified")) {
          break label222;
        }
        paramString = paramString.getString("iuid");
        if (TextUtils.isEmpty(paramString)) {
          break label222;
        }
        PrefsUtil.applyChanges(localSharedPreferences.edit().putString("iuid", paramString));
        return true;
      }
      if (this.mode == 2)
      {
        if (!str.equals("authorized")) {
          break label222;
        }
        paramString = paramString.getString("auid");
        if (TextUtils.isEmpty(paramString)) {
          break label222;
        }
        PrefsUtil.applyChanges(localSharedPreferences.edit().putString("auid", paramString));
        return true;
      }
      if (this.mode == 3)
      {
        if (str.equals("validated")) {
          return true;
        }
        PrefsUtil.applyChanges(localSharedPreferences.edit().remove("iuid").remove("auid"));
        return false;
      }
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
      return false;
    }
    throw new IllegalArgumentException("Login mode " + this.mode + " not supported.");
    label222:
    return false;
  }
  
  private HttpUriRequest makeRequest(int paramInt, Map<String, String> paramMap)
    throws UnsupportedEncodingException
  {
    Object localObject;
    if (paramInt == 1)
    {
      localObject = new ArrayList();
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        ((List)localObject).add(new BasicNameValuePair((String)localEntry.getKey(), (String)localEntry.getValue()));
      }
      paramMap = new UrlEncodedFormEntity((List)localObject, "UTF-8");
      paramMap.setContentEncoding("UTF-8");
      localObject = new HttpPost(this.urlString);
      ((HttpPost)localObject).setEntity(paramMap);
      return (HttpUriRequest)localObject;
    }
    if (paramInt == 2)
    {
      localObject = (String)paramMap.get("email");
      paramMap = (String)paramMap.get("password");
      paramMap = "Basic " + Base64.encodeToString(new StringBuilder().append((String)localObject).append(":").append(paramMap).toString().getBytes(), 2);
      localObject = new HttpPost(this.urlString);
      ((HttpPost)localObject).setHeader("Authorization", paramMap);
      return (HttpUriRequest)localObject;
    }
    if (paramInt == 3)
    {
      localObject = (String)paramMap.get("type");
      paramMap = (String)paramMap.get("id");
      return new HttpGet(this.urlString + "?" + (String)localObject + "=" + paramMap);
    }
    throw new IllegalArgumentException("Login mode " + paramInt + " not supported.");
  }
  
  public void attach(Context paramContext, Handler paramHandler)
  {
    this.context = paramContext;
    this.handler = paramHandler;
  }
  
  public void detach()
  {
    this.context = null;
    this.handler = null;
    this.progressDialog = null;
  }
  
  protected Boolean doInBackground(Void... paramVarArgs)
  {
    paramVarArgs = ConnectionManager.getInstance().getHttpClient();
    try
    {
      paramVarArgs = paramVarArgs.execute(makeRequest(this.mode, this.params));
      if (paramVarArgs != null)
      {
        paramVarArgs = EntityUtils.toString(paramVarArgs.getEntity());
        if (!TextUtils.isEmpty(paramVarArgs))
        {
          boolean bool = handleResponse(paramVarArgs);
          return Boolean.valueOf(bool);
        }
      }
    }
    catch (UnsupportedEncodingException paramVarArgs)
    {
      paramVarArgs.printStackTrace();
      return Boolean.valueOf(false);
    }
    catch (ClientProtocolException paramVarArgs)
    {
      for (;;)
      {
        paramVarArgs.printStackTrace();
      }
    }
    catch (IOException paramVarArgs)
    {
      for (;;)
      {
        paramVarArgs.printStackTrace();
      }
    }
  }
  
  protected void onPostExecute(Boolean paramBoolean)
  {
    if (this.progressDialog != null) {}
    try
    {
      this.progressDialog.dismiss();
      if (this.handler != null)
      {
        Message localMessage = new Message();
        Bundle localBundle = new Bundle();
        localBundle.putBoolean("success", paramBoolean.booleanValue());
        localMessage.setData(localBundle);
        this.handler.sendMessage(localMessage);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  protected void onPreExecute()
  {
    if (((this.progressDialog == null) || (!this.progressDialog.isShowing())) && (this.showProgressDialog)) {
      this.progressDialog = ProgressDialog.show(this.context, "", "Please wait...", true, false);
    }
  }
  
  public void setShowProgressDialog(boolean paramBoolean)
  {
    this.showProgressDialog = paramBoolean;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\tasks\LoginTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */