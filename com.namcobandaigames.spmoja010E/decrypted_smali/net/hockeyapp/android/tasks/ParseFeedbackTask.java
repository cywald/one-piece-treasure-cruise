package net.hockeyapp.android.tasks;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import net.hockeyapp.android.FeedbackActivity;
import net.hockeyapp.android.FeedbackManager;
import net.hockeyapp.android.FeedbackManagerListener;
import net.hockeyapp.android.objects.Feedback;
import net.hockeyapp.android.objects.FeedbackMessage;
import net.hockeyapp.android.objects.FeedbackResponse;
import net.hockeyapp.android.utils.FeedbackParser;
import net.hockeyapp.android.utils.PrefsUtil;

public class ParseFeedbackTask
  extends AsyncTask<Void, Void, FeedbackResponse>
{
  public static final String ID_LAST_MESSAGE_PROCESSED = "idLastMessageProcessed";
  public static final String ID_LAST_MESSAGE_SEND = "idLastMessageSend";
  public static final int NEW_ANSWER_NOTIFICATION_ID = 2;
  public static final String PREFERENCES_NAME = "net.hockeyapp.android.feedback";
  private Context context;
  private String feedbackResponse;
  private Handler handler;
  private String requestType;
  private String urlString;
  
  public ParseFeedbackTask(Context paramContext, String paramString1, Handler paramHandler, String paramString2)
  {
    this.context = paramContext;
    this.feedbackResponse = paramString1;
    this.handler = paramHandler;
    this.requestType = paramString2;
    this.urlString = null;
  }
  
  private void checkForNewAnswers(ArrayList<FeedbackMessage> paramArrayList)
  {
    paramArrayList = (FeedbackMessage)paramArrayList.get(paramArrayList.size() - 1);
    int i = paramArrayList.getId();
    Object localObject = this.context.getSharedPreferences("net.hockeyapp.android.feedback", 0);
    if (this.requestType.equals("send")) {
      PrefsUtil.applyChanges(((SharedPreferences)localObject).edit().putInt("idLastMessageSend", i).putInt("idLastMessageProcessed", i));
    }
    boolean bool;
    do
    {
      int j;
      int k;
      do
      {
        do
        {
          return;
        } while (!this.requestType.equals("fetch"));
        j = ((SharedPreferences)localObject).getInt("idLastMessageSend", -1);
        k = ((SharedPreferences)localObject).getInt("idLastMessageProcessed", -1);
      } while ((i == j) || (i == k));
      PrefsUtil.applyChanges(((SharedPreferences)localObject).edit().putInt("idLastMessageProcessed", i));
      bool = false;
      localObject = FeedbackManager.getLastListener();
      if (localObject != null) {
        bool = ((FeedbackManagerListener)localObject).feedbackAnswered(paramArrayList);
      }
    } while (bool);
    startNotification(this.context);
  }
  
  private void startNotification(Context paramContext)
  {
    if (this.urlString == null) {
      return;
    }
    NotificationManager localNotificationManager = (NotificationManager)paramContext.getSystemService("notification");
    Notification localNotification = new Notification(paramContext.getResources().getIdentifier("ic_menu_refresh", "drawable", "android"), "New Answer to Your Feedback.", System.currentTimeMillis());
    Object localObject1 = null;
    if (FeedbackManager.getLastListener() != null) {
      localObject1 = FeedbackManager.getLastListener().getFeedbackActivityClass();
    }
    Object localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = FeedbackActivity.class;
    }
    localObject1 = new Intent();
    ((Intent)localObject1).setFlags(805306368);
    ((Intent)localObject1).setClass(paramContext, (Class)localObject2);
    ((Intent)localObject1).putExtra("url", this.urlString);
    localNotification.setLatestEventInfo(paramContext, "HockeyApp Feedback", "A new answer to your feedback is available.", PendingIntent.getActivity(paramContext, 0, (Intent)localObject1, 1073741824));
    localNotificationManager.notify(2, localNotification);
  }
  
  protected FeedbackResponse doInBackground(Void... paramVarArgs)
  {
    if ((this.context != null) && (this.feedbackResponse != null))
    {
      paramVarArgs = FeedbackParser.getInstance().parseFeedbackResponse(this.feedbackResponse);
      if ((paramVarArgs != null) && (paramVarArgs.getFeedback() != null))
      {
        ArrayList localArrayList = paramVarArgs.getFeedback().getMessages();
        if ((localArrayList != null) && (!localArrayList.isEmpty())) {
          checkForNewAnswers(localArrayList);
        }
      }
      return paramVarArgs;
    }
    return null;
  }
  
  protected void onPostExecute(FeedbackResponse paramFeedbackResponse)
  {
    if ((paramFeedbackResponse != null) && (this.handler != null))
    {
      Message localMessage = new Message();
      Bundle localBundle = new Bundle();
      localBundle.putSerializable("parse_feedback_response", paramFeedbackResponse);
      localMessage.setData(localBundle);
      this.handler.sendMessage(localMessage);
    }
  }
  
  public void setUrlString(String paramString)
  {
    this.urlString = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\tasks\ParseFeedbackTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */