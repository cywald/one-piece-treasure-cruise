package net.hockeyapp.android.tasks;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import net.hockeyapp.android.Constants;
import net.hockeyapp.android.utils.ConnectionManager;
import net.hockeyapp.android.utils.SimpleMultipartEntity;
import net.hockeyapp.android.utils.Util;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class SendFeedbackTask
  extends AsyncTask<Void, Void, HashMap<String, String>>
{
  private List<Uri> attachmentUris;
  private Context context;
  private String email;
  private Handler handler;
  private boolean isFetchMessages;
  private int lastMessageId;
  private String name;
  private ProgressDialog progressDialog;
  private boolean showProgressDialog;
  private String subject;
  private String text;
  private String token;
  private String urlString;
  
  public SendFeedbackTask(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, List<Uri> paramList, String paramString6, Handler paramHandler, boolean paramBoolean)
  {
    this.context = paramContext;
    this.urlString = paramString1;
    this.name = paramString2;
    this.email = paramString3;
    this.subject = paramString4;
    this.text = paramString5;
    this.attachmentUris = paramList;
    this.token = paramString6;
    this.handler = paramHandler;
    this.isFetchMessages = paramBoolean;
    this.showProgressDialog = true;
    this.lastMessageId = -1;
    if (paramContext != null) {
      Constants.loadFromContext(paramContext);
    }
  }
  
  private HashMap<String, String> doGet(HttpClient paramHttpClient)
  {
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(this.urlString + Util.encodeParam(this.token));
    if (this.lastMessageId != -1) {
      ((StringBuilder)localObject).append("?last_message_id=" + this.lastMessageId);
    }
    HttpGet localHttpGet = new HttpGet(((StringBuilder)localObject).toString());
    localObject = new HashMap();
    ((HashMap)localObject).put("type", "fetch");
    try
    {
      paramHttpClient = paramHttpClient.execute(localHttpGet);
      ((HashMap)localObject).put("response", EntityUtils.toString(paramHttpClient.getEntity()));
      ((HashMap)localObject).put("status", "" + paramHttpClient.getStatusLine().getStatusCode());
      return (HashMap<String, String>)localObject;
    }
    catch (ClientProtocolException paramHttpClient)
    {
      paramHttpClient.printStackTrace();
      return (HashMap<String, String>)localObject;
    }
    catch (IllegalStateException paramHttpClient)
    {
      paramHttpClient.printStackTrace();
      return (HashMap<String, String>)localObject;
    }
    catch (IOException paramHttpClient)
    {
      paramHttpClient.printStackTrace();
    }
    return (HashMap<String, String>)localObject;
  }
  
  private HashMap<String, String> doPostPut(HttpClient paramHttpClient)
  {
    localHashMap = new HashMap();
    localHashMap.put("type", "send");
    try
    {
      Object localObject1 = new ArrayList();
      ((List)localObject1).add(new BasicNameValuePair("name", this.name));
      ((List)localObject1).add(new BasicNameValuePair("email", this.email));
      ((List)localObject1).add(new BasicNameValuePair("subject", this.subject));
      ((List)localObject1).add(new BasicNameValuePair("text", this.text));
      ((List)localObject1).add(new BasicNameValuePair("bundle_identifier", Constants.APP_PACKAGE));
      ((List)localObject1).add(new BasicNameValuePair("bundle_short_version", Constants.APP_VERSION_NAME));
      ((List)localObject1).add(new BasicNameValuePair("bundle_version", Constants.APP_VERSION));
      ((List)localObject1).add(new BasicNameValuePair("os_version", Constants.ANDROID_VERSION));
      ((List)localObject1).add(new BasicNameValuePair("oem", Constants.PHONE_MANUFACTURER));
      ((List)localObject1).add(new BasicNameValuePair("model", Constants.PHONE_MODEL));
      UrlEncodedFormEntity localUrlEncodedFormEntity = new UrlEncodedFormEntity((List)localObject1, "UTF-8");
      localUrlEncodedFormEntity.setContentEncoding("UTF-8");
      HttpPost localHttpPost = null;
      localObject1 = null;
      Object localObject2;
      if (this.token != null)
      {
        this.urlString = (this.urlString + this.token + "/");
        localObject1 = new HttpPut(this.urlString);
        localObject2 = null;
        if (localObject1 == null) {
          break label393;
        }
        ((HttpPut)localObject1).setEntity(localUrlEncodedFormEntity);
        localObject1 = paramHttpClient.execute((HttpUriRequest)localObject1);
      }
      for (;;)
      {
        if (localObject1 == null) {
          break label438;
        }
        localHashMap.put("response", EntityUtils.toString(((HttpResponse)localObject1).getEntity()));
        localHashMap.put("status", "" + ((HttpResponse)localObject1).getStatusLine().getStatusCode());
        return localHashMap;
        localHttpPost = new HttpPost(this.urlString);
        break;
        label393:
        localObject1 = localObject2;
        if (localHttpPost != null)
        {
          localHttpPost.setEntity(localUrlEncodedFormEntity);
          localObject1 = paramHttpClient.execute(localHttpPost);
        }
      }
      return localHashMap;
    }
    catch (UnsupportedEncodingException paramHttpClient)
    {
      paramHttpClient.printStackTrace();
      return localHashMap;
    }
    catch (ClientProtocolException paramHttpClient)
    {
      paramHttpClient.printStackTrace();
      return localHashMap;
    }
    catch (IOException paramHttpClient)
    {
      paramHttpClient.printStackTrace();
    }
  }
  
  private HashMap<String, String> doPostPutWithAttachments(HttpClient paramHttpClient)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("type", "send");
    for (;;)
    {
      try
      {
        Object localObject1 = new ArrayList();
        ((List)localObject1).add(new BasicNameValuePair("name", this.name));
        ((List)localObject1).add(new BasicNameValuePair("email", this.email));
        ((List)localObject1).add(new BasicNameValuePair("subject", this.subject));
        ((List)localObject1).add(new BasicNameValuePair("text", this.text));
        ((List)localObject1).add(new BasicNameValuePair("bundle_identifier", Constants.APP_PACKAGE));
        ((List)localObject1).add(new BasicNameValuePair("bundle_short_version", Constants.APP_VERSION_NAME));
        ((List)localObject1).add(new BasicNameValuePair("bundle_version", Constants.APP_VERSION));
        ((List)localObject1).add(new BasicNameValuePair("os_version", Constants.ANDROID_VERSION));
        ((List)localObject1).add(new BasicNameValuePair("oem", Constants.PHONE_MANUFACTURER));
        ((List)localObject1).add(new BasicNameValuePair("model", Constants.PHONE_MODEL));
        SimpleMultipartEntity localSimpleMultipartEntity = new SimpleMultipartEntity();
        localSimpleMultipartEntity.writeFirstBoundaryIfNeeds();
        localObject1 = ((List)localObject1).iterator();
        Object localObject2;
        if (((Iterator)localObject1).hasNext())
        {
          localObject2 = (NameValuePair)((Iterator)localObject1).next();
          localSimpleMultipartEntity.addPart(((NameValuePair)localObject2).getName(), ((NameValuePair)localObject2).getValue());
        }
        else
        {
          int i;
          Object localObject3;
          boolean bool = false;
        }
      }
      catch (UnsupportedEncodingException paramHttpClient)
      {
        paramHttpClient.printStackTrace();
        return localHashMap;
        i = 0;
        if (i < this.attachmentUris.size())
        {
          localObject2 = (Uri)this.attachmentUris.get(i);
          if (i == this.attachmentUris.size() - 1)
          {
            bool = true;
            localObject1 = this.context.getContentResolver().openInputStream((Uri)localObject2);
            localObject2 = ((Uri)localObject2).getLastPathSegment();
            localSimpleMultipartEntity.addPart("attachment" + i, (String)localObject2, (InputStream)localObject1, bool);
            i += 1;
          }
        }
        else
        {
          localSimpleMultipartEntity.writeLastBoundaryIfNeeds();
          localObject2 = null;
          localObject1 = null;
          if (this.token != null)
          {
            this.urlString = (this.urlString + this.token + "/");
            localObject1 = new HttpPut(this.urlString);
            localObject3 = null;
            if (localObject1 == null) {
              continue;
            }
            ((HttpPut)localObject1).setHeader("Content-type", "multipart/form-data; boundary=" + localSimpleMultipartEntity.getBoundary());
            ((HttpPut)localObject1).setEntity(localSimpleMultipartEntity);
            localObject1 = paramHttpClient.execute((HttpUriRequest)localObject1);
            if (localObject1 == null) {
              continue;
            }
            localHashMap.put("response", EntityUtils.toString(((HttpResponse)localObject1).getEntity()));
            localHashMap.put("status", "" + ((HttpResponse)localObject1).getStatusLine().getStatusCode());
            return localHashMap;
          }
        }
      }
      catch (ClientProtocolException paramHttpClient)
      {
        paramHttpClient.printStackTrace();
        return localHashMap;
        localObject2 = new HttpPost(this.urlString);
        continue;
        localObject1 = localObject3;
        if (localObject2 != null)
        {
          ((HttpPost)localObject2).setHeader("Content-type", "multipart/form-data; boundary=" + localSimpleMultipartEntity.getBoundary());
          ((HttpPost)localObject2).setEntity(localSimpleMultipartEntity);
          localObject1 = paramHttpClient.execute((HttpUriRequest)localObject2);
        }
      }
      catch (IOException paramHttpClient)
      {
        paramHttpClient.printStackTrace();
        return localHashMap;
      }
    }
  }
  
  public void attach(Context paramContext)
  {
    this.context = paramContext;
  }
  
  public void detach()
  {
    this.context = null;
    this.progressDialog = null;
  }
  
  protected HashMap<String, String> doInBackground(Void... paramVarArgs)
  {
    paramVarArgs = ConnectionManager.getInstance().getHttpClient();
    if ((this.isFetchMessages) && (this.token != null)) {
      paramVarArgs = doGet(paramVarArgs);
    }
    HashMap localHashMap;
    do
    {
      do
      {
        do
        {
          do
          {
            return paramVarArgs;
            if (this.isFetchMessages) {
              break;
            }
            if (this.attachmentUris.isEmpty()) {
              return doPostPut(paramVarArgs);
            }
            localHashMap = doPostPutWithAttachments(paramVarArgs);
            localObject = (String)localHashMap.get("status");
            paramVarArgs = localHashMap;
          } while (localObject == null);
          paramVarArgs = localHashMap;
        } while (!((String)localObject).startsWith("2"));
        paramVarArgs = localHashMap;
      } while (this.context == null);
      localObject = new File(this.context.getCacheDir(), "HockeyApp");
      paramVarArgs = localHashMap;
    } while (!((File)localObject).exists());
    Object localObject = ((File)localObject).listFiles();
    int j = localObject.length;
    int i = 0;
    for (;;)
    {
      paramVarArgs = localHashMap;
      if (i >= j) {
        break;
      }
      localObject[i].delete();
      i += 1;
    }
    return null;
  }
  
  protected void onPostExecute(HashMap<String, String> paramHashMap)
  {
    if (this.progressDialog != null) {}
    try
    {
      this.progressDialog.dismiss();
      if (this.handler != null)
      {
        Message localMessage = new Message();
        localBundle = new Bundle();
        if (paramHashMap != null)
        {
          localBundle.putString("request_type", (String)paramHashMap.get("type"));
          localBundle.putString("feedback_response", (String)paramHashMap.get("response"));
          localBundle.putString("feedback_status", (String)paramHashMap.get("status"));
          localMessage.setData(localBundle);
          this.handler.sendMessage(localMessage);
        }
      }
      else
      {
        return;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Bundle localBundle;
        localException.printStackTrace();
        continue;
        localBundle.putString("request_type", "unknown");
      }
    }
  }
  
  protected void onPreExecute()
  {
    String str = "Sending feedback..";
    if (this.isFetchMessages) {
      str = "Retrieving discussions...";
    }
    if (((this.progressDialog == null) || (!this.progressDialog.isShowing())) && (this.showProgressDialog)) {
      this.progressDialog = ProgressDialog.show(this.context, "", str, true, false);
    }
  }
  
  public void setLastMessageId(int paramInt)
  {
    this.lastMessageId = paramInt;
  }
  
  public void setShowProgressDialog(boolean paramBoolean)
  {
    this.showProgressDialog = paramBoolean;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\tasks\SendFeedbackTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */