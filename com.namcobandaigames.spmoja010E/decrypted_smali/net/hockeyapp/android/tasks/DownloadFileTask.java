package net.hockeyapp.android.tasks;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Environment;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;
import net.hockeyapp.android.Strings;
import net.hockeyapp.android.listeners.DownloadFileListener;

public class DownloadFileTask
  extends AsyncTask<Void, Integer, Long>
{
  protected static final int MAX_REDIRECTS = 6;
  protected Context context;
  private String downloadErrorMessage;
  protected String filePath;
  protected String filename;
  protected DownloadFileListener notifier;
  protected ProgressDialog progressDialog;
  protected String urlString;
  
  public DownloadFileTask(Context paramContext, String paramString, DownloadFileListener paramDownloadFileListener)
  {
    this.context = paramContext;
    this.urlString = paramString;
    this.filename = (UUID.randomUUID() + ".apk");
    this.filePath = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download");
    this.notifier = paramDownloadFileListener;
    this.downloadErrorMessage = null;
  }
  
  public void attach(Context paramContext)
  {
    this.context = paramContext;
  }
  
  protected URLConnection createConnection(URL paramURL, int paramInt)
    throws IOException
  {
    HttpURLConnection localHttpURLConnection = (HttpURLConnection)paramURL.openConnection();
    setConnectionProperties(localHttpURLConnection);
    int i = localHttpURLConnection.getResponseCode();
    if (((i != 301) && (i != 302) && (i != 303)) || (paramInt == 0)) {}
    URL localURL;
    do
    {
      return localHttpURLConnection;
      localURL = new URL(localHttpURLConnection.getHeaderField("Location"));
    } while (paramURL.getProtocol().equals(localURL.getProtocol()));
    localHttpURLConnection.disconnect();
    return createConnection(localURL, paramInt - 1);
  }
  
  public void detach()
  {
    this.context = null;
    this.progressDialog = null;
  }
  
  protected Long doInBackground(Void... paramVarArgs)
  {
    int i;
    try
    {
      paramVarArgs = createConnection(new URL(getURLString()), 6);
      paramVarArgs.connect();
      i = paramVarArgs.getContentLength();
      localObject = paramVarArgs.getContentType();
      if ((localObject != null) && (((String)localObject).contains("text")))
      {
        this.downloadErrorMessage = "The requested download does not appear to be a file.";
        return Long.valueOf(0L);
      }
      localObject = new File(this.filePath);
      if ((!((File)localObject).mkdirs()) && (!((File)localObject).exists())) {
        throw new IOException("Could not create the dir(s):" + ((File)localObject).getAbsolutePath());
      }
    }
    catch (Exception paramVarArgs)
    {
      paramVarArgs.printStackTrace();
      return Long.valueOf(0L);
    }
    Object localObject = new File((File)localObject, this.filename);
    paramVarArgs = new BufferedInputStream(paramVarArgs.getInputStream());
    localObject = new FileOutputStream((File)localObject);
    byte[] arrayOfByte = new byte['Ѐ'];
    long l = 0L;
    for (;;)
    {
      int j = paramVarArgs.read(arrayOfByte);
      if (j == -1) {
        break;
      }
      l += j;
      publishProgress(new Integer[] { Integer.valueOf(Math.round((float)l * 100.0F / i)) });
      ((OutputStream)localObject).write(arrayOfByte, 0, j);
    }
    ((OutputStream)localObject).flush();
    ((OutputStream)localObject).close();
    paramVarArgs.close();
    return Long.valueOf(l);
  }
  
  protected String getURLString()
  {
    return this.urlString + "&type=apk";
  }
  
  protected void onPostExecute(Long paramLong)
  {
    if (this.progressDialog != null) {}
    try
    {
      this.progressDialog.dismiss();
      if (paramLong.longValue() > 0L)
      {
        this.notifier.downloadSuccessful(this);
        paramLong = new Intent("android.intent.action.VIEW");
        paramLong.setDataAndType(Uri.fromFile(new File(this.filePath, this.filename)), "application/vnd.android.package-archive");
        paramLong.setFlags(268435456);
        this.context.startActivity(paramLong);
        return;
      }
    }
    catch (Exception localException)
    {
      try
      {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.context);
        localBuilder.setTitle(Strings.get(this.notifier, 256));
        if (this.downloadErrorMessage == null) {}
        for (paramLong = Strings.get(this.notifier, 257);; paramLong = this.downloadErrorMessage)
        {
          localBuilder.setMessage(paramLong);
          localBuilder.setNegativeButton(Strings.get(this.notifier, 258), new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              DownloadFileTask.this.notifier.downloadFailed(DownloadFileTask.this, Boolean.valueOf(false));
            }
          });
          localBuilder.setPositiveButton(Strings.get(this.notifier, 259), new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              DownloadFileTask.this.notifier.downloadFailed(DownloadFileTask.this, Boolean.valueOf(true));
            }
          });
          localBuilder.create().show();
          return;
        }
        localException = localException;
      }
      catch (Exception paramLong) {}
    }
  }
  
  protected void onProgressUpdate(Integer... paramVarArgs)
  {
    try
    {
      if (this.progressDialog == null)
      {
        this.progressDialog = new ProgressDialog(this.context);
        this.progressDialog.setProgressStyle(1);
        this.progressDialog.setMessage("Loading...");
        this.progressDialog.setCancelable(false);
        this.progressDialog.show();
      }
      this.progressDialog.setProgress(paramVarArgs[0].intValue());
      return;
    }
    catch (Exception paramVarArgs) {}
  }
  
  protected void setConnectionProperties(HttpURLConnection paramHttpURLConnection)
  {
    paramHttpURLConnection.addRequestProperty("User-Agent", "HockeySDK/Android");
    paramHttpURLConnection.setInstanceFollowRedirects(true);
    if (Build.VERSION.SDK_INT <= 9) {
      paramHttpURLConnection.setRequestProperty("connection", "close");
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\tasks\DownloadFileTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */