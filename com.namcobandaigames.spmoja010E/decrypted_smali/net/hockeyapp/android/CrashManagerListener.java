package net.hockeyapp.android;

public abstract class CrashManagerListener
  extends StringListener
{
  public String getContact()
  {
    return null;
  }
  
  public String getDescription()
  {
    return null;
  }
  
  public String getUserID()
  {
    return null;
  }
  
  public boolean ignoreDefaultHandler()
  {
    return false;
  }
  
  public boolean includeDeviceData()
  {
    return true;
  }
  
  public boolean includeDeviceIdentifier()
  {
    return true;
  }
  
  public void onConfirmedCrashesFound() {}
  
  public boolean onCrashesFound()
  {
    return false;
  }
  
  public void onCrashesNotSent() {}
  
  public void onCrashesSent() {}
  
  public void onNewCrashesFound() {}
  
  public void onUserDeniedCrashes() {}
  
  public boolean shouldAutoUploadCrashes()
  {
    return false;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\CrashManagerListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */