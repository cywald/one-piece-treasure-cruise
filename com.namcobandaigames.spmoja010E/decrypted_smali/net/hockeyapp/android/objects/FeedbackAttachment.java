package net.hockeyapp.android.objects;

import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import net.hockeyapp.android.Constants;

public class FeedbackAttachment
  implements Serializable
{
  private static final long serialVersionUID = 5059651319640956830L;
  private String createdAt;
  private String filename;
  private int id;
  private int messageId;
  private String updatedAt;
  private String url;
  
  public String getCacheId()
  {
    return "" + this.messageId + this.id;
  }
  
  public String getCreatedAt()
  {
    return this.createdAt;
  }
  
  public String getFilename()
  {
    return this.filename;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public int getMessageId()
  {
    return this.messageId;
  }
  
  public String getUpdatedAt()
  {
    return this.updatedAt;
  }
  
  public String getUrl()
  {
    return this.url;
  }
  
  public boolean isAvailableInCache()
  {
    Object localObject = Constants.getHockeyAppStorageDir();
    if ((((File)localObject).exists()) && (((File)localObject).isDirectory()))
    {
      localObject = ((File)localObject).listFiles(new FilenameFilter()
      {
        public boolean accept(File paramAnonymousFile, String paramAnonymousString)
        {
          return paramAnonymousString.equals(FeedbackAttachment.this.getCacheId());
        }
      });
      return (localObject != null) && (localObject.length == 1);
    }
    return false;
  }
  
  public void setCreatedAt(String paramString)
  {
    this.createdAt = paramString;
  }
  
  public void setFilename(String paramString)
  {
    this.filename = paramString;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setMessageId(int paramInt)
  {
    this.messageId = paramInt;
  }
  
  public void setUpdatedAt(String paramString)
  {
    this.updatedAt = paramString;
  }
  
  public void setUrl(String paramString)
  {
    this.url = paramString;
  }
  
  public String toString()
  {
    return "\n" + FeedbackAttachment.class.getSimpleName() + "\n" + "id         " + this.id + "\n" + "message id " + this.messageId + "\n" + "filename   " + this.filename + "\n" + "url        " + this.url + "\n" + "createdAt  " + this.createdAt + "\n" + "updatedAt  " + this.updatedAt;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\objects\FeedbackAttachment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */