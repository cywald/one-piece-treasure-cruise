package net.hockeyapp.android.objects;

import java.io.Serializable;

public class ErrorObject
  implements Serializable
{
  private static final long serialVersionUID = 1508110658372169868L;
  private int code;
  private String message;
  
  public int getCode()
  {
    return this.code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setCode(int paramInt)
  {
    this.code = paramInt;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\objects\ErrorObject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */