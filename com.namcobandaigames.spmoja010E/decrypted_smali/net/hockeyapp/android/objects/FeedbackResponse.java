package net.hockeyapp.android.objects;

import java.io.Serializable;

public class FeedbackResponse
  implements Serializable
{
  private static final long serialVersionUID = -1093570359639034766L;
  private Feedback feedback;
  private String status;
  private String token;
  
  public Feedback getFeedback()
  {
    return this.feedback;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getToken()
  {
    return this.token;
  }
  
  public void setFeedback(Feedback paramFeedback)
  {
    this.feedback = paramFeedback;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
  
  public void setToken(String paramString)
  {
    this.token = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\objects\FeedbackResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */