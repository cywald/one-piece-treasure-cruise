package net.hockeyapp.android.objects;

import java.io.Serializable;
import java.util.List;

public class FeedbackMessage
  implements Serializable
{
  private static final long serialVersionUID = -8773015828853994624L;
  private String appId;
  private String cleanText;
  private String createdAt;
  private List<FeedbackAttachment> feedbackAttachments;
  private int id;
  private String model;
  private String name;
  private String oem;
  private String osVersion;
  private String subject;
  private String text;
  private String token;
  private String userString;
  private int via;
  
  public String getAppId()
  {
    return this.appId;
  }
  
  public String getCleanText()
  {
    return this.cleanText;
  }
  
  public String getCreatedAt()
  {
    return this.createdAt;
  }
  
  public List<FeedbackAttachment> getFeedbackAttachments()
  {
    return this.feedbackAttachments;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public String getModel()
  {
    return this.model;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getOem()
  {
    return this.oem;
  }
  
  public String getOsVersion()
  {
    return this.osVersion;
  }
  
  public String getSubjec()
  {
    return this.subject;
  }
  
  public String getText()
  {
    return this.text;
  }
  
  public String getToken()
  {
    return this.token;
  }
  
  public String getUserString()
  {
    return this.userString;
  }
  
  public int getVia()
  {
    return this.via;
  }
  
  public void setAppId(String paramString)
  {
    this.appId = paramString;
  }
  
  public void setCleanText(String paramString)
  {
    this.cleanText = paramString;
  }
  
  public void setCreatedAt(String paramString)
  {
    this.createdAt = paramString;
  }
  
  public void setFeedbackAttachments(List<FeedbackAttachment> paramList)
  {
    this.feedbackAttachments = paramList;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setModel(String paramString)
  {
    this.model = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setOem(String paramString)
  {
    this.oem = paramString;
  }
  
  public void setOsVersion(String paramString)
  {
    this.osVersion = paramString;
  }
  
  public void setSubjec(String paramString)
  {
    this.subject = paramString;
  }
  
  public void setText(String paramString)
  {
    this.text = paramString;
  }
  
  public void setToken(String paramString)
  {
    this.token = paramString;
  }
  
  public void setUserString(String paramString)
  {
    this.userString = paramString;
  }
  
  public void setVia(int paramInt)
  {
    this.via = paramInt;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\objects\FeedbackMessage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */