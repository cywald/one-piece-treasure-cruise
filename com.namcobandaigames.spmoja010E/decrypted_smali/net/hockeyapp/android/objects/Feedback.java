package net.hockeyapp.android.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class Feedback
  implements Serializable
{
  private static final long serialVersionUID = 2590172806951065320L;
  private String createdAt;
  private String email;
  private int id;
  private ArrayList<FeedbackMessage> messages;
  private String name;
  
  public String getCreatedAt()
  {
    return this.createdAt;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public ArrayList<FeedbackMessage> getMessages()
  {
    return this.messages;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setCreatedAt(String paramString)
  {
    this.createdAt = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.email = paramString;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setMessages(ArrayList<FeedbackMessage> paramArrayList)
  {
    this.messages = paramArrayList;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\objects\Feedback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */