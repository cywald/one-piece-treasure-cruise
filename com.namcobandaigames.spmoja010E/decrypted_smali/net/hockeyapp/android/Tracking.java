package net.hockeyapp.android;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import net.hockeyapp.android.utils.PrefsUtil;

public class Tracking
{
  private static final String START_TIME_KEY = "startTime";
  private static final String USAGE_TIME_KEY = "usageTime";
  
  private static boolean checkVersion(Context paramContext)
  {
    if (Constants.APP_VERSION == null)
    {
      Constants.loadFromContext(paramContext);
      if (Constants.APP_VERSION == null) {
        return false;
      }
    }
    return true;
  }
  
  private static SharedPreferences getPreferences(Context paramContext)
  {
    return paramContext.getSharedPreferences("HockeyApp", 0);
  }
  
  public static long getUsageTime(Context paramContext)
  {
    if (!checkVersion(paramContext)) {
      return 0L;
    }
    return getPreferences(paramContext).getLong("usageTime" + Constants.APP_VERSION, 0L) / 1000L;
  }
  
  public static void startUsage(Activity paramActivity)
  {
    long l = System.currentTimeMillis();
    if (paramActivity == null) {
      return;
    }
    SharedPreferences.Editor localEditor = getPreferences(paramActivity).edit();
    localEditor.putLong("startTime" + paramActivity.hashCode(), l);
    PrefsUtil.applyChanges(localEditor);
  }
  
  public static void stopUsage(Activity paramActivity)
  {
    long l1 = System.currentTimeMillis();
    if (paramActivity == null) {}
    SharedPreferences localSharedPreferences;
    long l2;
    long l3;
    do
    {
      do
      {
        return;
      } while (!checkVersion(paramActivity));
      localSharedPreferences = getPreferences(paramActivity);
      l2 = localSharedPreferences.getLong("startTime" + paramActivity.hashCode(), 0L);
      l3 = localSharedPreferences.getLong("usageTime" + Constants.APP_VERSION, 0L);
    } while (l2 <= 0L);
    paramActivity = localSharedPreferences.edit();
    paramActivity.putLong("usageTime" + Constants.APP_VERSION, l3 + (l1 - l2));
    PrefsUtil.applyChanges(paramActivity);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\Tracking.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */