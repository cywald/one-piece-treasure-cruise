package net.hockeyapp.android.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import net.hockeyapp.android.objects.FeedbackAttachment;
import net.hockeyapp.android.objects.FeedbackMessage;
import net.hockeyapp.android.tasks.AttachmentDownloader;
import net.hockeyapp.android.views.AttachmentListView;
import net.hockeyapp.android.views.AttachmentView;
import net.hockeyapp.android.views.FeedbackMessageView;

public class MessagesAdapter
  extends BaseAdapter
{
  private AttachmentListView attachmentListView;
  private TextView authorTextView;
  private Context context;
  private Date date;
  private TextView dateTextView;
  private SimpleDateFormat format;
  private SimpleDateFormat formatNew;
  private TextView messageTextView;
  private ArrayList<FeedbackMessage> messagesList;
  
  public MessagesAdapter(Context paramContext, ArrayList<FeedbackMessage> paramArrayList)
  {
    this.context = paramContext;
    this.messagesList = paramArrayList;
    this.format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    this.formatNew = new SimpleDateFormat("d MMM h:mm a");
  }
  
  public void add(FeedbackMessage paramFeedbackMessage)
  {
    if ((paramFeedbackMessage != null) && (this.messagesList != null)) {
      this.messagesList.add(paramFeedbackMessage);
    }
  }
  
  public void clear()
  {
    if (this.messagesList != null) {
      this.messagesList.clear();
    }
  }
  
  public int getCount()
  {
    return this.messagesList.size();
  }
  
  public Object getItem(int paramInt)
  {
    return this.messagesList.get(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramViewGroup = (FeedbackMessage)this.messagesList.get(paramInt);
    if (paramView == null) {
      paramView = new FeedbackMessageView(this.context);
    }
    for (;;)
    {
      if (paramViewGroup != null)
      {
        this.authorTextView = ((TextView)paramView.findViewById(12289));
        this.dateTextView = ((TextView)paramView.findViewById(12290));
        this.messageTextView = ((TextView)paramView.findViewById(12291));
        this.attachmentListView = ((AttachmentListView)paramView.findViewById(12292));
        try
        {
          this.date = this.format.parse(paramViewGroup.getCreatedAt());
          this.dateTextView.setText(this.formatNew.format(this.date));
          this.authorTextView.setText(paramViewGroup.getName());
          this.messageTextView.setText(paramViewGroup.getText());
          this.attachmentListView.removeAllViews();
          paramViewGroup = paramViewGroup.getFeedbackAttachments().iterator();
          while (paramViewGroup.hasNext())
          {
            FeedbackAttachment localFeedbackAttachment = (FeedbackAttachment)paramViewGroup.next();
            AttachmentView localAttachmentView = new AttachmentView(this.context, this.attachmentListView, localFeedbackAttachment, false);
            AttachmentDownloader.getInstance().download(localFeedbackAttachment, localAttachmentView);
            this.attachmentListView.addView(localAttachmentView);
            continue;
            paramView = (FeedbackMessageView)paramView;
          }
        }
        catch (ParseException localParseException)
        {
          for (;;)
          {
            localParseException.printStackTrace();
          }
        }
      }
    }
    if (paramInt % 2 == 0) {}
    for (paramInt = 0;; paramInt = 1)
    {
      paramView.setFeedbackMessageViewBgAndTextColor(paramInt);
      return paramView;
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\adapters\MessagesAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */