package net.hockeyapp.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import net.hockeyapp.android.tasks.LoginTask;
import net.hockeyapp.android.utils.AsyncTaskUtils;
import net.hockeyapp.android.views.LoginView;

public class LoginActivity
  extends Activity
  implements View.OnClickListener
{
  private Handler loginHandler;
  private LoginTask loginTask;
  private int mode;
  private String secret;
  private String url;
  
  private void configureView()
  {
    if (this.mode == 1) {
      ((EditText)findViewById(12292)).setVisibility(4);
    }
    ((Button)findViewById(12293)).setOnClickListener(this);
  }
  
  private void initLoginHandler()
  {
    this.loginHandler = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        if (paramAnonymousMessage.getData().getBoolean("success"))
        {
          LoginActivity.this.finish();
          if (LoginManager.listener != null) {
            LoginManager.listener.onSuccess();
          }
          return;
        }
        Toast.makeText(LoginActivity.this, "Login failed. Check your credentials.", 2000).show();
      }
    };
  }
  
  private void performAuthentication()
  {
    String str1 = ((EditText)findViewById(12291)).getText().toString();
    String str2 = ((EditText)findViewById(12292)).getText().toString();
    int i = 0;
    HashMap localHashMap = new HashMap();
    if (this.mode == 1) {
      if (!TextUtils.isEmpty(str1))
      {
        i = 1;
        localHashMap.put("email", str1);
        localHashMap.put("authcode", md5(this.secret + str1));
      }
    }
    for (;;)
    {
      if (i != 0)
      {
        this.loginTask = new LoginTask(this, this.loginHandler, this.url, this.mode, localHashMap);
        AsyncTaskUtils.execute(this.loginTask);
        return;
        i = 0;
        break;
        if (this.mode == 2)
        {
          if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2))) {}
          for (i = 1;; i = 0)
          {
            localHashMap.put("email", str1);
            localHashMap.put("password", str2);
            break;
          }
        }
      }
    }
    Toast.makeText(this, Strings.get(1281), 1000).show();
  }
  
  public String md5(String paramString)
  {
    try
    {
      Object localObject = MessageDigest.getInstance("MD5");
      ((MessageDigest)localObject).update(paramString.getBytes());
      localObject = ((MessageDigest)localObject).digest();
      StringBuilder localStringBuilder = new StringBuilder();
      int j = localObject.length;
      int i = 0;
      while (i < j)
      {
        for (paramString = Integer.toHexString(localObject[i] & 0xFF); paramString.length() < 2; paramString = "0" + paramString) {}
        localStringBuilder.append(paramString);
        i += 1;
      }
      paramString = localStringBuilder.toString();
      return paramString;
    }
    catch (NoSuchAlgorithmException paramString)
    {
      paramString.printStackTrace();
    }
    return "";
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default: 
      return;
    }
    performAuthentication();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(new LoginView(this));
    paramBundle = getIntent().getExtras();
    if (paramBundle != null)
    {
      this.url = paramBundle.getString("url");
      this.secret = paramBundle.getString("secret");
      this.mode = paramBundle.getInt("mode");
    }
    configureView();
    initLoginHandler();
    paramBundle = getLastNonConfigurationInstance();
    if (paramBundle != null)
    {
      this.loginTask = ((LoginTask)paramBundle);
      this.loginTask.attach(this, this.loginHandler);
    }
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      if (LoginManager.listener != null) {
        LoginManager.listener.onBack();
      }
    }
    else {
      return super.onKeyDown(paramInt, paramKeyEvent);
    }
    paramKeyEvent = new Intent(this, LoginManager.mainActivity);
    paramKeyEvent.setFlags(67108864);
    paramKeyEvent.putExtra("net.hockeyapp.android.EXIT", true);
    startActivity(paramKeyEvent);
    return true;
  }
  
  public Object onRetainNonConfigurationInstance()
  {
    if (this.loginTask != null) {
      this.loginTask.detach();
    }
    return this.loginTask;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\LoginActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */