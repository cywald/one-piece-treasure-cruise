package net.hockeyapp.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;

public class PrefsUtil
{
  private SharedPreferences feedbackTokenPrefs;
  private SharedPreferences.Editor feedbackTokenPrefsEditor;
  private SharedPreferences nameEmailSubjectPrefs;
  private SharedPreferences.Editor nameEmailSubjectPrefsEditor;
  
  public static void applyChanges(SharedPreferences.Editor paramEditor)
  {
    if (applySupported().booleanValue())
    {
      paramEditor.apply();
      return;
    }
    paramEditor.commit();
  }
  
  public static Boolean applySupported()
  {
    try
    {
      if (Build.VERSION.SDK_INT >= 9) {}
      for (boolean bool = true;; bool = false) {
        return Boolean.valueOf(bool);
      }
      return Boolean.valueOf(false);
    }
    catch (NoClassDefFoundError localNoClassDefFoundError) {}
  }
  
  public static PrefsUtil getInstance()
  {
    return PrefsUtilHolder.INSTANCE;
  }
  
  public String getFeedbackTokenFromPrefs(Context paramContext)
  {
    if (paramContext == null) {}
    do
    {
      return null;
      this.feedbackTokenPrefs = paramContext.getSharedPreferences("net.hockeyapp.android.prefs_feedback_token", 0);
    } while (this.feedbackTokenPrefs == null);
    return this.feedbackTokenPrefs.getString("net.hockeyapp.android.prefs_key_feedback_token", null);
  }
  
  public String getNameEmailFromPrefs(Context paramContext)
  {
    if (paramContext == null) {}
    do
    {
      return null;
      this.nameEmailSubjectPrefs = paramContext.getSharedPreferences("net.hockeyapp.android.prefs_name_email", 0);
    } while (this.nameEmailSubjectPrefs == null);
    return this.nameEmailSubjectPrefs.getString("net.hockeyapp.android.prefs_key_name_email", null);
  }
  
  public void saveFeedbackTokenToPrefs(Context paramContext, String paramString)
  {
    if (paramContext != null)
    {
      this.feedbackTokenPrefs = paramContext.getSharedPreferences("net.hockeyapp.android.prefs_feedback_token", 0);
      if (this.feedbackTokenPrefs != null)
      {
        this.feedbackTokenPrefsEditor = this.feedbackTokenPrefs.edit();
        this.feedbackTokenPrefsEditor.putString("net.hockeyapp.android.prefs_key_feedback_token", paramString);
        applyChanges(this.feedbackTokenPrefsEditor);
      }
    }
  }
  
  public void saveNameEmailSubjectToPrefs(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    if (paramContext != null)
    {
      this.nameEmailSubjectPrefs = paramContext.getSharedPreferences("net.hockeyapp.android.prefs_name_email", 0);
      if (this.nameEmailSubjectPrefs != null)
      {
        this.nameEmailSubjectPrefsEditor = this.nameEmailSubjectPrefs.edit();
        if ((paramString1 != null) && (paramString2 != null) && (paramString3 != null)) {
          break label69;
        }
        this.nameEmailSubjectPrefsEditor.putString("net.hockeyapp.android.prefs_key_name_email", null);
      }
    }
    for (;;)
    {
      applyChanges(this.nameEmailSubjectPrefsEditor);
      return;
      label69:
      this.nameEmailSubjectPrefsEditor.putString("net.hockeyapp.android.prefs_key_name_email", String.format("%s|%s|%s", new Object[] { paramString1, paramString2, paramString3 }));
    }
  }
  
  private static class PrefsUtilHolder
  {
    public static final PrefsUtil INSTANCE = new PrefsUtil(null);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\utils\PrefsUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */