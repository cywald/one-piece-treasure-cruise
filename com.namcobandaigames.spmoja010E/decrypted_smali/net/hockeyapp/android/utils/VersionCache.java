package net.hockeyapp.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class VersionCache
{
  private static String VERSION_INFO_KEY = "versionInfo";
  
  public static String getVersionInfo(Context paramContext)
  {
    if (paramContext != null) {
      return paramContext.getSharedPreferences("HockeyApp", 0).getString(VERSION_INFO_KEY, "[]");
    }
    return "[]";
  }
  
  public static void setVersionInfo(Context paramContext, String paramString)
  {
    if (paramContext != null)
    {
      paramContext = paramContext.getSharedPreferences("HockeyApp", 0).edit();
      paramContext.putString(VERSION_INFO_KEY, paramString);
      PrefsUtil.applyChanges(paramContext);
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\utils\VersionCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */