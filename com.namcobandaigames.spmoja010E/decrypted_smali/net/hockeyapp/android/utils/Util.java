package net.hockeyapp.android.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Patterns;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util
{
  public static final int APP_IDENTIFIER_LENGTH = 32;
  public static final String APP_IDENTIFIER_PATTERN = "[0-9a-f]+";
  public static final String LOG_IDENTIFIER = "HockeyApp";
  public static final String PREFS_FEEDBACK_TOKEN = "net.hockeyapp.android.prefs_feedback_token";
  public static final String PREFS_KEY_FEEDBACK_TOKEN = "net.hockeyapp.android.prefs_key_feedback_token";
  public static final String PREFS_KEY_NAME_EMAIL_SUBJECT = "net.hockeyapp.android.prefs_key_name_email";
  public static final String PREFS_NAME_EMAIL_SUBJECT = "net.hockeyapp.android.prefs_name_email";
  private static final Pattern appIdentifierPattern = Pattern.compile("[0-9a-f]+", 2);
  
  public static String encodeParam(String paramString)
  {
    try
    {
      paramString = URLEncoder.encode(paramString, "UTF-8");
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      paramString.printStackTrace();
    }
    return "";
  }
  
  @SuppressLint({"NewApi"})
  public static Boolean fragmentsSupported()
  {
    try
    {
      if ((Build.VERSION.SDK_INT >= 11) && (Fragment.class != null)) {}
      for (boolean bool = true;; bool = false) {
        return Boolean.valueOf(bool);
      }
      return Boolean.valueOf(false);
    }
    catch (NoClassDefFoundError localNoClassDefFoundError) {}
  }
  
  @TargetApi(8)
  public static final boolean isValidEmail(String paramString)
  {
    if (Build.VERSION.SDK_INT >= 8) {
      if ((TextUtils.isEmpty(paramString)) || (!Patterns.EMAIL_ADDRESS.matcher(paramString).matches())) {}
    }
    while (!TextUtils.isEmpty(paramString))
    {
      return true;
      return false;
    }
    return false;
  }
  
  public static Boolean runsOnTablet(WeakReference<Activity> paramWeakReference)
  {
    boolean bool = false;
    if (paramWeakReference != null)
    {
      paramWeakReference = (Activity)paramWeakReference.get();
      if (paramWeakReference != null)
      {
        paramWeakReference = paramWeakReference.getResources().getConfiguration();
        if (((paramWeakReference.screenLayout & 0xF) == 3) || ((paramWeakReference.screenLayout & 0xF) == 4)) {
          bool = true;
        }
        return Boolean.valueOf(bool);
      }
    }
    return Boolean.valueOf(false);
  }
  
  public static String sanitizeAppIdentifier(String paramString)
    throws IllegalArgumentException
  {
    if (paramString == null) {
      throw new IllegalArgumentException("App ID must not be null.");
    }
    paramString = paramString.trim();
    Matcher localMatcher = appIdentifierPattern.matcher(paramString);
    if (paramString.length() != 32) {
      throw new IllegalArgumentException("App ID length must be 32 characters.");
    }
    if (!localMatcher.matches()) {
      throw new IllegalArgumentException("App ID must match regex pattern /[0-9a-f]+/i");
    }
    return paramString;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\utils\Util.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */