package net.hockeyapp.android.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

public class SimpleMultipartEntity
  implements HttpEntity
{
  private static final char[] BOUNDARY_CHARS = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
  private String boundary;
  private boolean isSetFirst = false;
  private boolean isSetLast = false;
  private ByteArrayOutputStream out = new ByteArrayOutputStream();
  
  public SimpleMultipartEntity()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    int i = 0;
    while (i < 30)
    {
      localStringBuffer.append(BOUNDARY_CHARS[localRandom.nextInt(BOUNDARY_CHARS.length)]);
      i += 1;
    }
    this.boundary = localStringBuffer.toString();
  }
  
  public void addPart(String paramString, File paramFile, boolean paramBoolean)
    throws IOException
  {
    addPart(paramString, paramFile.getName(), new FileInputStream(paramFile), paramBoolean);
  }
  
  public void addPart(String paramString1, String paramString2)
    throws IOException
  {
    writeFirstBoundaryIfNeeds();
    this.out.write(("Content-Disposition: form-data; name=\"" + paramString1 + "\"\r\n").getBytes());
    this.out.write("Content-Type: text/plain; charset=UTF-8\r\n".getBytes());
    this.out.write("Content-Transfer-Encoding: 8bit\r\n\r\n".getBytes());
    this.out.write(paramString2.getBytes());
    this.out.write(("\r\n--" + this.boundary + "\r\n").getBytes());
  }
  
  public void addPart(String paramString1, String paramString2, InputStream paramInputStream, String paramString3, boolean paramBoolean)
    throws IOException
  {
    writeFirstBoundaryIfNeeds();
    try
    {
      paramString3 = "Content-Type: " + paramString3 + "\r\n";
      this.out.write(("Content-Disposition: form-data; name=\"" + paramString1 + "\"; filename=\"" + paramString2 + "\"\r\n").getBytes());
      this.out.write(paramString3.getBytes());
      this.out.write("Content-Transfer-Encoding: binary\r\n\r\n".getBytes());
      paramString1 = new byte['က'];
      for (;;)
      {
        int i = paramInputStream.read(paramString1);
        if (i == -1) {
          break;
        }
        this.out.write(paramString1, 0, i);
      }
      try
      {
        paramInputStream.close();
        throw paramString1;
        this.out.flush();
        if (paramBoolean) {
          writeLastBoundaryIfNeeds();
        }
        for (;;)
        {
          try
          {
            paramInputStream.close();
            return;
          }
          catch (IOException paramString1)
          {
            paramString1.printStackTrace();
            return;
          }
          this.out.write(("\r\n--" + this.boundary + "\r\n").getBytes());
        }
      }
      catch (IOException paramString2)
      {
        for (;;)
        {
          paramString2.printStackTrace();
        }
      }
    }
    finally {}
  }
  
  public void addPart(String paramString1, String paramString2, InputStream paramInputStream, boolean paramBoolean)
    throws IOException
  {
    addPart(paramString1, paramString2, paramInputStream, "application/octet-stream", paramBoolean);
  }
  
  public void consumeContent()
    throws IOException, UnsupportedOperationException
  {
    if (isStreaming()) {
      throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
    }
  }
  
  public String getBoundary()
  {
    return this.boundary;
  }
  
  public InputStream getContent()
    throws IOException, UnsupportedOperationException
  {
    return new ByteArrayInputStream(this.out.toByteArray());
  }
  
  public Header getContentEncoding()
  {
    return null;
  }
  
  public long getContentLength()
  {
    writeLastBoundaryIfNeeds();
    return this.out.toByteArray().length;
  }
  
  public Header getContentType()
  {
    return new BasicHeader("Content-Type", "multipart/form-data; boundary=" + getBoundary());
  }
  
  public boolean isChunked()
  {
    return false;
  }
  
  public boolean isRepeatable()
  {
    return false;
  }
  
  public boolean isStreaming()
  {
    return false;
  }
  
  public void writeFirstBoundaryIfNeeds()
    throws IOException
  {
    if (!this.isSetFirst) {
      this.out.write(("--" + this.boundary + "\r\n").getBytes());
    }
    this.isSetFirst = true;
  }
  
  public void writeLastBoundaryIfNeeds()
  {
    if (this.isSetLast) {
      return;
    }
    try
    {
      this.out.write(("\r\n--" + this.boundary + "--\r\n").getBytes());
      this.isSetLast = true;
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localIOException.printStackTrace();
      }
    }
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    paramOutputStream.write(this.out.toByteArray());
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\utils\SimpleMultipartEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */