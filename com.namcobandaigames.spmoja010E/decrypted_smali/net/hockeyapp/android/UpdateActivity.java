package net.hockeyapp.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import net.hockeyapp.android.listeners.DownloadFileListener;
import net.hockeyapp.android.objects.ErrorObject;
import net.hockeyapp.android.tasks.DownloadFileTask;
import net.hockeyapp.android.tasks.GetFileSizeTask;
import net.hockeyapp.android.utils.AsyncTaskUtils;
import net.hockeyapp.android.utils.VersionHelper;
import net.hockeyapp.android.views.UpdateView;

public class UpdateActivity
  extends Activity
  implements UpdateActivityInterface, UpdateInfoListener, View.OnClickListener
{
  private final int DIALOG_ERROR_ID = 0;
  private Context context;
  protected DownloadFileTask downloadTask;
  private ErrorObject error;
  protected VersionHelper versionHelper;
  
  private boolean isUnknownSourcesChecked()
  {
    try
    {
      if ((Build.VERSION.SDK_INT >= 17) && (Build.VERSION.SDK_INT < 21))
      {
        if (Settings.Global.getInt(getContentResolver(), "install_non_market_apps") != 1) {
          break label51;
        }
        return true;
      }
      int i = Settings.Secure.getInt(getContentResolver(), "install_non_market_apps");
      if (i != 1) {
        return false;
      }
    }
    catch (Settings.SettingNotFoundException localSettingNotFoundException) {}
    return true;
    label51:
    return false;
  }
  
  private boolean isWriteExternalStorageSet(Context paramContext)
  {
    return paramContext.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == 0;
  }
  
  protected void configureView()
  {
    ((TextView)findViewById(4098)).setText(getAppName());
    final TextView localTextView = (TextView)findViewById(4099);
    final String str1 = "Version " + this.versionHelper.getVersionString();
    final String str2 = this.versionHelper.getFileDateString();
    Object localObject = "Unknown size";
    long l = this.versionHelper.getFileSizeBytes();
    if (l >= 0L) {
      localObject = String.format("%.2f", new Object[] { Float.valueOf((float)l / 1048576.0F) }) + " MB";
    }
    for (;;)
    {
      localTextView.setText(str1 + "\n" + str2 + " - " + (String)localObject);
      ((Button)findViewById(4100)).setOnClickListener(this);
      localObject = (WebView)findViewById(4101);
      ((WebView)localObject).clearCache(true);
      ((WebView)localObject).destroyDrawingCache();
      ((WebView)localObject).loadDataWithBaseURL("https://sdk.hockeyapp.net/", getReleaseNotes(), "text/html", "utf-8", null);
      return;
      AsyncTaskUtils.execute(new GetFileSizeTask(this, getIntent().getStringExtra("url"), new DownloadFileListener()
      {
        public void downloadSuccessful(DownloadFileTask paramAnonymousDownloadFileTask)
        {
          if ((paramAnonymousDownloadFileTask instanceof GetFileSizeTask))
          {
            long l = ((GetFileSizeTask)paramAnonymousDownloadFileTask).getSize();
            paramAnonymousDownloadFileTask = String.format("%.2f", new Object[] { Float.valueOf((float)l / 1048576.0F) }) + " MB";
            localTextView.setText(str1 + "\n" + str2 + " - " + paramAnonymousDownloadFileTask);
          }
        }
      }));
    }
  }
  
  protected void createDownloadTask(String paramString, DownloadFileListener paramDownloadFileListener)
  {
    this.downloadTask = new DownloadFileTask(this, paramString, paramDownloadFileListener);
  }
  
  public void enableUpdateButton()
  {
    findViewById(4100).setEnabled(true);
  }
  
  public String getAppName()
  {
    try
    {
      Object localObject = getPackageManager();
      localObject = ((PackageManager)localObject).getApplicationLabel(((PackageManager)localObject).getApplicationInfo(getPackageName(), 0)).toString();
      return (String)localObject;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return "";
  }
  
  public int getCurrentVersionCode()
  {
    try
    {
      int i = getPackageManager().getPackageInfo(getPackageName(), 128).versionCode;
      return i;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return -1;
  }
  
  public ViewGroup getLayoutView()
  {
    return new UpdateView(this);
  }
  
  protected String getReleaseNotes()
  {
    return this.versionHelper.getReleaseNotes(false);
  }
  
  public void onClick(View paramView)
  {
    if (!isWriteExternalStorageSet(this.context))
    {
      this.error = new ErrorObject();
      this.error.setMessage("The permission to access the external storage permission is not set. Please contact the developer.");
      runOnUiThread(new Runnable()
      {
        public void run()
        {
          UpdateActivity.this.showDialog(0);
        }
      });
      return;
    }
    if (!isUnknownSourcesChecked())
    {
      this.error = new ErrorObject();
      this.error.setMessage("The installation from unknown sources is not enabled. Please check the device settings.");
      runOnUiThread(new Runnable()
      {
        public void run()
        {
          UpdateActivity.this.showDialog(0);
        }
      });
      return;
    }
    startDownloadTask();
    paramView.setEnabled(false);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setTitle("App Update");
    setContentView(getLayoutView());
    this.context = this;
    this.versionHelper = new VersionHelper(this, getIntent().getStringExtra("json"), this);
    configureView();
    this.downloadTask = ((DownloadFileTask)getLastNonConfigurationInstance());
    if (this.downloadTask != null) {
      this.downloadTask.attach(this);
    }
  }
  
  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    }
    new AlertDialog.Builder(this).setMessage("An error has occured").setCancelable(false).setTitle("Error").setIcon(17301543).setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        UpdateActivity.access$002(UpdateActivity.this, null);
        paramAnonymousDialogInterface.cancel();
      }
    }).create();
  }
  
  protected void onPrepareDialog(int paramInt, Dialog paramDialog)
  {
    switch (paramInt)
    {
    default: 
      return;
    }
    paramDialog = (AlertDialog)paramDialog;
    if (this.error != null)
    {
      paramDialog.setMessage(this.error.getMessage());
      return;
    }
    paramDialog.setMessage("An unknown error has occured.");
  }
  
  public Object onRetainNonConfigurationInstance()
  {
    if (this.downloadTask != null) {
      this.downloadTask.detach();
    }
    return this.downloadTask;
  }
  
  protected void startDownloadTask()
  {
    startDownloadTask(getIntent().getStringExtra("url"));
  }
  
  protected void startDownloadTask(String paramString)
  {
    createDownloadTask(paramString, new DownloadFileListener()
    {
      public void downloadFailed(DownloadFileTask paramAnonymousDownloadFileTask, Boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean.booleanValue())
        {
          UpdateActivity.this.startDownloadTask();
          return;
        }
        UpdateActivity.this.enableUpdateButton();
      }
      
      public void downloadSuccessful(DownloadFileTask paramAnonymousDownloadFileTask)
      {
        UpdateActivity.this.enableUpdateButton();
      }
      
      public String getStringForResource(int paramAnonymousInt)
      {
        UpdateManagerListener localUpdateManagerListener = UpdateManager.getLastListener();
        if (localUpdateManagerListener != null) {
          return localUpdateManagerListener.getStringForResource(paramAnonymousInt);
        }
        return null;
      }
    });
    AsyncTaskUtils.execute(this.downloadTask);
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\UpdateActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */