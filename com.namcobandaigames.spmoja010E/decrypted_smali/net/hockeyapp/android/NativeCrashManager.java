package net.hockeyapp.android;

import android.app.Activity;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.Date;
import java.util.UUID;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class NativeCrashManager
{
  public static String createLogFile()
  {
    Object localObject1 = new Date();
    try
    {
      String str = UUID.randomUUID().toString();
      Object localObject2 = Constants.FILES_PATH + "/" + str + ".faketrace";
      Log.d("HockeyApp", "Writing unhandled exception to: " + (String)localObject2);
      localObject2 = new BufferedWriter(new FileWriter((String)localObject2));
      ((BufferedWriter)localObject2).write("Package: " + Constants.APP_PACKAGE + "\n");
      ((BufferedWriter)localObject2).write("Version: " + Constants.APP_VERSION + "\n");
      ((BufferedWriter)localObject2).write("Android: " + Constants.ANDROID_VERSION + "\n");
      ((BufferedWriter)localObject2).write("Manufacturer: " + Constants.PHONE_MANUFACTURER + "\n");
      ((BufferedWriter)localObject2).write("Model: " + Constants.PHONE_MODEL + "\n");
      ((BufferedWriter)localObject2).write("Date: " + localObject1 + "\n");
      ((BufferedWriter)localObject2).write("\n");
      ((BufferedWriter)localObject2).write("MinidumpContainer");
      ((BufferedWriter)localObject2).flush();
      ((BufferedWriter)localObject2).close();
      localObject1 = str + ".faketrace";
      return (String)localObject1;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public static void handleDumpFiles(Activity paramActivity, String paramString)
  {
    handleDumpFiles(paramActivity, paramString, null, null);
  }
  
  public static void handleDumpFiles(Activity paramActivity, String paramString1, String paramString2, String paramString3)
  {
    String[] arrayOfString = searchForDumpFiles();
    int j = arrayOfString.length;
    int i = 0;
    while (i < j)
    {
      String str1 = arrayOfString[i];
      String str2 = createLogFile();
      if (str2 != null) {
        uploadDumpAndLog(paramActivity, paramString1, str1, str2, paramString2, paramString3);
      }
      i += 1;
    }
  }
  
  private static String[] searchForDumpFiles()
  {
    if (Constants.FILES_PATH != null)
    {
      File localFile = new File(Constants.FILES_PATH + "/");
      if ((!localFile.mkdir()) && (!localFile.exists())) {
        return new String[0];
      }
      localFile.list(new FilenameFilter()
      {
        public boolean accept(File paramAnonymousFile, String paramAnonymousString)
        {
          return paramAnonymousString.endsWith(".dmp");
        }
      });
    }
    Log.d("HockeyApp", "Can't search for exception as file path is null.");
    return new String[0];
  }
  
  public static void uploadDumpAndLog(final Activity paramActivity, String paramString1, final String paramString2, final String paramString3, final String paramString4, final String paramString5)
  {
    new Thread()
    {
      public void run()
      {
        try
        {
          DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
          HttpPost localHttpPost = new HttpPost("https://rink.hockeyapp.net/api/2/apps/" + this.val$identifier + "/crashes/upload");
          MultipartEntity localMultipartEntity = new MultipartEntity();
          localMultipartEntity.addPart("attachment0", new FileBody(new File(Constants.FILES_PATH, paramString2)));
          localMultipartEntity.addPart("log", new FileBody(new File(Constants.FILES_PATH, paramString3)));
          if ((paramString4 != null) && (paramString4.length() > 0)) {
            localMultipartEntity.addPart("userID", new StringBody(paramString4));
          }
          if ((paramString5 != null) && (paramString5.length() > 0)) {
            localMultipartEntity.addPart("contact", new StringBody(paramString5));
          }
          localHttpPost.setEntity(localMultipartEntity);
          localDefaultHttpClient.execute(localHttpPost);
          return;
        }
        catch (Exception localException)
        {
          localException.printStackTrace();
          return;
        }
        finally
        {
          paramActivity.deleteFile(paramString3);
          paramActivity.deleteFile(paramString2);
        }
      }
    }.start();
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\NativeCrashManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */