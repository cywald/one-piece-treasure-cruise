package net.hockeyapp.android;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import net.hockeyapp.android.listeners.DownloadFileListener;
import net.hockeyapp.android.tasks.DownloadFileTask;
import net.hockeyapp.android.tasks.GetFileSizeTask;
import net.hockeyapp.android.utils.AsyncTaskUtils;
import net.hockeyapp.android.utils.VersionHelper;
import net.hockeyapp.android.views.UpdateView;
import org.json.JSONArray;
import org.json.JSONException;

public class UpdateFragment
  extends DialogFragment
  implements View.OnClickListener, UpdateInfoListener
{
  private DownloadFileTask downloadTask;
  private String urlString;
  private VersionHelper versionHelper;
  private JSONArray versionInfo;
  
  public static UpdateFragment newInstance(JSONArray paramJSONArray, String paramString)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("url", paramString);
    localBundle.putString("versionInfo", paramJSONArray.toString());
    paramJSONArray = new UpdateFragment();
    paramJSONArray.setArguments(localBundle);
    return paramJSONArray;
  }
  
  private void startDownloadTask(final Activity paramActivity)
  {
    this.downloadTask = new DownloadFileTask(paramActivity, this.urlString, new DownloadFileListener()
    {
      public void downloadFailed(DownloadFileTask paramAnonymousDownloadFileTask, Boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean.booleanValue()) {
          UpdateFragment.this.startDownloadTask(paramActivity);
        }
      }
      
      public void downloadSuccessful(DownloadFileTask paramAnonymousDownloadFileTask) {}
      
      public String getStringForResource(int paramAnonymousInt)
      {
        UpdateManagerListener localUpdateManagerListener = UpdateManager.getLastListener();
        if (localUpdateManagerListener != null) {
          return localUpdateManagerListener.getStringForResource(paramAnonymousInt);
        }
        return null;
      }
    });
    AsyncTaskUtils.execute(this.downloadTask);
  }
  
  public String getAppName()
  {
    Object localObject = getActivity();
    try
    {
      PackageManager localPackageManager = ((Activity)localObject).getPackageManager();
      localObject = localPackageManager.getApplicationLabel(localPackageManager.getApplicationInfo(((Activity)localObject).getPackageName(), 0)).toString();
      return (String)localObject;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return "";
  }
  
  public int getCurrentVersionCode()
  {
    try
    {
      int i = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 128).versionCode;
      return i;
    }
    catch (NullPointerException localNullPointerException)
    {
      return -1;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return -1;
  }
  
  public View getLayoutView()
  {
    return new UpdateView(getActivity(), false, true);
  }
  
  public void onClick(View paramView)
  {
    startDownloadTask(getActivity());
    dismiss();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    try
    {
      this.urlString = getArguments().getString("url");
      this.versionInfo = new JSONArray(getArguments().getString("versionInfo"));
      setStyle(1, 16973939);
      return;
    }
    catch (JSONException paramBundle)
    {
      dismiss();
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, final Bundle paramBundle)
  {
    paramViewGroup = getLayoutView();
    this.versionHelper = new VersionHelper(getActivity(), this.versionInfo.toString(), this);
    ((TextView)paramViewGroup.findViewById(4098)).setText(getAppName());
    paramBundle = (TextView)paramViewGroup.findViewById(4099);
    final String str1 = "Version " + this.versionHelper.getVersionString();
    final String str2 = this.versionHelper.getFileDateString();
    paramLayoutInflater = "Unknown size";
    long l = this.versionHelper.getFileSizeBytes();
    if (l >= 0L) {
      paramLayoutInflater = String.format("%.2f", new Object[] { Float.valueOf((float)l / 1048576.0F) }) + " MB";
    }
    for (;;)
    {
      paramBundle.setText(str1 + "\n" + str2 + " - " + paramLayoutInflater);
      ((Button)paramViewGroup.findViewById(4100)).setOnClickListener(this);
      paramLayoutInflater = (WebView)paramViewGroup.findViewById(4101);
      paramLayoutInflater.clearCache(true);
      paramLayoutInflater.destroyDrawingCache();
      paramLayoutInflater.loadDataWithBaseURL("https://sdk.hockeyapp.net/", this.versionHelper.getReleaseNotes(false), "text/html", "utf-8", null);
      return paramViewGroup;
      AsyncTaskUtils.execute(new GetFileSizeTask(getActivity(), this.urlString, new DownloadFileListener()
      {
        public void downloadSuccessful(DownloadFileTask paramAnonymousDownloadFileTask)
        {
          if ((paramAnonymousDownloadFileTask instanceof GetFileSizeTask))
          {
            long l = ((GetFileSizeTask)paramAnonymousDownloadFileTask).getSize();
            paramAnonymousDownloadFileTask = String.format("%.2f", new Object[] { Float.valueOf((float)l / 1048576.0F) }) + " MB";
            paramBundle.setText(str1 + "\n" + str2 + " - " + paramAnonymousDownloadFileTask);
          }
        }
      }));
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\UpdateFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */