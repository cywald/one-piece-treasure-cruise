package net.hockeyapp.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.util.HashMap;
import java.util.Map;
import net.hockeyapp.android.tasks.LoginTask;
import net.hockeyapp.android.utils.AsyncTaskUtils;
import net.hockeyapp.android.utils.PrefsUtil;
import net.hockeyapp.android.utils.Util;

public class LoginManager
{
  static final String LOGIN_EXIT_KEY = "net.hockeyapp.android.EXIT";
  public static final int LOGIN_MODE_ANONYMOUS = 0;
  public static final int LOGIN_MODE_EMAIL_ONLY = 1;
  public static final int LOGIN_MODE_EMAIL_PASSWORD = 2;
  public static final int LOGIN_MODE_VALIDATE = 3;
  private static String identifier = null;
  static LoginManagerListener listener;
  static Class<?> mainActivity;
  private static int mode;
  private static String secret = null;
  private static String urlString = null;
  private static Handler validateHandler = null;
  
  private static String getURLString(int paramInt)
  {
    String str = "";
    if (paramInt == 2) {
      str = "authorize";
    }
    for (;;)
    {
      return urlString + "api/3/apps/" + identifier + "/identity/" + str;
      if (paramInt == 1) {
        str = "check";
      } else if (paramInt == 3) {
        str = "validate";
      }
    }
  }
  
  public static void register(Context paramContext, String paramString1, String paramString2, int paramInt, Class<?> paramClass)
  {
    register(paramContext, paramString1, paramString2, "https://sdk.hockeyapp.net/", paramInt, paramClass);
  }
  
  public static void register(Context paramContext, String paramString1, String paramString2, int paramInt, LoginManagerListener paramLoginManagerListener)
  {
    listener = paramLoginManagerListener;
    register(paramContext, paramString1, paramString2, paramInt, (Class)null);
  }
  
  public static void register(Context paramContext, String paramString1, String paramString2, String paramString3, int paramInt, Class<?> paramClass)
  {
    if (paramContext != null)
    {
      identifier = Util.sanitizeAppIdentifier(paramString1);
      secret = paramString2;
      urlString = paramString3;
      mode = paramInt;
      mainActivity = paramClass;
      if (validateHandler == null) {
        validateHandler = new Handler()
        {
          public void handleMessage(Message paramAnonymousMessage)
          {
            if (!paramAnonymousMessage.getData().getBoolean("success")) {
              LoginManager.startLoginActivity(this.val$context);
            }
          }
        };
      }
      Constants.loadFromContext(paramContext);
    }
  }
  
  private static void startLoginActivity(Context paramContext)
  {
    Intent localIntent = new Intent();
    localIntent.setFlags(1342177280);
    localIntent.setClass(paramContext, LoginActivity.class);
    localIntent.putExtra("url", getURLString(mode));
    localIntent.putExtra("mode", mode);
    localIntent.putExtra("secret", secret);
    paramContext.startActivity(localIntent);
  }
  
  public static void verifyLogin(Activity paramActivity, Intent paramIntent)
  {
    if ((paramIntent != null) && (paramIntent.getBooleanExtra("net.hockeyapp.android.EXIT", false))) {
      paramActivity.finish();
    }
    while ((paramActivity == null) || (mode == 0) || (mode == 3)) {
      return;
    }
    Object localObject = paramActivity.getSharedPreferences("net.hockeyapp.android.login", 0);
    if (((SharedPreferences)localObject).getInt("mode", -1) != mode) {
      PrefsUtil.applyChanges(((SharedPreferences)localObject).edit().remove("auid").remove("iuid").putInt("mode", mode));
    }
    paramIntent = ((SharedPreferences)localObject).getString("auid", null);
    localObject = ((SharedPreferences)localObject).getString("iuid", null);
    int i;
    int j;
    if ((paramIntent == null) && (localObject == null))
    {
      i = 1;
      if ((paramIntent != null) || (mode != 2)) {
        break label180;
      }
      j = 1;
      label142:
      if ((localObject != null) || (mode != 1)) {
        break label185;
      }
    }
    label180:
    label185:
    for (int k = 1;; k = 0)
    {
      if ((i == 0) && (j == 0) && (k == 0)) {
        break label191;
      }
      startLoginActivity(paramActivity);
      return;
      i = 0;
      break;
      j = 0;
      break label142;
    }
    label191:
    HashMap localHashMap = new HashMap();
    if (paramIntent != null)
    {
      localHashMap.put("type", "auid");
      localHashMap.put("id", paramIntent);
    }
    for (;;)
    {
      paramActivity = new LoginTask(paramActivity, validateHandler, getURLString(3), 3, localHashMap);
      paramActivity.setShowProgressDialog(false);
      AsyncTaskUtils.execute(paramActivity);
      return;
      if (localObject != null)
      {
        localHashMap.put("type", "iuid");
        localHashMap.put("id", localObject);
      }
    }
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\LoginManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */