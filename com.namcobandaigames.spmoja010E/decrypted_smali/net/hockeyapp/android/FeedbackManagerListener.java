package net.hockeyapp.android;

import net.hockeyapp.android.objects.FeedbackMessage;

public abstract class FeedbackManagerListener
  extends StringListener
{
  public abstract boolean feedbackAnswered(FeedbackMessage paramFeedbackMessage);
  
  public Class<? extends FeedbackActivity> getFeedbackActivityClass()
  {
    return FeedbackActivity.class;
  }
}


/* Location:              C:\Users\Archit\Desktop\jd-gui-windows-1.4.0\output_jar.jar!\net\hockeyapp\android\FeedbackManagerListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */