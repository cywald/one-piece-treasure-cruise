.class final Lcom/google/android/gms/tagmanager/zzbw;
.super Ljava/lang/Object;


# annotations
.annotation build Lcom/google/android/gms/common/util/VisibleForTesting;
.end annotation


# instance fields
.field private final zzaax:J

.field private final zzbbz:J

.field private final zzbca:J

.field private zzbcb:Ljava/lang/String;


# direct methods
.method constructor <init>(JJJ)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-wide p1, p0, Lcom/google/android/gms/tagmanager/zzbw;->zzbbz:J

    .line 5
    iput-wide p3, p0, Lcom/google/android/gms/tagmanager/zzbw;->zzaax:J

    .line 6
    iput-wide p5, p0, Lcom/google/android/gms/tagmanager/zzbw;->zzbca:J

    .line 7
    return-void
.end method


# virtual methods
.method final zzdo(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 9
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12
    :cond_0
    :goto_0
    return-void

    .line 11
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/tagmanager/zzbw;->zzbcb:Ljava/lang/String;

    goto :goto_0
.end method

.method final zzom()J
    .locals 2

    .prologue
    .line 1
    iget-wide v0, p0, Lcom/google/android/gms/tagmanager/zzbw;->zzbbz:J

    return-wide v0
.end method

.method final zzon()J
    .locals 2

    .prologue
    .line 2
    iget-wide v0, p0, Lcom/google/android/gms/tagmanager/zzbw;->zzbca:J

    return-wide v0
.end method

.method final zzoo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzbw;->zzbcb:Ljava/lang/String;

    return-object v0
.end method
