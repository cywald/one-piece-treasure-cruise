.class public final Lcom/google/android/gms/tagmanager/zzdi;
.super Ljava/lang/Object;


# static fields
.field private static zzbdc:Lcom/google/android/gms/tagmanager/zzdj;
    .annotation build Lcom/google/android/gms/common/util/VisibleForTesting;
    .end annotation
.end field

.field static zzyn:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/gms/tagmanager/zzba;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/zzba;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzdj;->e(Ljava/lang/String;)V

    .line 5
    return-void
.end method

.method public static setLogLevel(I)V
    .locals 1

    .prologue
    .line 1
    sput p0, Lcom/google/android/gms/tagmanager/zzdi;->zzyn:I

    .line 2
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzdj;->setLogLevel(I)V

    .line 3
    return-void
.end method

.method public static v(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzdj;->v(Ljava/lang/String;)V

    .line 17
    return-void
.end method

.method public static zza(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/tagmanager/zzdj;->zza(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 7
    return-void
.end method

.method public static zzab(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzdj;->zzab(Ljava/lang/String;)V

    .line 9
    return-void
.end method

.method public static zzb(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/tagmanager/zzdj;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 11
    return-void
.end method

.method public static zzdi(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzdj;->zzdi(Ljava/lang/String;)V

    .line 13
    return-void
.end method

.method public static zzdj(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdi;->zzbdc:Lcom/google/android/gms/tagmanager/zzdj;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzdj;->zzdj(Ljava/lang/String;)V

    .line 15
    return-void
.end method
