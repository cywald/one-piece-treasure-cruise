.class final Lcom/google/android/gms/tagmanager/zzab;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/tagmanager/zzac;


# instance fields
.field private final synthetic zzbai:Lcom/google/android/gms/tagmanager/zzy;

.field private zzbaj:Ljava/lang/Long;

.field private final synthetic zzbak:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/tagmanager/zzy;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/android/gms/tagmanager/zzab;->zzbai:Lcom/google/android/gms/tagmanager/zzy;

    iput-boolean p2, p0, Lcom/google/android/gms/tagmanager/zzab;->zzbak:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final zzb(Lcom/google/android/gms/tagmanager/Container;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2
    iget-boolean v2, p0, Lcom/google/android/gms/tagmanager/zzab;->zzbak:Z

    if-eqz v2, :cond_3

    .line 3
    invoke-virtual {p1}, Lcom/google/android/gms/tagmanager/Container;->getLastRefreshTime()J

    move-result-wide v2

    .line 4
    iget-object v4, p0, Lcom/google/android/gms/tagmanager/zzab;->zzbaj:Ljava/lang/Long;

    if-nez v4, :cond_0

    .line 5
    iget-object v4, p0, Lcom/google/android/gms/tagmanager/zzab;->zzbai:Lcom/google/android/gms/tagmanager/zzy;

    invoke-static {v4}, Lcom/google/android/gms/tagmanager/zzy;->zzc(Lcom/google/android/gms/tagmanager/zzy;)Lcom/google/android/gms/tagmanager/zzai;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/tagmanager/zzai;->zznq()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/tagmanager/zzab;->zzbaj:Ljava/lang/Long;

    .line 6
    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/tagmanager/zzab;->zzbaj:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 7
    add-long/2addr v2, v4

    .line 8
    iget-object v4, p0, Lcom/google/android/gms/tagmanager/zzab;->zzbai:Lcom/google/android/gms/tagmanager/zzy;

    invoke-static {v4}, Lcom/google/android/gms/tagmanager/zzy;->zzd(Lcom/google/android/gms/tagmanager/zzy;)Lcom/google/android/gms/common/util/Clock;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/Clock;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    .line 9
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 8
    goto :goto_0

    .line 9
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/tagmanager/Container;->isDefault()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method
