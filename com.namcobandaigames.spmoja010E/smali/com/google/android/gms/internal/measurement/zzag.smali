.class public final Lcom/google/android/gms/internal/measurement/zzag;
.super Lcom/google/android/gms/analytics/zzi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/analytics/zzi",
        "<",
        "Lcom/google/android/gms/internal/measurement/zzag;",
        ">;"
    }
.end annotation


# instance fields
.field private zzuo:Ljava/lang/String;

.field private zzup:Ljava/lang/String;

.field private zzuq:Ljava/lang/String;

.field private zzur:Ljava/lang/String;

.field private zzus:Z

.field private zzut:Ljava/lang/String;

.field private zzuu:Z

.field private zzuv:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/analytics/zzi;-><init>()V

    return-void
.end method


# virtual methods
.method public final setClientId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzup:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public final setUserId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuq:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3
    const-string v1, "hitType"

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuo:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    const-string v1, "clientId"

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzup:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    const-string v1, "userId"

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuq:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    const-string v1, "androidAdId"

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzur:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    const-string v1, "AdTargetingEnabled"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzus:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    const-string v1, "sessionControl"

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzut:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    const-string v1, "nonInteraction"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuu:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    const-string v1, "sampleRate"

    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuv:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzag;->zza(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final zza(Z)V
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzus:Z

    .line 26
    return-void
.end method

.method public final synthetic zzb(Lcom/google/android/gms/analytics/zzi;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    .line 32
    check-cast p1, Lcom/google/android/gms/internal/measurement/zzag;

    .line 33
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuo:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 34
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuo:Ljava/lang/String;

    .line 35
    iput-object v1, p1, Lcom/google/android/gms/internal/measurement/zzag;->zzuo:Ljava/lang/String;

    .line 36
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzup:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 37
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzup:Ljava/lang/String;

    .line 38
    iput-object v1, p1, Lcom/google/android/gms/internal/measurement/zzag;->zzup:Ljava/lang/String;

    .line 39
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuq:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 40
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuq:Ljava/lang/String;

    .line 41
    iput-object v1, p1, Lcom/google/android/gms/internal/measurement/zzag;->zzuq:Ljava/lang/String;

    .line 42
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzur:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzur:Ljava/lang/String;

    .line 44
    iput-object v1, p1, Lcom/google/android/gms/internal/measurement/zzag;->zzur:Ljava/lang/String;

    .line 45
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzus:Z

    if-eqz v1, :cond_4

    .line 47
    iput-boolean v0, p1, Lcom/google/android/gms/internal/measurement/zzag;->zzus:Z

    .line 48
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzut:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 49
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzut:Ljava/lang/String;

    .line 50
    iput-object v1, p1, Lcom/google/android/gms/internal/measurement/zzag;->zzut:Ljava/lang/String;

    .line 51
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuu:Z

    if-eqz v1, :cond_6

    .line 52
    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuu:Z

    .line 53
    iput-boolean v1, p1, Lcom/google/android/gms/internal/measurement/zzag;->zzuu:Z

    .line 54
    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuv:D

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_7

    .line 55
    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuv:D

    .line 56
    cmpl-double v1, v2, v4

    if-ltz v1, :cond_8

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_8

    :goto_0
    const-string v1, "Sample rate must be between 0% and 100%"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 57
    iput-wide v2, p1, Lcom/google/android/gms/internal/measurement/zzag;->zzuv:D

    .line 58
    :cond_7
    return-void

    .line 56
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final zzb(Z)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuu:Z

    .line 30
    return-void
.end method

.method public final zzbc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuo:Ljava/lang/String;

    return-object v0
.end method

.method public final zzbd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzup:Ljava/lang/String;

    return-object v0
.end method

.method public final zzbe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuq:Ljava/lang/String;

    return-object v0
.end method

.method public final zzbf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzur:Ljava/lang/String;

    return-object v0
.end method

.method public final zzbg()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzus:Z

    return v0
.end method

.method public final zzbh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzut:Ljava/lang/String;

    return-object v0
.end method

.method public final zzbi()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuu:Z

    return v0
.end method

.method public final zzbj()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuv:D

    return-wide v0
.end method

.method public final zzl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzuo:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public final zzm(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/zzag;->zzur:Ljava/lang/String;

    .line 23
    return-void
.end method
