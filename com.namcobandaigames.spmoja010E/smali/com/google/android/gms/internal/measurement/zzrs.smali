.class public final Lcom/google/android/gms/internal/measurement/zzrs;
.super Ljava/lang/Object;


# direct methods
.method private static zza(ILcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;Ljava/util/Set;)Lcom/google/android/gms/internal/measurement/zzp;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/gms/internal/measurement/zzl;",
            "[",
            "Lcom/google/android/gms/internal/measurement/zzp;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/gms/internal/measurement/zzp;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/zzsa;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x5a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Value cycle detected.  Current value reference: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".  Previous value references: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzer(Ljava/lang/String;)V

    .line 69
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/internal/measurement/zzl;->zzou:[Lcom/google/android/gms/internal/measurement/zzp;

    const-string v1, "values"

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/internal/measurement/zzrs;->zza([Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzp;

    .line 70
    aget-object v1, p2, p0

    if-eqz v1, :cond_1

    .line 71
    aget-object v0, p2, p0

    .line 122
    :goto_0
    return-object v0

    .line 72
    :cond_1
    const/4 v1, 0x0

    .line 73
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 74
    iget v3, v0, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    packed-switch v3, :pswitch_data_0

    .line 118
    :cond_2
    :goto_1
    if-nez v1, :cond_3

    .line 119
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xf

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid value: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzer(Ljava/lang/String;)V

    .line 120
    :cond_3
    aput-object v1, p2, p0

    .line 121
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 122
    goto :goto_0

    .line 75
    :pswitch_0
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzl(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzg$zza;

    move-result-object v3

    .line 76
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzk(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v1

    .line 77
    iget-object v4, v3, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpv:[I

    array-length v4, v4

    new-array v4, v4, [Lcom/google/android/gms/internal/measurement/zzp;

    iput-object v4, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 79
    iget-object v5, v3, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpv:[I

    array-length v6, v5

    move v3, v2

    :goto_2
    if-ge v2, v6, :cond_2

    aget v7, v5, v2

    .line 80
    iget-object v8, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    add-int/lit8 v4, v3, 0x1

    .line 81
    invoke-static {v7, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(ILcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;Ljava/util/Set;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v7

    aput-object v7, v8, v3

    .line 82
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_2

    .line 84
    :pswitch_1
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzk(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v1

    .line 85
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzl(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzg$zza;

    move-result-object v6

    .line 86
    iget-object v3, v6, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpw:[I

    array-length v3, v3

    iget-object v4, v6, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpx:[I

    array-length v4, v4

    if-eq v3, v4, :cond_4

    .line 87
    iget-object v3, v6, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpw:[I

    array-length v3, v3

    iget-object v4, v6, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpx:[I

    array-length v4, v4

    const/16 v5, 0x3a

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Uneven map keys ("

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") and map values ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/internal/measurement/zzrs;->zzer(Ljava/lang/String;)V

    .line 88
    :cond_4
    iget-object v3, v6, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpw:[I

    array-length v3, v3

    new-array v3, v3, [Lcom/google/android/gms/internal/measurement/zzp;

    iput-object v3, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 89
    iget-object v3, v6, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpw:[I

    array-length v3, v3

    new-array v3, v3, [Lcom/google/android/gms/internal/measurement/zzp;

    iput-object v3, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 91
    iget-object v7, v6, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpw:[I

    array-length v8, v7

    move v3, v2

    move v4, v2

    :goto_3
    if-ge v3, v8, :cond_5

    aget v9, v7, v3

    .line 92
    iget-object v10, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    add-int/lit8 v5, v4, 0x1

    .line 93
    invoke-static {v9, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(ILcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;Ljava/util/Set;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v9

    aput-object v9, v10, v4

    .line 94
    add-int/lit8 v3, v3, 0x1

    move v4, v5

    goto :goto_3

    .line 96
    :cond_5
    iget-object v5, v6, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpx:[I

    array-length v6, v5

    move v3, v2

    :goto_4
    if-ge v2, v6, :cond_2

    aget v7, v5, v2

    .line 97
    iget-object v8, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    add-int/lit8 v4, v3, 0x1

    .line 98
    invoke-static {v7, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(ILcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;Ljava/util/Set;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v7

    aput-object v7, v8, v3

    .line 99
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_4

    .line 101
    :pswitch_2
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzk(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v1

    .line 102
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzl(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzg$zza;

    move-result-object v2

    .line 103
    iget v2, v2, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzqa:I

    .line 104
    invoke-static {v2, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(ILcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;Ljava/util/Set;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v2

    .line 105
    invoke-static {v2}, Lcom/google/android/gms/tagmanager/zzgj;->zzc(Lcom/google/android/gms/internal/measurement/zzp;)Ljava/lang/String;

    move-result-object v2

    .line 106
    iput-object v2, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    goto/16 :goto_1

    .line 108
    :pswitch_3
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzk(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v1

    .line 109
    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzl(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzg$zza;

    move-result-object v3

    .line 110
    iget-object v4, v3, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpz:[I

    array-length v4, v4

    new-array v4, v4, [Lcom/google/android/gms/internal/measurement/zzp;

    iput-object v4, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 112
    iget-object v5, v3, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpz:[I

    array-length v6, v5

    move v3, v2

    :goto_5
    if-ge v2, v6, :cond_2

    aget v7, v5, v2

    .line 113
    iget-object v8, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    add-int/lit8 v4, v3, 0x1

    .line 114
    invoke-static {v7, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(ILcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;Ljava/util/Set;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v7

    aput-object v7, v8, v3

    .line 115
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_5

    :pswitch_4
    move-object v1, v0

    .line 117
    goto/16 :goto_1

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static zza(Lcom/google/android/gms/internal/measurement/zzh;Lcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;I)Lcom/google/android/gms/internal/measurement/zzru;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/zzsa;
        }
    .end annotation

    .prologue
    .line 132
    invoke-static {}, Lcom/google/android/gms/internal/measurement/zzru;->zzsp()Lcom/google/android/gms/internal/measurement/zzrv;

    move-result-object v3

    .line 133
    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    aget v0, v4, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 134
    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/zzl;->zzov:[Lcom/google/android/gms/internal/measurement/zzk;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v6, "properties"

    invoke-static {v1, v0, v6}, Lcom/google/android/gms/internal/measurement/zzrs;->zza([Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzk;

    .line 135
    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/zzl;->zzot:[Ljava/lang/String;

    iget v6, v0, Lcom/google/android/gms/internal/measurement/zzk;->key:I

    const-string v7, "keys"

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/internal/measurement/zzrs;->zza([Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 136
    iget v0, v0, Lcom/google/android/gms/internal/measurement/zzk;->value:I

    const-string v6, "values"

    invoke-static {p2, v0, v6}, Lcom/google/android/gms/internal/measurement/zzrs;->zza([Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzp;

    .line 137
    sget-object v6, Lcom/google/android/gms/internal/measurement/zzb;->zzks:Lcom/google/android/gms/internal/measurement/zzb;

    invoke-virtual {v6}, Lcom/google/android/gms/internal/measurement/zzb;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 138
    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/measurement/zzrv;->zzm(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzrv;

    .line 140
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 139
    :cond_0
    invoke-virtual {v3, v1, v0}, Lcom/google/android/gms/internal/measurement/zzrv;->zzb(Ljava/lang/String;Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzrv;

    goto :goto_1

    .line 141
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/internal/measurement/zzrv;->zzsq()Lcom/google/android/gms/internal/measurement/zzru;

    move-result-object v0

    return-object v0
.end method

.method public static zza(Lcom/google/android/gms/internal/measurement/zzl;)Lcom/google/android/gms/internal/measurement/zzrw;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/zzsa;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzou:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    new-array v2, v0, [Lcom/google/android/gms/internal/measurement/zzp;

    move v0, v1

    .line 2
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzou:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 3
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v1}, Ljava/util/HashSet;-><init>(I)V

    invoke-static {v0, p0, v2, v3}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(ILcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;Ljava/util/Set;)Lcom/google/android/gms/internal/measurement/zzp;

    .line 4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5
    :cond_0
    invoke-static {}, Lcom/google/android/gms/internal/measurement/zzrw;->zzsr()Lcom/google/android/gms/internal/measurement/zzrx;

    move-result-object v4

    .line 6
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 7
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzox:[Lcom/google/android/gms/internal/measurement/zzh;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 8
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzox:[Lcom/google/android/gms/internal/measurement/zzh;

    aget-object v3, v3, v0

    invoke-static {v3, p0, v2, v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(Lcom/google/android/gms/internal/measurement/zzh;Lcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;I)Lcom/google/android/gms/internal/measurement/zzru;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 10
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 11
    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzoy:[Lcom/google/android/gms/internal/measurement/zzh;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 12
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzoy:[Lcom/google/android/gms/internal/measurement/zzh;

    aget-object v3, v3, v0

    invoke-static {v3, p0, v2, v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(Lcom/google/android/gms/internal/measurement/zzh;Lcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;I)Lcom/google/android/gms/internal/measurement/zzru;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 15
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzow:[Lcom/google/android/gms/internal/measurement/zzh;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 16
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzow:[Lcom/google/android/gms/internal/measurement/zzh;

    aget-object v3, v3, v0

    .line 17
    invoke-static {v3, p0, v2, v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zza(Lcom/google/android/gms/internal/measurement/zzh;Lcom/google/android/gms/internal/measurement/zzl;[Lcom/google/android/gms/internal/measurement/zzp;I)Lcom/google/android/gms/internal/measurement/zzru;

    move-result-object v3

    .line 18
    invoke-virtual {v4, v3}, Lcom/google/android/gms/internal/measurement/zzrx;->zzc(Lcom/google/android/gms/internal/measurement/zzru;)Lcom/google/android/gms/internal/measurement/zzrx;

    .line 19
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 21
    :cond_3
    iget-object v8, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzoz:[Lcom/google/android/gms/internal/measurement/zzm;

    array-length v9, v8

    move v3, v1

    :goto_4
    if-ge v3, v9, :cond_e

    aget-object v10, v8, v3

    .line 23
    new-instance v11, Lcom/google/android/gms/internal/measurement/zzrz;

    const/4 v0, 0x0

    invoke-direct {v11, v0}, Lcom/google/android/gms/internal/measurement/zzrz;-><init>(Lcom/google/android/gms/internal/measurement/zzrt;)V

    .line 25
    iget-object v12, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpj:[I

    array-length v13, v12

    move v2, v1

    :goto_5
    if-ge v2, v13, :cond_4

    aget v0, v12, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzru;

    invoke-virtual {v11, v0}, Lcom/google/android/gms/internal/measurement/zzrz;->zzd(Lcom/google/android/gms/internal/measurement/zzru;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 27
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 28
    :cond_4
    iget-object v12, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpk:[I

    array-length v13, v12

    move v2, v1

    :goto_6
    if-ge v2, v13, :cond_5

    aget v0, v12, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzru;

    invoke-virtual {v11, v0}, Lcom/google/android/gms/internal/measurement/zzrz;->zze(Lcom/google/android/gms/internal/measurement/zzru;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 30
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 31
    :cond_5
    iget-object v12, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpl:[I

    array-length v13, v12

    move v2, v1

    :goto_7
    if-ge v2, v13, :cond_6

    aget v0, v12, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzru;

    invoke-virtual {v11, v0}, Lcom/google/android/gms/internal/measurement/zzrz;->zzf(Lcom/google/android/gms/internal/measurement/zzru;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 33
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 34
    :cond_6
    iget-object v2, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpn:[I

    array-length v12, v2

    move v0, v1

    :goto_8
    if-ge v0, v12, :cond_7

    aget v13, v2, v0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 35
    iget-object v14, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzou:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    aget-object v13, v14, v13

    iget-object v13, v13, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    invoke-virtual {v11, v13}, Lcom/google/android/gms/internal/measurement/zzrz;->zzff(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 36
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 37
    :cond_7
    iget-object v12, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpm:[I

    array-length v13, v12

    move v2, v1

    :goto_9
    if-ge v2, v13, :cond_8

    aget v0, v12, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzru;

    invoke-virtual {v11, v0}, Lcom/google/android/gms/internal/measurement/zzrz;->zzg(Lcom/google/android/gms/internal/measurement/zzru;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 39
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 40
    :cond_8
    iget-object v2, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpo:[I

    array-length v12, v2

    move v0, v1

    :goto_a
    if-ge v0, v12, :cond_9

    aget v13, v2, v0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 41
    iget-object v14, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzou:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    aget-object v13, v14, v13

    iget-object v13, v13, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    invoke-virtual {v11, v13}, Lcom/google/android/gms/internal/measurement/zzrz;->zzfg(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 43
    :cond_9
    iget-object v12, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpp:[I

    array-length v13, v12

    move v2, v1

    :goto_b
    if-ge v2, v13, :cond_a

    aget v0, v12, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzru;

    invoke-virtual {v11, v0}, Lcom/google/android/gms/internal/measurement/zzrz;->zzh(Lcom/google/android/gms/internal/measurement/zzru;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 45
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    .line 46
    :cond_a
    iget-object v2, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpr:[I

    array-length v12, v2

    move v0, v1

    :goto_c
    if-ge v0, v12, :cond_b

    aget v13, v2, v0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 47
    iget-object v14, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzou:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    aget-object v13, v14, v13

    iget-object v13, v13, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    invoke-virtual {v11, v13}, Lcom/google/android/gms/internal/measurement/zzrz;->zzfh(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 49
    :cond_b
    iget-object v12, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzpq:[I

    array-length v13, v12

    move v2, v1

    :goto_d
    if-ge v2, v13, :cond_c

    aget v0, v12, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzru;

    invoke-virtual {v11, v0}, Lcom/google/android/gms/internal/measurement/zzrz;->zzi(Lcom/google/android/gms/internal/measurement/zzru;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 51
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    .line 52
    :cond_c
    iget-object v2, v10, Lcom/google/android/gms/internal/measurement/zzm;->zzps:[I

    array-length v10, v2

    move v0, v1

    :goto_e
    if-ge v0, v10, :cond_d

    aget v12, v2, v0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 53
    iget-object v13, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzou:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    aget-object v12, v13, v12

    iget-object v12, v12, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lcom/google/android/gms/internal/measurement/zzrz;->zzfi(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzrz;

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 55
    :cond_d
    invoke-virtual {v11}, Lcom/google/android/gms/internal/measurement/zzrz;->zzsw()Lcom/google/android/gms/internal/measurement/zzry;

    move-result-object v0

    .line 56
    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/measurement/zzrx;->zzb(Lcom/google/android/gms/internal/measurement/zzry;)Lcom/google/android/gms/internal/measurement/zzrx;

    .line 57
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_4

    .line 58
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzl;->version:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/measurement/zzrx;->zzfe(Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzrx;

    .line 59
    iget v0, p0, Lcom/google/android/gms/internal/measurement/zzl;->zzph:I

    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/measurement/zzrx;->zzag(I)Lcom/google/android/gms/internal/measurement/zzrx;

    .line 60
    invoke-virtual {v4}, Lcom/google/android/gms/internal/measurement/zzrx;->zzst()Lcom/google/android/gms/internal/measurement/zzrw;

    move-result-object v0

    return-object v0
.end method

.method private static zza([Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;I",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/zzsa;
        }
    .end annotation

    .prologue
    .line 129
    if-ltz p1, :cond_0

    array-length v0, p0

    if-lt p1, v0, :cond_1

    .line 130
    :cond_0
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Index out of bounds detected: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzer(Ljava/lang/String;)V

    .line 131
    :cond_1
    aget-object v0, p0, p1

    return-object v0
.end method

.method public static zza(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 143
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 144
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 145
    return-void

    .line 146
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method private static zzer(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/zzsa;
        }
    .end annotation

    .prologue
    .line 127
    invoke-static {p0}, Lcom/google/android/gms/tagmanager/zzdi;->e(Ljava/lang/String;)V

    .line 128
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzsa;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/measurement/zzsa;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static zzk(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzp;
    .locals 2

    .prologue
    .line 61
    new-instance v1, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v1}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    .line 62
    iget v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    iput v0, v1, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    .line 64
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    if-eqz v0, :cond_0

    .line 65
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    iput-boolean v0, v1, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    .line 66
    :cond_0
    return-object v1
.end method

.method private static zzl(Lcom/google/android/gms/internal/measurement/zzp;)Lcom/google/android/gms/internal/measurement/zzg$zza;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/zzsa;
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpt:Lcom/google/android/gms/internal/measurement/zzzb;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzzb;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzg$zza;

    .line 124
    if-nez v0, :cond_0

    .line 125
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x36

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Expected a ServingValue and didn\'t get one. Value is: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/measurement/zzrs;->zzer(Ljava/lang/String;)V

    .line 126
    :cond_0
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzg$zza;->zzpt:Lcom/google/android/gms/internal/measurement/zzzb;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzzb;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/measurement/zzg$zza;

    return-object v0
.end method
