.class public final Lcom/google/android/gms/internal/measurement/zzh;
.super Lcom/google/android/gms/internal/measurement/zzza;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/zzza",
        "<",
        "Lcom/google/android/gms/internal/measurement/zzh;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile zzod:[Lcom/google/android/gms/internal/measurement/zzh;


# instance fields
.field private name:I

.field public zzoe:[I

.field private zzof:I

.field private zzog:Z

.field private zzoh:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/zzza;-><init>()V

    .line 9
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzzj;->zzcax:[I

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    .line 10
    iput v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzof:I

    .line 11
    iput v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    .line 12
    iput-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzog:Z

    .line 13
    iput-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoh:Z

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzcfm:I

    .line 16
    return-void
.end method

.method public static zze()[Lcom/google/android/gms/internal/measurement/zzh;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzh;->zzod:[Lcom/google/android/gms/internal/measurement/zzh;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lcom/google/android/gms/internal/measurement/zzze;->zzcfl:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzh;->zzod:[Lcom/google/android/gms/internal/measurement/zzh;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/measurement/zzh;

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzh;->zzod:[Lcom/google/android/gms/internal/measurement/zzh;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzh;->zzod:[Lcom/google/android/gms/internal/measurement/zzh;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 17
    if-ne p1, p0, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v0

    .line 19
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/internal/measurement/zzh;

    if-nez v2, :cond_2

    move v0, v1

    .line 20
    goto :goto_0

    .line 21
    :cond_2
    check-cast p1, Lcom/google/android/gms/internal/measurement/zzh;

    .line 22
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzze;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 23
    goto :goto_0

    .line 24
    :cond_3
    iget v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzof:I

    iget v3, p1, Lcom/google/android/gms/internal/measurement/zzh;->zzof:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 25
    goto :goto_0

    .line 26
    :cond_4
    iget v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    iget v3, p1, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 27
    goto :goto_0

    .line 28
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzog:Z

    iget-boolean v3, p1, Lcom/google/android/gms/internal/measurement/zzh;->zzog:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 29
    goto :goto_0

    .line 30
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoh:Z

    iget-boolean v3, p1, Lcom/google/android/gms/internal/measurement/zzh;->zzoh:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 31
    goto :goto_0

    .line 32
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 33
    :cond_8
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 34
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/zzzc;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 35
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 36
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    .line 37
    invoke-static {v3}, Lcom/google/android/gms/internal/measurement/zzze;->hashCode([I)I

    move-result v3

    add-int/2addr v0, v3

    .line 38
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzof:I

    add-int/2addr v0, v3

    .line 39
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    add-int/2addr v0, v3

    .line 40
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzog:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 41
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoh:Z

    if-eqz v3, :cond_2

    :goto_1
    add-int/2addr v0, v1

    .line 42
    mul-int/lit8 v1, v0, 0x1f

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    .line 44
    :goto_2
    add-int/2addr v0, v1

    .line 45
    return v0

    :cond_1
    move v0, v2

    .line 40
    goto :goto_0

    :cond_2
    move v1, v2

    .line 41
    goto :goto_1

    .line 44
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/measurement/zzzc;->hashCode()I

    move-result v0

    goto :goto_2
.end method

.method public final synthetic zza(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 83
    .line 84
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    move-result v0

    .line 85
    sparse-switch v0, :sswitch_data_0

    .line 87
    invoke-super {p0, p1, v0}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyx;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    :sswitch_0
    return-object p0

    .line 89
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzum()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoh:Z

    goto :goto_0

    .line 92
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v0

    .line 93
    iput v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzof:I

    goto :goto_0

    .line 95
    :sswitch_3
    const/16 v0, 0x18

    .line 96
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/measurement/zzzj;->zzb(Lcom/google/android/gms/internal/measurement/zzyx;I)I

    move-result v2

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 98
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 99
    if-eqz v0, :cond_1

    .line 100
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 103
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v3

    .line 104
    aput v3, v2, v0

    .line 105
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    array-length v0, v0

    goto :goto_1

    .line 108
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v3

    .line 109
    aput v3, v2, v0

    .line 110
    iput-object v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    goto :goto_0

    .line 112
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v0

    .line 113
    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zzaq(I)I

    move-result v3

    .line 115
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->getPosition()I

    move-result v2

    move v0, v1

    .line 116
    :goto_3
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzyr()I

    move-result v4

    if-lez v4, :cond_4

    .line 118
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 120
    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/measurement/zzyx;->zzby(I)V

    .line 121
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    if-nez v2, :cond_6

    move v2, v1

    .line 122
    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 123
    if-eqz v2, :cond_5

    .line 124
    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 125
    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    .line 127
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v4

    .line 128
    aput v4, v0, v2

    .line 129
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 121
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    array-length v2, v2

    goto :goto_4

    .line 130
    :cond_7
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    .line 131
    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/measurement/zzyx;->zzar(I)V

    goto/16 :goto_0

    .line 134
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v0

    .line 135
    iput v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    goto/16 :goto_0

    .line 137
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzum()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzog:Z

    goto/16 :goto_0

    .line 85
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final zza(Lcom/google/android/gms/internal/measurement/zzyy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoh:Z

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoh:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(IZ)V

    .line 48
    :cond_0
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzof:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zzd(II)V

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 50
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 51
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzd(II)V

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_1
    iget v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    if-eqz v0, :cond_2

    .line 54
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zzd(II)V

    .line 55
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzog:Z

    if-eqz v0, :cond_3

    .line 56
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzog:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(IZ)V

    .line 57
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyy;)V

    .line 58
    return-void
.end method

.method protected final zzf()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-super {p0}, Lcom/google/android/gms/internal/measurement/zzza;->zzf()I

    move-result v0

    .line 60
    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoh:Z

    if-eqz v2, :cond_0

    .line 61
    const/4 v2, 0x1

    .line 62
    invoke-static {v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzbb(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 63
    add-int/2addr v0, v2

    .line 64
    :cond_0
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzof:I

    .line 65
    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzh(II)I

    move-result v2

    add-int/2addr v2, v0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 68
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 69
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    aget v3, v3, v0

    .line 71
    invoke-static {v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzbc(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    add-int v0, v2, v1

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzoe:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 75
    :goto_1
    iget v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    if-eqz v1, :cond_2

    .line 76
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/internal/measurement/zzh;->name:I

    .line 77
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzh(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzh;->zzog:Z

    if-eqz v1, :cond_3

    .line 79
    const/4 v1, 0x6

    .line 80
    invoke-static {v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zzbb(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 81
    add-int/2addr v0, v1

    .line 82
    :cond_3
    return v0

    :cond_4
    move v0, v2

    goto :goto_1
.end method
