.class public final Lcom/google/android/gms/internal/measurement/zzsb;
.super Ljava/lang/Object;


# static fields
.field private static final zzbqb:Ljava/lang/Integer;

.field private static final zzbqc:Ljava/lang/Integer;


# instance fields
.field private final zzaea:Ljava/util/concurrent/ExecutorService;

.field private final zzri:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzsb;->zzbqb:Ljava/lang/Integer;

    .line 8
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzsb;->zzbqc:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/measurement/zzsb;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .annotation build Lcom/google/android/gms/common/util/VisibleForTesting;
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/google/android/gms/internal/measurement/zzsb;->zzri:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Lcom/google/android/gms/internal/measurement/zzsb;->zzaea:Ljava/util/concurrent/ExecutorService;

    .line 6
    return-void
.end method
