.class public final Lcom/google/android/gms/internal/measurement/zzn;
.super Lcom/google/android/gms/internal/measurement/zzza;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/zzza",
        "<",
        "Lcom/google/android/gms/internal/measurement/zzn;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile zzqc:[Lcom/google/android/gms/internal/measurement/zzn;


# instance fields
.field public name:Ljava/lang/String;

.field private zzqd:Lcom/google/android/gms/internal/measurement/zzp;

.field public zzqe:Lcom/google/android/gms/internal/measurement/zzj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/zzza;-><init>()V

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    .line 11
    iput-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    .line 12
    iput-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzcfm:I

    .line 14
    return-void
.end method

.method public static zzj()[Lcom/google/android/gms/internal/measurement/zzn;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzn;->zzqc:[Lcom/google/android/gms/internal/measurement/zzn;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lcom/google/android/gms/internal/measurement/zzze;->zzcfl:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzn;->zzqc:[Lcom/google/android/gms/internal/measurement/zzn;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/measurement/zzn;

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzn;->zzqc:[Lcom/google/android/gms/internal/measurement/zzn;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzn;->zzqc:[Lcom/google/android/gms/internal/measurement/zzn;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 15
    if-ne p1, p0, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v0

    .line 17
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/internal/measurement/zzn;

    if-nez v2, :cond_2

    move v0, v1

    .line 18
    goto :goto_0

    .line 19
    :cond_2
    check-cast p1, Lcom/google/android/gms/internal/measurement/zzn;

    .line 20
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 21
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 22
    goto :goto_0

    .line 23
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 24
    goto :goto_0

    .line 25
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v2, :cond_5

    .line 26
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v2, :cond_6

    move v0, v1

    .line 27
    goto :goto_0

    .line 28
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/measurement/zzp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 29
    goto :goto_0

    .line 30
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    if-nez v2, :cond_7

    .line 31
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    if-eqz v2, :cond_8

    move v0, v1

    .line 32
    goto :goto_0

    .line 33
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/measurement/zzj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 34
    goto :goto_0

    .line 35
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 36
    :cond_9
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 37
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/zzzc;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 39
    mul-int/lit8 v2, v0, 0x1f

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 41
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    .line 42
    mul-int/lit8 v3, v0, 0x1f

    .line 43
    if-nez v2, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 44
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    .line 45
    mul-int/lit8 v3, v0, 0x1f

    .line 46
    if-nez v2, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 47
    mul-int/lit8 v0, v0, 0x1f

    .line 48
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 49
    :cond_0
    :goto_3
    add-int/2addr v0, v1

    .line 50
    return v0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 43
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzp;->hashCode()I

    move-result v0

    goto :goto_1

    .line 46
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzj;->hashCode()I

    move-result v0

    goto :goto_2

    .line 49
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/zzzc;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic zza(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    .line 71
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    move-result v0

    .line 72
    sparse-switch v0, :sswitch_data_0

    .line 74
    invoke-super {p0, p1, v0}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyx;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    :sswitch_0
    return-object p0

    .line 76
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    goto :goto_0

    .line 78
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v0, :cond_1

    .line 79
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    goto :goto_0

    .line 82
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    if-nez v0, :cond_2

    .line 83
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzj;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/zzj;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    goto :goto_0

    .line 72
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final zza(Lcom/google/android/gms/internal/measurement/zzyy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILjava/lang/String;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v0, :cond_1

    .line 54
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zza(ILcom/google/android/gms/internal/measurement/zzzg;)V

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    if-eqz v0, :cond_2

    .line 56
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zza(ILcom/google/android/gms/internal/measurement/zzzg;)V

    .line 57
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyy;)V

    .line 58
    return-void
.end method

.method protected final zzf()I
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, Lcom/google/android/gms/internal/measurement/zzza;->zzf()I

    move-result v0

    .line 60
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->name:Ljava/lang/String;

    .line 62
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzc(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v1, :cond_1

    .line 64
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqd:Lcom/google/android/gms/internal/measurement/zzp;

    .line 65
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILcom/google/android/gms/internal/measurement/zzzg;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    if-eqz v1, :cond_2

    .line 67
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzn;->zzqe:Lcom/google/android/gms/internal/measurement/zzj;

    .line 68
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILcom/google/android/gms/internal/measurement/zzzg;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_2
    return v0
.end method
