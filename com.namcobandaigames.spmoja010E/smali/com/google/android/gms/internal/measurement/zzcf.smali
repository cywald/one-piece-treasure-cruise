.class public final Lcom/google/android/gms/internal/measurement/zzcf;
.super Ljava/lang/Object;


# annotations
.annotation build Lcom/google/android/gms/common/util/VisibleForTesting;
.end annotation


# static fields
.field public static zzaaa:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzaab:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static zzaac:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static zzaad:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzaae:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static zzaaf:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzaag:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static zzaah:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static zzaai:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzaaj:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzaak:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzaal:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzaam:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static zzyv:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static zzyw:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static zzyx:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static zzyy:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static zzyz:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static zzza:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static zzzb:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzc:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzd:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzze:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static zzzf:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static zzzg:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzh:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzi:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzj:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzk:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzl:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzm:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzn:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzo:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzp:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzq:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzr:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static zzzs:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzt:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzu:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzv:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzw:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzx:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static zzzy:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static zzzz:Lcom/google/android/gms/internal/measurement/zzcg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/measurement/zzcg",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/32 v12, 0x5265c00

    const/16 v10, 0x2000

    const/16 v7, 0x14

    const-wide/16 v8, 0x1388

    const/4 v6, 0x0

    .line 1
    const-string v0, "analytics.service_enabled"

    .line 2
    invoke-static {v0, v6, v6}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;ZZ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 3
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzyv:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 4
    const-string v0, "analytics.service_client_enabled"

    .line 5
    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;ZZ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 6
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzyw:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 7
    const-string v0, "analytics.log_tag"

    const-string v1, "GAv4"

    const-string v2, "GAv4-SVC"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzyx:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 8
    const-string v0, "analytics.max_tokens"

    .line 9
    const-wide/16 v2, 0x3c

    const-wide/16 v4, 0x3c

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 10
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzyy:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 11
    const-string v0, "analytics.tokens_per_sec"

    .line 12
    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;FF)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 13
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzyz:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 14
    const-string v0, "analytics.max_stored_hits"

    const/16 v1, 0x7d0

    const/16 v2, 0x4e20

    .line 15
    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzza:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 16
    const-string v0, "analytics.max_stored_hits_per_app"

    .line 17
    const/16 v1, 0x7d0

    const/16 v2, 0x7d0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 18
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzb:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 19
    const-string v0, "analytics.max_stored_properties_per_app"

    .line 20
    const/16 v1, 0x64

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 21
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzc:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 22
    const-string v0, "analytics.local_dispatch_millis"

    const-wide/32 v2, 0x1b7740

    const-wide/32 v4, 0x1d4c0

    .line 23
    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzd:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 24
    const-string v0, "analytics.initial_local_dispatch_millis"

    .line 25
    invoke-static {v0, v8, v9, v8, v9}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzze:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 26
    const-string v0, "analytics.min_local_dispatch_millis"

    .line 27
    const-wide/32 v2, 0x1d4c0

    const-wide/32 v4, 0x1d4c0

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 28
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzf:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 29
    const-string v0, "analytics.max_local_dispatch_millis"

    .line 30
    const-wide/32 v2, 0x6ddd00

    const-wide/32 v4, 0x6ddd00

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 31
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzg:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 32
    const-string v0, "analytics.dispatch_alarm_millis"

    .line 33
    const-wide/32 v2, 0x6ddd00

    const-wide/32 v4, 0x6ddd00

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 34
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzh:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 35
    const-string v0, "analytics.max_dispatch_alarm_millis"

    .line 36
    const-wide/32 v2, 0x1ee6280

    const-wide/32 v4, 0x1ee6280

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 37
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzi:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 38
    const-string v0, "analytics.max_hits_per_dispatch"

    .line 39
    invoke-static {v0, v7, v7}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 40
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzj:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 41
    const-string v0, "analytics.max_hits_per_batch"

    .line 42
    invoke-static {v0, v7, v7}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 43
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzk:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 44
    const-string v0, "analytics.insecure_host"

    const-string v1, "http://www.google-analytics.com"

    .line 46
    invoke-static {v0, v1, v1}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 47
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzl:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 48
    const-string v0, "analytics.secure_host"

    const-string v1, "https://ssl.google-analytics.com"

    .line 50
    invoke-static {v0, v1, v1}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 51
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzm:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 52
    const-string v0, "analytics.simple_endpoint"

    const-string v1, "/collect"

    .line 53
    invoke-static {v0, v1, v1}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 54
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzn:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 55
    const-string v0, "analytics.batching_endpoint"

    const-string v1, "/batch"

    .line 56
    invoke-static {v0, v1, v1}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 57
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzo:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 58
    const-string v0, "analytics.max_get_length"

    .line 59
    const/16 v1, 0x7f4

    const/16 v2, 0x7f4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 60
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzp:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 61
    const-string v0, "analytics.batching_strategy.k"

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzbn;->zzya:Lcom/google/android/gms/internal/measurement/zzbn;

    .line 62
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/zzbn;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/internal/measurement/zzbn;->zzya:Lcom/google/android/gms/internal/measurement/zzbn;

    .line 63
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzbn;->name()Ljava/lang/String;

    move-result-object v2

    .line 64
    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzq:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 65
    const-string v0, "analytics.compression_strategy.k"

    sget-object v1, Lcom/google/android/gms/internal/measurement/zzbt;->zzyh:Lcom/google/android/gms/internal/measurement/zzbt;

    .line 66
    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/zzbt;->name()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-static {v0, v1, v1}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 69
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzr:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 70
    const-string v0, "analytics.max_hits_per_request.k"

    .line 71
    invoke-static {v0, v7, v7}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 72
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzs:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 73
    const-string v0, "analytics.max_hit_length.k"

    .line 74
    invoke-static {v0, v10, v10}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 75
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzt:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 76
    const-string v0, "analytics.max_post_length.k"

    .line 77
    invoke-static {v0, v10, v10}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 78
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzu:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 79
    const-string v0, "analytics.max_batch_post_length"

    .line 80
    invoke-static {v0, v10, v10}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 81
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzv:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 82
    const-string v0, "analytics.fallback_responses.k"

    const-string v1, "404,502"

    .line 84
    invoke-static {v0, v1, v1}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 85
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzw:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 86
    const-string v0, "analytics.batch_retry_interval.seconds.k"

    .line 87
    const/16 v1, 0xe10

    const/16 v2, 0xe10

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 88
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzx:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 89
    const-string v0, "analytics.service_monitor_interval"

    .line 90
    invoke-static {v0, v12, v13, v12, v13}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 91
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzy:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 92
    const-string v0, "analytics.http_connection.connect_timeout_millis"

    .line 93
    const v1, 0xea60

    const v2, 0xea60

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 94
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzzz:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 95
    const-string v0, "analytics.http_connection.read_timeout_millis"

    .line 96
    const v1, 0xee48

    const v2, 0xee48

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 97
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaaa:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 98
    const-string v0, "analytics.campaigns.time_limit"

    .line 99
    invoke-static {v0, v12, v13, v12, v13}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 100
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaab:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 101
    const-string v0, "analytics.first_party_experiment_id"

    const-string v1, ""

    .line 103
    invoke-static {v0, v1, v1}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 104
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaac:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 105
    const-string v0, "analytics.first_party_experiment_variant"

    .line 106
    invoke-static {v0, v6, v6}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;II)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 107
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaad:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 108
    const-string v0, "analytics.test.disable_receiver"

    .line 109
    invoke-static {v0, v6, v6}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;ZZ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 110
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaae:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 111
    const-string v0, "analytics.service_client.idle_disconnect_millis"

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x2710

    .line 112
    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaaf:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 113
    const-string v0, "analytics.service_client.connect_timeout_millis"

    .line 114
    invoke-static {v0, v8, v9, v8, v9}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 115
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaag:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 116
    const-string v0, "analytics.service_client.second_connect_delay_millis"

    .line 117
    invoke-static {v0, v8, v9, v8, v9}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 118
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaah:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 119
    const-string v0, "analytics.service_client.unexpected_reconnect_millis"

    .line 120
    const-wide/32 v2, 0xea60

    const-wide/32 v4, 0xea60

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 121
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaai:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 122
    const-string v0, "analytics.service_client.reconnect_throttle_millis"

    .line 123
    const-wide/32 v2, 0x1b7740

    const-wide/32 v4, 0x1b7740

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 124
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaaj:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 125
    const-string v0, "analytics.monitoring.sample_period_millis"

    .line 126
    invoke-static {v0, v12, v13, v12, v13}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 127
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaak:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 128
    const-string v0, "analytics.initialization_warning_threshold"

    .line 129
    invoke-static {v0, v8, v9, v8, v9}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;JJ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 130
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaal:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 131
    const-string v0, "analytics.gcm_task_service"

    .line 132
    invoke-static {v0, v6, v6}, Lcom/google/android/gms/internal/measurement/zzcg;->zza(Ljava/lang/String;ZZ)Lcom/google/android/gms/internal/measurement/zzcg;

    move-result-object v0

    .line 133
    sput-object v0, Lcom/google/android/gms/internal/measurement/zzcf;->zzaam:Lcom/google/android/gms/internal/measurement/zzcg;

    .line 134
    return-void
.end method
