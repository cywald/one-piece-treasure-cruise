.class public final Lcom/google/android/gms/internal/measurement/zzp;
.super Lcom/google/android/gms/internal/measurement/zzza;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/zzza",
        "<",
        "Lcom/google/android/gms/internal/measurement/zzp;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile zzqi:[Lcom/google/android/gms/internal/measurement/zzp;


# instance fields
.field public string:Ljava/lang/String;

.field public type:I

.field public zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

.field public zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

.field public zzql:[Lcom/google/android/gms/internal/measurement/zzp;

.field public zzqm:Ljava/lang/String;

.field public zzqn:Ljava/lang/String;

.field public zzqo:J

.field public zzqp:Z

.field public zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

.field public zzqr:[I

.field public zzqs:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 10
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/zzza;-><init>()V

    .line 12
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    .line 14
    invoke-static {}, Lcom/google/android/gms/internal/measurement/zzp;->zzk()[Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 15
    invoke-static {}, Lcom/google/android/gms/internal/measurement/zzp;->zzk()[Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 16
    invoke-static {}, Lcom/google/android/gms/internal/measurement/zzp;->zzk()[Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    .line 20
    iput-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqp:Z

    .line 21
    invoke-static {}, Lcom/google/android/gms/internal/measurement/zzp;->zzk()[Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 22
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzzj;->zzcax:[I

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    .line 23
    iput-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzcfm:I

    .line 26
    return-void
.end method

.method private final zzb(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzp;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x50

    const/4 v1, 0x0

    .line 197
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    move-result v3

    .line 198
    sparse-switch v3, :sswitch_data_0

    .line 200
    invoke-super {p0, p1, v3}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyx;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    :sswitch_0
    return-object p0

    .line 202
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->getPosition()I

    move-result v0

    .line 204
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v2

    .line 206
    if-lez v2, :cond_1

    const/16 v4, 0x8

    if-gt v2, v4, :cond_1

    .line 209
    iput v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->type:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 212
    :catch_0
    move-exception v2

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zzby(I)V

    .line 213
    invoke-virtual {p0, p1, v3}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyx;I)Z

    goto :goto_0

    .line 208
    :cond_1
    :try_start_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x24

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " is not a valid enum Type"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 215
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    goto :goto_0

    .line 217
    :sswitch_3
    const/16 v0, 0x1a

    .line 218
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/measurement/zzzj;->zzb(Lcom/google/android/gms/internal/measurement/zzyx;I)I

    move-result v2

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v0, :cond_3

    move v0, v1

    .line 220
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/internal/measurement/zzp;

    .line 221
    if-eqz v0, :cond_2

    .line 222
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 223
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 224
    new-instance v3, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v3}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    aput-object v3, v2, v0

    .line 225
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    .line 226
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 219
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    goto :goto_1

    .line 228
    :cond_4
    new-instance v3, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v3}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    aput-object v3, v2, v0

    .line 229
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    .line 230
    iput-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    goto/16 :goto_0

    .line 232
    :sswitch_4
    const/16 v0, 0x22

    .line 233
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/measurement/zzzj;->zzb(Lcom/google/android/gms/internal/measurement/zzyx;I)I

    move-result v2

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v0, :cond_6

    move v0, v1

    .line 235
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/internal/measurement/zzp;

    .line 236
    if-eqz v0, :cond_5

    .line 237
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 238
    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    .line 239
    new-instance v3, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v3}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    aput-object v3, v2, v0

    .line 240
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    .line 241
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    .line 242
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 234
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    goto :goto_3

    .line 243
    :cond_7
    new-instance v3, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v3}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    aput-object v3, v2, v0

    .line 244
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    .line 245
    iput-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    goto/16 :goto_0

    .line 247
    :sswitch_5
    const/16 v0, 0x2a

    .line 248
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/measurement/zzzj;->zzb(Lcom/google/android/gms/internal/measurement/zzyx;I)I

    move-result v2

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v0, :cond_9

    move v0, v1

    .line 250
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/internal/measurement/zzp;

    .line 251
    if-eqz v0, :cond_8

    .line 252
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 253
    :cond_8
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    .line 254
    new-instance v3, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v3}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    aput-object v3, v2, v0

    .line 255
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    .line 256
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 249
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    goto :goto_5

    .line 258
    :cond_a
    new-instance v3, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v3}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    aput-object v3, v2, v0

    .line 259
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    .line 260
    iput-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    goto/16 :goto_0

    .line 262
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    goto/16 :goto_0

    .line 264
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    goto/16 :goto_0

    .line 267
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuz()J

    move-result-wide v2

    .line 268
    iput-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    goto/16 :goto_0

    .line 270
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzum()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    goto/16 :goto_0

    .line 273
    :sswitch_a
    invoke-static {p1, v8}, Lcom/google/android/gms/internal/measurement/zzzj;->zzb(Lcom/google/android/gms/internal/measurement/zzyx;I)I

    move-result v4

    .line 274
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 276
    :goto_7
    if-ge v2, v4, :cond_c

    .line 277
    if-eqz v2, :cond_b

    .line 278
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    .line 279
    :cond_b
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->getPosition()I

    move-result v6

    .line 281
    :try_start_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v7

    .line 282
    invoke-static {v7}, Lcom/google/android/gms/internal/measurement/zzp;->zzc(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 283
    add-int/lit8 v0, v0, 0x1

    .line 288
    :goto_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 286
    :catch_1
    move-exception v7

    invoke-virtual {p1, v6}, Lcom/google/android/gms/internal/measurement/zzyx;->zzby(I)V

    .line 287
    invoke-virtual {p0, p1, v3}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyx;I)Z

    goto :goto_8

    .line 289
    :cond_c
    if-eqz v0, :cond_0

    .line 290
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    if-nez v2, :cond_d

    move v2, v1

    .line 291
    :goto_9
    if-nez v2, :cond_e

    array-length v3, v5

    if-ne v0, v3, :cond_e

    .line 292
    iput-object v5, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    goto/16 :goto_0

    .line 290
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    array-length v2, v2

    goto :goto_9

    .line 293
    :cond_e
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 294
    if-eqz v2, :cond_f

    .line 295
    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 296
    :cond_f
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 297
    iput-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    goto/16 :goto_0

    .line 299
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v0

    .line 300
    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zzaq(I)I

    move-result v3

    .line 302
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->getPosition()I

    move-result v2

    move v0, v1

    .line 303
    :goto_a
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzyr()I

    move-result v4

    if-lez v4, :cond_10

    .line 305
    :try_start_3
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v4

    .line 306
    invoke-static {v4}, Lcom/google/android/gms/internal/measurement/zzp;->zzc(I)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    .line 307
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 311
    :cond_10
    if-eqz v0, :cond_14

    .line 312
    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/measurement/zzyx;->zzby(I)V

    .line 313
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    if-nez v2, :cond_12

    move v2, v1

    .line 314
    :goto_b
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 315
    if-eqz v2, :cond_11

    .line 316
    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 317
    :cond_11
    :goto_c
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzyr()I

    move-result v4

    if-lez v4, :cond_13

    .line 318
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->getPosition()I

    move-result v4

    .line 320
    :try_start_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuy()I

    move-result v5

    .line 321
    invoke-static {v5}, Lcom/google/android/gms/internal/measurement/zzp;->zzc(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    .line 322
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 313
    :cond_12
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    array-length v2, v2

    goto :goto_b

    .line 325
    :catch_2
    move-exception v5

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/measurement/zzyx;->zzby(I)V

    .line 326
    invoke-virtual {p0, p1, v8}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyx;I)Z

    goto :goto_c

    .line 328
    :cond_13
    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    .line 329
    :cond_14
    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/measurement/zzyx;->zzar(I)V

    goto/16 :goto_0

    .line 331
    :sswitch_c
    const/16 v0, 0x5a

    .line 332
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/measurement/zzzj;->zzb(Lcom/google/android/gms/internal/measurement/zzyx;I)I

    move-result v2

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v0, :cond_16

    move v0, v1

    .line 334
    :goto_d
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/internal/measurement/zzp;

    .line 335
    if-eqz v0, :cond_15

    .line 336
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 337
    :cond_15
    :goto_e
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_17

    .line 338
    new-instance v3, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v3}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    aput-object v3, v2, v0

    .line 339
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    .line 340
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    .line 341
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 333
    :cond_16
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    goto :goto_d

    .line 342
    :cond_17
    new-instance v3, Lcom/google/android/gms/internal/measurement/zzp;

    invoke-direct {v3}, Lcom/google/android/gms/internal/measurement/zzp;-><init>()V

    aput-object v3, v2, v0

    .line 343
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    .line 344
    iput-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    goto/16 :goto_0

    .line 346
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzum()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqp:Z

    goto/16 :goto_0

    .line 310
    :catch_3
    move-exception v4

    goto/16 :goto_a

    .line 198
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x52 -> :sswitch_b
        0x5a -> :sswitch_c
        0x60 -> :sswitch_d
    .end sparse-switch
.end method

.method private static zzc(I)I
    .locals 3

    .prologue
    .line 1
    if-lez p0, :cond_0

    const/16 v0, 0x11

    if-gt p0, v0, :cond_0

    .line 2
    return p0

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum Escaping"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static zzk()[Lcom/google/android/gms/internal/measurement/zzp;
    .locals 2

    .prologue
    .line 4
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzp;->zzqi:[Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v0, :cond_1

    .line 5
    sget-object v1, Lcom/google/android/gms/internal/measurement/zzze;->zzcfl:Ljava/lang/Object;

    monitor-enter v1

    .line 6
    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzp;->zzqi:[Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v0, :cond_0

    .line 7
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/measurement/zzp;

    sput-object v0, Lcom/google/android/gms/internal/measurement/zzp;->zzqi:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 8
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    :cond_1
    sget-object v0, Lcom/google/android/gms/internal/measurement/zzp;->zzqi:[Lcom/google/android/gms/internal/measurement/zzp;

    return-object v0

    .line 8
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 27
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v0

    .line 29
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/internal/measurement/zzp;

    if-nez v2, :cond_2

    move v0, v1

    .line 30
    goto :goto_0

    .line 31
    :cond_2
    check-cast p1, Lcom/google/android/gms/internal/measurement/zzp;

    .line 32
    iget v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    iget v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 33
    goto :goto_0

    .line 34
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 35
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 36
    goto :goto_0

    .line 37
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 38
    goto :goto_0

    .line 39
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzze;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 40
    goto :goto_0

    .line 41
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzze;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 42
    goto :goto_0

    .line 43
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzze;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 44
    goto :goto_0

    .line 45
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 46
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 47
    goto :goto_0

    .line 48
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 49
    goto :goto_0

    .line 50
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 51
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 52
    goto :goto_0

    .line 53
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 54
    goto :goto_0

    .line 55
    :cond_c
    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    iget-wide v4, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    move v0, v1

    .line 56
    goto :goto_0

    .line 57
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqp:Z

    iget-boolean v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqp:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 58
    goto/16 :goto_0

    .line 59
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzze;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 60
    goto/16 :goto_0

    .line 61
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzze;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 62
    goto/16 :goto_0

    .line 63
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    iget-boolean v3, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 64
    goto/16 :goto_0

    .line 65
    :cond_11
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 66
    :cond_12
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 67
    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/zzzc;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 68
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 69
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    add-int/2addr v0, v4

    .line 70
    mul-int/lit8 v4, v0, 0x1f

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 72
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 73
    invoke-static {v4}, Lcom/google/android/gms/internal/measurement/zzze;->hashCode([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 74
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 75
    invoke-static {v4}, Lcom/google/android/gms/internal/measurement/zzze;->hashCode([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 76
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 77
    invoke-static {v4}, Lcom/google/android/gms/internal/measurement/zzze;->hashCode([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 78
    mul-int/lit8 v4, v0, 0x1f

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 80
    mul-int/lit8 v4, v0, 0x1f

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 82
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    iget-wide v6, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 83
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqp:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    .line 85
    invoke-static {v4}, Lcom/google/android/gms/internal/measurement/zzze;->hashCode([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    .line 87
    invoke-static {v4}, Lcom/google/android/gms/internal/measurement/zzze;->hashCode([I)I

    move-result v4

    add-int/2addr v0, v4

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    if-eqz v4, :cond_5

    :goto_4
    add-int/2addr v0, v2

    .line 89
    mul-int/lit8 v0, v0, 0x1f

    .line 90
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 91
    :cond_0
    :goto_5
    add-int/2addr v0, v1

    .line 92
    return v0

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    move v0, v3

    .line 83
    goto :goto_3

    :cond_5
    move v2, v3

    .line 88
    goto :goto_4

    .line 91
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/zzzc;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic zza(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/measurement/zzp;->zzb(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzp;

    move-result-object v0

    return-object v0
.end method

.method public final zza(Lcom/google/android/gms/internal/measurement/zzyy;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 93
    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzd(II)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILjava/lang/String;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 97
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 98
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    aget-object v2, v2, v0

    .line 99
    if-eqz v2, :cond_1

    .line 100
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zza(ILcom/google/android/gms/internal/measurement/zzzg;)V

    .line 101
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 103
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 104
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    aget-object v2, v2, v0

    .line 105
    if-eqz v2, :cond_3

    .line 106
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zza(ILcom/google/android/gms/internal/measurement/zzzg;)V

    .line 107
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 109
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 110
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    aget-object v2, v2, v0

    .line 111
    if-eqz v2, :cond_5

    .line 112
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zza(ILcom/google/android/gms/internal/measurement/zzzg;)V

    .line 113
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 114
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 115
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILjava/lang/String;)V

    .line 116
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 117
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILjava/lang/String;)V

    .line 118
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_9

    .line 119
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzi(IJ)V

    .line 120
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    if-eqz v0, :cond_a

    .line 121
    const/16 v0, 0x9

    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(IZ)V

    .line 122
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    .line 123
    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    array-length v2, v2

    if-ge v0, v2, :cond_b

    .line 124
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzd(II)V

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 126
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    if-lez v0, :cond_d

    .line 127
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    aget-object v0, v0, v1

    .line 129
    if-eqz v0, :cond_c

    .line 130
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/android/gms/internal/measurement/zzyy;->zza(ILcom/google/android/gms/internal/measurement/zzzg;)V

    .line 131
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 132
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqp:Z

    if-eqz v0, :cond_e

    .line 133
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqp:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(IZ)V

    .line 134
    :cond_e
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyy;)V

    .line 135
    return-void
.end method

.method protected final zzf()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 136
    invoke-super {p0}, Lcom/google/android/gms/internal/measurement/zzza;->zzf()I

    move-result v0

    .line 137
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->type:I

    .line 138
    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzh(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 139
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 140
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->string:Ljava/lang/String;

    .line 141
    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzc(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 142
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 143
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 144
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqj:[Lcom/google/android/gms/internal/measurement/zzp;

    aget-object v3, v3, v0

    .line 145
    if-eqz v3, :cond_1

    .line 146
    const/4 v4, 0x3

    .line 147
    invoke-static {v4, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILcom/google/android/gms/internal/measurement/zzzg;)I

    move-result v3

    add-int/2addr v2, v3

    .line 148
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 149
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 150
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 151
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqk:[Lcom/google/android/gms/internal/measurement/zzp;

    aget-object v3, v3, v0

    .line 152
    if-eqz v3, :cond_4

    .line 153
    const/4 v4, 0x4

    .line 154
    invoke-static {v4, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILcom/google/android/gms/internal/measurement/zzzg;)I

    move-result v3

    add-int/2addr v2, v3

    .line 155
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v2

    .line 156
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v0

    move v0, v1

    .line 157
    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v3, v3

    if-ge v0, v3, :cond_8

    .line 158
    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzql:[Lcom/google/android/gms/internal/measurement/zzp;

    aget-object v3, v3, v0

    .line 159
    if-eqz v3, :cond_7

    .line 160
    const/4 v4, 0x5

    .line 161
    invoke-static {v4, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILcom/google/android/gms/internal/measurement/zzzg;)I

    move-result v3

    add-int/2addr v2, v3

    .line 162
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    move v0, v2

    .line 163
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 164
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqm:Ljava/lang/String;

    .line 165
    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzc(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 166
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 167
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqn:Ljava/lang/String;

    .line 168
    invoke-static {v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzc(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 169
    :cond_b
    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 170
    const/16 v2, 0x8

    iget-wide v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqo:J

    .line 171
    invoke-static {v2, v4, v5}, Lcom/google/android/gms/internal/measurement/zzyy;->zzd(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 172
    :cond_c
    iget-boolean v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqs:Z

    if-eqz v2, :cond_d

    .line 173
    const/16 v2, 0x9

    .line 174
    invoke-static {v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzbb(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 175
    add-int/2addr v0, v2

    .line 176
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    array-length v2, v2

    if-lez v2, :cond_f

    move v2, v1

    move v3, v1

    .line 178
    :goto_3
    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    array-length v4, v4

    if-ge v2, v4, :cond_e

    .line 179
    iget-object v4, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    aget v4, v4, v2

    .line 181
    invoke-static {v4}, Lcom/google/android/gms/internal/measurement/zzyy;->zzbc(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 182
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 183
    :cond_e
    add-int/2addr v0, v3

    .line 184
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqr:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 185
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 186
    :goto_4
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 187
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqq:[Lcom/google/android/gms/internal/measurement/zzp;

    aget-object v2, v2, v1

    .line 188
    if-eqz v2, :cond_10

    .line 189
    const/16 v3, 0xb

    .line 190
    invoke-static {v3, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILcom/google/android/gms/internal/measurement/zzzg;)I

    move-result v2

    add-int/2addr v0, v2

    .line 191
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 192
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/gms/internal/measurement/zzp;->zzqp:Z

    if-eqz v1, :cond_12

    .line 193
    const/16 v1, 0xc

    .line 194
    invoke-static {v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zzbb(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 195
    add-int/2addr v0, v1

    .line 196
    :cond_12
    return v0
.end method
