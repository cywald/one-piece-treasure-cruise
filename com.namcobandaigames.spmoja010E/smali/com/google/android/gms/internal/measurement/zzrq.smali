.class public final Lcom/google/android/gms/internal/measurement/zzrq;
.super Lcom/google/android/gms/internal/measurement/zzza;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/measurement/zzza",
        "<",
        "Lcom/google/android/gms/internal/measurement/zzrq;",
        ">;"
    }
.end annotation


# instance fields
.field public zzbps:J

.field public zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

.field public zzqg:Lcom/google/android/gms/internal/measurement/zzl;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/measurement/zzza;-><init>()V

    .line 3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbps:J

    .line 4
    iput-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    .line 5
    iput-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    .line 6
    iput-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfm:I

    .line 8
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9
    if-ne p1, p0, :cond_1

    .line 28
    :cond_0
    :goto_0
    return v0

    .line 11
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/internal/measurement/zzrq;

    if-nez v2, :cond_2

    move v0, v1

    .line 12
    goto :goto_0

    .line 13
    :cond_2
    check-cast p1, Lcom/google/android/gms/internal/measurement/zzrq;

    .line 14
    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbps:J

    iget-wide v4, p1, Lcom/google/android/gms/internal/measurement/zzrq;->zzbps:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 15
    goto :goto_0

    .line 16
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    if-nez v2, :cond_4

    .line 17
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    if-eqz v2, :cond_5

    move v0, v1

    .line 18
    goto :goto_0

    .line 19
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/measurement/zzl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 20
    goto :goto_0

    .line 21
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    if-nez v2, :cond_6

    .line 22
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    if-eqz v2, :cond_7

    move v0, v1

    .line 23
    goto :goto_0

    .line 24
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    iget-object v3, p1, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/measurement/zzo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 25
    goto :goto_0

    .line 26
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 27
    :cond_8
    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 28
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    iget-object v1, p1, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/measurement/zzzc;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 30
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbps:J

    iget-wide v4, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbps:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 31
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    .line 32
    mul-int/lit8 v3, v0, 0x1f

    .line 33
    if-nez v2, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 34
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    .line 35
    mul-int/lit8 v3, v0, 0x1f

    .line 36
    if-nez v2, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 37
    mul-int/lit8 v0, v0, 0x1f

    .line 38
    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzzc;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 39
    :cond_0
    :goto_2
    add-int/2addr v0, v1

    .line 40
    return v0

    .line 33
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzl;->hashCode()I

    move-result v0

    goto :goto_0

    .line 36
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/internal/measurement/zzo;->hashCode()I

    move-result v0

    goto :goto_1

    .line 39
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzcfc:Lcom/google/android/gms/internal/measurement/zzzc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/measurement/zzzc;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic zza(Lcom/google/android/gms/internal/measurement/zzyx;)Lcom/google/android/gms/internal/measurement/zzzg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    .line 59
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzug()I

    move-result v0

    .line 60
    sparse-switch v0, :sswitch_data_0

    .line 62
    invoke-super {p0, p1, v0}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyx;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    :sswitch_0
    return-object p0

    .line 65
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/measurement/zzyx;->zzuz()J

    move-result-wide v0

    .line 66
    iput-wide v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbps:J

    goto :goto_0

    .line 68
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    if-nez v0, :cond_1

    .line 69
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzl;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/zzl;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    goto :goto_0

    .line 72
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    if-nez v0, :cond_2

    .line 73
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzo;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/zzo;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/measurement/zzyx;->zza(Lcom/google/android/gms/internal/measurement/zzzg;)V

    goto :goto_0

    .line 60
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final zza(Lcom/google/android/gms/internal/measurement/zzyy;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbps:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzi(IJ)V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zza(ILcom/google/android/gms/internal/measurement/zzzg;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    if-eqz v0, :cond_1

    .line 45
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/measurement/zzyy;->zza(ILcom/google/android/gms/internal/measurement/zzzg;)V

    .line 46
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/measurement/zzza;->zza(Lcom/google/android/gms/internal/measurement/zzyy;)V

    .line 47
    return-void
.end method

.method protected final zzf()I
    .locals 4

    .prologue
    .line 48
    invoke-super {p0}, Lcom/google/android/gms/internal/measurement/zzza;->zzf()I

    move-result v0

    .line 49
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbps:J

    .line 50
    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/measurement/zzyy;->zzd(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    if-eqz v1, :cond_0

    .line 52
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzqg:Lcom/google/android/gms/internal/measurement/zzl;

    .line 53
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILcom/google/android/gms/internal/measurement/zzzg;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    if-eqz v1, :cond_1

    .line 55
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/internal/measurement/zzrq;->zzbpt:Lcom/google/android/gms/internal/measurement/zzo;

    .line 56
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/measurement/zzyy;->zzb(ILcom/google/android/gms/internal/measurement/zzzg;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_1
    return v0
.end method
