.class public Lcom/adjust/sdk/Adjust2dxDeferredDeeplinkCallback;
.super Ljava/lang/Object;
.source "Adjust2dxDeferredDeeplinkCallback.java"

# interfaces
.implements Lcom/adjust/sdk/OnDeeplinkResponseListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native deferredDeeplinkReceived(Ljava/lang/String;)Z
.end method

.method public launchReceivedDeeplink(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/adjust/sdk/Adjust2dxDeferredDeeplinkCallback;->deferredDeeplinkReceived(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
