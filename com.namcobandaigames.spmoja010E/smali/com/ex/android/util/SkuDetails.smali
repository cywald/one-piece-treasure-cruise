.class public Lcom/ex/android/util/SkuDetails;
.super Ljava/lang/Object;
.source "SkuDetails.java"


# instance fields
.field private final mDescription:Ljava/lang/String;

.field private final mItemType:Ljava/lang/String;

.field private final mJson:Ljava/lang/String;

.field private final mPrice:Ljava/lang/String;

.field private final mPriceAmountMicros:J

.field private final mPriceAmountMicrosString:Ljava/lang/String;

.field private final mPriceCurrencyCode:Ljava/lang/String;

.field private final mPriceValueFloat:F

.field private final mPriceValueString:Ljava/lang/String;

.field private final mSku:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field private final mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "jsonSkuDetails"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 43
    const-string v0, "inapp"

    invoke-direct {p0, v0, p1}, Lcom/ex/android/util/SkuDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "itemType"    # Ljava/lang/String;
    .param p2, "jsonSkuDetails"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/ex/android/util/SkuDetails;->mItemType:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/ex/android/util/SkuDetails;->mJson:Ljava/lang/String;

    .line 49
    new-instance v1, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/ex/android/util/SkuDetails;->mJson:Ljava/lang/String;

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 50
    .local v1, "o":Lorg/json/JSONObject;
    const-string v3, "productId"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mSku:Ljava/lang/String;

    .line 51
    const-string v3, "type"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mType:Ljava/lang/String;

    .line 52
    const-string v3, "price"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mPrice:Ljava/lang/String;

    .line 53
    const-string v3, "price_amount_micros"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/ex/android/util/SkuDetails;->mPriceAmountMicros:J

    .line 54
    const-string v3, "price_currency_code"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceCurrencyCode:Ljava/lang/String;

    .line 55
    const-string v3, "title"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mTitle:Ljava/lang/String;

    .line 56
    const-string v3, "description"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mDescription:Ljava/lang/String;

    .line 58
    const-string v3, "price_amount_micros"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceAmountMicrosString:Ljava/lang/String;

    .line 59
    iget-object v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceAmountMicrosString:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceAmountMicrosString:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-lt v3, v4, :cond_0

    .line 60
    new-instance v2, Ljava/math/BigDecimal;

    iget-object v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceAmountMicrosString:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 61
    .local v2, "princeAmountDecimal":Ljava/math/BigDecimal;
    new-instance v3, Ljava/math/BigDecimal;

    const v4, 0xf4240

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 62
    invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F

    move-result v3

    iput v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceValueFloat:F

    .line 63
    sget-object v3, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-static {v3}, Ljava/text/DecimalFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    .line 64
    .local v0, "decimalFormat":Ljava/text/DecimalFormat;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/text/DecimalFormat;->setGroupingUsed(Z)V

    .line 65
    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceValueString:Ljava/lang/String;

    .line 71
    .end local v0    # "decimalFormat":Ljava/text/DecimalFormat;
    .end local v2    # "princeAmountDecimal":Ljava/math/BigDecimal;
    :goto_0
    return-void

    .line 68
    :cond_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceValueFloat:F

    .line 69
    const-string v3, ""

    iput-object v3, p0, Lcom/ex/android/util/SkuDetails;->mPriceValueString:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/ex/android/util/SkuDetails;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/ex/android/util/SkuDetails;->mPrice:Ljava/lang/String;

    return-object v0
.end method

.method public getPriceAmountMicros()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/ex/android/util/SkuDetails;->mPriceAmountMicros:J

    return-wide v0
.end method

.method public getPriceCurrencyCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/ex/android/util/SkuDetails;->mPriceCurrencyCode:Ljava/lang/String;

    return-object v0
.end method

.method public getPriceValueFloat()F
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/ex/android/util/SkuDetails;->mPriceValueFloat:F

    return v0
.end method

.method public getPriceValueString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/ex/android/util/SkuDetails;->mPriceValueString:Ljava/lang/String;

    return-object v0
.end method

.method public getSku()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/ex/android/util/SkuDetails;->mSku:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/ex/android/util/SkuDetails;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/ex/android/util/SkuDetails;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SkuDetails:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/ex/android/util/SkuDetails;->mJson:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
