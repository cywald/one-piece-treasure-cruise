.class public Lcom/namcobandaigames/spmoja010E/kizakura;
.super Lorg/cocos2dx/lib/Cocos2dxActivity;
.source "kizakura.java"

# interfaces
.implements Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;
.implements Ljp/co/drecom/bisque/lib/BQTouchDispatcher;
.implements Ljp/co/drecom/bisque/lib/BQPaymentDispatchable;
.implements Ljp/co/drecom/bisque/lib/BQUpdateDispatcher;
.implements Ljp/co/drecom/bisque/lib/BQClipboardDispatcher;
.implements Ljp/co/drecom/bisque/lib/BQHandlerDispatcher;
.implements Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcher;
.implements Landroid/view/View$OnTouchListener;
.implements Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcher;
.implements Landroid/support/v4/app/ActivityCompat$OnRequestPermissionsResultCallback;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "kizakura"

.field private static final META_DATA_KEY_ENABLE_MOVIEPLAYER:Ljava/lang/String; = "jp.co.drecom.bisque.enable.movieplayer"

.field private static final META_DATA_KEY_ENABLE_TRANSPARENT:Ljava/lang/String; = "jp.co.drecom.bisque.enable.glsurfaceview.transparent"

.field private static final META_DATA_KEY_GMS_APP_ID:Ljava/lang/String; = "com.google.android.gms.games.APP_ID"

.field private static final META_DATA_KEY_GMS_VERSION:Ljava/lang/String; = "com.google.android.gms.version"


# instance fields
.field private bqhandler:Ljp/co/drecom/bisque/lib/BQHandler;

.field private bqupdate:Ljp/co/drecom/bisque/lib/BQUpdate;

.field private bqurlscheme:Ljp/co/drecom/bisque/lib/BQUrlScheme;

.field private clipboard:Ljp/co/drecom/bisque/lib/BQClipboard;

.field private deviceSetting:Ljp/co/drecom/bisque/lib/BQSleepSetting;

.field private enableGlSurfaceviewTransparent:Z

.field private handler:Landroid/os/Handler;

.field private layout:Landroid/widget/RelativeLayout;

.field private paymentBridge:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

.field private playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 284
    const-string v0, "game"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 285
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;-><init>()V

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->enableGlSurfaceviewTransparent:Z

    .line 123
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    .line 125
    new-instance v0, Ljp/co/drecom/bisque/lib/BQClipboard;

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQClipboard;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->clipboard:Ljp/co/drecom/bisque/lib/BQClipboard;

    .line 126
    new-instance v0, Ljp/co/drecom/bisque/lib/BQHandler;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQHandler;-><init>()V

    iput-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqhandler:Ljp/co/drecom/bisque/lib/BQHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/namcobandaigames/spmoja010E/kizakura;)Ljp/co/drecom/bisque/lib/BQHandler;
    .locals 1
    .param p0, "x0"    # Lcom/namcobandaigames/spmoja010E/kizakura;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqhandler:Ljp/co/drecom/bisque/lib/BQHandler;

    return-object v0
.end method

.method private canUseAchievement()Z
    .locals 6

    .prologue
    .line 776
    const/4 v1, 0x0

    .line 779
    .local v1, "canUseGameService":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 780
    .local v0, "appliInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v3, :cond_0

    .line 781
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "com.google.android.gms.games.APP_ID"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "com.google.android.gms.version"

    .line 782
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    .line 787
    .end local v0    # "appliInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return v1

    .line 782
    .restart local v0    # "appliInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 784
    .end local v0    # "appliInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    .line 785
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;
    .locals 4
    .param p1, "tag"    # I

    .prologue
    .line 404
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 405
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Ljp/co/drecom/bisque/lib/BQWebView;

    if-ne v2, v3, :cond_0

    .line 406
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ljp/co/drecom/bisque/lib/BQWebView;

    .line 407
    .local v1, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    iget v2, v1, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    if-ne p1, v2, :cond_0

    .line 412
    .end local v1    # "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    :goto_1
    return-object v1

    .line 404
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 412
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private hideSystemUI()V
    .locals 2

    .prologue
    .line 885
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 886
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->mGLSurfaceView:Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    const/16 v1, 0x1706

    invoke-virtual {v0, v1}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->setSystemUiVisibility(I)V

    .line 894
    :cond_0
    return-void
.end method

.method private initializeTypefaces()V
    .locals 1

    .prologue
    .line 868
    new-instance v0, Lcom/namcobandaigames/spmoja010E/kizakura$1BQTypefacesImplement;

    invoke-direct {v0, p0}, Lcom/namcobandaigames/spmoja010E/kizakura$1BQTypefacesImplement;-><init>(Lcom/namcobandaigames/spmoja010E/kizakura;)V

    .line 869
    .local v0, "bqtypefaces":Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;
    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQTypefaces;->registImplement(Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;)V

    .line 870
    return-void
.end method

.method private isEnableMoviePlayer()Z
    .locals 6

    .prologue
    .line 845
    const/4 v2, 0x0

    .line 848
    .local v2, "enable":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 849
    .local v0, "appliInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v3, :cond_0

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "jp.co.drecom.bisque.enable.movieplayer"

    .line 850
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 851
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "jp.co.drecom.bisque.enable.movieplayer"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 856
    .end local v0    # "appliInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return v2

    .line 853
    :catch_0
    move-exception v1

    .line 854
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private snsAuthnicationCallbackInternal(Landroid/content/Intent;Z)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "invokedByOnNewIntent"    # Z

    .prologue
    .line 827
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_1

    .line 842
    :cond_0
    :goto_0
    return-void

    .line 830
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "onepiecetc.gb://sns-connection"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 831
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "code"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 832
    .local v0, "code":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "operation"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 833
    .local v1, "operation":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "token"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 834
    .local v3, "token":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "name"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 835
    .local v2, "sns_name":Ljava/lang/String;
    if-nez v3, :cond_2

    const-string v3, ""

    .line 836
    :cond_2
    if-nez v2, :cond_3

    const-string v2, ""

    .line 838
    :cond_3
    if-eqz v0, :cond_0

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 839
    invoke-static {v0, v1, v3, v2}, Ljp/co/drecom/util/config/AppConfigure;->callbackSnsAuthnication(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private twitterCallbackInternal(Landroid/content/Intent;Z)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "invokedByOnNewIntent"    # Z

    .prologue
    .line 811
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_1

    .line 824
    :cond_0
    :goto_0
    return-void

    .line 814
    :cond_1
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 815
    .local v0, "twiterCallackURL":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 816
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "oauth_verifier"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 817
    .local v1, "verifier":Ljava/lang/String;
    if-nez v1, :cond_2

    const-string v1, ""

    .line 818
    :cond_2
    invoke-static {v1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->setVerifier(Ljava/lang/String;)V

    .line 819
    if-eqz p2, :cond_0

    .line 820
    invoke-static {v1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->gotOAuthVerifier(Ljava/lang/String;)V

    .line 821
    const/4 v2, 0x0

    invoke-static {v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->setExecutingBrowser(Z)V

    goto :goto_0
.end method


# virtual methods
.method public addWebView(IIIILjava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "url"    # Ljava/lang/String;
    .param p6, "tag"    # I
    .param p7, "order"    # I
    .param p8, "bounces"    # I
    .param p9, "auth"    # Z
    .param p10, "username"    # Ljava/lang/String;
    .param p11, "password"    # Ljava/lang/String;

    .prologue
    .line 417
    new-instance v1, Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {}, Ljp/co/drecom/util/config/AppConfigure;->getWebViewErrorTemplate()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, p0, v2}, Ljp/co/drecom/bisque/lib/BQWebView;-><init>(Landroid/content/Context;Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;Ljava/lang/String;)V

    .line 418
    .local v1, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    invoke-virtual {v1, p9, p10, p11}, Ljp/co/drecom/bisque/lib/BQWebView;->setAuthInfo(ZLjava/lang/String;Ljava/lang/String;)V

    .line 419
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    if-ne p8, v2, :cond_2

    invoke-virtual {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->bouncesOff()V

    .line 421
    :cond_0
    :goto_0
    iput p6, v1, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    .line 422
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p3, p4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 423
    .local v0, "webview_params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, p2, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 424
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1, p7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 425
    invoke-virtual {v1, p0}, Ljp/co/drecom/bisque/lib/BQWebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 426
    invoke-virtual {v1, p5}, Ljp/co/drecom/bisque/lib/BQWebView;->loadUrl(Ljava/lang/String;)V

    .line 427
    iget-boolean v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->enableGlSurfaceviewTransparent:Z

    if-nez v2, :cond_1

    .line 428
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront(Z)V

    .line 430
    :cond_1
    const/4 v2, 0x1

    return v2

    .line 420
    .end local v0    # "webview_params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x2

    if-ne p8, v2, :cond_0

    invoke-virtual {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->bouncesOn()V

    goto :goto_0
.end method

.method public addWebView(IIIILjava/lang/String;Ljava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "url"    # Ljava/lang/String;
    .param p6, "post"    # Ljava/lang/String;
    .param p7, "tag"    # I
    .param p8, "order"    # I
    .param p9, "bounces"    # I
    .param p10, "auth"    # Z
    .param p11, "username"    # Ljava/lang/String;
    .param p12, "password"    # Ljava/lang/String;

    .prologue
    .line 458
    new-instance v6, Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {}, Ljp/co/drecom/util/config/AppConfigure;->getWebViewErrorTemplate()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p0, p0, v7}, Ljp/co/drecom/bisque/lib/BQWebView;-><init>(Landroid/content/Context;Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;Ljava/lang/String;)V

    .line 459
    .local v6, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    move/from16 v0, p10

    move-object/from16 v1, p11

    move-object/from16 v2, p12

    invoke-virtual {v6, v0, v1, v2}, Ljp/co/drecom/bisque/lib/BQWebView;->setAuthInfo(ZLjava/lang/String;Ljava/lang/String;)V

    .line 460
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x1

    move/from16 v0, p9

    if-ne v0, v7, :cond_2

    invoke-virtual {v6}, Ljp/co/drecom/bisque/lib/BQWebView;->bouncesOff()V

    .line 462
    :cond_0
    :goto_0
    move/from16 v0, p7

    iput v0, v6, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    .line 463
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, p3, p4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 464
    .local v5, "webview_params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, p1, p2, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 465
    iget-object v7, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    move/from16 v0, p8

    invoke-virtual {v7, v6, v0, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 466
    invoke-virtual {v6, p0}, Ljp/co/drecom/bisque/lib/BQWebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 469
    :try_start_0
    const-string v7, "UTF-8"

    invoke-virtual {p6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 473
    .local v4, "postData":[B
    invoke-virtual {v6, p5, v4}, Ljp/co/drecom/bisque/lib/BQWebView;->postUrl(Ljava/lang/String;[B)V

    .line 474
    iget-boolean v7, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->enableGlSurfaceviewTransparent:Z

    if-nez v7, :cond_1

    .line 475
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront(Z)V

    .line 477
    :cond_1
    const/4 v7, 0x1

    .end local v4    # "postData":[B
    :goto_1
    return v7

    .line 461
    .end local v5    # "webview_params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x2

    move/from16 v0, p9

    if-ne v0, v7, :cond_0

    invoke-virtual {v6}, Ljp/co/drecom/bisque/lib/BQWebView;->bouncesOn()V

    goto :goto_0

    .line 470
    .restart local v5    # "webview_params":Landroid/widget/RelativeLayout$LayoutParams;
    :catch_0
    move-exception v3

    .line 471
    .local v3, "ex":Ljava/io/UnsupportedEncodingException;
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public addWebView(IIIILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "url"    # Ljava/lang/String;
    .param p6, "headers"    # [Ljava/lang/String;
    .param p7, "values"    # [Ljava/lang/String;
    .param p8, "tag"    # I
    .param p9, "order"    # I
    .param p10, "bounces"    # I
    .param p11, "auth"    # Z
    .param p12, "username"    # Ljava/lang/String;
    .param p13, "password"    # Ljava/lang/String;

    .prologue
    .line 435
    new-instance v6, Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {}, Ljp/co/drecom/util/config/AppConfigure;->getWebViewErrorTemplate()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p0, p0, v7}, Ljp/co/drecom/bisque/lib/BQWebView;-><init>(Landroid/content/Context;Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;Ljava/lang/String;)V

    .line 436
    .local v6, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    move/from16 v0, p11

    move-object/from16 v1, p12

    move-object/from16 v2, p13

    invoke-virtual {v6, v0, v1, v2}, Ljp/co/drecom/bisque/lib/BQWebView;->setAuthInfo(ZLjava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x1

    move/from16 v0, p10

    if-ne v0, v7, :cond_1

    invoke-virtual {v6}, Ljp/co/drecom/bisque/lib/BQWebView;->bouncesOff()V

    .line 439
    :cond_0
    :goto_0
    move/from16 v0, p8

    iput v0, v6, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    .line 440
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, p3, p4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 441
    .local v5, "webview_params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, p1, p2, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 442
    iget-object v7, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    move/from16 v0, p9

    invoke-virtual {v7, v6, v0, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 443
    invoke-virtual {v6, p0}, Ljp/co/drecom/bisque/lib/BQWebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 444
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 445
    .local v3, "HTTPHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v7, p6

    if-ge v4, v7, :cond_2

    .line 447
    aget-object v7, p6, v4

    aget-object v8, p7, v4

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 438
    .end local v3    # "HTTPHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "i":I
    .end local v5    # "webview_params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x2

    move/from16 v0, p10

    if-ne v0, v7, :cond_0

    invoke-virtual {v6}, Ljp/co/drecom/bisque/lib/BQWebView;->bouncesOn()V

    goto :goto_0

    .line 449
    .restart local v3    # "HTTPHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "i":I
    .restart local v5    # "webview_params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    invoke-virtual {v6, p5, v3}, Ljp/co/drecom/bisque/lib/BQWebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 450
    iget-boolean v7, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->enableGlSurfaceviewTransparent:Z

    if-nez v7, :cond_3

    .line 451
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront(Z)V

    .line 453
    :cond_3
    const/4 v7, 0x1

    return v7
.end method

.method public bouncesWebView(ZI)V
    .locals 1
    .param p1, "b"    # Z
    .param p2, "tag"    # I

    .prologue
    .line 566
    invoke-direct {p0, p2}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v0

    .line 567
    .local v0, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v0, :cond_0

    .line 575
    :goto_0
    return-void

    .line 570
    :cond_0
    if-eqz p1, :cond_1

    .line 571
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->bouncesOn()V

    goto :goto_0

    .line 573
    :cond_1
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->bouncesOff()V

    goto :goto_0
.end method

.method public callbackForWebViewDidFailLoadWithError(Ljava/lang/String;)V
    .locals 0
    .param p1, "err"    # Ljava/lang/String;

    .prologue
    .line 399
    invoke-static {p1}, Ljp/co/drecom/bisque/lib/BQWebView;->nativeCallbackFuncForWebViewDidFailLoadWithError(Ljava/lang/String;)V

    .line 400
    return-void
.end method

.method public callbackForWebViewDidFinishLoad(I)V
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 388
    move v0, p1

    .line 389
    .local v0, "wvtag":I
    new-instance v1, Lcom/namcobandaigames/spmoja010E/kizakura$3;

    invoke-direct {v1, p0, v0}, Lcom/namcobandaigames/spmoja010E/kizakura$3;-><init>(Lcom/namcobandaigames/spmoja010E/kizakura;I)V

    invoke-virtual {p0, v1}, Lcom/namcobandaigames/spmoja010E/kizakura;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 395
    return-void
.end method

.method public callbackForWebViewShouldStartLoadWithRequest(Ljava/lang/String;I)Z
    .locals 10
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "tag"    # I

    .prologue
    .line 349
    move-object v6, p1

    .line 350
    .local v6, "string":Ljava/lang/String;
    move v8, p2

    .line 356
    .local v8, "wvtag":I
    new-instance v7, Lcom/namcobandaigames/spmoja010E/kizakura$2;

    invoke-direct {v7, p0, v6, v8}, Lcom/namcobandaigames/spmoja010E/kizakura$2;-><init>(Lcom/namcobandaigames/spmoja010E/kizakura;Ljava/lang/String;I)V

    .line 365
    .local v7, "task":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/namcobandaigames/spmoja010E/kizakura$1Result;>;"
    const/4 v2, 0x0

    .line 367
    .local v2, "executor":Ljava/util/concurrent/ExecutorService;
    :try_start_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    .line 368
    invoke-interface {v2, v7}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 369
    .local v3, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/namcobandaigames/spmoja010E/kizakura$1Result;>;"
    const/4 v4, 0x0

    .line 371
    .local v4, "result":Lcom/namcobandaigames/spmoja010E/kizakura$1Result;
    :try_start_1
    invoke-interface {v3}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/namcobandaigames/spmoja010E/kizakura$1Result;

    move-object v4, v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377
    :goto_0
    :try_start_2
    iget-boolean v5, v4, Lcom/namcobandaigames/spmoja010E/kizakura$1Result;->ret:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 379
    .local v5, "ret":Z
    if-eqz v2, :cond_0

    .line 380
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 383
    :cond_0
    return v5

    .line 372
    .end local v5    # "ret":Z
    :catch_0
    move-exception v1

    .line 373
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 379
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v3    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/namcobandaigames/spmoja010E/kizakura$1Result;>;"
    .end local v4    # "result":Lcom/namcobandaigames/spmoja010E/kizakura$1Result;
    :catchall_0
    move-exception v9

    if-eqz v2, :cond_1

    .line 380
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 382
    :cond_1
    throw v9

    .line 374
    .restart local v3    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/namcobandaigames/spmoja010E/kizakura$1Result;>;"
    .restart local v4    # "result":Lcom/namcobandaigames/spmoja010E/kizakura$1Result;
    :catch_1
    move-exception v1

    .line 375
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_4
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public checkUpdate(ZILjava/lang/String;)V
    .locals 1
    .param p1, "bqnotify"    # Z
    .param p2, "android_latestversion"    # I
    .param p3, "android_update_uri"    # Ljava/lang/String;

    .prologue
    .line 670
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqupdate:Ljp/co/drecom/bisque/lib/BQUpdate;

    invoke-virtual {v0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQUpdate;->checkUpdate(ZILjava/lang/String;)V

    .line 671
    return-void
.end method

.method public clearCache(I)V
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 536
    invoke-direct {p0, p1}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v0

    .line 537
    .local v0, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v0, :cond_0

    .line 543
    :goto_0
    return-void

    .line 542
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/BQWebView;->clearCache(Z)V

    goto :goto_0
.end method

.method public clearCookie(I)V
    .locals 4
    .param p1, "tag"    # I

    .prologue
    .line 546
    invoke-direct {p0, p1}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v2

    .line 547
    .local v2, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v2, :cond_0

    .line 562
    :goto_0
    return-void

    .line 552
    :cond_0
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 554
    .local v0, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v1

    .line 555
    .local v1, "cookieSyncManager":Landroid/webkit/CookieSyncManager;
    invoke-virtual {v1}, Landroid/webkit/CookieSyncManager;->startSync()V

    .line 557
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 558
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeSessionCookie()V

    .line 560
    invoke-virtual {v1}, Landroid/webkit/CookieSyncManager;->stopSync()V

    .line 561
    invoke-virtual {v1}, Landroid/webkit/CookieSyncManager;->sync()V

    goto :goto_0
.end method

.method public enableCache(ZI)V
    .locals 3
    .param p1, "b"    # Z
    .param p2, "tag"    # I

    .prologue
    .line 515
    invoke-direct {p0, p2}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v0

    .line 516
    .local v0, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v0, :cond_0

    .line 522
    :goto_0
    return-void

    .line 521
    :cond_0
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    if-eqz p1, :cond_1

    const/4 v1, -0x1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    goto :goto_1
.end method

.method public enableCookie(ZI)V
    .locals 2
    .param p1, "b"    # Z
    .param p2, "tag"    # I

    .prologue
    .line 525
    invoke-direct {p0, p2}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v0

    .line 526
    .local v0, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v0, :cond_0

    .line 532
    :goto_0
    return-void

    .line 531
    :cond_0
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    goto :goto_0
.end method

.method public enableWebView(ZI)V
    .locals 2
    .param p1, "b"    # Z
    .param p2, "tag"    # I

    .prologue
    .line 501
    invoke-direct {p0, p2}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v0

    .line 502
    .local v0, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v0, :cond_0

    .line 511
    :goto_0
    return-void

    .line 506
    :cond_0
    if-eqz p1, :cond_1

    .line 507
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/BQWebView;->setVisibility(I)V

    goto :goto_0

    .line 509
    :cond_1
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/BQWebView;->setVisibility(I)V

    goto :goto_0
.end method

.method public execOpenApplicationDetailsSettings()V
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqurlscheme:Ljp/co/drecom/bisque/lib/BQUrlScheme;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQUrlScheme;->execOpenApplicationDetailsSettings()V

    .line 727
    return-void
.end method

.method public execReview(Ljava/lang/String;)V
    .locals 1
    .param p1, "android_review_uri"    # Ljava/lang/String;

    .prologue
    .line 680
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqupdate:Ljp/co/drecom/bisque/lib/BQUpdate;

    invoke-virtual {v0, p1}, Ljp/co/drecom/bisque/lib/BQUpdate;->execReview(Ljava/lang/String;)V

    .line 681
    return-void
.end method

.method public execUpdate(Ljava/lang/String;)V
    .locals 1
    .param p1, "android_update_uri"    # Ljava/lang/String;

    .prologue
    .line 675
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqupdate:Ljp/co/drecom/bisque/lib/BQUpdate;

    invoke-virtual {v0, p1}, Ljp/co/drecom/bisque/lib/BQUpdate;->execUpdate(Ljava/lang/String;)V

    .line 676
    return-void
.end method

.method public execUrlScheme(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 720
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqurlscheme:Ljp/co/drecom/bisque/lib/BQUrlScheme;

    invoke-virtual {v0, p1}, Ljp/co/drecom/bisque/lib/BQUrlScheme;->execUrlScheme(Ljava/lang/String;)V

    .line 721
    return-void
.end method

.method public executeJsInWebView(Ljava/lang/String;I)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "tag"    # I

    .prologue
    .line 659
    invoke-direct {p0, p2}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v0

    .line 660
    .local v0, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v0, :cond_0

    .line 665
    :goto_0
    return-void

    .line 664
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/BQWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public getRealSize()Landroid/graphics/Point;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 907
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 908
    .local v0, "display":Landroid/view/Display;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5, v8, v8}, Landroid/graphics/Point;-><init>(II)V

    .line 910
    .local v5, "point":Landroid/graphics/Point;
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x11

    if-lt v7, v8, :cond_1

    .line 912
    invoke-virtual {v0, v5}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 929
    :cond_0
    :goto_0
    return-object v5

    .line 915
    :cond_1
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xd

    if-lt v7, v8, :cond_0

    .line 918
    :try_start_0
    const-class v7, Landroid/view/Display;

    const-string v8, "getRawWidth"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 919
    .local v3, "getRawWidth":Ljava/lang/reflect/Method;
    const-class v7, Landroid/view/Display;

    const-string v8, "getRawHeight"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 920
    .local v2, "getRawHeight":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 921
    .local v6, "width":I
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 922
    .local v4, "height":I
    invoke-virtual {v5, v6, v4}, Landroid/graphics/Point;->set(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 925
    .end local v2    # "getRawHeight":Ljava/lang/reflect/Method;
    .end local v3    # "getRawWidth":Ljava/lang/reflect/Method;
    .end local v4    # "height":I
    .end local v6    # "width":I
    :catch_0
    move-exception v1

    .line 926
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getStringFromClipboard()Ljava/lang/String;
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->clipboard:Ljp/co/drecom/bisque/lib/BQClipboard;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQClipboard;->getStringFromClipboard()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handlerPost(JJI)V
    .locals 9
    .param p1, "func"    # J
    .param p3, "data"    # J
    .param p5, "type"    # I

    .prologue
    .line 698
    const/4 v7, 0x0

    .line 699
    .local v7, "POST_MAIN":I
    const/4 v6, 0x1

    .line 700
    .local v6, "POST_GL":I
    packed-switch p5, :pswitch_data_0

    .line 715
    :goto_0
    return-void

    .line 702
    :pswitch_0
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqhandler:Ljp/co/drecom/bisque/lib/BQHandler;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljp/co/drecom/bisque/lib/BQHandler;->handlerPost(JJ)V

    goto :goto_0

    .line 706
    :pswitch_1
    move-wide v2, p1

    .local v2, "_func":J
    move-wide v4, p3

    .line 707
    .local v4, "_data":J
    new-instance v0, Lcom/namcobandaigames/spmoja010E/kizakura$4;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/namcobandaigames/spmoja010E/kizakura$4;-><init>(Lcom/namcobandaigames/spmoja010E/kizakura;JJ)V

    invoke-virtual {p0, v0}, Lcom/namcobandaigames/spmoja010E/kizakura;->runOnGLThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 700
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isWideScreen()Z
    .locals 4

    .prologue
    .line 897
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getRealSize()Landroid/graphics/Point;

    move-result-object v2

    .line 898
    .local v2, "size":Landroid/graphics/Point;
    iget v3, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v1, v3, 0x3

    .line 899
    .local v1, "rate_width":I
    iget v3, v2, Landroid/graphics/Point;->y:I

    div-int/lit8 v0, v3, 0x4

    .line 900
    .local v0, "rate_height":I
    if-ne v1, v0, :cond_0

    .line 901
    const/4 v3, 0x1

    .line 903
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    invoke-virtual {v0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQPlayGameService;->onActivityResult(IILandroid/content/Intent;)V

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->paymentBridge:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->paymentBridge:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .line 278
    invoke-virtual {v0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->handleActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 134
    invoke-super {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onCreate(Landroid/os/Bundle;)V

    .line 137
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->isWideScreen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x400

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 140
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 141
    .local v2, "decor":Landroid/view/View;
    const/16 v3, 0x1006

    invoke-virtual {v2, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 145
    .end local v2    # "decor":Landroid/view/View;
    :cond_0
    const-string v3, "enable_hockey_app_on_create"

    invoke-static {v3, p0}, Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;->getState(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "enable_hockey_app"

    invoke-static {v3, p0}, Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;->getState(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 146
    const-string v3, "enable_hockey_app_on_create"

    invoke-static {v3, p0}, Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;->disableState(Ljava/lang/String;Landroid/content/Context;)V

    .line 147
    invoke-static {p0}, Lnet/hockeyapp/android/Constants;->loadFromContext(Landroid/content/Context;)V

    .line 149
    sget-object v3, Lnet/hockeyapp/android/Constants;->FILES_PATH:Ljava/lang/String;

    invoke-static {v3}, Ljp/co/drecom/spice/hockeyapp/HockeyApp;->setUpBreakpad(Ljava/lang/String;)V

    .line 151
    :try_start_0
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 153
    .local v1, "appliInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v3, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "HOCKEYAPP_ID"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "apiKey":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/util/config/AppConfigure;->getUserId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v0, v3, v4}, Lnet/hockeyapp/android/NativeCrashManager;->handleDumpFiles(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    .end local v0    # "apiKey":Ljava/lang/String;
    .end local v1    # "appliInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    :goto_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->getBridge()Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->setContext(Landroid/app/Activity;)V

    .line 160
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->getBridge()Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    move-result-object v3

    invoke-virtual {v3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->setup()V

    .line 161
    new-instance v3, Ljp/co/drecom/bisque/lib/BQUpdate;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/BQUpdate;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqupdate:Ljp/co/drecom/bisque/lib/BQUpdate;

    .line 162
    new-instance v3, Ljp/co/drecom/bisque/lib/BQUrlScheme;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/BQUrlScheme;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->bqurlscheme:Ljp/co/drecom/bisque/lib/BQUrlScheme;

    .line 163
    new-instance v3, Ljp/co/drecom/bisque/lib/BQSleepSetting;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/BQSleepSetting;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->deviceSetting:Ljp/co/drecom/bisque/lib/BQSleepSetting;

    .line 164
    invoke-direct {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->canUseAchievement()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 165
    new-instance v3, Ljp/co/drecom/bisque/lib/BQPlayGameService;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/BQPlayGameService;-><init>(Landroid/app/Activity;)V

    iput-object v3, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    .line 168
    :cond_2
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3, v6}, Lcom/namcobandaigames/spmoja010E/kizakura;->twitterCallbackInternal(Landroid/content/Intent;Z)V

    .line 169
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3, v6}, Lcom/namcobandaigames/spmoja010E/kizakura;->snsAuthnicationCallbackInternal(Landroid/content/Intent;Z)V

    .line 170
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->initialize(Landroid/app/Activity;)V

    .line 171
    invoke-direct {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->initializeTypefaces()V

    .line 172
    return-void

    .line 155
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public onCreateView()Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;
    .locals 10

    .prologue
    const/4 v9, -0x1

    .line 225
    new-instance v6, Landroid/widget/RelativeLayout;

    invoke-direct {v6, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    .line 229
    :try_start_0
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x80

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 230
    .local v0, "appliInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v6, :cond_0

    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v7, "jp.co.drecom.bisque.enable.glsurfaceview.transparent"

    .line 231
    invoke-virtual {v6, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 232
    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v7, "jp.co.drecom.bisque.enable.glsurfaceview.transparent"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->enableGlSurfaceviewTransparent:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    .end local v0    # "appliInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    new-instance v3, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    iget-boolean v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->enableGlSurfaceviewTransparent:Z

    invoke-direct {v3, p0, v6}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;-><init>(Landroid/content/Context;Z)V

    .line 239
    .local v3, "glSurfaceView":Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v9, v9}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 240
    .local v4, "gl_params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v7, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 242
    new-instance v6, Lorg/cocos2dx/lib/Cocos2dxRenderer;

    invoke-direct {v6}, Lorg/cocos2dx/lib/Cocos2dxRenderer;-><init>()V

    invoke-virtual {v3, v6}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->setCocos2dxRenderer(Lorg/cocos2dx/lib/Cocos2dxRenderer;)V

    .line 243
    invoke-virtual {v3, p0}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 245
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v6}, Lcom/namcobandaigames/spmoja010E/kizakura;->setContentView(Landroid/view/View;)V

    .line 247
    invoke-static {p0}, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->setContext(Landroid/app/Activity;)V

    .line 248
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6}, Ljp/co/drecom/bisque/lib/BQTouchDispatcherNotify;->setDispatcher(Ljp/co/drecom/bisque/lib/BQTouchDispatcher;Landroid/os/Handler;)V

    .line 249
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->setDispatcher(Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;Landroid/os/Handler;)V

    .line 250
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6}, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify;->setDispatcher(Ljp/co/drecom/bisque/lib/BQUpdateDispatcher;Landroid/os/Handler;)V

    .line 251
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6}, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->setDispatcher(Ljp/co/drecom/bisque/lib/BQClipboardDispatcher;Landroid/os/Handler;)V

    .line 252
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6}, Ljp/co/drecom/bisque/lib/BQHandlerDispatcherNotify;->setDispatcher(Ljp/co/drecom/bisque/lib/BQHandlerDispatcher;Landroid/os/Handler;)V

    .line 253
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6}, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify;->setDispatcher(Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcher;Landroid/os/Handler;)V

    .line 254
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6}, Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcherNotify;->setDispatcher(Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcher;Landroid/os/Handler;)V

    .line 255
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6}, Ljp/co/drecom/bisque/lib/DRToastManager;->initialize(Landroid/app/Activity;Landroid/os/Handler;)V

    .line 257
    invoke-direct {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->isEnableMoviePlayer()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 258
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v6, v7}, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->initialize(Landroid/app/Activity;Landroid/os/Handler;Landroid/widget/RelativeLayout;)V

    .line 261
    :cond_1
    iget-object v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->handler:Landroid/os/Handler;

    invoke-static {p0, v6, v3}, Ljp/co/drecom/bisque/lib/HandlerJava;->initialize(Landroid/app/Activity;Landroid/os/Handler;Landroid/opengl/GLSurfaceView;)V

    .line 263
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080007

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 264
    .local v1, "connection_timeout":I
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080008

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 265
    .local v5, "read_timeout":I
    invoke-static {v1, v5}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->setHttpTimeout(II)V

    .line 267
    invoke-static {p0}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->initNativeContext(Ljava/lang/Object;)V

    .line 269
    return-object v3

    .line 234
    .end local v1    # "connection_timeout":I
    .end local v3    # "glSurfaceView":Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;
    .end local v4    # "gl_params":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "read_timeout":I
    :catch_0
    move-exception v2

    .line 235
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->enableGlSurfaceviewTransparent:Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 793
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onDestroy()V

    .line 794
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->paymentBridge:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    if-eqz v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->paymentBridge:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->dispose()V

    .line 796
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->paymentBridge:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .line 798
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 747
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 750
    :cond_0
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->mGLSurfaceView:Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    invoke-virtual {v0, p1, p2}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 752
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 758
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 761
    :cond_0
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->mGLSurfaceView:Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    invoke-virtual {v0, p1, p2}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 763
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x1

    .line 803
    invoke-super {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 804
    invoke-direct {p0, p1, v0}, Lcom/namcobandaigames/spmoja010E/kizakura;->twitterCallbackInternal(Landroid/content/Intent;Z)V

    .line 805
    invoke-direct {p0, p1, v0}, Lcom/namcobandaigames/spmoja010E/kizakura;->snsAuthnicationCallbackInternal(Landroid/content/Intent;Z)V

    .line 806
    invoke-virtual {p0, p1}, Lcom/namcobandaigames/spmoja010E/kizakura;->setIntent(Landroid/content/Intent;)V

    .line 807
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .prologue
    .line 768
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 769
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 189
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onResume()V

    .line 191
    const-string v2, "enable_hockey_app_on_resume"

    invoke-static {v2, p0}, Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;->getState(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "enable_hockey_app"

    invoke-static {v2, p0}, Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;->getState(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 192
    const-string v2, "enable_hockey_app_on_resume"

    invoke-static {v2, p0}, Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;->disableState(Ljava/lang/String;Landroid/content/Context;)V

    .line 193
    const/4 v1, 0x0

    .line 195
    .local v1, "appliInfo":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 197
    iget-object v2, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v3, "HOCKEYAPP_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "apiKey":Ljava/lang/String;
    new-instance v2, Lcom/namcobandaigames/spmoja010E/kizakura$1;

    invoke-direct {v2, p0}, Lcom/namcobandaigames/spmoja010E/kizakura$1;-><init>(Lcom/namcobandaigames/spmoja010E/kizakura;)V

    invoke-static {p0, v0, v2}, Lnet/hockeyapp/android/CrashManager;->register(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/CrashManagerListener;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    .end local v0    # "apiKey":Ljava/lang/String;
    .end local v1    # "appliInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v2

    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->handleNotification(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    :cond_1
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->isExecutingBrowser()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 214
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->closeAuthorizeURL()V

    .line 215
    const/4 v2, 0x0

    invoke-static {v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->setExecutingBrowser(Z)V

    .line 218
    :cond_2
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    if-eqz v2, :cond_3

    .line 219
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    invoke-virtual {v2}, Ljp/co/drecom/bisque/lib/BQPlayGameService;->onResume()V

    .line 221
    :cond_3
    return-void

    .line 207
    .restart local v1    # "appliInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onStart()V

    .line 176
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQPlayGameService;->onStart()V

    .line 179
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->playGameService:Ljp/co/drecom/bisque/lib/BQPlayGameService;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQPlayGameService;->onStop()V

    .line 185
    :cond_0
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onStop()V

    .line 186
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 290
    iget-boolean v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->enableGlSurfaceviewTransparent:Z

    if-eqz v2, :cond_3

    .line 291
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->mGLSurfaceView:Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    invoke-virtual {v2, p2}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 292
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 293
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Ljp/co/drecom/bisque/lib/BQWebView;

    if-ne v2, v3, :cond_0

    .line 294
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ljp/co/drecom/bisque/lib/BQWebView;

    .line 296
    .local v1, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    invoke-virtual {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->getVisibility()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 292
    .end local v1    # "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 299
    .restart local v1    # "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    :cond_1
    invoke-virtual {v1, p2}, Ljp/co/drecom/bisque/lib/BQWebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 326
    .end local v0    # "i":I
    .end local v1    # "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    :cond_2
    :goto_1
    return v4

    .line 307
    :cond_3
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->mGLSurfaceView:Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    if-ne p1, v2, :cond_4

    .line 308
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->mGLSurfaceView:Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    invoke-virtual {v2, p2}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 310
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 311
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Ljp/co/drecom/bisque/lib/BQWebView;

    if-ne v2, v3, :cond_5

    .line 312
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ljp/co/drecom/bisque/lib/BQWebView;

    .line 313
    .restart local v1    # "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-ne p1, v1, :cond_5

    .line 315
    invoke-virtual {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->getVisibility()I

    move-result v2

    if-ne v2, v5, :cond_6

    .line 310
    .end local v1    # "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 318
    .restart local v1    # "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    :cond_6
    invoke-virtual {v1, p2}, Ljp/co/drecom/bisque/lib/BQWebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 877
    invoke-super {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onWindowFocusChanged(Z)V

    .line 878
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->isWideScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 879
    invoke-direct {p0}, Lcom/namcobandaigames/spmoja010E/kizakura;->hideSystemUI()V

    .line 881
    :cond_0
    return-void
.end method

.method public reloadWebView(I)V
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 597
    invoke-direct {p0, p1}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v0

    .line 598
    .local v0, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v0, :cond_0

    .line 608
    :goto_0
    return-void

    .line 603
    :cond_0
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 604
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->goBack()V

    goto :goto_0

    .line 606
    :cond_1
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->reload()V

    goto :goto_0
.end method

.method public removeWebView(I)V
    .locals 5
    .param p1, "tag"    # I

    .prologue
    const/4 v4, 0x0

    .line 482
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 483
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Ljp/co/drecom/bisque/lib/BQWebView;

    if-ne v2, v3, :cond_0

    .line 484
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Ljp/co/drecom/bisque/lib/BQWebView;

    .line 485
    .local v1, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    iget v2, v1, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    if-ne p1, v2, :cond_0

    .line 486
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->removeViewAt(I)V

    .line 487
    invoke-virtual {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->stopLoading()V

    .line 488
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljp/co/drecom/bisque/lib/BQWebView;->clearCache(Z)V

    .line 489
    invoke-virtual {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->clearHistory()V

    .line 490
    invoke-virtual {v1, v4}, Ljp/co/drecom/bisque/lib/BQWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 491
    invoke-virtual {v1, v4}, Ljp/co/drecom/bisque/lib/BQWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 492
    invoke-virtual {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->destroy()V

    .line 482
    .end local v1    # "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 497
    :cond_1
    return-void
.end method

.method public requestWebView(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "tag"    # I
    .param p3, "auth"    # Z
    .param p4, "username"    # Ljava/lang/String;
    .param p5, "password"    # Ljava/lang/String;

    .prologue
    .line 612
    invoke-direct {p0, p2}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v0

    .line 613
    .local v0, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v0, :cond_0

    .line 620
    :goto_0
    return-void

    .line 617
    :cond_0
    invoke-virtual {v0, p3, p4, p5}, Ljp/co/drecom/bisque/lib/BQWebView;->setAuthInfo(ZLjava/lang/String;Ljava/lang/String;)V

    .line 619
    invoke-virtual {v0, p1}, Ljp/co/drecom/bisque/lib/BQWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestWebView(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "post"    # Ljava/lang/String;
    .param p3, "tag"    # I
    .param p4, "auth"    # Z
    .param p5, "username"    # Ljava/lang/String;
    .param p6, "password"    # Ljava/lang/String;

    .prologue
    .line 641
    invoke-direct {p0, p3}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v2

    .line 642
    .local v2, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v2, :cond_0

    .line 655
    :goto_0
    return-void

    .line 646
    :cond_0
    invoke-virtual {v2, p4, p5, p6}, Ljp/co/drecom/bisque/lib/BQWebView;->setAuthInfo(ZLjava/lang/String;Ljava/lang/String;)V

    .line 649
    :try_start_0
    const-string v3, "UTF-8"

    invoke-virtual {p2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 654
    .local v1, "postData":[B
    invoke-virtual {v2, p1, v1}, Ljp/co/drecom/bisque/lib/BQWebView;->postUrl(Ljava/lang/String;[B)V

    goto :goto_0

    .line 650
    .end local v1    # "postData":[B
    :catch_0
    move-exception v0

    .line 651
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    goto :goto_0
.end method

.method public requestWebView(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "headers"    # [Ljava/lang/String;
    .param p3, "values"    # [Ljava/lang/String;
    .param p4, "tag"    # I
    .param p5, "auth"    # Z
    .param p6, "username"    # Ljava/lang/String;
    .param p7, "password"    # Ljava/lang/String;

    .prologue
    .line 624
    invoke-direct {p0, p4}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v2

    .line 625
    .local v2, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v2, :cond_0

    .line 637
    :goto_0
    return-void

    .line 629
    :cond_0
    invoke-virtual {v2, p5, p6, p7}, Ljp/co/drecom/bisque/lib/BQWebView;->setAuthInfo(ZLjava/lang/String;Ljava/lang/String;)V

    .line 630
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 631
    .local v0, "HTTPHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p2

    if-ge v1, v3, :cond_1

    .line 633
    aget-object v3, p2, v1

    aget-object v4, p3, v1

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 631
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 636
    :cond_1
    invoke-virtual {v2, p1, v0}, Ljp/co/drecom/bisque/lib/BQWebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public setBQPaymentBridge(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V
    .locals 0
    .param p1, "bridge"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 737
    iput-object p1, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->paymentBridge:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .line 738
    return-void
.end method

.method public setDeviceSleep(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 732
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->deviceSetting:Ljp/co/drecom/bisque/lib/BQSleepSetting;

    invoke-virtual {v0, p1}, Ljp/co/drecom/bisque/lib/BQSleepSetting;->setDeviceSleep(Z)V

    .line 733
    return-void
.end method

.method public setOrderWebView(II)V
    .locals 0
    .param p1, "order"    # I
    .param p2, "tag"    # I

    .prologue
    .line 593
    return-void
.end method

.method public setRectWebView(IIIII)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "tag"    # I

    .prologue
    const/4 v2, 0x0

    .line 579
    invoke-direct {p0, p5}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v1

    .line 580
    .local v1, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-nez v1, :cond_0

    .line 588
    :goto_0
    return-void

    .line 584
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p3, p4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 585
    .local v0, "rlp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, p2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 586
    invoke-virtual {v1, v0}, Ljp/co/drecom/bisque/lib/BQWebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 587
    invoke-virtual {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->requestLayout()V

    goto :goto_0
.end method

.method public setStringToClipboard(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 686
    iget-object v0, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->clipboard:Ljp/co/drecom/bisque/lib/BQClipboard;

    invoke-virtual {v0, p1}, Ljp/co/drecom/bisque/lib/BQClipboard;->setStringToClipboard(Ljava/lang/String;)V

    .line 687
    return-void
.end method

.method public setUserInteractionEnabled(ZI)V
    .locals 3
    .param p1, "b"    # Z
    .param p2, "tag"    # I

    .prologue
    .line 333
    const/16 v0, 0x3e7

    .line 334
    .local v0, "GL_VIEW_TAG":I
    const/16 v2, 0x3e7

    if-ne p2, v2, :cond_1

    .line 335
    iget-object v2, p0, Lcom/namcobandaigames/spmoja010E/kizakura;->mGLSurfaceView:Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    invoke-virtual {v2, p1}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->setInteraction(Z)V

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    invoke-direct {p0, p2}, Lcom/namcobandaigames/spmoja010E/kizakura;->getWebView(I)Ljp/co/drecom/bisque/lib/BQWebView;

    move-result-object v1

    .line 338
    .local v1, "wv":Ljp/co/drecom/bisque/lib/BQWebView;
    if-eqz v1, :cond_0

    .line 342
    invoke-virtual {v1, p1}, Ljp/co/drecom/bisque/lib/BQWebView;->setInteraction(Z)V

    goto :goto_0
.end method
