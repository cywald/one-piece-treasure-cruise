.class public Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;
.super Ljava/lang/Object;
.source "ExternalServiceStateModel.java"


# static fields
.field static final PREFERENCE_KEY:Ljava/lang/String; = "external_state"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static disableState(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "con"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;->setState(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 29
    return-void
.end method

.method public static enableState(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "con"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/namcobandaigames/spmoja010E/ExternalServiceStateModel;->setState(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 24
    return-void
.end method

.method public static getState(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "con"    # Landroid/content/Context;

    .prologue
    .line 33
    const-string v1, "external_state"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 34
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static setState(Ljava/lang/String;ZLandroid/content/Context;)V
    .locals 4
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "state"    # Z
    .param p2, "con"    # Landroid/content/Context;

    .prologue
    .line 15
    const-string v2, "external_state"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 16
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 17
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 18
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 19
    return-void
.end method
