.class abstract Ltwitter4j/AsyncTwitterImpl$AsyncTask;
.super Ljava/lang/Object;
.source "AsyncTwitterImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltwitter4j/AsyncTwitterImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "AsyncTask"
.end annotation


# instance fields
.field final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;"
        }
    .end annotation
.end field

.field final method:Ltwitter4j/TwitterMethod;

.field final synthetic this$0:Ltwitter4j/AsyncTwitterImpl;


# direct methods
.method constructor <init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Ltwitter4j/AsyncTwitterImpl;
    .param p2, "method"    # Ltwitter4j/TwitterMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/TwitterMethod;",
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2799
    .local p3, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iput-object p1, p0, Ltwitter4j/AsyncTwitterImpl$AsyncTask;->this$0:Ltwitter4j/AsyncTwitterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2800
    iput-object p2, p0, Ltwitter4j/AsyncTwitterImpl$AsyncTask;->method:Ltwitter4j/TwitterMethod;

    .line 2801
    iput-object p3, p0, Ltwitter4j/AsyncTwitterImpl$AsyncTask;->listeners:Ljava/util/List;

    .line 2802
    return-void
.end method


# virtual methods
.method abstract invoke(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation
.end method

.method public run()V
    .locals 4

    .prologue
    .line 2809
    :try_start_0
    iget-object v2, p0, Ltwitter4j/AsyncTwitterImpl$AsyncTask;->listeners:Ljava/util/List;

    invoke-virtual {p0, v2}, Ltwitter4j/AsyncTwitterImpl$AsyncTask;->invoke(Ljava/util/List;)V
    :try_end_0
    .catch Ltwitter4j/TwitterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2820
    :cond_0
    return-void

    .line 2810
    :catch_0
    move-exception v1

    .line 2811
    .local v1, "te":Ltwitter4j/TwitterException;
    iget-object v2, p0, Ltwitter4j/AsyncTwitterImpl$AsyncTask;->listeners:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 2812
    iget-object v2, p0, Ltwitter4j/AsyncTwitterImpl$AsyncTask;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltwitter4j/TwitterListener;

    .line 2814
    .local v0, "listener":Ltwitter4j/TwitterListener;
    :try_start_1
    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl$AsyncTask;->method:Ltwitter4j/TwitterMethod;

    invoke-interface {v0, v1, v3}, Ltwitter4j/TwitterListener;->onException(Ltwitter4j/TwitterException;Ltwitter4j/TwitterMethod;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2815
    :catch_1
    move-exception v3

    goto :goto_0
.end method
