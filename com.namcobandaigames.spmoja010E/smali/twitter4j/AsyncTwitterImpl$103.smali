.class Ltwitter4j/AsyncTwitterImpl$103;
.super Ltwitter4j/AsyncTwitterImpl$AsyncTask;
.source "AsyncTwitterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltwitter4j/AsyncTwitterImpl;->destroyUserListMember(JLjava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ltwitter4j/AsyncTwitterImpl;

.field final synthetic val$ownerId:J

.field final synthetic val$slug:Ljava/lang/String;

.field final synthetic val$userId:J


# direct methods
.method constructor <init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;J)V
    .locals 0
    .param p1, "this$0"    # Ltwitter4j/AsyncTwitterImpl;
    .param p2, "method"    # Ltwitter4j/TwitterMethod;

    .prologue
    .line 1748
    .local p3, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iput-object p1, p0, Ltwitter4j/AsyncTwitterImpl$103;->this$0:Ltwitter4j/AsyncTwitterImpl;

    iput-wide p4, p0, Ltwitter4j/AsyncTwitterImpl$103;->val$ownerId:J

    iput-object p6, p0, Ltwitter4j/AsyncTwitterImpl$103;->val$slug:Ljava/lang/String;

    iput-wide p7, p0, Ltwitter4j/AsyncTwitterImpl$103;->val$userId:J

    invoke-direct {p0, p1, p2, p3}, Ltwitter4j/AsyncTwitterImpl$AsyncTask;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 1751
    .local p1, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl$103;->this$0:Ltwitter4j/AsyncTwitterImpl;

    invoke-static {v0}, Ltwitter4j/AsyncTwitterImpl;->access$000(Ltwitter4j/AsyncTwitterImpl;)Ltwitter4j/Twitter;

    move-result-object v0

    iget-wide v1, p0, Ltwitter4j/AsyncTwitterImpl$103;->val$ownerId:J

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl$103;->val$slug:Ljava/lang/String;

    iget-wide v4, p0, Ltwitter4j/AsyncTwitterImpl$103;->val$userId:J

    invoke-interface/range {v0 .. v5}, Ltwitter4j/Twitter;->destroyUserListMember(JLjava/lang/String;J)Ltwitter4j/UserList;

    move-result-object v6

    .line 1752
    .local v6, "list":Ltwitter4j/UserList;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ltwitter4j/TwitterListener;

    .line 1754
    .local v7, "listener":Ltwitter4j/TwitterListener;
    :try_start_0
    invoke-interface {v7, v6}, Ltwitter4j/TwitterListener;->destroyedUserListMember(Ltwitter4j/UserList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1755
    :catch_0
    move-exception v1

    goto :goto_0

    .line 1758
    .end local v7    # "listener":Ltwitter4j/TwitterListener;
    :cond_0
    return-void
.end method
