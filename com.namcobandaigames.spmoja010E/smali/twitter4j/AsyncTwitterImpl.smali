.class Ltwitter4j/AsyncTwitterImpl;
.super Ltwitter4j/TwitterBaseImpl;
.source "AsyncTwitterImpl.java"

# interfaces
.implements Ltwitter4j/AsyncTwitter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltwitter4j/AsyncTwitterImpl$AsyncTask;
    }
.end annotation


# static fields
.field private static transient dispatcher:Ltwitter4j/Dispatcher; = null

.field private static final serialVersionUID:J = 0x5a1823fd96bbb6bbL


# instance fields
.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;"
        }
    .end annotation
.end field

.field private final twitter:Ltwitter4j/Twitter;


# direct methods
.method constructor <init>(Ltwitter4j/conf/Configuration;Ltwitter4j/auth/Authorization;)V
    .locals 1
    .param p1, "conf"    # Ltwitter4j/conf/Configuration;
    .param p2, "auth"    # Ltwitter4j/auth/Authorization;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ltwitter4j/TwitterBaseImpl;-><init>(Ltwitter4j/conf/Configuration;Ltwitter4j/auth/Authorization;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    .line 53
    new-instance v0, Ltwitter4j/TwitterFactory;

    invoke-direct {v0, p1}, Ltwitter4j/TwitterFactory;-><init>(Ltwitter4j/conf/Configuration;)V

    invoke-virtual {v0, p2}, Ltwitter4j/TwitterFactory;->getInstance(Ltwitter4j/auth/Authorization;)Ltwitter4j/Twitter;

    move-result-object v0

    iput-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    .line 54
    return-void
.end method

.method static synthetic access$000(Ltwitter4j/AsyncTwitterImpl;)Ltwitter4j/Twitter;
    .locals 1
    .param p0, "x0"    # Ltwitter4j/AsyncTwitterImpl;

    .prologue
    .line 45
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    return-object v0
.end method

.method private getDispatcher()Ltwitter4j/Dispatcher;
    .locals 3

    .prologue
    .line 2697
    sget-object v0, Ltwitter4j/AsyncTwitterImpl;->dispatcher:Ltwitter4j/Dispatcher;

    if-nez v0, :cond_1

    .line 2698
    const-class v1, Ltwitter4j/AsyncTwitterImpl;

    monitor-enter v1

    .line 2699
    :try_start_0
    sget-object v0, Ltwitter4j/AsyncTwitterImpl;->dispatcher:Ltwitter4j/Dispatcher;

    if-nez v0, :cond_0

    .line 2703
    new-instance v0, Ltwitter4j/DispatcherFactory;

    iget-object v2, p0, Ltwitter4j/AsyncTwitterImpl;->conf:Ltwitter4j/conf/Configuration;

    invoke-direct {v0, v2}, Ltwitter4j/DispatcherFactory;-><init>(Ltwitter4j/conf/Configuration;)V

    invoke-virtual {v0}, Ltwitter4j/DispatcherFactory;->getInstance()Ltwitter4j/Dispatcher;

    move-result-object v0

    sput-object v0, Ltwitter4j/AsyncTwitterImpl;->dispatcher:Ltwitter4j/Dispatcher;

    .line 2705
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2707
    :cond_1
    sget-object v0, Ltwitter4j/AsyncTwitterImpl;->dispatcher:Ltwitter4j/Dispatcher;

    return-object v0

    .line 2705
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public addListener(Ltwitter4j/TwitterListener;)V
    .locals 1
    .param p1, "listener"    # Ltwitter4j/TwitterListener;

    .prologue
    .line 58
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    return-void
.end method

.method public createBlock(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 1134
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$65;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_BLOCK:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$65;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1146
    return-void
.end method

.method public createBlock(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 1150
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$66;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_BLOCK:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$66;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1162
    return-void
.end method

.method public createFavorite(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 1650
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$97;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_FAVORITE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$97;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1662
    return-void
.end method

.method public createFriendship(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 677
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$38;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$38;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 689
    return-void
.end method

.method public createFriendship(JZ)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "follow"    # Z

    .prologue
    .line 709
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$40;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$40;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JZ)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 721
    return-void
.end method

.method public createFriendship(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 693
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$39;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$39;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 705
    return-void
.end method

.method public createFriendship(Ljava/lang/String;Z)V
    .locals 7
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "follow"    # Z

    .prologue
    .line 725
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$41;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$41;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Z)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 737
    return-void
.end method

.method public createMute(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 1230
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$71;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_MUTE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$71;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1242
    return-void
.end method

.method public createMute(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 1246
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$72;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_MUTE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$72;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1258
    return-void
.end method

.method public createSavedSearch(Ljava/lang/String;)V
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 2276
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$135;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$135;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2288
    return-void
.end method

.method public createUserList(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8
    .param p1, "listName"    # Ljava/lang/String;
    .param p2, "isPublicList"    # Z
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 2175
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$129;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_USER_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$129;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2187
    return-void
.end method

.method public createUserListMember(JJ)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "userId"    # J

    .prologue
    .line 2079
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$123;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$123;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2091
    return-void
.end method

.method public createUserListMember(JLjava/lang/String;J)V
    .locals 10
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "userId"    # J

    .prologue
    .line 2095
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$124;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-wide v7, p4

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$124;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;J)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2107
    return-void
.end method

.method public createUserListMembers(JLjava/lang/String;[J)V
    .locals 9
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "userIds"    # [J

    .prologue
    .line 1967
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$116;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$116;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;[J)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1979
    return-void
.end method

.method public createUserListMembers(JLjava/lang/String;[Ljava/lang/String;)V
    .locals 9
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "screenNames"    # [Ljava/lang/String;

    .prologue
    .line 1999
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$118;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$118;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;[Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2011
    return-void
.end method

.method public createUserListMembers(J[J)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "userIds"    # [J

    .prologue
    .line 1951
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$115;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$115;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J[J)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1963
    return-void
.end method

.method public createUserListMembers(J[Ljava/lang/String;)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "screenNames"    # [Ljava/lang/String;

    .prologue
    .line 1983
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$117;

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$117;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J[Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1995
    return-void
.end method

.method public createUserListSubscription(J)V
    .locals 7
    .param p1, "listId"    # J

    .prologue
    .line 1854
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$109;

    sget-object v2, Ltwitter4j/TwitterMethod;->SUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$109;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1866
    return-void
.end method

.method public createUserListSubscription(JLjava/lang/String;)V
    .locals 9
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;

    .prologue
    .line 1870
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$110;

    sget-object v2, Ltwitter4j/TwitterMethod;->SUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$110;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1882
    return-void
.end method

.method public destroyBlock(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 1166
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$67;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_BLOCK:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$67;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1178
    return-void
.end method

.method public destroyBlock(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 1182
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$68;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_BLOCK:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$68;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1194
    return-void
.end method

.method public destroyDirectMessage(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 464
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$25;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$25;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 476
    return-void
.end method

.method public destroyFavorite(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 1634
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$96;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_FAVORITE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$96;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1646
    return-void
.end method

.method public destroyFriendship(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 741
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$42;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$42;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 753
    return-void
.end method

.method public destroyFriendship(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 757
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$43;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$43;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 769
    return-void
.end method

.method public destroyMute(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 1262
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$73;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_MUTE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$73;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1274
    return-void
.end method

.method public destroyMute(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 1278
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$74;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_MUTE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$74;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1290
    return-void
.end method

.method public destroySavedSearch(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 2292
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$136;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$136;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;I)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2304
    return-void
.end method

.method public destroyStatus(J)V
    .locals 7
    .param p1, "statusId"    # J

    .prologue
    .line 268
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$13;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_STATUS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$13;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 280
    return-void
.end method

.method public destroyUserList(J)V
    .locals 7
    .param p1, "listId"    # J

    .prologue
    .line 2111
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$125;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_USER_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$125;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2123
    return-void
.end method

.method public destroyUserList(JLjava/lang/String;)V
    .locals 9
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;

    .prologue
    .line 2127
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$126;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_USER_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$126;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2139
    return-void
.end method

.method public destroyUserListMember(JJ)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "userId"    # J

    .prologue
    .line 1732
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$102;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_LIST_MEMBER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$102;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1744
    return-void
.end method

.method public destroyUserListMember(JLjava/lang/String;J)V
    .locals 10
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "userId"    # J

    .prologue
    .line 1748
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$103;

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_LIST_MEMBER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-wide v7, p4

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$103;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;J)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1760
    return-void
.end method

.method public destroyUserListSubscription(J)V
    .locals 7
    .param p1, "listId"    # J

    .prologue
    .line 1918
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$113;

    sget-object v2, Ltwitter4j/TwitterMethod;->UNSUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$113;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1930
    return-void
.end method

.method public destroyUserListSubscription(JLjava/lang/String;)V
    .locals 9
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;

    .prologue
    .line 1934
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$114;

    sget-object v2, Ltwitter4j/TwitterMethod;->UNSUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$114;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1946
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2825
    if-ne p0, p1, :cond_1

    .line 2836
    :cond_0
    :goto_0
    return v1

    .line 2826
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    .line 2827
    :cond_3
    invoke-super {p0, p1}, Ltwitter4j/TwitterBaseImpl;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    move-object v0, p1

    .line 2829
    check-cast v0, Ltwitter4j/AsyncTwitterImpl;

    .line 2831
    .local v0, "that":Ltwitter4j/AsyncTwitterImpl;
    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    if-eqz v3, :cond_6

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    iget-object v4, v0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    :cond_5
    move v1, v2

    .line 2832
    goto :goto_0

    .line 2831
    :cond_6
    iget-object v3, v0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    if-nez v3, :cond_5

    .line 2833
    :cond_7
    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    if-eqz v3, :cond_8

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    iget-object v4, v0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 2834
    goto :goto_0

    .line 2833
    :cond_8
    iget-object v3, v0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public getAPIConfiguration()V
    .locals 4

    .prologue
    .line 2460
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$146;

    sget-object v2, Ltwitter4j/TwitterMethod;->CONFIGURATION:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$146;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2472
    return-void
.end method

.method public getAccountSettings()V
    .locals 4

    .prologue
    .line 907
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$52;

    sget-object v2, Ltwitter4j/TwitterMethod;->ACCOUNT_SETTINGS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$52;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 919
    return-void
.end method

.method public getAvailableTrends()V
    .locals 4

    .prologue
    .line 2392
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$142;

    sget-object v2, Ltwitter4j/TwitterMethod;->AVAILABLE_TRENDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$142;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2404
    return-void
.end method

.method public getBlocksIDs()V
    .locals 4

    .prologue
    .line 1102
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$63;

    sget-object v2, Ltwitter4j/TwitterMethod;->BLOCK_LIST_IDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$63;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1114
    return-void
.end method

.method public getBlocksIDs(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 1118
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$64;

    sget-object v2, Ltwitter4j/TwitterMethod;->BLOCK_LIST_IDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$64;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1130
    return-void
.end method

.method public getBlocksList()V
    .locals 4

    .prologue
    .line 1070
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$61;

    sget-object v2, Ltwitter4j/TwitterMethod;->BLOCK_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$61;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1082
    return-void
.end method

.method public getBlocksList(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 1086
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$62;

    sget-object v2, Ltwitter4j/TwitterMethod;->BLOCK_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$62;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1098
    return-void
.end method

.method public getClosestTrends(Ltwitter4j/GeoLocation;)V
    .locals 4
    .param p1, "location"    # Ltwitter4j/GeoLocation;

    .prologue
    .line 2408
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$143;

    sget-object v2, Ltwitter4j/TwitterMethod;->CLOSEST_TRENDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$143;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/GeoLocation;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2420
    return-void
.end method

.method public getContributees(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 1374
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$80;

    sget-object v2, Ltwitter4j/TwitterMethod;->CONTRIBUTEEES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$80;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1386
    return-void
.end method

.method public getContributees(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 1390
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$81;

    sget-object v2, Ltwitter4j/TwitterMethod;->CONTRIBUTEEES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$81;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1402
    return-void
.end method

.method public getContributors(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 1406
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$82;

    sget-object v2, Ltwitter4j/TwitterMethod;->CONTRIBUTORS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$82;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1418
    return-void
.end method

.method public getContributors(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 1422
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$83;

    sget-object v2, Ltwitter4j/TwitterMethod;->CONTRIBUTORS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$83;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1434
    return-void
.end method

.method public getDirectMessages()V
    .locals 4

    .prologue
    .line 384
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$20;

    sget-object v2, Ltwitter4j/TwitterMethod;->DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$20;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 396
    return-void
.end method

.method public getDirectMessages(Ltwitter4j/Paging;)V
    .locals 4
    .param p1, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 400
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$21;

    sget-object v2, Ltwitter4j/TwitterMethod;->DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$21;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/Paging;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 412
    return-void
.end method

.method public getFavorites()V
    .locals 4

    .prologue
    .line 1538
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$90;

    sget-object v2, Ltwitter4j/TwitterMethod;->FAVORITES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$90;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1550
    return-void
.end method

.method public getFavorites(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 1554
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$91;

    sget-object v2, Ltwitter4j/TwitterMethod;->FAVORITES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$91;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1566
    return-void
.end method

.method public getFavorites(JLtwitter4j/Paging;)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 1602
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$94;

    sget-object v2, Ltwitter4j/TwitterMethod;->FAVORITES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$94;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLtwitter4j/Paging;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1614
    return-void
.end method

.method public getFavorites(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 1570
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$92;

    sget-object v2, Ltwitter4j/TwitterMethod;->FAVORITES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$92;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1582
    return-void
.end method

.method public getFavorites(Ljava/lang/String;Ltwitter4j/Paging;)V
    .locals 7
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 1618
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$95;

    sget-object v2, Ltwitter4j/TwitterMethod;->FAVORITES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$95;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Ltwitter4j/Paging;)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1630
    return-void
.end method

.method public getFavorites(Ltwitter4j/Paging;)V
    .locals 4
    .param p1, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 1586
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$93;

    sget-object v2, Ltwitter4j/TwitterMethod;->FAVORITES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$93;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/Paging;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1598
    return-void
.end method

.method public getFollowersIDs(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 564
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$31;

    sget-object v2, Ltwitter4j/TwitterMethod;->FOLLOWERS_IDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$31;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 576
    return-void
.end method

.method public getFollowersIDs(JJ)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "cursor"    # J

    .prologue
    .line 580
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$32;

    sget-object v2, Ltwitter4j/TwitterMethod;->FOLLOWERS_IDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$32;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 592
    return-void
.end method

.method public getFollowersIDs(Ljava/lang/String;J)V
    .locals 8
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "cursor"    # J

    .prologue
    .line 596
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$33;

    sget-object v3, Ltwitter4j/TwitterMethod;->FOLLOWERS_IDS:Ltwitter4j/TwitterMethod;

    iget-object v4, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Ltwitter4j/AsyncTwitterImpl$33;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 608
    return-void
.end method

.method public getFollowersList(JJ)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "cursor"    # J

    .prologue
    .line 873
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$50;

    sget-object v2, Ltwitter4j/TwitterMethod;->FOLLOWERS_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$50;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 885
    return-void
.end method

.method public getFollowersList(Ljava/lang/String;J)V
    .locals 8
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "cursor"    # J

    .prologue
    .line 889
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$51;

    sget-object v3, Ltwitter4j/TwitterMethod;->FOLLOWERS_LIST:Ltwitter4j/TwitterMethod;

    iget-object v4, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Ltwitter4j/AsyncTwitterImpl$51;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 901
    return-void
.end method

.method public getFriendsIDs(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 514
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$28;

    sget-object v2, Ltwitter4j/TwitterMethod;->FRIENDS_IDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$28;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 527
    return-void
.end method

.method public getFriendsIDs(JJ)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "cursor"    # J

    .prologue
    .line 531
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$29;

    sget-object v2, Ltwitter4j/TwitterMethod;->FRIENDS_IDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$29;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 543
    return-void
.end method

.method public getFriendsIDs(Ljava/lang/String;J)V
    .locals 8
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "cursor"    # J

    .prologue
    .line 547
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$30;

    sget-object v3, Ltwitter4j/TwitterMethod;->FRIENDS_IDS:Ltwitter4j/TwitterMethod;

    iget-object v4, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Ltwitter4j/AsyncTwitterImpl$30;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 560
    return-void
.end method

.method public getFriendsList(JJ)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "cursor"    # J

    .prologue
    .line 841
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$48;

    sget-object v2, Ltwitter4j/TwitterMethod;->FRIENDS_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$48;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 853
    return-void
.end method

.method public getFriendsList(Ljava/lang/String;J)V
    .locals 8
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "cursor"    # J

    .prologue
    .line 857
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$49;

    sget-object v3, Ltwitter4j/TwitterMethod;->FRIENDS_LIST:Ltwitter4j/TwitterMethod;

    iget-object v4, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Ltwitter4j/AsyncTwitterImpl$49;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 869
    return-void
.end method

.method public getGeoDetails(Ljava/lang/String;)V
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 2310
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$137;

    sget-object v2, Ltwitter4j/TwitterMethod;->GEO_DETAILS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$137;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2322
    return-void
.end method

.method public getHomeTimeline()V
    .locals 4

    .prologue
    .line 174
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$7;

    sget-object v2, Ltwitter4j/TwitterMethod;->HOME_TIMELINE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$7;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 186
    return-void
.end method

.method public getHomeTimeline(Ltwitter4j/Paging;)V
    .locals 4
    .param p1, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 190
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$8;

    sget-object v2, Ltwitter4j/TwitterMethod;->HOME_TIMELINE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$8;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/Paging;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 202
    return-void
.end method

.method public getIncomingFriendships(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 645
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$36;

    sget-object v2, Ltwitter4j/TwitterMethod;->INCOMING_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$36;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 657
    return-void
.end method

.method public getLanguages()V
    .locals 4

    .prologue
    .line 2476
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$147;

    sget-object v2, Ltwitter4j/TwitterMethod;->LANGUAGES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$147;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2488
    return-void
.end method

.method public getMemberSuggestions(Ljava/lang/String;)V
    .locals 4
    .param p1, "categorySlug"    # Ljava/lang/String;

    .prologue
    .line 1520
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$89;

    sget-object v2, Ltwitter4j/TwitterMethod;->MEMBER_SUGGESTIONS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$89;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1532
    return-void
.end method

.method public getMentions()V
    .locals 4

    .prologue
    .line 64
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$1;

    sget-object v2, Ltwitter4j/TwitterMethod;->MENTIONS_TIMELINE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$1;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 76
    return-void
.end method

.method public getMentions(Ltwitter4j/Paging;)V
    .locals 4
    .param p1, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 80
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$2;

    sget-object v2, Ltwitter4j/TwitterMethod;->MENTIONS_TIMELINE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$2;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/Paging;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 92
    return-void
.end method

.method public getMutesIDs(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 1214
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$70;

    sget-object v2, Ltwitter4j/TwitterMethod;->MUTE_LIST_IDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$70;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1226
    return-void
.end method

.method public getMutesList(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 1198
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$69;

    sget-object v2, Ltwitter4j/TwitterMethod;->MUTE_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$69;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1210
    return-void
.end method

.method public declared-synchronized getOAuth2Token()Ltwitter4j/auth/OAuth2Token;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2765
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0}, Ltwitter4j/Twitter;->getOAuth2Token()Ltwitter4j/auth/OAuth2Token;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getOAuth2TokenAsync()V
    .locals 4

    .prologue
    .line 2775
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$160;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$160;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2787
    return-void
.end method

.method public getOAuthAccessToken()Ltwitter4j/auth/AccessToken;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2736
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0}, Ltwitter4j/Twitter;->getOAuthAccessToken()Ltwitter4j/auth/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthAccessToken(Ljava/lang/String;)Ltwitter4j/auth/AccessToken;
    .locals 1
    .param p1, "oauthVerifier"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2741
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0, p1}, Ltwitter4j/Twitter;->getOAuthAccessToken(Ljava/lang/String;)Ltwitter4j/auth/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;)Ltwitter4j/auth/AccessToken;
    .locals 1
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2760
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0, p1, p2}, Ltwitter4j/Twitter;->getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;)Ltwitter4j/auth/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthAccessToken(Ltwitter4j/auth/RequestToken;)Ltwitter4j/auth/AccessToken;
    .locals 1
    .param p1, "requestToken"    # Ltwitter4j/auth/RequestToken;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2746
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0, p1}, Ltwitter4j/Twitter;->getOAuthAccessToken(Ltwitter4j/auth/RequestToken;)Ltwitter4j/auth/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthAccessToken(Ltwitter4j/auth/RequestToken;Ljava/lang/String;)Ltwitter4j/auth/AccessToken;
    .locals 1
    .param p1, "requestToken"    # Ltwitter4j/auth/RequestToken;
    .param p2, "oauthVerifier"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2751
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0, p1, p2}, Ltwitter4j/Twitter;->getOAuthAccessToken(Ltwitter4j/auth/RequestToken;Ljava/lang/String;)Ltwitter4j/auth/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthAccessTokenAsync()V
    .locals 4

    .prologue
    .line 2606
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$155;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$155;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2618
    return-void
.end method

.method public getOAuthAccessTokenAsync(Ljava/lang/String;)V
    .locals 4
    .param p1, "oauthVerifier"    # Ljava/lang/String;

    .prologue
    .line 2622
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$156;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$156;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2634
    return-void
.end method

.method public getOAuthAccessTokenAsync(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 2670
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$159;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$159;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2682
    return-void
.end method

.method public getOAuthAccessTokenAsync(Ltwitter4j/auth/RequestToken;)V
    .locals 4
    .param p1, "requestToken"    # Ltwitter4j/auth/RequestToken;

    .prologue
    .line 2638
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$157;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$157;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/auth/RequestToken;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2650
    return-void
.end method

.method public getOAuthAccessTokenAsync(Ltwitter4j/auth/RequestToken;Ljava/lang/String;)V
    .locals 7
    .param p1, "requestToken"    # Ltwitter4j/auth/RequestToken;
    .param p2, "oauthVerifier"    # Ljava/lang/String;

    .prologue
    .line 2654
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$158;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$158;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/auth/RequestToken;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2666
    return-void
.end method

.method public getOAuthRequestToken()Ltwitter4j/auth/RequestToken;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2717
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0}, Ltwitter4j/Twitter;->getOAuthRequestToken()Ltwitter4j/auth/RequestToken;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthRequestToken(Ljava/lang/String;)Ltwitter4j/auth/RequestToken;
    .locals 1
    .param p1, "callbackUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2722
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0, p1}, Ltwitter4j/Twitter;->getOAuthRequestToken(Ljava/lang/String;)Ltwitter4j/auth/RequestToken;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthRequestTokenAsync()V
    .locals 4

    .prologue
    .line 2558
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$152;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_REQUEST_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$152;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2570
    return-void
.end method

.method public getOAuthRequestTokenAsync(Ljava/lang/String;)V
    .locals 4
    .param p1, "callbackURL"    # Ljava/lang/String;

    .prologue
    .line 2574
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$153;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_REQUEST_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$153;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2586
    return-void
.end method

.method public getOAuthRequestTokenAsync(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "callbackURL"    # Ljava/lang/String;
    .param p2, "xAuthAccessType"    # Ljava/lang/String;

    .prologue
    .line 2590
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$154;

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_REQUEST_TOKEN:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$154;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2602
    return-void
.end method

.method public getOEmbed(Ltwitter4j/OEmbedRequest;)V
    .locals 4
    .param p1, "req"    # Ltwitter4j/OEmbedRequest;

    .prologue
    .line 332
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$17;

    sget-object v2, Ltwitter4j/TwitterMethod;->OEMBED:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$17;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/OEmbedRequest;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 344
    return-void
.end method

.method public getOutgoingFriendships(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 661
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$37;

    sget-object v2, Ltwitter4j/TwitterMethod;->OUTGOING_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$37;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 673
    return-void
.end method

.method public getPlaceTrends(I)V
    .locals 4
    .param p1, "woeid"    # I

    .prologue
    .line 2377
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$141;

    sget-object v2, Ltwitter4j/TwitterMethod;->PLACE_TRENDS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$141;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;I)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2388
    return-void
.end method

.method public getPrivacyPolicy()V
    .locals 4

    .prologue
    .line 2492
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$148;

    sget-object v2, Ltwitter4j/TwitterMethod;->PRIVACY_POLICY:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$148;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2504
    return-void
.end method

.method public getRateLimitStatus()V
    .locals 4

    .prologue
    .line 2524
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$150;

    sget-object v2, Ltwitter4j/TwitterMethod;->RATE_LIMIT_STATUS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$150;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2536
    return-void
.end method

.method public varargs getRateLimitStatus([Ljava/lang/String;)V
    .locals 4
    .param p1, "resources"    # [Ljava/lang/String;

    .prologue
    .line 2540
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$151;

    sget-object v2, Ltwitter4j/TwitterMethod;->RATE_LIMIT_STATUS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$151;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;[Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2552
    return-void
.end method

.method public getRetweets(J)V
    .locals 7
    .param p1, "statusId"    # J

    .prologue
    .line 236
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$11;

    sget-object v2, Ltwitter4j/TwitterMethod;->RETWEETS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$11;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 248
    return-void
.end method

.method public getRetweetsOfMe()V
    .locals 4

    .prologue
    .line 205
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$9;

    sget-object v2, Ltwitter4j/TwitterMethod;->RETWEETS_OF_ME:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$9;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 216
    return-void
.end method

.method public getRetweetsOfMe(Ltwitter4j/Paging;)V
    .locals 4
    .param p1, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 219
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$10;

    sget-object v2, Ltwitter4j/TwitterMethod;->RETWEETS_OF_ME:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$10;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/Paging;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 230
    return-void
.end method

.method public getSavedSearches()V
    .locals 4

    .prologue
    .line 2244
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$133;

    sget-object v2, Ltwitter4j/TwitterMethod;->SAVED_SEARCHES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$133;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2256
    return-void
.end method

.method public getSentDirectMessages()V
    .locals 4

    .prologue
    .line 416
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$22;

    sget-object v2, Ltwitter4j/TwitterMethod;->SENT_DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$22;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 428
    return-void
.end method

.method public getSentDirectMessages(Ltwitter4j/Paging;)V
    .locals 4
    .param p1, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 432
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$23;

    sget-object v2, Ltwitter4j/TwitterMethod;->SENT_DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$23;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/Paging;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 444
    return-void
.end method

.method public getSimilarPlaces(Ltwitter4j/GeoLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "location"    # Ltwitter4j/GeoLocation;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "containedWithin"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;

    .prologue
    .line 2359
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$140;

    sget-object v2, Ltwitter4j/TwitterMethod;->SIMILAR_PLACES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$140;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/GeoLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2371
    return-void
.end method

.method public getSuggestedUserCategories()V
    .locals 4

    .prologue
    .line 1504
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$88;

    sget-object v2, Ltwitter4j/TwitterMethod;->SUGGESTED_USER_CATEGORIES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$88;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1516
    return-void
.end method

.method public getTermsOfService()V
    .locals 4

    .prologue
    .line 2508
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$149;

    sget-object v2, Ltwitter4j/TwitterMethod;->TERMS_OF_SERVICE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$149;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2520
    return-void
.end method

.method public getUserListMembers(JJ)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "cursor"    # J

    .prologue
    .line 2047
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$121;

    sget-object v2, Ltwitter4j/TwitterMethod;->LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$121;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2059
    return-void
.end method

.method public getUserListMembers(JLjava/lang/String;J)V
    .locals 10
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "cursor"    # J

    .prologue
    .line 2063
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$122;

    sget-object v2, Ltwitter4j/TwitterMethod;->LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-wide v7, p4

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$122;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;J)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2075
    return-void
.end method

.method public getUserListMemberships(J)V
    .locals 7
    .param p1, "cursor"    # J

    .prologue
    .line 1764
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$104;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LIST_MEMBERSHIPS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$104;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1776
    return-void
.end method

.method public getUserListMemberships(JJ)V
    .locals 7
    .param p1, "listMemberId"    # J
    .param p3, "cursor"    # J

    .prologue
    .line 1785
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Ltwitter4j/AsyncTwitterImpl;->getUserListMemberships(JJZ)V

    .line 1786
    return-void
.end method

.method public getUserListMemberships(JJZ)V
    .locals 11
    .param p1, "listMemberId"    # J
    .param p3, "cursor"    # J
    .param p5, "filterToOwnedLists"    # Z

    .prologue
    .line 1806
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$106;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LIST_MEMBERSHIPS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move/from16 v8, p5

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$106;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJZ)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1818
    return-void
.end method

.method public getUserListMemberships(Ljava/lang/String;J)V
    .locals 2
    .param p1, "listMemberScreenName"    # Ljava/lang/String;
    .param p2, "cursor"    # J

    .prologue
    .line 1780
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Ltwitter4j/AsyncTwitterImpl;->getUserListMemberships(Ljava/lang/String;JZ)V

    .line 1781
    return-void
.end method

.method public getUserListMemberships(Ljava/lang/String;JZ)V
    .locals 10
    .param p1, "listMemberScreenName"    # Ljava/lang/String;
    .param p2, "cursor"    # J
    .param p4, "filterToOwnedLists"    # Z

    .prologue
    .line 1790
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$105;

    sget-object v3, Ltwitter4j/TwitterMethod;->USER_LIST_MEMBERSHIPS:Ltwitter4j/TwitterMethod;

    iget-object v4, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    move v8, p4

    invoke-direct/range {v1 .. v8}, Ltwitter4j/AsyncTwitterImpl$105;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;JZ)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1802
    return-void
.end method

.method public getUserListStatuses(JLjava/lang/String;Ltwitter4j/Paging;)V
    .locals 9
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 1716
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$101;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LIST_STATUSES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$101;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;Ltwitter4j/Paging;)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1728
    return-void
.end method

.method public getUserListStatuses(JLtwitter4j/Paging;)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 1700
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$100;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LIST_STATUSES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$100;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLtwitter4j/Paging;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1712
    return-void
.end method

.method public getUserListSubscribers(JJ)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "cursor"    # J

    .prologue
    .line 1822
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$107;

    sget-object v2, Ltwitter4j/TwitterMethod;->LIST_SUBSCRIBERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$107;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1834
    return-void
.end method

.method public getUserListSubscribers(JLjava/lang/String;J)V
    .locals 10
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "cursor"    # J

    .prologue
    .line 1838
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$108;

    sget-object v2, Ltwitter4j/TwitterMethod;->LIST_SUBSCRIBERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-wide v7, p4

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$108;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;J)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1850
    return-void
.end method

.method public getUserListSubscriptions(Ljava/lang/String;J)V
    .locals 8
    .param p1, "listOwnerScreenName"    # Ljava/lang/String;
    .param p2, "cursor"    # J

    .prologue
    .line 2224
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$132;

    sget-object v3, Ltwitter4j/TwitterMethod;->USER_LIST_SUBSCRIPTIONS:Ltwitter4j/TwitterMethod;

    iget-object v4, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Ltwitter4j/AsyncTwitterImpl$132;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2236
    return-void
.end method

.method public getUserLists(J)V
    .locals 7
    .param p1, "listOwnerUserId"    # J

    .prologue
    .line 1668
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$98;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LISTS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$98;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1680
    return-void
.end method

.method public getUserLists(Ljava/lang/String;)V
    .locals 4
    .param p1, "listOwnerScreenName"    # Ljava/lang/String;

    .prologue
    .line 1684
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$99;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LISTS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$99;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1696
    return-void
.end method

.method public getUserSuggestions(Ljava/lang/String;)V
    .locals 4
    .param p1, "categorySlug"    # Ljava/lang/String;

    .prologue
    .line 1488
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$87;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_SUGGESTIONS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$87;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1500
    return-void
.end method

.method public getUserTimeline()V
    .locals 4

    .prologue
    .line 158
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$6;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_TIMELINE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$6;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 170
    return-void
.end method

.method public getUserTimeline(J)V
    .locals 1
    .param p1, "userId"    # J

    .prologue
    .line 153
    new-instance v0, Ltwitter4j/Paging;

    invoke-direct {v0}, Ltwitter4j/Paging;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Ltwitter4j/AsyncTwitterImpl;->getUserTimeline(JLtwitter4j/Paging;)V

    .line 154
    return-void
.end method

.method public getUserTimeline(JLtwitter4j/Paging;)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 114
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$4;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_TIMELINE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$4;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLtwitter4j/Paging;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 127
    return-void
.end method

.method public getUserTimeline(Ljava/lang/String;)V
    .locals 1
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 148
    new-instance v0, Ltwitter4j/Paging;

    invoke-direct {v0}, Ltwitter4j/Paging;-><init>()V

    invoke-virtual {p0, p1, v0}, Ltwitter4j/AsyncTwitterImpl;->getUserTimeline(Ljava/lang/String;Ltwitter4j/Paging;)V

    .line 149
    return-void
.end method

.method public getUserTimeline(Ljava/lang/String;Ltwitter4j/Paging;)V
    .locals 7
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 96
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$3;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_TIMELINE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$3;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Ltwitter4j/Paging;)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 110
    return-void
.end method

.method public getUserTimeline(Ltwitter4j/Paging;)V
    .locals 4
    .param p1, "paging"    # Ltwitter4j/Paging;

    .prologue
    .line 131
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$5;

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_TIMELINE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$5;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/Paging;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 144
    return-void
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2841
    invoke-super {p0}, Ltwitter4j/TwitterBaseImpl;->hashCode()I

    move-result v0

    .line 2842
    .local v0, "result":I
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    .line 2843
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v2, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 2844
    return v0

    :cond_1
    move v1, v2

    .line 2842
    goto :goto_0
.end method

.method public declared-synchronized invalidateOAuth2Token()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2791
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0}, Ltwitter4j/Twitter;->invalidateOAuth2Token()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2792
    monitor-exit p0

    return-void

    .line 2791
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public lookup([J)V
    .locals 4
    .param p1, "ids"    # [J

    .prologue
    .line 348
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$18;

    sget-object v2, Ltwitter4j/TwitterMethod;->RETWEET_STATUS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$18;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;[J)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 360
    return-void
.end method

.method public lookupFriendships([J)V
    .locals 4
    .param p1, "ids"    # [J

    .prologue
    .line 613
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$34;

    sget-object v2, Ltwitter4j/TwitterMethod;->LOOKUP_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$34;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;[J)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 625
    return-void
.end method

.method public lookupFriendships([Ljava/lang/String;)V
    .locals 4
    .param p1, "screenNames"    # [Ljava/lang/String;

    .prologue
    .line 629
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$35;

    sget-object v2, Ltwitter4j/TwitterMethod;->LOOKUP_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$35;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;[Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 641
    return-void
.end method

.method public lookupUsers([J)V
    .locals 4
    .param p1, "ids"    # [J

    .prologue
    .line 1294
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$75;

    sget-object v2, Ltwitter4j/TwitterMethod;->LOOKUP_USERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$75;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;[J)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1306
    return-void
.end method

.method public lookupUsers([Ljava/lang/String;)V
    .locals 4
    .param p1, "screenNames"    # [Ljava/lang/String;

    .prologue
    .line 1310
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$76;

    sget-object v2, Ltwitter4j/TwitterMethod;->LOOKUP_USERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$76;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;[Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1322
    return-void
.end method

.method public removeProfileBanner()V
    .locals 4

    .prologue
    .line 1438
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$84;

    sget-object v2, Ltwitter4j/TwitterMethod;->REMOVE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$84;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1450
    return-void
.end method

.method public reportSpam(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 2426
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$144;

    sget-object v2, Ltwitter4j/TwitterMethod;->REPORT_SPAM:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$144;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2438
    return-void
.end method

.method public reportSpam(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 2442
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$145;

    sget-object v2, Ltwitter4j/TwitterMethod;->REPORT_SPAM:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$145;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2454
    return-void
.end method

.method public retweetStatus(J)V
    .locals 7
    .param p1, "statusId"    # J

    .prologue
    .line 316
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$16;

    sget-object v2, Ltwitter4j/TwitterMethod;->RETWEET_STATUS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$16;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 328
    return-void
.end method

.method public reverseGeoCode(Ltwitter4j/GeoQuery;)V
    .locals 4
    .param p1, "query"    # Ltwitter4j/GeoQuery;

    .prologue
    .line 2326
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$138;

    sget-object v2, Ltwitter4j/TwitterMethod;->REVERSE_GEO_CODE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$138;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/GeoQuery;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2338
    return-void
.end method

.method public search(Ltwitter4j/Query;)V
    .locals 4
    .param p1, "query"    # Ltwitter4j/Query;

    .prologue
    .line 366
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$19;

    sget-object v2, Ltwitter4j/TwitterMethod;->SEARCH:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$19;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/Query;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 378
    return-void
.end method

.method public searchPlaces(Ltwitter4j/GeoQuery;)V
    .locals 4
    .param p1, "query"    # Ltwitter4j/GeoQuery;

    .prologue
    .line 2342
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$139;

    sget-object v2, Ltwitter4j/TwitterMethod;->SEARCH_PLACES:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$139;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/GeoQuery;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2354
    return-void
.end method

.method public searchUsers(Ljava/lang/String;I)V
    .locals 7
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "page"    # I

    .prologue
    .line 1358
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$79;

    sget-object v2, Ltwitter4j/TwitterMethod;->SEARCH_USERS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$79;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;I)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1370
    return-void
.end method

.method public sendDirectMessage(JLjava/lang/String;)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 480
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$26;

    sget-object v2, Ltwitter4j/TwitterMethod;->SEND_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$26;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 492
    return-void
.end method

.method public sendDirectMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 496
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$27;

    sget-object v2, Ltwitter4j/TwitterMethod;->SEND_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$27;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 508
    return-void
.end method

.method public setOAuth2Token(Ltwitter4j/auth/OAuth2Token;)V
    .locals 1
    .param p1, "oauth2Token"    # Ltwitter4j/auth/OAuth2Token;

    .prologue
    .line 2770
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0, p1}, Ltwitter4j/Twitter;->setOAuth2Token(Ltwitter4j/auth/OAuth2Token;)V

    .line 2771
    return-void
.end method

.method public setOAuthAccessToken(Ltwitter4j/auth/AccessToken;)V
    .locals 1
    .param p1, "accessToken"    # Ltwitter4j/auth/AccessToken;

    .prologue
    .line 2756
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0, p1}, Ltwitter4j/Twitter;->setOAuthAccessToken(Ltwitter4j/auth/AccessToken;)V

    .line 2757
    return-void
.end method

.method public setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "consumerKey"    # Ljava/lang/String;
    .param p2, "consumerSecret"    # Ljava/lang/String;

    .prologue
    .line 2712
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-interface {v0, p1, p2}, Ltwitter4j/Twitter;->setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V

    .line 2713
    return-void
.end method

.method public showDirectMessage(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 448
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$24;

    sget-object v2, Ltwitter4j/TwitterMethod;->DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$24;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 460
    return-void
.end method

.method public showFriendship(JJ)V
    .locals 9
    .param p1, "sourceId"    # J
    .param p3, "targetId"    # J

    .prologue
    .line 809
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$46;

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$46;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 821
    return-void
.end method

.method public showFriendship(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "sourceScreenName"    # Ljava/lang/String;
    .param p2, "targetScreenName"    # Ljava/lang/String;

    .prologue
    .line 825
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$47;

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$47;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 837
    return-void
.end method

.method public showSavedSearch(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 2260
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$134;

    sget-object v2, Ltwitter4j/TwitterMethod;->SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$134;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;I)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2272
    return-void
.end method

.method public showStatus(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 252
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$12;

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_STATUS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$12;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 264
    return-void
.end method

.method public showUser(J)V
    .locals 7
    .param p1, "userId"    # J

    .prologue
    .line 1326
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$77;

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_USER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$77;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1338
    return-void
.end method

.method public showUser(Ljava/lang/String;)V
    .locals 4
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 1342
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$78;

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_USER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$78;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1354
    return-void
.end method

.method public showUserList(J)V
    .locals 7
    .param p1, "listId"    # J

    .prologue
    .line 2191
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$130;

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_USER_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$130;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2203
    return-void
.end method

.method public showUserList(JLjava/lang/String;)V
    .locals 9
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;

    .prologue
    .line 2207
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$131;

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_USER_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$131;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2219
    return-void
.end method

.method public showUserListMembership(JJ)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "userId"    # J

    .prologue
    .line 2015
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$119;

    sget-object v2, Ltwitter4j/TwitterMethod;->CHECK_LIST_MEMBERSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$119;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2027
    return-void
.end method

.method public showUserListMembership(JLjava/lang/String;J)V
    .locals 10
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "userId"    # J

    .prologue
    .line 2031
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$120;

    sget-object v2, Ltwitter4j/TwitterMethod;->CHECK_LIST_MEMBERSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-wide v7, p4

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$120;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;J)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2043
    return-void
.end method

.method public showUserListSubscription(JJ)V
    .locals 9
    .param p1, "listId"    # J
    .param p3, "userId"    # J

    .prologue
    .line 1886
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$111;

    sget-object v2, Ltwitter4j/TwitterMethod;->CHECK_LIST_SUBSCRIPTION:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$111;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1898
    return-void
.end method

.method public showUserListSubscription(JLjava/lang/String;J)V
    .locals 10
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "userId"    # J

    .prologue
    .line 1902
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$112;

    sget-object v2, Ltwitter4j/TwitterMethod;->CHECK_LIST_SUBSCRIPTION:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-wide v7, p4

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$112;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;J)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1914
    return-void
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 2688
    const-class v1, Ltwitter4j/AsyncTwitterImpl;

    monitor-enter v1

    .line 2689
    :try_start_0
    sget-object v0, Ltwitter4j/AsyncTwitterImpl;->dispatcher:Ltwitter4j/Dispatcher;

    if-eqz v0, :cond_0

    .line 2690
    sget-object v0, Ltwitter4j/AsyncTwitterImpl;->dispatcher:Ltwitter4j/Dispatcher;

    invoke-interface {v0}, Ltwitter4j/Dispatcher;->shutdown()V

    .line 2691
    const/4 v0, 0x0

    sput-object v0, Ltwitter4j/AsyncTwitterImpl;->dispatcher:Ltwitter4j/Dispatcher;

    .line 2693
    :cond_0
    monitor-exit v1

    .line 2694
    return-void

    .line 2693
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2849
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncTwitterImpl{twitter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/AsyncTwitterImpl;->twitter:Ltwitter4j/Twitter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", listeners="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateAccountSettings(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "trend_locationWoeid"    # Ljava/lang/Integer;
    .param p2, "sleep_timeEnabled"    # Ljava/lang/Boolean;
    .param p3, "start_sleepTime"    # Ljava/lang/String;
    .param p4, "end_sleepTime"    # Ljava/lang/String;
    .param p5, "time_zone"    # Ljava/lang/String;
    .param p6, "lang"    # Ljava/lang/String;

    .prologue
    .line 939
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v10

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$54;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_ACCOUNT_SETTINGS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Ltwitter4j/AsyncTwitterImpl$54;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v10, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 951
    return-void
.end method

.method public updateFriendship(JZZ)V
    .locals 9
    .param p1, "userId"    # J
    .param p3, "enableDeviceNotification"    # Z
    .param p4, "retweet"    # Z

    .prologue
    .line 774
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$44;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$44;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JZZ)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 787
    return-void
.end method

.method public updateFriendship(Ljava/lang/String;ZZ)V
    .locals 8
    .param p1, "screenName"    # Ljava/lang/String;
    .param p2, "enableDeviceNotification"    # Z
    .param p3, "retweet"    # Z

    .prologue
    .line 792
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v7

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$45;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Ltwitter4j/AsyncTwitterImpl$45;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;ZZ)V

    invoke-interface {v7, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 805
    return-void
.end method

.method public updateProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 956
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v8

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$55;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Ltwitter4j/AsyncTwitterImpl$55;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 968
    return-void
.end method

.method public updateProfileBackgroundImage(Ljava/io/File;Z)V
    .locals 7
    .param p1, "image"    # Ljava/io/File;
    .param p2, "tile"    # Z

    .prologue
    .line 973
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$56;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_BACKGROUND_IMAGE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$56;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/io/File;Z)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 987
    return-void
.end method

.method public updateProfileBackgroundImage(Ljava/io/InputStream;Z)V
    .locals 7
    .param p1, "image"    # Ljava/io/InputStream;
    .param p2, "tile"    # Z

    .prologue
    .line 992
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v6

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$57;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_BACKGROUND_IMAGE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Ltwitter4j/AsyncTwitterImpl$57;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/io/InputStream;Z)V

    invoke-interface {v6, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1006
    return-void
.end method

.method public updateProfileBanner(Ljava/io/File;)V
    .locals 4
    .param p1, "image"    # Ljava/io/File;

    .prologue
    .line 1454
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$85;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$85;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/io/File;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1466
    return-void
.end method

.method public updateProfileBanner(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "image"    # Ljava/io/InputStream;

    .prologue
    .line 1470
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$86;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$86;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1482
    return-void
.end method

.method public updateProfileColors(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "profileBackgroundColor"    # Ljava/lang/String;
    .param p2, "profileTextColor"    # Ljava/lang/String;
    .param p3, "profileLinkColor"    # Ljava/lang/String;
    .param p4, "profileSidebarFillColor"    # Ljava/lang/String;
    .param p5, "profileSidebarBorderColor"    # Ljava/lang/String;

    .prologue
    .line 1013
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$58;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_COLORS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$58;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1030
    return-void
.end method

.method public updateProfileImage(Ljava/io/File;)V
    .locals 4
    .param p1, "image"    # Ljava/io/File;

    .prologue
    .line 1034
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$59;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_IMAGE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$59;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/io/File;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1048
    return-void
.end method

.method public updateProfileImage(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "image"    # Ljava/io/InputStream;

    .prologue
    .line 1052
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$60;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_IMAGE:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$60;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 1066
    return-void
.end method

.method public updateStatus(Ljava/lang/String;)V
    .locals 4
    .param p1, "statusText"    # Ljava/lang/String;

    .prologue
    .line 284
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$14;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_STATUS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$14;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 296
    return-void
.end method

.method public updateStatus(Ltwitter4j/StatusUpdate;)V
    .locals 4
    .param p1, "latestStatus"    # Ltwitter4j/StatusUpdate;

    .prologue
    .line 300
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$15;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_STATUS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3, p1}, Ltwitter4j/AsyncTwitterImpl$15;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ltwitter4j/StatusUpdate;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 312
    return-void
.end method

.method public updateUserList(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 11
    .param p1, "ownerId"    # J
    .param p3, "slug"    # Ljava/lang/String;
    .param p4, "newListName"    # Ljava/lang/String;
    .param p5, "isPublicList"    # Z
    .param p6, "newDescription"    # Ljava/lang/String;

    .prologue
    .line 2159
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v10

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$128;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_USER_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Ltwitter4j/AsyncTwitterImpl$128;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v10, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2171
    return-void
.end method

.method public updateUserList(JLjava/lang/String;ZLjava/lang/String;)V
    .locals 11
    .param p1, "listId"    # J
    .param p3, "newListName"    # Ljava/lang/String;
    .param p4, "isPublicList"    # Z
    .param p5, "newDescription"    # Ljava/lang/String;

    .prologue
    .line 2143
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v9

    new-instance v0, Ltwitter4j/AsyncTwitterImpl$127;

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_USER_LIST:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v0 .. v8}, Ltwitter4j/AsyncTwitterImpl$127;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v9, v0}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 2155
    return-void
.end method

.method public verifyCredentials()V
    .locals 4

    .prologue
    .line 923
    invoke-direct {p0}, Ltwitter4j/AsyncTwitterImpl;->getDispatcher()Ltwitter4j/Dispatcher;

    move-result-object v0

    new-instance v1, Ltwitter4j/AsyncTwitterImpl$53;

    sget-object v2, Ltwitter4j/TwitterMethod;->VERIFY_CREDENTIALS:Ltwitter4j/TwitterMethod;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl;->listeners:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Ltwitter4j/AsyncTwitterImpl$53;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ltwitter4j/Dispatcher;->invokeLater(Ljava/lang/Runnable;)V

    .line 935
    return-void
.end method
