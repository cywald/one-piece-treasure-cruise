.class Ltwitter4j/AsyncTwitterImpl$46;
.super Ltwitter4j/AsyncTwitterImpl$AsyncTask;
.source "AsyncTwitterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltwitter4j/AsyncTwitterImpl;->showFriendship(JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ltwitter4j/AsyncTwitterImpl;

.field final synthetic val$sourceId:J

.field final synthetic val$targetId:J


# direct methods
.method constructor <init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JJ)V
    .locals 0
    .param p1, "this$0"    # Ltwitter4j/AsyncTwitterImpl;
    .param p2, "method"    # Ltwitter4j/TwitterMethod;

    .prologue
    .line 809
    .local p3, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iput-object p1, p0, Ltwitter4j/AsyncTwitterImpl$46;->this$0:Ltwitter4j/AsyncTwitterImpl;

    iput-wide p4, p0, Ltwitter4j/AsyncTwitterImpl$46;->val$sourceId:J

    iput-wide p6, p0, Ltwitter4j/AsyncTwitterImpl$46;->val$targetId:J

    invoke-direct {p0, p1, p2, p3}, Ltwitter4j/AsyncTwitterImpl$AsyncTask;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 812
    .local p1, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iget-object v2, p0, Ltwitter4j/AsyncTwitterImpl$46;->this$0:Ltwitter4j/AsyncTwitterImpl;

    invoke-static {v2}, Ltwitter4j/AsyncTwitterImpl;->access$000(Ltwitter4j/AsyncTwitterImpl;)Ltwitter4j/Twitter;

    move-result-object v2

    iget-wide v4, p0, Ltwitter4j/AsyncTwitterImpl$46;->val$sourceId:J

    iget-wide v6, p0, Ltwitter4j/AsyncTwitterImpl$46;->val$targetId:J

    invoke-interface {v2, v4, v5, v6, v7}, Ltwitter4j/Twitter;->showFriendship(JJ)Ltwitter4j/Relationship;

    move-result-object v1

    .line 813
    .local v1, "relationship":Ltwitter4j/Relationship;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltwitter4j/TwitterListener;

    .line 815
    .local v0, "listener":Ltwitter4j/TwitterListener;
    :try_start_0
    invoke-interface {v0, v1}, Ltwitter4j/TwitterListener;->gotShowFriendship(Ltwitter4j/Relationship;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 816
    :catch_0
    move-exception v3

    goto :goto_0

    .line 819
    .end local v0    # "listener":Ltwitter4j/TwitterListener;
    :cond_0
    return-void
.end method
