.class Ltwitter4j/AsyncTwitterImpl$105;
.super Ltwitter4j/AsyncTwitterImpl$AsyncTask;
.source "AsyncTwitterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltwitter4j/AsyncTwitterImpl;->getUserListMemberships(Ljava/lang/String;JZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ltwitter4j/AsyncTwitterImpl;

.field final synthetic val$cursor:J

.field final synthetic val$filterToOwnedLists:Z

.field final synthetic val$listMemberScreenName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/String;JZ)V
    .locals 1
    .param p1, "this$0"    # Ltwitter4j/AsyncTwitterImpl;
    .param p2, "method"    # Ltwitter4j/TwitterMethod;

    .prologue
    .line 1790
    .local p3, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iput-object p1, p0, Ltwitter4j/AsyncTwitterImpl$105;->this$0:Ltwitter4j/AsyncTwitterImpl;

    iput-object p4, p0, Ltwitter4j/AsyncTwitterImpl$105;->val$listMemberScreenName:Ljava/lang/String;

    iput-wide p5, p0, Ltwitter4j/AsyncTwitterImpl$105;->val$cursor:J

    iput-boolean p7, p0, Ltwitter4j/AsyncTwitterImpl$105;->val$filterToOwnedLists:Z

    invoke-direct {p0, p1, p2, p3}, Ltwitter4j/AsyncTwitterImpl$AsyncTask;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 1793
    .local p1, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iget-object v2, p0, Ltwitter4j/AsyncTwitterImpl$105;->this$0:Ltwitter4j/AsyncTwitterImpl;

    invoke-static {v2}, Ltwitter4j/AsyncTwitterImpl;->access$000(Ltwitter4j/AsyncTwitterImpl;)Ltwitter4j/Twitter;

    move-result-object v2

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl$105;->val$listMemberScreenName:Ljava/lang/String;

    iget-wide v4, p0, Ltwitter4j/AsyncTwitterImpl$105;->val$cursor:J

    iget-boolean v6, p0, Ltwitter4j/AsyncTwitterImpl$105;->val$filterToOwnedLists:Z

    invoke-interface {v2, v3, v4, v5, v6}, Ltwitter4j/Twitter;->getUserListMemberships(Ljava/lang/String;JZ)Ltwitter4j/PagableResponseList;

    move-result-object v1

    .line 1794
    .local v1, "lists":Ltwitter4j/PagableResponseList;, "Ltwitter4j/PagableResponseList<Ltwitter4j/UserList;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltwitter4j/TwitterListener;

    .line 1796
    .local v0, "listener":Ltwitter4j/TwitterListener;
    :try_start_0
    invoke-interface {v0, v1}, Ltwitter4j/TwitterListener;->gotUserListMemberships(Ltwitter4j/PagableResponseList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1797
    :catch_0
    move-exception v3

    goto :goto_0

    .line 1800
    .end local v0    # "listener":Ltwitter4j/TwitterListener;
    :cond_0
    return-void
.end method
