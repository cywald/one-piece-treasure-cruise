.class public final enum Ltwitter4j/TwitterMethod;
.super Ljava/lang/Enum;
.source "TwitterMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltwitter4j/TwitterMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Ltwitter4j/TwitterMethod;

.field public static final enum ACCOUNT_SETTINGS:Ltwitter4j/TwitterMethod;

.field public static final enum AVAILABLE_TRENDS:Ltwitter4j/TwitterMethod;

.field public static final enum BLOCK_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum BLOCK_LIST_IDS:Ltwitter4j/TwitterMethod;

.field public static final enum CHECK_LIST_MEMBERSHIP:Ltwitter4j/TwitterMethod;

.field public static final enum CHECK_LIST_SUBSCRIPTION:Ltwitter4j/TwitterMethod;

.field public static final enum CLOSEST_TRENDS:Ltwitter4j/TwitterMethod;

.field public static final enum CONFIGURATION:Ltwitter4j/TwitterMethod;

.field public static final enum CONTRIBUTEEES:Ltwitter4j/TwitterMethod;

.field public static final enum CONTRIBUTORS:Ltwitter4j/TwitterMethod;

.field public static final enum CREATE_BLOCK:Ltwitter4j/TwitterMethod;

.field public static final enum CREATE_FAVORITE:Ltwitter4j/TwitterMethod;

.field public static final enum CREATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

.field public static final enum CREATE_LIST_MEMBER:Ltwitter4j/TwitterMethod;

.field public static final enum CREATE_LIST_MEMBERS:Ltwitter4j/TwitterMethod;

.field public static final enum CREATE_MUTE:Ltwitter4j/TwitterMethod;

.field public static final enum CREATE_SAVED_SEARCH:Ltwitter4j/TwitterMethod;

.field public static final enum CREATE_USER_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_BLOCK:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_FAVORITE:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_FRIENDSHIP:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_LIST_MEMBER:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_MUTE:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_SAVED_SEARCH:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_STATUS:Ltwitter4j/TwitterMethod;

.field public static final enum DESTROY_USER_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

.field public static final enum DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

.field public static final enum FAVORITES:Ltwitter4j/TwitterMethod;

.field public static final enum FOLLOWERS_IDS:Ltwitter4j/TwitterMethod;

.field public static final enum FOLLOWERS_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum FRIENDS_IDS:Ltwitter4j/TwitterMethod;

.field public static final enum FRIENDS_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum GEO_DETAILS:Ltwitter4j/TwitterMethod;

.field public static final enum HOME_TIMELINE:Ltwitter4j/TwitterMethod;

.field public static final enum INCOMING_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

.field public static final enum LANGUAGES:Ltwitter4j/TwitterMethod;

.field public static final enum LIST_MEMBERS:Ltwitter4j/TwitterMethod;

.field public static final enum LIST_SUBSCRIBERS:Ltwitter4j/TwitterMethod;

.field public static final enum LOOKUP:Ltwitter4j/TwitterMethod;

.field public static final enum LOOKUP_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

.field public static final enum LOOKUP_USERS:Ltwitter4j/TwitterMethod;

.field public static final enum MEMBER_SUGGESTIONS:Ltwitter4j/TwitterMethod;

.field public static final enum MENTIONS_TIMELINE:Ltwitter4j/TwitterMethod;

.field public static final enum MUTE_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum MUTE_LIST_IDS:Ltwitter4j/TwitterMethod;

.field public static final enum OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

.field public static final enum OAUTH_REQUEST_TOKEN:Ltwitter4j/TwitterMethod;

.field public static final enum OEMBED:Ltwitter4j/TwitterMethod;

.field public static final enum OUTGOING_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

.field public static final enum PLACE_TRENDS:Ltwitter4j/TwitterMethod;

.field public static final enum PRIVACY_POLICY:Ltwitter4j/TwitterMethod;

.field public static final enum RATE_LIMIT_STATUS:Ltwitter4j/TwitterMethod;

.field public static final enum RELATED_RESULTS:Ltwitter4j/TwitterMethod;

.field public static final enum REMOVE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

.field public static final enum REPORT_SPAM:Ltwitter4j/TwitterMethod;

.field public static final enum RETWEETS:Ltwitter4j/TwitterMethod;

.field public static final enum RETWEETS_OF_ME:Ltwitter4j/TwitterMethod;

.field public static final enum RETWEET_STATUS:Ltwitter4j/TwitterMethod;

.field public static final enum REVERSE_GEO_CODE:Ltwitter4j/TwitterMethod;

.field public static final enum SAVED_SEARCH:Ltwitter4j/TwitterMethod;

.field public static final enum SAVED_SEARCHES:Ltwitter4j/TwitterMethod;

.field public static final enum SEARCH:Ltwitter4j/TwitterMethod;

.field public static final enum SEARCH_PLACES:Ltwitter4j/TwitterMethod;

.field public static final enum SEARCH_USERS:Ltwitter4j/TwitterMethod;

.field public static final enum SEND_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

.field public static final enum SENT_DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

.field public static final enum SHOW_FRIENDSHIP:Ltwitter4j/TwitterMethod;

.field public static final enum SHOW_STATUS:Ltwitter4j/TwitterMethod;

.field public static final enum SHOW_USER:Ltwitter4j/TwitterMethod;

.field public static final enum SHOW_USER_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum SIMILAR_PLACES:Ltwitter4j/TwitterMethod;

.field public static final enum SUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum SUGGESTED_USER_CATEGORIES:Ltwitter4j/TwitterMethod;

.field public static final enum TERMS_OF_SERVICE:Ltwitter4j/TwitterMethod;

.field public static final enum UNSUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_ACCOUNT_SETTINGS:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_PROFILE:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_PROFILE_BACKGROUND_IMAGE:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_PROFILE_COLORS:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_PROFILE_IMAGE:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_STATUS:Ltwitter4j/TwitterMethod;

.field public static final enum UPDATE_USER_LIST:Ltwitter4j/TwitterMethod;

.field public static final enum USER_LISTS:Ltwitter4j/TwitterMethod;

.field public static final enum USER_LIST_MEMBERSHIPS:Ltwitter4j/TwitterMethod;

.field public static final enum USER_LIST_STATUSES:Ltwitter4j/TwitterMethod;

.field public static final enum USER_LIST_SUBSCRIPTIONS:Ltwitter4j/TwitterMethod;

.field public static final enum USER_SUGGESTIONS:Ltwitter4j/TwitterMethod;

.field public static final enum USER_TIMELINE:Ltwitter4j/TwitterMethod;

.field public static final enum VERIFY_CREDENTIALS:Ltwitter4j/TwitterMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "MENTIONS_TIMELINE"

    invoke-direct {v0, v1, v3}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->MENTIONS_TIMELINE:Ltwitter4j/TwitterMethod;

    .line 26
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "USER_TIMELINE"

    invoke-direct {v0, v1, v4}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->USER_TIMELINE:Ltwitter4j/TwitterMethod;

    .line 27
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "HOME_TIMELINE"

    invoke-direct {v0, v1, v5}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->HOME_TIMELINE:Ltwitter4j/TwitterMethod;

    .line 28
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "RETWEETS_OF_ME"

    invoke-direct {v0, v1, v6}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->RETWEETS_OF_ME:Ltwitter4j/TwitterMethod;

    .line 31
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "RETWEETS"

    invoke-direct {v0, v1, v7}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->RETWEETS:Ltwitter4j/TwitterMethod;

    .line 32
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SHOW_STATUS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SHOW_STATUS:Ltwitter4j/TwitterMethod;

    .line 33
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_STATUS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_STATUS:Ltwitter4j/TwitterMethod;

    .line 34
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_STATUS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_STATUS:Ltwitter4j/TwitterMethod;

    .line 35
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "RETWEET_STATUS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->RETWEET_STATUS:Ltwitter4j/TwitterMethod;

    .line 36
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "OEMBED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->OEMBED:Ltwitter4j/TwitterMethod;

    .line 37
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "LOOKUP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->LOOKUP:Ltwitter4j/TwitterMethod;

    .line 40
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SEARCH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SEARCH:Ltwitter4j/TwitterMethod;

    .line 43
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DIRECT_MESSAGES"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

    .line 44
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SENT_DIRECT_MESSAGES"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SENT_DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

    .line 45
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DIRECT_MESSAGE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    .line 46
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_DIRECT_MESSAGE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    .line 47
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SEND_DIRECT_MESSAGE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SEND_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    .line 50
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "FRIENDS_IDS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->FRIENDS_IDS:Ltwitter4j/TwitterMethod;

    .line 51
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "FOLLOWERS_IDS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->FOLLOWERS_IDS:Ltwitter4j/TwitterMethod;

    .line 52
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "LOOKUP_FRIENDSHIPS"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->LOOKUP_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    .line 53
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "INCOMING_FRIENDSHIPS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->INCOMING_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    .line 54
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "OUTGOING_FRIENDSHIPS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->OUTGOING_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    .line 55
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CREATE_FRIENDSHIP"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CREATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    .line 56
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_FRIENDSHIP"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    .line 57
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_FRIENDSHIP"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    .line 58
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SHOW_FRIENDSHIP"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SHOW_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    .line 59
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "FRIENDS_LIST"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->FRIENDS_LIST:Ltwitter4j/TwitterMethod;

    .line 60
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "FOLLOWERS_LIST"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->FOLLOWERS_LIST:Ltwitter4j/TwitterMethod;

    .line 63
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "ACCOUNT_SETTINGS"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->ACCOUNT_SETTINGS:Ltwitter4j/TwitterMethod;

    .line 64
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "VERIFY_CREDENTIALS"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->VERIFY_CREDENTIALS:Ltwitter4j/TwitterMethod;

    .line 65
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_ACCOUNT_SETTINGS"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_ACCOUNT_SETTINGS:Ltwitter4j/TwitterMethod;

    .line 67
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_PROFILE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE:Ltwitter4j/TwitterMethod;

    .line 68
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_PROFILE_BACKGROUND_IMAGE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_BACKGROUND_IMAGE:Ltwitter4j/TwitterMethod;

    .line 69
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_PROFILE_COLORS"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_COLORS:Ltwitter4j/TwitterMethod;

    .line 70
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_PROFILE_IMAGE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_IMAGE:Ltwitter4j/TwitterMethod;

    .line 71
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "BLOCK_LIST"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->BLOCK_LIST:Ltwitter4j/TwitterMethod;

    .line 72
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "BLOCK_LIST_IDS"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->BLOCK_LIST_IDS:Ltwitter4j/TwitterMethod;

    .line 73
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CREATE_BLOCK"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CREATE_BLOCK:Ltwitter4j/TwitterMethod;

    .line 74
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_BLOCK"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_BLOCK:Ltwitter4j/TwitterMethod;

    .line 75
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "MUTE_LIST"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->MUTE_LIST:Ltwitter4j/TwitterMethod;

    .line 76
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "MUTE_LIST_IDS"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->MUTE_LIST_IDS:Ltwitter4j/TwitterMethod;

    .line 77
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CREATE_MUTE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CREATE_MUTE:Ltwitter4j/TwitterMethod;

    .line 78
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_MUTE"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_MUTE:Ltwitter4j/TwitterMethod;

    .line 80
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SHOW_USER"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SHOW_USER:Ltwitter4j/TwitterMethod;

    .line 81
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "LOOKUP_USERS"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->LOOKUP_USERS:Ltwitter4j/TwitterMethod;

    .line 82
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SEARCH_USERS"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SEARCH_USERS:Ltwitter4j/TwitterMethod;

    .line 84
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CONTRIBUTORS"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CONTRIBUTORS:Ltwitter4j/TwitterMethod;

    .line 85
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CONTRIBUTEEES"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CONTRIBUTEEES:Ltwitter4j/TwitterMethod;

    .line 86
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "REMOVE_PROFILE_BANNER"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->REMOVE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

    .line 87
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_PROFILE_BANNER"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

    .line 89
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "RATE_LIMIT_STATUS"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->RATE_LIMIT_STATUS:Ltwitter4j/TwitterMethod;

    .line 92
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "USER_SUGGESTIONS"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->USER_SUGGESTIONS:Ltwitter4j/TwitterMethod;

    .line 93
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SUGGESTED_USER_CATEGORIES"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SUGGESTED_USER_CATEGORIES:Ltwitter4j/TwitterMethod;

    .line 94
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "MEMBER_SUGGESTIONS"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->MEMBER_SUGGESTIONS:Ltwitter4j/TwitterMethod;

    .line 97
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "FAVORITES"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->FAVORITES:Ltwitter4j/TwitterMethod;

    .line 98
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_FAVORITE"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_FAVORITE:Ltwitter4j/TwitterMethod;

    .line 99
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CREATE_FAVORITE"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CREATE_FAVORITE:Ltwitter4j/TwitterMethod;

    .line 102
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "USER_LISTS"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->USER_LISTS:Ltwitter4j/TwitterMethod;

    .line 103
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "USER_LIST_STATUSES"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->USER_LIST_STATUSES:Ltwitter4j/TwitterMethod;

    .line 104
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_LIST_MEMBER"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_LIST_MEMBER:Ltwitter4j/TwitterMethod;

    .line 105
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "USER_LIST_MEMBERSHIPS"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->USER_LIST_MEMBERSHIPS:Ltwitter4j/TwitterMethod;

    .line 106
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "LIST_SUBSCRIBERS"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->LIST_SUBSCRIBERS:Ltwitter4j/TwitterMethod;

    .line 107
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SUBSCRIBE_LIST"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

    .line 108
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CHECK_LIST_SUBSCRIPTION"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CHECK_LIST_SUBSCRIPTION:Ltwitter4j/TwitterMethod;

    .line 109
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UNSUBSCRIBE_LIST"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UNSUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

    .line 110
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CREATE_LIST_MEMBERS"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    .line 111
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CHECK_LIST_MEMBERSHIP"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CHECK_LIST_MEMBERSHIP:Ltwitter4j/TwitterMethod;

    .line 112
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "LIST_MEMBERS"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    .line 113
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CREATE_LIST_MEMBER"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBER:Ltwitter4j/TwitterMethod;

    .line 114
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_USER_LIST"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_USER_LIST:Ltwitter4j/TwitterMethod;

    .line 115
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "UPDATE_USER_LIST"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->UPDATE_USER_LIST:Ltwitter4j/TwitterMethod;

    .line 116
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CREATE_USER_LIST"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CREATE_USER_LIST:Ltwitter4j/TwitterMethod;

    .line 117
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SHOW_USER_LIST"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SHOW_USER_LIST:Ltwitter4j/TwitterMethod;

    .line 118
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "USER_LIST_SUBSCRIPTIONS"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->USER_LIST_SUBSCRIPTIONS:Ltwitter4j/TwitterMethod;

    .line 121
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SAVED_SEARCHES"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SAVED_SEARCHES:Ltwitter4j/TwitterMethod;

    .line 122
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SAVED_SEARCH"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    .line 123
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CREATE_SAVED_SEARCH"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CREATE_SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    .line 124
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "DESTROY_SAVED_SEARCH"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->DESTROY_SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    .line 127
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SEARCH_PLACES"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SEARCH_PLACES:Ltwitter4j/TwitterMethod;

    .line 128
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "SIMILAR_PLACES"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->SIMILAR_PLACES:Ltwitter4j/TwitterMethod;

    .line 129
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "REVERSE_GEO_CODE"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->REVERSE_GEO_CODE:Ltwitter4j/TwitterMethod;

    .line 130
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "GEO_DETAILS"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->GEO_DETAILS:Ltwitter4j/TwitterMethod;

    .line 133
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "PLACE_TRENDS"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->PLACE_TRENDS:Ltwitter4j/TwitterMethod;

    .line 134
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "AVAILABLE_TRENDS"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->AVAILABLE_TRENDS:Ltwitter4j/TwitterMethod;

    .line 135
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CLOSEST_TRENDS"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CLOSEST_TRENDS:Ltwitter4j/TwitterMethod;

    .line 138
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "REPORT_SPAM"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->REPORT_SPAM:Ltwitter4j/TwitterMethod;

    .line 141
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "OAUTH_REQUEST_TOKEN"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->OAUTH_REQUEST_TOKEN:Ltwitter4j/TwitterMethod;

    .line 142
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "OAUTH_ACCESS_TOKEN"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

    .line 145
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "TERMS_OF_SERVICE"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->TERMS_OF_SERVICE:Ltwitter4j/TwitterMethod;

    .line 146
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "CONFIGURATION"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->CONFIGURATION:Ltwitter4j/TwitterMethod;

    .line 147
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "LANGUAGES"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->LANGUAGES:Ltwitter4j/TwitterMethod;

    .line 148
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "PRIVACY_POLICY"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->PRIVACY_POLICY:Ltwitter4j/TwitterMethod;

    .line 151
    new-instance v0, Ltwitter4j/TwitterMethod;

    const-string v1, "RELATED_RESULTS"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Ltwitter4j/TwitterMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltwitter4j/TwitterMethod;->RELATED_RESULTS:Ltwitter4j/TwitterMethod;

    .line 23
    const/16 v0, 0x5d

    new-array v0, v0, [Ltwitter4j/TwitterMethod;

    sget-object v1, Ltwitter4j/TwitterMethod;->MENTIONS_TIMELINE:Ltwitter4j/TwitterMethod;

    aput-object v1, v0, v3

    sget-object v1, Ltwitter4j/TwitterMethod;->USER_TIMELINE:Ltwitter4j/TwitterMethod;

    aput-object v1, v0, v4

    sget-object v1, Ltwitter4j/TwitterMethod;->HOME_TIMELINE:Ltwitter4j/TwitterMethod;

    aput-object v1, v0, v5

    sget-object v1, Ltwitter4j/TwitterMethod;->RETWEETS_OF_ME:Ltwitter4j/TwitterMethod;

    aput-object v1, v0, v6

    sget-object v1, Ltwitter4j/TwitterMethod;->RETWEETS:Ltwitter4j/TwitterMethod;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_STATUS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_STATUS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_STATUS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ltwitter4j/TwitterMethod;->RETWEET_STATUS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ltwitter4j/TwitterMethod;->OEMBED:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Ltwitter4j/TwitterMethod;->LOOKUP:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Ltwitter4j/TwitterMethod;->SEARCH:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Ltwitter4j/TwitterMethod;->DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Ltwitter4j/TwitterMethod;->SENT_DIRECT_MESSAGES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Ltwitter4j/TwitterMethod;->DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Ltwitter4j/TwitterMethod;->SEND_DIRECT_MESSAGE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Ltwitter4j/TwitterMethod;->FRIENDS_IDS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Ltwitter4j/TwitterMethod;->FOLLOWERS_IDS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Ltwitter4j/TwitterMethod;->LOOKUP_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Ltwitter4j/TwitterMethod;->INCOMING_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Ltwitter4j/TwitterMethod;->OUTGOING_FRIENDSHIPS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_FRIENDSHIP:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Ltwitter4j/TwitterMethod;->FRIENDS_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Ltwitter4j/TwitterMethod;->FOLLOWERS_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Ltwitter4j/TwitterMethod;->ACCOUNT_SETTINGS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Ltwitter4j/TwitterMethod;->VERIFY_CREDENTIALS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_ACCOUNT_SETTINGS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_BACKGROUND_IMAGE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_COLORS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_IMAGE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Ltwitter4j/TwitterMethod;->BLOCK_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Ltwitter4j/TwitterMethod;->BLOCK_LIST_IDS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_BLOCK:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_BLOCK:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Ltwitter4j/TwitterMethod;->MUTE_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Ltwitter4j/TwitterMethod;->MUTE_LIST_IDS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_MUTE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_MUTE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_USER:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Ltwitter4j/TwitterMethod;->LOOKUP_USERS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Ltwitter4j/TwitterMethod;->SEARCH_USERS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Ltwitter4j/TwitterMethod;->CONTRIBUTORS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Ltwitter4j/TwitterMethod;->CONTRIBUTEEES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Ltwitter4j/TwitterMethod;->REMOVE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_PROFILE_BANNER:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Ltwitter4j/TwitterMethod;->RATE_LIMIT_STATUS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_SUGGESTIONS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Ltwitter4j/TwitterMethod;->SUGGESTED_USER_CATEGORIES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Ltwitter4j/TwitterMethod;->MEMBER_SUGGESTIONS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Ltwitter4j/TwitterMethod;->FAVORITES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_FAVORITE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_FAVORITE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LISTS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LIST_STATUSES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_LIST_MEMBER:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LIST_MEMBERSHIPS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Ltwitter4j/TwitterMethod;->LIST_SUBSCRIBERS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Ltwitter4j/TwitterMethod;->SUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Ltwitter4j/TwitterMethod;->CHECK_LIST_SUBSCRIPTION:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Ltwitter4j/TwitterMethod;->UNSUBSCRIBE_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Ltwitter4j/TwitterMethod;->CHECK_LIST_MEMBERSHIP:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Ltwitter4j/TwitterMethod;->LIST_MEMBERS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_LIST_MEMBER:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_USER_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Ltwitter4j/TwitterMethod;->UPDATE_USER_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_USER_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Ltwitter4j/TwitterMethod;->SHOW_USER_LIST:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Ltwitter4j/TwitterMethod;->USER_LIST_SUBSCRIPTIONS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Ltwitter4j/TwitterMethod;->SAVED_SEARCHES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Ltwitter4j/TwitterMethod;->SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Ltwitter4j/TwitterMethod;->CREATE_SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Ltwitter4j/TwitterMethod;->DESTROY_SAVED_SEARCH:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Ltwitter4j/TwitterMethod;->SEARCH_PLACES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Ltwitter4j/TwitterMethod;->SIMILAR_PLACES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Ltwitter4j/TwitterMethod;->REVERSE_GEO_CODE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Ltwitter4j/TwitterMethod;->GEO_DETAILS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Ltwitter4j/TwitterMethod;->PLACE_TRENDS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Ltwitter4j/TwitterMethod;->AVAILABLE_TRENDS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Ltwitter4j/TwitterMethod;->CLOSEST_TRENDS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Ltwitter4j/TwitterMethod;->REPORT_SPAM:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_REQUEST_TOKEN:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Ltwitter4j/TwitterMethod;->OAUTH_ACCESS_TOKEN:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Ltwitter4j/TwitterMethod;->TERMS_OF_SERVICE:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Ltwitter4j/TwitterMethod;->CONFIGURATION:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Ltwitter4j/TwitterMethod;->LANGUAGES:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Ltwitter4j/TwitterMethod;->PRIVACY_POLICY:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Ltwitter4j/TwitterMethod;->RELATED_RESULTS:Ltwitter4j/TwitterMethod;

    aput-object v2, v0, v1

    sput-object v0, Ltwitter4j/TwitterMethod;->$VALUES:[Ltwitter4j/TwitterMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltwitter4j/TwitterMethod;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Ltwitter4j/TwitterMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltwitter4j/TwitterMethod;

    return-object v0
.end method

.method public static values()[Ltwitter4j/TwitterMethod;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Ltwitter4j/TwitterMethod;->$VALUES:[Ltwitter4j/TwitterMethod;

    invoke-virtual {v0}, [Ltwitter4j/TwitterMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltwitter4j/TwitterMethod;

    return-object v0
.end method
