.class public final Ltwitter4j/AsyncTwitterFactory;
.super Ljava/lang/Object;
.source "AsyncTwitterFactory.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final SINGLETON:Ltwitter4j/AsyncTwitter;

.field private static final serialVersionUID:J = 0x135bd8d51afbbedeL


# instance fields
.field private final conf:Ltwitter4j/conf/Configuration;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Ltwitter4j/AsyncTwitterImpl;

    invoke-static {}, Ltwitter4j/conf/ConfigurationContext;->getInstance()Ltwitter4j/conf/Configuration;

    move-result-object v1

    sget-object v2, Ltwitter4j/TwitterFactory;->DEFAULT_AUTHORIZATION:Ltwitter4j/auth/Authorization;

    invoke-direct {v0, v1, v2}, Ltwitter4j/AsyncTwitterImpl;-><init>(Ltwitter4j/conf/Configuration;Ltwitter4j/auth/Authorization;)V

    sput-object v0, Ltwitter4j/AsyncTwitterFactory;->SINGLETON:Ltwitter4j/AsyncTwitter;

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Ltwitter4j/conf/ConfigurationContext;->getInstance()Ltwitter4j/conf/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Ltwitter4j/AsyncTwitterFactory;-><init>(Ltwitter4j/conf/Configuration;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "configTreePath"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {p1}, Ltwitter4j/conf/ConfigurationContext;->getInstance(Ljava/lang/String;)Ltwitter4j/conf/Configuration;

    move-result-object v0

    iput-object v0, p0, Ltwitter4j/AsyncTwitterFactory;->conf:Ltwitter4j/conf/Configuration;

    .line 72
    return-void
.end method

.method public constructor <init>(Ltwitter4j/conf/Configuration;)V
    .locals 2
    .param p1, "conf"    # Ltwitter4j/conf/Configuration;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    if-nez p1, :cond_0

    .line 59
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "configuration cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput-object p1, p0, Ltwitter4j/AsyncTwitterFactory;->conf:Ltwitter4j/conf/Configuration;

    .line 62
    return-void
.end method

.method public static getSingleton()Ltwitter4j/AsyncTwitter;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Ltwitter4j/AsyncTwitterFactory;->SINGLETON:Ltwitter4j/AsyncTwitter;

    return-object v0
.end method


# virtual methods
.method public getInstance()Ltwitter4j/AsyncTwitter;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Ltwitter4j/AsyncTwitterFactory;->conf:Ltwitter4j/conf/Configuration;

    invoke-static {v0}, Ltwitter4j/auth/AuthorizationFactory;->getInstance(Ltwitter4j/conf/Configuration;)Ltwitter4j/auth/Authorization;

    move-result-object v0

    invoke-virtual {p0, v0}, Ltwitter4j/AsyncTwitterFactory;->getInstance(Ltwitter4j/auth/Authorization;)Ltwitter4j/AsyncTwitter;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Ltwitter4j/Twitter;)Ltwitter4j/AsyncTwitter;
    .locals 3
    .param p1, "twitter"    # Ltwitter4j/Twitter;

    .prologue
    .line 118
    new-instance v0, Ltwitter4j/AsyncTwitterImpl;

    invoke-interface {p1}, Ltwitter4j/Twitter;->getConfiguration()Ltwitter4j/conf/Configuration;

    move-result-object v1

    invoke-interface {p1}, Ltwitter4j/Twitter;->getAuthorization()Ltwitter4j/auth/Authorization;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ltwitter4j/AsyncTwitterImpl;-><init>(Ltwitter4j/conf/Configuration;Ltwitter4j/auth/Authorization;)V

    return-object v0
.end method

.method public getInstance(Ltwitter4j/auth/AccessToken;)Ltwitter4j/AsyncTwitter;
    .locals 5
    .param p1, "accessToken"    # Ltwitter4j/auth/AccessToken;

    .prologue
    .line 92
    iget-object v3, p0, Ltwitter4j/AsyncTwitterFactory;->conf:Ltwitter4j/conf/Configuration;

    invoke-interface {v3}, Ltwitter4j/conf/Configuration;->getOAuthConsumerKey()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "consumerKey":Ljava/lang/String;
    iget-object v3, p0, Ltwitter4j/AsyncTwitterFactory;->conf:Ltwitter4j/conf/Configuration;

    invoke-interface {v3}, Ltwitter4j/conf/Configuration;->getOAuthConsumerSecret()Ljava/lang/String;

    move-result-object v1

    .line 94
    .local v1, "consumerSecret":Ljava/lang/String;
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 95
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Consumer key and Consumer secret not supplied."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 97
    :cond_0
    new-instance v2, Ltwitter4j/auth/OAuthAuthorization;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterFactory;->conf:Ltwitter4j/conf/Configuration;

    invoke-direct {v2, v3}, Ltwitter4j/auth/OAuthAuthorization;-><init>(Ltwitter4j/conf/Configuration;)V

    .line 98
    .local v2, "oauth":Ltwitter4j/auth/OAuthAuthorization;
    invoke-virtual {v2, v0, v1}, Ltwitter4j/auth/OAuthAuthorization;->setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v2, p1}, Ltwitter4j/auth/OAuthAuthorization;->setOAuthAccessToken(Ltwitter4j/auth/AccessToken;)V

    .line 100
    new-instance v3, Ltwitter4j/AsyncTwitterImpl;

    iget-object v4, p0, Ltwitter4j/AsyncTwitterFactory;->conf:Ltwitter4j/conf/Configuration;

    invoke-direct {v3, v4, v2}, Ltwitter4j/AsyncTwitterImpl;-><init>(Ltwitter4j/conf/Configuration;Ltwitter4j/auth/Authorization;)V

    return-object v3
.end method

.method public getInstance(Ltwitter4j/auth/Authorization;)Ltwitter4j/AsyncTwitter;
    .locals 2
    .param p1, "auth"    # Ltwitter4j/auth/Authorization;

    .prologue
    .line 108
    new-instance v0, Ltwitter4j/AsyncTwitterImpl;

    iget-object v1, p0, Ltwitter4j/AsyncTwitterFactory;->conf:Ltwitter4j/conf/Configuration;

    invoke-direct {v0, v1, p1}, Ltwitter4j/AsyncTwitterImpl;-><init>(Ltwitter4j/conf/Configuration;Ltwitter4j/auth/Authorization;)V

    return-object v0
.end method
