.class Ltwitter4j/AsyncTwitterImpl$128;
.super Ltwitter4j/AsyncTwitterImpl$AsyncTask;
.source "AsyncTwitterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltwitter4j/AsyncTwitterImpl;->updateUserList(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ltwitter4j/AsyncTwitterImpl;

.field final synthetic val$isPublicList:Z

.field final synthetic val$newDescription:Ljava/lang/String;

.field final synthetic val$newListName:Ljava/lang/String;

.field final synthetic val$ownerId:J

.field final synthetic val$slug:Ljava/lang/String;


# direct methods
.method constructor <init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Ltwitter4j/AsyncTwitterImpl;
    .param p2, "method"    # Ltwitter4j/TwitterMethod;

    .prologue
    .line 2159
    .local p3, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iput-object p1, p0, Ltwitter4j/AsyncTwitterImpl$128;->this$0:Ltwitter4j/AsyncTwitterImpl;

    iput-wide p4, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$ownerId:J

    iput-object p6, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$slug:Ljava/lang/String;

    iput-object p7, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$newListName:Ljava/lang/String;

    iput-boolean p8, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$isPublicList:Z

    iput-object p9, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$newDescription:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Ltwitter4j/AsyncTwitterImpl$AsyncTask;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 2162
    .local p1, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iget-object v1, p0, Ltwitter4j/AsyncTwitterImpl$128;->this$0:Ltwitter4j/AsyncTwitterImpl;

    invoke-static {v1}, Ltwitter4j/AsyncTwitterImpl;->access$000(Ltwitter4j/AsyncTwitterImpl;)Ltwitter4j/Twitter;

    move-result-object v1

    iget-wide v2, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$ownerId:J

    iget-object v4, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$slug:Ljava/lang/String;

    iget-object v5, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$newListName:Ljava/lang/String;

    iget-boolean v6, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$isPublicList:Z

    iget-object v7, p0, Ltwitter4j/AsyncTwitterImpl$128;->val$newDescription:Ljava/lang/String;

    invoke-interface/range {v1 .. v7}, Ltwitter4j/Twitter;->updateUserList(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ltwitter4j/UserList;

    move-result-object v0

    .line 2163
    .local v0, "list":Ltwitter4j/UserList;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ltwitter4j/TwitterListener;

    .line 2165
    .local v8, "listener":Ltwitter4j/TwitterListener;
    :try_start_0
    invoke-interface {v8, v0}, Ltwitter4j/TwitterListener;->updatedUserList(Ltwitter4j/UserList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2166
    :catch_0
    move-exception v2

    goto :goto_0

    .line 2169
    .end local v8    # "listener":Ltwitter4j/TwitterListener;
    :cond_0
    return-void
.end method
