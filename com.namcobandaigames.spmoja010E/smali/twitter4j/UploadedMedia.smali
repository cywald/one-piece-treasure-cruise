.class public final Ltwitter4j/UploadedMedia;
.super Ljava/lang/Object;
.source "UploadedMedia.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4ad81d1659e17caeL


# instance fields
.field private imageHeight:I

.field private imageType:Ljava/lang/String;

.field private imageWidth:I

.field private mediaId:J

.field private size:J


# direct methods
.method constructor <init>(Ltwitter4j/JSONObject;)V
    .locals 0
    .param p1, "json"    # Ltwitter4j/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-direct {p0, p1}, Ltwitter4j/UploadedMedia;->init(Ltwitter4j/JSONObject;)V

    .line 36
    return-void
.end method

.method private init(Ltwitter4j/JSONObject;)V
    .locals 4
    .param p1, "json"    # Ltwitter4j/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 59
    const-string v2, "media_id"

    invoke-static {v2, p1}, Ltwitter4j/ParseUtil;->getLong(Ljava/lang/String;Ltwitter4j/JSONObject;)J

    move-result-wide v2

    iput-wide v2, p0, Ltwitter4j/UploadedMedia;->mediaId:J

    .line 60
    const-string v2, "size"

    invoke-static {v2, p1}, Ltwitter4j/ParseUtil;->getLong(Ljava/lang/String;Ltwitter4j/JSONObject;)J

    move-result-wide v2

    iput-wide v2, p0, Ltwitter4j/UploadedMedia;->size:J

    .line 62
    :try_start_0
    const-string v2, "image"

    invoke-virtual {p1, v2}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 63
    const-string v2, "image"

    invoke-virtual {p1, v2}, Ltwitter4j/JSONObject;->getJSONObject(Ljava/lang/String;)Ltwitter4j/JSONObject;

    move-result-object v0

    .line 64
    .local v0, "image":Ltwitter4j/JSONObject;
    const-string v2, "w"

    invoke-static {v2, v0}, Ltwitter4j/ParseUtil;->getInt(Ljava/lang/String;Ltwitter4j/JSONObject;)I

    move-result v2

    iput v2, p0, Ltwitter4j/UploadedMedia;->imageWidth:I

    .line 65
    const-string v2, "h"

    invoke-static {v2, v0}, Ltwitter4j/ParseUtil;->getInt(Ljava/lang/String;Ltwitter4j/JSONObject;)I

    move-result v2

    iput v2, p0, Ltwitter4j/UploadedMedia;->imageHeight:I

    .line 66
    const-string v2, "image_type"

    invoke-static {v2, v0}, Ltwitter4j/ParseUtil;->getUnescapedString(Ljava/lang/String;Ltwitter4j/JSONObject;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ltwitter4j/UploadedMedia;->imageType:Ljava/lang/String;
    :try_end_0
    .catch Ltwitter4j/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v0    # "image":Ltwitter4j/JSONObject;
    :cond_0
    return-void

    .line 68
    :catch_0
    move-exception v1

    .line 69
    .local v1, "jsone":Ltwitter4j/JSONException;
    new-instance v2, Ltwitter4j/TwitterException;

    invoke-direct {v2, v1}, Ltwitter4j/TwitterException;-><init>(Ljava/lang/Exception;)V

    throw v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p0, p1, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 78
    check-cast v0, Ltwitter4j/UploadedMedia;

    .line 80
    .local v0, "that":Ltwitter4j/UploadedMedia;
    iget v3, p0, Ltwitter4j/UploadedMedia;->imageWidth:I

    iget v4, v0, Ltwitter4j/UploadedMedia;->imageWidth:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 81
    :cond_4
    iget v3, p0, Ltwitter4j/UploadedMedia;->imageHeight:I

    iget v4, v0, Ltwitter4j/UploadedMedia;->imageHeight:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 82
    :cond_5
    iget-object v3, p0, Ltwitter4j/UploadedMedia;->imageType:Ljava/lang/String;

    iget-object v4, v0, Ltwitter4j/UploadedMedia;->imageType:Ljava/lang/String;

    if-eq v3, v4, :cond_6

    move v1, v2

    goto :goto_0

    .line 83
    :cond_6
    iget-wide v4, p0, Ltwitter4j/UploadedMedia;->mediaId:J

    iget-wide v6, v0, Ltwitter4j/UploadedMedia;->mediaId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    .line 84
    :cond_7
    iget-wide v4, p0, Ltwitter4j/UploadedMedia;->size:J

    iget-wide v6, v0, Ltwitter4j/UploadedMedia;->size:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getImageHeight()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Ltwitter4j/UploadedMedia;->imageHeight:I

    return v0
.end method

.method public getImageType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ltwitter4j/UploadedMedia;->imageType:Ljava/lang/String;

    return-object v0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Ltwitter4j/UploadedMedia;->imageWidth:I

    return v0
.end method

.method public getMediaId()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Ltwitter4j/UploadedMedia;->mediaId:J

    return-wide v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Ltwitter4j/UploadedMedia;->size:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 91
    iget-wide v2, p0, Ltwitter4j/UploadedMedia;->mediaId:J

    iget-wide v4, p0, Ltwitter4j/UploadedMedia;->mediaId:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 92
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Ltwitter4j/UploadedMedia;->imageWidth:I

    add-int v0, v1, v2

    .line 93
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Ltwitter4j/UploadedMedia;->imageHeight:I

    add-int v0, v1, v2

    .line 94
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Ltwitter4j/UploadedMedia;->imageType:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ltwitter4j/UploadedMedia;->imageType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v2, v1

    .line 95
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Ltwitter4j/UploadedMedia;->size:J

    iget-wide v4, p0, Ltwitter4j/UploadedMedia;->size:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 96
    return v0

    .line 94
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UploadedMedia{mediaId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltwitter4j/UploadedMedia;->mediaId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltwitter4j/UploadedMedia;->imageWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltwitter4j/UploadedMedia;->imageHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/UploadedMedia;->imageType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltwitter4j/UploadedMedia;->size:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
