.class public Ltwitter4j/TwitterAdapter;
.super Ljava/lang/Object;
.source "TwitterAdapter.java"

# interfaces
.implements Ltwitter4j/TwitterListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkedUserListMembership(Ltwitter4j/User;)V
    .locals 0
    .param p1, "users"    # Ltwitter4j/User;

    .prologue
    .line 307
    return-void
.end method

.method public checkedUserListSubscription(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 295
    return-void
.end method

.method public createdBlock(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 192
    return-void
.end method

.method public createdFavorite(Ltwitter4j/Status;)V
    .locals 0
    .param p1, "status"    # Ltwitter4j/Status;

    .prologue
    .line 262
    return-void
.end method

.method public createdFriendship(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 130
    return-void
.end method

.method public createdMute(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 236
    return-void
.end method

.method public createdSavedSearch(Ltwitter4j/SavedSearch;)V
    .locals 0
    .param p1, "savedSearch"    # Ltwitter4j/SavedSearch;

    .prologue
    .line 348
    return-void
.end method

.method public createdUserList(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 323
    return-void
.end method

.method public createdUserListMember(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 311
    return-void
.end method

.method public createdUserListMembers(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 303
    return-void
.end method

.method public destroyedBlock(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 196
    return-void
.end method

.method public destroyedDirectMessage(Ltwitter4j/DirectMessage;)V
    .locals 0
    .param p1, "message"    # Ltwitter4j/DirectMessage;

    .prologue
    .line 101
    return-void
.end method

.method public destroyedFavorite(Ltwitter4j/Status;)V
    .locals 0
    .param p1, "status"    # Ltwitter4j/Status;

    .prologue
    .line 266
    return-void
.end method

.method public destroyedFriendship(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 134
    return-void
.end method

.method public destroyedMute(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 240
    return-void
.end method

.method public destroyedSavedSearch(Ltwitter4j/SavedSearch;)V
    .locals 0
    .param p1, "savedSearch"    # Ltwitter4j/SavedSearch;

    .prologue
    .line 352
    return-void
.end method

.method public destroyedStatus(Ltwitter4j/Status;)V
    .locals 0
    .param p1, "destroyedStatus"    # Ltwitter4j/Status;

    .prologue
    .line 63
    return-void
.end method

.method public destroyedUserList(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 315
    return-void
.end method

.method public destroyedUserListMember(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 279
    return-void
.end method

.method public gotAPIConfiguration(Ltwitter4j/TwitterAPIConfiguration;)V
    .locals 0
    .param p1, "conf"    # Ltwitter4j/TwitterAPIConfiguration;

    .prologue
    .line 406
    return-void
.end method

.method public gotAccountSettings(Ltwitter4j/AccountSettings;)V
    .locals 0
    .param p1, "settings"    # Ltwitter4j/AccountSettings;

    .prologue
    .line 155
    return-void
.end method

.method public gotAvailableTrends(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 378
    .local p1, "locations":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Location;>;"
    return-void
.end method

.method public gotBlockIDs(Ltwitter4j/IDs;)V
    .locals 0
    .param p1, "blockingUsersIDs"    # Ltwitter4j/IDs;

    .prologue
    .line 188
    return-void
.end method

.method public gotBlocksList(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "blockingUsers":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotClosestTrends(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 382
    .local p1, "locations":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Location;>;"
    return-void
.end method

.method public gotContributees(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "users":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotContributors(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p1, "users":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotDirectMessage(Ltwitter4j/DirectMessage;)V
    .locals 0
    .param p1, "message"    # Ltwitter4j/DirectMessage;

    .prologue
    .line 97
    return-void
.end method

.method public gotDirectMessages(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/DirectMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "messages":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/DirectMessage;>;"
    return-void
.end method

.method public gotFavorites(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 258
    .local p1, "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    return-void
.end method

.method public gotFollowersIDs(Ltwitter4j/IDs;)V
    .locals 0
    .param p1, "ids"    # Ltwitter4j/IDs;

    .prologue
    .line 114
    return-void
.end method

.method public gotFollowersList(Ltwitter4j/PagableResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "users":Ltwitter4j/PagableResponseList;, "Ltwitter4j/PagableResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotFriendsIDs(Ltwitter4j/IDs;)V
    .locals 0
    .param p1, "ids"    # Ltwitter4j/IDs;

    .prologue
    .line 110
    return-void
.end method

.method public gotFriendsList(Ltwitter4j/PagableResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "users":Ltwitter4j/PagableResponseList;, "Ltwitter4j/PagableResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotGeoDetails(Ltwitter4j/Place;)V
    .locals 0
    .param p1, "place"    # Ltwitter4j/Place;

    .prologue
    .line 357
    return-void
.end method

.method public gotHomeTimeline(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    return-void
.end method

.method public gotIncomingFriendships(Ltwitter4j/IDs;)V
    .locals 0
    .param p1, "ids"    # Ltwitter4j/IDs;

    .prologue
    .line 122
    return-void
.end method

.method public gotLanguages(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/api/HelpResources$Language;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 410
    .local p1, "languages":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/api/HelpResources$Language;>;"
    return-void
.end method

.method public gotMemberSuggestions(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "users":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotMentions(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    return-void
.end method

.method public gotMuteIDs(Ltwitter4j/IDs;)V
    .locals 0
    .param p1, "blockingUsersIDs"    # Ltwitter4j/IDs;

    .prologue
    .line 232
    return-void
.end method

.method public gotMutesList(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228
    .local p1, "blockingUsers":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotOAuth2Token(Ltwitter4j/auth/OAuth2Token;)V
    .locals 0
    .param p1, "token"    # Ltwitter4j/auth/OAuth2Token;

    .prologue
    .line 401
    return-void
.end method

.method public gotOAuthAccessToken(Ltwitter4j/auth/AccessToken;)V
    .locals 0
    .param p1, "token"    # Ltwitter4j/auth/AccessToken;

    .prologue
    .line 396
    return-void
.end method

.method public gotOAuthRequestToken(Ltwitter4j/auth/RequestToken;)V
    .locals 0
    .param p1, "token"    # Ltwitter4j/auth/RequestToken;

    .prologue
    .line 392
    return-void
.end method

.method public gotOEmbed(Ltwitter4j/OEmbed;)V
    .locals 0
    .param p1, "oembed"    # Ltwitter4j/OEmbed;

    .prologue
    .line 75
    return-void
.end method

.method public gotOutgoingFriendships(Ltwitter4j/IDs;)V
    .locals 0
    .param p1, "ids"    # Ltwitter4j/IDs;

    .prologue
    .line 126
    return-void
.end method

.method public gotPlaceTrends(Ltwitter4j/Trends;)V
    .locals 0
    .param p1, "trends"    # Ltwitter4j/Trends;

    .prologue
    .line 374
    return-void
.end method

.method public gotPrivacyPolicy(Ljava/lang/String;)V
    .locals 0
    .param p1, "privacyPolicy"    # Ljava/lang/String;

    .prologue
    .line 414
    return-void
.end method

.method public gotRateLimitStatus(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltwitter4j/RateLimitStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422
    .local p1, "rateLimitStatus":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltwitter4j/RateLimitStatus;>;"
    return-void
.end method

.method public gotRetweets(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "retweets":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    return-void
.end method

.method public gotRetweetsOfMe(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    return-void
.end method

.method public gotReverseGeoCode(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Place;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 361
    .local p1, "places":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Place;>;"
    return-void
.end method

.method public gotSavedSearch(Ltwitter4j/SavedSearch;)V
    .locals 0
    .param p1, "savedSearch"    # Ltwitter4j/SavedSearch;

    .prologue
    .line 344
    return-void
.end method

.method public gotSavedSearches(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/SavedSearch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 340
    .local p1, "savedSearches":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/SavedSearch;>;"
    return-void
.end method

.method public gotSentDirectMessages(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/DirectMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "messages":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/DirectMessage;>;"
    return-void
.end method

.method public gotShowFriendship(Ltwitter4j/Relationship;)V
    .locals 0
    .param p1, "relationship"    # Ltwitter4j/Relationship;

    .prologue
    .line 142
    return-void
.end method

.method public gotShowStatus(Ltwitter4j/Status;)V
    .locals 0
    .param p1, "status"    # Ltwitter4j/Status;

    .prologue
    .line 59
    return-void
.end method

.method public gotShowUserList(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 327
    return-void
.end method

.method public gotSimilarPlaces(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Place;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 369
    .local p1, "places":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Place;>;"
    return-void
.end method

.method public gotSuggestedUserCategories(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Category;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 249
    .local p1, "category":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Category;>;"
    return-void
.end method

.method public gotTermsOfService(Ljava/lang/String;)V
    .locals 0
    .param p1, "tof"    # Ljava/lang/String;

    .prologue
    .line 418
    return-void
.end method

.method public gotUserDetail(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 204
    return-void
.end method

.method public gotUserListMembers(Ltwitter4j/PagableResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    .local p1, "users":Ltwitter4j/PagableResponseList;, "Ltwitter4j/PagableResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotUserListMemberships(Ltwitter4j/PagableResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/UserList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283
    .local p1, "userLists":Ltwitter4j/PagableResponseList;, "Ltwitter4j/PagableResponseList<Ltwitter4j/UserList;>;"
    return-void
.end method

.method public gotUserListStatuses(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 275
    .local p1, "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    return-void
.end method

.method public gotUserListSubscribers(Ltwitter4j/PagableResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 287
    .local p1, "users":Ltwitter4j/PagableResponseList;, "Ltwitter4j/PagableResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotUserListSubscriptions(Ltwitter4j/PagableResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/PagableResponseList",
            "<",
            "Ltwitter4j/UserList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 331
    .local p1, "userLists":Ltwitter4j/PagableResponseList;, "Ltwitter4j/PagableResponseList<Ltwitter4j/UserList;>;"
    return-void
.end method

.method public gotUserLists(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/UserList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 271
    .local p1, "userLists":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/UserList;>;"
    return-void
.end method

.method public gotUserSuggestions(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245
    .local p1, "users":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public gotUserTimeline(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    return-void
.end method

.method public lookedUpFriendships(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Friendship;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "friendships":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Friendship;>;"
    return-void
.end method

.method public lookedup(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    return-void
.end method

.method public lookedupUsers(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, "users":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public onException(Ltwitter4j/TwitterException;Ltwitter4j/TwitterMethod;)V
    .locals 0
    .param p1, "te"    # Ltwitter4j/TwitterException;
    .param p2, "method"    # Ltwitter4j/TwitterMethod;

    .prologue
    .line 426
    return-void
.end method

.method public removedProfileBanner()V
    .locals 0

    .prologue
    .line 220
    return-void
.end method

.method public reportedSpam(Ltwitter4j/User;)V
    .locals 0
    .param p1, "reportedSpammer"    # Ltwitter4j/User;

    .prologue
    .line 387
    return-void
.end method

.method public retweetedStatus(Ltwitter4j/Status;)V
    .locals 0
    .param p1, "retweetedStatus"    # Ltwitter4j/Status;

    .prologue
    .line 71
    return-void
.end method

.method public searched(Ltwitter4j/QueryResult;)V
    .locals 0
    .param p1, "queryResult"    # Ltwitter4j/QueryResult;

    .prologue
    .line 84
    return-void
.end method

.method public searchedPlaces(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Place;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 365
    .local p1, "places":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Place;>;"
    return-void
.end method

.method public searchedUser(Ltwitter4j/ResponseList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "userList":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/User;>;"
    return-void
.end method

.method public sentDirectMessage(Ltwitter4j/DirectMessage;)V
    .locals 0
    .param p1, "message"    # Ltwitter4j/DirectMessage;

    .prologue
    .line 105
    return-void
.end method

.method public subscribedUserList(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 291
    return-void
.end method

.method public unsubscribedUserList(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 299
    return-void
.end method

.method public updatedAccountSettings(Ltwitter4j/AccountSettings;)V
    .locals 0
    .param p1, "settings"    # Ltwitter4j/AccountSettings;

    .prologue
    .line 163
    return-void
.end method

.method public updatedFriendship(Ltwitter4j/Relationship;)V
    .locals 0
    .param p1, "relationship"    # Ltwitter4j/Relationship;

    .prologue
    .line 138
    return-void
.end method

.method public updatedProfile(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 168
    return-void
.end method

.method public updatedProfileBackgroundImage(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 172
    return-void
.end method

.method public updatedProfileBanner()V
    .locals 0

    .prologue
    .line 224
    return-void
.end method

.method public updatedProfileColors(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 176
    return-void
.end method

.method public updatedProfileImage(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 180
    return-void
.end method

.method public updatedStatus(Ltwitter4j/Status;)V
    .locals 0
    .param p1, "status"    # Ltwitter4j/Status;

    .prologue
    .line 67
    return-void
.end method

.method public updatedUserList(Ltwitter4j/UserList;)V
    .locals 0
    .param p1, "userList"    # Ltwitter4j/UserList;

    .prologue
    .line 319
    return-void
.end method

.method public verifiedCredentials(Ltwitter4j/User;)V
    .locals 0
    .param p1, "user"    # Ltwitter4j/User;

    .prologue
    .line 159
    return-void
.end method
