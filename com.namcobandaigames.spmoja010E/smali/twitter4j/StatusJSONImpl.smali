.class final Ltwitter4j/StatusJSONImpl;
.super Ltwitter4j/TwitterResponseImpl;
.source "StatusJSONImpl.java"

# interfaces
.implements Ltwitter4j/Status;
.implements Ljava/io/Serializable;


# static fields
.field private static final logger:Ltwitter4j/Logger;

.field private static final serialVersionUID:J = -0x59aac71ba3de09f1L


# instance fields
.field private contributorsIDs:[J

.field private createdAt:Ljava/util/Date;

.field private currentUserRetweetId:J

.field private extendedMediaEntities:[Ltwitter4j/MediaEntity;

.field private favoriteCount:I

.field private geoLocation:Ltwitter4j/GeoLocation;

.field private hashtagEntities:[Ltwitter4j/HashtagEntity;

.field private id:J

.field private inReplyToScreenName:Ljava/lang/String;

.field private inReplyToStatusId:J

.field private inReplyToUserId:J

.field private isFavorited:Z

.field private isPossiblySensitive:Z

.field private isRetweeted:Z

.field private isTruncated:Z

.field private lang:Ljava/lang/String;

.field private mediaEntities:[Ltwitter4j/MediaEntity;

.field private place:Ltwitter4j/Place;

.field private retweetCount:J

.field private retweetedStatus:Ltwitter4j/Status;

.field private scopes:Ltwitter4j/Scopes;

.field private source:Ljava/lang/String;

.field private symbolEntities:[Ltwitter4j/SymbolEntity;

.field private text:Ljava/lang/String;

.field private urlEntities:[Ltwitter4j/URLEntity;

.field private user:Ltwitter4j/User;

.field private userMentionEntities:[Ltwitter4j/UserMentionEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Ltwitter4j/StatusJSONImpl;

    invoke-static {v0}, Ltwitter4j/Logger;->getLogger(Ljava/lang/Class;)Ltwitter4j/Logger;

    move-result-object v0

    sput-object v0, Ltwitter4j/StatusJSONImpl;->logger:Ltwitter4j/Logger;

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 90
    invoke-direct {p0}, Ltwitter4j/TwitterResponseImpl;-><init>()V

    .line 46
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->geoLocation:Ltwitter4j/GeoLocation;

    .line 47
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->place:Ltwitter4j/Place;

    .line 62
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ltwitter4j/StatusJSONImpl;->currentUserRetweetId:J

    .line 64
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->user:Ltwitter4j/User;

    .line 92
    return-void
.end method

.method constructor <init>(Ltwitter4j/HttpResponse;Ltwitter4j/conf/Configuration;)V
    .locals 4
    .param p1, "res"    # Ltwitter4j/HttpResponse;
    .param p2, "conf"    # Ltwitter4j/conf/Configuration;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0, p1}, Ltwitter4j/TwitterResponseImpl;-><init>(Ltwitter4j/HttpResponse;)V

    .line 46
    iput-object v1, p0, Ltwitter4j/StatusJSONImpl;->geoLocation:Ltwitter4j/GeoLocation;

    .line 47
    iput-object v1, p0, Ltwitter4j/StatusJSONImpl;->place:Ltwitter4j/Place;

    .line 62
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Ltwitter4j/StatusJSONImpl;->currentUserRetweetId:J

    .line 64
    iput-object v1, p0, Ltwitter4j/StatusJSONImpl;->user:Ltwitter4j/User;

    .line 68
    invoke-virtual {p1}, Ltwitter4j/HttpResponse;->asJSONObject()Ltwitter4j/JSONObject;

    move-result-object v0

    .line 69
    .local v0, "json":Ltwitter4j/JSONObject;
    invoke-direct {p0, v0}, Ltwitter4j/StatusJSONImpl;->init(Ltwitter4j/JSONObject;)V

    .line 70
    invoke-interface {p2}, Ltwitter4j/conf/Configuration;->isJSONStoreEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    invoke-static {}, Ltwitter4j/TwitterObjectFactory;->clearThreadLocalMap()V

    .line 72
    invoke-static {p0, v0}, Ltwitter4j/TwitterObjectFactory;->registerJSONObject(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_0
    return-void
.end method

.method constructor <init>(Ltwitter4j/JSONObject;)V
    .locals 3
    .param p1, "json"    # Ltwitter4j/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 85
    invoke-direct {p0}, Ltwitter4j/TwitterResponseImpl;-><init>()V

    .line 46
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->geoLocation:Ltwitter4j/GeoLocation;

    .line 47
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->place:Ltwitter4j/Place;

    .line 62
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ltwitter4j/StatusJSONImpl;->currentUserRetweetId:J

    .line 64
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->user:Ltwitter4j/User;

    .line 86
    invoke-direct {p0, p1}, Ltwitter4j/StatusJSONImpl;->init(Ltwitter4j/JSONObject;)V

    .line 87
    return-void
.end method

.method constructor <init>(Ltwitter4j/JSONObject;Ltwitter4j/conf/Configuration;)V
    .locals 3
    .param p1, "json"    # Ltwitter4j/JSONObject;
    .param p2, "conf"    # Ltwitter4j/conf/Configuration;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Ltwitter4j/TwitterResponseImpl;-><init>()V

    .line 46
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->geoLocation:Ltwitter4j/GeoLocation;

    .line 47
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->place:Ltwitter4j/Place;

    .line 62
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ltwitter4j/StatusJSONImpl;->currentUserRetweetId:J

    .line 64
    iput-object v2, p0, Ltwitter4j/StatusJSONImpl;->user:Ltwitter4j/User;

    .line 78
    invoke-direct {p0, p1}, Ltwitter4j/StatusJSONImpl;->init(Ltwitter4j/JSONObject;)V

    .line 79
    invoke-interface {p2}, Ltwitter4j/conf/Configuration;->isJSONStoreEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {p0, p1}, Ltwitter4j/TwitterObjectFactory;->registerJSONObject(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    :cond_0
    return-void
.end method

.method static createStatusList(Ltwitter4j/HttpResponse;Ltwitter4j/conf/Configuration;)Ltwitter4j/ResponseList;
    .locals 8
    .param p0, "res"    # Ltwitter4j/HttpResponse;
    .param p1, "conf"    # Ltwitter4j/conf/Configuration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltwitter4j/HttpResponse;",
            "Ltwitter4j/conf/Configuration;",
            ")",
            "Ltwitter4j/ResponseList",
            "<",
            "Ltwitter4j/Status;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 378
    :try_start_0
    invoke-interface {p1}, Ltwitter4j/conf/Configuration;->isJSONStoreEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 379
    invoke-static {}, Ltwitter4j/TwitterObjectFactory;->clearThreadLocalMap()V

    .line 381
    :cond_0
    invoke-virtual {p0}, Ltwitter4j/HttpResponse;->asJSONArray()Ltwitter4j/JSONArray;

    move-result-object v3

    .line 382
    .local v3, "list":Ltwitter4j/JSONArray;
    invoke-virtual {v3}, Ltwitter4j/JSONArray;->length()I

    move-result v4

    .line 383
    .local v4, "size":I
    new-instance v6, Ltwitter4j/ResponseListImpl;

    invoke-direct {v6, v4, p0}, Ltwitter4j/ResponseListImpl;-><init>(ILtwitter4j/HttpResponse;)V

    .line 384
    .local v6, "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_2

    .line 385
    invoke-virtual {v3, v0}, Ltwitter4j/JSONArray;->getJSONObject(I)Ltwitter4j/JSONObject;

    move-result-object v1

    .line 386
    .local v1, "json":Ltwitter4j/JSONObject;
    new-instance v5, Ltwitter4j/StatusJSONImpl;

    invoke-direct {v5, v1}, Ltwitter4j/StatusJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    .line 387
    .local v5, "status":Ltwitter4j/Status;
    invoke-interface {p1}, Ltwitter4j/conf/Configuration;->isJSONStoreEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 388
    invoke-static {v5, v1}, Ltwitter4j/TwitterObjectFactory;->registerJSONObject(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    :cond_1
    invoke-interface {v6, v5}, Ltwitter4j/ResponseList;->add(Ljava/lang/Object;)Z

    .line 384
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 392
    .end local v1    # "json":Ltwitter4j/JSONObject;
    .end local v5    # "status":Ltwitter4j/Status;
    :cond_2
    invoke-interface {p1}, Ltwitter4j/conf/Configuration;->isJSONStoreEnabled()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 393
    invoke-static {v6, v3}, Ltwitter4j/TwitterObjectFactory;->registerJSONObject(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ltwitter4j/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :cond_3
    return-object v6

    .line 396
    .end local v0    # "i":I
    .end local v3    # "list":Ltwitter4j/JSONArray;
    .end local v4    # "size":I
    .end local v6    # "statuses":Ltwitter4j/ResponseList;, "Ltwitter4j/ResponseList<Ltwitter4j/Status;>;"
    :catch_0
    move-exception v2

    .line 397
    .local v2, "jsone":Ltwitter4j/JSONException;
    new-instance v7, Ltwitter4j/TwitterException;

    invoke-direct {v7, v2}, Ltwitter4j/TwitterException;-><init>(Ljava/lang/Exception;)V

    throw v7
.end method

.method private init(Ltwitter4j/JSONObject;)V
    .locals 22
    .param p1, "json"    # Ltwitter4j/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 95
    const-string v17, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getLong(Ljava/lang/String;Ltwitter4j/JSONObject;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ltwitter4j/StatusJSONImpl;->id:J

    .line 96
    const-string v17, "source"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getUnescapedString(Ljava/lang/String;Ltwitter4j/JSONObject;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->source:Ljava/lang/String;

    .line 97
    const-string v17, "created_at"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getDate(Ljava/lang/String;Ltwitter4j/JSONObject;)Ljava/util/Date;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->createdAt:Ljava/util/Date;

    .line 98
    const-string v17, "truncated"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getBoolean(Ljava/lang/String;Ltwitter4j/JSONObject;)Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Ltwitter4j/StatusJSONImpl;->isTruncated:Z

    .line 99
    const-string v17, "in_reply_to_status_id"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getLong(Ljava/lang/String;Ltwitter4j/JSONObject;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ltwitter4j/StatusJSONImpl;->inReplyToStatusId:J

    .line 100
    const-string v17, "in_reply_to_user_id"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getLong(Ljava/lang/String;Ltwitter4j/JSONObject;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ltwitter4j/StatusJSONImpl;->inReplyToUserId:J

    .line 101
    const-string v17, "favorited"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getBoolean(Ljava/lang/String;Ltwitter4j/JSONObject;)Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Ltwitter4j/StatusJSONImpl;->isFavorited:Z

    .line 102
    const-string v17, "retweeted"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getBoolean(Ljava/lang/String;Ltwitter4j/JSONObject;)Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Ltwitter4j/StatusJSONImpl;->isRetweeted:Z

    .line 103
    const-string v17, "in_reply_to_screen_name"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getUnescapedString(Ljava/lang/String;Ltwitter4j/JSONObject;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->inReplyToScreenName:Ljava/lang/String;

    .line 104
    const-string v17, "retweet_count"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getLong(Ljava/lang/String;Ltwitter4j/JSONObject;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ltwitter4j/StatusJSONImpl;->retweetCount:J

    .line 105
    const-string v17, "favorite_count"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getInt(Ljava/lang/String;Ltwitter4j/JSONObject;)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Ltwitter4j/StatusJSONImpl;->favoriteCount:I

    .line 106
    const-string v17, "possibly_sensitive"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getBoolean(Ljava/lang/String;Ltwitter4j/JSONObject;)Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Ltwitter4j/StatusJSONImpl;->isPossiblySensitive:Z

    .line 108
    :try_start_0
    const-string v17, "user"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 109
    new-instance v17, Ltwitter4j/UserJSONImpl;

    const-string v18, "user"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getJSONObject(Ljava/lang/String;)Ltwitter4j/JSONObject;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ltwitter4j/UserJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->user:Ltwitter4j/User;

    .line 111
    :cond_0
    invoke-static/range {p1 .. p1}, Ltwitter4j/JSONImplFactory;->createGeoLocation(Ltwitter4j/JSONObject;)Ltwitter4j/GeoLocation;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->geoLocation:Ltwitter4j/GeoLocation;

    .line 112
    const-string v17, "place"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 113
    new-instance v17, Ltwitter4j/PlaceJSONImpl;

    const-string v18, "place"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getJSONObject(Ljava/lang/String;)Ltwitter4j/JSONObject;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ltwitter4j/PlaceJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->place:Ltwitter4j/Place;

    .line 116
    :cond_1
    const-string v17, "retweeted_status"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 117
    new-instance v17, Ltwitter4j/StatusJSONImpl;

    const-string v18, "retweeted_status"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getJSONObject(Ljava/lang/String;)Ltwitter4j/JSONObject;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ltwitter4j/StatusJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->retweetedStatus:Ltwitter4j/Status;

    .line 119
    :cond_2
    const-string v17, "contributors"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 120
    const-string v17, "contributors"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getJSONArray(Ljava/lang/String;)Ltwitter4j/JSONArray;

    move-result-object v4

    .line 121
    .local v4, "contributorsArray":Ltwitter4j/JSONArray;
    invoke-virtual {v4}, Ltwitter4j/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    new-array v0, v0, [J

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->contributorsIDs:[J

    .line 122
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v4}, Ltwitter4j/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_4

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->contributorsIDs:[J

    move-object/from16 v17, v0

    invoke-virtual {v4, v8}, Ltwitter4j/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    aput-wide v18, v17, v8

    .line 122
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 126
    .end local v4    # "contributorsArray":Ltwitter4j/JSONArray;
    .end local v8    # "i":I
    :cond_3
    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [J

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->contributorsIDs:[J

    .line 128
    :cond_4
    const-string v17, "entities"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_9

    .line 129
    const-string v17, "entities"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getJSONObject(Ljava/lang/String;)Ltwitter4j/JSONObject;

    move-result-object v5

    .line 131
    .local v5, "entities":Ltwitter4j/JSONObject;
    const-string v17, "user_mentions"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 132
    const-string v17, "user_mentions"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->getJSONArray(Ljava/lang/String;)Ltwitter4j/JSONArray;

    move-result-object v16

    .line 133
    .local v16, "userMentionsArray":Ltwitter4j/JSONArray;
    invoke-virtual/range {v16 .. v16}, Ltwitter4j/JSONArray;->length()I

    move-result v10

    .line 134
    .local v10, "len":I
    new-array v0, v10, [Ltwitter4j/UserMentionEntity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->userMentionEntities:[Ltwitter4j/UserMentionEntity;

    .line 135
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_1
    if-ge v8, v10, :cond_5

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->userMentionEntities:[Ltwitter4j/UserMentionEntity;

    move-object/from16 v17, v0

    new-instance v18, Ltwitter4j/UserMentionEntityJSONImpl;

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ltwitter4j/JSONArray;->getJSONObject(I)Ltwitter4j/JSONObject;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ltwitter4j/UserMentionEntityJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    aput-object v18, v17, v8

    .line 135
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 139
    .end local v8    # "i":I
    .end local v10    # "len":I
    .end local v16    # "userMentionsArray":Ltwitter4j/JSONArray;
    :cond_5
    const-string v17, "urls"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_6

    .line 140
    const-string v17, "urls"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->getJSONArray(Ljava/lang/String;)Ltwitter4j/JSONArray;

    move-result-object v15

    .line 141
    .local v15, "urlsArray":Ltwitter4j/JSONArray;
    invoke-virtual {v15}, Ltwitter4j/JSONArray;->length()I

    move-result v10

    .line 142
    .restart local v10    # "len":I
    new-array v0, v10, [Ltwitter4j/URLEntity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->urlEntities:[Ltwitter4j/URLEntity;

    .line 143
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_2
    if-ge v8, v10, :cond_6

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->urlEntities:[Ltwitter4j/URLEntity;

    move-object/from16 v17, v0

    new-instance v18, Ltwitter4j/URLEntityJSONImpl;

    invoke-virtual {v15, v8}, Ltwitter4j/JSONArray;->getJSONObject(I)Ltwitter4j/JSONObject;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ltwitter4j/URLEntityJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    aput-object v18, v17, v8

    .line 143
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 148
    .end local v8    # "i":I
    .end local v10    # "len":I
    .end local v15    # "urlsArray":Ltwitter4j/JSONArray;
    :cond_6
    const-string v17, "hashtags"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_7

    .line 149
    const-string v17, "hashtags"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->getJSONArray(Ljava/lang/String;)Ltwitter4j/JSONArray;

    move-result-object v7

    .line 150
    .local v7, "hashtagsArray":Ltwitter4j/JSONArray;
    invoke-virtual {v7}, Ltwitter4j/JSONArray;->length()I

    move-result v10

    .line 151
    .restart local v10    # "len":I
    new-array v0, v10, [Ltwitter4j/HashtagEntity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->hashtagEntities:[Ltwitter4j/HashtagEntity;

    .line 152
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_3
    if-ge v8, v10, :cond_7

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->hashtagEntities:[Ltwitter4j/HashtagEntity;

    move-object/from16 v17, v0

    new-instance v18, Ltwitter4j/HashtagEntityJSONImpl;

    invoke-virtual {v7, v8}, Ltwitter4j/JSONArray;->getJSONObject(I)Ltwitter4j/JSONObject;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ltwitter4j/HashtagEntityJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    aput-object v18, v17, v8

    .line 152
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 157
    .end local v7    # "hashtagsArray":Ltwitter4j/JSONArray;
    .end local v8    # "i":I
    .end local v10    # "len":I
    :cond_7
    const-string v17, "symbols"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_8

    .line 158
    const-string v17, "symbols"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->getJSONArray(Ljava/lang/String;)Ltwitter4j/JSONArray;

    move-result-object v7

    .line 159
    .restart local v7    # "hashtagsArray":Ltwitter4j/JSONArray;
    invoke-virtual {v7}, Ltwitter4j/JSONArray;->length()I

    move-result v10

    .line 160
    .restart local v10    # "len":I
    new-array v0, v10, [Ltwitter4j/SymbolEntity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->symbolEntities:[Ltwitter4j/SymbolEntity;

    .line 161
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_4
    if-ge v8, v10, :cond_8

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->symbolEntities:[Ltwitter4j/SymbolEntity;

    move-object/from16 v17, v0

    new-instance v18, Ltwitter4j/HashtagEntityJSONImpl;

    invoke-virtual {v7, v8}, Ltwitter4j/JSONArray;->getJSONObject(I)Ltwitter4j/JSONObject;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ltwitter4j/HashtagEntityJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    aput-object v18, v17, v8

    .line 161
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 167
    .end local v7    # "hashtagsArray":Ltwitter4j/JSONArray;
    .end local v8    # "i":I
    .end local v10    # "len":I
    :cond_8
    const-string v17, "media"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_9

    .line 168
    const-string v17, "media"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ltwitter4j/JSONObject;->getJSONArray(Ljava/lang/String;)Ltwitter4j/JSONArray;

    move-result-object v11

    .line 169
    .local v11, "mediaArray":Ltwitter4j/JSONArray;
    invoke-virtual {v11}, Ltwitter4j/JSONArray;->length()I

    move-result v10

    .line 170
    .restart local v10    # "len":I
    new-array v0, v10, [Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->mediaEntities:[Ltwitter4j/MediaEntity;

    .line 171
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_5
    if-ge v8, v10, :cond_9

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->mediaEntities:[Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    new-instance v18, Ltwitter4j/MediaEntityJSONImpl;

    invoke-virtual {v11, v8}, Ltwitter4j/JSONArray;->getJSONObject(I)Ltwitter4j/JSONObject;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ltwitter4j/MediaEntityJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    aput-object v18, v17, v8

    .line 171
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 176
    .end local v5    # "entities":Ltwitter4j/JSONObject;
    .end local v8    # "i":I
    .end local v10    # "len":I
    .end local v11    # "mediaArray":Ltwitter4j/JSONArray;
    :cond_9
    const-string v17, "extended_entities"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_a

    .line 177
    const-string v17, "extended_entities"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getJSONObject(Ljava/lang/String;)Ltwitter4j/JSONObject;

    move-result-object v6

    .line 178
    .local v6, "extendedEntities":Ltwitter4j/JSONObject;
    const-string v17, "media"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_a

    .line 179
    const-string v17, "media"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ltwitter4j/JSONObject;->getJSONArray(Ljava/lang/String;)Ltwitter4j/JSONArray;

    move-result-object v11

    .line 180
    .restart local v11    # "mediaArray":Ltwitter4j/JSONArray;
    invoke-virtual {v11}, Ltwitter4j/JSONArray;->length()I

    move-result v10

    .line 181
    .restart local v10    # "len":I
    new-array v0, v10, [Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->extendedMediaEntities:[Ltwitter4j/MediaEntity;

    .line 182
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_6
    if-ge v8, v10, :cond_a

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->extendedMediaEntities:[Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    new-instance v18, Ltwitter4j/MediaEntityJSONImpl;

    invoke-virtual {v11, v8}, Ltwitter4j/JSONArray;->getJSONObject(I)Ltwitter4j/JSONObject;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ltwitter4j/MediaEntityJSONImpl;-><init>(Ltwitter4j/JSONObject;)V

    aput-object v18, v17, v8

    .line 182
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 188
    .end local v6    # "extendedEntities":Ltwitter4j/JSONObject;
    .end local v8    # "i":I
    .end local v10    # "len":I
    .end local v11    # "mediaArray":Ltwitter4j/JSONArray;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->userMentionEntities:[Ltwitter4j/UserMentionEntity;

    move-object/from16 v17, v0

    if-nez v17, :cond_d

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ltwitter4j/UserMentionEntity;

    move-object/from16 v17, v0

    :goto_7
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->userMentionEntities:[Ltwitter4j/UserMentionEntity;

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->urlEntities:[Ltwitter4j/URLEntity;

    move-object/from16 v17, v0

    if-nez v17, :cond_e

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ltwitter4j/URLEntity;

    move-object/from16 v17, v0

    :goto_8
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->urlEntities:[Ltwitter4j/URLEntity;

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->hashtagEntities:[Ltwitter4j/HashtagEntity;

    move-object/from16 v17, v0

    if-nez v17, :cond_f

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ltwitter4j/HashtagEntity;

    move-object/from16 v17, v0

    :goto_9
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->hashtagEntities:[Ltwitter4j/HashtagEntity;

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->symbolEntities:[Ltwitter4j/SymbolEntity;

    move-object/from16 v17, v0

    if-nez v17, :cond_10

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ltwitter4j/SymbolEntity;

    move-object/from16 v17, v0

    :goto_a
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->symbolEntities:[Ltwitter4j/SymbolEntity;

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->mediaEntities:[Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    if-nez v17, :cond_11

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    :goto_b
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->mediaEntities:[Ltwitter4j/MediaEntity;

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->extendedMediaEntities:[Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    if-nez v17, :cond_12

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    :goto_c
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->extendedMediaEntities:[Ltwitter4j/MediaEntity;

    .line 194
    const-string v17, "text"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->userMentionEntities:[Ltwitter4j/UserMentionEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->urlEntities:[Ltwitter4j/URLEntity;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->hashtagEntities:[Ltwitter4j/HashtagEntity;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->mediaEntities:[Ltwitter4j/MediaEntity;

    move-object/from16 v21, v0

    invoke-static/range {v17 .. v21}, Ltwitter4j/HTMLEntity;->unescapeAndSlideEntityIncdices(Ljava/lang/String;[Ltwitter4j/UserMentionEntity;[Ltwitter4j/URLEntity;[Ltwitter4j/HashtagEntity;[Ltwitter4j/MediaEntity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->text:Ljava/lang/String;

    .line 196
    const-string v17, "current_user_retweet"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_b

    .line 197
    const-string v17, "current_user_retweet"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getJSONObject(Ljava/lang/String;)Ltwitter4j/JSONObject;

    move-result-object v17

    const-string v18, "id"

    invoke-virtual/range {v17 .. v18}, Ltwitter4j/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ltwitter4j/StatusJSONImpl;->currentUserRetweetId:J

    .line 199
    :cond_b
    const-string v17, "lang"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_c

    .line 200
    const-string v17, "lang"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ltwitter4j/ParseUtil;->getUnescapedString(Ljava/lang/String;Ltwitter4j/JSONObject;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->lang:Ljava/lang/String;

    .line 203
    :cond_c
    const-string v17, "scopes"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_14

    .line 204
    const-string v17, "scopes"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ltwitter4j/JSONObject;->getJSONObject(Ljava/lang/String;)Ltwitter4j/JSONObject;

    move-result-object v14

    .line 205
    .local v14, "scopesJson":Ltwitter4j/JSONObject;
    const-string v17, "place_ids"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ltwitter4j/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_14

    .line 206
    const-string v17, "place_ids"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ltwitter4j/JSONObject;->getJSONArray(Ljava/lang/String;)Ltwitter4j/JSONArray;

    move-result-object v13

    .line 207
    .local v13, "placeIdsArray":Ltwitter4j/JSONArray;
    invoke-virtual {v13}, Ltwitter4j/JSONArray;->length()I

    move-result v10

    .line 208
    .restart local v10    # "len":I
    new-array v12, v10, [Ljava/lang/String;

    .line 209
    .local v12, "placeIds":[Ljava/lang/String;
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_d
    if-ge v8, v10, :cond_13

    .line 210
    invoke-virtual {v13, v8}, Ltwitter4j/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v12, v8

    .line 209
    add-int/lit8 v8, v8, 0x1

    goto :goto_d

    .line 188
    .end local v8    # "i":I
    .end local v10    # "len":I
    .end local v12    # "placeIds":[Ljava/lang/String;
    .end local v13    # "placeIdsArray":Ltwitter4j/JSONArray;
    .end local v14    # "scopesJson":Ltwitter4j/JSONObject;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->userMentionEntities:[Ltwitter4j/UserMentionEntity;

    move-object/from16 v17, v0

    goto/16 :goto_7

    .line 189
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->urlEntities:[Ltwitter4j/URLEntity;

    move-object/from16 v17, v0

    goto/16 :goto_8

    .line 190
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->hashtagEntities:[Ltwitter4j/HashtagEntity;

    move-object/from16 v17, v0

    goto/16 :goto_9

    .line 191
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->symbolEntities:[Ltwitter4j/SymbolEntity;

    move-object/from16 v17, v0

    goto/16 :goto_a

    .line 192
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->mediaEntities:[Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    goto/16 :goto_b

    .line 193
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Ltwitter4j/StatusJSONImpl;->extendedMediaEntities:[Ltwitter4j/MediaEntity;

    move-object/from16 v17, v0

    goto/16 :goto_c

    .line 212
    .restart local v8    # "i":I
    .restart local v10    # "len":I
    .restart local v12    # "placeIds":[Ljava/lang/String;
    .restart local v13    # "placeIdsArray":Ltwitter4j/JSONArray;
    .restart local v14    # "scopesJson":Ltwitter4j/JSONObject;
    :cond_13
    new-instance v17, Ltwitter4j/ScopesImpl;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Ltwitter4j/ScopesImpl;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Ltwitter4j/StatusJSONImpl;->scopes:Ltwitter4j/Scopes;
    :try_end_0
    .catch Ltwitter4j/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    .end local v8    # "i":I
    .end local v10    # "len":I
    .end local v12    # "placeIds":[Ljava/lang/String;
    .end local v13    # "placeIdsArray":Ltwitter4j/JSONArray;
    .end local v14    # "scopesJson":Ltwitter4j/JSONObject;
    :cond_14
    return-void

    .line 215
    :catch_0
    move-exception v9

    .line 216
    .local v9, "jsone":Ltwitter4j/JSONException;
    new-instance v17, Ltwitter4j/TwitterException;

    move-object/from16 v0, v17

    invoke-direct {v0, v9}, Ltwitter4j/TwitterException;-><init>(Ljava/lang/Exception;)V

    throw v17
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 31
    check-cast p1, Ltwitter4j/Status;

    invoke-virtual {p0, p1}, Ltwitter4j/StatusJSONImpl;->compareTo(Ltwitter4j/Status;)I

    move-result v0

    return v0
.end method

.method public compareTo(Ltwitter4j/Status;)I
    .locals 6
    .param p1, "that"    # Ltwitter4j/Status;

    .prologue
    .line 222
    iget-wide v2, p0, Ltwitter4j/StatusJSONImpl;->id:J

    invoke-interface {p1}, Ltwitter4j/Status;->getId()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 223
    .local v0, "delta":J
    const-wide/32 v2, -0x80000000

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 224
    const/high16 v2, -0x80000000

    .line 228
    :goto_0
    return v2

    .line 225
    :cond_0
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 226
    const v2, 0x7fffffff

    goto :goto_0

    .line 228
    :cond_1
    long-to-int v2, v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 408
    if-nez p1, :cond_1

    move v0, v1

    .line 414
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 411
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    if-eq p0, p1, :cond_0

    .line 414
    instance-of v2, p1, Ltwitter4j/Status;

    if-eqz v2, :cond_2

    check-cast p1, Ltwitter4j/Status;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-interface {p1}, Ltwitter4j/Status;->getId()J

    move-result-wide v2

    iget-wide v4, p0, Ltwitter4j/StatusJSONImpl;->id:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public getContributors()[J
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->contributorsIDs:[J

    return-object v0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->createdAt:Ljava/util/Date;

    return-object v0
.end method

.method public getCurrentUserRetweetId()J
    .locals 2

    .prologue
    .line 329
    iget-wide v0, p0, Ltwitter4j/StatusJSONImpl;->currentUserRetweetId:J

    return-wide v0
.end method

.method public getExtendedMediaEntities()[Ltwitter4j/MediaEntity;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->extendedMediaEntities:[Ltwitter4j/MediaEntity;

    return-object v0
.end method

.method public getFavoriteCount()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Ltwitter4j/StatusJSONImpl;->favoriteCount:I

    return v0
.end method

.method public getGeoLocation()Ltwitter4j/GeoLocation;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->geoLocation:Ltwitter4j/GeoLocation;

    return-object v0
.end method

.method public getHashtagEntities()[Ltwitter4j/HashtagEntity;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->hashtagEntities:[Ltwitter4j/HashtagEntity;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 238
    iget-wide v0, p0, Ltwitter4j/StatusJSONImpl;->id:J

    return-wide v0
.end method

.method public getInReplyToScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->inReplyToScreenName:Ljava/lang/String;

    return-object v0
.end method

.method public getInReplyToStatusId()J
    .locals 2

    .prologue
    .line 259
    iget-wide v0, p0, Ltwitter4j/StatusJSONImpl;->inReplyToStatusId:J

    return-wide v0
.end method

.method public getInReplyToUserId()J
    .locals 2

    .prologue
    .line 264
    iget-wide v0, p0, Ltwitter4j/StatusJSONImpl;->inReplyToUserId:J

    return-wide v0
.end method

.method public getLang()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->lang:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaEntities()[Ltwitter4j/MediaEntity;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->mediaEntities:[Ltwitter4j/MediaEntity;

    return-object v0
.end method

.method public getPlace()Ltwitter4j/Place;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->place:Ltwitter4j/Place;

    return-object v0
.end method

.method public getRetweetCount()I
    .locals 2

    .prologue
    .line 319
    iget-wide v0, p0, Ltwitter4j/StatusJSONImpl;->retweetCount:J

    long-to-int v0, v0

    return v0
.end method

.method public getRetweetedStatus()Ltwitter4j/Status;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->retweetedStatus:Ltwitter4j/Status;

    return-object v0
.end method

.method public getScopes()Ltwitter4j/Scopes;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->scopes:Ltwitter4j/Scopes;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getSymbolEntities()[Ltwitter4j/SymbolEntity;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->symbolEntities:[Ltwitter4j/SymbolEntity;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getURLEntities()[Ltwitter4j/URLEntity;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->urlEntities:[Ltwitter4j/URLEntity;

    return-object v0
.end method

.method public getUser()Ltwitter4j/User;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->user:Ltwitter4j/User;

    return-object v0
.end method

.method public getUserMentionEntities()[Ltwitter4j/UserMentionEntity;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->userMentionEntities:[Ltwitter4j/UserMentionEntity;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 403
    iget-wide v0, p0, Ltwitter4j/StatusJSONImpl;->id:J

    long-to-int v0, v0

    return v0
.end method

.method public isFavorited()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Ltwitter4j/StatusJSONImpl;->isFavorited:Z

    return v0
.end method

.method public isPossiblySensitive()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Ltwitter4j/StatusJSONImpl;->isPossiblySensitive:Z

    return v0
.end method

.method public isRetweet()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Ltwitter4j/StatusJSONImpl;->retweetedStatus:Ltwitter4j/Status;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRetweeted()Z
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Ltwitter4j/StatusJSONImpl;->isRetweeted:Z

    return v0
.end method

.method public isRetweetedByMe()Z
    .locals 4

    .prologue
    .line 324
    iget-wide v0, p0, Ltwitter4j/StatusJSONImpl;->currentUserRetweetId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTruncated()Z
    .locals 1

    .prologue
    .line 254
    iget-boolean v0, p0, Ltwitter4j/StatusJSONImpl;->isTruncated:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StatusJSONImpl{createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->createdAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltwitter4j/StatusJSONImpl;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", text=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", source=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isTruncated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltwitter4j/StatusJSONImpl;->isTruncated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToStatusId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltwitter4j/StatusJSONImpl;->inReplyToStatusId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltwitter4j/StatusJSONImpl;->inReplyToUserId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFavorited="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltwitter4j/StatusJSONImpl;->isFavorited:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isRetweeted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltwitter4j/StatusJSONImpl;->isRetweeted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", favoriteCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltwitter4j/StatusJSONImpl;->favoriteCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inReplyToScreenName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->inReplyToScreenName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", geoLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->geoLocation:Ltwitter4j/GeoLocation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", place="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->place:Ltwitter4j/Place;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retweetCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltwitter4j/StatusJSONImpl;->retweetCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isPossiblySensitive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltwitter4j/StatusJSONImpl;->isPossiblySensitive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lang=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->lang:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contributorsIDs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->contributorsIDs:[J

    .line 436
    invoke-static {v1}, Ljava/util/Arrays;->toString([J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retweetedStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->retweetedStatus:Ltwitter4j/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userMentionEntities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->userMentionEntities:[Ltwitter4j/UserMentionEntity;

    .line 438
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", urlEntities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->urlEntities:[Ltwitter4j/URLEntity;

    .line 439
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hashtagEntities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->hashtagEntities:[Ltwitter4j/HashtagEntity;

    .line 440
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mediaEntities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->mediaEntities:[Ltwitter4j/MediaEntity;

    .line 441
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", symbolEntities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->symbolEntities:[Ltwitter4j/SymbolEntity;

    .line 442
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currentUserRetweetId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltwitter4j/StatusJSONImpl;->currentUserRetweetId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/StatusJSONImpl;->user:Ltwitter4j/User;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
