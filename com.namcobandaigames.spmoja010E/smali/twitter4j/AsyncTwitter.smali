.class public interface abstract Ltwitter4j/AsyncTwitter;
.super Ljava/lang/Object;
.source "AsyncTwitter.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ltwitter4j/auth/OAuthSupport;
.implements Ltwitter4j/auth/OAuth2Support;
.implements Ltwitter4j/auth/AsyncOAuthSupport;
.implements Ltwitter4j/auth/AsyncOAuth2Support;
.implements Ltwitter4j/TwitterBase;
.implements Ltwitter4j/api/TimelinesResourcesAsync;
.implements Ltwitter4j/api/TweetsResourcesAsync;
.implements Ltwitter4j/api/SearchResourceAsync;
.implements Ltwitter4j/api/DirectMessagesResourcesAsync;
.implements Ltwitter4j/api/FriendsFollowersResourcesAsync;
.implements Ltwitter4j/api/UsersResourcesAsync;
.implements Ltwitter4j/api/SuggestedUsersResourcesAsync;
.implements Ltwitter4j/api/FavoritesResourcesAsync;
.implements Ltwitter4j/api/ListsResourcesAsync;
.implements Ltwitter4j/api/SavedSearchesResourcesAsync;
.implements Ltwitter4j/api/PlacesGeoResourcesAsync;
.implements Ltwitter4j/api/TrendsResourcesAsync;
.implements Ltwitter4j/api/SpamReportingResourceAsync;
.implements Ltwitter4j/api/HelpResourcesAsync;


# virtual methods
.method public abstract addListener(Ltwitter4j/TwitterListener;)V
.end method

.method public abstract shutdown()V
.end method
