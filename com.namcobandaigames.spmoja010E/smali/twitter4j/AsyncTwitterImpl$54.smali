.class Ltwitter4j/AsyncTwitterImpl$54;
.super Ltwitter4j/AsyncTwitterImpl$AsyncTask;
.source "AsyncTwitterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltwitter4j/AsyncTwitterImpl;->updateAccountSettings(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ltwitter4j/AsyncTwitterImpl;

.field final synthetic val$end_sleepTime:Ljava/lang/String;

.field final synthetic val$lang:Ljava/lang/String;

.field final synthetic val$sleep_timeEnabled:Ljava/lang/Boolean;

.field final synthetic val$start_sleepTime:Ljava/lang/String;

.field final synthetic val$time_zone:Ljava/lang/String;

.field final synthetic val$trend_locationWoeid:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Ltwitter4j/AsyncTwitterImpl;
    .param p2, "method"    # Ltwitter4j/TwitterMethod;

    .prologue
    .line 939
    .local p3, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iput-object p1, p0, Ltwitter4j/AsyncTwitterImpl$54;->this$0:Ltwitter4j/AsyncTwitterImpl;

    iput-object p4, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$trend_locationWoeid:Ljava/lang/Integer;

    iput-object p5, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$sleep_timeEnabled:Ljava/lang/Boolean;

    iput-object p6, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$start_sleepTime:Ljava/lang/String;

    iput-object p7, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$end_sleepTime:Ljava/lang/String;

    iput-object p8, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$time_zone:Ljava/lang/String;

    iput-object p9, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$lang:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Ltwitter4j/AsyncTwitterImpl$AsyncTask;-><init>(Ltwitter4j/AsyncTwitterImpl;Ltwitter4j/TwitterMethod;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltwitter4j/TwitterListener;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ltwitter4j/TwitterException;
        }
    .end annotation

    .prologue
    .line 942
    .local p1, "listeners":Ljava/util/List;, "Ljava/util/List<Ltwitter4j/TwitterListener;>;"
    iget-object v0, p0, Ltwitter4j/AsyncTwitterImpl$54;->this$0:Ltwitter4j/AsyncTwitterImpl;

    invoke-static {v0}, Ltwitter4j/AsyncTwitterImpl;->access$000(Ltwitter4j/AsyncTwitterImpl;)Ltwitter4j/Twitter;

    move-result-object v0

    iget-object v1, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$trend_locationWoeid:Ljava/lang/Integer;

    iget-object v2, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$sleep_timeEnabled:Ljava/lang/Boolean;

    iget-object v3, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$start_sleepTime:Ljava/lang/String;

    iget-object v4, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$end_sleepTime:Ljava/lang/String;

    iget-object v5, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$time_zone:Ljava/lang/String;

    iget-object v6, p0, Ltwitter4j/AsyncTwitterImpl$54;->val$lang:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Ltwitter4j/Twitter;->updateAccountSettings(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltwitter4j/AccountSettings;

    move-result-object v7

    .line 943
    .local v7, "accountSettings":Ltwitter4j/AccountSettings;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ltwitter4j/TwitterListener;

    .line 945
    .local v8, "listener":Ltwitter4j/TwitterListener;
    :try_start_0
    invoke-interface {v8, v7}, Ltwitter4j/TwitterListener;->updatedAccountSettings(Ltwitter4j/AccountSettings;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 946
    :catch_0
    move-exception v1

    goto :goto_0

    .line 949
    .end local v8    # "listener":Ltwitter4j/TwitterListener;
    :cond_0
    return-void
.end method
