.class public Ljp/co/drecom/bisque/lib/BQSleepSetting;
.super Ljava/lang/Object;
.source "BQSleepSetting.java"


# instance fields
.field private parent:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQSleepSetting;->parent:Landroid/app/Activity;

    .line 15
    return-void
.end method


# virtual methods
.method public setDeviceSleep(Z)V
    .locals 2
    .param p1, "set"    # Z

    .prologue
    const/16 v1, 0x80

    .line 18
    if-eqz p1, :cond_0

    .line 21
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQSleepSetting;->parent:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 29
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQSleepSetting;->parent:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method
