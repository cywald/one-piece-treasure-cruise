.class Ljp/co/drecom/bisque/lib/BQGameHelper$1;
.super Ljava/lang/Object;
.source "BQGameHelper.java"

# interfaces
.implements Lcom/google/android/gms/tasks/OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQGameHelper;->signInSilently()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/tasks/OnCompleteListener",
        "<",
        "Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQGameHelper;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQGameHelper;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQGameHelper;

    .prologue
    .line 94
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGameHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/tasks/Task;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task",
            "<",
            "Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "task":Lcom/google/android/gms/tasks/Task;, "Lcom/google/android/gms/tasks/Task<Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;>;"
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    .line 99
    .local v0, "googleSignInAccount":Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGameHelper;

    invoke-static {v1, v0}, Ljp/co/drecom/bisque/lib/BQGameHelper;->access$000(Ljp/co/drecom/bisque/lib/BQGameHelper;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    .line 100
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGameHelper;

    iget-object v1, v1, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGameHelper;

    iget-object v1, v1, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    invoke-interface {v1, v0}, Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;->onSilentSignInSucceeded(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    .line 106
    .end local v0    # "googleSignInAccount":Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGameHelper;

    iget-object v1, v1, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGameHelper;

    iget-object v1, v1, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getException()Ljava/lang/Exception;

    move-result-object v2

    invoke-interface {v1, v2}, Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;->onSilentSignInFailed(Ljava/lang/Exception;)V

    .line 104
    :cond_2
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGameHelper;

    invoke-static {v1}, Ljp/co/drecom/bisque/lib/BQGameHelper;->access$100(Ljp/co/drecom/bisque/lib/BQGameHelper;)V

    goto :goto_0
.end method
