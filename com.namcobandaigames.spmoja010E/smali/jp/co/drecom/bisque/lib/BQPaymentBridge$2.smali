.class Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;
.super Ljava/lang/Object;
.source "BQPaymentBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQPaymentBridge;->requestPayment(JLjava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

.field final synthetic val$productIdentifier:Ljava/lang/String;

.field final synthetic val$requestIdentifier:J


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;JLjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 184
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    iput-wide p2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$requestIdentifier:J

    iput-object p4, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$productIdentifier:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 188
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 191
    iget-wide v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$requestIdentifier:J

    const-string v2, ""

    const/4 v3, -0x1

    const-string v4, "Can not call getBuyIntent()"

    invoke-static {v0, v1, v2, v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$600(JLjava/lang/String;ILjava/lang/String;)V

    .line 230
    :goto_0
    return-void

    .line 196
    :cond_0
    :try_start_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v2}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$400(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$productIdentifier:Ljava/lang/String;

    const-string v4, "inapp"

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/android/vending/billing/IInAppBillingService;->getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .line 197
    .local v7, "buyIntentBundle":Landroid/os/Bundle;
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-virtual {v0, v7}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v10

    .line 198
    .local v10, "response":I
    if-eqz v10, :cond_1

    .line 200
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    new-instance v1, Lcom/ex/android/util/IabResult;

    const-string v2, "Unable to buy item"

    invoke-direct {v1, v10, v2}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$702(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/ex/android/util/IabResult;)Lcom/ex/android/util/IabResult;

    .line 202
    iget-wide v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$requestIdentifier:J

    const-string v2, ""

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$700(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/ex/android/util/IabResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v3

    iget-object v4, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$700(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/ex/android/util/IabResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$600(JLjava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 217
    .end local v7    # "buyIntentBundle":Landroid/os/Bundle;
    .end local v10    # "response":I
    :catch_0
    move-exception v8

    .line 219
    .local v8, "e":Landroid/content/IntentSender$SendIntentException;
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    new-instance v1, Lcom/ex/android/util/IabResult;

    const/16 v2, -0x3ec

    const-string v3, "Failed to send intent."

    invoke-direct {v1, v2, v3}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$702(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/ex/android/util/IabResult;)Lcom/ex/android/util/IabResult;

    .line 221
    iget-wide v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$requestIdentifier:J

    const-string v2, ""

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$700(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/ex/android/util/IabResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v3

    iget-object v4, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$700(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/ex/android/util/IabResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$600(JLjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 206
    .end local v8    # "e":Landroid/content/IntentSender$SendIntentException;
    .restart local v7    # "buyIntentBundle":Landroid/os/Bundle;
    .restart local v10    # "response":I
    :cond_1
    :try_start_1
    const-string v0, "BUY_INTENT"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/app/PendingIntent;

    .line 208
    .local v9, "pendingIntent":Landroid/app/PendingIntent;
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    iget-wide v2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$requestIdentifier:J

    invoke-static {v0, v2, v3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$802(Ljp/co/drecom/bisque/lib/BQPaymentBridge;J)J

    .line 209
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$400(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;

    move-result-object v0

    .line 210
    invoke-virtual {v9}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    iget-wide v2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$requestIdentifier:J

    long-to-int v2, v2

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const/4 v4, 0x0

    .line 213
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    .line 214
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x0

    .line 215
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 209
    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_1
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 223
    .end local v7    # "buyIntentBundle":Landroid/os/Bundle;
    .end local v9    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v10    # "response":I
    :catch_1
    move-exception v8

    .line 225
    .local v8, "e":Landroid/os/RemoteException;
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    new-instance v1, Lcom/ex/android/util/IabResult;

    const/16 v2, -0x3e9

    const-string v3, "Remote exception while starting purchase flow"

    invoke-direct {v1, v2, v3}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$702(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/ex/android/util/IabResult;)Lcom/ex/android/util/IabResult;

    .line 227
    iget-wide v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->val$requestIdentifier:J

    const-string v2, ""

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$700(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/ex/android/util/IabResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v3

    iget-object v4, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$700(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/ex/android/util/IabResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$600(JLjava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0
.end method
