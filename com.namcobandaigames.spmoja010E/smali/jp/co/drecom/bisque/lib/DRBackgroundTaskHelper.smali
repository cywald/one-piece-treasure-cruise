.class public Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;
.super Ljava/lang/Object;
.source "DRBackgroundTaskHelper.java"


# static fields
.field private static mInstance:Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;


# instance fields
.field private mBackgroundTaskIntentFilter:Landroid/content/IntentFilter;

.field private mBackgroundTaskReceiver:Ljp/co/drecom/bisque/lib/DRBackgroundTaskReceiver;

.field private mInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInitialized:Z

    .line 49
    return-void
.end method

.method public static finalization()V
    .locals 1

    .prologue
    .line 82
    sget-object v0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;

    .line 85
    :cond_0
    return-void
.end method

.method public static finish(Ljava/lang/String;)V
    .locals 0
    .param p0, "uuid"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-static {p0}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->nativeCallbackFuncForFinish(Ljava/lang/String;)V

    .line 115
    return-void
.end method

.method private static getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/HandlerJava;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;

    .line 43
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;

    return-object v0
.end method

.method private initInternal()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 67
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInitialized:Z

    if-ne v0, v3, :cond_0

    .line 75
    :goto_0
    return v3

    .line 71
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "jp.co.drecom.bisque.lib.drbackgroundtask.broadcast.BGTASK"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mBackgroundTaskIntentFilter:Landroid/content/IntentFilter;

    .line 72
    new-instance v0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskReceiver;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskReceiver;-><init>()V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mBackgroundTaskReceiver:Ljp/co/drecom/bisque/lib/DRBackgroundTaskReceiver;

    .line 73
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mBackgroundTaskReceiver:Ljp/co/drecom/bisque/lib/DRBackgroundTaskReceiver;

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mBackgroundTaskIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 74
    iput-boolean v3, p0, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->mInitialized:Z

    goto :goto_0
.end method

.method public static initialize()Z
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    const/4 v0, 0x0

    .line 59
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->getInstance()Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->initInternal()Z

    move-result v0

    goto :goto_0
.end method

.method private static native nativeCallbackFuncForFinish(Ljava/lang/String;)V
.end method

.method private static native nativeCallbackFuncForTask(Ljava/lang/String;)V
.end method

.method public static start(Ljava/lang/String;)Z
    .locals 1
    .param p0, "uuid"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p0}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskIntentService;->startAction(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static task(Ljava/lang/String;)V
    .locals 0
    .param p0, "uuid"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-static {p0}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->nativeCallbackFuncForTask(Ljava/lang/String;)V

    .line 107
    return-void
.end method
