.class public final Ljp/co/drecom/bisque/lib/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_font:I = 0x0

.field public static final FontFamilyFont_fontStyle:I = 0x1

.field public static final FontFamilyFont_fontWeight:I = 0x2

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x1

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3

.field public static final FontFamily_fontProviderPackage:I = 0x4

.field public static final FontFamily_fontProviderQuery:I = 0x5

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x0

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x2

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 218
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ljp/co/drecom/bisque/lib/R$styleable;->FontFamily:[I

    .line 225
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Ljp/co/drecom/bisque/lib/R$styleable;->FontFamilyFont:[I

    .line 229
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Ljp/co/drecom/bisque/lib/R$styleable;->LoadingImageView:[I

    .line 233
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Ljp/co/drecom/bisque/lib/R$styleable;->SignInButton:[I

    return-void

    .line 218
    nop

    :array_0
    .array-data 4
        0x7f020074
        0x7f020075
        0x7f020076
        0x7f020077
        0x7f020078
        0x7f020079
    .end array-data

    .line 225
    :array_1
    .array-data 4
        0x7f020072
        0x7f02007a
        0x7f02007b
    .end array-data

    .line 229
    :array_2
    .array-data 4
        0x7f020046
        0x7f020086
        0x7f020087
    .end array-data

    .line 233
    :array_3
    .array-data 4
        0x7f02003f
        0x7f020055
        0x7f0200b6
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
