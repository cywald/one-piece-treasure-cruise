.class Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;
.super Ljava/lang/Object;
.source "BQGeoLocationHeloper.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->initListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    .prologue
    .line 153
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 157
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-static {v1}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->access$000(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->updateLocation(Landroid/location/Location;Z)V

    .line 160
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 1
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 162
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-virtual {v0, p1}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->handleProviderDisabled(Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 1
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 165
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-virtual {v0, p1}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->handleProviderEnabled(Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 168
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-virtual {v0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->handleStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 169
    return-void
.end method
