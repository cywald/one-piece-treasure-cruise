.class public Ljp/co/drecom/bisque/lib/BQClipboard;
.super Ljava/lang/Object;
.source "BQClipboard.java"


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQClipboard;->context:Landroid/content/Context;

    .line 14
    return-void
.end method


# virtual methods
.method public getStringFromClipboard()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 24
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQClipboard;->context:Landroid/content/Context;

    const-string v3, "clipboard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 25
    .local v0, "manager":Landroid/text/ClipboardManager;
    if-nez v0, :cond_1

    .line 27
    :cond_0
    :goto_0
    return-object v1

    .line 26
    :cond_1
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 27
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public setStringToClipboard(Ljava/lang/String;)V
    .locals 3
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 18
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQClipboard;->context:Landroid/content/Context;

    const-string v2, "clipboard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 19
    .local v0, "manager":Landroid/text/ClipboardManager;
    invoke-virtual {v0, p1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 20
    return-void
.end method
