.class public Ljp/co/drecom/bisque/lib/DRMoviePlayer;
.super Ljava/lang/Object;
.source "DRMoviePlayer.java"


# instance fields
.field private m_context:Landroid/app/Activity;

.field private m_height:I

.field private m_isPaused:Z

.field private m_isPausedOnSuspend:Z

.field private m_isSuspendSequence:Z

.field private m_layout:Landroid/widget/RelativeLayout;

.field private m_posOnPause:I

.field private m_source:Ljava/lang/String;

.field private m_uri:Landroid/net/Uri;

.field private m_videoView:Landroid/widget/VideoView;

.field private m_width:I

.field private m_x:I

.field private m_y:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V
    .locals 2
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPaused:Z

    .line 22
    iput-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_source:Ljava/lang/String;

    .line 23
    iput-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_uri:Landroid/net/Uri;

    .line 24
    iput v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_x:I

    .line 25
    iput v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_y:I

    .line 26
    iput v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_width:I

    .line 27
    iput v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_height:I

    .line 28
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isSuspendSequence:Z

    .line 29
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPausedOnSuspend:Z

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_posOnPause:I

    .line 38
    iput-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    .line 42
    iput-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_context:Landroid/app/Activity;

    .line 46
    iput-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_layout:Landroid/widget/RelativeLayout;

    .line 53
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_context:Landroid/app/Activity;

    .line 54
    iput-object p2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_layout:Landroid/widget/RelativeLayout;

    .line 55
    return-void
.end method

.method static synthetic access$000(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    .prologue
    .line 17
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isSuspendSequence:Z

    return v0
.end method

.method static synthetic access$002(Ljp/co/drecom/bisque/lib/DRMoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isSuspendSequence:Z

    return p1
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    .prologue
    .line 17
    iget v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_posOnPause:I

    return v0
.end method

.method static synthetic access$102(Ljp/co/drecom/bisque/lib/DRMoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 17
    iput p1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_posOnPause:I

    return p1
.end method

.method static synthetic access$200(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Landroid/widget/VideoView;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    .prologue
    .line 17
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$300(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    .prologue
    .line 17
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPausedOnSuspend:Z

    return v0
.end method

.method static synthetic access$302(Ljp/co/drecom/bisque/lib/DRMoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPausedOnSuspend:Z

    return p1
.end method

.method static synthetic access$402(Ljp/co/drecom/bisque/lib/DRMoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPaused:Z

    return p1
.end method

.method public static native nativeCallbackMovieFinished()V
.end method

.method public static native nativeCallbackMovieStarted()V
.end method


# virtual methods
.method public isPaused()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPaused:Z

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isSuspendSequence:Z

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->play()Z

    .line 75
    :cond_0
    return-void
.end method

.method public onSuspend()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    if-nez v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isSuspendSequence:Z

    .line 64
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->isPaused()Z

    move-result v0

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPausedOnSuspend:Z

    .line 65
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_posOnPause:I

    .line 67
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->stop()Z

    goto :goto_0
.end method

.method public pause()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 192
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    if-nez v2, :cond_1

    .line 202
    :cond_0
    :goto_0
    return v0

    .line 196
    :cond_1
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->canPause()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 201
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPaused:Z

    move v0, v1

    .line 202
    goto :goto_0
.end method

.method public play()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 105
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    if-eqz v3, :cond_1

    .line 107
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v3}, Landroid/widget/VideoView;->start()V

    .line 108
    iput-boolean v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPaused:Z

    .line 167
    :cond_0
    :goto_0
    return v1

    .line 112
    :cond_1
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_source:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_uri:Landroid/net/Uri;

    if-nez v3, :cond_2

    move v1, v2

    .line 114
    goto :goto_0

    .line 117
    :cond_2
    new-instance v2, Landroid/widget/VideoView;

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_context:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    .line 118
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_width:I

    iget v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_height:I

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 119
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_x:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 120
    iget v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_y:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 121
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v2, v0}, Landroid/widget/VideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_layout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v2, v1}, Landroid/widget/VideoView;->setZOrderOnTop(Z)V

    .line 125
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    new-instance v3, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;-><init>(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)V

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 152
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    new-instance v3, Ljp/co/drecom/bisque/lib/DRMoviePlayer$2;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer$2;-><init>(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)V

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 159
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_source:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 161
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_source:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_3
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_uri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 165
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_uri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public setFile(Ljava/lang/String;)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_source:Ljava/lang/String;

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_uri:Landroid/net/Uri;

    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public setRect(IIII)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 96
    iput p1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_x:I

    .line 97
    iput p2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_y:I

    .line 98
    iput p3, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_width:I

    .line 99
    iput p4, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_height:I

    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public setUri(Ljava/lang/String;)Z
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_source:Ljava/lang/String;

    .line 86
    if-nez p1, :cond_0

    .line 88
    const/4 v0, 0x0

    .line 91
    :goto_0
    return v0

    .line 90
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_uri:Landroid/net/Uri;

    .line 91
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stop()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 172
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    if-nez v1, :cond_0

    .line 187
    :goto_0
    return v0

    .line 177
    :cond_0
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->stopPlayback()V

    .line 178
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_layout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 179
    const/4 v1, 0x0

    iput-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_videoView:Landroid/widget/VideoView;

    .line 180
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isPaused:Z

    .line 183
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->m_isSuspendSequence:Z

    if-nez v0, :cond_1

    .line 185
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->nativeCallbackMovieFinished()V

    .line 187
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
