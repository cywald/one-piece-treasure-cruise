.class Ljp/co/drecom/bisque/lib/BQWebView$1;
.super Landroid/webkit/WebViewClient;
.source "BQWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQWebView;->initDispatcher(Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQWebView;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQWebView;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQWebView;

    .prologue
    .line 306
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 322
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/BQWebView;->requestFocus(I)Z

    .line 323
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->access$000(Ljp/co/drecom/bisque/lib/BQWebView;)Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    move-result-object v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    iget v1, v1, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    invoke-interface {v0, v1}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;->callbackForWebViewDidFinishLoad(I)V

    .line 324
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 317
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 329
    const-string v2, ""

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQWebView;->access$100(Ljp/co/drecom/bisque/lib/BQWebView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 330
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file:///android_asset/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQWebView;->access$100(Ljp/co/drecom/bisque/lib/BQWebView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 331
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 332
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file:///android_asset/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQWebView;->access$100(Ljp/co/drecom/bisque/lib/BQWebView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 338
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 340
    .local v1, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "FailingURL"

    invoke-virtual {v1, v2, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 341
    const-string v2, "ErrorDescription"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(errorCode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :goto_1
    invoke-virtual {p1}, Landroid/webkit/WebView;->goForward()V

    .line 345
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v2}, Ljp/co/drecom/bisque/lib/BQWebView;->access$000(Ljp/co/drecom/bisque/lib/BQWebView;)Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    move-result-object v2

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;->callbackForWebViewDidFailLoadWithError(Ljava/lang/String;)V

    .line 346
    return-void

    .line 334
    .end local v1    # "json":Lorg/json/JSONObject;
    .restart local v0    # "file":Ljava/io/File;
    :cond_1
    const-string v2, "<html><body style=\'background: black;\'><p style=\'color: red;\'>Unable to load webview error template file.</p></body></html>"

    const-string v3, "text/html"

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v4}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 342
    .end local v0    # "file":Ljava/io/File;
    .restart local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "handler"    # Landroid/webkit/HttpAuthHandler;
    .param p3, "host"    # Ljava/lang/String;
    .param p4, "realm"    # Ljava/lang/String;

    .prologue
    .line 351
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->access$200(Ljp/co/drecom/bisque/lib/BQWebView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->access$300(Ljp/co/drecom/bisque/lib/BQWebView;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v1}, Ljp/co/drecom/bisque/lib/BQWebView;->access$400(Ljp/co/drecom/bisque/lib/BQWebView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/webkit/HttpAuthHandler;->proceed(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 311
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQWebView;->access$000(Ljp/co/drecom/bisque/lib/BQWebView;)Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    move-result-object v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQWebView$1;->this$0:Ljp/co/drecom/bisque/lib/BQWebView;

    iget v1, v1, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    invoke-interface {v0, p2, v1}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;->callbackForWebViewShouldStartLoadWithRequest(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method
