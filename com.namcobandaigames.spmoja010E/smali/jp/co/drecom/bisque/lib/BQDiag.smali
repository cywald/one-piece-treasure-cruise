.class public Ljp/co/drecom/bisque/lib/BQDiag;
.super Ljava/lang/Object;
.source "BQDiag.java"


# static fields
.field private static final BQDIAG_RESULT_EXISTSAPP_FALSE:I = 0x0

.field private static final BQDIAG_RESULT_EXISTSAPP_TRUE:I = 0x1

.field private static final BQDIAG_RESULT_EXISTSAPP_UNKNOWN:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static existsPackage(Ljava/lang/String;)I
    .locals 7
    .param p0, "targetPath"    # Ljava/lang/String;

    .prologue
    .line 27
    const/4 v4, 0x0

    .line 29
    .local v4, "ret":I
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v6

    invoke-virtual {v6}, Ljp/co/drecom/bisque/lib/HandlerJava;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 30
    .local v1, "context":Landroid/content/Context;
    if-nez v1, :cond_0

    .line 32
    const/4 v4, -0x1

    move v5, v4

    .line 45
    .end local v4    # "ret":I
    .local v5, "ret":I
    :goto_0
    return v5

    .line 36
    .end local v5    # "ret":I
    .restart local v4    # "ret":I
    :cond_0
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 38
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v3, p0, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 39
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    const/4 v4, 0x1

    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :goto_1
    move v5, v4

    .line 45
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto :goto_0

    .line 41
    .end local v5    # "ret":I
    .restart local v4    # "ret":I
    :catch_0
    move-exception v2

    .line 42
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v4, 0x0

    goto :goto_1
.end method
