.class public Ljp/co/drecom/bisque/lib/DRBackgroundTaskReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DRBackgroundTaskReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 17
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 25
    .local v0, "action":Ljava/lang/String;
    const-string v3, "jp.co.drecom.bisque.lib.drbackgroundtask.broadcast.BGTASK"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 26
    const-string v3, "jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 27
    .local v2, "uuid":Ljava/lang/String;
    const-string v3, "jp.co.drecom.bisque.lib.drbackgroundtask.extra.STATUS"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "status":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "jp.co.drecom.bisque.lib.drbackgroundtask.status.FINISH"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 29
    invoke-static {v2}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->finish(Ljava/lang/String;)V

    .line 32
    .end local v1    # "status":Ljava/lang/String;
    .end local v2    # "uuid":Ljava/lang/String;
    :cond_0
    return-void
.end method
