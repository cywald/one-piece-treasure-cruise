.class public final enum Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
.super Ljava/lang/Enum;
.source "DRToast.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/DRToast;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ANCHOR_POS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

.field public static final enum BOTTOM:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

.field public static final enum CENTER:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

.field public static final enum TOP:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;


# instance fields
.field private m_value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v2, v2}, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->TOP:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    .line 39
    new-instance v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3, v3}, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->CENTER:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    .line 40
    new-instance v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4, v4}, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->BOTTOM:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    .line 36
    const/4 v0, 0x3

    new-array v0, v0, [Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    sget-object v1, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->TOP:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    aput-object v1, v0, v2

    sget-object v1, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->CENTER:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    aput-object v1, v0, v3

    sget-object v1, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->BOTTOM:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    aput-object v1, v0, v4

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->$VALUES:[Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->m_value:I

    .line 47
    return-void
.end method

.method public static toEnum(I)Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    .locals 5
    .param p0, "value"    # I

    .prologue
    .line 54
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->values()[Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    move-result-object v1

    .line 55
    .local v1, "enumValues":[Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 57
    .local v0, "enumValue":Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 63
    .end local v0    # "enumValue":Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    :goto_1
    return-object v0

    .line 55
    .restart local v0    # "enumValue":Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 63
    .end local v0    # "enumValue":Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    :cond_1
    sget-object v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->TOP:Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    const-class v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    return-object v0
.end method

.method public static values()[Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->$VALUES:[Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    invoke-virtual {v0}, [Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->m_value:I

    return v0
.end method
