.class Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;
.super Ljava/lang/Object;
.source "BQPaymentBridge.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQPaymentBridge;->setup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 459
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 477
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {p2}, Lcom/android/vending/billing/IInAppBillingService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v4

    invoke-static {v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$002(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/android/vending/billing/IInAppBillingService;)Lcom/android/vending/billing/IInAppBillingService;

    .line 478
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$400(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 483
    .local v1, "packageName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v3

    const/4 v4, 0x3

    const-string v5, "inapp"

    invoke-interface {v3, v4, v1, v5}, Lcom/android/vending/billing/IInAppBillingService;->isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 484
    .local v2, "response":I
    if-eqz v2, :cond_0

    .line 485
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    new-instance v4, Lcom/ex/android/util/IabResult;

    const-string v5, "Error checking for billing v3 support."

    invoke-direct {v4, v2, v5}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-static {v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$702(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/ex/android/util/IabResult;)Lcom/ex/android/util/IabResult;

    .line 500
    .end local v2    # "response":I
    :goto_0
    return-void

    .line 490
    .restart local v2    # "response":I
    :cond_0
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1502(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 498
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    new-instance v4, Lcom/ex/android/util/IabResult;

    const/4 v5, 0x0

    const-string v6, "Setup successful."

    invoke-direct {v4, v5, v6}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-static {v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$702(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/ex/android/util/IabResult;)Lcom/ex/android/util/IabResult;

    goto :goto_0

    .line 492
    .end local v2    # "response":I
    :catch_0
    move-exception v0

    .line 493
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    new-instance v4, Lcom/ex/android/util/IabResult;

    const/16 v5, -0x3e9

    const-string v6, "RemoteException while setting up in-app billing."

    invoke-direct {v4, v5, v6}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-static {v3, v4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$702(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/ex/android/util/IabResult;)Lcom/ex/android/util/IabResult;

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 463
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$002(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/android/vending/billing/IInAppBillingService;)Lcom/android/vending/billing/IInAppBillingService;

    .line 464
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1502(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Z)Z

    .line 466
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$400(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5$1;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5$1;-><init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 472
    return-void
.end method
