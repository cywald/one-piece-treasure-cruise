.class public Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify;
.super Ljava/lang/Object;
.source "BQUrlSchemeDispatcherNotify.java"


# static fields
.field private static dispatcher:Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcher;

.field private static handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcher;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcher;

    return-object v0
.end method

.method public static execOpenApplicationDetailsSettings()V
    .locals 2

    .prologue
    .line 29
    sget-object v0, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify$2;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify$2;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 35
    return-void
.end method

.method public static execUrlScheme(Ljava/lang/String;)V
    .locals 3
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 19
    move-object v0, p0

    .line 20
    .local v0, "_string":Ljava/lang/String;
    sget-object v1, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify$1;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify$1;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 26
    return-void
.end method

.method public static setDispatcher(Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcher;Landroid/os/Handler;)V
    .locals 0
    .param p0, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcher;
    .param p1, "_handler"    # Landroid/os/Handler;

    .prologue
    .line 13
    sput-object p0, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcher;

    .line 14
    sput-object p1, Ljp/co/drecom/bisque/lib/BQUrlSchemeDispatcherNotify;->handler:Landroid/os/Handler;

    .line 15
    return-void
.end method
