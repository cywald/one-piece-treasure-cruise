.class public Ljp/co/drecom/bisque/lib/BQHandlerDispatcherNotify;
.super Ljava/lang/Object;
.source "BQHandlerDispatcherNotify.java"


# static fields
.field public static dispatcher:Ljp/co/drecom/bisque/lib/BQHandlerDispatcher;

.field public static handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static handlerPost(JJI)V
    .locals 8
    .param p0, "func"    # J
    .param p2, "data"    # J
    .param p4, "type"    # I

    .prologue
    .line 22
    move-wide v2, p0

    .local v2, "_func":J
    move-wide v4, p2

    .line 23
    .local v4, "_data":J
    move v6, p4

    .line 24
    .local v6, "_type":I
    sget-object v0, Ljp/co/drecom/bisque/lib/BQHandlerDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQHandlerDispatcherNotify$1;

    invoke-direct/range {v1 .. v6}, Ljp/co/drecom/bisque/lib/BQHandlerDispatcherNotify$1;-><init>(JJI)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 30
    return-void
.end method

.method public static setDispatcher(Ljp/co/drecom/bisque/lib/BQHandlerDispatcher;Landroid/os/Handler;)V
    .locals 0
    .param p0, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQHandlerDispatcher;
    .param p1, "_handler"    # Landroid/os/Handler;

    .prologue
    .line 16
    sput-object p0, Ljp/co/drecom/bisque/lib/BQHandlerDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQHandlerDispatcher;

    .line 17
    sput-object p1, Ljp/co/drecom/bisque/lib/BQHandlerDispatcherNotify;->handler:Landroid/os/Handler;

    .line 18
    return-void
.end method
