.class public Ljp/co/drecom/bisque/lib/BQTypefaces;
.super Ljava/lang/Object;
.source "BQTypefaces.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;
    }
.end annotation


# static fields
.field private static m_implement:Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/BQTypefaces;->m_implement:Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 1
    .param p0, "fontName"    # Ljava/lang/String;

    .prologue
    .line 30
    sget-object v0, Ljp/co/drecom/bisque/lib/BQTypefaces;->m_implement:Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;

    if-eqz v0, :cond_0

    .line 32
    sget-object v0, Ljp/co/drecom/bisque/lib/BQTypefaces;->m_implement:Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;

    invoke-interface {v0, p0}, Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static registImplement(Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;)V
    .locals 0
    .param p0, "bqtypefaces"    # Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;

    .prologue
    .line 26
    sput-object p0, Ljp/co/drecom/bisque/lib/BQTypefaces;->m_implement:Ljp/co/drecom/bisque/lib/BQTypefaces$Implement;

    .line 27
    return-void
.end method
