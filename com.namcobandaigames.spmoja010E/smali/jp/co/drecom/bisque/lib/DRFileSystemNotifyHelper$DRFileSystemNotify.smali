.class Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;
.super Landroid/os/FileObserver;
.source "DRFileSystemNotifyHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DRFileSystemNotify"
.end annotation


# static fields
.field private static final NOTIFY_UPDATE:Ljava/lang/String; = "NOTIFY_UPDATE"


# instance fields
.field private monitoredPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;)V

    .line 115
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;->monitoredPath:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mask"    # I

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    .line 124
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;->monitoredPath:Ljava/lang/String;

    .line 125
    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 2
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 134
    .local v0, "eventString":Ljava/lang/String;
    const/16 v1, 0x100

    if-eq p1, v1, :cond_0

    const/16 v1, 0x200

    if-ne p1, v1, :cond_1

    .line 135
    :cond_0
    const-string v0, "NOTIFY_UPDATE"

    .line 136
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;->monitoredPath:Ljava/lang/String;

    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->access$000(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_1
    return-void
.end method
