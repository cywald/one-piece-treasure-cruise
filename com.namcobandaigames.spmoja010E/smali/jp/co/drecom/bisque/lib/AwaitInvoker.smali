.class public final Ljp/co/drecom/bisque/lib/AwaitInvoker;
.super Ljava/lang/Object;
.source "AwaitInvoker.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private m_runnable:Ljava/lang/Runnable;

.field private m_signal:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/AwaitInvoker;->m_runnable:Ljava/lang/Runnable;

    .line 19
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/AwaitInvoker;->m_signal:Ljava/util/concurrent/CountDownLatch;

    .line 20
    return-void
.end method


# virtual methods
.method public invokeAndWait(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    .locals 6
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "runnable"    # Ljava/lang/Runnable;
    .param p3, "maxWaitTime"    # I

    .prologue
    .line 39
    const/4 v1, 0x0

    .line 40
    .local v1, "succeeded":Z
    iput-object p2, p0, Ljp/co/drecom/bisque/lib/AwaitInvoker;->m_runnable:Ljava/lang/Runnable;

    .line 41
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v2, p0, Ljp/co/drecom/bisque/lib/AwaitInvoker;->m_signal:Ljava/util/concurrent/CountDownLatch;

    .line 43
    invoke-virtual {p1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 45
    :try_start_0
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/AwaitInvoker;->m_signal:Ljava/util/concurrent/CountDownLatch;

    int-to-long v4, p3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 49
    :goto_0
    return v1

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/InterruptedException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 25
    :try_start_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/AwaitInvoker;->m_runnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/AwaitInvoker;->m_signal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 29
    return-void

    .line 27
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/AwaitInvoker;->m_signal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 28
    throw v0
.end method
