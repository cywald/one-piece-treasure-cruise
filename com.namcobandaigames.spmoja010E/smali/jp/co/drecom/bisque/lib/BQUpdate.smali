.class public Ljp/co/drecom/bisque/lib/BQUpdate;
.super Ljava/lang/Object;
.source "BQUpdate.java"


# static fields
.field private static final BQUPDATE_CHECKUPDATE_ERROR:I = 0x2

.field private static final BQUPDATE_CHECKUPDATE_NO:I = 0x1

.field private static final BQUPDATE_CHECKUPDATE_YES:I


# instance fields
.field private latestVersionCode:I

.field private packageName:Ljava/lang/String;

.field private parent:Landroid/app/Activity;

.field private versionCode:I

.field private versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->packageName:Ljava/lang/String;

    .line 25
    iput v1, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->latestVersionCode:I

    .line 26
    iput v1, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->versionCode:I

    .line 27
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->versionName:Ljava/lang/String;

    .line 35
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "ctx":Landroid/content/Context;
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->parent:Landroid/app/Activity;

    .line 36
    return-void
.end method

.method public static native nativeCallbackFuncForCheckUpdate(I)V
.end method


# virtual methods
.method public checkUpdate(ZILjava/lang/String;)V
    .locals 10
    .param p1, "bqnotify"    # Z
    .param p2, "android_latestversion"    # I
    .param p3, "android_update_uri"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 42
    iput p2, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->latestVersionCode:I

    .line 43
    move-object v0, p3

    .line 46
    .local v0, "_android_update_uri":Ljava/lang/String;
    iget-object v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->parent:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 48
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    iget-object v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->parent:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 49
    .local v4, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v6, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->packageName:Ljava/lang/String;

    .line 50
    iget v6, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->versionCode:I

    .line 51
    iget-object v6, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    if-ne p1, v8, :cond_0

    .line 67
    :cond_0
    if-nez p1, :cond_1

    .line 72
    :cond_1
    iget v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->latestVersionCode:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    .line 76
    const/16 v6, 0xa

    iput v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->latestVersionCode:I

    .line 80
    :cond_2
    iget v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->versionCode:I

    iget v7, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->latestVersionCode:I

    if-ge v6, v7, :cond_4

    .line 84
    invoke-static {v9}, Ljp/co/drecom/bisque/lib/BQUpdate;->nativeCallbackFuncForCheckUpdate(I)V

    .line 85
    if-ne p1, v8, :cond_3

    .line 88
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->parent:Landroid/app/Activity;

    invoke-direct {v2, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 89
    .local v2, "al_db":Landroid/app/AlertDialog$Builder;
    sget v6, Ljp/co/drecom/bisque/lib/R$string;->jp_co_drecom_bqupdate_dialog_title:I

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 90
    sget v6, Ljp/co/drecom/bisque/lib/R$string;->jp_co_drecom_bqupdate_dialog_message:I

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 91
    sget v6, Ljp/co/drecom/bisque/lib/R$string;->jp_co_drecom_common_label_ok:I

    new-instance v7, Ljp/co/drecom/bisque/lib/BQUpdate$1;

    invoke-direct {v7, p0, v0}, Ljp/co/drecom/bisque/lib/BQUpdate$1;-><init>(Ljp/co/drecom/bisque/lib/BQUpdate;Ljava/lang/String;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 99
    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 100
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 101
    .local v1, "al_d":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 110
    .end local v1    # "al_d":Landroid/app/AlertDialog;
    .end local v2    # "al_db":Landroid/app/AlertDialog$Builder;
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_3
    :goto_0
    return-void

    .line 52
    :catch_0
    move-exception v3

    .line 53
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 54
    const/4 v6, 0x2

    invoke-static {v6}, Ljp/co/drecom/bisque/lib/BQUpdate;->nativeCallbackFuncForCheckUpdate(I)V

    goto :goto_0

    .line 108
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_4
    invoke-static {v8}, Ljp/co/drecom/bisque/lib/BQUpdate;->nativeCallbackFuncForCheckUpdate(I)V

    goto :goto_0
.end method

.method public execReview(Ljava/lang/String;)V
    .locals 4
    .param p1, "android_review_uri"    # Ljava/lang/String;

    .prologue
    .line 128
    move-object v1, p1

    .line 129
    .local v1, "reviewuri":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 132
    .local v2, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 133
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->parent:Landroid/app/Activity;

    invoke-virtual {v3, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 134
    return-void
.end method

.method public execUpdate(Ljava/lang/String;)V
    .locals 4
    .param p1, "android_update_uri"    # Ljava/lang/String;

    .prologue
    .line 116
    move-object v1, p1

    .line 117
    .local v1, "updateuri":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 120
    .local v2, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 121
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQUpdate;->parent:Landroid/app/Activity;

    invoke-virtual {v3, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 122
    return-void
.end method
