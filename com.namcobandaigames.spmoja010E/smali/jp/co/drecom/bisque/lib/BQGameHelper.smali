.class public Ljp/co/drecom/bisque/lib/BQGameHelper;
.super Ljava/lang/Object;
.source "BQGameHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;
    }
.end annotation


# static fields
.field private static final RC_ACHIEVEMENT_UI:I = 0x232b

.field private static final RC_SIGN_IN:I = 0x2329

.field private static final RC_UNUSED:I = 0x1389

.field private static final TAG:Ljava/lang/String; = "BQGameHelper"


# instance fields
.field private mAchievementsClient:Lcom/google/android/gms/games/AchievementsClient;

.field mActivity:Landroid/app/Activity;

.field mAppContext:Landroid/content/Context;

.field private mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

.field mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    .line 37
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mAppContext:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    .line 39
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mAchievementsClient:Lcom/google/android/gms/games/AchievementsClient;

    .line 59
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    .line 66
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    .line 67
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mAppContext:Landroid/content/Context;

    .line 68
    return-void
.end method

.method static synthetic access$000(Ljp/co/drecom/bisque/lib/BQGameHelper;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQGameHelper;
    .param p1, "x1"    # Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Ljp/co/drecom/bisque/lib/BQGameHelper;->onConnected(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    return-void
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/BQGameHelper;)V
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQGameHelper;

    .prologue
    .line 31
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQGameHelper;->onDisconnected()V

    return-void
.end method

.method private onConnected(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    .locals 1
    .param p1, "googleSignInAccount"    # Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    .prologue
    .line 150
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/Games;->getAchievementsClient(Landroid/app/Activity;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)Lcom/google/android/gms/games/AchievementsClient;

    move-result-object v0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mAchievementsClient:Lcom/google/android/gms/games/AchievementsClient;

    .line 151
    return-void
.end method

.method private onDisconnected()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mAchievementsClient:Lcom/google/android/gms/games/AchievementsClient;

    .line 155
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    invoke-interface {v0}, Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;->onDisconnected()V

    .line 156
    :cond_0
    return-void
.end method

.method private signInSilently()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    if-nez v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;->silentSignIn()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQGameHelper$1;

    invoke-direct {v2, p0}, Ljp/co/drecom/bisque/lib/BQGameHelper$1;-><init>(Ljp/co/drecom/bisque/lib/BQGameHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Landroid/app/Activity;Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    goto :goto_0
.end method


# virtual methods
.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getLastSignedInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 129
    const/16 v2, 0x2329

    if-ne p1, v2, :cond_3

    .line 130
    sget-object v2, Lcom/google/android/gms/auth/api/Auth;->GoogleSignInApi:Lcom/google/android/gms/auth/api/signin/GoogleSignInApi;

    invoke-interface {v2, p3}, Lcom/google/android/gms/auth/api/signin/GoogleSignInApi;->getSignInResultFromIntent(Landroid/content/Intent;)Lcom/google/android/gms/auth/api/signin/GoogleSignInResult;

    move-result-object v0

    .line 131
    .local v0, "result":Lcom/google/android/gms/auth/api/signin/GoogleSignInResult;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInResult;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 133
    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInResult;->getSignInAccount()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v1

    .line 134
    .local v1, "signedInAccount":Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
    invoke-direct {p0, v1}, Ljp/co/drecom/bisque/lib/BQGameHelper;->onConnected(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    .line 135
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    invoke-interface {v2, v1}, Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;->onInteractiveSignInSucceeded(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    .line 147
    .end local v0    # "result":Lcom/google/android/gms/auth/api/signin/GoogleSignInResult;
    .end local v1    # "signedInAccount":Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;
    :cond_0
    :goto_0
    return-void

    .line 138
    .restart local v0    # "result":Lcom/google/android/gms/auth/api/signin/GoogleSignInResult;
    :cond_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    if-eqz v2, :cond_2

    .line 139
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-interface {v2, v3}, Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;->onInteractiveSignInFailed(Lcom/google/android/gms/common/api/Status;)V

    .line 141
    :cond_2
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQGameHelper;->onDisconnected()V

    goto :goto_0

    .line 144
    .end local v0    # "result":Lcom/google/android/gms/auth/api/signin/GoogleSignInResult;
    :cond_3
    const/16 v2, 0x232b

    if-ne p1, v2, :cond_0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQGameHelper;->signInSilently()V

    .line 114
    return-void
.end method

.method public setup(Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;)V
    .locals 3
    .param p1, "listener"    # Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    .prologue
    .line 75
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mListener:Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;

    .line 78
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;

    sget-object v2, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;->DEFAULT_GAMES_SIGN_IN:Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    invoke-direct {v1, v2}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;-><init>(Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)V

    .line 79
    invoke-virtual {v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;->build()Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    move-result-object v1

    .line 78
    invoke-static {v0, v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getClient(Landroid/app/Activity;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    move-result-object v0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    .line 80
    return-void
.end method

.method public showAchievements()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getLastSignedInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/Games;->getAchievementsClient(Landroid/app/Activity;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)Lcom/google/android/gms/games/AchievementsClient;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Lcom/google/android/gms/games/AchievementsClient;->getAchievementsIntent()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQGameHelper$3;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQGameHelper$3;-><init>(Ljp/co/drecom/bisque/lib/BQGameHelper;)V

    .line 179
    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    .line 185
    return-void
.end method

.method public signOut()V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    if-nez v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQGameHelper;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;->signOut()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQGameHelper$2;

    invoke-direct {v2, p0}, Ljp/co/drecom/bisque/lib/BQGameHelper$2;-><init>(Ljp/co/drecom/bisque/lib/BQGameHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Landroid/app/Activity;Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    goto :goto_0
.end method

.method public startSignInIntent()V
    .locals 3

    .prologue
    .line 120
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    if-nez v1, :cond_0

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mGoogleSignInClient:Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;->getSignInIntent()Landroid/content/Intent;

    move-result-object v0

    .line 122
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    const/16 v2, 0x2329

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public unlockAchievement(Ljava/lang/String;)V
    .locals 2
    .param p1, "achId"    # Ljava/lang/String;

    .prologue
    .line 172
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGameHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getLastSignedInAccount(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/Games;->getAchievementsClient(Landroid/app/Activity;Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)Lcom/google/android/gms/games/AchievementsClient;

    move-result-object v0

    .line 173
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/AchievementsClient;->unlock(Ljava/lang/String;)V

    .line 174
    return-void
.end method
