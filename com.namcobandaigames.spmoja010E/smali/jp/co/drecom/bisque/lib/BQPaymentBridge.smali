.class public Ljp/co/drecom/bisque/lib/BQPaymentBridge;
.super Ljava/lang/Object;
.source "BQPaymentBridge.java"


# static fields
.field public static final BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE:I = 0x3

.field public static final BILLING_RESPONSE_RESULT_DEVELOPER_ERROR:I = 0x5

.field public static final BILLING_RESPONSE_RESULT_ERROR:I = 0x6

.field public static final BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED:I = 0x7

.field public static final BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED:I = 0x8

.field public static final BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE:I = 0x4

.field public static final BILLING_RESPONSE_RESULT_OK:I = 0x0

.field public static final BILLING_RESPONSE_RESULT_USER_CANCELED:I = 0x1

.field public static final GET_SKU_DETAILS_ITEM_LIST:Ljava/lang/String; = "ITEM_ID_LIST"

.field public static final GET_SKU_DETAILS_ITEM_TYPE_LIST:Ljava/lang/String; = "ITEM_TYPE_LIST"

.field public static final IABHELPER_BAD_RESPONSE:I = -0x3ea

.field public static final IABHELPER_ERROR_BASE:I = -0x3e8

.field public static final IABHELPER_INVALID_CONSUMPTION:I = -0x3f2

.field public static final IABHELPER_MISSING_TOKEN:I = -0x3ef

.field public static final IABHELPER_REMOTE_EXCEPTION:I = -0x3e9

.field public static final IABHELPER_SEND_INTENT_FAILED:I = -0x3ec

.field public static final IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE:I = -0x3f1

.field public static final IABHELPER_UNKNOWN_ERROR:I = -0x3f0

.field public static final IABHELPER_UNKNOWN_PURCHASE_RESPONSE:I = -0x3ee

.field public static final IABHELPER_USER_CANCELLED:I = -0x3ed

.field public static final IABHELPER_VERIFICATION_FAILED:I = -0x3eb

.field public static final INAPP_CONTINUATION_TOKEN:Ljava/lang/String; = "INAPP_CONTINUATION_TOKEN"

.field public static final ITEM_TYPE_INAPP:Ljava/lang/String; = "inapp"

.field public static final ITEM_TYPE_SUBS:Ljava/lang/String; = "subs"

.field private static final LOG_TAG:Ljava/lang/String; = "BQPayment"

.field public static final RESPONSE_BUY_INTENT:Ljava/lang/String; = "BUY_INTENT"

.field public static final RESPONSE_CODE:Ljava/lang/String; = "RESPONSE_CODE"

.field public static final RESPONSE_GET_SKU_DETAILS_LIST:Ljava/lang/String; = "DETAILS_LIST"

.field public static final RESPONSE_INAPP_ITEM_LIST:Ljava/lang/String; = "INAPP_PURCHASE_ITEM_LIST"

.field public static final RESPONSE_INAPP_PURCHASE_DATA:Ljava/lang/String; = "INAPP_PURCHASE_DATA"

.field public static final RESPONSE_INAPP_PURCHASE_DATA_LIST:Ljava/lang/String; = "INAPP_PURCHASE_DATA_LIST"

.field public static final RESPONSE_INAPP_SIGNATURE:Ljava/lang/String; = "INAPP_DATA_SIGNATURE"

.field public static final RESPONSE_INAPP_SIGNATURE_LIST:Ljava/lang/String; = "INAPP_DATA_SIGNATURE_LIST"

.field private static instance:Ljp/co/drecom/bisque/lib/BQPaymentBridge;


# instance fields
.field private context:Landroid/app/Activity;

.field private currentRequestIdentifier:J

.field private handler:Landroid/os/Handler;

.field private iabService:Lcom/android/vending/billing/IInAppBillingService;

.field private productIdentifiers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/ex/android/util/SkuDetails;",
            ">;"
        }
    .end annotation
.end field

.field private purchases:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/ex/android/util/Purchase;",
            ">;"
        }
    .end annotation
.end field

.field private reason:Lcom/ex/android/util/IabResult;

.field private serviceConnection:Landroid/content/ServiceConnection;

.field private setuped:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->instance:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->productIdentifiers:Ljava/util/Map;

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->purchases:Ljava/util/Map;

    .line 440
    return-void
.end method

.method static synthetic access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 42
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->iabService:Lcom/android/vending/billing/IInAppBillingService;

    return-object v0
.end method

.method static synthetic access$002(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/android/vending/billing/IInAppBillingService;)Lcom/android/vending/billing/IInAppBillingService;
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;
    .param p1, "x1"    # Lcom/android/vending/billing/IInAppBillingService;

    .prologue
    .line 42
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->iabService:Lcom/android/vending/billing/IInAppBillingService;

    return-object p1
.end method

.method static synthetic access$100(ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # I
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0, p1}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailLoadingProductList(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # I
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0, p1}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailLoadingPurchaseList(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 42
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->syncPurchasesToNative()V

    return-void
.end method

.method static synthetic access$1200()V
    .locals 0

    .prologue
    .line 42
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFinishLoadingPurchaseList()V

    return-void
.end method

.method static synthetic access$1300(JILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # I
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailConsumePurchase(JILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0, p1, p2, p3, p4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFinishConsumePurchase(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1502(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Z)Z
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->setuped:Z

    return p1
.end method

.method static synthetic access$200(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 42
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->productIdentifiers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300()V
    .locals 0

    .prologue
    .line 42
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFinishLoadingProductList()V

    return-void
.end method

.method static synthetic access$400(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 42
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 42
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->syncProductsToNative()V

    return-void
.end method

.method static synthetic access$600(JLjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # I
    .param p4, "x3"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0, p1, p2, p3, p4}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailPurchaseProduct(JLjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/ex/android/util/IabResult;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 42
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->reason:Lcom/ex/android/util/IabResult;

    return-object v0
.end method

.method static synthetic access$702(Ljp/co/drecom/bisque/lib/BQPaymentBridge;Lcom/ex/android/util/IabResult;)Lcom/ex/android/util/IabResult;
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;
    .param p1, "x1"    # Lcom/ex/android/util/IabResult;

    .prologue
    .line 42
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->reason:Lcom/ex/android/util/IabResult;

    return-object p1
.end method

.method static synthetic access$802(Ljp/co/drecom/bisque/lib/BQPaymentBridge;J)J
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;
    .param p1, "x1"    # J

    .prologue
    .line 42
    iput-wide p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    return-wide p1
.end method

.method static synthetic access$900(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 42
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->purchases:Ljava/util/Map;

    return-object v0
.end method

.method public static getBridge()Ljp/co/drecom/bisque/lib/BQPaymentBridge;
    .locals 1

    .prologue
    .line 442
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->instance:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    return-object v0
.end method

.method public static getResponseDesc(I)Ljava/lang/String;
    .locals 5
    .param p0, "code"    # I

    .prologue
    .line 656
    const-string v3, "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned"

    const-string v4, "/"

    .line 659
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 660
    .local v0, "iab_msgs":[Ljava/lang/String;
    const-string v3, "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt"

    const-string v4, "/"

    .line 669
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 671
    .local v1, "iabhelper_msgs":[Ljava/lang/String;
    const/16 v3, -0x3e8

    if-gt p0, v3, :cond_1

    .line 672
    rsub-int v2, p0, -0x3e8

    .line 673
    .local v2, "index":I
    if-ltz v2, :cond_0

    array-length v3, v1

    if-ge v2, v3, :cond_0

    aget-object v3, v1, v2

    .line 679
    .end local v2    # "index":I
    :goto_0
    return-object v3

    .line 674
    .restart local v2    # "index":I
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Unknown IAB Helper Error"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 676
    .end local v2    # "index":I
    :cond_1
    if-ltz p0, :cond_2

    array-length v3, v0

    if-lt p0, v3, :cond_3

    .line 677
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Unknown"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 679
    :cond_3
    aget-object v3, v0, p0

    goto :goto_0
.end method

.method private static native nativeCallbackFuncForDidFailConsumePurchase(JILjava/lang/String;)V
.end method

.method private static native nativeCallbackFuncForDidFailLoadingProductList(ILjava/lang/String;)V
.end method

.method private static native nativeCallbackFuncForDidFailLoadingPurchaseList(ILjava/lang/String;)V
.end method

.method private static native nativeCallbackFuncForDidFailPurchaseProduct(JLjava/lang/String;ILjava/lang/String;)V
.end method

.method private static native nativeCallbackFuncForDidFinishConsumePurchase(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static native nativeCallbackFuncForDidFinishLoadingProductList()V
.end method

.method private static native nativeCallbackFuncForDidFinishLoadingPurchaseList()V
.end method

.method private static native nativeCallbackFuncForDidFinishPurchaseProduct(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static native nativeCallbackFuncForLoadProduct(Ljava/lang/String;ZLjava/lang/String;FLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static native nativeCallbackFuncForLoadPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private showDialogNotFoundSevice()V
    .locals 4

    .prologue
    .line 521
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 522
    .local v1, "al_db":Landroid/app/AlertDialog$Builder;
    sget v2, Ljp/co/drecom/bisque/lib/R$string;->jp_co_drecom_bqpayment_notfoundservice_dialog_title:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 523
    sget v2, Ljp/co/drecom/bisque/lib/R$string;->jp_co_drecom_bqpayment_notfoundservice_dialog_message:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 524
    sget v2, Ljp/co/drecom/bisque/lib/R$string;->jp_co_drecom_common_label_Exit:I

    new-instance v3, Ljp/co/drecom/bisque/lib/BQPaymentBridge$6;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge$6;-><init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 531
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 532
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 533
    .local v0, "al_d":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 534
    return-void
.end method

.method private syncProductsToNative()V
    .locals 10

    .prologue
    .line 367
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->productIdentifiers:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 368
    .local v0, "sku":Ljava/lang/String;
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->productIdentifiers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ex/android/util/SkuDetails;

    .line 370
    .local v8, "d":Lcom/ex/android/util/SkuDetails;
    if-eqz v8, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-nez v8, :cond_1

    const-string v2, "UNK"

    .line 373
    :goto_2
    if-nez v8, :cond_2

    const/4 v3, 0x0

    .line 374
    :goto_3
    if-nez v8, :cond_3

    const-string v4, ""

    .line 375
    :goto_4
    if-nez v8, :cond_4

    const-string v5, ""

    .line 376
    :goto_5
    if-nez v8, :cond_5

    const-string v6, ""

    .line 377
    :goto_6
    if-nez v8, :cond_6

    const-string v7, ""

    .line 370
    :goto_7
    invoke-static/range {v0 .. v7}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForLoadProduct(Ljava/lang/String;ZLjava/lang/String;FLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 373
    :cond_1
    invoke-virtual {v8}, Lcom/ex/android/util/SkuDetails;->getPriceCurrencyCode()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 374
    :cond_2
    invoke-virtual {v8}, Lcom/ex/android/util/SkuDetails;->getPriceValueFloat()F

    move-result v3

    goto :goto_3

    .line 375
    :cond_3
    invoke-virtual {v8}, Lcom/ex/android/util/SkuDetails;->getPriceValueString()Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 376
    :cond_4
    invoke-virtual {v8}, Lcom/ex/android/util/SkuDetails;->getPrice()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 377
    :cond_5
    invoke-virtual {v8}, Lcom/ex/android/util/SkuDetails;->getTitle()Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 378
    :cond_6
    invoke-virtual {v8}, Lcom/ex/android/util/SkuDetails;->getDescription()Ljava/lang/String;

    move-result-object v7

    goto :goto_7

    .line 380
    .end local v0    # "sku":Ljava/lang/String;
    .end local v8    # "d":Lcom/ex/android/util/SkuDetails;
    :cond_7
    return-void
.end method

.method private syncPurchasesToNative()V
    .locals 6

    .prologue
    .line 383
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->purchases:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 384
    .local v1, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/ex/android/util/Purchase;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ex/android/util/Purchase;

    .line 385
    .local v0, "p":Lcom/ex/android/util/Purchase;
    if-eqz v0, :cond_0

    .line 387
    invoke-virtual {v0}, Lcom/ex/android/util/Purchase;->getSku()Ljava/lang/String;

    move-result-object v3

    .line 388
    invoke-virtual {v0}, Lcom/ex/android/util/Purchase;->getToken()Ljava/lang/String;

    move-result-object v4

    .line 389
    invoke-virtual {v0}, Lcom/ex/android/util/Purchase;->getSignature()Ljava/lang/String;

    move-result-object v5

    .line 386
    invoke-static {v3, v4, v5}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForLoadPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 391
    .end local v0    # "p":Lcom/ex/android/util/Purchase;
    .end local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/ex/android/util/Purchase;>;"
    :cond_1
    return-void
.end method


# virtual methods
.method public consumePurchase(JLjava/lang/String;)V
    .locals 3
    .param p1, "requestIdentifier"    # J
    .param p3, "token"    # Ljava/lang/String;

    .prologue
    .line 310
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;

    invoke-direct {v1, p0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;-><init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 364
    return-void
.end method

.method public dispose()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 537
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->serviceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_1

    .line 539
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 540
    :cond_0
    iput-object v2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->serviceConnection:Landroid/content/ServiceConnection;

    .line 541
    iput-object v2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->iabService:Lcom/android/vending/billing/IInAppBillingService;

    .line 543
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->setuped:Z

    .line 544
    return-void
.end method

.method getResponseCodeFromBundle(Landroid/os/Bundle;)I
    .locals 4
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 642
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 643
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 645
    const/4 v1, 0x0

    .line 648
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 647
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 648
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 652
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type for bundle response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method getResponseCodeFromIntent(Landroid/content/Intent;)I
    .locals 4
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 627
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 628
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 630
    const/4 v1, 0x0

    .line 633
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 632
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 633
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 637
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type for intent response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public handleActivityResult(IILandroid/content/Intent;)Z
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v11, -0x1

    const/16 v10, -0x3ea

    const/4 v6, 0x1

    .line 555
    const/4 v5, 0x0

    .line 561
    .local v5, "result":Lcom/ex/android/util/IabResult;
    iget-wide v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    long-to-int v7, v8

    if-eq p1, v7, :cond_0

    const/4 v6, 0x0

    .line 623
    :goto_0
    return v6

    .line 563
    :cond_0
    if-nez p3, :cond_1

    .line 565
    new-instance v5, Lcom/ex/android/util/IabResult;

    .end local v5    # "result":Lcom/ex/android/util/IabResult;
    const-string v7, "Null data in IAB result"

    invoke-direct {v5, v10, v7}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 566
    .restart local v5    # "result":Lcom/ex/android/util/IabResult;
    iget-wide v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    const-string v7, ""

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v10

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v7, v10, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailPurchaseProduct(JLjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 570
    :cond_1
    invoke-virtual {p0, p3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->getResponseCodeFromIntent(Landroid/content/Intent;)I

    move-result v4

    .line 571
    .local v4, "responseCode":I
    const-string v7, "INAPP_PURCHASE_DATA"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 572
    .local v3, "purchaseData":Ljava/lang/String;
    const-string v7, "INAPP_DATA_SIGNATURE"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 574
    .local v0, "dataSignature":Ljava/lang/String;
    if-ne p2, v11, :cond_4

    if-nez v4, :cond_4

    .line 581
    if-eqz v3, :cond_2

    if-nez v0, :cond_3

    .line 584
    :cond_2
    new-instance v5, Lcom/ex/android/util/IabResult;

    .end local v5    # "result":Lcom/ex/android/util/IabResult;
    const/16 v7, -0x3f0

    const-string v8, "IAB returned null purchaseData or dataSignature"

    invoke-direct {v5, v7, v8}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 585
    .restart local v5    # "result":Lcom/ex/android/util/IabResult;
    iget-wide v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    const-string v7, ""

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v10

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v7, v10, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailPurchaseProduct(JLjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 589
    :cond_3
    const/4 v2, 0x0

    .line 591
    .local v2, "purchase":Lcom/ex/android/util/Purchase;
    :try_start_0
    new-instance v2, Lcom/ex/android/util/Purchase;

    .end local v2    # "purchase":Lcom/ex/android/util/Purchase;
    const-string v7, "inapp"

    invoke-direct {v2, v7, v3, v0}, Lcom/ex/android/util/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    .restart local v2    # "purchase":Lcom/ex/android/util/Purchase;
    iget-wide v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    .line 602
    invoke-virtual {v2}, Lcom/ex/android/util/Purchase;->getSku()Ljava/lang/String;

    move-result-object v7

    .line 603
    invoke-virtual {v2}, Lcom/ex/android/util/Purchase;->getOriginalJson()Ljava/lang/String;

    move-result-object v10

    .line 604
    invoke-virtual {v2}, Lcom/ex/android/util/Purchase;->getSignature()Ljava/lang/String;

    move-result-object v11

    .line 600
    invoke-static {v8, v9, v7, v10, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFinishPurchaseProduct(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 593
    .end local v2    # "purchase":Lcom/ex/android/util/Purchase;
    :catch_0
    move-exception v1

    .line 595
    .local v1, "e":Lorg/json/JSONException;
    new-instance v5, Lcom/ex/android/util/IabResult;

    .end local v5    # "result":Lcom/ex/android/util/IabResult;
    const-string v7, "Failed to parse purchase data."

    invoke-direct {v5, v10, v7}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 596
    .restart local v5    # "result":Lcom/ex/android/util/IabResult;
    iget-wide v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    const-string v7, ""

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v10

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v7, v10, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailPurchaseProduct(JLjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 606
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_4
    if-ne p2, v11, :cond_5

    .line 609
    new-instance v5, Lcom/ex/android/util/IabResult;

    .end local v5    # "result":Lcom/ex/android/util/IabResult;
    const-string v7, "Problem purchashing item."

    invoke-direct {v5, v4, v7}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 610
    .restart local v5    # "result":Lcom/ex/android/util/IabResult;
    iget-wide v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    const-string v7, ""

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v10

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v7, v10, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailPurchaseProduct(JLjava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 612
    :cond_5
    if-nez p2, :cond_6

    .line 614
    new-instance v5, Lcom/ex/android/util/IabResult;

    .end local v5    # "result":Lcom/ex/android/util/IabResult;
    const/16 v7, -0x3ed

    const-string v8, "User canceled."

    invoke-direct {v5, v7, v8}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 615
    .restart local v5    # "result":Lcom/ex/android/util/IabResult;
    iget-wide v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    const-string v7, ""

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v10

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v7, v10, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailPurchaseProduct(JLjava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 620
    :cond_6
    new-instance v5, Lcom/ex/android/util/IabResult;

    .end local v5    # "result":Lcom/ex/android/util/IabResult;
    const/16 v7, -0x3ee

    const-string v8, "Unknown purchase response."

    invoke-direct {v5, v7, v8}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 621
    .restart local v5    # "result":Lcom/ex/android/util/IabResult;
    iget-wide v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->currentRequestIdentifier:J

    const-string v7, ""

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getResponse()I

    move-result v10

    invoke-virtual {v5}, Lcom/ex/android/util/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v7, v10, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->nativeCallbackFuncForDidFailPurchaseProduct(JLjava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public isAvailablePayment()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->setuped:Z

    return v0
.end method

.method public requestPayment(JLjava/lang/String;I)V
    .locals 3
    .param p1, "requestIdentifier"    # J
    .param p3, "productIdentifier"    # Ljava/lang/String;
    .param p4, "quantity"    # I

    .prologue
    .line 184
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;

    invoke-direct {v1, p0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge$2;-><init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 232
    return-void
.end method

.method public requestPaymentDetails([Ljava/lang/String;)V
    .locals 2
    .param p1, "products"    # [Ljava/lang/String;

    .prologue
    .line 105
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;

    invoke-direct {v1, p0, p1}, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;-><init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 180
    return-void
.end method

.method public requestPurchases()V
    .locals 2

    .prologue
    .line 236
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;-><init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 306
    return-void
.end method

.method public setContext(Landroid/app/Activity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Landroid/app/Activity;",
            ":",
            "Ljp/co/drecom/bisque/lib/BQPaymentDispatchable;",
            ">(TA;)V"
        }
    .end annotation

    .prologue
    .line 546
    .local p1, "context":Landroid/app/Activity;, "TA;"
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    .line 547
    if-nez p1, :cond_0

    .line 548
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->handler:Landroid/os/Handler;

    .line 553
    .end local p1    # "context":Landroid/app/Activity;, "TA;"
    :goto_0
    return-void

    .restart local p1    # "context":Landroid/app/Activity;, "TA;"
    :cond_0
    move-object v0, p1

    .line 550
    check-cast v0, Ljp/co/drecom/bisque/lib/BQPaymentDispatchable;

    invoke-interface {v0, p0}, Ljp/co/drecom/bisque/lib/BQPaymentDispatchable;->setBQPaymentBridge(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V

    .line 551
    check-cast p1, Ljp/co/drecom/bisque/lib/BQPaymentDispatchable;

    .end local p1    # "context":Landroid/app/Activity;, "TA;"
    invoke-interface {p1}, Ljp/co/drecom/bisque/lib/BQPaymentDispatchable;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->handler:Landroid/os/Handler;

    goto :goto_0
.end method

.method public setup()V
    .locals 6

    .prologue
    .line 445
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->isEmulator()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    iget-boolean v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->setuped:Z

    if-nez v3, :cond_0

    .line 458
    const/4 v3, 0x0

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->reason:Lcom/ex/android/util/IabResult;

    .line 459
    new-instance v3, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge$5;-><init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->serviceConnection:Landroid/content/ServiceConnection;

    .line 503
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 504
    .local v2, "serviceIntent":Landroid/content/Intent;
    const-string v3, "com.android.vending"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 505
    const/4 v0, 0x0

    .line 506
    .local v0, "bindSuccessed":Z
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 507
    .local v1, "intentServices":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 509
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    iget-object v4, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->serviceConnection:Landroid/content/ServiceConnection;

    const/4 v5, 0x1

    invoke-virtual {v3, v2, v4, v5}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 511
    :cond_2
    if-nez v0, :cond_0

    .line 514
    new-instance v3, Lcom/ex/android/util/IabResult;

    const/4 v4, 0x3

    const-string v5, "Billing service unavailable on device."

    invoke-direct {v3, v4, v5}, Lcom/ex/android/util/IabResult;-><init>(ILjava/lang/String;)V

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->reason:Lcom/ex/android/util/IabResult;

    .line 516
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->showDialogNotFoundSevice()V

    goto :goto_0
.end method

.method public unbindService()V
    .locals 2

    .prologue
    .line 684
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->serviceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_1

    .line 685
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->context:Landroid/app/Activity;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 686
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->serviceConnection:Landroid/content/ServiceConnection;

    .line 688
    :cond_1
    return-void
.end method
