.class public Ljp/co/drecom/bisque/lib/BQTouchDispatcherNotify;
.super Ljava/lang/Object;
.source "BQTouchDispatcherNotify.java"


# static fields
.field private static dispatcher:Ljp/co/drecom/bisque/lib/BQTouchDispatcher;

.field private static handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljp/co/drecom/bisque/lib/BQTouchDispatcher;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Ljp/co/drecom/bisque/lib/BQTouchDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQTouchDispatcher;

    return-object v0
.end method

.method public static setDispatcher(Ljp/co/drecom/bisque/lib/BQTouchDispatcher;Landroid/os/Handler;)V
    .locals 0
    .param p0, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQTouchDispatcher;
    .param p1, "_handler"    # Landroid/os/Handler;

    .prologue
    .line 14
    sput-object p0, Ljp/co/drecom/bisque/lib/BQTouchDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQTouchDispatcher;

    .line 15
    sput-object p1, Ljp/co/drecom/bisque/lib/BQTouchDispatcherNotify;->handler:Landroid/os/Handler;

    .line 16
    return-void
.end method

.method public static setUserInteractionEnabled(ZI)V
    .locals 4
    .param p0, "b"    # Z
    .param p1, "tag"    # I

    .prologue
    .line 20
    move v0, p0

    .line 21
    .local v0, "_b":Z
    move v1, p1

    .line 22
    .local v1, "_tag":I
    sget-object v2, Ljp/co/drecom/bisque/lib/BQTouchDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v3, Ljp/co/drecom/bisque/lib/BQTouchDispatcherNotify$1;

    invoke-direct {v3, v0, v1}, Ljp/co/drecom/bisque/lib/BQTouchDispatcherNotify$1;-><init>(ZI)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 28
    return-void
.end method
