.class Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;
.super Ljava/lang/Object;
.source "DRMoviePlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/DRMoviePlayer;->play()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    .prologue
    .line 125
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$000(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$200(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Landroid/widget/VideoView;

    move-result-object v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$100(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 132
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$102(Ljp/co/drecom/bisque/lib/DRMoviePlayer;I)I

    .line 133
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$300(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$200(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 138
    :goto_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$300(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Z

    move-result v1

    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$402(Ljp/co/drecom/bisque/lib/DRMoviePlayer;Z)Z

    .line 144
    :goto_1
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$000(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->nativeCallbackMovieStarted()V

    .line 147
    :cond_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0, v2}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$302(Ljp/co/drecom/bisque/lib/DRMoviePlayer;Z)Z

    .line 148
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0, v2}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$002(Ljp/co/drecom/bisque/lib/DRMoviePlayer;Z)Z

    .line 149
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$200(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    goto :goto_0

    .line 141
    :cond_2
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$200(Ljp/co/drecom/bisque/lib/DRMoviePlayer;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 142
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayer$1;->this$0:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-static {v0, v2}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->access$402(Ljp/co/drecom/bisque/lib/DRMoviePlayer;Z)Z

    goto :goto_1
.end method
