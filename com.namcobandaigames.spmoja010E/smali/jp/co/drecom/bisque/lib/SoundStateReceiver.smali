.class public Ljp/co/drecom/bisque/lib/SoundStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SoundStateReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static ringerModeToStr(I)Ljava/lang/String;
    .locals 3
    .param p0, "_Rm"    # I

    .prologue
    .line 47
    const-string v0, ""

    .line 48
    .local v0, "sm":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UNKNOWN<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 50
    :pswitch_0
    const-string v0, "RINGER_MODE_NORMAL"

    .line 51
    goto :goto_0

    .line 53
    :pswitch_1
    const-string v0, "RINGER_MODE_SILENT"

    .line 54
    goto :goto_0

    .line 56
    :pswitch_2
    const-string v0, "RINGER_MODE_VIBRATE"

    .line 57
    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 14
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 18
    :cond_1
    const-string v4, "audio"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 19
    .local v0, "audioMgr":Landroid/media/AudioManager;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 21
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getLastAudioRingMode()I

    move-result v1

    .line 23
    .local v1, "last":I
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v2

    .line 25
    .local v2, "rm":I
    invoke-static {v2}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->setLastAudioRingMode(I)V

    .line 27
    if-eqz v1, :cond_2

    if-nez v2, :cond_2

    .line 28
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->enterAudioSilentMode()V

    goto :goto_0

    .line 30
    :cond_2
    if-nez v1, :cond_0

    if-eqz v2, :cond_0

    .line 31
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->leaveAudioSilentMode()V

    goto :goto_0

    .line 35
    .end local v1    # "last":I
    .end local v2    # "rm":I
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 37
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getLastAudioVolume()I

    move-result v1

    .line 38
    .restart local v1    # "last":I
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v3

    .line 40
    .local v3, "vo":I
    if-eq v1, v3, :cond_0

    .line 41
    invoke-static {v3}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->setLastAudioVolume(I)V

    goto :goto_0
.end method
