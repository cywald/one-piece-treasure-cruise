.class public Ljp/co/drecom/bisque/lib/BQWebView;
.super Landroid/webkit/WebView;
.source "BQWebView.java"


# static fields
.field private static wcontext:Landroid/content/Context;


# instance fields
.field public final BQ_WEBVIEW_BOUNCES_OFF:I

.field public final BQ_WEBVIEW_BOUNCES_ON:I

.field public final BQ_WEBVIEW_BOUNCES_OSDEFAULT:I

.field private final ERROR_TEMPLATE_CODE:Ljava/lang/String;

.field private ERROR_TEMPLATE_FILE:Ljava/lang/String;

.field private final ERROR_TEMPLATE_PATH:Ljava/lang/String;

.field private final META_DATA_KEY_ENABLE_WEBVIEW_ZOOM:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private auth:Z

.field private authpassword:Ljava/lang/String;

.field private authusername:Ljava/lang/String;

.field private bounces:Z

.field private count:I

.field private dispatcher:Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

.field private enableBackKey:Z

.field private enableBringToFront:Z

.field private interaction:Z

.field private move:Z

.field private old_scrollY:I

.field private onwv:Z

.field private scrollHeightMax:I

.field public tag:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    iput v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    .line 26
    iput-boolean v2, p0, Ljp/co/drecom/bisque/lib/BQWebView;->interaction:Z

    .line 30
    const-string v0, "BQWebView"

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->TAG:Ljava/lang/String;

    .line 35
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->move:Z

    .line 36
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->onwv:Z

    .line 38
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBackKey:Z

    .line 39
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront:Z

    .line 40
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->auth:Z

    .line 41
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->authusername:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->authpassword:Ljava/lang/String;

    .line 44
    iput v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->BQ_WEBVIEW_BOUNCES_OSDEFAULT:I

    .line 45
    iput v2, p0, Ljp/co/drecom/bisque/lib/BQWebView;->BQ_WEBVIEW_BOUNCES_OFF:I

    .line 46
    const/4 v0, 0x2

    iput v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->BQ_WEBVIEW_BOUNCES_ON:I

    .line 48
    const-string v0, "file:///android_asset/"

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->ERROR_TEMPLATE_PATH:Ljava/lang/String;

    .line 49
    const-string v0, "<html><body style=\'background: black;\'><p style=\'color: red;\'>Unable to load webview error template file.</p></body></html>"

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->ERROR_TEMPLATE_CODE:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->ERROR_TEMPLATE_FILE:Ljava/lang/String;

    .line 52
    const-string v0, "jp.co.drecom.bisque.enable.webview.zoom"

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->META_DATA_KEY_ENABLE_WEBVIEW_ZOOM:Ljava/lang/String;

    .line 89
    invoke-virtual {p0, p3}, Ljp/co/drecom/bisque/lib/BQWebView;->initDispatcher(Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;Ljava/lang/String;)V
    .locals 8
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;
    .param p3, "_file"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 56
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 25
    iput v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->tag:I

    .line 26
    iput-boolean v6, p0, Ljp/co/drecom/bisque/lib/BQWebView;->interaction:Z

    .line 30
    const-string v3, "BQWebView"

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->TAG:Ljava/lang/String;

    .line 35
    iput-boolean v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->move:Z

    .line 36
    iput-boolean v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->onwv:Z

    .line 38
    iput-boolean v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBackKey:Z

    .line 39
    iput-boolean v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront:Z

    .line 40
    iput-boolean v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->auth:Z

    .line 41
    const-string v3, ""

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->authusername:Ljava/lang/String;

    .line 42
    const-string v3, ""

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->authpassword:Ljava/lang/String;

    .line 44
    iput v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->BQ_WEBVIEW_BOUNCES_OSDEFAULT:I

    .line 45
    iput v6, p0, Ljp/co/drecom/bisque/lib/BQWebView;->BQ_WEBVIEW_BOUNCES_OFF:I

    .line 46
    iput v7, p0, Ljp/co/drecom/bisque/lib/BQWebView;->BQ_WEBVIEW_BOUNCES_ON:I

    .line 48
    const-string v3, "file:///android_asset/"

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->ERROR_TEMPLATE_PATH:Ljava/lang/String;

    .line 49
    const-string v3, "<html><body style=\'background: black;\'><p style=\'color: red;\'>Unable to load webview error template file.</p></body></html>"

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->ERROR_TEMPLATE_CODE:Ljava/lang/String;

    .line 50
    const-string v3, ""

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->ERROR_TEMPLATE_FILE:Ljava/lang/String;

    .line 52
    const-string v3, "jp.co.drecom.bisque.enable.webview.zoom"

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->META_DATA_KEY_ENABLE_WEBVIEW_ZOOM:Ljava/lang/String;

    .line 57
    sput-object p1, Ljp/co/drecom/bisque/lib/BQWebView;->wcontext:Landroid/content/Context;

    .line 58
    invoke-virtual {p0, p2}, Ljp/co/drecom/bisque/lib/BQWebView;->initDispatcher(Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;)V

    .line 59
    iput v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->old_scrollY:I

    .line 60
    iput v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->count:I

    .line 61
    iput-boolean v5, p0, Ljp/co/drecom/bisque/lib/BQWebView;->bounces:Z

    .line 62
    invoke-virtual {p0, v5}, Ljp/co/drecom/bisque/lib/BQWebView;->setOverScrollMode(I)V

    .line 63
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getContentHeight()I

    move-result v3

    iput v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->scrollHeightMax:I

    .line 64
    iput-object p3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->ERROR_TEMPLATE_FILE:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "localstorage"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "databasePath":Ljava/lang/String;
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 68
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setDatabasePath(Ljava/lang/String;)V

    .line 70
    const/4 v2, 0x0

    .line 72
    .local v2, "enableZoom":Z
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 73
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v7, :cond_0

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v3, :cond_0

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "jp.co.drecom.bisque.enable.webview.zoom"

    .line 76
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 78
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "jp.co.drecom.bisque.enable.webview.zoom"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 83
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 84
    return-void

    .line 80
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method static synthetic access$000(Ljp/co/drecom/bisque/lib/BQWebView;)Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQWebView;

    .prologue
    .line 22
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->dispatcher:Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    return-object v0
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/BQWebView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQWebView;

    .prologue
    .line 22
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->ERROR_TEMPLATE_FILE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Ljp/co/drecom/bisque/lib/BQWebView;)Z
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQWebView;

    .prologue
    .line 22
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->auth:Z

    return v0
.end method

.method static synthetic access$300(Ljp/co/drecom/bisque/lib/BQWebView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQWebView;

    .prologue
    .line 22
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->authusername:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Ljp/co/drecom/bisque/lib/BQWebView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQWebView;

    .prologue
    .line 22
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->authpassword:Ljava/lang/String;

    return-object v0
.end method

.method public static native nativeCallbackFuncForWebViewDidFailLoadWithError(Ljava/lang/String;)V
.end method

.method public static native nativeCallbackFuncForWebViewDidFinishLoad(I)V
.end method

.method public static native nativeCallbackFuncForWebViewShouldStartLoadWithRequest(Ljava/lang/String;I)Z
.end method


# virtual methods
.method public backKeyDisable()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBackKey:Z

    .line 106
    return-void
.end method

.method public backKeyEnable()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBackKey:Z

    .line 112
    return-void
.end method

.method public bouncesOff()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->bounces:Z

    .line 95
    return-void
.end method

.method public bouncesOn()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->bounces:Z

    .line 100
    return-void
.end method

.method public enableBringToFront(Z)V
    .locals 1
    .param p1, "enableBringToFront"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront:Z

    .line 126
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront:Z

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->bringToFront()V

    .line 129
    :cond_0
    return-void
.end method

.method public initDispatcher(Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;)V
    .locals 2
    .param p1, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    .prologue
    const/4 v1, 0x1

    .line 300
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->dispatcher:Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    .line 301
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 302
    invoke-virtual {p0, v1}, Ljp/co/drecom/bisque/lib/BQWebView;->setVerticalScrollbarOverlay(Z)V

    .line 303
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 304
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Ljp/co/drecom/bisque/lib/BQWebView;->setBackgroundColor(I)V

    .line 306
    new-instance v0, Ljp/co/drecom/bisque/lib/BQWebView$1;

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQWebView$1;-><init>(Ljp/co/drecom/bisque/lib/BQWebView;)V

    invoke-virtual {p0, v0}, Ljp/co/drecom/bisque/lib/BQWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 356
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 279
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 282
    :cond_0
    const/4 v0, 0x0

    .line 284
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 290
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 293
    :cond_0
    const/4 v0, 0x0

    .line 295
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 7
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    const/4 v6, 0x0

    .line 254
    iget-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->bounces:Z

    if-eqz v1, :cond_1

    .line 256
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getHeight()I

    move-result v1

    int-to-double v2, v1

    const-wide v4, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 265
    .local v0, "h":I
    :goto_0
    iget v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->old_scrollY:I

    neg-int v2, v0

    if-gt v1, v2, :cond_0

    neg-int v1, v0

    if-le p2, v1, :cond_0

    iget v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->count:I

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    .line 269
    :cond_0
    iput p2, p0, Ljp/co/drecom/bisque/lib/BQWebView;->old_scrollY:I

    .line 270
    neg-int v1, v0

    if-gt p2, v1, :cond_2

    iget v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->count:I

    .line 273
    :goto_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->onOverScrolled(IIZZ)V

    .line 274
    return-void

    .line 261
    .end local v0    # "h":I
    :cond_1
    const/4 v0, 0x0

    .line 262
    .restart local v0    # "h":I
    iput v6, p0, Ljp/co/drecom/bisque/lib/BQWebView;->count:I

    goto :goto_0

    .line 271
    :cond_2
    iput v6, p0, Ljp/co/drecom/bisque/lib/BQWebView;->count:I

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 134
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 135
    .local v0, "abs_x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 138
    .local v1, "abs_y":F
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getLeft()I

    move-result v4

    .line 139
    .local v4, "wvpos_x":I
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getTop()I

    move-result v6

    .line 140
    .local v6, "wvpos_y":I
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getRight()I

    move-result v5

    .line 141
    .local v5, "wvpos_xx":I
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getBottom()I

    move-result v7

    .line 142
    .local v7, "wvpos_yy":I
    iget-boolean v9, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront:Z

    if-nez v9, :cond_0

    int-to-float v9, v4

    cmpl-float v9, v0, v9

    if-ltz v9, :cond_0

    int-to-float v9, v5

    cmpg-float v9, v0, v9

    if-gtz v9, :cond_0

    int-to-float v9, v6

    cmpl-float v9, v1, v9

    if-ltz v9, :cond_0

    int-to-float v9, v7

    cmpg-float v9, v1, v9

    if-lez v9, :cond_1

    :cond_0
    iget-boolean v9, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront:Z

    if-ne v9, v10, :cond_5

    cmpl-float v9, v0, v11

    if-ltz v9, :cond_5

    sub-int v9, v5, v4

    int-to-float v9, v9

    cmpg-float v9, v0, v9

    if-gtz v9, :cond_5

    cmpl-float v9, v1, v11

    if-ltz v9, :cond_5

    sub-int v9, v7, v6

    int-to-float v9, v9

    cmpg-float v9, v1, v9

    if-gtz v9, :cond_5

    .line 149
    :cond_1
    iget-boolean v9, p0, Ljp/co/drecom/bisque/lib/BQWebView;->onwv:Z

    if-nez v9, :cond_3

    .line 150
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v9, v9, 0xff

    if-eqz v9, :cond_3

    .line 152
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->resetOverScroll()V

    .line 204
    :cond_2
    :goto_0
    return v8

    .line 157
    :cond_3
    iput-boolean v10, p0, Ljp/co/drecom/bisque/lib/BQWebView;->onwv:Z

    .line 158
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v9, v9, 0xff

    if-ne v9, v12, :cond_4

    .line 160
    iput-boolean v10, p0, Ljp/co/drecom/bisque/lib/BQWebView;->move:Z

    .line 192
    :goto_1
    iget-boolean v9, p0, Ljp/co/drecom/bisque/lib/BQWebView;->enableBringToFront:Z

    if-nez v9, :cond_8

    .line 193
    int-to-float v9, v4

    sub-float v2, v0, v9

    .line 194
    .local v2, "rel_x":F
    int-to-float v9, v6

    sub-float v3, v1, v9

    .line 199
    .local v3, "rel_y":F
    :goto_2
    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 201
    iget-boolean v9, p0, Ljp/co/drecom/bisque/lib/BQWebView;->interaction:Z

    if-eqz v9, :cond_2

    .line 202
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto :goto_0

    .line 164
    .end local v2    # "rel_x":F
    .end local v3    # "rel_y":F
    :cond_4
    iput-boolean v8, p0, Ljp/co/drecom/bisque/lib/BQWebView;->move:Z

    goto :goto_1

    .line 170
    :cond_5
    iget-boolean v9, p0, Ljp/co/drecom/bisque/lib/BQWebView;->move:Z

    if-ne v9, v10, :cond_7

    .line 173
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v9, v9, 0xff

    if-ne v9, v12, :cond_6

    .line 175
    iput-boolean v10, p0, Ljp/co/drecom/bisque/lib/BQWebView;->move:Z

    goto :goto_1

    .line 180
    :cond_6
    iput-boolean v8, p0, Ljp/co/drecom/bisque/lib/BQWebView;->move:Z

    .line 181
    iput-boolean v8, p0, Ljp/co/drecom/bisque/lib/BQWebView;->onwv:Z

    goto :goto_1

    .line 186
    :cond_7
    iput-boolean v8, p0, Ljp/co/drecom/bisque/lib/BQWebView;->onwv:Z

    .line 187
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->resetOverScroll()V

    goto :goto_0

    .line 196
    :cond_8
    move v2, v0

    .line 197
    .restart local v2    # "rel_x":F
    move v3, v1

    .restart local v3    # "rel_y":F
    goto :goto_2
.end method

.method protected overScrollBy(IIIIIIIIZ)Z
    .locals 12
    .param p1, "deltaX"    # I
    .param p2, "deltaY"    # I
    .param p3, "scrollX"    # I
    .param p4, "scrollY"    # I
    .param p5, "scrollRangeX"    # I
    .param p6, "scrollRangeY"    # I
    .param p7, "maxOverScrollX"    # I
    .param p8, "maxOverScrollY"    # I
    .param p9, "isTouchEvent"    # Z

    .prologue
    .line 232
    move/from16 v0, p6

    iput v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->scrollHeightMax:I

    .line 234
    iget-boolean v2, p0, Ljp/co/drecom/bisque/lib/BQWebView;->bounces:Z

    if-eqz v2, :cond_0

    .line 236
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQWebView;->getHeight()I

    move-result v2

    int-to-double v2, v2

    const-wide v4, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v2, v4

    double-to-int v10, v2

    .line 244
    .local v10, "h":I
    :goto_0
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v2, p0

    move v4, p2

    move/from16 v6, p4

    move/from16 v8, p6

    move/from16 v11, p9

    invoke-super/range {v2 .. v11}, Landroid/webkit/WebView;->overScrollBy(IIIIIIIIZ)Z

    move-result v2

    return v2

    .line 240
    .end local v10    # "h":I
    :cond_0
    const/4 v10, 0x0

    .restart local v10    # "h":I
    goto :goto_0
.end method

.method resetOverScroll()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 209
    iget v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->scrollHeightMax:I

    .line 210
    .local v0, "_shmax":I
    iget v1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->old_scrollY:I

    .line 211
    .local v1, "scrollY":I
    iget v2, p0, Ljp/co/drecom/bisque/lib/BQWebView;->old_scrollY:I

    if-gez v2, :cond_1

    .line 213
    const/4 v1, 0x0

    .line 214
    iput v3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->old_scrollY:I

    .line 215
    invoke-super {p0, v3, v1, v3, v3}, Landroid/webkit/WebView;->onOverScrolled(IIZZ)V

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget v2, p0, Ljp/co/drecom/bisque/lib/BQWebView;->old_scrollY:I

    if-le v2, v0, :cond_0

    .line 219
    move v1, v0

    .line 220
    iput v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->old_scrollY:I

    .line 221
    invoke-super {p0, v3, v1, v3, v3}, Landroid/webkit/WebView;->onOverScrolled(IIZZ)V

    goto :goto_0
.end method

.method public setAuthInfo(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "_auth"    # Z
    .param p2, "username"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->auth:Z

    .line 117
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQWebView;->auth:Z

    if-eqz v0, :cond_0

    .line 118
    iput-object p2, p0, Ljp/co/drecom/bisque/lib/BQWebView;->authusername:Ljava/lang/String;

    .line 119
    iput-object p3, p0, Ljp/co/drecom/bisque/lib/BQWebView;->authpassword:Ljava/lang/String;

    .line 121
    :cond_0
    return-void
.end method

.method public setInteraction(Z)V
    .locals 0
    .param p1, "_b"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/BQWebView;->interaction:Z

    .line 29
    return-void
.end method
