.class public Ljp/co/drecom/bisque/lib/BQAppPlatformManager;
.super Ljava/lang/Object;
.source "BQAppPlatformManager.java"


# static fields
.field private static final MOCKLOCATIONSETTING_OFF:J = 0x0L

.field private static final MOCKLOCATIONSETTING_ON:J = 0x1L

.field private static final MOCKLOCATIONSETTING_UNKNOWN:J = -0x1L

.field private static m_activityManager:Landroid/app/ActivityManager;

.field private static m_context:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-object v0, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    .line 40
    sput-object v0, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAndroidID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 88
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "deviceId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .end local v0    # "deviceId":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "deviceId":Ljava/lang/String;
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static getAvailMem()J
    .locals 4

    .prologue
    .line 222
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    if-nez v1, :cond_0

    .line 223
    const-wide/16 v2, -0x1

    .line 227
    .local v0, "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    :goto_0
    return-wide v2

    .line 225
    .end local v0    # "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    :cond_0
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 226
    .restart local v0    # "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 227
    iget-wide v2, v0, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    goto :goto_0
.end method

.method public static getBatteryLevel()F
    .locals 9

    .prologue
    .line 346
    sget-object v5, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    if-nez v5, :cond_0

    .line 347
    const/high16 v3, -0x40800000    # -1.0f

    .line 359
    .local v3, "ret":F
    :goto_0
    return v3

    .line 349
    .end local v3    # "ret":F
    :cond_0
    const/4 v3, 0x0

    .line 351
    .restart local v3    # "ret":F
    :try_start_0
    sget-object v5, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v7, Landroid/content/IntentFilter;

    const-string v8, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 352
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 353
    .local v0, "battery":Landroid/content/Intent;
    const-string v5, "level"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 354
    .local v2, "level":I
    const-string v5, "scale"

    const/16 v6, 0x64

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 355
    .local v4, "scale":I
    int-to-float v5, v2

    int-to-float v6, v4

    div-float v3, v5, v6

    goto :goto_0

    .line 356
    .end local v0    # "battery":Landroid/content/Intent;
    .end local v2    # "level":I
    .end local v4    # "scale":I
    :catch_0
    move-exception v1

    .line 357
    .local v1, "e":Ljava/lang/Exception;
    const/high16 v3, -0x40000000    # -2.0f

    goto :goto_0
.end method

.method public static getCountryCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 138
    .local v0, "locale":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getCurrencyCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 146
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 147
    .local v2, "locale":Ljava/util/Locale;
    const/4 v0, 0x0

    .line 149
    .local v0, "currency":Ljava/util/Currency;
    :try_start_0
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 153
    :goto_0
    if-nez v0, :cond_0

    .line 154
    const/4 v3, 0x0

    .line 156
    :goto_1
    return-object v3

    .line 150
    :catch_0
    move-exception v1

    .line 151
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const/4 v0, 0x0

    goto :goto_0

    .line 156
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    invoke-virtual {v0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public static getDalvikHeapAllocatedSize()J
    .locals 4

    .prologue
    .line 214
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static getDalvikHeapFreeSize()J
    .locals 2

    .prologue
    .line 206
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getDalvikHeapMaxSize()J
    .locals 2

    .prologue
    .line 198
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getDalvikHeapTotalSize()J
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getDalvikPss()J
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    const/4 v6, 0x0

    .line 278
    sget-object v4, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    if-nez v4, :cond_1

    .line 287
    .local v0, "memInfos":[Landroid/os/Debug$MemoryInfo;
    .local v1, "myPid":I
    :cond_0
    :goto_0
    return-wide v2

    .line 281
    .end local v0    # "memInfos":[Landroid/os/Debug$MemoryInfo;
    .end local v1    # "myPid":I
    :cond_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 282
    .restart local v1    # "myPid":I
    sget-object v4, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    const/4 v5, 0x1

    new-array v5, v5, [I

    aput v1, v5, v6

    invoke-virtual {v4, v5}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v0

    .line 284
    .restart local v0    # "memInfos":[Landroid/os/Debug$MemoryInfo;
    array-length v4, v0

    if-eqz v4, :cond_0

    .line 287
    aget-object v2, v0, v6

    iget v2, v2, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    int-to-long v2, v2

    const-wide/16 v4, 0x400

    mul-long/2addr v2, v4

    goto :goto_0
.end method

.method public static getDiskFreeSpaceExternal()J
    .locals 2

    .prologue
    .line 342
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public static getDiskFreeSpaceInternal()J
    .locals 8

    .prologue
    .line 330
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    .line 331
    .local v2, "path":Ljava/io/File;
    new-instance v3, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 332
    .local v3, "stat":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    .line 333
    .local v0, "ab":J
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v4, v6

    .line 334
    .local v4, "vb":J
    mul-long v6, v0, v4

    return-wide v6
.end method

.method public static getMockLocation()J
    .locals 5

    .prologue
    .line 388
    const-wide/16 v2, -0x1

    .line 389
    .local v2, "ret":J
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 397
    :goto_0
    return-wide v2

    .line 393
    :cond_0
    :try_start_0
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "mock_location"

    invoke-static {v1, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    const-wide/16 v2, 0x1

    :goto_1
    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_1

    .line 394
    :catch_0
    move-exception v0

    .line 395
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public static getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method public static getNativeHeapAllocatedSize()J
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getNativeHeapFreeSize()J
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Landroid/os/Debug;->getNativeHeapFreeSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getNativeHeapSize()J
    .locals 2

    .prologue
    .line 165
    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getNativePss()J
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    const/4 v6, 0x0

    .line 261
    sget-object v4, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    if-nez v4, :cond_1

    .line 270
    .local v0, "memInfos":[Landroid/os/Debug$MemoryInfo;
    .local v1, "myPid":I
    :cond_0
    :goto_0
    return-wide v2

    .line 264
    .end local v0    # "memInfos":[Landroid/os/Debug$MemoryInfo;
    .end local v1    # "myPid":I
    :cond_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 265
    .restart local v1    # "myPid":I
    sget-object v4, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    const/4 v5, 0x1

    new-array v5, v5, [I

    aput v1, v5, v6

    invoke-virtual {v4, v5}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v0

    .line 267
    .restart local v0    # "memInfos":[Landroid/os/Debug$MemoryInfo;
    array-length v4, v0

    if-eqz v4, :cond_0

    .line 270
    aget-object v2, v0, v6

    iget v2, v2, Landroid/os/Debug$MemoryInfo;->nativePss:I

    int-to-long v2, v2

    const-wide/16 v4, 0x400

    mul-long/2addr v2, v4

    goto :goto_0
.end method

.method public static getOsVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public static getOsVersionCode()I
    .locals 1

    .prologue
    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method

.method public static getOtherPss()J
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    const/4 v6, 0x0

    .line 295
    sget-object v4, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    if-nez v4, :cond_1

    .line 304
    .local v0, "memInfos":[Landroid/os/Debug$MemoryInfo;
    .local v1, "myPid":I
    :cond_0
    :goto_0
    return-wide v2

    .line 298
    .end local v0    # "memInfos":[Landroid/os/Debug$MemoryInfo;
    .end local v1    # "myPid":I
    :cond_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 299
    .restart local v1    # "myPid":I
    sget-object v4, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    const/4 v5, 0x1

    new-array v5, v5, [I

    aput v1, v5, v6

    invoke-virtual {v4, v5}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v0

    .line 301
    .restart local v0    # "memInfos":[Landroid/os/Debug$MemoryInfo;
    array-length v4, v0

    if-eqz v4, :cond_0

    .line 304
    aget-object v2, v0, v6

    iget v2, v2, Landroid/os/Debug$MemoryInfo;->otherPss:I

    int-to-long v2, v2

    const-wide/16 v4, 0x400

    mul-long/2addr v2, v4

    goto :goto_0
.end method

.method public static getThreshold()J
    .locals 4

    .prologue
    .line 248
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    if-nez v1, :cond_0

    .line 249
    const-wide/16 v2, -0x1

    .line 253
    .local v0, "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    :goto_0
    return-wide v2

    .line 251
    .end local v0    # "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    :cond_0
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 252
    .restart local v0    # "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 253
    iget-wide v2, v0, Landroid/app/ActivityManager$MemoryInfo;->threshold:J

    goto :goto_0
.end method

.method public static getTotalPss()J
    .locals 9

    .prologue
    const-wide/16 v4, -0x1

    const/4 v8, 0x0

    .line 312
    sget-object v6, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    if-nez v6, :cond_1

    .line 322
    .local v0, "memInfos":[Landroid/os/Debug$MemoryInfo;
    .local v1, "myPid":I
    :cond_0
    :goto_0
    return-wide v4

    .line 315
    .end local v0    # "memInfos":[Landroid/os/Debug$MemoryInfo;
    .end local v1    # "myPid":I
    :cond_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 316
    .restart local v1    # "myPid":I
    sget-object v6, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    const/4 v7, 0x1

    new-array v7, v7, [I

    aput v1, v7, v8

    invoke-virtual {v6, v7}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    move-result-object v0

    .line 318
    .restart local v0    # "memInfos":[Landroid/os/Debug$MemoryInfo;
    array-length v6, v0

    if-eqz v6, :cond_0

    .line 321
    aget-object v4, v0, v8

    invoke-virtual {v4}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v4

    int-to-long v2, v4

    .line 322
    .local v2, "total":J
    const-wide/16 v4, 0x400

    mul-long/2addr v4, v2

    goto :goto_0
.end method

.method public static getTrustStorePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 405
    const-string v0, ""

    return-object v0
.end method

.method public static getVersionCode()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 117
    sget-object v3, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    if-nez v3, :cond_1

    .line 129
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-object v2

    .line 120
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    const/4 v1, 0x0

    .line 122
    .restart local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    sget-object v3, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    sget-object v4, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 126
    :goto_1
    if-eqz v1, :cond_0

    .line 129
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getVersionName()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 97
    sget-object v3, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    if-nez v3, :cond_1

    .line 109
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-object v2

    .line 100
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    const/4 v1, 0x0

    .line 102
    .restart local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    sget-object v3, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    sget-object v4, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 106
    :goto_1
    if-eqz v1, :cond_0

    .line 109
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static isEmulator()Z
    .locals 2

    .prologue
    .line 56
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "sdk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "sdk_phone_armv7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isExternalPowerSupplyConnected()Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 363
    sget-object v5, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    if-nez v5, :cond_0

    .line 375
    .local v2, "ret":Z
    :goto_0
    return v4

    .line 366
    .end local v2    # "ret":Z
    :cond_0
    const/4 v2, 0x0

    .line 368
    .restart local v2    # "ret":Z
    :try_start_0
    sget-object v5, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v7, Landroid/content/IntentFilter;

    const-string v8, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 369
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 370
    .local v0, "battery":Landroid/content/Intent;
    const-string v5, "plugged"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 371
    .local v3, "stat":I
    if-eqz v3, :cond_1

    const/4 v2, 0x1

    .end local v0    # "battery":Landroid/content/Intent;
    .end local v3    # "stat":I
    :goto_1
    move v4, v2

    .line 375
    goto :goto_0

    .restart local v0    # "battery":Landroid/content/Intent;
    .restart local v3    # "stat":I
    :cond_1
    move v2, v4

    .line 371
    goto :goto_1

    .line 372
    .end local v0    # "battery":Landroid/content/Intent;
    .end local v3    # "stat":I
    :catch_0
    move-exception v1

    .line 373
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static isLowMemory()Z
    .locals 2

    .prologue
    .line 235
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    if-nez v1, :cond_0

    .line 236
    const/4 v1, 0x0

    .line 240
    .local v0, "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    :goto_0
    return v1

    .line 238
    .end local v0    # "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    :cond_0
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 239
    .restart local v0    # "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    sget-object v1, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 240
    iget-boolean v1, v0, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    goto :goto_0
.end method

.method public static setContext(Landroid/app/Activity;)V
    .locals 1
    .param p0, "context"    # Landroid/app/Activity;

    .prologue
    .line 47
    sput-object p0, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_context:Landroid/app/Activity;

    .line 48
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    sput-object v0, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->m_activityManager:Landroid/app/ActivityManager;

    .line 49
    return-void
.end method
