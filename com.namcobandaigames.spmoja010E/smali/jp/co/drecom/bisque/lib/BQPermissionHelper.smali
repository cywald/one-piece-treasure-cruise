.class public Ljp/co/drecom/bisque/lib/BQPermissionHelper;
.super Ljava/lang/Object;
.source "BQPermissionHelper.java"


# static fields
.field private static instance:Ljp/co/drecom/bisque/lib/BQPermissionHelper;

.field private static permissionArray:[Ljava/lang/String;


# instance fields
.field private m_activity:Landroid/app/Activity;

.field private m_currentRequestCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->instance:Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    .line 19
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_CALENDAR"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.WRITE_CALENDAR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "android.permission.READ_CONTACTS"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "android.permission.WRITE_CONTACTS"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "android.permission.GET_ACCOUNTS"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "android.permission.READ_PHONE_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "android.permission.CALL_PHONE"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.android.voicemail.permission.ADD_VOICEMAIL"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "android.permission.USE_SIP"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "android.permission.PROCESS_OUTGOING_CALLS"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "android.permission.SEND_SMS"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "android.permission.RECEIVE_SMS"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "android.permission.READ_SMS"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "android.permission.RECEIVE_WAP_PUSH"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "android.permission.RECEIVE_MMS"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->permissionArray:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->m_activity:Landroid/app/Activity;

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->m_currentRequestCode:I

    return-void
.end method

.method static synthetic access$000(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 14
    invoke-static {p0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->nativePermissionResult(Z)V

    return-void
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/BQPermissionHelper;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    .prologue
    .line 14
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->m_activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 14
    invoke-static {p0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->nativeCheckPermission(I)V

    return-void
.end method

.method static synthetic access$300(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 14
    invoke-static {p0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->nativeShouldShowRequestPermissionRationale(Z)V

    return-void
.end method

.method public static checkPermission(I)V
    .locals 1
    .param p0, "permissionIdx"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->checkPermissionInternal(I)V

    .line 153
    return-void
.end method

.method private checkPermissionInternal(I)V
    .locals 3
    .param p1, "permissionIdx"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {p1}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->getPermissionStr(I)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "permissionStr":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/BQPermissionHelper$2;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper$2;-><init>(Ljp/co/drecom/bisque/lib/BQPermissionHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 114
    return-void
.end method

.method public static getInstance()Ljp/co/drecom/bisque/lib/BQPermissionHelper;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->instance:Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->instance:Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    .line 59
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->instance:Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    return-object v0
.end method

.method private static getPermissionStr(I)Ljava/lang/String;
    .locals 1
    .param p0, "idx"    # I

    .prologue
    .line 50
    if-ltz p0, :cond_0

    sget-object v0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->permissionArray:[Ljava/lang/String;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 51
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 52
    :cond_1
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->permissionArray:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method

.method private static native nativeCheckPermission(I)V
.end method

.method private static native nativePermissionResult(Z)V
.end method

.method private static native nativeShouldShowRequestPermissionRationale(Z)V
.end method

.method public static requestPermission(I)V
    .locals 1
    .param p0, "permissionIdx"    # I

    .prologue
    .line 168
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->requestPermissionInternal(I)V

    .line 169
    return-void
.end method

.method private requestPermissionInternal(I)V
    .locals 4
    .param p1, "permissionIdx"    # I

    .prologue
    .line 135
    invoke-static {p1}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->getPermissionStr(I)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "permissionStr":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 137
    .local v1, "permissions":[Ljava/lang/String;
    iput p1, p0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->m_currentRequestCode:I

    .line 138
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v2

    new-instance v3, Ljp/co/drecom/bisque/lib/BQPermissionHelper$4;

    invoke-direct {v3, p0, v1, p1}, Ljp/co/drecom/bisque/lib/BQPermissionHelper$4;-><init>(Ljp/co/drecom/bisque/lib/BQPermissionHelper;[Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 145
    return-void
.end method

.method public static shouldShowRequestPermissionRationale(I)V
    .locals 1
    .param p0, "permissionIdx"    # I

    .prologue
    .line 160
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQPermissionHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->shouldShowRequestPermissionRationaleInternal(I)V

    .line 161
    return-void
.end method

.method private shouldShowRequestPermissionRationaleInternal(I)V
    .locals 3
    .param p1, "permissionIdx"    # I

    .prologue
    .line 117
    invoke-static {p1}, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->getPermissionStr(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "permissionStr":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/BQPermissionHelper$3;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper$3;-><init>(Ljp/co/drecom/bisque/lib/BQPermissionHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 132
    return-void
.end method


# virtual methods
.method public initialize(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 63
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->m_activity:Landroid/app/Activity;

    .line 64
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .prologue
    .line 75
    iget v1, p0, Ljp/co/drecom/bisque/lib/BQPermissionHelper;->m_currentRequestCode:I

    if-ne p1, v1, :cond_0

    .line 77
    array-length v1, p3

    if-lez v1, :cond_1

    const/4 v1, 0x0

    aget v1, p3, v1

    if-nez v1, :cond_1

    .line 80
    const/4 v0, 0x1

    .line 86
    .local v0, "permissionGranted":Z
    :goto_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/BQPermissionHelper$1;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/BQPermissionHelper$1;-><init>(Ljp/co/drecom/bisque/lib/BQPermissionHelper;Z)V

    invoke-virtual {v1, v2}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 93
    .end local v0    # "permissionGranted":Z
    :cond_0
    return-void

    .line 84
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "permissionGranted":Z
    goto :goto_0
.end method
