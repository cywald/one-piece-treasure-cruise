.class public Ljp/co/drecom/bisque/lib/BQTwitterHelper;
.super Ljava/lang/Object;
.source "BQTwitterHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;
    }
.end annotation


# static fields
.field private static http_connection_timeout:I

.field private static http_read_timeout:I

.field private static self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mAccessTokenSecret:Ljava/lang/String;

.field private mCallbackURL:Ljava/lang/String;

.field private mConsumerKey:Ljava/lang/String;

.field private mConsumerSecret:Ljava/lang/String;

.field private mInitialized:Z

.field private mMediaFile:Ljava/lang/String;

.field private mRequestToken:Ljava/lang/String;

.field private mRequestTokenSecret:Ljava/lang/String;

.field private mTwitter:Ltwitter4j/AsyncTwitter;

.field private mVerifier:Ljava/lang/String;

.field private m_cb:Ltwitter4j/conf/ConfigurationBuilder;

.field private m_executing_browser:Z

.field private m_factory:Ltwitter4j/AsyncTwitterFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/16 v0, 0x4e20

    sput v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->http_connection_timeout:I

    .line 23
    const v0, 0x1d4c0

    sput v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->http_read_timeout:I

    .line 24
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_cb:Ltwitter4j/conf/ConfigurationBuilder;

    .line 26
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_factory:Ltwitter4j/AsyncTwitterFactory;

    .line 27
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mRequestToken:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mRequestTokenSecret:Ljava/lang/String;

    .line 30
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mInitialized:Z

    .line 31
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mMediaFile:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mAccessToken:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mAccessTokenSecret:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerKey:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerSecret:Ljava/lang/String;

    .line 36
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mCallbackURL:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mVerifier:Ljava/lang/String;

    .line 38
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_executing_browser:Z

    .line 61
    return-void
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)Ltwitter4j/AsyncTwitter;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    .prologue
    .line 21
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    return-object v0
.end method

.method static synthetic access$202(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_executing_browser:Z

    return p1
.end method

.method static synthetic access$300(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    .prologue
    .line 21
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mRequestToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mRequestToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    .prologue
    .line 21
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mRequestTokenSecret:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mRequestTokenSecret:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    .prologue
    .line 21
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mMediaFile:Ljava/lang/String;

    return-object v0
.end method

.method public static closeAuthorizeURL()V
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->closeAuthorizeURLInternal()V

    .line 282
    return-void
.end method

.method private closeAuthorizeURLInternal()V
    .locals 2

    .prologue
    .line 284
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQTwitterHelper$5;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$5;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 290
    return-void
.end method

.method public static finalization()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    sget-object v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    if-eqz v0, :cond_0

    .line 101
    sget-object v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    iput-object v1, v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    .line 102
    sget-object v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    iput-object v1, v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_factory:Ltwitter4j/AsyncTwitterFactory;

    .line 103
    sput-object v1, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    .line 105
    :cond_0
    return-void
.end method

.method public static getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    .line 57
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->self:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    return-object v0
.end method

.method public static getOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "verifier"    # Ljava/lang/String;
    .param p1, "requestToken"    # Ljava/lang/String;
    .param p2, "requestTokenSecret"    # Ljava/lang/String;

    .prologue
    .line 164
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getOAuthAccessTokenInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method private getOAuthAccessTokenInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "verifier"    # Ljava/lang/String;
    .param p2, "requestToken"    # Ljava/lang/String;
    .param p3, "requestTokenSecret"    # Ljava/lang/String;

    .prologue
    .line 167
    move-object v1, p1

    .line 168
    .local v1, "_verifier":Ljava/lang/String;
    new-instance v0, Ltwitter4j/auth/RequestToken;

    invoke-direct {v0, p2, p3}, Ltwitter4j/auth/RequestToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .local v0, "_token":Ltwitter4j/auth/RequestToken;
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v2

    new-instance v3, Ljp/co/drecom/bisque/lib/BQTwitterHelper$2;

    invoke-direct {v3, p0, v0, v1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$2;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ltwitter4j/auth/RequestToken;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 175
    return-void
.end method

.method public static getOAuthRequestToken(Ljava/lang/String;)V
    .locals 2
    .param p0, "callbackURL"    # Ljava/lang/String;

    .prologue
    .line 146
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getOAuthRequestTokenInternal(Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method private getOAuthRequestTokenInternal(Ljava/lang/String;)V
    .locals 3
    .param p1, "callbackURL"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mCallbackURL:Ljava/lang/String;

    .line 150
    move-object v0, p1

    .line 151
    .local v0, "_callbackURL":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/BQTwitterHelper$1;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$1;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 157
    return-void
.end method

.method public static getVerifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getVerifierInternal()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getVerifierInternal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mVerifier:Ljava/lang/String;

    return-object v0
.end method

.method public static gotOAuthVerifier(Ljava/lang/String;)V
    .locals 1
    .param p0, "verifier"    # Ljava/lang/String;

    .prologue
    .line 297
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->gotOAuthVerifierInternal(Ljava/lang/String;)V

    .line 298
    return-void
.end method

.method private gotOAuthVerifierInternal(Ljava/lang/String;)V
    .locals 3
    .param p1, "verifier"    # Ljava/lang/String;

    .prologue
    .line 304
    move-object v0, p1

    .line 305
    .local v0, "_verifier":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/BQTwitterHelper$6;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$6;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 311
    return-void
.end method

.method private initInternal()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 78
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mInitialized:Z

    if-ne v0, v3, :cond_0

    .line 92
    :goto_0
    return v3

    .line 81
    :cond_0
    new-instance v0, Ltwitter4j/conf/ConfigurationBuilder;

    invoke-direct {v0}, Ltwitter4j/conf/ConfigurationBuilder;-><init>()V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_cb:Ltwitter4j/conf/ConfigurationBuilder;

    .line 82
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_cb:Ltwitter4j/conf/ConfigurationBuilder;

    sget v1, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->http_connection_timeout:I

    invoke-virtual {v0, v1}, Ltwitter4j/conf/ConfigurationBuilder;->setHttpConnectionTimeout(I)Ltwitter4j/conf/ConfigurationBuilder;

    .line 83
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_cb:Ltwitter4j/conf/ConfigurationBuilder;

    sget v1, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->http_read_timeout:I

    invoke-virtual {v0, v1}, Ltwitter4j/conf/ConfigurationBuilder;->setHttpReadTimeout(I)Ltwitter4j/conf/ConfigurationBuilder;

    .line 85
    new-instance v0, Ltwitter4j/AsyncTwitterFactory;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_cb:Ltwitter4j/conf/ConfigurationBuilder;

    invoke-virtual {v1}, Ltwitter4j/conf/ConfigurationBuilder;->build()Ltwitter4j/conf/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Ltwitter4j/AsyncTwitterFactory;-><init>(Ltwitter4j/conf/Configuration;)V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_factory:Ltwitter4j/AsyncTwitterFactory;

    .line 86
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_factory:Ltwitter4j/AsyncTwitterFactory;

    invoke-virtual {v0}, Ltwitter4j/AsyncTwitterFactory;->getInstance()Ltwitter4j/AsyncTwitter;

    move-result-object v0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    .line 89
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljp/co/drecom/bisque/lib/BQTwitterHelper$1;)V

    invoke-interface {v0, v1}, Ltwitter4j/AsyncTwitter;->addListener(Ltwitter4j/TwitterListener;)V

    .line 91
    iput-boolean v3, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mInitialized:Z

    goto :goto_0
.end method

.method public static initialize()Z
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 71
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->initInternal()Z

    move-result v0

    goto :goto_0
.end method

.method public static isExecutingBrowser()Z
    .locals 1

    .prologue
    .line 337
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->isExecutingBrowserInternal()Z

    move-result v0

    return v0
.end method

.method private isExecutingBrowserInternal()Z
    .locals 1

    .prologue
    .line 343
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_executing_browser:Z

    return v0
.end method

.method private loadAccessToken()Ltwitter4j/auth/AccessToken;
    .locals 3

    .prologue
    .line 182
    new-instance v0, Ltwitter4j/auth/AccessToken;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mAccessToken:Ljava/lang/String;

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mAccessTokenSecret:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ltwitter4j/auth/AccessToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static native nativecloseAuthorizeURL()V
.end method

.method public static native nativegotOAuthAccessToken(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static native nativegotOAuthRequestToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static native nativegotOAuthVerifier(Ljava/lang/String;)V
.end method

.method public static native nativeonException(Ljava/lang/String;IILjava/lang/String;Z)V
.end method

.method public static native nativeupdatedStatus()V
.end method

.method public static native nativeupdatedStatusWithMedia()V
.end method

.method public static openAuthorizeURL(Ljava/lang/String;)V
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 318
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->openAuthorizeURLInternal(Ljava/lang/String;)V

    .line 319
    return-void
.end method

.method private openAuthorizeURLInternal(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 321
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v2

    invoke-virtual {v2}, Ljp/co/drecom/bisque/lib/HandlerJava;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 322
    .local v0, "_activity":Landroid/app/Activity;
    move-object v1, p1

    .line 323
    .local v1, "_url":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v2

    new-instance v3, Ljp/co/drecom/bisque/lib/BQTwitterHelper$7;

    invoke-direct {v3, p0, v0, v1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$7;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 331
    return-void
.end method

.method public static refreshAuthorization()V
    .locals 1

    .prologue
    .line 111
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->refreshAuthorizationInternal()V

    .line 112
    return-void
.end method

.method private refreshAuthorizationInternal()V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_factory:Ltwitter4j/AsyncTwitterFactory;

    invoke-virtual {v0}, Ltwitter4j/AsyncTwitterFactory;->getInstance()Ltwitter4j/AsyncTwitter;

    move-result-object v0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    .line 116
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljp/co/drecom/bisque/lib/BQTwitterHelper$1;)V

    invoke-interface {v0, v1}, Ltwitter4j/AsyncTwitter;->addListener(Ltwitter4j/TwitterListener;)V

    .line 118
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerKey:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerSecret:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerKey:Ljava/lang/String;

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerSecret:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ltwitter4j/AsyncTwitter;->setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method

.method public static setAccessToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "accessToken"    # Ljava/lang/String;
    .param p1, "accessTokenSecret"    # Ljava/lang/String;

    .prologue
    .line 191
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->setAccessTokenInternal(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method private setAccessTokenInternal(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "accessTokenSecret"    # Ljava/lang/String;

    .prologue
    .line 194
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mAccessToken:Ljava/lang/String;

    .line 195
    iput-object p2, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mAccessTokenSecret:Ljava/lang/String;

    .line 196
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->loadAccessToken()Ltwitter4j/auth/AccessToken;

    move-result-object v1

    invoke-interface {v0, v1}, Ltwitter4j/AsyncTwitter;->setOAuthAccessToken(Ltwitter4j/auth/AccessToken;)V

    .line 197
    return-void
.end method

.method public static setConsumerKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "consumerKey"    # Ljava/lang/String;
    .param p1, "consumerSecret"    # Ljava/lang/String;

    .prologue
    .line 129
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->setConsumerKeyInternal(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method private setConsumerKeyInternal(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "consumerKey"    # Ljava/lang/String;
    .param p2, "consumerSecret"    # Ljava/lang/String;

    .prologue
    .line 133
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerKey:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerSecret:Ljava/lang/String;

    .line 134
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mTwitter:Ltwitter4j/AsyncTwitter;

    invoke-interface {v0, p1, p2}, Ltwitter4j/AsyncTwitter;->setOAuthConsumer(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerKey:Ljava/lang/String;

    .line 137
    iput-object p2, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mConsumerSecret:Ljava/lang/String;

    .line 139
    :cond_0
    return-void
.end method

.method public static setExecutingBrowser(Z)V
    .locals 1
    .param p0, "ret"    # Z

    .prologue
    .line 350
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->setExecutingBrowserInternal(Z)V

    .line 351
    return-void
.end method

.method private setExecutingBrowserInternal(Z)V
    .locals 0
    .param p1, "ret"    # Z

    .prologue
    .line 357
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->m_executing_browser:Z

    .line 358
    return-void
.end method

.method public static setHttpTimeout(II)V
    .locals 0
    .param p0, "connection_timeout"    # I
    .param p1, "read_timeout"    # I

    .prologue
    .line 41
    sput p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->http_connection_timeout:I

    .line 42
    sput p1, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->http_read_timeout:I

    .line 43
    return-void
.end method

.method public static setVerifier(Ljava/lang/String;)V
    .locals 2
    .param p0, "verifier"    # Ljava/lang/String;

    .prologue
    .line 251
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->setVerifierInternal(Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method private setVerifierInternal(Ljava/lang/String;)V
    .locals 0
    .param p1, "verifier"    # Ljava/lang/String;

    .prologue
    .line 258
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mVerifier:Ljava/lang/String;

    .line 259
    return-void
.end method

.method public static updateStatus(Ljava/lang/String;)V
    .locals 4
    .param p0, "status"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->updateStatusInternal(ZLjava/lang/String;Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method private updateStatusInternal(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "withMedia"    # Z
    .param p2, "status"    # Ljava/lang/String;
    .param p3, "filepath"    # Ljava/lang/String;

    .prologue
    .line 223
    move-object v1, p2

    .line 224
    .local v1, "_status":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 225
    move-object v0, p3

    .line 226
    .local v0, "_filepath":Ljava/lang/String;
    new-instance v2, Ltwitter4j/StatusUpdate;

    invoke-direct {v2, v1}, Ltwitter4j/StatusUpdate;-><init>(Ljava/lang/String;)V

    .line 227
    .local v2, "statusUpdate":Ltwitter4j/StatusUpdate;
    iput-object p3, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mMediaFile:Ljava/lang/String;

    .line 228
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mMediaFile:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ltwitter4j/StatusUpdate;->media(Ljava/io/File;)Ltwitter4j/StatusUpdate;

    .line 229
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v3

    new-instance v4, Ljp/co/drecom/bisque/lib/BQTwitterHelper$3;

    invoke-direct {v4, p0, v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$3;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ltwitter4j/StatusUpdate;)V

    invoke-virtual {v3, v4}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    .line 244
    .end local v0    # "_filepath":Ljava/lang/String;
    .end local v2    # "statusUpdate":Ltwitter4j/StatusUpdate;
    :goto_0
    return-void

    .line 236
    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mMediaFile:Ljava/lang/String;

    .line 237
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v3

    new-instance v4, Ljp/co/drecom/bisque/lib/BQTwitterHelper$4;

    invoke-direct {v4, p0, v1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$4;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnUIThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static updateStatusWithMedia(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "status"    # Ljava/lang/String;
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 213
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->updateStatusInternal(ZLjava/lang/String;Ljava/lang/String;)V

    .line 214
    return-void
.end method


# virtual methods
.method public getCallbackURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->mCallbackURL:Ljava/lang/String;

    return-object v0
.end method
