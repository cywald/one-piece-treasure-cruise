.class Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;
.super Ljava/lang/Object;
.source "BQPaymentBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQPaymentBridge;->consumePurchase(JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

.field final synthetic val$requestIdentifier:J

.field final synthetic val$token:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;JLjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 310
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    iput-wide p2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$requestIdentifier:J

    iput-object p4, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$token:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, -0x1

    .line 314
    iget-object v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v6}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v6

    if-nez v6, :cond_0

    .line 317
    iget-wide v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$requestIdentifier:J

    const-string v8, "Can not call consumePurchase()"

    invoke-static {v6, v7, v9, v8}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1300(JILjava/lang/String;)V

    .line 362
    :goto_0
    return-void

    .line 320
    :cond_0
    iget-object v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$token:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-object v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$token:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 322
    :cond_1
    iget-wide v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$requestIdentifier:J

    const-string v8, "Can\'t consume. No token."

    invoke-static {v6, v7, v9, v8}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1300(JILjava/lang/String;)V

    goto :goto_0

    .line 326
    :cond_2
    iget-object v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v6}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$900(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;

    move-result-object v6

    iget-object v7, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$token:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ex/android/util/Purchase;

    .line 329
    .local v2, "purchase":Lcom/ex/android/util/Purchase;
    :try_start_0
    iget-object v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v6}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v6

    const/4 v7, 0x3

    iget-object v8, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v8}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$400(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$token:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v9}, Lcom/android/vending/billing/IInAppBillingService;->consumePurchase(ILjava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 331
    .local v4, "response":I
    if-nez v4, :cond_4

    .line 333
    const-string v1, ""

    .line 334
    .local v1, "productIdentifier":Ljava/lang/String;
    const-string v3, ""

    .line 335
    .local v3, "purchaseData":Ljava/lang/String;
    const-string v5, ""

    .line 336
    .local v5, "signature":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 337
    invoke-virtual {v2}, Lcom/ex/android/util/Purchase;->getSku()Ljava/lang/String;

    move-result-object v1

    .line 338
    invoke-virtual {v2}, Lcom/ex/android/util/Purchase;->getOriginalJson()Ljava/lang/String;

    move-result-object v3

    .line 339
    invoke-virtual {v2}, Lcom/ex/android/util/Purchase;->getSignature()Ljava/lang/String;

    move-result-object v5

    .line 341
    :cond_3
    iget-wide v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$requestIdentifier:J

    invoke-static {v6, v7, v1, v3, v5}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1400(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 353
    .end local v1    # "productIdentifier":Ljava/lang/String;
    .end local v3    # "purchaseData":Ljava/lang/String;
    .end local v4    # "response":I
    .end local v5    # "signature":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Landroid/os/RemoteException;
    iget-wide v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$requestIdentifier:J

    const/16 v8, -0x3e9

    const-string v9, "Remote exception while consuming."

    invoke-static {v6, v7, v8, v9}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1300(JILjava/lang/String;)V

    goto :goto_0

    .line 349
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v4    # "response":I
    :cond_4
    :try_start_1
    iget-wide v6, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$4;->val$requestIdentifier:J

    const-string v8, "Can\'t consume. No token."

    invoke-static {v6, v7, v4, v8}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1300(JILjava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
