.class public Ljp/co/drecom/bisque/lib/BQPlayGameService;
.super Ljava/lang/Object;
.source "BQPlayGameService.java"

# interfaces
.implements Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;


# static fields
.field private static final REQUEST_ACHIEVEMENTS:I = 0x0

.field static final TAG:Ljava/lang/String; = "BQPlayGameService"

.field static activity:Landroid/app/Activity;

.field static gameHelper:Ljp/co/drecom/bisque/lib/BQGameHelper;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "parent"    # Landroid/app/Activity;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sput-object p1, Ljp/co/drecom/bisque/lib/BQPlayGameService;->activity:Landroid/app/Activity;

    .line 26
    new-instance v0, Ljp/co/drecom/bisque/lib/BQGameHelper;

    sget-object v1, Ljp/co/drecom/bisque/lib/BQPlayGameService;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Ljp/co/drecom/bisque/lib/BQGameHelper;-><init>(Landroid/app/Activity;)V

    sput-object v0, Ljp/co/drecom/bisque/lib/BQPlayGameService;->gameHelper:Ljp/co/drecom/bisque/lib/BQGameHelper;

    .line 27
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPlayGameService;->gameHelper:Ljp/co/drecom/bisque/lib/BQGameHelper;

    invoke-virtual {v0, p0}, Ljp/co/drecom/bisque/lib/BQGameHelper;->setup(Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;)V

    .line 28
    return-void
.end method

.method static synthetic access$000(ZZ)V
    .locals 0
    .param p0, "x0"    # Z
    .param p1, "x1"    # Z

    .prologue
    .line 14
    invoke-static {p0, p1}, Ljp/co/drecom/bisque/lib/BQPlayGameService;->native_ReportSignInState(ZZ)V

    return-void
.end method

.method static synthetic access$100(ZZ)V
    .locals 0
    .param p0, "x0"    # Z
    .param p1, "x1"    # Z

    .prologue
    .line 14
    invoke-static {p0, p1}, Ljp/co/drecom/bisque/lib/BQPlayGameService;->native_ReportSignInResult(ZZ)V

    return-void
.end method

.method private static native native_ReportSignInResult(ZZ)V
.end method

.method private static native native_ReportSignInState(ZZ)V
.end method

.method public static postStartSignIn()V
    .locals 2

    .prologue
    .line 119
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPlayGameService;->activity:Landroid/app/Activity;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$8;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/BQPlayGameService$8;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 128
    return-void
.end method

.method public static postStartSignOut()V
    .locals 2

    .prologue
    .line 131
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPlayGameService;->activity:Landroid/app/Activity;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$9;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/BQPlayGameService$9;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 140
    return-void
.end method

.method public static postUnlockAchievement(Ljava/lang/String;)V
    .locals 2
    .param p0, "achId"    # Ljava/lang/String;

    .prologue
    .line 154
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPlayGameService;->activity:Landroid/app/Activity;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$11;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPlayGameService$11;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 162
    return-void
.end method

.method public static showAchievement()V
    .locals 2

    .prologue
    .line 143
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPlayGameService;->activity:Landroid/app/Activity;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$10;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/BQPlayGameService$10;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 151
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 31
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPlayGameService;->gameHelper:Ljp/co/drecom/bisque/lib/BQGameHelper;

    invoke-virtual {v0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/BQGameHelper;->onActivityResult(IILandroid/content/Intent;)V

    .line 32
    return-void
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 109
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$7;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPlayGameService$7;-><init>(Ljp/co/drecom/bisque/lib/BQPlayGameService;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 116
    return-void
.end method

.method public onInteractiveSignInFailed(Lcom/google/android/gms/common/api/Status;)V
    .locals 4
    .param p1, "status"    # Lcom/google/android/gms/common/api/Status;

    .prologue
    .line 91
    const/4 v1, 0x0

    .line 92
    .local v1, "tmp_isCanceled":Z
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v2

    const/16 v3, 0x30d5

    if-ne v2, v3, :cond_0

    .line 94
    const/4 v1, 0x1

    .line 96
    :cond_0
    move v0, v1

    .line 98
    .local v0, "isCanceled":Z
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v2

    new-instance v3, Ljp/co/drecom/bisque/lib/BQPlayGameService$6;

    invoke-direct {v3, p0, v0}, Ljp/co/drecom/bisque/lib/BQPlayGameService$6;-><init>(Ljp/co/drecom/bisque/lib/BQPlayGameService;Z)V

    invoke-virtual {v2, v3}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 105
    return-void
.end method

.method public onInteractiveSignInSucceeded(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    .locals 2
    .param p1, "googleSignInAccount"    # Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    .prologue
    .line 80
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$5;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPlayGameService$5;-><init>(Ljp/co/drecom/bisque/lib/BQPlayGameService;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 87
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 53
    sget-object v0, Ljp/co/drecom/bisque/lib/BQPlayGameService;->gameHelper:Ljp/co/drecom/bisque/lib/BQGameHelper;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQGameHelper;->onResume()V

    .line 54
    return-void
.end method

.method public onSilentSignInFailed(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 69
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$4;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPlayGameService$4;-><init>(Ljp/co/drecom/bisque/lib/BQPlayGameService;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 76
    return-void
.end method

.method public onSilentSignInSucceeded(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    .locals 2
    .param p1, "googleSignInAccount"    # Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    .prologue
    .line 58
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$3;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPlayGameService$3;-><init>(Ljp/co/drecom/bisque/lib/BQPlayGameService;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 65
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 35
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$1;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPlayGameService$1;-><init>(Ljp/co/drecom/bisque/lib/BQPlayGameService;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 41
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 44
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQPlayGameService$2;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQPlayGameService$2;-><init>(Ljp/co/drecom/bisque/lib/BQPlayGameService;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 50
    return-void
.end method
