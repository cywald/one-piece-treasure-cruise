.class public Ljp/co/drecom/bisque/lib/HandlerJava;
.super Ljava/lang/Object;
.source "HandlerJava.java"


# static fields
.field private static m_instance:Ljp/co/drecom/bisque/lib/HandlerJava;


# instance fields
.field private m_activity:Landroid/app/Activity;

.field private m_handler:Landroid/os/Handler;

.field private m_surfaceview:Landroid/opengl/GLSurfaceView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_instance:Ljp/co/drecom/bisque/lib/HandlerJava;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_activity:Landroid/app/Activity;

    .line 11
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_handler:Landroid/os/Handler;

    .line 12
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_surfaceview:Landroid/opengl/GLSurfaceView;

    return-void
.end method

.method public static getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_instance:Ljp/co/drecom/bisque/lib/HandlerJava;

    return-object v0
.end method

.method public static initialize(Landroid/app/Activity;Landroid/os/Handler;Landroid/opengl/GLSurfaceView;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "surfaceview"    # Landroid/opengl/GLSurfaceView;

    .prologue
    .line 15
    new-instance v0, Ljp/co/drecom/bisque/lib/HandlerJava;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/HandlerJava;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_instance:Ljp/co/drecom/bisque/lib/HandlerJava;

    .line 16
    sget-object v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_instance:Ljp/co/drecom/bisque/lib/HandlerJava;

    iput-object p1, v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_handler:Landroid/os/Handler;

    .line 17
    sget-object v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_instance:Ljp/co/drecom/bisque/lib/HandlerJava;

    iput-object p0, v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_activity:Landroid/app/Activity;

    .line 18
    sget-object v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_instance:Ljp/co/drecom/bisque/lib/HandlerJava;

    iput-object p2, v0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_surfaceview:Landroid/opengl/GLSurfaceView;

    .line 19
    return-void
.end method


# virtual methods
.method public enableRunOnGLThread()Z
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_surfaceview:Landroid/opengl/GLSurfaceView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enableRunOnUIThread()Z
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_activity:Landroid/app/Activity;

    return-object v0
.end method

.method public runOnGLThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "pRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 27
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_surfaceview:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, p1}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    .line 28
    return-void
.end method

.method public runOnUIThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "pRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 33
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/HandlerJava;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 34
    return-void
.end method
