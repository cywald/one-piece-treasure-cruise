.class public Ljp/co/drecom/bisque/lib/IndependenceException;
.super Ljava/lang/RuntimeException;
.source "IndependenceException.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 8
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 17
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/IndependenceException;->destruct()V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 22
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/IndependenceException;->destruct()V

    .line 23
    return-void
.end method


# virtual methods
.method destruct()V
    .locals 0

    .prologue
    .line 26
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->abort()V

    .line 27
    return-void
.end method
