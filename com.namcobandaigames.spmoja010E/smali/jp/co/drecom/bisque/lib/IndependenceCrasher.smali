.class public Ljp/co/drecom/bisque/lib/IndependenceCrasher;
.super Ljava/lang/Object;
.source "IndependenceCrasher.java"


# static fields
.field static _instance:Ljp/co/drecom/bisque/lib/IndependenceCrasher;


# instance fields
.field private _target:Ljp/co/drecom/bisque/lib/IndependenceHandler;


# direct methods
.method public constructor <init>(Ljp/co/drecom/bisque/lib/IndependenceHandler;)V
    .locals 1
    .param p1, "_Target"    # Ljp/co/drecom/bisque/lib/IndependenceHandler;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/IndependenceCrasher;->_target:Ljp/co/drecom/bisque/lib/IndependenceHandler;

    .line 11
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/IndependenceCrasher;->_target:Ljp/co/drecom/bisque/lib/IndependenceHandler;

    .line 12
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/IndependenceCrasher;->initInstance()V

    .line 13
    return-void
.end method

.method public static crash(Ljava/lang/String;)V
    .locals 1
    .param p0, "_Message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 25
    sget-object v0, Ljp/co/drecom/bisque/lib/IndependenceCrasher;->_instance:Ljp/co/drecom/bisque/lib/IndependenceCrasher;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Ljp/co/drecom/bisque/lib/IndependenceException;

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/IndependenceException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/IndependenceCrasher;->_instance:Ljp/co/drecom/bisque/lib/IndependenceCrasher;

    invoke-virtual {v0, p0}, Ljp/co/drecom/bisque/lib/IndependenceCrasher;->crashInternal(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public static init(Ljp/co/drecom/bisque/lib/IndependenceHandler;)V
    .locals 1
    .param p0, "_Target"    # Ljp/co/drecom/bisque/lib/IndependenceHandler;

    .prologue
    .line 17
    new-instance v0, Ljp/co/drecom/bisque/lib/IndependenceCrasher;

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/IndependenceCrasher;-><init>(Ljp/co/drecom/bisque/lib/IndependenceHandler;)V

    sput-object v0, Ljp/co/drecom/bisque/lib/IndependenceCrasher;->_instance:Ljp/co/drecom/bisque/lib/IndependenceCrasher;

    .line 18
    return-void
.end method


# virtual methods
.method crashInternal(Ljava/lang/String;)V
    .locals 1
    .param p1, "_Message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/IndependenceCrasher;->_target:Ljp/co/drecom/bisque/lib/IndependenceHandler;

    invoke-interface {v0, p1}, Ljp/co/drecom/bisque/lib/IndependenceHandler;->independence(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method initInstance()V
    .locals 0

    .prologue
    .line 14
    return-void
.end method
