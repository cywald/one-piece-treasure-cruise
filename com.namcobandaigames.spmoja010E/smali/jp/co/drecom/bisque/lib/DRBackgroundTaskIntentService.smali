.class public Ljp/co/drecom/bisque/lib/DRBackgroundTaskIntentService;
.super Landroid/app/IntentService;
.source "DRBackgroundTaskIntentService.java"


# static fields
.field public static final ACTION_BGTASK:Ljava/lang/String; = "jp.co.drecom.bisque.lib.drbackgroundtask.action.BGTASK"

.field public static final BROADCAST_BGTASK:Ljava/lang/String; = "jp.co.drecom.bisque.lib.drbackgroundtask.broadcast.BGTASK"

.field public static final EXTENDED_STATUS:Ljava/lang/String; = "jp.co.drecom.bisque.lib.drbackgroundtask.extra.STATUS"

.field public static final EXTENDED_UUID:Ljava/lang/String; = "jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID"

.field public static final STATUS_FINISH:Ljava/lang/String; = "jp.co.drecom.bisque.lib.drbackgroundtask.status.FINISH"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "DRBackgroundTaskIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method private handleActionBGTask(Ljava/lang/String;)V
    .locals 3
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-static {p1}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskHelper;->task(Ljava/lang/String;)V

    .line 59
    new-instance v0, Landroid/content/Intent;

    const-string v1, "jp.co.drecom.bisque.lib.drbackgroundtask.broadcast.BGTASK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 60
    .local v0, "localIntent":Landroid/content/Intent;
    const-string v1, "jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v1, "jp.co.drecom.bisque.lib.drbackgroundtask.extra.STATUS"

    const-string v2, "jp.co.drecom.bisque.lib.drbackgroundtask.status.FINISH"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 63
    return-void
.end method

.method public static startAction(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 25
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ljp/co/drecom/bisque/lib/DRBackgroundTaskIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 26
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "jp.co.drecom.bisque.lib.drbackgroundtask.action.BGTASK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 27
    const-string v1, "jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 29
    const/4 v1, 0x1

    return v1
.end method


# virtual methods
.method public onDestroy()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 71
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 44
    if-eqz p1, :cond_0

    .line 45
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "action":Ljava/lang/String;
    const-string v2, "jp.co.drecom.bisque.lib.drbackgroundtask.extra.UUID"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 47
    .local v1, "uuid":Ljava/lang/String;
    const-string v2, "jp.co.drecom.bisque.lib.drbackgroundtask.action.BGTASK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    invoke-direct {p0, v1}, Ljp/co/drecom/bisque/lib/DRBackgroundTaskIntentService;->handleActionBGTask(Ljava/lang/String;)V

    .line 51
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "uuid":Ljava/lang/String;
    :cond_0
    return-void
.end method
