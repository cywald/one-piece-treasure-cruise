.class Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;
.super Ljava/lang/Object;
.source "BQFontUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/BQFontUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TextProperty"
.end annotation


# instance fields
.field private final m_height:I

.field private final m_stringLength:I

.field private final m_width:I


# direct methods
.method constructor <init>(III)V
    .locals 0
    .param p1, "pWidth"    # I
    .param p2, "pHeight"    # I
    .param p3, "pLength"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;->m_width:I

    .line 35
    iput p2, p0, Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;->m_height:I

    .line 36
    iput p3, p0, Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;->m_stringLength:I

    .line 37
    return-void
.end method
