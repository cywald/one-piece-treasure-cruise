.class public Ljp/co/drecom/bisque/lib/BQArtilleryJava;
.super Ljava/lang/Object;
.source "BQArtilleryJava.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljp/co/drecom/bisque/lib/BQArtilleryJava;J)Z
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQArtilleryJava;
    .param p1, "x1"    # J

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljp/co/drecom/bisque/lib/BQArtilleryJava;->callbackMethod(J)Z

    move-result v0

    return v0
.end method

.method private native callbackMethod(J)Z
.end method


# virtual methods
.method public invoke(J)Z
    .locals 3
    .param p1, "_Klass"    # J

    .prologue
    .line 10
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 11
    .local v0, "acc":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 12
    const-string v1, "BQArtilleryJava"

    const-string v2, "NOT initialized!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 13
    const/4 v1, 0x0

    .line 23
    :goto_0
    return v1

    .line 16
    :cond_0
    new-instance v1, Ljp/co/drecom/bisque/lib/BQArtilleryJava$1;

    invoke-direct {v1, p0, p1, p2}, Ljp/co/drecom/bisque/lib/BQArtilleryJava$1;-><init>(Ljp/co/drecom/bisque/lib/BQArtilleryJava;J)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 23
    const/4 v1, 0x1

    goto :goto_0
.end method
