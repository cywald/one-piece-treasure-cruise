.class public Ljp/co/drecom/bisque/lib/DRToastManager;
.super Ljava/lang/Object;
.source "DRToastManager.java"


# static fields
.field private static m_instance:Ljp/co/drecom/bisque/lib/DRToastManager;


# instance fields
.field private m_handler:Landroid/os/Handler;

.field private m_toast:Ljp/co/drecom/bisque/lib/DRToast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_instance:Ljp/co/drecom/bisque/lib/DRToastManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_handler:Landroid/os/Handler;

    .line 12
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_toast:Ljp/co/drecom/bisque/lib/DRToast;

    return-void
.end method

.method static synthetic access$000()Ljp/co/drecom/bisque/lib/DRToastManager;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_instance:Ljp/co/drecom/bisque/lib/DRToastManager;

    return-object v0
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/DRToastManager;)Ljp/co/drecom/bisque/lib/DRToast;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRToastManager;

    .prologue
    .line 9
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_toast:Ljp/co/drecom/bisque/lib/DRToast;

    return-object v0
.end method

.method public static initialize(Landroid/app/Activity;Landroid/os/Handler;)V
    .locals 2
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 22
    new-instance v0, Ljp/co/drecom/bisque/lib/DRToastManager;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/DRToastManager;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_instance:Ljp/co/drecom/bisque/lib/DRToastManager;

    .line 23
    sget-object v0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_instance:Ljp/co/drecom/bisque/lib/DRToastManager;

    iput-object p1, v0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_handler:Landroid/os/Handler;

    .line 24
    sget-object v0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_instance:Ljp/co/drecom/bisque/lib/DRToastManager;

    new-instance v1, Ljp/co/drecom/bisque/lib/DRToast;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/DRToast;-><init>(Landroid/app/Activity;)V

    iput-object v1, v0, Ljp/co/drecom/bisque/lib/DRToastManager;->m_toast:Ljp/co/drecom/bisque/lib/DRToast;

    .line 25
    return-void
.end method

.method public static showMessage(Ljava/lang/String;II)V
    .locals 5
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "anchor"    # I
    .param p2, "showTime"    # I

    .prologue
    .line 29
    sget-object v3, Ljp/co/drecom/bisque/lib/DRToastManager;->m_instance:Ljp/co/drecom/bisque/lib/DRToastManager;

    if-nez v3, :cond_0

    .line 45
    :goto_0
    return-void

    .line 34
    :cond_0
    move-object v1, p0

    .line 35
    .local v1, "messageValue":Ljava/lang/String;
    invoke-static {p1}, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->toEnum(I)Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;

    move-result-object v0

    .line 36
    .local v0, "anchorValue":Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    invoke-static {p2}, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->toEnum(I)Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    move-result-object v2

    .line 37
    .local v2, "showTimeValue":Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    sget-object v3, Ljp/co/drecom/bisque/lib/DRToastManager;->m_instance:Ljp/co/drecom/bisque/lib/DRToastManager;

    iget-object v3, v3, Ljp/co/drecom/bisque/lib/DRToastManager;->m_handler:Landroid/os/Handler;

    new-instance v4, Ljp/co/drecom/bisque/lib/DRToastManager$1;

    invoke-direct {v4, v1, v0, v2}, Ljp/co/drecom/bisque/lib/DRToastManager$1;-><init>(Ljava/lang/String;Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
