.class public Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;
.super Lcom/google/firebase/messaging/FirebaseMessagingService;
.source "BQFcmListenerService.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService$MessageThread;
    }
.end annotation


# static fields
.field private static final EXTRA_CHANNEL_ID:Ljava/lang/String; = "android_channel_id"

.field private static final EXTRA_MESSAGE:Ljava/lang/String; = "message"

.field private static final TAG:Ljava/lang/String; = "BQFcmListenerService"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;-><init>()V

    return-void
.end method

.method private getActivityFullName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "activityName"    # Ljava/lang/String;

    .prologue
    .line 126
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "packageActivityName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 129
    .local v0, "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    move-object p1, v2

    .line 137
    :cond_0
    :goto_0
    return-object p1

    .line 134
    :catch_0
    move-exception v1

    .line 135
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    invoke-direct {p0, p1}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->searchActivityFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private getActivityName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "activityName"    # Ljava/lang/String;

    .prologue
    .line 112
    const/4 v0, 0x0

    .line 114
    .local v0, "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return-object p1

    .line 116
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    invoke-direct {p0, p1}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->getActivityFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private isLessThanAndroidO()Z
    .locals 2

    .prologue
    .line 161
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->getOsVersionCode()I

    move-result v0

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private postNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "channelId"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 73
    .local v17, "res":Landroid/content/res/Resources;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    .line 74
    .local v16, "pkgName":Ljava/lang/String;
    const-string v2, "application/vnd.jp.co.drecom.bisque.lib.Notification.remote-notification-key"

    .line 75
    .local v2, "type":Ljava/lang/String;
    const-string v4, "ic_statusbar"

    const-string v5, "drawable"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v5, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 76
    .local v3, "iconId":I
    const-string v4, "app_name"

    const-string v5, "string"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v5, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 77
    .local v8, "notifyId":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 78
    .local v6, "title":Ljava/lang/String;
    const-string v4, "notification_startup_activity"

    const-string v5, "string"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v5, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 79
    .local v9, "activityName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->getActivityName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 80
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v7, p2

    move-object/from16 v10, p3

    invoke-static/range {v2 .. v10}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->createRegistParams(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;

    move-result-object v11

    .line 91
    .local v11, "params":Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;
    invoke-direct/range {p0 .. p0}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->isLessThanAndroidO()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 92
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p1

    invoke-static {v0, v11, v4, v5}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationIntentService;->schedule(Landroid/content/Context;Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;J)V

    .line 97
    :goto_0
    return-void

    .line 95
    :cond_0
    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v10, p1

    invoke-static/range {v10 .. v15}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;->schedule(Landroid/content/Context;Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;JJ)V

    goto :goto_0
.end method

.method private receiveRemoteNotification(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService$1;

    invoke-direct {v1, p0, p1}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService$1;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 109
    :cond_0
    return-void
.end method

.method private searchActivityFullName(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "activityName"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 146
    .local v3, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 147
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    .line 148
    .local v0, "activities":[Landroid/content/pm/ActivityInfo;
    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 149
    .local v1, "activity":Landroid/content/pm/ActivityInfo;
    iget-object v6, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 150
    iget-object p1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    .end local v0    # "activities":[Landroid/content/pm/ActivityInfo;
    .end local v1    # "activity":Landroid/content/pm/ActivityInfo;
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_1
    return-object p1

    .line 148
    .restart local v0    # "activities":[Landroid/content/pm/ActivityInfo;
    .restart local v1    # "activity":Landroid/content/pm/ActivityInfo;
    .restart local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 155
    .end local v0    # "activities":[Landroid/content/pm/ActivityInfo;
    .end local v1    # "activity":Landroid/content/pm/ActivityInfo;
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method public onMessageReceived(Lcom/google/firebase/messaging/RemoteMessage;)V
    .locals 7
    .param p1, "message"    # Lcom/google/firebase/messaging/RemoteMessage;

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/firebase/messaging/RemoteMessage;->getFrom()Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "from":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/firebase/messaging/RemoteMessage;->getData()Ljava/util/Map;

    move-result-object v1

    .line 53
    .local v1, "data":Ljava/util/Map;
    const/4 v3, 0x0

    .line 54
    .local v3, "dataMsg":Ljava/lang/String;
    const/4 v2, 0x0

    .line 55
    .local v2, "dataChId":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 56
    const-string v6, "message"

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "dataMsg":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 57
    .restart local v3    # "dataMsg":Ljava/lang/String;
    const-string v6, "android_channel_id"

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "dataChId":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 59
    .restart local v2    # "dataChId":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/google/firebase/messaging/RemoteMessage;->getNotification()Lcom/google/firebase/messaging/RemoteMessage$Notification;

    move-result-object v5

    .line 61
    .local v5, "notify":Lcom/google/firebase/messaging/RemoteMessage$Notification;
    if-nez v5, :cond_1

    if-eqz v3, :cond_1

    .line 62
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 63
    .local v0, "context":Landroid/content/Context;
    invoke-direct {p0, v0, v3, v2}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->postNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    if-eqz v3, :cond_2

    .line 67
    invoke-direct {p0, v3}, Ljp/co/drecom/bisque/lib/Notification/BQFcmListenerService;->receiveRemoteNotification(Ljava/lang/String;)V

    .line 69
    :cond_2
    return-void
.end method
