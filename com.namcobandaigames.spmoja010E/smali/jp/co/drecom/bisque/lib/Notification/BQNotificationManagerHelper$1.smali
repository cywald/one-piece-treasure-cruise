.class Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;
.super Ljava/lang/Object;
.source "BQNotificationManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_createNotificationChannel(Ljava/lang/String;Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

.field final synthetic val$_enableVibration:Z

.field final synthetic val$_identifier:Ljava/lang/String;

.field final synthetic val$_importance:I

.field final synthetic val$_name:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    .prologue
    .line 223
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    iput-object p2, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->val$_identifier:Ljava/lang/String;

    iput-object p3, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->val$_name:Ljava/lang/String;

    iput p4, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->val$_importance:I

    iput-boolean p5, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->val$_enableVibration:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 226
    new-instance v0, Landroid/app/NotificationChannel;

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->val$_identifier:Ljava/lang/String;

    iget-object v2, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->val$_name:Ljava/lang/String;

    iget v3, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->val$_importance:I

    invoke-direct {v0, v1, v2, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 227
    .local v0, "channel":Landroid/app/NotificationChannel;
    iget-boolean v1, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;->val$_enableVibration:Z

    invoke-virtual {v0, v1}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 228
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->access$000()Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 229
    return-void
.end method
