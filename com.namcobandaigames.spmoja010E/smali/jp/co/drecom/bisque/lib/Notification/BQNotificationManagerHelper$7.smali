.class Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;
.super Ljava/lang/Object;
.source "BQNotificationManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_registLocalNotification(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

.field final synthetic val$_channelId:Ljava/lang/String;

.field final synthetic val$_deadline:J

.field final synthetic val$_fireDate:J

.field final synthetic val$_iconColor:Ljava/lang/String;

.field final synthetic val$_key:Ljava/lang/String;

.field final synthetic val$_message:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    .prologue
    .line 434
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    iput-object p2, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_key:Ljava/lang/String;

    iput-object p3, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_channelId:Ljava/lang/String;

    iput-wide p4, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_fireDate:J

    iput-object p6, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_iconColor:Ljava/lang/String;

    iput-object p7, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_message:Ljava/lang/String;

    iput-wide p8, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_deadline:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 437
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v14

    .line 438
    .local v14, "context":Landroid/content/Context;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_key:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 439
    .local v4, "type":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    const-string v6, "app_name"

    invoke-static {v3, v6}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->access$100(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 440
    .local v8, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    const-string v6, "app_name"

    const-string v7, "string"

    invoke-static {v3, v6, v7}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->access$200(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 441
    .local v10, "notifyId":I
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    .line 442
    .local v11, "activityName":Ljava/lang/String;
    if-nez v8, :cond_0

    .line 468
    :goto_0
    return-void

    .line 445
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    const-string v6, "ic_statusbar"

    const-string v7, "drawable"

    invoke-static {v3, v6, v7}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->access$200(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 446
    .local v5, "iconId":I
    move-object/from16 v0, p0

    iget-object v12, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_channelId:Ljava/lang/String;

    .line 447
    .local v12, "chlId":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    invoke-virtual {v3}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResChannelId()Ljava/lang/String;

    move-result-object v12

    .line 448
    :cond_1
    const-wide/16 v6, 0x3e8

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_fireDate:J

    move-wide/from16 v18, v0

    mul-long v20, v6, v18

    .line 450
    .local v20, "when":J
    new-instance v2, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-object/from16 v0, p0

    iget-object v6, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_iconColor:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_key:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_message:Ljava/lang/String;

    invoke-direct/range {v2 .. v12}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 461
    .local v2, "params":Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;
    move-object/from16 v0, p0

    iget-object v3, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    invoke-static {v3}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->access$300(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 462
    move-wide/from16 v0, v20

    invoke-static {v14, v2, v0, v1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationIntentService;->schedule(Landroid/content/Context;Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;J)V

    goto :goto_0

    .line 465
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v16, v20, v6

    .line 466
    .local v16, "latency":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;->val$_deadline:J

    move-wide/from16 v18, v0

    move-object v15, v2

    invoke-static/range {v14 .. v19}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;->schedule(Landroid/content/Context;Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;JJ)V

    goto :goto_0
.end method
