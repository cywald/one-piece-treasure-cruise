.class public Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;
.super Landroid/app/job/JobService;
.source "BQNotificationJobService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService$AlarmRunnable;
    }
.end annotation


# static fields
.field private static final OVERRIDE_DEADLINE:J = 0x7530L

.field private static final TAG:Ljava/lang/String; = "BQNotificationJobService"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;Landroid/app/job/JobParameters;)V
    .locals 0
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;
    .param p1, "x1"    # Landroid/app/job/JobParameters;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;->registRemoteNotification(Landroid/app/job/JobParameters;)V

    return-void
.end method

.method public static cancel(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 83
    const-string v1, "jobscheduler"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 84
    .local v0, "jobScheduler":Landroid/app/job/JobScheduler;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    .line 85
    return-void
.end method

.method private registRemoteNotification(Landroid/app/job/JobParameters;)V
    .locals 23
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .prologue
    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/app/job/JobParameters;->getExtras()Landroid/os/PersistableBundle;

    move-result-object v6

    .line 146
    .local v6, "extras":Landroid/os/PersistableBundle;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TYPE"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 147
    .local v18, "type":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID"

    const/16 v20, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 148
    .local v8, "iconId":I
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 149
    .local v7, "iconColor":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 150
    .local v9, "key":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 151
    .local v17, "title":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 152
    .local v10, "message":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

    const/16 v20, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I

    move-result v12

    .line 153
    .local v12, "notifyId":I
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 154
    .local v2, "activityName":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 156
    .local v4, "channelId":Ljava/lang/String;
    if-eqz v18, :cond_0

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v8, v0, :cond_0

    if-eqz v17, :cond_0

    if-eqz v10, :cond_0

    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v12, v0, :cond_1

    .line 158
    :cond_0
    const-string v19, "BQNotificationJobService"

    const-string v20, "registRemoteNotification() Invalid Extras."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :goto_0
    return-void

    .line 161
    :cond_1
    if-nez v4, :cond_2

    const-string v4, ""

    .line 163
    :cond_2
    const/4 v13, 0x0

    .line 164
    .local v13, "remoteIntentClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v19

    if-eqz v19, :cond_4

    .line 166
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    .line 177
    :cond_3
    :goto_1
    if-nez v13, :cond_5

    .line 179
    const-string v19, "BQNotificationJobService"

    const-string v20, "registRemoteNotification() Could not get Activity."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 168
    :cond_4
    if-eqz v2, :cond_3

    .line 170
    :try_start_0
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    goto :goto_1

    .line 171
    :catch_0
    move-exception v5

    .line 172
    .local v5, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v5}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 173
    new-instance v19, Ljava/lang/IllegalStateException;

    const-string v20, "Failed get startup activity class. class name=%s"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v2, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 183
    .end local v5    # "e":Ljava/lang/ClassNotFoundException;
    :cond_5
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v19

    if-eqz v19, :cond_6

    .line 184
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResChannelId()Ljava/lang/String;

    move-result-object v4

    .line 187
    :cond_6
    new-instance v19, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 189
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 190
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 191
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    const/16 v20, 0x1

    .line 193
    invoke-virtual/range {v19 .. v20}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    .line 195
    .local v3, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    if-eqz v7, :cond_7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_7

    .line 196
    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 199
    :cond_7
    const/4 v14, 0x0

    .line 200
    .local v14, "requestCode":I
    const-string v19, "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 201
    const/4 v14, -0x1

    .line 203
    :cond_8
    new-instance v19, Landroid/content/Intent;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 204
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v20, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

    .line 205
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v20, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

    .line 206
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v20, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

    .line 207
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v15

    .line 208
    .local v15, "resultIntent":Landroid/content/Intent;
    const/high16 v19, 0x8000000

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v14, v15, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 210
    .local v16, "resultPendingIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 211
    const-string v19, "notification"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/NotificationManager;

    .line 212
    .local v11, "notificationManager":Landroid/app/NotificationManager;
    if-nez v9, :cond_9

    .line 213
    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v12, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 216
    :cond_9
    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v9, v12, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method public static schedule(Landroid/content/Context;Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;JJ)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "params"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;
    .param p2, "latency"    # J
    .param p4, "deadline"    # J

    .prologue
    const-wide/16 v8, 0x0

    .line 52
    new-instance v2, Landroid/os/PersistableBundle;

    invoke-direct {v2}, Landroid/os/PersistableBundle;-><init>()V

    .line 53
    .local v2, "extras":Landroid/os/PersistableBundle;
    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TYPE"

    iget-object v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->type:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID"

    iget v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->iconId:I

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putInt(Ljava/lang/String;I)V

    .line 55
    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->iconColor:Ljava/lang/String;

    if-eqz v5, :cond_0

    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR"

    iget-object v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->iconColor:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_0
    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->key:Ljava/lang/String;

    if-eqz v5, :cond_1

    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

    iget-object v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->key:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_1
    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE"

    iget-object v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->title:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

    iget-object v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->message:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

    iget v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->notifyId:I

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putInt(Ljava/lang/String;I)V

    .line 60
    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME"

    iget-object v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->activityName:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->channelId:Ljava/lang/String;

    if-eqz v5, :cond_2

    const-string v5, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID"

    iget-object v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->channelId:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_2
    cmp-long v5, p2, v8

    if-gtz v5, :cond_3

    const-wide/16 p2, 0x0

    .line 63
    :cond_3
    cmp-long v5, p4, v8

    if-gtz v5, :cond_4

    .line 64
    const-wide/16 p4, 0x7530

    .line 66
    :cond_4
    new-instance v1, Landroid/content/ComponentName;

    const-class v5, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;

    invoke-direct {v1, p0, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    .local v1, "componentName":Landroid/content/ComponentName;
    new-instance v5, Landroid/app/job/JobInfo$Builder;

    iget-object v6, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->type:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-direct {v5, v6, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 68
    invoke-virtual {v5, v2}, Landroid/app/job/JobInfo$Builder;->setExtras(Landroid/os/PersistableBundle;)Landroid/app/job/JobInfo$Builder;

    move-result-object v5

    .line 69
    invoke-virtual {v5, p2, p3}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v5

    add-long v6, p2, p4

    .line 70
    invoke-virtual {v5, v6, v7}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    .line 71
    .local v0, "builder":Landroid/app/job/JobInfo$Builder;
    invoke-virtual {v0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v3

    .line 72
    .local v3, "jobInfo":Landroid/app/job/JobInfo;
    const-string v5, "jobscheduler"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/job/JobScheduler;

    .line 73
    .local v4, "jobScheduler":Landroid/app/job/JobScheduler;
    invoke-virtual {v4, v3}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 74
    return-void
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 2
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .prologue
    .line 124
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService$AlarmRunnable;

    invoke-direct {v1, p0, p1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService$AlarmRunnable;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;Landroid/app/job/JobParameters;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 125
    const/4 v0, 0x1

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .prologue
    const/4 v0, 0x0

    .line 136
    invoke-virtual {p0, p1, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 137
    return v0
.end method
