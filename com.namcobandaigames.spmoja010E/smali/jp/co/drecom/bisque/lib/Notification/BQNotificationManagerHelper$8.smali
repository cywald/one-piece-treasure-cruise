.class Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$8;
.super Ljava/lang/Object;
.source "BQNotificationManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_cancelLocalNotification(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

.field final synthetic val$_key:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    .prologue
    .line 476
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$8;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    iput-object p2, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$8;->val$_key:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 479
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 480
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$8;->val$_key:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 482
    .local v1, "type":Ljava/lang/String;
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$8;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    invoke-static {v2}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->access$300(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 483
    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationIntentService;->cancel(Landroid/content/Context;Ljava/lang/String;)V

    .line 488
    :goto_0
    return-void

    .line 486
    :cond_0
    invoke-static {v0, v1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationJobService;->cancel(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
