.class public Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;
.super Ljava/lang/Object;
.source "BQNotificationManagerHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;
    }
.end annotation


# static fields
.field public static final EXTRA_ACTIVITY_NAME:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME"

.field public static final EXTRA_CHANNEL_ID:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID"

.field public static final EXTRA_ICON_COLOR:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR"

.field public static final EXTRA_ICON_ID:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID"

.field public static final EXTRA_ID:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

.field public static final EXTRA_KEY:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

.field public static final EXTRA_MESSAGE:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE"

.field public static final EXTRA_TYPE:Ljava/lang/String; = "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TYPE"

.field private static final TAG:Ljava/lang/String; = "BQNotificationManagerHelper"

.field public static final TYPE_LOCAL_NOTIFICATION_PREFIX:Ljava/lang/String; = "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-"

.field public static final TYPE_REMOTE_NOTIFICATION:Ljava/lang/String; = "application/vnd.jp.co.drecom.bisque.lib.Notification.remote-notification-key"

.field private static mInstance:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;


# instance fields
.field private mInitialized:Z

.field private mToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInstance:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInitialized:Z

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mToken:Ljava/lang/String;

    .line 131
    return-void
.end method

.method private _createNotificationChannel(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 7
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "importance"    # I
    .param p4, "enableVibration"    # Z

    .prologue
    .line 218
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->isLessThanAndroidO()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    move-object v2, p1

    .line 220
    .local v2, "_identifier":Ljava/lang/String;
    move-object v3, p2

    .line 221
    .local v3, "_name":Ljava/lang/String;
    move v4, p3

    .line 222
    .local v4, "_importance":I
    move v5, p4

    .line 223
    .local v5, "_enableVibration":Z
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v6

    new-instance v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$1;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {v6, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 232
    .end local v2    # "_identifier":Ljava/lang/String;
    .end local v3    # "_name":Ljava/lang/String;
    .end local v4    # "_importance":I
    .end local v5    # "_enableVibration":Z
    :cond_0
    return-void
.end method

.method private _deleteNotificationChannel(Ljava/lang/String;)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 254
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->isLessThanAndroidO()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    move-object v0, p1

    .line 256
    .local v0, "_identifier":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$2;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$2;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 263
    .end local v0    # "_identifier":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private _isAutoInitEnabled()Z
    .locals 1

    .prologue
    .line 273
    invoke-static {}, Lcom/google/firebase/messaging/FirebaseMessaging;->getInstance()Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/messaging/FirebaseMessaging;->isAutoInitEnabled()Z

    move-result v0

    return v0
.end method

.method private _registLocalNotification(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 11
    .param p1, "fireDate"    # J
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "iconColor"    # Ljava/lang/String;
    .param p6, "channelId"    # Ljava/lang/String;
    .param p7, "deadline"    # J

    .prologue
    .line 426
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 470
    :goto_0
    return-void

    .line 428
    :cond_0
    move-wide v4, p1

    .line 429
    .local v4, "_fireDate":J
    move-object v2, p3

    .line 430
    .local v2, "_key":Ljava/lang/String;
    move-object v7, p4

    .line 431
    .local v7, "_message":Ljava/lang/String;
    move-object/from16 v6, p5

    .line 432
    .local v6, "_iconColor":Ljava/lang/String;
    move-object/from16 v3, p6

    .line 433
    .local v3, "_channelId":Ljava/lang/String;
    move-wide/from16 v8, p7

    .line 434
    .local v8, "_deadline":J
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v10

    new-instance v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$7;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v10, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private _registRemoteNotification()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mToken:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 302
    invoke-static {}, Lcom/google/firebase/iid/FirebaseInstanceId;->getInstance()Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/iid/FirebaseInstanceId;->getToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mToken:Ljava/lang/String;

    .line 304
    :cond_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mToken:Ljava/lang/String;

    invoke-direct {p0, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->handleRegistered(Ljava/lang/String;)V

    .line 308
    :cond_1
    return-void
.end method

.method private _setAutoInitEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 284
    invoke-static {}, Lcom/google/firebase/messaging/FirebaseMessaging;->getInstance()Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/messaging/FirebaseMessaging;->setAutoInitEnabled(Z)V

    .line 285
    return-void
.end method

.method static synthetic access$000()Landroid/app/NotificationManager;
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;)Z
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    .prologue
    .line 20
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->isLessThanAndroidO()Z

    move-result v0

    return v0
.end method

.method public static cancelLocalNotification(Ljava/lang/String;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 422
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_cancelLocalNotification(Ljava/lang/String;)V

    .line 423
    return-void
.end method

.method public static createNotificationChannel()V
    .locals 6

    .prologue
    .line 183
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v4

    const-string v5, "notification_channel_id"

    invoke-direct {v4, v5}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "identifier":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v4

    const-string v5, "notification_channel_name"

    invoke-direct {v4, v5}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 185
    .local v3, "name":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v4

    const-string v5, "notification_channel_importance"

    invoke-direct {v4, v5}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResInteger(Ljava/lang/String;)I

    move-result v2

    .line 186
    .local v2, "importance":I
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v4

    const-string v5, "notification_channel_enable_vibration"

    invoke-direct {v4, v5}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 187
    .local v0, "enableVibration":Z
    invoke-static {v1, v3, v2, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->createNotificationChannel(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 188
    return-void
.end method

.method public static createNotificationChannel(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "identifier"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 196
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->createNotificationChannel(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 197
    return-void
.end method

.method public static createNotificationChannel(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 1
    .param p0, "identifier"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "importance"    # I
    .param p3, "enableVibration"    # Z

    .prologue
    .line 207
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-direct {v0, p0, p1, p2, p3}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_createNotificationChannel(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 208
    return-void
.end method

.method public static createRegistParams(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;
    .locals 11
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "iconId"    # I
    .param p2, "iconColor"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "message"    # Ljava/lang/String;
    .param p6, "notifyId"    # I
    .param p7, "activityName"    # Ljava/lang/String;
    .param p8, "channelId"    # Ljava/lang/String;

    .prologue
    .line 104
    new-instance v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;

    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static deleteNotificationChannel()V
    .locals 1

    .prologue
    .line 238
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResChannelId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->deleteNotificationChannel(Ljava/lang/String;)V

    .line 239
    return-void
.end method

.method public static deleteNotificationChannel(Ljava/lang/String;)V
    .locals 1
    .param p0, "identifier"    # Ljava/lang/String;

    .prologue
    .line 246
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_deleteNotificationChannel(Ljava/lang/String;)V

    .line 247
    return-void
.end method

.method public static finalization()V
    .locals 1

    .prologue
    .line 166
    sget-object v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInstance:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    if-eqz v0, :cond_0

    .line 167
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInstance:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    .line 169
    :cond_0
    return-void
.end method

.method public static getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInstance:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInstance:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    .line 124
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInstance:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    return-object v0
.end method

.method private static getNotificationManager()Landroid/app/NotificationManager;
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 176
    .local v0, "context":Landroid/content/Context;
    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    return-object v1
.end method

.method private getResBoolean(Ljava/lang/String;)Z
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 569
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 570
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 571
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 572
    .local v1, "pkgName":Ljava/lang/String;
    const-string v4, "bool"

    invoke-virtual {v2, p1, v4, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 573
    .local v3, "resId":I
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    return v4
.end method

.method private getResId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "defType"    # Ljava/lang/String;

    .prologue
    .line 562
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 563
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 564
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 565
    .local v1, "pkgName":Ljava/lang/String;
    invoke-virtual {v2, p1, p2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method private getResInteger(Ljava/lang/String;)I
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 577
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 578
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 579
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 580
    .local v1, "pkgName":Ljava/lang/String;
    const-string v4, "integer"

    invoke-virtual {v2, p1, v4, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 581
    .local v3, "resId":I
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    return v4
.end method

.method private getResString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 585
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 586
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 587
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 588
    .local v1, "pkgName":Ljava/lang/String;
    const-string v4, "string"

    invoke-virtual {v2, p1, v4, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 589
    .local v3, "resId":I
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private getResources()Landroid/content/res/Resources;
    .locals 2

    .prologue
    .line 593
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 594
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    return-object v1
.end method

.method private handleError(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->receiveRegisterForRemoteNotificationError(Ljava/lang/String;)V

    .line 322
    return-void
.end method

.method private handleRegistered(Ljava/lang/String;)V
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 314
    invoke-virtual {p0, p1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->receiveRegisterForRemoteNotification(Ljava/lang/String;)V

    .line 315
    return-void
.end method

.method private initInternal()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 151
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInitialized:Z

    if-ne v0, v1, :cond_0

    .line 159
    :goto_0
    return v1

    .line 155
    :cond_0
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mInitialized:Z

    .line 158
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->createNotificationChannel()V

    goto :goto_0
.end method

.method public static initialize()Z
    .locals 1

    .prologue
    .line 139
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 142
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->initInternal()Z

    move-result v0

    goto :goto_0
.end method

.method public static isAutoInitEnabled()Z
    .locals 1

    .prologue
    .line 269
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_isAutoInitEnabled()Z

    move-result v0

    return v0
.end method

.method private isLessThanAndroidO()Z
    .locals 2

    .prologue
    .line 558
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQAppPlatformManager;->getOsVersionCode()I

    move-result v0

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static native nativeCallbackFuncForReceiveLocalNotification(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static native nativeCallbackFuncForReceiveRegistRemoteNotification(Ljava/lang/String;)V
.end method

.method public static native nativeCallbackFuncForReceiveRegistRemoteNotificationError(Ljava/lang/String;)V
.end method

.method public static native nativeCallbackFuncForReceiveRemoteNotification(Ljava/lang/String;)V
.end method

.method public static native nativeCallbackFuncForReceiveRemoteNotificationError(Ljava/lang/String;)V
.end method

.method public static registLocalNotification(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "fireDate"    # J
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "iconColor"    # Ljava/lang/String;

    .prologue
    .line 402
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResChannelId()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, 0x0

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v7}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->registLocalNotification(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 403
    return-void
.end method

.method public static registLocalNotification(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 10
    .param p0, "fireDate"    # J
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "iconColor"    # Ljava/lang/String;
    .param p5, "channelId"    # Ljava/lang/String;
    .param p6, "deadline"    # J

    .prologue
    .line 415
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v1

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_registLocalNotification(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 416
    return-void
.end method

.method public static registRemoteNotification()V
    .locals 1

    .prologue
    .line 291
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_registRemoteNotification()V

    .line 292
    return-void
.end method

.method public static setAutoInitEnabled(Z)V
    .locals 1
    .param p0, "enable"    # Z

    .prologue
    .line 280
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->_setAutoInitEnabled(Z)V

    .line 281
    return-void
.end method


# virtual methods
.method public _cancelLocalNotification(Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 473
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 490
    :goto_0
    return-void

    .line 475
    :cond_0
    move-object v0, p1

    .line 476
    .local v0, "_key":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$8;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$8;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getResChannelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 598
    const-string v0, "notification_channel_id"

    invoke-direct {p0, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleNotification(Landroid/content/Intent;)Z
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v8, -0x1

    .line 517
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v5

    .line 518
    .local v5, "type":Ljava/lang/String;
    const-string v7, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 519
    .local v1, "key":Ljava/lang/String;
    const-string v7, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 520
    .local v2, "message":Ljava/lang/String;
    const-string v7, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 523
    .local v4, "notifyId":I
    if-eqz v1, :cond_0

    const-string v7, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 524
    :cond_0
    if-eqz v2, :cond_1

    const-string v7, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 525
    :cond_1
    if-eq v4, v8, :cond_2

    const-string v7, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 528
    :cond_2
    if-eqz v5, :cond_4

    const-string v7, "application/vnd.jp.co.drecom.bisque.lib.Notification.remote-notification-key"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 552
    :cond_3
    :goto_0
    return v6

    .line 540
    :cond_4
    if-eqz v5, :cond_5

    const-string v7, "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 542
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 543
    .local v0, "context":Landroid/content/Context;
    const-string v7, "notification"

    invoke-virtual {v0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 544
    .local v3, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v3, v1, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 547
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    .line 548
    invoke-virtual {p0, v1, v2}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->receiveLocalNotification(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 552
    .end local v0    # "context":Landroid/content/Context;
    .end local v3    # "notificationManager":Landroid/app/NotificationManager;
    :cond_5
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public receiveLocalNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 496
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 506
    :goto_0
    return-void

    .line 498
    :cond_0
    move-object v0, p1

    .line 499
    .local v0, "_key":Ljava/lang/String;
    move-object v1, p2

    .line 500
    .local v1, "_message":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$9;

    invoke-direct {v3, p0, v0, v1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$9;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public receiveRegisterForRemoteNotification(Ljava/lang/String;)V
    .locals 3
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 328
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->mToken:Ljava/lang/String;

    .line 330
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 339
    :goto_0
    return-void

    .line 332
    :cond_0
    move-object v0, p1

    .line 333
    .local v0, "_token":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$3;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$3;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public receiveRegisterForRemoteNotificationError(Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 345
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 354
    :goto_0
    return-void

    .line 347
    :cond_0
    move-object v0, p1

    .line 348
    .local v0, "_error":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$4;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$4;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public receiveRemoteNotification(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 360
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 369
    :goto_0
    return-void

    .line 362
    :cond_0
    move-object v0, p1

    .line 363
    .local v0, "_message":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$5;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$5;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public receiveRemoteNotificationError(Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 375
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 384
    :goto_0
    return-void

    .line 377
    :cond_0
    move-object v0, p1

    .line 378
    .local v0, "_error":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$6;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$6;-><init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
