.class public Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;
.super Ljava/lang/Object;
.source "BQNotificationManagerHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RegistParams"
.end annotation


# instance fields
.field public activityName:Ljava/lang/String;

.field public channelId:Ljava/lang/String;

.field public iconColor:Ljava/lang/String;

.field public iconId:I

.field public key:Ljava/lang/String;

.field public message:Ljava/lang/String;

.field public notifyId:I

.field final synthetic this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

.field public title:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "iconId"    # I
    .param p4, "iconColor"    # Ljava/lang/String;
    .param p5, "key"    # Ljava/lang/String;
    .param p6, "title"    # Ljava/lang/String;
    .param p7, "message"    # Ljava/lang/String;
    .param p8, "notifyId"    # I
    .param p9, "activityName"    # Ljava/lang/String;
    .param p10, "channelId"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->this$0:Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p2, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->type:Ljava/lang/String;

    .line 79
    iput p3, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->iconId:I

    .line 80
    iput-object p4, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->iconColor:Ljava/lang/String;

    .line 81
    iput-object p5, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->key:Ljava/lang/String;

    .line 82
    iput-object p6, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->title:Ljava/lang/String;

    .line 83
    iput-object p7, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->message:Ljava/lang/String;

    .line 84
    iput p8, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->notifyId:I

    .line 85
    iput-object p9, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->activityName:Ljava/lang/String;

    .line 86
    iput-object p10, p0, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->channelId:Ljava/lang/String;

    .line 87
    return-void
.end method
