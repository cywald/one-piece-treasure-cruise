.class public Ljp/co/drecom/bisque/lib/Notification/BQNotificationIntentService;
.super Landroid/app/IntentService;
.source "BQNotificationIntentService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BQNotificationIntentService"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "BQNotificationIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public static cancel(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 72
    new-instance v3, Landroid/content/Intent;

    const-class v4, Ljp/co/drecom/bisque/lib/Notification/BQNotificationIntentService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    invoke-virtual {v3, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 74
    .local v1, "intent":Landroid/content/Intent;
    const/4 v3, -0x1

    const/high16 v4, 0x8000000

    invoke-static {p0, v3, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 77
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 78
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 79
    return-void
.end method

.method public static schedule(Landroid/content/Context;Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "params"    # Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;
    .param p2, "when"    # J

    .prologue
    .line 44
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v1, "extras":Landroid/os/Bundle;
    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TYPE"

    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->type:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID"

    iget v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->iconId:I

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 47
    iget-object v4, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->iconColor:Ljava/lang/String;

    if-eqz v4, :cond_0

    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR"

    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->iconColor:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    iget-object v4, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->key:Ljava/lang/String;

    if-eqz v4, :cond_1

    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->key:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_1
    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE"

    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->title:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->message:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

    iget v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->notifyId:I

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 52
    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME"

    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->activityName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-object v4, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->channelId:Ljava/lang/String;

    if-eqz v4, :cond_2

    const-string v4, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID"

    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->channelId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :cond_2
    new-instance v4, Landroid/content/Intent;

    const-class v5, Ljp/co/drecom/bisque/lib/Notification/BQNotificationIntentService;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v5, p1, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper$RegistParams;->type:Ljava/lang/String;

    .line 55
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 56
    invoke-virtual {v4, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    .line 57
    .local v2, "intent":Landroid/content/Intent;
    const/4 v4, -0x1

    const/high16 v5, 0x8000000

    invoke-static {p0, v4, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 61
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    const-string v4, "alarm"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 62
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    const/4 v4, 0x1

    invoke-virtual {v0, v4, p2, p3, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 63
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 23
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 86
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v18

    .line 87
    .local v18, "type":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ICON_ID"

    const/16 v20, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 88
    .local v8, "iconId":I
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_COLOR"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 89
    .local v7, "iconColor":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 90
    .local v9, "key":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_TITLE"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 91
    .local v17, "title":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 92
    .local v10, "message":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

    const/16 v20, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 93
    .local v12, "notifyId":I
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ACTIVITY_NAME"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 94
    .local v3, "activityName":Ljava/lang/String;
    const-string v19, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_CHANNEL_ID"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 96
    .local v5, "channelId":Ljava/lang/String;
    if-eqz v18, :cond_0

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v8, v0, :cond_0

    if-eqz v17, :cond_0

    if-eqz v10, :cond_0

    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v12, v0, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    if-nez v5, :cond_2

    const-string v5, ""

    .line 102
    :cond_2
    const/4 v13, 0x0

    .line 103
    .local v13, "remoteIntentClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v19

    if-eqz v19, :cond_7

    .line 105
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    .line 116
    :cond_3
    :goto_1
    if-eqz v13, :cond_0

    .line 121
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v19

    if-eqz v19, :cond_4

    .line 122
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getResChannelId()Ljava/lang/String;

    move-result-object v5

    .line 125
    :cond_4
    new-instance v19, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 127
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 128
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 129
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    const/16 v20, 0x1

    .line 131
    invoke-virtual/range {v19 .. v20}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    .line 133
    .local v4, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_5

    .line 134
    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 137
    :cond_5
    const/4 v14, 0x0

    .line 138
    .local v14, "requestCode":I
    const-string v19, "application/vnd.jp.co.drecom.bisque.lib.Notification.local-notification-key-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 139
    const/4 v14, -0x1

    .line 141
    :cond_6
    new-instance v19, Landroid/content/Intent;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 142
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v20, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_KEY"

    .line 143
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v20, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_MESSAGE"

    .line 144
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v20, "jp.co.drecom.bisque.lib.Notification.LOCAL_NOTIFICATION_ID"

    .line 145
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v15

    .line 146
    .local v15, "resultIntent":Landroid/content/Intent;
    const/high16 v19, 0x8000000

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v14, v15, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 148
    .local v16, "resultPendingIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 149
    const-string v19, "notification"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/NotificationManager;

    .line 150
    .local v11, "notificationManager":Landroid/app/NotificationManager;
    if-nez v9, :cond_8

    .line 151
    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v12, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 107
    .end local v4    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v11    # "notificationManager":Landroid/app/NotificationManager;
    .end local v14    # "requestCode":I
    .end local v15    # "resultIntent":Landroid/content/Intent;
    .end local v16    # "resultPendingIntent":Landroid/app/PendingIntent;
    :cond_7
    if-eqz v3, :cond_3

    .line 109
    :try_start_0
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    goto/16 :goto_1

    .line 110
    :catch_0
    move-exception v6

    .line 111
    .local v6, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v6}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 112
    new-instance v19, Ljava/lang/IllegalStateException;

    const-string v20, "Failed get startup activity class. class name=%s"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v3, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 154
    .end local v6    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v4    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .restart local v11    # "notificationManager":Landroid/app/NotificationManager;
    .restart local v14    # "requestCode":I
    .restart local v15    # "resultIntent":Landroid/content/Intent;
    .restart local v16    # "resultPendingIntent":Landroid/app/PendingIntent;
    :cond_8
    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v9, v12, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method
