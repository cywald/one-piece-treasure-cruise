.class public Ljp/co/drecom/bisque/lib/Notification/BQFcmInstanceIDListenerService;
.super Lcom/google/firebase/iid/FirebaseInstanceIdService;
.source "BQFcmInstanceIDListenerService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/google/firebase/iid/FirebaseInstanceIdService;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenRefresh()V
    .locals 2

    .prologue
    .line 20
    invoke-static {}, Lcom/google/firebase/iid/FirebaseInstanceId;->getInstance()Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/iid/FirebaseInstanceId;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "refreshedToken":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->getInstance()Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljp/co/drecom/bisque/lib/Notification/BQNotificationManagerHelper;->receiveRegisterForRemoteNotification(Ljava/lang/String;)V

    .line 23
    return-void
.end method
