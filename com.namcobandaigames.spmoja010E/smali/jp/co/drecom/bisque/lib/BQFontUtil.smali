.class public Ljp/co/drecom/bisque/lib/BQFontUtil;
.super Ljava/lang/Object;
.source "BQFontUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static computeTextProperty(Ljava/lang/String;FLandroid/graphics/Paint;)Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;
    .locals 11
    .param p0, "pString"    # Ljava/lang/String;
    .param p1, "pMaxWidth"    # F
    .param p2, "pPaint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v10, 0x0

    .line 85
    invoke-virtual {p2}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    .line 86
    .local v1, "fm":Landroid/graphics/Paint$FontMetricsInt;
    iget v7, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v8, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v7, v8

    int-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v2, v8

    .line 87
    .local v2, "h":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 89
    .local v5, "stringLength":I
    move v3, p1

    .line 90
    .local v3, "maxContentWidth":F
    const/4 v4, 0x0

    .line 92
    .local v4, "resultLength":I
    move v4, v5

    :goto_0
    if-lez v4, :cond_0

    .line 94
    const/4 v6, 0x0

    .line 95
    .local v6, "temp":F
    invoke-virtual {p2, p0, v10, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v7

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-float v6, v8

    .line 96
    cmpg-float v7, v6, v3

    if-gez v7, :cond_1

    .line 98
    move v3, v6

    .line 105
    .end local v6    # "temp":F
    :cond_0
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p0, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "UTF-8"

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    array-length v4, v7
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_1
    new-instance v7, Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;

    float-to-int v8, v3

    invoke-direct {v7, v8, v2, v4}, Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;-><init>(III)V

    return-object v7

    .line 92
    .restart local v6    # "temp":F
    :cond_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 107
    .end local v6    # "temp":F
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static getDrawSize(Ljava/lang/String;Ljava/lang/String;FF)Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;
    .locals 2
    .param p0, "pString"    # Ljava/lang/String;
    .param p1, "pFontName"    # Ljava/lang/String;
    .param p2, "pFontSize"    # F
    .param p3, "pMaxWidth"    # F

    .prologue
    .line 46
    invoke-static {p1, p2}, Ljp/co/drecom/bisque/lib/BQFontUtil;->newPaint(Ljava/lang/String;F)Landroid/graphics/Paint;

    move-result-object v0

    .line 48
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-static {p0, p3, v0}, Ljp/co/drecom/bisque/lib/BQFontUtil;->computeTextProperty(Ljava/lang/String;FLandroid/graphics/Paint;)Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;

    move-result-object v1

    .line 50
    .local v1, "textProperty":Ljp/co/drecom/bisque/lib/BQFontUtil$TextProperty;
    return-object v1
.end method

.method private static newPaint(Ljava/lang/String;F)Landroid/graphics/Paint;
    .locals 7
    .param p0, "pFontName"    # Ljava/lang/String;
    .param p1, "pFontSize"    # F

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 55
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 56
    .local v1, "paint":Landroid/graphics/Paint;
    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 58
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 59
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setHinting(I)V

    .line 60
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setSubpixelText(Z)V

    .line 61
    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 64
    const-string v3, ".ttf"

    invoke-virtual {p0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    :try_start_0
    invoke-static {p0}, Ljp/co/drecom/bisque/lib/BQTypefaces;->get(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 68
    .local v2, "typeFace":Landroid/graphics/Typeface;
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .end local v2    # "typeFace":Landroid/graphics/Typeface;
    :goto_0
    return-object v1

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "BQFontUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "error to create ttf type face: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-static {p0, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0

    .line 76
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-static {p0, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0
.end method
