.class public final Ljp/co/drecom/bisque/lib/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final common_google_play_services_enable_button:I = 0x7f0a0021

.field public static final common_google_play_services_enable_text:I = 0x7f0a0022

.field public static final common_google_play_services_enable_title:I = 0x7f0a0023

.field public static final common_google_play_services_install_button:I = 0x7f0a0024

.field public static final common_google_play_services_install_text:I = 0x7f0a0025

.field public static final common_google_play_services_install_title:I = 0x7f0a0026

.field public static final common_google_play_services_notification_channel_name:I = 0x7f0a0027

.field public static final common_google_play_services_notification_ticker:I = 0x7f0a0028

.field public static final common_google_play_services_unknown_issue:I = 0x7f0a0029

.field public static final common_google_play_services_unsupported_text:I = 0x7f0a002a

.field public static final common_google_play_services_update_button:I = 0x7f0a002b

.field public static final common_google_play_services_update_text:I = 0x7f0a002c

.field public static final common_google_play_services_update_title:I = 0x7f0a002d

.field public static final common_google_play_services_updating_text:I = 0x7f0a002e

.field public static final common_google_play_services_wear_update_text:I = 0x7f0a002f

.field public static final common_open_on_phone:I = 0x7f0a0030

.field public static final common_signin_button_text:I = 0x7f0a0031

.field public static final common_signin_button_text_long:I = 0x7f0a0032

.field public static final fcm_fallback_notification_channel_label:I = 0x7f0a0034

.field public static final gamehelper_app_misconfigured:I = 0x7f0a0036

.field public static final gamehelper_license_failed:I = 0x7f0a0037

.field public static final gamehelper_sign_in_failed:I = 0x7f0a0038

.field public static final gamehelper_unknown_error:I = 0x7f0a0039

.field public static final jp_co_drecom_bqpayment_notfoundservice_dialog_message:I = 0x7f0a003f

.field public static final jp_co_drecom_bqpayment_notfoundservice_dialog_title:I = 0x7f0a0040

.field public static final jp_co_drecom_bqupdate_dialog_message:I = 0x7f0a0041

.field public static final jp_co_drecom_bqupdate_dialog_title:I = 0x7f0a0042

.field public static final jp_co_drecom_common_label_Exit:I = 0x7f0a0043

.field public static final jp_co_drecom_common_label_cancel:I = 0x7f0a0044

.field public static final jp_co_drecom_common_label_ok:I = 0x7f0a0045

.field public static final status_bar_notification_info_overflow:I = 0x7f0a004c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
