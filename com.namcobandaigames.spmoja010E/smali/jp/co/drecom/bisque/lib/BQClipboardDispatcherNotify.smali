.class public Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;
.super Ljava/lang/Object;
.source "BQClipboardDispatcherNotify.java"


# static fields
.field private static final MAX_INVOKE_WAIT_TIME:I = 0x1

.field private static clipBoardText:Ljava/lang/String;

.field public static dispatcher:Ljp/co/drecom/bisque/lib/BQClipboardDispatcher;

.field public static handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 8
    sput-object p0, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->clipBoardText:Ljava/lang/String;

    return-object p0
.end method

.method public static getStringFromClipboard()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 35
    sput-object v5, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->clipBoardText:Ljava/lang/String;

    .line 36
    new-instance v1, Ljp/co/drecom/bisque/lib/AwaitInvoker;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/AwaitInvoker;-><init>()V

    .line 37
    .local v1, "invoker":Ljp/co/drecom/bisque/lib/AwaitInvoker;
    new-instance v0, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify$2;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify$2;-><init>()V

    .line 44
    .local v0, "getClipBoardTextMethod":Ljava/lang/Runnable;
    sget-object v3, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->handler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v0, v4}, Ljp/co/drecom/bisque/lib/AwaitInvoker;->invokeAndWait(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    move-result v2

    .line 45
    .local v2, "succeeded":Z
    if-nez v2, :cond_0

    .line 47
    sput-object v5, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->clipBoardText:Ljava/lang/String;

    .line 49
    :cond_0
    sget-object v3, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->clipBoardText:Ljava/lang/String;

    return-object v3
.end method

.method public static setDispatcher(Ljp/co/drecom/bisque/lib/BQClipboardDispatcher;Landroid/os/Handler;)V
    .locals 0
    .param p0, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQClipboardDispatcher;
    .param p1, "_handler"    # Landroid/os/Handler;

    .prologue
    .line 15
    sput-object p0, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQClipboardDispatcher;

    .line 16
    sput-object p1, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->handler:Landroid/os/Handler;

    .line 17
    return-void
.end method

.method public static setStringToClipboard(Ljava/lang/String;)V
    .locals 3
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 21
    move-object v0, p0

    .line 22
    .local v0, "_string":Ljava/lang/String;
    sget-object v1, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify$1;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQClipboardDispatcherNotify$1;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 28
    return-void
.end method
