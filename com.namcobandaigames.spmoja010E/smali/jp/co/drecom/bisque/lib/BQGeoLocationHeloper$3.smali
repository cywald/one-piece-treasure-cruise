.class Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;
.super Ljava/lang/Object;
.source "BQGeoLocationHeloper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->startInternal()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    .prologue
    .line 217
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x1388

    const/high16 v4, 0x41200000    # 10.0f

    .line 221
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->initListeners()V

    .line 223
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->access$100(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->access$200(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "network"

    iget-object v5, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-static {v5}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->access$100(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationListener;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 226
    :cond_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->access$300(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->access$200(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "gps"

    iget-object v5, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;->this$0:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-static {v5}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->access$300(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationListener;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 229
    :cond_1
    return-void
.end method
