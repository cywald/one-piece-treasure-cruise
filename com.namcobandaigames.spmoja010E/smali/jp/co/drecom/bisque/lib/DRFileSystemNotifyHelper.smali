.class public Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;
.super Ljava/lang/Object;
.source "DRFileSystemNotifyHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;
    }
.end annotation


# static fields
.field private static mInstMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;",
            ">;"
        }
    .end annotation
.end field

.field private static mInstance:Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;


# instance fields
.field private mInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInitialized:Z

    .line 27
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-static {p0, p1}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->nativeCallbackFuncForReceiveNotify(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static finalization()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    if-eqz v0, :cond_0

    .line 58
    sget-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 59
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    .line 61
    :cond_0
    return-void
.end method

.method public static getInstance()Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    .line 21
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstance:Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    return-object v0
.end method

.method private initInternal()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 45
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInitialized:Z

    if-ne v0, v1, :cond_0

    .line 50
    :goto_0
    return v1

    .line 48
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstMap:Ljava/util/HashMap;

    .line 49
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInitialized:Z

    goto :goto_0
.end method

.method public static initialize()Z
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 37
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->getInstance()Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->initInternal()Z

    move-result v0

    goto :goto_0
.end method

.method private static native nativeCallbackFuncForReceiveNotify(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static setPath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 68
    sget-object v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 71
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->getInstance()Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->setPathInternal(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private setPathInternal(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 79
    new-instance v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;

    invoke-direct {v0, p1}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;-><init>(Ljava/lang/String;)V

    .line 80
    .local v0, "fsNotify":Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;->startWatching()V

    .line 81
    sget-object v1, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    const/4 v1, 0x1

    return v1
.end method

.method public static unsetPath(Ljava/lang/String;)V
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 90
    sget-object v1, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper;->mInstMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;

    .line 91
    .local v0, "fsNotify":Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/DRFileSystemNotifyHelper$DRFileSystemNotify;->stopWatching()V

    .line 94
    :cond_0
    return-void
.end method
