.class public final enum Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
.super Ljava/lang/Enum;
.source "DRToast.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/DRToast;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SHOW_TIME"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

.field public static final enum LONG:Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

.field public static final enum SHORT:Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;


# instance fields
.field private m_value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    const-string v1, "SHORT"

    invoke-direct {v0, v1, v2, v2}, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->SHORT:Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    .line 12
    new-instance v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v3, v3}, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->LONG:Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    sget-object v1, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->SHORT:Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    aput-object v1, v0, v2

    sget-object v1, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->LONG:Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    aput-object v1, v0, v3

    sput-object v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->$VALUES:[Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput p3, p0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->m_value:I

    .line 18
    return-void
.end method

.method public static toEnum(I)Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    .locals 5
    .param p0, "value"    # I

    .prologue
    .line 25
    invoke-static {}, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->values()[Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    move-result-object v1

    .line 26
    .local v1, "enumValues":[Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 28
    .local v0, "enumValue":Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 33
    .end local v0    # "enumValue":Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    :goto_1
    return-object v0

    .line 26
    .restart local v0    # "enumValue":Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 33
    .end local v0    # "enumValue":Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    :cond_1
    sget-object v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->SHORT:Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    return-object v0
.end method

.method public static values()[Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->$VALUES:[Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    invoke-virtual {v0}, [Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->m_value:I

    return v0
.end method
