.class public interface abstract Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;
.super Ljava/lang/Object;
.source "BQWebViewDispatcher.java"


# virtual methods
.method public abstract addWebView(IIIILjava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract addWebView(IIIILjava/lang/String;Ljava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract addWebView(IIIILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract bouncesWebView(ZI)V
.end method

.method public abstract callbackForWebViewDidFailLoadWithError(Ljava/lang/String;)V
.end method

.method public abstract callbackForWebViewDidFinishLoad(I)V
.end method

.method public abstract callbackForWebViewShouldStartLoadWithRequest(Ljava/lang/String;I)Z
.end method

.method public abstract clearCache(I)V
.end method

.method public abstract clearCookie(I)V
.end method

.method public abstract enableCache(ZI)V
.end method

.method public abstract enableCookie(ZI)V
.end method

.method public abstract enableWebView(ZI)V
.end method

.method public abstract executeJsInWebView(Ljava/lang/String;I)V
.end method

.method public abstract reloadWebView(I)V
.end method

.method public abstract removeWebView(I)V
.end method

.method public abstract requestWebView(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract requestWebView(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract requestWebView(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setOrderWebView(II)V
.end method

.method public abstract setRectWebView(IIIII)V
.end method
