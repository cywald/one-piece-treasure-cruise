.class public Ljp/co/drecom/bisque/lib/BQJNIHelper;
.super Ljava/lang/Object;
.source "BQJNIHelper.java"


# static fields
.field private static self:Ljp/co/drecom/bisque/lib/BQJNIHelper;


# instance fields
.field private activity:Landroid/app/Activity;

.field private audioSampleBufSize:I

.field private audioSampleRate:I

.field private audioSvc:Landroid/media/AudioManager;

.field private isSSRChecked:Z

.field private isSSRMapped:Z

.field private lastAudioRingMode:I

.field private lastAudioVolume:I

.field private vibratorSvc:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput v0, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->lastAudioRingMode:I

    .line 25
    iput v0, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->lastAudioVolume:I

    .line 26
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isSSRChecked:Z

    .line 27
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isSSRMapped:Z

    .line 28
    iput v0, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSampleRate:I

    .line 29
    iput v0, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSampleBufSize:I

    .line 32
    return-void
.end method

.method public static native abort()V
.end method

.method static synthetic access$000()Ljp/co/drecom/bisque/lib/BQJNIHelper;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    return-object v0
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/BQJNIHelper;)Landroid/os/Vibrator;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQJNIHelper;

    .prologue
    .line 18
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->vibratorSvc:Landroid/os/Vibrator;

    return-object v0
.end method

.method public static native enterAudioSilentMode()V
.end method

.method public static getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    move-result-object v0

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method public static getAudioMasterVolume()F
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 85
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    move-result-object v3

    iget-object v3, v3, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSvc:Landroid/media/AudioManager;

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    .line 86
    .local v2, "vol":I
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    move-result-object v3

    iget-object v3, v3, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSvc:Landroid/media/AudioManager;

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 87
    .local v1, "max":I
    if-nez v2, :cond_0

    .line 88
    const/4 v3, 0x0

    .line 91
    :goto_0
    return v3

    .line 90
    :cond_0
    int-to-float v3, v2

    int-to-float v4, v1

    div-float v0, v3, v4

    .line 91
    .local v0, "fv":F
    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v3, v0

    goto :goto_0
.end method

.method public static getAudioOutputDevice()I
    .locals 3

    .prologue
    .line 95
    const/4 v0, 0x0

    .line 96
    .local v0, "ret":I
    const/4 v1, 0x1

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getAudioSvc()Landroid/media/AudioManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getAudioSvc()Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 97
    :cond_0
    const/high16 v1, 0x2000000

    or-int/2addr v0, v1

    .line 103
    :goto_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getAudioSvc()Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    or-int/lit16 v0, v0, 0x4000

    .line 106
    :cond_1
    return v0

    .line 100
    :cond_2
    const/high16 v1, 0x1000000

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method public static getAudioSampleBufSize()I
    .locals 1

    .prologue
    .line 151
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v0, :cond_0

    .line 152
    const/4 v0, -0x1

    .line 154
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSampleBufSize:I

    goto :goto_0
.end method

.method public static getAudioSampleRate()I
    .locals 1

    .prologue
    .line 144
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v0, :cond_0

    .line 145
    const/4 v0, -0x1

    .line 147
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSampleRate:I

    goto :goto_0
.end method

.method public static getAudioSvc()Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 81
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    move-result-object v0

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSvc:Landroid/media/AudioManager;

    return-object v0
.end method

.method public static getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQJNIHelper;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    .line 51
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    return-object v0
.end method

.method public static getLastAudioRingMode()I
    .locals 1

    .prologue
    .line 123
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v0, :cond_0

    .line 124
    const/4 v0, -0x1

    .line 126
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->lastAudioRingMode:I

    goto :goto_0
.end method

.method public static getLastAudioVolume()I
    .locals 1

    .prologue
    .line 137
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v0, :cond_0

    .line 138
    const/4 v0, -0x1

    .line 140
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->lastAudioVolume:I

    goto :goto_0
.end method

.method public static getSystemServiceWXP(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p0, "_Name"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private initInstance(Landroid/app/Activity;)V
    .locals 3
    .param p1, "_Acc"    # Landroid/app/Activity;

    .prologue
    .line 35
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->activity:Landroid/app/Activity;

    .line 36
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->activity:Landroid/app/Activity;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSvc:Landroid/media/AudioManager;

    .line 39
    :try_start_0
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->activity:Landroid/app/Activity;

    const-string v2, "vibrator"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->vibratorSvc:Landroid/os/Vibrator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->initAudioState()V

    .line 45
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    iput-object v1, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->vibratorSvc:Landroid/os/Vibrator;

    goto :goto_0
.end method

.method public static native initNativeContext(Ljava/lang/Object;)V
.end method

.method public static native initNativeContextDisableSound(Ljava/lang/Object;)V
.end method

.method public static isAudioSilentMode()Z
    .locals 3

    .prologue
    .line 110
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getAudioSvc()Landroid/media/AudioManager;

    move-result-object v0

    .line 111
    .local v0, "mgr":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    .line 112
    .local v1, "rm":I
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 66
    sget-object v1, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    sget-object v1, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget-object v1, v1, Ljp/co/drecom/bisque/lib/BQJNIHelper;->activity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 73
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isSoundStateRecieverMapped()Z
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 201
    sget-object v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v6, :cond_1

    .line 231
    .local v0, "acc":Landroid/app/Activity;
    .local v3, "pkgInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return v4

    .line 204
    .end local v0    # "acc":Landroid/app/Activity;
    .end local v3    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    sget-object v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget-boolean v6, v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isSSRChecked:Z

    if-ne v5, v6, :cond_2

    .line 205
    sget-object v4, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget-boolean v4, v4, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isSSRMapped:Z

    goto :goto_0

    .line 208
    :cond_2
    sget-object v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iput-boolean v5, v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isSSRChecked:Z

    .line 209
    sget-object v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iput-boolean v4, v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isSSRMapped:Z

    .line 211
    sget-object v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget-object v0, v6, Ljp/co/drecom/bisque/lib/BQJNIHelper;->activity:Landroid/app/Activity;

    .line 212
    .restart local v0    # "acc":Landroid/app/Activity;
    const/4 v3, 0x0

    .line 214
    .restart local v3    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 215
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    .line 214
    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 216
    if-eqz v3, :cond_0

    iget-object v6, v3, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v6, :cond_0

    .line 224
    iget-object v7, v3, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    array-length v8, v7

    move v6, v4

    :goto_1
    if-ge v6, v8, :cond_0

    aget-object v1, v7, v6

    .line 225
    .local v1, "ai":Landroid/content/pm/ActivityInfo;
    iget-object v9, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-class v10, Ljp/co/drecom/bisque/lib/SoundStateReceiver;

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 227
    sget-object v4, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iput-boolean v5, v4, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isSSRMapped:Z

    move v4, v5

    .line 228
    goto :goto_0

    .line 220
    .end local v1    # "ai":Landroid/content/pm/ActivityInfo;
    :catch_0
    move-exception v2

    .line 221
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0

    .line 224
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "ai":Landroid/content/pm/ActivityInfo;
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public static native leaveAudioSilentMode()V
.end method

.method public static setContext(Landroid/app/Activity;)V
    .locals 1
    .param p0, "_Acc"    # Landroid/app/Activity;

    .prologue
    .line 55
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getInstance()Ljp/co/drecom/bisque/lib/BQJNIHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->initInstance(Landroid/app/Activity;)V

    .line 56
    return-void
.end method

.method public static setLastAudioRingMode(I)V
    .locals 1
    .param p0, "_Mode"    # I

    .prologue
    .line 116
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 119
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iput p0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->lastAudioRingMode:I

    goto :goto_0
.end method

.method public static setLastAudioVolume(I)V
    .locals 1
    .param p0, "_Volume"    # I

    .prologue
    .line 130
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iput p0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->lastAudioVolume:I

    goto :goto_0
.end method

.method public static vibrate(II)V
    .locals 2
    .param p0, "_Count"    # I
    .param p1, "_Inteval"    # I

    .prologue
    .line 238
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    if-eqz v0, :cond_0

    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->vibratorSvc:Landroid/os/Vibrator;

    if-nez v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    sget-object v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->self:Ljp/co/drecom/bisque/lib/BQJNIHelper;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->activity:Landroid/app/Activity;

    new-instance v1, Ljp/co/drecom/bisque/lib/BQJNIHelper$1;

    invoke-direct {v1, p1, p0}, Ljp/co/drecom/bisque/lib/BQJNIHelper$1;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public initAudioState()V
    .locals 11

    .prologue
    const/4 v10, -0x1

    .line 158
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getAudioSvc()Landroid/media/AudioManager;

    move-result-object v3

    .line 159
    .local v3, "mgr":Landroid/media/AudioManager;
    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v5

    iput v5, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->lastAudioVolume:I

    .line 160
    invoke-virtual {v3}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v5

    iput v5, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->lastAudioRingMode:I

    .line 163
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-ge v5, v6, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    const/4 v1, 0x0

    .line 171
    .local v1, "ccsf":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "getProperty"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 172
    if-eqz v1, :cond_0

    .line 182
    const/4 v5, 0x1

    :try_start_1
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "android.media.property.OUTPUT_SAMPLE_RATE"

    aput-object v7, v5, v6

    invoke-virtual {v1, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 183
    .local v4, "sr":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 184
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSampleRate:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 191
    .end local v4    # "sr":Ljava/lang/String;
    :cond_2
    :goto_1
    const/4 v5, 0x1

    :try_start_2
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "android.media.property.OUTPUT_FRAMES_PER_BUFFER"

    aput-object v7, v5, v6

    invoke-virtual {v1, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 192
    .local v0, "bs":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 193
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSampleBufSize:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 195
    .end local v0    # "bs":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 196
    .local v2, "e":Ljava/lang/Exception;
    iput v10, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSampleBufSize:I

    goto :goto_0

    .line 175
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 177
    .restart local v2    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 186
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 187
    .restart local v2    # "e":Ljava/lang/Exception;
    iput v10, p0, Ljp/co/drecom/bisque/lib/BQJNIHelper;->audioSampleRate:I

    goto :goto_1
.end method
