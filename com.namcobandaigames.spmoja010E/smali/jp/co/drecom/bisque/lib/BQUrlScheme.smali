.class public Ljp/co/drecom/bisque/lib/BQUrlScheme;
.super Ljava/lang/Object;
.source "BQUrlScheme.java"


# instance fields
.field private parent:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "ctx":Landroid/content/Context;
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQUrlScheme;->parent:Landroid/app/Activity;

    .line 16
    return-void
.end method


# virtual methods
.method public execOpenApplicationDetailsSettings()V
    .locals 5

    .prologue
    .line 36
    const-string v2, "package"

    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQUrlScheme;->parent:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 38
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 40
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQUrlScheme;->parent:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public execUrlScheme(Ljava/lang/String;)V
    .locals 3
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 26
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 27
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQUrlScheme;->parent:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 28
    :catch_0
    move-exception v2

    goto :goto_0
.end method
