.class public Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcherNotify;
.super Ljava/lang/Object;
.source "BQSleepSettingDispatcherNotify.java"


# static fields
.field private static dispatcher:Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcher;

.field private static handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcher;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcher;

    return-object v0
.end method

.method public static setDeviceSleep(Z)V
    .locals 3
    .param p0, "set"    # Z

    .prologue
    .line 19
    move v0, p0

    .line 20
    .local v0, "_set":Z
    sget-object v1, Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcherNotify$1;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcherNotify$1;-><init>(Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 26
    return-void
.end method

.method public static setDispatcher(Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcher;Landroid/os/Handler;)V
    .locals 0
    .param p0, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcher;
    .param p1, "_handler"    # Landroid/os/Handler;

    .prologue
    .line 13
    sput-object p0, Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcher;

    .line 14
    sput-object p1, Ljp/co/drecom/bisque/lib/BQSleepSettingDispatcherNotify;->handler:Landroid/os/Handler;

    .line 15
    return-void
.end method
