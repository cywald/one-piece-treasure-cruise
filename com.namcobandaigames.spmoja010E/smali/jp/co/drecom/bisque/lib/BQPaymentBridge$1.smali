.class Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;
.super Ljava/lang/Object;
.source "BQPaymentBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQPaymentBridge;->requestPaymentDetails([Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

.field final synthetic val$products:[Ljava/lang/String;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;[Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 105
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    iput-object p2, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->val$products:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    .line 109
    iget-object v9, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v9}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v9

    if-nez v9, :cond_0

    .line 112
    const/4 v9, -0x1

    const-string v10, "Can not call getSkuDetails()"

    invoke-static {v9, v10}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$100(ILjava/lang/String;)V

    .line 178
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v9, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v9}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$200(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->clear()V

    .line 118
    iget-object v9, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->val$products:[Ljava/lang/String;

    if-nez v9, :cond_1

    .line 120
    const-string v9, "INAPP_PURCHASE_ITEM_LIST"

    invoke-static {v14, v9}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$100(ILjava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    iget-object v9, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->val$products:[Ljava/lang/String;

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 124
    .local v7, "skuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-nez v9, :cond_2

    .line 126
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$300()V

    goto :goto_0

    .line 129
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 130
    .local v5, "sku":Ljava/lang/String;
    iget-object v10, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v10}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$200(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v5, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 133
    .end local v5    # "sku":Ljava/lang/String;
    :cond_3
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 134
    .local v2, "querySkus":Landroid/os/Bundle;
    const-string v9, "ITEM_ID_LIST"

    invoke-virtual {v2, v9, v7}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 138
    const/4 v6, 0x0

    .line 140
    .local v6, "skuDetails":Landroid/os/Bundle;
    :try_start_0
    iget-object v9, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v9}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v9

    const/4 v10, 0x3

    iget-object v11, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$400(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "inapp"

    invoke-interface {v9, v10, v11, v12, v2}, Lcom/android/vending/billing/IInAppBillingService;->getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 146
    if-eqz v6, :cond_5

    const-string v9, "DETAILS_LIST"

    invoke-virtual {v6, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 147
    iget-object v9, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-virtual {v9, v6}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v3

    .line 148
    .local v3, "response":I
    if-eqz v3, :cond_4

    .line 150
    const/4 v9, 0x6

    const-string v10, "INAPP_PURCHASE_ITEM_LIST"

    invoke-static {v9, v10}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$100(ILjava/lang/String;)V

    goto :goto_0

    .line 141
    .end local v3    # "response":I
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Landroid/os/RemoteException;
    const-string v9, "INAPP_PURCHASE_ITEM_LIST"

    invoke-static {v14, v9}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$100(ILjava/lang/String;)V

    .line 143
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 153
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v3    # "response":I
    :cond_4
    const-string v9, "INAPP_PURCHASE_ITEM_LIST"

    invoke-static {v13, v9}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$100(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 158
    .end local v3    # "response":I
    :cond_5
    const-string v9, "DETAILS_LIST"

    invoke-virtual {v6, v9}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 160
    .local v4, "responseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 161
    .local v8, "thisResponse":Ljava/lang/String;
    const/4 v0, 0x0

    .line 163
    .local v0, "d":Lcom/ex/android/util/SkuDetails;
    :try_start_1
    new-instance v0, Lcom/ex/android/util/SkuDetails;

    .end local v0    # "d":Lcom/ex/android/util/SkuDetails;
    const-string v10, "inapp"

    invoke-direct {v0, v10, v8}, Lcom/ex/android/util/SkuDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 169
    .restart local v0    # "d":Lcom/ex/android/util/SkuDetails;
    iget-object v10, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v10}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$200(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;

    move-result-object v10

    invoke-virtual {v0}, Lcom/ex/android/util/SkuDetails;->getSku()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 164
    .end local v0    # "d":Lcom/ex/android/util/SkuDetails;
    :catch_1
    move-exception v1

    .line 167
    .local v1, "e":Lorg/json/JSONException;
    goto :goto_2

    .line 172
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v8    # "thisResponse":Ljava/lang/String;
    :cond_6
    iget-object v9, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$1;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v9}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$500(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V

    .line 174
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$300()V

    goto/16 :goto_0
.end method
