.class Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;
.super Ltwitter4j/TwitterAdapter;
.source "BQTwitterHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/BQTwitterHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BQTwitterAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQTwitterHelper;


# direct methods
.method private constructor <init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;->this$0:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    invoke-direct {p0}, Ltwitter4j/TwitterAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljp/co/drecom/bisque/lib/BQTwitterHelper$1;)V
    .locals 0
    .param p1, "x0"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper;
    .param p2, "x1"    # Ljp/co/drecom/bisque/lib/BQTwitterHelper$1;

    .prologue
    .line 365
    invoke-direct {p0, p1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)V

    return-void
.end method


# virtual methods
.method public gotOAuthAccessToken(Ltwitter4j/auth/AccessToken;)V
    .locals 4
    .param p1, "token"    # Ltwitter4j/auth/AccessToken;

    .prologue
    .line 396
    invoke-virtual {p1}, Ltwitter4j/auth/AccessToken;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "_token":Ljava/lang/String;
    invoke-virtual {p1}, Ltwitter4j/auth/AccessToken;->getTokenSecret()Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "_tokenSecret":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v2

    new-instance v3, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$2;

    invoke-direct {v3, p0, v0, v1}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$2;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 404
    return-void
.end method

.method public gotOAuthRequestToken(Ltwitter4j/auth/RequestToken;)V
    .locals 3
    .param p1, "token"    # Ltwitter4j/auth/RequestToken;

    .prologue
    .line 371
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;->this$0:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    invoke-virtual {p1}, Ltwitter4j/auth/RequestToken;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->access$302(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 372
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;->this$0:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    invoke-virtual {p1}, Ltwitter4j/auth/RequestToken;->getTokenSecret()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->access$402(Ljp/co/drecom/bisque/lib/BQTwitterHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 379
    invoke-virtual {p1}, Ltwitter4j/auth/RequestToken;->getAuthenticationURL()Ljava/lang/String;

    move-result-object v0

    .line 380
    .local v0, "_authURL":Ljava/lang/String;
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v1

    new-instance v2, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$1;

    invoke-direct {v2, p0, v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$1;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 387
    return-void
.end method

.method public onException(Ltwitter4j/TwitterException;Ltwitter4j/TwitterMethod;)V
    .locals 9
    .param p1, "e"    # Ltwitter4j/TwitterException;
    .param p2, "method"    # Ltwitter4j/TwitterMethod;

    .prologue
    .line 438
    sget-object v0, Ltwitter4j/TwitterMethod;->UPDATE_STATUS:Ltwitter4j/TwitterMethod;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;->this$0:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->access$500(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 440
    const-string v7, "UPDATE_STATUS_WITH_MEDIA"

    .line 444
    .local v7, "method_str":Ljava/lang/String;
    :goto_0
    move-object v2, v7

    .line 445
    .local v2, "_method_str":Ljava/lang/String;
    invoke-virtual {p1}, Ltwitter4j/TwitterException;->getStatusCode()I

    move-result v3

    .line 446
    .local v3, "_statusCode":I
    invoke-virtual {p1}, Ltwitter4j/TwitterException;->getErrorCode()I

    move-result v4

    .line 447
    .local v4, "_errorCode":I
    invoke-virtual {p1}, Ltwitter4j/TwitterException;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 448
    .local v5, "_getMessage":Ljava/lang/String;
    invoke-virtual {p1}, Ltwitter4j/TwitterException;->isCausedByNetworkIssue()Z

    move-result v6

    .line 449
    .local v6, "_networkIssue":Z
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v8

    new-instance v0, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$5;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$5;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;Ljava/lang/String;IILjava/lang/String;Z)V

    invoke-virtual {v8, v0}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 455
    return-void

    .line 442
    .end local v2    # "_method_str":Ljava/lang/String;
    .end local v3    # "_statusCode":I
    .end local v4    # "_errorCode":I
    .end local v5    # "_getMessage":Ljava/lang/String;
    .end local v6    # "_networkIssue":Z
    .end local v7    # "method_str":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2}, Ltwitter4j/TwitterMethod;->name()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "method_str":Ljava/lang/String;
    goto :goto_0
.end method

.method public updatedStatus(Ltwitter4j/Status;)V
    .locals 2
    .param p1, "status"    # Ltwitter4j/Status;

    .prologue
    .line 410
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;->this$0:Ljp/co/drecom/bisque/lib/BQTwitterHelper;

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper;->access$500(Ljp/co/drecom/bisque/lib/BQTwitterHelper;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 413
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$3;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$3;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 430
    :goto_0
    return-void

    .line 423
    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$4;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter$4;-><init>(Ljp/co/drecom/bisque/lib/BQTwitterHelper$BQTwitterAdapter;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
