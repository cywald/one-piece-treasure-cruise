.class public Ljp/co/drecom/bisque/lib/BQSensorEventHelper;
.super Ljava/lang/Object;
.source "BQSensorEventHelper.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field static m_kInstance:Ljp/co/drecom/bisque/lib/BQSensorEventHelper;


# instance fields
.field private m_kManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;->m_kInstance:Ljp/co/drecom/bisque/lib/BQSensorEventHelper;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;->m_kManager:Landroid/hardware/SensorManager;

    .line 17
    return-void
.end method

.method private initInternal()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "sensor"

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getSystemServiceWXP(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;->m_kManager:Landroid/hardware/SensorManager;

    .line 32
    return-void
.end method

.method public static initialize()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;->m_kInstance:Ljp/co/drecom/bisque/lib/BQSensorEventHelper;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;->m_kInstance:Ljp/co/drecom/bisque/lib/BQSensorEventHelper;

    .line 22
    sget-object v0, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;->m_kInstance:Ljp/co/drecom/bisque/lib/BQSensorEventHelper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;->initInternal()V

    .line 24
    :cond_0
    return-void
.end method

.method public static self()Ljp/co/drecom/bisque/lib/BQSensorEventHelper;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Ljp/co/drecom/bisque/lib/BQSensorEventHelper;->m_kInstance:Ljp/co/drecom/bisque/lib/BQSensorEventHelper;

    return-object v0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 38
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 44
    return-void
.end method
