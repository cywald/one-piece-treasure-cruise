.class public Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;
.super Ljava/lang/Object;
.source "DRMoviePlayerManager.java"


# static fields
.field private static m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;


# instance fields
.field private m_handler:Landroid/os/Handler;

.field private m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_handler:Landroid/os/Handler;

    .line 17
    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    return-void
.end method

.method static synthetic access$000()Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    return-object v0
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;)Ljp/co/drecom/bisque/lib/DRMoviePlayer;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    .prologue
    .line 14
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    return-object v0
.end method

.method public static initialize(Landroid/app/Activity;Landroid/os/Handler;Landroid/widget/RelativeLayout;)V
    .locals 2
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 27
    new-instance v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    .line 28
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iput-object p1, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_handler:Landroid/os/Handler;

    .line 29
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    new-instance v1, Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-direct {v1, p0, p2}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;-><init>(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V

    iput-object v1, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    .line 30
    return-void
.end method

.method public static isPaused()Z
    .locals 1

    .prologue
    .line 121
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 123
    const/4 v0, 0x0

    .line 125
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->isPaused()Z

    move-result v0

    goto :goto_0
.end method

.method public static isPlaying()Z
    .locals 1

    .prologue
    .line 112
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 116
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->isPlaying()Z

    move-result v0

    goto :goto_0
.end method

.method public static onResume()V
    .locals 2

    .prologue
    .line 146
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 150
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_handler:Landroid/os/Handler;

    new-instance v1, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$5;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$5;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public static onSuspend()V
    .locals 2

    .prologue
    .line 130
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 134
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_handler:Landroid/os/Handler;

    new-instance v1, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$4;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$4;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public static pause()Z
    .locals 2

    .prologue
    .line 95
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 97
    const/4 v0, 0x0

    .line 107
    :goto_0
    return v0

    .line 99
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_handler:Landroid/os/Handler;

    new-instance v1, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$3;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$3;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 107
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static play()Z
    .locals 2

    .prologue
    .line 60
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 62
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    .line 64
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_handler:Landroid/os/Handler;

    new-instance v1, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$1;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$1;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 72
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setFile(Ljava/lang/String;)Z
    .locals 1
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    .line 34
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 38
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-virtual {v0, p0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->setFile(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static setRect(FFFF)Z
    .locals 5
    .param p0, "x"    # F
    .param p1, "y"    # F
    .param p2, "width"    # F
    .param p3, "height"    # F

    .prologue
    .line 51
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 55
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    float-to-int v1, p0

    float-to-int v2, p1

    float-to-int v3, p2

    float-to-int v4, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->setRect(IIII)Z

    move-result v0

    goto :goto_0
.end method

.method public static setUri(Ljava/lang/String;)Z
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 42
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_player:Ljp/co/drecom/bisque/lib/DRMoviePlayer;

    invoke-virtual {v0, p0}, Ljp/co/drecom/bisque/lib/DRMoviePlayer;->setUri(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static stop()Z
    .locals 2

    .prologue
    .line 78
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    if-nez v0, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    .line 82
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_instance:Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;

    iget-object v0, v0, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager;->m_handler:Landroid/os/Handler;

    new-instance v1, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$2;

    invoke-direct {v1}, Ljp/co/drecom/bisque/lib/DRMoviePlayerManager$2;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 90
    const/4 v0, 0x1

    goto :goto_0
.end method
