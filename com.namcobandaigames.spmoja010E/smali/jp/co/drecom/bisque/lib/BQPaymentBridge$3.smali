.class Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;
.super Ljava/lang/Object;
.source "BQPaymentBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljp/co/drecom/bisque/lib/BQPaymentBridge;->requestPurchases()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;


# direct methods
.method constructor <init>(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V
    .locals 0
    .param p1, "this$0"    # Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    .prologue
    .line 236
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 240
    iget-object v11, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$900(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Map;->clear()V

    .line 241
    const/4 v0, 0x0

    .line 242
    .local v0, "continueToken":Ljava/lang/String;
    iget-object v11, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v11

    if-nez v11, :cond_0

    .line 245
    const/4 v11, -0x1

    const-string v12, "Can not call getPurchases()"

    invoke-static {v11, v12}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1000(ILjava/lang/String;)V

    .line 304
    :goto_0
    return-void

    .line 250
    :cond_0
    const/4 v3, 0x0

    .line 252
    .local v3, "ownedItems":Landroid/os/Bundle;
    :try_start_0
    iget-object v11, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$000(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v11

    const/4 v12, 0x3

    iget-object v13, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v13}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$400(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v13

    const-string v14, "inapp"

    invoke-interface {v11, v12, v13, v14, v0}, Lcom/android/vending/billing/IInAppBillingService;->getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 259
    iget-object v11, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-virtual {v11, v3}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v8

    .line 261
    .local v8, "response":I
    if-eqz v8, :cond_1

    .line 263
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getPurchases() failed: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v8}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1000(ILjava/lang/String;)V

    goto :goto_0

    .line 253
    .end local v8    # "response":I
    :catch_0
    move-exception v1

    .line 255
    .local v1, "e":Landroid/os/RemoteException;
    const/4 v11, -0x1

    const-string v12, "Can not call getPurchases()"

    invoke-static {v11, v12}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1000(ILjava/lang/String;)V

    goto :goto_0

    .line 266
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v8    # "response":I
    :cond_1
    const-string v11, "INAPP_PURCHASE_ITEM_LIST"

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    const-string v11, "INAPP_PURCHASE_DATA_LIST"

    .line 267
    invoke-virtual {v3, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    const-string v11, "INAPP_DATA_SIGNATURE_LIST"

    .line 268
    invoke-virtual {v3, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 270
    :cond_2
    const-string v11, "Bundle returned from getPurchases() doesn\'t contain required fields."

    invoke-static {v8, v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1000(ILjava/lang/String;)V

    goto :goto_0

    .line 274
    :cond_3
    const-string v11, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 276
    .local v7, "purchaseDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v11, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 279
    .local v10, "signatureList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v2, v11, :cond_5

    .line 280
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 281
    .local v6, "purchaseData":Ljava/lang/String;
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 282
    .local v9, "signature":Ljava/lang/String;
    const/4 v4, 0x0

    .line 284
    .local v4, "purchase":Lcom/ex/android/util/Purchase;
    :try_start_1
    new-instance v5, Lcom/ex/android/util/Purchase;

    const-string v11, "inapp"

    invoke-direct {v5, v11, v6, v9}, Lcom/ex/android/util/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 285
    .end local v4    # "purchase":Lcom/ex/android/util/Purchase;
    .local v5, "purchase":Lcom/ex/android/util/Purchase;
    :try_start_2
    invoke-virtual {v5}, Lcom/ex/android/util/Purchase;->getToken()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v11

    if-eqz v11, :cond_4

    .line 293
    :cond_4
    iget-object v11, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$900(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)Ljava/util/Map;

    move-result-object v11

    invoke-virtual {v5}, Lcom/ex/android/util/Purchase;->getToken()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v5

    .line 279
    .end local v5    # "purchase":Lcom/ex/android/util/Purchase;
    .restart local v4    # "purchase":Lcom/ex/android/util/Purchase;
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 288
    :catch_1
    move-exception v1

    .line 291
    .local v1, "e":Lorg/json/JSONException;
    :goto_3
    goto :goto_2

    .line 296
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v4    # "purchase":Lcom/ex/android/util/Purchase;
    .end local v6    # "purchaseData":Ljava/lang/String;
    .end local v9    # "signature":Ljava/lang/String;
    :cond_5
    const-string v11, "INAPP_CONTINUATION_TOKEN"

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 298
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 300
    iget-object v11, p0, Ljp/co/drecom/bisque/lib/BQPaymentBridge$3;->this$0:Ljp/co/drecom/bisque/lib/BQPaymentBridge;

    invoke-static {v11}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1100(Ljp/co/drecom/bisque/lib/BQPaymentBridge;)V

    .line 302
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQPaymentBridge;->access$1200()V

    goto/16 :goto_0

    .line 288
    .restart local v5    # "purchase":Lcom/ex/android/util/Purchase;
    .restart local v6    # "purchaseData":Ljava/lang/String;
    .restart local v9    # "signature":Ljava/lang/String;
    :catch_2
    move-exception v1

    move-object v4, v5

    .end local v5    # "purchase":Lcom/ex/android/util/Purchase;
    .restart local v4    # "purchase":Lcom/ex/android/util/Purchase;
    goto :goto_3
.end method
