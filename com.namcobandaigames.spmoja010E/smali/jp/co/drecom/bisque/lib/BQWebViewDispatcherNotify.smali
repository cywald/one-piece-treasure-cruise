.class public Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;
.super Ljava/lang/Object;
.source "BQWebViewDispatcherNotify.java"


# static fields
.field private static dispatcher:Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

.field private static handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    return-object v0
.end method

.method public static addWebView(IIIILjava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "tag"    # I
    .param p6, "order"    # I
    .param p7, "bounces"    # I
    .param p8, "auth"    # Z
    .param p9, "username"    # Ljava/lang/String;
    .param p10, "password"    # Ljava/lang/String;

    .prologue
    .line 21
    move v1, p0

    .local v1, "_x":I
    move v2, p1

    .local v2, "_y":I
    move v3, p2

    .local v3, "_w":I
    move/from16 v4, p3

    .local v4, "_h":I
    move/from16 v6, p5

    .local v6, "_tag":I
    move/from16 v7, p6

    .local v7, "_order":I
    move/from16 v8, p7

    .line 22
    .local v8, "_bounces":I
    move-object/from16 v5, p4

    .local v5, "_url":Ljava/lang/String;
    move-object/from16 v10, p9

    .local v10, "_username":Ljava/lang/String;
    move-object/from16 v11, p10

    .line 23
    .local v11, "_password":Ljava/lang/String;
    move/from16 v9, p8

    .line 24
    .local v9, "_auth":Z
    sget-object v12, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$1;

    invoke-direct/range {v0 .. v11}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$1;-><init>(IIIILjava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 30
    const/4 v0, 0x1

    return v0
.end method

.method public static addWebView(IIIILjava/lang/String;Ljava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "post"    # Ljava/lang/String;
    .param p6, "tag"    # I
    .param p7, "order"    # I
    .param p8, "bounces"    # I
    .param p9, "auth"    # Z
    .param p10, "username"    # Ljava/lang/String;
    .param p11, "password"    # Ljava/lang/String;

    .prologue
    .line 51
    move v1, p0

    .local v1, "_x":I
    move v2, p1

    .local v2, "_y":I
    move/from16 v3, p2

    .local v3, "_w":I
    move/from16 v4, p3

    .local v4, "_h":I
    move/from16 v7, p6

    .local v7, "_tag":I
    move/from16 v8, p7

    .local v8, "_order":I
    move/from16 v9, p8

    .line 52
    .local v9, "_bounces":I
    move-object/from16 v5, p4

    .local v5, "_url":Ljava/lang/String;
    move-object/from16 v6, p5

    .local v6, "_post":Ljava/lang/String;
    move-object/from16 v11, p10

    .local v11, "_username":Ljava/lang/String;
    move-object/from16 v12, p11

    .line 53
    .local v12, "_password":Ljava/lang/String;
    move/from16 v10, p9

    .line 54
    .local v10, "_auth":Z
    sget-object v13, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$3;

    invoke-direct/range {v0 .. v12}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$3;-><init>(IIIILjava/lang/String;Ljava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public static addWebView(IIIILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)Z
    .locals 15
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "headers"    # [Ljava/lang/String;
    .param p6, "values"    # [Ljava/lang/String;
    .param p7, "tag"    # I
    .param p8, "order"    # I
    .param p9, "bounces"    # I
    .param p10, "auth"    # Z
    .param p11, "username"    # Ljava/lang/String;
    .param p12, "password"    # Ljava/lang/String;

    .prologue
    .line 35
    move v1, p0

    .local v1, "_x":I
    move/from16 v2, p1

    .local v2, "_y":I
    move/from16 v3, p2

    .local v3, "_w":I
    move/from16 v4, p3

    .local v4, "_h":I
    move/from16 v8, p7

    .local v8, "_tag":I
    move/from16 v9, p8

    .local v9, "_order":I
    move/from16 v10, p9

    .line 36
    .local v10, "_bounces":I
    move-object/from16 v5, p4

    .local v5, "_url":Ljava/lang/String;
    move-object/from16 v12, p11

    .local v12, "_username":Ljava/lang/String;
    move-object/from16 v13, p12

    .line 37
    .local v13, "_password":Ljava/lang/String;
    move-object/from16 v6, p5

    .line 38
    .local v6, "_headers":[Ljava/lang/String;
    move-object/from16 v7, p6

    .line 39
    .local v7, "_values":[Ljava/lang/String;
    move/from16 v11, p10

    .line 40
    .local v11, "_auth":Z
    sget-object v14, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$2;

    invoke-direct/range {v0 .. v13}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$2;-><init>(IIIILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IIIZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public static bouncesWebView(ZI)V
    .locals 4
    .param p0, "b"    # Z
    .param p1, "tag"    # I

    .prologue
    .line 134
    move v0, p0

    .line 135
    .local v0, "_b":Z
    move v1, p1

    .line 136
    .local v1, "_tag":I
    sget-object v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v3, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$10;

    invoke-direct {v3, v0, v1}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$10;-><init>(ZI)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 142
    return-void
.end method

.method public static clearCache(I)V
    .locals 3
    .param p0, "tag"    # I

    .prologue
    .line 112
    move v0, p0

    .line 113
    .local v0, "_tag":I
    sget-object v1, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$8;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$8;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 119
    return-void
.end method

.method public static clearCookie(I)V
    .locals 3
    .param p0, "tag"    # I

    .prologue
    .line 123
    move v0, p0

    .line 124
    .local v0, "_tag":I
    sget-object v1, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$9;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$9;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 130
    return-void
.end method

.method public static enableCache(ZI)V
    .locals 4
    .param p0, "b"    # Z
    .param p1, "tag"    # I

    .prologue
    .line 88
    move v0, p0

    .line 89
    .local v0, "_b":Z
    move v1, p1

    .line 90
    .local v1, "_tag":I
    sget-object v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v3, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$6;

    invoke-direct {v3, v0, v1}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$6;-><init>(ZI)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 96
    return-void
.end method

.method public static enableCookie(ZI)V
    .locals 4
    .param p0, "b"    # Z
    .param p1, "tag"    # I

    .prologue
    .line 100
    move v0, p0

    .line 101
    .local v0, "_b":Z
    move v1, p1

    .line 102
    .local v1, "_tag":I
    sget-object v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v3, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$7;

    invoke-direct {v3, v0, v1}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$7;-><init>(ZI)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 108
    return-void
.end method

.method public static enableWebView(ZI)V
    .locals 4
    .param p0, "b"    # Z
    .param p1, "tag"    # I

    .prologue
    .line 76
    move v0, p0

    .line 77
    .local v0, "_b":Z
    move v1, p1

    .line 78
    .local v1, "_tag":I
    sget-object v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v3, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$5;

    invoke-direct {v3, v0, v1}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$5;-><init>(ZI)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 84
    return-void
.end method

.method public static executeJsInWebView(Ljava/lang/String;I)V
    .locals 4
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "tag"    # I

    .prologue
    .line 220
    move-object v1, p0

    .line 221
    .local v1, "_url":Ljava/lang/String;
    move v0, p1

    .line 222
    .local v0, "_tag":I
    sget-object v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v3, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$17;

    invoke-direct {v3, v1, v0}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$17;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 228
    return-void
.end method

.method public static reloadWebView(I)V
    .locals 3
    .param p0, "tag"    # I

    .prologue
    .line 168
    move v0, p0

    .line 169
    .local v0, "_tag":I
    sget-object v1, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$13;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$13;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 175
    return-void
.end method

.method public static removeWebView(I)V
    .locals 3
    .param p0, "tag"    # I

    .prologue
    .line 65
    move v0, p0

    .line 66
    .local v0, "_tag":I
    sget-object v1, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$4;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$4;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 72
    return-void
.end method

.method public static requestWebView(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "tag"    # I
    .param p2, "auth"    # Z
    .param p3, "username"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    .line 179
    move v2, p1

    .line 180
    .local v2, "_tag":I
    move-object v1, p0

    .local v1, "_url":Ljava/lang/String;
    move-object v4, p3

    .local v4, "_username":Ljava/lang/String;
    move-object v5, p4

    .line 181
    .local v5, "_password":Ljava/lang/String;
    move v3, p2

    .line 182
    .local v3, "_auth":Z
    sget-object v6, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$14;

    invoke-direct/range {v0 .. v5}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$14;-><init>(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 188
    return-void
.end method

.method public static requestWebView(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "post"    # Ljava/lang/String;
    .param p2, "tag"    # I
    .param p3, "auth"    # Z
    .param p4, "username"    # Ljava/lang/String;
    .param p5, "password"    # Ljava/lang/String;

    .prologue
    .line 207
    move v3, p2

    .line 208
    .local v3, "_tag":I
    move-object v1, p0

    .local v1, "_url":Ljava/lang/String;
    move-object v2, p1

    .local v2, "_post":Ljava/lang/String;
    move-object v5, p4

    .local v5, "_username":Ljava/lang/String;
    move-object v6, p5

    .line 209
    .local v6, "_password":Ljava/lang/String;
    move v4, p3

    .line 210
    .local v4, "_auth":Z
    sget-object v7, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$16;

    invoke-direct/range {v0 .. v6}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$16;-><init>(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 216
    return-void
.end method

.method public static requestWebView(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # [Ljava/lang/String;
    .param p2, "values"    # [Ljava/lang/String;
    .param p3, "tag"    # I
    .param p4, "auth"    # Z
    .param p5, "username"    # Ljava/lang/String;
    .param p6, "password"    # Ljava/lang/String;

    .prologue
    .line 192
    move v4, p3

    .line 193
    .local v4, "_tag":I
    move-object v1, p0

    .local v1, "_url":Ljava/lang/String;
    move-object v6, p5

    .local v6, "_username":Ljava/lang/String;
    move-object v7, p6

    .line 194
    .local v7, "_password":Ljava/lang/String;
    move-object v2, p1

    .line 195
    .local v2, "_headers":[Ljava/lang/String;
    move-object v3, p2

    .line 196
    .local v3, "_values":[Ljava/lang/String;
    move v5, p4

    .line 197
    .local v5, "_auth":Z
    sget-object v8, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$15;

    invoke-direct/range {v0 .. v7}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$15;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 203
    return-void
.end method

.method public static setDispatcher(Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;Landroid/os/Handler;)V
    .locals 0
    .param p0, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;
    .param p1, "_handler"    # Landroid/os/Handler;

    .prologue
    .line 15
    sput-object p0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQWebViewDispatcher;

    .line 16
    sput-object p1, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    .line 17
    return-void
.end method

.method public static setOrderWebView(II)V
    .locals 4
    .param p0, "order"    # I
    .param p1, "tag"    # I

    .prologue
    .line 157
    move v0, p0

    .local v0, "_order":I
    move v1, p1

    .line 158
    .local v1, "_tag":I
    sget-object v2, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v3, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$12;

    invoke-direct {v3, v0, v1}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$12;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 164
    return-void
.end method

.method public static setRectWebView(IIIII)V
    .locals 7
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "tag"    # I

    .prologue
    .line 146
    move v1, p0

    .local v1, "_x":I
    move v2, p1

    .local v2, "_y":I
    move v3, p2

    .local v3, "_w":I
    move v4, p3

    .local v4, "_h":I
    move v5, p4

    .line 147
    .local v5, "_tag":I
    sget-object v6, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v0, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$11;

    invoke-direct/range {v0 .. v5}, Ljp/co/drecom/bisque/lib/BQWebViewDispatcherNotify$11;-><init>(IIIII)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 153
    return-void
.end method
