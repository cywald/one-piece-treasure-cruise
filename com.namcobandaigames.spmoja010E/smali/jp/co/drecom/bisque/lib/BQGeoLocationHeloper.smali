.class public Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;
.super Ljava/lang/Object;
.source "BQGeoLocationHeloper.java"


# static fields
.field public static final DEFAULT_USE_LAST_KNOWN_LOCATION:Z = true

.field private static self:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;


# instance fields
.field private gpsLocationListener:Landroid/location/LocationListener;

.field private m_bGpsUsable:Z

.field private m_bInitialized:Z

.field private m_bLocationUsable:Z

.field private m_bNetUsable:Z

.field private m_kCurrentLocation:Landroid/location/Location;

.field private m_kGpsListener:Landroid/location/LocationListener;

.field private m_kLocationMgr:Landroid/location/LocationManager;

.field private m_kLock:Ljava/util/concurrent/locks/Lock;

.field private m_kNetListener:Landroid/location/LocationListener;

.field private m_significantlyNewer:J

.field private m_zMinDistance:F

.field private m_zMinTime:J

.field private networkLocationListener:Landroid/location/LocationListener;

.field private useLastKnownLocation:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->self:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bGpsUsable:Z

    .line 23
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bNetUsable:Z

    .line 24
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bInitialized:Z

    .line 25
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bLocationUsable:Z

    .line 29
    iput-object v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kGpsListener:Landroid/location/LocationListener;

    .line 30
    iput-object v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kNetListener:Landroid/location/LocationListener;

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->useLastKnownLocation:Z

    .line 45
    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_significantlyNewer:J

    .line 55
    iput-object v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    .line 56
    return-void
.end method

.method static synthetic access$000(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/Location;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    .prologue
    .line 17
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kCurrentLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$100(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationListener;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    .prologue
    .line 17
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kNetListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$200(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationManager;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    .prologue
    .line 17
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$300(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Landroid/location/LocationListener;
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    .prologue
    .line 17
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kGpsListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$400(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)Z
    .locals 1
    .param p0, "x0"    # Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    .prologue
    .line 17
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->checkAndUpdateProviderStatus()Z

    move-result v0

    return v0
.end method

.method private checkAndUpdateProviderStatus()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 87
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    if-nez v2, :cond_0

    .line 101
    :goto_0
    return v0

    .line 92
    :cond_0
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    const-string v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bGpsUsable:Z

    .line 93
    iget-object v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    const-string v3, "network"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bNetUsable:Z

    .line 94
    iget-boolean v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bGpsUsable:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bNetUsable:Z

    if-nez v2, :cond_1

    .line 97
    iput-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bLocationUsable:Z

    goto :goto_0

    .line 100
    :cond_1
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bLocationUsable:Z

    move v0, v1

    .line 101
    goto :goto_0
.end method

.method private checkProviderStatus(Z)Z
    .locals 6
    .param p1, "_AllowOneLunged"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 105
    iget-object v4, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    if-nez v4, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v2

    .line 110
    :cond_1
    iget-object v4, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    const-string v5, "gps"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 111
    .local v0, "bGpsUsable":Z
    iget-object v4, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    const-string v5, "network"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 112
    .local v1, "bNetUsable":Z
    if-ne v3, p1, :cond_3

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    move v2, v3

    .line 114
    goto :goto_0

    .line 116
    :cond_3
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    move v2, v3

    .line 120
    goto :goto_0
.end method

.method public static getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->self:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;-><init>()V

    sput-object v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->self:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    .line 62
    :cond_0
    sget-object v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->self:Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    return-object v0
.end method

.method private initInternal()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 125
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bInitialized:Z

    if-ne v1, v0, :cond_0

    move v0, v1

    .line 145
    :goto_0
    return v0

    .line 129
    :cond_0
    const-string v0, "location"

    invoke-static {v0}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getSystemServiceWXP(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    .line 130
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    if-nez v0, :cond_1

    .line 132
    const/4 v0, 0x0

    goto :goto_0

    .line 135
    :cond_1
    iput-boolean v1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bInitialized:Z

    .line 136
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLock:Ljava/util/concurrent/locks/Lock;

    .line 138
    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->checkAndUpdateProviderStatus()Z

    move-result v0

    if-eq v1, v0, :cond_2

    .line 141
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->handleProviderNotAvailable()V

    :cond_2
    move v0, v1

    .line 145
    goto :goto_0
.end method

.method public static initialize()Z
    .locals 1

    .prologue
    .line 66
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 69
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    move-result-object v0

    invoke-direct {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->initInternal()Z

    move-result v0

    goto :goto_0
.end method

.method public static isAvailable()Z
    .locals 2

    .prologue
    .line 73
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    move-result-object v0

    iget-boolean v0, v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bInitialized:Z

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x0

    .line 76
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->checkProviderStatus(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static isGpsUsable()Z
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    move-result-object v0

    iget-boolean v0, v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bGpsUsable:Z

    return v0
.end method

.method public static isNetUsable()Z
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    move-result-object v0

    iget-boolean v0, v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bNetUsable:Z

    return v0
.end method

.method private isSameProvider(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "provider1"    # Ljava/lang/String;
    .param p2, "provider2"    # Ljava/lang/String;

    .prologue
    .line 322
    if-nez p1, :cond_1

    .line 323
    if-nez p2, :cond_0

    const/4 v0, 0x1

    .line 325
    :goto_0
    return v0

    .line 323
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 325
    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static native lockationChangedKallBacqk(DDF)V
.end method

.method public static pause()Z
    .locals 1

    .prologue
    .line 240
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 243
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    move-result-object v0

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->stopInternal()Z

    move-result v0

    goto :goto_0
.end method

.method public static native providerDisabledKallBacqk()V
.end method

.method public static native providerEnabledKallBacqk()V
.end method

.method public static start()Z
    .locals 1

    .prologue
    .line 195
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    const/4 v0, 0x0

    .line 198
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    move-result-object v0

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->startInternal()Z

    move-result v0

    goto :goto_0
.end method

.method public static native statusChangedKallBacqk()V
.end method

.method public static stop()Z
    .locals 1

    .prologue
    .line 236
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getInstance()Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;

    move-result-object v0

    invoke-virtual {v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->stopInternal()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getLastKnownLocation()Landroid/location/Location;
    .locals 3

    .prologue
    .line 329
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "gps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "network"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getLastKnownLocation([Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public getLastKnownLocation([Ljava/lang/String;)Landroid/location/Location;
    .locals 6
    .param p1, "providers"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 333
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    if-eqz v3, :cond_0

    if-nez p1, :cond_2

    .line 334
    :cond_0
    const/4 v2, 0x0

    .line 346
    :cond_1
    return-object v2

    .line 337
    :cond_2
    const/4 v2, 0x0

    .line 338
    .local v2, "result":Landroid/location/Location;
    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, p1, v3

    .line 339
    .local v1, "provider":Ljava/lang/String;
    if-eqz v1, :cond_3

    iget-object v5, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    invoke-virtual {v5, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 340
    iget-object v5, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    invoke-virtual {v5, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 341
    .local v0, "lastKnownLocation":Landroid/location/Location;
    invoke-virtual {p0, v2, v0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 342
    move-object v2, v0

    .line 338
    .end local v0    # "lastKnownLocation":Landroid/location/Location;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getSignificantlyNewer()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_significantlyNewer:J

    return-wide v0
.end method

.method public handleProviderDisabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 379
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$6;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$6;-><init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 387
    return-void
.end method

.method public handleProviderEnabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 392
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$7;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$7;-><init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 400
    return-void
.end method

.method public handleProviderNotAvailable()V
    .locals 0

    .prologue
    .line 418
    return-void
.end method

.method public handleStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/os/Bundle;

    .prologue
    .line 405
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$8;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$8;-><init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V

    .line 413
    return-void
.end method

.method initListeners()V
    .locals 2

    .prologue
    .line 149
    const/4 v0, 0x1

    invoke-direct {p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->checkAndUpdateProviderStatus()Z

    move-result v1

    if-eq v0, v1, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bGpsUsable:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kGpsListener:Landroid/location/LocationListener;

    if-nez v0, :cond_2

    .line 153
    new-instance v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$1;-><init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kGpsListener:Landroid/location/LocationListener;

    .line 172
    :cond_2
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_bNetUsable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kNetListener:Landroid/location/LocationListener;

    if-nez v0, :cond_0

    .line 173
    new-instance v0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$2;

    invoke-direct {v0, p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$2;-><init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kNetListener:Landroid/location/LocationListener;

    goto :goto_0
.end method

.method protected isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 14
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "currentBestLocation"    # Landroid/location/Location;

    .prologue
    .line 274
    if-nez p1, :cond_0

    .line 276
    const/4 v10, 0x0

    .line 317
    :goto_0
    return v10

    .line 279
    :cond_0
    if-nez p2, :cond_1

    .line 281
    const/4 v10, 0x1

    goto :goto_0

    .line 285
    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v10

    invoke-virtual/range {p2 .. p2}, Landroid/location/Location;->getTime()J

    move-result-wide v12

    sub-long v8, v10, v12

    .line 286
    .local v8, "timeDelta":J
    iget-wide v10, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_significantlyNewer:J

    cmp-long v10, v8, v10

    if-lez v10, :cond_2

    const/4 v6, 0x1

    .line 287
    .local v6, "isSignificantlyNewer":Z
    :goto_1
    iget-wide v10, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_significantlyNewer:J

    neg-long v10, v10

    cmp-long v10, v8, v10

    if-gez v10, :cond_3

    const/4 v7, 0x1

    .line 288
    .local v7, "isSignificantlyOlder":Z
    :goto_2
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-lez v10, :cond_4

    const/4 v4, 0x1

    .line 292
    .local v4, "isNewer":Z
    :goto_3
    if-eqz v6, :cond_5

    .line 293
    const/4 v10, 0x1

    goto :goto_0

    .line 286
    .end local v4    # "isNewer":Z
    .end local v6    # "isSignificantlyNewer":Z
    .end local v7    # "isSignificantlyOlder":Z
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 287
    .restart local v6    # "isSignificantlyNewer":Z
    :cond_3
    const/4 v7, 0x0

    goto :goto_2

    .line 288
    .restart local v7    # "isSignificantlyOlder":Z
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 295
    .restart local v4    # "isNewer":Z
    :cond_5
    if-eqz v7, :cond_6

    .line 296
    const/4 v10, 0x0

    goto :goto_0

    .line 300
    :cond_6
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v10

    invoke-virtual/range {p2 .. p2}, Landroid/location/Location;->getAccuracy()F

    move-result v11

    sub-float/2addr v10, v11

    float-to-int v0, v10

    .line 301
    .local v0, "accuracyDelta":I
    if-lez v0, :cond_7

    const/4 v2, 0x1

    .line 302
    .local v2, "isLessAccurate":Z
    :goto_4
    if-gez v0, :cond_8

    const/4 v3, 0x1

    .line 303
    .local v3, "isMoreAccurate":Z
    :goto_5
    const/16 v10, 0xc8

    if-le v0, v10, :cond_9

    const/4 v5, 0x1

    .line 306
    .local v5, "isSignificantlyLessAccurate":Z
    :goto_6
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v10

    .line 307
    invoke-virtual/range {p2 .. p2}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v11

    .line 306
    invoke-direct {p0, v10, v11}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->isSameProvider(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 310
    .local v1, "isFromSameProvider":Z
    if-eqz v3, :cond_a

    .line 311
    const/4 v10, 0x1

    goto :goto_0

    .line 301
    .end local v1    # "isFromSameProvider":Z
    .end local v2    # "isLessAccurate":Z
    .end local v3    # "isMoreAccurate":Z
    .end local v5    # "isSignificantlyLessAccurate":Z
    :cond_7
    const/4 v2, 0x0

    goto :goto_4

    .line 302
    .restart local v2    # "isLessAccurate":Z
    :cond_8
    const/4 v3, 0x0

    goto :goto_5

    .line 303
    .restart local v3    # "isMoreAccurate":Z
    :cond_9
    const/4 v5, 0x0

    goto :goto_6

    .line 312
    .restart local v1    # "isFromSameProvider":Z
    .restart local v5    # "isSignificantlyLessAccurate":Z
    :cond_a
    if-eqz v4, :cond_b

    if-nez v2, :cond_b

    .line 313
    const/4 v10, 0x1

    goto :goto_0

    .line 314
    :cond_b
    if-eqz v4, :cond_c

    if-nez v5, :cond_c

    if-eqz v1, :cond_c

    .line 315
    const/4 v10, 0x1

    goto :goto_0

    .line 317
    :cond_c
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public isUseLastKnownLocation()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->useLastKnownLocation:Z

    return v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 362
    :try_start_0
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 364
    invoke-static {}, Ljp/co/drecom/bisque/lib/HandlerJava;->getInstance()Ljp/co/drecom/bisque/lib/HandlerJava;

    move-result-object v0

    new-instance v1, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$5;

    invoke-direct {v1, p0, p1}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$5;-><init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;Landroid/location/Location;)V

    invoke-virtual {v0, v1}, Ljp/co/drecom/bisque/lib/HandlerJava;->runOnGLThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    iget-object v0, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 374
    return-void

    .line 372
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 373
    throw v0
.end method

.method public setSignificantlyNewer(J)V
    .locals 1
    .param p1, "significantlyNewer"    # J

    .prologue
    .line 48
    iput-wide p1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_significantlyNewer:J

    return-void
.end method

.method public setUseLastKnownLocation(Z)V
    .locals 0
    .param p1, "useLastKnownLocation"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->useLastKnownLocation:Z

    return-void
.end method

.method public startInternal()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 202
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    if-nez v3, :cond_0

    .line 203
    const/4 v2, 0x0

    .line 232
    :goto_0
    return v2

    .line 205
    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kCurrentLocation:Landroid/location/Location;

    .line 207
    iget-boolean v3, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->useLastKnownLocation:Z

    if-eqz v3, :cond_1

    .line 208
    invoke-virtual {p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v1

    .line 209
    .local v1, "lastKnownLocation":Landroid/location/Location;
    if-eqz v1, :cond_1

    .line 211
    invoke-virtual {p0, v1, v2}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->updateLocation(Landroid/location/Location;Z)V

    .line 215
    .end local v1    # "lastKnownLocation":Landroid/location/Location;
    :cond_1
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 217
    .local v0, "acc":Landroid/app/Activity;
    new-instance v3, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;

    invoke-direct {v3, p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$3;-><init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V

    invoke-virtual {v0, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public stopInternal()Z
    .locals 2

    .prologue
    .line 247
    iget-object v1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kLocationMgr:Landroid/location/LocationManager;

    if-nez v1, :cond_0

    .line 248
    const/4 v1, 0x0

    .line 265
    :goto_0
    return v1

    .line 250
    :cond_0
    invoke-static {}, Ljp/co/drecom/bisque/lib/BQJNIHelper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 252
    .local v0, "acc":Landroid/app/Activity;
    new-instance v1, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$4;

    invoke-direct {v1, p0}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper$4;-><init>(Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 265
    const/4 v1, 0x1

    goto :goto_0
.end method

.method updateLocation(Landroid/location/Location;Z)V
    .locals 0
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "lastKnownLocation"    # Z

    .prologue
    .line 355
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->m_kCurrentLocation:Landroid/location/Location;

    .line 356
    invoke-virtual {p0, p1}, Ljp/co/drecom/bisque/lib/BQGeoLocationHeloper;->onLocationChanged(Landroid/location/Location;)V

    .line 357
    return-void
.end method
