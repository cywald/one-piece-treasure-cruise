.class public Ljp/co/drecom/bisque/lib/DRToast;
.super Ljava/lang/Object;
.source "DRToast.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;,
        Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;
    }
.end annotation


# instance fields
.field private m_context:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Ljp/co/drecom/bisque/lib/DRToast;->m_context:Landroid/app/Activity;

    .line 76
    iput-object p1, p0, Ljp/co/drecom/bisque/lib/DRToast;->m_context:Landroid/app/Activity;

    .line 77
    return-void
.end method


# virtual methods
.method public showMessage(Ljava/lang/String;Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "anchor"    # Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;
    .param p3, "showTime"    # Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    .prologue
    const/16 v5, 0x30

    const/4 v2, 0x0

    .line 81
    sget-object v3, Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;->LONG:Ljp/co/drecom/bisque/lib/DRToast$SHOW_TIME;

    if-ne p3, v3, :cond_0

    const/4 v0, 0x1

    .line 82
    .local v0, "showTimeArg":I
    :goto_0
    iget-object v3, p0, Ljp/co/drecom/bisque/lib/DRToast;->m_context:Landroid/app/Activity;

    invoke-static {v3, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 84
    .local v1, "t":Landroid/widget/Toast;
    sget-object v3, Ljp/co/drecom/bisque/lib/DRToast$1;->$SwitchMap$jp$co$drecom$bisque$lib$DRToast$ANCHOR_POS:[I

    invoke-virtual {p2}, Ljp/co/drecom/bisque/lib/DRToast$ANCHOR_POS;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 96
    invoke-virtual {v1, v5, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 100
    :goto_1
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 101
    return-void

    .end local v0    # "showTimeArg":I
    .end local v1    # "t":Landroid/widget/Toast;
    :cond_0
    move v0, v2

    .line 81
    goto :goto_0

    .line 87
    .restart local v0    # "showTimeArg":I
    .restart local v1    # "t":Landroid/widget/Toast;
    :pswitch_0
    invoke-virtual {v1, v5, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1

    .line 90
    :pswitch_1
    const/16 v3, 0x50

    invoke-virtual {v1, v3, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1

    .line 93
    :pswitch_2
    const/16 v3, 0x11

    invoke-virtual {v1, v3, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
