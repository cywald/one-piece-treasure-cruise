.class public interface abstract Ljp/co/drecom/bisque/lib/BQGameHelper$BQGameHelperListener;
.super Ljava/lang/Object;
.source "BQGameHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljp/co/drecom/bisque/lib/BQGameHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BQGameHelperListener"
.end annotation


# virtual methods
.method public abstract onDisconnected()V
.end method

.method public abstract onInteractiveSignInFailed(Lcom/google/android/gms/common/api/Status;)V
.end method

.method public abstract onInteractiveSignInSucceeded(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
.end method

.method public abstract onSilentSignInFailed(Ljava/lang/Exception;)V
.end method

.method public abstract onSilentSignInSucceeded(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
.end method
