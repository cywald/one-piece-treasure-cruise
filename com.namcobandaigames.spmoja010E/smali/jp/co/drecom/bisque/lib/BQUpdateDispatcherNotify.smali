.class public Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify;
.super Ljava/lang/Object;
.source "BQUpdateDispatcherNotify.java"


# static fields
.field private static dispatcher:Ljp/co/drecom/bisque/lib/BQUpdateDispatcher;

.field private static handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljp/co/drecom/bisque/lib/BQUpdateDispatcher;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQUpdateDispatcher;

    return-object v0
.end method

.method public static checkUpdate(ZILjava/lang/String;)V
    .locals 5
    .param p0, "bqnotify"    # Z
    .param p1, "android_latestversion"    # I
    .param p2, "android_update_uri"    # Ljava/lang/String;

    .prologue
    .line 21
    move v2, p0

    .line 22
    .local v2, "_bqnotify":Z
    move v0, p1

    .line 23
    .local v0, "_android_latestversion":I
    move-object v1, p2

    .line 24
    .local v1, "_android_update_uri":Ljava/lang/String;
    sget-object v3, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v4, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify$1;

    invoke-direct {v4, v2, v0, v1}, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify$1;-><init>(ZILjava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 31
    return-void
.end method

.method public static execReview(Ljava/lang/String;)V
    .locals 3
    .param p0, "android_review_uri"    # Ljava/lang/String;

    .prologue
    .line 46
    move-object v0, p0

    .line 47
    .local v0, "_android_review_uri":Ljava/lang/String;
    sget-object v1, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify$3;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify$3;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 53
    return-void
.end method

.method public static execUpdate(Ljava/lang/String;)V
    .locals 3
    .param p0, "android_update_uri"    # Ljava/lang/String;

    .prologue
    .line 35
    move-object v0, p0

    .line 36
    .local v0, "_android_update_uri":Ljava/lang/String;
    sget-object v1, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify;->handler:Landroid/os/Handler;

    new-instance v2, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify$2;

    invoke-direct {v2, v0}, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify$2;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 42
    return-void
.end method

.method public static setDispatcher(Ljp/co/drecom/bisque/lib/BQUpdateDispatcher;Landroid/os/Handler;)V
    .locals 0
    .param p0, "_dispatcher"    # Ljp/co/drecom/bisque/lib/BQUpdateDispatcher;
    .param p1, "_handler"    # Landroid/os/Handler;

    .prologue
    .line 14
    sput-object p0, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify;->dispatcher:Ljp/co/drecom/bisque/lib/BQUpdateDispatcher;

    .line 15
    sput-object p1, Ljp/co/drecom/bisque/lib/BQUpdateDispatcherNotify;->handler:Landroid/os/Handler;

    .line 16
    return-void
.end method
