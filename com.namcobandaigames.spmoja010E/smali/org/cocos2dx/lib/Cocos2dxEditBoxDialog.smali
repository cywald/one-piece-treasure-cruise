.class public Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;
.super Landroid/app/Dialog;
.source "Cocos2dxEditBoxDialog.java"


# instance fields
.field private ctx:Landroid/content/Context;

.field private final kCCTextAlignmentCenter:I

.field private final kCCTextAlignmentLeft:I

.field private final kCCTextAlignmentRight:I

.field private final kEditBoxInputFlagInitialCapsAllCharacters:I

.field private final kEditBoxInputFlagInitialCapsSentence:I

.field private final kEditBoxInputFlagInitialCapsWord:I

.field private final kEditBoxInputFlagPassword:I

.field private final kEditBoxInputFlagSensitive:I

.field private final kEditBoxInputModeAny:I

.field private final kEditBoxInputModeDecimal:I

.field private final kEditBoxInputModeEmailAddr:I

.field private final kEditBoxInputModeNumeric:I

.field private final kEditBoxInputModePhoneNumber:I

.field private final kEditBoxInputModeSingleLine:I

.field private final kEditBoxInputModeUrl:I

.field private final kKeyboardReturnTypeDefault:I

.field private final kKeyboardReturnTypeDone:I

.field private final kKeyboardReturnTypeGo:I

.field private final kKeyboardReturnTypeSearch:I

.field private final kKeyboardReturnTypeSend:I

.field private mAlignment:I

.field private mDialogCocosDefault:Z

.field private mDialogFontBackgroundColor:I

.field private mDialogFontColor:I

.field private mDialogGrayout:Z

.field private mDialogPosx:I

.field private mDialogPosy:I

.field private mDialogSizeh:I

.field private mDialogSizew:I

.field private mFontColor:I

.field private mFontColorPlaceHolder:I

.field private mFontName:Ljava/lang/String;

.field private mFontNamePlaceHolder:Ljava/lang/String;

.field private mFontSize:F

.field private mFontSizePlaceHolder:F

.field private mInputEditText:Landroid/widget/EditText;

.field private final mInputFlag:I

.field private mInputFlagConstraints:I

.field private final mInputMode:I

.field private mInputModeContraints:I

.field private mIsMultiline:Z

.field private final mMaxLength:I

.field private final mMessage:Ljava/lang/String;

.field private final mReturnType:I

.field private mTextViewTitle:Landroid/widget/TextView;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 6
    .param p1, "pContext"    # Landroid/content/Context;
    .param p2, "pTitle"    # Ljava/lang/String;
    .param p3, "pMessage"    # Ljava/lang/String;
    .param p4, "pInputMode"    # I
    .param p5, "pInputFlag"    # I
    .param p6, "pReturnType"    # I
    .param p7, "pMaxLength"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 174
    const v0, 0x1030011

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 62
    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeAny:I

    .line 67
    iput v2, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeEmailAddr:I

    .line 72
    iput v3, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeNumeric:I

    .line 77
    iput v4, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModePhoneNumber:I

    .line 82
    iput v5, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeUrl:I

    .line 87
    const/4 v0, 0x5

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeDecimal:I

    .line 92
    const/4 v0, 0x6

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeSingleLine:I

    .line 97
    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagPassword:I

    .line 102
    iput v2, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagSensitive:I

    .line 107
    iput v3, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagInitialCapsWord:I

    .line 112
    iput v4, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagInitialCapsSentence:I

    .line 117
    iput v5, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagInitialCapsAllCharacters:I

    .line 119
    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeDefault:I

    .line 120
    iput v2, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeDone:I

    .line 121
    iput v3, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeSend:I

    .line 122
    iput v4, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeSearch:I

    .line 123
    iput v5, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeGo:I

    .line 128
    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kCCTextAlignmentLeft:I

    .line 129
    iput v2, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kCCTextAlignmentCenter:I

    .line 130
    iput v3, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kCCTextAlignmentRight:I

    .line 147
    const-string v0, ""

    iput-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontName:Ljava/lang/String;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontSize:F

    .line 150
    const-string v0, ""

    iput-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontNamePlaceHolder:Ljava/lang/String;

    .line 151
    const/4 v0, 0x0

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontSizePlaceHolder:F

    .line 152
    iput v4, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mAlignment:I

    .line 153
    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogPosx:I

    .line 154
    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogPosy:I

    .line 155
    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizew:I

    .line 156
    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizeh:I

    .line 158
    iput-boolean v2, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogGrayout:Z

    .line 159
    const/4 v0, -0x1

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontColor:I

    .line 160
    const v0, -0x59595a

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontColorPlaceHolder:I

    .line 161
    const/high16 v0, -0x1000000

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogFontColor:I

    .line 162
    const/4 v0, -0x1

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogFontBackgroundColor:I

    .line 163
    iput-boolean v2, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogCocosDefault:Z

    .line 177
    iput-object p2, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mTitle:Ljava/lang/String;

    .line 178
    iput-object p3, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mMessage:Ljava/lang/String;

    .line 179
    iput p4, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputMode:I

    .line 180
    iput p5, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlag:I

    .line 181
    iput p6, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mReturnType:I

    .line 182
    iput p7, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mMaxLength:I

    .line 184
    iput-object p1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->ctx:Landroid/content/Context;

    .line 185
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FIIIILjava/lang/String;FIIIIIZIIII)V
    .locals 4
    .param p1, "pContext"    # Landroid/content/Context;
    .param p2, "pTitle"    # Ljava/lang/String;
    .param p3, "pMessage"    # Ljava/lang/String;
    .param p4, "pfontName"    # Ljava/lang/String;
    .param p5, "pfontSize"    # F
    .param p6, "pInputMode"    # I
    .param p7, "pInputFlag"    # I
    .param p8, "pReturnType"    # I
    .param p9, "pMaxLength"    # I
    .param p10, "pFontNamePlaceHolder"    # Ljava/lang/String;
    .param p11, "pFontSizePlaceHolder"    # F
    .param p12, "pAlignment"    # I
    .param p13, "pDialogPosx"    # I
    .param p14, "pDialogPosy"    # I
    .param p15, "pDialogSizew"    # I
    .param p16, "pDialogSizeh"    # I
    .param p17, "pDialogGrayout"    # Z
    .param p18, "pFontColor"    # I
    .param p19, "pFontColorPlaceHolder"    # I
    .param p20, "pDialogFontColor"    # I
    .param p21, "pDialogFontBackgroundColor"    # I

    .prologue
    .line 193
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 62
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeAny:I

    .line 67
    const/4 v1, 0x1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeEmailAddr:I

    .line 72
    const/4 v1, 0x2

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeNumeric:I

    .line 77
    const/4 v1, 0x3

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModePhoneNumber:I

    .line 82
    const/4 v1, 0x4

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeUrl:I

    .line 87
    const/4 v1, 0x5

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeDecimal:I

    .line 92
    const/4 v1, 0x6

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputModeSingleLine:I

    .line 97
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagPassword:I

    .line 102
    const/4 v1, 0x1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagSensitive:I

    .line 107
    const/4 v1, 0x2

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagInitialCapsWord:I

    .line 112
    const/4 v1, 0x3

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagInitialCapsSentence:I

    .line 117
    const/4 v1, 0x4

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kEditBoxInputFlagInitialCapsAllCharacters:I

    .line 119
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeDefault:I

    .line 120
    const/4 v1, 0x1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeDone:I

    .line 121
    const/4 v1, 0x2

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeSend:I

    .line 122
    const/4 v1, 0x3

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeSearch:I

    .line 123
    const/4 v1, 0x4

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kKeyboardReturnTypeGo:I

    .line 128
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kCCTextAlignmentLeft:I

    .line 129
    const/4 v1, 0x1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kCCTextAlignmentCenter:I

    .line 130
    const/4 v1, 0x2

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->kCCTextAlignmentRight:I

    .line 147
    const-string v1, ""

    iput-object v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontName:Ljava/lang/String;

    .line 148
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontSize:F

    .line 150
    const-string v1, ""

    iput-object v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontNamePlaceHolder:Ljava/lang/String;

    .line 151
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontSizePlaceHolder:F

    .line 152
    const/4 v1, 0x3

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mAlignment:I

    .line 153
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogPosx:I

    .line 154
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogPosy:I

    .line 155
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizew:I

    .line 156
    const/4 v1, 0x0

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizeh:I

    .line 158
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogGrayout:Z

    .line 159
    const/4 v1, -0x1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontColor:I

    .line 160
    const v1, -0x59595a

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontColorPlaceHolder:I

    .line 161
    const/high16 v1, -0x1000000

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogFontColor:I

    .line 162
    const/4 v1, -0x1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogFontBackgroundColor:I

    .line 163
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogCocosDefault:Z

    .line 194
    invoke-super {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 195
    invoke-super {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 196
    invoke-super {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 198
    iput-object p1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->ctx:Landroid/content/Context;

    .line 199
    iput-object p2, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mTitle:Ljava/lang/String;

    .line 200
    iput-object p3, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mMessage:Ljava/lang/String;

    .line 201
    iput-object p4, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontName:Ljava/lang/String;

    .line 202
    iput p5, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontSize:F

    .line 203
    iput p6, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputMode:I

    .line 204
    iput p7, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlag:I

    .line 205
    iput p8, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mReturnType:I

    .line 206
    iput p9, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mMaxLength:I

    .line 208
    iput-object p10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontNamePlaceHolder:Ljava/lang/String;

    .line 209
    iput p11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontSizePlaceHolder:F

    .line 210
    move/from16 v0, p12

    invoke-direct {p0, v0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->convCCAlignmentToGravity(I)I

    move-result v1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mAlignment:I

    .line 211
    move/from16 v0, p13

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogPosx:I

    .line 212
    move/from16 v0, p14

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogPosy:I

    .line 213
    move/from16 v0, p15

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizew:I

    .line 214
    move/from16 v0, p16

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizeh:I

    .line 216
    move/from16 v0, p17

    iput-boolean v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogGrayout:Z

    .line 217
    move/from16 v0, p18

    invoke-direct {p0, v0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->convColor3BToColor(I)I

    move-result v1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontColor:I

    .line 218
    move/from16 v0, p19

    invoke-direct {p0, v0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->convColor3BToColor(I)I

    move-result v1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontColorPlaceHolder:I

    .line 219
    move/from16 v0, p20

    invoke-direct {p0, v0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->convColor3BToColor(I)I

    move-result v1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogFontColor:I

    .line 220
    move/from16 v0, p21

    invoke-direct {p0, v0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->convColor3BToColor(I)I

    move-result v1

    iput v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogFontBackgroundColor:I

    .line 221
    iget v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizew:I

    if-nez v1, :cond_0

    iget v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizeh:I

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogCocosDefault:Z

    .line 222
    return-void

    .line 221
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;)V
    .locals 0
    .param p0, "x0"    # Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->openKeyboard()V

    return-void
.end method

.method static synthetic access$200(Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;)V
    .locals 0
    .param p0, "x0"    # Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->closeKeyboard()V

    return-void
.end method

.method private closeKeyboard()V
    .locals 3

    .prologue
    .line 465
    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 466
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 467
    return-void
.end method

.method private convCCAlignmentToGravity(I)I
    .locals 2
    .param p1, "ccalignment"    # I

    .prologue
    const/4 v0, 0x3

    .line 472
    if-nez p1, :cond_1

    .line 475
    :cond_0
    :goto_0
    return v0

    .line 473
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    const/16 v0, 0x11

    goto :goto_0

    .line 474
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0
.end method

.method private convColor3BToColor(I)I
    .locals 3
    .param p1, "color3b"    # I

    .prologue
    .line 481
    const/4 v0, 0x0

    .line 482
    .local v0, "color":I
    const/high16 v1, -0x1000000

    const v2, 0xffffff

    and-int/2addr v2, p1

    or-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 483
    return v0
.end method

.method private convertDipsToPixels(F)I
    .locals 2
    .param p1, "pDIPs"    # F

    .prologue
    .line 455
    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 456
    .local v0, "scale":F
    mul-float v1, p1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method private openKeyboard()V
    .locals 3

    .prologue
    .line 460
    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 461
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 462
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "pSavedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 226
    invoke-super/range {p0 .. p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 229
    iget-boolean v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogCocosDefault:Z

    if-nez v10, :cond_0

    iget-boolean v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogCocosDefault:Z

    if-nez v10, :cond_1

    iget-boolean v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogGrayout:Z

    if-eqz v10, :cond_1

    .line 230
    :cond_0
    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->getWindow()Landroid/view/Window;

    move-result-object v10

    new-instance v11, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v12, -0x80000000

    invoke-direct {v11, v12}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v10, v11}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 233
    :cond_1
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v4, v10}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 234
    .local v4, "layout":Landroid/widget/LinearLayout;
    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 237
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->ctx:Landroid/content/Context;

    const-string v11, "window"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager;

    .line 238
    .local v9, "wm":Landroid/view/WindowManager;
    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 239
    .local v0, "disp":Landroid/view/Display;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v10

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v11

    invoke-direct {v5, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 241
    .local v5, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v10, Landroid/widget/TextView;

    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mTextViewTitle:Landroid/widget/TextView;

    .line 243
    iget-boolean v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogCocosDefault:Z

    if-eqz v10, :cond_2

    .line 245
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x2

    invoke-direct {v7, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 246
    .local v7, "textviewParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v10, 0x41200000    # 10.0f

    invoke-direct {p0, v10}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->convertDipsToPixels(F)I

    move-result v10

    iput v10, v7, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iput v10, v7, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 247
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mTextViewTitle:Landroid/widget/TextView;

    const/4 v11, 0x1

    const/high16 v12, 0x41a00000    # 20.0f

    invoke-virtual {v10, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 248
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mTextViewTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v10, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    .end local v7    # "textviewParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    new-instance v10, Landroid/widget/EditText;

    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    .line 254
    iget-boolean v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogCocosDefault:Z

    if-eqz v10, :cond_5

    .line 256
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v2, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 257
    .local v2, "editTextParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v10, 0x41200000    # 10.0f

    invoke-direct {p0, v10}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->convertDipsToPixels(F)I

    move-result v10

    iput v10, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iput v10, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 302
    :goto_0
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v4, v10, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 304
    invoke-virtual {p0, v4, v5}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 306
    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->getWindow()Landroid/view/Window;

    move-result-object v10

    const/16 v11, 0x400

    invoke-virtual {v10, v11}, Landroid/view/Window;->addFlags(I)V

    .line 308
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mTextViewTitle:Landroid/widget/TextView;

    iget-object v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mTitle:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget-object v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getImeOptions()I

    move-result v6

    .line 312
    .local v6, "oldImeOptions":I
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    const/high16 v11, 0x10000000

    or-int/2addr v11, v6

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 313
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getImeOptions()I

    move-result v6

    .line 315
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputMode:I

    packed-switch v10, :pswitch_data_0

    .line 342
    :goto_1
    iget-boolean v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mIsMultiline:Z

    if-eqz v10, :cond_3

    .line 343
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    const/high16 v11, 0x20000

    or-int/2addr v10, v11

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    .line 346
    :cond_3
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    iget v12, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlagConstraints:I

    or-int/2addr v11, v12

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setInputType(I)V

    .line 348
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlag:I

    packed-switch v10, :pswitch_data_1

    .line 368
    :goto_2
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlagConstraints:I

    iget v12, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    or-int/2addr v11, v12

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setInputType(I)V

    .line 370
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mReturnType:I

    packed-switch v10, :pswitch_data_2

    .line 387
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    or-int/lit8 v11, v6, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 391
    :goto_3
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mMaxLength:I

    if-lez v10, :cond_4

    .line 392
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    const/4 v11, 0x1

    new-array v11, v11, [Landroid/text/InputFilter;

    const/4 v12, 0x0

    new-instance v13, Landroid/text/InputFilter$LengthFilter;

    iget v14, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mMaxLength:I

    invoke-direct {v13, v14}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v13, v11, v12

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 395
    :cond_4
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 396
    .local v3, "initHandler":Landroid/os/Handler;
    new-instance v10, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog$1;

    invoke-direct {v10, p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog$1;-><init>(Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;)V

    const-wide/16 v12, 0xc8

    invoke-virtual {v3, v10, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 405
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    new-instance v11, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog$2;

    invoke-direct {v11, p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog$2;-><init>(Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;)V

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 420
    new-instance v10, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog$3;

    invoke-direct {v10, p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog$3;-><init>(Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;)V

    invoke-virtual {p0, v10}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 426
    return-void

    .line 261
    .end local v2    # "editTextParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "initHandler":Landroid/os/Handler;
    .end local v6    # "oldImeOptions":I
    :cond_5
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x2

    invoke-direct {v2, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 262
    .restart local v2    # "editTextParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontName:Ljava/lang/String;

    if-eqz v10, :cond_6

    .line 265
    const/4 v8, 0x0

    .line 266
    .local v8, "typeface":Landroid/graphics/Typeface;
    :try_start_0
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontName:Ljava/lang/String;

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 267
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontName:Ljava/lang/String;

    invoke-static {v10}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v8

    .line 272
    :goto_4
    if-eqz v8, :cond_6

    .line 274
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v10, v8}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    .end local v8    # "typeface":Landroid/graphics/Typeface;
    :cond_6
    :goto_5
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontSize:F

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_7

    .line 286
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    const/4 v11, 0x0

    iget v12, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontSize:F

    invoke-virtual {v10, v11, v12}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 288
    :cond_7
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogFontColor:I

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 289
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogPosx:I

    iput v10, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 290
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogPosy:I

    iput v10, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 292
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizew:I

    iput v10, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 293
    iget v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizeh:I

    iput v10, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 294
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogFontBackgroundColor:I

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setBackgroundColor(I)V

    .line 295
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mAlignment:I

    or-int/lit8 v11, v11, 0x10

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setGravity(I)V

    .line 296
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizew:I

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setWidth(I)V

    .line 297
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mDialogSizeh:I

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setHeight(I)V

    .line 298
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 299
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget-object v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mTitle:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    iget v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontColorPlaceHolder:I

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setHintTextColor(I)V

    goto/16 :goto_0

    .line 270
    .restart local v8    # "typeface":Landroid/graphics/Typeface;
    :cond_8
    :try_start_1
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->ctx:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v10

    iget-object v11, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontName:Ljava/lang/String;

    invoke-static {v10, v11}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto :goto_4

    .line 276
    :catch_0
    move-exception v1

    .line 277
    .local v1, "e":Ljava/lang/Exception;
    const/4 v8, 0x0

    .line 278
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mFontName:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    .line 279
    if-eqz v8, :cond_6

    .line 280
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v10, v8}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_5

    .line 317
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v8    # "typeface":Landroid/graphics/Typeface;
    .restart local v6    # "oldImeOptions":I
    :pswitch_0
    const v10, 0x20001

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    goto/16 :goto_1

    .line 320
    :pswitch_1
    const/16 v10, 0x21

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    goto/16 :goto_1

    .line 323
    :pswitch_2
    const/16 v10, 0x1002

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    goto/16 :goto_1

    .line 326
    :pswitch_3
    const/4 v10, 0x3

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    goto/16 :goto_1

    .line 329
    :pswitch_4
    const/16 v10, 0x11

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    goto/16 :goto_1

    .line 332
    :pswitch_5
    const/16 v10, 0x3002

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    goto/16 :goto_1

    .line 335
    :pswitch_6
    const/4 v10, 0x1

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputModeContraints:I

    goto/16 :goto_1

    .line 350
    :pswitch_7
    const/16 v10, 0x81

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlagConstraints:I

    goto/16 :goto_2

    .line 353
    :pswitch_8
    const/high16 v10, 0x80000

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlagConstraints:I

    goto/16 :goto_2

    .line 356
    :pswitch_9
    const/16 v10, 0x2000

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlagConstraints:I

    goto/16 :goto_2

    .line 359
    :pswitch_a
    const/16 v10, 0x4000

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlagConstraints:I

    goto/16 :goto_2

    .line 362
    :pswitch_b
    const/16 v10, 0x1000

    iput v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputFlagConstraints:I

    goto/16 :goto_2

    .line 372
    :pswitch_c
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    or-int/lit8 v11, v6, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    goto/16 :goto_3

    .line 375
    :pswitch_d
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    or-int/lit8 v11, v6, 0x6

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    goto/16 :goto_3

    .line 378
    :pswitch_e
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    or-int/lit8 v11, v6, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    goto/16 :goto_3

    .line 381
    :pswitch_f
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    or-int/lit8 v11, v6, 0x3

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    goto/16 :goto_3

    .line 384
    :pswitch_10
    iget-object v10, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    or-int/lit8 v11, v6, 0x2

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    goto/16 :goto_3

    .line 315
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 348
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 370
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 440
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 441
    iget-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/cocos2dx/lib/Cocos2dxHelper;->setEditTextDialogResult(Ljava/lang/String;)V

    .line 442
    invoke-direct {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->closeKeyboard()V

    .line 443
    invoke-virtual {p0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->dismiss()V

    .line 444
    const/4 v0, 0x1

    .line 446
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
