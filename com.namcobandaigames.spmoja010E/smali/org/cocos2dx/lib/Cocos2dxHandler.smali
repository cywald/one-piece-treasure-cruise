.class public Lorg/cocos2dx/lib/Cocos2dxHandler;
.super Landroid/os/Handler;
.source "Cocos2dxHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/cocos2dx/lib/Cocos2dxHandler$termEditing;,
        Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;,
        Lorg/cocos2dx/lib/Cocos2dxHandler$DialogMessage;
    }
.end annotation


# static fields
.field public static final HANDLER_HIDE_EDITBOX_DIALOG:I = 0x3

.field public static final HANDLER_SHOW_DIALOG:I = 0x1

.field public static final HANDLER_SHOW_EDITBOX_DIALOG:I = 0x2


# instance fields
.field private cocos2dxEditBoxDialog:Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

.field private mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lorg/cocos2dx/lib/Cocos2dxActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/cocos2dx/lib/Cocos2dxActivity;)V
    .locals 1
    .param p1, "activity"    # Lorg/cocos2dx/lib/Cocos2dxActivity;

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler;->cocos2dxEditBoxDialog:Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    .line 54
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler;->mActivity:Ljava/lang/ref/WeakReference;

    .line 55
    return-void
.end method

.method private hideEditBoxDialog()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler;->cocos2dxEditBoxDialog:Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler;->cocos2dxEditBoxDialog:Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    invoke-virtual {v0}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->dismiss()V

    .line 137
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler;->cocos2dxEditBoxDialog:Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    .line 138
    return-void
.end method

.method private showDialog(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 85
    iget-object v2, p0, Lorg/cocos2dx/lib/Cocos2dxHandler;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/cocos2dx/lib/Cocos2dxActivity;

    .line 86
    .local v1, "theActivity":Lorg/cocos2dx/lib/Cocos2dxActivity;
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lorg/cocos2dx/lib/Cocos2dxHandler$DialogMessage;

    .line 87
    .local v0, "dialogMessage":Lorg/cocos2dx/lib/Cocos2dxHandler$DialogMessage;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$DialogMessage;->titile:Ljava/lang/String;

    .line 88
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$DialogMessage;->message:Ljava/lang/String;

    .line 89
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Ok"

    new-instance v4, Lorg/cocos2dx/lib/Cocos2dxHandler$1;

    invoke-direct {v4, p0}, Lorg/cocos2dx/lib/Cocos2dxHandler$1;-><init>(Lorg/cocos2dx/lib/Cocos2dxHandler;)V

    .line 90
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 98
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 99
    return-void
.end method

.method private showEditBoxDialog(Landroid/os/Message;)V
    .locals 24
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 103
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;

    .line 104
    .local v23, "editBoxMessage":Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;
    new-instance v1, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/cocos2dx/lib/Cocos2dxHandler;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    move-object/from16 v0, v23

    iget-object v3, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->title:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v4, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->content:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v5, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontName:Ljava/lang/String;

    move-object/from16 v0, v23

    iget v6, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontSize:F

    move-object/from16 v0, v23

    iget v7, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->inputMode:I

    move-object/from16 v0, v23

    iget v8, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->inputFlag:I

    move-object/from16 v0, v23

    iget v9, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->returnType:I

    move-object/from16 v0, v23

    iget v10, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->maxLength:I

    move-object/from16 v0, v23

    iget-object v11, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontNamePlaceHolder:Ljava/lang/String;

    move-object/from16 v0, v23

    iget v12, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontSizePlaceHolder:F

    move-object/from16 v0, v23

    iget v13, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->alignment:I

    move-object/from16 v0, v23

    iget v14, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogPosx:I

    move-object/from16 v0, v23

    iget v15, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogPosy:I

    move-object/from16 v0, v23

    iget v0, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogSizew:I

    move/from16 v16, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogSizeh:I

    move/from16 v17, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogGrayout:Z

    move/from16 v18, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontColor:I

    move/from16 v19, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontColorPlaceHolder:I

    move/from16 v20, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogFontColor:I

    move/from16 v21, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogFontBackgroundColor:I

    move/from16 v22, v0

    invoke-direct/range {v1 .. v22}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FIIIILjava/lang/String;FIIIIIZIIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lorg/cocos2dx/lib/Cocos2dxHandler;->cocos2dxEditBoxDialog:Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    .line 129
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/cocos2dx/lib/Cocos2dxHandler;->cocos2dxEditBoxDialog:Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;

    invoke-virtual {v1}, Lorg/cocos2dx/lib/Cocos2dxEditBoxDialog;->show()V

    .line 130
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 70
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 82
    :goto_0
    return-void

    .line 72
    :pswitch_0
    invoke-direct {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxHandler;->showDialog(Landroid/os/Message;)V

    goto :goto_0

    .line 75
    :pswitch_1
    invoke-direct {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxHandler;->showEditBoxDialog(Landroid/os/Message;)V

    goto :goto_0

    .line 79
    :pswitch_2
    invoke-direct {p0}, Lorg/cocos2dx/lib/Cocos2dxHandler;->hideEditBoxDialog()V

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
