.class public Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;
.super Ljava/lang/Object;
.source "Cocos2dxHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/cocos2dx/lib/Cocos2dxHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EditBoxMessage"
.end annotation


# instance fields
.field public alignment:I

.field public content:Ljava/lang/String;

.field public dialogFontBackgroundColor:I

.field public dialogFontColor:I

.field public dialogGrayout:Z

.field public dialogPosx:I

.field public dialogPosy:I

.field public dialogSizeh:I

.field public dialogSizew:I

.field public fontColor:I

.field public fontColorPlaceHolder:I

.field public fontName:Ljava/lang/String;

.field public fontNamePlaceHolder:Ljava/lang/String;

.field public fontSize:F

.field public fontSizePlaceHolder:F

.field public inputFlag:I

.field public inputMode:I

.field public maxLength:I

.field public returnType:I

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FIIIILjava/lang/String;FIIIIIZIIII)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "fontName"    # Ljava/lang/String;
    .param p4, "fontSize"    # F
    .param p5, "inputMode"    # I
    .param p6, "inputFlag"    # I
    .param p7, "returnType"    # I
    .param p8, "maxLength"    # I
    .param p9, "fontNamePlaceHolder"    # Ljava/lang/String;
    .param p10, "fontSizePlaceHolder"    # F
    .param p11, "alignment"    # I
    .param p12, "dialogPosx"    # I
    .param p13, "dialogPosy"    # I
    .param p14, "dialogSizew"    # I
    .param p15, "dialogSizeh"    # I
    .param p16, "dialogGrayout"    # Z
    .param p17, "fontColor"    # I
    .param p18, "fontColorPlaceHolder"    # I
    .param p19, "dialogFontColor"    # I
    .param p20, "dialogFontBackgroundColor"    # I

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    iput-object p2, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->content:Ljava/lang/String;

    .line 182
    iput-object p1, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->title:Ljava/lang/String;

    .line 184
    iput-object p3, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontName:Ljava/lang/String;

    .line 185
    iput p4, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontSize:F

    .line 186
    iput p5, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->inputMode:I

    .line 187
    iput p6, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->inputFlag:I

    .line 188
    iput p7, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->returnType:I

    .line 189
    iput p8, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->maxLength:I

    .line 191
    iput-object p9, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontNamePlaceHolder:Ljava/lang/String;

    .line 192
    iput p10, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontSizePlaceHolder:F

    .line 193
    iput p11, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->alignment:I

    .line 194
    iput p12, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogPosx:I

    .line 195
    iput p13, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogPosy:I

    .line 196
    iput p14, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogSizew:I

    .line 197
    move/from16 v0, p15

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogSizeh:I

    .line 199
    move/from16 v0, p16

    iput-boolean v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogGrayout:Z

    .line 200
    move/from16 v0, p17

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontColor:I

    .line 201
    move/from16 v0, p18

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->fontColorPlaceHolder:I

    .line 202
    move/from16 v0, p19

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogFontColor:I

    .line 203
    move/from16 v0, p20

    iput v0, p0, Lorg/cocos2dx/lib/Cocos2dxHandler$EditBoxMessage;->dialogFontBackgroundColor:I

    .line 204
    return-void
.end method
